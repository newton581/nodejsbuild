	.file	"regexp.cc"
	.text
	.section	.text._ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi:
.LFB26261:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE26261:
	.size	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB26929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r13
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzbl	0(%r13,%r9), %r9d
	cmpl	%edx, %ecx
	jg	.L16
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L16
.L7:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	cmpb	%r9b, %cl
	jne	.L20
	testl	%edi, %edi
	js	.L3
	movslq	%eax, %r12
	movq	-48(%rbp), %rcx
	addq	%rsi, %r12
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r8, %rcx
.L8:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L3
	movzbl	(%rcx,%r12), %r11d
	leaq	-1(%rcx), %r8
	cmpb	%r11b, 0(%r13,%rcx)
	je	.L21
	cmpl	%ebx, %r15d
	jle	.L22
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L7
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$-1, %eax
.L3:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L7
	jmp	.L16
	.cfi_endproc
.LFE26929:
	.size	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB26935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	16(%rdi), %r10
	movq	8(%rdi), %r15
	movl	32(%rdi), %ebx
	leaq	43396(%rcx), %rdi
	subl	%r10d, %edx
	movl	%r10d, -84(%rbp)
	movq	%rdi, -64(%rbp)
	leal	-1(%r10), %edi
	movslq	%edi, %r8
	movl	%ebx, -44(%rbp)
	movzbl	(%r15,%r8), %r8d
	cmpl	%edx, %eax
	jg	.L40
	movl	%r10d, %r14d
	leaq	42372(%rcx), %r9
	movzbl	%r8b, %r10d
	movslq	%ebx, %rcx
	leaq	(%r9,%r10,4), %rbx
	leal	-2(%r14), %r10d
	movq	%rbx, -72(%rbp)
	movslq	%r10d, %rbx
	movl	$1, %r10d
	subq	%rcx, %r10
	movq	%rbx, -56(%rbp)
	movq	%r10, -80(%rbp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	cmpw	$255, %cx
	ja	.L27
	movl	%edi, %ebx
	subl	(%r9,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L40
.L29:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r8w, %cx
	jne	.L45
	testl	%edi, %edi
	js	.L23
	movslq	%eax, %r10
	movl	%edi, -48(%rbp)
	movq	-56(%rbp), %rcx
	leaq	(%rsi,%r10,2), %r12
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rbx, %rcx
.L30:
	leal	1(%rcx), %r14d
	movl	%ecx, %r13d
	testl	%ecx, %ecx
	js	.L23
	movzwl	(%r12,%rcx,2), %edi
	movzbl	(%r15,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%rdi, %r10
	cmpl	%edi, %r11d
	je	.L46
	movl	-48(%rbp), %edi
	cmpl	%r13d, -44(%rbp)
	jg	.L35
	movq	-64(%rbp), %rbx
	addq	-80(%rbp), %rcx
	movl	(%rbx,%rcx,4), %ecx
	cmpw	$255, %r10w
	ja	.L34
	movl	%r13d, %r14d
	subl	(%r9,%r10,4), %r14d
.L34:
	cmpl	%r14d, %ecx
	cmovl	%r14d, %ecx
	addl	%ecx, %eax
.L44:
	cmpl	%edx, %eax
	jle	.L29
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$-1, %eax
.L23:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	-72(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	jmp	.L44
.L27:
	addl	-84(%rbp), %eax
	jmp	.L44
	.cfi_endproc
.LFE26935:
	.size	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB26940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	8(%rdi), %r14
	movslq	32(%rdi), %r10
	movq	(%rdi), %rdi
	subl	%r11d, %edx
	leaq	42372(%rdi), %r9
	addq	$43396, %rdi
	movq	%rdi, -56(%rbp)
	leal	-1(%r11), %edi
	movslq	%edi, %r8
	movzwl	(%r14,%r8,2), %r8d
	cmpl	%edx, %ecx
	jg	.L60
	movl	%r11d, %ecx
	movzwl	%r8w, %r11d
	movq	%r10, %r15
	leaq	(%r9,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -64(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r10, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%edi, %ebx
	subl	(%r9,%r10,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L60
.L51:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %r10d
	cmpw	%r10w, %r8w
	jne	.L64
	testl	%edi, %edi
	js	.L47
	movslq	%eax, %r13
	movq	-48(%rbp), %rcx
	addq	%rsi, %r13
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rbx, %rcx
.L52:
	movl	%ecx, %r12d
	testl	%ecx, %ecx
	js	.L47
	movzbl	0(%r13,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%r11, %r10
	cmpw	%r11w, (%r14,%rcx,2)
	je	.L65
	cmpl	%r12d, %r15d
	jle	.L66
	movq	-64(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$-1, %eax
.L47:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	-56(%rbp), %rbx
	addq	-72(%rbp), %rcx
	subl	(%r9,%r10,4), %r12d
	cmpl	%r12d, (%rbx,%rcx,4)
	cmovge	(%rbx,%rcx,4), %r12d
	addl	%r12d, %eax
	cmpl	%eax, %edx
	jge	.L51
	jmp	.L60
	.cfi_endproc
.LFE26940:
	.size	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB26945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r12
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzwl	(%r12,%r9,2), %r9d
	cmpl	%edx, %ecx
	jg	.L80
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L84:
	movzbl	%cl, %ecx
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L80
.L71:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r9w, %cx
	jne	.L84
	testl	%edi, %edi
	js	.L67
	movslq	%eax, %r8
	movq	-48(%rbp), %rcx
	leaq	(%rsi,%r8,2), %r13
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r8, %rcx
.L72:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L67
	leaq	-1(%rcx), %r8
	movzwl	2(%r13,%r8,2), %r11d
	cmpw	%r11w, (%r12,%rcx,2)
	je	.L85
	cmpl	%ebx, %r15d
	jle	.L86
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L71
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$-1, %eax
.L67:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	movzbl	%r11b, %r11d
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L71
	jmp	.L80
	.cfi_endproc
.LFE26945:
	.size	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal15InterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD2Ev
	.type	_ZN2v88internal15InterruptsScopeD2Ev, @function
_ZN2v88internal15InterruptsScopeD2Ev:
.LFB15475:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L89
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE15475:
	.size	_ZN2v88internal15InterruptsScopeD2Ev, .-_ZN2v88internal15InterruptsScopeD2Ev
	.weak	_ZN2v88internal15InterruptsScopeD1Ev
	.set	_ZN2v88internal15InterruptsScopeD1Ev,_ZN2v88internal15InterruptsScopeD2Ev
	.section	.text._ZN2v88internal15InterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal15InterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15InterruptsScopeD0Ev
	.type	_ZN2v88internal15InterruptsScopeD0Ev, @function
_ZN2v88internal15InterruptsScopeD0Ev:
.LFB15477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L91
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L91:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE15477:
	.size	_ZN2v88internal15InterruptsScopeD0Ev, .-_ZN2v88internal15InterruptsScopeD0Ev
	.section	.text._ZN2v88internal23PostponeInterruptsScopeD2Ev,"axG",@progbits,_ZN2v88internal23PostponeInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.type	_ZN2v88internal23PostponeInterruptsScopeD2Ev, @function
_ZN2v88internal23PostponeInterruptsScopeD2Ev:
.LFB27236:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	jne	.L95
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	.cfi_endproc
.LFE27236:
	.size	_ZN2v88internal23PostponeInterruptsScopeD2Ev, .-_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.weak	_ZN2v88internal23PostponeInterruptsScopeD1Ev
	.set	_ZN2v88internal23PostponeInterruptsScopeD1Ev,_ZN2v88internal23PostponeInterruptsScopeD2Ev
	.section	.text._ZN2v88internal23PostponeInterruptsScopeD0Ev,"axG",@progbits,_ZN2v88internal23PostponeInterruptsScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.type	_ZN2v88internal23PostponeInterruptsScopeD0Ev, @function
_ZN2v88internal23PostponeInterruptsScopeD0Ev:
.LFB27238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$2, 32(%rdi)
	movq	%rax, (%rdi)
	je	.L97
	movq	8(%rdi), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L97:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27238:
	.size	_ZN2v88internal23PostponeInterruptsScopeD0Ev, .-_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.section	.text._ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB26254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r13d
	leal	1(%r13), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r14d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L105:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	(%r12,%rdx), %r14b
	je	.L99
	leal	1(%rax), %ecx
	cmpl	%eax, %r13d
	jle	.L102
.L101:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L105
.L102:
	movl	$-1, %r8d
.L99:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26254:
	.size	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB26262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movzwl	(%rax), %r15d
	cmpw	$255, %r15w
	ja	.L110
	movl	%edx, %r14d
	subl	16(%rdi), %r14d
	movq	%rsi, %r13
	movl	%r15d, %ebx
	leal	1(%r14), %r12d
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L114:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	0(%r13,%rdx), %bl
	je	.L106
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L110
.L109:
	movl	%r12d, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	0(%r13,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L114
.L110:
	movl	$-1, %r8d
.L106:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26262:
	.size	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB26258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r13d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L121:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L115
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L118
.L117:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L121
.L118:
	movl	$-1, %r8d
.L115:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26258:
	.size	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB26266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzwl	(%rax), %r13d
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	movl	%eax, %r15d
	cmova	%r13d, %r15d
	movzbl	%r15b, %r15d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L128:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L122
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L125
.L124:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L128
.L125:
	movl	$-1, %r8d
.L122:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26266:
	.size	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB21776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE21776:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB27512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27512:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB21777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21777:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB27513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE27513:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB26255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L142
	movl	%eax, %ecx
	movzbl	(%r12), %r14d
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r13d
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L156:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	(%rdx), %r13b
	je	.L143
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L142
.L144:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L156
.L142:
	movl	$-1, %r9d
.L137:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L142
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L157:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L137
.L146:
	movzbl	1(%rdx,%rax), %ecx
	cmpb	%cl, 1(%r12,%rax)
	je	.L157
	cmpl	%edi, %ebx
	jge	.L144
	jmp	.L142
	.cfi_endproc
.LFE26255:
	.size	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB26263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r13
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L163
	movl	%eax, %ecx
	movzwl	0(%r13), %eax
	movq	%rsi, %r15
	leal	1(%rbx), %esi
	movl	%esi, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r14d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L177:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	%r14b, (%rdx)
	je	.L164
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L163
.L165:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L177
.L163:
	movl	$-1, %r9d
.L158:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L163
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L178:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L158
.L167:
	movzbl	1(%rdx,%rax), %ecx
	cmpw	%cx, 2(%r13,%rax,2)
	je	.L178
	cmpl	%edi, %ebx
	jge	.L165
	jmp	.L163
	.cfi_endproc
.LFE26263:
	.size	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB26259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L184
	movzbl	0(%r13), %r14d
	movq	%rsi, %r15
	movl	%eax, %esi
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	leal	-1(%rsi), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r12d
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L198:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r15,%rdx,2), %rdx
	cmpw	%r12w, (%rdx)
	je	.L185
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L184
.L186:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r15,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L198
.L184:
	movl	$-1, %r9d
.L179:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L184
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L199:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L179
.L188:
	movzbl	1(%r13,%rax), %edi
	movzwl	2(%rdx,%rax,2), %esi
	cmpl	%esi, %edi
	je	.L199
	cmpl	%ecx, %ebx
	jge	.L186
	jmp	.L184
	.cfi_endproc
.LFE26259:
	.size	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB26267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L205
	movzwl	(%r14), %r13d
	movl	%eax, %edi
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	movslq	%ecx, %rsi
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	cmova	%r13d, %eax
	movzbl	%al, %r12d
	leal	-1(%rdi), %eax
	movl	%eax, -56(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L219:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	cmpw	%r13w, (%r15,%rdx,2)
	je	.L206
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L205
	movslq	%ecx, %rsi
.L207:
	movl	-52(%rbp), %edx
	leaq	(%r15,%rsi,2), %rdi
	movl	%r12d, %esi
	subl	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L219
.L205:
	movl	$-1, %r9d
.L200:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L205
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	movslq	%ecx, %rsi
	leaq	(%r15,%rsi,2), %rdx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L220:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L200
.L209:
	movzwl	(%rdx,%rax,2), %edi
	cmpw	%di, 2(%r14,%rax,2)
	je	.L220
	cmpl	%ecx, %ebx
	jge	.L207
	jmp	.L205
	.cfi_endproc
.LFE26267:
	.size	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.rodata._ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE:
.LFB21804:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	11(%rdx), %ebx
	cmpl	$2, %ebx
	jg	.L239
.L222:
	xorl	%eax, %eax
.L221:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L240
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	leaq	-192(%rbp), %rsi
	movq	%rdi, %r12
	movl	$16, %ecx
	cmpl	$8, %ebx
	movq	%rsi, %rdi
	leaq	.L225(%rip), %r15
	rep stosq
	movl	$8, %eax
	cmovg	%eax, %ebx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L235:
	movq	-1(%rdx), %rax
	movl	%r13d, %esi
	leaq	-1(%rdx), %rcx
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L223
	movzwl	%ax, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE,"a",@progbits
	.align 4
	.align 4
.L225:
	.long	.L231-.L225
	.long	.L228-.L225
	.long	.L230-.L225
	.long	.L226-.L225
	.long	.L223-.L225
	.long	.L224-.L225
	.long	.L223-.L225
	.long	.L223-.L225
	.long	.L229-.L225
	.long	.L228-.L225
	.long	.L227-.L225
	.long	.L226-.L225
	.long	.L223-.L225
	.long	.L224-.L225
	.section	.text._ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	-200(%rbp), %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	andl	$127, %eax
	cmpb	$0, -192(%rbp,%rax)
	jne	.L233
	addl	$1, %r14d
	movb	$1, -192(%rbp,%rax)
	leal	(%r14,%r14,2), %eax
	cmpl	%ebx, %eax
	jg	.L222
.L233:
	addq	$1, %r13
	cmpl	%r13d, %ebx
	jle	.L237
	movq	(%r12), %rdx
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	-200(%rbp), %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	-200(%rbp), %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L230:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r13,2), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L231:
	movzwl	16(%rcx,%r13,2), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L227:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r13), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L229:
	movzbl	16(%rcx,%r13), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$1, %eax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21804:
	.size	_ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE, .-_ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE
	.section	.rodata._ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/regexp/regexp.cc:136"
	.section	.text._ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE
	.type	_ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE, @function
_ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE:
.LFB21805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rdx, %r14
	leaq	.LC1(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$280, %rsp
	movq	41136(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	40952(%r12), %rax
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal16CompilationCache12LookupRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	testq	%rax, %rax
	je	.L275
	movq	(%rbx), %r13
	movq	(%rax), %r12
	movq	%r12, 23(%r13)
	leaq	23(%r13), %rsi
	testb	$1, %r12b
	je	.L263
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L276
	testb	$24, %al
	je	.L263
.L279:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L277
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%rbx, %r12
.L248:
	movq	%r15, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-312(%rbp), %rsi
	testb	$24, %al
	jne	.L279
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%ecx, %ecx
	leaq	-288(%rbp), %rdi
	movl	$255, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdx
	movq	%r12, %rsi
	leaq	-240(%rbp), %r9
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%r9, %rdi
	movq	%rax, -288(%rbp)
	movl	$1, %eax
	movq	%r9, -320(%rbp)
	movq	$0, -112(%rbp)
	movw	%ax, -104(%rbp)
	movq	$0, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	leaq	-128(%rbp), %r8
	movl	%r13d, %ecx
	movq	%r15, %rsi
	movq	-320(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE@PLT
	testb	%al, %al
	je	.L280
	cmpb	$0, -104(%rbp)
	je	.L253
	testb	$10, %r13b
	je	.L281
.L253:
	movq	-128(%rbp), %rdi
	movq	(%rdi), %rax
	call	*160(%rax)
	testb	%al, %al
	je	.L272
	movl	-80(%rbp), %r9d
	movl	%r13d, %eax
	andl	$8, %eax
	orl	%r9d, %eax
	je	.L282
.L255:
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi@PLT
.L254:
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L259
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L260:
	movq	-312(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rbx, %r12
	call	_ZN2v88internal16CompilationCache9PutRegExpENS0_6HandleINS0_6StringEEENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS2_INS0_10FixedArrayEEE@PLT
.L249:
	movq	-232(%rbp), %rax
	movq	-224(%rbp), %rdx
	cmpl	$2, -256(%rbp)
	movq	%rdx, 41704(%rax)
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rax, -288(%rbp)
	je	.L248
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L280:
	movq	-88(%rbp), %rcx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movl	$271, %esi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L259:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L283
.L261:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L272:
	movl	-80(%rbp), %r9d
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-128(%rbp), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rax), %rax
	movq	8(%rcx), %rdx
	movq	%rcx, -320(%rbp)
	movq	%rax, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewStringFromTwoByteERKNS0_6VectorIKtEENS0_14AllocationTypeE@PLT
	movq	-320(%rbp), %rcx
	testq	%rax, %rax
	je	.L284
	testb	$2, 24(%rcx)
	jne	.L272
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE
	movq	-320(%rbp), %r9
	testb	%al, %al
	jne	.L272
.L274:
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE@PLT
	jmp	.L254
.L281:
	movq	%r14, %rdi
	call	_ZN2v88internalL25HasFewDifferentCharactersENS0_6HandleINS0_6StringEEE
	movq	%r14, %r9
	testb	%al, %al
	jne	.L253
	jmp	.L274
.L283:
	movq	%r12, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L261
.L284:
	xorl	%r12d, %r12d
	jmp	.L249
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21805:
	.size	_ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE, .-_ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE
	.section	.text._ZN2v88internal10RegExpImpl11AtomCompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl11AtomCompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEES8_
	.type	_ZN2v88internal10RegExpImpl11AtomCompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEES8_, @function
_ZN2v88internal10RegExpImpl11AtomCompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEES8_:
.LFB21810:
	.cfi_startproc
	endbr64
	movq	%r8, %r9
	movl	%ecx, %r8d
	movq	%rdx, %rcx
	movl	$1, %edx
	jmp	_ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21810:
	.size	_ZN2v88internal10RegExpImpl11AtomCompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEES8_, .-_ZN2v88internal10RegExpImpl11AtomCompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEES8_
	.section	.text._ZN2v88internal10RegExpImpl24IrregexpMaxRegisterCountENS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl24IrregexpMaxRegisterCountENS0_10FixedArrayE
	.type	_ZN2v88internal10RegExpImpl24IrregexpMaxRegisterCountENS0_10FixedArrayE, @function
_ZN2v88internal10RegExpImpl24IrregexpMaxRegisterCountENS0_10FixedArrayE:
.LFB21816:
	.cfi_startproc
	endbr64
	movq	71(%rdi), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE21816:
	.size	_ZN2v88internal10RegExpImpl24IrregexpMaxRegisterCountENS0_10FixedArrayE, .-_ZN2v88internal10RegExpImpl24IrregexpMaxRegisterCountENS0_10FixedArrayE
	.section	.text._ZN2v88internal10RegExpImpl27SetIrregexpMaxRegisterCountENS0_10FixedArrayEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl27SetIrregexpMaxRegisterCountENS0_10FixedArrayEi
	.type	_ZN2v88internal10RegExpImpl27SetIrregexpMaxRegisterCountENS0_10FixedArrayEi, @function
_ZN2v88internal10RegExpImpl27SetIrregexpMaxRegisterCountENS0_10FixedArrayEi:
.LFB21817:
	.cfi_startproc
	endbr64
	salq	$32, %rsi
	movq	%rsi, 71(%rdi)
	ret
	.cfi_endproc
.LFE21817:
	.size	_ZN2v88internal10RegExpImpl27SetIrregexpMaxRegisterCountENS0_10FixedArrayEi, .-_ZN2v88internal10RegExpImpl27SetIrregexpMaxRegisterCountENS0_10FixedArrayEi
	.section	.text._ZN2v88internal10RegExpImpl25SetIrregexpCaptureNameMapENS0_10FixedArrayENS0_6HandleIS2_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl25SetIrregexpCaptureNameMapENS0_10FixedArrayENS0_6HandleIS2_EE
	.type	_ZN2v88internal10RegExpImpl25SetIrregexpCaptureNameMapENS0_10FixedArrayENS0_6HandleIS2_EE, @function
_ZN2v88internal10RegExpImpl25SetIrregexpCaptureNameMapENS0_10FixedArrayENS0_6HandleIS2_EE:
.LFB21818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	87(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L318
	movq	(%rsi), %r13
	movq	%r13, 87(%rdi)
	testb	$1, %r13b
	je	.L288
.L317:
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L319
	testb	$24, %al
	je	.L288
.L321:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L320
.L288:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L321
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L318:
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 87(%rdi)
	testb	$1, %r13b
	jne	.L317
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L320:
	popq	%rbx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE21818:
	.size	_ZN2v88internal10RegExpImpl25SetIrregexpCaptureNameMapENS0_10FixedArrayENS0_6HandleIS2_EE, .-_ZN2v88internal10RegExpImpl25SetIrregexpCaptureNameMapENS0_10FixedArrayENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal10RegExpImpl24IrregexpNumberOfCapturesENS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl24IrregexpNumberOfCapturesENS0_10FixedArrayE
	.type	_ZN2v88internal10RegExpImpl24IrregexpNumberOfCapturesENS0_10FixedArrayE, @function
_ZN2v88internal10RegExpImpl24IrregexpNumberOfCapturesENS0_10FixedArrayE:
.LFB21819:
	.cfi_startproc
	endbr64
	movq	79(%rdi), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE21819:
	.size	_ZN2v88internal10RegExpImpl24IrregexpNumberOfCapturesENS0_10FixedArrayE, .-_ZN2v88internal10RegExpImpl24IrregexpNumberOfCapturesENS0_10FixedArrayE
	.section	.text._ZN2v88internal10RegExpImpl25IrregexpNumberOfRegistersENS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl25IrregexpNumberOfRegistersENS0_10FixedArrayE
	.type	_ZN2v88internal10RegExpImpl25IrregexpNumberOfRegistersENS0_10FixedArrayE, @function
_ZN2v88internal10RegExpImpl25IrregexpNumberOfRegistersENS0_10FixedArrayE:
.LFB27511:
	.cfi_startproc
	endbr64
	movq	71(%rdi), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE27511:
	.size	_ZN2v88internal10RegExpImpl25IrregexpNumberOfRegistersENS0_10FixedArrayE, .-_ZN2v88internal10RegExpImpl25IrregexpNumberOfRegistersENS0_10FixedArrayE
	.section	.text._ZN2v88internal10RegExpImpl16IrregexpByteCodeENS0_10FixedArrayEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl16IrregexpByteCodeENS0_10FixedArrayEb
	.type	_ZN2v88internal10RegExpImpl16IrregexpByteCodeENS0_10FixedArrayEb, @function
_ZN2v88internal10RegExpImpl16IrregexpByteCodeENS0_10FixedArrayEb:
.LFB21821:
	.cfi_startproc
	endbr64
	cmpb	$1, %sil
	sbbq	%rax, %rax
	andl	$8, %eax
	addq	$56, %rax
	movq	-1(%rax,%rdi), %rax
	ret
	.cfi_endproc
.LFE21821:
	.size	_ZN2v88internal10RegExpImpl16IrregexpByteCodeENS0_10FixedArrayEb, .-_ZN2v88internal10RegExpImpl16IrregexpByteCodeENS0_10FixedArrayEb
	.section	.text._ZN2v88internal10RegExpImpl18IrregexpNativeCodeENS0_10FixedArrayEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl18IrregexpNativeCodeENS0_10FixedArrayEb
	.type	_ZN2v88internal10RegExpImpl18IrregexpNativeCodeENS0_10FixedArrayEb, @function
_ZN2v88internal10RegExpImpl18IrregexpNativeCodeENS0_10FixedArrayEb:
.LFB21822:
	.cfi_startproc
	endbr64
	cmpb	$1, %sil
	sbbq	%rax, %rax
	andl	$8, %eax
	addq	$40, %rax
	movq	-1(%rax,%rdi), %rax
	ret
	.cfi_endproc
.LFE21822:
	.size	_ZN2v88internal10RegExpImpl18IrregexpNativeCodeENS0_10FixedArrayEb, .-_ZN2v88internal10RegExpImpl18IrregexpNativeCodeENS0_10FixedArrayEb
	.section	.text._ZN2v88internal10RegExpImpl18IrregexpInitializeEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl18IrregexpInitializeEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEEi
	.type	_ZN2v88internal10RegExpImpl18IrregexpInitializeEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEEi, @function
_ZN2v88internal10RegExpImpl18IrregexpInitializeEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEEi:
.LFB21823:
	.cfi_startproc
	endbr64
	movl	%r8d, %r9d
	movl	%ecx, %r8d
	movq	%rdx, %rcx
	movl	$2, %edx
	jmp	_ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi@PLT
	.cfi_endproc
.LFE21823:
	.size	_ZN2v88internal10RegExpImpl18IrregexpInitializeEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEEi, .-_ZN2v88internal10RegExpImpl18IrregexpInitializeEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEEi
	.section	.text._ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi
	.type	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi, @function
_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi:
.LFB21829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leal	2(%rcx,%rcx), %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	%r14d, %edx
	subq	$40, %rsp
	movq	%r8, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal15RegExpMatchInfo15ReserveCapturesEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	%r14, %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	salq	$32, %rdx
	movq	%rdx, 15(%rax)
	movq	-64(%rbp), %r10
	movq	(%r12), %r13
	cmpq	%r13, (%r10)
	jne	.L377
.L332:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L343
	testl	%r14d, %r14d
	jle	.L343
	leal	-1(%r14), %ecx
	movq	%rax, %r8
	movl	$40, %eax
	shrl	%ecx
	addq	$4, %r8
	salq	$4, %rcx
	addq	$56, %rcx
	.p2align 4,,10
	.p2align 3
.L345:
	movslq	-4(%r8), %rdx
	addq	$8, %r8
	salq	$32, %rdx
	movq	%rdx, -1(%r13,%rax)
	addq	$16, %rax
	movslq	-8(%r8), %rdx
	movq	(%r12), %rsi
	salq	$32, %rdx
	movq	%rdx, -9(%rax,%rsi)
	movq	(%r12), %r13
	cmpq	%rcx, %rax
	jne	.L345
.L343:
	movq	(%rbx), %r14
	leaq	23(%r13), %rsi
	movq	%r14, 23(%r13)
	testb	$1, %r14b
	je	.L353
	movq	%r14, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L378
	testb	$24, %al
	je	.L353
.L383:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L379
	.p2align 4,,10
	.p2align 3
.L353:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r13b
	je	.L352
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L380
	testb	$24, %al
	je	.L352
.L382:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L381
.L352:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L382
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L383
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L377:
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %r10
	movq	(%rax), %rsi
.L334:
	cmpq	%rsi, (%r10)
	je	.L336
.L354:
	movq	(%r12), %r13
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L333:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L384
.L335:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L336:
	movq	12464(%r15), %rax
	movq	39(%rax), %r13
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L337
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L338:
	movq	(%r12), %rdx
	leaq	1055(%r13), %rsi
	movq	%rdx, 1055(%r13)
	testb	$1, %dl
	je	.L354
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -64(%rbp)
	testl	$262144, %eax
	jne	.L385
.L341:
	testb	$24, %al
	je	.L354
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L354
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L354
.L384:
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	jmp	.L335
.L385:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L341
.L337:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L386
.L339:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r13, (%rax)
	jmp	.L338
.L386:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L339
	.cfi_endproc
.LFE21829:
	.size	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi, .-_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi
	.section	.text._ZN2v88internal6RegExp18DotPrintForTestingEPKcPNS0_10RegExpNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6RegExp18DotPrintForTestingEPKcPNS0_10RegExpNodeE
	.type	_ZN2v88internal6RegExp18DotPrintForTestingEPKcPNS0_10RegExpNodeE, @function
_ZN2v88internal6RegExp18DotPrintForTestingEPKcPNS0_10RegExpNodeE:
.LFB21830:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal10DotPrinter8DotPrintEPKcPNS0_10RegExpNodeE@PLT
	.cfi_endproc
.LFE21830:
	.size	_ZN2v88internal6RegExp18DotPrintForTestingEPKcPNS0_10RegExpNodeE, .-_ZN2v88internal6RegExp18DotPrintForTestingEPKcPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal17RegExpGlobalCacheD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpGlobalCacheD2Ev
	.type	_ZN2v88internal17RegExpGlobalCacheD2Ev, @function
_ZN2v88internal17RegExpGlobalCacheD2Ev:
.LFB21870:
	.cfi_startproc
	endbr64
	cmpl	$128, 24(%rdi)
	jle	.L388
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L388
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L388:
	ret
	.cfi_endproc
.LFE21870:
	.size	_ZN2v88internal17RegExpGlobalCacheD2Ev, .-_ZN2v88internal17RegExpGlobalCacheD2Ev
	.globl	_ZN2v88internal17RegExpGlobalCacheD1Ev
	.set	_ZN2v88internal17RegExpGlobalCacheD1Ev,_ZN2v88internal17RegExpGlobalCacheD2Ev
	.section	.text._ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi
	.type	_ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi, @function
_ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi:
.LFB21872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leal	1(%rsi), %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rax
	btq	$36, %rax
	jnc	.L393
	movq	40(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rax), %rax
	cmpl	%r13d, 11(%rax)
	jg	.L421
.L393:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L422
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L395
	leaq	.L397(%rip), %rcx
	movzwl	%dx, %edx
	movl	%esi, %r12d
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi,"a",@progbits
	.align 4
	.align 4
.L397:
	.long	.L403-.L397
	.long	.L400-.L397
	.long	.L402-.L397
	.long	.L398-.L397
	.long	.L395-.L397
	.long	.L396-.L397
	.long	.L395-.L397
	.long	.L395-.L397
	.long	.L393-.L397
	.long	.L400-.L397
	.long	.L399-.L397
	.long	.L398-.L397
	.long	.L395-.L397
	.long	.L396-.L397
	.section	.text._ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
.L404:
	andw	$-1024, %ax
	cmpw	$-10240, %ax
	jne	.L393
	movq	40(%rbx), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L395
	leaq	.L406(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi
	.align 4
	.align 4
.L406:
	.long	.L412-.L406
	.long	.L409-.L406
	.long	.L411-.L406
	.long	.L407-.L406
	.long	.L395-.L406
	.long	.L405-.L406
	.long	.L395-.L406
	.long	.L395-.L406
	.long	.L393-.L406
	.long	.L409-.L406
	.long	.L408-.L406
	.long	.L407-.L406
	.long	.L395-.L406
	.long	.L405-.L406
	.section	.text._ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L404
.L395:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L402:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r12d, %rdx
	movzwl	(%rax,%rdx,2), %eax
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L403:
	leal	16(%rsi,%rsi), %edx
	movslq	%edx, %rdx
	movzwl	-1(%rax,%rdx), %eax
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L399:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L393
.L409:
	leaq	-48(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
.L413:
	andw	$-1024, %ax
	addl	$2, %r12d
	cmpw	$-9216, %ax
	cmove	%r12d, %r13d
	jmp	.L393
.L405:
	leaq	-48(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	jmp	.L413
.L407:
	leaq	-48(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L413
.L408:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L393
.L411:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rdx
	movzwl	(%rax,%rdx,2), %eax
	jmp	.L413
.L412:
	leal	18(%r12,%r12), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L413
.L422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21872:
	.size	_ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi, .-_ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi
	.section	.text._ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv
	.type	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv, @function
_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv:
.LFB21874:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	8(%rdi), %eax
	imull	%edx, %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	cmove	%edx, %eax
	movq	16(%rdi), %rdx
	cltq
	leaq	(%rdx,%rax,4), %rax
	ret
	.cfi_endproc
.LFE21874:
	.size	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv, .-_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv
	.section	.text._ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE
	.type	_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE, @function
_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE:
.LFB21875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L431
	movq	%rsi, %rbx
	movq	%rdx, %r14
	movq	%rcx, %r13
	cmpl	$1, %r8d
	je	.L441
	movq	-32960(%rdi), %r12
.L430:
	movl	7(%rbx), %eax
	testb	$1, %al
	jne	.L432
	shrl	$2, %eax
.L433:
	andl	$252, %eax
	leaq	-1(%r12), %rdi
	leal	16(,%rax,8), %esi
	movslq	%esi, %rsi
	movq	-1(%r12,%rsi), %rsi
	cmpq	%rsi, %rbx
	je	.L434
.L436:
	addl	$4, %eax
	movzbl	%al, %eax
	leal	16(,%rax,8), %esi
	movslq	%esi, %rsi
	movq	(%rsi,%rdi), %rsi
	cmpq	%rsi, %rbx
	jne	.L431
	leal	24(,%rax,8), %esi
	movslq	%esi, %rsi
	movq	(%rsi,%rdi), %rsi
	cmpq	%rsi, %r14
	je	.L437
	.p2align 4,,10
	.p2align 3
.L431:
	xorl	%eax, %eax
.L427:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L442
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	leaq	-48(%rbp), %rdi
	movq	%rbx, -48(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L441:
	testb	$1, %dl
	je	.L431
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L431
	movq	-32968(%rdi), %r12
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L434:
	leal	24(,%rax,8), %esi
	movslq	%esi, %rsi
	movq	-1(%r12,%rsi), %rsi
	cmpq	%rsi, %r14
	jne	.L436
.L437:
	leal	40(,%rax,8), %edx
	leal	32(,%rax,8), %eax
	movslq	%edx, %rdx
	cltq
	movq	(%rdx,%rdi), %rdx
	movq	%rdx, 0(%r13)
	movq	(%rax,%rdi), %rax
	jmp	.L427
.L442:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21875:
	.size	_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE, .-_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE
	.section	.text._ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE
	.type	_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE, @function
_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE:
.LFB21876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	(%rsi), %rdx
	movq	%r8, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L443
	movq	%rdi, %rbx
	movq	%rsi, %r10
	movq	%rcx, %r12
	movl	%r9d, %r14d
	leaq	4632(%rdi), %r15
	cmpl	$1, %r9d
	je	.L656
.L448:
	movl	7(%rdx), %r13d
	testb	$1, %r13b
	jne	.L449
	shrl	$2, %r13d
.L450:
	andl	$252, %r13d
	movq	(%r15), %rdi
	leal	16(,%r13,8), %eax
	movslq	%eax, %rcx
	movl	%eax, -88(%rbp)
	movq	-1(%rcx,%rdi), %rax
	cmpq	%rax, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L655
	leal	4(%r13), %r8d
	movzbl	%r8b, %r8d
	leal	16(,%r8,8), %r11d
	movslq	%r11d, %rax
	movq	-1(%rax,%rdi), %rsi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	cmpq	%rdx, %rsi
	je	.L657
	leaq	-1(%rax,%rdi), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L529
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -96(%rbp)
	testl	$262144, %r9d
	je	.L479
	movq	%r10, -152(%rbp)
	movl	%r11d, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rax
	movl	-128(%rbp), %r8d
	movq	-152(%rbp), %r10
	movl	-144(%rbp), %r11d
	movq	8(%rax), %r9
	movq	-136(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
.L479:
	andl	$24, %r9d
	je	.L529
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L658
	.p2align 4,,10
	.p2align 3
.L529:
	movq	(%r15), %rdi
	leal	24(,%r8,8), %eax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L528
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -96(%rbp)
	testl	$262144, %r9d
	je	.L482
	movq	%r10, -152(%rbp)
	movl	%r11d, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rax
	movl	-128(%rbp), %r8d
	movq	-152(%rbp), %r10
	movl	-144(%rbp), %r11d
	movq	8(%rax), %r9
	movq	-136(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
.L482:
	andl	$24, %r9d
	je	.L528
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L659
	.p2align 4,,10
	.p2align 3
.L528:
	movq	(%r15), %rdi
	leal	16(%r11), %eax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L527
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -96(%rbp)
	testl	$262144, %r9d
	je	.L485
	movq	%r10, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rax
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %rcx
	movl	-128(%rbp), %r8d
	movq	8(%rax), %r9
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
.L485:
	andl	$24, %r9d
	je	.L527
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L660
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%r15), %rdi
	leal	40(,%r8,8), %eax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L526
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r8
	movq	%rax, -96(%rbp)
	testl	$262144, %r8d
	je	.L488
	movq	%r10, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rax
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	8(%rax), %r8
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
.L488:
	andl	$24, %r8d
	je	.L526
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L661
	.p2align 4,,10
	.p2align 3
.L526:
	movq	(%r15), %rdi
.L655:
	movq	(%r10), %rdx
	leaq	-1(%rdi,%rcx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L525
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r8
	movq	%rax, -96(%rbp)
	testl	$262144, %r8d
	jne	.L662
	andl	$24, %r8d
	je	.L525
.L669:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L525
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L525:
	movq	-72(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %rdx
	leal	24(,%r13,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L524
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r8
	movq	%rax, -72(%rbp)
	testl	$262144, %r8d
	jne	.L663
	andl	$24, %r8d
	je	.L524
.L668:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L524
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L524:
	movl	-88(%rbp), %eax
	movq	(%r15), %rdi
	movq	(%r12), %rdx
	addl	$16, %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L523
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -72(%rbp)
	testl	$262144, %eax
	jne	.L664
	testb	$24, %al
	je	.L523
.L671:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L523
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L523:
	movq	-80(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %r15
	leal	40(,%r13,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %r13
	movq	%r15, 0(%r13)
	testb	$1, %r15b
	je	.L464
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L665
	testb	$24, %al
	je	.L464
.L670:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L464
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L464:
	movq	(%r12), %rax
	cmpl	$1, %r14d
	je	.L666
.L504:
	movq	160(%rbx), %rdx
	movq	%rdx, -1(%rax)
.L443:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L667
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	testb	$1, %al
	je	.L443
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L443
	leaq	4624(%rdi), %r15
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	-64(%rbp), %rdi
	movq	%r10, -88(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-88(%rbp), %r10
	movl	%eax, %r13d
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rax), %r8
	andl	$24, %r8d
	jne	.L668
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	8(%rax), %r8
	andl	$24, %r8d
	jne	.L669
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L670
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%r8), %rax
	testb	$24, %al
	jne	.L671
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L666:
	movslq	11(%rax), %rdx
	cmpq	$99, %rdx
	jg	.L504
	testq	%rdx, %rdx
	jle	.L504
	movl	$15, %r15d
	xorl	%r13d, %r13d
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r14, %rsi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rdx
	movq	(%rax), %r14
	movq	%rax, %rsi
	movq	-1(%r14), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L672
.L508:
	movq	(%r12), %rdi
	movq	(%rsi), %r14
	leaq	-1(%rdi,%rdx), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L513
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	je	.L510
	movq	%r14, %rdx
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
.L510:
	testb	$24, %al
	je	.L513
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L513
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L513:
	movq	(%r12), %rax
	addl	$1, %r13d
	addq	$8, %r15
	cmpl	%r13d, 11(%rax)
	jle	.L504
.L512:
	movq	(%rax,%r15), %r14
	movq	41112(%rbx), %rdi
	leaq	1(%r15), %rdx
	testq	%rdi, %rdi
	jne	.L673
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L674
.L507:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	movq	-1(%r14), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L508
.L672:
	movq	%rbx, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L657:
	movq	(%r10), %r13
	leaq	-1(%rax,%rdi), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L521
	movq	%r13, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -88(%rbp)
	testl	$262144, %edx
	jne	.L675
.L467:
	andl	$24, %edx
	je	.L521
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L521
	movq	%r13, %rdx
	movl	%r11d, -96(%rbp)
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-96(%rbp), %r11d
	movl	-88(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L521:
	movq	-72(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %r13
	leal	24(,%r8,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L520
	movq	%r13, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -72(%rbp)
	testl	$262144, %edx
	jne	.L676
.L470:
	andl	$24, %edx
	je	.L520
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L520
	movq	%r13, %rdx
	movl	%r11d, -88(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-88(%rbp), %r11d
	movl	-72(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L520:
	movq	(%r15), %rdi
	leal	16(%r11), %eax
	movq	(%r12), %r13
	cltq
	leaq	-1(%rdi,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L519
	movq	%r13, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -72(%rbp)
	testl	$262144, %eax
	jne	.L677
.L473:
	testb	$24, %al
	je	.L519
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L519
	movq	%r13, %rdx
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-72(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L519:
	movq	-80(%rbp), %rax
	movq	(%r15), %rdi
	movq	(%rax), %r13
	leal	40(,%r8,8), %eax
	cltq
	leaq	-1(%rdi,%rax), %r15
	movq	%r13, (%r15)
	testb	$1, %r13b
	je	.L464
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L678
.L476:
	testb	$24, %al
	je	.L464
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L464
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L674:
	movq	%rbx, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	8(%rcx), %rax
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L677:
	movq	%r13, %rdx
	movl	%r8d, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r9
	movl	-104(%rbp), %r8d
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	8(%r9), %rax
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L676:
	movq	%r13, %rdx
	movl	%r11d, -112(%rbp)
	movl	%r8d, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movl	-112(%rbp), %r11d
	movl	-104(%rbp), %r8d
	movq	-96(%rbp), %rsi
	movq	8(%rax), %rdx
	movq	-88(%rbp), %rdi
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%r13, %rdx
	movl	%r11d, -120(%rbp)
	movl	%r8d, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rax
	movl	-120(%rbp), %r11d
	movl	-112(%rbp), %r8d
	movq	-104(%rbp), %rsi
	movq	8(%rax), %rdx
	movq	-96(%rbp), %rdi
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L658:
	movq	%r10, -120(%rbp)
	movl	%r11d, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%r8d, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %r10
	movl	-112(%rbp), %r11d
	movq	-104(%rbp), %rcx
	movl	-96(%rbp), %r8d
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L659:
	movq	%r10, -120(%rbp)
	movl	%r11d, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%r8d, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %r10
	movl	-112(%rbp), %r11d
	movq	-104(%rbp), %rcx
	movl	-96(%rbp), %r8d
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%r10, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%r8d, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rcx
	movl	-96(%rbp), %r8d
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L661:
	movq	%r10, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %rcx
	jmp	.L526
.L667:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21876:
	.size	_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE, .-_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE
	.section	.text._ZN2v88internal18RegExpResultsCache5ClearENS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18RegExpResultsCache5ClearENS0_10FixedArrayE
	.type	_ZN2v88internal18RegExpResultsCache5ClearENS0_10FixedArrayE, @function
_ZN2v88internal18RegExpResultsCache5ClearENS0_10FixedArrayE:
.LFB21877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN2v88internal3Smi5kZeroE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	2063(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	15(%rdi), %rbx
	subq	$24, %rsp
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L683:
	movq	(%r15), %r12
	movq	%r12, (%rbx)
	testb	$1, %r12b
	je	.L684
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L681
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	8(%rcx), %rax
.L681:
	testb	$24, %al
	je	.L684
	movq	-64(%rbp), %rax
	testb	$24, 8(%rax)
	jne	.L684
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L684:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L683
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21877:
	.size	_ZN2v88internal18RegExpResultsCache5ClearENS0_10FixedArrayE, .-_ZN2v88internal18RegExpResultsCache5ClearENS0_10FixedArrayE
	.section	.text._ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	.type	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE, @function
_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE:
.LFB23994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	12(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rdi), %ecx
	cmpl	%ecx, %eax
	jge	.L694
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%rdi), %rax
	movl	%edx, 12(%rdi)
	movdqu	(%rsi), %xmm1
	movups	%xmm1, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	leal	1(%rcx,%rcx), %r14d
	movq	24(%rdx), %rax
	movq	16(%rdx), %rcx
	movq	(%rsi), %r12
	movq	8(%rsi), %r13
	movslq	%r14d, %rsi
	salq	$4, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L700
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdx)
.L697:
	movslq	12(%rbx), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jg	.L701
.L698:
	movq	%r12, %xmm0
	movq	%r13, %xmm2
	addl	$1, %eax
	movq	%rcx, (%rbx)
	punpcklqdq	%xmm2, %xmm0
	movl	%r14d, 8(%rbx)
	movl	%eax, 12(%rbx)
	movups	%xmm0, (%rcx,%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L701:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movslq	12(%rbx), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rax
	salq	$4, %rdx
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L700:
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L697
	.cfi_endproc
.LFE23994:
	.size	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE, .-_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	.section	.rodata._ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b.str1.1,"aMS",@progbits,1
.LC2:
	.string	"RegExp too big"
.LC3:
	.string	"(location_) != nullptr"
.LC4:
	.string	"Check failed: %s."
.LC5:
	.string	"ab"
	.section	.rodata._ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b.str1.1
.LC7:
	.string	"Stack overflow"
.LC8:
	.string	"Aborting on stack overflow"
	.section	.text._ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b
	.type	_ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b, @function
_ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b:
.LFB21833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1688, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movl	%ecx, -1664(%rbp)
	movq	%rdi, -1624(%rbp)
	movl	48(%rdx), %ecx
	movq	%rsi, -1640(%rbp)
	movq	%rdx, -1616(%rbp)
	movq	%r8, -1656(%rbp)
	movl	%eax, -1648(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	2(%rcx,%rcx), %eax
	cmpl	$65536, %eax
	jg	.L869
	movl	-1664(%rbp), %eax
	movq	%rsi, %rdx
	movq	%r9, %r12
	movq	-1624(%rbp), %rsi
	leaq	-1168(%rbp), %r15
	movq	%r9, -1608(%rbp)
	movl	%eax, %ebx
	movq	%r15, %rdi
	andl	$8, %ebx
	movl	%ebx, -1668(%rbp)
	movl	%eax, %ebx
	andl	$16, %eax
	movl	%eax, -1680(%rbp)
	movzbl	-1648(%rbp), %eax
	andl	$1, %ebx
	movb	%bl, -1673(%rbp)
	movl	%eax, %r8d
	movl	%eax, -1672(%rbp)
	call	_ZN2v88internal14RegExpCompilerC1EPNS0_7IsolateEPNS0_4ZoneEib@PLT
	movzbl	-1117(%rbp), %eax
	testb	%al, %al
	jne	.L870
.L706:
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %r13
	cmpw	$63, 11(%rdx)
	jbe	.L871
.L709:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	jbe	.L718
	movq	-1608(%rbp), %rax
	movq	(%rax), %rsi
.L717:
	movl	11(%rsi), %eax
	leal	-128(%rax), %edx
	movl	%edx, %r8d
	shrl	$31, %r8d
	addl	%edx, %r8d
	movl	$0, %edx
	sarl	%r8d
	cmovs	%edx, %r8d
	movslq	%r8d, %rbx
	movl	%r8d, %r13d
	cmpl	%eax, %r8d
	jge	.L739
	leaq	-1552(%rbp), %rax
	movl	$1, %r12d
	leaq	.L730(%rip), %r14
	movq	%rax, -1632(%rbp)
	movq	%r15, %rax
	subl	%r8d, %r12d
	movq	%rbx, %r15
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L738:
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %rdi
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L728
	movzwl	%ax, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b,"a",@progbits
	.align 4
	.align 4
.L730:
	.long	.L736-.L730
	.long	.L733-.L730
	.long	.L735-.L730
	.long	.L731-.L730
	.long	.L728-.L730
	.long	.L729-.L730
	.long	.L728-.L730
	.long	.L728-.L730
	.long	.L734-.L730
	.long	.L733-.L730
	.long	.L732-.L730
	.long	.L731-.L730
	.long	.L728-.L730
	.long	.L729-.L730
	.section	.text._ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b
	.p2align 4,,10
	.p2align 3
.L871:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L709
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L712
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L712
	movq	-1624(%rbp), %rdi
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, -1608(%rbp)
	movq	(%rax), %rsi
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-1632(%rbp), %rdi
	movq	%rsi, -1552(%rbp)
	movl	%r13d, %esi
	call	_ZN2v88internal10ThinString3GetEi@PLT
	.p2align 4,,10
	.p2align 3
.L737:
	andl	$127, %eax
	addl	$1, %r13d
	addl	$1, 60(%rbx,%rax,8)
	movq	-1608(%rbp), %rax
	addl	$1, -84(%rbp)
	movq	(%rax), %rsi
	leal	(%r12,%r15), %eax
	cmpl	$128, %eax
	setne	%dil
	cmpl	11(%rsi), %r13d
	setl	%al
	addq	$1, %r15
	testb	%al, %dil
	jne	.L738
	movq	%rbx, %r15
.L739:
	movq	-1616(%rbp), %r14
	movq	-1168(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	(%r14), %rdi
	call	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_10RegExpTreeEiPNS0_14RegExpCompilerEPNS0_10RegExpNodeE@PLT
	movq	(%r14), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	(%r14), %rdi
	movb	%al, -1632(%rbp)
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r14), %rdi
	movl	%eax, %ebx
	movq	(%rdi), %rax
	call	*64(%rax)
	movl	%eax, -1684(%rbp)
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	sete	%r14b
	xorb	$1, %bl
	movb	%bl, -1608(%rbp)
	je	.L727
	testb	%r14b, %r14b
	je	.L727
	movq	-1616(%rbp), %rax
	movzbl	25(%rax), %r12d
	movq	-1640(%rbp), %rax
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rax, -1696(%rbp)
	subq	%rcx, %rax
	cmpq	$31, %rax
	jbe	.L872
	movq	-1640(%rbp), %rdx
	leaq	32(%rcx), %rax
	movq	%rax, 16(%rdx)
.L741:
	subq	$8, %rsp
	movl	$42, %r11d
	movq	%r15, %r8
	xorl	%edx, %edx
	movw	%r11w, 16(%rcx)
	xorl	%edi, %edi
	movq	%r13, %r9
	movl	$2147483647, %esi
	leaq	16+_ZTVN2v88internal20RegExpCharacterClassE(%rip), %rbx
	movq	$0, 8(%rcx)
	movq	%rbx, (%rcx)
	movq	$0, 24(%rcx)
	pushq	%r12
	call	_ZN2v88internal16RegExpQuantifier6ToNodeEiibPNS0_10RegExpTreeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeEb@PLT
	popq	%r12
	movq	%rax, %r8
	popq	%rax
	movq	-1616(%rbp), %rax
	cmpb	$0, 25(%rax)
	jne	.L873
	movq	%r8, %r13
.L727:
	cmpb	$0, -1648(%rbp)
	je	.L761
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movl	$100, %esi
	call	*80(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L767
	movq	(%rax), %rax
	movl	$100, %esi
	call	*80(%rax)
	movq	%rax, %r13
.L765:
	testq	%r13, %r13
	je	.L767
.L768:
	movq	-1616(%rbp), %rax
	movl	-1672(%rbp), %esi
	movq	%r13, %rdx
	movq	-1624(%rbp), %rdi
	movq	%r13, 8(%rax)
	call	_ZN2v88internal13AnalyzeRegExpEPNS0_7IsolateEbPNS0_10RegExpNodeE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L874
	movq	-1616(%rbp), %rax
	cmpl	$1, 56(%rax)
	jne	.L772
	xorl	%ebx, %ebx
	movl	$696, %edi
	cmpb	$0, -1648(%rbp)
	sete	%bl
	call	_Znwm@PLT
	addl	$1, %ebx
	movq	-1640(%rbp), %rdx
	movq	-1624(%rbp), %rsi
	movq	%rax, %r12
	movq	-1616(%rbp), %rax
	movl	%ebx, %ecx
	movq	%r12, %rdi
	movl	48(%rax), %eax
	leal	2(%rax,%rax), %r8d
	call	_ZN2v88internal23RegExpMacroAssemblerX64C1EPNS0_7IsolateEPNS0_4ZoneENS0_26NativeRegExpMacroAssembler4ModeEi@PLT
.L774:
	movq	-1656(%rbp), %rax
	movq	(%rax), %rdx
	movl	$1, %eax
	cmpl	$20480, 11(%rdx)
	jg	.L775
	movq	-1624(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	$1048576, 45736(%rcx)
	ja	.L875
.L775:
	cmpb	$0, -1608(%rbp)
	movb	%al, 8(%r12)
	je	.L776
	cmpb	$0, -1632(%rbp)
	je	.L776
	movl	-1684(%rbp), %esi
	cmpl	$1023, %esi
	jg	.L776
	testb	%r14b, %r14b
	je	.L776
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*328(%rax)
.L776:
	cmpb	$0, -1673(%rbp)
	je	.L777
	movq	-1616(%rbp), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jle	.L876
.L778:
	movl	%eax, 12(%r12)
.L777:
	movq	-1616(%rbp), %rbx
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r15, %rsi
	subq	$8, %rsp
	pushq	-1656(%rbp)
	movq	-1624(%rbp), %rdx
	leaq	-1552(%rbp), %rdi
	movl	48(%rbx), %r9d
	call	_ZN2v88internal14RegExpCompiler8AssembleEPNS0_7IsolateEPNS0_20RegExpMacroAssemblerEPNS0_10RegExpNodeEiNS0_6HandleINS0_6StringEEE@PLT
	movq	-1544(%rbp), %rax
	popq	%rdx
	cmpb	$0, _ZN2v88internal22FLAG_print_regexp_codeE(%rip)
	movq	-1552(%rbp), %r13
	movq	%rax, -1608(%rbp)
	popq	%rcx
	je	.L779
	cmpl	$1, 56(%rbx)
	je	.L877
.L779:
	cmpb	$0, _ZN2v88internal26FLAG_print_regexp_bytecodeE(%rip)
	je	.L790
	movq	-1616(%rbp), %rax
	movl	56(%rax), %eax
	testl	%eax, %eax
	jne	.L790
	movq	-1624(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L791
	movq	-1608(%rbp), %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L792:
	movq	-1656(%rbp), %rax
	xorl	%r8d, %r8d
	leaq	-1568(%rbp), %r14
	leaq	-1584(%rbp), %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	(%rax), %rax
	leaq	-1504(%rbp), %rbx
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-1584(%rbp), %r8
	leaq	-1520(%rbp), %rax
	movq	%rbx, -1520(%rbp)
	movq	%rax, -1632(%rbp)
	testq	%r8, %r8
	jne	.L878
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L731:
	movq	-1632(%rbp), %rdi
	movq	%rsi, -1552(%rbp)
	movl	%r13d, %esi
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L733:
	movq	-1632(%rbp), %rdi
	movq	%rsi, -1552(%rbp)
	movl	%r13d, %esi
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L736:
	leal	16(%r13,%r13), %eax
	cltq
	movzwl	(%rax,%rdi), %eax
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L735:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r15,2), %eax
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L732:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r15), %eax
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L734:
	leal	16(%r13), %eax
	cltq
	movzbl	(%rax,%rdi), %eax
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L718:
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	movq	-1608(%rbp), %rax
	jne	.L867
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	movq	-1624(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L721
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -1608(%rbp)
.L867:
	movq	(%rax), %rsi
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L870:
	movq	-1656(%rbp), %rcx
	movq	(%rcx), %rdx
	cmpl	$20480, 11(%rdx)
	jg	.L807
	movq	-1624(%rbp), %rcx
	cmpq	$1048576, 45736(%rcx)
	ja	.L879
.L707:
	movb	%al, -1117(%rbp)
	jmp	.L706
.L878:
	movq	%r8, %rdi
	movq	%r8, -1640(%rbp)
	call	strlen@PLT
	movq	-1640(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -1568(%rbp)
	movq	%rax, %r9
	ja	.L880
	cmpq	$1, %rax
	jne	.L795
	movzbl	(%r8), %edx
	movb	%dl, -1504(%rbp)
	movq	%rbx, %rdx
.L796:
	movq	%rax, -1512(%rbp)
	movq	-1632(%rbp), %rsi
	movb	$0, (%rdx,%rax)
	movq	(%r15), %rdi
	call	_ZN2v88internal19IrregexpInterpreter11DisassembleENS0_9ByteArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1520(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L790
	call	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L790:
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	je	.L799
	movl	$15, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L881
.L800:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	-1624(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-1568(%rbp), %rsi
	movq	%r13, -1568(%rbp)
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L771
	movq	-1616(%rbp), %rcx
	movq	%rax, 40(%rcx)
.L801:
	movq	-1616(%rbp), %rcx
	movq	-1608(%rbp), %rax
	testq	%r13, %r13
	movq	%r12, %rdi
	sete	%r13b
	movq	%rax, 16(%rcx)
	movl	-1536(%rbp), %eax
	movl	%eax, 52(%rcx)
	movq	(%r12), %rax
	call	*8(%rax)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L869:
	leaq	.LC2(%rip), %rax
	xorl	%edx, %edx
	leaq	-1600(%rbp), %rsi
	movq	$14, -1592(%rbp)
	movq	%rax, -1600(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L771
.L868:
	movq	-1616(%rbp), %rcx
	xorl	%r13d, %r13d
	movq	%rax, 40(%rcx)
.L702:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L882
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	movl	-1680(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L765
	movl	-1668(%rbp), %edi
	testl	%edi, %edi
	jne	.L815
	cmpb	$0, -1673(%rbp)
	je	.L765
.L815:
	movl	-1664(%rbp), %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14RegExpCompiler33OptionallyStepBackToLeadSurrogateEPS1_PNS0_10RegExpNodeENS_4base5FlagsINS0_8JSRegExp4FlagEiEE@PLT
	movq	%rax, %r13
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L874:
	movq	%rax, %rdi
	call	strlen@PLT
	movq	-1624(%rbp), %rdi
	xorl	%edx, %edx
	leaq	-1584(%rbp), %rsi
	movq	%rbx, -1584(%rbp)
	movq	%rax, -1576(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L868
.L771:
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L799:
	testq	%r13, %r13
	je	.L801
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L772:
	movl	$80, %edi
	call	_Znwm@PLT
	movq	-1640(%rbp), %rdx
	movq	-1624(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal23RegExpBytecodeGeneratorC1EPNS0_7IsolateEPNS0_4ZoneE@PLT
	jmp	.L774
.L875:
	leaq	37592(%rcx), %rdi
	call	_ZN2v88internal4Heap25CommittedMemoryExecutableEv@PLT
	cmpq	$16777216, %rax
	seta	%al
	jmp	.L775
.L876:
	movl	-1680(%rbp), %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	setne	%al
	addl	$2, %eax
	jmp	.L778
.L807:
	xorl	%eax, %eax
	jmp	.L707
.L712:
	movq	15(%rax), %r13
	movq	-1624(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L714
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -1608(%rbp)
	movq	(%rax), %r13
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L767:
	movq	-1640(%rbp), %rax
	movq	16(%rax), %r11
	movq	24(%rax), %rax
	movq	%rax, -1664(%rbp)
	subq	%r11, %rax
	cmpq	$63, %rax
	jbe	.L883
	movq	-1640(%rbp), %rcx
	leaq	64(%r11), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %rax
.L769:
	movq	%rax, 48(%r11)
	pxor	%xmm0, %xmm0
	movq	%r11, %r13
	leaq	16+_ZTVN2v88internal7EndNodeE(%rip), %rax
	movq	$0, 8(%r11)
	movq	$0, 16(%r11)
	movq	$0, 24(%r11)
	movq	%rax, (%r11)
	movl	$1, 56(%r11)
	movups	%xmm0, 32(%r11)
	jmp	.L768
.L873:
	movq	-1640(%rbp), %rax
	movq	16(%rax), %r12
	movq	24(%rax), %rax
	movq	%rax, -1696(%rbp)
	subq	%r12, %rax
	cmpq	$71, %rax
	jbe	.L884
	movq	-1640(%rbp), %rcx
	leaq	72(%r12), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %rax
.L743:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal10ChoiceNodeE(%rip), %rcx
	movq	%rax, 48(%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%rcx, (%r12)
	movups	%xmm0, 32(%r12)
	movq	16(%rax), %r9
	movq	24(%rax), %rax
	movq	%rax, %rdx
	subq	%r9, %rdx
	cmpq	$15, %rdx
	jbe	.L885
	movq	-1640(%rbp), %rcx
	leaq	16(%r9), %rdx
	movq	%rdx, 16(%rcx)
.L745:
	movq	16(%rcx), %rdx
	subq	%rdx, %rax
	cmpq	$31, %rax
	jbe	.L886
	leaq	32(%rdx), %rax
	movq	%rax, 16(%rcx)
.L747:
	xorl	%r10d, %r10d
	movq	%rdx, (%r9)
	movq	%r9, %rdi
	movq	$2, 8(%r9)
	movq	%r9, 56(%r12)
	movw	%r10w, 64(%r12)
	movq	%r13, -1552(%rbp)
	leaq	-1552(%rbp), %r13
	movq	$0, -1544(%rbp)
	movq	48(%r12), %rdx
	movq	%r13, %rsi
	movq	%r8, -1704(%rbp)
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	movq	-1640(%rbp), %rax
	movq	-1704(%rbp), %r8
	movq	16(%rax), %r11
	movq	24(%rax), %rax
	movq	%rax, -1696(%rbp)
	subq	%r11, %rax
	cmpq	$31, %rax
	jbe	.L887
	movq	-1640(%rbp), %rcx
	leaq	32(%r11), %rax
	movq	%rax, 16(%rcx)
	movq	%rcx, %rax
.L749:
	movl	$42, %r9d
	movq	%rbx, (%r11)
	movq	$0, 8(%r11)
	movw	%r9w, 16(%r11)
	movq	$0, 24(%r11)
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rax, -1696(%rbp)
	subq	%rbx, %rax
	cmpq	$79, %rax
	jbe	.L888
	movq	-1640(%rbp), %rcx
	leaq	80(%rbx), %rax
	movq	%rax, 16(%rcx)
.L751:
	movq	48(%r8), %rdi
	pxor	%xmm0, %xmm0
	movq	%r8, %xmm1
	leaq	16+_ZTVN2v88internal8TextNodeE(%rip), %rax
	movups	%xmm0, 32(%rbx)
	movq	%rdi, %xmm0
	movq	$0, 8(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	24(%rdi), %rax
	movq	16(%rdi), %r8
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	$15, %rdx
	jbe	.L889
	leaq	16(%r8), %rdx
	movq	%rdx, 16(%rdi)
.L753:
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L890
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
.L755:
	movq	%rdx, (%r8)
	movq	%r11, %rdi
	movq	$1, 8(%r8)
	movq	48(%rbx), %r9
	movq	%r8, 64(%rbx)
	movb	$0, 72(%rbx)
	movq	%r8, -1704(%rbp)
	movq	%r9, -1696(%rbp)
	call	_ZN2v88internal11TextElement9CharClassEPNS0_20RegExpCharacterClassE@PLT
	movq	-1704(%rbp), %r8
	movq	-1696(%rbp), %r9
	movq	%rax, %r11
	movq	%rdx, %rcx
	movslq	12(%r8), %rax
	movl	8(%r8), %edx
	cmpl	%edx, %eax
	jge	.L756
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	(%r8), %rax
	movl	%edx, 12(%r8)
	movq	%r11, (%rax)
	movq	%rcx, 8(%rax)
.L757:
	movq	%rbx, -1552(%rbp)
	movq	%r13, %rsi
	movq	%r12, %r13
	movq	$0, -1544(%rbp)
	movq	48(%r12), %rdx
	movq	56(%r12), %rdi
	call	_ZN2v88internal8ZoneListINS0_18GuardedAlternativeEE3AddERKS2_PNS0_4ZoneE
	jmp	.L727
.L721:
	movq	41088(%rax), %rcx
	movq	%rcx, -1608(%rbp)
	cmpq	41096(%rax), %rcx
	je	.L891
.L723:
	movq	-1608(%rbp), %rcx
	movq	-1624(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rcx)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L728:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L877:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rax), %rsi
	movq	%rax, %rbx
	je	.L781
	testq	%rsi, %rsi
	je	.L892
.L782:
	addl	$1, 152(%rbx)
.L781:
	leaq	-1520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	-1624(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L783
	movq	-1608(%rbp), %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L784:
	movq	-1656(%rbp), %rax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	leaq	-1568(%rbp), %r9
	leaq	-1584(%rbp), %rdi
	movq	(%rax), %rax
	movq	%r9, %rsi
	movq	%r9, -1640(%rbp)
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-1640(%rbp), %r9
	movq	(%r14), %rax
	xorl	%ecx, %ecx
	movq	-1632(%rbp), %rdx
	movq	-1584(%rbp), %rsi
	movq	%r9, %rdi
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal4Code11DisassembleEPKcRSom@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L786
	call	_ZdaPv@PLT
.L786:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -1440(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-1456(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -1520(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1520(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L779
	subl	$1, 152(%rbx)
	jne	.L779
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L779
.L795:
	testq	%r9, %r9
	jne	.L893
	movq	%rbx, %rdx
	jmp	.L796
.L714:
	movq	41088(%rax), %rcx
	movq	%rcx, -1608(%rbp)
	cmpq	41096(%rax), %rcx
	je	.L894
.L716:
	movq	-1608(%rbp), %rcx
	movq	-1624(%rbp), %rdx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movq	%r13, (%rcx)
	jmp	.L709
.L756:
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	leal	1(%rdx,%rdx), %r10d
	movslq	%r10d, %rsi
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L895
	addq	%rdi, %rsi
	movq	%rsi, 16(%r9)
.L759:
	movslq	12(%r8), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	testl	%eax, %eax
	jle	.L760
	movq	(%r8), %rsi
	movq	%rcx, -1720(%rbp)
	movq	%r11, -1712(%rbp)
	movl	%r10d, -1704(%rbp)
	movq	%r8, -1696(%rbp)
	call	memcpy@PLT
	movq	-1696(%rbp), %r8
	movq	-1720(%rbp), %rcx
	movq	-1712(%rbp), %r11
	movl	-1704(%rbp), %r10d
	movq	%rax, %rdi
	movslq	12(%r8), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
.L760:
	movq	%rdi, (%r8)
	addl	$1, %eax
	addq	%rdx, %rdi
	movl	%r10d, 8(%r8)
	movl	%eax, 12(%r8)
	movq	%r11, (%rdi)
	movq	%rcx, 8(%rdi)
	jmp	.L757
.L872:
	movq	-1640(%rbp), %rdi
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L741
.L791:
	movq	41088(%rax), %r15
	cmpq	41096(%rax), %r15
	je	.L896
.L793:
	movq	-1624(%rbp), %rcx
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rcx)
	movq	-1608(%rbp), %rax
	movq	%rax, (%r15)
	jmp	.L792
.L879:
	leaq	37592(%rcx), %rdi
	call	_ZN2v88internal4Heap25CommittedMemoryExecutableEv@PLT
	cmpq	$16777216, %rax
	setbe	%al
	jmp	.L707
.L880:
	movq	-1632(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r9, -1648(%rbp)
	movq	%r8, -1640(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1640(%rbp), %r8
	movq	-1648(%rbp), %r9
	movq	%rax, -1520(%rbp)
	movq	%rax, %rdi
	movq	-1568(%rbp), %rax
	movq	%rax, -1504(%rbp)
.L794:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-1568(%rbp), %rax
	movq	-1520(%rbp), %rdx
	jmp	.L796
.L783:
	movq	41088(%rax), %r14
	cmpq	%r14, 41096(%rax)
	je	.L897
.L785:
	movq	-1624(%rbp), %rcx
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rcx)
	movq	-1608(%rbp), %rax
	movq	%rax, (%r14)
	jmp	.L784
.L891:
	movq	%rax, %rdi
	movq	%rsi, -1632(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1632(%rbp), %rsi
	movq	%rax, -1608(%rbp)
	jmp	.L723
.L883:
	movq	-1640(%rbp), %rdi
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r11
	movq	-1640(%rbp), %rax
	jmp	.L769
.L896:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r15
	jmp	.L793
.L884:
	movq	-1640(%rbp), %rdi
	movl	$72, %esi
	movq	%r8, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %r8
	movq	%rax, %r12
	movq	-1640(%rbp), %rax
	jmp	.L743
.L888:
	movq	-1640(%rbp), %rdi
	movl	$80, %esi
	movq	%r11, -1704(%rbp)
	movq	%r8, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %r8
	movq	-1704(%rbp), %r11
	movq	%rax, %rbx
	jmp	.L751
.L887:
	movq	-1640(%rbp), %rdi
	movl	$32, %esi
	movq	%r8, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %r8
	movq	%rax, %r11
	movq	-1640(%rbp), %rax
	jmp	.L749
.L886:
	movl	$32, %esi
	movq	%rcx, %rdi
	movq	%r9, -1704(%rbp)
	movq	%r8, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %r8
	movq	-1704(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L747
.L885:
	movq	-1640(%rbp), %rdi
	movl	$16, %esi
	movq	%r8, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %r8
	movq	-1640(%rbp), %rcx
	movq	%rax, %r9
	movq	-1640(%rbp), %rax
	movq	24(%rax), %rax
	jmp	.L745
.L890:
	movl	$16, %esi
	movq	%r8, -1704(%rbp)
	movq	%r11, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %r11
	movq	-1704(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L755
.L889:
	movl	$16, %esi
	movq	%r11, -1704(%rbp)
	movq	%rdi, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %rdi
	movq	-1704(%rbp), %r11
	movq	%rax, %r8
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	jmp	.L753
.L894:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -1608(%rbp)
	jmp	.L716
.L892:
	movq	(%rax), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L782
.L897:
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L785
.L895:
	movq	%r9, %rdi
	movq	%rcx, -1720(%rbp)
	movq	%r11, -1712(%rbp)
	movl	%r10d, -1704(%rbp)
	movq	%r8, -1696(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1696(%rbp), %r8
	movl	-1704(%rbp), %r10d
	movq	-1712(%rbp), %r11
	movq	-1720(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L759
.L881:
	leaq	.LC8(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L893:
	movq	%rbx, %rdi
	jmp	.L794
.L882:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21833:
	.size	_ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b, .-_ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b
	.section	.rodata._ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"../deps/v8/src/regexp/regexp.cc:365"
	.section	.text._ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb
	.type	_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb, @function
_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb:
.LFB21815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$296, %rsp
	movq	%rdx, -336(%rbp)
	movq	41136(%rdi), %rsi
	leaq	.LC9(%rip), %rdx
	movl	%ecx, -312(%rbp)
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movl	$255, %edx
	leaq	-288(%rbp), %rdi
	call	_ZN2v88internal15InterruptsScopeC2EPNS0_7IsolateElNS1_4ModeE@PLT
	leaq	16+_ZTVN2v88internal23PostponeInterruptsScopeE(%rip), %rax
	movq	%rax, -288(%rbp)
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %r15
	sarq	$32, %r15
	movq	%r15, -320(%rbp)
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L899
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
.L900:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L902
.L904:
	movq	(%r10), %rsi
.L903:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L996
.L911:
	pxor	%xmm0, %xmm0
	movq	%r10, %rdx
	movl	$1, %ecx
	movq	%r12, %rsi
	leaq	-240(%rbp), %r13
	movq	%r10, -328(%rbp)
	leaq	-128(%rbp), %r15
	movq	%r13, %rdi
	movw	%cx, -104(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -80(%rbp)
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %r8
	movq	%r14, %rsi
	movl	-320(%rbp), %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal12RegExpParser11ParseRegExpEPNS0_7IsolateEPNS0_4ZoneEPNS0_16FlatStringReaderENS_4base5FlagsINS0_8JSRegExp4FlagEiEEPNS0_17RegExpCompileDataE@PLT
	movq	-328(%rbp), %r10
	testb	%al, %al
	movl	%eax, %r13d
	je	.L997
	movq	(%rbx), %rax
	leaq	-296(%rbp), %rdi
	movq	%r10, -328(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8JSRegExp21ShouldProduceBytecodeEv@PLT
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r14, %rsi
	xorl	$1, %eax
	movq	-328(%rbp), %r10
	movq	-336(%rbp), %r9
	movq	%r12, %rdi
	movzbl	%al, %eax
	movl	-320(%rbp), %ecx
	movl	%eax, -72(%rbp)
	movzbl	-312(%rbp), %eax
	movq	%r10, %r8
	pushq	%rax
	call	_ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testb	%r13b, %r13b
	je	.L998
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	23(%rax), %r15
	testq	%rdi, %rdi
	je	.L923
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %rbx
.L924:
	cmpb	$1, -312(%rbp)
	sbbq	%rax, %rax
	andl	$8, %eax
	cmpl	$1, -72(%rbp)
	je	.L999
	movq	-112(%rbp), %rdx
	leaq	55(%rax,%r15), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L954
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -320(%rbp)
	testl	$262144, %eax
	jne	.L1000
	testb	$24, %al
	je	.L954
.L1010:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L954
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L954:
	leaq	41184(%r12), %rdi
	movl	$557, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	cmpb	$1, -312(%rbp)
	movq	(%rbx), %r15
	movq	(%rax), %r12
	sbbq	%rax, %rax
	andl	$8, %eax
	leaq	39(%r15,%rax), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L932
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -312(%rbp)
	testl	$262144, %eax
	jne	.L1001
	testb	$24, %al
	je	.L932
.L1009:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L932
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L932:
	movq	(%rbx), %r12
	movq	-96(%rbp), %rax
	leaq	87(%r12), %rsi
	testq	%rax, %rax
	je	.L1002
	movq	(%rax), %r15
	movq	%r15, 87(%r12)
	testb	$1, %r15b
	je	.L945
.L995:
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -312(%rbp)
	testl	$262144, %eax
	jne	.L1003
.L947:
	testb	$24, %al
	je	.L945
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1004
	.p2align 4,,10
	.p2align 3
.L945:
	movq	(%rbx), %rcx
	movq	71(%rcx), %rax
	movl	-76(%rbp), %edx
	sarq	$32, %rax
	cmpl	%eax, %edx
	jg	.L1005
.L918:
	movq	-232(%rbp), %rax
	movq	-224(%rbp), %rdx
	cmpl	$2, -256(%rbp)
	movq	%rdx, 41704(%rax)
	leaq	16+_ZTVN2v88internal15InterruptsScopeE(%rip), %rax
	movq	%rax, -288(%rbp)
	je	.L949
	movq	-280(%rbp), %rdi
	call	_ZN2v88internal10StackGuard18PopInterruptsScopeEv@PLT
.L949:
	movq	%r14, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1006
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L911
	movq	(%r10), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L914
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L923:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1007
.L925:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rbx)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L902:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L904
	movq	(%r10), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L906
	movq	23(%rdx), %rax
	movl	11(%rax), %esi
	testl	%esi, %esi
	je	.L906
	movq	%r10, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r10
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L899:
	movq	41088(%r12), %r10
	cmpq	%r10, 41096(%r12)
	je	.L1008
.L901:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L997:
	movq	-88(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r10, %rdx
.L993:
	movq	%r12, %rdi
	movl	$271, %esi
	call	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1009
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	%r15, %rdi
	movq	%rdx, -336(%rbp)
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-320(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movq	-328(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1010
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L914:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L1011
.L916:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L999:
	movq	-112(%rbp), %r12
	leaq	39(%rax,%r15), %rsi
	movq	%r12, (%rsi)
	testb	$1, %r12b
	je	.L952
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -320(%rbp)
	testl	$262144, %eax
	je	.L929
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-320(%rbp), %rcx
	movq	-328(%rbp), %rsi
	movq	8(%rcx), %rax
.L929:
	testb	$24, %al
	je	.L952
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1012
	.p2align 4,,10
	.p2align 3
.L952:
	cmpb	$1, -312(%rbp)
	movq	(%rbx), %rdx
	movabsq	$-4294967296, %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	%rcx, 55(%rax,%rdx)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1005:
	salq	$32, %rdx
	movq	%rdx, 71(%rcx)
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r15
	movq	%r15, 87(%r12)
	testb	$1, %r15b
	jne	.L995
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L998:
	movq	(%rbx), %rax
	movq	-88(%rbp), %r15
	movq	23(%rax), %rax
	movq	23(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L920
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L921:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L906:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L908
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r10
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	%r12, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L920:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1013
.L922:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L908:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L1014
.L910:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L952
.L1014:
	movq	%r12, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rbx
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	%r12, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	%r12, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L922
.L1006:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21815:
	.size	_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb, .-_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb
	.section	.text._ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii
	.type	_ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii, @function
_ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii:
.LFB21825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movl	%ecx, -84(%rbp)
	movq	41112(%rdi), %rdi
	movq	%r8, -96(%rbp)
	movl	%r9d, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1016
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1017:
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L1044
.L1054:
	cmpw	$8, %ax
	je	.L1045
	movq	15(%rdx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L1054
.L1044:
	xorl	%r12d, %r12d
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1045:
	movl	$1, %r12d
.L1019:
	movq	(%rbx), %rax
	leaq	-64(%rbp), %r14
	movq	%rcx, -72(%rbp)
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8JSRegExp21ShouldProduceBytecodeEv@PLT
	movq	-72(%rbp), %rcx
	testb	%al, %al
	jne	.L1021
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	(%rbx), %rax
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8JSRegExp4CodeEb@PLT
	movq	%r14, %rdi
	movl	%r12d, %esi
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8JSRegExp8BytecodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8JSRegExp15MarkedForTierUpEv@PLT
	testb	%al, %al
	je	.L1025
	testb	$1, -80(%rbp)
	jne	.L1055
.L1025:
	movabsq	$-4294967296, %rcx
	cmpq	%rcx, -72(%rbp)
	je	.L1024
.L1023:
	movl	-84(%rbp), %r8d
	movl	-104(%rbp), %ecx
	movq	%r13, %r9
	movq	%r15, %rsi
	movq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal26NativeRegExpMacroAssembler5MatchENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPiiiPNS0_7IsolateE@PLT
	cmpl	$-2, %eax
	jne	.L1051
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L1046
.L1056:
	cmpw	$8, %ax
	je	.L1047
	movq	15(%rdx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L1056
.L1046:
	xorl	%r12d, %r12d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1047:
	movl	$1, %r12d
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	-80(%rbp), %rax
	movq	-1(%rax), %rax
	cmpw	$70, 11(%rax)
	jne	.L1025
	.p2align 4,,10
	.p2align 3
.L1024:
	movl	%r12d, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1051:
	movl	%eax, %r12d
.L1015:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1057
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1021:
	.cfi_restore_state
	movq	(%rcx), %rax
	movq	79(%rax), %rax
	movq	-96(%rbp), %r10
	movq	%r14, -80(%rbp)
	movq	%r15, %r14
	sarq	$32, %rax
	leal	2(%rax,%rax), %r8d
	movslq	%r8d, %rax
	salq	$2, %rax
	movq	%rax, -104(%rbp)
	addq	%rax, %r10
	movq	%r10, %r15
.L1050:
	movl	-84(%rbp), %r9d
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal19IrregexpInterpreter23MatchForCallFromRuntimeEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEPiii@PLT
	movl	-72(%rbp), %r8d
	testl	%eax, %eax
	movl	%eax, %r12d
	jg	.L1031
	cmpl	$-1, %eax
	jge	.L1015
	cmpl	$-2, %eax
	jne	.L1050
	cmpb	$0, _ZN2v88internal19FLAG_regexp_tier_upE(%rip)
	jne	.L1058
.L1034:
	movq	(%r14), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L1048
.L1062:
	cmpw	$8, %ax
	jne	.L1059
	movl	$1, %r12d
.L1035:
	movq	(%rbx), %rax
	movq	-80(%rbp), %rdi
	movl	%r12d, %esi
	movl	%r8d, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8JSRegExp4CodeEb@PLT
	movq	-80(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, -112(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8JSRegExp8BytecodeEb@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8JSRegExp15MarkedForTierUpEv@PLT
	movl	-72(%rbp), %r8d
	testb	%al, %al
	je	.L1040
	testb	$1, -120(%rbp)
	jne	.L1060
.L1040:
	movabsq	$-4294967296, %rsi
	cmpq	%rsi, -112(%rbp)
	jne	.L1050
.L1039:
	movl	%r12d, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb
	movl	-72(%rbp), %r8d
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1031:
	cmpl	$1, %eax
	jne	.L1050
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%r15, %rsi
	call	memcpy@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L1061
.L1018:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	15(%rcx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L1062
.L1048:
	xorl	%r12d, %r12d
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	(%rbx), %rax
	movq	-80(%rbp), %rdi
	movl	%r8d, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8JSRegExp11ResetTierUpEv@PLT
	movl	-72(%rbp), %r8d
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1018
.L1060:
	movq	-120(%rbp), %rax
	movq	-1(%rax), %rax
	cmpw	$70, 11(%rax)
	jne	.L1040
	jmp	.L1039
.L1057:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21825:
	.size	_ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii, .-_ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii
	.section	.text._ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE
	.type	_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE, @function
_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE:
.LFB21824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L1075
.L1079:
	cmpw	$8, %ax
	je	.L1076
	movq	15(%rcx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L1079
.L1075:
	xorl	%r15d, %r15d
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	$1, %r15d
.L1064:
	leaq	-64(%rbp), %r12
	movq	(%rbx), %rax
	movl	%r15d, %esi
	movq	%rdx, -72(%rbp)
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8JSRegExp4CodeEb@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8JSRegExp8BytecodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8JSRegExp15MarkedForTierUpEv@PLT
	movq	-72(%rbp), %rdx
	testb	%al, %al
	jne	.L1066
.L1069:
	movabsq	$-4294967296, %rax
	cmpq	%rax, %r14
	je	.L1068
.L1071:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	23(%rax), %rbx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8JSRegExp21ShouldProduceBytecodeEv@PLT
	testb	%al, %al
	jne	.L1080
	movq	79(%rbx), %rax
	sarq	$32, %rax
	leal	2(%rax,%rax), %eax
.L1063:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1081
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	.cfi_restore_state
	testb	$1, -80(%rbp)
	je	.L1069
	movq	-80(%rbp), %rax
	movq	-1(%rax), %rax
	cmpw	$70, 11(%rax)
	jne	.L1069
	.p2align 4,,10
	.p2align 3
.L1068:
	movl	%r15d, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10RegExpImpl15CompileIrregexpEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEb
	testb	%al, %al
	jne	.L1071
	movl	$-1, %eax
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	71(%rbx), %rax
	movq	79(%rbx), %rdx
	sarq	$32, %rax
	sarq	$32, %rdx
	leal	2(%rax,%rdx,2), %eax
	jmp	.L1063
.L1081:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21824:
	.size	_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE, .-_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE
	.section	.rodata._ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"NewArray"
	.section	.text._ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE
	.type	_ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE, @function
_ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE:
.LFB21867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movq	(%rsi), %rax
	movl	$0, 24(%rdi)
	movq	%rsi, 32(%rdi)
	movq	%rdx, 40(%rdi)
	movq	%rcx, 48(%rdi)
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8JSRegExp21ShouldProduceBytecodeEv@PLT
	movl	%eax, %r13d
	movq	32(%rbx), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1094
.L1083:
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	jne	.L1084
	movl	$2, 12(%rbx)
	movl	$64, %eax
	movl	$64, 4(%rbx)
	movl	$128, 24(%rbx)
.L1085:
	leaq	41860(%r12), %rcx
	movq	%rcx, 16(%rbx)
.L1090:
	leal	-1(%rax), %edx
	movl	%eax, (%rbx)
	movl	$4294967295, %eax
	movl	%edx, 8(%rbx)
	imull	12(%rbx), %edx
	movslq	%edx, %rdx
	movq	%rax, (%rcx,%rdx,4)
.L1082:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1095
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1094:
	.cfi_restore_state
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	jne	.L1083
.L1084:
	movq	40(%rbx), %rdx
	movq	32(%rbx), %rsi
	movq	48(%rbx), %rdi
	call	_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE
	movslq	%eax, %rcx
	movl	%ecx, 12(%rbx)
	testl	%ecx, %ecx
	js	.L1096
	movl	$1, %eax
	testb	%r13b, %r13b
	je	.L1097
.L1088:
	movl	%eax, 4(%rbx)
	movl	%ecx, 24(%rbx)
	cmpl	$128, %ecx
	jle	.L1085
	leaq	0(,%rcx,4), %r12
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1098
.L1089:
	movq	%rcx, 16(%rbx)
	movl	4(%rbx), %eax
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1097:
	cmpl	$128, %ecx
	movl	$128, %esi
	cmovge	%ecx, %esi
	movl	%esi, %eax
	cltd
	idivl	%ecx
	movslq	%esi, %rcx
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1096:
	movl	$-1, (%rbx)
	jmp	.L1082
.L1095:
	call	__stack_chk_fail@PLT
.L1098:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L1089
	leaq	.LC10(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE21867:
	.size	_ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE, .-_ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE
	.globl	_ZN2v88internal17RegExpGlobalCacheC1ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE
	.set	_ZN2v88internal17RegExpGlobalCacheC1ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE,_ZN2v88internal17RegExpGlobalCacheC2ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE
	.section	.text._ZN2v88internal10RegExpImpl12IrregexpExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl12IrregexpExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	.type	_ZN2v88internal10RegExpImpl12IrregexpExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE, @function
_ZN2v88internal10RegExpImpl12IrregexpExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE:
.LFB21826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rax
	movq	%r8, -64(%rbp)
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L1128
.L1101:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1129
.L1109:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE
	movl	%eax, %r9d
	testl	%eax, %eax
	js	.L1130
	leaq	41860(%r12), %r8
	xorl	%r14d, %r14d
	cmpl	$128, %eax
	jg	.L1131
.L1118:
	movl	%r13d, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii
	movq	-56(%rbp), %r8
	cmpl	$1, %eax
	je	.L1132
	addq	$104, %r12
	cmpl	$-1, %eax
	movl	$0, %eax
	cmove	%rax, %r12
.L1120:
	testq	%r14, %r14
	je	.L1116
	movq	%r14, %rdi
	call	_ZdaPv@PLT
.L1116:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1129:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1109
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1112
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1101
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1104
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1104
	movq	%r15, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1130:
	xorl	%r12d, %r12d
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1131:
	movslq	%eax, %r14
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	%eax, -56(%rbp)
	salq	$2, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1133
	movq	%rax, %r14
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	23(%rax), %rax
	movq	79(%rax), %rcx
	movq	-64(%rbp), %rsi
	sarq	$32, %rcx
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi
	movq	%rax, %r12
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1134
.L1114:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1106
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L1135
.L1108:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L1101
.L1135:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1114
.L1133:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	movq	%r14, %rdi
	leaq	_ZSt7nothrow(%rip), %rsi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r8
	movq	%rax, %r14
	jne	.L1118
	leaq	.LC10(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE21826:
	.size	_ZN2v88internal10RegExpImpl12IrregexpExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE, .-_ZN2v88internal10RegExpImpl12IrregexpExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	.section	.text._ZN2v88internal6RegExp17CompileForTestingEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6RegExp17CompileForTestingEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b
	.type	_ZN2v88internal6RegExp17CompileForTestingEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b, @function
_ZN2v88internal6RegExp17CompileForTestingEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b:
.LFB21832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzbl	16(%rbp), %eax
	movl	%eax, 16(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10RegExpImpl7CompileEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b
	.cfi_endproc
.LFE21832:
	.size	_ZN2v88internal6RegExp17CompileForTestingEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b, .-_ZN2v88internal6RegExp17CompileForTestingEPNS0_7IsolateEPNS0_4ZoneEPNS0_17RegExpCompileDataENS_4base5FlagsINS0_8JSRegExp4FlagEiEENS0_6HandleINS0_6StringEEESF_b
	.section	.text._ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi,"axG",@progbits,_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	.type	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi, @function
_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi:
.LFB24079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leal	-250(%r8), %eax
	movq	%rdi, -48(%rbp)
	movl	$0, %edi
	testl	%eax, %eax
	movq	%rcx, -40(%rbp)
	cmovs	%edi, %eax
	movq	%r8, -32(%rbp)
	movl	%eax, -16(%rbp)
	movslq	%r8d, %rax
	cmpq	$7, %rax
	leaq	(%rcx,%rax,2), %rdi
	movq	%rcx, %rax
	jbe	.L1164
	testb	$7, %cl
	jne	.L1140
	.p2align 4,,10
	.p2align 3
.L1144:
	leaq	16(%rax), %r10
	cmpq	%r10, %rdi
	jb	.L1164
	leaq	-16(%rdi), %r10
	subq	%rax, %r10
	shrq	$3, %r10
	leaq	8(%rax,%r10,8), %r11
	movabsq	$-71777214294589696, %r10
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1145:
	addq	$8, %rax
	cmpq	%rax, %r11
	je	.L1164
.L1148:
	testq	%r10, (%rax)
	je	.L1145
	cmpq	%rax, %rdi
	jbe	.L1146
	.p2align 4,,10
	.p2align 3
.L1147:
	cmpw	$255, (%rax)
	ja	.L1146
	addq	$2, %rax
.L1164:
	cmpq	%rax, %rdi
	ja	.L1147
.L1146:
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jg	.L1165
.L1150:
	cmpl	$6, %r8d
	jle	.L1166
	leaq	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
.L1151:
	leaq	-48(%rbp), %rdi
	movl	%r9d, %ecx
	call	*%rax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1167
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1142:
	.cfi_restore_state
	addq	$2, %rax
	testb	$7, %al
	je	.L1144
.L1140:
	cmpw	$255, (%rax)
	jbe	.L1142
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jle	.L1150
.L1165:
	leaq	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1166:
	cmpl	$1, %r8d
	je	.L1168
	leaq	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1168:
	leaq	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	.L1151
.L1167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24079:
	.size	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi, .-_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	.section	.text._ZN2v88internal10RegExpImpl11AtomExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl11AtomExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii
	.type	_ZN2v88internal10RegExpImpl11AtomExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii, @function
_ZN2v88internal10RegExpImpl11AtomExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii:
.LFB21812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	%rdi, -136(%rbp)
	movl	%r9d, -172(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, %rsi
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1206
.L1171:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1207
.L1179:
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movq	39(%rax), %rax
	movq	%rax, -104(%rbp)
	movl	11(%rax), %eax
	movq	(%r12), %rdx
	movl	%eax, -140(%rbp)
	leal	(%rcx,%rax), %esi
	xorl	%eax, %eax
	cmpl	11(%rdx), %esi
	jg	.L1169
	movl	-172(%rbp), %eax
	testl	%eax, %eax
	jle	.L1201
	movl	-172(%rbp), %eax
	xorl	%r15d, %r15d
	leaq	-105(%rbp), %r13
	leaq	-96(%rbp), %r14
	movq	%rbx, -168(%rbp)
	subl	$1, %eax
	shrl	%eax
	leaq	2(%rax,%rax), %rax
	movq	%rax, -160(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -152(%rbp)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1187:
	cmpl	$1, %edi
	je	.L1208
	leal	-250(%rbx), %eax
	movl	$0, %edi
	movq	%r8, -80(%rbp)
	movq	-136(%rbp), %xmm0
	testl	%eax, %eax
	cmovs	%edi, %eax
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$6, %ebx
	jg	.L1197
	cmpl	$1, %ebx
	je	.L1209
	leaq	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	%r14, %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L1210
.L1200:
	movl	-140(%rbp), %esi
	movq	-168(%rbp), %rbx
	leal	(%rax,%rsi), %ecx
	movl	%eax, (%rbx,%r15,4)
	movl	%ecx, 4(%rbx,%r15,4)
	addq	$2, %r15
	cmpq	-160(%rbp), %r15
	je	.L1201
.L1202:
	movq	-152(%rbp), %rdi
	movq	%r13, %rsi
	movl	%ecx, -120(%rbp)
	movl	%r15d, -144(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	movq	(%r12), %rax
	movq	%rdx, %rbx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-120(%rbp), %ecx
	movslq	%ebx, %r8
	movq	%rax, %rsi
	movq	%rbx, %rax
	movq	%rdx, %rdi
	movslq	%edx, %rdx
	shrq	$32, %rax
	shrq	$32, %rdi
	cmpl	$1, %eax
	jne	.L1187
	movl	%ebx, %eax
	movl	$0, %r11d
	movq	%r8, -80(%rbp)
	movq	-136(%rbp), %xmm0
	subl	$250, %eax
	cmovs	%r11d, %eax
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$1, %edi
	je	.L1211
	cmpl	$6, %ebx
	jg	.L1193
	cmpl	$1, %ebx
	je	.L1212
	leaq	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	%ecx, %r9d
	movq	-136(%rbp), %rdi
	movq	-128(%rbp), %rcx
	call	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	cmpl	$-1, %eax
	jne	.L1200
.L1210:
	movl	-144(%rbp), %eax
	sarl	%eax
.L1169:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1213
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1211:
	cmpl	$6, %ebx
	jg	.L1189
	cmpl	$1, %ebx
	je	.L1214
	leaq	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1179
	movq	(%r12), %rax
	movq	15(%rax), %rsi
	movq	-136(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1182
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-120(%rbp), %ecx
	movq	%rax, %r12
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1189:
	leaq	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1212:
	leaq	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1171
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1174
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1174
	movq	%r12, %rsi
	xorl	%edx, %edx
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movl	-120(%rbp), %ecx
	movq	%rax, %r12
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1209:
	leaq	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1201:
	movl	-172(%rbp), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	41088(%rax), %r12
	cmpq	41096(%rax), %r12
	je	.L1215
.L1184:
	movq	-136(%rbp), %rdi
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%r12)
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1214:
	leaq	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	15(%rax), %rsi
	movq	-136(%rbp), %rax
	movq	41112(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1176
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-120(%rbp), %ecx
	movq	(%rax), %rsi
	movq	%rax, %r12
	jmp	.L1171
.L1176:
	movq	41088(%rax), %r12
	cmpq	41096(%rax), %r12
	je	.L1216
.L1178:
	movq	-136(%rbp), %rdi
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rdi)
	movq	%rsi, (%r12)
	jmp	.L1171
.L1216:
	movq	%rax, %rdi
	movl	%ecx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-128(%rbp), %ecx
	movq	-120(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1178
.L1215:
	movq	%rax, %rdi
	movl	%ecx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-128(%rbp), %ecx
	movq	-120(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1184
.L1213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21812:
	.size	_ZN2v88internal10RegExpImpl11AtomExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii, .-_ZN2v88internal10RegExpImpl11AtomExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii
	.section	.text._ZN2v88internal17RegExpGlobalCache9FetchNextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpGlobalCache9FetchNextEv
	.type	_ZN2v88internal17RegExpGlobalCache9FetchNextEv, @function
_ZN2v88internal17RegExpGlobalCache9FetchNextEv:
.LFB21873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %edx
	movl	(%rdi), %ecx
	leal	1(%rdx), %eax
	movl	%eax, 8(%rdi)
	cmpl	%ecx, %eax
	jl	.L1218
	cmpl	4(%rdi), %ecx
	jl	.L1225
	movq	32(%rdi), %rax
	imull	12(%rdi), %edx
	movq	16(%rdi), %r8
	movq	(%rax), %rax
	movslq	%edx, %rdx
	movq	23(%rax), %rax
	leaq	(%r8,%rdx,4), %rdx
	movl	4(%rdx), %ecx
	testb	$1, %al
	jne	.L1228
.L1221:
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L1229
.L1222:
	cmpl	(%rdx), %ecx
	je	.L1230
.L1224:
	movq	40(%rbx), %rdx
	movq	(%rdx), %rax
	cmpl	11(%rax), %ecx
	jg	.L1225
	movq	32(%rbx), %rsi
	movq	48(%rbx), %rdi
	movl	24(%rbx), %r9d
	movq	16(%rbx), %r8
	call	_ZN2v88internal10RegExpImpl15IrregexpExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii
	movl	%eax, (%rbx)
	movl	%eax, %edx
.L1223:
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L1217
	movl	$0, 8(%rbx)
	movq	16(%rbx), %rax
.L1217:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1225:
	.cfi_restore_state
	movl	$0, (%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1218:
	.cfi_restore_state
	imull	12(%rdi), %eax
	movq	16(%rdi), %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cltq
	leaq	(%rdx,%rax,4), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1228:
	.cfi_restore_state
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	je	.L1222
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	40(%rbx), %rdx
	movq	32(%rbx), %rsi
	movq	48(%rbx), %rdi
	movl	24(%rbx), %r9d
	call	_ZN2v88internal10RegExpImpl11AtomExecRawEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiPii
	movl	%eax, (%rbx)
	movl	%eax, %edx
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1230:
	movl	%ecx, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal17RegExpGlobalCache17AdvanceZeroLengthEi
	movl	%eax, %ecx
	jmp	.L1224
	.cfi_endproc
.LFE21873:
	.size	_ZN2v88internal17RegExpGlobalCache9FetchNextEv, .-_ZN2v88internal17RegExpGlobalCache9FetchNextEv
	.section	.text._ZN2v88internal10RegExpImpl8AtomExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpImpl8AtomExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	.type	_ZN2v88internal10RegExpImpl8AtomExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE, @function
_ZN2v88internal10RegExpImpl8AtomExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE:
.LFB21813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1232
	movq	%rdx, %rsi
	movq	%r14, %r8
.L1233:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1287
.L1241:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	39(%rax), %rdx
	movl	%ecx, -140(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -104(%rbp)
	movl	11(%rdx), %r15d
	movq	(%r8), %rax
	leal	(%rcx,%r15), %edx
	cmpl	11(%rax), %edx
	jg	.L1262
	leaq	-105(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%rax, -120(%rbp)
	movq	%rdx, %rbx
	movq	(%r8), %rax
	movq	%rdi, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-128(%rbp), %rdi
	movl	-140(%rbp), %ecx
	movslq	%ebx, %r8
	movq	%rax, %rsi
	movq	%rbx, %rax
	movq	%rdx, %r9
	movslq	%edx, %rdx
	shrq	$32, %rax
	shrq	$32, %r9
	cmpl	$1, %eax
	je	.L1288
	cmpl	$1, %r9d
	je	.L1289
	leal	-250(%rbx), %eax
	movq	%r8, -80(%rbp)
	movl	$0, %r8d
	movq	%r12, %xmm0
	testl	%eax, %eax
	movhps	-120(%rbp), %xmm0
	cmovs	%r8d, %eax
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$6, %ebx
	jg	.L1258
	cmpl	$1, %ebx
	je	.L1290
	leaq	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %r8
	movq	%r8, -72(%rbp)
.L1260:
	call	*%r8
.L1253:
	cmpl	$-1, %eax
	je	.L1262
.L1296:
	addl	%eax, %r15d
	movl	%eax, 41860(%r12)
	movabsq	$8589934592, %rdx
	movl	%r15d, 41864(%r12)
	movq	0(%r13), %rcx
	movq	(%r14), %r12
	movq	%rdx, 15(%rcx)
	movq	0(%r13), %r14
	movq	%r12, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %r12b
	je	.L1291
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	jne	.L1292
	andl	$24, %edx
	je	.L1267
.L1302:
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1267
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	%eax, -120(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-120(%rbp), %eax
.L1267:
	movq	0(%r13), %r14
	movq	%r12, 31(%r14)
	leaq	31(%r14), %rsi
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	jne	.L1293
	andl	$24, %edx
	je	.L1270
.L1301:
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1270
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	%eax, -120(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-120(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	0(%r13), %rdx
	salq	$32, %rax
	salq	$32, %r15
	movq	%rax, 39(%rdx)
	movq	0(%r13), %rax
	movq	%r15, 47(%rax)
	movq	%r13, %rax
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1288:
	movl	%ebx, %eax
	movl	$0, %r10d
	movq	%r12, %xmm0
	movq	%r8, -80(%rbp)
	subl	$250, %eax
	movhps	-120(%rbp), %xmm0
	cmovs	%r10d, %eax
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$1, %r9d
	je	.L1294
	cmpl	$6, %ebx
	jg	.L1254
	cmpl	$1, %ebx
	je	.L1295
	leaq	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	call	*%rax
.L1299:
	cmpl	$-1, %eax
	jne	.L1296
	.p2align 4,,10
	.p2align 3
.L1262:
	leaq	104(%r12), %rax
.L1265:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1297
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1258:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %r8
	movq	%r8, -72(%rbp)
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1289:
	movl	%ecx, %r9d
	movq	-120(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12SearchStringIhtEEiPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEi
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1294:
	cmpl	$6, %ebx
	jg	.L1250
	cmpl	$1, %ebx
	je	.L1298
	leaq	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	call	*%rax
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1254:
	leaq	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	call	*%rax
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1241
	movq	(%r8), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1244
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-120(%rbp), %ecx
	movq	%rax, %r8
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1250:
	leaq	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	call	*%rax
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1295:
	leaq	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	call	*%rax
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	-1(%rdx), %rax
	movq	%rdx, %rsi
	movq	%r14, %r8
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1233
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1236
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L1236
	xorl	%edx, %edx
	movq	%r14, %rsi
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movl	-120(%rbp), %ecx
	movq	%rax, %r8
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1290:
	leaq	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %r8
	movq	%r8, -72(%rbp)
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1300
.L1246:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	%eax, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movl	-128(%rbp), %eax
	movq	-120(%rbp), %rsi
	andl	$24, %edx
	jne	.L1301
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	%eax, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	movl	-128(%rbp), %eax
	movq	-120(%rbp), %rsi
	andl	$24, %edx
	jne	.L1302
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1298:
	leaq	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	call	*%rax
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1291:
	movq	0(%r13), %rdx
	movq	%r12, 31(%rdx)
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1238
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-120(%rbp), %ecx
	movq	(%rax), %rsi
	movq	%rax, %r8
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1303
.L1240:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1233
.L1303:
	movq	%r12, %rdi
	movl	%ecx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-128(%rbp), %ecx
	movq	-120(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1300:
	movq	%r12, %rdi
	movl	%ecx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-128(%rbp), %ecx
	movq	-120(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1246
.L1297:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21813:
	.size	_ZN2v88internal10RegExpImpl8AtomExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE, .-_ZN2v88internal10RegExpImpl8AtomExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	.section	.text._ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	.type	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE, @function
_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE:
.LFB21809:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	23(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$1, %al
	jne	.L1312
.L1305:
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L1307
	cmpl	$2, %eax
	jne	.L1306
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10RegExpImpl12IrregexpExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	.p2align 4,,10
	.p2align 3
.L1307:
	.cfi_restore_state
	call	_ZN2v88internal10RegExpImpl8AtomExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	movq	%rax, %r9
	andq	$-262144, %r9
	movq	24(%r9), %r9
	cmpq	-37504(%r9), %rax
	jne	.L1305
.L1306:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21809:
	.size	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE, .-_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE
	.section	.text._ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv:
.LFB26928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L1314
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L1338
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L1316:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1316
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L1317
.L1315:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L1317
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L1317
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L1317:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L1337:
	cmpl	%eax, %r8d
	jl	.L1319
	.p2align 4,,10
	.p2align 3
.L1323:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L1319
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L1344
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L1323
.L1319:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L1345
.L1324:
	cmpl	%esi, %r12d
	jge	.L1318
.L1347:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1344:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L1323
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1345:
	cmpl	%esi, %r12d
	jl	.L1330
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1346:
	cmpl	%r10d, (%r14)
	jne	.L1327
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L1327:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L1325
.L1330:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L1346
	cmpl	%r12d, %ecx
	jle	.L1313
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L1347
	.p2align 4,,10
	.p2align 3
.L1318:
	cmpl	-64(%rbp), %edx
	jge	.L1313
	.p2align 4,,10
	.p2align 3
.L1332:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L1334
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L1334:
	cmpl	%r13d, %edx
	je	.L1348
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L1332
.L1313:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1348:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L1332
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1325:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L1324
.L1314:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1338:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L1315
	.cfi_endproc
.LFE26928:
	.size	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB26670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	leal	-2(%rax), %r15d
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %ebx
	negl	%r8d
	movzbl	0(%r13,%r9), %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r14d, %r10d
	movq	%r11, %r9
	movl	$1, %r11d
	movl	%ebx, -56(%rbp)
	subl	%eax, %r10d
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1364:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L1362:
	cmpl	%r10d, %ecx
	jg	.L1363
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	cmpb	%r9b, %al
	jne	.L1364
	testl	%r15d, %r15d
	js	.L1359
	movslq	%ecx, %rbx
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	addq	%rsi, %rbx
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1365:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L1359
.L1354:
	movzbl	(%rbx,%rax), %edx
	movl	%eax, %r14d
	cmpb	%dl, 0(%r13,%rax)
	je	.L1365
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %ebx
	movl	-64(%rbp), %edx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L1362
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L1363:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1359:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26670:
	.size	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB26256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r13
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L1371
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L1369
	movzbl	0(%r13), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r14d
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1397:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r12, %rdx
	cmpb	%r14b, (%rdx)
	je	.L1372
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L1371
.L1373:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r15d, %esi
	addq	%r12, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L1397
.L1371:
	movl	$-1, %r9d
.L1366:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1372:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L1371
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1398:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L1375
.L1376:
	movzbl	(%rdx,%rax), %edi
	movl	%eax, %ecx
	cmpb	%dil, 0(%r13,%rax)
	je	.L1398
.L1375:
	cmpl	-56(%rbp), %ecx
	je	.L1366
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L1371
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L1373
.L1369:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L1399
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1381:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1381
.L1380:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L1382
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1400:
	addq	$1, %rax
.L1383:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rax, %rdx
	jne	.L1400
.L1382:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L1399:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L1380
	.cfi_endproc
.LFE26256:
	.size	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv:
.LFB26934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L1402
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L1426
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L1404:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1404
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L1405
.L1403:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L1405
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L1405
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L1405:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L1425:
	cmpl	%eax, %r8d
	jl	.L1407
	.p2align 4,,10
	.p2align 3
.L1411:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L1407
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L1432
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L1411
.L1407:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L1433
.L1412:
	cmpl	%esi, %r12d
	jge	.L1406
.L1435:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1432:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L1411
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1433:
	cmpl	%esi, %r12d
	jl	.L1418
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1434:
	cmpl	%r10d, (%r14)
	jne	.L1415
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L1415:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L1413
.L1418:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L1434
	cmpl	%r12d, %ecx
	jle	.L1401
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L1435
	.p2align 4,,10
	.p2align 3
.L1406:
	cmpl	-64(%rbp), %edx
	jge	.L1401
	.p2align 4,,10
	.p2align 3
.L1420:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L1422
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L1422:
	cmpl	%r13d, %edx
	je	.L1436
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L1420
.L1401:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1436:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L1420
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1413:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L1412
.L1402:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1426:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L1403
	.cfi_endproc
.LFE26934:
	.size	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB26676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -88(%rbp)
	movq	(%rdi), %r10
	movq	%rdi, -80(%rbp)
	leal	-1(%rax), %edx
	movl	%eax, %r15d
	movl	%eax, %edi
	movl	%eax, -56(%rbp)
	movslq	%edx, %r8
	leaq	42372(%r10), %r11
	movl	%edx, %ebx
	negl	%edi
	movzbl	0(%r13,%r8), %r9d
	movl	%r14d, %r8d
	subl	%eax, %r8d
	subl	42372(%r10,%r9,4), %ebx
	movl	$1, %r10d
	movl	%ebx, -64(%rbp)
	leal	-2(%rax), %ebx
	movl	%r10d, %eax
	subl	%r15d, %eax
	movl	%ebx, -60(%rbp)
	movl	%eax, -68(%rbp)
.L1453:
	cmpl	%r8d, %ecx
	jle	.L1442
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1454:
	cmpw	$255, %bx
	ja	.L1440
	movl	%edx, %ebx
	subl	(%r11,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r10d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %edi
	cmpl	%r8d, %ecx
	jg	.L1449
.L1442:
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %ebx
	movq	%rbx, %rax
	cmpl	%ebx, %r9d
	jne	.L1454
	movslq	-60(%rbp), %rax
	testl	%eax, %eax
	js	.L1450
	movslq	%ecx, %rbx
	leaq	(%rsi,%rbx,2), %r15
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1455:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L1450
.L1444:
	movzbl	0(%r13,%rax), %r12d
	movzwl	(%r15,%rax,2), %ebx
	movl	%eax, %r14d
	cmpl	%ebx, %r12d
	je	.L1455
	movl	-56(%rbp), %eax
	movl	-64(%rbp), %ebx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %edi
	testl	%edi, %edi
	jle	.L1453
	movq	-80(%rbp), %rbx
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, 24(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L1449:
	.cfi_restore_state
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	addq	$56, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1440:
	.cfi_restore_state
	addl	-56(%rbp), %ecx
	addl	-68(%rbp), %edi
	jmp	.L1453
	.cfi_endproc
.LFE26676:
	.size	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB26260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L1461
	movl	$-9, %edx
	movq	%rsi, %r12
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L1459
	movzbl	(%r14), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r13d
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1487:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r12,%rdx,2), %rdx
	cmpw	%r13w, (%rdx)
	je	.L1462
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L1461
.L1463:
	movl	-52(%rbp), %edx
	movl	%r15d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	(%r12,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L1487
.L1461:
	movl	$-1, %r9d
.L1456:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L1461
	movl	-56(%rbp), %r8d
	movl	$1, %eax
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1488:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %r8d
	jle	.L1465
.L1466:
	movzbl	(%r14,%rax), %edi
	movzwl	(%rdx,%rax,2), %esi
	movl	%eax, %ecx
	cmpl	%esi, %edi
	je	.L1488
.L1465:
	cmpl	-56(%rbp), %ecx
	je	.L1456
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L1461
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L1463
.L1459:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L1489
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1471:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1471
.L1470:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L1472
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1490:
	addq	$1, %rax
.L1473:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rdx, %rax
	jne	.L1490
.L1472:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L1489:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L1470
	.cfi_endproc
.LFE26260:
	.size	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv:
.LFB26939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L1492
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L1516
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L1494:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1494
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L1495
.L1493:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L1495
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L1495
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L1495:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L1521:
	cmpl	%eax, %r8d
	jl	.L1497
.L1501:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L1497
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L1523
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L1501
.L1497:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L1524
.L1502:
	cmpl	%esi, %r12d
	jge	.L1496
.L1526:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1523:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1524:
	cmpl	%esi, %r12d
	jl	.L1508
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1525:
	cmpl	%r10d, (%r14)
	jne	.L1505
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L1505:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L1503
.L1508:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L1525
	cmpl	%r12d, %ecx
	jle	.L1491
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L1526
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L1491
	.p2align 4,,10
	.p2align 3
.L1510:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L1512
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L1512:
	cmpl	%r13d, %edx
	je	.L1527
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L1510
.L1491:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L1510
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1503:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L1502
.L1492:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1516:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L1493
	.cfi_endproc
.LFE26939:
	.size	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB26681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -64(%rbp)
	movslq	%edx, %r9
	movl	%edx, %ebx
	leaq	42372(%r10), %rdi
	negl	%r8d
	movzwl	(%r14,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r15d, %r10d
	movl	$1, %r11d
	movl	%ebx, -60(%rbp)
	leal	-2(%rax), %ebx
	subl	%eax, %r10d
	movl	%ebx, -56(%rbp)
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1543:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L1541:
	cmpl	%r10d, %ecx
	jg	.L1542
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %ebx
	movq	%rbx, %rax
	cmpw	%bx, %r9w
	jne	.L1543
	movslq	-56(%rbp), %rax
	testl	%eax, %eax
	js	.L1538
	movslq	%ecx, %r13
	addq	%rsi, %r13
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1544:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L1538
.L1533:
	movzbl	0(%r13,%rax), %ebx
	movl	%eax, %r15d
	cmpw	%bx, (%r14,%rax,2)
	je	.L1544
	movl	-64(%rbp), %eax
	movl	-60(%rbp), %ebx
	subl	%r15d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L1541
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L1542:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1538:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26681:
	.size	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB26264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L1550
	movl	$-9, %edx
	movq	%rsi, %r13
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L1548
	movzwl	(%r14), %eax
	leal	1(%rbx), %ecx
	movl	%ecx, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r15d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1577:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r13, %rdx
	cmpb	%r15b, (%rdx)
	je	.L1551
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L1550
.L1552:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r12d, %esi
	addq	%r13, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L1577
.L1550:
	movl	$-1, %r9d
.L1545:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1551:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L1550
	movl	-56(%rbp), %edi
	movl	$1, %eax
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1578:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %edi
	jle	.L1554
.L1555:
	movzbl	(%rdx,%rax), %esi
	movl	%eax, %ecx
	cmpw	%si, (%r14,%rax,2)
	je	.L1578
.L1554:
	cmpl	-56(%rbp), %ecx
	je	.L1545
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L1550
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L1552
.L1548:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L1579
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1560:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L1560
.L1559:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L1561
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L1562
.L1561:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L1579:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L1559
	.cfi_endproc
.LFE26264:
	.size	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv:
.LFB26944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L1581
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L1605
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L1583:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1583
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L1584
.L1582:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L1584
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L1584
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L1584:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L1610:
	cmpl	%eax, %r8d
	jl	.L1586
.L1590:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L1586
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L1612
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L1590
.L1586:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L1613
.L1591:
	cmpl	%esi, %r12d
	jge	.L1585
.L1615:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1612:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1613:
	cmpl	%esi, %r12d
	jl	.L1597
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1614:
	cmpl	%r10d, (%r14)
	jne	.L1594
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L1594:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L1592
.L1597:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L1614
	cmpl	%r12d, %ecx
	jle	.L1580
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L1615
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L1580
	.p2align 4,,10
	.p2align 3
.L1599:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L1601
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L1601:
	cmpl	%r13d, %edx
	je	.L1616
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L1599
.L1580:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L1599
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1592:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L1591
.L1581:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1605:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L1582
	.cfi_endproc
.LFE26944:
	.size	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB26686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %r15d
	negl	%r8d
	movzwl	(%rbx,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %r15d
	movl	%r14d, %r10d
	movl	$1, %r11d
	movl	%r15d, -56(%rbp)
	subl	%eax, %r10d
	leal	-2(%rax), %r15d
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1632:
	movzbl	%al, %eax
	movl	%edx, %r14d
	subl	(%rdi,%rax,4), %r14d
	movl	%r14d, %eax
	addl	%r14d, %ecx
	movl	%r11d, %r14d
	subl	%eax, %r14d
	addl	%r14d, %r8d
.L1630:
	cmpl	%r10d, %ecx
	jg	.L1631
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpw	%r9w, %ax
	jne	.L1632
	testl	%r15d, %r15d
	js	.L1627
	movslq	%ecx, %r13
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	leaq	(%rsi,%r13,2), %r14
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1633:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L1627
.L1622:
	movzwl	(%r14,%rax,2), %edx
	movl	%eax, %r13d
	cmpw	%dx, (%rbx,%rax,2)
	je	.L1633
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %r14d
	movl	-64(%rbp), %edx
	subl	%r13d, %eax
	addl	%r14d, %ecx
	subl	%r14d, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L1630
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L1631:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1627:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26686:
	.size	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB26268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L1639
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L1637
	movzwl	(%r15), %r14d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r14d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r14b
	cmova	%r14d, %eax
	movzbl	%al, %r12d
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1666:
	andq	$-2, %rax
	subq	%r13, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	0(%r13,%rdx,2), %rdx
	cmpw	%r14w, (%rdx)
	je	.L1640
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L1639
.L1641:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	0(%r13,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L1666
.L1639:
	movl	$-1, %r9d
.L1634:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1640:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L1639
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1667:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L1643
.L1644:
	movzwl	(%rdx,%rax,2), %edi
	movl	%eax, %ecx
	cmpw	%di, (%r15,%rax,2)
	je	.L1667
.L1643:
	cmpl	-56(%rbp), %ecx
	je	.L1634
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L1639
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L1641
.L1637:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L1668
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1649:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L1649
.L1648:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L1650
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L1651
.L1650:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L1668:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L1648
	.cfi_endproc
.LFE26268:
	.size	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE, @function
_GLOBAL__sub_I__ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE:
.LFB27401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27401:
	.size	_GLOBAL__sub_I__ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE, .-_GLOBAL__sub_I__ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6RegExp7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEENS_4base5FlagsINS5_4FlagEiEE
	.weak	_ZTVN2v88internal15InterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal15InterruptsScopeE,"awG",@progbits,_ZTVN2v88internal15InterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal15InterruptsScopeE, @object
	.size	_ZTVN2v88internal15InterruptsScopeE, 32
_ZTVN2v88internal15InterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15InterruptsScopeD1Ev
	.quad	_ZN2v88internal15InterruptsScopeD0Ev
	.weak	_ZTVN2v88internal23PostponeInterruptsScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal23PostponeInterruptsScopeE,"awG",@progbits,_ZTVN2v88internal23PostponeInterruptsScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal23PostponeInterruptsScopeE, @object
	.size	_ZTVN2v88internal23PostponeInterruptsScopeE, 32
_ZTVN2v88internal23PostponeInterruptsScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23PostponeInterruptsScopeD1Ev
	.quad	_ZN2v88internal23PostponeInterruptsScopeD0Ev
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
