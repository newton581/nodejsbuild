	.file	"regexp-ast.cc"
	.text
	.section	.text._ZN2v88internal10RegExpTree13IsTextElementEv,"axG",@progbits,_ZN2v88internal10RegExpTree13IsTextElementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree13IsTextElementEv
	.type	_ZN2v88internal10RegExpTree13IsTextElementEv, @function
_ZN2v88internal10RegExpTree13IsTextElementEv:
.LFB7848:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7848:
	.size	_ZN2v88internal10RegExpTree13IsTextElementEv, .-_ZN2v88internal10RegExpTree13IsTextElementEv
	.section	.text._ZN2v88internal10RegExpTree17IsAnchoredAtStartEv,"axG",@progbits,_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.type	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv, @function
_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv:
.LFB7849:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7849:
	.size	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv, .-_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal10RegExpTree15IsAnchoredAtEndEv,"axG",@progbits,_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.type	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv, @function
_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv:
.LFB7850:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7850:
	.size	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv, .-_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.section	.text._ZN2v88internal10RegExpTree16CaptureRegistersEv,"axG",@progbits,_ZN2v88internal10RegExpTree16CaptureRegistersEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.type	_ZN2v88internal10RegExpTree16CaptureRegistersEv, @function
_ZN2v88internal10RegExpTree16CaptureRegistersEv:
.LFB7851:
	.cfi_startproc
	endbr64
	movabsq	$-4294967297, %rax
	ret
	.cfi_endproc
.LFE7851:
	.size	_ZN2v88internal10RegExpTree16CaptureRegistersEv, .-_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.section	.text._ZN2v88internal17RegExpDisjunction9min_matchEv,"axG",@progbits,_ZN2v88internal17RegExpDisjunction9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpDisjunction9min_matchEv
	.type	_ZN2v88internal17RegExpDisjunction9min_matchEv, @function
_ZN2v88internal17RegExpDisjunction9min_matchEv:
.LFB7852:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE7852:
	.size	_ZN2v88internal17RegExpDisjunction9min_matchEv, .-_ZN2v88internal17RegExpDisjunction9min_matchEv
	.section	.text._ZN2v88internal17RegExpDisjunction9max_matchEv,"axG",@progbits,_ZN2v88internal17RegExpDisjunction9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpDisjunction9max_matchEv
	.type	_ZN2v88internal17RegExpDisjunction9max_matchEv, @function
_ZN2v88internal17RegExpDisjunction9max_matchEv:
.LFB7853:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	ret
	.cfi_endproc
.LFE7853:
	.size	_ZN2v88internal17RegExpDisjunction9max_matchEv, .-_ZN2v88internal17RegExpDisjunction9max_matchEv
	.section	.text._ZN2v88internal17RegExpAlternative9min_matchEv,"axG",@progbits,_ZN2v88internal17RegExpAlternative9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpAlternative9min_matchEv
	.type	_ZN2v88internal17RegExpAlternative9min_matchEv, @function
_ZN2v88internal17RegExpAlternative9min_matchEv:
.LFB7855:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE7855:
	.size	_ZN2v88internal17RegExpAlternative9min_matchEv, .-_ZN2v88internal17RegExpAlternative9min_matchEv
	.section	.text._ZN2v88internal17RegExpAlternative9max_matchEv,"axG",@progbits,_ZN2v88internal17RegExpAlternative9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpAlternative9max_matchEv
	.type	_ZN2v88internal17RegExpAlternative9max_matchEv, @function
_ZN2v88internal17RegExpAlternative9max_matchEv:
.LFB7856:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	ret
	.cfi_endproc
.LFE7856:
	.size	_ZN2v88internal17RegExpAlternative9max_matchEv, .-_ZN2v88internal17RegExpAlternative9max_matchEv
	.section	.text._ZN2v88internal15RegExpAssertion9min_matchEv,"axG",@progbits,_ZN2v88internal15RegExpAssertion9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15RegExpAssertion9min_matchEv
	.type	_ZN2v88internal15RegExpAssertion9min_matchEv, @function
_ZN2v88internal15RegExpAssertion9min_matchEv:
.LFB7868:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7868:
	.size	_ZN2v88internal15RegExpAssertion9min_matchEv, .-_ZN2v88internal15RegExpAssertion9min_matchEv
	.section	.text._ZN2v88internal15RegExpAssertion9max_matchEv,"axG",@progbits,_ZN2v88internal15RegExpAssertion9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15RegExpAssertion9max_matchEv
	.type	_ZN2v88internal15RegExpAssertion9max_matchEv, @function
_ZN2v88internal15RegExpAssertion9max_matchEv:
.LFB7869:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7869:
	.size	_ZN2v88internal15RegExpAssertion9max_matchEv, .-_ZN2v88internal15RegExpAssertion9max_matchEv
	.section	.text._ZN2v88internal20RegExpCharacterClass13IsTextElementEv,"axG",@progbits,_ZN2v88internal20RegExpCharacterClass13IsTextElementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClass13IsTextElementEv
	.type	_ZN2v88internal20RegExpCharacterClass13IsTextElementEv, @function
_ZN2v88internal20RegExpCharacterClass13IsTextElementEv:
.LFB7878:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7878:
	.size	_ZN2v88internal20RegExpCharacterClass13IsTextElementEv, .-_ZN2v88internal20RegExpCharacterClass13IsTextElementEv
	.section	.text._ZN2v88internal20RegExpCharacterClass9min_matchEv,"axG",@progbits,_ZN2v88internal20RegExpCharacterClass9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClass9min_matchEv
	.type	_ZN2v88internal20RegExpCharacterClass9min_matchEv, @function
_ZN2v88internal20RegExpCharacterClass9min_matchEv:
.LFB7879:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7879:
	.size	_ZN2v88internal20RegExpCharacterClass9min_matchEv, .-_ZN2v88internal20RegExpCharacterClass9min_matchEv
	.section	.text._ZN2v88internal20RegExpCharacterClass9max_matchEv,"axG",@progbits,_ZN2v88internal20RegExpCharacterClass9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClass9max_matchEv
	.type	_ZN2v88internal20RegExpCharacterClass9max_matchEv, @function
_ZN2v88internal20RegExpCharacterClass9max_matchEv:
.LFB7880:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE7880:
	.size	_ZN2v88internal20RegExpCharacterClass9max_matchEv, .-_ZN2v88internal20RegExpCharacterClass9max_matchEv
	.section	.text._ZN2v88internal10RegExpAtom13IsTextElementEv,"axG",@progbits,_ZN2v88internal10RegExpAtom13IsTextElementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpAtom13IsTextElementEv
	.type	_ZN2v88internal10RegExpAtom13IsTextElementEv, @function
_ZN2v88internal10RegExpAtom13IsTextElementEv:
.LFB7890:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7890:
	.size	_ZN2v88internal10RegExpAtom13IsTextElementEv, .-_ZN2v88internal10RegExpAtom13IsTextElementEv
	.section	.text._ZN2v88internal10RegExpAtom9min_matchEv,"axG",@progbits,_ZN2v88internal10RegExpAtom9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpAtom9min_matchEv
	.type	_ZN2v88internal10RegExpAtom9min_matchEv, @function
_ZN2v88internal10RegExpAtom9min_matchEv:
.LFB7891:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE7891:
	.size	_ZN2v88internal10RegExpAtom9min_matchEv, .-_ZN2v88internal10RegExpAtom9min_matchEv
	.section	.text._ZN2v88internal10RegExpAtom9max_matchEv,"axG",@progbits,_ZN2v88internal10RegExpAtom9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpAtom9max_matchEv
	.type	_ZN2v88internal10RegExpAtom9max_matchEv, @function
_ZN2v88internal10RegExpAtom9max_matchEv:
.LFB7892:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE7892:
	.size	_ZN2v88internal10RegExpAtom9max_matchEv, .-_ZN2v88internal10RegExpAtom9max_matchEv
	.section	.text._ZN2v88internal10RegExpText13IsTextElementEv,"axG",@progbits,_ZN2v88internal10RegExpText13IsTextElementEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpText13IsTextElementEv
	.type	_ZN2v88internal10RegExpText13IsTextElementEv, @function
_ZN2v88internal10RegExpText13IsTextElementEv:
.LFB7900:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7900:
	.size	_ZN2v88internal10RegExpText13IsTextElementEv, .-_ZN2v88internal10RegExpText13IsTextElementEv
	.section	.text._ZN2v88internal10RegExpText9min_matchEv,"axG",@progbits,_ZN2v88internal10RegExpText9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpText9min_matchEv
	.type	_ZN2v88internal10RegExpText9min_matchEv, @function
_ZN2v88internal10RegExpText9min_matchEv:
.LFB7901:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7901:
	.size	_ZN2v88internal10RegExpText9min_matchEv, .-_ZN2v88internal10RegExpText9min_matchEv
	.section	.text._ZN2v88internal10RegExpText9max_matchEv,"axG",@progbits,_ZN2v88internal10RegExpText9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpText9max_matchEv
	.type	_ZN2v88internal10RegExpText9max_matchEv, @function
_ZN2v88internal10RegExpText9max_matchEv:
.LFB7902:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7902:
	.size	_ZN2v88internal10RegExpText9max_matchEv, .-_ZN2v88internal10RegExpText9max_matchEv
	.section	.text._ZN2v88internal16RegExpQuantifier9min_matchEv,"axG",@progbits,_ZN2v88internal16RegExpQuantifier9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpQuantifier9min_matchEv
	.type	_ZN2v88internal16RegExpQuantifier9min_matchEv, @function
_ZN2v88internal16RegExpQuantifier9min_matchEv:
.LFB7908:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7908:
	.size	_ZN2v88internal16RegExpQuantifier9min_matchEv, .-_ZN2v88internal16RegExpQuantifier9min_matchEv
	.section	.text._ZN2v88internal16RegExpQuantifier9max_matchEv,"axG",@progbits,_ZN2v88internal16RegExpQuantifier9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpQuantifier9max_matchEv
	.type	_ZN2v88internal16RegExpQuantifier9max_matchEv, @function
_ZN2v88internal16RegExpQuantifier9max_matchEv:
.LFB7909:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	ret
	.cfi_endproc
.LFE7909:
	.size	_ZN2v88internal16RegExpQuantifier9max_matchEv, .-_ZN2v88internal16RegExpQuantifier9max_matchEv
	.section	.text._ZN2v88internal13RegExpCapture9min_matchEv,"axG",@progbits,_ZN2v88internal13RegExpCapture9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13RegExpCapture9min_matchEv
	.type	_ZN2v88internal13RegExpCapture9min_matchEv, @function
_ZN2v88internal13RegExpCapture9min_matchEv:
.LFB7919:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE7919:
	.size	_ZN2v88internal13RegExpCapture9min_matchEv, .-_ZN2v88internal13RegExpCapture9min_matchEv
	.section	.text._ZN2v88internal13RegExpCapture9max_matchEv,"axG",@progbits,_ZN2v88internal13RegExpCapture9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13RegExpCapture9max_matchEv
	.type	_ZN2v88internal13RegExpCapture9max_matchEv, @function
_ZN2v88internal13RegExpCapture9max_matchEv:
.LFB7920:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE7920:
	.size	_ZN2v88internal13RegExpCapture9max_matchEv, .-_ZN2v88internal13RegExpCapture9max_matchEv
	.section	.text._ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,"axG",@progbits,_ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.type	_ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, @function
_ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE:
.LFB7931:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE7931:
	.size	_ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE, .-_ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.section	.text._ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv,"axG",@progbits,_ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv
	.type	_ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv, @function
_ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv:
.LFB7932:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE7932:
	.size	_ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv, .-_ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv,"axG",@progbits,_ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv
	.type	_ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv, @function
_ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv:
.LFB7933:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.cfi_endproc
.LFE7933:
	.size	_ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv, .-_ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv
	.section	.text._ZN2v88internal11RegExpGroup9min_matchEv,"axG",@progbits,_ZN2v88internal11RegExpGroup9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroup9min_matchEv
	.type	_ZN2v88internal11RegExpGroup9min_matchEv, @function
_ZN2v88internal11RegExpGroup9min_matchEv:
.LFB7934:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE7934:
	.size	_ZN2v88internal11RegExpGroup9min_matchEv, .-_ZN2v88internal11RegExpGroup9min_matchEv
	.section	.text._ZN2v88internal11RegExpGroup9max_matchEv,"axG",@progbits,_ZN2v88internal11RegExpGroup9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroup9max_matchEv
	.type	_ZN2v88internal11RegExpGroup9max_matchEv, @function
_ZN2v88internal11RegExpGroup9max_matchEv:
.LFB7935:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE7935:
	.size	_ZN2v88internal11RegExpGroup9max_matchEv, .-_ZN2v88internal11RegExpGroup9max_matchEv
	.section	.text._ZN2v88internal11RegExpGroup16CaptureRegistersEv,"axG",@progbits,_ZN2v88internal11RegExpGroup16CaptureRegistersEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroup16CaptureRegistersEv
	.type	_ZN2v88internal11RegExpGroup16CaptureRegistersEv, @function
_ZN2v88internal11RegExpGroup16CaptureRegistersEv:
.LFB7936:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE7936:
	.size	_ZN2v88internal11RegExpGroup16CaptureRegistersEv, .-_ZN2v88internal11RegExpGroup16CaptureRegistersEv
	.section	.text._ZN2v88internal16RegExpLookaround9min_matchEv,"axG",@progbits,_ZN2v88internal16RegExpLookaround9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpLookaround9min_matchEv
	.type	_ZN2v88internal16RegExpLookaround9min_matchEv, @function
_ZN2v88internal16RegExpLookaround9min_matchEv:
.LFB7941:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7941:
	.size	_ZN2v88internal16RegExpLookaround9min_matchEv, .-_ZN2v88internal16RegExpLookaround9min_matchEv
	.section	.text._ZN2v88internal16RegExpLookaround9max_matchEv,"axG",@progbits,_ZN2v88internal16RegExpLookaround9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpLookaround9max_matchEv
	.type	_ZN2v88internal16RegExpLookaround9max_matchEv, @function
_ZN2v88internal16RegExpLookaround9max_matchEv:
.LFB7942:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7942:
	.size	_ZN2v88internal16RegExpLookaround9max_matchEv, .-_ZN2v88internal16RegExpLookaround9max_matchEv
	.section	.text._ZN2v88internal19RegExpBackReference9min_matchEv,"axG",@progbits,_ZN2v88internal19RegExpBackReference9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19RegExpBackReference9min_matchEv
	.type	_ZN2v88internal19RegExpBackReference9min_matchEv, @function
_ZN2v88internal19RegExpBackReference9min_matchEv:
.LFB7955:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7955:
	.size	_ZN2v88internal19RegExpBackReference9min_matchEv, .-_ZN2v88internal19RegExpBackReference9min_matchEv
	.section	.text._ZN2v88internal19RegExpBackReference9max_matchEv,"axG",@progbits,_ZN2v88internal19RegExpBackReference9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19RegExpBackReference9max_matchEv
	.type	_ZN2v88internal19RegExpBackReference9max_matchEv, @function
_ZN2v88internal19RegExpBackReference9max_matchEv:
.LFB7956:
	.cfi_startproc
	endbr64
	movl	$2147483647, %eax
	ret
	.cfi_endproc
.LFE7956:
	.size	_ZN2v88internal19RegExpBackReference9max_matchEv, .-_ZN2v88internal19RegExpBackReference9max_matchEv
	.section	.text._ZN2v88internal11RegExpEmpty9min_matchEv,"axG",@progbits,_ZN2v88internal11RegExpEmpty9min_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpEmpty9min_matchEv
	.type	_ZN2v88internal11RegExpEmpty9min_matchEv, @function
_ZN2v88internal11RegExpEmpty9min_matchEv:
.LFB7962:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7962:
	.size	_ZN2v88internal11RegExpEmpty9min_matchEv, .-_ZN2v88internal11RegExpEmpty9min_matchEv
	.section	.text._ZN2v88internal11RegExpEmpty9max_matchEv,"axG",@progbits,_ZN2v88internal11RegExpEmpty9max_matchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpEmpty9max_matchEv
	.type	_ZN2v88internal11RegExpEmpty9max_matchEv, @function
_ZN2v88internal11RegExpEmpty9max_matchEv:
.LFB7963:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7963:
	.size	_ZN2v88internal11RegExpEmpty9max_matchEv, .-_ZN2v88internal11RegExpEmpty9max_matchEv
	.section	.text._ZN2v88internal20RegExpCharacterClass6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpCharacterClass6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal20RegExpCharacterClass6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal20RegExpCharacterClass6AcceptEPNS0_13RegExpVisitorEPv:
.LFB7996:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	movq	40(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7996:
	.size	_ZN2v88internal20RegExpCharacterClass6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal20RegExpCharacterClass6AcceptEPNS0_13RegExpVisitorEPv
	.section	.text._ZN2v88internal16RegExpQuantifier6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpQuantifier6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal16RegExpQuantifier6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal16RegExpQuantifier6AcceptEPNS0_13RegExpVisitorEPv:
.LFB7998:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	movq	56(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7998:
	.size	_ZN2v88internal16RegExpQuantifier6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal16RegExpQuantifier6AcceptEPNS0_13RegExpVisitorEPv
	.section	.text._ZN2v88internal10RegExpText6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpText6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal10RegExpText6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal10RegExpText6AcceptEPNS0_13RegExpVisitorEPv:
.LFB8004:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	movq	104(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE8004:
	.size	_ZN2v88internal10RegExpText6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal10RegExpText6AcceptEPNS0_13RegExpVisitorEPv
	.section	.text._ZN2v88internal10RegExpTree13AsDisjunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.type	_ZN2v88internal10RegExpTree13AsDisjunctionEv, @function
_ZN2v88internal10RegExpTree13AsDisjunctionEv:
.LFB8005:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8005:
	.size	_ZN2v88internal10RegExpTree13AsDisjunctionEv, .-_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.section	.text._ZN2v88internal10RegExpTree13IsDisjunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.type	_ZN2v88internal10RegExpTree13IsDisjunctionEv, @function
_ZN2v88internal10RegExpTree13IsDisjunctionEv:
.LFB8006:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8006:
	.size	_ZN2v88internal10RegExpTree13IsDisjunctionEv, .-_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree6IsTextEv
	.set	_ZN2v88internal10RegExpTree6IsTextEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree7IsEmptyEv
	.set	_ZN2v88internal10RegExpTree7IsEmptyEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.set	_ZN2v88internal10RegExpTree15IsBackReferenceEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.set	_ZN2v88internal10RegExpTree12IsLookaroundEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree7IsGroupEv
	.set	_ZN2v88internal10RegExpTree7IsGroupEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree9IsCaptureEv
	.set	_ZN2v88internal10RegExpTree9IsCaptureEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.set	_ZN2v88internal10RegExpTree12IsQuantifierEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree6IsAtomEv
	.set	_ZN2v88internal10RegExpTree6IsAtomEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.set	_ZN2v88internal10RegExpTree16IsCharacterClassEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree11IsAssertionEv
	.set	_ZN2v88internal10RegExpTree11IsAssertionEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.set	_ZN2v88internal10RegExpTree13IsAlternativeEv,_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.section	.text._ZN2v88internal10RegExpTree13AsAlternativeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.type	_ZN2v88internal10RegExpTree13AsAlternativeEv, @function
_ZN2v88internal10RegExpTree13AsAlternativeEv:
.LFB8007:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8007:
	.size	_ZN2v88internal10RegExpTree13AsAlternativeEv, .-_ZN2v88internal10RegExpTree13AsAlternativeEv
	.section	.text._ZN2v88internal10RegExpTree11AsAssertionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree11AsAssertionEv
	.type	_ZN2v88internal10RegExpTree11AsAssertionEv, @function
_ZN2v88internal10RegExpTree11AsAssertionEv:
.LFB8009:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8009:
	.size	_ZN2v88internal10RegExpTree11AsAssertionEv, .-_ZN2v88internal10RegExpTree11AsAssertionEv
	.section	.text._ZN2v88internal10RegExpTree16AsCharacterClassEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.type	_ZN2v88internal10RegExpTree16AsCharacterClassEv, @function
_ZN2v88internal10RegExpTree16AsCharacterClassEv:
.LFB8011:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8011:
	.size	_ZN2v88internal10RegExpTree16AsCharacterClassEv, .-_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.section	.text._ZN2v88internal10RegExpTree6AsAtomEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree6AsAtomEv
	.type	_ZN2v88internal10RegExpTree6AsAtomEv, @function
_ZN2v88internal10RegExpTree6AsAtomEv:
.LFB8013:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8013:
	.size	_ZN2v88internal10RegExpTree6AsAtomEv, .-_ZN2v88internal10RegExpTree6AsAtomEv
	.section	.text._ZN2v88internal10RegExpTree12AsQuantifierEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.type	_ZN2v88internal10RegExpTree12AsQuantifierEv, @function
_ZN2v88internal10RegExpTree12AsQuantifierEv:
.LFB8015:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8015:
	.size	_ZN2v88internal10RegExpTree12AsQuantifierEv, .-_ZN2v88internal10RegExpTree12AsQuantifierEv
	.section	.text._ZN2v88internal10RegExpTree9AsCaptureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree9AsCaptureEv
	.type	_ZN2v88internal10RegExpTree9AsCaptureEv, @function
_ZN2v88internal10RegExpTree9AsCaptureEv:
.LFB8017:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8017:
	.size	_ZN2v88internal10RegExpTree9AsCaptureEv, .-_ZN2v88internal10RegExpTree9AsCaptureEv
	.section	.text._ZN2v88internal10RegExpTree7AsGroupEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree7AsGroupEv
	.type	_ZN2v88internal10RegExpTree7AsGroupEv, @function
_ZN2v88internal10RegExpTree7AsGroupEv:
.LFB8019:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8019:
	.size	_ZN2v88internal10RegExpTree7AsGroupEv, .-_ZN2v88internal10RegExpTree7AsGroupEv
	.section	.text._ZN2v88internal10RegExpTree12AsLookaroundEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.type	_ZN2v88internal10RegExpTree12AsLookaroundEv, @function
_ZN2v88internal10RegExpTree12AsLookaroundEv:
.LFB8021:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8021:
	.size	_ZN2v88internal10RegExpTree12AsLookaroundEv, .-_ZN2v88internal10RegExpTree12AsLookaroundEv
	.section	.text._ZN2v88internal10RegExpTree15AsBackReferenceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.type	_ZN2v88internal10RegExpTree15AsBackReferenceEv, @function
_ZN2v88internal10RegExpTree15AsBackReferenceEv:
.LFB8023:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8023:
	.size	_ZN2v88internal10RegExpTree15AsBackReferenceEv, .-_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.section	.text._ZN2v88internal10RegExpTree7AsEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree7AsEmptyEv
	.type	_ZN2v88internal10RegExpTree7AsEmptyEv, @function
_ZN2v88internal10RegExpTree7AsEmptyEv:
.LFB8025:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8025:
	.size	_ZN2v88internal10RegExpTree7AsEmptyEv, .-_ZN2v88internal10RegExpTree7AsEmptyEv
	.section	.text._ZN2v88internal10RegExpTree6AsTextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree6AsTextEv
	.type	_ZN2v88internal10RegExpTree6AsTextEv, @function
_ZN2v88internal10RegExpTree6AsTextEv:
.LFB8027:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8027:
	.size	_ZN2v88internal10RegExpTree6AsTextEv, .-_ZN2v88internal10RegExpTree6AsTextEv
	.section	.text._ZN2v88internal17RegExpDisjunction13AsDisjunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction13AsDisjunctionEv
	.type	_ZN2v88internal17RegExpDisjunction13AsDisjunctionEv, @function
_ZN2v88internal17RegExpDisjunction13AsDisjunctionEv:
.LFB8029:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8029:
	.size	_ZN2v88internal17RegExpDisjunction13AsDisjunctionEv, .-_ZN2v88internal17RegExpDisjunction13AsDisjunctionEv
	.section	.text._ZN2v88internal17RegExpDisjunction13IsDisjunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.type	_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv, @function
_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv:
.LFB8030:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE8030:
	.size	_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv, .-_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal11RegExpEmpty7IsEmptyEv
	.set	_ZN2v88internal11RegExpEmpty7IsEmptyEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal19RegExpBackReference15IsBackReferenceEv
	.set	_ZN2v88internal19RegExpBackReference15IsBackReferenceEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal16RegExpLookaround12IsLookaroundEv
	.set	_ZN2v88internal16RegExpLookaround12IsLookaroundEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal11RegExpGroup7IsGroupEv
	.set	_ZN2v88internal11RegExpGroup7IsGroupEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal13RegExpCapture9IsCaptureEv
	.set	_ZN2v88internal13RegExpCapture9IsCaptureEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal16RegExpQuantifier12IsQuantifierEv
	.set	_ZN2v88internal16RegExpQuantifier12IsQuantifierEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpText6IsTextEv
	.set	_ZN2v88internal10RegExpText6IsTextEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal10RegExpAtom6IsAtomEv
	.set	_ZN2v88internal10RegExpAtom6IsAtomEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal20RegExpCharacterClass16IsCharacterClassEv
	.set	_ZN2v88internal20RegExpCharacterClass16IsCharacterClassEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal15RegExpAssertion11IsAssertionEv
	.set	_ZN2v88internal15RegExpAssertion11IsAssertionEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.globl	_ZN2v88internal17RegExpAlternative13IsAlternativeEv
	.set	_ZN2v88internal17RegExpAlternative13IsAlternativeEv,_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.section	.text._ZN2v88internal17RegExpAlternative13AsAlternativeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpAlternative13AsAlternativeEv
	.type	_ZN2v88internal17RegExpAlternative13AsAlternativeEv, @function
_ZN2v88internal17RegExpAlternative13AsAlternativeEv:
.LFB8031:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8031:
	.size	_ZN2v88internal17RegExpAlternative13AsAlternativeEv, .-_ZN2v88internal17RegExpAlternative13AsAlternativeEv
	.section	.text._ZN2v88internal15RegExpAssertion11AsAssertionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RegExpAssertion11AsAssertionEv
	.type	_ZN2v88internal15RegExpAssertion11AsAssertionEv, @function
_ZN2v88internal15RegExpAssertion11AsAssertionEv:
.LFB8033:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8033:
	.size	_ZN2v88internal15RegExpAssertion11AsAssertionEv, .-_ZN2v88internal15RegExpAssertion11AsAssertionEv
	.section	.text._ZN2v88internal20RegExpCharacterClass16AsCharacterClassEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RegExpCharacterClass16AsCharacterClassEv
	.type	_ZN2v88internal20RegExpCharacterClass16AsCharacterClassEv, @function
_ZN2v88internal20RegExpCharacterClass16AsCharacterClassEv:
.LFB8035:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8035:
	.size	_ZN2v88internal20RegExpCharacterClass16AsCharacterClassEv, .-_ZN2v88internal20RegExpCharacterClass16AsCharacterClassEv
	.section	.text._ZN2v88internal10RegExpAtom6AsAtomEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpAtom6AsAtomEv
	.type	_ZN2v88internal10RegExpAtom6AsAtomEv, @function
_ZN2v88internal10RegExpAtom6AsAtomEv:
.LFB8037:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8037:
	.size	_ZN2v88internal10RegExpAtom6AsAtomEv, .-_ZN2v88internal10RegExpAtom6AsAtomEv
	.section	.text._ZN2v88internal16RegExpQuantifier12AsQuantifierEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpQuantifier12AsQuantifierEv
	.type	_ZN2v88internal16RegExpQuantifier12AsQuantifierEv, @function
_ZN2v88internal16RegExpQuantifier12AsQuantifierEv:
.LFB8039:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8039:
	.size	_ZN2v88internal16RegExpQuantifier12AsQuantifierEv, .-_ZN2v88internal16RegExpQuantifier12AsQuantifierEv
	.section	.text._ZN2v88internal13RegExpCapture9AsCaptureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpCapture9AsCaptureEv
	.type	_ZN2v88internal13RegExpCapture9AsCaptureEv, @function
_ZN2v88internal13RegExpCapture9AsCaptureEv:
.LFB8041:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8041:
	.size	_ZN2v88internal13RegExpCapture9AsCaptureEv, .-_ZN2v88internal13RegExpCapture9AsCaptureEv
	.section	.text._ZN2v88internal11RegExpGroup7AsGroupEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpGroup7AsGroupEv
	.type	_ZN2v88internal11RegExpGroup7AsGroupEv, @function
_ZN2v88internal11RegExpGroup7AsGroupEv:
.LFB8043:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8043:
	.size	_ZN2v88internal11RegExpGroup7AsGroupEv, .-_ZN2v88internal11RegExpGroup7AsGroupEv
	.section	.text._ZN2v88internal16RegExpLookaround12AsLookaroundEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpLookaround12AsLookaroundEv
	.type	_ZN2v88internal16RegExpLookaround12AsLookaroundEv, @function
_ZN2v88internal16RegExpLookaround12AsLookaroundEv:
.LFB8045:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8045:
	.size	_ZN2v88internal16RegExpLookaround12AsLookaroundEv, .-_ZN2v88internal16RegExpLookaround12AsLookaroundEv
	.section	.text._ZN2v88internal19RegExpBackReference15AsBackReferenceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19RegExpBackReference15AsBackReferenceEv
	.type	_ZN2v88internal19RegExpBackReference15AsBackReferenceEv, @function
_ZN2v88internal19RegExpBackReference15AsBackReferenceEv:
.LFB8047:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8047:
	.size	_ZN2v88internal19RegExpBackReference15AsBackReferenceEv, .-_ZN2v88internal19RegExpBackReference15AsBackReferenceEv
	.section	.text._ZN2v88internal11RegExpEmpty7AsEmptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpEmpty7AsEmptyEv
	.type	_ZN2v88internal11RegExpEmpty7AsEmptyEv, @function
_ZN2v88internal11RegExpEmpty7AsEmptyEv:
.LFB8049:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8049:
	.size	_ZN2v88internal11RegExpEmpty7AsEmptyEv, .-_ZN2v88internal11RegExpEmpty7AsEmptyEv
	.section	.text._ZN2v88internal10RegExpText6AsTextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpText6AsTextEv
	.type	_ZN2v88internal10RegExpText6AsTextEv, @function
_ZN2v88internal10RegExpText6AsTextEv:
.LFB8051:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8051:
	.size	_ZN2v88internal10RegExpText6AsTextEv, .-_ZN2v88internal10RegExpText6AsTextEv
	.section	.text._ZN2v88internal17RegExpDisjunction16CaptureRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction16CaptureRegistersEv
	.type	_ZN2v88internal17RegExpDisjunction16CaptureRegistersEv, @function
_ZN2v88internal17RegExpDisjunction16CaptureRegistersEv:
.LFB8055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L70
	xorl	%r13d, %r13d
	movl	$-2, %r12d
	movl	$-1, %ebx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	%edx, %r12d
	cmovl	%edx, %r12d
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
.L68:
	addq	$1, %r13
	cmpl	%r13d, 12(%r14)
	jle	.L67
.L69:
	movq	(%r14), %rax
	movq	(%rax,%r13,8), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	%rax, %rdx
	sarq	$32, %rdx
	cmpl	$-1, %eax
	je	.L68
	cmpl	$-1, %ebx
	jne	.L74
	movl	%edx, %r12d
	movl	%eax, %ebx
	addq	$1, %r13
	cmpl	%r13d, 12(%r14)
	jg	.L69
.L67:
	salq	$32, %r12
	movl	%ebx, %eax
	popq	%rbx
	orq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$-2, %r12d
	movl	$-1, %ebx
	jmp	.L67
	.cfi_endproc
.LFE8055:
	.size	_ZN2v88internal17RegExpDisjunction16CaptureRegistersEv, .-_ZN2v88internal17RegExpDisjunction16CaptureRegistersEv
	.section	.text._ZN2v88internal16RegExpLookaround16CaptureRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpLookaround16CaptureRegistersEv
	.type	_ZN2v88internal16RegExpLookaround16CaptureRegistersEv, @function
_ZN2v88internal16RegExpLookaround16CaptureRegistersEv:
.LFB8056:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE8056:
	.size	_ZN2v88internal16RegExpLookaround16CaptureRegistersEv, .-_ZN2v88internal16RegExpLookaround16CaptureRegistersEv
	.section	.text._ZN2v88internal13RegExpCapture16CaptureRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpCapture16CaptureRegistersEv
	.type	_ZN2v88internal13RegExpCapture16CaptureRegistersEv, @function
_ZN2v88internal13RegExpCapture16CaptureRegistersEv:
.LFB8057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %eax
	movq	8(%rdi), %rdi
	leal	(%rax,%rax), %ebx
	movq	(%rdi), %rax
	leal	1(%rbx), %r12d
	call	*72(%rax)
	movq	%rax, %rdx
	sarq	$32, %rdx
	cmpl	$-1, %eax
	je	.L77
	cmpl	%edx, %r12d
	cmovl	%edx, %r12d
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
.L77:
	salq	$32, %r12
	movl	%ebx, %eax
	popq	%rbx
	orq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8057:
	.size	_ZN2v88internal13RegExpCapture16CaptureRegistersEv, .-_ZN2v88internal13RegExpCapture16CaptureRegistersEv
	.section	.text._ZN2v88internal16RegExpQuantifier16CaptureRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpQuantifier16CaptureRegistersEv
	.type	_ZN2v88internal16RegExpQuantifier16CaptureRegistersEv, @function
_ZN2v88internal16RegExpQuantifier16CaptureRegistersEv:
.LFB8058:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE8058:
	.size	_ZN2v88internal16RegExpQuantifier16CaptureRegistersEv, .-_ZN2v88internal16RegExpQuantifier16CaptureRegistersEv
	.section	.text._ZN2v88internal15RegExpAssertion17IsAnchoredAtStartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RegExpAssertion17IsAnchoredAtStartEv
	.type	_ZN2v88internal15RegExpAssertion17IsAnchoredAtStartEv, @function
_ZN2v88internal15RegExpAssertion17IsAnchoredAtStartEv:
.LFB8059:
	.cfi_startproc
	endbr64
	cmpl	$1, 8(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE8059:
	.size	_ZN2v88internal15RegExpAssertion17IsAnchoredAtStartEv, .-_ZN2v88internal15RegExpAssertion17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal15RegExpAssertion15IsAnchoredAtEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RegExpAssertion15IsAnchoredAtEndEv
	.type	_ZN2v88internal15RegExpAssertion15IsAnchoredAtEndEv, @function
_ZN2v88internal15RegExpAssertion15IsAnchoredAtEndEv:
.LFB8060:
	.cfi_startproc
	endbr64
	cmpl	$3, 8(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE8060:
	.size	_ZN2v88internal15RegExpAssertion15IsAnchoredAtEndEv, .-_ZN2v88internal15RegExpAssertion15IsAnchoredAtEndEv
	.section	.text._ZN2v88internal17RegExpAlternative17IsAnchoredAtStartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpAlternative17IsAnchoredAtStartEv
	.type	_ZN2v88internal17RegExpAlternative17IsAnchoredAtStartEv, @function
_ZN2v88internal17RegExpAlternative17IsAnchoredAtStartEv:
.LFB8061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L83
	xorl	%ebx, %ebx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*64(%rax)
	testl	%eax, %eax
	jg	.L83
	addq	$1, %rbx
	cmpl	%ebx, 12(%r13)
	jle	.L83
.L85:
	movq	0(%r13), %rax
	movq	(%rax,%rbx,8), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	je	.L88
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8061:
	.size	_ZN2v88internal17RegExpAlternative17IsAnchoredAtStartEv, .-_ZN2v88internal17RegExpAlternative17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal17RegExpAlternative15IsAnchoredAtEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpAlternative15IsAnchoredAtEndEv
	.type	_ZN2v88internal17RegExpAlternative15IsAnchoredAtEndEv, @function
_ZN2v88internal17RegExpAlternative15IsAnchoredAtEndEv:
.LFB8062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	movslq	12(%r13), %rax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L90
	movslq	%edx, %rbx
	movl	%edx, %edx
	subq	%rdx, %rax
	salq	$3, %rbx
	leaq	-16(,%rax,8), %r14
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*64(%rax)
	testl	%eax, %eax
	jg	.L90
	subq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L90
.L92:
	movq	0(%r13), %rax
	movq	(%rax,%rbx), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L95
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8062:
	.size	_ZN2v88internal17RegExpAlternative15IsAnchoredAtEndEv, .-_ZN2v88internal17RegExpAlternative15IsAnchoredAtEndEv
	.section	.text._ZN2v88internal17RegExpDisjunction17IsAnchoredAtStartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction17IsAnchoredAtStartEv
	.type	_ZN2v88internal17RegExpDisjunction17IsAnchoredAtStartEv, @function
_ZN2v88internal17RegExpDisjunction17IsAnchoredAtStartEv:
.LFB8063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movl	12(%r12), %eax
	testl	%eax, %eax
	jle	.L97
	xorl	%ebx, %ebx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L105:
	addq	$1, %rbx
	cmpl	%ebx, 12(%r12)
	jle	.L97
.L99:
	movq	(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	jne	.L105
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8063:
	.size	_ZN2v88internal17RegExpDisjunction17IsAnchoredAtStartEv, .-_ZN2v88internal17RegExpDisjunction17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal17RegExpDisjunction15IsAnchoredAtEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction15IsAnchoredAtEndEv
	.type	_ZN2v88internal17RegExpDisjunction15IsAnchoredAtEndEv, @function
_ZN2v88internal17RegExpDisjunction15IsAnchoredAtEndEv:
.LFB8064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movl	12(%r12), %eax
	testl	%eax, %eax
	jle	.L107
	xorl	%ebx, %ebx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L115:
	addq	$1, %rbx
	cmpl	%ebx, 12(%r12)
	jle	.L107
.L109:
	movq	(%r12), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	testb	%al, %al
	jne	.L115
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8064:
	.size	_ZN2v88internal17RegExpDisjunction15IsAnchoredAtEndEv, .-_ZN2v88internal17RegExpDisjunction15IsAnchoredAtEndEv
	.section	.text._ZN2v88internal13RegExpCapture17IsAnchoredAtStartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpCapture17IsAnchoredAtStartEv
	.type	_ZN2v88internal13RegExpCapture17IsAnchoredAtStartEv, @function
_ZN2v88internal13RegExpCapture17IsAnchoredAtStartEv:
.LFB8066:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE8066:
	.size	_ZN2v88internal13RegExpCapture17IsAnchoredAtStartEv, .-_ZN2v88internal13RegExpCapture17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal13RegExpCapture15IsAnchoredAtEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpCapture15IsAnchoredAtEndEv
	.type	_ZN2v88internal13RegExpCapture15IsAnchoredAtEndEv, @function
_ZN2v88internal13RegExpCapture15IsAnchoredAtEndEv:
.LFB8067:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.cfi_endproc
.LFE8067:
	.size	_ZN2v88internal13RegExpCapture15IsAnchoredAtEndEv, .-_ZN2v88internal13RegExpCapture15IsAnchoredAtEndEv
	.section	.text._ZN2v88internal14RegExpUnparserD2Ev,"axG",@progbits,_ZN2v88internal14RegExpUnparserD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14RegExpUnparserD2Ev
	.type	_ZN2v88internal14RegExpUnparserD2Ev, @function
_ZN2v88internal14RegExpUnparserD2Ev:
.LFB9035:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9035:
	.size	_ZN2v88internal14RegExpUnparserD2Ev, .-_ZN2v88internal14RegExpUnparserD2Ev
	.weak	_ZN2v88internal14RegExpUnparserD1Ev
	.set	_ZN2v88internal14RegExpUnparserD1Ev,_ZN2v88internal14RegExpUnparserD2Ev
	.section	.text._ZN2v88internal10RegExpTextD2Ev,"axG",@progbits,_ZN2v88internal10RegExpTextD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTextD2Ev
	.type	_ZN2v88internal10RegExpTextD2Ev, @function
_ZN2v88internal10RegExpTextD2Ev:
.LFB9039:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9039:
	.size	_ZN2v88internal10RegExpTextD2Ev, .-_ZN2v88internal10RegExpTextD2Ev
	.weak	_ZN2v88internal10RegExpTextD1Ev
	.set	_ZN2v88internal10RegExpTextD1Ev,_ZN2v88internal10RegExpTextD2Ev
	.section	.text._ZN2v88internal11RegExpEmptyD2Ev,"axG",@progbits,_ZN2v88internal11RegExpEmptyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpEmptyD2Ev
	.type	_ZN2v88internal11RegExpEmptyD2Ev, @function
_ZN2v88internal11RegExpEmptyD2Ev:
.LFB9043:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9043:
	.size	_ZN2v88internal11RegExpEmptyD2Ev, .-_ZN2v88internal11RegExpEmptyD2Ev
	.weak	_ZN2v88internal11RegExpEmptyD1Ev
	.set	_ZN2v88internal11RegExpEmptyD1Ev,_ZN2v88internal11RegExpEmptyD2Ev
	.section	.text._ZN2v88internal19RegExpBackReferenceD2Ev,"axG",@progbits,_ZN2v88internal19RegExpBackReferenceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19RegExpBackReferenceD2Ev
	.type	_ZN2v88internal19RegExpBackReferenceD2Ev, @function
_ZN2v88internal19RegExpBackReferenceD2Ev:
.LFB9047:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9047:
	.size	_ZN2v88internal19RegExpBackReferenceD2Ev, .-_ZN2v88internal19RegExpBackReferenceD2Ev
	.weak	_ZN2v88internal19RegExpBackReferenceD1Ev
	.set	_ZN2v88internal19RegExpBackReferenceD1Ev,_ZN2v88internal19RegExpBackReferenceD2Ev
	.section	.text._ZN2v88internal16RegExpLookaroundD2Ev,"axG",@progbits,_ZN2v88internal16RegExpLookaroundD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpLookaroundD2Ev
	.type	_ZN2v88internal16RegExpLookaroundD2Ev, @function
_ZN2v88internal16RegExpLookaroundD2Ev:
.LFB9051:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9051:
	.size	_ZN2v88internal16RegExpLookaroundD2Ev, .-_ZN2v88internal16RegExpLookaroundD2Ev
	.weak	_ZN2v88internal16RegExpLookaroundD1Ev
	.set	_ZN2v88internal16RegExpLookaroundD1Ev,_ZN2v88internal16RegExpLookaroundD2Ev
	.section	.text._ZN2v88internal11RegExpGroupD2Ev,"axG",@progbits,_ZN2v88internal11RegExpGroupD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroupD2Ev
	.type	_ZN2v88internal11RegExpGroupD2Ev, @function
_ZN2v88internal11RegExpGroupD2Ev:
.LFB9055:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9055:
	.size	_ZN2v88internal11RegExpGroupD2Ev, .-_ZN2v88internal11RegExpGroupD2Ev
	.weak	_ZN2v88internal11RegExpGroupD1Ev
	.set	_ZN2v88internal11RegExpGroupD1Ev,_ZN2v88internal11RegExpGroupD2Ev
	.section	.text._ZN2v88internal13RegExpCaptureD2Ev,"axG",@progbits,_ZN2v88internal13RegExpCaptureD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13RegExpCaptureD2Ev
	.type	_ZN2v88internal13RegExpCaptureD2Ev, @function
_ZN2v88internal13RegExpCaptureD2Ev:
.LFB9059:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9059:
	.size	_ZN2v88internal13RegExpCaptureD2Ev, .-_ZN2v88internal13RegExpCaptureD2Ev
	.weak	_ZN2v88internal13RegExpCaptureD1Ev
	.set	_ZN2v88internal13RegExpCaptureD1Ev,_ZN2v88internal13RegExpCaptureD2Ev
	.section	.text._ZN2v88internal16RegExpQuantifierD2Ev,"axG",@progbits,_ZN2v88internal16RegExpQuantifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpQuantifierD2Ev
	.type	_ZN2v88internal16RegExpQuantifierD2Ev, @function
_ZN2v88internal16RegExpQuantifierD2Ev:
.LFB9063:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9063:
	.size	_ZN2v88internal16RegExpQuantifierD2Ev, .-_ZN2v88internal16RegExpQuantifierD2Ev
	.weak	_ZN2v88internal16RegExpQuantifierD1Ev
	.set	_ZN2v88internal16RegExpQuantifierD1Ev,_ZN2v88internal16RegExpQuantifierD2Ev
	.section	.text._ZN2v88internal10RegExpAtomD2Ev,"axG",@progbits,_ZN2v88internal10RegExpAtomD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpAtomD2Ev
	.type	_ZN2v88internal10RegExpAtomD2Ev, @function
_ZN2v88internal10RegExpAtomD2Ev:
.LFB9067:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9067:
	.size	_ZN2v88internal10RegExpAtomD2Ev, .-_ZN2v88internal10RegExpAtomD2Ev
	.weak	_ZN2v88internal10RegExpAtomD1Ev
	.set	_ZN2v88internal10RegExpAtomD1Ev,_ZN2v88internal10RegExpAtomD2Ev
	.section	.text._ZN2v88internal20RegExpCharacterClassD2Ev,"axG",@progbits,_ZN2v88internal20RegExpCharacterClassD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClassD2Ev
	.type	_ZN2v88internal20RegExpCharacterClassD2Ev, @function
_ZN2v88internal20RegExpCharacterClassD2Ev:
.LFB9071:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9071:
	.size	_ZN2v88internal20RegExpCharacterClassD2Ev, .-_ZN2v88internal20RegExpCharacterClassD2Ev
	.weak	_ZN2v88internal20RegExpCharacterClassD1Ev
	.set	_ZN2v88internal20RegExpCharacterClassD1Ev,_ZN2v88internal20RegExpCharacterClassD2Ev
	.section	.text._ZN2v88internal15RegExpAssertionD2Ev,"axG",@progbits,_ZN2v88internal15RegExpAssertionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15RegExpAssertionD2Ev
	.type	_ZN2v88internal15RegExpAssertionD2Ev, @function
_ZN2v88internal15RegExpAssertionD2Ev:
.LFB9075:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9075:
	.size	_ZN2v88internal15RegExpAssertionD2Ev, .-_ZN2v88internal15RegExpAssertionD2Ev
	.weak	_ZN2v88internal15RegExpAssertionD1Ev
	.set	_ZN2v88internal15RegExpAssertionD1Ev,_ZN2v88internal15RegExpAssertionD2Ev
	.section	.text._ZN2v88internal17RegExpAlternativeD2Ev,"axG",@progbits,_ZN2v88internal17RegExpAlternativeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpAlternativeD2Ev
	.type	_ZN2v88internal17RegExpAlternativeD2Ev, @function
_ZN2v88internal17RegExpAlternativeD2Ev:
.LFB9079:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9079:
	.size	_ZN2v88internal17RegExpAlternativeD2Ev, .-_ZN2v88internal17RegExpAlternativeD2Ev
	.weak	_ZN2v88internal17RegExpAlternativeD1Ev
	.set	_ZN2v88internal17RegExpAlternativeD1Ev,_ZN2v88internal17RegExpAlternativeD2Ev
	.section	.text._ZN2v88internal17RegExpDisjunctionD2Ev,"axG",@progbits,_ZN2v88internal17RegExpDisjunctionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpDisjunctionD2Ev
	.type	_ZN2v88internal17RegExpDisjunctionD2Ev, @function
_ZN2v88internal17RegExpDisjunctionD2Ev:
.LFB9083:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9083:
	.size	_ZN2v88internal17RegExpDisjunctionD2Ev, .-_ZN2v88internal17RegExpDisjunctionD2Ev
	.weak	_ZN2v88internal17RegExpDisjunctionD1Ev
	.set	_ZN2v88internal17RegExpDisjunctionD1Ev,_ZN2v88internal17RegExpDisjunctionD2Ev
	.section	.text._ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv
	.type	_ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv, @function
_ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv:
.LFB8090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-9(%rbp), %rsi
	movb	$37, -9(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
.L134:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8090:
	.size	_ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv, .-_ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv
	.section	.text._ZN2v88internal14RegExpUnparserD0Ev,"axG",@progbits,_ZN2v88internal14RegExpUnparserD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14RegExpUnparserD0Ev
	.type	_ZN2v88internal14RegExpUnparserD0Ev, @function
_ZN2v88internal14RegExpUnparserD0Ev:
.LFB9037:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9037:
	.size	_ZN2v88internal14RegExpUnparserD0Ev, .-_ZN2v88internal14RegExpUnparserD0Ev
	.section	.rodata._ZN2v88internal10ZoneObjectdlEPvm.isra.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal10ZoneObjectdlEPvm.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal10ZoneObjectdlEPvm.isra.0, @function
_ZN2v88internal10ZoneObjectdlEPvm.isra.0:
.LFB9110:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9110:
	.size	_ZN2v88internal10ZoneObjectdlEPvm.isra.0, .-_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.section	.text._ZN2v88internal11RegExpEmptyD0Ev,"axG",@progbits,_ZN2v88internal11RegExpEmptyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpEmptyD0Ev
	.type	_ZN2v88internal11RegExpEmptyD0Ev, @function
_ZN2v88internal11RegExpEmptyD0Ev:
.LFB9045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9045:
	.size	_ZN2v88internal11RegExpEmptyD0Ev, .-_ZN2v88internal11RegExpEmptyD0Ev
	.section	.text._ZN2v88internal19RegExpBackReferenceD0Ev,"axG",@progbits,_ZN2v88internal19RegExpBackReferenceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19RegExpBackReferenceD0Ev
	.type	_ZN2v88internal19RegExpBackReferenceD0Ev, @function
_ZN2v88internal19RegExpBackReferenceD0Ev:
.LFB9049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9049:
	.size	_ZN2v88internal19RegExpBackReferenceD0Ev, .-_ZN2v88internal19RegExpBackReferenceD0Ev
	.section	.text._ZN2v88internal16RegExpLookaroundD0Ev,"axG",@progbits,_ZN2v88internal16RegExpLookaroundD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpLookaroundD0Ev
	.type	_ZN2v88internal16RegExpLookaroundD0Ev, @function
_ZN2v88internal16RegExpLookaroundD0Ev:
.LFB9053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9053:
	.size	_ZN2v88internal16RegExpLookaroundD0Ev, .-_ZN2v88internal16RegExpLookaroundD0Ev
	.section	.text._ZN2v88internal11RegExpGroupD0Ev,"axG",@progbits,_ZN2v88internal11RegExpGroupD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RegExpGroupD0Ev
	.type	_ZN2v88internal11RegExpGroupD0Ev, @function
_ZN2v88internal11RegExpGroupD0Ev:
.LFB9057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9057:
	.size	_ZN2v88internal11RegExpGroupD0Ev, .-_ZN2v88internal11RegExpGroupD0Ev
	.section	.text._ZN2v88internal13RegExpCaptureD0Ev,"axG",@progbits,_ZN2v88internal13RegExpCaptureD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13RegExpCaptureD0Ev
	.type	_ZN2v88internal13RegExpCaptureD0Ev, @function
_ZN2v88internal13RegExpCaptureD0Ev:
.LFB9061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9061:
	.size	_ZN2v88internal13RegExpCaptureD0Ev, .-_ZN2v88internal13RegExpCaptureD0Ev
	.section	.text._ZN2v88internal16RegExpQuantifierD0Ev,"axG",@progbits,_ZN2v88internal16RegExpQuantifierD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16RegExpQuantifierD0Ev
	.type	_ZN2v88internal16RegExpQuantifierD0Ev, @function
_ZN2v88internal16RegExpQuantifierD0Ev:
.LFB9065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9065:
	.size	_ZN2v88internal16RegExpQuantifierD0Ev, .-_ZN2v88internal16RegExpQuantifierD0Ev
	.section	.text._ZN2v88internal10RegExpAtomD0Ev,"axG",@progbits,_ZN2v88internal10RegExpAtomD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpAtomD0Ev
	.type	_ZN2v88internal10RegExpAtomD0Ev, @function
_ZN2v88internal10RegExpAtomD0Ev:
.LFB9069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9069:
	.size	_ZN2v88internal10RegExpAtomD0Ev, .-_ZN2v88internal10RegExpAtomD0Ev
	.section	.text._ZN2v88internal20RegExpCharacterClassD0Ev,"axG",@progbits,_ZN2v88internal20RegExpCharacterClassD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20RegExpCharacterClassD0Ev
	.type	_ZN2v88internal20RegExpCharacterClassD0Ev, @function
_ZN2v88internal20RegExpCharacterClassD0Ev:
.LFB9073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9073:
	.size	_ZN2v88internal20RegExpCharacterClassD0Ev, .-_ZN2v88internal20RegExpCharacterClassD0Ev
	.section	.text._ZN2v88internal15RegExpAssertionD0Ev,"axG",@progbits,_ZN2v88internal15RegExpAssertionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15RegExpAssertionD0Ev
	.type	_ZN2v88internal15RegExpAssertionD0Ev, @function
_ZN2v88internal15RegExpAssertionD0Ev:
.LFB9077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9077:
	.size	_ZN2v88internal15RegExpAssertionD0Ev, .-_ZN2v88internal15RegExpAssertionD0Ev
	.section	.text._ZN2v88internal17RegExpAlternativeD0Ev,"axG",@progbits,_ZN2v88internal17RegExpAlternativeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpAlternativeD0Ev
	.type	_ZN2v88internal17RegExpAlternativeD0Ev, @function
_ZN2v88internal17RegExpAlternativeD0Ev:
.LFB9081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9081:
	.size	_ZN2v88internal17RegExpAlternativeD0Ev, .-_ZN2v88internal17RegExpAlternativeD0Ev
	.section	.text._ZN2v88internal17RegExpDisjunctionD0Ev,"axG",@progbits,_ZN2v88internal17RegExpDisjunctionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17RegExpDisjunctionD0Ev
	.type	_ZN2v88internal17RegExpDisjunctionD0Ev, @function
_ZN2v88internal17RegExpDisjunctionD0Ev:
.LFB9085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9085:
	.size	_ZN2v88internal17RegExpDisjunctionD0Ev, .-_ZN2v88internal17RegExpDisjunctionD0Ev
	.section	.text._ZN2v88internal10RegExpTextD0Ev,"axG",@progbits,_ZN2v88internal10RegExpTextD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10RegExpTextD0Ev
	.type	_ZN2v88internal10RegExpTextD0Ev, @function
_ZN2v88internal10RegExpTextD0Ev:
.LFB9041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal10ZoneObjectdlEPvm.isra.0
	.cfi_endproc
.LFE9041:
	.size	_ZN2v88internal10RegExpTextD0Ev, .-_ZN2v88internal10RegExpTextD0Ev
	.section	.text._ZN2v88internal16RegExpLookaround17IsAnchoredAtStartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpLookaround17IsAnchoredAtStartEv
	.type	_ZN2v88internal16RegExpLookaround17IsAnchoredAtStartEv, @function
_ZN2v88internal16RegExpLookaround17IsAnchoredAtStartEv:
.LFB8065:
	.cfi_startproc
	endbr64
	cmpb	$0, 16(%rdi)
	je	.L162
	movl	28(%rdi), %eax
	testl	%eax, %eax
	jne	.L162
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L162:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8065:
	.size	_ZN2v88internal16RegExpLookaround17IsAnchoredAtStartEv, .-_ZN2v88internal16RegExpLookaround17IsAnchoredAtStartEv
	.section	.text._ZN2v88internal11RegExpEmpty6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpEmpty6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal11RegExpEmpty6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal11RegExpEmpty6AcceptEPNS0_13RegExpVisitorEPv:
.LFB8003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv(%rip), %rcx
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	96(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L169
	movq	8(%rsi), %rdi
	movl	$1, %edx
	leaq	-9(%rbp), %rsi
	movb	$37, -9(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
.L168:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L173
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	%r8, %rsi
	call	*%rax
	jmp	.L168
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8003:
	.size	_ZN2v88internal11RegExpEmpty6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal11RegExpEmpty6AcceptEPNS0_13RegExpVisitorEPv
	.section	.text._ZN2v88internal17RegExpAlternative16CaptureRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpAlternative16CaptureRegistersEv
	.type	_ZN2v88internal17RegExpAlternative16CaptureRegistersEv, @function
_ZN2v88internal17RegExpAlternative16CaptureRegistersEv:
.LFB8054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L178
	xorl	%r13d, %r13d
	movl	$-2, %r12d
	movl	$-1, %ebx
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L182:
	cmpl	%edx, %r12d
	cmovl	%edx, %r12d
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
.L176:
	addq	$1, %r13
	cmpl	%r13d, 12(%r14)
	jle	.L175
.L177:
	movq	(%r14), %rax
	movq	(%rax,%r13,8), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	%rax, %rdx
	sarq	$32, %rdx
	cmpl	$-1, %eax
	je	.L176
	cmpl	$-1, %ebx
	jne	.L182
	movl	%edx, %r12d
	movl	%eax, %ebx
	addq	$1, %r13
	cmpl	%r13d, 12(%r14)
	jg	.L177
.L175:
	salq	$32, %r12
	movl	%ebx, %eax
	popq	%rbx
	orq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	$-2, %r12d
	movl	$-1, %ebx
	jmp	.L175
	.cfi_endproc
.LFE8054:
	.size	_ZN2v88internal17RegExpAlternative16CaptureRegistersEv, .-_ZN2v88internal17RegExpAlternative16CaptureRegistersEv
	.section	.rodata._ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(<- "
.LC2:
	.string	")"
	.section	.text._ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv:
.LFB8002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rsi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	88(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L189
	movq	8(%rsi), %r13
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rax
	movq	%r13, %rdi
	movl	16(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8002:
	.size	_ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv
	.section	.rodata._ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"(^ "
	.section	.text._ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv:
.LFB7999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L196
	movq	8(%rsi), %rdi
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7999:
	.size	_ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv
	.section	.rodata._ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"(?: "
	.section	.text._ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv:
.LFB8000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L203
	movq	8(%rsi), %rdi
	movl	$4, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8000:
	.size	_ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv
	.section	.rodata._ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"'"
	.section	.text._ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv:
.LFB7997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L205
	movq	8(%rsi), %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rbx
	testl	%eax, %eax
	jle	.L206
	subl	$1, %eax
	leaq	-42(%rbp), %r14
	leaq	2(%rbx,%rax,2), %r13
	.p2align 4,,10
	.p2align 3
.L207:
	movzwl	(%rbx), %eax
	movq	8(%r12), %rdi
	movq	%r14, %rsi
	addq	$2, %rbx
	movw	%ax, -42(%rbp)
	call	_ZN2v88internallsERSoRKNS0_6AsUC16E@PLT
	cmpq	%r13, %rbx
	jne	.L207
.L206:
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
.L204:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L212
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L204
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7997:
	.size	_ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv
	.section	.text._ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv
	.type	_ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv, @function
_ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv:
.LFB8089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movl	16(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8089:
	.size	_ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv, .-_ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv
	.section	.text._ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv
	.type	_ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv, @function
_ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv:
.LFB8086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC3(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$3, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8086:
	.size	_ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv, .-_ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv
	.section	.text._ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv
	.type	_ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv, @function
_ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv:
.LFB8087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	leaq	.LC4(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	movl	$4, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8087:
	.size	_ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv, .-_ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv
	.section	.text._ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv
	.type	_ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv, @function
_ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv:
.LFB8083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	.LC5(%rip), %rsi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r12), %rax
	movq	8(%r12), %rbx
	testl	%eax, %eax
	jle	.L220
	subl	$1, %eax
	leaq	-42(%rbp), %r12
	leaq	2(%rbx,%rax,2), %r13
	.p2align 4,,10
	.p2align 3
.L221:
	movzwl	(%rbx), %eax
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	addq	$2, %rbx
	movw	%ax, -42(%rbp)
	call	_ZN2v88internallsERSoRKNS0_6AsUC16E@PLT
	cmpq	%r13, %rbx
	jne	.L221
.L220:
	movq	8(%r14), %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8083:
	.size	_ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv, .-_ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv
	.section	.rodata._ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"(:"
.LC7:
	.string	" "
	.section	.text._ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv:
.LFB7994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	_ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L227
	movq	8(%rsi), %rdi
	movl	$2, %edx
	xorl	%ebx, %ebx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.L231
	.p2align 4,,10
	.p2align 3
.L230:
	movq	8(%r12), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r14), %rax
	cmpl	%ebx, 12(%rax)
	jg	.L230
.L231:
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7994:
	.size	_ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv
	.section	.rodata._ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"(|"
	.section	.text._ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv:
.LFB7993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	_ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L237
	movq	8(%rsi), %rdi
	movl	$2, %edx
	xorl	%ebx, %ebx
	leaq	.LC8(%rip), %rsi
	leaq	.LC7(%rip), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.L241
	.p2align 4,,10
	.p2align 3
.L240:
	movq	8(%r12), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r14), %rax
	cmpl	%ebx, 12(%rax)
	jg	.L240
.L241:
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7993:
	.size	_ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv
	.section	.text._ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv
	.type	_ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv, @function
_ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv:
.LFB8079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC6(%rip), %rsi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$2, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.L249
	xorl	%ebx, %ebx
	leaq	.LC7(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L248:
	movq	8(%r12), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r15), %rax
	cmpl	%ebx, 12(%rax)
	jg	.L248
.L249:
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8079:
	.size	_ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv, .-_ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv
	.section	.text._ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv
	.type	_ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv, @function
_ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv:
.LFB8078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC8(%rip), %rsi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$2, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	jle	.L257
	xorl	%ebx, %ebx
	leaq	.LC7(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L256:
	movq	8(%r12), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r15), %rax
	cmpl	%ebx, 12(%rax)
	jg	.L256
.L257:
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8078:
	.size	_ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv, .-_ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv
	.section	.rodata._ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv.str1.1,"aMS",@progbits,1
.LC9:
	.string	"(!"
	.section	.text._ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv
	.type	_ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv, @function
_ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv:
.LFB8084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpl	$1, 20(%rsi)
	jne	.L263
	movq	8(%rsi), %rax
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
.L264:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$2, %edx
	leaq	.LC9(%rip), %rsi
	xorl	%ebx, %ebx
	leaq	.LC7(%rip), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %eax
	testl	%eax, %eax
	jle	.L267
	.p2align 4,,10
	.p2align 3
.L266:
	movq	8(%r15), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rax
	movq	%r13, %rdx
	movq	%r15, %rsi
	salq	$4, %rax
	addq	8(%r12), %rax
	addq	$1, %rbx
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpl	%ebx, 20(%r12)
	jg	.L266
.L267:
	movq	8(%r15), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L264
	.cfi_endproc
.LFE8084:
	.size	_ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv, .-_ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv
	.section	.rodata._ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC10:
	.string	" + "
.LC11:
	.string	" - "
.LC12:
	.string	"->"
.LC13:
	.string	"<-"
.LC14:
	.string	"("
	.section	.text._ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv:
.LFB8001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	leaq	_ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L281
	movq	8(%rsi), %rdi
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	28(%r13), %eax
	movq	8(%r12), %rdi
	leaq	.LC12(%rip), %rsi
	movl	$2, %edx
	testl	%eax, %eax
	leaq	.LC13(%rip), %rax
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 16(%r13)
	movq	8(%r12), %rdi
	leaq	.LC11(%rip), %rax
	leaq	.LC10(%rip), %rsi
	movl	$3, %edx
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8001:
	.size	_ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv
	.section	.text._ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv
	.type	_ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv, @function
_ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv:
.LFB8088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC14(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	28(%r12), %eax
	movq	8(%rbx), %rdi
	leaq	.LC12(%rip), %rsi
	movl	$2, %edx
	testl	%eax, %eax
	leaq	.LC13(%rip), %rax
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 16(%r12)
	leaq	.LC11(%rip), %rax
	leaq	.LC10(%rip), %rsi
	cmove	%rax, %rsi
	movq	8(%rbx), %rdi
	movl	$3, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8088:
	.size	_ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv, .-_ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv
	.section	.rodata._ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv.str1.1,"aMS",@progbits,1
.LC15:
	.string	"@^i"
.LC16:
	.string	"@$i"
.LC17:
	.string	"@^l"
.LC18:
	.string	"@$l"
.LC19:
	.string	"@b"
.LC20:
	.string	"@B"
	.section	.text._ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv
	.type	_ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv, @function
_ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv:
.LFB7995:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	leaq	_ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv(%rip), %rcx
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L290
	cmpl	$5, 8(%r8)
	ja	.L301
	movl	8(%r8), %eax
	leaq	.L293(%rip), %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rsi), %rdi
	movslq	(%rdx,%rax,4), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv,"a",@progbits
	.align 4
	.align 4
.L293:
	.long	.L298-.L293
	.long	.L297-.L293
	.long	.L296-.L293
	.long	.L295-.L293
	.long	.L294-.L293
	.long	.L292-.L293
	.section	.text._ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	%r8, %rsi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$2, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L301:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7995:
	.size	_ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv, .-_ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv
	.section	.rodata._ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv.str1.1,"aMS",@progbits,1
.LC21:
	.string	"^"
.LC22:
	.string	"["
.LC23:
	.string	"-"
.LC24:
	.string	"]"
	.section	.text._ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv
	.type	_ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv, @function
_ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv:
.LFB8081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, 28(%rsi)
	jne	.L315
.L305:
	movl	$1, %edx
	addq	$8, %r13
	leaq	-60(%rbp), %r15
	xorl	%ebx, %ebx
	leaq	.LC22(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L307:
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	movq	8(%r12), %rdi
	movq	%r15, %rsi
	movq	(%rax), %rax
	leaq	(%rax,%rbx,8), %rax
	movl	(%rax), %edx
	movl	4(%rax), %r14d
	movl	%edx, -72(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internallsERSoRKNS0_6AsUC32E@PLT
	movl	-72(%rbp), %edx
	cmpl	%edx, %r14d
	je	.L308
	movq	8(%r12), %rdi
	leaq	.LC23(%rip), %rsi
	movl	$1, %edx
	movq	%rdi, -72(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r14d, -60(%rbp)
	call	_ZN2v88internallsERSoRKNS0_6AsUC32E@PLT
.L308:
	addq	$1, %rbx
.L309:
	movq	16(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12CharacterSet6rangesEPNS0_4ZoneE@PLT
	cmpl	%ebx, 12(%rax)
	jle	.L306
	testl	%ebx, %ebx
	je	.L307
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L306:
	movq	8(%r12), %rdi
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rdi
	jmp	.L305
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8081:
	.size	_ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv, .-_ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv
	.section	.rodata._ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv.str1.1,"aMS",@progbits,1
.LC25:
	.string	"p "
.LC26:
	.string	"g "
.LC27:
	.string	"n "
.LC28:
	.string	"(# "
.LC29:
	.string	"- "
	.section	.text._ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv
	.type	_ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv, @function
_ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv:
.LFB8085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movl	$3, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	.LC28(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	16(%r12), %esi
	movq	%r14, %rdi
	call	_ZNSolsEi@PLT
	leaq	.LC7(%rip), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	8(%rbx), %rdi
	cmpl	$2147483647, %esi
	je	.L325
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L319:
	movl	32(%r12), %eax
	movq	8(%rbx), %rdi
	leaq	.LC26(%rip), %rsi
	testl	%eax, %eax
	je	.L320
	cmpl	$2, %eax
	leaq	.LC25(%rip), %rsi
	leaq	.LC27(%rip), %rax
	cmovne	%rax, %rsi
.L320:
	movl	$2, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC29(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L319
	.cfi_endproc
.LFE8085:
	.size	_ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv, .-_ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv
	.section	.text._ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv
	.type	_ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv, @function
_ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv:
.LFB8082:
	.cfi_startproc
	endbr64
	cmpl	$5, 8(%rsi)
	ja	.L336
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rsi), %eax
	leaq	.L329(%rip), %rdx
	movq	8(%rdi), %rdi
	movslq	(%rdx,%rax,4), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv,"a",@progbits
	.align 4
	.align 4
.L329:
	.long	.L334-.L329
	.long	.L333-.L329
	.long	.L332-.L329
	.long	.L331-.L329
	.long	.L330-.L329
	.long	.L328-.L329
	.section	.text._ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv
	.p2align 4,,10
	.p2align 3
.L330:
	movl	$2, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movl	$3, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L336:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8082:
	.size	_ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv, .-_ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv
	.section	.text._ZN2v88internal14RegExpUnparser19VisitCharacterRangeENS0_14CharacterRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14RegExpUnparser19VisitCharacterRangeENS0_14CharacterRangeE
	.type	_ZN2v88internal14RegExpUnparser19VisitCharacterRangeENS0_14CharacterRangeE, @function
_ZN2v88internal14RegExpUnparser19VisitCharacterRangeENS0_14CharacterRangeE:
.LFB8080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-44(%rbp), %r13
	sarq	$32, %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%esi, -44(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internallsERSoRKNS0_6AsUC32E@PLT
	cmpl	%r14d, %ebx
	je	.L339
	movq	8(%r12), %r12
	leaq	.LC23(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r14d, -44(%rbp)
	call	_ZN2v88internallsERSoRKNS0_6AsUC32E@PLT
.L339:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L343:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8080:
	.size	_ZN2v88internal14RegExpUnparser19VisitCharacterRangeENS0_14CharacterRangeE, .-_ZN2v88internal14RegExpUnparser19VisitCharacterRangeENS0_14CharacterRangeE
	.section	.text._ZN2v88internal10RegExpTree5PrintERSoPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10RegExpTree5PrintERSoPNS0_4ZoneE
	.type	_ZN2v88internal10RegExpTree5PrintERSoPNS0_4ZoneE, @function
_ZN2v88internal10RegExpTree5PrintERSoPNS0_4ZoneE:
.LFB8091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal14RegExpUnparserE(%rip), %rax
	movq	%rsi, -40(%rbp)
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -32(%rbp)
	xorl	%edx, %edx
	call	*16(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L347:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8091:
	.size	_ZN2v88internal10RegExpTree5PrintERSoPNS0_4ZoneE, .-_ZN2v88internal10RegExpTree5PrintERSoPNS0_4ZoneE
	.section	.text._ZN2v88internal17RegExpDisjunctionC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpDisjunctionC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.type	_ZN2v88internal17RegExpDisjunctionC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE, @function
_ZN2v88internal17RegExpDisjunctionC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE:
.LFB8093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17RegExpDisjunctionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	movq	%rsi, 8(%rdi)
	movq	(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	movq	%r12, %rdi
	movl	%eax, 16(%rbx)
	movq	(%r12), %rax
	call	*64(%rax)
	cmpl	$1, 12(%r15)
	movl	%eax, 20(%rbx)
	jle	.L348
	movl	$8, %r14d
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L350:
	movq	(%r15), %rax
	movq	(%rax,%r14), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	cmpl	%eax, 16(%rbx)
	cmovle	16(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, 16(%rbx)
	movq	(%r12), %rax
	call	*64(%rax)
	cmpl	%eax, 20(%rbx)
	cmovge	20(%rbx), %eax
	addl	$1, %r13d
	addq	$8, %r14
	movl	%eax, 20(%rbx)
	cmpl	%r13d, 12(%r15)
	jg	.L350
.L348:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8093:
	.size	_ZN2v88internal17RegExpDisjunctionC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE, .-_ZN2v88internal17RegExpDisjunctionC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.globl	_ZN2v88internal17RegExpDisjunctionC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.set	_ZN2v88internal17RegExpDisjunctionC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE,_ZN2v88internal17RegExpDisjunctionC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.section	.text._ZN2v88internal17RegExpAlternativeC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17RegExpAlternativeC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.type	_ZN2v88internal17RegExpAlternativeC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE, @function
_ZN2v88internal17RegExpAlternativeC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE:
.LFB8097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal17RegExpAlternativeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movl	12(%rsi), %eax
	movq	%rsi, 8(%rdi)
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jle	.L353
	movq	%rsi, %r15
	xorl	%r14d, %r14d
	movl	$2147483647, %ebx
	.p2align 4,,10
	.p2align 3
.L358:
	movq	(%r15), %rax
	movq	(%rax,%r14,8), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	movl	16(%r13), %edx
	movl	%ebx, %ecx
	movq	%r12, %rdi
	subl	%edx, %ecx
	addl	%eax, %edx
	cmpl	%ecx, %eax
	movq	(%r12), %rax
	cmovg	%ebx, %edx
	movl	%edx, 16(%r13)
	call	*64(%rax)
	movl	20(%r13), %edx
	movl	%ebx, %ecx
	subl	%edx, %ecx
	cmpl	%eax, %ecx
	jl	.L356
	addl	%edx, %eax
	addq	$1, %r14
	movl	%eax, 20(%r13)
	cmpl	%r14d, 12(%r15)
	jg	.L358
.L353:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movl	$2147483647, 20(%r13)
	addq	$1, %r14
	cmpl	%r14d, 12(%r15)
	jg	.L358
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8097:
	.size	_ZN2v88internal17RegExpAlternativeC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE, .-_ZN2v88internal17RegExpAlternativeC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.globl	_ZN2v88internal17RegExpAlternativeC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.set	_ZN2v88internal17RegExpAlternativeC1EPNS0_8ZoneListIPNS0_10RegExpTreeEEE,_ZN2v88internal17RegExpAlternativeC2EPNS0_8ZoneListIPNS0_10RegExpTreeEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv, @function
_GLOBAL__sub_I__ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv:
.LFB9107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9107:
	.size	_GLOBAL__sub_I__ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv, .-_GLOBAL__sub_I__ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv
	.weak	_ZTVN2v88internal17RegExpDisjunctionE
	.section	.data.rel.ro._ZTVN2v88internal17RegExpDisjunctionE,"awG",@progbits,_ZTVN2v88internal17RegExpDisjunctionE,comdat
	.align 8
	.type	_ZTVN2v88internal17RegExpDisjunctionE, @object
	.size	_ZTVN2v88internal17RegExpDisjunctionE, 296
_ZTVN2v88internal17RegExpDisjunctionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17RegExpDisjunctionD1Ev
	.quad	_ZN2v88internal17RegExpDisjunctionD0Ev
	.quad	_ZN2v88internal17RegExpDisjunction6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal17RegExpDisjunction6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal17RegExpDisjunction17IsAnchoredAtStartEv
	.quad	_ZN2v88internal17RegExpDisjunction15IsAnchoredAtEndEv
	.quad	_ZN2v88internal17RegExpDisjunction9min_matchEv
	.quad	_ZN2v88internal17RegExpDisjunction9max_matchEv
	.quad	_ZN2v88internal17RegExpDisjunction16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal17RegExpDisjunction13AsDisjunctionEv
	.quad	_ZN2v88internal17RegExpDisjunction13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal17RegExpAlternativeE
	.section	.data.rel.ro._ZTVN2v88internal17RegExpAlternativeE,"awG",@progbits,_ZTVN2v88internal17RegExpAlternativeE,comdat
	.align 8
	.type	_ZTVN2v88internal17RegExpAlternativeE, @object
	.size	_ZTVN2v88internal17RegExpAlternativeE, 296
_ZTVN2v88internal17RegExpAlternativeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal17RegExpAlternativeD1Ev
	.quad	_ZN2v88internal17RegExpAlternativeD0Ev
	.quad	_ZN2v88internal17RegExpAlternative6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal17RegExpAlternative6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal17RegExpAlternative17IsAnchoredAtStartEv
	.quad	_ZN2v88internal17RegExpAlternative15IsAnchoredAtEndEv
	.quad	_ZN2v88internal17RegExpAlternative9min_matchEv
	.quad	_ZN2v88internal17RegExpAlternative9max_matchEv
	.quad	_ZN2v88internal17RegExpAlternative16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal17RegExpAlternative13AsAlternativeEv
	.quad	_ZN2v88internal17RegExpAlternative13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal15RegExpAssertionE
	.section	.data.rel.ro._ZTVN2v88internal15RegExpAssertionE,"awG",@progbits,_ZTVN2v88internal15RegExpAssertionE,comdat
	.align 8
	.type	_ZTVN2v88internal15RegExpAssertionE, @object
	.size	_ZTVN2v88internal15RegExpAssertionE, 296
_ZTVN2v88internal15RegExpAssertionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15RegExpAssertionD1Ev
	.quad	_ZN2v88internal15RegExpAssertionD0Ev
	.quad	_ZN2v88internal15RegExpAssertion6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal15RegExpAssertion6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal15RegExpAssertion17IsAnchoredAtStartEv
	.quad	_ZN2v88internal15RegExpAssertion15IsAnchoredAtEndEv
	.quad	_ZN2v88internal15RegExpAssertion9min_matchEv
	.quad	_ZN2v88internal15RegExpAssertion9max_matchEv
	.quad	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal15RegExpAssertion11AsAssertionEv
	.quad	_ZN2v88internal15RegExpAssertion11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal20RegExpCharacterClassE
	.section	.data.rel.ro._ZTVN2v88internal20RegExpCharacterClassE,"awG",@progbits,_ZTVN2v88internal20RegExpCharacterClassE,comdat
	.align 8
	.type	_ZTVN2v88internal20RegExpCharacterClassE, @object
	.size	_ZTVN2v88internal20RegExpCharacterClassE, 296
_ZTVN2v88internal20RegExpCharacterClassE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20RegExpCharacterClassD1Ev
	.quad	_ZN2v88internal20RegExpCharacterClassD0Ev
	.quad	_ZN2v88internal20RegExpCharacterClass6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal20RegExpCharacterClass6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal20RegExpCharacterClass13IsTextElementEv
	.quad	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	_ZN2v88internal20RegExpCharacterClass9min_matchEv
	.quad	_ZN2v88internal20RegExpCharacterClass9max_matchEv
	.quad	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.quad	_ZN2v88internal20RegExpCharacterClass12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal20RegExpCharacterClass16AsCharacterClassEv
	.quad	_ZN2v88internal20RegExpCharacterClass16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal10RegExpAtomE
	.section	.data.rel.ro._ZTVN2v88internal10RegExpAtomE,"awG",@progbits,_ZTVN2v88internal10RegExpAtomE,comdat
	.align 8
	.type	_ZTVN2v88internal10RegExpAtomE, @object
	.size	_ZTVN2v88internal10RegExpAtomE, 296
_ZTVN2v88internal10RegExpAtomE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10RegExpAtomD1Ev
	.quad	_ZN2v88internal10RegExpAtomD0Ev
	.quad	_ZN2v88internal10RegExpAtom6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal10RegExpAtom6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpAtom13IsTextElementEv
	.quad	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	_ZN2v88internal10RegExpAtom9min_matchEv
	.quad	_ZN2v88internal10RegExpAtom9max_matchEv
	.quad	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpAtom12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpAtom6AsAtomEv
	.quad	_ZN2v88internal10RegExpAtom6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal16RegExpQuantifierE
	.section	.data.rel.ro._ZTVN2v88internal16RegExpQuantifierE,"awG",@progbits,_ZTVN2v88internal16RegExpQuantifierE,comdat
	.align 8
	.type	_ZTVN2v88internal16RegExpQuantifierE, @object
	.size	_ZTVN2v88internal16RegExpQuantifierE, 296
_ZTVN2v88internal16RegExpQuantifierE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal16RegExpQuantifierD1Ev
	.quad	_ZN2v88internal16RegExpQuantifierD0Ev
	.quad	_ZN2v88internal16RegExpQuantifier6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal16RegExpQuantifier6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	_ZN2v88internal16RegExpQuantifier9min_matchEv
	.quad	_ZN2v88internal16RegExpQuantifier9max_matchEv
	.quad	_ZN2v88internal16RegExpQuantifier16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal16RegExpQuantifier12AsQuantifierEv
	.quad	_ZN2v88internal16RegExpQuantifier12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal13RegExpCaptureE
	.section	.data.rel.ro._ZTVN2v88internal13RegExpCaptureE,"awG",@progbits,_ZTVN2v88internal13RegExpCaptureE,comdat
	.align 8
	.type	_ZTVN2v88internal13RegExpCaptureE, @object
	.size	_ZTVN2v88internal13RegExpCaptureE, 296
_ZTVN2v88internal13RegExpCaptureE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13RegExpCaptureD1Ev
	.quad	_ZN2v88internal13RegExpCaptureD0Ev
	.quad	_ZN2v88internal13RegExpCapture6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal13RegExpCapture6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal13RegExpCapture17IsAnchoredAtStartEv
	.quad	_ZN2v88internal13RegExpCapture15IsAnchoredAtEndEv
	.quad	_ZN2v88internal13RegExpCapture9min_matchEv
	.quad	_ZN2v88internal13RegExpCapture9max_matchEv
	.quad	_ZN2v88internal13RegExpCapture16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal13RegExpCapture9AsCaptureEv
	.quad	_ZN2v88internal13RegExpCapture9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal11RegExpGroupE
	.section	.data.rel.ro._ZTVN2v88internal11RegExpGroupE,"awG",@progbits,_ZTVN2v88internal11RegExpGroupE,comdat
	.align 8
	.type	_ZTVN2v88internal11RegExpGroupE, @object
	.size	_ZTVN2v88internal11RegExpGroupE, 296
_ZTVN2v88internal11RegExpGroupE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11RegExpGroupD1Ev
	.quad	_ZN2v88internal11RegExpGroupD0Ev
	.quad	_ZN2v88internal11RegExpGroup6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal11RegExpGroup6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal11RegExpGroup17IsAnchoredAtStartEv
	.quad	_ZN2v88internal11RegExpGroup15IsAnchoredAtEndEv
	.quad	_ZN2v88internal11RegExpGroup9min_matchEv
	.quad	_ZN2v88internal11RegExpGroup9max_matchEv
	.quad	_ZN2v88internal11RegExpGroup16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal11RegExpGroup7AsGroupEv
	.quad	_ZN2v88internal11RegExpGroup7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal16RegExpLookaroundE
	.section	.data.rel.ro._ZTVN2v88internal16RegExpLookaroundE,"awG",@progbits,_ZTVN2v88internal16RegExpLookaroundE,comdat
	.align 8
	.type	_ZTVN2v88internal16RegExpLookaroundE, @object
	.size	_ZTVN2v88internal16RegExpLookaroundE, 296
_ZTVN2v88internal16RegExpLookaroundE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal16RegExpLookaroundD1Ev
	.quad	_ZN2v88internal16RegExpLookaroundD0Ev
	.quad	_ZN2v88internal16RegExpLookaround6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal16RegExpLookaround6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal16RegExpLookaround17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	_ZN2v88internal16RegExpLookaround9min_matchEv
	.quad	_ZN2v88internal16RegExpLookaround9max_matchEv
	.quad	_ZN2v88internal16RegExpLookaround16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal16RegExpLookaround12AsLookaroundEv
	.quad	_ZN2v88internal16RegExpLookaround12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal19RegExpBackReferenceE
	.section	.data.rel.ro._ZTVN2v88internal19RegExpBackReferenceE,"awG",@progbits,_ZTVN2v88internal19RegExpBackReferenceE,comdat
	.align 8
	.type	_ZTVN2v88internal19RegExpBackReferenceE, @object
	.size	_ZTVN2v88internal19RegExpBackReferenceE, 296
_ZTVN2v88internal19RegExpBackReferenceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19RegExpBackReferenceD1Ev
	.quad	_ZN2v88internal19RegExpBackReferenceD0Ev
	.quad	_ZN2v88internal19RegExpBackReference6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal19RegExpBackReference6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	_ZN2v88internal19RegExpBackReference9min_matchEv
	.quad	_ZN2v88internal19RegExpBackReference9max_matchEv
	.quad	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal19RegExpBackReference15AsBackReferenceEv
	.quad	_ZN2v88internal19RegExpBackReference15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal11RegExpEmptyE
	.section	.data.rel.ro._ZTVN2v88internal11RegExpEmptyE,"awG",@progbits,_ZTVN2v88internal11RegExpEmptyE,comdat
	.align 8
	.type	_ZTVN2v88internal11RegExpEmptyE, @object
	.size	_ZTVN2v88internal11RegExpEmptyE, 296
_ZTVN2v88internal11RegExpEmptyE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11RegExpEmptyD1Ev
	.quad	_ZN2v88internal11RegExpEmptyD0Ev
	.quad	_ZN2v88internal11RegExpEmpty6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal11RegExpEmpty6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpTree13IsTextElementEv
	.quad	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	_ZN2v88internal11RegExpEmpty9min_matchEv
	.quad	_ZN2v88internal11RegExpEmpty9max_matchEv
	.quad	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpTree12AppendToTextEPNS0_10RegExpTextEPNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal11RegExpEmpty7AsEmptyEv
	.quad	_ZN2v88internal11RegExpEmpty7IsEmptyEv
	.quad	_ZN2v88internal10RegExpTree6AsTextEv
	.quad	_ZN2v88internal10RegExpTree6IsTextEv
	.weak	_ZTVN2v88internal10RegExpTextE
	.section	.data.rel.ro._ZTVN2v88internal10RegExpTextE,"awG",@progbits,_ZTVN2v88internal10RegExpTextE,comdat
	.align 8
	.type	_ZTVN2v88internal10RegExpTextE, @object
	.size	_ZTVN2v88internal10RegExpTextE, 296
_ZTVN2v88internal10RegExpTextE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal10RegExpTextD1Ev
	.quad	_ZN2v88internal10RegExpTextD0Ev
	.quad	_ZN2v88internal10RegExpText6AcceptEPNS0_13RegExpVisitorEPv
	.quad	_ZN2v88internal10RegExpText6ToNodeEPNS0_14RegExpCompilerEPNS0_10RegExpNodeE
	.quad	_ZN2v88internal10RegExpText13IsTextElementEv
	.quad	_ZN2v88internal10RegExpTree17IsAnchoredAtStartEv
	.quad	_ZN2v88internal10RegExpTree15IsAnchoredAtEndEv
	.quad	_ZN2v88internal10RegExpText9min_matchEv
	.quad	_ZN2v88internal10RegExpText9max_matchEv
	.quad	_ZN2v88internal10RegExpTree16CaptureRegistersEv
	.quad	_ZN2v88internal10RegExpText12AppendToTextEPS1_PNS0_4ZoneE
	.quad	_ZN2v88internal10RegExpTree13AsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13IsDisjunctionEv
	.quad	_ZN2v88internal10RegExpTree13AsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree13IsAlternativeEv
	.quad	_ZN2v88internal10RegExpTree11AsAssertionEv
	.quad	_ZN2v88internal10RegExpTree11IsAssertionEv
	.quad	_ZN2v88internal10RegExpTree16AsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree16IsCharacterClassEv
	.quad	_ZN2v88internal10RegExpTree6AsAtomEv
	.quad	_ZN2v88internal10RegExpTree6IsAtomEv
	.quad	_ZN2v88internal10RegExpTree12AsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree12IsQuantifierEv
	.quad	_ZN2v88internal10RegExpTree9AsCaptureEv
	.quad	_ZN2v88internal10RegExpTree9IsCaptureEv
	.quad	_ZN2v88internal10RegExpTree7AsGroupEv
	.quad	_ZN2v88internal10RegExpTree7IsGroupEv
	.quad	_ZN2v88internal10RegExpTree12AsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree12IsLookaroundEv
	.quad	_ZN2v88internal10RegExpTree15AsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree15IsBackReferenceEv
	.quad	_ZN2v88internal10RegExpTree7AsEmptyEv
	.quad	_ZN2v88internal10RegExpTree7IsEmptyEv
	.quad	_ZN2v88internal10RegExpText6AsTextEv
	.quad	_ZN2v88internal10RegExpText6IsTextEv
	.weak	_ZTVN2v88internal14RegExpUnparserE
	.section	.data.rel.ro.local._ZTVN2v88internal14RegExpUnparserE,"awG",@progbits,_ZTVN2v88internal14RegExpUnparserE,comdat
	.align 8
	.type	_ZTVN2v88internal14RegExpUnparserE, @object
	.size	_ZTVN2v88internal14RegExpUnparserE, 128
_ZTVN2v88internal14RegExpUnparserE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14RegExpUnparserD1Ev
	.quad	_ZN2v88internal14RegExpUnparserD0Ev
	.quad	_ZN2v88internal14RegExpUnparser16VisitDisjunctionEPNS0_17RegExpDisjunctionEPv
	.quad	_ZN2v88internal14RegExpUnparser16VisitAlternativeEPNS0_17RegExpAlternativeEPv
	.quad	_ZN2v88internal14RegExpUnparser14VisitAssertionEPNS0_15RegExpAssertionEPv
	.quad	_ZN2v88internal14RegExpUnparser19VisitCharacterClassEPNS0_20RegExpCharacterClassEPv
	.quad	_ZN2v88internal14RegExpUnparser9VisitAtomEPNS0_10RegExpAtomEPv
	.quad	_ZN2v88internal14RegExpUnparser15VisitQuantifierEPNS0_16RegExpQuantifierEPv
	.quad	_ZN2v88internal14RegExpUnparser12VisitCaptureEPNS0_13RegExpCaptureEPv
	.quad	_ZN2v88internal14RegExpUnparser10VisitGroupEPNS0_11RegExpGroupEPv
	.quad	_ZN2v88internal14RegExpUnparser15VisitLookaroundEPNS0_16RegExpLookaroundEPv
	.quad	_ZN2v88internal14RegExpUnparser18VisitBackReferenceEPNS0_19RegExpBackReferenceEPv
	.quad	_ZN2v88internal14RegExpUnparser10VisitEmptyEPNS0_11RegExpEmptyEPv
	.quad	_ZN2v88internal14RegExpUnparser9VisitTextEPNS0_10RegExpTextEPv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
