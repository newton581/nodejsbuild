	.file	"lookup-cache.cc"
	.text
	.section	.text._ZN2v88internal21DescriptorLookupCache5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DescriptorLookupCache5ClearEv
	.type	_ZN2v88internal21DescriptorLookupCache5ClearEv, @function
_ZN2v88internal21DescriptorLookupCache5ClearEv:
.LFB6157:
	.cfi_startproc
	endbr64
	leaq	1024(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L2:
	movq	$0, (%rdi)
	addq	$16, %rdi
	cmpq	%rax, %rdi
	jne	.L2
	ret
	.cfi_endproc
.LFE6157:
	.size	_ZN2v88internal21DescriptorLookupCache5ClearEv, .-_ZN2v88internal21DescriptorLookupCache5ClearEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21DescriptorLookupCache5ClearEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21DescriptorLookupCache5ClearEv, @function
_GLOBAL__sub_I__ZN2v88internal21DescriptorLookupCache5ClearEv:
.LFB7017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7017:
	.size	_GLOBAL__sub_I__ZN2v88internal21DescriptorLookupCache5ClearEv, .-_GLOBAL__sub_I__ZN2v88internal21DescriptorLookupCache5ClearEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21DescriptorLookupCache5ClearEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
