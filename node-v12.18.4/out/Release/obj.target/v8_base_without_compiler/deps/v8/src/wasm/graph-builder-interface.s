	.file	"graph-builder-interface.cc"
	.text
	.section	.text._ZN2v88internal4wasm7Decoder12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm7Decoder12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.type	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, @function
_ZN2v88internal4wasm7Decoder12onFirstErrorEv:
.LFB6912:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6912:
	.size	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, .-_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE12onFirstErrorEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE12onFirstErrorEv, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE12onFirstErrorEv:
.LFB24578:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	%rax, 24(%rdi)
	ret
	.cfi_endproc
.LFE24578:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE12onFirstErrorEv, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE12onFirstErrorEv
	.section	.text._ZN2v88internal4wasm7DecoderD2Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD2Ev
	.type	_ZN2v88internal4wasm7DecoderD2Ev, @function
_ZN2v88internal4wasm7DecoderD2Ev:
.LFB21722:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L4
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE21722:
	.size	_ZN2v88internal4wasm7DecoderD2Ev, .-_ZN2v88internal4wasm7DecoderD2Ev
	.weak	_ZN2v88internal4wasm7DecoderD1Ev
	.set	_ZN2v88internal4wasm7DecoderD1Ev,_ZN2v88internal4wasm7DecoderD2Ev
	.section	.text._ZN2v88internal4wasm7DecoderD0Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD0Ev
	.type	_ZN2v88internal4wasm7DecoderD0Ev, @function
_ZN2v88internal4wasm7DecoderD0Ev:
.LFB21724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21724:
	.size	_ZN2v88internal4wasm7DecoderD0Ev, .-_ZN2v88internal4wasm7DecoderD0Ev
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED2Ev, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED2Ev:
.LFB24542:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L9
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	ret
	.cfi_endproc
.LFE24542:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED2Ev, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED2Ev
	.set	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED1Ev,_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED2Ev
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED0Ev, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED0Ev:
.LFB24544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$256, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE24544:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED0Ev, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED0Ev
	.section	.text._ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm, @function
_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm:
.LFB24340:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	16(%rdi), %rdi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movabsq	$-6148914691236517205, %rax
	sarq	$3, %rdi
	imulq	%rax, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	leaq	(%rax,%rax,2), %r13
	leaq	0(,%r13,8), %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	(%r12), %r15
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%r12), %rax
	cmpq	%rax, %r15
	je	.L15
	movq	%r15, %rdi
	call	free@PLT
.L15:
	leaq	0(%r13,%rbx), %rax
	movq	%r13, (%r12)
	addq	%r14, %r13
	movq	%r13, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24340:
	.size	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm, .-_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<end>"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0:
.LFB24678:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movzbl	(%rsi), %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	%r12d, %edi
	testb	%al, %al
	je	.L21
	leaq	1(%rbx), %rax
	cmpq	%rax, 0(%r13)
	ja	.L22
	addq	$8, %rsp
	leaq	.LC0(%rip), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movzbl	1(%rbx), %r12d
	sall	$8, %edi
	orl	%r12d, %edi
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	.cfi_endproc
.LFE24678:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	.section	.text._ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0, @function
_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0:
.LFB24716:
	.cfi_startproc
	leal	-7(%rdi), %eax
	cmpb	$2, %al
	setbe	%al
	cmpb	$6, %sil
	sete	%dl
	andb	%dl, %al
	je	.L25
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	subl	$7, %esi
	andl	$253, %esi
	sete	%al
	cmpb	$8, %dil
	sete	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE24716:
	.size	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0, .-_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0:
.LFB24742:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L30
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L28:
	movl	$2, (%rax)
	movq	48(%rbx), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdx, 48(%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movdqu	24(%rbx), %xmm1
	movups	%xmm1, 24(%rax)
	movq	40(%rbx), %rdx
	movq	%rdx, 40(%rax)
	movups	%xmm0, 8(%rbx)
	pxor	%xmm0, %xmm0
	movl	$1, (%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 40(%rbx)
	movups	%xmm0, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L28
	.cfi_endproc
.LFE24742:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0, .-_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv:
.LFB23593:
	.cfi_startproc
	movq	240(%rdi), %rax
	movl	-132(%rax), %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	200(%rdi), %rdx
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	208(%rdi), %rdx
	je	.L32
	movq	%rdx, 208(%rdi)
.L32:
	cmpq	$0, 56(%rdi)
	jne	.L33
	cmpb	$0, -120(%rax)
	jne	.L33
	movq	136(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movl	$0, (%rdx)
	movq	$0, 48(%rdx)
	movq	$0, 40(%rdx)
	movups	%xmm0, 8(%rdx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rdx)
.L33:
	movb	$2, -120(%rax)
	ret
	.cfi_endproc
.LFE23593:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
	.section	.rodata._ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_:
.LFB24185:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L50
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L44
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L51
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L36:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L52
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L39:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L51:
	testq	%rcx, %rcx
	jne	.L53
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L37:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L40
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L41:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L41
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L40:
	cmpq	%r12, %rbx
	je	.L42
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L43:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L43
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L42:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L36
.L52:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L39
.L53:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L36
.L50:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24185:
	.size	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0:
.LFB24741:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	192(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	(%rsi,%rsi,2), %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	200(%rdi), %rax
	movq	%rdi, %rbx
	leaq	(%rax,%rdx,8), %rsi
	cmpq	%rsi, 208(%rdi)
	je	.L55
	movq	%rsi, 208(%rdi)
.L55:
	movl	0(%r13), %eax
	cmpl	$1, %eax
	je	.L67
	testl	%eax, %eax
	je	.L54
	xorl	%r12d, %r12d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L68:
	movdqu	(%rdx), %xmm0
	addl	$1, %r12d
	movups	%xmm0, (%rsi)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 208(%rbx)
	cmpl	%r12d, 0(%r13)
	jbe	.L54
.L61:
	movq	208(%rbx), %rsi
.L62:
	movl	%r12d, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	8(%r13), %rax
	leaq	(%rax,%rdx,8), %rdx
	cmpq	216(%rbx), %rsi
	jne	.L68
	movq	%r14, %rdi
	addl	$1, %r12d
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	cmpl	%r12d, 0(%r13)
	ja	.L61
.L54:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	cmpq	216(%rbx), %rsi
	je	.L57
	movdqu	8(%r13), %xmm1
	movups	%xmm1, (%rsi)
	movq	24(%r13), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 208(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	popq	%rbx
	leaq	8(%r13), %rdx
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.cfi_endproc
.LFE24741:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0:
.LFB24749:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	128(%rdi), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L77
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdi)
.L71:
	movq	8(%rbx), %rax
	movq	176(%r13), %rdx
	subl	168(%r13), %edx
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	salq	$3, %rdx
	movl	$2, (%r12)
	movq	%rax, 16(%r12)
	jne	.L78
	movq	$0, 48(%r12)
.L75:
	movdqu	24(%rbx), %xmm0
	movups	%xmm0, 24(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	128(%r13), %r8
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L79
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L74:
	movq	%rdi, 48(%r12)
	movq	48(%rbx), %rsi
	call	memcpy@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r8, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L74
	.cfi_endproc
.LFE24749:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0, .-_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"0 < len"
.LC3:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0:
.LFB24935:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L107
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	%esi, %r12d
	leaq	-320(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -336(%rbp)
	movq	$256, -328(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L109
	movq	-336(%rbp), %r14
	leaq	-400(%rbp), %r13
	movslq	%eax, %r15
	leaq	-416(%rbp), %rdi
	movq	%r13, -416(%rbp)
	testq	%r14, %r14
	je	.L110
	movq	%r15, -424(%rbp)
	cmpl	$15, %eax
	jg	.L111
	movq	%r13, %rdi
	cmpl	$1, %eax
	jne	.L86
	movzbl	(%r14), %eax
	movq	%r13, %rdx
	movb	%al, -400(%rbp)
	movl	$1, %eax
.L87:
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	-416(%rbp), %rax
	movl	%r12d, -384(%rbp)
	movq	%r14, -376(%rbp)
	cmpq	%r13, %rax
	je	.L112
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %rdx
	movl	%r12d, 40(%rbx)
	movq	%rax, -376(%rbp)
	movq	48(%rbx), %rdi
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%r13, -416(%rbp)
	movq	$0, -408(%rbp)
	movb	$0, -400(%rbp)
	cmpq	%r14, %rax
	je	.L89
	leaq	64(%rbx), %rsi
	cmpq	%rsi, %rdi
	je	.L113
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	64(%rbx), %rsi
	movq	%rax, 48(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	testq	%rdi, %rdi
	je	.L95
	movq	%rdi, -376(%rbp)
	movq	%rsi, -360(%rbp)
.L93:
	movq	$0, -368(%rbp)
	movb	$0, (%rdi)
	movq	-376(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	-416(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	-424(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -416(%rbp)
	movq	%rax, %rdi
	movq	-424(%rbp), %rax
	movq	%rax, -400(%rbp)
.L86:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-424(%rbp), %rax
	movq	-416(%rbp), %rdx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-408(%rbp), %rdx
	movdqa	-400(%rbp), %xmm2
	movl	%r12d, 40(%rbx)
	movq	$0, -408(%rbp)
	movq	48(%rbx), %rdi
	movq	%rdx, -368(%rbp)
	movb	$0, -400(%rbp)
	movups	%xmm2, -360(%rbp)
.L89:
	testq	%rdx, %rdx
	je	.L91
	cmpq	$1, %rdx
	je	.L114
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-368(%rbp), %rdx
	movq	48(%rbx), %rdi
.L91:
	movq	%rdx, 56(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-376(%rbp), %rdi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 48(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 56(%rbx)
.L95:
	movq	%r14, -376(%rbp)
	leaq	-360(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L114:
	movzbl	-360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-368(%rbp), %rdx
	movq	48(%rbx), %rdi
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L108:
	call	__stack_chk_fail@PLT
.L110:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE24935:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0
	.section	.rodata._ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"%s"
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0:
.LFB24936:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L116
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L116:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	.LC5(%rip), %rdx
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24936:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0, @function
_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0:
.LFB24937:
	.cfi_startproc
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rdx
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	subq	8(%rdi), %rsi
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0
	.cfi_endproc
.LFE24937:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0, .-_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0, @function
_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0:
.LFB24938:
	.cfi_startproc
	movq	%rdx, %rcx
	subq	8(%rdi), %rsi
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0
	.cfi_endproc
.LFE24938:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0, .-_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0, @function
_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0:
.LFB24939:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L123
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L123:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	8(%rdi), %rsi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	addl	32(%rdi), %esi
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24939:
	.size	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0, .-_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_:
.LFB23947:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L128
	movq	(%rsi), %rcx
	movzbl	(%rdx), %eax
	movq	$0, 16(%r12)
	movq	%rcx, (%r12)
	movb	%al, 8(%r12)
	addq	$24, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	movabsq	$-6148914691236517205, %rcx
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	$89478485, %rax
	je	.L142
	testq	%rax, %rax
	je	.L137
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L143
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L131:
	movq	(%rbx), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L144
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L134:
	leaq	(%rax,%rcx), %rdi
	leaq	24(%rax), %rsi
.L132:
	movq	0(%r13), %r8
	movzbl	(%rdx), %edx
	leaq	(%rax,%r15), %rcx
	movq	$0, 16(%rcx)
	movq	%r8, (%rcx)
	movb	%dl, 8(%rcx)
	cmpq	%r14, %r12
	je	.L135
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L136:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L136
	subq	$24, %r12
	subq	%r14, %r12
	shrq	$3, %r12
	leaq	48(%rax,%r12,8), %rsi
.L135:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L145
	movl	$24, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$24, %esi
	movl	$24, %ecx
	jmp	.L131
.L144:
	movq	%r8, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L134
.L142:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L145:
	cmpq	$89478485, %rcx
	movl	$89478485, %eax
	cmova	%rax, %rcx
	imulq	$24, %rcx, %rcx
	movq	%rcx, %rsi
	jmp	.L131
	.cfi_endproc
.LFE23947:
	.size	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_, .-_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0.str1.1,"aMS",@progbits,1
.LC6:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0:
.LFB24776:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	movl	%eax, -124(%rbp)
	cmpl	$2, %eax
	je	.L147
	cmpl	$3, %eax
	je	.L148
	cmpl	$1, %eax
	je	.L177
.L149:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$3, (%rdx)
	movq	8(%rdx), %xmm0
	movq	%rdi, %rbx
	leaq	-96(%rbp), %rdx
	movq	8(%rdi), %rdi
	movl	$2, %esi
	movhps	8(%r15), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder5MergeEjPPNS1_4NodeE@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %r8
	movq	16(%r15), %rdx
	movq	16(%r12), %rax
	cmpq	%rax, %rdx
	je	.L151
	movq	%rdx, %xmm3
	movq	8(%rbx), %rdi
	movq	%r8, %rcx
	leaq	-80(%rbp), %rdx
	movq	%rax, %xmm0
	movl	$2, %esi
	movq	%r8, -64(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movq	%r8, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder9EffectPhiEjPPNS1_4NodeES4_@PLT
	movq	-112(%rbp), %r8
	movq	%rax, 16(%r12)
.L151:
	movq	-104(%rbp), %rax
	movq	176(%r14), %r10
	subq	168(%r14), %r10
	movq	8(%rax), %rdi
	subl	$1, %r10d
	js	.L152
	leaq	-80(%rbp), %rax
	movq	%r13, -136(%rbp)
	movslq	%r10d, %rbx
	movq	%r8, %r13
	movq	%rax, -120(%rbp)
	movq	%r12, %r10
	.p2align 4,,10
	.p2align 3
.L154:
	movq	48(%r10), %rax
	leaq	(%rax,%rbx,8), %r12
	movq	48(%r15), %rax
	movq	(%r12), %rdx
	movq	(%rax,%rbx,8), %rax
	cmpq	%rax, %rdx
	je	.L153
	movq	%rax, %xmm1
	movq	-120(%rbp), %rcx
	movq	%r13, %r8
	movq	%r10, -112(%rbp)
	movq	168(%r14), %rax
	movq	%rdx, %xmm0
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movzbl	(%rax,%rbx), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	-112(%rbp), %r10
	movq	%rax, (%r12)
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
.L153:
	subq	$1, %rbx
	testl	%ebx, %ebx
	jns	.L154
	movq	%r13, %r8
	movq	-136(%rbp), %r13
	movq	%r10, %r12
.L152:
	leaq	24(%r15), %rdx
	leaq	24(%r12), %rsi
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder21NewInstanceCacheMergeEPNS1_22WasmInstanceCacheNodesES4_PNS1_4NodeE@PLT
.L150:
	pxor	%xmm0, %xmm0
	movl	$0, (%r15)
	movups	%xmm0, 8(%r15)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%r15)
	movq	$0, 40(%r15)
	movups	%xmm0, 24(%r15)
	movl	0(%r13), %eax
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L146
	movq	208(%r14), %rbx
	movl	$0, %r10d
	movabsq	$-6148914691236517205, %rdx
	movq	%rbx, %rsi
	subq	200(%r14), %rsi
	sarq	$3, %rsi
	imulq	%rdx, %rsi
	movq	240(%r14), %rdx
	movl	-132(%rdx), %edx
	movl	%esi, %edi
	subl	%edx, %edi
	addl	%eax, %edx
	subl	%esi, %edx
	cmpl	%edi, %eax
	movl	%eax, %edi
	cmova	%edx, %r10d
	movq	%rdi, -112(%rbp)
	movl	%r10d, %r15d
	cmpl	%r10d, %eax
	jbe	.L146
	cmpl	$1, -124(%rbp)
	je	.L160
	movq	%r13, %rdi
	movl	%r10d, %r14d
	movq	%r12, %r13
	movq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L163:
	movl	%r14d, %edx
	leaq	8(%r12), %r15
	movq	%rdx, %rcx
	subq	-112(%rbp), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rbx,%rcx,8), %rcx
	cmpl	$1, %eax
	je	.L162
	movq	8(%r12), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %r15
.L162:
	movq	-104(%rbp), %rax
	movq	16(%rcx), %r8
	movq	8(%r13), %rdx
	movq	16(%r15), %rcx
	movq	8(%rax), %rdi
	movzbl	8(%r15), %eax
	cmpb	$9, %al
	ja	.L149
	leaq	CSWTCH.1121(%rip), %rsi
	addl	$1, %r14d
	movzbl	(%rsi,%rax), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder20CreateOrMergeIntoPhiENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%rax, 16(%r15)
	movl	(%r12), %eax
	cmpl	%r14d, %eax
	ja	.L163
.L146:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	16(%rdx), %rax
	addl	$1, %r15d
	movq	%rax, 24(%r13)
	cmpl	%r15d, %ecx
	jbe	.L146
.L160:
	movl	%r15d, %eax
	movq	%rax, %rdx
	subq	-112(%rbp), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rbx,%rdx,8), %rdx
	cmpl	$1, %ecx
	je	.L179
	movq	8(%r13), %rcx
	movq	16(%rdx), %rdx
	leaq	(%rax,%rax,2), %rax
	addl	$1, %r15d
	leaq	(%rcx,%rax,8), %rax
	movq	%rdx, 16(%rax)
	movl	0(%r13), %ecx
	cmpl	%r15d, %ecx
	ja	.L160
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$2, (%rdx)
	movq	48(%r15), %rax
	movq	%rax, 48(%rdx)
	movq	8(%r15), %rax
	movq	%rax, 8(%rdx)
	movq	16(%r15), %rax
	movq	%rax, 16(%rdx)
	movdqu	24(%r15), %xmm2
	movups	%xmm2, 24(%rdx)
	movq	40(%r15), %rax
	movq	%rax, 40(%rdx)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L148:
	movq	8(%rdx), %r11
	movq	%rdi, %rbx
	movq	8(%r15), %rdx
	movq	8(%rdi), %rdi
	movq	%r11, %rsi
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder13AppendToMergeEPNS1_4NodeES4_@PLT
	movq	-112(%rbp), %r11
	movq	8(%rbx), %rdi
	movq	16(%r15), %rcx
	movq	16(%r12), %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder26CreateOrMergeIntoEffectPhiEPNS1_4NodeES4_S4_@PLT
	movq	-112(%rbp), %r11
	movq	%rax, 16(%r12)
	movq	168(%r14), %rax
	movl	%eax, %ebx
	notl	%ebx
	addl	176(%r14), %ebx
	js	.L155
	movq	%r13, -120(%rbp)
	movslq	%ebx, %rbx
	movq	%r11, %r13
	movq	%r14, -112(%rbp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L180:
	movq	-112(%rbp), %rax
	movq	168(%rax), %rax
.L156:
	movq	48(%r15), %rdx
	movq	-104(%rbp), %rdi
	movzbl	(%rax,%rbx), %eax
	movq	(%rdx,%rbx,8), %r8
	movq	48(%r12), %rdx
	movq	8(%rdi), %rdi
	leaq	(%rdx,%rbx,8), %r14
	movq	(%r14), %rcx
	cmpb	$9, %al
	ja	.L149
	leaq	CSWTCH.1121(%rip), %rsi
	movq	%r13, %rdx
	subq	$1, %rbx
	movzbl	(%rsi,%rax), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder20CreateOrMergeIntoPhiENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%rax, (%r14)
	testl	%ebx, %ebx
	jns	.L180
	movq	%r13, %r11
	movq	-112(%rbp), %r14
	movq	-120(%rbp), %r13
.L155:
	movq	-104(%rbp), %rax
	leaq	24(%r15), %rdx
	leaq	24(%r12), %rsi
	movq	%r11, %rcx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder22MergeInstanceCacheIntoEPNS1_22WasmInstanceCacheNodesES4_PNS1_4NodeE@PLT
	jmp	.L150
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24776:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0, .-_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0:
.LFB24753:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-104(%rbp), %rcx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	movq	8(%rdi), %rdi
	leaq	-112(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder15ThrowsExceptionEPNS1_4NodeEPS4_S5_@PLT
	testb	%al, %al
	jne	.L210
.L182:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	movq	-120(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	128(%r14), %rdi
	movq	0(%r13), %rbx
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L212
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdi)
.L184:
	movl	$2, (%r12)
	movq	48(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 48(%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	movdqu	24(%rbx), %xmm2
	movups	%xmm2, 24(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	movl	$1, (%rbx)
	movq	-112(%rbp), %rax
	movups	%xmm0, 8(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rbx)
	movq	$0, 40(%rbx)
	movups	%xmm0, 24(%rbx)
	movq	%rax, 8(%r12)
	movq	128(%r14), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L213
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L186:
	movq	8(%r12), %rax
	movq	176(%r14), %rdx
	subl	168(%r14), %edx
	movq	%rax, 8(%rbx)
	movq	16(%r12), %rax
	salq	$3, %rdx
	movl	$2, (%rbx)
	movq	%rax, 16(%rbx)
	jne	.L214
	movq	$0, 48(%rbx)
.L190:
	movdqu	24(%r12), %xmm3
	movq	-104(%rbp), %rdx
	movups	%xmm3, 24(%rbx)
	movq	40(%r12), %rax
	movq	%rdx, 8(%rbx)
	movq	%rax, 40(%rbx)
	movq	240(%r14), %rcx
	movq	%rcx, %rax
	subq	232(%r14), %rax
	sarq	$3, %rax
	imull	$-252645135, %eax, %eax
	subl	16(%r13), %eax
	leal	-1(%rax), %esi
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%rsi, %rax
	salq	$3, %rax
	subq	%rax, %rcx
	movq	-16(%rcx), %rax
	movq	(%rax), %r10
	movq	%rax, -136(%rbp)
	movl	(%r10), %eax
	cmpl	$2, %eax
	je	.L191
	cmpl	$3, %eax
	jne	.L215
	movq	8(%r10), %r11
	movq	8(%r13), %rdi
	movq	%r10, -144(%rbp)
	movq	%r11, %rsi
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder13AppendToMergeEPNS1_4NodeES4_@PLT
	movq	-144(%rbp), %r10
	movq	-128(%rbp), %r11
	movq	16(%rbx), %rcx
	movq	8(%r13), %rdi
	movq	16(%r10), %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder26CreateOrMergeIntoEffectPhiEPNS1_4NodeES4_S4_@PLT
	movq	-144(%rbp), %r10
	movq	-128(%rbp), %r11
	movq	%rax, 16(%r10)
	movq	168(%r14), %rax
	movl	%eax, %r15d
	notl	%r15d
	addl	176(%r14), %r15d
	js	.L199
	movq	%r12, -152(%rbp)
	movslq	%r15d, %r15
	movq	%r10, %r12
	movq	%r13, -128(%rbp)
	movq	%rbx, %r13
	movq	%r11, %rbx
	movq	%r14, -144(%rbp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-144(%rbp), %rax
	movq	168(%rax), %rax
.L200:
	movq	48(%r13), %rdx
	movq	-128(%rbp), %rcx
	movzbl	(%rax,%r15), %eax
	movq	(%rdx,%r15,8), %r8
	movq	48(%r12), %rdx
	movq	8(%rcx), %rdi
	leaq	(%rdx,%r15,8), %r14
	movq	(%r14), %rcx
	cmpb	$9, %al
	ja	.L193
	leaq	CSWTCH.1121(%rip), %rsi
	movq	%rbx, %rdx
	subq	$1, %r15
	movzbl	(%rsi,%rax), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder20CreateOrMergeIntoPhiENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%rax, (%r14)
	testl	%r15d, %r15d
	jns	.L216
	movq	%r12, %r10
	movq	%rbx, %r11
	movq	-152(%rbp), %r12
	movq	%r13, %rbx
	movq	-128(%rbp), %r13
.L199:
	movq	8(%r13), %rdi
	leaq	24(%rbx), %rdx
	leaq	24(%r10), %rsi
	movq	%r11, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder22MergeInstanceCacheIntoEPNS1_22WasmInstanceCacheNodesES4_PNS1_4NodeE@PLT
.L194:
	pxor	%xmm0, %xmm0
	movq	-136(%rbp), %rsi
	movl	$0, (%rbx)
	movups	%xmm0, 8(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rbx)
	movq	$0, 40(%rbx)
	movups	%xmm0, 24(%rbx)
	movq	8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L217
	movq	(%rsi), %rax
	movq	8(%r13), %rdi
	movq	%rsi, %rbx
	movl	$4, %esi
	movq	-104(%rbp), %r8
	movq	8(%rax), %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder20CreateOrMergeIntoPhiENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%rax, 8(%rbx)
.L202:
	movq	8(%r13), %rax
	leaq	8(%r12), %rdx
	movq	%r12, 0(%r13)
	movq	%rdx, 24(%rax)
	movq	8(%r13), %rax
	leaq	16(%r12), %rdx
	addq	$24, %r12
	movq	%rdx, 32(%rax)
	movq	8(%r13), %rax
	movq	%r12, 40(%rax)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L215:
	cmpl	$1, %eax
	je	.L218
.L193:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsi)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$3, (%r10)
	movq	8(%r10), %xmm0
	leaq	-96(%rbp), %rdx
	movl	$2, %esi
	movq	8(%r13), %rdi
	movq	%r10, -128(%rbp)
	movhps	8(%rbx), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder5MergeEjPPNS1_4NodeE@PLT
	movq	-128(%rbp), %r10
	movq	%rax, %r15
	movq	%rax, 8(%r10)
	movq	16(%r10), %rax
	movq	16(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L195
	movq	%rdx, %xmm5
	movq	8(%r13), %rdi
	leaq	-80(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rax, %xmm0
	movl	$2, %esi
	movq	%r15, -64(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder9EffectPhiEjPPNS1_4NodeES4_@PLT
	movq	-128(%rbp), %r10
	movq	%rax, 16(%r10)
.L195:
	movq	176(%r14), %r9
	subq	168(%r14), %r9
	movq	8(%r13), %rdi
	subl	$1, %r9d
	js	.L196
	leaq	-80(%rbp), %rax
	movslq	%r9d, %r9
	movq	%r14, %r11
	movq	%r12, -160(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rbx, %r12
	movq	%r9, %r14
	movq	%r13, -144(%rbp)
	movq	%r15, %r13
	movq	%r10, %r15
	.p2align 4,,10
	.p2align 3
.L198:
	movq	48(%r15), %rax
	leaq	(%rax,%r14,8), %rbx
	movq	48(%r12), %rax
	movq	(%rbx), %rdx
	movq	(%rax,%r14,8), %rax
	cmpq	%rax, %rdx
	je	.L197
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r13, %r8
	movl	$2, %edx
	movq	168(%r11), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	-152(%rbp), %rcx
	movq	%r11, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	movzbl	(%rax,%r14), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	-128(%rbp), %r11
	movq	%rax, (%rbx)
	movq	-144(%rbp), %rax
	movq	8(%rax), %rdi
.L197:
	subq	$1, %r14
	testl	%r14d, %r14d
	jns	.L198
	movq	%r15, %r10
	movq	%r12, %rbx
	movq	%r13, %r15
	movq	-160(%rbp), %r12
	movq	-144(%rbp), %r13
.L196:
	leaq	24(%rbx), %rdx
	leaq	24(%r10), %rsi
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder21NewInstanceCacheMergeEPNS1_22WasmInstanceCacheNodesES4_PNS1_4NodeE@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$2, (%r10)
	movq	48(%rbx), %rax
	movq	%rax, 48(%r10)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r10)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r10)
	movdqu	24(%rbx), %xmm4
	movups	%xmm4, 24(%r10)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r10)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L214:
	movq	128(%r14), %r8
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L219
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L189:
	movq	%rdi, 48(%rbx)
	movq	48(%r12), %rsi
	call	memcpy@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r8, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L189
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24753:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0, .-_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface12DoReturnCallEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEjPNS0_8compiler4NodeEPNS0_9SignatureINS1_9ValueTypeEEEjPKNS3_5ValueE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface12DoReturnCallEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEjPNS0_8compiler4NodeEPNS0_9SignatureINS1_9ValueTypeEEEjPKNS3_5ValueE, @function
_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface12DoReturnCallEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEjPNS0_8compiler4NodeEPNS0_9SignatureINS1_9ValueTypeEEEjPKNS3_5ValueE:
.LFB19763:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	8(%r8), %r8
	movq	8(%rdi), %rdx
	movq	16(%rbp), %r15
	leal	1(%r8), %eax
	movq	104(%rdx), %rcx
	cltq
	cmpq	%rcx, %rax
	ja	.L221
	movq	96(%rdx), %r10
.L222:
	movq	%rbx, (%r10)
	testl	%r8d, %r8d
	jle	.L231
	movl	%r8d, %edx
	leaq	16(%r15), %rcx
	leaq	8(%r10), %rax
	leaq	8(%r10,%rdx,8), %rdi
	leaq	(%rdx,%rdx,2), %rdx
	cmpq	%rdi, %rcx
	leaq	(%r15,%rdx,8), %rdx
	leal	-1(%r8), %esi
	setnb	%dil
	cmpq	%rdx, %rax
	setnb	%dl
	orb	%dl, %dil
	je	.L228
	cmpl	$22, %esi
	jbe	.L228
	movl	%r8d, %eax
	movq	%r10, %rdx
	shrl	%eax
	salq	$4, %rax
	addq	%r10, %rax
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%rcx), %xmm0
	addq	$16, %rdx
	addq	$48, %rcx
	movhps	-24(%rcx), %xmm0
	movups	%xmm0, -8(%rdx)
	cmpq	%rax, %rdx
	jne	.L230
	movl	%r8d, %edx
	andl	$-2, %edx
	andl	$1, %r8d
	je	.L231
	movslq	%edx, %rcx
	addl	$1, %edx
	leaq	(%rcx,%rcx,2), %rax
	movslq	%edx, %rdx
	movq	16(%r15,%rax,8), %rax
	movq	%rax, (%r10,%rdx,8)
.L231:
	movq	16(%r13), %r8
	movq	8(%r12), %rdi
	subq	8(%r13), %r8
	testq	%rbx, %rbx
	je	.L255
	movl	%r9d, %edx
	movq	%r10, %rcx
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder18ReturnCallIndirectEjjPPNS1_4NodeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L220
.L254:
	cmpl	$-1, 16(%r12)
	je	.L220
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%r10, %rdx
	movl	%r8d, %ecx
	movl	%r9d, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder10ReturnCallEjPPNS1_4NodeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L254
.L220:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	(%rdx), %rdi
	leaq	5(%rax,%rcx), %rcx
	leaq	0(,%rcx,8), %rsi
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	subq	%r10, %rax
	cmpq	%rax, %rsi
	ja	.L256
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L224:
	movq	%r10, 96(%rdx)
	movq	%rcx, 104(%rdx)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	16(%r10,%rsi,8), %rsi
	.p2align 4,,10
	.p2align 3
.L234:
	movq	(%rcx), %rdx
	addq	$8, %rax
	addq	$24, %rcx
	movq	%rdx, -8(%rax)
	cmpq	%rsi, %rax
	jne	.L234
	jmp	.L231
.L256:
	movl	%r9d, -76(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movl	-76(%rbp), %r9d
	movq	%rax, %r10
	jmp	.L224
	.cfi_endproc
.LFE19763:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface12DoReturnCallEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEjPNS0_8compiler4NodeEPNS0_9SignatureINS1_9ValueTypeEEEjPKNS3_5ValueE, .-_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface12DoReturnCallEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEjPNS0_8compiler4NodeEPNS0_9SignatureINS1_9ValueTypeEEEjPKNS3_5ValueE
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb.str1.1,"aMS",@progbits,1
.LC7:
	.string	"<unknown>"
.LC8:
	.string	"i32"
.LC9:
	.string	"f32"
.LC10:
	.string	"f64"
.LC11:
	.string	"anyref"
.LC12:
	.string	"funcref"
.LC13:
	.string	"nullref"
.LC14:
	.string	"exn"
.LC15:
	.string	"s128"
.LC16:
	.string	"<stmt>"
.LC17:
	.string	"i64"
.LC18:
	.string	"%s found empty stack"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"%s[%d] expected type %s, found %s of type %s"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb:
.LFB23953:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movb	%dl, -64(%rbp)
	movl	(%rsi), %edx
	leal	-1(%rdx), %r14d
	testl	%edx, %edx
	jle	.L258
	movq	208(%rdi), %rbx
	movl	%r14d, %eax
	movl	%r14d, -72(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, -56(%rbp)
	movq	%rsi, %r14
	movq	%rbx, %rcx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L353:
	cmpb	$2, -120(%rsi)
	movq	16(%r12), %r13
	jne	.L352
.L266:
	movq	208(%r12), %rcx
	movl	%edx, %eax
	leaq	1(%r15), %rsi
	cmpq	%r15, -56(%rbp)
	je	.L345
.L354:
	movq	%rsi, %r15
.L303:
	leaq	8(%r14), %rax
	cmpl	$1, %edx
	je	.L260
	movq	8(%r14), %rsi
	leaq	(%r15,%r15,2), %rax
	leaq	(%rsi,%rax,8), %rax
.L260:
	movzbl	8(%rax), %ebx
	movq	%rcx, %rax
	subq	200(%r12), %rax
	movabsq	$-6148914691236517205, %r11
	movq	240(%r12), %rsi
	sarq	$3, %rax
	imulq	%r11, %rax
	movl	-132(%rsi), %edi
	cmpq	%rax, %rdi
	jnb	.L353
	movzbl	-16(%rcx), %eax
	movq	-24(%rcx), %r13
	subq	$24, %rcx
	movq	%rcx, 208(%r12)
	cmpb	%al, %bl
	je	.L351
	cmpb	$6, %bl
	sete	%dl
	cmpb	$8, %al
	sete	%dil
	testb	%dl, %dl
	je	.L326
	testb	%dil, %dil
	je	.L326
.L351:
	movl	(%r14), %edx
	leaq	1(%r15), %rsi
	movl	%edx, %eax
	cmpq	%r15, -56(%rbp)
	jne	.L354
.L345:
	movq	%r14, %r13
	movl	-72(%rbp), %r14d
	movq	%rcx, %rbx
.L302:
	movslq	%r14d, %rdx
	leaq	8(%r13), %r10
	leaq	(%rdx,%rdx,2), %r15
	salq	$3, %r15
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L305:
	movq	8(%r13), %rax
	addq	%r15, %rax
	movzbl	8(%rax), %edx
	cmpq	%rbx, 216(%r12)
	je	.L307
.L355:
	movq	16(%r12), %rax
	movb	%dl, 8(%rbx)
	subl	$1, %r14d
	subq	$24, %r15
	movq	$0, 16(%rbx)
	movq	%rax, (%rbx)
	movq	208(%r12), %rax
	leaq	24(%rax), %rbx
	movq	%rbx, 208(%r12)
	cmpl	$-1, %r14d
	je	.L304
.L360:
	movl	0(%r13), %eax
.L316:
	cmpl	$1, %eax
	jne	.L305
	movq	%r10, %rax
	movzbl	8(%rax), %edx
	cmpq	%rbx, 216(%r12)
	jne	.L355
.L307:
	movq	200(%r12), %r8
	movq	%rbx, %rcx
	movabsq	$-6148914691236517205, %rsi
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	$89478485, %rax
	je	.L356
	testq	%rax, %rax
	je	.L322
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L357
	movl	$2147483640, %esi
	movl	$2147483640, %r9d
.L310:
	movq	192(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r11
	subq	%rax, %r11
	cmpq	%rsi, %r11
	jb	.L358
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L313:
	leaq	(%rax,%r9), %rsi
	leaq	24(%rax), %rdi
	jmp	.L311
.L326:
	leal	-7(%rax), %r9d
	andl	$253, %r9d
	jne	.L327
	testb	%dl, %dl
	jne	.L351
.L327:
	leal	-7(%rbx), %edx
	andb	$-3, %dl
	sete	%dl
	testb	%dil, %dl
	jne	.L349
	cmpb	$10, %al
	setne	%cl
	cmpb	$10, %bl
	setne	%dl
	testb	%dl, %cl
	jne	.L270
.L349:
	movl	(%r14), %edx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L352:
	leaq	.LC0(%rip), %rcx
	cmpq	%r13, 24(%r12)
	jbe	.L263
	movzbl	0(%r13), %edi
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	testb	%al, %al
	je	.L348
	leaq	1(%r13), %rax
	cmpq	%rax, 24(%r12)
	ja	.L265
	movq	16(%r12), %r13
	leaq	.LC0(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	.LC18(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	(%r14), %edx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L357:
	testq	%rsi, %rsi
	jne	.L359
	movl	$24, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
.L311:
	movq	16(%r12), %r9
	addq	%rax, %rcx
	movb	%dl, 8(%rcx)
	movq	%r9, (%rcx)
	movq	$0, 16(%rcx)
	cmpq	%rbx, %r8
	je	.L325
	movq	%r8, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L315:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rdi
	movq	%rdi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L315
	movabsq	$768614336404564651, %rdi
	subq	%r8, %rbx
	leaq	-24(%rbx), %rdx
	shrq	$3, %rdx
	imulq	%rdi, %rdx
	movabsq	$2305843009213693951, %rdi
	andq	%rdi, %rdx
	leaq	6(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rbx
.L314:
	movq	%rax, %xmm0
	subl	$1, %r14d
	subq	$24, %r15
	movq	%rsi, 216(%r12)
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 200(%r12)
	cmpl	$-1, %r14d
	jne	.L360
.L304:
	cmpq	$0, 56(%r12)
	sete	%al
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movzbl	1(%r13), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L348:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r13
	movq	%rax, %rcx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L322:
	movl	$24, %esi
	movl	$24, %r9d
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%rdi, %rbx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L258:
	testl	%r14d, %r14d
	js	.L304
	movq	208(%rdi), %rbx
	movl	%edx, %eax
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L358:
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movb	%dl, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-56(%rbp), %edx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r10
	jmp	.L313
.L356:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	cmpb	$9, %al
	ja	.L272
	leaq	.L274(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb,"a",@progbits
	.align 4
	.align 4
.L274:
	.long	.L283-.L274
	.long	.L318-.L274
	.long	.L281-.L274
	.long	.L280-.L274
	.long	.L279-.L274
	.long	.L278-.L274
	.long	.L277-.L274
	.long	.L276-.L274
	.long	.L275-.L274
	.long	.L273-.L274
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb
.L281:
	leaq	.LC17(%rip), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L282:
	movq	24(%r12), %rdx
	leaq	.LC0(%rip), %r11
	cmpq	%rdx, %r13
	jnb	.L284
	movzbl	0(%r13), %edi
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	testb	%al, %al
	je	.L350
	movq	24(%r12), %rdx
	leaq	1(%r13), %rax
	leaq	.LC0(%rip), %r11
	cmpq	%rax, %rdx
	jbe	.L284
	movzbl	1(%r13), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L350:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdx
	movq	%rax, %r11
.L284:
	cmpb	$9, %bl
	ja	.L286
	leaq	.L288(%rip), %rsi
	movzbl	%bl, %r8d
	movslq	(%rsi,%r8,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb
	.align 4
	.align 4
.L288:
	.long	.L297-.L288
	.long	.L321-.L288
	.long	.L295-.L288
	.long	.L294-.L288
	.long	.L293-.L288
	.long	.L292-.L288
	.long	.L291-.L288
	.long	.L290-.L288
	.long	.L289-.L288
	.long	.L287-.L288
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb
.L295:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L296:
	movq	16(%r12), %rcx
	cmpq	%rdx, %rcx
	jnb	.L301
	movzbl	(%rcx), %edi
	movq	%r9, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movq	-96(%rbp), %rcx
	testb	%al, %al
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %r9
	je	.L361
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L301
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -96(%rbp)
	movq	%r11, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r11
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L299:
	pushq	-80(%rbp)
	movzbl	-64(%rbp), %edx
	xorl	%eax, %eax
	movq	%r13, %rsi
	pushq	%r11
	movq	%r12, %rdi
	leal	(%rdx,%r15), %r8d
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	movq	208(%r12), %rcx
	popq	%rdx
	jmp	.L351
.L321:
	leaq	.LC8(%rip), %r9
	jmp	.L296
.L318:
	leaq	.LC8(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L297:
	leaq	.LC16(%rip), %r9
	jmp	.L296
.L287:
	leaq	.LC14(%rip), %r9
	jmp	.L296
.L275:
	leaq	.LC13(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L276:
	leaq	.LC12(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L277:
	leaq	.LC11(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L278:
	leaq	.LC15(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L279:
	leaq	.LC10(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L280:
	leaq	.LC9(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L291:
	leaq	.LC11(%rip), %r9
	jmp	.L296
.L292:
	leaq	.LC15(%rip), %r9
	jmp	.L296
.L293:
	leaq	.LC10(%rip), %r9
	jmp	.L296
.L294:
	leaq	.LC9(%rip), %r9
	jmp	.L296
.L289:
	leaq	.LC13(%rip), %r9
	jmp	.L296
.L290:
	leaq	.LC12(%rip), %r9
	jmp	.L296
.L283:
	leaq	.LC16(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L273:
	leaq	.LC14(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	.LC0(%rip), %rcx
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%r9, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L299
.L286:
	leaq	.LC7(%rip), %r9
	jmp	.L296
.L272:
	leaq	.LC7(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L282
.L359:
	cmpq	$89478485, %rsi
	movl	$89478485, %eax
	cmova	%rax, %rsi
	imulq	$24, %rsi, %r9
	movq	%r9, %rsi
	jmp	.L310
	.cfi_endproc
.LFE23953:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb.str1.1,"aMS",@progbits,1
.LC20:
	.string	"<bot>"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"expected %u elements on the stack for br to @%d, found %u"
	.align 8
.LC22:
	.string	"type error in merge[%u] (expected %s, got %s)"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb:
.LFB23615:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	240(%rdi), %rcx
	movzbl	(%rsi), %r8d
	cmpb	$0, -120(%rcx)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L363
	leaq	24(%rsi), %rdx
	leaq	64(%rsi), %rax
	cmpb	$3, %r8b
	cmove	%rdx, %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	je	.L362
	movq	208(%rdi), %rdx
	movq	%rdx, %r9
	subq	200(%rdi), %r9
	sarq	$3, %r9
	imull	$-1431655765, %r9d, %r9d
	subl	-132(%rcx), %r9d
	cmpl	%r9d, %r10d
	ja	.L432
	movl	%r10d, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	0(,%rcx,8), %rsi
	cmpl	$1, %r10d
	je	.L368
	subq	%rsi, %rdx
	movq	8(%rax), %r9
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	movq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L372:
	movzbl	8(%r9,%rcx), %eax
	movzbl	8(%r8,%rcx), %edx
	cmpb	%al, %dl
	je	.L369
	leal	-7(%rdx), %esi
	cmpb	$2, %sil
	ja	.L410
	cmpb	$6, %al
	jne	.L410
.L369:
	addl	$1, %r11d
	addq	$24, %rcx
	cmpl	%r11d, %r10d
	jne	.L372
	xorl	%r10d, %r10d
.L362:
	movl	%r10d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	leal	-7(%rax), %esi
	andl	$253, %esi
	jne	.L371
	cmpb	$8, %dl
	je	.L369
.L371:
	cmpb	$10, %dl
	ja	.L375
	leaq	.L377(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb,"a",@progbits
	.align 4
	.align 4
.L377:
	.long	.L387-.L377
	.long	.L407-.L377
	.long	.L385-.L377
	.long	.L384-.L377
	.long	.L383-.L377
	.long	.L382-.L377
	.long	.L381-.L377
	.long	.L380-.L377
	.long	.L379-.L377
	.long	.L378-.L377
	.long	.L376-.L377
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb
	.p2align 4,,10
	.p2align 3
.L432:
	movq	8(%rsi), %r8
	movq	16(%rdi), %rsi
	movl	%r10d, %ecx
	xorl	%eax, %eax
	subl	8(%rdi), %r8d
	leaq	.LC21(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$2, %r10d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	subq	%rsi, %rdx
	movzbl	16(%rax), %eax
	xorl	%r10d, %r10d
	movzbl	8(%rdx), %edx
	cmpb	%al, %dl
	je	.L362
	leal	-7(%rdx), %ecx
	cmpb	$2, %cl
	ja	.L411
	cmpb	$6, %al
	je	.L362
.L411:
	leal	-7(%rax), %ecx
	andl	$253, %ecx
	jne	.L412
	xorl	%r10d, %r10d
	cmpb	$8, %dl
	je	.L362
.L412:
	xorl	%r11d, %r11d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	24(%rsi), %rax
	addq	$64, %rsi
	cmpb	$3, %r8b
	movzbl	%dl, %edx
	cmove	%rax, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb
	xorl	%r10d, %r10d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%r10b
	addl	$1, %r10d
	movl	%r10d, %eax
	ret
.L378:
	.cfi_restore_state
	leaq	.LC14(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L386:
	cmpb	$10, %al
	ja	.L388
	leaq	.L390(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb
	.align 4
	.align 4
.L390:
	.long	.L400-.L390
	.long	.L408-.L390
	.long	.L398-.L390
	.long	.L397-.L390
	.long	.L396-.L390
	.long	.L395-.L390
	.long	.L394-.L390
	.long	.L393-.L390
	.long	.L392-.L390
	.long	.L391-.L390
	.long	.L389-.L390
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	.LC17(%rip), %r8
.L399:
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	movl	%r11d, %ecx
	leaq	.LC22(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$2, %r10d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	leaq	.LC8(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	.LC16(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L389:
	leaq	.LC20(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L391:
	leaq	.LC14(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	.LC13(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	.LC12(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	.LC11(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L395:
	leaq	.LC15(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	.LC10(%rip), %r8
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	.LC9(%rip), %r8
	jmp	.L399
.L380:
	leaq	.LC12(%rip), %r9
	jmp	.L386
.L381:
	leaq	.LC11(%rip), %r9
	jmp	.L386
.L379:
	leaq	.LC13(%rip), %r9
	jmp	.L386
.L382:
	leaq	.LC15(%rip), %r9
	jmp	.L386
.L383:
	leaq	.LC10(%rip), %r9
	jmp	.L386
.L384:
	leaq	.LC9(%rip), %r9
	jmp	.L386
.L387:
	leaq	.LC16(%rip), %r9
	jmp	.L386
.L376:
	leaq	.LC20(%rip), %r9
	jmp	.L386
.L407:
	leaq	.LC8(%rip), %r9
	jmp	.L386
.L385:
	leaq	.LC17(%rip), %r9
	jmp	.L386
.L375:
	leaq	.LC7(%rip), %r9
	jmp	.L386
.L388:
	leaq	.LC7(%rip), %r8
	jmp	.L399
	.cfi_endproc
.LFE23615:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj, @function
_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj:
.LFB19680:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	240(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	subq	232(%rsi), %rax
	sarq	$3, %rax
	imull	$-252645135, %eax, %eax
	subl	$1, %eax
	cmpl	%edx, %eax
	je	.L491
	movl	%edx, %edx
	movq	(%rdi), %r14
	movq	%rdx, %rax
	salq	$4, %rax
	addq	%rax, %rdx
	salq	$3, %rdx
	subq	%rdx, %rcx
	movq	-32(%rcx), %r13
	leaq	-112(%rcx), %rdx
	leaq	-72(%rcx), %rbx
	cmpb	$3, -136(%rcx)
	cmove	%rdx, %rbx
	movl	0(%r13), %eax
	movl	%eax, -124(%rbp)
	cmpl	$2, %eax
	je	.L452
	cmpl	$3, %eax
	je	.L453
	cmpl	$1, %eax
	je	.L492
.L454:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L492:
	movl	$2, 0(%r13)
	movq	48(%r14), %rax
	movq	%rax, 48(%r13)
	movq	8(%r14), %rax
	movq	%rax, 8(%r13)
	movq	16(%r14), %rax
	movq	%rax, 16(%r13)
	movdqu	24(%r14), %xmm2
	movups	%xmm2, 24(%r13)
	movq	40(%r14), %rax
	movq	%rax, 40(%r13)
.L455:
	pxor	%xmm0, %xmm0
	movl	$0, (%r14)
	movups	%xmm0, 8(%r14)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%r14)
	movq	$0, 40(%r14)
	movups	%xmm0, 24(%r14)
	movl	(%rbx), %eax
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L433
	movq	208(%r12), %r15
	movl	%eax, %r14d
	movabsq	$-6148914691236517205, %rsi
	movq	%r15, %rcx
	subq	200(%r12), %rcx
	sarq	$3, %rcx
	imulq	%rsi, %rcx
	movq	240(%r12), %rsi
	movl	-132(%rsi), %r12d
	movl	%ecx, %esi
	subl	%r12d, %esi
	subl	%ecx, %r12d
	movl	$0, %ecx
	addl	%eax, %r12d
	cmpl	%esi, %eax
	cmovbe	%ecx, %r12d
	cmpl	%r12d, %eax
	jbe	.L433
	cmpl	$1, -124(%rbp)
	je	.L465
	movq	%r14, -112(%rbp)
	movl	%r12d, %r14d
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L468:
	movl	%r14d, %edx
	leaq	8(%r12), %rbx
	movq	%rdx, %rcx
	subq	-112(%rbp), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%r15,%rcx,8), %rcx
	cmpl	$1, %eax
	je	.L467
	movq	8(%r12), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rbx
.L467:
	movq	-104(%rbp), %rax
	movq	16(%rcx), %r8
	movq	8(%r13), %rdx
	movq	16(%rbx), %rcx
	movq	8(%rax), %rdi
	movzbl	8(%rbx), %eax
	cmpb	$9, %al
	ja	.L454
	leaq	CSWTCH.1121(%rip), %rsi
	addl	$1, %r14d
	movzbl	(%rsi,%rax), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder20CreateOrMergeIntoPhiENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%rax, 16(%rbx)
	movl	(%r12), %eax
	cmpl	%r14d, %eax
	ja	.L468
.L433:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movq	16(%rcx), %rax
	addl	$1, %r12d
	movq	%rax, 24(%rbx)
	cmpl	%r12d, %edx
	jbe	.L433
.L465:
	movl	%r12d, %eax
	movq	%rax, %rcx
	subq	%r14, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%r15,%rcx,8), %rcx
	cmpl	$1, %edx
	je	.L494
	movq	16(%rcx), %rdx
	movq	8(%rbx), %rcx
	leaq	(%rax,%rax,2), %rax
	addl	$1, %r12d
	leaq	(%rcx,%rax,8), %rax
	movq	%rdx, 16(%rax)
	movl	(%rbx), %edx
	cmpl	%r12d, %edx
	ja	.L465
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L453:
	movq	8(%r13), %r11
	movq	%rdi, %r15
	movq	8(%r14), %rdx
	movq	8(%rdi), %rdi
	movq	%r11, %rsi
	movq	%r11, -112(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder13AppendToMergeEPNS1_4NodeES4_@PLT
	movq	-112(%rbp), %r11
	movq	8(%r15), %rdi
	movq	16(%r14), %rcx
	movq	16(%r13), %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder26CreateOrMergeIntoEffectPhiEPNS1_4NodeES4_S4_@PLT
	movq	-112(%rbp), %r11
	movq	%rax, 16(%r13)
	movq	168(%r12), %rax
	movl	%eax, %r15d
	notl	%r15d
	addl	176(%r12), %r15d
	js	.L460
	movq	%rbx, -120(%rbp)
	movslq	%r15d, %r15
	movq	%r14, %rbx
	movq	%r12, -112(%rbp)
	movq	%r11, %r12
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L495:
	movq	-112(%rbp), %rax
	movq	168(%rax), %rax
.L461:
	movq	48(%rbx), %rdx
	movq	-104(%rbp), %rdi
	movzbl	(%rax,%r15), %eax
	movq	(%rdx,%r15,8), %r8
	movq	48(%r13), %rdx
	movq	8(%rdi), %rdi
	leaq	(%rdx,%r15,8), %r14
	movq	(%r14), %rcx
	cmpb	$9, %al
	ja	.L454
	leaq	CSWTCH.1121(%rip), %rsi
	movq	%r12, %rdx
	subq	$1, %r15
	movzbl	(%rsi,%rax), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder20CreateOrMergeIntoPhiENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%rax, (%r14)
	testl	%r15d, %r15d
	jns	.L495
	movq	%rbx, %r14
	movq	%r12, %r11
	movq	-120(%rbp), %rbx
	movq	-112(%rbp), %r12
.L460:
	movq	-104(%rbp), %rax
	leaq	24(%r14), %rdx
	leaq	24(%r13), %rsi
	movq	%r11, %rcx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder22MergeInstanceCacheIntoEPNS1_22WasmInstanceCacheNodesES4_PNS1_4NodeE@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L452:
	movq	8(%r13), %xmm0
	leaq	-96(%rbp), %rdx
	movl	$2, %esi
	movl	$3, 0(%r13)
	movq	8(%rdi), %rdi
	movhps	8(%r14), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder5MergeEjPPNS1_4NodeE@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %r15
	movq	16(%r14), %rdx
	movq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L456
	movq	%rax, %xmm0
	movq	-104(%rbp), %rax
	movq	%r15, %rcx
	movq	%r15, -64(%rbp)
	movq	%rdx, %xmm3
	movl	$2, %esi
	leaq	-80(%rbp), %rdx
	movq	8(%rax), %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder9EffectPhiEjPPNS1_4NodeES4_@PLT
	movq	%rax, 16(%r13)
.L456:
	movq	-104(%rbp), %rax
	movq	176(%r12), %r10
	subq	168(%r12), %r10
	movq	8(%rax), %rdi
	subl	$1, %r10d
	js	.L457
	leaq	-80(%rbp), %rax
	movslq	%r10d, %r10
	movq	%rbx, -136(%rbp)
	movq	%r12, %r11
	movq	%rax, -120(%rbp)
	movq	%r15, %rax
	movq	%r10, %rbx
	movq	%r13, %r15
	movq	%r14, %r13
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L459:
	movq	48(%r15), %rax
	leaq	(%rax,%rbx,8), %r12
	movq	48(%r13), %rax
	movq	(%r12), %rdx
	movq	(%rax,%rbx,8), %rax
	cmpq	%rax, %rdx
	je	.L458
	movq	%rax, %xmm1
	movq	-120(%rbp), %rcx
	movq	%r14, %r8
	movq	%r11, -112(%rbp)
	movq	168(%r11), %rax
	movq	%rdx, %xmm0
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movzbl	(%rax,%rbx), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	-112(%rbp), %r11
	movq	%rax, (%r12)
	movq	-104(%rbp), %rax
	movq	8(%rax), %rdi
.L458:
	subq	$1, %rbx
	testl	%ebx, %ebx
	jns	.L459
	movq	%r14, %rax
	movq	-136(%rbp), %rbx
	movq	%r13, %r14
	movq	%r11, %r12
	movq	%r15, %r13
	movq	%rax, %r15
.L457:
	leaq	24(%r14), %rdx
	leaq	24(%r13), %rsi
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder21NewInstanceCacheMergeEPNS1_22WasmInstanceCacheNodesES4_PNS1_4NodeE@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L491:
	movq	112(%rsi), %rax
	movq	8(%rdi), %r15
	movq	(%rax), %rbx
	testl	%ebx, %ebx
	jne	.L435
	xorl	%esi, %esi
	xorl	%edx, %edx
.L436:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder6ReturnENS0_6VectorIPNS1_4NodeEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L433
	movq	-104(%rbp), %rax
	cmpl	$-1, 16(%rax)
	je	.L433
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L435:
	movq	104(%r15), %rax
	movl	%ebx, %edx
	movq	208(%rsi), %r14
	cmpq	%rax, %rdx
	ja	.L496
.L437:
	movq	96(%r15), %rsi
	testq	%rdx, %rdx
	je	.L444
	leaq	0(,%rdx,4), %rax
	movq	%rdx, %rdi
	subq	%rax, %rdi
	leaq	0(,%rdi,8), %rcx
	leaq	(%rsi,%rdx,8), %rdi
	leaq	16(%r14,%rcx), %rax
	cmpq	%rdi, %rax
	setnb	%r8b
	cmpq	%rsi, %r14
	setbe	%dil
	orb	%dil, %r8b
	je	.L441
	leaq	-1(%rdx), %rdi
	cmpq	$22, %rdi
	jbe	.L441
	movq	%rdx, %rdi
	movq	%rsi, %rcx
	shrq	%rdi
	salq	$4, %rdi
	addq	%rsi, %rdi
	.p2align 4,,10
	.p2align 3
.L443:
	movq	(%rax), %xmm0
	addq	$16, %rcx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rcx, %rdi
	jne	.L443
	movq	%rbx, %rax
	andl	$4294967294, %eax
	cmpq	%rax, %rdx
	je	.L444
	movq	%rax, %rcx
	subq	%rdx, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	16(%r14,%rcx,8), %rcx
	movq	%rcx, (%rsi,%rax,8)
.L444:
	movq	-104(%rbp), %rax
	movq	8(%rax), %r15
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L496:
	movq	(%r15), %r8
	leaq	5(%rdx,%rax), %r13
	leaq	0(,%r13,8), %rsi
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L497
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L439:
	movq	%rax, 96(%r15)
	movq	%r13, 104(%r15)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	(%r14,%rcx), %rax
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L447:
	movq	16(%rax), %rdi
	addq	$24, %rax
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%rax, %r14
	jne	.L447
	jmp	.L444
.L497:
	movq	%r8, %rdi
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rdx
	jmp	.L439
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19680:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj, .-_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"expected %u elements on the stack for fallthru to @%d, found %u"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv:
.LFB23620:
	.cfi_startproc
	movq	208(%rdi), %r8
	movq	240(%rdi), %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	200(%rdi), %r9
	movl	-72(%rsi), %ecx
	sarq	$3, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	imull	$-1431655765, %r9d, %r9d
	subl	-132(%rsi), %r9d
	cmpb	$0, -120(%rsi)
	jne	.L499
	cmpl	%r9d, %ecx
	jne	.L551
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.L552
.L498:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movl	%ecx, %edx
	leaq	(%rdx,%rdx,2), %r9
	salq	$3, %r9
	cmpl	$1, %ecx
	je	.L502
	movq	-64(%rsi), %r11
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	subq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L506:
	movzbl	8(%r11,%rax), %edx
	movzbl	8(%r8,%rax), %esi
	cmpb	%dl, %sil
	je	.L503
	leal	-7(%rsi), %r9d
	cmpb	$2, %r9b
	ja	.L539
	cmpb	$6, %dl
	jne	.L539
.L503:
	addl	$1, %r10d
	addq	$24, %rax
	cmpl	%ecx, %r10d
	jne	.L506
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	movq	-128(%rsi), %r8
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rdx
	subl	8(%rdi), %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	subq	%r9, %r8
	movzbl	-56(%rsi), %edx
	movzbl	8(%r8), %esi
	cmpb	%dl, %sil
	je	.L498
	leal	-7(%rsi), %eax
	cmpb	$2, %al
	setbe	%al
	cmpb	$6, %dl
	sete	%cl
	andb	%cl, %al
	jne	.L498
	leal	-7(%rdx), %eax
	testb	$-3, %al
	sete	%al
	cmpb	$8, %sil
	sete	%cl
	andb	%cl, %al
	jne	.L498
	xorl	%r10d, %r10d
.L505:
	cmpb	$10, %sil
	ja	.L507
	leaq	.L509(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv,"a",@progbits
	.align 4
	.align 4
.L509:
	.long	.L519-.L509
	.long	.L537-.L509
	.long	.L517-.L509
	.long	.L516-.L509
	.long	.L515-.L509
	.long	.L514-.L509
	.long	.L513-.L509
	.long	.L512-.L509
	.long	.L511-.L509
	.long	.L510-.L509
	.long	.L508-.L509
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv
	.p2align 4,,10
	.p2align 3
.L539:
	leal	-7(%rdx), %r9d
	andl	$253, %r9d
	jne	.L505
	cmpb	$8, %sil
	je	.L503
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L499:
	cmpl	%r9d, %ecx
	jl	.L551
	subq	$72, %rsi
	xorl	%edx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb
.L510:
	.cfi_restore_state
	leaq	.LC14(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L518:
	cmpb	$10, %dl
	ja	.L520
	leaq	.L522(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv
	.align 4
	.align 4
.L522:
	.long	.L532-.L522
	.long	.L538-.L522
	.long	.L530-.L522
	.long	.L529-.L522
	.long	.L528-.L522
	.long	.L527-.L522
	.long	.L526-.L522
	.long	.L525-.L522
	.long	.L524-.L522
	.long	.L523-.L522
	.long	.L521-.L522
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	.LC17(%rip), %r8
.L531:
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	movl	%r10d, %ecx
	leaq	.LC22(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	leaq	.LC8(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L532:
	leaq	.LC16(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L521:
	leaq	.LC20(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L523:
	leaq	.LC14(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L524:
	leaq	.LC13(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L525:
	leaq	.LC12(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L526:
	leaq	.LC11(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L527:
	leaq	.LC15(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	.LC10(%rip), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L529:
	leaq	.LC9(%rip), %r8
	jmp	.L531
.L512:
	leaq	.LC12(%rip), %r9
	jmp	.L518
.L513:
	leaq	.LC11(%rip), %r9
	jmp	.L518
.L511:
	leaq	.LC13(%rip), %r9
	jmp	.L518
.L514:
	leaq	.LC15(%rip), %r9
	jmp	.L518
.L515:
	leaq	.LC10(%rip), %r9
	jmp	.L518
.L516:
	leaq	.LC9(%rip), %r9
	jmp	.L518
.L519:
	leaq	.LC16(%rip), %r9
	jmp	.L518
.L508:
	leaq	.LC20(%rip), %r9
	jmp	.L518
.L537:
	leaq	.LC8(%rip), %r9
	jmp	.L518
.L517:
	leaq	.LC17(%rip), %r9
	jmp	.L518
.L507:
	leaq	.LC7(%rip), %r9
	jmp	.L518
.L520:
	leaq	.LC7(%rip), %r8
	jmp	.L531
	.cfi_endproc
.LFE23620:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_:
.LFB23912:
	.cfi_startproc
	movabsq	$-6148914691236517205, %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	240(%rdi), %rsi
	movq	208(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rax
	subq	200(%r12), %rax
	movl	-132(%rsi), %r8d
	sarq	$3, %rax
	imulq	%r9, %rax
	cmpq	%rax, %r8
	jb	.L554
	cmpb	$2, -120(%rsi)
	jne	.L611
.L555:
	xorl	%r14d, %r14d
.L559:
	leaq	-57(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%bl, -57(%rbp)
	leaq	192(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	$0, 56(%r12)
	movq	208(%r12), %rbx
	je	.L612
.L553:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L613
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movzbl	-16(%rdi), %eax
	movq	-24(%rdi), %r15
	subq	$24, %rdi
	movq	16(%rdi), %r14
	movq	%rdi, 208(%r12)
	cmpb	$10, %al
	je	.L559
	cmpb	%cl, %al
	je	.L559
	movzbl	%cl, %edx
	cmpb	$9, %al
	ja	.L560
	leaq	.L562(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_,"a",@progbits
	.align 4
	.align 4
.L562:
	.long	.L571-.L562
	.long	.L595-.L562
	.long	.L569-.L562
	.long	.L568-.L562
	.long	.L567-.L562
	.long	.L566-.L562
	.long	.L565-.L562
	.long	.L564-.L562
	.long	.L563-.L562
	.long	.L561-.L562
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	.p2align 4,,10
	.p2align 3
.L612:
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L553
	movq	144(%r12), %rdi
	movq	16(%r12), %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	subq	8(%r12), %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder4UnopENS0_4wasm10WasmOpcodeEPNS1_4NodeEi@PLT
	testq	%rax, %rax
	je	.L592
	cmpl	$-1, 152(%r12)
	je	.L592
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L592:
	movq	%rax, -8(%rbx)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L611:
	movq	16(%r12), %r14
	leaq	.LC0(%rip), %r15
	cmpq	24(%r12), %r14
	jnb	.L556
	movzbl	(%r14), %edi
	movl	%edi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-72(%rbp), %edi
	testb	%al, %al
	je	.L609
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%r12)
	ja	.L558
.L610:
	movq	16(%r12), %r14
.L556:
	movq	%r15, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L558:
	movl	%edi, %eax
	movzbl	1(%r14), %edi
	sall	$8, %eax
	orl	%eax, %edi
.L609:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L595:
	leaq	.LC8(%rip), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L570:
	movq	24(%r12), %r8
	leaq	.LC0(%rip), %rax
	movq	%rax, -72(%rbp)
	cmpq	%r8, %r15
	jnb	.L572
	movzbl	(%r15), %edi
	movl	%ecx, -104(%rbp)
	movb	%dl, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movzbl	-96(%rbp), %edx
	testb	%al, %al
	movl	-104(%rbp), %ecx
	je	.L614
	movq	24(%r12), %r8
	leaq	1(%r15), %rax
	cmpq	%rax, %r8
	jbe	.L572
	movzbl	1(%r15), %eax
	sall	$8, %edi
	movl	%ecx, -96(%rbp)
	movb	%dl, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %r8
	movl	-96(%rbp), %ecx
	movq	%rax, -72(%rbp)
	movzbl	-88(%rbp), %edx
	.p2align 4,,10
	.p2align 3
.L572:
	cmpb	$10, %cl
	ja	.L574
	leaq	.L576(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	.align 4
	.align 4
.L576:
	.long	.L586-.L576
	.long	.L598-.L576
	.long	.L584-.L576
	.long	.L583-.L576
	.long	.L582-.L576
	.long	.L581-.L576
	.long	.L580-.L576
	.long	.L579-.L576
	.long	.L578-.L576
	.long	.L577-.L576
	.long	.L575-.L576
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	.LC17(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L561:
	leaq	.LC14(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	.LC16(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	.LC10(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	.LC15(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	.LC9(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L565:
	leaq	.LC11(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L564:
	leaq	.LC12(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L563:
	leaq	.LC13(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L584:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L585:
	movq	16(%r12), %rdx
	cmpq	%r8, %rdx
	jnb	.L590
	movzbl	(%rdx), %edi
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movq	-96(%rbp), %rdx
	testb	%al, %al
	movq	-104(%rbp), %r9
	je	.L615
	leaq	1(%rdx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L590
	movzbl	1(%rdx), %eax
	sall	$8, %edi
	movq	%r9, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L588:
	pushq	-80(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-72(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L598:
	leaq	.LC8(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L586:
	leaq	.LC16(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L577:
	leaq	.LC14(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	.LC13(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L579:
	leaq	.LC12(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L580:
	leaq	.LC11(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	.LC9(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L581:
	leaq	.LC15(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L582:
	leaq	.LC10(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L575:
	leaq	.LC20(%rip), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L590:
	leaq	.LC0(%rip), %rcx
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L615:
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L614:
	movl	%ecx, -96(%rbp)
	movb	%dl, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %r8
	movzbl	-88(%rbp), %edx
	movq	%rax, -72(%rbp)
	movl	-96(%rbp), %ecx
	jmp	.L572
.L574:
	leaq	.LC7(%rip), %r9
	jmp	.L585
.L560:
	leaq	.LC7(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L570
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23912:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"lane"
.LC25:
	.string	"invalid lane index"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE:
.LFB24023:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	leaq	2(%r8), %rsi
	cmpq	%rax, %rsi
	ja	.L617
	cmpl	%esi, %eax
	je	.L617
	leal	-64773(%r13), %eax
	movzbl	2(%r8), %ebx
	cmpl	$18, %eax
	ja	.L618
	leaq	.L620(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"a",@progbits
	.align 4
	.align 4
.L620:
	.long	.L623-.L620
	.long	.L618-.L620
	.long	.L623-.L620
	.long	.L618-.L620
	.long	.L622-.L620
	.long	.L618-.L620
	.long	.L622-.L620
	.long	.L618-.L620
	.long	.L661-.L620
	.long	.L661-.L620
	.long	.L618-.L620
	.long	.L619-.L620
	.long	.L619-.L620
	.long	.L618-.L620
	.long	.L661-.L620
	.long	.L661-.L620
	.long	.L618-.L620
	.long	.L619-.L620
	.long	.L619-.L620
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.p2align 4,,10
	.p2align 3
.L665:
	xorl	%ebx, %ebx
.L619:
	movl	$2, %eax
.L621:
	cmpb	%bl, %al
	jbe	.L683
.L625:
	movq	208(%r12), %rdx
	movq	240(%r12), %rcx
	movabsq	$-6148914691236517205, %rdi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L684
	cmpb	$2, -120(%rcx)
	jne	.L685
.L630:
	xorl	%r14d, %r14d
.L635:
	leaq	-57(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%r15b, -57(%rbp)
	leaq	192(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	$0, 56(%r12)
	movq	208(%r12), %r15
	jne	.L629
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	je	.L686
.L629:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L687
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	movl	$4, %eax
	cmpb	%bl, %al
	ja	.L625
.L683:
	leaq	2(%r8), %rsi
	leaq	.LC25(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L663:
	xorl	%ebx, %ebx
.L622:
	movl	$8, %eax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L662:
	xorl	%ebx, %ebx
.L623:
	movl	$16, %eax
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L684:
	movzbl	-16(%rdx), %eax
	movq	-24(%rdx), %rsi
	subq	$24, %rdx
	movq	16(%rdx), %r14
	movq	%rdx, 208(%r12)
	cmpb	$5, %al
	je	.L635
	cmpb	$10, %al
	je	.L635
	cmpb	$9, %al
	ja	.L636
	leaq	.L638(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.align 4
	.align 4
.L638:
	.long	.L647-.L638
	.long	.L666-.L638
	.long	.L645-.L638
	.long	.L644-.L638
	.long	.L643-.L638
	.long	.L642-.L638
	.long	.L641-.L638
	.long	.L640-.L638
	.long	.L639-.L638
	.long	.L637-.L638
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
.L645:
	leaq	.LC17(%rip), %rdx
.L646:
	movq	24(%r12), %rax
	leaq	.LC0(%rip), %rcx
	movq	%rcx, -72(%rbp)
	cmpq	%rax, %rsi
	jb	.L688
.L648:
	cmpq	%rax, %r8
	jnb	.L653
	movzbl	(%r8), %edi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%r8, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movq	-88(%rbp), %r8
	testb	%al, %al
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdx
	je	.L689
	leaq	1(%r8), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L653
	movzbl	1(%r8), %eax
	sall	$8, %edi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L651
.L666:
	leaq	.LC8(%rip), %rdx
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L686:
	movq	144(%r12), %rdx
	cmpq	$0, 104(%rdx)
	je	.L655
	movq	96(%rdx), %rcx
.L656:
	movq	%r14, (%rcx)
	movq	144(%r12), %rdi
	movzbl	%bl, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder10SimdLaneOpENS0_4wasm10WasmOpcodeEhPKPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L659
	cmpl	$-1, 152(%r12)
	je	.L659
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L659:
	movq	%rax, -8(%r15)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L685:
	cmpq	%r8, 24(%r12)
	ja	.L631
.L634:
	leaq	.LC0(%rip), %rcx
.L632:
	leaq	.LC18(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L688:
	movzbl	(%rsi), %edi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movq	-88(%rbp), %rsi
	testb	%al, %al
	movq	-96(%rbp), %rdx
	je	.L690
	movq	24(%r12), %rax
	leaq	1(%rsi), %rcx
	cmpq	%rcx, %rax
	jbe	.L668
	movzbl	1(%rsi), %eax
	movq	%rdx, -88(%rbp)
	sall	$8, %edi
	movq	%rsi, -80(%rbp)
	orl	%eax, %edi
.L682:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rdx
	movq	24(%r12), %rax
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L617:
	leaq	.LC24(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	leal	-64773(%r13), %eax
	movq	16(%r12), %r8
	cmpl	$18, %eax
	ja	.L618
	leaq	.L624(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.align 4
	.align 4
.L624:
	.long	.L662-.L624
	.long	.L618-.L624
	.long	.L662-.L624
	.long	.L618-.L624
	.long	.L663-.L624
	.long	.L618-.L624
	.long	.L663-.L624
	.long	.L618-.L624
	.long	.L664-.L624
	.long	.L664-.L624
	.long	.L618-.L624
	.long	.L665-.L624
	.long	.L665-.L624
	.long	.L618-.L624
	.long	.L664-.L624
	.long	.L664-.L624
	.long	.L618-.L624
	.long	.L665-.L624
	.long	.L665-.L624
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.p2align 4,,10
	.p2align 3
.L653:
	leaq	.LC0(%rip), %rcx
.L651:
	pushq	%rdx
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rdx
	xorl	%r8d, %r8d
	pushq	-72(%rbp)
	leaq	.LC15(%rip), %r9
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L635
.L639:
	leaq	.LC13(%rip), %rdx
	jmp	.L646
.L637:
	leaq	.LC14(%rip), %rdx
	jmp	.L646
.L647:
	leaq	.LC16(%rip), %rdx
	jmp	.L646
.L644:
	leaq	.LC9(%rip), %rdx
	jmp	.L646
.L643:
	leaq	.LC10(%rip), %rdx
	jmp	.L646
.L642:
	leaq	.LC15(%rip), %rdx
	jmp	.L646
.L641:
	leaq	.LC11(%rip), %rdx
	jmp	.L646
.L640:
	leaq	.LC12(%rip), %rdx
	jmp	.L646
.L618:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L631:
	movzbl	(%r8), %r14d
	movq	%r8, -72(%rbp)
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-72(%rbp), %r8
	testb	%al, %al
	je	.L691
	leaq	1(%r8), %rax
	cmpq	%rax, 24(%r12)
	ja	.L680
	movq	16(%r12), %r8
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L664:
	xorl	%ebx, %ebx
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L690:
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L655:
	movq	(%rdx), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$47, %rax
	jbe	.L692
	leaq	48(%rcx), %rax
	movq	%rax, 16(%rdi)
.L658:
	movq	%rcx, 96(%rdx)
	movq	$6, 104(%rdx)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L668:
	movq	16(%r12), %r8
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L691:
	movl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r8
	movq	%rax, %rcx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L680:
	movzbl	1(%r8), %edi
	sall	$8, %r14d
	orl	%r14d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r8
	movq	%rax, %rcx
	jmp	.L632
.L636:
	leaq	.LC7(%rip), %rdx
	jmp	.L646
.L692:
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L658
.L687:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24023:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_:
.LFB23914:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$88, %rsp
	movl	%esi, -76(%rbp)
	movq	200(%r12), %r9
	movabsq	$-6148914691236517205, %rsi
	movb	%cl, -72(%rbp)
	movq	208(%r12), %rcx
	movq	240(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rdi), %esi
	cmpq	%rax, %rsi
	jb	.L694
	cmpb	$2, -120(%rdi)
	jne	.L801
.L695:
	xorl	%r14d, %r14d
.L699:
	cmpq	%rsi, %rax
	ja	.L733
	cmpb	$2, -120(%rdi)
	jne	.L802
.L734:
	xorl	%r15d, %r15d
.L738:
	leaq	-57(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%bl, -57(%rbp)
	leaq	192(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	$0, 56(%r12)
	movq	208(%r12), %rbx
	jne	.L693
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	je	.L803
.L693:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L804
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	movzbl	-16(%rcx), %eax
	movq	-24(%rcx), %r15
	subq	$24, %rcx
	movq	16(%rcx), %r14
	movq	%rcx, 208(%r12)
	cmpb	$10, %al
	je	.L783
	cmpb	%r8b, %al
	je	.L783
	movzbl	%r8b, %edx
	cmpb	$9, %al
	ja	.L702
	leaq	.L704(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_,"a",@progbits
	.align 4
	.align 4
.L704:
	.long	.L713-.L704
	.long	.L774-.L704
	.long	.L711-.L704
	.long	.L710-.L704
	.long	.L709-.L704
	.long	.L708-.L704
	.long	.L707-.L704
	.long	.L706-.L704
	.long	.L705-.L704
	.long	.L703-.L704
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.p2align 4,,10
	.p2align 3
.L733:
	movzbl	-16(%rcx), %eax
	movq	-24(%rcx), %rsi
	subq	$24, %rcx
	movq	16(%rcx), %r15
	movq	%rcx, 208(%r12)
	cmpb	$10, %al
	je	.L738
	cmpb	%r13b, %al
	je	.L738
	cmpb	$9, %al
	ja	.L739
	leaq	.L741(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.align 4
	.align 4
.L741:
	.long	.L750-.L741
	.long	.L779-.L741
	.long	.L748-.L741
	.long	.L747-.L741
	.long	.L746-.L741
	.long	.L745-.L741
	.long	.L744-.L741
	.long	.L743-.L741
	.long	.L742-.L741
	.long	.L740-.L741
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.p2align 4,,10
	.p2align 3
.L801:
	movq	16(%r12), %r14
	leaq	.LC0(%rip), %r15
	cmpq	24(%r12), %r14
	jnb	.L696
	movzbl	(%r14), %edi
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	testb	%al, %al
	je	.L797
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%r12)
	ja	.L698
.L798:
	movq	16(%r12), %r14
.L696:
	movq	%r15, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	208(%r12), %rcx
	movq	240(%r12), %rdi
	movabsq	$-6148914691236517205, %rdx
	movq	%rcx, %rax
	subq	200(%r12), %rax
	movl	-132(%rdi), %esi
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L783:
	movq	%rcx, %rax
	movl	-132(%rdi), %esi
	movabsq	$-6148914691236517205, %rdx
	subq	%r9, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L802:
	movq	16(%r12), %r13
	leaq	.LC0(%rip), %r15
	cmpq	24(%r12), %r13
	jnb	.L735
	movzbl	0(%r13), %edi
	movl	%edi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-72(%rbp), %edi
	testb	%al, %al
	je	.L799
	leaq	1(%r13), %rax
	cmpq	%rax, 24(%r12)
	ja	.L737
.L800:
	movq	16(%r12), %r13
.L735:
	movq	%r15, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L803:
	movl	-76(%rbp), %esi
	movq	16(%r12), %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	144(%r12), %rdi
	subl	8(%r12), %r8d
	call	_ZN2v88internal8compiler16WasmGraphBuilder5BinopENS0_4wasm10WasmOpcodeEPNS1_4NodeES6_i@PLT
	testq	%rax, %rax
	je	.L771
	cmpl	$-1, 152(%r12)
	je	.L771
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L771:
	movq	%rax, -8(%rbx)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L737:
	movzbl	1(%r13), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L799:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L698:
	movzbl	1(%r14), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L797:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L779:
	leaq	.LC8(%rip), %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L749:
	movq	24(%r12), %rdx
	leaq	.LC0(%rip), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rdx, %rsi
	jnb	.L751
	movzbl	(%rsi), %edi
	movq	%rsi, -112(%rbp)
	movl	%edi, -104(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-104(%rbp), %edi
	movq	-112(%rbp), %rsi
	testb	%al, %al
	je	.L805
	movq	24(%r12), %rdx
	leaq	1(%rsi), %rax
	cmpq	%rax, %rdx
	jbe	.L751
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	movq	%rsi, -104(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdx
	movq	-104(%rbp), %rsi
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L751:
	cmpb	$10, %r13b
	ja	.L753
	movzbl	-72(%rbp), %r13d
	leaq	.L755(%rip), %rcx
	movslq	(%rcx,%r13,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.align 4
	.align 4
.L755:
	.long	.L765-.L755
	.long	.L782-.L755
	.long	.L763-.L755
	.long	.L762-.L755
	.long	.L761-.L755
	.long	.L760-.L755
	.long	.L759-.L755
	.long	.L758-.L755
	.long	.L757-.L755
	.long	.L756-.L755
	.long	.L754-.L755
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.p2align 4,,10
	.p2align 3
.L748:
	leaq	.LC17(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L774:
	leaq	.LC8(%rip), %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L712:
	movq	24(%r12), %rcx
	leaq	.LC0(%rip), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rcx, %r15
	jnb	.L714
	movzbl	(%r15), %edi
	movl	%r8d, -120(%rbp)
	movb	%dl, -112(%rbp)
	movl	%edi, -104(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-104(%rbp), %edi
	movzbl	-112(%rbp), %edx
	testb	%al, %al
	movl	-120(%rbp), %r8d
	je	.L806
	movq	24(%r12), %rcx
	leaq	1(%r15), %rax
	cmpq	%rax, %rcx
	jbe	.L714
	movzbl	1(%r15), %eax
	sall	$8, %edi
	movl	%r8d, -112(%rbp)
	movb	%dl, -104(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rcx
	movl	-112(%rbp), %r8d
	movq	%rax, -88(%rbp)
	movzbl	-104(%rbp), %edx
	.p2align 4,,10
	.p2align 3
.L714:
	cmpb	$10, %r8b
	ja	.L716
	leaq	.L718(%rip), %rdi
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.align 4
	.align 4
.L718:
	.long	.L728-.L718
	.long	.L777-.L718
	.long	.L726-.L718
	.long	.L725-.L718
	.long	.L724-.L718
	.long	.L723-.L718
	.long	.L722-.L718
	.long	.L721-.L718
	.long	.L720-.L718
	.long	.L719-.L718
	.long	.L717-.L718
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.p2align 4,,10
	.p2align 3
.L711:
	leaq	.LC17(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L745:
	leaq	.LC15(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	.LC11(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L743:
	leaq	.LC12(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L742:
	leaq	.LC13(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	.LC9(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L709:
	leaq	.LC10(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L708:
	leaq	.LC15(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	.LC11(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	.LC14(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	.LC16(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L740:
	leaq	.LC14(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L750:
	leaq	.LC16(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L706:
	leaq	.LC12(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L705:
	leaq	.LC13(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L747:
	leaq	.LC9(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	.LC10(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L726:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L727:
	movq	16(%r12), %rdx
	cmpq	%rcx, %rdx
	jnb	.L732
	movzbl	(%rdx), %edi
	movq	%r9, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%edi, -104(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-104(%rbp), %edi
	movq	-112(%rbp), %rdx
	testb	%al, %al
	movq	-120(%rbp), %r9
	je	.L807
	leaq	1(%rdx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L732
	movzbl	1(%rdx), %eax
	sall	$8, %edi
	movq	%r9, -104(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L730:
	pushq	-96(%rbp)
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	-88(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	208(%r12), %rcx
	movq	240(%r12), %rdi
	movabsq	$-6148914691236517205, %rdx
	popq	%r8
	popq	%r9
	movq	%rcx, %rax
	subq	200(%r12), %rax
	movl	-132(%rdi), %esi
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L777:
	leaq	.LC8(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L763:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L764:
	movq	16(%r12), %r13
	cmpq	%rdx, %r13
	jnb	.L769
	movzbl	0(%r13), %edi
	movq	%r9, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movl	%edi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-72(%rbp), %edi
	movq	-104(%rbp), %rsi
	testb	%al, %al
	movq	-112(%rbp), %r9
	je	.L808
	leaq	1(%r13), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L769
	movzbl	1(%r13), %eax
	sall	$8, %edi
	movq	%r9, -104(%rbp)
	movq	%rsi, -72(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-104(%rbp), %r9
	movq	-72(%rbp), %rsi
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L767:
	pushq	-96(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-88(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L782:
	leaq	.LC8(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L765:
	leaq	.LC16(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L719:
	leaq	.LC14(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L720:
	leaq	.LC13(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	.LC12(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	.LC11(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L725:
	leaq	.LC9(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L723:
	leaq	.LC15(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	.LC10(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L717:
	leaq	.LC20(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	.LC14(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	.LC13(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L758:
	leaq	.LC12(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L759:
	leaq	.LC11(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L728:
	leaq	.LC16(%rip), %r9
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	.LC9(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L760:
	leaq	.LC15(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L761:
	leaq	.LC10(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L754:
	leaq	.LC20(%rip), %r9
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L769:
	leaq	.LC0(%rip), %rcx
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L732:
	leaq	.LC0(%rip), %rcx
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L807:
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r9, -104(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-72(%rbp), %rsi
	movq	-104(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L806:
	movl	%r8d, -112(%rbp)
	movb	%dl, -104(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rcx
	movzbl	-104(%rbp), %edx
	movq	%rax, -88(%rbp)
	movl	-112(%rbp), %r8d
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L805:
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdx
	movq	-104(%rbp), %rsi
	movq	%rax, -88(%rbp)
	jmp	.L751
.L753:
	leaq	.LC7(%rip), %r9
	jmp	.L764
.L716:
	leaq	.LC7(%rip), %r9
	jmp	.L727
.L702:
	leaq	.LC7(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L712
.L739:
	leaq	.LC7(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L749
.L804:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23914:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE:
.LFB23715:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%esi, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	cmpq	$1, %rax
	je	.L810
	cmpq	$2, %rax
	jne	.L998
	movq	16(%rdx), %rdx
	movq	0(%r13), %rax
	movabsq	$-6148914691236517205, %rsi
	movq	240(%rdi), %rcx
	movzbl	1(%rdx,%rax), %r15d
	movq	208(%rdi), %rdx
	movq	%rdx, %rax
	subq	200(%rdi), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L999
	movq	-24(%rdx), %rbx
	movzbl	-16(%rdx), %eax
	subq	$24, %rdx
	movq	16(%rdx), %r14
	movq	%rdx, 208(%rdi)
.L859:
	cmpb	%r15b, %al
	je	.L860
	leal	-7(%rax), %edx
	cmpb	$2, %dl
	ja	.L951
	cmpb	$6, %r15b
	jne	.L951
.L860:
	movq	0(%r13), %rax
	movq	16(%r13), %rdx
	movabsq	$-6148914691236517205, %rdi
	movq	208(%r12), %rcx
	movq	240(%r12), %rsi
	movzbl	(%rdx,%rax), %edx
	movq	%rcx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	ja	.L892
	cmpb	$2, -120(%rsi)
	movq	16(%r12), %r15
	jne	.L1000
.L893:
	xorl	%r15d, %r15d
.L897:
	xorl	%ebx, %ebx
	cmpq	$0, 0(%r13)
	jne	.L1001
.L929:
	cmpq	$0, 56(%r12)
	jne	.L809
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	je	.L1002
	.p2align 4,,10
	.p2align 3
.L809:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1003
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	cmpb	$2, -120(%rcx)
	movq	16(%rdi), %rbx
	jne	.L1004
.L855:
	xorl	%r14d, %r14d
	movl	$10, %eax
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L810:
	movq	16(%rdx), %rdx
	movq	0(%r13), %rax
	movabsq	$-6148914691236517205, %r8
	movq	240(%rdi), %rsi
	movzbl	(%rdx,%rax), %ecx
	movq	208(%rdi), %rdx
	movl	-132(%rsi), %edi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%r8, %rax
	cmpq	%rax, %rdi
	jb	.L813
	cmpb	$2, -120(%rsi)
	movq	16(%r12), %r14
	jne	.L1005
.L814:
	xorl	%r15d, %r15d
.L818:
	xorl	%ebx, %ebx
	cmpq	$0, 0(%r13)
	jne	.L1006
.L850:
	cmpq	$0, 56(%r12)
	jne	.L809
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L809
	movq	144(%r12), %rdi
	movl	-68(%rbp), %esi
	movq	%r15, %rdx
	movq	16(%r12), %rcx
	subq	8(%r12), %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder4UnopENS0_4wasm10WasmOpcodeEPNS1_4NodeEi@PLT
	testq	%rax, %rax
	je	.L997
	cmpl	$-1, 152(%r12)
	je	.L997
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L997:
	movq	%rax, 16(%rbx)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L813:
	movzbl	-16(%rdx), %eax
	movq	-24(%rdx), %r14
	subq	$24, %rdx
	movq	16(%rdx), %r15
	movq	%rdx, 208(%r12)
	cmpb	%cl, %al
	je	.L818
	leal	-7(%rax), %edx
	cmpb	$2, %dl
	ja	.L950
	cmpb	$6, %cl
	je	.L818
.L950:
	leal	-7(%rcx), %edx
	andl	$253, %edx
	sete	%dl
	cmpb	$8, %al
	sete	%dil
	andl	%edi, %edx
	xorl	$1, %edx
	cmpb	$10, %al
	setne	%dil
	testb	%dil, %dl
	je	.L818
	cmpb	$10, %cl
	je	.L818
	cmpb	$9, %al
	ja	.L820
	leaq	.L822(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE,"a",@progbits
	.align 4
	.align 4
.L822:
	.long	.L831-.L822
	.long	.L934-.L822
	.long	.L829-.L822
	.long	.L828-.L822
	.long	.L827-.L822
	.long	.L826-.L822
	.long	.L825-.L822
	.long	.L824-.L822
	.long	.L823-.L822
	.long	.L821-.L822
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.p2align 4,,10
	.p2align 3
.L892:
	movzbl	-16(%rcx), %eax
	movq	-24(%rcx), %rbx
	subq	$24, %rcx
	movq	16(%rcx), %r15
	movq	%rcx, 208(%r12)
	cmpb	%al, %dl
	je	.L897
	leal	-7(%rax), %ecx
	cmpb	$2, %cl
	ja	.L952
	cmpb	$6, %dl
	je	.L897
.L952:
	leal	-7(%rdx), %ecx
	andl	$253, %ecx
	sete	%cl
	cmpb	$8, %al
	sete	%dil
	andl	%edi, %ecx
	xorl	$1, %ecx
	cmpb	$10, %al
	setne	%dil
	testb	%dil, %cl
	je	.L897
	cmpb	$10, %dl
	je	.L897
	cmpb	$9, %al
	ja	.L899
	leaq	.L901(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.align 4
	.align 4
.L901:
	.long	.L910-.L901
	.long	.L945-.L901
	.long	.L908-.L901
	.long	.L907-.L901
	.long	.L906-.L901
	.long	.L905-.L901
	.long	.L904-.L901
	.long	.L903-.L901
	.long	.L902-.L901
	.long	.L900-.L901
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.p2align 4,,10
	.p2align 3
.L1005:
	leaq	.LC0(%rip), %r15
	cmpq	%r14, 24(%r12)
	ja	.L1007
.L815:
	movq	%r15, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	-68(%rbp), %esi
	movq	16(%r12), %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	144(%r12), %rdi
	subl	8(%r12), %r8d
	call	_ZN2v88internal8compiler16WasmGraphBuilder5BinopENS0_4wasm10WasmOpcodeEPNS1_4NodeES6_i@PLT
	testq	%rax, %rax
	je	.L931
	cmpl	$-1, 152(%r12)
	je	.L931
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L931:
	testq	%rbx, %rbx
	jne	.L997
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L1004:
	leaq	.LC0(%rip), %r14
	cmpq	24(%rdi), %rbx
	jb	.L1008
.L856:
	movq	%rbx, %rsi
	movq	%r14, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %rbx
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1000:
	leaq	.LC0(%rip), %rbx
	cmpq	%r15, 24(%r12)
	ja	.L1009
.L894:
	movq	%rbx, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	16(%r13), %rax
	leaq	-57(%rbp), %rdx
	leaq	16(%r12), %rsi
	leaq	192(%r12), %rdi
	movzbl	(%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	208(%r12), %rax
	leaq	-24(%rax), %rbx
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	16(%r13), %rax
	leaq	-57(%rbp), %rdx
	leaq	16(%r12), %rsi
	leaq	192(%r12), %rdi
	movzbl	(%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	208(%r12), %rax
	leaq	-24(%rax), %rbx
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L1007:
	movzbl	(%r14), %ebx
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L1010
	leaq	1(%r14), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L991
	movzbl	1(%r14), %edi
	sall	$8, %ebx
	orl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
.L991:
	movq	16(%r12), %r14
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L1009:
	movzbl	(%r15), %edi
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	testb	%al, %al
	je	.L995
	leaq	1(%r15), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L996
	movzbl	1(%r15), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L995:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rbx
.L996:
	movq	16(%r12), %r15
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L1008:
	movzbl	(%rbx), %edi
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	testb	%al, %al
	je	.L992
	leaq	1(%rbx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L993
	movl	%edi, %eax
	movzbl	1(%rbx), %edi
	sall	$8, %eax
	orl	%eax, %edi
.L992:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r14
.L993:
	movq	16(%r12), %rbx
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L951:
	leal	-7(%r15), %edx
	andl	$253, %edx
	sete	%dl
	cmpb	$8, %al
	sete	%cl
	andl	%ecx, %edx
	xorl	$1, %edx
	cmpb	$10, %al
	setne	%cl
	testb	%cl, %dl
	je	.L860
	cmpb	$10, %r15b
	je	.L860
	cmpb	$9, %al
	ja	.L862
	leaq	.L864(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.align 4
	.align 4
.L864:
	.long	.L873-.L864
	.long	.L940-.L864
	.long	.L871-.L864
	.long	.L870-.L864
	.long	.L869-.L864
	.long	.L868-.L864
	.long	.L867-.L864
	.long	.L866-.L864
	.long	.L865-.L864
	.long	.L863-.L864
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
.L871:
	leaq	.LC17(%rip), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L872:
	movq	24(%r12), %rdx
	leaq	.LC0(%rip), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rbx, %rdx
	jbe	.L874
	movzbl	(%rbx), %edi
	movl	%edi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-96(%rbp), %edi
	testb	%al, %al
	je	.L994
	movq	24(%r12), %rdx
	leaq	1(%rbx), %rax
	cmpq	%rax, %rdx
	jbe	.L874
	movzbl	1(%rbx), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L994:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdx
	movq	%rax, -80(%rbp)
.L874:
	cmpb	$9, %r15b
	ja	.L876
	leaq	.L878(%rip), %rcx
	movzbl	%r15b, %r8d
	movslq	(%rcx,%r8,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.align 4
	.align 4
.L878:
	.long	.L887-.L878
	.long	.L943-.L878
	.long	.L885-.L878
	.long	.L884-.L878
	.long	.L883-.L878
	.long	.L882-.L878
	.long	.L881-.L878
	.long	.L880-.L878
	.long	.L879-.L878
	.long	.L877-.L878
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
.L885:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L886:
	movq	16(%r12), %rcx
	cmpq	%rcx, %rdx
	jbe	.L891
	movzbl	(%rcx), %edi
	movq	%r9, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%edi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-96(%rbp), %edi
	movq	-104(%rbp), %rcx
	testb	%al, %al
	movq	-112(%rbp), %r9
	je	.L1011
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L891
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -96(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L889:
	pushq	-88(%rbp)
	movq	%rbx, %rsi
	movl	$1, %r8d
	movq	%r12, %rdi
	pushq	-80(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	popq	%rsi
	jmp	.L860
.L943:
	leaq	.LC8(%rip), %r9
	jmp	.L886
.L940:
	leaq	.LC8(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L887:
	leaq	.LC16(%rip), %r9
	jmp	.L886
.L873:
	leaq	.LC16(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L863:
	leaq	.LC14(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L865:
	leaq	.LC13(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L866:
	leaq	.LC12(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L877:
	leaq	.LC14(%rip), %r9
	jmp	.L886
.L879:
	leaq	.LC13(%rip), %r9
	jmp	.L886
.L880:
	leaq	.LC12(%rip), %r9
	jmp	.L886
.L881:
	leaq	.LC11(%rip), %r9
	jmp	.L886
.L882:
	leaq	.LC15(%rip), %r9
	jmp	.L886
.L883:
	leaq	.LC10(%rip), %r9
	jmp	.L886
.L884:
	leaq	.LC9(%rip), %r9
	jmp	.L886
.L867:
	leaq	.LC11(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L868:
	leaq	.LC15(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L869:
	leaq	.LC10(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L870:
	leaq	.LC9(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L891:
	leaq	.LC0(%rip), %rcx
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L1010:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r14
	movq	%rax, %r15
	jmp	.L815
.L1011:
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L889
.L876:
	leaq	.LC7(%rip), %r9
	jmp	.L886
.L862:
	leaq	.LC7(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L872
.L908:
	leaq	.LC17(%rip), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L909:
	movq	24(%r12), %rcx
	leaq	.LC0(%rip), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rcx, %rbx
	jnb	.L911
	movzbl	(%rbx), %edi
	movb	%dl, -104(%rbp)
	movl	%edi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-96(%rbp), %edi
	movzbl	-104(%rbp), %edx
	testb	%al, %al
	je	.L1012
	movq	24(%r12), %rcx
	leaq	1(%rbx), %rax
	cmpq	%rax, %rcx
	jbe	.L911
	movzbl	1(%rbx), %eax
	sall	$8, %edi
	movb	%dl, -96(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rcx
	movzbl	-96(%rbp), %edx
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L911:
	cmpb	$9, %dl
	ja	.L913
	leaq	.L915(%rip), %rdi
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.align 4
	.align 4
.L915:
	.long	.L924-.L915
	.long	.L948-.L915
	.long	.L922-.L915
	.long	.L921-.L915
	.long	.L920-.L915
	.long	.L919-.L915
	.long	.L918-.L915
	.long	.L917-.L915
	.long	.L916-.L915
	.long	.L914-.L915
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
.L922:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L923:
	movq	16(%r12), %rdx
	cmpq	%rcx, %rdx
	jnb	.L928
	movzbl	(%rdx), %edi
	movq	%r9, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movl	%edi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-96(%rbp), %edi
	movq	-104(%rbp), %rdx
	testb	%al, %al
	movq	-112(%rbp), %r9
	je	.L1013
	leaq	1(%rdx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L928
	movzbl	1(%rdx), %eax
	sall	$8, %edi
	movq	%r9, -96(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L926:
	pushq	-88(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-80(%rbp)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L897
.L948:
	leaq	.LC8(%rip), %r9
	jmp	.L923
.L945:
	leaq	.LC8(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L910:
	leaq	.LC16(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L900:
	leaq	.LC14(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L914:
	leaq	.LC14(%rip), %r9
	jmp	.L923
.L924:
	leaq	.LC16(%rip), %r9
	jmp	.L923
.L916:
	leaq	.LC13(%rip), %r9
	jmp	.L923
.L917:
	leaq	.LC12(%rip), %r9
	jmp	.L923
.L918:
	leaq	.LC11(%rip), %r9
	jmp	.L923
.L919:
	leaq	.LC15(%rip), %r9
	jmp	.L923
.L920:
	leaq	.LC10(%rip), %r9
	jmp	.L923
.L921:
	leaq	.LC9(%rip), %r9
	jmp	.L923
.L902:
	leaq	.LC13(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L903:
	leaq	.LC12(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L904:
	leaq	.LC11(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L905:
	leaq	.LC15(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L906:
	leaq	.LC10(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L907:
	leaq	.LC9(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L928:
	leaq	.LC0(%rip), %rcx
	jmp	.L926
.L1013:
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L926
.L1012:
	movb	%dl, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rcx
	movzbl	-96(%rbp), %edx
	movq	%rax, -80(%rbp)
	jmp	.L911
.L913:
	leaq	.LC7(%rip), %r9
	jmp	.L923
.L899:
	leaq	.LC7(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L909
.L829:
	leaq	.LC17(%rip), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L830:
	movq	24(%r12), %rdx
	leaq	.LC0(%rip), %rbx
	cmpq	%rdx, %r14
	jnb	.L832
	movzbl	(%r14), %edi
	movb	%cl, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movzbl	-96(%rbp), %ecx
	testb	%al, %al
	je	.L1014
	movq	24(%r12), %rdx
	leaq	1(%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L832
	movzbl	1(%r14), %eax
	sall	$8, %edi
	movb	%cl, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdx
	movzbl	-88(%rbp), %ecx
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L832:
	cmpb	$9, %cl
	ja	.L834
	leaq	.L836(%rip), %rdi
	movslq	(%rdi,%rcx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.align 4
	.align 4
.L836:
	.long	.L845-.L836
	.long	.L937-.L836
	.long	.L843-.L836
	.long	.L842-.L836
	.long	.L841-.L836
	.long	.L840-.L836
	.long	.L839-.L836
	.long	.L838-.L836
	.long	.L837-.L836
	.long	.L835-.L836
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
.L843:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L844:
	movq	16(%r12), %rcx
	cmpq	%rdx, %rcx
	jnb	.L849
	movzbl	(%rcx), %edi
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movq	-96(%rbp), %rcx
	testb	%al, %al
	movq	-104(%rbp), %r9
	je	.L1015
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L849
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L847:
	pushq	-80(%rbp)
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC19(%rip), %rdx
	pushq	%rbx
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	jmp	.L818
.L937:
	leaq	.LC8(%rip), %r9
	jmp	.L844
.L934:
	leaq	.LC8(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L831:
	leaq	.LC16(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L821:
	leaq	.LC14(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L839:
	leaq	.LC11(%rip), %r9
	jmp	.L844
.L840:
	leaq	.LC15(%rip), %r9
	jmp	.L844
.L841:
	leaq	.LC10(%rip), %r9
	jmp	.L844
.L842:
	leaq	.LC9(%rip), %r9
	jmp	.L844
.L845:
	leaq	.LC16(%rip), %r9
	jmp	.L844
.L835:
	leaq	.LC14(%rip), %r9
	jmp	.L844
.L837:
	leaq	.LC13(%rip), %r9
	jmp	.L844
.L838:
	leaq	.LC12(%rip), %r9
	jmp	.L844
.L823:
	leaq	.LC13(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L824:
	leaq	.LC12(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L825:
	leaq	.LC11(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L826:
	leaq	.LC15(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L827:
	leaq	.LC10(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L828:
	leaq	.LC9(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L849:
	leaq	.LC0(%rip), %rcx
	jmp	.L847
.L1015:
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L847
.L1014:
	movb	%cl, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdx
	movzbl	-88(%rbp), %ecx
	movq	%rax, %rbx
	jmp	.L832
.L820:
	leaq	.LC7(%rip), %rax
	movq	%rax, -80(%rbp)
	jmp	.L830
.L834:
	leaq	.LC7(%rip), %r9
	jmp	.L844
.L998:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1003:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23715:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE:
.LFB24026:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movzbl	%dl, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	leaq	2(%r13), %rsi
	cmpq	%rax, %rsi
	ja	.L1017
	cmpl	%esi, %eax
	je	.L1017
	leal	-64773(%r14), %eax
	movzbl	2(%r13), %ebx
	cmpl	$18, %eax
	ja	.L1018
	leaq	.L1020(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE,"a",@progbits
	.align 4
	.align 4
.L1020:
	.long	.L1023-.L1020
	.long	.L1018-.L1020
	.long	.L1023-.L1020
	.long	.L1018-.L1020
	.long	.L1022-.L1020
	.long	.L1018-.L1020
	.long	.L1022-.L1020
	.long	.L1018-.L1020
	.long	.L1100-.L1020
	.long	.L1100-.L1020
	.long	.L1018-.L1020
	.long	.L1019-.L1020
	.long	.L1019-.L1020
	.long	.L1018-.L1020
	.long	.L1100-.L1020
	.long	.L1100-.L1020
	.long	.L1018-.L1020
	.long	.L1019-.L1020
	.long	.L1019-.L1020
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.p2align 4,,10
	.p2align 3
.L1104:
	xorl	%ebx, %ebx
.L1019:
	movl	$2, %eax
.L1021:
	cmpb	%bl, %al
	jbe	.L1130
.L1025:
	movq	208(%r12), %rdx
	movq	%r13, -112(%rbp)
	movabsq	$-6148914691236517205, %r10
	movq	200(%r12), %r11
	movq	240(%r12), %rdi
	movb	$10, -104(%rbp)
	movq	%rdx, %rax
	movq	$0, -96(%rbp)
	subq	%r11, %rax
	movl	-132(%rdi), %r9d
	movq	%r13, -88(%rbp)
	sarq	$3, %rax
	movb	$10, -80(%rbp)
	imulq	%r10, %rax
	movq	$0, -72(%rbp)
	cmpq	%rax, %r9
	jb	.L1131
	cmpb	$2, -120(%rdi)
	jne	.L1132
.L1030:
	movq	$0, -136(%rbp)
	movq	%r13, %rsi
	movl	$10, %r15d
.L1035:
	movq	-136(%rbp), %rcx
	movq	%rsi, -88(%rbp)
	movb	%r15b, -80(%rbp)
	movq	%rcx, -72(%rbp)
	cmpq	%rax, %r9
	jb	.L1069
	cmpb	$2, -120(%rdi)
	movq	16(%r12), %r15
	jne	.L1133
.L1070:
	xorl	%r10d, %r10d
	movl	$10, %r13d
.L1074:
	leaq	-113(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	%r13b, -104(%rbp)
	leaq	192(%r12), %rdi
	movq	%r15, -112(%rbp)
	movq	%r10, -96(%rbp)
	movb	$5, -113(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	$0, 56(%r12)
	movq	208(%r12), %r13
	jne	.L1029
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	je	.L1134
.L1029:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1135
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	movl	$4, %eax
	cmpb	%bl, %al
	ja	.L1025
.L1130:
	leaq	2(%r13), %rsi
	leaq	.LC25(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1102:
	xorl	%ebx, %ebx
.L1022:
	movl	$8, %eax
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1101:
	xorl	%ebx, %ebx
.L1023:
	movl	$16, %eax
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %r15d
	subq	$24, %rdx
	movq	(%rdx), %rsi
	movq	%rdx, 208(%r12)
	movq	%rax, -136(%rbp)
	cmpb	%r15b, %cl
	jne	.L1036
.L1127:
	movq	%rdx, %rax
	movl	-132(%rdi), %r9d
	subq	%r11, %rax
	sarq	$3, %rax
	imulq	%r10, %rax
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1069:
	movzbl	-16(%rdx), %r13d
	movq	-24(%rdx), %r15
	subq	$24, %rdx
	movq	16(%rdx), %r10
	movq	%rdx, 208(%r12)
	cmpb	$5, %r13b
	je	.L1074
	cmpb	$10, %r13b
	je	.L1074
	cmpb	$9, %r13b
	ja	.L1075
	leaq	.L1077(%rip), %rcx
	movzbl	%r13b, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.align 4
	.align 4
.L1077:
	.long	.L1086-.L1077
	.long	.L1110-.L1077
	.long	.L1084-.L1077
	.long	.L1083-.L1077
	.long	.L1082-.L1077
	.long	.L1081-.L1077
	.long	.L1080-.L1077
	.long	.L1079-.L1077
	.long	.L1078-.L1077
	.long	.L1076-.L1077
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
.L1084:
	leaq	.LC17(%rip), %rdx
.L1085:
	movq	24(%r12), %rax
	leaq	.LC0(%rip), %rcx
	movq	%rcx, -136(%rbp)
	cmpq	%rax, %r15
	jnb	.L1087
	movzbl	(%r15), %edi
	movq	%rdx, -160(%rbp)
	movq	%r10, -152(%rbp)
	movl	%edi, -144(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-144(%rbp), %edi
	movq	-152(%rbp), %r10
	testb	%al, %al
	movq	-160(%rbp), %rdx
	je	.L1136
	movq	24(%r12), %rax
	leaq	1(%r15), %rcx
	cmpq	%rcx, %rax
	jbe	.L1087
	movzbl	1(%r15), %eax
	sall	$8, %edi
	movq	%rdx, -152(%rbp)
	movq	%r10, -144(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %r10
	movq	%rax, -136(%rbp)
	movq	24(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	16(%r12), %rcx
	cmpq	%rax, %rcx
	jnb	.L1092
	movzbl	(%rcx), %edi
	movq	%rdx, -168(%rbp)
	movq	%r10, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movl	%edi, -144(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-144(%rbp), %edi
	movq	-152(%rbp), %rcx
	testb	%al, %al
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %rdx
	je	.L1137
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1092
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%rdx, -152(%rbp)
	movq	%r10, -144(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L1090
.L1110:
	leaq	.LC8(%rip), %rdx
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1133:
	leaq	.LC0(%rip), %r13
	cmpq	%r15, 24(%r12)
	ja	.L1138
.L1071:
	movq	%r15, %rsi
	movq	%r13, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %r15
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1132:
	cmpq	%r13, 24(%r12)
	ja	.L1031
.L1034:
	leaq	.LC0(%rip), %rcx
.L1032:
	leaq	.LC18(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	208(%r12), %rdx
	movq	16(%r12), %r13
	movabsq	$-6148914691236517205, %rcx
	movq	240(%r12), %rdi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	movl	-132(%rdi), %r9d
	imulq	%rcx, %rax
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	144(%r12), %r15
	movq	104(%r15), %rax
	cmpq	$1, %rax
	jbe	.L1094
	movq	96(%r15), %rcx
.L1095:
	movq	-96(%rbp), %xmm0
	movzbl	%bl, %edx
	movl	%r14d, %esi
	movhps	-72(%rbp), %xmm0
	movups	%xmm0, (%rcx)
	movq	144(%r12), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder10SimdLaneOpENS0_4wasm10WasmOpcodeEhPKPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1098
	cmpl	$-1, 152(%r12)
	je	.L1098
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L1098:
	movq	%rax, -8(%r13)
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1017:
	leaq	.LC24(%rip), %rdx
	movq	%r12, %rdi
	movl	%ecx, -144(%rbp)
	movb	%r8b, -136(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	leal	-64773(%r14), %eax
	movq	16(%r12), %r13
	cmpl	$18, %eax
	ja	.L1018
	leaq	.L1024(%rip), %rdx
	movzbl	-136(%rbp), %r8d
	movl	-144(%rbp), %ecx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.align 4
	.align 4
.L1024:
	.long	.L1101-.L1024
	.long	.L1018-.L1024
	.long	.L1101-.L1024
	.long	.L1018-.L1024
	.long	.L1102-.L1024
	.long	.L1018-.L1024
	.long	.L1102-.L1024
	.long	.L1018-.L1024
	.long	.L1103-.L1024
	.long	.L1103-.L1024
	.long	.L1018-.L1024
	.long	.L1104-.L1024
	.long	.L1104-.L1024
	.long	.L1018-.L1024
	.long	.L1103-.L1024
	.long	.L1103-.L1024
	.long	.L1018-.L1024
	.long	.L1104-.L1024
	.long	.L1104-.L1024
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.p2align 4,,10
	.p2align 3
.L1092:
	leaq	.LC0(%rip), %rcx
.L1090:
	pushq	%rdx
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rdx
	xorl	%r8d, %r8d
	pushq	-136(%rbp)
	leaq	.LC15(%rip), %r9
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, -144(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	movq	-144(%rbp), %r10
	popq	%rdx
	jmp	.L1074
.L1078:
	leaq	.LC13(%rip), %rdx
	jmp	.L1085
.L1079:
	leaq	.LC12(%rip), %rdx
	jmp	.L1085
.L1080:
	leaq	.LC11(%rip), %rdx
	jmp	.L1085
.L1081:
	leaq	.LC15(%rip), %rdx
	jmp	.L1085
.L1082:
	leaq	.LC10(%rip), %rdx
	jmp	.L1085
.L1083:
	leaq	.LC9(%rip), %rdx
	jmp	.L1085
.L1086:
	leaq	.LC16(%rip), %rdx
	jmp	.L1085
.L1076:
	leaq	.LC14(%rip), %rdx
	jmp	.L1085
.L1018:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1036:
	cmpb	$10, %r15b
	je	.L1127
	cmpb	$9, %r15b
	ja	.L1038
	leaq	.L1040(%rip), %rdi
	movzbl	%r15b, %edx
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.align 4
	.align 4
.L1040:
	.long	.L1049-.L1040
	.long	.L1105-.L1040
	.long	.L1047-.L1040
	.long	.L1046-.L1040
	.long	.L1045-.L1040
	.long	.L1044-.L1040
	.long	.L1043-.L1040
	.long	.L1042-.L1040
	.long	.L1041-.L1040
	.long	.L1039-.L1040
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
.L1047:
	leaq	.LC17(%rip), %rdx
.L1048:
	movq	24(%r12), %rdi
	leaq	.LC0(%rip), %rax
	movq	%rax, -144(%rbp)
	cmpq	%rdi, %rsi
	jnb	.L1050
	movzbl	(%rsi), %r13d
	movl	%ecx, -176(%rbp)
	movb	%r8b, -168(%rbp)
	movl	%r13d, %edi
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-152(%rbp), %rsi
	movl	%r13d, %edi
	movq	-160(%rbp), %rdx
	testb	%al, %al
	movzbl	-168(%rbp), %r8d
	movl	-176(%rbp), %ecx
	je	.L1126
	movq	24(%r12), %rdi
	leaq	1(%rsi), %rax
	cmpq	%rax, %rdi
	jbe	.L1107
	movl	%r13d, %edi
	movl	%ecx, -176(%rbp)
	movzbl	1(%rsi), %r13d
	movb	%r8b, -168(%rbp)
	sall	$8, %edi
	movq	%rdx, -160(%rbp)
	orl	%r13d, %edi
	movq	%rsi, -152(%rbp)
.L1126:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r12), %rdi
	movq	16(%r12), %r13
	movq	%rax, -144(%rbp)
	movq	-152(%rbp), %rsi
	movq	-160(%rbp), %rdx
	movzbl	-168(%rbp), %r8d
	movl	-176(%rbp), %ecx
.L1050:
	cmpb	$10, %cl
	ja	.L1052
	leaq	.L1054(%rip), %rcx
	movslq	(%rcx,%r8,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.align 4
	.align 4
.L1054:
	.long	.L1064-.L1054
	.long	.L1108-.L1054
	.long	.L1062-.L1054
	.long	.L1061-.L1054
	.long	.L1060-.L1054
	.long	.L1059-.L1054
	.long	.L1058-.L1054
	.long	.L1057-.L1054
	.long	.L1056-.L1054
	.long	.L1055-.L1054
	.long	.L1053-.L1054
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
.L1105:
	leaq	.LC8(%rip), %rdx
	jmp	.L1048
.L1049:
	leaq	.LC16(%rip), %rdx
	jmp	.L1048
.L1039:
	leaq	.LC14(%rip), %rdx
	jmp	.L1048
.L1041:
	leaq	.LC13(%rip), %rdx
	jmp	.L1048
.L1042:
	leaq	.LC12(%rip), %rdx
	jmp	.L1048
.L1043:
	leaq	.LC11(%rip), %rdx
	jmp	.L1048
.L1044:
	leaq	.LC15(%rip), %rdx
	jmp	.L1048
.L1045:
	leaq	.LC10(%rip), %rdx
	jmp	.L1048
.L1046:
	leaq	.LC9(%rip), %rdx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1138:
	movzbl	(%r15), %edi
	movl	%edi, -136(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-136(%rbp), %edi
	testb	%al, %al
	je	.L1128
	leaq	1(%r15), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1129
	movzbl	1(%r15), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1128:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
.L1129:
	movq	16(%r12), %r15
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1031:
	movzbl	0(%r13), %r15d
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L1139
	leaq	1(%r13), %rax
	cmpq	%rax, 24(%r12)
	ja	.L1124
	movq	16(%r12), %r13
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1103:
	xorl	%ebx, %ebx
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1136:
	movq	%rdx, -152(%rbp)
	movq	%r10, -144(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %rdx
	movq	%rax, -136(%rbp)
	movq	24(%r12), %rax
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	%rdx, -152(%rbp)
	movq	%r10, -144(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1090
.L1062:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L1063:
	cmpq	%rdi, %r13
	jnb	.L1068
	movzbl	0(%r13), %edi
	movq	%r9, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movl	%edi, -152(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-152(%rbp), %edi
	movq	-160(%rbp), %rsi
	testb	%al, %al
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %r9
	je	.L1140
	leaq	1(%r13), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1068
	movzbl	1(%r13), %eax
	sall	$8, %edi
	movq	%r9, -168(%rbp)
	movq	%rdx, -160(%rbp)
	orl	%eax, %edi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-168(%rbp), %r9
	movq	-160(%rbp), %rdx
	movq	-152(%rbp), %rsi
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1066:
	pushq	%rdx
	movq	%r12, %rdi
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	pushq	-144(%rbp)
	movl	$1, %r8d
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	208(%r12), %rdx
	movq	240(%r12), %rdi
	movabsq	$-6148914691236517205, %rcx
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movl	-132(%rdi), %r9d
	sarq	$3, %rax
	imulq	%rcx, %rax
	popq	%rcx
	popq	%rsi
	movq	-144(%rbp), %rsi
	jmp	.L1035
.L1108:
	leaq	.LC8(%rip), %r9
	jmp	.L1063
.L1064:
	leaq	.LC16(%rip), %r9
	jmp	.L1063
.L1055:
	leaq	.LC14(%rip), %r9
	jmp	.L1063
.L1056:
	leaq	.LC13(%rip), %r9
	jmp	.L1063
.L1057:
	leaq	.LC12(%rip), %r9
	jmp	.L1063
.L1058:
	leaq	.LC11(%rip), %r9
	jmp	.L1063
.L1061:
	leaq	.LC9(%rip), %r9
	jmp	.L1063
.L1059:
	leaq	.LC15(%rip), %r9
	jmp	.L1063
.L1060:
	leaq	.LC10(%rip), %r9
	jmp	.L1063
.L1053:
	leaq	.LC20(%rip), %r9
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1068:
	leaq	.LC0(%rip), %rcx
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	(%r15), %rdi
	leaq	7(%rax), %rdx
	leaq	56(,%rax,8), %rsi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1141
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1097:
	movq	%rcx, 96(%r15)
	movq	%rdx, 104(%r15)
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	16(%r12), %r13
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	%r9, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-152(%rbp), %rsi
	movq	-160(%rbp), %rdx
	movq	-168(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r13
	movq	%rax, %rcx
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1124:
	movzbl	1(%r13), %edi
	sall	$8, %r15d
	orl	%r15d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	16(%r12), %r13
	movq	%rax, %rcx
	jmp	.L1032
.L1052:
	leaq	.LC7(%rip), %r9
	jmp	.L1063
.L1075:
	leaq	.LC7(%rip), %rdx
	jmp	.L1085
.L1038:
	leaq	.LC7(%rip), %rdx
	jmp	.L1048
.L1141:
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1097
.L1135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24026:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,"axG",@progbits,_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag:
.LFB6932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L1169
.L1142:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1170
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_restore_state
	movq	$256, -328(%rbp)
	movq	%rdi, %r12
	movl	%esi, %ebx
	leaq	-320(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L1171
	movq	-336(%rbp), %r14
	leaq	-400(%rbp), %r13
	movslq	%eax, %r15
	leaq	-416(%rbp), %rdi
	movq	%r13, -416(%rbp)
	testq	%r14, %r14
	je	.L1172
	movq	%r15, -424(%rbp)
	cmpl	$15, %eax
	jg	.L1173
	movq	%r13, %rdi
	cmpl	$1, %eax
	jne	.L1148
	movzbl	(%r14), %eax
	movq	%r13, %rdx
	movb	%al, -400(%rbp)
	movl	$1, %eax
.L1149:
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	-416(%rbp), %rax
	movl	%ebx, -384(%rbp)
	movq	%r14, -376(%rbp)
	cmpq	%r13, %rax
	je	.L1174
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %rdx
	movq	%rax, -376(%rbp)
	movq	%r13, -416(%rbp)
	movq	48(%r12), %rdi
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	$0, -408(%rbp)
	movb	$0, -400(%rbp)
	movl	%ebx, 40(%r12)
	cmpq	%r14, %rax
	je	.L1151
	leaq	64(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L1175
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	64(%r12), %rsi
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%r12)
	testq	%rdi, %rdi
	je	.L1157
	movq	%rdi, -376(%rbp)
	movq	%rsi, -360(%rbp)
.L1155:
	movq	$0, -368(%rbp)
	movb	$0, (%rdi)
	movq	-376(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1158
	call	_ZdlPv@PLT
.L1158:
	movq	-416(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1159
	call	_ZdlPv@PLT
.L1159:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1173:
	leaq	-424(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -416(%rbp)
	movq	%rax, %rdi
	movq	-424(%rbp), %rax
	movq	%rax, -400(%rbp)
.L1148:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-424(%rbp), %rax
	movq	-416(%rbp), %rdx
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	-408(%rbp), %rdx
	movq	48(%r12), %rdi
	movl	%ebx, 40(%r12)
	movdqa	-400(%rbp), %xmm2
	movq	$0, -408(%rbp)
	movq	%rdx, -368(%rbp)
	movb	$0, -400(%rbp)
	movups	%xmm2, -360(%rbp)
.L1151:
	testq	%rdx, %rdx
	je	.L1153
	cmpq	$1, %rdx
	je	.L1176
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
.L1153:
	movq	%rdx, 56(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-376(%rbp), %rdi
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 56(%r12)
.L1157:
	movq	%r14, -376(%rbp)
	leaq	-360(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1176:
	movzbl	-360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1171:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1170:
	call	__stack_chk_fail@PLT
.L1172:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE6932:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz:
.LFB6911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L1178
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L1178:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	8(%rdi), %rsi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	addl	32(%rdi), %esi
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1181
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1181:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6911:
	.size	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEjPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz:
.LFB6910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L1183
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L1183:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1186
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1186:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6910:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKhPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKhPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKhPKc:
.LFB6908:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	subq	8(%rdi), %rsi
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE6908:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.section	.rodata._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0.str1.1,"aMS",@progbits,1
.LC26:
	.string	"expected %s"
.LC27:
	.string	"extra bits in varint"
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0:
.LFB24942:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r8
	cmpq	%rsi, %r8
	ja	.L1206
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	xorl	%eax, %eax
.L1188:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movl	%r9d, %eax
	andl	$127, %eax
	testb	%r9b, %r9b
	js	.L1207
	movl	$1, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1207:
	.cfi_restore_state
	leaq	1(%rsi), %r9
	cmpq	%r9, %r8
	jbe	.L1191
	movzbl	1(%rsi), %r10d
	movl	%r10d, %r9d
	sall	$7, %r9d
	andl	$16256, %r9d
	orl	%r9d, %eax
	testb	%r10b, %r10b
	js	.L1208
	movl	$2, (%rdx)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1191:
	movl	$1, (%rdx)
.L1205:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	leaq	2(%rsi), %r9
	cmpq	%r9, %r8
	jbe	.L1193
	movzbl	2(%rsi), %r10d
	movl	%r10d, %r9d
	sall	$14, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %eax
	testb	%r10b, %r10b
	js	.L1209
	movl	$3, (%rdx)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1193:
	movl	$2, (%rdx)
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1209:
	leaq	3(%rsi), %r9
	cmpq	%r9, %r8
	ja	.L1210
	movl	$3, (%rdx)
	jmp	.L1205
.L1210:
	movzbl	3(%rsi), %r10d
	movl	%r10d, %r9d
	sall	$21, %r9d
	andl	$266338304, %r9d
	orl	%r9d, %eax
	testb	%r10b, %r10b
	js	.L1211
	movl	$4, (%rdx)
	jmp	.L1188
.L1211:
	leaq	4(%rsi), %r12
	cmpq	%r12, %r8
	jbe	.L1197
	movzbl	4(%rsi), %ebx
	movl	$5, (%rdx)
	testb	%bl, %bl
	js	.L1198
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, %eax
.L1199:
	andl	$240, %ebx
	je	.L1188
	leaq	.LC27(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1188
.L1197:
	movl	$4, (%rdx)
	xorl	%ebx, %ebx
.L1198:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L1199
	.cfi_endproc
.LFE24942:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	.section	.rodata._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0.str1.1,"aMS",@progbits,1
.LC28:
	.string	"alignment"
	.section	.rodata._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"invalid alignment; expected maximum alignment is %u, actual alignment is %u"
	.section	.rodata._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0.str1.1
.LC30:
	.string	"offset"
	.section	.text._ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0, @function
_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0:
.LFB24943:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	1(%rdx), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$0, 8(%rdi)
	movq	24(%r14), %rax
	cmpq	%rax, %rsi
	jb	.L1247
	leaq	.LC28(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	%rsi, -56(%rbp)
	leaq	.LC26(%rip), %rdx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-56(%rbp), %rsi
.L1222:
	movl	$0, 0(%r13)
	movq	%rsi, %r12
.L1223:
	movq	24(%r14), %rax
	cmpq	%r12, %rax
	ja	.L1248
.L1224:
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	xorl	%edx, %edx
.L1226:
	movl	%r15d, 8(%r13)
	movl	%edx, 4(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1247:
	.cfi_restore_state
	movzbl	1(%rdx), %edi
	movq	%rdx, %rbx
	movl	$2, %r12d
	movl	$1, %r15d
	movl	%edi, %r8d
	andl	$127, %r8d
	testb	%dil, %dil
	js	.L1249
.L1214:
	movl	%r8d, 0(%r13)
	addq	%rbx, %r12
	cmpl	%r8d, %ecx
	jnb	.L1223
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	24(%r14), %rax
	cmpq	%r12, %rax
	jbe	.L1224
.L1248:
	movzbl	(%r12), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L1225
	addl	$1, %r15d
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1249:
	leaq	2(%rdx), %r12
	cmpq	%r12, %rax
	jbe	.L1215
	movzbl	2(%rdx), %edi
	movl	$3, %r12d
	movl	$2, %r15d
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r8d
	testb	%dil, %dil
	jns	.L1214
	leaq	3(%rbx), %r12
	cmpq	%r12, %rax
	jbe	.L1215
	movzbl	3(%rbx), %edi
	movl	$4, %r12d
	movl	$3, %r15d
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r8d
	testb	%dil, %dil
	jns	.L1214
	leaq	4(%rbx), %r12
	cmpq	%r12, %rax
	jbe	.L1215
	movzbl	4(%rbx), %edi
	movl	$5, %r12d
	movl	$4, %r15d
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r8d
	testb	%dil, %dil
	jns	.L1214
	leaq	5(%rbx), %r12
	cmpq	%r12, %rax
	ja	.L1250
	leaq	.LC28(%rip), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1221:
	movl	%r15d, %eax
	movl	$0, 0(%r13)
	leaq	1(%rbx,%rax), %r12
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1225:
	leaq	1(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L1227
	movzbl	1(%r12), %esi
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1228
	addl	$2, %r15d
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	%r12, %rsi
	leaq	.LC28(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	%r12, %rsi
	movl	$0, 0(%r13)
	movq	%rsi, %r12
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1227:
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$1, %r15d
	leaq	.LC30(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1228:
	leaq	2(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L1229
	movzbl	2(%r12), %esi
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1230
	addl	$3, %r15d
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1229:
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$2, %r15d
	leaq	.LC30(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	3(%r12), %rsi
	cmpq	%rsi, %rax
	ja	.L1251
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$3, %r15d
	leaq	.LC30(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1226
.L1251:
	movzbl	3(%r12), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1232
	addl	$4, %r15d
	jmp	.L1226
.L1232:
	leaq	4(%r12), %rbx
	cmpq	%rbx, %rax
	jbe	.L1240
	movzbl	4(%r12), %r12d
	testb	%r12b, %r12b
	js	.L1241
	movl	%r12d, %eax
	movl	$5, %r8d
	sall	$28, %eax
	orl	%eax, %edx
.L1234:
	addl	%r8d, %r15d
	andl	$240, %r12d
	je	.L1226
	leaq	.LC27(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%edx, %edx
	jmp	.L1226
.L1250:
	movzbl	5(%rbx), %eax
	movl	%eax, %r15d
	andl	$-16, %r15d
	testb	%al, %al
	js	.L1252
	sall	$28, %eax
	orl	%eax, %r8d
	testb	%r15b, %r15b
	je	.L1239
.L1220:
	movq	%r12, %rsi
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rdi
	movl	$5, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	leaq	6(%rbx), %rsi
	jmp	.L1222
.L1240:
	xorl	%r12d, %r12d
	movl	$4, %r8d
.L1233:
	leaq	.LC26(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rcx
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-56(%rbp), %r8d
	xorl	%edx, %edx
	jmp	.L1234
.L1241:
	movl	$5, %r8d
	jmp	.L1233
.L1252:
	xorl	%eax, %eax
	leaq	.LC28(%rip), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%r15b, %r15b
	jne	.L1220
	movl	$5, %r15d
	jmp	.L1221
.L1239:
	movl	$6, %r12d
	movl	$5, %r15d
	jmp	.L1214
	.cfi_endproc
.LFE24943:
	.size	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0, .-_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"memory instruction with no memory"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi:
.LFB23683:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movb	%sil, -72(%rbp)
	movq	16(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	cmpb	$0, 18(%rax)
	je	.L1331
	movzbl	-72(%rbp), %r13d
	leaq	_ZN2v88internal4wasm8LoadType13kLoadSizeLog2E(%rip), %rax
	movslq	%edx, %rdx
	addq	%rdx, %r15
	movzbl	(%rax,%r13), %ecx
	movq	24(%rdi), %rax
	leaq	1(%r15), %rsi
	cmpq	%rax, %rsi
	jnb	.L1256
	movzbl	1(%r15), %edx
	movl	$2, %r9d
	movl	$1, %ebx
	movl	%edx, %r14d
	andl	$127, %r14d
	testb	%dl, %dl
	js	.L1332
.L1257:
	addq	%r15, %r9
	cmpl	%r14d, %ecx
	jnb	.L1266
	xorl	%eax, %eax
	movl	%r14d, %r8d
	leaq	.LC29(%rip), %rdx
	movq	%r12, %rdi
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	24(%r12), %rax
	movq	-80(%rbp), %r9
.L1266:
	cmpq	%rax, %r9
	jb	.L1333
.L1267:
	leaq	.LC30(%rip), %rcx
	movq	%r9, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
.L1269:
	movq	208(%r12), %rdx
	movq	240(%r12), %rcx
	movabsq	$-6148914691236517205, %rdi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L1278
	cmpb	$2, -120(%rcx)
	jne	.L1334
.L1279:
	movq	$0, -80(%rbp)
.L1283:
	leaq	_ZN2v88internal4wasm8LoadType10kValueTypeE(%rip), %rax
	leaq	-57(%rbp), %rdx
	movzbl	(%rax,%r13), %r13d
	leaq	16(%r12), %rsi
	leaq	192(%r12), %rdi
	movb	%r13b, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	$0, 56(%r12)
	movq	208(%r12), %r10
	jne	.L1302
	movq	240(%r12), %rdx
	cmpb	$0, -120(%rdx)
	je	.L1335
.L1302:
	movl	%ebx, %eax
.L1253:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1336
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1256:
	.cfi_restore_state
	leaq	.LC28(%rip), %rcx
	xorl	%eax, %eax
	movq	%rsi, -80(%rbp)
	xorl	%ebx, %ebx
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-80(%rbp), %rsi
.L1265:
	movq	24(%r12), %rax
	movq	%rsi, %r9
	xorl	%r14d, %r14d
	cmpq	%rax, %r9
	jnb	.L1267
.L1333:
	movzbl	(%r9), %edx
	movl	%edx, %r15d
	andl	$127, %r15d
	testb	%dl, %dl
	js	.L1268
	addl	$1, %ebx
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	-8(%rdx), %rcx
	movzbl	-16(%rdx), %eax
	subq	$24, %rdx
	movq	(%rdx), %rsi
	movq	%rdx, 208(%r12)
	movq	%rcx, -80(%rbp)
	cmpb	$1, %al
	je	.L1283
	cmpb	$10, %al
	je	.L1283
	cmpb	$9, %al
	ja	.L1284
	leaq	.L1286(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi,"a",@progbits
	.align 4
	.align 4
.L1286:
	.long	.L1295-.L1286
	.long	.L1313-.L1286
	.long	.L1293-.L1286
	.long	.L1292-.L1286
	.long	.L1291-.L1286
	.long	.L1290-.L1286
	.long	.L1289-.L1286
	.long	.L1288-.L1286
	.long	.L1287-.L1286
	.long	.L1285-.L1286
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
.L1293:
	leaq	.LC17(%rip), %rdx
.L1294:
	movq	24(%r12), %rax
	leaq	.LC0(%rip), %rcx
	movq	%rcx, -88(%rbp)
	cmpq	%rax, %rsi
	jb	.L1337
.L1296:
	movq	16(%r12), %rcx
	cmpq	%rax, %rcx
	jb	.L1298
.L1301:
	leaq	.LC0(%rip), %rcx
.L1299:
	pushq	%rdx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	pushq	-88(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rsi
	popq	%rdi
	jmp	.L1283
.L1313:
	leaq	.LC8(%rip), %rdx
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1332:
	leaq	2(%r15), %r8
	cmpq	%r8, %rax
	jbe	.L1258
	movzbl	2(%r15), %edi
	movl	$3, %r9d
	movl	$2, %ebx
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r14d
	testb	%dil, %dil
	jns	.L1257
	leaq	3(%r15), %r8
	cmpq	%r8, %rax
	jbe	.L1258
	movzbl	3(%r15), %edi
	movl	$4, %r9d
	movl	$3, %ebx
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r14d
	testb	%dil, %dil
	jns	.L1257
	leaq	4(%r15), %r8
	cmpq	%r8, %rax
	jbe	.L1258
	movzbl	4(%r15), %edi
	movl	$5, %r9d
	movl	$4, %ebx
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r14d
	testb	%dil, %dil
	jns	.L1257
	leaq	5(%r15), %r8
	cmpq	%r8, %rax
	ja	.L1338
	leaq	.LC28(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movl	$4, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1264:
	movl	%ebx, %eax
	xorl	%r14d, %r14d
	leaq	1(%r15,%rax), %r9
	movq	24(%r12), %rax
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1268:
	leaq	1(%r9), %rsi
	cmpq	%rax, %rsi
	jnb	.L1270
	movzbl	1(%r9), %ecx
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	js	.L1271
	addl	$2, %ebx
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1331:
	leaq	-1(%r15), %rsi
	leaq	.LC31(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	xorl	%eax, %eax
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1335:
	movzbl	-72(%rbp), %eax
	subq	$8, %rsp
	movq	16(%r12), %rdx
	movl	%r14d, %r9d
	subq	8(%r12), %rdx
	movzbl	%r13b, %esi
	movl	%r15d, %r8d
	movq	%r10, -88(%rbp)
	leaq	_ZN2v88internal4wasm8LoadType8kMemTypeE(%rip), %rcx
	movq	144(%r12), %rdi
	movzwl	(%rcx,%rax,2), %r11d
	movq	-80(%rbp), %rcx
	pushq	%rdx
	movl	%r11d, %edx
	call	_ZN2v88internal8compiler16WasmGraphBuilder7LoadMemENS0_4wasm9ValueTypeENS0_11MachineTypeEPNS1_4NodeEjji@PLT
	popq	%rdx
	movq	-88(%rbp), %r10
	testq	%rax, %rax
	popq	%rcx
	je	.L1303
	cmpl	$-1, 152(%r12)
	je	.L1303
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-72(%rbp), %r10
.L1303:
	movq	%rax, -8(%r10)
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	%r8, %rsi
	leaq	.LC28(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-80(%rbp), %r8
	movq	%r8, %rsi
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1270:
	leaq	.LC30(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	addl	$1, %ebx
	leaq	.LC26(%rip), %rdx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1337:
	movzbl	(%rsi), %edi
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movl	%edi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-96(%rbp), %edi
	movq	-104(%rbp), %rsi
	testb	%al, %al
	movq	-112(%rbp), %rdx
	je	.L1339
	movq	24(%r12), %rax
	leaq	1(%rsi), %rcx
	cmpq	%rcx, %rax
	jbe	.L1296
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%rax, -88(%rbp)
	movq	24(%r12), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1298:
	movzbl	(%rcx), %edi
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%edi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-96(%rbp), %edi
	movq	-104(%rbp), %rcx
	testb	%al, %al
	movq	-112(%rbp), %rsi
	movq	-120(%rbp), %rdx
	je	.L1340
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r12)
	jbe	.L1301
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1334:
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L1280
	movzbl	(%rsi), %edi
	movq	%rcx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movq	-88(%rbp), %rsi
	testb	%al, %al
	movq	-96(%rbp), %rcx
	je	.L1329
	leaq	1(%rsi), %rax
	cmpq	%rax, 24(%r12)
	ja	.L1282
.L1330:
	movq	16(%r12), %rsi
.L1280:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	2(%r9), %rsi
	cmpq	%rax, %rsi
	jnb	.L1272
	movzbl	2(%r9), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	js	.L1273
	addl	$3, %ebx
	jmp	.L1269
.L1287:
	leaq	.LC13(%rip), %rdx
	jmp	.L1294
.L1285:
	leaq	.LC14(%rip), %rdx
	jmp	.L1294
.L1295:
	leaq	.LC16(%rip), %rdx
	jmp	.L1294
.L1292:
	leaq	.LC9(%rip), %rdx
	jmp	.L1294
.L1291:
	leaq	.LC10(%rip), %rdx
	jmp	.L1294
.L1290:
	leaq	.LC15(%rip), %rdx
	jmp	.L1294
.L1289:
	leaq	.LC11(%rip), %rdx
	jmp	.L1294
.L1288:
	leaq	.LC12(%rip), %rdx
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1272:
	leaq	.LC30(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	addl	$2, %ebx
	leaq	.LC26(%rip), %rdx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1282:
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1329:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rcx
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1340:
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	24(%r12), %rax
	jmp	.L1296
.L1284:
	leaq	.LC7(%rip), %rdx
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1273:
	leaq	3(%r9), %rsi
	cmpq	%rax, %rsi
	jb	.L1341
	leaq	.LC30(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	addl	$3, %ebx
	leaq	.LC26(%rip), %rdx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1269
.L1341:
	movzbl	3(%r9), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	js	.L1275
	addl	$4, %ebx
	jmp	.L1269
.L1275:
	leaq	4(%r9), %rsi
	cmpq	%rax, %rsi
	jnb	.L1310
	movzbl	4(%r9), %r8d
	testb	%r8b, %r8b
	js	.L1311
	movl	%r8d, %eax
	movl	$5, %r9d
	sall	$28, %eax
	orl	%eax, %r15d
.L1277:
	addl	%r9d, %ebx
	andl	$240, %r8d
	je	.L1269
	leaq	.LC27(%rip), %rdx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1269
.L1338:
	movzbl	5(%r15), %edx
	movl	%edx, %ebx
	andl	$-16, %ebx
	testb	%dl, %dl
	js	.L1342
	sall	$28, %edx
	orl	%edx, %r14d
	testb	%bl, %bl
	je	.L1309
.L1263:
	movq	%r8, %rsi
	leaq	.LC27(%rip), %rdx
	movq	%r12, %rdi
	movl	$5, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	leaq	6(%r15), %rsi
	jmp	.L1265
.L1310:
	xorl	%r8d, %r8d
	movl	$4, %r9d
.L1276:
	leaq	.LC30(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	%r9d, -96(%rbp)
	leaq	.LC26(%rip), %rdx
	movb	%r8b, -88(%rbp)
	xorl	%r15d, %r15d
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-96(%rbp), %r9d
	movzbl	-88(%rbp), %r8d
	movq	-80(%rbp), %rsi
	jmp	.L1277
.L1311:
	movl	$5, %r9d
	jmp	.L1276
.L1342:
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC28(%rip), %rcx
	movq	%r12, %rdi
	leaq	.LC26(%rip), %rdx
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%bl, %bl
	movq	-80(%rbp), %r8
	jne	.L1263
	movl	$5, %ebx
	jmp	.L1264
.L1336:
	call	__stack_chk_fail@PLT
.L1309:
	movl	$6, %r9d
	movl	$5, %ebx
	jmp	.L1257
	.cfi_endproc
.LFE23683:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi:
.LFB23685:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	80(%rdi), %rax
	movq	16(%rdi), %r13
	cmpb	$0, 18(%rax)
	je	.L1477
	leaq	_ZN2v88internal4wasm9StoreType14kStoreSizeLog2E(%rip), %rax
	movzbl	%sil, %ebx
	movslq	%edx, %rdx
	movzbl	(%rax,%rbx), %ecx
	addq	%rdx, %r13
	movq	24(%rdi), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r15
	jnb	.L1346
	movzbl	1(%r13), %edx
	movl	$2, %esi
	movl	$1, %r12d
	movl	%edx, %r10d
	andl	$127, %r10d
	movl	%r10d, -56(%rbp)
	testb	%dl, %dl
	js	.L1478
.L1347:
	movl	-56(%rbp), %edi
	addq	%rsi, %r13
	cmpl	%edi, %ecx
	jnb	.L1356
	movl	%edi, %r8d
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	24(%r14), %rax
.L1356:
	cmpq	%rax, %r13
	jb	.L1479
.L1357:
	leaq	.LC30(%rip), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$0, -60(%rbp)
.L1359:
	movq	208(%r14), %rcx
	leaq	_ZN2v88internal4wasm9StoreType10kValueTypeE(%rip), %rax
	movq	240(%r14), %rdx
	movabsq	$-6148914691236517205, %rdi
	movzbl	(%rax,%rbx), %r13d
	movq	%rcx, %rax
	subq	200(%r14), %rax
	movl	-132(%rdx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L1368
	cmpb	$2, -120(%rdx)
	movq	16(%r14), %rsi
	jne	.L1480
.L1369:
	movq	$0, -72(%rbp)
	movl	$10, %eax
	cmpb	$10, %r13b
	jne	.L1374
.L1474:
	movq	240(%r14), %rdx
.L1376:
	movq	208(%r14), %rcx
	movl	-132(%rdx), %esi
	movabsq	$-6148914691236517205, %rdi
	movq	%rcx, %rax
	subq	200(%r14), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L1408
	cmpb	$2, -120(%rdx)
	jne	.L1481
.L1409:
	xorl	%r15d, %r15d
.L1413:
	cmpq	$0, 56(%r14)
	jne	.L1431
	movq	240(%r14), %rax
	cmpb	$0, -120(%rax)
	je	.L1482
.L1431:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1346:
	.cfi_restore_state
	leaq	.LC28(%rip), %rcx
	movq	%r15, %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
.L1355:
	movq	24(%r14), %rax
	movq	%r15, %r13
	movl	$0, -56(%rbp)
	cmpq	%rax, %r13
	jnb	.L1357
.L1479:
	movzbl	0(%r13), %edx
	movl	%edx, %edi
	andl	$127, %edi
	movl	%edi, -60(%rbp)
	testb	%dl, %dl
	js	.L1358
	addl	$1, %r12d
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1408:
	movzbl	-16(%rcx), %eax
	leaq	-24(%rcx), %rdx
	movq	-24(%rcx), %rsi
	movq	-8(%rcx), %r15
	movq	%rdx, 208(%r14)
	cmpb	$10, %al
	je	.L1413
	cmpb	$1, %al
	je	.L1413
	cmpb	$9, %al
	ja	.L1414
	leaq	.L1416(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi,"a",@progbits
	.align 4
	.align 4
.L1416:
	.long	.L1424-.L1416
	.long	.L1414-.L1416
	.long	.L1423-.L1416
	.long	.L1447-.L1416
	.long	.L1421-.L1416
	.long	.L1420-.L1416
	.long	.L1419-.L1416
	.long	.L1418-.L1416
	.long	.L1417-.L1416
	.long	.L1415-.L1416
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
.L1447:
	leaq	.LC9(%rip), %rdx
.L1422:
	movq	24(%r14), %rax
	leaq	.LC0(%rip), %rcx
	movq	%rcx, -80(%rbp)
	cmpq	%rax, %rsi
	jb	.L1483
.L1425:
	movq	16(%r14), %rcx
	cmpq	%rax, %rcx
	jb	.L1427
.L1430:
	leaq	.LC0(%rip), %rcx
.L1428:
	pushq	%rdx
	movq	%r14, %rdi
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	pushq	-80(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rsi
	popq	%rdi
	jmp	.L1413
.L1423:
	leaq	.LC17(%rip), %rdx
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	-8(%rcx), %rdi
	movzbl	-16(%rcx), %eax
	subq	$24, %rcx
	movq	(%rcx), %rsi
	movq	%rcx, 208(%r14)
	movq	%rdi, -72(%rbp)
	cmpb	%r13b, %al
	je	.L1376
	leal	-7(%rax), %ecx
	cmpb	$2, %cl
	ja	.L1374
	cmpb	$6, %r13b
	je	.L1376
	.p2align 4,,10
	.p2align 3
.L1374:
	leal	-7(%r13), %edx
	andl	$253, %edx
	sete	%dl
	cmpb	$8, %al
	sete	%cl
	andl	%ecx, %edx
	xorl	$1, %edx
	cmpb	$10, %al
	setne	%cl
	testb	%cl, %dl
	je	.L1474
	cmpb	$10, %r13b
	je	.L1474
	cmpb	$9, %al
	ja	.L1378
	leaq	.L1380(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	.align 4
	.align 4
.L1380:
	.long	.L1389-.L1380
	.long	.L1442-.L1380
	.long	.L1387-.L1380
	.long	.L1386-.L1380
	.long	.L1385-.L1380
	.long	.L1384-.L1380
	.long	.L1383-.L1380
	.long	.L1382-.L1380
	.long	.L1381-.L1380
	.long	.L1379-.L1380
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	.p2align 4,,10
	.p2align 3
.L1478:
	leaq	2(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1348
	movzbl	2(%r13), %edi
	movl	$3, %esi
	movl	$2, %r12d
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r10d
	movl	%r10d, -56(%rbp)
	testb	%dil, %dil
	jns	.L1347
	leaq	3(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1348
	movzbl	3(%r13), %edi
	movl	$4, %esi
	movl	$3, %r12d
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r10d
	movl	%r10d, -56(%rbp)
	testb	%dil, %dil
	jns	.L1347
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1348
	movzbl	4(%r13), %edi
	movl	$5, %esi
	movl	$4, %r12d
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r10d
	movl	%r10d, -56(%rbp)
	testb	%dil, %dil
	jns	.L1347
	leaq	5(%r13), %r12
	cmpq	%r12, %rax
	ja	.L1484
	movq	%r12, %rsi
	leaq	.LC28(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movl	$4, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1354:
	movl	%r12d, %eax
	movl	$0, -56(%rbp)
	leaq	1(%r13,%rax), %r13
	movq	24(%r14), %rax
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1358:
	leaq	1(%r13), %rsi
	cmpq	%rax, %rsi
	jnb	.L1360
	movzbl	1(%r13), %ecx
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, -60(%rbp)
	testb	%cl, %cl
	js	.L1361
	addl	$2, %r12d
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1477:
	leaq	-1(%r13), %rsi
	leaq	.LC31(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1348:
	.cfi_restore_state
	leaq	.LC28(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	%rsi, -56(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-56(%rbp), %rsi
	movq	%rsi, %r15
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1360:
	leaq	.LC30(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$1, %r12d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -60(%rbp)
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1483:
	movzbl	(%rsi), %edi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movq	-96(%rbp), %rsi
	testb	%al, %al
	movq	-104(%rbp), %rdx
	je	.L1485
	movq	24(%r14), %rax
	leaq	1(%rsi), %rcx
	cmpq	%rcx, %rax
	jbe	.L1425
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	24(%r14), %rax
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1427:
	movzbl	(%rcx), %edi
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movq	-96(%rbp), %rcx
	testb	%al, %al
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdx
	je	.L1486
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r14)
	jbe	.L1430
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1482:
	leaq	_ZN2v88internal4wasm9StoreType7kMemRepE(%rip), %rax
	pushq	%r13
	movl	-60(%rbp), %ecx
	movq	%r15, %rdx
	movzbl	(%rax,%rbx), %esi
	movq	16(%r14), %rax
	subq	8(%r14), %rax
	movq	144(%r14), %rdi
	movq	-72(%rbp), %r9
	movl	-56(%rbp), %r8d
	pushq	%rax
	call	_ZN2v88internal8compiler16WasmGraphBuilder8StoreMemENS0_21MachineRepresentationEPNS1_4NodeEjjS5_iNS0_4wasm9ValueTypeE@PLT
	movq	%rax, %rdx
	popq	%rax
	popq	%rcx
	testq	%rdx, %rdx
	je	.L1431
	cmpl	$-1, 152(%r14)
	je	.L1431
	leaq	136(%r14), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1480:
	leaq	.LC0(%rip), %r15
	cmpq	%rsi, 24(%r14)
	jbe	.L1370
	movzbl	(%rsi), %edi
	movq	%rsi, -80(%rbp)
	movl	%edi, -72(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-72(%rbp), %edi
	movq	-80(%rbp), %rsi
	testb	%al, %al
	je	.L1472
	leaq	1(%rsi), %rax
	cmpq	%rax, 24(%r14)
	ja	.L1372
.L1473:
	movq	16(%r14), %rsi
.L1370:
	movq	%r15, %rcx
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r14), %rsi
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	16(%r14), %r15
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r14), %r15
	jnb	.L1410
	movzbl	(%r15), %edi
	movq	%rcx, -88(%rbp)
	movl	%edi, -80(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-80(%rbp), %edi
	movq	-88(%rbp), %rcx
	testb	%al, %al
	je	.L1475
	leaq	1(%r15), %rax
	cmpq	%rax, 24(%r14)
	ja	.L1412
.L1476:
	movq	16(%r14), %r15
.L1410:
	leaq	.LC18(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1361:
	leaq	2(%r13), %rsi
	cmpq	%rax, %rsi
	jnb	.L1362
	movzbl	2(%r13), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, -60(%rbp)
	testb	%cl, %cl
	js	.L1363
	addl	$3, %r12d
	jmp	.L1359
.L1417:
	leaq	.LC13(%rip), %rdx
	jmp	.L1422
.L1414:
	leaq	.LC7(%rip), %rdx
	jmp	.L1422
.L1424:
	leaq	.LC16(%rip), %rdx
	jmp	.L1422
.L1415:
	leaq	.LC14(%rip), %rdx
	jmp	.L1422
.L1421:
	leaq	.LC10(%rip), %rdx
	jmp	.L1422
.L1420:
	leaq	.LC15(%rip), %rdx
	jmp	.L1422
.L1419:
	leaq	.LC11(%rip), %rdx
	jmp	.L1422
.L1418:
	leaq	.LC12(%rip), %rdx
	jmp	.L1422
.L1387:
	leaq	.LC17(%rip), %r15
.L1388:
	movq	24(%r14), %rdi
	leaq	.LC0(%rip), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rsi, %rdi
	jbe	.L1390
	movzbl	(%rsi), %r8d
	movq	%rsi, -96(%rbp)
	movl	%r8d, %edi
	movl	%r8d, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %r8d
	movq	-96(%rbp), %rsi
	testb	%al, %al
	je	.L1487
	movq	24(%r14), %rdi
	leaq	1(%rsi), %rax
	cmpq	%rax, %rdi
	jbe	.L1390
	movl	%r8d, %edi
	movzbl	1(%rsi), %r8d
	movq	%rsi, -88(%rbp)
	sall	$8, %edi
	orl	%r8d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r14), %rdi
	movq	-88(%rbp), %rsi
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L1390:
	cmpb	$9, %r13b
	ja	.L1392
	leaq	.L1394(%rip), %r8
	movzbl	%r13b, %ecx
	movslq	(%r8,%rcx,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	.align 4
	.align 4
.L1394:
	.long	.L1403-.L1394
	.long	.L1445-.L1394
	.long	.L1401-.L1394
	.long	.L1400-.L1394
	.long	.L1399-.L1394
	.long	.L1398-.L1394
	.long	.L1397-.L1394
	.long	.L1396-.L1394
	.long	.L1395-.L1394
	.long	.L1393-.L1394
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
.L1442:
	leaq	.LC8(%rip), %r15
	jmp	.L1388
.L1389:
	leaq	.LC16(%rip), %r15
	jmp	.L1388
.L1379:
	leaq	.LC14(%rip), %r15
	jmp	.L1388
.L1381:
	leaq	.LC13(%rip), %r15
	jmp	.L1388
.L1382:
	leaq	.LC12(%rip), %r15
	jmp	.L1388
.L1383:
	leaq	.LC11(%rip), %r15
	jmp	.L1388
.L1384:
	leaq	.LC15(%rip), %r15
	jmp	.L1388
.L1385:
	leaq	.LC10(%rip), %r15
	jmp	.L1388
.L1386:
	leaq	.LC9(%rip), %r15
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1362:
	leaq	.LC30(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$2, %r12d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -60(%rbp)
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1412:
	movzbl	1(%r15), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1475:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rcx
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1372:
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	orl	%eax, %edi
.L1472:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r15
	jmp	.L1473
.L1401:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	16(%r14), %rcx
	cmpq	%rdi, %rcx
	jnb	.L1407
	movzbl	(%rcx), %edi
	movq	%rsi, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%edi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-88(%rbp), %edi
	movq	-96(%rbp), %rcx
	testb	%al, %al
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %rsi
	je	.L1488
	leaq	1(%rcx), %rax
	cmpq	%rax, 24(%r14)
	jbe	.L1407
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%rsi, -96(%rbp)
	movq	%r9, -88(%rbp)
	orl	%eax, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r9
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1405:
	pushq	%r15
	movl	$1, %r8d
	movq	%r14, %rdi
	xorl	%eax, %eax
	pushq	-80(%rbp)
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r8
	popq	%r9
	jmp	.L1474
.L1445:
	leaq	.LC8(%rip), %r9
	jmp	.L1402
.L1403:
	leaq	.LC16(%rip), %r9
	jmp	.L1402
.L1400:
	leaq	.LC9(%rip), %r9
	jmp	.L1402
.L1398:
	leaq	.LC15(%rip), %r9
	jmp	.L1402
.L1399:
	leaq	.LC10(%rip), %r9
	jmp	.L1402
.L1395:
	leaq	.LC13(%rip), %r9
	jmp	.L1402
.L1396:
	leaq	.LC12(%rip), %r9
	jmp	.L1402
.L1397:
	leaq	.LC11(%rip), %r9
	jmp	.L1402
.L1393:
	leaq	.LC14(%rip), %r9
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1407:
	leaq	.LC0(%rip), %rcx
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	24(%r14), %rax
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	%rsi, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1487:
	movl	%r8d, %edi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	24(%r14), %rdi
	movq	-88(%rbp), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1363:
	leaq	3(%r13), %rsi
	cmpq	%rax, %rsi
	jb	.L1489
	leaq	.LC30(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$3, %r12d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -60(%rbp)
	jmp	.L1359
.L1392:
	leaq	.LC7(%rip), %r9
	jmp	.L1402
.L1378:
	leaq	.LC7(%rip), %r15
	jmp	.L1388
.L1489:
	movzbl	3(%r13), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, -60(%rbp)
	testb	%cl, %cl
	js	.L1365
	addl	$4, %r12d
	jmp	.L1359
.L1365:
	leaq	4(%r13), %r15
	cmpq	%rax, %r15
	jnb	.L1438
	movzbl	4(%r13), %r13d
	testb	%r13b, %r13b
	js	.L1439
	movl	%r13d, %eax
	movl	$5, %r8d
	sall	$28, %eax
	orl	%eax, -60(%rbp)
.L1367:
	addl	%r8d, %r12d
	andl	$240, %r13d
	je	.L1359
	leaq	.LC27(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$0, -60(%rbp)
	jmp	.L1359
.L1484:
	movzbl	5(%r13), %edx
	movl	%edx, %r8d
	andl	$-16, %r8d
	testb	%dl, %dl
	js	.L1490
	sall	$28, %edx
	orl	%edx, -56(%rbp)
	testb	%r8b, %r8b
	je	.L1437
.L1353:
	movq	%r12, %rsi
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rdi
	movl	$5, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	leaq	6(%r13), %r15
	jmp	.L1355
.L1438:
	xorl	%r13d, %r13d
	movl	$4, %r8d
.L1366:
	leaq	.LC30(%rip), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -60(%rbp)
	movl	-72(%rbp), %r8d
	jmp	.L1367
.L1439:
	movl	$5, %r8d
	jmp	.L1366
.L1490:
	leaq	.LC28(%rip), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movb	%r8b, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-56(%rbp), %r8d
	testb	%r8b, %r8b
	jne	.L1353
	movl	$5, %r12d
	jmp	.L1354
.L1437:
	movl	$6, %esi
	movl	$5, %r12d
	jmp	.L1347
	.cfi_endproc
.LFE23685:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKc:
.LFB6907:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rdx
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	subq	8(%rdi), %rsi
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE6907:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKc
	.section	.text._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	.type	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE, @function
_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE:
.LFB6970:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edx
	cmpb	$10, %dil
	ja	.L1493
	leaq	.L1495(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L1495:
	.long	.L1505-.L1495
	.long	.L1506-.L1495
	.long	.L1503-.L1495
	.long	.L1502-.L1495
	.long	.L1501-.L1495
	.long	.L1500-.L1495
	.long	.L1499-.L1495
	.long	.L1498-.L1495
	.long	.L1497-.L1495
	.long	.L1496-.L1495
	.long	.L1494-.L1495
	.section	.text._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L1503:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1506:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1502:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1501:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1505:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1496:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1494:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1499:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	leaq	.LC13(%rip), %rax
	ret
.L1493:
	leaq	.LC7(%rip), %rax
	ret
	.cfi_endproc
.LFE6970:
	.size	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE, .-_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	.section	.rodata._ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv.str1.1,"aMS",@progbits,1
.LC32:
	.string	"branch table entry"
	.section	.text._ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv,"axG",@progbits,_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv
	.type	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv, @function
_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv:
.LFB21697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	addl	$1, 24(%rdi)
	movq	(%rdi), %rdi
	movq	16(%rbx), %rsi
	movq	24(%rdi), %rax
	cmpq	%rax, %rsi
	jb	.L1523
	leaq	.LC32(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %r12
	xorl	%r9d, %r9d
.L1509:
	movq	%r12, 16(%rbx)
	addq	$16, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1523:
	.cfi_restore_state
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r12
	movl	%edx, %r9d
	andl	$127, %r9d
	testb	%dl, %dl
	jns	.L1509
	cmpq	%r12, %rax
	jbe	.L1510
	movzbl	1(%rsi), %ecx
	leaq	2(%rsi), %r12
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L1509
	leaq	2(%rsi), %r8
	cmpq	%r8, %rax
	jbe	.L1512
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r12
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L1509
	leaq	3(%rsi), %r8
	cmpq	%r8, %rax
	ja	.L1524
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %rax
	xorl	%r9d, %r9d
	leaq	3(%rax), %r12
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	%r12, %rsi
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %rax
	xorl	%r9d, %r9d
	leaq	1(%rax), %r12
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1512:
	xorl	%eax, %eax
	leaq	.LC32(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %rax
	xorl	%r9d, %r9d
	leaq	2(%rax), %r12
	jmp	.L1509
.L1524:
	movzbl	3(%rsi), %ecx
	leaq	4(%rsi), %r12
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r9d
	testb	%cl, %cl
	jns	.L1509
	leaq	4(%rsi), %r13
	cmpq	%r13, %rax
	jbe	.L1519
	movzbl	4(%rsi), %r14d
	testb	%r14b, %r14b
	js	.L1520
	movl	%r14d, %eax
	movl	$5, %r12d
	sall	$28, %eax
	orl	%eax, %r9d
.L1517:
	andl	$240, %r14d
	jne	.L1518
	addq	16(%rbx), %r12
	jmp	.L1509
.L1519:
	xorl	%r14d, %r14d
	movl	$4, %r12d
.L1516:
	leaq	.LC32(%rip), %rcx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%rdi, -40(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-40(%rbp), %rdi
	xorl	%r9d, %r9d
	jmp	.L1517
.L1518:
	leaq	.LC27(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	addq	16(%rbx), %r12
	xorl	%r9d, %r9d
	jmp	.L1509
.L1520:
	movl	$5, %r12d
	jmp	.L1516
	.cfi_endproc
.LFE21697:
	.size	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv, .-_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv
	.section	.rodata._ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh.str1.1,"aMS",@progbits,1
.LC33:
	.string	"signature index"
.LC34:
	.string	"table index"
	.section	.rodata._ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"expected table index 0, found %u"
	.section	.text._ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC5ENS1_12WasmFeaturesEPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh
	.type	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh, @function
_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh:
.LFB23453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	1(%r8), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	24(%rcx), %rax
	movq	%rsi, -64(%rbp)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	cmpq	%rax, %r12
	jb	.L1572
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC33(%rip), %rcx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
.L1528:
	movl	%edx, 4(%rbx)
	cmpq	%r12, %rax
	ja	.L1573
	leaq	.LC34(%rip), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1571:
	xorl	%r14d, %r14d
.L1556:
	cmpb	$0, -57(%rbp)
	je	.L1574
.L1557:
	movl	%r14d, (%rbx)
	movl	%r15d, 16(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1573:
	.cfi_restore_state
	movzbl	(%r12), %edx
	movl	%edx, %ecx
	movl	%edx, %r14d
	andl	$127, %ecx
	andl	$127, %r14d
	testb	%dl, %dl
	js	.L1575
	addl	$1, %r15d
	testb	%cl, %cl
	jne	.L1556
	xorl	%r14d, %r14d
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1572:
	movzbl	1(%r8), %ecx
	leaq	2(%r8), %r12
	movl	$1, %r15d
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	jns	.L1528
	cmpq	%r12, %rax
	jbe	.L1529
	movzbl	2(%r8), %esi
	leaq	3(%r8), %r12
	movl	$2, %r15d
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L1528
	cmpq	%r12, %rax
	jbe	.L1531
	movzbl	3(%r8), %esi
	leaq	4(%r8), %r12
	movl	$3, %r15d
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L1528
	cmpq	%r12, %rax
	ja	.L1576
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC33(%rip), %rcx
	movl	$3, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1575:
	leaq	1(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1540
	movzbl	1(%r12), %ecx
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1541
	movl	$2, %eax
	testl	%r14d, %r14d
	je	.L1543
.L1542:
	addl	%eax, %r15d
	cmpb	$0, -57(%rbp)
	jne	.L1557
.L1574:
	movl	%r14d, %ecx
	leaq	.LC35(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1540:
	leaq	.LC34(%rip), %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$1, %r15d
	leaq	.LC26(%rip), %rdx
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1529:
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC33(%rip), %rcx
	movl	$1, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1541:
	leaq	2(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1544
	movzbl	2(%r12), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1545
	movl	$3, %eax
	testl	%r14d, %r14d
	jne	.L1542
.L1543:
	addl	%eax, %r15d
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1531:
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC33(%rip), %rcx
	movl	$2, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1544:
	leaq	.LC34(%rip), %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$2, %r15d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1545:
	leaq	3(%r12), %rsi
	cmpq	%rax, %rsi
	jb	.L1577
	leaq	.LC34(%rip), %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	addl	$3, %r15d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1571
.L1576:
	movzbl	4(%r8), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1534
	leaq	5(%r8), %r12
	movl	$4, %r15d
	jmp	.L1528
.L1577:
	movzbl	3(%r12), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1547
	movl	$4, %eax
	testl	%r14d, %r14d
	jne	.L1542
	jmp	.L1543
.L1534:
	leaq	5(%r8), %r14
	cmpq	%r14, %rax
	jbe	.L1558
	movzbl	5(%r8), %r9d
	testb	%r9b, %r9b
	js	.L1559
	movl	%r9d, %eax
	movl	$6, %r12d
	movl	$5, %r15d
	sall	$28, %eax
	orl	%eax, %edx
.L1536:
	addq	%r8, %r12
	andl	$240, %r9d
	jne	.L1537
	movq	24(%r13), %rax
	jmp	.L1528
.L1547:
	leaq	4(%r12), %rsi
	cmpq	%rax, %rsi
	setnb	%dl
	jb	.L1578
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$4, %eax
	jmp	.L1543
.L1558:
	xorl	%r9d, %r9d
	movl	$4, %r15d
.L1535:
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC33(%rip), %rcx
	movl	%r15d, %r12d
	movq	%r8, -80(%rbp)
	movb	%r9b, -72(%rbp)
	addq	$1, %r12
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-80(%rbp), %r8
	movzbl	-72(%rbp), %r9d
	xorl	%edx, %edx
	jmp	.L1536
.L1578:
	movzbl	4(%r12), %eax
	movl	%eax, %ecx
	movl	%eax, %r8d
	sall	$28, %ecx
	andl	$-16, %r8d
	orl	%ecx, %r14d
	testb	%al, %al
	js	.L1564
	testb	%dl, %dl
	jne	.L1564
	testb	%r8b, %r8b
	je	.L1579
.L1555:
	leaq	.LC27(%rip), %rdx
	movq	%r13, %rdi
	addl	$5, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1571
.L1537:
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r13), %rax
	xorl	%edx, %edx
	jmp	.L1528
.L1559:
	movl	$5, %r15d
	jmp	.L1535
.L1564:
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rcx
	movq	%r13, %rdi
	movb	%r8b, -80(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-80(%rbp), %r8d
	movq	-72(%rbp), %rsi
	testb	%r8b, %r8b
	jne	.L1555
	movl	$5, %eax
	jmp	.L1543
.L1579:
	movl	$5, %eax
	testl	%r14d, %r14d
	jne	.L1542
	jmp	.L1543
	.cfi_endproc
.LFE23453:
	.size	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh, .-_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh
	.weak	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	.set	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh,_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC2ENS1_12WasmFeaturesEPS3_PKh
	.section	.rodata._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh.str1.1,"aMS",@progbits,1
.LC36:
	.string	"block type"
.LC37:
	.string	"invalid block type"
.LC38:
	.string	"block arity"
.LC39:
	.string	"invalid block type index"
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.type	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh, @function
_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh:
.LFB23459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	1(%rcx), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdx), %rax
	movb	$0, 4(%rdi)
	movl	$1, (%rdi)
	movl	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	cmpq	%rax, %r14
	ja	.L1581
	cmpl	%r14d, %eax
	je	.L1581
	movzbl	1(%rcx), %eax
	subl	$64, %eax
	cmpb	$63, %al
	ja	.L1582
	leaq	.L1584(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 4
	.align 4
.L1584:
	.long	.L1580-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1591-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1590-.L1584
	.long	.L1589-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1582-.L1584
	.long	.L1588-.L1584
	.long	.L1587-.L1584
	.long	.L1586-.L1584
	.long	.L1585-.L1584
	.long	.L1583-.L1584
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.p2align 4,,10
	.p2align 3
.L1581:
	leaq	.LC36(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L1582:
	cmpb	$0, (%r15)
	movb	$10, 4(%rbx)
	leaq	.LC37(%rip), %rdx
	je	.L1619
	cmpq	$0, 56(%r12)
	je	.L1620
.L1580:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1583:
	.cfi_restore_state
	movb	$1, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1585:
	.cfi_restore_state
	movb	$2, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1586:
	.cfi_restore_state
	movb	$3, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1587:
	.cfi_restore_state
	movb	$4, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1588:
	.cfi_restore_state
	movb	$5, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1589:
	.cfi_restore_state
	movb	$7, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	.cfi_restore_state
	movb	$6, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1591:
	.cfi_restore_state
	movb	$9, 4(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1620:
	.cfi_restore_state
	movq	24(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L1621
	movl	$0, (%rbx)
	leaq	.LC38(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rsi
.L1618:
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	(%rbx), %eax
	testl	%eax, %eax
	sete	%al
	xorl	%ecx, %ecx
.L1607:
	testb	%al, %al
	jne	.L1609
.L1608:
	movl	%ecx, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1628:
	.cfi_restore_state
	leaq	5(%r13), %r15
	cmpq	%r15, %rcx
	jbe	.L1602
	movzbl	5(%r13), %r13d
	movl	$5, (%rbx)
	testb	%r13b, %r13b
	js	.L1603
	movl	%r13d, %ecx
	sall	$28, %ecx
	orl	%eax, %ecx
.L1604:
	andb	$-8, %r13b
	je	.L1615
	cmpb	$120, %r13b
	je	.L1615
	leaq	.LC27(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%ecx, %ecx
	cmpl	$0, (%rbx)
	jne	.L1608
	.p2align 4,,10
	.p2align 3
.L1609:
	leaq	.LC39(%rip), %rdx
.L1619:
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.p2align 4,,10
	.p2align 3
.L1621:
	.cfi_restore_state
	movzbl	1(%r13), %edx
	movl	%edx, %eax
	andl	$127, %eax
	testb	%dl, %dl
	js	.L1622
	sall	$25, %eax
	movl	$1, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$25, %ecx
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1622:
	leaq	2(%r13), %rsi
	cmpq	%rsi, %rcx
	ja	.L1623
	movl	$1, (%rbx)
.L1617:
	leaq	.LC38(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1623:
	movzbl	2(%r13), %esi
	movzbl	%al, %eax
	movl	%esi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1624
	sall	$18, %eax
	movl	$2, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$18, %ecx
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1624:
	leaq	3(%r13), %rsi
	cmpq	%rsi, %rcx
	ja	.L1625
	movl	$2, (%rbx)
	jmp	.L1617
.L1625:
	movzbl	3(%r13), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1626
	sall	$11, %eax
	movl	$3, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$11, %ecx
	jmp	.L1607
.L1626:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rcx
	ja	.L1627
	movl	$3, (%rbx)
	jmp	.L1617
.L1627:
	movzbl	4(%r13), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L1628
	sall	$4, %eax
	movl	$4, (%rbx)
	movl	%eax, %ecx
	shrl	$31, %eax
	sarl	$4, %ecx
	jmp	.L1607
.L1602:
	movl	$4, (%rbx)
	xorl	%r13d, %r13d
.L1603:
	leaq	.LC38(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%ecx, %ecx
	jmp	.L1604
.L1615:
	cmpl	$0, (%rbx)
	movl	%ecx, %edx
	sete	%al
	shrl	$31, %edx
	orl	%edx, %eax
	jmp	.L1607
	.cfi_endproc
.LFE23459:
	.size	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh, .-_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	.set	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.section	.rodata._ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh.str1.1,"aMS",@progbits,1
.LC41:
	.string	"elem segment index"
	.section	.text._ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.type	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, @function
_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh:
.LFB23505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	2(%rdx), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movdqa	.LC40(%rip), %xmm0
	movq	24(%rsi), %rax
	movups	%xmm0, (%rdi)
	cmpq	%rax, %r12
	jb	.L1668
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC41(%rip), %rcx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	xorl	%edx, %edx
.L1642:
	movl	%edx, (%rbx)
	cmpq	%rax, %r12
	jb	.L1669
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rcx
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
.L1645:
	movl	%r12d, 8(%rbx)
	movl	%r15d, 12(%rbx)
	movl	%edx, 4(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1669:
	.cfi_restore_state
	movzbl	(%r12), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L1644
	addl	$1, %r15d
	movl	$1, %r12d
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1644:
	leaq	1(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1646
	movzbl	1(%r12), %esi
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1647
	addl	$2, %r15d
	movl	$2, %r12d
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1668:
	movzbl	2(%rdx), %ecx
	movq	%rdx, %r13
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L1670
	sall	$25, %edx
	leaq	3(%r13), %r12
	movl	$1, %r15d
	sarl	$25, %edx
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1670:
	leaq	3(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1632
	movzbl	3(%r13), %esi
	movzbl	%dl, %edx
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1671
	sall	$18, %edx
	movl	$4, %r12d
	movl	$2, %r15d
	sarl	$18, %edx
.L1656:
	addq	%r13, %r12
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1646:
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$1, %r15d
	leaq	.LC34(%rip), %rcx
	movl	$1, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1632:
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC41(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$3, %r12d
	movq	24(%r14), %rax
	xorl	%edx, %edx
	movl	$1, %r15d
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1647:
	leaq	2(%r12), %rsi
	cmpq	%rax, %rsi
	jnb	.L1648
	movzbl	2(%r12), %esi
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1649
	addl	$3, %r15d
	movl	$3, %r12d
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1671:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1634
	movzbl	4(%r13), %esi
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1672
	sall	$11, %edx
	movl	$5, %r12d
	movl	$3, %r15d
	sarl	$11, %edx
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1648:
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$2, %r15d
	leaq	.LC34(%rip), %rcx
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1634:
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC41(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$4, %r12d
	movq	24(%r14), %rax
	xorl	%edx, %edx
	movl	$2, %r15d
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1649:
	leaq	3(%r12), %rsi
	cmpq	%rax, %rsi
	jb	.L1673
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$3, %r15d
	leaq	.LC34(%rip), %rcx
	movl	$3, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1672:
	leaq	5(%r13), %rsi
	cmpq	%rsi, %rax
	ja	.L1674
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC41(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$5, %r12d
	movq	24(%r14), %rax
	xorl	%edx, %edx
	movl	$3, %r15d
	jmp	.L1656
.L1673:
	movzbl	3(%r12), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1651
	addl	$4, %r15d
	movl	$4, %r12d
	jmp	.L1645
.L1674:
	movzbl	5(%r13), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L1675
	sall	$4, %edx
	movl	$6, %r12d
	movl	$4, %r15d
	sarl	$4, %edx
	jmp	.L1656
.L1651:
	leaq	4(%r12), %r13
	cmpq	%rax, %r13
	jnb	.L1660
	movzbl	4(%r12), %r8d
	testb	%r8b, %r8b
	js	.L1661
	movl	%r8d, %eax
	movl	$5, %r12d
	sall	$28, %eax
	orl	%eax, %edx
.L1653:
	addl	%r12d, %r15d
	andl	$240, %r8d
	je	.L1645
	leaq	.LC27(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%edx, %edx
	jmp	.L1645
.L1675:
	leaq	6(%r13), %rsi
	cmpq	%rsi, %rax
	jbe	.L1658
	movzbl	6(%r13), %r8d
	testb	%r8b, %r8b
	js	.L1659
	movl	%r8d, %eax
	movl	$7, %r12d
	movl	$5, %r15d
	sall	$28, %eax
	orl	%eax, %edx
.L1639:
	addq	%r13, %r12
	andl	$248, %r8d
	je	.L1662
	cmpb	$120, %r8b
	je	.L1662
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r14), %rax
	xorl	%edx, %edx
	jmp	.L1642
.L1660:
	xorl	%r8d, %r8d
	movl	$4, %r12d
.L1652:
	leaq	.LC26(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rcx
	movb	%r8b, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-56(%rbp), %r8d
	xorl	%edx, %edx
	jmp	.L1653
.L1658:
	xorl	%r8d, %r8d
	movl	$4, %r15d
.L1638:
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%r15d, %r12d
	leaq	.LC41(%rip), %rcx
	movb	%r8b, -57(%rbp)
	addq	$2, %r12
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-57(%rbp), %r8d
	movq	-56(%rbp), %rsi
	xorl	%edx, %edx
	jmp	.L1639
.L1662:
	movq	24(%r14), %rax
	jmp	.L1642
.L1659:
	movl	$5, %r15d
	jmp	.L1638
.L1661:
	movl	$5, %r12d
	jmp	.L1652
	.cfi_endproc
.LFE23505:
	.size	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, .-_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.weak	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	.set	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh,_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.section	.text._ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC5EPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.type	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, @function
_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh:
.LFB23511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	2(%rdx), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movdqa	.LC42(%rip), %xmm0
	movl	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	24(%r14), %rax
	cmpq	%rax, %rsi
	jb	.L1707
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	leaq	.LC34(%rip), %rcx
	movl	$2, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	xorl	%edx, %edx
.L1678:
	leaq	(%r12,%r13), %rsi
	movl	%edx, (%rbx)
	movl	%r15d, 4(%rbx)
	cmpq	%rax, %rsi
	jb	.L1708
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	leaq	.LC34(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
.L1687:
	movl	%r12d, 12(%rbx)
	movl	%edx, 8(%rbx)
	movl	%eax, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1708:
	.cfi_restore_state
	movzbl	(%rsi), %ecx
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L1686
	leal	1(%r15), %eax
	movl	$1, %r12d
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1707:
	movzbl	2(%rdx), %ecx
	movl	$3, %r13d
	movl	$1, %r15d
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	jns	.L1678
	leaq	3(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L1679
	movzbl	3(%r12), %esi
	movl	$4, %r13d
	movl	$2, %r15d
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L1678
	leaq	4(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L1679
	movzbl	4(%r12), %esi
	movl	$5, %r13d
	movl	$3, %r15d
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L1678
	leaq	5(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L1679
	movzbl	5(%r12), %esi
	movl	$6, %r13d
	movl	$4, %r15d
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%sil, %sil
	jns	.L1678
	leaq	6(%r12), %rsi
	cmpq	%rsi, %rax
	jbe	.L1701
	movzbl	6(%r12), %r8d
	testb	%r8b, %r8b
	js	.L1702
	movl	%r8d, %eax
	movl	$7, %r13d
	movl	$5, %r15d
	sall	$28, %eax
	orl	%eax, %edx
.L1683:
	andl	$240, %r8d
	jne	.L1684
	movq	24(%r14), %rax
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1686:
	leaq	1(%rsi), %r8
	cmpq	%rax, %r8
	jnb	.L1688
	movzbl	1(%rsi), %edi
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	js	.L1689
	leal	2(%r15), %eax
	movl	$2, %r12d
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1679:
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	xorl	%edx, %edx
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1688:
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, %rsi
	movq	%r14, %rdi
	leaq	.LC34(%rip), %rcx
	movl	$1, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1689:
	leaq	2(%rsi), %r8
	cmpq	%rax, %r8
	jnb	.L1690
	movzbl	2(%rsi), %edi
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	js	.L1691
	leal	3(%r15), %eax
	movl	$3, %r12d
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1690:
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, %rsi
	movq	%r14, %rdi
	leaq	.LC34(%rip), %rcx
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	$2, %eax
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1691:
	leaq	3(%rsi), %r8
	cmpq	%rax, %r8
	jb	.L1709
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, %rsi
	movq	%r14, %rdi
	leaq	.LC34(%rip), %rcx
	movl	$3, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	$3, %eax
	jmp	.L1687
.L1709:
	movzbl	3(%rsi), %edi
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	js	.L1693
	leal	4(%r15), %eax
	movl	$4, %r12d
	jmp	.L1687
.L1693:
	leaq	4(%rsi), %r13
	cmpq	%rax, %r13
	jnb	.L1703
	movzbl	4(%rsi), %r15d
	testb	%r15b, %r15b
	js	.L1704
	movl	%r15d, %eax
	movl	$5, %r12d
	sall	$28, %eax
	orl	%eax, %edx
.L1695:
	andl	$240, %r15d
	jne	.L1696
	movl	4(%rbx), %eax
	addl	%r12d, %eax
	jmp	.L1687
.L1701:
	xorl	%r8d, %r8d
.L1682:
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%r15d, %r13d
	leaq	.LC34(%rip), %rcx
	movb	%r8b, -57(%rbp)
	addq	$2, %r13
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-57(%rbp), %r8d
	movq	-56(%rbp), %rsi
	xorl	%edx, %edx
	jmp	.L1683
.L1703:
	xorl	%r15d, %r15d
	movl	$4, %r12d
.L1694:
	leaq	.LC26(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC34(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	jmp	.L1695
.L1684:
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r14), %rax
	xorl	%edx, %edx
	jmp	.L1678
.L1696:
	leaq	.LC27(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	4(%rbx), %eax
	xorl	%edx, %edx
	addl	%r12d, %eax
	jmp	.L1687
.L1704:
	movl	$5, %r12d
	jmp	.L1694
.L1702:
	movl	$5, %r15d
	jmp	.L1682
	.cfi_endproc
.LFE23511:
	.size	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh, .-_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.weak	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	.set	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh,_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKh
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB23870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L1728
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L1710:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1728:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movl	%r9d, %eax
	andl	$127, %eax
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L1729
	movl	$1, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1729:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L1713
	movzbl	1(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$7, %r8d
	andl	$16256, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L1730
	movl	$2, (%rdx)
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1713:
	movl	$1, (%rdx)
.L1727:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1730:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L1715
	movzbl	2(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$14, %r8d
	andl	$2080768, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L1731
	movl	$3, (%rdx)
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1715:
	movl	$2, (%rdx)
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1731:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L1732
	movl	$3, (%rdx)
	jmp	.L1727
.L1732:
	movzbl	3(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$21, %r8d
	andl	$266338304, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L1733
	movl	$4, (%rdx)
	jmp	.L1710
.L1733:
	leaq	4(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L1719
	movzbl	4(%rsi), %ebx
	movl	$5, (%rdx)
	testb	%bl, %bl
	js	.L1720
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, %eax
.L1721:
	andl	$240, %ebx
	je	.L1710
	leaq	.LC27(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1710
.L1719:
	movl	$4, (%rdx)
	xorl	%ebx, %ebx
.L1720:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L1721
	.cfi_endproc
.LFE23870:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.rodata._ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_.str1.1,"aMS",@progbits,1
.LC43:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.type	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, @function
_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_:
.LFB23904:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1866
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%rdx, %rax
	jb	.L1737
	movq	%rdi, %r15
	movzbl	(%rcx), %r14d
	subq	%rsi, %r15
	cmpq	%r15, %rdx
	jb	.L1870
	subq	%r15, %r12
	je	.L1777
	movq	%r12, %rdx
	movzbl	%r14b, %esi
	call	memset@PLT
	movq	%rax, %rdi
	addq	%rax, %r12
.L1745:
	movq	%r12, 16(%rbx)
	cmpq	%r13, %rdi
	je	.L1746
	movq	%r13, %rax
	movq	%rdi, %rcx
	notq	%rax
	subq	%r13, %rcx
	addq	%rdi, %rax
	cmpq	$14, %rax
	jbe	.L1747
	leaq	15(%r13), %rax
	subq	%r12, %rax
	cmpq	$30, %rax
	jbe	.L1747
	movq	%rcx, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L1748:
	movdqu	0(%r13,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1748
	movq	%rcx, %rdx
	andq	$-16, %rdx
	leaq	0(%r13,%rdx), %rax
	addq	%rdx, %r12
	cmpq	%rdx, %rcx
	je	.L1751
	movzbl	(%rax), %edx
	movb	%dl, (%r12)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	1(%rax), %edx
	movb	%dl, 1(%r12)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	2(%rax), %edx
	movb	%dl, 2(%r12)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	3(%rax), %edx
	movb	%dl, 3(%r12)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	4(%rax), %edx
	movb	%dl, 4(%r12)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	5(%rax), %edx
	movb	%dl, 5(%r12)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	6(%rax), %edx
	movb	%dl, 6(%r12)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	7(%rax), %edx
	movb	%dl, 7(%r12)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	8(%rax), %edx
	movb	%dl, 8(%r12)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	9(%rax), %edx
	movb	%dl, 9(%r12)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	10(%rax), %edx
	movb	%dl, 10(%r12)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	11(%rax), %edx
	movb	%dl, 11(%r12)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	12(%rax), %edx
	movb	%dl, 12(%r12)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	13(%rax), %edx
	movb	%dl, 13(%r12)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L1751
	movzbl	14(%rax), %eax
	movb	%al, 14(%r12)
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	%rdi, %rdx
	addq	%r15, 16(%rbx)
	movzbl	%r14b, %esi
	subq	%r13, %rdx
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1866:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1870:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdi, %rdx
	leaq	16(%rdi), %rax
	subq	%r12, %rdx
	cmpq	%rax, %rdx
	movl	$16, %eax
	setnb	%cl
	subq	%r12, %rax
	testq	%rax, %rax
	setle	%al
	orb	%al, %cl
	je	.L1776
	leaq	-1(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$14, %rax
	jbe	.L1776
	movq	%r12, %rcx
	xorl	%eax, %eax
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L1740:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1740
	movq	%r12, %rsi
	andq	$-16, %rsi
	leaq	(%rdx,%rsi), %rax
	leaq	(%rdi,%rsi), %rcx
	cmpq	%rsi, %r12
	je	.L1742
	movzbl	(%rax), %esi
	movb	%sil, (%rcx)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	1(%rax), %esi
	movb	%sil, 1(%rcx)
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	2(%rax), %esi
	movb	%sil, 2(%rcx)
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	3(%rax), %esi
	movb	%sil, 3(%rcx)
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	4(%rax), %esi
	movb	%sil, 4(%rcx)
	leaq	5(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	5(%rax), %esi
	movb	%sil, 5(%rcx)
	leaq	6(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	6(%rax), %esi
	movb	%sil, 6(%rcx)
	leaq	7(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	7(%rax), %esi
	movb	%sil, 7(%rcx)
	leaq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	8(%rax), %esi
	movb	%sil, 8(%rcx)
	leaq	9(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	9(%rax), %esi
	movb	%sil, 9(%rcx)
	leaq	10(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	10(%rax), %esi
	movb	%sil, 10(%rcx)
	leaq	11(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	11(%rax), %esi
	movb	%sil, 11(%rcx)
	leaq	12(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	12(%rax), %esi
	movb	%sil, 12(%rcx)
	leaq	13(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	13(%rax), %esi
	movb	%sil, 13(%rcx)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L1742
	movzbl	14(%rax), %eax
	movb	%al, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L1742:
	addq	%r12, 16(%rbx)
	subq	%r13, %rdx
	jne	.L1871
.L1743:
	leaq	0(%r13,%r12), %rax
	cmpq	%rax, %r13
	je	.L1734
	movzbl	%r14b, %esi
	movq	%r12, %rdx
.L1869:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L1737:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movl	$2147483647, %r14d
	movq	%r14, %rdx
	subq	%rax, %rdi
	subq	%rdi, %rdx
	cmpq	%rdx, %r12
	ja	.L1872
	cmpq	%rdi, %r12
	movq	%rdi, %rdx
	movq	%rsi, %r15
	cmovnb	%r12, %rdx
	addq	%rdx, %rdi
	setc	%dl
	subq	%rax, %r15
	movzbl	%dl, %edx
	testq	%rdx, %rdx
	jne	.L1778
	testq	%rdi, %rdi
	jne	.L1873
	xorl	%edx, %edx
	xorl	%eax, %eax
.L1756:
	leaq	(%rax,%r15), %rsi
	addq	%r12, %r15
	leaq	1(%rcx), %rdi
	addq	%rax, %r15
	cmpq	%r15, %rcx
	setnb	%r8b
	cmpq	%rdi, %rsi
	setnb	%dil
	orb	%dil, %r8b
	je	.L1759
	leaq	-1(%r12), %rdi
	cmpq	$14, %rdi
	jbe	.L1759
	movzbl	(%rcx), %edi
	leaq	-16(%r12), %r8
	shrq	$4, %r8
	movd	%edi, %xmm0
	addq	$1, %r8
	xorl	%edi, %edi
	punpcklbw	%xmm0, %xmm0
	punpcklwd	%xmm0, %xmm0
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1760:
	movq	%rdi, %r9
	addq	$1, %rdi
	salq	$4, %r9
	movups	%xmm0, (%rsi,%r9)
	cmpq	%rdi, %r8
	ja	.L1760
	salq	$4, %r8
	movq	%r12, %rdi
	subq	%r8, %rdi
	addq	%r8, %rsi
	cmpq	%r8, %r12
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rsi)
	cmpq	$1, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 1(%rsi)
	cmpq	$2, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 2(%rsi)
	cmpq	$3, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 3(%rsi)
	cmpq	$4, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 4(%rsi)
	cmpq	$5, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 5(%rsi)
	cmpq	$6, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 6(%rsi)
	cmpq	$7, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 7(%rsi)
	cmpq	$8, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 8(%rsi)
	cmpq	$9, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 9(%rsi)
	cmpq	$10, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 10(%rsi)
	cmpq	$11, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 11(%rsi)
	cmpq	$12, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 12(%rsi)
	cmpq	$13, %rdi
	je	.L1762
	movzbl	(%rcx), %r8d
	movb	%r8b, 13(%rsi)
	cmpq	$14, %rdi
	je	.L1762
	movzbl	(%rcx), %ecx
	movb	%cl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	8(%rbx), %rsi
	cmpq	%rsi, %r13
	je	.L1780
	leaq	-1(%r13), %rcx
	subq	%rsi, %rcx
	cmpq	$14, %rcx
	jbe	.L1765
	leaq	15(%rsi), %rcx
	subq	%rax, %rcx
	cmpq	$30, %rcx
	jbe	.L1765
	movq	%r13, %r9
	xorl	%ecx, %ecx
	subq	%rsi, %r9
	movq	%r9, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L1766:
	movdqu	(%rsi,%rcx), %xmm3
	movups	%xmm3, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L1766
	movq	%r9, %r8
	andq	$-16, %r8
	leaq	(%rsi,%r8), %rcx
	leaq	(%rax,%r8), %rdi
	cmpq	%r8, %r9
	je	.L1769
	movzbl	(%rcx), %r8d
	movb	%r8b, (%rdi)
	leaq	1(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	1(%rcx), %r8d
	movb	%r8b, 1(%rdi)
	leaq	2(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	2(%rcx), %r8d
	movb	%r8b, 2(%rdi)
	leaq	3(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	3(%rcx), %r8d
	movb	%r8b, 3(%rdi)
	leaq	4(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	4(%rcx), %r8d
	movb	%r8b, 4(%rdi)
	leaq	5(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	5(%rcx), %r8d
	movb	%r8b, 5(%rdi)
	leaq	6(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	6(%rcx), %r8d
	movb	%r8b, 6(%rdi)
	leaq	7(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	7(%rcx), %r8d
	movb	%r8b, 7(%rdi)
	leaq	8(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	8(%rcx), %r8d
	movb	%r8b, 8(%rdi)
	leaq	9(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	9(%rcx), %r8d
	movb	%r8b, 9(%rdi)
	leaq	10(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	10(%rcx), %r8d
	movb	%r8b, 10(%rdi)
	leaq	11(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	11(%rcx), %r8d
	movb	%r8b, 11(%rdi)
	leaq	12(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	12(%rcx), %r8d
	movb	%r8b, 12(%rdi)
	leaq	13(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	13(%rcx), %r8d
	movb	%r8b, 13(%rdi)
	leaq	14(%rcx), %r8
	cmpq	%r8, %r13
	je	.L1769
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L1769:
	movq	%r13, %rcx
	subq	%rsi, %rcx
	addq	%rax, %rcx
.L1764:
	movq	16(%rbx), %rdi
	leaq	(%rcx,%r12), %rsi
	cmpq	%rdi, %r13
	je	.L1770
	leaq	16(%rcx,%r12), %rcx
	cmpq	%rcx, %r13
	leaq	16(%r13), %rcx
	setnb	%r8b
	cmpq	%rcx, %rsi
	setnb	%cl
	orb	%cl, %r8b
	je	.L1771
	movq	%r13, %rcx
	notq	%rcx
	addq	%rdi, %rcx
	cmpq	$14, %rcx
	jbe	.L1771
	movq	%rdi, %r10
	xorl	%ecx, %ecx
	subq	%r13, %r10
	movq	%r10, %r8
	andq	$-16, %r8
	.p2align 4,,10
	.p2align 3
.L1772:
	movdqu	0(%r13,%rcx), %xmm4
	movups	%xmm4, (%rsi,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L1772
	movq	%r10, %r9
	andq	$-16, %r9
	leaq	0(%r13,%r9), %rcx
	leaq	(%rsi,%r9), %r8
	cmpq	%r9, %r10
	je	.L1775
	movzbl	(%rcx), %r9d
	movb	%r9b, (%r8)
	leaq	1(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	1(%rcx), %r9d
	movb	%r9b, 1(%r8)
	leaq	2(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	2(%rcx), %r9d
	movb	%r9b, 2(%r8)
	leaq	3(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	3(%rcx), %r9d
	movb	%r9b, 3(%r8)
	leaq	4(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	4(%rcx), %r9d
	movb	%r9b, 4(%r8)
	leaq	5(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	5(%rcx), %r9d
	movb	%r9b, 5(%r8)
	leaq	6(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	6(%rcx), %r9d
	movb	%r9b, 6(%r8)
	leaq	7(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	7(%rcx), %r9d
	movb	%r9b, 7(%r8)
	leaq	8(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	8(%rcx), %r9d
	movb	%r9b, 8(%r8)
	leaq	9(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	9(%rcx), %r9d
	movb	%r9b, 9(%r8)
	leaq	10(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	10(%rcx), %r9d
	movb	%r9b, 10(%r8)
	leaq	11(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	11(%rcx), %r9d
	movb	%r9b, 11(%r8)
	leaq	12(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	12(%rcx), %r9d
	movb	%r9b, 12(%r8)
	leaq	13(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	13(%rcx), %r9d
	movb	%r9b, 13(%r8)
	leaq	14(%rcx), %r9
	cmpq	%r9, %rdi
	je	.L1775
	movzbl	14(%rcx), %ecx
	movb	%cl, 14(%r8)
	.p2align 4,,10
	.p2align 3
.L1775:
	subq	%r13, %rdi
	addq	%rdi, %rsi
.L1770:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rdx, 24(%rbx)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%rbx)
.L1734:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	.cfi_restore_state
	movl	$2147483648, %esi
	movl	$2147483647, %r14d
.L1755:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1874
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1758:
	leaq	(%rax,%r14), %rdx
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1871:
	subq	%rdx, %rdi
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1776:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1739:
	movzbl	(%rdx,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L1739
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1759:
	leaq	(%rsi,%r12), %r8
	.p2align 4,,10
	.p2align 3
.L1763:
	movzbl	(%rcx), %edi
	addq	$1, %rsi
	movb	%dil, -1(%rsi)
	cmpq	%r8, %rsi
	jne	.L1763
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1747:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1750:
	movzbl	0(%r13,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L1750
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	%rdi, %r9
	xorl	%ecx, %ecx
	subq	%r13, %r9
	.p2align 4,,10
	.p2align 3
.L1774:
	movzbl	0(%r13,%rcx), %r8d
	movb	%r8b, (%rsi,%rcx)
	addq	$1, %rcx
	cmpq	%r9, %rcx
	jne	.L1774
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	%r13, %r8
	xorl	%ecx, %ecx
	subq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L1768:
	movzbl	(%rsi,%rcx), %edi
	movb	%dil, (%rax,%rcx)
	addq	$1, %rcx
	cmpq	%r8, %rcx
	jne	.L1768
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1746:
	addq	%r15, %r12
	movq	%r12, 16(%rbx)
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1777:
	movq	%rdi, %r12
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1780:
	movq	%rax, %rcx
	jmp	.L1764
.L1874:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1758
.L1873:
	cmpq	$2147483647, %rdi
	cmovbe	%rdi, %r14
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	jmp	.L1755
.L1872:
	leaq	.LC43(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23904:
	.size	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, .-_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"local decls count"
.LC46:
	.string	"local count"
.LC47:
	.string	"local count too large"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.8
	.align 8
.LC48:
	.string	"expected %u bytes, fell off end"
	.align 8
.LC49:
	.string	"invalid local type 'anyref', enable with --experimental-wasm-anyref"
	.align 8
.LC50:
	.string	"invalid local type 'funcref', enable with --experimental-wasm-anyref"
	.align 8
.LC51:
	.string	"invalid local type 'exception ref', enable with --experimental-wasm-eh"
	.align 8
.LC52:
	.string	"invalid local type 'Simd128', enable with --experimental-wasm-simd"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE.str1.1
.LC53:
	.string	"invalid local type"
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	.type	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE, @function
_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE:
.LFB22820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1876
	movq	(%rdx), %r8
	movq	8(%rdx), %rcx
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rax
	movq	16(%rdx), %r9
	leaq	(%rcx,%r8), %r15
	subq	%rdi, %rax
	leaq	(%r9,%r8), %r14
	addq	%r9, %r15
	cmpq	%rax, %rcx
	jbe	.L1877
	cmpq	$2147483647, %rcx
	ja	.L2011
	movq	(%rbx), %rdi
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L2012
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1880:
	cmpq	%r15, %r14
	je	.L2007
	leaq	16(%r9,%r8), %rdx
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%sil
	cmpq	%rdx, %r14
	setnb	%dl
	orb	%dl, %sil
	je	.L1883
	leaq	-1(%rcx), %rdx
	cmpq	$14, %rdx
	jbe	.L1883
	movq	%rcx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	andq	$-16, %rsi
	subq	%rax, %rdi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L1885:
	movdqu	(%rdx,%rdi), %xmm0
	addq	$16, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rsi
	jne	.L1885
	movq	%rcx, %rsi
	andq	$-16, %rsi
	addq	%rsi, %r14
	leaq	(%rax,%rsi), %rdx
	cmpq	%rsi, %rcx
	je	.L1886
	movzbl	(%r14), %esi
	movb	%sil, (%rdx)
	leaq	1(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	1(%r14), %esi
	movb	%sil, 1(%rdx)
	leaq	2(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	2(%r14), %esi
	movb	%sil, 2(%rdx)
	leaq	3(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	3(%r14), %esi
	movb	%sil, 3(%rdx)
	leaq	4(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	4(%r14), %esi
	movb	%sil, 4(%rdx)
	leaq	5(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	5(%r14), %esi
	movb	%sil, 5(%rdx)
	leaq	6(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	6(%r14), %esi
	movb	%sil, 6(%rdx)
	leaq	7(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	7(%r14), %esi
	movb	%sil, 7(%rdx)
	leaq	8(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	8(%r14), %esi
	movb	%sil, 8(%rdx)
	leaq	9(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	9(%r14), %esi
	movb	%sil, 9(%rdx)
	leaq	10(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	10(%r14), %esi
	movb	%sil, 10(%rdx)
	leaq	11(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	11(%r14), %esi
	movb	%sil, 11(%rdx)
	leaq	12(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	12(%r14), %esi
	movb	%sil, 12(%rdx)
	leaq	13(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	13(%r14), %esi
	movb	%sil, 13(%rdx)
	leaq	14(%r14), %rsi
	cmpq	%rsi, %r15
	je	.L2007
	movzbl	14(%r14), %esi
	movb	%sil, 14(%rdx)
	leaq	(%rax,%rcx), %rdx
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	16(%rbx), %rax
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%rdx, %rcx
	ja	.L1890
	testq	%rcx, %rcx
	jne	.L2013
.L1891:
	addq	%rdi, %rcx
	cmpq	%rax, %rcx
	je	.L1876
	movq	%rcx, 16(%rbx)
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L1899
.L2022:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %ecx
	andl	$127, %ecx
	testb	%dl, %dl
	js	.L2014
.L1900:
	movq	%r8, 16(%r12)
.L1910:
	xorl	%eax, %eax
	cmpq	$0, 56(%r12)
	jne	.L1875
	leal	-1(%rcx), %r15d
	testl	%ecx, %ecx
	je	.L1913
	leaq	.L1931(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	16(%r12), %rax
	movq	24(%r12), %rsi
	cmpq	%rsi, %rax
	jnb	.L1913
	movzbl	(%rax), %ecx
	leaq	1(%rax), %r8
	movl	%ecx, %edx
	andl	$127, %edx
	testb	%cl, %cl
	js	.L2015
	movq	%r8, 16(%r12)
.L1923:
	cmpq	$0, 56(%r12)
	jne	.L1927
	movq	16(%rbx), %rcx
	movl	$50000, %eax
	subq	8(%rbx), %rcx
	subq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L2016
.L1926:
	movq	16(%r12), %rsi
	cmpl	%esi, 24(%r12)
	je	.L1928
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rcx
	movq	%rcx, 16(%r12)
	subl	$104, %eax
	cmpb	$23, %al
	ja	.L1929
	movzbl	%al, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,"aG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,comdat
	.align 4
	.align 4
.L1931:
	.long	.L1938-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1937-.L1931
	.long	.L1936-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1929-.L1931
	.long	.L1935-.L1931
	.long	.L1934-.L1931
	.long	.L1933-.L1931
	.long	.L1932-.L1931
	.long	.L1930-.L1931
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,"axG",@progbits,_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE,comdat
	.p2align 4,,10
	.p2align 3
.L1928:
	movl	$1, %ecx
	xorl	%eax, %eax
	leaq	.LC48(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rcx
	cmpq	$0, 56(%r12)
	movq	%rcx, 16(%r12)
	jne	.L1927
	.p2align 4,,10
	.p2align 3
.L1929:
	leaq	-1(%rcx), %rsi
	leaq	.LC53(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L1927:
	xorl	%eax, %eax
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1899:
	leaq	.LC45(%rip), %rcx
	leaq	.LC26(%rip), %rdx
.L2010:
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	cmpq	$0, 56(%r12)
	sete	%al
.L1875:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2017
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1930:
	.cfi_restore_state
	movb	$1, -57(%rbp)
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	16(%rbx), %rsi
	leaq	-57(%rbp), %rcx
	movq	%rbx, %rdi
	subl	$1, %r15d
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	cmpl	$-1, %r15d
	jne	.L1912
.L1913:
	movl	$1, %eax
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1932:
	movb	$2, -57(%rbp)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1933:
	movb	$3, -57(%rbp)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1934:
	movb	$4, -57(%rbp)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1935:
	cmpb	$0, 3(%r13)
	je	.L1943
	movb	$5, -57(%rbp)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1936:
	cmpb	$0, 7(%r13)
	je	.L1941
	movb	$7, -57(%rbp)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1937:
	cmpb	$0, 7(%r13)
	je	.L1940
	movb	$6, -57(%rbp)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1938:
	cmpb	$0, 1(%r13)
	je	.L1942
	movb	$9, -57(%rbp)
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L2015:
	cmpq	%r8, %rsi
	ja	.L2018
.L1921:
	movq	%r8, 16(%r12)
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC46(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2009:
	movq	56(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1926
	xorl	%eax, %eax
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L2018:
	movzbl	1(%rax), %edi
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L1916
	leaq	2(%rax), %r8
	cmpq	%r8, %rsi
	jbe	.L1921
	movzbl	2(%rax), %edi
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L1918
	leaq	3(%rax), %r8
	cmpq	%r8, %rsi
	jbe	.L1921
	movzbl	3(%rax), %edi
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %edx
	testb	%dil, %dil
	jns	.L1920
	leaq	4(%rax), %r8
	cmpq	%r8, %rsi
	jbe	.L1921
	movzbl	4(%rax), %ecx
	addq	$5, %rax
	movq	%rax, 16(%r12)
	movl	%ecx, %esi
	movl	%ecx, %r9d
	sall	$28, %esi
	andl	$-16, %r9d
	orl	%esi, %edx
	testb	%cl, %cl
	js	.L2019
.L1922:
	testb	%r9b, %r9b
	je	.L1923
	leaq	.LC27(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2014:
	cmpq	%r8, %rax
	jbe	.L1901
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L1900
	cmpq	%r8, %rax
	jbe	.L1901
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L1900
	cmpq	%r8, %rax
	jbe	.L1901
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r14
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	js	.L2020
	movq	%r14, 16(%r12)
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L1890:
	leaq	(%r14,%rdx), %rcx
	testq	%rdx, %rdx
	jne	.L2021
.L1892:
	cmpq	%rcx, %r15
	je	.L1893
	addq	%rdx, %r8
	leaq	16(%r9,%r8), %rdx
	leaq	(%r9,%r8), %rsi
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%dil
	cmpq	%rdx, %rsi
	setnb	%dl
	orb	%dl, %dil
	je	.L1894
	leaq	-1(%r15), %rdx
	subq	%rcx, %rdx
	cmpq	$14, %rdx
	jbe	.L1894
	movq	%r15, %r8
	xorl	%edx, %edx
	subq	%rcx, %r8
	movq	%r8, %rdi
	andq	$-16, %rdi
	.p2align 4,,10
	.p2align 3
.L1895:
	movdqu	(%rsi,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1895
	movq	%r8, %rdi
	andq	$-16, %rdi
	leaq	(%rcx,%rdi), %rdx
	leaq	(%rax,%rdi), %rsi
	cmpq	%r8, %rdi
	je	.L1897
	movzbl	(%rdx), %edi
	movb	%dil, (%rsi)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rsi)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rsi)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rsi)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rsi)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rsi)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rsi)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rsi)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rsi)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rsi)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rsi)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rsi)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rsi)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rsi)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %r15
	je	.L1897
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L1897:
	subq	%rcx, %r15
	addq	%r15, %rax
.L1893:
	movq	%rax, 16(%rbx)
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L1899
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L1901:
	movq	%r8, 16(%r12)
	leaq	.LC45(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r8, %rsi
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2007:
	leaq	(%rax,%rcx), %rdx
.L1886:
	movq	%rax, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	cmpq	%rax, %rsi
	jnb	.L1899
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L1916:
	addq	$2, %rax
	movq	%rax, 16(%r12)
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L2016:
	movq	16(%r12), %rax
	leaq	.LC47(%rip), %rdx
	movq	%r12, %rdi
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1883:
	leaq	(%rcx,%rax), %rdx
	subq	%rax, %r14
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1889:
	movzbl	(%rcx,%r14), %esi
	addq	$1, %rcx
	movb	%sil, -1(%rcx)
	cmpq	%rcx, %rdx
	jne	.L1889
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1918:
	addq	$3, %rax
	movq	%rax, 16(%r12)
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	%rcx, %rdx
	movq	%r14, %rsi
	movq	%rcx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L1891
.L2021:
	movq	%r14, %rsi
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdx
	jmp	.L1892
.L2012:
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	jmp	.L1880
.L1894:
	movq	%r15, %rdi
	xorl	%edx, %edx
	subq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L1898:
	movzbl	(%rcx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L1898
	jmp	.L1897
.L1920:
	addq	$4, %rax
	movq	%rax, 16(%r12)
	jmp	.L1923
.L1940:
	leaq	.LC49(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1875
.L1942:
	leaq	.LC51(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1875
.L1943:
	leaq	.LC52(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1875
.L1941:
	leaq	.LC50(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1875
.L2020:
	cmpq	%r14, %rax
	jbe	.L1907
	movzbl	4(%rsi), %r15d
	addq	$5, %rsi
	movq	%rsi, 16(%r12)
	testb	%r15b, %r15b
	js	.L1908
	movl	%r15d, %eax
	sall	$28, %eax
	orl	%eax, %ecx
.L1909:
	andl	$240, %r15d
	je	.L1910
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	cmpq	$0, 56(%r12)
	sete	%al
	jmp	.L1875
.L1907:
	movq	%r14, 16(%r12)
	xorl	%r15d, %r15d
.L1908:
	leaq	.LC45(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%ecx, %ecx
	jmp	.L1909
.L2017:
	call	__stack_chk_fail@PLT
.L2019:
	leaq	.LC26(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC46(%rip), %rcx
	movb	%r9b, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-80(%rbp), %r9d
	movq	-72(%rbp), %r8
	xorl	%edx, %edx
	jmp	.L1922
.L2011:
	leaq	.LC44(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22820:
	.size	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE, .-_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L2042
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2023:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2042:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movl	%r9d, %eax
	andl	$127, %eax
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L2043
	movl	$1, (%rdx)
	sall	$25, %eax
	addq	$16, %rsp
	popq	%rbx
	sarl	$25, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2043:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2026
	movzbl	1(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$7, %r8d
	andl	$16256, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L2044
	sall	$18, %eax
	movl	$2, (%rdx)
	sarl	$18, %eax
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2026:
	movl	$1, (%rdx)
.L2041:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2044:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2028
	movzbl	2(%rsi), %r8d
	movl	%r8d, %r9d
	sall	$14, %r9d
	andl	$2080768, %r9d
	orl	%r9d, %eax
	testb	%r8b, %r8b
	js	.L2045
	sall	$11, %eax
	movl	$3, (%rdx)
	sarl	$11, %eax
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2028:
	movl	$2, (%rdx)
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2045:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2046
	movl	$3, (%rdx)
	jmp	.L2041
.L2046:
	movzbl	3(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$21, %r8d
	andl	$266338304, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L2047
	sall	$4, %eax
	movl	$4, (%rdx)
	sarl	$4, %eax
	jmp	.L2023
.L2047:
	leaq	4(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L2032
	movzbl	4(%rsi), %ebx
	movl	$5, (%rdx)
	testb	%bl, %bl
	js	.L2033
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, %eax
.L2034:
	andl	$248, %ebx
	je	.L2023
	cmpb	$120, %bl
	je	.L2023
	leaq	.LC27(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L2023
.L2032:
	movl	$4, (%rdx)
	xorl	%ebx, %ebx
.L2033:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L2034
	.cfi_endproc
.LFE24299:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE.str1.1,"aMS",@progbits,1
.LC54:
	.string	"data segment index"
.LC55:
	.string	"memory index"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"expected memory index 0, found %u"
	.align 8
.LC57:
	.string	"invalid data segment index: %u"
	.align 8
.LC58:
	.string	"invalid element segment index: %u"
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE.str1.1
.LC59:
	.string	"invalid table index: %u"
.LC60:
	.string	"invalid numeric opcode"
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE:
.LFB23695:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	je	.L2049
	movq	%rax, %r15
	leal	-64512(%r13), %eax
	cmpl	$17, %eax
	ja	.L2049
	leaq	.L2052(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE,"a",@progbits
	.align 4
	.align 4
.L2052:
	.long	.L2062-.L2052
	.long	.L2062-.L2052
	.long	.L2062-.L2052
	.long	.L2062-.L2052
	.long	.L2062-.L2052
	.long	.L2062-.L2052
	.long	.L2062-.L2052
	.long	.L2062-.L2052
	.long	.L2061-.L2052
	.long	.L2060-.L2052
	.long	.L2059-.L2052
	.long	.L2058-.L2052
	.long	.L2057-.L2052
	.long	.L2056-.L2052
	.long	.L2055-.L2052
	.long	.L2054-.L2052
	.long	.L2053-.L2052
	.long	.L2051-.L2052
	.section	.text._ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE
.L2049:
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
.L2048:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2467
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2053:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	leaq	.LC34(%rip), %rcx
	leaq	-300(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	movq	16(%r12), %rax
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	%eax, -304(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L2207
	movq	192(%rax), %rdx
	subq	184(%rax), %rdx
	movl	%ecx, %esi
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2207
	leaq	-305(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$1, -305(%rbp)
	movl	-300(%rbp), %r14d
	leaq	192(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	$0, 56(%r12)
	movq	208(%r12), %rbx
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	144(%r12), %rdi
	movl	-304(%rbp), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder9TableSizeEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2210
	cmpl	$-1, 152(%r12)
	je	.L2210
	leaq	136(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %rdx
.L2210:
	movq	%rdx, -8(%rbx)
	jmp	.L2048
.L2051:
	movabsq	$4294967296, %rax
	leaq	.LC34(%rip), %rcx
	leaq	-300(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	movq	16(%r12), %rax
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	16(%r12), %rbx
	movl	%eax, -304(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	movq	%rbx, %r13
	testq	%rax, %rax
	je	.L2211
	movq	192(%rax), %rdx
	subq	184(%rax), %rdx
	movl	%ecx, %esi
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2211
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	movl	-300(%rbp), %r14d
	movzbl	2(%rdx,%rax), %r8d
	movq	208(%r12), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2214
	cmpb	$2, -120(%rcx)
	jne	.L2468
.L2215:
	movq	$0, -336(%rbp)
	movl	$10, %edi
	movl	$10, %ecx
.L2217:
	cmpb	%cl, %r8b
	je	.L2218
	movzbl	%r8b, %r9d
	movb	%r8b, -328(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -344(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movzbl	-328(%rbp), %r8d
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r8b
	setne	%dl
	testb	%dl, %cl
	je	.L2218
	cmpb	$1, %al
	movl	-344(%rbp), %r9d
	jne	.L2469
.L2218:
	movq	80(%r12), %rdx
	movl	-304(%rbp), %eax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	salq	$4, %rax
	addq	184(%rdx), %rax
	movq	208(%r12), %rdx
	movzbl	(%rax), %r9d
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2221
	cmpb	$2, -120(%rcx)
	movq	16(%r12), %rsi
	jne	.L2470
.L2222:
	movq	$0, -328(%rbp)
.L2224:
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movabsq	$-6148914691236517205, %rdi
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2227
	cmpb	$2, -120(%rcx)
	jne	.L2471
.L2228:
	xorl	%r13d, %r13d
.L2230:
	cmpq	$0, 56(%r12)
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	-336(%rbp), %r8
	movq	-328(%rbp), %rcx
	movq	%r13, %rdx
	movq	144(%r12), %rdi
	movl	-304(%rbp), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder9TableFillEjPNS1_4NodeES4_S4_@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L2461
	jmp	.L2048
.L2062:
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	jmp	.L2048
.L2061:
	movq	16(%r12), %rbx
	leaq	-304(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC54(%rip), %rcx
	movl	$0, -304(%rbp)
	leaq	2(%rbx), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, %r13d
	movl	-304(%rbp), %eax
	leaq	1(%rbx,%rax), %rcx
	movq	%rax, %rdx
	movq	24(%r12), %rax
	leaq	1(%rcx), %rsi
	cmpq	%rax, %rsi
	ja	.L2064
	cmpl	%esi, %eax
	je	.L2064
	movzbl	1(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.L2472
.L2066:
	movq	80(%r12), %rax
	movq	16(%r12), %rbx
	testq	%rax, %rax
	je	.L2067
	cmpl	%r13d, 76(%rax)
	jbe	.L2067
	cmpb	$0, 18(%rax)
	leal	1(%rdx), %r14d
	je	.L2473
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	movzbl	2(%rdx,%rax), %r9d
	movq	208(%r12), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2071
	cmpb	$2, -120(%rcx)
	jne	.L2474
.L2072:
	movq	$0, -336(%rbp)
	movl	$10, %edi
	movl	$10, %ecx
.L2074:
	cmpb	%cl, %r9b
	je	.L2075
	movzbl	%r9b, %r8d
	movb	%r9b, -328(%rbp)
	movl	%r8d, %esi
	movl	%r8d, -344(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movzbl	-328(%rbp), %r9d
	cmpb	$10, %r9b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2075
	cmpb	$1, %al
	movl	-344(%rbp), %r8d
	jne	.L2475
.L2075:
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	movzbl	1(%rdx,%rax), %r9d
	movq	208(%r12), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2078
	cmpb	$2, -120(%rcx)
	movq	16(%r12), %rsi
	jne	.L2476
.L2079:
	movq	$0, -328(%rbp)
.L2081:
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movabsq	$-6148914691236517205, %rdi
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2084
	cmpb	$2, -120(%rcx)
	jne	.L2477
.L2085:
	xorl	%r15d, %r15d
.L2087:
	cmpq	$0, 56(%r12)
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	144(%r12), %rdi
	movq	16(%r12), %r9
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	-336(%rbp), %r8
	movq	-328(%rbp), %rcx
	subl	8(%r12), %r9d
	call	_ZN2v88internal8compiler16WasmGraphBuilder10MemoryInitEjPNS1_4NodeES4_S4_i@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2048
.L2461:
	cmpl	$-1, 152(%r12)
	je	.L2048
	leaq	136(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	jmp	.L2048
.L2060:
	movq	16(%r12), %rax
	leaq	.LC54(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-300(%rbp), %rdx
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -304(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L2091
	cmpl	76(%rax), %ecx
	jnb	.L2091
	cmpq	$0, 56(%r12)
	movl	-300(%rbp), %r14d
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	144(%r12), %rdi
	movq	16(%r12), %rdx
	movl	%ecx, %esi
	subq	8(%r12), %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder8DataDropEji@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L2461
	jmp	.L2048
.L2059:
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	leaq	2(%rbx), %rsi
	cmpq	%rax, %rsi
	ja	.L2095
	cmpl	%esi, %eax
	je	.L2095
	movzbl	2(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L2478
.L2097:
	leaq	3(%rbx), %rsi
	cmpq	%rax, %rsi
	ja	.L2098
	cmpl	%esi, %eax
	je	.L2098
	movzbl	3(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L2479
.L2100:
	movq	80(%r12), %rax
	movq	16(%r12), %r14
	testq	%rax, %rax
	je	.L2101
	cmpb	$0, 18(%rax)
	je	.L2101
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	movzbl	2(%rdx,%rax), %ebx
	movq	208(%r12), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2480
	cmpb	$2, -120(%rcx)
	jne	.L2481
.L2105:
	movq	$0, -328(%rbp)
	movl	$10, %edi
	movl	$10, %ecx
.L2107:
	cmpb	%cl, %bl
	je	.L2108
	movzbl	%bl, %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %bl
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2108
	cmpb	$1, %al
	jne	.L2482
.L2108:
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	movzbl	1(%rdx,%rax), %r9d
	movq	208(%r12), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2111
	cmpb	$2, -120(%rcx)
	movq	16(%r12), %rsi
	jne	.L2483
.L2112:
	xorl	%r13d, %r13d
.L2114:
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movabsq	$-6148914691236517205, %rdi
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2117
	cmpb	$2, -120(%rcx)
	jne	.L2484
.L2118:
	xorl	%r15d, %r15d
.L2120:
	cmpq	$0, 56(%r12)
	movl	$2, %r14d
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	144(%r12), %rdi
	movq	-328(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	16(%r12), %r8
	subl	8(%r12), %r8d
	call	_ZN2v88internal8compiler16WasmGraphBuilder10MemoryCopyEPNS1_4NodeES4_S4_i@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2123
	cmpl	$-1, 152(%r12)
	je	.L2123
	leaq	136(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L2123:
	movl	$2, %r14d
	jmp	.L2048
.L2058:
	movq	16(%r12), %r13
	movq	24(%r12), %rax
	leaq	2(%r13), %rsi
	movq	%r13, %r14
	cmpq	%rax, %rsi
	ja	.L2124
	cmpl	%esi, %eax
	je	.L2124
	movzbl	2(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L2485
.L2126:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L2127
	cmpb	$0, 18(%rax)
	je	.L2127
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	movzbl	2(%rdx,%rax), %ebx
	movq	208(%r12), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jnb	.L2486
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %edi
	subq	$24, %rdx
	movq	(%rdx), %r14
	movq	%rdx, 208(%r12)
	movq	%rax, -328(%rbp)
	movl	%edi, %ecx
.L2133:
	cmpb	%cl, %bl
	je	.L2134
	movzbl	%bl, %r9d
	movl	%r9d, %esi
	movl	%r9d, -336(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %bl
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2134
	cmpb	$1, %al
	movl	-336(%rbp), %r9d
	jne	.L2487
.L2134:
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %rdi
	movq	240(%r12), %rcx
	movzbl	1(%rdx,%rax), %r9d
	movq	208(%r12), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2137
	cmpb	$2, -120(%rcx)
	movq	16(%r12), %rsi
	jne	.L2488
.L2138:
	xorl	%r13d, %r13d
.L2140:
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movabsq	$-6148914691236517205, %rdi
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2143
	cmpb	$2, -120(%rcx)
	jne	.L2489
.L2144:
	xorl	%r15d, %r15d
.L2146:
	cmpq	$0, 56(%r12)
	movl	$1, %r14d
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	144(%r12), %rdi
	movq	-328(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	16(%r12), %r8
	subl	8(%r12), %r8d
	call	_ZN2v88internal8compiler16WasmGraphBuilder10MemoryFillEPNS1_4NodeES4_S4_i@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2149
	cmpl	$-1, 152(%r12)
	je	.L2149
	leaq	136(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L2149:
	movl	$1, %r14d
	jmp	.L2048
.L2057:
	movq	16(%r12), %rdx
	leaq	-304(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movq	80(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2490
	movq	288(%rdx), %rax
	subq	280(%rdx), %rax
	movabsq	$7905747460161236407, %r8
	sarq	$3, %rax
	movl	-304(%rbp), %esi
	movq	16(%r12), %rdi
	imulq	%r8, %rax
	movq	%rsi, %rcx
	cmpq	%rax, %rsi
	jb	.L2152
.L2151:
	leaq	2(%rdi), %rsi
	leaq	.LC58(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%r14d, %r14d
	jmp	.L2048
.L2055:
	movq	16(%r12), %rdx
	movq	%r12, %rsi
	leaq	-304(%rbp), %rdi
	call	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movq	80(%r12), %rdx
	movq	16(%r12), %rsi
	movl	-296(%rbp), %ecx
	testq	%rdx, %rdx
	je	.L2173
	movq	192(%rdx), %rax
	subq	184(%rdx), %rax
	movl	%ecx, %edx
	sarq	$4, %rax
	cmpq	%rax, %rdx
	jnb	.L2173
	movl	-304(%rbp), %edx
	addq	$2, %rsi
	movq	%rdx, %rcx
	cmpq	%rdx, %rax
	jbe	.L2462
	movq	8(%r15), %r13
	leaq	-248(%rbp), %rdi
	leaq	-56(%rbp), %rax
	movl	-288(%rbp), %r14d
	movq	%rdi, %xmm0
	movq	%rdi, -328(%rbp)
	leaq	-272(%rbp), %r8
	punpcklqdq	%xmm0, %xmm0
	movslq	%r13d, %rbx
	movq	%rax, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	cmpq	$8, %rbx
	ja	.L2491
.L2177:
	leaq	(%rbx,%rbx,2), %rax
	movl	%r13d, %r8d
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, -264(%rbp)
	subl	$1, %r8d
	js	.L2178
	movslq	%r8d, %r11
	leaq	24(%r12), %rax
	movl	%r14d, -360(%rbp)
	movq	%r15, %r10
	movq	%rax, -352(%rbp)
	movq	%r11, %rbx
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2494:
	cmpb	$2, -120(%rsi)
	movq	16(%r12), %r15
	jne	.L2492
.L2180:
	xorl	%r11d, %r11d
	movl	$10, %r14d
.L2182:
	subl	$1, %r8d
	movq	%r15, 0(%r13)
	subq	$1, %rbx
	movb	%r14b, 8(%r13)
	movq	%r11, 16(%r13)
	cmpl	$-1, %r8d
	je	.L2493
	movq	-272(%rbp), %rdi
.L2188:
	movq	16(%r10), %rax
	movq	208(%r12), %rcx
	movabsq	$-6148914691236517205, %r11
	movq	240(%r12), %rsi
	addq	%rbx, %rax
	addq	(%r10), %rax
	movzbl	(%rax), %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdi,%rax,8), %r13
	movq	%rcx, %rax
	subq	200(%r12), %rax
	movl	-132(%rsi), %edi
	sarq	$3, %rax
	imulq	%r11, %rax
	cmpq	%rax, %rdi
	jnb	.L2494
	movzbl	-16(%rcx), %r14d
	movq	-24(%rcx), %r15
	subq	$24, %rcx
	movq	16(%rcx), %r11
	movq	%rcx, 208(%r12)
	cmpb	%dl, %r14b
	je	.L2182
	cmpb	$6, %dl
	sete	%al
	cmpb	$8, %r14b
	sete	%cl
	testb	%al, %al
	je	.L2295
	testb	%cl, %cl
	je	.L2295
	movl	$8, %r14d
	jmp	.L2182
.L2054:
	movabsq	$4294967296, %rax
	leaq	.LC34(%rip), %rcx
	leaq	-300(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	movq	16(%r12), %rax
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	16(%r12), %rsi
	movl	%eax, -304(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	movq	%rsi, %r13
	testq	%rax, %rax
	je	.L2211
	movq	192(%rax), %rdx
	subq	184(%rax), %rdx
	movl	%ecx, %edi
	sarq	$4, %rdx
	cmpq	%rdx, %rdi
	jnb	.L2211
	movq	16(%r15), %rdx
	movq	(%r15), %rax
	movabsq	$-6148914691236517205, %r9
	movq	240(%r12), %rcx
	movl	-300(%rbp), %r14d
	movzbl	1(%rdx,%rax), %r10d
	movq	208(%r12), %rdx
	movl	-132(%rcx), %edi
	movq	%rdx, %rax
	subq	200(%r12), %rax
	sarq	$3, %rax
	imulq	%r9, %rax
	cmpq	%rax, %rdi
	jb	.L2194
	cmpb	$2, -120(%rcx)
	jne	.L2495
.L2195:
	xorl	%r15d, %r15d
.L2197:
	movq	%rdx, %rax
	subq	200(%r12), %rax
	movabsq	$-6148914691236517205, %rdi
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rsi
	jb	.L2200
	cmpb	$2, -120(%rcx)
	jne	.L2496
.L2201:
	xorl	%r13d, %r13d
.L2203:
	leaq	-305(%rbp), %rdx
	leaq	16(%r12), %rsi
	movb	$1, -305(%rbp)
	leaq	192(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	$0, 56(%r12)
	movq	208(%r12), %rbx
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	144(%r12), %rdi
	movl	-304(%rbp), %esi
	movq	%r15, %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder9TableGrowEjPNS1_4NodeES4_@PLT
	testq	%rax, %rax
	je	.L2206
	cmpl	$-1, 152(%r12)
	je	.L2206
	leaq	136(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L2206:
	movq	%rax, -8(%rbx)
	jmp	.L2048
.L2056:
	movq	16(%r12), %rax
	leaq	.LC41(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-300(%rbp), %rdx
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -304(%rbp)
	movl	%eax, %ecx
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L2169
	movq	288(%rax), %rdx
	subq	280(%rax), %rdx
	movl	%ecx, %esi
	movabsq	$7905747460161236407, %rax
	sarq	$3, %rdx
	imulq	%rax, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2169
	cmpq	$0, 56(%r12)
	movl	-300(%rbp), %r14d
	jne	.L2048
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2048
	movq	144(%r12), %rdi
	movq	16(%r12), %rdx
	movl	%ecx, %esi
	subq	8(%r12), %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder8ElemDropEji@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L2461
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2211:
	leaq	.LC59(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%r14d, %r14d
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2152:
	movl	-300(%rbp), %esi
	movq	192(%rdx), %rax
	subq	184(%rdx), %rax
	movl	-292(%rbp), %r14d
	sarq	$4, %rax
	movq	%rsi, %rcx
	cmpq	%rax, %rsi
	jnb	.L2497
	movq	8(%r15), %r13
	leaq	-248(%rbp), %rdi
	leaq	-56(%rbp), %rax
	movq	%rdi, %xmm0
	movq	%rdi, -328(%rbp)
	leaq	-272(%rbp), %r8
	punpcklqdq	%xmm0, %xmm0
	movslq	%r13d, %rbx
	movq	%rax, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	cmpq	$8, %rbx
	ja	.L2498
.L2155:
	leaq	(%rbx,%rbx,2), %rax
	movl	%r13d, %r8d
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, -264(%rbp)
	subl	$1, %r8d
	js	.L2156
	movslq	%r8d, %r11
	leaq	24(%r12), %rax
	movl	%r14d, -360(%rbp)
	movq	%r15, %r10
	movq	%rax, -352(%rbp)
	movq	%r11, %rbx
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2501:
	cmpb	$2, -120(%rsi)
	movq	16(%r12), %r15
	jne	.L2499
.L2158:
	xorl	%r11d, %r11d
	movl	$10, %r14d
.L2160:
	subl	$1, %r8d
	movq	%r15, 0(%r13)
	subq	$1, %rbx
	movb	%r14b, 8(%r13)
	movq	%r11, 16(%r13)
	cmpl	$-1, %r8d
	je	.L2500
	movq	-272(%rbp), %rdi
.L2166:
	movq	16(%r10), %rax
	movq	208(%r12), %rcx
	movabsq	$-6148914691236517205, %r9
	movq	240(%r12), %rsi
	addq	%rbx, %rax
	addq	(%r10), %rax
	movzbl	(%rax), %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdi,%rax,8), %r13
	movq	%rcx, %rax
	subq	200(%r12), %rax
	movl	-132(%rsi), %edi
	sarq	$3, %rax
	imulq	%r9, %rax
	cmpq	%rax, %rdi
	jnb	.L2501
	movzbl	-16(%rcx), %r14d
	movq	-24(%rcx), %r15
	subq	$24, %rcx
	movq	16(%rcx), %r11
	movq	%rcx, 208(%r12)
	cmpb	%dl, %r14b
	je	.L2160
	cmpb	$6, %dl
	sete	%al
	cmpb	$8, %r14b
	sete	%cl
	testb	%al, %al
	je	.L2293
	testb	%cl, %cl
	je	.L2293
	movl	$8, %r14d
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2486:
	cmpb	$2, -120(%rcx)
	jne	.L2502
.L2131:
	movq	$0, -328(%rbp)
	movl	$10, %edi
	movl	$10, %ecx
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2485:
	leaq	.LC56(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %r13
	movq	%r13, %r14
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2472:
	leaq	.LC56(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-304(%rbp), %edx
	jmp	.L2066
	.p2align 4,,10
	.p2align 3
.L2478:
	xorl	%eax, %eax
	leaq	.LC56(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2479:
	leaq	.LC56(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2173:
	addq	$1, %rsi
.L2462:
	leaq	.LC59(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2101:
	leaq	2(%r14), %rsi
.L2459:
	leaq	.LC31(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2207:
	movq	16(%r12), %rsi
	jmp	.L2462
	.p2align 4,,10
	.p2align 3
.L2091:
	movq	16(%r12), %rax
	leaq	.LC57(%rip), %rdx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	leaq	2(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	16(%r12), %rax
	leaq	.LC58(%rip), %rdx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	leaq	2(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2127:
	leaq	2(%r13), %rsi
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2067:
	leaq	2(%rbx), %rsi
	movl	%r13d, %ecx
	leaq	.LC57(%rip), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2048
.L2293:
	leal	-7(%r14), %esi
	andl	$253, %esi
	jne	.L2294
	testb	%al, %al
	jne	.L2160
.L2294:
	leal	-7(%rdx), %eax
	movzbl	%r14b, %edi
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L2160
	cmpb	$10, %dl
	setne	%cl
	cmpb	$10, %r14b
	setne	%al
	testb	%al, %cl
	je	.L2160
	movl	%r8d, -388(%rbp)
	movq	%r10, -384(%rbp)
	movb	%dl, -376(%rbp)
	movq	%r11, -368(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	cmpq	24(%r12), %r15
	movq	-368(%rbp), %r11
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movzbl	-376(%rbp), %edx
	movq	%rax, -336(%rbp)
	movq	-384(%rbp), %r10
	movl	-388(%rbp), %r8d
	jnb	.L2163
	movq	-352(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-388(%rbp), %r8d
	movq	-384(%rbp), %r10
	movq	%rax, -336(%rbp)
	movzbl	-376(%rbp), %edx
	movq	-368(%rbp), %r11
.L2163:
	movzbl	%dl, %edi
	movl	%r8d, -384(%rbp)
	movq	%r10, -376(%rbp)
	movq	%r11, -368(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	-368(%rbp), %r11
	movq	-376(%rbp), %r10
	movq	%rax, %r9
	movl	-384(%rbp), %r8d
	jnb	.L2164
	movq	-352(%rbp), %rdi
	movl	%r8d, -388(%rbp)
	movq	%r10, -384(%rbp)
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-388(%rbp), %r8d
	movq	-384(%rbp), %r10
	movq	-376(%rbp), %r9
	movq	-368(%rbp), %r11
	movq	%rax, %rcx
.L2164:
	pushq	-344(%rbp)
	movq	%r12, %rdi
	movq	%r15, %rsi
	xorl	%eax, %eax
	pushq	-336(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	%r10, -376(%rbp)
	movq	%r11, -368(%rbp)
	movl	%r8d, -336(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	movq	-368(%rbp), %r11
	movl	-336(%rbp), %r8d
	movq	-376(%rbp), %r10
	jmp	.L2160
.L2295:
	leal	-7(%r14), %esi
	andl	$253, %esi
	jne	.L2296
	testb	%al, %al
	jne	.L2182
.L2296:
	leal	-7(%rdx), %eax
	movzbl	%r14b, %edi
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L2182
	cmpb	$10, %r14b
	setne	%cl
	cmpb	$10, %dl
	setne	%al
	testb	%al, %cl
	je	.L2182
	movl	%r8d, -388(%rbp)
	movq	%r10, -384(%rbp)
	movb	%dl, -376(%rbp)
	movq	%r11, -368(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	cmpq	24(%r12), %r15
	movq	-368(%rbp), %r11
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movzbl	-376(%rbp), %edx
	movq	%rax, -336(%rbp)
	movq	-384(%rbp), %r10
	movl	-388(%rbp), %r8d
	jnb	.L2185
	movq	-352(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-388(%rbp), %r8d
	movq	-384(%rbp), %r10
	movq	%rax, -336(%rbp)
	movzbl	-376(%rbp), %edx
	movq	-368(%rbp), %r11
.L2185:
	movzbl	%dl, %edi
	movl	%r8d, -384(%rbp)
	movq	%r10, -376(%rbp)
	movq	%r11, -368(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	-368(%rbp), %r11
	movq	-376(%rbp), %r10
	movq	%rax, %r9
	movl	-384(%rbp), %r8d
	jnb	.L2186
	movq	-352(%rbp), %rdi
	movl	%r8d, -388(%rbp)
	movq	%r10, -384(%rbp)
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-388(%rbp), %r8d
	movq	-384(%rbp), %r10
	movq	-376(%rbp), %r9
	movq	-368(%rbp), %r11
	movq	%rax, %rcx
.L2186:
	pushq	-344(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	-336(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	%r10, -376(%rbp)
	movq	%r11, -368(%rbp)
	movl	%r8d, -336(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	movl	-336(%rbp), %r8d
	movq	-368(%rbp), %r11
	movq	-376(%rbp), %r10
	jmp	.L2182
	.p2align 4,,10
	.p2align 3
.L2499:
	leaq	.LC0(%rip), %rcx
	cmpq	%r15, 24(%r12)
	jbe	.L2159
	movq	-352(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r8d, -344(%rbp)
	movq	%r10, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movl	-344(%rbp), %r8d
	movq	-336(%rbp), %r10
	movq	%rax, %rcx
.L2159:
	movq	%r15, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	%r8d, -344(%rbp)
	movq	%r10, -336(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %r15
	movl	-344(%rbp), %r8d
	movq	-336(%rbp), %r10
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2492:
	leaq	.LC0(%rip), %rcx
	cmpq	%r15, 24(%r12)
	jbe	.L2181
	movq	-352(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r8d, -344(%rbp)
	movq	%r10, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r15
	movl	-344(%rbp), %r8d
	movq	-336(%rbp), %r10
	movq	%rax, %rcx
.L2181:
	movq	%r15, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	%r8d, -344(%rbp)
	movq	%r10, -336(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %r15
	movl	-344(%rbp), %r8d
	movq	-336(%rbp), %r10
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	16(%r15), %rcx
	movq	(%r15), %rax
	subq	$24, %rdx
	movzbl	8(%rdx), %r8d
	movq	(%rdx), %r14
	movzbl	(%rcx,%rax), %ecx
	movq	16(%rdx), %r15
	movq	%rdx, 208(%r12)
	cmpb	%cl, %r8b
	je	.L2146
	movzbl	%cl, %ebx
	movzbl	%r8b, %edi
	movl	%ebx, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2146
	cmpb	$1, %al
	je	.L2146
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -352(%rbp)
	movq	%rax, -336(%rbp)
	cmpq	24(%r12), %r14
	jnb	.L2147
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -336(%rbp)
.L2147:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %rbx
	cmpq	24(%r12), %rsi
	jnb	.L2148
	movq	-352(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L2148:
	pushq	-344(%rbp)
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	pushq	-336(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r9
	popq	%r10
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2137:
	movzbl	-16(%rdx), %edi
	leaq	-24(%rdx), %r10
	movq	-24(%rdx), %r14
	movq	-8(%rdx), %r13
	movq	%r10, 208(%r12)
	movq	%r10, %rdx
	cmpb	%r9b, %dil
	je	.L2140
	movzbl	%r9b, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %dil
	movq	%r10, %rdx
	setne	%r11b
	cmpb	$10, %r9b
	setne	%sil
	testb	%sil, %r11b
	je	.L2140
	cmpb	$1, %al
	je	.L2140
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -352(%rbp)
	movq	%rax, -336(%rbp)
	cmpq	24(%r12), %r14
	jnb	.L2141
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -336(%rbp)
.L2141:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2142
	movq	-352(%rbp), %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-360(%rbp), %r9
	movq	%rax, %rcx
.L2142:
	pushq	-344(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	-336(%rbp)
	leaq	.LC19(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r11
	popq	%rbx
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2493:
	movl	-360(%rbp), %r14d
	movq	-272(%rbp), %rdi
.L2178:
	cmpq	$0, 56(%r12)
	jne	.L2189
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	je	.L2503
	.p2align 4,,10
	.p2align 3
.L2189:
	cmpq	-328(%rbp), %rdi
	je	.L2048
	call	free@PLT
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2500:
	movl	-360(%rbp), %r14d
	movq	-272(%rbp), %rdi
.L2156:
	cmpq	$0, 56(%r12)
	jne	.L2189
	movq	240(%r12), %rax
	cmpb	$0, -120(%rax)
	jne	.L2189
	subq	$8, %rsp
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	movq	16(%rdi), %rcx
	movq	64(%rdi), %r9
	movq	40(%rdi), %r8
	movl	-304(%rbp), %edx
	pushq	%rax
	movl	-300(%rbp), %esi
	movq	144(%r12), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder9TableInitEjjPNS1_4NodeES4_S4_i@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2190
.L2466:
	cmpl	$-1, 152(%r12)
	je	.L2190
	leaq	136(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L2190:
	movq	-272(%rbp), %rdi
	jmp	.L2189
	.p2align 4,,10
	.p2align 3
.L2200:
	movq	80(%r12), %rcx
	movl	-304(%rbp), %eax
	subq	$24, %rdx
	movq	(%rdx), %rbx
	movq	16(%rdx), %r13
	salq	$4, %rax
	addq	184(%rcx), %rax
	movzbl	8(%rdx), %ecx
	movzbl	(%rax), %r9d
	movq	%rdx, 208(%r12)
	cmpb	%cl, %r9b
	je	.L2203
	movzbl	%r9b, %r8d
	movzbl	%cl, %edi
	movl	%r8d, %esi
	movl	%r8d, -328(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2203
	cmpb	$1, %al
	je	.L2203
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %rbx
	movl	-328(%rbp), %r8d
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -352(%rbp)
	movq	%rax, -336(%rbp)
	jnb	.L2204
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-328(%rbp), %r8d
	movq	%rax, -336(%rbp)
.L2204:
	movl	%r8d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2205
	movq	-352(%rbp), %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-328(%rbp), %r9
	movq	%rax, %rcx
.L2205:
	pushq	-344(%rbp)
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r12, %rdi
	pushq	-336(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r9
	popq	%r10
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2194:
	movq	-24(%rdx), %rsi
	movzbl	-16(%rdx), %edi
	leaq	-24(%rdx), %r11
	movq	-8(%rdx), %r15
	movq	%r11, 208(%r12)
	movq	%r11, %rdx
	movq	%rsi, -328(%rbp)
	cmpb	%r10b, %dil
	je	.L2197
	movzbl	%r10b, %r9d
	movl	%r9d, %esi
	movl	%r9d, -336(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %dil
	movq	%r11, %rdx
	setne	%bl
	cmpb	$10, %r10b
	setne	%sil
	testb	%sil, %bl
	je	.L2197
	cmpb	$1, %al
	je	.L2197
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-328(%rbp), %rsi
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %rsi
	movq	%rax, -344(%rbp)
	movl	-336(%rbp), %r9d
	leaq	.LC0(%rip), %rbx
	movq	%rdi, -352(%rbp)
	jnb	.L2198
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r13
	movl	-336(%rbp), %r9d
	movq	%rax, %rbx
.L2198:
	movl	%r9d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r13
	jnb	.L2199
	movq	-352(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-336(%rbp), %r9
	movq	%rax, %rcx
.L2199:
	pushq	-344(%rbp)
	movq	-328(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r11
	popq	%rbx
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2197
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	16(%r15), %rcx
	movq	(%r15), %rax
	subq	$24, %rdx
	movq	(%rdx), %r14
	movq	16(%rdx), %r15
	movzbl	(%rcx,%rax), %r8d
	movzbl	8(%rdx), %ecx
	movq	%rdx, 208(%r12)
	cmpb	%r8b, %cl
	je	.L2120
	movzbl	%r8b, %ebx
	movzbl	%cl, %edi
	movl	%ebx, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2120
	cmpb	$1, %al
	je	.L2120
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -352(%rbp)
	movq	%rax, -336(%rbp)
	cmpq	24(%r12), %r14
	jnb	.L2121
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -336(%rbp)
.L2121:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2122
	movq	-352(%rbp), %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-360(%rbp), %r9
	movq	%rax, %rcx
.L2122:
	pushq	-344(%rbp)
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	pushq	-336(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L2120
	.p2align 4,,10
	.p2align 3
.L2071:
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %edi
	movq	-24(%rdx), %rbx
	movq	%rax, -336(%rbp)
	leaq	-24(%rdx), %rax
	movl	%edi, %ecx
	movq	%rax, 208(%r12)
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2111:
	movzbl	-16(%rdx), %edi
	leaq	-24(%rdx), %r10
	movq	-24(%rdx), %r14
	movq	-8(%rdx), %r13
	movq	%r10, 208(%r12)
	movq	%r10, %rdx
	cmpb	%r9b, %dil
	je	.L2114
	movzbl	%r9b, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	movq	%r10, %rdx
	setne	%r9b
	cmpb	$10, %dil
	setne	%sil
	testb	%sil, %r9b
	je	.L2114
	cmpb	$1, %al
	je	.L2114
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -352(%rbp)
	movq	%rax, -336(%rbp)
	cmpq	24(%r12), %r14
	jnb	.L2115
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -336(%rbp)
.L2115:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2116
	movq	-352(%rbp), %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-360(%rbp), %r9
	movq	%rax, %rcx
.L2116:
	pushq	-344(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	-336(%rbp)
	leaq	.LC19(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	movq	208(%r12), %rdx
	movq	240(%r12), %rcx
	popq	%rsi
	jmp	.L2114
	.p2align 4,,10
	.p2align 3
.L2480:
	movzbl	-16(%rdx), %edi
	movq	-8(%rdx), %rax
	subq	$24, %rdx
	movq	(%rdx), %r14
	movq	%rdx, 208(%r12)
	movq	%rax, -328(%rbp)
	movl	%edi, %ecx
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2084:
	movq	16(%r15), %rcx
	movq	(%r15), %rax
	subq	$24, %rdx
	movzbl	8(%rdx), %r9d
	movq	(%rdx), %rbx
	movzbl	(%rcx,%rax), %ecx
	movq	16(%rdx), %r15
	movq	%rdx, 208(%r12)
	cmpb	%cl, %r9b
	je	.L2087
	movzbl	%cl, %r8d
	movzbl	%r9b, %edi
	movl	%r8d, %esi
	movl	%r8d, -344(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2087
	cmpb	$1, %al
	je	.L2087
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %rbx
	movl	-344(%rbp), %r8d
	movq	%rax, -360(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -368(%rbp)
	movq	%rax, -352(%rbp)
	jnb	.L2088
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-344(%rbp), %r8d
	movq	%rax, -352(%rbp)
.L2088:
	movl	%r8d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2089
	movq	-368(%rbp), %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-344(%rbp), %r9
	movq	%rax, %rcx
.L2089:
	pushq	-360(%rbp)
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r12, %rdi
	pushq	-352(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r9
	popq	%r10
	jmp	.L2087
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	-24(%rdx), %rsi
	movq	-8(%rdx), %rax
	leaq	-24(%rdx), %r10
	movzbl	-16(%rdx), %edi
	movq	%r10, 208(%r12)
	movq	%r10, %rdx
	movq	%rsi, -344(%rbp)
	movq	%rax, -328(%rbp)
	cmpb	%r9b, %dil
	je	.L2081
	movzbl	%r9b, %r8d
	movl	%r8d, %esi
	movl	%r8d, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	movq	%r10, %rdx
	setne	%sil
	cmpb	$10, %dil
	setne	%r9b
	testb	%r9b, %sil
	je	.L2081
	cmpb	$1, %al
	je	.L2081
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-344(%rbp), %rsi
	cmpq	24(%r12), %rsi
	leaq	24(%r12), %rbx
	movq	%rax, -368(%rbp)
	leaq	.LC0(%rip), %rax
	movl	-352(%rbp), %r8d
	movq	%rax, -360(%rbp)
	jnb	.L2082
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-352(%rbp), %r8d
	movq	%rax, -360(%rbp)
.L2082:
	movl	%r8d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2083
	movq	%rbx, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-352(%rbp), %r9
	movq	%rax, %rcx
.L2083:
	pushq	-368(%rbp)
	movq	-344(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	-360(%rbp)
	leaq	.LC19(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r11
	popq	%rbx
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2227:
	movq	16(%r15), %rcx
	movq	(%r15), %rax
	subq	$24, %rdx
	movq	(%rdx), %r15
	movq	16(%rdx), %r13
	movzbl	(%rcx,%rax), %r9d
	movzbl	8(%rdx), %ecx
	movq	%rdx, 208(%r12)
	cmpb	%r9b, %cl
	je	.L2230
	movzbl	%r9b, %r8d
	movzbl	%cl, %edi
	movl	%r8d, %esi
	movl	%r8d, -344(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L2230
	cmpb	$1, %al
	je	.L2230
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	24(%r12), %r15
	movl	-344(%rbp), %r8d
	movq	%rax, -352(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -360(%rbp)
	movq	%rax, %rbx
	jnb	.L2231
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-344(%rbp), %r8d
	movq	%rax, %rbx
.L2231:
	movl	%r8d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2232
	movq	-360(%rbp), %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-344(%rbp), %r9
	movq	%rax, %rcx
.L2232:
	pushq	-352(%rbp)
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	-24(%rdx), %rax
	movq	-8(%rdx), %rbx
	leaq	-24(%rdx), %r10
	movzbl	-16(%rdx), %edi
	movq	%r10, 208(%r12)
	movq	%r10, %rdx
	movq	%rax, -344(%rbp)
	movq	%rbx, -328(%rbp)
	cmpb	%dil, %r9b
	je	.L2224
	movzbl	%r9b, %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	movq	%r10, %rdx
	setne	%sil
	cmpb	$10, %dil
	setne	%r9b
	testb	%r9b, %sil
	je	.L2224
	cmpb	$1, %al
	je	.L2224
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC0(%rip), %rbx
	movq	%rax, -352(%rbp)
	movq	-344(%rbp), %rax
	movq	%rdi, -360(%rbp)
	cmpq	24(%r12), %rax
	jnb	.L2225
	movq	%rax, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
.L2225:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2226
	movq	-360(%rbp), %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-368(%rbp), %r9
	movq	%rax, %rcx
.L2226:
	pushq	-352(%rbp)
	movq	-344(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	movq	208(%r12), %rdx
	movq	240(%r12), %rcx
	popq	%rsi
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %edi
	movq	-24(%rdx), %rbx
	movq	%rax, -336(%rbp)
	leaq	-24(%rdx), %rax
	movl	%edi, %ecx
	movq	%rax, 208(%r12)
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2473:
	movl	%r14d, %eax
	leaq	-1(%rbx,%rax), %rsi
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2064:
	leaq	.LC55(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-304(%rbp), %edx
	jmp	.L2066
	.p2align 4,,10
	.p2align 3
.L2095:
	leaq	.LC55(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rax
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2124:
	leaq	.LC55(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r12), %r13
	movq	%r13, %r14
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2098:
	leaq	.LC55(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2497:
	movl	-296(%rbp), %eax
	movl	%r14d, %esi
	subq	$1, %rsi
	subq	%rax, %rsi
	addq	%rdi, %rsi
	jmp	.L2462
.L2502:
	leaq	.LC0(%rip), %rcx
	cmpq	%r13, 24(%r12)
	jbe	.L2132
	leaq	24(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	%rax, %rcx
.L2132:
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %r14
	movq	%r14, %r13
	jmp	.L2131
.L2488:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L2139
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2139:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2138
.L2489:
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L2145
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2145:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2144
.L2491:
	movq	%r8, %rdi
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-272(%rbp), %rdi
	jmp	.L2177
.L2498:
	movq	%r8, %rdi
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-272(%rbp), %rdi
	jmp	.L2155
.L2490:
	movl	-304(%rbp), %ecx
	movq	16(%r12), %rdi
	jmp	.L2151
.L2487:
	movl	%r9d, -344(%rbp)
	leaq	.LC0(%rip), %rbx
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	%r14, 24(%r12)
	movl	-344(%rbp), %r9d
	movq	%rax, -336(%rbp)
	movq	%rdi, -352(%rbp)
	jbe	.L2135
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r13
	movl	-344(%rbp), %r9d
	movq	%rax, %rbx
.L2135:
	movl	%r9d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r13
	jnb	.L2136
	movq	-352(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-344(%rbp), %r9
	movq	%rax, %rcx
.L2136:
	pushq	-336(%rbp)
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	pushq	%rbx
	movl	$2, %r8d
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r13
	popq	%r14
	jmp	.L2134
.L2484:
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L2119
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2119:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2118
.L2468:
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rbx
	jnb	.L2216
	movq	%rbx, %rsi
	leaq	24(%r12), %rdi
	movb	%r8b, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rbx
	movzbl	-328(%rbp), %r8d
	movq	%rax, %rcx
.L2216:
	movq	%rbx, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r8b, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %rbx
	movzbl	-328(%rbp), %r8d
	movq	%rbx, %r13
	jmp	.L2215
.L2496:
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L2202
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2202:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2201
.L2495:
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L2196
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2196:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2195
.L2483:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L2113
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2113:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2112
.L2474:
	leaq	.LC0(%rip), %rcx
	cmpq	%rbx, 24(%r12)
	jbe	.L2073
	movq	%rbx, %rsi
	leaq	24(%r12), %rdi
	movb	%r9b, -328(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rbx
	movzbl	-328(%rbp), %r9d
	movq	%rax, %rcx
.L2073:
	movq	%rbx, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r9b, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %rbx
	movzbl	-328(%rbp), %r9d
	jmp	.L2072
.L2471:
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L2229
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2229:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2228
.L2470:
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L2223
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2223:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2222
.L2477:
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	24(%r12), %rsi
	jnb	.L2086
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2086:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2085
.L2476:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, 24(%r12)
	jbe	.L2080
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %rsi
	movq	%rax, %rcx
.L2080:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	240(%r12), %rcx
	movq	208(%r12), %rdx
	jmp	.L2079
.L2481:
	leaq	.LC0(%rip), %rcx
	cmpq	%r14, 24(%r12)
	jbe	.L2106
	movq	%r14, %rsi
	leaq	24(%r12), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r14
	movq	%rax, %rcx
.L2106:
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	16(%r12), %r14
	jmp	.L2105
.L2482:
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	leaq	.LC0(%rip), %rbx
	movq	%rax, -336(%rbp)
	movq	%rdi, -344(%rbp)
	cmpq	%r14, 24(%r12)
	jbe	.L2109
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
.L2109:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2110
	movq	-344(%rbp), %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-352(%rbp), %r9
	movq	%rax, %rcx
.L2110:
	pushq	-336(%rbp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	xorl	%eax, %eax
	pushq	%rbx
	movl	$2, %r8d
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	jmp	.L2108
.L2469:
	movl	%r9d, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	%rbx, 24(%r12)
	movl	-352(%rbp), %r9d
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -360(%rbp)
	movq	%rax, -328(%rbp)
	jbe	.L2219
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	16(%r12), %r13
	movl	-352(%rbp), %r9d
	movq	%rax, -328(%rbp)
.L2219:
	movl	%r9d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %r13
	jnb	.L2220
	movq	-360(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-352(%rbp), %r9
	movq	%rax, %rcx
.L2220:
	pushq	-344(%rbp)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	pushq	-328(%rbp)
	movl	$2, %r8d
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	jmp	.L2218
.L2503:
	subq	$8, %rsp
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	movq	64(%rdi), %r9
	movq	40(%rdi), %r8
	movq	16(%rdi), %rcx
	movl	-296(%rbp), %edx
	pushq	%rax
	movq	144(%r12), %rdi
	movl	-304(%rbp), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder9TableCopyEjjPNS1_4NodeES4_S4_i@PLT
	popq	%r13
	popq	%r15
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L2466
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2475:
	movl	%r8d, -352(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	24(%r12), %rdi
	cmpq	%rbx, 24(%r12)
	movl	-352(%rbp), %r8d
	movq	%rax, -344(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rdi, -360(%rbp)
	movq	%rax, -328(%rbp)
	jbe	.L2076
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-352(%rbp), %r8d
	movq	%rax, -328(%rbp)
.L2076:
	movl	%r8d, %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	16(%r12), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	24(%r12), %rsi
	jnb	.L2077
	movq	-360(%rbp), %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-352(%rbp), %r9
	movq	%rax, %rcx
.L2077:
	pushq	-344(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	pushq	-328(%rbp)
	leaq	.LC19(%rip), %rdx
	movl	$2, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L2075
.L2467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23695:
	.size	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L2538
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2504:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2538:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movq	%r9, %rax
	andl	$127, %eax
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2539
	movl	$1, (%rdx)
	salq	$57, %rax
	addq	$16, %rsp
	popq	%rbx
	sarq	$57, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2539:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2507
	movzbl	1(%rsi), %r9d
	movq	%r9, %r8
	salq	$7, %r8
	andl	$16256, %r8d
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2540
	salq	$50, %rax
	movl	$2, (%rdx)
	sarq	$50, %rax
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2507:
	movl	$1, (%rdx)
.L2537:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2540:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2509
	movzbl	2(%rsi), %r8d
	movq	%r8, %r9
	salq	$14, %r9
	andl	$2080768, %r9d
	orq	%r9, %rax
	testb	%r8b, %r8b
	js	.L2541
	salq	$43, %rax
	movl	$3, (%rdx)
	sarq	$43, %rax
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2509:
	movl	$2, (%rdx)
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2541:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2542
	movl	$3, (%rdx)
	jmp	.L2537
.L2542:
	movzbl	3(%rsi), %r9d
	movq	%r9, %r8
	salq	$21, %r8
	andl	$266338304, %r8d
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2543
	salq	$36, %rax
	movl	$4, (%rdx)
	sarq	$36, %rax
	jmp	.L2504
.L2543:
	leaq	4(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2544
	movl	$4, (%rdx)
	jmp	.L2537
.L2544:
	movabsq	$34091302912, %r11
	movzbl	4(%rsi), %r9d
	movq	%r9, %r8
	salq	$28, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2545
	salq	$29, %rax
	movl	$5, (%rdx)
	sarq	$29, %rax
	jmp	.L2504
.L2545:
	leaq	5(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2546
	movl	$5, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2530:
	salq	$22, %rax
	sarq	$22, %rax
	jmp	.L2504
.L2546:
	movabsq	$4363686772736, %r11
	movzbl	5(%rsi), %r9d
	movq	%r9, %r8
	salq	$35, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2547
	movl	$6, (%rdx)
	jmp	.L2530
.L2547:
	leaq	6(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2548
	movl	$6, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2529:
	salq	$15, %rax
	sarq	$15, %rax
	jmp	.L2504
.L2548:
	movabsq	$558551906910208, %r11
	movzbl	6(%rsi), %r9d
	movq	%r9, %r8
	salq	$42, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2549
	movl	$7, (%rdx)
	jmp	.L2529
.L2549:
	leaq	7(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2550
	movl	$7, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2528:
	salq	$8, %rax
	sarq	$8, %rax
	jmp	.L2504
.L2550:
	movabsq	$71494644084506624, %r11
	movzbl	7(%rsi), %r9d
	movq	%r9, %r8
	salq	$49, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2551
	movl	$8, (%rdx)
	jmp	.L2528
.L2551:
	leaq	8(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2552
	movl	$8, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2527:
	addq	%rax, %rax
	sarq	%rax
	jmp	.L2504
.L2552:
	movabsq	$9151314442816847872, %r11
	movzbl	8(%rsi), %r9d
	movq	%r9, %r8
	salq	$56, %r8
	andq	%r11, %r8
	orq	%rax, %r8
	movq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2553
	movl	$9, (%rdx)
	jmp	.L2527
.L2553:
	leaq	9(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L2523
	movzbl	9(%rsi), %ebx
	movl	$10, (%rdx)
	testb	%bl, %bl
	js	.L2524
	movq	%rbx, %rax
	salq	$63, %rax
	orq	%r8, %rax
.L2525:
	testb	%bl, %bl
	je	.L2504
	cmpb	$127, %bl
	je	.L2504
	leaq	.LC27(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L2504
.L2523:
	movl	$9, (%rdx)
	xorl	%ebx, %ebx
.L2524:
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L2525
	.cfi_endproc
.LFE24300:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0.str1.1,"aMS",@progbits,1
.LC61:
	.string	"branch depth"
.LC62:
	.string	"global index"
.LC63:
	.string	"function index"
.LC64:
	.string	"exception index"
.LC65:
	.string	"local index"
.LC66:
	.string	"number of select types"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC67:
	.string	"Invalid number of types. Select accepts exactly one type"
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0.str1.1
.LC68:
	.string	"select type"
.LC69:
	.string	"invalid select type"
.LC70:
	.string	"table count"
.LC71:
	.string	"immi32"
.LC72:
	.string	"immi64"
.LC73:
	.string	"numeric_index"
.LC74:
	.string	"simd_index"
.LC75:
	.string	"invalid SIMD opcode"
.LC76:
	.string	"atomic_index"
.LC77:
	.string	"invalid Atomics opcode"
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0, @function
_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0:
.LFB24944:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rsi), %eax
	cmpb	$-46, %al
	ja	.L2555
	cmpb	$1, %al
	jbe	.L2733
	cmpb	$-46, %al
	ja	.L2733
	leaq	.L2559(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0,"a",@progbits
	.align 4
	.align 4
.L2559:
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2575-.L2559
	.long	.L2575-.L2559
	.long	.L2575-.L2559
	.long	.L2733-.L2559
	.long	.L2575-.L2559
	.long	.L2733-.L2559
	.long	.L2574-.L2559
	.long	.L2733-.L2559
	.long	.L2573-.L2559
	.long	.L2733-.L2559
	.long	.L2572-.L2559
	.long	.L2572-.L2559
	.long	.L2571-.L2559
	.long	.L2733-.L2559
	.long	.L2570-.L2559
	.long	.L2569-.L2559
	.long	.L2570-.L2559
	.long	.L2569-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2568-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2567-.L2559
	.long	.L2567-.L2559
	.long	.L2567-.L2559
	.long	.L2566-.L2559
	.long	.L2566-.L2559
	.long	.L2565-.L2559
	.long	.L2565-.L2559
	.long	.L2733-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2564-.L2559
	.long	.L2563-.L2559
	.long	.L2563-.L2559
	.long	.L2562-.L2559
	.long	.L2561-.L2559
	.long	.L2560-.L2559
	.long	.L2693-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2733-.L2559
	.long	.L2558-.L2559
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
.L2693:
	movl	$9, %r12d
.L2554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2739
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2560:
	.cfi_restore_state
	movl	$5, %r12d
	jmp	.L2554
.L2562:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %rsi
	cmpq	%rax, %rsi
	jb	.L2740
	leaq	.LC71(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2733:
	movl	$1, %r12d
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2555:
	cmpb	$-3, %al
	je	.L2576
	cmpb	$-2, %al
	jne	.L2741
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	cmpq	%rax, %r13
	ja	.L2685
	cmpl	%r13d, %eax
	je	.L2685
	movzbl	1(%rsi), %eax
	movl	$3, %r12d
	orb	$-2, %ah
	cmpl	$65027, %eax
	je	.L2554
	jg	.L2686
	jne	.L2688
.L2687:
	leaq	.LC77(%rip), %rdx
	movq	%r8, %rsi
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2741:
	cmpb	$-4, %al
	jne	.L2733
	movq	24(%rdi), %rcx
	leaq	1(%rsi), %r13
	cmpq	%rcx, %r13
	ja	.L2643
	cmpl	%r13d, %ecx
	je	.L2643
	movzbl	1(%rsi), %eax
	orb	$-4, %ah
	subl	$64512, %eax
	cmpl	$17, %eax
	ja	.L2644
	leaq	.L2646(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	.align 4
	.align 4
.L2646:
	.long	.L2737-.L2646
	.long	.L2737-.L2646
	.long	.L2737-.L2646
	.long	.L2737-.L2646
	.long	.L2737-.L2646
	.long	.L2737-.L2646
	.long	.L2737-.L2646
	.long	.L2737-.L2646
	.long	.L2653-.L2646
	.long	.L2652-.L2646
	.long	.L2651-.L2646
	.long	.L2650-.L2646
	.long	.L2649-.L2646
	.long	.L2648-.L2646
	.long	.L2647-.L2646
	.long	.L2645-.L2646
	.long	.L2645-.L2646
	.long	.L2645-.L2646
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
.L2558:
	movabsq	$4294967296, %rax
	leaq	-76(%rbp), %rdx
	leaq	1(%rsi), %rsi
	leaq	.LC63(%rip), %rcx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-76(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2561:
	leaq	-72(%rbp), %rdx
	leaq	1(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC72(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-72(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2563:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %rsi
	cmpq	%rax, %rsi
	ja	.L2640
	cmpl	%esi, %eax
	je	.L2640
	movzbl	1(%r8), %ecx
	testl	%ecx, %ecx
	je	.L2737
	leaq	.LC56(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.p2align 4,,10
	.p2align 3
.L2737:
	movl	$2, %r12d
	jmp	.L2554
.L2564:
	leaq	-80(%rbp), %r9
	movq	%rsi, %rdx
	movl	$-1, %ecx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0
	movl	-72(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2565:
	movabsq	$4294967296, %rax
	leaq	-76(%rbp), %rdx
	leaq	1(%rsi), %rsi
	leaq	.LC34(%rip), %rcx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-76(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2566:
	leaq	-64(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movb	$0, -76(%rbp)
	leaq	.LC62(%rip), %rcx
	movq	$0, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-64(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2567:
	leaq	-72(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movb	$0, -76(%rbp)
	leaq	.LC65(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-72(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2568:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	cmpq	%rax, %r13
	jb	.L2742
	leaq	.LC66(%rip), %rcx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	movl	$1, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
.L2613:
	leaq	.LC67(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2554
.L2569:
	leaq	-80(%rbp), %r9
	movq	%rdi, %rcx
	movq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movabsq	$1099511627775, %rdx
	movq	%r9, %rdi
	andq	8+_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rdx
	call	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	movl	-64(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2570:
	leaq	-64(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movq	$0, -72(%rbp)
	leaq	.LC63(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-64(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2571:
	movq	24(%rdi), %rcx
	leaq	1(%rsi), %r13
	cmpq	%rcx, %r13
	jb	.L2743
	leaq	.LC70(%rip), %rcx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	%r13, %r9
.L2620:
	cmpq	$0, 56(%rdi)
	jne	.L2631
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %r12
	leaq	.LC32(%rip), %r15
.L2630:
	xorl	%r8d, %r8d
	movq	%r9, %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r9, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-80(%rbp), %eax
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdi
	addq	%rax, %r9
	cmpq	$0, 56(%rdi)
	jne	.L2631
	addl	$1, %r14d
	cmpl	%r14d, %ebx
	jnb	.L2630
.L2631:
	subq	%r13, %r9
	leal	1(%r9), %r12d
	jmp	.L2554
.L2572:
	leaq	-76(%rbp), %rdx
	leaq	1(%rsi), %rsi
	leaq	.LC61(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-76(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2573:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r13
	cmpq	%rax, %r13
	jb	.L2744
	xorl	%eax, %eax
	leaq	.LC61(%rip), %rcx
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	movl	$1, %r12d
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rax
.L2581:
	cmpq	%r13, %rax
	ja	.L2745
	leaq	.LC64(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r13, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2554
.L2574:
	leaq	-64(%rbp), %rdx
	leaq	1(%rsi), %rsi
	movq	$0, -72(%rbp)
	leaq	.LC64(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-64(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2575:
	leaq	-80(%rbp), %r9
	movq	%rsi, %rcx
	movq	%rdi, %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	-80(%rbp), %eax
	leal	1(%rax), %r12d
	jmp	.L2554
.L2645:
	leaq	-76(%rbp), %rdx
	leaq	.LC34(%rip), %rcx
	movq	%r13, %rsi
	movabsq	$4294967296, %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	-76(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L2554
.L2685:
	leaq	.LC76(%rip), %rdx
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-88(%rbp), %rdi
.L2688:
	leaq	-80(%rbp), %r8
	movl	$-1, %ecx
	movq	%r13, %rdx
.L2738:
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal4wasm21MemoryAccessImmediateILNS1_7Decoder12ValidateFlagE1EEC2EPS3_PKhj.constprop.0
	movl	-72(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L2554
.L2643:
	leaq	.LC73(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L2737
.L2647:
	leaq	-80(%rbp), %r9
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm18TableCopyImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movl	-64(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L2554
.L2648:
	leaq	-76(%rbp), %rdx
	leaq	2(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC41(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-76(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L2554
.L2649:
	leaq	-80(%rbp), %r9
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm18TableInitImmediateILNS1_7Decoder12ValidateFlagE1EEC1EPS3_PKh
	movl	-68(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L2554
.L2650:
	leaq	2(%rsi), %rsi
	cmpq	%rsi, %rcx
	jb	.L2676
	cmpl	%esi, %ecx
	je	.L2676
	movzbl	2(%r8), %ecx
	testl	%ecx, %ecx
	je	.L2682
	leaq	.LC56(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2682:
	movl	$3, %r12d
	jmp	.L2554
.L2651:
	leaq	2(%rsi), %rsi
	cmpq	%rsi, %rcx
	jb	.L2670
	cmpl	%esi, %ecx
	je	.L2670
	movzbl	2(%r8), %r9d
	testl	%r9d, %r9d
	je	.L2672
	movl	%r9d, %ecx
	leaq	.LC56(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	-96(%rbp), %r8
	movq	24(%rdi), %rcx
.L2672:
	leaq	3(%r8), %rsi
	cmpq	%rcx, %rsi
	ja	.L2673
	cmpl	%esi, %ecx
	je	.L2673
	movzbl	3(%r8), %ecx
	testl	%ecx, %ecx
	je	.L2675
	leaq	.LC56(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2675:
	movl	$4, %r12d
	jmp	.L2554
.L2652:
	leaq	-76(%rbp), %rdx
	leaq	2(%rsi), %rsi
	xorl	%r8d, %r8d
	leaq	.LC54(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	-76(%rbp), %eax
	leal	2(%rax), %r12d
	jmp	.L2554
.L2653:
	leaq	2(%rsi), %r14
	cmpq	%r14, %rcx
	ja	.L2746
	leaq	.LC54(%rip), %rcx
	movq	%r14, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	movl	$3, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rcx
.L2656:
	cmpq	%rcx, %r14
	ja	.L2667
	cmpl	%r14d, %ecx
	je	.L2667
	movzbl	1(%r13), %ecx
	testl	%ecx, %ecx
	je	.L2554
	leaq	.LC56(%rip), %rdx
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	24(%rdi), %rax
	leaq	1(%rsi), %r12
	cmpq	%rax, %r12
	ja	.L2679
	cmpl	%r12d, %eax
	je	.L2679
	movzbl	1(%rsi), %eax
	orb	$-3, %ah
	subl	$64768, %eax
	cmpl	$209, %eax
	ja	.L2680
	leaq	.L2681(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	.align 4
	.align 4
.L2681:
	.long	.L2684-.L2681
	.long	.L2684-.L2681
	.long	.L2680-.L2681
	.long	.L2683-.L2681
	.long	.L2737-.L2681
	.long	.L2682-.L2681
	.long	.L2680-.L2681
	.long	.L2682-.L2681
	.long	.L2737-.L2681
	.long	.L2682-.L2681
	.long	.L2680-.L2681
	.long	.L2682-.L2681
	.long	.L2737-.L2681
	.long	.L2682-.L2681
	.long	.L2682-.L2681
	.long	.L2737-.L2681
	.long	.L2682-.L2681
	.long	.L2682-.L2681
	.long	.L2737-.L2681
	.long	.L2682-.L2681
	.long	.L2682-.L2681
	.long	.L2737-.L2681
	.long	.L2682-.L2681
	.long	.L2682-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2680-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.long	.L2737-.L2681
	.section	.text._ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
.L2680:
	leaq	.LC75(%rip), %rdx
	movq	%r8, %rsi
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L2554
.L2679:
	leaq	.LC74(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-88(%rbp), %rdi
.L2684:
	leaq	-80(%rbp), %r8
	movl	$-1, %ecx
	movq	%r12, %rdx
	jmp	.L2738
.L2683:
	movl	$18, %r12d
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2686:
	subl	$65040, %eax
	cmpl	$62, %eax
	ja	.L2687
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2740:
	cmpb	$0, 1(%r8)
	movl	$2, %r12d
	jns	.L2554
	leaq	2(%r8), %rsi
	cmpq	%rsi, %rax
	jbe	.L2635
	cmpb	$0, 2(%r8)
	movl	$3, %r12d
	jns	.L2554
	leaq	3(%r8), %rsi
	cmpq	%rsi, %rax
	jbe	.L2635
	cmpb	$0, 3(%r8)
	movl	$4, %r12d
	jns	.L2554
	leaq	4(%r8), %rsi
	cmpq	%rsi, %rax
	jbe	.L2635
	cmpb	$0, 4(%r8)
	movl	$5, %r12d
	jns	.L2554
	leaq	5(%r8), %r13
	cmpq	%r13, %rax
	jbe	.L2705
	movzbl	5(%r8), %ebx
	testb	%bl, %bl
	js	.L2638
.L2639:
	addl	$1, %r12d
	andb	$-8, %bl
	je	.L2554
	cmpb	$120, %bl
	je	.L2554
	leaq	.LC27(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2743:
	movzbl	1(%rsi), %eax
	leaq	2(%rsi), %r9
	movl	%eax, %ebx
	andl	$127, %ebx
	testb	%al, %al
	jns	.L2620
	leaq	2(%rsi), %r12
	cmpq	%r12, %rcx
	jbe	.L2621
	movzbl	2(%rsi), %edx
	leaq	3(%rsi), %r9
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ebx
	testb	%dl, %dl
	jns	.L2620
	leaq	3(%rsi), %r12
	cmpq	%r12, %rcx
	jbe	.L2621
	movzbl	3(%rsi), %edx
	leaq	4(%rsi), %r9
	movl	%edx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ebx
	testb	%dl, %dl
	jns	.L2620
	leaq	4(%rsi), %r12
	cmpq	%r12, %rcx
	jbe	.L2621
	movzbl	4(%rsi), %edx
	leaq	5(%rsi), %r9
	movl	%edx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %ebx
	testb	%dl, %dl
	jns	.L2620
	leaq	5(%rsi), %r14
	cmpq	%r14, %rcx
	setbe	%al
	ja	.L2747
	xorl	%r15d, %r15d
	movl	$4, %r12d
.L2689:
	leaq	.LC70(%rip), %rcx
	movq	%r14, %rsi
	xorl	%eax, %eax
	movq	%r8, -96(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%rdi, -88(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdi
.L2628:
	andb	$-16, %r15b
	leaq	1(%r8,%r12), %r9
	je	.L2620
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rsi
	movq	%r9, -96(%rbp)
	xorl	%ebx, %ebx
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdi
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2745:
	cmpb	$0, 0(%r13)
	js	.L2592
.L2734:
	leal	2(%rbx), %r12d
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2744:
	cmpb	$0, 1(%rsi)
	leaq	2(%rsi), %r13
	js	.L2580
.L2727:
	movl	$2, %r12d
	movl	$1, %ebx
	jmp	.L2581
	.p2align 4,,10
	.p2align 3
.L2742:
	movzbl	1(%rsi), %esi
	movl	$2, %r12d
	movl	$1, %edx
	movl	%esi, %ecx
	movl	%esi, %r9d
	andl	$127, %ecx
	andl	$127, %r9d
	testb	%sil, %sil
	js	.L2748
.L2603:
	cmpb	$1, %cl
	jne	.L2613
	leaq	1(%r8,%rdx), %rsi
	addl	$1, %r12d
	cmpq	%rax, %rsi
	ja	.L2616
	cmpl	%esi, %eax
	je	.L2616
	movzbl	(%rsi), %ecx
	subl	$104, %ecx
	cmpb	$23, %cl
	ja	.L2617
	movl	$1, %eax
	salq	%cl, %rax
	testl	$16253313, %eax
	jne	.L2554
.L2617:
	leaq	.LC69(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2554
.L2748:
	leaq	2(%r8), %rsi
	cmpq	%rsi, %rax
	ja	.L2749
	leaq	.LC66(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	jmp	.L2613
.L2592:
	leaq	1(%r13), %rsi
	cmpq	%rax, %rsi
	jb	.L2750
	leaq	.LC64(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2734
.L2580:
	cmpq	%r13, %rax
	ja	.L2751
	xorl	%eax, %eax
	leaq	.LC61(%rip), %rcx
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rax
	jmp	.L2727
.L2640:
	leaq	.LC55(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2737
.L2616:
	leaq	.LC68(%rip), %rdx
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-88(%rbp), %rdi
	jmp	.L2617
.L2750:
	cmpb	$0, 1(%r13)
	js	.L2595
.L2735:
	leal	3(%rbx), %r12d
	jmp	.L2554
.L2749:
	movzbl	2(%r8), %edx
	movl	%edx, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%r9d, %ecx
	testb	%dl, %dl
	js	.L2605
	movl	$3, %r12d
	movl	$2, %edx
	jmp	.L2603
.L2751:
	cmpb	$0, 2(%rsi)
	leaq	3(%rsi), %r13
	js	.L2583
.L2728:
	movl	$3, %r12d
	movl	$2, %ebx
	jmp	.L2581
.L2635:
	leaq	.LC71(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2554
.L2621:
	leaq	.LC70(%rip), %rcx
	movq	%r12, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	%r12, %r9
	jmp	.L2620
.L2644:
	leaq	.LC60(%rip), %rdx
	movl	$2, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L2554
.L2583:
	cmpq	%r13, %rax
	ja	.L2752
	xorl	%eax, %eax
	leaq	.LC61(%rip), %rcx
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rax
	jmp	.L2728
.L2595:
	leaq	2(%r13), %rsi
	cmpq	%rax, %rsi
	jb	.L2753
	leaq	.LC64(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2735
.L2605:
	leaq	3(%r8), %rsi
	cmpq	%rsi, %rax
	ja	.L2754
	leaq	.LC66(%rip), %rcx
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	movl	$3, %r12d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	jmp	.L2613
.L2746:
	cmpb	$0, 2(%rsi)
	js	.L2655
	movq	%r14, %r13
	movl	$4, %r12d
	leaq	3(%rsi), %r14
	jmp	.L2656
.L2752:
	cmpb	$0, 3(%rsi)
	leaq	4(%rsi), %r13
	js	.L2585
.L2730:
	movl	$4, %r12d
	movl	$3, %ebx
	jmp	.L2581
.L2754:
	movzbl	3(%r8), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%sil, %sil
	js	.L2607
	movl	$4, %r12d
	movl	$3, %edx
	jmp	.L2603
.L2753:
	cmpb	$0, 2(%r13)
	js	.L2597
.L2736:
	leal	4(%rbx), %r12d
	jmp	.L2554
.L2655:
	leaq	3(%rsi), %rbx
	cmpq	%rbx, %rcx
	ja	.L2755
	leaq	.LC54(%rip), %rcx
	movq	%rbx, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%r14, %r13
	movl	$4, %r12d
	movq	%rbx, %r14
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rcx
	jmp	.L2656
.L2676:
	leaq	.LC55(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2682
.L2673:
	leaq	.LC55(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2675
.L2670:
	leaq	.LC55(%rip), %rdx
	movq	%r8, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-88(%rbp), %rdi
	movq	-96(%rbp), %r8
	movq	24(%rdi), %rcx
	jmp	.L2672
.L2667:
	leaq	.LC55(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2554
.L2585:
	cmpq	%r13, %rax
	ja	.L2756
	xorl	%eax, %eax
	leaq	.LC61(%rip), %rcx
	movq	%r13, %rsi
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rax
	jmp	.L2730
.L2607:
	leaq	4(%r8), %rsi
	cmpq	%rsi, %rax
	ja	.L2757
	leaq	.LC66(%rip), %rcx
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	movl	$4, %r12d
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	jmp	.L2613
.L2597:
	leaq	3(%r13), %rsi
	cmpq	%rax, %rsi
	jb	.L2758
	leaq	.LC64(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2736
.L2755:
	cmpb	$0, 3(%rsi)
	js	.L2658
	movq	%rbx, %r13
	leaq	4(%rsi), %r14
	movl	$5, %r12d
	jmp	.L2656
.L2758:
	cmpb	$0, 3(%r13)
	js	.L2599
	leal	5(%rbx), %r12d
	jmp	.L2554
.L2739:
	call	__stack_chk_fail@PLT
.L2756:
	cmpb	$0, 4(%rsi)
	js	.L2587
	leaq	5(%rsi), %r13
	movl	$5, %r12d
	movl	$4, %ebx
	jmp	.L2581
.L2757:
	movzbl	4(%r8), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%ecx, %edx
	testb	%sil, %sil
	js	.L2609
	movl	%edx, %ecx
	movl	$5, %r12d
	movl	$4, %edx
	jmp	.L2603
.L2658:
	leaq	4(%rsi), %r14
	cmpq	%r14, %rcx
	ja	.L2759
	leaq	.LC54(%rip), %rcx
	movq	%r14, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%rbx, %r13
	movl	$5, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rcx
	jmp	.L2656
.L2609:
	leaq	5(%r8), %r12
	cmpq	%r12, %rax
	ja	.L2760
	movq	%r12, %rsi
	leaq	.LC66(%rip), %rcx
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	movl	$5, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	jmp	.L2613
.L2599:
	leaq	4(%r13), %r14
	cmpq	%rax, %r14
	jnb	.L2696
	movzbl	4(%r13), %ebx
	movl	$5, %r13d
	testb	%bl, %bl
	js	.L2600
.L2601:
	addl	%r13d, %r12d
	andb	$-16, %bl
	je	.L2554
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2554
.L2587:
	leaq	5(%rsi), %r14
	cmpq	%r14, %rax
	jbe	.L2694
	movzbl	5(%rsi), %r15d
	movl	$5, %ebx
	testb	%r15b, %r15b
	js	.L2588
.L2589:
	movl	%ebx, %eax
	andb	$-16, %r15b
	leal	1(%rbx), %r12d
	leaq	1(%r8,%rax), %r13
	je	.L2729
	leaq	.LC27(%rip), %rdx
	movq	%r14, %rsi
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-88(%rbp), %rdi
.L2729:
	movq	24(%rdi), %rax
	jmp	.L2581
.L2694:
	xorl	%r15d, %r15d
	movl	$4, %ebx
.L2588:
	leaq	.LC61(%rip), %rcx
	movq	%r14, %rsi
	xorl	%eax, %eax
	movq	%r8, -96(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdi
	jmp	.L2589
.L2705:
	xorl	%ebx, %ebx
	movl	$4, %r12d
.L2638:
	leaq	.LC71(%rip), %rcx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	jmp	.L2639
.L2759:
	cmpb	$0, 4(%rsi)
	js	.L2660
	movq	%r14, %r13
	movl	$6, %r12d
	leaq	5(%rsi), %r14
	jmp	.L2656
.L2747:
	movzbl	5(%rsi), %r15d
	movl	%r15d, %edx
	sall	$28, %edx
	orl	%edx, %ebx
	testb	%r15b, %r15b
	js	.L2713
	movl	$5, %r12d
	testb	%al, %al
	je	.L2628
.L2713:
	movl	$5, %r12d
	jmp	.L2689
.L2696:
	xorl	%ebx, %ebx
	movl	$4, %r13d
.L2600:
	leaq	.LC64(%rip), %rcx
	movq	%r14, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	jmp	.L2601
.L2760:
	movzbl	5(%r8), %ebx
	testb	%bl, %bl
	jns	.L2611
	xorl	%eax, %eax
	leaq	.LC66(%rip), %rcx
	movq	%r12, %rsi
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	andb	$-16, %bl
	movq	-88(%rbp), %rdi
	je	.L2761
.L2612:
	movq	%r12, %rsi
	leaq	.LC27(%rip), %rdx
	movq	%rdi, -88(%rbp)
	movl	$6, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-88(%rbp), %rdi
	jmp	.L2613
.L2660:
	leaq	5(%rsi), %rbx
	cmpq	%rbx, %rcx
	ja	.L2762
	leaq	.LC54(%rip), %rcx
	movq	%rbx, %rsi
	xorl	%eax, %eax
	movq	%rdi, -88(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%r14, %r13
	movl	$6, %r12d
	movq	%rbx, %r14
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %rdi
	movq	24(%rdi), %rcx
	jmp	.L2656
.L2611:
	movl	%ebx, %ecx
	sall	$28, %ecx
	orl	%edx, %ecx
	andb	$-16, %bl
	jne	.L2612
	movl	$6, %r12d
	movl	$5, %edx
	jmp	.L2603
.L2761:
	movl	$6, %r12d
	jmp	.L2613
.L2762:
	cmpb	$0, 5(%rsi)
	js	.L2662
	movq	%rbx, %r13
	leaq	6(%rsi), %r14
	movl	$7, %r12d
	jmp	.L2656
.L2662:
	leaq	6(%rsi), %r15
	cmpq	%r15, %rcx
	jbe	.L2708
	movzbl	6(%rsi), %ebx
	movl	$5, %r13d
	testb	%bl, %bl
	js	.L2663
.L2664:
	leal	3(%r13), %r12d
	leaq	1(%r8,%r13), %r13
	leaq	1(%r13), %r14
	andb	$-8, %bl
	je	.L2731
	cmpb	$120, %bl
	je	.L2731
	leaq	.LC27(%rip), %rdx
	movq	%r15, %rsi
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-88(%rbp), %rdi
.L2731:
	movq	24(%rdi), %rcx
	jmp	.L2656
.L2708:
	xorl	%ebx, %ebx
	movl	$4, %r13d
.L2663:
	leaq	.LC54(%rip), %rcx
	movq	%r15, %rsi
	xorl	%eax, %eax
	movq	%r8, -96(%rbp)
	leaq	.LC26(%rip), %rdx
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rdi
	jmp	.L2664
	.cfi_endproc
.LFE24944:
	.size	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0, .-_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE, @function
_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE:
.LFB19642:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r14
	movq	128(%rsi), %rdi
	movq	%rdx, -72(%rbp)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L2871
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdi)
.L2765:
	movl	$2, (%r12)
	movq	48(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 48(%r12)
	movq	8(%r14), %rax
	movq	%rax, 8(%r12)
	movq	16(%r14), %rax
	movq	%rax, 16(%r12)
	movdqu	24(%r14), %xmm1
	movups	%xmm1, 24(%r12)
	movq	40(%r14), %rax
	movq	%rax, 40(%r12)
	movq	-72(%rbp), %rax
	movups	%xmm0, 8(%r14)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r14)
	movl	$1, (%r14)
	movq	$0, 48(%r14)
	movq	$0, 40(%r14)
	movq	%r12, 104(%rax)
	movq	8(%r12), %rsi
	movl	$3, (%r12)
	movq	8(%r13), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder4LoopEPNS1_4NodeE@PLT
	movl	$1, %esi
	movq	%rax, 8(%r12)
	movq	%rax, %rcx
	movq	8(%r13), %rdi
	leaq	16(%r12), %rax
	movq	%rax, %rdx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder9EffectPhiEjPPNS1_4NodeES4_@PLT
	movq	8(%r12), %rdx
	movq	%rax, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder13TerminateLoopEPNS1_4NodeES4_@PLT
	movq	120(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2838
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	movl	%eax, %r14d
	leal	1(%rax), %ecx
.L2766:
	movq	16(%rbx), %r15
	cmpq	24(%rbx), %r15
	jnb	.L2767
	cmpb	$3, (%r15)
	jne	.L2767
	movq	128(%rbx), %rdi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	$15, %rax
	jbe	.L2872
	leaq	16(%r9), %rax
	movq	%rax, 16(%rdi)
.L2770:
	movl	%ecx, (%r9)
	cmpl	$64, %ecx
	jle	.L2873
	movl	%r14d, %eax
	movq	$0, 8(%r9)
	sarl	$6, %eax
	addl	$1, %eax
	movl	%eax, 4(%r9)
	cltq
	movq	24(%rdi), %rcx
	leaq	0(,%rax,8), %rsi
	movq	16(%rdi), %rax
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L2874
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2774:
	movl	4(%r9), %ecx
	movq	%rax, 8(%r9)
	cmpl	$1, %ecx
	je	.L2875
	testl	%ecx, %ecx
	jle	.L2772
	movq	$0, (%rax)
	cmpl	$1, 4(%r9)
	jle	.L2772
	movl	$8, %ecx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2777:
	movq	8(%r9), %rdx
	addl	$1, %eax
	movq	$0, (%rdx,%rcx)
	addq	$8, %rcx
	cmpl	%eax, 4(%r9)
	jg	.L2777
.L2772:
	movq	24(%rbx), %rax
	movq	56(%rbx), %rsi
	cmpq	%rax, %r15
	jnb	.L2778
	testl	%r14d, %r14d
	leal	63(%r14), %ecx
	movq	%r12, -88(%rbp)
	movq	%rbx, %r12
	cmovns	%r14d, %ecx
	movq	%r15, %rbx
	sarl	$6, %ecx
	movslq	%ecx, %rcx
	leaq	0(,%rcx,8), %rdi
	movq	%rdi, -64(%rbp)
	movl	%r14d, %edi
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%r14,%rdi), %ecx
	andl	$63, %ecx
	subl	%edi, %ecx
	movl	$1, %edi
	movq	%rdi, %rdx
	salq	%cl, %rdx
	movl	%r14d, %ecx
	xorl	%r14d, %r14d
	salq	%cl, %rdi
	movq	%rdx, -80(%rbp)
	movl	%r14d, %r15d
	movq	%r9, %r14
	movq	%rdi, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L2802:
	testq	%rsi, %rsi
	jne	.L2876
	cmpb	$64, (%rbx)
	ja	.L2781
	movzbl	(%rbx), %edx
	leaq	.L2783(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE,"a",@progbits
	.align 4
	.align 4
.L2783:
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2786-.L2783
	.long	.L2786-.L2783
	.long	.L2786-.L2783
	.long	.L2781-.L2783
	.long	.L2786-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2785-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2782-.L2783
	.long	.L2782-.L2783
	.long	.L2782-.L2783
	.long	.L2782-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2784-.L2783
	.long	.L2784-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2781-.L2783
	.long	.L2782-.L2783
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE
	.p2align 4,,10
	.p2align 3
.L2767:
	cmpq	$0, 56(%rbx)
	jne	.L2868
	movq	168(%rbx), %rax
	movl	%eax, %r14d
	notl	%r14d
	addl	176(%rbx), %r14d
	js	.L2823
	movslq	%r14d, %r14
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	168(%rbx), %rax
.L2824:
	movq	48(%r12), %rdx
	movzbl	(%rax,%r14), %esi
	movq	8(%r13), %rdi
	movq	8(%r12), %r8
	leaq	(%rdx,%r14,8), %r15
	movl	$1, %edx
	subq	$1, %r14
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	%rax, (%r15)
	testl	%r14d, %r14d
	jns	.L2877
.L2823:
	movq	8(%r13), %rdi
	movq	8(%r12), %rdx
	leaq	24(%r12), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder27PrepareInstanceCacheForLoopEPNS1_22WasmInstanceCacheNodesEPNS1_4NodeE@PLT
	movq	128(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$55, %rax
	jbe	.L2878
	leaq	56(%r14), %rax
	movq	%rax, 16(%rdi)
.L2826:
	movq	8(%r12), %rax
	movq	176(%rbx), %rdx
	movq	%r14, %r15
	subl	168(%rbx), %edx
	movq	%rax, 8(%r14)
	movq	16(%r12), %rax
	salq	$3, %rdx
	movl	$2, (%r14)
	movq	%rax, 16(%r14)
	jne	.L2879
	movq	$0, 48(%r14)
.L2830:
	movdqu	24(%r12), %xmm3
	movups	%xmm3, 24(%r14)
.L2869:
	movq	40(%r12), %rax
	leaq	16(%r14), %rdx
	movq	%rax, 40(%r14)
	leaq	8(%r14), %rax
	movq	8(%r13), %rdi
	movq	16(%rbx), %rsi
	movq	%rax, %rcx
	subq	8(%rbx), %rsi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder10StackCheckEiPPNS1_4NodeES5_@PLT
	movq	-64(%rbp), %rax
.L2780:
	movq	8(%r13), %rdx
	movq	-56(%rbp), %rdi
	movq	%r15, 0(%r13)
	movq	%rax, 24(%rdx)
	movq	8(%r13), %rax
	movq	%rdi, 32(%rax)
	movq	8(%r13), %rdx
	leaq	24(%r15), %rax
	movq	%rax, 40(%rdx)
	movq	0(%r13), %rax
	cmpl	$3, (%rax)
	jne	.L2831
	movl	$2, (%rax)
.L2831:
	cmpq	$0, 56(%rbx)
	jne	.L2763
	movq	-72(%rbp), %r14
	movl	24(%r14), %eax
	testl	%eax, %eax
	je	.L2763
	xorl	%ebx, %ebx
	leaq	48(%r14), %r12
	.p2align 4,,10
	.p2align 3
.L2836:
	cmpl	$1, %eax
	je	.L2880
	movl	%ebx, %eax
	movq	8(%r13), %rdi
	addl	$1, %ebx
	leaq	(%rax,%rax,2), %rdx
	movq	32(%r14), %rax
	leaq	(%rax,%rdx,8), %r15
	movq	104(%r14), %rax
	movl	$1, %edx
	movzbl	8(%r15), %esi
	leaq	16(%r15), %rcx
	movq	8(%rax), %r8
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	%rax, 16(%r15)
	movl	24(%r14), %eax
	cmpl	%ebx, %eax
	ja	.L2836
.L2763:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2876:
	.cfi_restore_state
	movq	%r12, %rbx
	movq	-88(%rbp), %r12
.L2868:
	leaq	8(%r12), %rax
	movq	%r12, %r15
	jmp	.L2780
	.p2align 4,,10
	.p2align 3
.L2880:
	movq	104(%r14), %rax
	movzbl	40(%r14), %esi
	movq	%r12, %rcx
	addl	$1, %ebx
	movq	8(%r13), %rdi
	movl	$1, %edx
	movq	8(%rax), %r8
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	%rax, 48(%r14)
	movl	24(%r14), %eax
	cmpl	%ebx, %eax
	ja	.L2836
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2838:
	.cfi_restore_state
	movl	$1, %ecx
	xorl	%r14d, %r14d
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2782:
	cmpl	$1, 4(%r14)
	movq	8(%r14), %rax
	je	.L2881
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rcx
	orq	%rcx, (%rax,%rsi)
	.p2align 4,,10
	.p2align 3
.L2781:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	movl	%eax, %r8d
.L2866:
	movq	56(%r12), %rsi
.L2799:
	testl	%r15d, %r15d
	jle	.L2863
.L2787:
	movq	24(%r12), %rax
	addq	%r8, %rbx
	cmpq	%rbx, %rax
	ja	.L2802
.L2863:
	movq	%r12, %rbx
	movq	-88(%rbp), %r12
	movq	%r14, %r9
.L2778:
	testq	%rsi, %rsi
	jne	.L2868
	movq	120(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2844
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	cltd
	movl	%eax, -88(%rbp)
	shrl	$26, %edx
	leal	(%rdx,%rax), %r14d
	andl	$63, %r14d
	subl	%edx, %r14d
	movl	%r14d, -80(%rbp)
.L2806:
	movq	176(%rbx), %r10
	subq	168(%rbx), %r10
	movl	%r10d, %r15d
	movl	4(%r9), %ecx
	movq	8(%r9), %rdx
	subl	$1, %r15d
	js	.L2807
	movq	%rbx, %rax
	movslq	%r15d, %r14
	movq	%r9, %rbx
	movq	%rax, %r9
	.p2align 4,,10
	.p2align 3
.L2813:
	movq	%rdx, %rax
	cmpl	$1, %ecx
	je	.L2809
	movl	%r15d, %eax
	sarl	$6, %eax
	cltq
	movq	(%rdx,%rax,8), %rax
.L2809:
	btq	%r15, %rax
	jnc	.L2810
	movq	48(%r12), %rax
	movq	8(%r13), %rdi
	movl	$1, %edx
	movq	%r9, -64(%rbp)
	movq	8(%r12), %r8
	subl	$1, %r15d
	leaq	(%rax,%r14,8), %rcx
	movq	168(%r9), %rax
	movq	%rcx, -56(%rbp)
	movzbl	(%rax,%r14), %esi
	subq	$1, %r14
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	-56(%rbp), %rcx
	cmpl	$-1, %r15d
	movq	-64(%rbp), %r9
	movq	%rax, (%rcx)
	movl	4(%rbx), %ecx
	movq	8(%rbx), %rdx
	jne	.L2813
.L2867:
	movq	%r9, %rbx
.L2807:
	cmpl	$1, %ecx
	je	.L2815
	movl	-88(%rbp), %edi
	leal	63(%rdi), %eax
	testl	%edi, %edi
	movl	%edi, %r15d
	cmovs	%eax, %r15d
	sarl	$6, %r15d
	movslq	%r15d, %rax
	movq	(%rdx,%rax,8), %rdx
.L2815:
	movl	-80(%rbp), %eax
	btq	%rax, %rdx
	jc	.L2882
.L2816:
	movq	128(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$55, %rax
	jbe	.L2883
	leaq	56(%r14), %rax
	movq	%rax, 16(%rdi)
.L2818:
	movq	8(%r12), %rax
	movq	176(%rbx), %rdx
	movq	%r14, %r15
	subl	168(%rbx), %edx
	movq	%rax, 8(%r14)
	movq	16(%r12), %rax
	salq	$3, %rdx
	movl	$2, (%r14)
	movq	%rax, 16(%r14)
	jne	.L2884
	movq	$0, 48(%r14)
.L2822:
	movdqu	24(%r12), %xmm2
	movups	%xmm2, 24(%r14)
	jmp	.L2869
	.p2align 4,,10
	.p2align 3
.L2786:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12OpcodeLengthEPS3_PKh.constprop.0
	movq	56(%r12), %rsi
	movl	%eax, %r8d
	jmp	.L2787
	.p2align 4,,10
	.p2align 3
.L2784:
	leaq	1(%rbx), %rsi
	cmpq	%rax, %rsi
	jb	.L2885
	movl	$1, %r8d
	leaq	.LC65(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movl	%r8d, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	-104(%rbp), %r8d
.L2796:
	movl	(%r14), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jle	.L2866
.L2837:
	cmpl	$1, 4(%r14)
	movq	8(%r14), %rax
	je	.L2886
	movl	%ecx, %edx
	movl	$1, %esi
	sarl	$6, %edx
	salq	%cl, %rsi
	movslq	%edx, %rdx
	orq	%rsi, (%rax,%rdx,8)
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2875:
	movq	$0, 8(%r9)
	jmp	.L2772
	.p2align 4,,10
	.p2align 3
.L2785:
	subl	$1, %r15d
	movl	$1, %r8d
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2810:
	subl	$1, %r15d
	subq	$1, %r14
	cmpl	$-1, %r15d
	jne	.L2813
	jmp	.L2867
	.p2align 4,,10
	.p2align 3
.L2885:
	movzbl	1(%rbx), %edx
	movl	%edx, %ecx
	andl	$127, %ecx
	testb	%dl, %dl
	jns	.L2789
	leaq	2(%rbx), %rsi
	cmpq	%rax, %rsi
	jb	.L2887
	leaq	.LC65(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$2, %r8d
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2881:
	orq	-96(%rbp), %rax
	movq	%rax, 8(%r14)
	jmp	.L2781
	.p2align 4,,10
	.p2align 3
.L2789:
	movl	$2, %r8d
.L2791:
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L2866
	cmpl	%ecx, %eax
	jbe	.L2866
	jmp	.L2837
	.p2align 4,,10
	.p2align 3
.L2873:
	movl	$1, 4(%r9)
	movq	$0, 8(%r9)
	jmp	.L2772
	.p2align 4,,10
	.p2align 3
.L2884:
	movq	128(%rbx), %r8
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L2888
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L2821:
	movq	%rdi, 48(%r14)
	movq	48(%r12), %rsi
	call	memcpy@PLT
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2882:
	movq	8(%r12), %rdx
	movq	8(%r13), %rdi
	leaq	24(%r12), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder27PrepareInstanceCacheForLoopEPNS1_22WasmInstanceCacheNodesEPNS1_4NodeE@PLT
	jmp	.L2816
	.p2align 4,,10
	.p2align 3
.L2844:
	movl	$0, -80(%rbp)
	movl	$0, -88(%rbp)
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2887:
	movzbl	2(%rbx), %esi
	movl	%esi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %ecx
	testb	%sil, %sil
	jns	.L2839
	leaq	3(%rbx), %rsi
	cmpq	%rax, %rsi
	jnb	.L2792
	movzbl	3(%rbx), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%sil, %sil
	jns	.L2840
	leaq	4(%rbx), %rsi
	cmpq	%rax, %rsi
	jb	.L2889
	leaq	.LC65(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$4, %r8d
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2871:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	128(%rbx), %r8
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L2890
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L2829:
	movq	%rdi, 48(%r14)
	movq	48(%r12), %rsi
	call	memcpy@PLT
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2886:
	btsq	%rcx, %rax
	movq	%rax, 8(%r14)
	jmp	.L2866
.L2792:
	leaq	.LC65(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$3, %r8d
	jmp	.L2796
.L2839:
	movl	$3, %r8d
	jmp	.L2791
.L2872:
	movl	$16, %esi
	movl	%ecx, -80(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdi
	movl	-80(%rbp), %ecx
	movq	%rax, %r9
	jmp	.L2770
.L2883:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2818
.L2878:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2826
.L2874:
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r9
	jmp	.L2774
.L2840:
	movl	$4, %r8d
	jmp	.L2791
.L2888:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2821
.L2890:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2829
.L2889:
	movzbl	4(%rbx), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%sil, %sil
	jns	.L2841
	leaq	5(%rbx), %rsi
	cmpq	%rax, %rsi
	jnb	.L2842
	movzbl	5(%rbx), %r10d
	movl	%r10d, %eax
	sall	$28, %eax
	orl	%eax, %ecx
	testb	%r10b, %r10b
	js	.L2891
	movl	$6, %r8d
.L2795:
	andl	$240, %r10d
	je	.L2791
	leaq	.LC27(%rip), %rdx
	movq	%r12, %rdi
	movl	%r8d, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-104(%rbp), %r8d
	jmp	.L2796
.L2841:
	movl	$5, %r8d
	jmp	.L2791
.L2842:
	movl	$5, %r8d
	xorl	%r10d, %r10d
.L2794:
	leaq	.LC65(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r10b, -109(%rbp)
	leaq	.LC26(%rip), %rdx
	movl	%r8d, -108(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-104(%rbp), %rsi
	movl	-108(%rbp), %r8d
	xorl	%ecx, %ecx
	movzbl	-109(%rbp), %r10d
	jmp	.L2795
.L2891:
	movl	$6, %r8d
	jmp	.L2794
	.cfi_endproc
.LFE19642:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE, .-_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"../deps/v8/src/wasm/graph-builder-interface.cc:928"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1,"aMS",@progbits,1
.LC79:
	.string	"function body end < start"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC82:
	.string	"block type index %u out of bounds (%zu signatures)"
	.align 8
.LC83:
	.string	"Invalid opcode (enable with --experimental-wasm-eh)"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC84:
	.string	"Invalid exception index: %u"
.LC85:
	.string	"catch does not match any try"
.LC86:
	.string	"catch already present for try"
.LC87:
	.string	"invalid branch depth: %u"
.LC88:
	.string	"else does not match any if"
.LC89:
	.string	"else does not match an if"
.LC90:
	.string	"else already present for if"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC91:
	.string	"end does not match any if, try, or block"
	.align 8
.LC92:
	.string	"missing catch or catch-all in try"
	.align 8
.LC93:
	.string	"start-arity and end-arity of one-armed if must match"
	.align 8
.LC94:
	.string	"trailing code after function end"
	.align 8
.LC95:
	.string	"select without type is only valid for value type inputs"
	.align 8
.LC96:
	.string	"Invalid opcode (enable with --experimental-wasm-anyref)"
	.align 8
.LC97:
	.string	"invalid table count (> max function size): %u"
	.align 8
.LC98:
	.string	"improper branch in br_table target %u (depth %u)"
	.align 8
.LC99:
	.string	"inconsistent arity in br_table target %u (previous was %zu, this one is %u)"
	.align 8
.LC100:
	.string	"inconsistent type in br_table target %u (previous was %s, this one is %s)"
	.align 8
.LC101:
	.string	"expected %u elements on the stack for branch to @%d, found %u"
	.align 8
.LC102:
	.string	"expected %u elements on the stack for return, found %u"
	.align 8
.LC103:
	.string	"type error in return[%u] (expected %s, got %s)"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC104:
	.string	"immf32"
.LC105:
	.string	"immf64"
.LC106:
	.string	"invalid function index: %u"
.LC107:
	.string	"invalid local index: %u"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC108:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC109:
	.string	"invalid global index: %u"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC110:
	.string	"immutable global #%u cannot be assigned"
	.align 8
.LC111:
	.string	"grow_memory is not supported for asmjs modules"
	.align 8
.LC112:
	.string	"function table has to exist to execute call_indirect"
	.align 8
.LC113:
	.string	"table of call_indirect must be of type funcref"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC114:
	.string	"invalid signature index: #%u"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC115:
	.string	"Invalid opcode (enable with --experimental-wasm-return_call)"
	.align 8
.LC116:
	.string	"tail call return types mismatch"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC117:
	.string	"%s: %s"
.LC118:
	.string	"numeric index"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC119:
	.string	"Invalid opcode (enable with --experimental-wasm-sat_f2i_conversions)"
	.align 8
.LC120:
	.string	"Invalid opcode (enable with --experimental-wasm-bulk_memory)"
	.align 8
.LC121:
	.string	"Invalid opcode (enable with --experimental-wasm-simd)"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC122:
	.string	"simd index"
.LC123:
	.string	"shuffle"
.LC124:
	.string	"invalid shuffle mask"
.LC125:
	.string	"invalid simd opcode"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC126:
	.string	"Invalid opcode (enable with --experimental-wasm-threads)"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC127:
	.string	"invalid atomic opcode"
.LC128:
	.string	"atomic index"
.LC129:
	.string	"zero"
.LC130:
	.string	"invalid atomic operand"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC131:
	.string	"Atomic opcodes used without shared memory"
	.align 8
.LC132:
	.string	"Invalid opcode (enable with --experimental-wasm-se)"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.1
.LC133:
	.string	"Invalid opcode"
.LC134:
	.string	"Beyond end of code"
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE.str1.8
	.align 8
.LC135:
	.string	"unterminated control structure"
	.align 8
.LC136:
	.string	"function body must end with \"end\" opcode"
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.type	_ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE, @function
_ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE:
.LFB19766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1280(%rbp), %r15
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r15, %xmm5
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	.LC78(%rip), %rdx
	subq	$1576, %rsp
	movq	24(%rbp), %rax
	movq	%rdi, -1432(%rbp)
	movq	%r15, %rdi
	movq	%r8, -1456(%rbp)
	movq	16(%rbp), %r12
	movq	%rax, -1448(%rbp)
	movq	%r9, -1440(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	leaq	-160(%rbp), %rcx
	movq	%r15, -1464(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -1488(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movl	8(%r12), %ecx
	movq	%r15, -128(%rbp)
	movq	-1440(%rbp), %xmm1
	movq	16(%r12), %rax
	movl	$0, -280(%rbp)
	movl	%ecx, -288(%rbp)
	movq	24(%r12), %rdx
	leaq	-256(%rbp), %rcx
	movq	%rcx, -1472(%rbp)
	movq	-1456(%rbp), %rdi
	movhps	(%r12), %xmm1
	movq	%rcx, -272(%rbp)
	movq	(%rbx), %rcx
	movq	%rax, -312(%rbp)
	movq	%rcx, -232(%rbp)
	movl	8(%rbx), %ecx
	movq	%rax, -304(%rbp)
	movl	%ecx, -224(%rbp)
	movzbl	12(%rbx), %ecx
	movq	%rdx, -296(%rbp)
	movb	%cl, -220(%rbp)
	leaq	16+_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEEE(%rip), %rcx
	movq	$0, -264(%rbp)
	movb	$0, -256(%rbp)
	movq	%r13, -240(%rbp)
	movq	%rcx, -320(%rbp)
	movq	%rdi, -176(%rbp)
	movl	$-1, -168(%rbp)
	movq	%r15, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -120(%rbp)
	movups	%xmm1, -216(%rbp)
	movq	$0, -112(%rbp)
	movdqa	-1488(%rbp), %xmm0
	movq	-1448(%rbp), %rbx
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movups	%xmm0, -200(%rbp)
	testq	%rbx, %rbx
	je	.L2893
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder28AddBytecodePositionDecoratorEPNS1_15NodeOriginTableEPNS0_4wasm7DecoderE@PLT
	movq	-296(%rbp), %rdx
	movq	-304(%rbp), %rax
.L2893:
	cmpq	%rax, %rdx
	jb	.L5354
	leaq	-232(%rbp), %rax
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal4wasm11WasmDecoderILNS1_7Decoder12ValidateFlagE1EE12DecodeLocalsERKNS1_12WasmFeaturesEPS3_PKNS0_9SignatureINS1_9ValueTypeEEEPNS0_10ZoneVectorISB_EE
	movq	-192(%rbp), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L5355
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdi)
.L2897:
	movq	-144(%rbp), %rsi
	subq	-152(%rbp), %rsi
	xorl	%eax, %eax
	movl	$2, (%r12)
	movl	%esi, -1440(%rbp)
	movl	%esi, %esi
	salq	$3, %rsi
	jne	.L5356
.L2898:
	movq	%rax, 48(%r12)
	movq	-208(%rbp), %rax
	leaq	8(%r12), %rbx
	movq	-176(%rbp), %rdi
	movq	8(%rax), %rsi
	addl	$2, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder5StartEj@PLT
	movq	%rbx, -1496(%rbp)
	xorl	%esi, %esi
	movq	%rax, %xmm0
	leaq	16(%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, %rdi
	movq	%rax, -1488(%rbp)
	movups	%xmm0, 8(%r12)
	movq	-176(%rbp), %rax
	movq	%rdi, 32(%rax)
	movq	-176(%rbp), %rax
	movq	%rbx, 24(%rax)
	movq	-176(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder5ParamEj@PLT
	movq	%rax, 48(%rbx)
	movq	-208(%rbp), %rax
	cmpq	$0, 8(%rax)
	je	.L4354
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2902:
	movq	48(%r12), %rdx
	addl	$1, %ebx
	movq	-176(%rbp), %rdi
	movl	%ebx, %esi
	leaq	(%rdx,%rax,8), %r13
	call	_ZN2v88internal8compiler16WasmGraphBuilder5ParamEj@PLT
	movq	%rax, 0(%r13)
	movq	-208(%rbp), %rdx
	movl	%ebx, %eax
	cmpq	8(%rdx), %rax
	jb	.L2902
.L2901:
	cmpl	%ebx, -1440(%rbp)
	jbe	.L2903
	movq	-152(%rbp), %rax
	movl	%ebx, %r15d
	movzbl	(%rax,%r15), %r13d
	cmpb	$9, %r13b
	ja	.L2905
.L5360:
	leaq	.L2907(%rip), %rdi
	movzbl	%r13b, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE,"a",@progbits
	.align 4
	.align 4
.L2907:
	.long	.L2905-.L2907
	.long	.L2912-.L2907
	.long	.L2911-.L2907
	.long	.L2910-.L2907
	.long	.L2909-.L2907
	.long	.L2908-.L2907
	.long	.L2906-.L2907
	.long	.L2906-.L2907
	.long	.L2905-.L2907
	.long	.L2906-.L2907
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.p2align 4,,10
	.p2align 3
.L2906:
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder7RefNullEv@PLT
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L5357:
	movl	%ebx, %r15d
.L2915:
	movq	-152(%rbp), %rdx
	movzbl	(%rdx,%r15), %edx
	cmpb	%r13b, %dl
	jne	.L2914
	movq	48(%r12), %rdx
	addl	$1, %ebx
	movq	%rax, (%rdx,%r15,8)
	cmpl	%ebx, -1440(%rbp)
	ja	.L5357
.L2903:
	movq	-176(%rbp), %rdi
	leaq	24(%r12), %rbx
	testq	%r12, %r12
	je	.L2917
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder17InitInstanceCacheEPNS1_22WasmInstanceCacheNodesE@PLT
	movq	-176(%rbp), %rdi
.L2917:
	movq	-1496(%rbp), %rax
	movq	%r12, -184(%rbp)
	xorl	%r13d, %r13d
	movq	-1488(%rbp), %rsi
	movq	%rax, 24(%rdi)
	movq	-176(%rbp), %rax
	movq	%rsi, 32(%rax)
	movq	-176(%rbp), %rax
	movq	%rbx, 40(%rax)
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L2918
	cmpb	$0, -120(%rbx)
	setne	%r13b
.L2918:
	movabsq	$-6148914691236517205, %rax
	movq	-112(%rbp), %r12
	subq	-120(%rbp), %r12
	sarq	$3, %r12
	imulq	%rax, %r12
	cmpq	-72(%rbp), %rbx
	je	.L2919
	movq	-304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movb	%r13b, 16(%rbx)
	xorl	$1, %r13d
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 72(%rbx)
	pxor	%xmm0, %xmm0
	movb	%r13b, 56(%rbx)
	movb	$2, (%rbx)
	andb	$1, 56(%rbx)
	movl	%r12d, 4(%rbx)
	movq	%rax, 8(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 48(%rbx)
	movl	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	movb	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movl	$-1, 128(%rbx)
	movups	%xmm0, 104(%rbx)
	movq	-80(%rbp), %rax
	leaq	136(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L2920:
	movl	$0, -112(%rcx)
	movq	-208(%rbp), %rax
	movq	(%rax), %rbx
	movl	%ebx, -72(%rcx)
	cmpl	$1, %ebx
	je	.L5358
	ja	.L5359
.L2929:
	movq	-304(%rbp), %r13
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	cmpq	%rax, %r13
	jnb	.L2934
	.p2align 4,,10
	.p2align 3
.L4300:
	movzbl	0(%r13), %r12d
	movl	%r12d, %ebx
	cmpb	$-2, %r12b
	ja	.L2935
	leaq	.L2937(%rip), %rcx
	movzbl	%r12b, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L2937:
	.long	.L3124-.L2937
	.long	.L3123-.L2937
	.long	.L3122-.L2937
	.long	.L3121-.L2937
	.long	.L3120-.L2937
	.long	.L3119-.L2937
	.long	.L3118-.L2937
	.long	.L3117-.L2937
	.long	.L3116-.L2937
	.long	.L3115-.L2937
	.long	.L3114-.L2937
	.long	.L3113-.L2937
	.long	.L3112-.L2937
	.long	.L3111-.L2937
	.long	.L3110-.L2937
	.long	.L3109-.L2937
	.long	.L3108-.L2937
	.long	.L3107-.L2937
	.long	.L3106-.L2937
	.long	.L3105-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L3104-.L2937
	.long	.L3103-.L2937
	.long	.L3102-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L3101-.L2937
	.long	.L3100-.L2937
	.long	.L3099-.L2937
	.long	.L3098-.L2937
	.long	.L3097-.L2937
	.long	.L3096-.L2937
	.long	.L3095-.L2937
	.long	.L2935-.L2937
	.long	.L3094-.L2937
	.long	.L3093-.L2937
	.long	.L3092-.L2937
	.long	.L3091-.L2937
	.long	.L3090-.L2937
	.long	.L3089-.L2937
	.long	.L3088-.L2937
	.long	.L3087-.L2937
	.long	.L3086-.L2937
	.long	.L3085-.L2937
	.long	.L3084-.L2937
	.long	.L3083-.L2937
	.long	.L3082-.L2937
	.long	.L3081-.L2937
	.long	.L3080-.L2937
	.long	.L3079-.L2937
	.long	.L3078-.L2937
	.long	.L3077-.L2937
	.long	.L3076-.L2937
	.long	.L3075-.L2937
	.long	.L3074-.L2937
	.long	.L3073-.L2937
	.long	.L3072-.L2937
	.long	.L3071-.L2937
	.long	.L3070-.L2937
	.long	.L3069-.L2937
	.long	.L3068-.L2937
	.long	.L3067-.L2937
	.long	.L3066-.L2937
	.long	.L3065-.L2937
	.long	.L3064-.L2937
	.long	.L3063-.L2937
	.long	.L3062-.L2937
	.long	.L3061-.L2937
	.long	.L3060-.L2937
	.long	.L3059-.L2937
	.long	.L3058-.L2937
	.long	.L3057-.L2937
	.long	.L3056-.L2937
	.long	.L3055-.L2937
	.long	.L3054-.L2937
	.long	.L3053-.L2937
	.long	.L3052-.L2937
	.long	.L3051-.L2937
	.long	.L3050-.L2937
	.long	.L3049-.L2937
	.long	.L3048-.L2937
	.long	.L3047-.L2937
	.long	.L3046-.L2937
	.long	.L3045-.L2937
	.long	.L3044-.L2937
	.long	.L3043-.L2937
	.long	.L3042-.L2937
	.long	.L3041-.L2937
	.long	.L3040-.L2937
	.long	.L3039-.L2937
	.long	.L3038-.L2937
	.long	.L3037-.L2937
	.long	.L3036-.L2937
	.long	.L3035-.L2937
	.long	.L3034-.L2937
	.long	.L3033-.L2937
	.long	.L3032-.L2937
	.long	.L3031-.L2937
	.long	.L3030-.L2937
	.long	.L3029-.L2937
	.long	.L3028-.L2937
	.long	.L3027-.L2937
	.long	.L3026-.L2937
	.long	.L3025-.L2937
	.long	.L3024-.L2937
	.long	.L3023-.L2937
	.long	.L3022-.L2937
	.long	.L3021-.L2937
	.long	.L3020-.L2937
	.long	.L3019-.L2937
	.long	.L3018-.L2937
	.long	.L3017-.L2937
	.long	.L3016-.L2937
	.long	.L3015-.L2937
	.long	.L3014-.L2937
	.long	.L3013-.L2937
	.long	.L3012-.L2937
	.long	.L3011-.L2937
	.long	.L3010-.L2937
	.long	.L3009-.L2937
	.long	.L3008-.L2937
	.long	.L3007-.L2937
	.long	.L3006-.L2937
	.long	.L3005-.L2937
	.long	.L3004-.L2937
	.long	.L3003-.L2937
	.long	.L3002-.L2937
	.long	.L3001-.L2937
	.long	.L3000-.L2937
	.long	.L2999-.L2937
	.long	.L2998-.L2937
	.long	.L2997-.L2937
	.long	.L2996-.L2937
	.long	.L2995-.L2937
	.long	.L2994-.L2937
	.long	.L2993-.L2937
	.long	.L2992-.L2937
	.long	.L2991-.L2937
	.long	.L2990-.L2937
	.long	.L2989-.L2937
	.long	.L2988-.L2937
	.long	.L2987-.L2937
	.long	.L2986-.L2937
	.long	.L2985-.L2937
	.long	.L2984-.L2937
	.long	.L2983-.L2937
	.long	.L2982-.L2937
	.long	.L2981-.L2937
	.long	.L2980-.L2937
	.long	.L2979-.L2937
	.long	.L2978-.L2937
	.long	.L2977-.L2937
	.long	.L2976-.L2937
	.long	.L2975-.L2937
	.long	.L2974-.L2937
	.long	.L2973-.L2937
	.long	.L2972-.L2937
	.long	.L2971-.L2937
	.long	.L2970-.L2937
	.long	.L2969-.L2937
	.long	.L2968-.L2937
	.long	.L2967-.L2937
	.long	.L2966-.L2937
	.long	.L2965-.L2937
	.long	.L2964-.L2937
	.long	.L2963-.L2937
	.long	.L2962-.L2937
	.long	.L2961-.L2937
	.long	.L2960-.L2937
	.long	.L2959-.L2937
	.long	.L2958-.L2937
	.long	.L2957-.L2937
	.long	.L2956-.L2937
	.long	.L2955-.L2937
	.long	.L2954-.L2937
	.long	.L2953-.L2937
	.long	.L2952-.L2937
	.long	.L2951-.L2937
	.long	.L2950-.L2937
	.long	.L2949-.L2937
	.long	.L2948-.L2937
	.long	.L2947-.L2937
	.long	.L2946-.L2937
	.long	.L2945-.L2937
	.long	.L2944-.L2937
	.long	.L2943-.L2937
	.long	.L2941-.L2937
	.long	.L2941-.L2937
	.long	.L2941-.L2937
	.long	.L2941-.L2937
	.long	.L2941-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2942-.L2937
	.long	.L2941-.L2937
	.long	.L2940-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2935-.L2937
	.long	.L2939-.L2937
	.long	.L2938-.L2937
	.long	.L2936-.L2937
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.p2align 4,,10
	.p2align 3
.L2908:
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder8S128ZeroEv@PLT
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2909:
	movq	-176(%rbp), %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler16WasmGraphBuilder15Float64ConstantEd@PLT
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2910:
	movq	-176(%rbp), %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler16WasmGraphBuilder15Float32ConstantEf@PLT
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2912:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder13Int32ConstantEi@PLT
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2911:
	movq	-176(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder13Int64ConstantEl@PLT
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2914:
	cmpl	%ebx, -1440(%rbp)
	jbe	.L2903
	movl	%edx, %r13d
	cmpb	$9, %r13b
	jbe	.L5360
.L2905:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5358:
	movq	-208(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
	movq	%rdx, -64(%rcx)
	movq	$0, -48(%rcx)
	movb	%al, -56(%rcx)
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L5356:
	movq	-192(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5361
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L2943:
	movl	$2, %ecx
	movl	$4, %edx
	movl	$191, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L3125:
	movq	%r13, -304(%rbp)
	cmpq	%r13, %rax
	ja	.L4300
.L2934:
	cmpq	%rax, %r13
	je	.L4298
	testq	%r15, %r15
	je	.L5362
.L4302:
	movabsq	$-1085102592571150095, %rsi
	movq	-80(%rbp), %rcx
	movq	%rcx, %rdx
	subq	-88(%rbp), %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	cmpq	$1, %rax
	ja	.L5363
	cmpq	$136, %rdx
	je	.L5364
.L2895:
	cmpq	$0, -1448(%rbp)
	je	.L4306
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder31RemoveBytecodePositionDecoratorEv@PLT
.L4306:
	movq	-264(%rbp), %r12
	movq	-272(%rbp), %r13
	testq	%r12, %r12
	jne	.L5365
	movq	-1432(%rbp), %rbx
	movb	$0, -512(%rbp)
	movdqa	-512(%rbp), %xmm4
	movq	$0, (%rbx)
	leaq	32(%rbx), %rax
	movl	$0, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rbx, %rax
	movaps	%xmm4, -736(%rbp)
.L4315:
	movdqa	-736(%rbp), %xmm7
	movups	%xmm7, 32(%rax)
.L4317:
	movq	%r12, 24(%rax)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rax, -320(%rbp)
	cmpq	-1472(%rbp), %r13
	je	.L4318
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4318:
	movq	-1464(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5366
	movq	-1432(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5362:
	.cfi_restore_state
	leaq	.LC134(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
.L4298:
	testq	%r15, %r15
	jne	.L4302
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder25PatchInStackCheckIfNeededEv@PLT
	jmp	.L4302
	.p2align 4,,10
	.p2align 3
.L5354:
	leaq	.LC79(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L5365:
	movl	-280(%rbp), %eax
	leaq	-968(%rbp), %rbx
	movq	%rbx, -984(%rbp)
	movl	%eax, -992(%rbp)
	testq	%r13, %r13
	je	.L5367
	movq	%r12, -1328(%rbp)
	cmpq	$15, %r12
	ja	.L5368
	cmpq	$1, %r12
	jne	.L4605
	movzbl	0(%r13), %eax
	movb	%al, -968(%rbp)
	movq	%rbx, %rax
.L4311:
	movq	%r12, -976(%rbp)
	movb	$0, (%rax,%r12)
	movl	-992(%rbp), %ecx
	movq	-984(%rbp), %rax
	movq	$0, -544(%rbp)
	movl	%ecx, -536(%rbp)
	cmpq	%rbx, %rax
	je	.L5369
	movq	-968(%rbp), %rdx
	movq	-976(%rbp), %r12
	leaq	-512(%rbp), %rsi
	movq	%rax, -528(%rbp)
	movq	%rbx, -984(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%r12, -520(%rbp)
	movq	$0, -976(%rbp)
	movb	$0, -968(%rbp)
	movq	$0, -768(%rbp)
	movl	%ecx, -760(%rbp)
	cmpq	%rsi, %rax
	je	.L4313
	movq	-1432(%rbp), %rdi
	movq	%rax, -752(%rbp)
	movq	%rdx, -736(%rbp)
	movq	-272(%rbp), %r13
	movl	%ecx, 8(%rdi)
	leaq	32(%rdi), %rcx
	movq	%rcx, 16(%rdi)
	leaq	-736(%rbp), %rcx
	movq	%r12, -744(%rbp)
	movq	$0, (%rdi)
	cmpq	%rcx, %rax
	je	.L5370
	movq	%rax, 16(%rdi)
	movq	%rdi, %rax
	movq	%rdx, 32(%rdi)
	jmp	.L4317
	.p2align 4,,10
	.p2align 3
.L5363:
	movq	-128(%rcx), %rsi
	leaq	.LC135(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L2935:
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L4296
	cmpb	$0, 392(%rax)
	je	.L4296
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L5371
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2941:
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L4292
	cmpb	$0, -221(%rbp)
	je	.L5372
	movq	-216(%rbp), %rax
	movb	$1, 11(%rax)
.L4292:
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE@PLT
	testb	%al, %al
	je	.L4294
	cmpb	$0, -225(%rbp)
	je	.L5373
	movq	-216(%rbp), %rax
	movb	$1, 7(%rax)
.L4294:
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeEPNS0_9SignatureINS1_9ValueTypeEEE
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L4605:
	movq	%rbx, %rdi
.L4310:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-1328(%rbp), %r12
	movq	-984(%rbp), %rax
	jmp	.L4311
	.p2align 4,,10
	.p2align 3
.L2942:
	cmpb	$0, -225(%rbp)
	je	.L5374
	movq	-216(%rbp), %rax
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movb	$1, 7(%rax)
	movb	$8, -1328(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	testq	%r15, %r15
	je	.L3742
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2936:
	cmpb	$0, -230(%rbp)
	je	.L5375
	movq	-216(%rbp), %rax
	movb	$1, 2(%rax)
	movq	-304(%rbp), %rdx
	movq	-296(%rbp), %rax
	leaq	1(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L4134
	cmpl	%esi, %eax
	je	.L4134
	movzbl	1(%rdx), %r12d
	orl	$65024, %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	movq	%rax, -1488(%rbp)
	testq	%rax, %rax
	je	.L4139
	leal	-65024(%r12), %eax
	cmpl	$78, %eax
	ja	.L4139
	leaq	.L4141(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L4141:
	.long	.L4138-.L4141
	.long	.L4205-.L4141
	.long	.L4204-.L4141
	.long	.L4203-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4139-.L4141
	.long	.L4202-.L4141
	.long	.L4201-.L4141
	.long	.L4200-.L4141
	.long	.L4199-.L4141
	.long	.L4198-.L4141
	.long	.L4197-.L4141
	.long	.L4196-.L4141
	.long	.L4190-.L4141
	.long	.L4527-.L4141
	.long	.L4194-.L4141
	.long	.L4193-.L4141
	.long	.L4192-.L4141
	.long	.L4191-.L4141
	.long	.L4190-.L4141
	.long	.L4189-.L4141
	.long	.L4188-.L4141
	.long	.L4187-.L4141
	.long	.L4186-.L4141
	.long	.L4185-.L4141
	.long	.L4184-.L4141
	.long	.L4183-.L4141
	.long	.L4182-.L4141
	.long	.L4181-.L4141
	.long	.L4180-.L4141
	.long	.L4179-.L4141
	.long	.L4178-.L4141
	.long	.L4177-.L4141
	.long	.L4176-.L4141
	.long	.L4175-.L4141
	.long	.L4174-.L4141
	.long	.L4173-.L4141
	.long	.L4172-.L4141
	.long	.L4171-.L4141
	.long	.L4170-.L4141
	.long	.L4169-.L4141
	.long	.L4168-.L4141
	.long	.L4167-.L4141
	.long	.L4166-.L4141
	.long	.L4165-.L4141
	.long	.L4164-.L4141
	.long	.L4163-.L4141
	.long	.L4162-.L4141
	.long	.L4161-.L4141
	.long	.L4160-.L4141
	.long	.L4159-.L4141
	.long	.L4158-.L4141
	.long	.L4157-.L4141
	.long	.L4156-.L4141
	.long	.L4155-.L4141
	.long	.L4154-.L4141
	.long	.L4153-.L4141
	.long	.L4152-.L4141
	.long	.L4151-.L4141
	.long	.L4150-.L4141
	.long	.L4149-.L4141
	.long	.L4148-.L4141
	.long	.L4147-.L4141
	.long	.L4146-.L4141
	.long	.L4145-.L4141
	.long	.L4144-.L4141
	.long	.L4143-.L4141
	.long	.L4142-.L4141
	.long	.L4140-.L4141
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L4194:
	xorl	%ecx, %ecx
	movl	$65049, %r12d
	xorl	%r13d, %r13d
.L4195:
	movq	-240(%rbp), %rax
	movq	-304(%rbp), %r15
	cmpb	$0, 16(%rax)
	je	.L5376
	movq	-296(%rbp), %rax
	leaq	2(%r15), %rsi
	cmpq	%rax, %rsi
	jb	.L5377
	leaq	.LC28(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$2, %ebx
	movl	$0, -1512(%rbp)
.L5349:
	movl	$0, -1520(%rbp)
	movq	-296(%rbp), %rax
.L4221:
	leaq	(%r15,%rbx), %rsi
	cmpq	%rax, %rsi
	jb	.L5378
	leaq	.LC30(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$0, -1568(%rbp)
.L4224:
	movq	-1488(%rbp), %rax
	leaq	-520(%rbp), %rdx
	leaq	-544(%rbp), %rdi
	movq	%rdx, %xmm0
	movq	%rdx, -1440(%rbp)
	movq	8(%rax), %r15
	punpcklqdq	%xmm0, %xmm0
	leaq	-328(%rbp), %rax
	movq	%rax, -528(%rbp)
	movslq	%r15d, %rbx
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %rbx
	ja	.L5379
.L4233:
	leaq	(%rbx,%rbx,2), %rax
	movl	%r15d, %r11d
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, -536(%rbp)
	subl	$1, %r11d
	js	.L4234
	movslq	%r11d, %r10
	movl	%r12d, -1528(%rbp)
	movl	%r11d, %r15d
	movq	%r14, -1496(%rbp)
	movq	%r10, %r12
	movq	-1488(%rbp), %r14
	movb	%r13b, -1536(%rbp)
	jmp	.L4273
	.p2align 4,,10
	.p2align 3
.L5381:
	cmpb	$2, -120(%rdi)
	movq	-304(%rbp), %rsi
	jne	.L5380
.L4236:
	xorl	%r11d, %r11d
	movl	$10, %ebx
.L4240:
	subl	$1, %r15d
	movq	%rsi, 0(%r13)
	subq	$1, %r12
	movb	%bl, 8(%r13)
	movq	%r11, 16(%r13)
	cmpl	$-1, %r15d
	je	.L5342
	movq	-544(%rbp), %rdx
.L4273:
	movq	16(%r14), %rax
	movq	-80(%rbp), %rdi
	movabsq	$-6148914691236517205, %rbx
	addq	%r12, %rax
	addq	(%r14), %rax
	movl	-132(%rdi), %esi
	movzbl	(%rax), %ecx
	leaq	(%r12,%r12,2), %rax
	leaq	(%rdx,%rax,8), %r13
	movq	-112(%rbp), %rdx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	cmpq	%rsi, %rax
	jbe	.L5381
	movzbl	-16(%rdx), %ebx
	movq	-24(%rdx), %rsi
	subq	$24, %rdx
	movq	16(%rdx), %r11
	movq	%rdx, -112(%rbp)
	cmpb	%bl, %cl
	je	.L4240
	cmpb	$6, %cl
	sete	%al
	cmpb	$8, %bl
	sete	%dl
	testb	%al, %al
	je	.L4630
	testb	%dl, %dl
	je	.L4630
	movl	$8, %ebx
	jmp	.L4240
.L4527:
	movl	$3, %ecx
	movl	$65048, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
	.p2align 4,,10
	.p2align 3
.L2938:
	cmpb	$0, -229(%rbp)
	je	.L5382
	movq	-216(%rbp), %rax
	movb	$1, 3(%rax)
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	leaq	1(%rbx), %rsi
	cmpq	%rax, %rsi
	ja	.L3999
	cmpl	%esi, %eax
	je	.L3999
	movzbl	1(%rbx), %r12d
	orl	$64768, %r12d
	leal	-64768(%r12), %edx
	cmpl	$23, %edx
	ja	.L4000
	leaq	.L4002(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L4002:
	.long	.L4012-.L4002
	.long	.L4011-.L4002
	.long	.L4000-.L4002
	.long	.L4010-.L4002
	.long	.L4000-.L4002
	.long	.L4009-.L4002
	.long	.L4000-.L4002
	.long	.L4008-.L4002
	.long	.L4000-.L4002
	.long	.L4009-.L4002
	.long	.L4000-.L4002
	.long	.L4008-.L4002
	.long	.L4000-.L4002
	.long	.L4009-.L4002
	.long	.L4008-.L4002
	.long	.L4000-.L4002
	.long	.L4007-.L4002
	.long	.L4006-.L4002
	.long	.L4000-.L4002
	.long	.L4005-.L4002
	.long	.L4004-.L4002
	.long	.L4000-.L4002
	.long	.L4003-.L4002
	.long	.L4001-.L4002
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.p2align 4,,10
	.p2align 3
.L2939:
	leaq	1(%r13), %rsi
	cmpq	%rax, %rsi
	ja	.L3990
	cmpl	%esi, %eax
	je	.L3990
	movzbl	1(%r13), %eax
	orb	$-4, %ah
	movl	%eax, %esi
	cmpl	$64519, %eax
	jle	.L3991
	subl	$64527, %eax
	cmpl	$2, %eax
	ja	.L3995
	cmpb	$0, -225(%rbp)
	je	.L5383
	movq	-216(%rbp), %rax
	movb	$1, 7(%rax)
.L3994:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19DecodeNumericOpcodeENS1_10WasmOpcodeE
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	2(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2940:
	cmpb	$0, -225(%rbp)
	je	.L5384
	movq	-216(%rbp), %rax
	leaq	.LC63(%rip), %rcx
	leaq	-1324(%rbp), %rdx
	movq	%r14, %rdi
	movb	$1, 7(%rax)
	movabsq	$4294967296, %rax
	movq	%rax, -1328(%rbp)
	movq	-304(%rbp), %rax
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	%eax, -1328(%rbp)
	movl	%eax, %ecx
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L3746
	movq	144(%rax), %rdx
	subq	136(%rax), %rdx
	movl	%ecx, %esi
	sarq	$5, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3746
	leaq	-128(%rbp), %r15
	leaq	-1360(%rbp), %r12
	movb	$7, -1360(%rbp)
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	testq	%r15, %r15
	jne	.L3748
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5385
.L3748:
	movl	-1324(%rbp), %eax
	leal	1(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L3745:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2944:
	movl	$1, %ecx
	movl	$3, %edx
	movl	$190, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2945:
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$2, %edx
	movl	$189, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2946:
	movl	$3, %ecx
	movl	$1, %edx
	movl	$188, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2947:
	movl	$3, %ecx
	movl	$4, %edx
	movl	$187, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2948:
	movq	%r14, %rdi
	movl	$2, %ecx
	movl	$4, %edx
	movl	$186, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2949:
	movl	$2, %ecx
	movl	$4, %edx
	movl	$185, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2950:
	movl	$1, %ecx
	movl	$4, %edx
	movl	$184, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2951:
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	$4, %edx
	movl	$183, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2952:
	movl	$4, %ecx
	movl	$3, %edx
	movl	$182, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2953:
	movl	$2, %ecx
	movl	$3, %edx
	movl	$181, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2954:
	movq	%r14, %rdi
	movl	$2, %ecx
	movl	$3, %edx
	movl	$180, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2955:
	movl	$1, %ecx
	movl	$3, %edx
	movl	$179, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2956:
	movl	$1, %ecx
	movl	$3, %edx
	movl	$178, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2957:
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$2, %edx
	movl	$177, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2958:
	movl	$4, %ecx
	movl	$2, %edx
	movl	$176, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2959:
	movl	$3, %ecx
	movl	$2, %edx
	movl	$175, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2960:
	movq	%r14, %rdi
	movl	$3, %ecx
	movl	$2, %edx
	movl	$174, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2961:
	movl	$1, %ecx
	movl	$2, %edx
	movl	$173, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2962:
	movl	$1, %ecx
	movl	$2, %edx
	movl	$172, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2963:
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$1, %edx
	movl	$171, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2964:
	movl	$4, %ecx
	movl	$1, %edx
	movl	$170, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2965:
	movl	$3, %ecx
	movl	$1, %edx
	movl	$169, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2966:
	movq	%r14, %rdi
	movl	$3, %ecx
	movl	$1, %edx
	movl	$168, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2967:
	movl	$2, %ecx
	movl	$1, %edx
	movl	$167, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2968:
	movl	$4, %ecx
	movl	$4, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movl	$166, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2969:
	movq	%r14, %rdi
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movl	$165, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2970:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r14, %rdi
	movl	$164, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2971:
	movl	$4, %ecx
	movl	$4, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movl	$163, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2972:
	movq	%r14, %rdi
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movl	$162, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2973:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r14, %rdi
	movl	$161, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2974:
	movl	$4, %ecx
	movl	$4, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movl	$160, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2975:
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$4, %edx
	movl	$159, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2976:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$158, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2977:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$157, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2978:
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$4, %edx
	movl	$156, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2979:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$155, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2980:
	movl	$4, %ecx
	movl	$4, %edx
	movl	$154, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2981:
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$4, %edx
	movl	$153, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2982:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r14, %rdi
	movl	$152, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2983:
	movl	$3, %ecx
	movl	$3, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movl	$151, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2984:
	movq	%r14, %rdi
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movl	$150, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2985:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r14, %rdi
	movl	$149, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2986:
	movl	$3, %ecx
	movl	$3, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movl	$148, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2987:
	movq	%r14, %rdi
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movl	$147, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2988:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$3, %edx
	movq	%r14, %rdi
	movl	$146, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2989:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$145, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	%r14, %rdi
	movl	$3, %ecx
	movl	$3, %edx
	movl	$144, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2991:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$143, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2992:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$142, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2993:
	movq	%r14, %rdi
	movl	$3, %ecx
	movl	$3, %edx
	movl	$141, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2994:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$140, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2995:
	movl	$3, %ecx
	movl	$3, %edx
	movl	$139, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2996:
	movq	%r14, %rdi
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movl	$138, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2997:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$137, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2998:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$136, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L2999:
	movq	%r14, %rdi
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movl	$135, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3000:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$134, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3001:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$133, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3002:
	movq	%r14, %rdi
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movl	$132, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3003:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$131, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3004:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$130, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3005:
	movq	%r14, %rdi
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movl	$129, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3006:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$128, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3007:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$127, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3008:
	movq	%r14, %rdi
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movl	$126, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3009:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$125, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3010:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movl	$124, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	%r14, %rdi
	movl	$2, %ecx
	movl	$2, %edx
	movl	$123, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3012:
	movl	$2, %ecx
	movl	$2, %edx
	movl	$122, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3013:
	movl	$2, %ecx
	movl	$2, %edx
	movl	$121, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3014:
	movq	%r14, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movl	$120, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3015:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$119, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3016:
	movl	$1, %ecx
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$118, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3017:
	movq	%r14, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movl	$117, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3018:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$116, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3019:
	movl	$1, %ecx
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$115, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	%r14, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movl	$114, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3021:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$113, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3022:
	movl	$1, %ecx
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$112, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3023:
	movq	%r14, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movl	$111, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3024:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$110, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3025:
	movl	$1, %ecx
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$109, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3026:
	movq	%r14, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movl	$108, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3027:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$107, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3028:
	movl	$1, %ecx
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$106, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3029:
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movl	$105, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3030:
	movl	$1, %ecx
	movl	$1, %edx
	movl	$104, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3031:
	movl	$1, %ecx
	movl	$1, %edx
	movl	$103, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3032:
	movq	%r14, %rdi
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movl	$102, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3033:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$101, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3034:
	movl	$4, %ecx
	movl	$4, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$100, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3035:
	movq	%r14, %rdi
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movl	$99, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3036:
	movl	$4, %r8d
	movl	$4, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$98, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3037:
	movl	$4, %ecx
	movl	$4, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$97, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3038:
	movq	%r14, %rdi
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movl	$96, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3039:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$95, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3040:
	movl	$3, %ecx
	movl	$3, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$94, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3041:
	movq	%r14, %rdi
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movl	$93, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3042:
	movl	$3, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$92, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3043:
	movl	$3, %ecx
	movl	$3, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$91, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3044:
	movq	%r14, %rdi
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movl	$90, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3045:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$89, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3046:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$88, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3047:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$87, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3048:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$86, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3049:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$85, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3050:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$84, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3051:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$83, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3052:
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$82, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3053:
	movl	$2, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$81, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3054:
	movl	$2, %ecx
	movl	$1, %edx
	movl	$80, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3055:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$79, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3056:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$78, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3057:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$77, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3058:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$76, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3059:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$75, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3060:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$74, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3061:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$73, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3062:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$72, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3063:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$71, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3064:
	movl	$1, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	$70, %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_S9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3065:
	movl	$1, %ecx
	movl	$1, %edx
	movl	$69, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE19BuildSimpleOperatorENS1_10WasmOpcodeENS1_9ValueTypeES9_
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3066:
	leaq	1(%r13), %rsi
	cmpq	%rax, %rsi
	ja	.L3738
	subq	%rsi, %rax
	cmpl	$7, %eax
	jbe	.L3738
	movq	1(%r13), %rbx
.L3739:
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movb	$4, -1328(%rbp)
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %r12
	testq	%r15, %r15
	jne	.L3740
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3740
	movq	-176(%rbp), %rdi
	movq	%rbx, %xmm0
	call	_ZN2v88internal8compiler16WasmGraphBuilder15Float64ConstantEd@PLT
	movq	%rax, -8(%r12)
	movq	-264(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3740:
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	leaq	9(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3067:
	leaq	1(%r13), %rsi
	cmpq	%rax, %rsi
	ja	.L3735
	subq	%rsi, %rax
	cmpl	$3, %eax
	jbe	.L3735
	movl	1(%r13), %ebx
.L3736:
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movb	$3, -1328(%rbp)
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %r12
	testq	%r15, %r15
	jne	.L3737
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3737
	movq	-176(%rbp), %rdi
	movd	%ebx, %xmm0
	call	_ZN2v88internal8compiler16WasmGraphBuilder15Float32ConstantEf@PLT
	movq	%rax, -8(%r12)
	movq	-264(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3737:
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	leaq	5(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3068:
	leaq	1(%r13), %rsi
	xorl	%r8d, %r8d
	leaq	-128(%rbp), %r15
	movq	%r14, %rdi
	leaq	-1320(%rbp), %rdx
	leaq	.LC72(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	leaq	-1360(%rbp), %r12
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, -1328(%rbp)
	movb	$2, -1360(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	testq	%r15, %r15
	jne	.L3734
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3734
	movq	-1328(%rbp), %rsi
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder13Int64ConstantEl@PLT
	movq	%rax, -8(%rbx)
	movq	-264(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3734:
	movl	-1320(%rbp), %edx
	movq	-296(%rbp), %rax
	leal	1(%rdx), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3069:
	leaq	1(%r13), %rsi
	xorl	%r8d, %r8d
	leaq	-128(%rbp), %r15
	movq	%r14, %rdi
	leaq	-1324(%rbp), %rdx
	leaq	.LC71(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIiLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	leaq	-1360(%rbp), %r12
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdx
	movl	%eax, -1328(%rbp)
	movb	$1, -1360(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	testq	%r15, %r15
	jne	.L3733
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3733
	movl	-1328(%rbp), %esi
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder13Int32ConstantEi@PLT
	movq	%rax, -8(%rbx)
	movq	-264(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3733:
	movl	-1324(%rbp), %edx
	movq	-296(%rbp), %rax
	leal	1(%rdx), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3093:
	movl	$5, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3077:
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3078:
	movl	$7, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3079:
	movl	$3, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3080:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3081:
	movl	$11, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3082:
	movl	$10, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3083:
	movl	$9, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3084:
	movl	$8, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3085:
	movl	$7, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3086:
	movl	$6, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3087:
	movl	$4, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3088:
	movl	$3, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3089:
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3090:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3091:
	movl	$13, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3092:
	movl	$12, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3073:
	movl	$5, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3074:
	movl	$4, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3075:
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3076:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3071:
	movq	-240(%rbp), %rdx
	cmpb	$0, 18(%rdx)
	je	.L5386
	leaq	1(%r13), %rsi
	cmpq	%rax, %rsi
	ja	.L3846
	cmpl	%esi, %eax
	je	.L3846
	movzbl	1(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L5387
.L3848:
	leaq	-128(%rbp), %r15
	leaq	-304(%rbp), %rsi
	movb	$1, -1328(%rbp)
	movq	%r15, %rdi
	leaq	-1328(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	movl	$2, %esi
	testq	%r15, %r15
	jne	.L3845
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3845
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder18CurrentMemoryPagesEv@PLT
	testq	%rax, %rax
	je	.L3849
	cmpl	$-1, -168(%rbp)
	je	.L3849
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3849:
	movq	%rax, -8(%rbx)
	movq	-264(%rbp), %r15
	movl	$2, %esi
	.p2align 4,,10
	.p2align 3
.L3845:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3072:
	movl	$6, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3070:
	movq	-240(%rbp), %rdx
	cmpb	$0, 18(%rdx)
	je	.L5388
	leaq	1(%r13), %rsi
	cmpq	%rax, %rsi
	ja	.L3832
	cmpl	%esi, %eax
	je	.L3832
	movzbl	1(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L5389
.L3834:
	cmpb	$0, 392(%rdx)
	jne	.L5390
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rsi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L5391
	movzbl	-16(%rdx), %ecx
	movq	-24(%rdx), %r13
	subq	$24, %rdx
	movq	16(%rdx), %r12
	movq	%rdx, -112(%rbp)
	cmpb	$1, %cl
	jne	.L5392
.L3839:
	leaq	-128(%rbp), %r15
	leaq	-304(%rbp), %rsi
	movb	$1, -1328(%rbp)
	movq	%r15, %rdi
	leaq	-1328(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	movl	$2, %esi
	testq	%r15, %r15
	jne	.L3831
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5393
	.p2align 4,,10
	.p2align 3
.L3831:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	-80(%rbp), %rsi
	movq	-208(%rbp), %rdx
	cmpb	$0, -120(%rsi)
	movq	(%rdx), %rcx
	jne	.L3699
	testl	%ecx, %ecx
	je	.L3700
	movq	-112(%rbp), %rdi
	movq	%rdi, %r8
	subq	-120(%rbp), %r8
	sarq	$3, %r8
	imull	$-1431655765, %r8d, %r8d
	subl	-132(%rsi), %r8d
	cmpl	%ecx, %r8d
	jl	.L5394
	movslq	%ecx, %rax
	testl	%ecx, %ecx
	jle	.L3700
	movq	16(%rdx), %r8
	leaq	0(,%rax,4), %rdx
	leal	-1(%rcx), %r9d
	subq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rsi
	xorl	%eax, %eax
	jmp	.L3705
	.p2align 4,,10
	.p2align 3
.L4459:
	movq	%rdx, %rax
.L3705:
	movzbl	(%r8,%rax), %ebx
	movzbl	8(%rsi), %edi
	movl	%eax, %r12d
	cmpb	%dil, %bl
	je	.L3702
	leal	-7(%rdi), %edx
	cmpb	$2, %dl
	ja	.L4625
	cmpb	$6, %bl
	jne	.L4625
.L3702:
	leaq	1(%rax), %rdx
	addq	$24, %rsi
	cmpq	%rax, %r9
	jne	.L4459
.L3700:
	testq	%rcx, %rcx
	je	.L3706
	movq	-112(%rbp), %rbx
	leaq	(%rcx,%rcx,2), %rax
	salq	$3, %rax
	subq	%rax, %rbx
	testq	%r15, %r15
	jne	.L3707
	movq	-176(%rbp), %r12
	movq	104(%r12), %rax
	cmpq	%rcx, %rax
	jnb	.L4347
	movq	(%r12), %rdi
	leaq	5(%rax,%rcx), %r13
	leaq	0(,%r13,8), %rsi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L5395
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L3712:
	movq	%r8, 96(%r12)
	movq	%r13, 104(%r12)
.L3710:
	leaq	(%rcx,%rcx,2), %rdx
	leaq	16(%rbx), %rax
	leaq	(%rbx,%rdx,8), %rdx
	leaq	(%r8,%rcx,8), %rsi
	cmpq	%rdx, %r8
	setnb	%dil
	cmpq	%rsi, %rax
	setnb	%dl
	orb	%dl, %dil
	je	.L3715
	leaq	-1(%rcx), %rdx
	cmpq	$22, %rdx
	jbe	.L3715
	movq	%rcx, %rsi
	movq	%r8, %rdx
	shrq	%rsi
	salq	$4, %rsi
	addq	%r8, %rsi
	.p2align 4,,10
	.p2align 3
.L3717:
	movq	(%rax), %xmm0
	addq	$16, %rdx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rsi
	jne	.L3717
	movq	%rcx, %rax
	andq	$-2, %rax
	testb	$1, %cl
	je	.L3709
	leaq	(%rax,%rax,2), %rdx
	movq	16(%rbx,%rdx,8), %rdx
	movq	%rdx, (%r8,%rax,8)
.L3709:
	movq	-176(%rbp), %rdi
	movq	%rcx, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder6ReturnENS0_6VectorIPNS1_4NodeEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3707
	cmpl	$-1, -168(%rbp)
	je	.L3707
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	.p2align 4,,10
	.p2align 3
.L3707:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3101:
	leaq	.LC65(%rip), %rcx
	leaq	-1320(%rbp), %rdx
	movq	%r14, %rdi
	movb	$0, -1324(%rbp)
	leaq	1(%r13), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	%eax, %ecx
	movq	-200(%rbp), %rax
	movl	%ecx, -1328(%rbp)
	testq	%rax, %rax
	je	.L3750
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	subq	%rsi, %rdx
	cmpl	%edx, %ecx
	jnb	.L3750
	cmpq	%rdx, %rcx
	jnb	.L5351
	movzbl	(%rsi,%rcx), %eax
	leaq	-128(%rbp), %r15
	leaq	-1360(%rbp), %r12
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdx
	movb	%al, -1324(%rbp)
	movb	%al, -1360(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rax
	testq	%r15, %r15
	jne	.L3754
	movq	-80(%rbp), %rdx
	cmpb	$0, -120(%rdx)
	jne	.L3754
	movq	-184(%rbp), %rdx
	movq	48(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L3754
	movl	-1328(%rbp), %ecx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, -8(%rax)
	movq	-264(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3754:
	movl	-1320(%rbp), %eax
	leal	1(%rax), %esi
.L3752:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3102:
	cmpb	$0, -225(%rbp)
	je	.L5396
	movq	-216(%rbp), %rax
	movb	$1, 7(%rax)
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rdi
	leaq	1(%rcx), %r12
	cmpq	%rdi, %r12
	jb	.L5397
	leaq	.LC66(%rip), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L3501:
	leaq	.LC67(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L3503:
	movq	-264(%rbp), %r15
	movl	$1, %esi
	testq	%r15, %r15
	jne	.L3489
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	movabsq	$-6148914691236517205, %rdx
	movq	-80(%rbp), %r8
	movq	%rcx, %rax
	movq	%r9, %r11
	subq	%r9, %rax
	movq	%r8, %r10
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	-132(%r8), %edx
	cmpq	%rdx, %rax
	ja	.L3516
	cmpb	$2, -120(%r8)
	movq	-304(%rbp), %rsi
	jne	.L5398
.L3517:
	xorl	%r12d, %r12d
.L3519:
	movq	%r8, %r10
	movq	%r9, %r11
.L3520:
	movq	%rcx, %rax
	movl	-132(%r10), %esi
	movabsq	$-6148914691236517205, %rdx
	movzbl	-1545(%rbp), %r15d
	subq	%r11, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %rax
	ja	.L3525
	cmpb	$2, -120(%r10)
	jne	.L5399
.L3526:
	movq	$0, -1488(%rbp)
.L3528:
	cmpq	%rsi, %rax
	ja	.L3534
	cmpb	$2, -120(%r10)
	jne	.L5400
.L3535:
	movq	$0, -1496(%rbp)
	movl	$10, %r13d
.L3537:
	movzbl	-1545(%rbp), %eax
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movb	%al, -1328(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-112(%rbp), %rax
	movq	-264(%rbp), %r15
	movq	%rax, -1440(%rbp)
	testq	%r15, %r15
	jne	.L3540
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5401
.L3540:
	leal	1(%rbx), %esi
	jmp	.L3489
	.p2align 4,,10
	.p2align 3
.L3103:
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	movabsq	$-6148914691236517205, %rdi
	movq	-80(%rbp), %r8
	movq	%rcx, %rax
	movq	%r9, %rsi
	subq	%r9, %rax
	movq	%r8, %rdx
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%r8), %edi
	cmpq	%rdi, %rax
	jbe	.L5402
	movq	-8(%rcx), %rax
	movzbl	-16(%rcx), %r10d
	subq	$24, %rcx
	movq	(%rcx), %r12
	movq	%rcx, -112(%rbp)
	movq	%rax, -1440(%rbp)
	cmpb	$1, %r10b
	jne	.L5403
.L3464:
	movq	%rcx, %rax
	movl	-132(%rdx), %r9d
	movabsq	$-6148914691236517205, %rdi
	subq	%rsi, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%r9, %rax
	ja	.L3469
	cmpb	$2, -120(%rdx)
	jne	.L5404
.L3470:
	movq	$0, -1488(%rbp)
	movl	$10, %ebx
	movl	$10, %r13d
.L3472:
	cmpq	%r9, %rax
	ja	.L3473
	cmpb	$2, -120(%rdx)
	movq	-304(%rbp), %rsi
	jne	.L5405
.L3474:
	cmpb	$10, %r13b
	je	.L4419
	movl	$10, -1512(%rbp)
	movq	$0, -1496(%rbp)
.L3477:
	movl	%ebx, %r12d
	movl	%r13d, %r15d
.L3481:
	cmpb	$6, %r15b
	je	.L3482
.L3476:
	movl	$6, %esi
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	testb	%al, %al
	je	.L3483
.L3482:
	leaq	.LC95(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
.L3484:
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3104:
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	movq	%rcx, %rdx
	subq	-120(%rbp), %rdx
	sarq	$3, %rdx
	imulq	%rdi, %rdx
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rdx
	ja	.L3778
	cmpb	$2, -120(%rsi)
	jne	.L3779
	addq	$1, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3105:
	cmpb	$0, -227(%rbp)
	je	.L5406
	movq	-216(%rbp), %rax
	leaq	-1328(%rbp), %rdi
	movq	%r14, %rcx
	movabsq	$1099511627775, %rdx
	movb	$1, 5(%rax)
	movq	-304(%rbp), %r8
	movq	-232(%rbp), %rsi
	andq	-224(%rbp), %rdx
	call	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	movl	-1312(%rbp), %eax
	movq	-240(%rbp), %rdx
	leal	1(%rax), %r11d
	movl	%r11d, %ebx
	testq	%rdx, %rdx
	je	.L3965
	movq	184(%rdx), %rsi
	movq	192(%rdx), %rax
	movl	%r11d, %ebx
	movl	-1328(%rbp), %ecx
	subq	%rsi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rcx
	jnb	.L3965
	salq	$4, %rcx
	cmpb	$7, (%rsi,%rcx)
	jne	.L3967
	movq	88(%rdx), %rcx
	movq	96(%rdx), %rax
	movl	-1324(%rbp), %esi
	movq	-304(%rbp), %r9
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, %r8
	cmpq	%rax, %rsi
	jnb	.L5407
	movq	(%rcx,%rsi,8), %r13
	movq	%r13, -1320(%rbp)
	testq	%r13, %r13
	je	.L3970
	movq	-208(%rbp), %rax
	movq	(%rax), %rdx
	cmpq	0(%r13), %rdx
	jne	.L3970
	testq	%rdx, %rdx
	je	.L3972
	movq	16(%rax), %rsi
	movq	16(%r13), %rcx
	xorl	%eax, %eax
	jmp	.L3973
	.p2align 4,,10
	.p2align 3
.L5408:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L3972
.L3973:
	movzbl	(%rcx,%rax), %edi
	cmpb	%dil, (%rsi,%rax)
	je	.L5408
.L3970:
	movl	$19, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-304(%rbp), %rsi
	leaq	.LC116(%rip), %r8
	movq	%r14, %rdi
	movq	%rax, %rcx
	leaq	.LC117(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3963
	.p2align 4,,10
	.p2align 3
.L3106:
	cmpb	$0, -227(%rbp)
	je	.L5409
	movq	-216(%rbp), %rax
	leaq	.LC63(%rip), %rcx
	leaq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movb	$1, 5(%rax)
	movq	-304(%rbp), %rax
	movq	$0, -1320(%rbp)
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	%eax, -1328(%rbp)
	movl	%eax, %ecx
	movl	-1312(%rbp), %eax
	leal	1(%rax), %r13d
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L3948
	movq	136(%rax), %rsi
	movq	144(%rax), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L3948
	salq	$5, %rdx
	movq	(%rsi,%rdx), %r11
	movq	%r11, -1320(%rbp)
	testq	%r11, %r11
	je	.L3949
	movq	-208(%rbp), %rax
	movq	(%rax), %rdx
	cmpq	(%r11), %rdx
	jne	.L3949
	testq	%rdx, %rdx
	je	.L3951
	movq	16(%rax), %rsi
	movq	16(%r11), %rcx
	xorl	%eax, %eax
	jmp	.L3952
	.p2align 4,,10
	.p2align 3
.L5410:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L3951
.L3952:
	movzbl	(%rcx,%rax), %ebx
	cmpb	%bl, (%rsi,%rax)
	je	.L5410
.L3949:
	movl	$18, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-304(%rbp), %rsi
	leaq	.LC116(%rip), %r8
	movq	%r14, %rdi
	movq	%rax, %rcx
	leaq	.LC117(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	-232(%rbp), %rsi
	leaq	-1328(%rbp), %rdi
	movq	%r13, %r8
	movq	%r14, %rcx
	movabsq	$1099511627775, %rdx
	andq	-224(%rbp), %rdx
	call	_ZN2v88internal4wasm21CallIndirectImmediateILNS1_7Decoder12ValidateFlagE1EEC1ENS1_12WasmFeaturesEPS3_PKh
	movl	-1312(%rbp), %eax
	movq	-240(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, -1496(%rbp)
	testq	%rdx, %rdx
	je	.L3892
	movq	184(%rdx), %rsi
	movq	192(%rdx), %rax
	movl	-1328(%rbp), %ecx
	subq	%rsi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rcx
	jnb	.L3892
	salq	$4, %rcx
	cmpb	$7, (%rsi,%rcx)
	jne	.L3895
	movq	88(%rdx), %rcx
	movq	96(%rdx), %rax
	movl	-1324(%rbp), %esi
	movq	-304(%rbp), %r12
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, %r8
	cmpq	%rax, %rsi
	jnb	.L5411
	movq	(%rcx,%rsi,8), %rax
	movq	-112(%rbp), %rdx
	movabsq	$-6148914691236517205, %rsi
	movq	-80(%rbp), %rcx
	movq	%rax, -1320(%rbp)
	movq	%rax, -1488(%rbp)
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	ja	.L5412
	movq	$0, -1568(%rbp)
	cmpb	$2, -120(%rcx)
	jne	.L5413
.L3900:
	movq	-1488(%rbp), %rax
	testq	%rax, %rax
	je	.L3904
	movq	8(%rax), %r15
	leaq	-520(%rbp), %rcx
	leaq	-328(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -528(%rbp)
	leaq	-544(%rbp), %rdi
	movq	%rcx, %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r15d, %rbx
	movq	%rcx, -1440(%rbp)
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %rbx
	jbe	.L3905
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-544(%rbp), %rax
.L3905:
	leaq	(%rbx,%rbx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, -536(%rbp)
	subl	$1, %r15d
	js	.L3906
	leaq	-296(%rbp), %rdi
	movq	%r14, -1520(%rbp)
	movslq	%r15d, %rbx
	movq	%rdi, -1536(%rbp)
	jmp	.L3915
	.p2align 4,,10
	.p2align 3
.L5416:
	cmpb	$2, -120(%rcx)
	movq	-304(%rbp), %r10
	jne	.L5414
.L3908:
	xorl	%r14d, %r14d
	movl	$10, %r13d
.L3910:
	subl	$1, %r15d
	movq	%r10, (%r12)
	subq	$1, %rbx
	movb	%r13b, 8(%r12)
	movq	%r14, 16(%r12)
	cmpl	$-1, %r15d
	je	.L5415
	movq	-544(%rbp), %rax
.L3915:
	leaq	(%rbx,%rbx,2), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rsi
	leaq	(%rax,%rdx,8), %r12
	movq	-112(%rbp), %rdx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L5416
	movq	-1488(%rbp), %rdi
	movzbl	-16(%rdx), %r13d
	subq	$24, %rdx
	movq	(%rdx), %r10
	movq	16(%rdx), %r14
	movq	16(%rdi), %rax
	addq	%rbx, %rax
	addq	(%rdi), %rax
	movzbl	(%rax), %ecx
	movq	%rdx, -112(%rbp)
	cmpb	%r13b, %cl
	je	.L3910
	movzbl	%cl, %r9d
	movzbl	%r13b, %edi
	movq	%r10, -1512(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r13b
	movq	-1512(%rbp), %r10
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3910
	cmpb	$1, %al
	je	.L3910
	movq	%r10, -1560(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1560(%rbp), %r10
	movq	%rax, -1544(%rbp)
	leaq	.LC0(%rip), %rax
	movl	-1528(%rbp), %r9d
	cmpq	%rdx, %r10
	movq	%rax, -1512(%rbp)
	jnb	.L3911
	movq	-1536(%rbp), %rdi
	movq	%r10, %rsi
	movl	%r9d, -1560(%rbp)
	movq	%r10, -1528(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movl	-1560(%rbp), %r9d
	movq	%rax, -1512(%rbp)
	movq	-1528(%rbp), %r10
.L3911:
	movl	%r9d, %edi
	movq	%rdx, -1560(%rbp)
	movq	%r10, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1560(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	-1528(%rbp), %r10
	movq	%rax, %r9
	cmpq	%rdx, %rsi
	jnb	.L3912
	movq	-1536(%rbp), %rdi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1560(%rbp), %r9
	movq	-1528(%rbp), %r10
	movq	%rax, %rcx
.L3912:
	pushq	-1544(%rbp)
	movq	%r10, %rsi
	xorl	%eax, %eax
	movl	%r15d, %r8d
	pushq	-1512(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1520(%rbp), %rdi
	movq	%r10, -1512(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	movq	-1512(%rbp), %r10
	popq	%rdx
	jmp	.L3910
	.p2align 4,,10
	.p2align 3
.L3108:
	leaq	.LC63(%rip), %rcx
	leaq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, -1320(%rbp)
	leaq	1(%r13), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	%eax, -1328(%rbp)
	movl	%eax, %ecx
	movl	-1312(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -1496(%rbp)
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L3850
	movq	136(%rax), %rsi
	movq	144(%rax), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L3850
	salq	$5, %rdx
	movq	(%rsi,%rdx), %rax
	movq	%rax, -1488(%rbp)
	movq	%rax, -1320(%rbp)
	testq	%rax, %rax
	je	.L5417
	movq	%rax, %rbx
	leaq	-520(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	8(%rbx), %r15
	movq	%rax, %xmm0
	movq	%rax, -1440(%rbp)
	leaq	-328(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -528(%rbp)
	movslq	%r15d, %rbx
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %rbx
	jbe	.L3854
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
.L3854:
	movq	-544(%rbp), %rax
	leaq	(%rbx,%rbx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, -536(%rbp)
	subl	$1, %r15d
	js	.L3855
	leaq	-296(%rbp), %rdi
	movq	%r14, -1520(%rbp)
	movslq	%r15d, %rbx
	movq	%rdi, -1536(%rbp)
	jmp	.L3864
	.p2align 4,,10
	.p2align 3
.L5420:
	cmpb	$2, -120(%rcx)
	movq	-304(%rbp), %r10
	jne	.L5418
.L3857:
	xorl	%r14d, %r14d
	movl	$10, %r13d
.L3859:
	subl	$1, %r15d
	movq	%r10, (%r12)
	subq	$1, %rbx
	movb	%r13b, 8(%r12)
	movq	%r14, 16(%r12)
	cmpl	$-1, %r15d
	je	.L5419
	movq	-544(%rbp), %rax
.L3864:
	leaq	(%rbx,%rbx,2), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rsi
	leaq	(%rax,%rdx,8), %r12
	movq	-112(%rbp), %rdx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L5420
	movq	-1488(%rbp), %rcx
	movzbl	-16(%rdx), %r13d
	subq	$24, %rdx
	movq	(%rdx), %r10
	movq	16(%rdx), %r14
	movq	16(%rcx), %rax
	addq	%rbx, %rax
	addq	(%rcx), %rax
	movzbl	(%rax), %ecx
	movq	%rdx, -112(%rbp)
	cmpb	%r13b, %cl
	je	.L3859
	movzbl	%cl, %r9d
	movzbl	%r13b, %edi
	movq	%r10, -1512(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r13b
	movq	-1512(%rbp), %r10
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3859
	cmpb	$1, %al
	je	.L3859
	movq	%r10, -1560(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1560(%rbp), %r10
	movq	%rax, -1544(%rbp)
	leaq	.LC0(%rip), %rax
	movl	-1528(%rbp), %r9d
	cmpq	%rdx, %r10
	movq	%rax, -1512(%rbp)
	jnb	.L3860
	movq	-1536(%rbp), %rdi
	movq	%r10, %rsi
	movl	%r9d, -1560(%rbp)
	movq	%r10, -1528(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movl	-1560(%rbp), %r9d
	movq	%rax, -1512(%rbp)
	movq	-1528(%rbp), %r10
.L3860:
	movl	%r9d, %edi
	movq	%rdx, -1560(%rbp)
	movq	%r10, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1560(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	-1528(%rbp), %r10
	movq	%rax, %r9
	cmpq	%rdx, %rsi
	jnb	.L3861
	movq	-1536(%rbp), %rdi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1560(%rbp), %r9
	movq	-1528(%rbp), %r10
	movq	%rax, %rcx
.L3861:
	pushq	-1544(%rbp)
	movl	%r15d, %r8d
	movq	%r10, %rsi
	xorl	%eax, %eax
	pushq	-1512(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1520(%rbp), %rdi
	movq	%r10, -1512(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	movq	-1512(%rbp), %r10
	popq	%r8
	jmp	.L3859
	.p2align 4,,10
	.p2align 3
.L3097:
	leaq	1(%r13), %rsi
	leaq	.LC62(%rip), %rcx
	movq	%r14, %rdi
	movb	$0, -1324(%rbp)
	leaq	-1312(%rbp), %rdx
	movq	$0, -1320(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-304(%rbp), %rbx
	movl	%eax, -1328(%rbp)
	movl	%eax, %ecx
	movl	-1312(%rbp), %eax
	leal	1(%rax), %r13d
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L3784
	movq	24(%rax), %rdx
	movq	32(%rax), %rax
	movl	%ecx, %esi
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rsi
	jnb	.L3784
	salq	$5, %rsi
	addq	%rsi, %rdx
	movq	%rdx, -1320(%rbp)
	movzbl	(%rdx), %r9d
	movb	%r9b, -1324(%rbp)
	cmpb	$0, 1(%rdx)
	je	.L5421
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rsi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	ja	.L3788
	cmpb	$2, -120(%rcx)
	jne	.L5422
.L3789:
	xorl	%r12d, %r12d
.L3791:
	movq	-264(%rbp), %r15
	testq	%r15, %r15
	jne	.L3786
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5423
	.p2align 4,,10
	.p2align 3
.L3786:
	movl	%r13d, %esi
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3098:
	leaq	-1312(%rbp), %rdx
	leaq	1(%r13), %rsi
	movq	%r14, %rdi
	movb	$0, -1324(%rbp)
	leaq	.LC62(%rip), %rcx
	movq	$0, -1320(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-240(%rbp), %rdx
	movl	%eax, -1328(%rbp)
	movl	%eax, %ecx
	movl	-1312(%rbp), %eax
	leal	1(%rax), %r13d
	testq	%rdx, %rdx
	je	.L3780
	movq	24(%rdx), %rax
	movq	32(%rdx), %rdx
	movl	%ecx, %esi
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3780
	salq	$5, %rsi
	leaq	-128(%rbp), %r15
	leaq	-1360(%rbp), %r12
	addq	%rsi, %rax
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, -1320(%rbp)
	movzbl	(%rax), %eax
	movb	%al, -1324(%rbp)
	movb	%al, -1360(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	testq	%r15, %r15
	jne	.L3782
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3782
	movl	-1328(%rbp), %esi
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder9GetGlobalEj@PLT
	testq	%rax, %rax
	je	.L3783
	cmpl	$-1, -168(%rbp)
	je	.L3783
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3783:
	movq	%rax, -8(%rbx)
	movq	-264(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L3782:
	movl	%r13d, %esi
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3099:
	leaq	.LC65(%rip), %rcx
	leaq	-1320(%rbp), %rdx
	movq	%r14, %rdi
	movb	$0, -1324(%rbp)
	leaq	1(%r13), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-304(%rbp), %r15
	movl	%eax, %ecx
	movq	-200(%rbp), %rax
	movl	%ecx, -1328(%rbp)
	testq	%rax, %rax
	je	.L3766
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	subq	%rsi, %rdx
	cmpl	%edx, %ecx
	jnb	.L3766
	cmpq	%rdx, %rcx
	jnb	.L5351
	movzbl	(%rsi,%rcx), %eax
	movq	-112(%rbp), %rdx
	movabsq	$-6148914691236517205, %rdi
	movq	-80(%rbp), %rsi
	movb	%al, -1324(%rbp)
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	jbe	.L5424
	movq	-152(%rbp), %rax
	movq	-24(%rdx), %rsi
	subq	$24, %rdx
	movzbl	8(%rdx), %r13d
	movq	16(%rdx), %rbx
	movzbl	(%rax,%rcx), %ecx
	movq	%rsi, -1440(%rbp)
	movq	%rdx, -112(%rbp)
	cmpb	%r13b, %cl
	je	.L3773
	movzbl	%cl, %r12d
	movzbl	%r13b, %edi
	movl	%r12d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r13b
	setne	%dl
	testb	%dl, %cl
	je	.L3773
	cmpb	$1, %al
	je	.L3773
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1440(%rbp), %rsi
	movq	%rax, -1496(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -1488(%rbp)
	cmpq	%rdx, %rsi
	jnb	.L3774
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r15
	movq	-296(%rbp), %rdx
	movq	%rax, -1488(%rbp)
.L3774:
	movl	%r12d, %edi
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-1512(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	%rdx, %r15
	jnb	.L3775
	leaq	-296(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1512(%rbp), %r9
	movq	%rax, %rcx
.L3775:
	pushq	-1496(%rbp)
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	xorl	%eax, %eax
	pushq	-1488(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1440(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r11
	popq	%r12
	.p2align 4,,10
	.p2align 3
.L3773:
	leaq	-128(%rbp), %r15
	leaq	-1360(%rbp), %r12
	movb	%r13b, -1360(%rbp)
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rax
	testq	%r15, %r15
	jne	.L3776
	movq	-80(%rbp), %rdx
	cmpb	$0, -120(%rdx)
	jne	.L3776
	movq	%rbx, -8(%rax)
	movq	-184(%rbp), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L5348
	movl	-1328(%rbp), %edx
	movq	%rbx, (%rax,%rdx,8)
.L5348:
	movq	-264(%rbp), %r15
.L3776:
	movl	-1320(%rbp), %eax
	leal	1(%rax), %esi
.L3768:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3100:
	leaq	.LC65(%rip), %rcx
	leaq	-1320(%rbp), %rdx
	movq	%r14, %rdi
	movb	$0, -1324(%rbp)
	leaq	1(%r13), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-304(%rbp), %r15
	movl	%eax, %ecx
	movq	-200(%rbp), %rax
	movl	%ecx, -1328(%rbp)
	testq	%rax, %rax
	je	.L3755
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	subq	%rsi, %rdx
	cmpl	%edx, %ecx
	jnb	.L3755
	cmpq	%rdx, %rcx
	jnb	.L5351
	movzbl	(%rsi,%rcx), %eax
	movq	-112(%rbp), %rdx
	movabsq	$-6148914691236517205, %rdi
	movq	-80(%rbp), %rsi
	movb	%al, -1324(%rbp)
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	jbe	.L5425
	movq	-152(%rbp), %rax
	movq	-24(%rdx), %r12
	subq	$24, %rdx
	movq	16(%rdx), %rbx
	movzbl	(%rax,%rcx), %r9d
	movzbl	8(%rdx), %ecx
	movq	%rdx, -112(%rbp)
	cmpb	%cl, %r9b
	je	.L3762
	movzbl	%r9b, %r13d
	movzbl	%cl, %edi
	movl	%r13d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3762
	cmpb	$1, %al
	je	.L3762
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	%rax, -1488(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -1440(%rbp)
	cmpq	%rdx, %r12
	jnb	.L3763
	leaq	-296(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r15
	movq	-296(%rbp), %rdx
	movq	%rax, -1440(%rbp)
.L3763:
	movl	%r13d, %edi
	movq	%rdx, -1496(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-1496(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	%rdx, %r15
	jnb	.L3764
	leaq	-296(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -1496(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1496(%rbp), %r9
	movq	%rax, %rcx
.L3764:
	pushq	-1488(%rbp)
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	pushq	-1440(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r13
	popq	%r15
	.p2align 4,,10
	.p2align 3
.L3762:
	movq	-264(%rbp), %r15
	testq	%r15, %r15
	jne	.L3765
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3765
	movq	-184(%rbp), %rax
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L3765
	movl	-1328(%rbp), %edx
	movq	%rbx, (%rax,%rdx,8)
	movq	-264(%rbp), %r15
.L3765:
	movl	-1320(%rbp), %eax
	leal	1(%rax), %esi
.L3757:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3095:
	cmpb	$0, -225(%rbp)
	je	.L5426
	movq	-216(%rbp), %rax
	leaq	.LC34(%rip), %rcx
	leaq	-1324(%rbp), %rdx
	movq	%r14, %rdi
	movb	$1, 7(%rax)
	movabsq	$4294967296, %rax
	movq	%rax, -1328(%rbp)
	movq	-304(%rbp), %rax
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-304(%rbp), %r8
	movl	%eax, -1328(%rbp)
	movl	%eax, %ecx
	movl	-1324(%rbp), %eax
	leal	1(%rax), %r13d
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L3812
	movq	184(%rax), %rdi
	movq	192(%rax), %rdx
	movl	%ecx, %esi
	subq	%rdi, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3812
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r11
	movabsq	$-6148914691236517205, %rdx
	movq	-80(%rbp), %r9
	movq	%rcx, %rax
	subq	%r11, %rax
	movl	-132(%r9), %r10d
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%r10, %rax
	ja	.L3814
	cmpb	$2, -120(%r9)
	jne	.L5427
.L3815:
	xorl	%r12d, %r12d
.L3817:
	cmpq	%r10, %rax
	ja	.L3823
	cmpb	$2, -120(%r9)
	movq	-304(%rbp), %rsi
	jne	.L5428
.L3824:
	xorl	%ebx, %ebx
.L3826:
	movq	-264(%rbp), %r15
	movq	-304(%rbp), %r8
	testq	%r15, %r15
	jne	.L3811
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3811
	movl	-1328(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	movq	-176(%rbp), %rdi
	subl	-312(%rbp), %r8d
	call	_ZN2v88internal8compiler16WasmGraphBuilder8TableSetEjPNS1_4NodeES4_i@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3829
	cmpl	$-1, -168(%rbp)
	je	.L3829
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3829:
	movq	-264(%rbp), %r15
	movq	-304(%rbp), %r8
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3096:
	cmpb	$0, -225(%rbp)
	je	.L5429
	movq	-216(%rbp), %rax
	leaq	.LC34(%rip), %rcx
	leaq	-1324(%rbp), %rdx
	movq	%r14, %rdi
	movb	$1, 7(%rax)
	movabsq	$4294967296, %rax
	movq	%rax, -1328(%rbp)
	movq	-304(%rbp), %rax
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-240(%rbp), %rsi
	movq	-304(%rbp), %r12
	movl	%eax, -1328(%rbp)
	movl	%eax, %ecx
	movl	-1324(%rbp), %eax
	leal	1(%rax), %r13d
	testq	%rsi, %rsi
	je	.L3797
	movq	192(%rsi), %rdx
	subq	184(%rsi), %rdx
	movl	%ecx, %eax
	sarq	$4, %rdx
	cmpq	%rdx, %rax
	jnb	.L3797
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movabsq	$-6148914691236517205, %r8
	movq	%rcx, %rdx
	subq	-120(%rbp), %rdx
	sarq	$3, %rdx
	imulq	%r8, %rdx
	movl	-132(%rdi), %r8d
	cmpq	%r8, %rdx
	ja	.L3799
	cmpb	$2, -120(%rdi)
	jne	.L5430
.L3800:
	movq	$0, -1440(%rbp)
.L3802:
	movl	-1328(%rbp), %eax
	movq	-240(%rbp), %rsi
.L3803:
	salq	$4, %rax
	addq	184(%rsi), %rax
	leaq	-128(%rbp), %r15
	movzbl	(%rax), %eax
	leaq	-1360(%rbp), %r12
	leaq	-304(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r12, %rdx
	movb	%al, -1360(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	movq	-304(%rbp), %rsi
	testq	%r15, %r15
	jne	.L3796
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5431
	.p2align 4,,10
	.p2align 3
.L3796:
	movq	-296(%rbp), %rax
	addq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3094:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	movl	%eax, %r8d
	movq	-296(%rbp), %rax
	leal	1(%r8), %esi
	addq	-304(%rbp), %rsi
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3117:
	cmpb	$0, -231(%rbp)
	je	.L5432
	movq	-216(%rbp), %rax
	movb	$1, 1(%rax)
	movq	-80(%rbp), %rbx
	cmpq	-88(%rbp), %rbx
	je	.L5433
	movzbl	-136(%rbx), %eax
	cmpb	$4, %al
	je	.L3267
	cmpb	$5, %al
	je	.L3268
	movq	%r14, %rdi
	leaq	.LC85(%rip), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3118:
	cmpb	$0, -231(%rbp)
	je	.L5434
	movq	-216(%rbp), %rax
	movq	-1504(%rbp), %rsi
	leaq	-1392(%rbp), %rdi
	movq	%r14, %rdx
	movb	$1, 1(%rax)
	movq	-304(%rbp), %rcx
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -1388(%rbp)
	jne	.L3218
	movq	-240(%rbp), %rax
	movl	-1384(%rbp), %ecx
	testq	%rax, %rax
	je	.L4375
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L3219
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -1376(%rbp)
	movq	%rax, %r12
.L3220:
	testq	%r12, %r12
	je	.L3221
	movq	8(%r12), %r15
	leaq	-968(%rbp), %rcx
	leaq	-776(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rcx, -1512(%rbp)
	leaq	-992(%rbp), %rdi
	punpcklqdq	%xmm0, %xmm0
	movslq	%r15d, %rbx
	movq	%rax, -976(%rbp)
	movaps	%xmm0, -992(%rbp)
	cmpq	$8, %rbx
	jbe	.L3222
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-992(%rbp), %rcx
.L3222:
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, -984(%rbp)
	subl	$1, %r15d
	js	.L4330
	leaq	-296(%rbp), %rax
	movq	%r14, -1440(%rbp)
	movslq	%r15d, %rbx
	movq	%r12, %r10
	movq	%rax, -1496(%rbp)
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L5436:
	cmpb	$2, -120(%rsi)
	movq	-304(%rbp), %r14
	jne	.L5435
.L3225:
	xorl	%r11d, %r11d
	movl	$10, %r13d
.L3227:
	subl	$1, %r15d
	movq	%r14, (%r12)
	subq	$1, %rbx
	movb	%r13b, 8(%r12)
	movq	%r11, 16(%r12)
	cmpl	$-1, %r15d
	je	.L5316
	movq	-992(%rbp), %rcx
.L3232:
	movq	16(%r10), %rax
	movq	-80(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	addq	%rbx, %rax
	addq	(%r10), %rax
	movzbl	(%rax), %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %r12
	movq	-112(%rbp), %rcx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	jbe	.L5436
	movzbl	-16(%rcx), %r13d
	movq	-24(%rcx), %r14
	subq	$24, %rcx
	movq	16(%rcx), %r11
	movq	%rcx, -112(%rbp)
	cmpb	%r13b, %dl
	je	.L3227
	cmpb	$6, %dl
	sete	%al
	cmpb	$8, %r13b
	sete	%cl
	testb	%al, %al
	je	.L4608
	testb	%cl, %cl
	je	.L4608
	movl	$8, %r13d
	jmp	.L3227
	.p2align 4,,10
	.p2align 3
.L3119:
	movq	-80(%rbp), %rbx
	cmpq	-88(%rbp), %rbx
	je	.L5437
	movzbl	-136(%rbx), %eax
	testb	%al, %al
	jne	.L5438
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv
	testb	%al, %al
	jne	.L3426
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3120:
	movq	-1504(%rbp), %rsi
	leaq	-1328(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -1324(%rbp)
	jne	.L3368
	movq	-240(%rbp), %rax
	movl	-1320(%rbp), %ecx
	testq	%rax, %rax
	je	.L4401
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L3369
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -1312(%rbp)
.L3368:
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rsi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L5439
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %ecx
	subq	$24, %rdx
	movq	(%rdx), %r12
	movq	%rdx, -112(%rbp)
	movq	%rax, -1560(%rbp)
	cmpb	$1, %cl
	jne	.L5440
.L3374:
	movq	-1312(%rbp), %rax
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3377
	movq	8(%rax), %r15
	leaq	-520(%rbp), %rcx
	leaq	-328(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rcx, -1440(%rbp)
	leaq	-544(%rbp), %rdi
	punpcklqdq	%xmm0, %xmm0
	movslq	%r15d, %rbx
	movq	%rax, -528(%rbp)
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %rbx
	jbe	.L3378
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-544(%rbp), %rcx
.L3378:
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, -536(%rbp)
	subl	$1, %r15d
	js	.L4327
	leaq	-296(%rbp), %rax
	movslq	%r15d, %rbx
	movl	%r15d, %r8d
	movq	%r14, -1488(%rbp)
	movq	%rax, -1512(%rbp)
	movq	%r12, %r15
	jmp	.L3388
	.p2align 4,,10
	.p2align 3
.L5442:
	cmpb	$2, -120(%rsi)
	movq	-304(%rbp), %r14
	jne	.L5441
.L3381:
	xorl	%r11d, %r11d
	movl	$10, %r13d
.L3383:
	subl	$1, %r8d
	movq	%r14, (%r12)
	subq	$1, %rbx
	movb	%r13b, 8(%r12)
	movq	%r11, 16(%r12)
	cmpl	$-1, %r8d
	je	.L5322
	movq	-544(%rbp), %rcx
.L3388:
	movq	16(%r15), %rax
	movq	-80(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	addq	%rbx, %rax
	addq	(%r15), %rax
	movzbl	(%rax), %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %r12
	movq	-112(%rbp), %rcx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	jbe	.L5442
	movzbl	-16(%rcx), %r13d
	movq	-24(%rcx), %r14
	subq	$24, %rcx
	movq	16(%rcx), %r11
	movq	%rcx, -112(%rbp)
	cmpb	%r13b, %dl
	je	.L3383
	cmpb	$6, %dl
	sete	%al
	cmpb	$8, %r13b
	sete	%cl
	testb	%al, %al
	je	.L4613
	testb	%cl, %cl
	je	.L4613
	movl	$8, %r13d
	jmp	.L3383
	.p2align 4,,10
	.p2align 3
.L3121:
	movq	-1504(%rbp), %rsi
	leaq	-1360(%rbp), %r12
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -1356(%rbp)
	jne	.L3314
	movq	-240(%rbp), %rax
	movl	-1352(%rbp), %ecx
	testq	%rax, %rax
	je	.L4391
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L3315
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -1344(%rbp)
	movq	%rax, %r12
.L3316:
	testq	%r12, %r12
	je	.L3318
	movq	8(%r12), %r15
	leaq	-744(%rbp), %rcx
	leaq	-552(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rcx, -1512(%rbp)
	leaq	-768(%rbp), %rdi
	punpcklqdq	%xmm0, %xmm0
	movslq	%r15d, %rbx
	movq	%rax, -752(%rbp)
	movaps	%xmm0, -768(%rbp)
	cmpq	$8, %rbx
	jbe	.L3319
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-768(%rbp), %rcx
.L3319:
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, -760(%rbp)
	subl	$1, %r15d
	js	.L4324
	leaq	-296(%rbp), %rax
	movq	%r14, -1440(%rbp)
	movslq	%r15d, %rbx
	movq	%r12, %r10
	movq	%rax, -1496(%rbp)
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L5444:
	cmpb	$2, -120(%rsi)
	movq	-304(%rbp), %r14
	jne	.L5443
.L3322:
	xorl	%r11d, %r11d
	movl	$10, %r13d
.L3324:
	subl	$1, %r15d
	movq	%r14, (%r12)
	subq	$1, %rbx
	movb	%r13b, 8(%r12)
	movq	%r11, 16(%r12)
	cmpl	$-1, %r15d
	je	.L5319
	movq	-768(%rbp), %rcx
.L3329:
	movq	16(%r10), %rax
	movq	-80(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	addq	%rbx, %rax
	addq	(%r10), %rax
	movzbl	(%rax), %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %r12
	movq	-112(%rbp), %rcx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	jbe	.L5444
	movzbl	-16(%rcx), %r13d
	movq	-24(%rcx), %r14
	subq	$24, %rcx
	movq	16(%rcx), %r11
	movq	%rcx, -112(%rbp)
	cmpb	%r13b, %dl
	je	.L3324
	cmpb	$6, %dl
	sete	%al
	cmpb	$8, %r13b
	sete	%cl
	testb	%al, %al
	je	.L4611
	testb	%cl, %cl
	je	.L4611
	movl	$8, %r13d
	jmp	.L3324
	.p2align 4,,10
	.p2align 3
.L3124:
	movq	-80(%rbp), %rax
	testq	%r15, %r15
	je	.L5445
	movl	-132(%rax), %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	-120(%rbp), %rdx
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, -112(%rbp)
	je	.L3732
.L4341:
	movq	%rdx, -112(%rbp)
.L3731:
	testq	%r15, %r15
	jne	.L3732
.L4340:
	cmpb	$0, -120(%rax)
	jne	.L3732
	movq	-184(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$0, (%rdx)
	movq	$0, 48(%rdx)
	movq	$0, 40(%rdx)
	movups	%xmm0, 8(%rdx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rdx)
.L3732:
	movb	$2, -120(%rax)
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3122:
	movq	-1504(%rbp), %rsi
	leaq	-1424(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE1EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	cmpb	$10, -1420(%rbp)
	jne	.L3126
	movq	-240(%rbp), %rax
	movl	-1416(%rbp), %ecx
	testq	%rax, %rax
	je	.L4359
	movq	88(%rax), %rdx
	movq	96(%rax), %r8
	movl	%ecx, %esi
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L3127
	movq	(%rdx,%rsi,8), %rax
	movq	%rax, -1408(%rbp)
	movq	%rax, %r12
.L3128:
	testq	%r12, %r12
	je	.L3130
	movq	8(%r12), %r15
	leaq	-1192(%rbp), %rcx
	leaq	-1000(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rcx, -1512(%rbp)
	leaq	-1216(%rbp), %rdi
	punpcklqdq	%xmm0, %xmm0
	movslq	%r15d, %rbx
	movq	%rax, -1200(%rbp)
	movaps	%xmm0, -1216(%rbp)
	cmpq	$8, %rbx
	jbe	.L3131
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-1216(%rbp), %rcx
.L3131:
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, -1208(%rbp)
	subl	$1, %r15d
	js	.L4321
	leaq	-296(%rbp), %rax
	movq	%r14, -1440(%rbp)
	movslq	%r15d, %rbx
	movq	%r12, %r10
	movq	%rax, -1496(%rbp)
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L5447:
	cmpb	$2, -120(%rsi)
	movq	-304(%rbp), %r14
	jne	.L5446
.L3134:
	xorl	%r11d, %r11d
	movl	$10, %r13d
.L3136:
	subl	$1, %r15d
	movq	%r14, (%r12)
	subq	$1, %rbx
	movb	%r13b, 8(%r12)
	movq	%r11, 16(%r12)
	cmpl	$-1, %r15d
	je	.L5312
	movq	-1216(%rbp), %rcx
.L3141:
	movq	16(%r10), %rax
	movq	-80(%rbp), %rsi
	movabsq	$-6148914691236517205, %rdi
	addq	%rbx, %rax
	addq	(%r10), %rax
	movzbl	(%rax), %edx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %r12
	movq	-112(%rbp), %rcx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	jbe	.L5447
	movzbl	-16(%rcx), %r13d
	movq	-24(%rcx), %r14
	subq	$24, %rcx
	movq	16(%rcx), %r11
	movq	%rcx, -112(%rbp)
	cmpb	%r13b, %dl
	je	.L3136
	cmpb	$6, %dl
	sete	%al
	cmpb	$8, %r13b
	sete	%cl
	testb	%al, %al
	je	.L4606
	testb	%cl, %cl
	je	.L4606
	movl	$8, %r13d
	jmp	.L3136
	.p2align 4,,10
	.p2align 3
.L3113:
	movq	-80(%rbp), %rbx
	cmpq	-88(%rbp), %rbx
	je	.L5448
	movzbl	-136(%rbx), %eax
	cmpb	$4, %al
	je	.L5449
	testb	%al, %al
	jne	.L3433
	movl	-112(%rbx), %eax
	cmpl	%eax, -72(%rbx)
	je	.L3433
	movq	-128(%rbx), %rsi
	leaq	.LC93(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3114:
	cmpb	$0, -231(%rbp)
	je	.L5450
	movq	-216(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	-1324(%rbp), %rdx
	leaq	.LC61(%rip), %rcx
	movb	$1, 1(%rax)
	movq	-304(%rbp), %r12
	leaq	1(%r12), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	leaq	-1304(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	%eax, -1328(%rbp)
	movl	-1324(%rbp), %eax
	leaq	.LC64(%rip), %rcx
	movq	$0, -1312(%rbp)
	leaq	1(%r12,%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-80(%rbp), %rdi
	movl	-1304(%rbp), %edx
	movl	%eax, -1320(%rbp)
	movl	%eax, %ecx
	movl	-1324(%rbp), %eax
	movq	%rdi, -1488(%rbp)
	subq	-88(%rbp), %rdi
	addl	%eax, %edx
	movl	-1328(%rbp), %esi
	movq	-304(%rbp), %r15
	movl	%edx, -1296(%rbp)
	movq	%rdi, %rdx
	movabsq	$-1085102592571150095, %rdi
	sarq	$3, %rdx
	movq	%rsi, -1496(%rbp)
	movq	%r15, %r9
	movq	%rsi, %r8
	imulq	%rdi, %rdx
	cmpq	%rsi, %rdx
	jbe	.L5451
	movq	-240(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3278
	movq	256(%rdx), %rsi
	movq	264(%rdx), %rdx
	movl	%ecx, %edi
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rdi
	jnb	.L3278
	movq	-112(%rbp), %rdx
	leaq	(%rsi,%rdi,8), %rcx
	movq	-1488(%rbp), %rdi
	movabsq	$-6148914691236517205, %rsi
	movq	%rcx, -1312(%rbp)
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rdi), %esi
	cmpq	%rsi, %rax
	ja	.L5452
	cmpb	$2, -120(%rdi)
	jne	.L5453
.L3281:
	movq	$0, -1512(%rbp)
.L3283:
	movq	%r15, -1520(%rbp)
	movq	-1312(%rbp), %rcx
.L3284:
	movq	(%rcx), %rax
	movq	8(%rax), %rcx
	movq	%rcx, -1440(%rbp)
	testq	%rcx, %rcx
	je	.L3292
	xorl	%r13d, %r13d
	leaq	-304(%rbp), %rsi
	leaq	-128(%rbp), %r15
	movb	%bl, -1528(%rbp)
	movq	%r14, -1536(%rbp)
	leaq	-1360(%rbp), %r12
	movq	%r13, %r14
	movq	%rax, %rbx
	movq	%rsi, %r13
	.p2align 4,,10
	.p2align 3
.L3289:
	movq	16(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	addq	%r14, %rdx
	addq	(%rbx), %rdx
	addq	$1, %r14
	movzbl	(%rdx), %edx
	movb	%dl, -1360(%rbp)
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	%r14, -1440(%rbp)
	jne	.L3289
	movzbl	-1528(%rbp), %ebx
	movq	-1536(%rbp), %r14
.L3292:
	movq	-1496(%rbp), %rcx
	movq	-1488(%rbp), %r12
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rcx, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	salq	$3, %rax
	subq	%rax, %r12
	movq	-120(%rbp), %rax
	leaq	-136(%r12), %rsi
	movl	-132(%r12), %r13d
	movq	%rsi, -1496(%rbp)
	movq	%rax, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb
	testl	%eax, %eax
	jne	.L5454
	movl	-1328(%rbp), %eax
	movq	-1512(%rbp), %rsi
	movq	$0, -1392(%rbp)
	movq	-176(%rbp), %rdi
	movq	$0, -1360(%rbp)
	movl	%eax, -1528(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder15GetExceptionTagEPNS1_4NodeE@PLT
	leaq	-184(%rbp), %r11
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3293
	cmpl	$-1, -168(%rbp)
	je	.L3293
	movq	%r11, %rdi
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, -1536(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-1536(%rbp), %r11
	movq	%rax, %r15
.L3293:
	movl	-1320(%rbp), %esi
	movq	-176(%rbp), %rdi
	movq	%r11, -1536(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder25LoadExceptionTagFromTableEj@PLT
	movq	-1536(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3294
	cmpl	$-1, -168(%rbp)
	je	.L3294
	movq	%r11, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-1536(%rbp), %r11
	movq	%rax, %rdx
.L3294:
	movq	-176(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r11, -1536(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder17ExceptionTagEqualEPNS1_4NodeES4_@PLT
	movq	-1536(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L3295
	cmpl	$-1, -168(%rbp)
	je	.L3295
	movq	%r14, %rsi
	movq	%r11, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-1536(%rbp), %r11
	movq	%rax, %rsi
.L3295:
	movq	-176(%rbp), %rdi
	leaq	-1360(%rbp), %r12
	leaq	-1392(%rbp), %rdx
	movq	%r11, -1536(%rbp)
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder12BranchNoHintEPNS1_4NodeEPS4_S5_@PLT
	movq	-1536(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3296
	cmpl	$-1, -168(%rbp)
	je	.L3296
	movq	%r11, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-1536(%rbp), %r11
.L3296:
	movq	-184(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r11, -1536(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0
	movq	-1360(%rbp), %rdx
	movq	-1512(%rbp), %rsi
	leaq	8(%rax), %rcx
	movq	%rdx, 8(%r15)
	movq	-1392(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-176(%rbp), %rdx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	movq	%rax, 40(%rdx)
	movq	-1312(%rbp), %rdx
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder18GetExceptionValuesEPNS1_4NodeEPKNS0_4wasm13WasmExceptionE@PLT
	cmpq	$0, -1440(%rbp)
	movq	-1536(%rbp), %r11
	je	.L3303
	movq	-1488(%rbp), %rdi
	movq	-1440(%rbp), %r10
	leaq	0(%r13,%r13,2), %rdx
	leaq	16(%rdi,%rdx,8), %rcx
	leaq	(%r10,%r13), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%r10,8), %rsi
	leaq	(%rdi,%rdx,8), %rdx
	cmpq	%rdx, %rax
	setnb	%dil
	cmpq	%rsi, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L3300
	leaq	-1(%r10), %rdx
	cmpq	$22, %rdx
	jbe	.L3300
	movq	%r10, %rsi
	movq	%rax, %rdx
	shrq	%rsi
	salq	$4, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L3302:
	movdqu	(%rdx), %xmm0
	addq	$16, %rdx
	addq	$48, %rcx
	movq	%xmm0, -48(%rcx)
	movhps	%xmm0, -24(%rcx)
	cmpq	%rsi, %rdx
	jne	.L3302
	movq	-1440(%rbp), %rdi
	movq	%rdi, %rdx
	andq	$-2, %rdx
	andl	$1, %edi
	je	.L3303
	movq	(%rax,%rdx,8), %rcx
	movq	-1488(%rbp), %rdi
	addq	%rdx, %r13
	leaq	0(%r13,%r13,2), %rax
	movq	%rcx, 16(%rdi,%rax,8)
.L3303:
	movl	-1528(%rbp), %edx
	movq	%r11, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj
	movq	-176(%rbp), %rax
	leaq	8(%r15), %rdx
	movq	%r15, -184(%rbp)
	movq	-1496(%rbp), %rdi
	movq	%rdx, 24(%rax)
	movq	-176(%rbp), %rax
	leaq	16(%r15), %rdx
	addq	$24, %r15
	movq	%rdx, 32(%rax)
	movq	-176(%rbp), %rax
	leaq	64(%rdi), %rdx
	movq	%r15, 40(%rax)
	leaq	24(%rdi), %rax
	cmpb	$3, (%rdi)
	cmovne	%rdx, %rax
	movb	$1, 32(%rax)
.L3308:
	movl	-1296(%rbp), %eax
	xorl	%r15d, %r15d
	movabsq	$-6148914691236517205, %r13
	addl	$1, %eax
	cmpq	$0, -1440(%rbp)
	movl	%eax, -1488(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, -1528(%rbp)
	je	.L3313
	movb	%bl, -1496(%rbp)
	movq	%r14, %rbx
	movq	-1440(%rbp), %r14
	jmp	.L3309
	.p2align 4,,10
	.p2align 3
.L5457:
	cmpb	$2, -120(%rcx)
	jne	.L5455
.L3311:
	addq	$1, %r15
	cmpq	%r15, %r14
	je	.L5456
.L3309:
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	movl	-132(%rcx), %esi
	sarq	$3, %rax
	imulq	%r13, %rax
	cmpq	%rsi, %rax
	jbe	.L5457
	subq	$24, %rdx
	movq	%rdx, -112(%rbp)
	jmp	.L3311
	.p2align 4,,10
	.p2align 3
.L3115:
	cmpb	$0, -231(%rbp)
	je	.L5458
	movq	-216(%rbp), %rax
	movabsq	$-6148914691236517205, %rsi
	movb	$1, 1(%rax)
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L5459
	movzbl	-16(%rdx), %ecx
	movq	-24(%rdx), %r13
	subq	$24, %rdx
	movq	16(%rdx), %r12
	movq	%rdx, -112(%rbp)
	cmpb	$9, %cl
	je	.L3186
	movzbl	%cl, %edi
	movl	$9, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3186
	cmpb	$10, %cl
	je	.L3186
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r15
	movq	-296(%rbp), %rax
	cmpq	%rax, %r13
	jnb	.L3187
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
	movq	-296(%rbp), %rax
.L3187:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3188
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3188:
	pushq	%r15
	movq	%r13, %rsi
	leaq	.LC14(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	popq	%rsi
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3116:
	cmpb	$0, -231(%rbp)
	je	.L5460
	movq	-216(%rbp), %rax
	leaq	.LC64(%rip), %rcx
	leaq	-1344(%rbp), %rdx
	movq	%r14, %rdi
	movb	$1, 1(%rax)
	movq	-304(%rbp), %rax
	movq	$0, -1352(%rbp)
	leaq	1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movl	%eax, -1360(%rbp)
	movl	%eax, %ecx
	movl	-1344(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -1496(%rbp)
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L3193
	movq	256(%rax), %rdx
	movq	264(%rax), %rax
	movl	%ecx, %esi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	jnb	.L3193
	leaq	(%rdx,%rsi,8), %rax
	movq	%rax, -1352(%rbp)
	movq	(%rax), %rax
	movq	%rax, -1488(%rbp)
	testq	%rax, %rax
	je	.L5461
	movq	%rax, %rbx
	leaq	-520(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	8(%rbx), %r12
	movq	%rax, %xmm0
	movq	%rax, -1440(%rbp)
	leaq	-328(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -528(%rbp)
	movslq	%r12d, %rbx
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %rbx
	jbe	.L3196
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
.L3196:
	movq	-544(%rbp), %rax
	leaq	(%rbx,%rbx,2), %rdx
	movl	%r12d, %r8d
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, -536(%rbp)
	subl	$1, %r8d
	js	.L4331
	leaq	-296(%rbp), %rbx
	movq	%r14, -1520(%rbp)
	movslq	%r8d, %r13
	movl	%r8d, %r14d
	movq	%rbx, -1536(%rbp)
	jmp	.L3204
	.p2align 4,,10
	.p2align 3
.L5463:
	cmpb	$2, -120(%rcx)
	movq	-304(%rbp), %r10
	jne	.L5462
.L3199:
	xorl	%r15d, %r15d
	movl	$10, %r12d
.L3201:
	subl	$1, %r14d
	movq	%r10, (%rbx)
	subq	$1, %r13
	movb	%r12b, 8(%rbx)
	movq	%r15, 16(%rbx)
	cmpl	$-1, %r14d
	je	.L5315
	movq	-544(%rbp), %rax
.L3204:
	leaq	0(%r13,%r13,2), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rdi
	leaq	(%rax,%rdx,8), %rbx
	movq	-112(%rbp), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rsi, %rax
	jbe	.L5463
	movq	-1488(%rbp), %rcx
	movzbl	-16(%rdx), %r12d
	subq	$24, %rdx
	movq	(%rdx), %r10
	movq	16(%rdx), %r15
	movq	16(%rcx), %rax
	addq	%r13, %rax
	addq	(%rcx), %rax
	movzbl	(%rax), %ecx
	movq	%rdx, -112(%rbp)
	cmpb	%r12b, %cl
	je	.L3201
	movzbl	%cl, %r9d
	movzbl	%r12b, %edi
	movq	%r10, -1512(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-1512(%rbp), %r10
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r12b
	setne	%dl
	testb	%dl, %cl
	je	.L3201
	cmpb	$1, %al
	je	.L3201
	movq	%r10, -1560(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1560(%rbp), %r10
	movq	%rax, -1544(%rbp)
	leaq	.LC0(%rip), %rax
	movl	-1528(%rbp), %r9d
	cmpq	%rdx, %r10
	movq	%rax, -1512(%rbp)
	jnb	.L3202
	movq	-1536(%rbp), %rdi
	movq	%r10, %rsi
	movl	%r9d, -1560(%rbp)
	movq	%r10, -1528(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movl	-1560(%rbp), %r9d
	movq	%rax, -1512(%rbp)
	movq	-1528(%rbp), %r10
.L3202:
	movl	%r9d, %edi
	movq	%rdx, -1560(%rbp)
	movq	%r10, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1560(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	-1528(%rbp), %r10
	movq	%rax, %r9
	cmpq	%rdx, %rsi
	jnb	.L3203
	movq	-1536(%rbp), %rdi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1560(%rbp), %r9
	movq	-1528(%rbp), %r10
	movq	%rax, %rcx
.L3203:
	pushq	-1544(%rbp)
	movq	%r10, %rsi
	xorl	%eax, %eax
	movl	%r14d, %r8d
	pushq	-1512(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1520(%rbp), %rdi
	movq	%r10, -1512(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	movq	-1512(%rbp), %r10
	popq	%rdx
	jmp	.L3201
	.p2align 4,,10
	.p2align 3
.L3111:
	leaq	-1324(%rbp), %rdx
	leaq	1(%r13), %rsi
	movq	%r14, %rdi
	leaq	.LC61(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rsi
	movl	%eax, -1328(%rbp)
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L5464
	movzbl	-16(%rdx), %ecx
	movq	-24(%rdx), %r13
	subq	$24, %rdx
	movq	16(%rdx), %r12
	movq	-304(%rbp), %r9
	movq	%rdx, -112(%rbp)
	cmpb	$1, %cl
	jne	.L5465
.L3608:
	movq	-264(%rbp), %r15
	movq	%r9, %rsi
	movl	$1, %r13d
	testq	%r15, %r15
	jne	.L3611
	movq	-80(%rbp), %rdx
	movl	-1328(%rbp), %ecx
	movabsq	$-1085102592571150095, %rbx
	movq	%rdx, %rax
	subq	-88(%rbp), %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	cmpq	%rcx, %rax
	jbe	.L5466
	movq	%rcx, %rax
	movq	%r14, %rdi
	salq	$4, %rax
	addq	%rcx, %rax
	salq	$3, %rax
	subq	%rax, %rdx
	leaq	-136(%rdx), %rbx
	movl	$1, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15TypeCheckBranchEPNS6_7ControlEb
	testl	%eax, %eax
	jne	.L3613
	movq	-184(%rbp), %r15
	movl	-1328(%rbp), %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%eax, -1440(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0
	cmpl	$3, (%r15)
	movq	%rax, %r8
	jne	.L3614
	movl	$2, (%r15)
.L3614:
	leaq	8(%r8), %r9
	movq	-176(%rbp), %rdi
	leaq	8(%r15), %r13
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rcx
	movq	%r8, -1496(%rbp)
	movq	%r9, -1488(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder12BranchNoHintEPNS1_4NodeEPS4_S5_@PLT
	movq	-1488(%rbp), %r9
	movq	-1496(%rbp), %r8
	leaq	-184(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3615
	cmpl	$-1, -168(%rbp)
	je	.L3615
	movq	%r11, %rdi
	movq	%r14, %rsi
	movq	%r9, -1512(%rbp)
	movq	%r8, -1496(%rbp)
	movq	%r11, -1488(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-1512(%rbp), %r9
	movq	-1496(%rbp), %r8
	movq	-1488(%rbp), %r11
.L3615:
	movq	-176(%rbp), %rax
	movq	%r8, -184(%rbp)
	movq	%r14, %rsi
	movq	%r11, %rdi
	leaq	16(%r8), %rdx
	addq	$24, %r8
	movq	%r9, 24(%rax)
	movq	-176(%rbp), %rax
	movq	%rdx, 32(%rax)
	movq	-176(%rbp), %rax
	movl	-1440(%rbp), %edx
	movq	%r8, 40(%rax)
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj
	movq	-176(%rbp), %rax
	leaq	16(%r15), %rdx
	movq	%r15, -184(%rbp)
	addq	$24, %r15
	movq	-304(%rbp), %rsi
	movq	%r13, 24(%rax)
	movq	-176(%rbp), %rax
	movq	%rdx, 32(%rax)
	movq	-176(%rbp), %rax
	leaq	24(%rbx), %rdx
	movq	%r15, 40(%rax)
	leaq	64(%rbx), %rax
	cmpb	$3, (%rbx)
	cmove	%rdx, %rax
	movb	$1, 32(%rax)
	movq	-264(%rbp), %r15
.L3618:
	movl	-1324(%rbp), %eax
	leal	1(%rax), %r13d
.L3611:
	movq	-296(%rbp), %rax
	addq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3112:
	leaq	-1324(%rbp), %rdx
	leaq	1(%r13), %rsi
	movq	%r14, %rdi
	movabsq	$-1085102592571150095, %rbx
	leaq	.LC61(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_.constprop.0
	movq	-80(%rbp), %rdx
	movq	-304(%rbp), %r11
	movl	%eax, %edi
	movl	%eax, -1328(%rbp)
	movq	%rdx, %rsi
	subq	-88(%rbp), %rsi
	sarq	$3, %rsi
	imulq	%rbx, %rsi
	cmpq	%rdi, %rsi
	jbe	.L5467
	movq	%rdi, %rax
	movq	%rdx, %rbx
	salq	$4, %rax
	addq	%rdi, %rax
	salq	$3, %rax
	subq	%rax, %rbx
	cmpb	$0, -120(%rdx)
	leaq	-136(%rbx), %rbx
	movzbl	(%rbx), %r8d
	leaq	24(%rbx), %rax
	jne	.L3546
	leaq	64(%rbx), %rcx
	cmpb	$3, %r8b
	cmovne	%rcx, %rax
	movl	(%rax), %r12d
	testl	%r12d, %r12d
	je	.L3554
	movq	-112(%rbp), %rcx
	movq	%rcx, %r9
	subq	-120(%rbp), %r9
	sarq	$3, %r9
	imull	$-1431655765, %r9d, %r9d
	subl	-132(%rdx), %r9d
	cmpl	%r12d, %r9d
	jb	.L5468
	movl	%r12d, %edx
	leaq	(%rdx,%rdx,2), %r9
	salq	$3, %r9
	cmpl	$1, %r12d
	je	.L3550
	subq	%r9, %rcx
	movq	8(%rax), %r13
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L3555:
	movzbl	8(%r13,%rax), %edx
	movzbl	8(%r15,%rax), %ecx
	cmpb	%cl, %dl
	je	.L3551
	leal	-7(%rcx), %r9d
	cmpb	$2, %r9b
	ja	.L4616
	cmpb	$6, %dl
	jne	.L4616
.L3551:
	addl	$1, %r10d
	addq	$24, %rax
	cmpl	%r12d, %r10d
	jne	.L3555
	.p2align 4,,10
	.p2align 3
.L3554:
	subq	$1, %rsi
	cmpq	%rsi, %rdi
	je	.L5469
	leaq	24(%rbx), %r12
	cmpb	$3, %r8b
	leaq	64(%rbx), %r13
	movq	104(%rbx), %rdx
	movq	%r12, %rcx
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	cmovne	%r13, %rcx
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0
	cmpb	$3, (%rbx)
	cmove	%r12, %r13
	movb	$1, 32(%r13)
.L3600:
	movl	-1324(%rbp), %eax
	movq	%r14, %rdi
	leal	1(%rax), %esi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
.L3545:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3110:
	leaq	1(%r13), %r12
	leaq	-1328(%rbp), %rbx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	leaq	.LC70(%rip), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movl	$0, -1328(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-112(%rbp), %rdx
	movq	%r12, -1320(%rbp)
	movabsq	$-6148914691236517205, %rsi
	movl	%eax, %edi
	movl	%eax, -1440(%rbp)
	movl	-1328(%rbp), %eax
	movl	$0, -1304(%rbp)
	leaq	1(%r13,%rax), %rcx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	movq	%r14, -1328(%rbp)
	movq	%rcx, -1312(%rbp)
	sarq	$3, %rax
	movq	%rcx, -1488(%rbp)
	movq	-80(%rbp), %rcx
	imulq	%rsi, %rax
	movl	%edi, -1300(%rbp)
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	jbe	.L5470
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %ecx
	subq	$24, %rdx
	movq	(%rdx), %r12
	movq	-304(%rbp), %r9
	movq	%rdx, -112(%rbp)
	movq	%rax, -1496(%rbp)
	cmpb	$1, %cl
	jne	.L5471
.L3622:
	movq	-264(%rbp), %r15
	movq	%r9, %rsi
	movl	$1, %r13d
	testq	%r15, %r15
	je	.L5472
.L3625:
	movq	-296(%rbp), %rax
	addq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3123:
	addq	$1, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5359:
	movq	-192(%rbp), %rdi
	movl	%ebx, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5473
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2932:
	movq	%rax, -64(%rcx)
	movq	-208(%rbp), %rdx
	leal	-2(%rbx), %r9d
	movq	-304(%rbp), %rsi
	addq	$2, %r9
	movq	16(%rdx), %rdx
	movzbl	(%rdx), %edx
	movq	%rsi, (%rax)
	movl	$24, %esi
	movq	$0, 16(%rax)
	movb	%dl, 8(%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2933:
	movq	-208(%rbp), %rdi
	movq	-64(%rcx), %rdx
	movq	-304(%rbp), %r8
	movq	16(%rdi), %rdi
	addq	%rsi, %rdx
	addq	$24, %rsi
	movzbl	(%rdi,%rax), %edi
	addq	$1, %rax
	movq	%r8, (%rdx)
	movq	$0, 16(%rdx)
	movb	%dil, 8(%rdx)
	cmpq	%rax, %r9
	jne	.L2933
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L2919:
	movabsq	$-1085102592571150095, %rdx
	movq	%rbx, %r8
	subq	%r15, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$15790320, %rax
	je	.L3145
	testq	%rax, %rax
	je	.L4356
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L5474
	movl	$2147483520, %esi
	movl	$2147483520, %edx
.L2922:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L5475
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2925:
	addq	%rax, %rdx
	leaq	136(%rax), %rcx
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L5474:
	testq	%rdx, %rdx
	jne	.L5476
	movl	$136, %ecx
	xorl	%edx, %edx
	xorl	%eax, %eax
.L2923:
	leaq	(%rax,%r8), %rsi
	movq	-304(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movb	%r13b, 16(%rsi)
	xorl	$1, %r13d
	movups	%xmm0, 32(%rsi)
	movups	%xmm0, 72(%rsi)
	pxor	%xmm0, %xmm0
	movb	%r13b, 56(%rsi)
	movb	$2, (%rsi)
	andb	$1, 56(%rsi)
	movl	%r12d, 4(%rsi)
	movq	%rdi, 8(%rsi)
	movl	$0, 24(%rsi)
	movq	$0, 48(%rsi)
	movl	$0, 64(%rsi)
	movq	$0, 88(%rsi)
	movb	$0, 96(%rsi)
	movq	$0, 120(%rsi)
	movl	$-1, 128(%rsi)
	movups	%xmm0, 104(%rsi)
	cmpq	%r15, %rbx
	je	.L2926
	movq	%r15, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L2927:
	movdqu	(%rcx), %xmm2
	addq	$136, %rcx
	addq	$136, %rsi
	movups	%xmm2, -136(%rsi)
	movdqu	-120(%rcx), %xmm3
	movups	%xmm3, -120(%rsi)
	movdqu	-104(%rcx), %xmm4
	movups	%xmm4, -104(%rsi)
	movdqu	-88(%rcx), %xmm5
	movups	%xmm5, -88(%rsi)
	movdqu	-72(%rcx), %xmm6
	movups	%xmm6, -72(%rsi)
	movdqu	-56(%rcx), %xmm7
	movups	%xmm7, -56(%rsi)
	movdqu	-40(%rcx), %xmm2
	movups	%xmm2, -40(%rsi)
	movdqu	-24(%rcx), %xmm3
	movups	%xmm3, -24(%rsi)
	movq	-8(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	cmpq	%rcx, %rbx
	jne	.L2927
	leaq	-136(%rbx), %rcx
	movabsq	$1220740416642543857, %rbx
	movabsq	$2305843009213693951, %rsi
	subq	%r15, %rcx
	shrq	$3, %rcx
	imulq	%rbx, %rcx
	andq	%rsi, %rcx
	addq	$2, %rcx
	movq	%rcx, %rsi
	salq	$4, %rsi
	addq	%rsi, %rcx
	leaq	(%rax,%rcx,8), %rcx
.L2926:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdx, -72(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L5368:
	leaq	-984(%rbp), %rdi
	leaq	-1328(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -984(%rbp)
	movq	%rax, %rdi
	movq	-1328(%rbp), %rax
	movq	%rax, -968(%rbp)
	jmp	.L4310
	.p2align 4,,10
	.p2align 3
.L5369:
	movdqu	-968(%rbp), %xmm5
	movq	-976(%rbp), %r12
	movaps	%xmm5, -512(%rbp)
.L4313:
	movq	-1432(%rbp), %rsi
	movdqa	-512(%rbp), %xmm6
	movq	-272(%rbp), %r13
	leaq	32(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, 16(%rsi)
	movq	%rsi, %rax
	movl	%ecx, 8(%rsi)
	movaps	%xmm6, -736(%rbp)
	jmp	.L4315
	.p2align 4,,10
	.p2align 3
.L5364:
	leaq	.LC136(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L4354:
	xorl	%ebx, %ebx
	jmp	.L2901
	.p2align 4,,10
	.p2align 3
.L5355:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2897
	.p2align 4,,10
	.p2align 3
.L4134:
	leaq	.LC128(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movl	$65024, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	movq	%rax, -1488(%rbp)
	testq	%rax, %rax
	jne	.L4138
.L4139:
	leaq	.LC127(%rip), %rsi
	movq	%r14, %rdi
	movl	$2, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	.p2align 4,,10
	.p2align 3
.L4136:
	addq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	movq	%rbx, %r13
	jmp	.L3125
.L4140:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4586
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65102, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4142:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4585
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65101, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4143:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4584
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65100, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4173:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4554
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65070, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4157:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4570
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65086, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4189:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4538
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65054, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4149:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4578
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65094, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4181:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4546
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65062, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4165:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4562
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65078, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4199:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4534
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65043, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4145:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4582
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65098, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4177:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4550
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65066, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4161:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4566
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65082, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4190:
	movl	$2, %ecx
	xorl	%r13d, %r13d
	jmp	.L4195
.L4153:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4574
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65090, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4154:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4573
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65089, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4185:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4542
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65058, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4169:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4558
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65074, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4203:
	movq	-304(%rbp), %rax
	movq	-296(%rbp), %rdx
	leaq	2(%rax), %rsi
	cmpq	%rdx, %rsi
	ja	.L4206
	cmpl	%esi, %edx
	je	.L4206
	cmpb	$0, 2(%rax)
	jne	.L5477
.L4208:
	cmpq	$0, -264(%rbp)
	movl	$3, %ebx
	jne	.L4136
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L4136
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder11AtomicFenceEv@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4209
	cmpl	$-1, -168(%rbp)
	je	.L4209
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L4209:
	movl	$3, %ebx
	jmp	.L4136
.L4144:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4583
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65099, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4147:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4580
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65096, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4175:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4552
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65068, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4159:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4568
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65084, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4192:
	xorl	%ecx, %ecx
	movl	$65051, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4151:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4576
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65092, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4152:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4575
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65091, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4183:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4544
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65060, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4167:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4560
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65076, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4201:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4532
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65041, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4146:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4581
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65097, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4179:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4548
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65064, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4163:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4564
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65080, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4197:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4536
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65045, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4155:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4572
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65088, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4187:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4539
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65056, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4171:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4556
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65072, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4205:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4529
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65025, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4148:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4579
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65095, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4174:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4553
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65069, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4158:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4569
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65085, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4191:
	movl	$1, %ecx
	movl	$65052, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4150:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4577
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65093, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4156:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4571
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65087, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4166:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4561
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65077, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4182:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4545
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65061, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4162:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4565
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65081, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4200:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4533
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65042, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4170:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4557
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65073, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4178:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4549
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65065, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4160:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4567
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65083, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4196:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4537
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65046, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4168:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4559
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65075, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4186:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4540
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65057, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4164:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4563
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65079, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4204:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4530
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65026, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4172:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4555
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65071, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4176:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4551
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65067, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4193:
	movl	$1, %ecx
	movl	$65050, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4184:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4543
	movq	16(%rax), %rax
	movl	$1, %ecx
	movl	$65059, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4188:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4541
	movq	16(%rax), %rax
	movl	$3, %ecx
	movl	$65055, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4202:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4531
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65040, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4180:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4547
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65063, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4198:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4535
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movl	$65044, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
.L4138:
	movq	-1488(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L4528
	movq	16(%rax), %rax
	movl	$2, %ecx
	movl	$65024, %r12d
	movzbl	(%rax), %r13d
	jmp	.L4195
	.p2align 4,,10
	.p2align 3
.L3126:
	movq	-1408(%rbp), %rax
	movq	%rax, %r12
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L5460:
	leaq	.LC83(%rip), %rsi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
.L3192:
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	addq	-304(%rbp), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5450:
	leaq	.LC83(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movl	$1, %esi
.L3276:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5434:
	leaq	.LC83(%rip), %rsi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
.L3217:
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	addq	-304(%rbp), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5464:
	movq	-304(%rbp), %rsi
	cmpb	$2, -120(%rcx)
	movq	%rsi, %r9
	jne	.L5478
.L3606:
	xorl	%r12d, %r12d
	jmp	.L3608
	.p2align 4,,10
	.p2align 3
.L5470:
	movq	-304(%rbp), %rsi
	cmpb	$2, -120(%rcx)
	movq	%rsi, %r9
	jne	.L5479
.L3620:
	movq	$0, -1496(%rbp)
	jmp	.L3622
	.p2align 4,,10
	.p2align 3
.L3314:
	movq	-1344(%rbp), %rax
	movq	%rax, %r12
	jmp	.L3316
	.p2align 4,,10
	.p2align 3
.L5445:
	cmpb	$0, -120(%rax)
	je	.L5480
	movl	-132(%rax), %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	-120(%rbp), %rdx
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, -112(%rbp)
	jne	.L4341
	jmp	.L4340
	.p2align 4,,10
	.p2align 3
.L5396:
	leaq	.LC96(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
	movl	$1, %esi
.L3489:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5374:
	leaq	.LC96(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5409:
	leaq	.LC115(%rip), %rsi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
.L3947:
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	addq	-304(%rbp), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5406:
	leaq	.LC115(%rip), %rsi
	movq	%r14, %rdi
	movl	$1, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
.L3963:
	addq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	movq	%rbx, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3778:
	subq	$24, %rcx
	addq	$1, %r13
	movq	%rcx, -112(%rbp)
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5384:
	leaq	.LC96(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
	movl	$1, %esi
	jmp	.L3745
	.p2align 4,,10
	.p2align 3
.L5426:
	leaq	.LC96(%rip), %rsi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
	movq	-304(%rbp), %r8
.L3811:
	movq	-296(%rbp), %rax
	addq	%r8, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5429:
	leaq	.LC96(%rip), %rsi
	movq	%r14, %rdi
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
	movq	-304(%rbp), %rsi
	jmp	.L3796
	.p2align 4,,10
	.p2align 3
.L5402:
	cmpb	$2, -120(%r8)
	jne	.L5481
.L3462:
	movq	$0, -1440(%rbp)
.L3463:
	movq	%r8, %rdx
	movq	%r9, %rsi
	jmp	.L3464
	.p2align 4,,10
	.p2align 3
.L5371:
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5438:
	cmpb	$1, %al
	je	.L3425
	movq	%r13, %rsi
	leaq	.LC89(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5424:
	cmpb	$2, -120(%rsi)
	jne	.L5482
.L3771:
	xorl	%ebx, %ebx
	movl	$10, %r13d
	jmp	.L3773
	.p2align 4,,10
	.p2align 3
.L5425:
	cmpb	$2, -120(%rsi)
	jne	.L5483
	xorl	%ebx, %ebx
	jmp	.L3762
	.p2align 4,,10
	.p2align 3
.L5459:
	cmpb	$2, -120(%rcx)
	jne	.L5484
.L3184:
	xorl	%r12d, %r12d
.L3186:
	cmpq	$0, -264(%rbp)
	jne	.L3189
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5485
.L3189:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
.L3182:
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5439:
	cmpb	$2, -120(%rcx)
	movq	-304(%rbp), %rsi
	jne	.L5486
.L3372:
	movq	$0, -1560(%rbp)
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L3990:
	leaq	.LC118(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movl	$64512, %esi
.L3991:
	cmpb	$0, -222(%rbp)
	je	.L5487
	movq	-216(%rbp), %rax
	movb	$1, 10(%rax)
	jmp	.L3994
	.p2align 4,,10
	.p2align 3
.L5391:
	cmpb	$2, -120(%rcx)
	movq	-304(%rbp), %rsi
	jne	.L5488
.L3837:
	xorl	%r12d, %r12d
	jmp	.L3839
	.p2align 4,,10
	.p2align 3
.L5389:
	leaq	.LC56(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-240(%rbp), %rdx
	jmp	.L3834
	.p2align 4,,10
	.p2align 3
.L5387:
	leaq	.LC56(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3848
	.p2align 4,,10
	.p2align 3
.L5417:
	leaq	-520(%rbp), %rbx
	leaq	-328(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%rbx, -1440(%rbp)
	movq	%rax, -528(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -544(%rbp)
.L3863:
	movq	-1488(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L4491
	xorl	%r13d, %r13d
	movq	-112(%rbp), %r12
	leaq	-304(%rbp), %rsi
	subq	-120(%rbp), %r12
	movq	%r12, -1512(%rbp)
	leaq	-128(%rbp), %r15
	leaq	-1360(%rbp), %r12
	movq	%rbx, -1488(%rbp)
	movq	%r13, %rbx
	movq	%rsi, %r13
	movq	%r14, -1520(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L3866:
	movq	16(%r14), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movzbl	(%rdx,%rbx), %edx
	addq	$1, %rbx
	movb	%dl, -1360(%rbp)
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	%rbx, -1488(%rbp)
	jne	.L3866
	movq	-1520(%rbp), %r14
	movq	-1512(%rbp), %r13
	addq	-120(%rbp), %r13
.L3865:
	cmpq	$0, -264(%rbp)
	movq	-544(%rbp), %r12
	jne	.L3867
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5489
.L3867:
	cmpq	-1440(%rbp), %r12
	je	.L3853
	movq	%r12, %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L3853:
	movl	-1496(%rbp), %esi
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5361:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L3433:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv
	testb	%al, %al
	jne	.L3434
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3895:
	leaq	.LC113(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	.p2align 4,,10
	.p2align 3
.L3894:
	movl	-1496(%rbp), %esi
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	movq	%rsi, %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5421:
	leaq	.LC110(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-264(%rbp), %r15
	jmp	.L3786
	.p2align 4,,10
	.p2align 3
.L3267:
	movb	$5, -136(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE17TypeCheckFallThruEv
	testb	%al, %al
	je	.L3269
	cmpb	$0, -120(%rbx)
	jne	.L3269
	cmpb	$3, -136(%rbx)
	je	.L3270
	leaq	-184(%rbp), %r11
	movq	-32(%rbx), %rdx
	leaq	-72(%rbx), %rcx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0
.L3270:
	movb	$1, -40(%rbx)
.L3269:
	movl	-132(%rbx), %eax
	leaq	(%rax,%rax,2), %rdx
	movq	-120(%rbp), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rax, -112(%rbp)
	je	.L3271
	movq	%rax, -112(%rbp)
.L3271:
	movq	-80(%rbp), %rax
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	cmpb	$0, -256(%rax)
	setne	-120(%rbx)
	movb	$9, -1328(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rdx
	testq	%r15, %r15
	je	.L3272
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5394:
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC102(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5376:
	leaq	-1(%r15), %rsi
	leaq	.LC131(%rip), %rdx
	movq	%r14, %rdi
	movl	$2, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L4136
	.p2align 4,,10
	.p2align 3
.L4000:
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE@PLT
	movq	%rax, -1488(%rbp)
	testq	%rax, %rax
	je	.L5490
	movq	8(%rax), %r13
	leaq	-520(%rbp), %rdx
	leaq	-328(%rbp), %rax
	movq	%rdx, %xmm0
	movq	%rdx, -1440(%rbp)
	leaq	-544(%rbp), %rdi
	punpcklqdq	%xmm0, %xmm0
	movslq	%r13d, %rbx
	movq	%rax, -528(%rbp)
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %rbx
	ja	.L5491
.L4076:
	leaq	(%rbx,%rbx,2), %rax
	movl	%r13d, %r10d
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, -536(%rbp)
	subl	$1, %r10d
	js	.L4077
	movl	%r12d, -1512(%rbp)
	movslq	%r10d, %r13
	movl	%r10d, %r12d
	movq	%r14, -1496(%rbp)
	movq	-1488(%rbp), %r14
	jmp	.L4116
	.p2align 4,,10
	.p2align 3
.L5493:
	cmpb	$2, -120(%rdi)
	movq	-304(%rbp), %rsi
	jne	.L5492
.L4079:
	xorl	%r10d, %r10d
	movl	$10, %ebx
.L4083:
	subl	$1, %r12d
	movq	%rsi, (%r15)
	subq	$1, %r13
	movb	%bl, 8(%r15)
	movq	%r10, 16(%r15)
	cmpl	$-1, %r12d
	je	.L5339
	movq	-544(%rbp), %rdx
.L4116:
	movq	16(%r14), %rax
	movq	-80(%rbp), %rdi
	movabsq	$-6148914691236517205, %rbx
	addq	%r13, %rax
	addq	(%r14), %rax
	movl	-132(%rdi), %esi
	movzbl	(%rax), %ecx
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rdx,%rax,8), %r15
	movq	-112(%rbp), %rdx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	cmpq	%rsi, %rax
	jbe	.L5493
	movzbl	-16(%rdx), %ebx
	movq	-24(%rdx), %rsi
	subq	$24, %rdx
	movq	16(%rdx), %r10
	movq	%rdx, -112(%rbp)
	cmpb	%bl, %cl
	je	.L4083
	cmpb	$6, %cl
	sete	%al
	cmpb	$8, %bl
	sete	%dl
	testb	%al, %al
	je	.L4628
	testb	%dl, %dl
	je	.L4628
	movl	$8, %ebx
	jmp	.L4083
.L5483:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r15
	jnb	.L3761
	movq	%r15, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r15
	movq	%rax, %rcx
.L3761:
	leaq	.LC18(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	xorl	%ebx, %ebx
	jmp	.L3762
.L4008:
	movl	%r12d, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L4013:
	addq	-304(%rbp), %rsi
	movq	-296(%rbp), %rax
	movq	%rsi, %r13
	jmp	.L3125
.L4009:
	movl	%r12d, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
.L5428:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L3825
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3825:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3824
.L4007:
	movl	$64784, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
.L4010:
	pxor	%xmm0, %xmm0
	leaq	2(%rbx), %rsi
	movaps	%xmm0, -768(%rbp)
	cmpq	%rsi, %rax
	jb	.L4016
	cmpl	%esi, %eax
	je	.L4016
	movzbl	2(%rbx), %edx
.L4015:
	leaq	3(%rbx), %rsi
	movb	%dl, -768(%rbp)
	cmpq	%rax, %rsi
	ja	.L4019
	cmpl	%esi, %eax
	je	.L4019
	movzbl	3(%rbx), %edx
.L4018:
	leaq	4(%rbx), %rsi
	movb	%dl, -767(%rbp)
	cmpq	%rax, %rsi
	ja	.L4022
	cmpl	%esi, %eax
	je	.L4022
	movzbl	4(%rbx), %edx
.L4021:
	leaq	5(%rbx), %rsi
	movb	%dl, -766(%rbp)
	cmpq	%rax, %rsi
	ja	.L4025
	cmpl	%esi, %eax
	je	.L4025
	movzbl	5(%rbx), %edx
.L4024:
	leaq	6(%rbx), %rsi
	movb	%dl, -765(%rbp)
	cmpq	%rax, %rsi
	ja	.L4028
	cmpl	%esi, %eax
	je	.L4028
	movzbl	6(%rbx), %edx
.L4027:
	leaq	7(%rbx), %rsi
	movb	%dl, -764(%rbp)
	cmpq	%rax, %rsi
	ja	.L4031
	cmpl	%esi, %eax
	je	.L4031
	movzbl	7(%rbx), %edx
.L4030:
	leaq	8(%rbx), %rsi
	movb	%dl, -763(%rbp)
	cmpq	%rax, %rsi
	ja	.L4034
	cmpl	%esi, %eax
	je	.L4034
	movzbl	8(%rbx), %edx
.L4033:
	leaq	9(%rbx), %rsi
	movb	%dl, -762(%rbp)
	cmpq	%rax, %rsi
	ja	.L4037
	cmpl	%esi, %eax
	je	.L4037
	movzbl	9(%rbx), %edx
.L4036:
	leaq	10(%rbx), %rsi
	movb	%dl, -761(%rbp)
	cmpq	%rax, %rsi
	ja	.L4040
	cmpl	%esi, %eax
	je	.L4040
	movzbl	10(%rbx), %edx
.L4039:
	leaq	11(%rbx), %rsi
	movb	%dl, -760(%rbp)
	cmpq	%rax, %rsi
	ja	.L4043
	cmpl	%esi, %eax
	je	.L4043
	movzbl	11(%rbx), %edx
.L4042:
	leaq	12(%rbx), %rsi
	movb	%dl, -759(%rbp)
	cmpq	%rax, %rsi
	ja	.L4046
	cmpl	%esi, %eax
	je	.L4046
	movzbl	12(%rbx), %edx
.L4045:
	leaq	13(%rbx), %rsi
	movb	%dl, -758(%rbp)
	cmpq	%rax, %rsi
	ja	.L4049
	cmpl	%esi, %eax
	je	.L4049
	movzbl	13(%rbx), %edx
.L4048:
	leaq	14(%rbx), %rsi
	movb	%dl, -757(%rbp)
	cmpq	%rax, %rsi
	ja	.L4052
	cmpl	%esi, %eax
	je	.L4052
	movzbl	14(%rbx), %edx
.L4051:
	leaq	15(%rbx), %rsi
	movb	%dl, -756(%rbp)
	cmpq	%rax, %rsi
	ja	.L4055
	cmpl	%esi, %eax
	je	.L4055
	movzbl	15(%rbx), %edx
.L4054:
	leaq	16(%rbx), %rsi
	movb	%dl, -755(%rbp)
	cmpq	%rax, %rsi
	ja	.L4058
	cmpl	%esi, %eax
	je	.L4058
	movzbl	16(%rbx), %edx
.L4057:
	leaq	17(%rbx), %rsi
	movb	%dl, -754(%rbp)
	cmpq	%rax, %rsi
	ja	.L4061
	cmpl	%esi, %eax
	je	.L4061
	movzbl	17(%rbx), %ecx
.L4060:
	movzbl	-767(%rbp), %esi
	movzbl	-768(%rbp), %eax
	movb	%cl, -753(%rbp)
	cmpb	%sil, -768(%rbp)
	cmovb	%esi, %eax
	movzbl	-766(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-765(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-764(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-763(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-762(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-761(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-760(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-759(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-758(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-757(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-756(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	movzbl	-755(%rbp), %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
	cmpb	%cl, %al
	cmovb	%ecx, %eax
	cmpb	%dl, %al
	cmovb	%edx, %eax
	cmpb	$32, %al
	ja	.L5494
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %r8
	movabsq	$-6148914691236517205, %rcx
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movl	-132(%r8), %ecx
	cmpq	%rcx, %rax
	ja	.L4064
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	cmpb	$2, -120(%r8)
	jne	.L5495
.L4065:
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movb	$5, -1328(%rbp)
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %r13
	testq	%r15, %r15
	jne	.L4063
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5496
.L4063:
	movl	$18, %esi
	jmp	.L4013
.L4011:
	movl	$9, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE14DecodeStoreMemENS1_9StoreTypeEi
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
	.p2align 4,,10
	.p2align 3
.L3999:
	leaq	.LC122(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
.L4012:
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE13DecodeLoadMemENS1_8LoadTypeEi
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
.L4005:
	movl	$64787, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
.L4006:
	movl	$64785, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
.L4001:
	movl	$64791, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
.L4003:
	movl	$64790, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdExtractLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
.L4004:
	movl	$64788, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15SimdReplaceLaneENS1_10WasmOpcodeENS1_9ValueTypeE
	movq	-264(%rbp), %r15
	leal	2(%rax), %esi
	jmp	.L4013
	.p2align 4,,10
	.p2align 3
.L4606:
	leal	-7(%r13), %esi
	andl	$253, %esi
	jne	.L4607
	testb	%al, %al
	jne	.L3136
.L4607:
	leal	-7(%rdx), %eax
	movzbl	%r13b, %edi
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L3136
	cmpb	$10, %r13b
	setne	%cl
	cmpb	$10, %dl
	setne	%al
	testb	%al, %cl
	je	.L3136
	movq	%r10, -1544(%rbp)
	movq	%r11, -1536(%rbp)
	movb	%dl, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rcx
	movzbl	-1528(%rbp), %edx
	movq	%rax, -1520(%rbp)
	leaq	.LC0(%rip), %rax
	movq	-1536(%rbp), %r11
	cmpq	%rcx, %r14
	movq	%rax, -1488(%rbp)
	movq	-1544(%rbp), %r10
	jnb	.L3139
	movq	-1496(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rcx
	movq	-1544(%rbp), %r10
	movq	%rax, -1488(%rbp)
	movq	-1536(%rbp), %r11
	movzbl	-1528(%rbp), %edx
.L3139:
	movzbl	%dl, %edi
	movq	%r10, -1544(%rbp)
	movq	%rcx, -1536(%rbp)
	movq	%r11, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1536(%rbp), %rcx
	movq	%rax, %r9
	movq	-1528(%rbp), %r11
	movq	-1544(%rbp), %r10
	leaq	.LC0(%rip), %rax
	cmpq	%rcx, %rsi
	jnb	.L3140
	movq	-1496(%rbp), %rdi
	movq	%r9, -1536(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1544(%rbp), %r10
	movq	-1536(%rbp), %r9
	movq	-1528(%rbp), %r11
.L3140:
	pushq	-1520(%rbp)
	movl	%r15d, %r8d
	movq	%rax, %rcx
	movq	%r14, %rsi
	pushq	-1488(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	movq	-1440(%rbp), %rdi
	movq	%r10, -1536(%rbp)
	movq	%r11, -1528(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	movq	-1528(%rbp), %r11
	movq	-1536(%rbp), %r10
	jmp	.L3136
.L4613:
	leal	-7(%r13), %esi
	andl	$253, %esi
	jne	.L4614
	testb	%al, %al
	jne	.L3383
.L4614:
	leal	-7(%rdx), %eax
	movzbl	%r13b, %edi
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L3383
	cmpb	$10, %r13b
	setne	%cl
	cmpb	$10, %dl
	setne	%al
	testb	%al, %cl
	je	.L3383
	movl	%r8d, -1544(%rbp)
	movq	%r11, -1536(%rbp)
	movb	%dl, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rcx
	movzbl	-1528(%rbp), %edx
	movq	%rax, -1520(%rbp)
	leaq	.LC0(%rip), %rax
	movq	-1536(%rbp), %r11
	cmpq	%rcx, %r14
	movq	%rax, -1496(%rbp)
	movl	-1544(%rbp), %r8d
	jnb	.L3386
	movq	-1512(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rcx
	movl	-1544(%rbp), %r8d
	movq	%rax, -1496(%rbp)
	movq	-1536(%rbp), %r11
	movzbl	-1528(%rbp), %edx
.L3386:
	movzbl	%dl, %edi
	movl	%r8d, -1544(%rbp)
	movq	%rcx, -1536(%rbp)
	movq	%r11, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1536(%rbp), %rcx
	leaq	.LC0(%rip), %r10
	movq	-1528(%rbp), %r11
	movl	-1544(%rbp), %r8d
	movq	%rax, %r9
	cmpq	%rcx, %rsi
	jnb	.L3387
	movq	-1512(%rbp), %rdi
	movq	%rax, -1536(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-1544(%rbp), %r8d
	movq	-1536(%rbp), %r9
	movq	-1528(%rbp), %r11
	movq	%rax, %r10
.L3387:
	pushq	-1520(%rbp)
	xorl	%eax, %eax
	movq	%r10, %rcx
	movq	%r14, %rsi
	pushq	-1496(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1488(%rbp), %rdi
	movq	%r11, -1528(%rbp)
	movl	%r8d, -1496(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	movl	-1496(%rbp), %r8d
	movq	-1528(%rbp), %r11
	jmp	.L3383
.L4611:
	leal	-7(%r13), %esi
	andl	$253, %esi
	jne	.L4612
	testb	%al, %al
	jne	.L3324
.L4612:
	leal	-7(%rdx), %eax
	movzbl	%r13b, %edi
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L3324
	cmpb	$10, %dl
	setne	%cl
	cmpb	$10, %r13b
	setne	%al
	testb	%al, %cl
	je	.L3324
	movq	%r10, -1544(%rbp)
	movq	%r11, -1536(%rbp)
	movb	%dl, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rcx
	movzbl	-1528(%rbp), %edx
	movq	%rax, -1520(%rbp)
	leaq	.LC0(%rip), %rax
	movq	-1536(%rbp), %r11
	cmpq	%rcx, %r14
	movq	%rax, -1488(%rbp)
	movq	-1544(%rbp), %r10
	jnb	.L3327
	movq	-1496(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rcx
	movq	-1544(%rbp), %r10
	movq	%rax, -1488(%rbp)
	movq	-1536(%rbp), %r11
	movzbl	-1528(%rbp), %edx
.L3327:
	movzbl	%dl, %edi
	movq	%r10, -1544(%rbp)
	movq	%rcx, -1536(%rbp)
	movq	%r11, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1536(%rbp), %rcx
	movq	%rax, %r9
	movq	-1528(%rbp), %r11
	movq	-1544(%rbp), %r10
	leaq	.LC0(%rip), %rax
	cmpq	%rcx, %rsi
	jnb	.L3328
	movq	-1496(%rbp), %rdi
	movq	%r9, -1536(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1544(%rbp), %r10
	movq	-1536(%rbp), %r9
	movq	-1528(%rbp), %r11
.L3328:
	pushq	-1520(%rbp)
	movl	%r15d, %r8d
	movq	%rax, %rcx
	movq	%r14, %rsi
	pushq	-1488(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	movq	-1440(%rbp), %rdi
	movq	%r10, -1536(%rbp)
	movq	%r11, -1528(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	movq	-1528(%rbp), %r11
	movq	-1536(%rbp), %r10
	jmp	.L3324
.L4630:
	leal	-7(%rbx), %edi
	andl	$253, %edi
	jne	.L4631
	testb	%al, %al
	jne	.L4240
.L4631:
	leal	-7(%rcx), %eax
	testb	$-3, %al
	sete	%al
	testb	%dl, %al
	jne	.L4240
	cmpb	$10, %bl
	setne	%dl
	cmpb	$10, %cl
	setne	%al
	testb	%al, %dl
	je	.L4240
	cmpb	$9, %bl
	ja	.L4243
	leaq	.L4245(%rip), %rdi
	movzbl	%bl, %edx
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L4245:
	.long	.L4254-.L4245
	.long	.L4599-.L4245
	.long	.L4252-.L4245
	.long	.L4251-.L4245
	.long	.L4250-.L4245
	.long	.L4249-.L4245
	.long	.L4248-.L4245
	.long	.L4247-.L4245
	.long	.L4246-.L4245
	.long	.L4244-.L4245
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L4252:
	leaq	.LC17(%rip), %rdx
.L4253:
	movq	-296(%rbp), %rax
	leaq	.LC0(%rip), %r8
	cmpq	%rax, %rsi
	jnb	.L4255
	movzbl	(%rsi), %edi
	movq	%rdx, -1592(%rbp)
	movq	%r11, -1584(%rbp)
	movb	%cl, -1576(%rbp)
	movq	%rsi, -1560(%rbp)
	movl	%edi, -1544(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-1544(%rbp), %edi
	movq	-1560(%rbp), %rsi
	testb	%al, %al
	movzbl	-1576(%rbp), %ecx
	movq	-1584(%rbp), %r11
	movq	-1592(%rbp), %rdx
	je	.L5497
	movq	-296(%rbp), %rax
	leaq	1(%rsi), %r9
	leaq	.LC0(%rip), %r8
	cmpq	%r9, %rax
	jbe	.L4255
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	movq	%rdx, -1584(%rbp)
	movq	%r11, -1576(%rbp)
	orl	%eax, %edi
	movb	%cl, -1560(%rbp)
	movq	%rsi, -1544(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-1584(%rbp), %rdx
	movq	-1576(%rbp), %r11
	movq	%rax, %r8
	movzbl	-1560(%rbp), %ecx
	movq	-296(%rbp), %rax
	movq	-1544(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L4255:
	cmpb	$9, %cl
	ja	.L4257
	leaq	.L4259(%rip), %rdi
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L4259:
	.long	.L4268-.L4259
	.long	.L4602-.L4259
	.long	.L4266-.L4259
	.long	.L4265-.L4259
	.long	.L4264-.L4259
	.long	.L4263-.L4259
	.long	.L4262-.L4259
	.long	.L4261-.L4259
	.long	.L4260-.L4259
	.long	.L4258-.L4259
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L4599:
	leaq	.LC8(%rip), %rdx
	jmp	.L4253
.L4254:
	leaq	.LC16(%rip), %rdx
	jmp	.L4253
.L4244:
	leaq	.LC14(%rip), %rdx
	jmp	.L4253
.L4246:
	leaq	.LC13(%rip), %rdx
	jmp	.L4253
.L4247:
	leaq	.LC12(%rip), %rdx
	jmp	.L4253
.L4248:
	leaq	.LC11(%rip), %rdx
	jmp	.L4253
.L4249:
	leaq	.LC15(%rip), %rdx
	jmp	.L4253
.L4250:
	leaq	.LC10(%rip), %rdx
	jmp	.L4253
.L4251:
	leaq	.LC9(%rip), %rdx
	jmp	.L4253
.L4266:
	leaq	.LC17(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L4267:
	movq	-304(%rbp), %rcx
	cmpq	%rax, %rcx
	jnb	.L4272
	movzbl	(%rcx), %edi
	movq	%r9, -1608(%rbp)
	movq	%rdx, -1600(%rbp)
	movq	%r8, -1592(%rbp)
	movq	%r11, -1584(%rbp)
	movq	%rsi, -1576(%rbp)
	movq	%rcx, -1560(%rbp)
	movl	%edi, -1544(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-1544(%rbp), %edi
	movq	-1560(%rbp), %rcx
	testb	%al, %al
	movq	-1576(%rbp), %rsi
	movq	-1584(%rbp), %r11
	movq	-1592(%rbp), %r8
	movq	-1600(%rbp), %rdx
	movq	-1608(%rbp), %r9
	je	.L5498
	leaq	1(%rcx), %rax
	cmpq	%rax, -296(%rbp)
	jbe	.L4272
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -1592(%rbp)
	movq	%rdx, -1584(%rbp)
	orl	%eax, %edi
	movq	%r8, -1576(%rbp)
	movq	%r11, -1560(%rbp)
	movq	%rsi, -1544(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-1592(%rbp), %r9
	movq	-1584(%rbp), %rdx
	movq	-1576(%rbp), %r8
	movq	-1560(%rbp), %r11
	movq	%rax, %rcx
	movq	-1544(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L4270:
	pushq	%rdx
	movq	-1496(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rdx
	pushq	%r8
	movl	%r15d, %r8d
	movq	%r11, -1560(%rbp)
	movq	%rsi, -1544(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	movq	-1544(%rbp), %rsi
	movq	-1560(%rbp), %r11
	jmp	.L4240
.L4602:
	leaq	.LC8(%rip), %r9
	jmp	.L4267
.L4260:
	leaq	.LC13(%rip), %r9
	jmp	.L4267
.L4261:
	leaq	.LC12(%rip), %r9
	jmp	.L4267
.L4262:
	leaq	.LC11(%rip), %r9
	jmp	.L4267
.L4263:
	leaq	.LC15(%rip), %r9
	jmp	.L4267
.L4264:
	leaq	.LC10(%rip), %r9
	jmp	.L4267
.L4265:
	leaq	.LC9(%rip), %r9
	jmp	.L4267
.L4268:
	leaq	.LC16(%rip), %r9
	jmp	.L4267
.L4258:
	leaq	.LC14(%rip), %r9
	jmp	.L4267
	.p2align 4,,10
	.p2align 3
.L5446:
	leaq	.LC0(%rip), %rcx
	cmpq	%r14, -296(%rbp)
	jbe	.L3135
	movq	-1496(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r14
	movq	-1488(%rbp), %r10
	movq	%rax, %rcx
.L3135:
	movq	-1440(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r14
	movq	-1488(%rbp), %r10
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L5443:
	leaq	.LC0(%rip), %rcx
	cmpq	%r14, -296(%rbp)
	jbe	.L3323
	movq	-1496(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r14
	movq	-1488(%rbp), %r10
	movq	%rax, %rcx
.L3323:
	movq	-1440(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r14
	movq	-1488(%rbp), %r10
	jmp	.L3322
	.p2align 4,,10
	.p2align 3
.L5441:
	leaq	.LC0(%rip), %rcx
	cmpq	%r14, -296(%rbp)
	jbe	.L3382
	movq	-1512(%rbp), %rdi
	movq	%r14, %rsi
	movl	%r8d, -1496(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r14
	movl	-1496(%rbp), %r8d
	movq	%rax, %rcx
.L3382:
	movq	-1488(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	movl	%r8d, -1496(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r14
	movl	-1496(%rbp), %r8d
	jmp	.L3381
.L4608:
	leal	-7(%r13), %esi
	andl	$253, %esi
	jne	.L4609
	testb	%al, %al
	jne	.L3227
.L4609:
	leal	-7(%rdx), %eax
	movzbl	%r13b, %edi
	testb	$-3, %al
	sete	%al
	testb	%cl, %al
	jne	.L3227
	cmpb	$10, %r13b
	setne	%cl
	cmpb	$10, %dl
	setne	%al
	testb	%al, %cl
	je	.L3227
	movq	%r10, -1544(%rbp)
	movq	%r11, -1536(%rbp)
	movb	%dl, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rcx
	movzbl	-1528(%rbp), %edx
	movq	%rax, -1520(%rbp)
	leaq	.LC0(%rip), %rax
	movq	-1536(%rbp), %r11
	cmpq	%rcx, %r14
	movq	%rax, -1488(%rbp)
	movq	-1544(%rbp), %r10
	jnb	.L3230
	movq	-1496(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rcx
	movq	-1544(%rbp), %r10
	movq	%rax, -1488(%rbp)
	movq	-1536(%rbp), %r11
	movzbl	-1528(%rbp), %edx
.L3230:
	movzbl	%dl, %edi
	movq	%r10, -1544(%rbp)
	movq	%rcx, -1536(%rbp)
	movq	%r11, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1536(%rbp), %rcx
	movq	%rax, %r9
	movq	-1528(%rbp), %r11
	movq	-1544(%rbp), %r10
	leaq	.LC0(%rip), %rax
	cmpq	%rcx, %rsi
	jnb	.L3231
	movq	-1496(%rbp), %rdi
	movq	%r9, -1536(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1544(%rbp), %r10
	movq	-1536(%rbp), %r9
	movq	-1528(%rbp), %r11
.L3231:
	pushq	-1520(%rbp)
	movq	%rax, %rcx
	movl	%r15d, %r8d
	xorl	%eax, %eax
	pushq	-1488(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rsi
	movq	-1440(%rbp), %rdi
	movq	%r10, -1536(%rbp)
	movq	%r11, -1528(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r11
	movq	-1536(%rbp), %r10
	movq	-1528(%rbp), %r11
	popq	%rax
	jmp	.L3227
	.p2align 4,,10
	.p2align 3
.L5312:
	movq	-1440(%rbp), %r14
.L4321:
	movq	-80(%rbp), %r12
	movq	-88(%rbp), %r13
	xorl	%r15d, %r15d
	cmpq	%r13, %r12
	je	.L3142
	cmpb	$0, -120(%r12)
	setne	%r15b
.L3142:
	movabsq	$-6148914691236517205, %rdx
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movq	%rax, %r8
	cmpq	-72(%rbp), %r12
	je	.L3143
	movq	-304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movb	%r15b, 16(%r12)
	xorl	$1, %r15d
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 72(%r12)
	pxor	%xmm0, %xmm0
	movb	%r15b, 56(%r12)
	movb	$2, (%r12)
	andb	$1, 56(%r12)
	movl	%r8d, 4(%r12)
	movq	%rax, 8(%r12)
	movl	$0, 24(%r12)
	movq	$0, 48(%r12)
	movl	$0, 64(%r12)
	movq	$0, 88(%r12)
	movb	$0, 96(%r12)
	movq	$0, 120(%r12)
	movl	$-1, 128(%r12)
	movups	%xmm0, 104(%r12)
	movq	-80(%rbp), %rax
	leaq	136(%rax), %rbx
	movq	%rbx, -80(%rbp)
.L3144:
	movzbl	-1420(%rbp), %eax
	movq	-1216(%rbp), %r12
	testb	%al, %al
	je	.L5499
	movq	-304(%rbp), %r13
	cmpb	$10, %al
	je	.L3156
	movl	$1, -72(%rbx)
.L3157:
	movzbl	-1420(%rbp), %eax
	cmpb	$10, %al
	jne	.L3159
	movq	-1408(%rbp), %rax
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
.L3159:
	movq	%r13, -64(%rbx)
	movb	%al, -56(%rbx)
	movq	$0, -48(%rbx)
.L3155:
	cmpb	$10, -1420(%rbp)
	jne	.L5500
	movq	-1408(%rbp), %rax
	movq	8(%rax), %r13
	movl	%r13d, -112(%rbx)
	cmpl	$1, %r13d
	je	.L5501
	jbe	.L3167
	movq	-192(%rbp), %rdi
	movl	%r13d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5502
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3170:
	movq	%rax, -104(%rbx)
	movq	(%r12), %rsi
	movzbl	8(%r12), %ecx
	movq	16(%r12), %rdx
	movq	%rsi, (%rax)
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	leal	-2(%r13), %eax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %r8
	movl	$24, %eax
	.p2align 4,,10
	.p2align 3
.L3171:
	movq	-104(%rbx), %rdx
	movq	(%r12,%rax), %rdi
	movzbl	8(%r12,%rax), %esi
	movq	16(%r12,%rax), %rcx
	addq	%rax, %rdx
	addq	$24, %rax
	movq	%rdi, (%rdx)
	movb	%sil, 8(%rdx)
	movq	%rcx, 16(%rdx)
	cmpq	%rax, %r8
	jne	.L3171
.L3167:
	cmpq	$0, -264(%rbp)
	jne	.L3172
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5503
.L3172:
	movl	-132(%rbx), %eax
	leaq	(%rax,%rax,2), %rdx
	movq	-120(%rbp), %rax
	leaq	(%rax,%rdx,8), %rsi
	cmpq	%rsi, -112(%rbp)
	je	.L3173
	movq	%rsi, -112(%rbp)
.L3173:
	movl	-112(%rbx), %eax
	cmpl	$1, %eax
	je	.L5504
	testl	%eax, %eax
	je	.L3176
	xorl	%r12d, %r12d
	leaq	-128(%rbp), %r13
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L5505:
	movdqu	(%rdx), %xmm5
	addl	$1, %r12d
	movups	%xmm5, (%rsi)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rsi)
	addq	$24, -112(%rbp)
	cmpl	-112(%rbx), %r12d
	jnb	.L3176
.L3179:
	movq	-112(%rbp), %rsi
.L3180:
	movl	%r12d, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	-104(%rbx), %rax
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rsi, -104(%rbp)
	jne	.L5505
	movq	%r13, %rdi
	addl	$1, %r12d
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	cmpl	%r12d, -112(%rbx)
	ja	.L3179
	.p2align 4,,10
	.p2align 3
.L3176:
	movl	-1424(%rbp), %eax
	movq	-1216(%rbp), %rdi
	leal	1(%rax), %r13d
	cmpq	-1512(%rbp), %rdi
	je	.L3129
	call	free@PLT
.L3129:
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	addq	-304(%rbp), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5322:
	movq	-1488(%rbp), %r14
.L4327:
	movq	-264(%rbp), %r12
	movl	$1, %r13d
	testq	%r12, %r12
	je	.L5506
.L3389:
	movq	-544(%rbp), %rdi
	cmpq	-1440(%rbp), %rdi
	je	.L3370
	call	free@PLT
.L3370:
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	addq	-304(%rbp), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5319:
	movq	-1440(%rbp), %r14
.L4324:
	movq	-80(%rbp), %r12
	movq	-88(%rbp), %r13
	xorl	%r15d, %r15d
	cmpq	%r13, %r12
	je	.L3330
	cmpb	$0, -120(%r12)
	setne	%r15b
.L3330:
	movabsq	$-6148914691236517205, %rdx
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movq	%rax, %r8
	cmpq	-72(%rbp), %r12
	je	.L3331
	movq	-304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movb	%r15b, 16(%r12)
	xorl	$1, %r15d
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 72(%r12)
	pxor	%xmm0, %xmm0
	movb	%r15b, 56(%r12)
	movb	$3, (%r12)
	andb	$1, 56(%r12)
	movl	%r8d, 4(%r12)
	movq	%rax, 8(%r12)
	movl	$0, 24(%r12)
	movq	$0, 48(%r12)
	movl	$0, 64(%r12)
	movq	$0, 88(%r12)
	movb	$0, 96(%r12)
	movq	$0, 120(%r12)
	movl	$-1, 128(%r12)
	movups	%xmm0, 104(%r12)
	movq	-80(%rbp), %rdx
	leaq	136(%rdx), %rbx
	movq	%rbx, -80(%rbp)
.L3332:
	movzbl	-1356(%rbp), %eax
	movq	-768(%rbp), %r12
	testb	%al, %al
	je	.L5507
	movq	-304(%rbp), %r13
	cmpb	$10, %al
	je	.L3343
	movl	$1, -72(%rbx)
.L3344:
	movzbl	-1356(%rbp), %eax
	cmpb	$10, %al
	jne	.L3346
	movq	-1344(%rbp), %rax
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
.L3346:
	movq	%r13, -64(%rbx)
	movb	%al, -56(%rbx)
	movq	$0, -48(%rbx)
.L3342:
	cmpb	$10, -1356(%rbp)
	jne	.L5508
	movq	-1344(%rbp), %rax
	movq	8(%rax), %r13
	movl	%r13d, -112(%rbx)
	cmpl	$1, %r13d
	je	.L5509
	jbe	.L3354
	movq	-192(%rbp), %rdi
	movl	%r13d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L5510
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3357:
	movq	%rax, -104(%rbx)
	movq	(%r12), %rdi
	movzbl	8(%r12), %esi
	movq	16(%r12), %rcx
	movq	%rdi, (%rax)
	movb	%sil, 8(%rax)
	movq	%rcx, 16(%rax)
	leal	-2(%r13), %eax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %r9
	movl	$24, %eax
	.p2align 4,,10
	.p2align 3
.L3358:
	movq	-104(%rbx), %rcx
	movq	(%r12,%rax), %r8
	movzbl	8(%r12,%rax), %edi
	movq	16(%r12,%rax), %rsi
	addq	%rax, %rcx
	addq	$24, %rax
	movq	%r8, (%rcx)
	movb	%dil, 8(%rcx)
	movq	%rsi, 16(%rcx)
	cmpq	%r9, %rax
	jne	.L3358
.L3354:
	movl	-1360(%rbp), %eax
	cmpq	$0, -264(%rbp)
	leal	1(%rax), %r13d
	jne	.L3359
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5511
.L3359:
	movl	-132(%rbx), %eax
	leaq	(%rax,%rax,2), %rdx
	movq	-120(%rbp), %rax
	leaq	(%rax,%rdx,8), %rsi
	cmpq	%rsi, -112(%rbp)
	je	.L3360
	movq	%rsi, -112(%rbp)
.L3360:
	movl	-112(%rbx), %eax
	cmpl	$1, %eax
	je	.L5512
	testl	%eax, %eax
	je	.L3363
	xorl	%r12d, %r12d
	leaq	-128(%rbp), %r15
	jmp	.L3367
	.p2align 4,,10
	.p2align 3
.L5513:
	movdqu	(%rdx), %xmm6
	addl	$1, %r12d
	movups	%xmm6, (%rsi)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rsi)
	addq	$24, -112(%rbp)
	cmpl	-112(%rbx), %r12d
	jnb	.L3363
.L3366:
	movq	-112(%rbp), %rsi
.L3367:
	movl	%r12d, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	-104(%rbx), %rax
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rsi, -104(%rbp)
	jne	.L5513
	movq	%r15, %rdi
	addl	$1, %r12d
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	cmpl	%r12d, -112(%rbx)
	ja	.L3366
	.p2align 4,,10
	.p2align 3
.L3363:
	movq	-768(%rbp), %rdi
	cmpq	-1512(%rbp), %rdi
	je	.L3317
	call	free@PLT
.L3317:
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	addq	-304(%rbp), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5380:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L4237
	movzbl	(%rsi), %ebx
	movq	%rsi, -1544(%rbp)
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-1544(%rbp), %rsi
	testb	%al, %al
	je	.L5514
	leaq	1(%rsi), %rax
	cmpq	%rax, -296(%rbp)
	ja	.L4239
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L4237:
	movq	-1496(%rbp), %rdi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %rsi
	jmp	.L4236
	.p2align 4,,10
	.p2align 3
.L3473:
	movq	-24(%rcx), %rsi
	movzbl	-16(%rcx), %r12d
	subq	$24, %rcx
	movq	16(%rcx), %rax
	movq	%rcx, -112(%rbp)
	movq	%rsi, -1512(%rbp)
	movl	%r12d, %r15d
	movq	%rax, -1496(%rbp)
	cmpb	%r13b, %r12b
	je	.L4420
	movl	%ebx, %esi
	movl	%r12d, %edi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r12b
	setne	%cl
	cmpb	$10, %r13b
	setne	%dl
	testb	%dl, %cl
	je	.L3478
	cmpb	$1, %al
	je	.L3478
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1512(%rbp), %rsi
	leaq	.LC0(%rip), %r13
	movq	%rax, -1520(%rbp)
	cmpq	%rdx, %rsi
	jnb	.L3479
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movq	%rax, %r13
.L3479:
	movl	%ebx, %edi
	movq	%rdx, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1528(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %rbx
	cmpq	%rdx, %rsi
	jnb	.L3480
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3480:
	pushq	-1520(%rbp)
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	pushq	%r13
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	movq	-1512(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r10
	popq	%r11
	movl	%r12d, -1512(%rbp)
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3469:
	movq	-8(%rcx), %rax
	movzbl	-16(%rcx), %ebx
	subq	$24, %rcx
	movq	%rcx, -112(%rbp)
	movl	-132(%rdx), %r9d
	movq	%rax, -1488(%rbp)
	movq	%rcx, %rax
	movl	%ebx, %r13d
	subq	%rsi, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	jmp	.L3472
	.p2align 4,,10
	.p2align 3
.L5342:
	movl	-1528(%rbp), %r12d
	movzbl	-1536(%rbp), %r13d
	movq	-1496(%rbp), %r14
.L4234:
	xorl	%ebx, %ebx
	testb	%r13b, %r13b
	je	.L4274
	movq	-1488(%rbp), %rdi
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L4275
	movq	16(%rdi), %rax
	movzbl	(%rax), %eax
.L4275:
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movb	%al, -1328(%rbp)
	leaq	-304(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-112(%rbp), %rax
	leaq	-24(%rax), %rbx
.L4274:
	cmpq	$0, -264(%rbp)
	movq	-544(%rbp), %r13
	jne	.L4276
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5515
.L4276:
	movl	-1512(%rbp), %eax
	leal	2(%rax), %ebx
	cmpq	-1440(%rbp), %r13
	je	.L4136
	movq	%r13, %rdi
	call	free@PLT
	jmp	.L4136
	.p2align 4,,10
	.p2align 3
.L3850:
	movq	-304(%rbp), %rax
	leaq	.LC106(%rip), %rdx
	movq	%r14, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3853
	.p2align 4,,10
	.p2align 3
.L5435:
	leaq	.LC0(%rip), %rcx
	cmpq	%r14, -296(%rbp)
	jbe	.L3226
	movq	-1496(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r14
	movq	-1488(%rbp), %r10
	movq	%rax, %rcx
.L3226:
	movq	-1440(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r14
	movq	-1488(%rbp), %r10
	jmp	.L3225
	.p2align 4,,10
	.p2align 3
.L4625:
	leal	-7(%rbx), %edx
	andl	$253, %edx
	jne	.L4626
	cmpb	$8, %dil
	je	.L3702
.L4626:
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movzbl	%bl, %edi
	movq	%rax, %r15
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%r15, %r9
	movl	%r12d, %ecx
	movq	%r13, %rsi
	movq	%rax, %r8
	leaq	.LC103(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L4356:
	movl	$136, %esi
	movl	$136, %edx
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L3784:
	leaq	1(%rbx), %rsi
	leaq	.LC109(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-264(%rbp), %r15
	jmp	.L3786
	.p2align 4,,10
	.p2align 3
.L3780:
	movq	-304(%rbp), %rax
	leaq	.LC109(%rip), %rdx
	movq	%r14, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-264(%rbp), %r15
	jmp	.L3782
	.p2align 4,,10
	.p2align 3
.L3892:
	leaq	.LC112(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L3894
	.p2align 4,,10
	.p2align 3
.L4420:
	movl	%r13d, %r15d
.L3478:
	movl	%r12d, -1512(%rbp)
	cmpb	$10, %r15b
	jne	.L3481
	jmp	.L3477
	.p2align 4,,10
	.p2align 3
.L5414:
	leaq	.LC0(%rip), %rcx
	cmpq	%r10, -296(%rbp)
	jbe	.L3909
	movq	-1536(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r10
	movq	%rax, %rcx
.L3909:
	movq	-1520(%rbp), %rdi
	movq	%r10, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r10
	jmp	.L3908
	.p2align 4,,10
	.p2align 3
.L5418:
	leaq	.LC0(%rip), %rcx
	cmpq	%r10, -296(%rbp)
	jbe	.L3858
	movq	-1536(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r10
	movq	%rax, %rcx
.L3858:
	movq	-1520(%rbp), %rdi
	movq	%r10, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r10
	jmp	.L3857
	.p2align 4,,10
	.p2align 3
.L5458:
	leaq	.LC83(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	jmp	.L3182
	.p2align 4,,10
	.p2align 3
.L5472:
	cmpl	$7654320, -1440(%rbp)
	ja	.L5516
	movq	-296(%rbp), %rax
	subq	%r9, %rax
	cmpl	%eax, -1440(%rbp)
	ja	.L5517
	movabsq	$-1085102592571150095, %rdi
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	$0, -1512(%rbp)
	sarq	$3, %rax
	imulq	%rdi, %rax
	testq	%rax, %rax
	jne	.L5518
.L4338:
	movq	$0, -1528(%rbp)
	movq	$0, -1520(%rbp)
.L3632:
	movq	-1328(%rbp), %rax
	cmpq	$0, 56(%rax)
	jne	.L3630
	movl	-1304(%rbp), %r13d
	cmpl	-1300(%rbp), %r13d
	ja	.L3630
	movq	-1312(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -1536(%rbp)
	call	_ZN2v88internal4wasm19BranchTableIteratorILNS1_7Decoder12ValidateFlagE1EE4nextEv
	movq	-80(%rbp), %rsi
	movabsq	$-1085102592571150095, %rdi
	movl	%eax, %edx
	movq	%rsi, %rax
	subq	-88(%rbp), %rax
	movq	%rdx, %rcx
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rdx
	jnb	.L5519
	movq	-1512(%rbp), %rdi
	movq	%rdx, %rax
	shrq	$6, %rax
	leaq	(%rdi,%rax,8), %rdi
	movl	$1, %eax
	salq	%cl, %rax
	movq	(%rdi), %rcx
	movq	%rax, %r12
	andq	%rcx, %r12
	jne	.L3632
	imulq	$-136, %rdx, %rdx
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	leaq	-136(%rsi,%rdx), %rax
	leaq	64(%rax), %rcx
	testl	%r13d, %r13d
	jne	.L3633
	cmpb	$3, (%rax)
	jne	.L3635
	leaq	24(%rax), %rcx
.L3635:
	movslq	(%rcx), %rdx
	movq	%rcx, -1544(%rbp)
	testl	%edx, %edx
	movl	%edx, -1536(%rbp)
	js	.L3636
	testq	%rdx, %rdx
	je	.L4448
	movq	%rdx, %rdi
	movq	%rdx, -1528(%rbp)
	call	_Znwm@PLT
	movq	-1528(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	movq	-1528(%rbp), %rdx
	movq	-1544(%rbp), %rcx
	movl	-1536(%rbp), %r8d
	leaq	0(%r13,%rdx), %rax
	movq	%rax, -1528(%rbp)
.L3640:
	cmpl	$1, (%rcx)
	je	.L5520
	movq	8(%rcx), %rdi
	leaq	(%r12,%r12,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movzbl	8(%rax), %eax
	movb	%al, 0(%r13,%r12)
	addq	$1, %r12
	cmpl	%r12d, %r8d
	jg	.L3640
.L3637:
	movq	-1520(%rbp), %rax
	testq	%rax, %rax
	je	.L3641
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3641:
	movq	%r13, -1520(%rbp)
	jmp	.L3632
	.p2align 4,,10
	.p2align 3
.L5432:
	leaq	.LC83(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5375:
	movq	%r14, %rdi
	leaq	.LC126(%rip), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5382:
	leaq	.LC121(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5316:
	movq	-1440(%rbp), %r14
.L4330:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r13
	xorl	%r15d, %r15d
	cmpq	%r13, %rbx
	je	.L3233
	cmpb	$0, -120(%rbx)
	setne	%r15b
.L3233:
	movabsq	$-6148914691236517205, %rdx
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movq	%rax, %r8
	cmpq	-72(%rbp), %rbx
	je	.L3234
	movq	-304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movb	%r15b, 16(%rbx)
	xorl	$1, %r15d
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 72(%rbx)
	pxor	%xmm0, %xmm0
	movb	%r15b, 56(%rbx)
	movb	$4, (%rbx)
	andb	$1, 56(%rbx)
	movl	%r8d, 4(%rbx)
	movq	%rax, 8(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 48(%rbx)
	movl	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	movb	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movl	$-1, 128(%rbx)
	movups	%xmm0, 104(%rbx)
	movq	-80(%rbp), %rax
	leaq	136(%rax), %r12
	movq	%r12, -80(%rbp)
.L3235:
	movzbl	-1388(%rbp), %eax
	movq	-992(%rbp), %rbx
	testb	%al, %al
	je	.L5521
	movq	-304(%rbp), %r13
	cmpb	$10, %al
	je	.L3246
	movl	$1, -72(%r12)
.L3247:
	movzbl	-1388(%rbp), %eax
	cmpb	$10, %al
	jne	.L3249
	movq	-1376(%rbp), %rax
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
.L3249:
	movq	%r13, -64(%r12)
	movb	%al, -56(%r12)
	movq	$0, -48(%r12)
.L3245:
	cmpb	$10, -1388(%rbp)
	leaq	-112(%r12), %r9
	jne	.L5522
	movq	-1376(%rbp), %rax
	movq	8(%rax), %r13
	movl	%r13d, -112(%r12)
	cmpl	$1, %r13d
	je	.L5523
	jbe	.L3257
	movq	-192(%rbp), %rdi
	movl	%r13d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5524
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3260:
	movq	%rax, -104(%r12)
	movq	(%rbx), %rsi
	movzbl	8(%rbx), %ecx
	movq	16(%rbx), %rdx
	movq	%rsi, (%rax)
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	leal	-2(%r13), %eax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %r8
	movl	$24, %eax
	.p2align 4,,10
	.p2align 3
.L3261:
	movq	-104(%r12), %rdx
	movq	(%rbx,%rax), %rdi
	movzbl	8(%rbx,%rax), %esi
	movq	16(%rbx,%rax), %rcx
	addq	%rax, %rdx
	addq	$24, %rax
	movq	%rdi, (%rdx)
	movb	%sil, 8(%rdx)
	movq	%rcx, 16(%rdx)
	cmpq	%rax, %r8
	jne	.L3261
.L3257:
	movl	-1392(%rbp), %eax
	cmpq	$0, -264(%rbp)
	leal	1(%rax), %r13d
	jne	.L3262
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5525
.L3262:
	movl	-132(%r12), %esi
	movq	%r14, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0
	movq	-992(%rbp), %rdi
	cmpq	-1512(%rbp), %rdi
	je	.L3217
	call	free@PLT
	jmp	.L3217
	.p2align 4,,10
	.p2align 3
.L5390:
	leaq	.LC111(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
	movl	$2, %esi
	jmp	.L3831
	.p2align 4,,10
	.p2align 3
.L3130:
	leaq	-1192(%rbp), %rbx
	leaq	-1000(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%rbx, -1512(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -1200(%rbp)
	movaps	%xmm0, -1216(%rbp)
	jmp	.L4321
	.p2align 4,,10
	.p2align 3
.L3318:
	leaq	-744(%rbp), %rbx
	leaq	-552(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%rbx, -1512(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -752(%rbp)
	movaps	%xmm0, -768(%rbp)
	jmp	.L4324
	.p2align 4,,10
	.p2align 3
.L3377:
	leaq	-520(%rbp), %rdi
	leaq	-328(%rbp), %rax
	movq	%rdi, %xmm0
	movq	%rdi, -1440(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -528(%rbp)
	movaps	%xmm0, -544(%rbp)
	jmp	.L4327
	.p2align 4,,10
	.p2align 3
.L4272:
	leaq	.LC0(%rip), %rcx
	jmp	.L4270
	.p2align 4,,10
	.p2align 3
.L5397:
	movzbl	1(%rcx), %edx
	movl	$1, %ebx
	movl	%edx, %eax
	movl	%edx, %esi
	andl	$127, %eax
	andl	$127, %esi
	testb	%dl, %dl
	js	.L5526
.L3491:
	cmpb	$1, %al
	jne	.L3501
	movl	%ebx, %eax
	addl	$1, %ebx
	leaq	1(%rcx,%rax), %rsi
	cmpq	%rdi, %rsi
	ja	.L3504
	cmpl	%esi, %edi
	je	.L3504
	movzbl	(%rsi), %eax
	subl	$64, %eax
	cmpb	$63, %al
	ja	.L3505
	leaq	.L3507(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L3507:
	.long	.L4425-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3514-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3513-.L3507
	.long	.L3512-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3505-.L3507
	.long	.L3511-.L3507
	.long	.L3510-.L3507
	.long	.L4426-.L3507
	.long	.L3508-.L3507
	.long	.L3506-.L3507
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L3504:
	leaq	.LC68(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L3505:
	movb	$10, -1545(%rbp)
.L3515:
	leaq	.LC69(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3503
.L3510:
	movb	$4, -1545(%rbp)
	jmp	.L3503
.L3511:
	movb	$5, -1545(%rbp)
	jmp	.L3503
.L3512:
	movb	$7, -1545(%rbp)
	jmp	.L3503
.L3513:
	movb	$6, -1545(%rbp)
	jmp	.L3503
.L3514:
	movb	$9, -1545(%rbp)
	jmp	.L3503
.L3506:
	movb	$1, -1545(%rbp)
	jmp	.L3503
.L4425:
	movb	$0, -1545(%rbp)
	jmp	.L3515
.L4426:
	movb	$3, -1545(%rbp)
	jmp	.L3503
.L3508:
	movb	$2, -1545(%rbp)
	jmp	.L3503
	.p2align 4,,10
	.p2align 3
.L3995:
	cmpb	$0, -223(%rbp)
	je	.L5527
	movq	-216(%rbp), %rax
	movb	$1, 9(%rax)
	jmp	.L3994
	.p2align 4,,10
	.p2align 3
.L3742:
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L3743
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3218:
	movq	-1376(%rbp), %rax
	movq	%rax, %r12
	jmp	.L3220
	.p2align 4,,10
	.p2align 3
.L5419:
	movq	-1320(%rbp), %rax
	movq	-1520(%rbp), %r14
	movq	%rax, -1488(%rbp)
	jmp	.L3863
	.p2align 4,,10
	.p2align 3
.L5415:
	movq	-1320(%rbp), %rax
	movq	-1520(%rbp), %r14
	movq	%rax, -1488(%rbp)
.L3914:
	movq	-1488(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L4500
	xorl	%r13d, %r13d
	movq	-112(%rbp), %r12
	leaq	-304(%rbp), %rsi
	subq	-120(%rbp), %r12
	movq	%r12, -1512(%rbp)
	leaq	-128(%rbp), %r15
	leaq	-1360(%rbp), %r12
	movq	%rbx, -1488(%rbp)
	movq	%r13, %rbx
	movq	%rsi, %r13
	movq	%r14, -1520(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L3917:
	movq	16(%r14), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movzbl	(%rdx,%rbx), %edx
	addq	$1, %rbx
	movb	%dl, -1360(%rbp)
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	cmpq	%rbx, -1488(%rbp)
	jne	.L3917
	movq	-1520(%rbp), %r14
	movq	-1512(%rbp), %r13
	addq	-120(%rbp), %r13
.L3916:
	cmpq	$0, -264(%rbp)
	movq	-544(%rbp), %r15
	jne	.L3918
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5528
.L3918:
	cmpq	-1440(%rbp), %r15
	je	.L3894
	movq	%r15, %rdi
	call	free@PLT
	jmp	.L3894
	.p2align 4,,10
	.p2align 3
.L5378:
	movzbl	(%rsi), %edx
	movl	%edx, %ebx
	andl	$127, %ebx
	movl	%ebx, -1568(%rbp)
	testb	%dl, %dl
	js	.L4223
	addl	$1, -1512(%rbp)
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L5377:
	movzbl	2(%r15), %edx
	movl	$3, %ebx
	movl	$1, -1512(%rbp)
	movl	%edx, %r11d
	andl	$127, %r11d
	movl	%r11d, -1520(%rbp)
	testb	%dl, %dl
	js	.L5529
.L4212:
	movl	-1520(%rbp), %edi
	cmpl	%ecx, %edi
	jbe	.L4221
	movl	%edi, %r8d
	xorl	%eax, %eax
	leaq	.LC29(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-296(%rbp), %rax
	jmp	.L4221
.L4359:
	xorl	%r8d, %r8d
.L3127:
	movq	-304(%rbp), %rsi
	leaq	.LC82(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3129
.L4391:
	xorl	%r8d, %r8d
.L3315:
	movq	-304(%rbp), %rsi
	leaq	.LC82(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3317
.L4401:
	xorl	%r8d, %r8d
.L3369:
	movq	-304(%rbp), %rsi
	leaq	.LC82(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3370
	.p2align 4,,10
	.p2align 3
.L3193:
	movq	-304(%rbp), %rax
	leaq	.LC84(%rip), %rdx
	movq	%r14, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-1496(%rbp), %r13d
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3948:
	movq	-304(%rbp), %rax
	leaq	.LC106(%rip), %rdx
	movq	%r14, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L3434:
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%r8, %rax
	subq	%rdx, %rax
	cmpq	$136, %rax
	jne	.L3435
	movq	-304(%rbp), %rax
	leaq	1(%rax), %r13
	movq	-296(%rbp), %rax
	cmpq	%rax, %r13
	je	.L3436
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC94(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3483:
	movb	%r15b, -1328(%rbp)
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-304(%rbp), %rsi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-264(%rbp), %r15
	movq	-112(%rbp), %rbx
	testq	%r15, %r15
	jne	.L3484
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	jne	.L3484
	movq	-1440(%rbp), %rsi
	movq	-176(%rbp), %rdi
	leaq	-768(%rbp), %r15
	leaq	-760(%rbp), %rcx
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder12BranchNoHintEPNS1_4NodeEPS4_S5_@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3485
	cmpl	$-1, -168(%rbp)
	je	.L3485
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3485:
	movq	-176(%rbp), %rdi
	movq	%r15, %rdx
	movl	$2, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder5MergeEjPPNS1_4NodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3486
	cmpl	$-1, -168(%rbp)
	je	.L3486
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %r15
.L3486:
	movl	-1512(%rbp), %esi
	movq	-176(%rbp), %rdi
	movl	$2, %edx
	movq	%r15, %r8
	movq	-1496(%rbp), %xmm0
	leaq	-544(%rbp), %rcx
	movhps	-1488(%rbp), %xmm0
	movaps	%xmm0, -544(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3487
	cmpl	$-1, -168(%rbp)
	je	.L3487
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %rdx
.L3487:
	movq	%rdx, -8(%rbx)
	movq	-184(%rbp), %rax
	movq	%r15, 8(%rax)
	movq	-264(%rbp), %r15
	jmp	.L3484
	.p2align 4,,10
	.p2align 3
.L5465:
	movzbl	%cl, %edi
	movl	$1, %esi
	movq	%r9, -1440(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-1440(%rbp), %r9
	cmpb	$1, %al
	je	.L3608
	cmpb	$10, %cl
	je	.L3608
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r15
	movq	-296(%rbp), %rax
	cmpq	%rax, %r13
	jnb	.L3609
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
	movq	-296(%rbp), %rax
.L3609:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3610
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3610:
	pushq	%r15
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r10
	movq	-304(%rbp), %r9
	popq	%r11
	jmp	.L3608
	.p2align 4,,10
	.p2align 3
.L5471:
	movzbl	%cl, %edi
	movl	$1, %esi
	movq	%r9, -1512(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-1512(%rbp), %r9
	cmpb	$1, %al
	je	.L3622
	cmpb	$10, %cl
	je	.L3622
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %r15
	movq	%rax, %r13
	movq	-296(%rbp), %rax
	cmpq	%rax, %r12
	jnb	.L3623
	leaq	-296(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %r15
	movq	-296(%rbp), %rax
.L3623:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3624
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3624:
	pushq	%r13
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	pushq	%r15
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r8
	popq	%r9
	movq	-304(%rbp), %r9
	jmp	.L3622
	.p2align 4,,10
	.p2align 3
.L5403:
	movzbl	%r10b, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3463
	cmpb	$10, %r10b
	je	.L3463
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r15
	movq	-296(%rbp), %rax
	cmpq	%rax, %r12
	jnb	.L3467
	leaq	-296(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r13
	movq	%rax, %rbx
	movq	-296(%rbp), %rax
.L3467:
	leaq	.LC0(%rip), %rcx
	cmpq	%r13, %rax
	jbe	.L3468
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3468:
	pushq	%r15
	movq	%r12, %rsi
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	pushq	%rbx
	leaq	.LC8(%rip), %r9
	movl	$2, %r8d
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rbx
	movq	-80(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rcx
	popq	%r12
	jmp	.L3464
	.p2align 4,,10
	.p2align 3
.L5509:
	movq	(%r12), %rsi
	movzbl	8(%r12), %ecx
	movq	16(%r12), %rax
	movq	%rsi, -104(%rbx)
	movb	%cl, -96(%rbx)
	movq	%rax, -88(%rbx)
	jmp	.L3354
	.p2align 4,,10
	.p2align 3
.L5507:
	movl	$0, -72(%rbx)
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L5501:
	movq	(%r12), %rcx
	movzbl	8(%r12), %edx
	movq	16(%r12), %rax
	movq	%rcx, -104(%rbx)
	movb	%dl, -96(%rbx)
	movq	%rax, -88(%rbx)
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L5499:
	movl	$0, -72(%rbx)
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L4616:
	leal	-7(%rdx), %r9d
	andl	$253, %r9d
	jne	.L3553
	cmpb	$8, %cl
	je	.L3551
.L3553:
	cmpb	$10, %cl
	ja	.L3558
	leaq	.L3560(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L3560:
	.long	.L3570-.L3560
	.long	.L4437-.L3560
	.long	.L3568-.L3560
	.long	.L3567-.L3560
	.long	.L3566-.L3560
	.long	.L3565-.L3560
	.long	.L3564-.L3560
	.long	.L3563-.L3560
	.long	.L3562-.L3560
	.long	.L3561-.L3560
	.long	.L3559-.L3560
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L3561:
	leaq	.LC14(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L3569:
	cmpb	$10, %dl
	ja	.L3571
	leaq	.L3573(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L3573:
	.long	.L3583-.L3573
	.long	.L4438-.L3573
	.long	.L3581-.L3573
	.long	.L3580-.L3573
	.long	.L3579-.L3573
	.long	.L3578-.L3573
	.long	.L3577-.L3573
	.long	.L3576-.L3573
	.long	.L3575-.L3573
	.long	.L3574-.L3573
	.long	.L3572-.L3573
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L3581:
	leaq	.LC17(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L3582:
	movq	%r11, %rsi
	movl	%r10d, %ecx
	leaq	.LC22(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$1, %esi
	jmp	.L3545
.L4438:
	leaq	.LC8(%rip), %r8
	jmp	.L3582
.L3583:
	leaq	.LC16(%rip), %r8
	jmp	.L3582
.L3572:
	leaq	.LC20(%rip), %r8
	jmp	.L3582
.L3574:
	leaq	.LC14(%rip), %r8
	jmp	.L3582
.L3575:
	leaq	.LC13(%rip), %r8
	jmp	.L3582
.L3576:
	leaq	.LC12(%rip), %r8
	jmp	.L3582
.L3577:
	leaq	.LC11(%rip), %r8
	jmp	.L3582
.L3578:
	leaq	.LC15(%rip), %r8
	jmp	.L3582
.L3579:
	leaq	.LC10(%rip), %r8
	jmp	.L3582
.L3580:
	leaq	.LC9(%rip), %r8
	jmp	.L3582
.L3563:
	leaq	.LC12(%rip), %r9
	jmp	.L3569
.L3564:
	leaq	.LC11(%rip), %r9
	jmp	.L3569
.L3562:
	leaq	.LC13(%rip), %r9
	jmp	.L3569
.L3565:
	leaq	.LC15(%rip), %r9
	jmp	.L3569
.L3566:
	leaq	.LC10(%rip), %r9
	jmp	.L3569
.L3567:
	leaq	.LC9(%rip), %r9
	jmp	.L3569
.L3570:
	leaq	.LC16(%rip), %r9
	jmp	.L3569
.L3559:
	leaq	.LC20(%rip), %r9
	jmp	.L3569
.L4437:
	leaq	.LC8(%rip), %r9
	jmp	.L3569
.L3568:
	leaq	.LC17(%rip), %r9
	jmp	.L3569
	.p2align 4,,10
	.p2align 3
.L5508:
	movl	$0, -112(%rbx)
	jmp	.L3354
	.p2align 4,,10
	.p2align 3
.L3343:
	movq	-1344(%rbp), %rax
	movq	(%rax), %r15
	movl	%r15d, -72(%rbx)
	cmpl	$1, %r15d
	je	.L3344
	jbe	.L3342
	movq	-192(%rbp), %rdi
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L5530
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3349:
	movq	%rax, -64(%rbx)
	leal	-1(%r15), %r8d
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.L3352
	.p2align 4,,10
	.p2align 3
.L5531:
	movq	-64(%rbx), %rax
	movq	%rdi, %rcx
.L3352:
	movzbl	-1356(%rbp), %edi
	addq	%rsi, %rax
	cmpb	$10, %dil
	jne	.L3350
	movq	-1344(%rbp), %rdi
	movq	16(%rdi), %rdi
	movzbl	(%rdi,%rcx), %edi
.L3350:
	movb	%dil, 8(%rax)
	addq	$24, %rsi
	leaq	1(%rcx), %rdi
	movq	%r13, (%rax)
	movq	$0, 16(%rax)
	cmpq	%r8, %rcx
	jne	.L5531
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3156:
	movq	-1408(%rbp), %rax
	movq	(%rax), %r15
	movl	%r15d, -72(%rbx)
	cmpl	$1, %r15d
	je	.L3157
	jbe	.L3155
	movq	-192(%rbp), %rdi
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5532
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3162:
	movq	%rax, -64(%rbx)
	leal	-1(%r15), %edi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L3165
	.p2align 4,,10
	.p2align 3
.L5533:
	movq	-64(%rbx), %rax
	movq	%rsi, %rdx
.L3165:
	movzbl	-1420(%rbp), %esi
	addq	%rcx, %rax
	cmpb	$10, %sil
	jne	.L3163
	movq	-1408(%rbp), %rsi
	movq	16(%rsi), %rsi
	movzbl	(%rsi,%rdx), %esi
.L3163:
	movb	%sil, 8(%rax)
	addq	$24, %rcx
	leaq	1(%rdx), %rsi
	movq	%r13, (%rax)
	movq	$0, 16(%rax)
	cmpq	%rdx, %rdi
	jne	.L5533
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L5500:
	movl	$0, -112(%rbx)
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L5506:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r13
	xorl	%r15d, %r15d
	cmpq	%r13, %rbx
	je	.L3390
	cmpb	$0, -120(%rbx)
	setne	%r15b
.L3390:
	movabsq	$-6148914691236517205, %rdx
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rax, %rdx
	cmpq	-72(%rbp), %rbx
	je	.L3391
	movq	-304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movb	%r15b, 16(%rbx)
	xorl	$1, %r15d
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 72(%rbx)
	pxor	%xmm0, %xmm0
	movb	%r15b, 56(%rbx)
	movb	$0, (%rbx)
	andb	$1, 56(%rbx)
	movl	%edx, 4(%rbx)
	movq	%rax, 8(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 48(%rbx)
	movl	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	movb	$0, 96(%rbx)
	movq	$0, 120(%rbx)
	movl	$-1, 128(%rbx)
	movups	%xmm0, 104(%rbx)
	movq	-80(%rbp), %rax
	leaq	136(%rax), %r9
	movq	%r9, -80(%rbp)
.L3392:
	movzbl	-1324(%rbp), %eax
	movq	-544(%rbp), %rbx
	testb	%al, %al
	je	.L5534
	movq	-304(%rbp), %r13
	cmpb	$10, %al
	je	.L3403
	movl	$1, -72(%r9)
.L3404:
	movzbl	-1324(%rbp), %eax
	cmpb	$10, %al
	jne	.L3406
	movq	-1312(%rbp), %rax
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
.L3406:
	movq	%r13, -64(%r9)
	movb	%al, -56(%r9)
	movq	$0, -48(%r9)
.L3402:
	cmpb	$10, -1324(%rbp)
	leaq	-112(%r9), %r15
	jne	.L5535
	movq	-1312(%rbp), %rax
	movq	8(%rax), %r12
	movl	%r12d, -112(%r9)
	cmpl	$1, %r12d
	je	.L5536
	jbe	.L3414
	movq	-192(%rbp), %rdi
	movl	%r12d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L5537
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L3417:
	movq	%rdx, -104(%r9)
	movq	16(%rbx), %rax
	movq	(%rbx), %rsi
	movzbl	8(%rbx), %ecx
	movq	%rax, 16(%rdx)
	leal	-2(%r12), %eax
	movq	%rsi, (%rdx)
	leaq	(%rax,%rax,2), %rax
	movb	%cl, 8(%rdx)
	leaq	48(,%rax,8), %r8
	movl	$24, %eax
	.p2align 4,,10
	.p2align 3
.L3418:
	movq	-104(%r9), %rdx
	movq	(%rbx,%rax), %rdi
	movzbl	8(%rbx,%rax), %esi
	movq	16(%rbx,%rax), %rcx
	addq	%rax, %rdx
	addq	$24, %rax
	movq	%rdi, (%rdx)
	movb	%sil, 8(%rdx)
	movq	%rcx, 16(%rdx)
	cmpq	%rax, %r8
	jne	.L3418
.L3414:
	cmpq	$0, -264(%rbp)
	jne	.L3419
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5538
.L3419:
	movl	-1328(%rbp), %eax
	movl	-132(%r9), %esi
	movq	%r15, %rdx
	movq	%r14, %rdi
	leal	1(%rax), %r13d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0
	jmp	.L3389
	.p2align 4,,10
	.p2align 3
.L3268:
	leaq	.LC86(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3797:
	movq	%r12, %rsi
	leaq	.LC59(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-264(%rbp), %r15
	movq	-304(%rbp), %rsi
	jmp	.L3796
	.p2align 4,,10
	.p2align 3
.L5386:
	leaq	-1(%r13), %rsi
	leaq	.LC31(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-264(%rbp), %r15
	movl	$1, %esi
	jmp	.L3845
	.p2align 4,,10
	.p2align 3
.L3965:
	leaq	.LC112(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L3963
	.p2align 4,,10
	.p2align 3
.L3766:
	leaq	1(%r15), %rsi
	leaq	.LC107(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-264(%rbp), %r15
	movl	$1, %esi
	jmp	.L3768
	.p2align 4,,10
	.p2align 3
.L5388:
	leaq	-1(%r13), %rsi
	leaq	.LC31(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-264(%rbp), %r15
	movl	$1, %esi
	jmp	.L3831
	.p2align 4,,10
	.p2align 3
.L3812:
	movq	%r8, %rsi
	leaq	.LC59(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-264(%rbp), %r15
	movq	-304(%rbp), %r8
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3750:
	movq	-304(%rbp), %rax
	leaq	.LC107(%rip), %rdx
	movq	%r14, %rdi
	leaq	1(%rax), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-264(%rbp), %r15
	movl	$1, %esi
	jmp	.L3752
	.p2align 4,,10
	.p2align 3
.L3746:
	movq	-304(%rbp), %rsi
	leaq	.LC106(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-264(%rbp), %r15
	movl	$1, %esi
	jmp	.L3745
	.p2align 4,,10
	.p2align 3
.L5467:
	leaq	1(%r11), %rsi
	movl	%eax, %ecx
	leaq	.LC87(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$1, %esi
	jmp	.L3545
	.p2align 4,,10
	.p2align 3
.L3755:
	leaq	1(%r15), %rsi
	leaq	.LC107(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-264(%rbp), %r15
	movl	$1, %esi
	jmp	.L3757
	.p2align 4,,10
	.p2align 3
.L3735:
	leaq	.LC104(%rip), %rdx
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3738:
	leaq	.LC105(%rip), %rdx
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L5473:
	movq	%rcx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rcx
	jmp	.L2932
	.p2align 4,,10
	.p2align 3
.L3425:
	movq	%r13, %rsi
	leaq	.LC90(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3426:
	movb	$1, -136(%rbx)
	cmpq	$0, -264(%rbp)
	jne	.L3427
	movq	-80(%rbp), %rax
	movq	%rax, %rdx
	subq	-88(%rbp), %rdx
	cmpq	$136, %rdx
	je	.L3428
	cmpb	$0, -256(%rax)
	jne	.L3427
.L3428:
	cmpb	$0, -120(%rbx)
	je	.L5539
.L3429:
	movq	-24(%rbx), %rax
	movq	-176(%rbp), %rdx
	leaq	8(%rax), %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	movq	%rax, 40(%rdx)
.L3427:
	cmpb	$0, -120(%rbx)
	jne	.L3430
	movb	$1, -40(%rbx)
.L3430:
	movl	-132(%rbx), %esi
	leaq	-112(%rbx), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0
	movq	-80(%rbp), %rax
	cmpb	$0, -256(%rax)
	setne	-120(%rbx)
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3278:
	leaq	1(%r9,%rax), %rsi
	leaq	.LC84(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$1, %esi
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L5462:
	leaq	.LC0(%rip), %rcx
	cmpq	%r10, -296(%rbp)
	jbe	.L3200
	movq	-1536(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r10
	movq	%rax, %rcx
.L3200:
	movq	-1520(%rbp), %rdi
	movq	%r10, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r10
	jmp	.L3199
	.p2align 4,,10
	.p2align 3
.L5437:
	leaq	.LC88(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rcx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5448:
	leaq	.LC91(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3832:
	leaq	.LC55(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-240(%rbp), %rdx
	jmp	.L3834
	.p2align 4,,10
	.p2align 3
.L4419:
	movl	$10, -1512(%rbp)
	movl	%ebx, %r12d
	movl	$10, %r15d
	movq	$0, -1496(%rbp)
	jmp	.L3476
	.p2align 4,,10
	.p2align 3
.L3846:
	leaq	.LC55(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3848
	.p2align 4,,10
	.p2align 3
.L5449:
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC92(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3788:
	movq	-24(%rdx), %rsi
	movzbl	-16(%rdx), %ecx
	subq	$24, %rdx
	movq	16(%rdx), %r12
	movq	%rdx, -112(%rbp)
	movq	%rsi, -1440(%rbp)
	cmpb	%r9b, %cl
	je	.L3791
	movzbl	%r9b, %r15d
	movzbl	%cl, %edi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3791
	cmpb	$1, %al
	je	.L3791
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1440(%rbp), %rsi
	movq	%rax, -1496(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -1488(%rbp)
	cmpq	%rdx, %rsi
	jnb	.L3792
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rdx
	movq	%rax, -1488(%rbp)
.L3792:
	movl	%r15d, %edi
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-1512(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	%rdx, %rbx
	jnb	.L3793
	leaq	-296(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1512(%rbp), %r9
	movq	%rax, %rcx
.L3793:
	pushq	-1496(%rbp)
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	xorl	%eax, %eax
	pushq	-1488(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1440(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r9
	popq	%r10
	jmp	.L3791
	.p2align 4,,10
	.p2align 3
.L5480:
	movq	-176(%rbp), %rdi
	movq	%r13, %rsi
	subq	-312(%rbp), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder11UnreachableEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3730
	cmpl	$-1, -168(%rbp)
	je	.L3730
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3730:
	movq	-80(%rbp), %rax
	movq	-264(%rbp), %r15
	movl	-132(%rax), %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	-120(%rbp), %rdx
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, -112(%rbp)
	jne	.L4341
	jmp	.L3731
	.p2align 4,,10
	.p2align 3
.L5487:
	leaq	.LC119(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	2(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3779:
	movq	%r13, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC18(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rdi), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5405:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L3475
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3475:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3474
	.p2align 4,,10
	.p2align 3
.L5478:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L3607
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3607:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r9
	jmp	.L3606
	.p2align 4,,10
	.p2align 3
.L5481:
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-80(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	jmp	.L3462
	.p2align 4,,10
	.p2align 3
.L5373:
	leaq	.LC96(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	jmp	.L4294
	.p2align 4,,10
	.p2align 3
.L5479:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L3621
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3621:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r9
	jmp	.L3620
	.p2align 4,,10
	.p2align 3
.L5372:
	leaq	.LC132(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	jmp	.L4292
	.p2align 4,,10
	.p2align 3
.L5404:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L3471
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3471:
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movabsq	$-6148914691236517205, %rsi
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	movl	-132(%rdx), %r9d
	sarq	$3, %rax
	imulq	%rsi, %rax
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L5379:
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-544(%rbp), %rdx
	jmp	.L4233
	.p2align 4,,10
	.p2align 3
.L3706:
	testq	%r15, %r15
	jne	.L3707
	movq	-176(%rbp), %r12
	xorl	%ebx, %ebx
.L4347:
	movq	96(%r12), %r8
	testq	%rcx, %rcx
	jne	.L3710
	jmp	.L3709
	.p2align 4,,10
	.p2align 3
.L3221:
	leaq	-968(%rbp), %rbx
	leaq	-776(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%rbx, -1512(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -976(%rbp)
	movaps	%xmm0, -992(%rbp)
	jmp	.L4330
	.p2align 4,,10
	.p2align 3
.L5469:
	movq	-208(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L3587
	movq	-112(%rbp), %r13
	leaq	(%r12,%r12,2), %rax
	salq	$3, %rax
	movq	%r13, %rbx
	subq	%rax, %rbx
	cmpq	$0, -264(%rbp)
	jne	.L3600
	movq	-176(%rbp), %rcx
	movq	104(%rcx), %rax
	cmpq	%rax, %r12
	jbe	.L3589
	movq	(%rcx), %rdi
	leaq	5(%r12,%rax), %r15
	leaq	0(,%r15,8), %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5540
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3591:
	movq	%rax, 96(%rcx)
	movq	%r15, 104(%rcx)
.L3589:
	movq	96(%rcx), %rsi
	leaq	16(%rbx), %rax
	leaq	(%rsi,%r12,8), %rcx
	cmpq	%rcx, %rax
	setnb	%dil
	cmpq	%rsi, %r13
	setbe	%dl
	orb	%dl, %dil
	je	.L4351
	leaq	-1(%r12), %rdx
	cmpq	$22, %rdx
	jbe	.L4351
	movq	%r12, %rcx
	movq	%rsi, %rdx
	shrq	%rcx
	salq	$4, %rcx
	addq	%rsi, %rcx
.L3595:
	movq	(%rax), %xmm0
	addq	$16, %rdx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rcx
	jne	.L3595
	movq	%r12, %rax
	andq	$-2, %rax
	testb	$1, %r12b
	je	.L3596
	leaq	(%rax,%rax,2), %rdx
	movq	16(%rbx,%rdx,8), %rdx
	movq	%rdx, (%rsi,%rax,8)
.L3596:
	movq	-176(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder6ReturnENS0_6VectorIPNS1_4NodeEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3600
	cmpl	$-1, -168(%rbp)
	je	.L3600
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	jmp	.L3600
.L5440:
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3374
	cmpb	$10, %cl
	je	.L3374
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r13
	movq	-296(%rbp), %rax
	cmpq	%rax, %r12
	jnb	.L3375
	leaq	-296(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
	movq	-296(%rbp), %rax
.L3375:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3376
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3376:
	pushq	%r13
	movq	%r12, %rsi
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	popq	%rsi
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L5455:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L3312
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3312:
	leaq	.LC18(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3311
	.p2align 4,,10
	.p2align 3
.L5411:
	leaq	1(%r12), %rsi
	movl	%r8d, %ecx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC114(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3894
	.p2align 4,,10
	.p2align 3
.L5315:
	movq	-1520(%rbp), %r14
.L4331:
	cmpq	$0, -264(%rbp)
	jne	.L3205
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5541
.L3205:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
	movq	-544(%rbp), %rdi
	movl	-1496(%rbp), %r13d
	cmpq	-1440(%rbp), %rdi
	je	.L3192
	call	free@PLT
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L5412:
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %ecx
	subq	$24, %rdx
	movq	(%rdx), %r13
	movq	%rdx, -112(%rbp)
	movq	%rax, -1568(%rbp)
	cmpb	$1, %cl
	je	.L3900
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	je	.L3900
	cmpb	$1, %al
	je	.L3900
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r15
	movq	-296(%rbp), %rax
	cmpq	%rax, %r13
	jnb	.L3902
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r12
	movq	%rax, %rbx
	movq	-296(%rbp), %rax
.L3902:
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %r12
	jnb	.L3903
	leaq	-296(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3903:
	pushq	%r15
	movq	%r13, %rsi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %r9
	pushq	%rbx
	xorl	%r8d, %r8d
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-1320(%rbp), %rax
	popq	%rcx
	popq	%rsi
	movq	%rax, -1488(%rbp)
	jmp	.L3900
.L5392:
	movzbl	%cl, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3839
	cmpb	$10, %cl
	je	.L3839
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rbx
	movq	%rax, %r15
	movq	-296(%rbp), %rax
	cmpq	%rax, %r13
	jnb	.L3840
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rbx
	movq	-296(%rbp), %rax
.L3840:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3841
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3841:
	pushq	%r15
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r9
	popq	%r10
	jmp	.L3839
	.p2align 4,,10
	.p2align 3
.L5504:
	cmpq	-104(%rbp), %rsi
	je	.L3175
	movdqu	-104(%rbx), %xmm2
	movups	%xmm2, (%rsi)
	movq	-88(%rbx), %rax
	movq	%rax, 16(%rsi)
	addq	$24, -112(%rbp)
	jmp	.L3176
	.p2align 4,,10
	.p2align 3
.L5512:
	cmpq	-104(%rbp), %rsi
	je	.L3362
	movdqu	-104(%rbx), %xmm3
	movups	%xmm3, (%rsi)
	movq	-88(%rbx), %rax
	movq	%rax, 16(%rsi)
	addq	$24, -112(%rbp)
	jmp	.L3363
.L5526:
	leaq	2(%rcx), %r8
	cmpq	%r8, %rdi
	ja	.L5542
	leaq	.LC66(%rip), %rcx
	movq	%r8, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3501
	.p2align 4,,10
	.p2align 3
.L3550:
	subq	%r9, %rcx
	movzbl	16(%rax), %edx
	movzbl	8(%rcx), %ecx
	cmpb	%dl, %cl
	je	.L3554
	leal	-7(%rcx), %eax
	cmpb	$2, %al
	ja	.L4617
	cmpb	$6, %dl
	je	.L3554
.L4617:
	leal	-7(%rdx), %eax
	testb	$-3, %al
	jne	.L4618
	cmpb	$8, %cl
	je	.L3554
.L4618:
	xorl	%r10d, %r10d
	jmp	.L3553
	.p2align 4,,10
	.p2align 3
.L3516:
	movzbl	-16(%rcx), %r15d
	movq	-24(%rcx), %r13
	subq	$24, %rcx
	movq	16(%rcx), %r12
	movq	%rcx, -112(%rbp)
	cmpb	$1, %r15b
	je	.L3520
	movzbl	%r15b, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3519
	cmpb	$10, %r15b
	je	.L3519
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rdx
	movq	%rax, %r15
	movq	-296(%rbp), %rax
	cmpq	%rax, %r13
	jnb	.L3523
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rdx
	movq	-296(%rbp), %rax
.L3523:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3524
	leaq	-296(%rbp), %rdi
	movq	%rdx, -1440(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1440(%rbp), %rdx
	movq	%rax, %rcx
.L3524:
	pushq	%r15
	leaq	.LC8(%rip), %r9
	movq	%r13, %rsi
	movq	%r14, %rdi
	pushq	%rdx
	movl	$2, %r8d
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r8
	movq	-80(%rbp), %r10
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %rcx
	popq	%r9
	jmp	.L3520
	.p2align 4,,10
	.p2align 3
.L3534:
	movq	-24(%rcx), %rsi
	movq	-8(%rcx), %rax
	subq	$24, %rcx
	movzbl	8(%rcx), %r13d
	movq	%rcx, -112(%rbp)
	movzbl	-1545(%rbp), %ecx
	movq	%rsi, -1440(%rbp)
	movq	%rax, -1496(%rbp)
	cmpb	%cl, %r13b
	je	.L3537
	movl	%r15d, %esi
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r13b
	setne	%dl
	testb	%dl, %cl
	je	.L3537
	cmpb	$1, %al
	je	.L3537
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1440(%rbp), %rsi
	movq	%rax, -1520(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -1512(%rbp)
	cmpq	%rdx, %rsi
	jnb	.L3538
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movq	%rax, -1512(%rbp)
.L3538:
	movl	%r15d, %edi
	movq	%rdx, -1528(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1528(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r15
	cmpq	%rdx, %rsi
	jnb	.L3539
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3539:
	pushq	-1520(%rbp)
	movq	%r15, %r9
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	pushq	-1512(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	movq	-1440(%rbp), %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r15
	popq	%rax
	jmp	.L3537
	.p2align 4,,10
	.p2align 3
.L3525:
	movq	-8(%rcx), %rax
	movq	-24(%rcx), %r13
	subq	$24, %rcx
	movzbl	8(%rcx), %r8d
	movq	%rcx, -112(%rbp)
	movq	%rax, -1488(%rbp)
	cmpb	-1545(%rbp), %r8b
	je	.L5543
	movl	%r15d, %esi
	movzbl	%r8b, %edi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, -1545(%rbp)
	setne	%dl
	testb	%dl, %sil
	je	.L4615
	cmpb	$1, %al
	je	.L4615
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	%rax, -1496(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -1440(%rbp)
	cmpq	%rdx, %r13
	jnb	.L3532
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movq	%rax, -1440(%rbp)
.L3532:
	movl	%r15d, %edi
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1512(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	%rdx, %rsi
	jnb	.L3533
	leaq	-296(%rbp), %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1512(%rbp), %r9
	movq	%rax, %rcx
.L3533:
	pushq	-1496(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	pushq	-1440(%rbp)
	leaq	.LC19(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %r10
	movabsq	$-6148914691236517205, %rdx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	movl	-132(%r10), %esi
	sarq	$3, %rax
	imulq	%rdx, %rax
	popq	%rdx
	popq	%rdi
	jmp	.L3528
	.p2align 4,,10
	.p2align 3
.L3904:
	leaq	-520(%rbp), %rbx
	leaq	-328(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%rbx, -1440(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -528(%rbp)
	movaps	%xmm0, -544(%rbp)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3823:
	movzbl	-16(%rcx), %r8d
	movq	-24(%rcx), %r15
	subq	$24, %rcx
	movq	16(%rcx), %rbx
	movq	%rcx, -112(%rbp)
	cmpb	$1, %r8b
	je	.L3826
	movzbl	%r8b, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3826
	cmpb	$10, %r8b
	je	.L3826
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rdx
	movq	%rax, %r8
	movq	-296(%rbp), %rax
	cmpq	%rax, %r15
	jnb	.L3827
	leaq	-296(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r8, -1440(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1440(%rbp), %r8
	movq	%rax, %rdx
	movq	-296(%rbp), %rax
.L3827:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %rsi
	jnb	.L3828
	leaq	-296(%rbp), %rdi
	movq	%rdx, -1488(%rbp)
	movq	%r8, -1440(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1488(%rbp), %rdx
	movq	-1440(%rbp), %r8
	movq	%rax, %rcx
.L3828:
	pushq	%r8
	movq	%r15, %rsi
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%rdx
	movq	%r14, %rdi
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r11
	popq	%r15
	jmp	.L3826
	.p2align 4,,10
	.p2align 3
.L3799:
	movq	-8(%rcx), %rbx
	movzbl	-16(%rcx), %r8d
	subq	$24, %rcx
	movq	(%rcx), %r15
	movq	%rcx, -112(%rbp)
	movq	%rbx, -1440(%rbp)
	cmpb	$1, %r8b
	je	.L3803
	movzbl	%r8b, %edi
	movl	$1, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L3802
	cmpb	$10, %r8b
	je	.L3802
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	leaq	.LC0(%rip), %rbx
	movq	%rax, %rdx
	movq	-296(%rbp), %rax
	cmpq	%rax, %r15
	jnb	.L3806
	leaq	-296(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rdx, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r12
	movq	-1488(%rbp), %rdx
	movq	%rax, %rbx
	movq	-296(%rbp), %rax
.L3806:
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %r12
	jnb	.L3807
	leaq	-296(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdx, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1488(%rbp), %rdx
	movq	%rax, %rcx
.L3807:
	pushq	%rdx
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%r14, %rdi
	pushq	%rbx
	xorl	%eax, %eax
	leaq	.LC8(%rip), %r9
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	movl	-1328(%rbp), %eax
	movq	-240(%rbp), %rsi
	jmp	.L3803
	.p2align 4,,10
	.p2align 3
.L3814:
	salq	$4, %rsi
	movq	-24(%rcx), %rax
	movzbl	-16(%rcx), %ebx
	subq	$24, %rcx
	movzbl	(%rdi,%rsi), %r15d
	movq	16(%rcx), %r12
	movq	%rcx, -112(%rbp)
	movq	%rax, -1440(%rbp)
	cmpb	%bl, %r15b
	je	.L5544
	movzbl	%r15b, %r10d
	movzbl	%bl, %edi
	movq	%r8, -1496(%rbp)
	movl	%r10d, %esi
	movl	%r10d, -1488(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r15b
	setne	%sil
	cmpb	$10, %bl
	setne	%dl
	testb	%dl, %sil
	je	.L4627
	cmpb	$1, %al
	movl	-1488(%rbp), %r10d
	movq	-1496(%rbp), %r8
	je	.L4627
	movl	%r10d, -1496(%rbp)
	leaq	.LC0(%rip), %rbx
	movq	%r8, -1488(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1440(%rbp), %rsi
	movq	-1488(%rbp), %r8
	movl	-1496(%rbp), %r10d
	movq	%rax, %r15
	cmpq	%rdx, %rsi
	jnb	.L3821
	leaq	-296(%rbp), %rdi
	movl	%r10d, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r8
	movq	-296(%rbp), %rdx
	movl	-1488(%rbp), %r10d
	movq	%rax, %rbx
.L3821:
	movl	%r10d, %edi
	movq	%rdx, -1496(%rbp)
	movq	%r8, -1488(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-1488(%rbp), %r8
	movq	-1496(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	%rdx, %r8
	jnb	.L3822
	leaq	-296(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rax, -1488(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1488(%rbp), %r9
	movq	%rax, %rcx
.L3822:
	pushq	%r15
	movq	-1440(%rbp), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	pushq	%rbx
	leaq	.LC19(%rip), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %r9
	movabsq	$-6148914691236517205, %rdx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	movl	-132(%r9), %r10d
	sarq	$3, %rax
	imulq	%rdx, %rax
	popq	%rdx
	popq	%rsi
	jmp	.L3817
.L4223:
	leaq	1(%rsi), %r8
	cmpq	%rax, %r8
	jb	.L5545
	leaq	.LC30(%rip), %rcx
	movq	%r8, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addl	$1, -1512(%rbp)
	movl	$0, -1568(%rbp)
	jmp	.L4224
.L5529:
	leaq	3(%r15), %r8
	cmpq	%r8, %rax
	jbe	.L4213
	movzbl	3(%r15), %edi
	movl	$4, %ebx
	movl	$2, -1512(%rbp)
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r11d
	movl	%r11d, -1520(%rbp)
	testb	%dil, %dil
	jns	.L4212
	leaq	4(%r15), %r8
	cmpq	%r8, %rax
	jbe	.L4213
	movzbl	4(%r15), %edi
	movl	$5, %ebx
	movl	$3, -1512(%rbp)
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r11d
	movl	%r11d, -1520(%rbp)
	testb	%dil, %dil
	jns	.L4212
	leaq	5(%r15), %r8
	cmpq	%r8, %rax
	jbe	.L4213
	movzbl	5(%r15), %edi
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r11d
	movl	%r11d, -1520(%rbp)
	testb	%dil, %dil
	js	.L5546
	movl	$4, -1512(%rbp)
	movl	$6, %ebx
	jmp	.L4212
	.p2align 4,,10
	.p2align 3
.L5433:
	leaq	.LC85(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
.L4375:
	xorl	%r8d, %r8d
.L3219:
	movq	-304(%rbp), %rsi
	leaq	.LC82(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3217
	.p2align 4,,10
	.p2align 3
.L3331:
	movabsq	$-1085102592571150095, %rdi
	movq	%r12, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	$15790320, %rax
	je	.L3145
	testq	%rax, %rax
	movl	$1, %ecx
	cmovne	%rax, %rcx
	addq	%rcx, %rax
	jc	.L3334
	testq	%rax, %rax
	jne	.L5547
	movl	$136, %ebx
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
.L3336:
	movq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	addq	%rdx, %rax
	movq	-304(%rbp), %rdx
	movups	%xmm1, 32(%rax)
	movb	%r15b, 16(%rax)
	xorl	$1, %r15d
	movups	%xmm1, 72(%rax)
	pxor	%xmm1, %xmm1
	movb	%r15b, 56(%rax)
	movb	$3, (%rax)
	andb	$1, 56(%rax)
	movl	%r8d, 4(%rax)
	movq	%rdx, 8(%rax)
	movl	$0, 24(%rax)
	movq	$0, 48(%rax)
	movl	$0, 64(%rax)
	movq	$0, 88(%rax)
	movb	$0, 96(%rax)
	movq	$0, 120(%rax)
	movl	$-1, 128(%rax)
	movups	%xmm1, 104(%rax)
	cmpq	%r13, %r12
	je	.L4400
	movq	%r13, %rax
	movq	%xmm0, %rdx
	.p2align 4,,10
	.p2align 3
.L3340:
	movdqu	(%rax), %xmm5
	addq	$136, %rax
	addq	$136, %rdx
	movups	%xmm5, -136(%rdx)
	movdqu	-120(%rax), %xmm6
	movups	%xmm6, -120(%rdx)
	movdqu	-104(%rax), %xmm4
	movups	%xmm4, -104(%rdx)
	movdqu	-88(%rax), %xmm7
	movups	%xmm7, -88(%rdx)
	movdqu	-72(%rax), %xmm5
	movups	%xmm5, -72(%rdx)
	movdqu	-56(%rax), %xmm6
	movups	%xmm6, -56(%rdx)
	movdqu	-40(%rax), %xmm4
	movups	%xmm4, -40(%rdx)
	movdqu	-24(%rax), %xmm7
	movups	%xmm7, -24(%rdx)
	movq	-8(%rax), %rsi
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L3340
	leaq	-136(%r12), %rax
	movq	%xmm0, %rdi
	movabsq	$1220740416642543857, %rdx
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rdx
	leaq	136(%rdx), %rbx
.L3339:
	movq	%rbx, %xmm6
	movq	%rcx, -72(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L3332
	.p2align 4,,10
	.p2align 3
.L3143:
	movabsq	$-1085102592571150095, %rcx
	movq	%r12, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	$15790320, %rax
	je	.L3145
	testq	%rax, %rax
	movl	$1, %ecx
	cmovne	%rax, %rcx
	addq	%rcx, %rax
	jc	.L3147
	testq	%rax, %rax
	jne	.L5548
	movl	$136, %ebx
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
.L3149:
	movq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	addq	%rdx, %rax
	movq	-304(%rbp), %rdx
	movups	%xmm1, 32(%rax)
	movb	%r15b, 16(%rax)
	xorl	$1, %r15d
	movups	%xmm1, 72(%rax)
	pxor	%xmm1, %xmm1
	movb	%r15b, 56(%rax)
	movb	$2, (%rax)
	andb	$1, 56(%rax)
	movl	%r8d, 4(%rax)
	movq	%rdx, 8(%rax)
	movl	$0, 24(%rax)
	movq	$0, 48(%rax)
	movl	$0, 64(%rax)
	movq	$0, 88(%rax)
	movb	$0, 96(%rax)
	movq	$0, 120(%rax)
	movl	$-1, 128(%rax)
	movups	%xmm1, 104(%rax)
	cmpq	%r13, %r12
	je	.L3152
	movq	%r13, %rax
	movq	%xmm0, %rdx
	.p2align 4,,10
	.p2align 3
.L3153:
	movdqu	(%rax), %xmm7
	addq	$136, %rax
	addq	$136, %rdx
	movups	%xmm7, -136(%rdx)
	movdqu	-120(%rax), %xmm7
	movups	%xmm7, -120(%rdx)
	movdqu	-104(%rax), %xmm5
	movups	%xmm5, -104(%rdx)
	movdqu	-88(%rax), %xmm6
	movups	%xmm6, -88(%rdx)
	movdqu	-72(%rax), %xmm7
	movups	%xmm7, -72(%rdx)
	movdqu	-56(%rax), %xmm5
	movups	%xmm5, -56(%rdx)
	movdqu	-40(%rax), %xmm6
	movups	%xmm6, -40(%rdx)
	movdqu	-24(%rax), %xmm7
	movups	%xmm7, -24(%rdx)
	movq	-8(%rax), %rsi
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L3153
	leaq	-136(%r12), %rax
	movq	%xmm0, %rbx
	movabsq	$1220740416642543857, %rdx
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rbx,%rax,8), %rbx
.L3152:
	movq	%rbx, %xmm5
	movq	%rcx, -72(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L3144
	.p2align 4,,10
	.p2align 3
.L5484:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L3185
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3185:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3184
	.p2align 4,,10
	.p2align 3
.L5485:
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder7RethrowEPNS1_4NodeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3190
	cmpl	$-1, -168(%rbp)
	je	.L3190
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3190:
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %rdi
	movq	8(%rax), %rdx
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder14TerminateThrowEPNS1_4NodeES4_@PLT
	jmp	.L3189
.L5523:
	movq	(%rbx), %rcx
	movzbl	8(%rbx), %edx
	movq	16(%rbx), %rax
	movq	%rcx, -104(%r12)
	movb	%dl, -96(%r12)
	movq	%rax, -88(%r12)
	jmp	.L3257
	.p2align 4,,10
	.p2align 3
.L3435:
	cmpq	$0, -264(%rbp)
	movzbl	-136(%rbx), %eax
	jne	.L3451
	cmpb	$0, -256(%r8)
	jne	.L3451
	cmpb	$3, %al
	je	.L3453
	cmpb	$0, -120(%rbx)
	je	.L5549
.L3454:
	movq	-176(%rbp), %rdx
	testb	%al, %al
	je	.L5550
.L3455:
	movq	-32(%rbx), %rax
	leaq	8(%rax), %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	movq	%rax, 40(%rdx)
	movzbl	-136(%rbx), %eax
.L3451:
	cmpb	$3, %al
	je	.L5344
	movl	-132(%rbx), %esi
	leaq	-72(%rbx), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE15PushMergeValuesEPNS6_7ControlEPNS1_5MergeINS6_5ValueEEE.isra.0
.L5344:
	movq	-80(%rbp), %r8
.L3453:
	cmpb	$0, -120(%rbx)
	leaq	-136(%r8), %rax
	je	.L3457
	cmpb	$0, -40(%rbx)
	jne	.L3458
	cmpb	$0, -136(%rbx)
	movq	%rax, -80(%rbp)
	jne	.L3459
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5521:
	movl	$0, -72(%r12)
	jmp	.L3245
.L3246:
	movq	-1376(%rbp), %rax
	movq	(%rax), %r15
	movl	%r15d, -72(%r12)
	cmpl	$1, %r15d
	je	.L3247
	jbe	.L3245
	movq	-192(%rbp), %rdi
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5551
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3252:
	movq	%rax, -64(%r12)
	leal	-1(%r15), %edi
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L5552:
	movq	-64(%r12), %rax
	movq	%rdx, %rcx
.L3255:
	movzbl	-1388(%rbp), %edx
	addq	%rsi, %rax
	cmpb	$10, %dl
	jne	.L3253
	movq	-1376(%rbp), %rdx
	movq	16(%rdx), %rdx
	movzbl	(%rdx,%rcx), %edx
.L3253:
	movb	%dl, 8(%rax)
	addq	$24, %rsi
	leaq	1(%rcx), %rdx
	movq	%r13, (%rax)
	movq	$0, 16(%rax)
	cmpq	%rdi, %rcx
	jne	.L5552
	jmp	.L3245
.L5522:
	movl	$0, -112(%r12)
	jmp	.L3257
.L5503:
	movq	-184(%rbp), %rsi
	movq	%rsi, -32(%rbx)
	movq	-192(%rbp), %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0
	movq	-176(%rbp), %rdx
	leaq	8(%rax), %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	movq	%rax, 40(%rdx)
	jmp	.L3172
.L5486:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L3373
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3373:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3372
.L5511:
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface4LoopEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlE
	jmp	.L3359
.L5468:
	movq	8(%rbx), %r8
	movq	%r11, %rsi
	subl	-312(%rbp), %r8d
	xorl	%eax, %eax
	movl	%r12d, %ecx
	leaq	.LC21(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$1, %esi
	jmp	.L3545
.L5451:
	leaq	1(%r15), %rsi
	movl	%r8d, %ecx
	leaq	.LC87(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movl	$1, %esi
	jmp	.L3276
.L5456:
	movq	%rbx, %r14
	movzbl	-1496(%rbp), %ebx
.L3313:
	leaq	-128(%rbp), %r15
	leaq	-304(%rbp), %rsi
	movq	%r12, %rdx
	movb	$9, -1360(%rbp)
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-112(%rbp), %rax
	movq	-1520(%rbp), %rdi
	movl	-1488(%rbp), %esi
	movb	%bl, -16(%rax)
	movq	-1512(%rbp), %rbx
	movq	%rdi, -24(%rax)
	movq	%rbx, -8(%rax)
	jmp	.L3276
.L5466:
	addq	$1, %rsi
	leaq	.LC87(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %rsi
	movq	-264(%rbp), %r15
	jmp	.L3611
.L3699:
	testl	%ecx, %ecx
	jle	.L3707
	leaq	-296(%rbp), %rax
	movq	%r14, -1440(%rbp)
	leal	-1(%rcx), %r15d
	xorl	%r12d, %r12d
	movq	%rax, -1488(%rbp)
	jmp	.L3727
	.p2align 4,,10
	.p2align 3
.L5554:
	cmpb	$2, -120(%rsi)
	jne	.L5553
.L3724:
	leaq	1(%r12), %rax
	cmpq	%r12, %r15
	je	.L5334
	movq	-208(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%rax, %r12
.L3727:
	movq	-112(%rbp), %rcx
	movl	%r12d, %ebx
	movabsq	$-6148914691236517205, %rdi
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movl	-132(%rsi), %edi
	cmpq	%rdi, %rax
	jbe	.L5554
	movq	16(%rdx), %rax
	movzbl	-16(%rcx), %r8d
	subq	$24, %rcx
	movq	(%rcx), %r13
	movzbl	(%rax,%r12), %r9d
	movq	%rcx, -112(%rbp)
	cmpb	%r8b, %r9b
	je	.L3724
	movzbl	%r9b, %r14d
	movzbl	%r8b, %edi
	movl	%r14d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r9b
	setne	%cl
	cmpb	$10, %r8b
	setne	%dl
	testb	%dl, %cl
	je	.L3724
	cmpb	$1, %al
	je	.L3724
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	%rax, -1512(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -1496(%rbp)
	cmpq	%rdx, %r13
	jnb	.L3725
	movq	-1488(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movq	%rax, -1496(%rbp)
.L3725:
	movl	%r14d, %edi
	movq	%rdx, -1520(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1520(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	%rdx, %rsi
	jnb	.L3726
	movq	-1488(%rbp), %rdi
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1520(%rbp), %r9
	movq	%rax, %rcx
.L3726:
	pushq	-1512(%rbp)
	xorl	%eax, %eax
	movl	%ebx, %r8d
	movq	%r13, %rsi
	pushq	-1496(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1440(%rbp), %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	popq	%rdx
	jmp	.L3724
	.p2align 4,,10
	.p2align 3
.L5553:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L3723
	movq	-1488(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3723:
	movq	-1440(%rbp), %rdi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3724
.L4239:
	movzbl	1(%rsi), %edi
	sall	$8, %ebx
	orl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L4237
.L5514:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L4237
.L5488:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L3838
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3838:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3837
.L5393:
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder10MemoryGrowEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3842
	cmpl	$-1, -168(%rbp)
	je	.L3842
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3842:
	movq	%rax, -8(%rbx)
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L3843
	movq	-176(%rbp), %rdi
	leaq	24(%rax), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder17InitInstanceCacheEPNS1_22WasmInstanceCacheNodesE@PLT
.L3843:
	movq	-264(%rbp), %r15
	movl	$2, %esi
	jmp	.L3831
.L3967:
	leaq	.LC113(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L3963
	.p2align 4,,10
	.p2align 3
.L5461:
	leaq	-520(%rbp), %rbx
	leaq	-328(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%rbx, -1440(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -528(%rbp)
	movaps	%xmm0, -544(%rbp)
	jmp	.L4331
.L5334:
	movq	-1440(%rbp), %r14
	jmp	.L3707
.L5482:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r15
	jnb	.L3772
	movq	%r15, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r15
	movq	%rax, %rcx
.L3772:
	leaq	.LC18(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3771
.L5383:
	movq	%r14, %rdi
	leaq	.LC96(%rip), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rdi
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	2(%rdi), %r13
	jmp	.L3125
.L5527:
	leaq	.LC120(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	2(%rbx), %r13
	jmp	.L3125
.L3743:
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder7RefNullEv@PLT
	movq	%rax, -8(%rbx)
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
.L3546:
	cmpb	$3, %r8b
	leaq	64(%rbx), %rsi
	movq	%r14, %rdi
	cmove	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE25TypeCheckUnreachableMergeERNS1_5MergeINS6_5ValueEEEb
	movl	$1, %esi
	testb	%al, %al
	je	.L3545
	jmp	.L3600
	.p2align 4,,10
	.p2align 3
.L4628:
	leal	-7(%rbx), %edi
	andl	$253, %edi
	jne	.L4629
	testb	%al, %al
	jne	.L4083
.L4629:
	leal	-7(%rcx), %eax
	testb	$-3, %al
	sete	%al
	testb	%dl, %al
	jne	.L4083
	cmpb	$10, %cl
	setne	%dl
	cmpb	$10, %bl
	setne	%al
	testb	%al, %dl
	je	.L4083
	cmpb	$9, %bl
	ja	.L4086
	leaq	.L4088(%rip), %rdi
	movzbl	%bl, %edx
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L4088:
	.long	.L4097-.L4088
	.long	.L4522-.L4088
	.long	.L4095-.L4088
	.long	.L4094-.L4088
	.long	.L4093-.L4088
	.long	.L4092-.L4088
	.long	.L4091-.L4088
	.long	.L4090-.L4088
	.long	.L4089-.L4088
	.long	.L4087-.L4088
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L4095:
	leaq	.LC17(%rip), %rdx
.L4096:
	movq	-296(%rbp), %rax
	leaq	.LC0(%rip), %r8
	cmpq	%rax, %rsi
	jnb	.L4098
	movzbl	(%rsi), %edi
	movq	%rdx, -1560(%rbp)
	movq	%r10, -1544(%rbp)
	movb	%cl, -1536(%rbp)
	movq	%rsi, -1528(%rbp)
	movl	%edi, -1520(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-1520(%rbp), %edi
	movq	-1528(%rbp), %rsi
	testb	%al, %al
	movzbl	-1536(%rbp), %ecx
	movq	-1544(%rbp), %r10
	movq	-1560(%rbp), %rdx
	je	.L5555
	movq	-296(%rbp), %rax
	leaq	1(%rsi), %r9
	leaq	.LC0(%rip), %r8
	cmpq	%r9, %rax
	jbe	.L4098
	movzbl	1(%rsi), %eax
	sall	$8, %edi
	movq	%rdx, -1544(%rbp)
	movq	%r10, -1536(%rbp)
	orl	%eax, %edi
	movb	%cl, -1528(%rbp)
	movq	%rsi, -1520(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-1544(%rbp), %rdx
	movq	-1536(%rbp), %r10
	movq	%rax, %r8
	movzbl	-1528(%rbp), %ecx
	movq	-296(%rbp), %rax
	movq	-1520(%rbp), %rsi
.L4098:
	cmpb	$9, %cl
	ja	.L4100
	leaq	.L4102(%rip), %rdi
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.align 4
	.align 4
.L4102:
	.long	.L4111-.L4102
	.long	.L4525-.L4102
	.long	.L4109-.L4102
	.long	.L4108-.L4102
	.long	.L4107-.L4102
	.long	.L4106-.L4102
	.long	.L4105-.L4102
	.long	.L4104-.L4102
	.long	.L4103-.L4102
	.long	.L4101-.L4102
	.section	.text._ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
.L4522:
	leaq	.LC8(%rip), %rdx
	jmp	.L4096
.L4097:
	leaq	.LC16(%rip), %rdx
	jmp	.L4096
.L4087:
	leaq	.LC14(%rip), %rdx
	jmp	.L4096
.L4091:
	leaq	.LC11(%rip), %rdx
	jmp	.L4096
.L4092:
	leaq	.LC15(%rip), %rdx
	jmp	.L4096
.L4093:
	leaq	.LC10(%rip), %rdx
	jmp	.L4096
.L4094:
	leaq	.LC9(%rip), %rdx
	jmp	.L4096
.L4089:
	leaq	.LC13(%rip), %rdx
	jmp	.L4096
.L4090:
	leaq	.LC12(%rip), %rdx
	jmp	.L4096
.L4103:
	leaq	.LC13(%rip), %r9
.L4110:
	movq	-304(%rbp), %rcx
	cmpq	%rax, %rcx
	jnb	.L4115
	movzbl	(%rcx), %edi
	movq	%r9, -1576(%rbp)
	movq	%rdx, -1568(%rbp)
	movq	%r8, -1560(%rbp)
	movq	%r10, -1544(%rbp)
	movq	%rsi, -1536(%rbp)
	movq	%rcx, -1528(%rbp)
	movl	%edi, -1520(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movl	-1520(%rbp), %edi
	movq	-1528(%rbp), %rcx
	testb	%al, %al
	movq	-1536(%rbp), %rsi
	movq	-1544(%rbp), %r10
	movq	-1560(%rbp), %r8
	movq	-1568(%rbp), %rdx
	movq	-1576(%rbp), %r9
	je	.L5556
	leaq	1(%rcx), %rax
	cmpq	%rax, -296(%rbp)
	jbe	.L4115
	movzbl	1(%rcx), %eax
	sall	$8, %edi
	movq	%r9, -1560(%rbp)
	movq	%rdx, -1544(%rbp)
	orl	%eax, %edi
	movq	%r8, -1536(%rbp)
	movq	%r10, -1528(%rbp)
	movq	%rsi, -1520(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-1560(%rbp), %r9
	movq	-1544(%rbp), %rdx
	movq	-1536(%rbp), %r8
	movq	-1528(%rbp), %r10
	movq	%rax, %rcx
	movq	-1520(%rbp), %rsi
.L4113:
	pushq	%rdx
	movq	-1496(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rdx
	pushq	%r8
	movl	%r12d, %r8d
	movq	%r10, -1528(%rbp)
	movq	%rsi, -1520(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	movq	-1528(%rbp), %r10
	popq	%rsi
	movq	-1520(%rbp), %rsi
	jmp	.L4083
.L4104:
	leaq	.LC12(%rip), %r9
	jmp	.L4110
.L4105:
	leaq	.LC11(%rip), %r9
	jmp	.L4110
.L4106:
	leaq	.LC15(%rip), %r9
	jmp	.L4110
.L4107:
	leaq	.LC10(%rip), %r9
	jmp	.L4110
.L4108:
	leaq	.LC9(%rip), %r9
	jmp	.L4110
.L4109:
	leaq	.LC17(%rip), %r9
	jmp	.L4110
.L4525:
	leaq	.LC8(%rip), %r9
	jmp	.L4110
.L4111:
	leaq	.LC16(%rip), %r9
	jmp	.L4110
.L4101:
	leaq	.LC14(%rip), %r9
	jmp	.L4110
	.p2align 4,,10
	.p2align 3
.L5515:
	movq	-536(%rbp), %rcx
	movq	-176(%rbp), %r15
	movq	%rcx, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	movq	%rax, %rdx
	movabsq	$-6148914691236517205, %rax
	imulq	%rax, %rdx
	movq	104(%r15), %rax
	movq	%rdx, %r9
	cmpq	%rax, %rdx
	ja	.L4277
	movq	96(%r15), %rdx
.L4278:
	testq	%r9, %r9
	je	.L4287
	leaq	16(%r13), %rax
	leaq	(%rdx,%r9,8), %rsi
	cmpq	%rsi, %rax
	setnb	%dil
	cmpq	%rdx, %rcx
	setbe	%cl
	orb	%cl, %dil
	je	.L4284
	leaq	-1(%r9), %rcx
	cmpq	$22, %rcx
	jbe	.L4284
	movq	%r9, %rsi
	movq	%rdx, %rcx
	shrq	%rsi
	salq	$4, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L4286:
	movq	(%rax), %xmm0
	addq	$16, %rcx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rcx, %rsi
	jne	.L4286
	movq	%r9, %rax
	andq	$-2, %rax
	andl	$1, %r9d
	je	.L4287
	leaq	(%rax,%rax,2), %rcx
	movq	16(%r13,%rcx,8), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L4287:
	movl	-1568(%rbp), %r8d
	movl	-1520(%rbp), %ecx
	movl	%r12d, %esi
	movq	-176(%rbp), %rdi
	movq	-304(%rbp), %r9
	subl	-312(%rbp), %r9d
	call	_ZN2v88internal8compiler16WasmGraphBuilder8AtomicOpENS0_4wasm10WasmOpcodeEPKPNS1_4NodeEjji@PLT
	testq	%rax, %rax
	je	.L4282
	cmpl	$-1, -168(%rbp)
	je	.L4282
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L4282:
	testq	%rbx, %rbx
	je	.L4291
	movq	%rax, 16(%rbx)
.L4291:
	movq	-544(%rbp), %r13
	jmp	.L4276
.L3587:
	cmpq	$0, -264(%rbp)
	jne	.L3600
	movq	-176(%rbp), %rax
	movq	96(%rax), %rsi
	jmp	.L3596
.L5534:
	movl	$0, -72(%r9)
	jmp	.L3402
.L5536:
	movq	(%rbx), %rcx
	movzbl	8(%rbx), %edx
	movq	16(%rbx), %rax
	movq	%rcx, -104(%r9)
	movb	%dl, -96(%r9)
	movq	%rax, -88(%r9)
	jmp	.L3414
.L4296:
	leaq	.LC133(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
	jmp	.L4298
.L3403:
	movq	-1312(%rbp), %rax
	movq	(%rax), %r15
	movl	%r15d, -72(%r9)
	cmpl	$1, %r15d
	je	.L3404
	jbe	.L3402
	movq	-192(%rbp), %rdi
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rsi
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5557
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3409:
	movq	%rax, -64(%r9)
	leal	-1(%r15), %esi
	xorl	%ecx, %ecx
	jmp	.L3412
	.p2align 4,,10
	.p2align 3
.L5558:
	movq	-64(%r9), %rax
	movq	%rdx, %rcx
.L3412:
	movzbl	-1324(%rbp), %edx
	addq	%r12, %rax
	cmpb	$10, %dl
	jne	.L3410
	movq	-1312(%rbp), %rdx
	movq	16(%rdx), %rdx
	movzbl	(%rdx,%rcx), %edx
.L3410:
	movb	%dl, 8(%rax)
	addq	$24, %r12
	leaq	1(%rcx), %rdx
	movq	%r13, (%rax)
	movq	$0, 16(%rax)
	cmpq	%rcx, %rsi
	jne	.L5558
	jmp	.L3402
.L5535:
	movl	$0, -112(%r9)
	jmp	.L3414
.L5407:
	leaq	1(%r9), %rsi
	movl	%r8d, %ecx
	leaq	.LC114(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3963
.L5516:
	movl	-1440(%rbp), %ecx
	addq	$1, %rsi
	leaq	.LC97(%rip), %rdx
.L5346:
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %rsi
	movq	-264(%rbp), %r15
	jmp	.L3625
.L5475:
	movq	%rdx, -1488(%rbp)
	movq	%r8, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %r8
	movq	-1488(%rbp), %rdx
	jmp	.L2925
.L5497:
	movq	%rdx, -1584(%rbp)
	movq	%r11, -1576(%rbp)
	movb	%cl, -1544(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movzbl	-1544(%rbp), %ecx
	movq	-1560(%rbp), %rsi
	movq	%rax, %r8
	movq	-1576(%rbp), %r11
	movq	-296(%rbp), %rax
	movq	-1584(%rbp), %rdx
	jmp	.L4255
.L5498:
	movq	%r9, -1592(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%r8, -1576(%rbp)
	movq	%r11, -1560(%rbp)
	movq	%rsi, -1544(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-1544(%rbp), %rsi
	movq	-1560(%rbp), %r11
	movq	-1576(%rbp), %r8
	movq	-1584(%rbp), %rdx
	movq	%rax, %rcx
	movq	-1592(%rbp), %r9
	jmp	.L4270
.L5542:
	movzbl	2(%rcx), %edx
	movl	$2, %ebx
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%esi, %eax
	testb	%dl, %dl
	jns	.L3491
	leaq	3(%rcx), %rsi
	cmpq	%rsi, %rdi
	ja	.L5559
	leaq	.LC66(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$2, %ebx
	jmp	.L3501
.L5545:
	movzbl	1(%rsi), %ecx
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, -1568(%rbp)
	testb	%cl, %cl
	js	.L4226
	addl	$2, -1512(%rbp)
	jmp	.L4224
.L4213:
	leaq	.LC28(%rip), %rcx
	movq	%r8, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L5349
.L4615:
	movq	%rcx, %rax
	movl	-132(%r10), %esi
	movabsq	$-6148914691236517205, %rdx
	subq	%r11, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L3528
.L3234:
	movabsq	$-1085102592571150095, %rdi
	movq	%rbx, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	$15790320, %rax
	je	.L3145
	testq	%rax, %rax
	movl	$1, %ecx
	cmovne	%rax, %rcx
	addq	%rcx, %rax
	jc	.L3237
	testq	%rax, %rax
	jne	.L5560
	movl	$136, %r12d
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
.L3239:
	movq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	addq	%rdx, %rax
	movq	-304(%rbp), %rdx
	movups	%xmm1, 32(%rax)
	movb	%r15b, 16(%rax)
	xorl	$1, %r15d
	movups	%xmm1, 72(%rax)
	pxor	%xmm1, %xmm1
	movb	%r15b, 56(%rax)
	movb	$4, (%rax)
	andb	$1, 56(%rax)
	movl	%r8d, 4(%rax)
	movq	%rdx, 8(%rax)
	movl	$0, 24(%rax)
	movq	$0, 48(%rax)
	movl	$0, 64(%rax)
	movq	$0, 88(%rax)
	movb	$0, 96(%rax)
	movq	$0, 120(%rax)
	movl	$-1, 128(%rax)
	movups	%xmm1, 104(%rax)
	cmpq	%r13, %rbx
	je	.L3242
	movq	%r13, %rax
	movq	%xmm0, %rdx
	.p2align 4,,10
	.p2align 3
.L3243:
	movdqu	(%rax), %xmm6
	addq	$136, %rax
	addq	$136, %rdx
	movups	%xmm6, -136(%rdx)
	movdqu	-120(%rax), %xmm4
	movups	%xmm4, -120(%rdx)
	movdqu	-104(%rax), %xmm6
	movups	%xmm6, -104(%rdx)
	movdqu	-88(%rax), %xmm4
	movups	%xmm4, -88(%rdx)
	movdqu	-72(%rax), %xmm5
	movups	%xmm5, -72(%rdx)
	movdqu	-56(%rax), %xmm2
	movups	%xmm2, -56(%rdx)
	movdqu	-40(%rax), %xmm3
	movups	%xmm3, -40(%rdx)
	movdqu	-24(%rax), %xmm7
	movups	%xmm7, -24(%rdx)
	movq	-8(%rax), %rsi
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L3243
	leaq	-136(%rbx), %rax
	movq	%xmm0, %rbx
	movabsq	$1220740416642543857, %rdx
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rbx,%rax,8), %r12
.L3242:
	movq	%r12, %xmm5
	movq	%rcx, -72(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L3235
.L4627:
	movq	%rcx, %rax
	movl	-132(%r9), %r10d
	movabsq	$-6148914691236517205, %rdx
	subq	%r11, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L3817
.L5423:
	movl	-1328(%rbp), %esi
	movq	-176(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder9SetGlobalEjPNS1_4NodeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3794
	cmpl	$-1, -168(%rbp)
	je	.L3794
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3794:
	movq	-264(%rbp), %r15
	jmp	.L3786
.L5422:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rbx
	jnb	.L3790
	movq	%rbx, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rbx
	movq	%rax, %rcx
.L3790:
	leaq	.LC18(%rip), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3789
.L3457:
	movq	-304(%rbp), %rbx
	movq	%rax, -80(%rbp)
	movq	-264(%rbp), %r15
	movq	-296(%rbp), %rax
	leaq	1(%rbx), %r13
	jmp	.L3125
.L3459:
	cmpb	$0, -256(%r8)
	je	.L3460
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L5452:
	movq	-24(%rdx), %rax
	movzbl	-16(%rdx), %ebx
	movq	%rax, -1520(%rbp)
	movq	-8(%rdx), %rax
	movq	%rax, -1512(%rbp)
	leaq	-24(%rdx), %rax
	movq	%rax, -112(%rbp)
	cmpb	$9, %bl
	je	.L3284
	movzbl	%bl, %edi
	movl	$9, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$1, %al
	je	.L4610
	cmpb	$10, %bl
	je	.L4610
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-1520(%rbp), %rsi
	leaq	.LC0(%rip), %r12
	movq	%rax, %r13
	movq	-296(%rbp), %rax
	cmpq	%rax, %rsi
	jnb	.L3287
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r15
	movq	%rax, %r12
	movq	-296(%rbp), %rax
.L3287:
	leaq	.LC0(%rip), %rcx
	cmpq	%rax, %r15
	jnb	.L3288
	leaq	-296(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3288:
	pushq	%r13
	movq	-1520(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	pushq	%r12
	leaq	.LC14(%rip), %r9
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r9
	movq	-1312(%rbp), %rcx
	popq	%r10
	jmp	.L3284
.L3272:
	movq	-80(%rbp), %rax
	movq	%rax, %rcx
	subq	-88(%rbp), %rcx
	cmpq	$136, %rcx
	je	.L3273
	cmpb	$0, -256(%rax)
	je	.L3273
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	leaq	1(%rbx), %r13
	jmp	.L3125
.L5525:
	movq	-184(%rbp), %r15
	movq	%r14, %rdi
	movq	%r9, -1440(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0
	movq	%r15, %rsi
	movl	$1, (%rax)
	movq	-192(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5StealEPNS0_4ZoneEPNS2_6SsaEnvE.isra.0
	movq	-176(%rbp), %rdx
	movq	-1440(%rbp), %r9
	leaq	8(%rax), %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	movq	%rax, 40(%rdx)
	movq	-192(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L5561
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3264:
	movq	%rbx, (%rax)
	movabsq	$-1085102592571150095, %rbx
	movq	$0, 8(%rax)
	movq	%r15, -32(%r12)
	movq	%rax, -16(%r12)
	movl	-168(%rbp), %eax
	movl	%eax, -8(%r12)
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	subl	$1, %eax
	movl	%eax, -168(%rbp)
	jmp	.L3262
.L5492:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L4080
	movzbl	(%rsi), %ebx
	movq	%rsi, -1520(%rbp)
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE@PLT
	movq	-1520(%rbp), %rsi
	testb	%al, %al
	je	.L5562
	leaq	1(%rsi), %rax
	cmpq	%rax, -296(%rbp)
	ja	.L4082
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
.L4080:
	movq	-1496(%rbp), %rdi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %rsi
	jmp	.L4079
.L3633:
	cmpb	$3, (%rax)
	jne	.L3644
	leaq	24(%rax), %rcx
.L3644:
	movl	(%rcx), %r9d
	movq	-1528(%rbp), %r8
	subq	-1520(%rbp), %r8
	cmpl	%r8d, %r9d
	jne	.L5563
	testl	%r9d, %r9d
	jle	.L3632
	movzbl	-225(%rbp), %r10d
	leal	-1(%r9), %r8d
	leaq	8(%rcx), %r11
	jmp	.L3659
.L5564:
	movq	%r11, %rax
	cmpl	$1, %r9d
	je	.L3648
	movq	8(%rcx), %rdx
	leaq	(%r12,%r12,2), %rax
	leaq	(%rdx,%rax,8), %rax
.L3648:
	movzbl	8(%rax), %r9d
	cmpb	%sil, %r9b
	je	.L3649
	cmpb	$9, %sil
	leal	-6(%rsi), %eax
	sete	%dil
	cmpb	$1, %al
	jbe	.L4619
	testb	%dil, %dil
	je	.L4450
.L4619:
	leal	-6(%r9), %eax
	cmpb	$1, %al
	jbe	.L4620
	cmpb	$9, %r9b
	je	.L4620
.L4450:
	movl	$10, %esi
.L3649:
	movq	-1520(%rbp), %rax
	movb	%sil, (%rax,%r12)
.L3656:
	leaq	1(%r12), %rax
	cmpq	%r12, %r8
	je	.L3632
	movl	(%rcx), %r9d
	movq	%rax, %r12
.L3659:
	movq	-1520(%rbp), %rax
	movzbl	(%rax,%r12), %esi
	testb	%r10b, %r10b
	jne	.L5564
	movq	%r11, %rax
	cmpl	$1, %r9d
	je	.L3658
	movq	8(%rcx), %rdx
	leaq	(%r12,%r12,2), %rax
	leaq	(%rdx,%rax,8), %rax
.L3658:
	movzbl	8(%rax), %eax
	cmpb	%sil, %al
	je	.L3656
	movzbl	%al, %edi
	movb	%sil, -1544(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movzbl	-1544(%rbp), %edi
	movq	%rax, %rbx
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%rbx, %r9
	movl	%r13d, %ecx
	movq	%r14, %rdi
	movq	-1536(%rbp), %rsi
	movq	%rax, %r8
	xorl	%eax, %eax
	leaq	.LC100(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
.L3630:
	movq	-80(%rbp), %r8
	movq	-1528(%rbp), %rcx
	subq	-1520(%rbp), %rcx
	cmpb	$0, -120(%r8)
	jne	.L5565
	movq	-112(%rbp), %rdx
	movq	%rdx, %r9
	subq	-120(%rbp), %r9
	sarq	$3, %r9
	imull	$-1431655765, %r9d, %r9d
	subl	-132(%r8), %r9d
	cmpl	%r9d, %ecx
	jg	.L5566
	movslq	%ecx, %rax
	testl	%ecx, %ecx
	jle	.L3665
	leaq	0(,%rax,4), %rsi
	movq	-1520(%rbp), %r9
	subl	$1, %ecx
	xorl	%ebx, %ebx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %r8
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L5568:
	movq	%rax, %rbx
.L3668:
	movzbl	(%r9,%rbx), %eax
	movzbl	8(%r8), %edx
	cmpb	%dl, %al
	je	.L3666
	movzbl	%al, %r12d
	movzbl	%dl, %edi
	movl	%r12d, %esi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	testb	%al, %al
	je	.L5567
.L3666:
	leaq	1(%rbx), %rax
	addq	$24, %r8
	cmpq	%rbx, %rcx
	jne	.L5568
.L3665:
	cmpq	$0, -264(%rbp)
	je	.L3677
.L4337:
	movl	$1, %r13d
.L3664:
	cmpq	$0, -1520(%rbp)
	jne	.L3667
.L3697:
	cmpq	$0, -1512(%rbp)
	je	.L5347
	movq	-1512(%rbp), %rdi
	call	_ZdlPv@PLT
.L5347:
	movq	-304(%rbp), %rsi
	movq	-264(%rbp), %r15
	jmp	.L3625
.L4491:
	xorl	%r13d, %r13d
	jmp	.L3865
.L4500:
	xorl	%r13d, %r13d
	jmp	.L3916
.L5413:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r12
	jnb	.L3901
	movq	%r12, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r12
	movq	%rax, %rcx
.L3901:
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-1320(%rbp), %rax
	movq	$0, -1568(%rbp)
	movq	%rax, -1488(%rbp)
	jmp	.L3900
.L5489:
	movq	-1320(%rbp), %rbx
	movq	-176(%rbp), %rcx
	movl	-1328(%rbp), %r9d
	movq	8(%rbx), %r15
	movq	104(%rcx), %rdx
	leal	1(%r15), %eax
	cltq
	cmpq	%rdx, %rax
	ja	.L3868
	movq	96(%rcx), %rdx
.L3869:
	movq	$0, -1360(%rbp)
	movq	$0, (%rdx)
	testl	%r15d, %r15d
	jle	.L3878
	movl	%r15d, %ecx
	leaq	16(%r12), %rax
	leaq	8(%rdx), %rsi
	leaq	8(%rdx,%rcx,8), %r8
	leaq	(%rcx,%rcx,2), %rcx
	cmpq	%r8, %rax
	leaq	(%r12,%rcx,8), %rcx
	leal	-1(%r15), %edi
	setnb	%r8b
	cmpq	%rcx, %rsi
	setnb	%cl
	orb	%cl, %r8b
	je	.L3875
	cmpl	$22, %edi
	jbe	.L3875
	movl	%r15d, %esi
	movq	%rdx, %rcx
	shrl	%esi
	salq	$4, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L3877:
	movq	(%rax), %xmm0
	addq	$16, %rcx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -8(%rcx)
	cmpq	%rcx, %rsi
	jne	.L3877
	movl	%r15d, %eax
	andl	$-2, %eax
	andl	$1, %r15d
	je	.L3878
	movslq	%eax, %rcx
	addl	$1, %eax
	leaq	(%rcx,%rcx,2), %rcx
	cltq
	movq	16(%r12,%rcx,8), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L3878:
	movq	-176(%rbp), %rdi
	leaq	-1360(%rbp), %r12
	movq	-304(%rbp), %r8
	movl	%r9d, %esi
	subl	-312(%rbp), %r8d
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder10CallDirectEjPPNS1_4NodeEPS5_i@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3873
	cmpl	$-1, -168(%rbp)
	je	.L3873
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3873:
	movq	(%rbx), %rdi
	testl	%edi, %edi
	jle	.L3888
	movq	-1360(%rbp), %rsi
	movl	%edi, %eax
	leaq	16(%r13), %rdx
	leal	-1(%rdi), %ecx
	leaq	(%rsi,%rax,8), %r8
	leaq	(%rax,%rax,2), %rax
	cmpq	%r8, %rdx
	leaq	0(%r13,%rax,8), %rax
	setnb	%r8b
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %r8b
	movq	%rsi, %rax
	je	.L3885
	cmpl	$22, %ecx
	jbe	.L3885
	movl	%edi, %ecx
	shrl	%ecx
	salq	$4, %rcx
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L3887:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	addq	$48, %rdx
	movq	%xmm0, -48(%rdx)
	movhps	%xmm0, -24(%rdx)
	cmpq	%rax, %rcx
	jne	.L3887
	movl	%edi, %eax
	andl	$-2, %eax
	andl	$1, %edi
	je	.L3888
	cltq
	movq	(%rsi,%rax,8), %rdx
	leaq	(%rax,%rax,2), %rax
	movq	%rdx, 16(%r13,%rax,8)
.L3888:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L3884
	movq	-176(%rbp), %rdi
	leaq	24(%rax), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder17InitInstanceCacheEPNS1_22WasmInstanceCacheNodesE@PLT
.L3884:
	movq	-544(%rbp), %r12
	jmp	.L3867
.L5528:
	movq	-1320(%rbp), %rbx
	movq	-176(%rbp), %rcx
	movl	-1324(%rbp), %r11d
	movl	-1328(%rbp), %r12d
	movq	8(%rbx), %rdx
	movq	104(%rcx), %rsi
	leal	1(%rdx), %eax
	cltq
	cmpq	%rsi, %rax
	ja	.L3919
	movq	96(%rcx), %r10
.L3920:
	movq	-1568(%rbp), %rax
	movq	$0, -1360(%rbp)
	movq	%rax, (%r10)
	testl	%edx, %edx
	jle	.L3929
	movl	%edx, %esi
	leaq	16(%r15), %rax
	leaq	8(%r10), %rcx
	leaq	8(%r10,%rsi,8), %rdi
	leaq	(%rsi,%rsi,2), %rsi
	cmpq	%rdi, %rax
	leaq	(%r15,%rsi,8), %rsi
	leal	-1(%rdx), %r8d
	setnb	%dil
	cmpq	%rsi, %rcx
	setnb	%sil
	orb	%sil, %dil
	je	.L3926
	cmpl	$22, %r8d
	jbe	.L3926
	movl	%edx, %esi
	movq	%r10, %rcx
	shrl	%esi
	salq	$4, %rsi
	addq	%r10, %rsi
	.p2align 4,,10
	.p2align 3
.L3928:
	movq	(%rax), %xmm0
	addq	$16, %rcx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -8(%rcx)
	cmpq	%rcx, %rsi
	jne	.L3928
	movl	%edx, %eax
	andl	$-2, %eax
	andl	$1, %edx
	je	.L3929
	movslq	%eax, %rdx
	addl	$1, %eax
	leaq	(%rdx,%rdx,2), %rdx
	cltq
	movq	16(%r15,%rdx,8), %rdx
	movq	%rdx, (%r10,%rax,8)
.L3929:
	movq	-304(%rbp), %r9
	subq	-312(%rbp), %r9
	cmpq	$0, -1568(%rbp)
	movq	-176(%rbp), %rdi
	je	.L5569
	leaq	-1360(%rbp), %r8
	movq	%r10, %rcx
	movl	%r11d, %edx
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder12CallIndirectEjjPPNS1_4NodeEPS5_i@PLT
	testq	%rax, %rax
	je	.L3934
	cmpl	$-1, -168(%rbp)
	je	.L3934
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3934:
	movq	(%rbx), %rdi
	testl	%edi, %edi
	jle	.L3942
	movq	-1360(%rbp), %rsi
	movl	%edi, %eax
	leaq	16(%r13), %rdx
	leal	-1(%rdi), %ecx
	leaq	(%rsi,%rax,8), %r8
	leaq	(%rax,%rax,2), %rax
	cmpq	%r8, %rdx
	leaq	0(%r13,%rax,8), %rax
	setnb	%r8b
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %r8b
	movq	%rsi, %rax
	je	.L3939
	cmpl	$22, %ecx
	jbe	.L3939
	movl	%edi, %ecx
	shrl	%ecx
	salq	$4, %rcx
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L3941:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	addq	$48, %rdx
	movq	%xmm0, -48(%rdx)
	movhps	%xmm0, -24(%rdx)
	cmpq	%rax, %rcx
	jne	.L3941
	movl	%edi, %eax
	andl	$-2, %eax
	andl	$1, %edi
	je	.L3942
	cltq
	movq	(%rsi,%rax,8), %rdx
	leaq	(%rax,%rax,2), %rax
	movq	%rdx, 16(%r13,%rax,8)
.L3942:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L3938
	movq	-176(%rbp), %rdi
	leaq	24(%rax), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder17InitInstanceCacheEPNS1_22WasmInstanceCacheNodesE@PLT
.L3938:
	movq	-544(%rbp), %r15
	jmp	.L3918
.L5543:
	movq	%rcx, %rax
	movl	-132(%r10), %esi
	subq	%r11, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L3528
.L5544:
	movq	%rcx, %rax
	movl	-132(%r9), %r10d
	subq	%r11, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L3817
.L5431:
	movq	-1440(%rbp), %rdx
	movq	%rsi, %rcx
	movq	-176(%rbp), %rdi
	movl	-1328(%rbp), %esi
	subq	-312(%rbp), %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder8TableGetEjPNS1_4NodeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3809
	cmpl	$-1, -168(%rbp)
	je	.L3809
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %rdx
.L3809:
	movq	%rdx, -8(%rbx)
	movq	-264(%rbp), %r15
	movq	-304(%rbp), %rsi
	jmp	.L3796
.L5398:
	leaq	.LC0(%rip), %rcx
	cmpq	%rsi, -296(%rbp)
	jbe	.L3518
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3518:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-80(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	jmp	.L3517
.L5430:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r12
	jnb	.L3801
	movq	%r12, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r12
	movq	%rax, %rcx
.L3801:
	leaq	.LC18(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3800
.L5427:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r8
	jnb	.L3816
	movq	%r8, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r8
	movq	%rax, %rcx
.L3816:
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %r9
	movabsq	$-6148914691236517205, %rdx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	movl	-132(%r9), %r10d
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L3815
.L5399:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L3527
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3527:
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-112(%rbp), %rcx
	movq	-80(%rbp), %r10
	movabsq	$-6148914691236517205, %rdx
	movq	%rcx, %rax
	subq	-120(%rbp), %rax
	movl	-132(%r10), %esi
	sarq	$3, %rax
	imulq	%rdx, %rax
	jmp	.L3526
.L5400:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L3536
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3536:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3535
.L5401:
	movq	-176(%rbp), %rdi
	leaq	-768(%rbp), %r15
	leaq	-760(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder12BranchNoHintEPNS1_4NodeEPS4_S5_@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3541
	cmpl	$-1, -168(%rbp)
	je	.L3541
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3541:
	movq	-176(%rbp), %rdi
	movq	%r15, %rdx
	movl	$2, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder5MergeEjPPNS1_4NodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3542
	cmpl	$-1, -168(%rbp)
	je	.L3542
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %r15
.L3542:
	movq	-1496(%rbp), %xmm0
	movl	$2, %edx
	movq	%r15, %r8
	movl	%r13d, %esi
	movq	-176(%rbp), %rdi
	leaq	-544(%rbp), %rcx
	movhps	-1488(%rbp), %xmm0
	movaps	%xmm0, -544(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder3PhiENS0_4wasm9ValueTypeEjPPNS1_4NodeES6_@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3543
	cmpl	$-1, -168(%rbp)
	je	.L3543
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %rdx
.L3543:
	movq	-1440(%rbp), %rax
	movq	%rdx, -8(%rax)
	movq	-184(%rbp), %rax
	movq	%r15, 8(%rax)
	movq	-264(%rbp), %r15
	jmp	.L3540
.L3613:
	movq	-304(%rbp), %rsi
	movq	-264(%rbp), %r15
	cmpl	$2, %eax
	jne	.L3618
	jmp	.L3611
.L5339:
	movl	-1512(%rbp), %r12d
	movq	-1496(%rbp), %r14
.L4077:
	movq	-1488(%rbp), %rax
	xorl	%r10d, %r10d
	cmpq	$0, (%rax)
	jne	.L5570
.L4117:
	movq	-264(%rbp), %r15
	movq	-544(%rbp), %r13
	testq	%r15, %r15
	jne	.L4118
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5571
.L4118:
	cmpq	-1440(%rbp), %r13
	je	.L4132
	movq	%r13, %rdi
	call	free@PLT
.L4132:
	movq	-264(%rbp), %r15
	movl	$2, %esi
	jmp	.L4013
.L3391:
	movabsq	$-1085102592571150095, %rdi
	movq	%rbx, %r8
	subq	%r13, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	$15790320, %rax
	je	.L3145
	testq	%rax, %rax
	movl	$1, %ecx
	cmovne	%rax, %rcx
	addq	%rcx, %rax
	jc	.L3394
	testq	%rax, %rax
	jne	.L5572
	movl	$136, %r9d
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
.L3396:
	movq	%xmm0, %rax
	movq	-304(%rbp), %rsi
	pxor	%xmm1, %xmm1
	addq	%r8, %rax
	movb	%r15b, 16(%rax)
	xorl	$1, %r15d
	movups	%xmm1, 32(%rax)
	movups	%xmm1, 72(%rax)
	pxor	%xmm1, %xmm1
	movb	%r15b, 56(%rax)
	movb	$0, (%rax)
	andb	$1, 56(%rax)
	movl	%edx, 4(%rax)
	movq	%rsi, 8(%rax)
	movl	$0, 24(%rax)
	movq	$0, 48(%rax)
	movl	$0, 64(%rax)
	movq	$0, 88(%rax)
	movb	$0, 96(%rax)
	movq	$0, 120(%rax)
	movl	$-1, 128(%rax)
	movups	%xmm1, 104(%rax)
	cmpq	%r13, %rbx
	je	.L3399
	movq	%r13, %rax
	movq	%xmm0, %rdx
	.p2align 4,,10
	.p2align 3
.L3400:
	movdqu	(%rax), %xmm4
	addq	$136, %rax
	addq	$136, %rdx
	movups	%xmm4, -136(%rdx)
	movdqu	-120(%rax), %xmm5
	movups	%xmm5, -120(%rdx)
	movdqu	-104(%rax), %xmm2
	movups	%xmm2, -104(%rdx)
	movdqu	-88(%rax), %xmm3
	movups	%xmm3, -88(%rdx)
	movdqu	-72(%rax), %xmm4
	movups	%xmm4, -72(%rdx)
	movdqu	-56(%rax), %xmm6
	movups	%xmm6, -56(%rdx)
	movdqu	-40(%rax), %xmm5
	movups	%xmm5, -40(%rdx)
	movdqu	-24(%rax), %xmm7
	movups	%xmm7, -24(%rdx)
	movq	-8(%rax), %rsi
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L3400
	leaq	-136(%rbx), %rax
	movq	%xmm0, %rbx
	movabsq	$1220740416642543857, %rdx
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rbx,%rax,8), %r9
.L3399:
	movq	%r9, %xmm6
	movq	%rcx, -72(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L3392
.L3273:
	movl	-8(%rbx), %eax
	movl	%eax, -168(%rbp)
	movq	-16(%rbx), %rax
	cmpq	$0, 8(%rax)
	je	.L5573
	movq	(%rax), %rax
	movq	-176(%rbp), %rcx
	leaq	8(%rax), %rsi
	movq	%rax, -184(%rbp)
	movq	%rsi, 24(%rcx)
	movq	-176(%rbp), %rcx
	leaq	16(%rax), %rsi
	addq	$24, %rax
	movq	%rsi, 32(%rcx)
	movq	-176(%rbp), %rcx
	movq	%rax, 40(%rcx)
	movq	-16(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, -8(%rdx)
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
.L5518:
	leaq	63(%rax), %r12
	shrq	$6, %r12
	salq	$3, %r12
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -1512(%rbp)
	call	memset@PLT
	jmp	.L4338
.L5538:
	movq	-1560(%rbp), %rsi
	movq	-176(%rbp), %rdi
	leaq	-1360(%rbp), %r12
	leaq	-1392(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r9, -1488(%rbp)
	movq	$0, -1392(%rbp)
	movq	$0, -1360(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder12BranchNoHintEPNS1_4NodeEPS4_S5_@PLT
	movq	-1488(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3420
	cmpl	$-1, -168(%rbp)
	je	.L3420
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-1488(%rbp), %r9
.L3420:
	movq	-184(%rbp), %rbx
	movq	%r14, %rdi
	movq	%r9, -1488(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0
	movq	-1488(%rbp), %r9
	movq	%rax, %r13
	movq	-1360(%rbp), %rax
	movq	%rax, 8(%r13)
	movq	-192(%rbp), %rdi
	movq	-184(%rbp), %r12
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L5574
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3422:
	movl	$2, (%rax)
	movq	48(%r12), %rdx
	pxor	%xmm0, %xmm0
	leaq	8(%rax), %rcx
	movq	%r13, %xmm6
	movq	%rdx, 48(%rax)
	movq	8(%r12), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movdqu	24(%r12), %xmm4
	movups	%xmm4, 24(%rax)
	movq	40(%r12), %rdx
	movq	%rdx, 40(%rax)
	movups	%xmm0, 8(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
	movq	%rbx, %xmm0
	movl	$1, (%r12)
	punpcklqdq	%xmm6, %xmm0
	movq	$0, 48(%r12)
	movq	$0, 40(%r12)
	movq	-1392(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movups	%xmm0, -32(%r9)
	movq	-176(%rbp), %rdx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	movq	%rax, 40(%rdx)
	jmp	.L3419
.L4257:
	leaq	.LC7(%rip), %r9
	jmp	.L4267
.L4243:
	leaq	.LC7(%rip), %rdx
	jmp	.L4253
.L3436:
	movq	-208(%rbp), %rcx
	xorl	%ebx, %ebx
	movq	(%rcx), %r12
	testq	%r12, %r12
	je	.L3437
	movq	-112(%rbp), %rbx
	leaq	(%r12,%r12,2), %rcx
	salq	$3, %rcx
	subq	%rcx, %rbx
.L3437:
	movq	-264(%rbp), %r15
	testq	%r15, %r15
	jne	.L3438
	cmpb	$0, -120(%r8)
	je	.L5575
.L3438:
	cmpq	%r8, %rdx
	je	.L5576
.L4344:
	movq	%rdx, -80(%rbp)
	jmp	.L3125
.L4226:
	leaq	2(%rsi), %r8
	cmpq	%rax, %r8
	jb	.L5577
	leaq	.LC30(%rip), %rcx
	movq	%r8, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addl	$2, -1512(%rbp)
	movl	$0, -1568(%rbp)
	jmp	.L4224
.L3460:
	movb	$1, -256(%r8)
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
.L5385:
	movl	-1328(%rbp), %esi
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler16WasmGraphBuilder7RefFuncEj@PLT
	testq	%rax, %rax
	je	.L3749
	cmpl	$-1, -168(%rbp)
	je	.L3749
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3749:
	movq	%rax, -8(%rbx)
	movq	-264(%rbp), %r15
	jmp	.L3748
.L5567:
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movl	%r12d, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%r13, %r9
	movl	%ebx, %ecx
	movq	%r14, %rdi
	movq	-304(%rbp), %rsi
	movq	%rax, %r8
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rdx
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
.L3667:
	movq	-1520(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L3697
.L3972:
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rsi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movl	-132(%rcx), %esi
	cmpq	%rsi, %rax
	ja	.L5578
	movq	$0, -1544(%rbp)
	cmpb	$2, -120(%rcx)
	jne	.L5579
.L4339:
	movq	8(%r13), %r12
	leaq	-520(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	%rax, %xmm0
	movq	%rax, -1440(%rbp)
	leaq	-328(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%r12d, %r15
	movq	%rax, -528(%rbp)
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %r15
	jbe	.L3980
	movq	%r15, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
.L3980:
	movq	-544(%rbp), %rax
	leaq	(%r15,%r15,2), %rdx
	movl	%r12d, %r8d
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, -536(%rbp)
	subl	$1, %r8d
	js	.L3981
	movslq	%r8d, %r11
	leaq	-296(%rbp), %rdi
	movq	%rbx, -1560(%rbp)
	movq	%r14, -1512(%rbp)
	movq	%r11, %rbx
	movl	%r8d, %r14d
	movq	%rdi, -1528(%rbp)
	movq	%r13, -1496(%rbp)
	jmp	.L3988
	.p2align 4,,10
	.p2align 3
.L5581:
	cmpb	$2, -120(%rcx)
	movq	-304(%rbp), %r10
	jne	.L5580
.L3983:
	xorl	%r13d, %r13d
	movl	$10, %r15d
.L3985:
	subl	$1, %r14d
	movq	%r10, (%r12)
	subq	$1, %rbx
	movb	%r15b, 8(%r12)
	movq	%r13, 16(%r12)
	cmpl	$-1, %r14d
	je	.L5338
	movq	-544(%rbp), %rax
.L3988:
	leaq	(%rbx,%rbx,2), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rdi
	leaq	(%rax,%rdx,8), %r12
	movq	-112(%rbp), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rsi, %rax
	jbe	.L5581
	movq	-1496(%rbp), %rcx
	movzbl	-16(%rdx), %r15d
	subq	$24, %rdx
	movq	(%rdx), %r10
	movq	16(%rdx), %r13
	movq	16(%rcx), %rax
	addq	%rbx, %rax
	addq	(%rcx), %rax
	movzbl	(%rax), %ecx
	movq	%rdx, -112(%rbp)
	cmpb	%r15b, %cl
	je	.L3985
	movzbl	%cl, %r9d
	movzbl	%r15b, %edi
	movq	%r10, -1488(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -1520(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-1488(%rbp), %r10
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r15b
	setne	%dl
	testb	%dl, %cl
	je	.L3985
	cmpb	$1, %al
	je	.L3985
	movq	%r10, -1568(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-296(%rbp), %rdx
	movq	-1568(%rbp), %r10
	movq	%rax, -1536(%rbp)
	leaq	.LC0(%rip), %rax
	movl	-1520(%rbp), %r9d
	cmpq	%rdx, %r10
	movq	%rax, -1488(%rbp)
	jnb	.L3986
	movq	-1528(%rbp), %rdi
	movq	%r10, %rsi
	movl	%r9d, -1568(%rbp)
	movq	%r10, -1520(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-296(%rbp), %rdx
	movl	-1568(%rbp), %r9d
	movq	%rax, -1488(%rbp)
	movq	-1520(%rbp), %r10
.L3986:
	movl	%r9d, %edi
	movq	%rdx, -1568(%rbp)
	movq	%r10, -1520(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	movq	-1568(%rbp), %rdx
	leaq	.LC0(%rip), %rcx
	movq	-1520(%rbp), %r10
	movq	%rax, %r9
	cmpq	%rdx, %rsi
	jnb	.L3987
	movq	-1528(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1568(%rbp), %r9
	movq	-1520(%rbp), %r10
	movq	%rax, %rcx
.L3987:
	pushq	-1536(%rbp)
	movq	%r10, %rsi
	xorl	%eax, %eax
	movl	%r14d, %r8d
	pushq	-1488(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1512(%rbp), %rdi
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rax
	movq	-1488(%rbp), %r10
	popq	%rdx
	jmp	.L3985
.L5580:
	leaq	.LC0(%rip), %rcx
	cmpq	%r10, -296(%rbp)
	jbe	.L3984
	movq	-1528(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r10
	movq	%rax, %rcx
.L3984:
	movq	-1512(%rbp), %rdi
	movq	%r10, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r10
	jmp	.L3983
.L5578:
	movq	-8(%rdx), %rax
	movzbl	-16(%rdx), %ecx
	subq	$24, %rdx
	movq	(%rdx), %r12
	movq	%rdx, -112(%rbp)
	movq	%rax, -1544(%rbp)
	cmpb	$1, %cl
	je	.L4339
	movzbl	%cl, %edi
	movl	$1, %esi
	movq	%r9, -1440(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %cl
	je	.L4339
	subb	$1, %al
	je	.L4339
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	cmpq	-296(%rbp), %r12
	movq	-1440(%rbp), %r9
	leaq	.LC0(%rip), %r13
	movq	%rax, %r15
	jnb	.L3977
	leaq	-296(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r9
	movq	%rax, %r13
.L3977:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r9
	jnb	.L3978
	leaq	-296(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L3978:
	pushq	%r15
	movq	%r12, %rsi
	leaq	.LC8(%rip), %r9
	xorl	%r8d, %r8d
	pushq	%r13
	leaq	.LC19(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	popq	%rsi
.L3976:
	movq	-1320(%rbp), %r13
	testq	%r13, %r13
	jne	.L4339
	leaq	-520(%rbp), %rdi
	leaq	-328(%rbp), %rax
	movq	%rdi, %xmm0
	movq	%rdi, -1440(%rbp)
	movq	%rax, -528(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -544(%rbp)
.L3981:
	cmpq	$0, -264(%rbp)
	jne	.L3989
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5582
.L3989:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
	movq	-544(%rbp), %rdi
	cmpq	-1440(%rbp), %rdi
	je	.L3963
	call	free@PLT
	jmp	.L3963
.L3458:
	movq	-304(%rbp), %rbx
	movq	%rax, -80(%rbp)
	movq	-264(%rbp), %r15
	movq	-296(%rbp), %rax
	leaq	1(%rbx), %r13
	jmp	.L3125
.L5517:
	movl	-1440(%rbp), %ecx
	leaq	.LC48(%rip), %rdx
	jmp	.L5346
.L4277:
	movq	(%r15), %rdi
	leaq	5(%rax,%rdx), %r8
	leaq	0(,%r8,8), %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L5583
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L4280:
	movq	%rdx, 96(%r15)
	movq	%r8, 104(%r15)
	jmp	.L4278
.L5338:
	movq	-1560(%rbp), %rbx
	movq	-1512(%rbp), %r14
	jmp	.L3981
.L4115:
	leaq	.LC0(%rip), %rcx
	jmp	.L4113
.L3362:
	leaq	-128(%rbp), %r15
	leaq	-104(%rbx), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L3363
.L5541:
	movq	-544(%rbp), %r15
	movq	-536(%rbp), %rbx
	movabsq	$-6148914691236517205, %rax
	movq	-192(%rbp), %rdi
	subq	%r15, %rbx
	sarq	$3, %rbx
	imulq	%rax, %rbx
	movslq	%ebx, %rax
	cmpq	$268435455, %rax
	ja	.L3636
	testq	%rax, %rax
	je	.L4374
	leaq	0(,%rax,8), %r13
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	%r13, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %r13
	ja	.L5584
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L3209:
	movq	%rcx, %r12
	movq	%rcx, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	%r12, %rcx
	leaq	(%r12,%r13), %r8
	testl	%ebx, %ebx
	jle	.L3207
	movl	%ebx, %edx
	leaq	16(%r15), %rax
	leal	-1(%rbx), %esi
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%r12,%rdx,8), %rdx
	leaq	(%r15,%rdi,8), %rdi
	cmpq	%rdi, %r12
	setnb	%dil
	cmpq	%rdx, %rax
	setnb	%dl
	orb	%dl, %dil
	je	.L3210
	cmpl	$22, %esi
	jbe	.L3210
	movl	%ebx, %edx
	shrl	%edx
	salq	$4, %rdx
	addq	%r12, %rdx
.L3211:
	movq	(%rax), %xmm0
	addq	$16, %rcx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rdx, %rcx
	jne	.L3211
	movl	%ebx, %eax
	andl	$-2, %eax
	andl	$1, %ebx
	je	.L3207
	cltq
	leaq	(%rax,%rax,2), %rdx
	movq	16(%r15,%rdx,8), %rdx
	movq	%rdx, (%r12,%rax,8)
.L3207:
	movq	-1352(%rbp), %rdx
	movl	-1360(%rbp), %esi
	subq	%r12, %r8
	movq	%r12, %rcx
	movq	-176(%rbp), %rdi
	movq	-304(%rbp), %r9
	sarq	$3, %r8
	subl	-312(%rbp), %r9d
	call	_ZN2v88internal8compiler16WasmGraphBuilder5ThrowEjPKNS0_4wasm13WasmExceptionENS0_6VectorIPNS1_4NodeEEEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3215
	cmpl	$-1, -168(%rbp)
	je	.L3215
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3215:
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %rdi
	movq	8(%rax), %rdx
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder14TerminateThrowEPNS1_4NodeES4_@PLT
	jmp	.L3205
.L3175:
	leaq	-128(%rbp), %r15
	leaq	-104(%rbx), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L3176
.L5539:
	leaq	-184(%rbp), %r11
	movq	-32(%rbx), %rdx
	leaq	-72(%rbx), %rcx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0
	jmp	.L3429
.L5453:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r15
	jnb	.L3282
	leaq	-296(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r9
	movq	%rax, %rcx
.L3282:
	leaq	.LC18(%rip), %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r15
	jmp	.L3281
.L5530:
	movq	%rdx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rdx
	jmp	.L3349
.L5532:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3162
.L5502:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3170
.L5510:
	movq	%rdx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rdx
	jmp	.L3357
.L3300:
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L3306:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$24, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %rsi
	jne	.L3306
	jmp	.L3303
.L5490:
	leaq	.LC125(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-264(%rbp), %r15
	movl	$2, %esi
	jmp	.L4013
.L5559:
	movzbl	3(%rcx), %esi
	movl	$3, %ebx
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	jns	.L3491
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %rdi
	ja	.L5585
	leaq	.LC66(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$3, %ebx
	jmp	.L3501
.L3951:
	movq	8(%r11), %rbx
	leaq	-520(%rbp), %rcx
	leaq	-328(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -528(%rbp)
	leaq	-544(%rbp), %rdi
	movq	%rcx, %rax
	punpcklqdq	%xmm0, %xmm0
	movslq	%ebx, %r12
	movq	%rcx, -1440(%rbp)
	movaps	%xmm0, -544(%rbp)
	cmpq	$8, %r12
	ja	.L5586
.L4334:
	leaq	(%r12,%r12,2), %rdx
	movl	%ebx, %r8d
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, -536(%rbp)
	subl	$1, %r8d
	js	.L3953
	leaq	-296(%rbp), %rbx
	movslq	%r8d, %r10
	movq	%r14, -1512(%rbp)
	movl	%r8d, %r14d
	movq	%rbx, -1528(%rbp)
	movq	%r10, %rbx
	movl	%r13d, -1544(%rbp)
	movq	%r11, -1496(%rbp)
	jmp	.L3960
.L5588:
	cmpb	$2, -120(%rcx)
	movq	-304(%rbp), %r10
	jne	.L5587
.L3955:
	xorl	%r15d, %r15d
	movl	$10, %r13d
.L3957:
	subl	$1, %r14d
	movq	%r10, (%r12)
	subq	$1, %rbx
	movb	%r13b, 8(%r12)
	movq	%r15, 16(%r12)
	cmpl	$-1, %r14d
	je	.L5336
	movq	-544(%rbp), %rax
.L3960:
	leaq	(%rbx,%rbx,2), %rdx
	movq	-80(%rbp), %rcx
	movabsq	$-6148914691236517205, %rdi
	leaq	(%rax,%rdx,8), %r12
	movq	-112(%rbp), %rdx
	movl	-132(%rcx), %esi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rsi, %rax
	jbe	.L5588
	movq	-1496(%rbp), %rcx
	movzbl	-16(%rdx), %r13d
	subq	$24, %rdx
	movq	(%rdx), %r10
	movq	16(%rdx), %r15
	movq	16(%rcx), %rax
	addq	%rbx, %rax
	addq	(%rcx), %rax
	movzbl	(%rax), %ecx
	movq	%rdx, -112(%rbp)
	cmpb	%r13b, %cl
	je	.L3957
	movzbl	%cl, %r9d
	movzbl	%r13b, %edi
	movq	%r10, -1488(%rbp)
	movl	%r9d, %esi
	movl	%r9d, -1520(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movq	-1488(%rbp), %r10
	cmpb	$10, %cl
	setne	%cl
	cmpb	$10, %r13b
	setne	%dl
	testb	%dl, %cl
	je	.L3957
	cmpb	$1, %al
	je	.L3957
	movq	%r10, -1560(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-1560(%rbp), %r10
	cmpq	-296(%rbp), %r10
	movq	%rax, -1536(%rbp)
	leaq	.LC0(%rip), %rax
	movl	-1520(%rbp), %r9d
	movq	%rax, -1488(%rbp)
	jnb	.L3958
	movq	-1528(%rbp), %rdi
	movq	%r10, %rsi
	movl	%r9d, -1560(%rbp)
	movq	%r10, -1520(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movl	-1560(%rbp), %r9d
	movq	-1520(%rbp), %r10
	movq	%rax, -1488(%rbp)
.L3958:
	movl	%r9d, %edi
	movq	%r10, -1520(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	cmpq	-296(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	movq	-1520(%rbp), %r10
	movq	%rax, %r9
	jnb	.L3959
	movq	-1528(%rbp), %rdi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1560(%rbp), %r9
	movq	-1520(%rbp), %r10
	movq	%rax, %rcx
.L3959:
	pushq	-1536(%rbp)
	movq	%r10, %rsi
	movl	%r14d, %r8d
	xorl	%eax, %eax
	pushq	-1488(%rbp)
	leaq	.LC19(%rip), %rdx
	movq	-1512(%rbp), %rdi
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%r10
	movq	-1488(%rbp), %r10
	popq	%r11
	jmp	.L3957
.L5587:
	leaq	.LC0(%rip), %rcx
	cmpq	%r10, -296(%rbp)
	jbe	.L3956
	movq	-1528(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r10
	movq	%rax, %rcx
.L3956:
	movq	-1512(%rbp), %rdi
	movq	%r10, %rsi
	leaq	.LC18(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-304(%rbp), %r10
	jmp	.L3955
.L5336:
	movl	-1544(%rbp), %r13d
	movq	-1512(%rbp), %r14
.L3953:
	cmpq	$0, -264(%rbp)
	jne	.L3961
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L5589
.L3961:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
	movq	-544(%rbp), %rdi
	cmpq	-1440(%rbp), %rdi
	je	.L3947
	call	free@PLT
	jmp	.L3947
.L3868:
	movq	(%rcx), %rdi
	leaq	5(%rax,%rdx), %r8
	leaq	0(,%r8,8), %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L5590
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L3871:
	movq	%rdx, 96(%rcx)
	movq	%r8, 104(%rcx)
	jmp	.L3869
.L5577:
	movzbl	2(%rsi), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, -1568(%rbp)
	testb	%cl, %cl
	js	.L4228
	addl	$3, -1512(%rbp)
	jmp	.L4224
.L3919:
	movq	(%rcx), %rdi
	leaq	5(%rax,%rsi), %r8
	leaq	0(,%r8,8), %rsi
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	subq	%r10, %rax
	cmpq	%rax, %rsi
	ja	.L5591
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L3922:
	movq	%r10, 96(%rcx)
	movq	%r8, 104(%rcx)
	jmp	.L3920
.L4064:
	movzbl	-16(%rdx), %eax
	movq	-24(%rdx), %r13
	subq	$24, %rdx
	movq	16(%rdx), %r12
	movq	%rdx, -112(%rbp)
	cmpb	$10, %al
	je	.L4067
	cmpb	$5, %al
	je	.L4067
	movzbl	%al, %edi
	leaq	.LC0(%rip), %r15
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%rax, %rbx
	cmpq	-296(%rbp), %r13
	jnb	.L4068
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %r15
.L4068:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L4069
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4069:
	pushq	%rbx
	leaq	.LC15(%rip), %r9
	movl	$1, %r8d
	xorl	%eax, %eax
	pushq	%r15
	leaq	.LC19(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-80(%rbp), %r8
	popq	%r9
	popq	%r10
.L4067:
	movabsq	$-6148914691236517205, %rcx
	movq	-112(%rbp), %rax
	movq	%rax, %rdx
	subq	-120(%rbp), %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	movl	-132(%r8), %ecx
	cmpq	%rcx, %rdx
	jbe	.L5592
	movzbl	-16(%rax), %edx
	movq	-24(%rax), %r13
	subq	$24, %rax
	movq	16(%rax), %rbx
	movq	%rax, -112(%rbp)
	cmpb	$10, %dl
	je	.L4065
	cmpb	$5, %dl
	je	.L4065
	movzbl	%dl, %edi
	leaq	.LC0(%rip), %r15
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%rax, -1440(%rbp)
	cmpq	-296(%rbp), %r13
	jnb	.L4072
	leaq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %r15
.L4072:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L4073
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, %rcx
.L4073:
	pushq	-1440(%rbp)
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	pushq	%r15
	leaq	.LC15(%rip), %r9
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rdi
	popq	%r8
	jmp	.L4065
.L4284:
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L4290:
	movq	(%rax), %rdi
	addq	$8, %rcx
	addq	$24, %rax
	movq	%rdi, -8(%rcx)
	cmpq	%rcx, %rsi
	jne	.L4290
	jmp	.L4287
.L5569:
	leaq	-1360(%rbp), %r12
	movq	%r10, %rdx
	movl	%r9d, %r8d
	movl	%r11d, %esi
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler16WasmGraphBuilder10CallDirectEjPPNS1_4NodeEPS5_i@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3934
	cmpl	$-1, -168(%rbp)
	je	.L3934
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	jmp	.L3934
.L5570:
	movq	16(%rax), %rax
	leaq	-128(%rbp), %r15
	leaq	-1328(%rbp), %rdx
	leaq	-304(%rbp), %rsi
	movq	%r15, %rdi
	movzbl	(%rax), %eax
	movb	%al, -1328(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueENS1_13ZoneAllocatorIS5_EEE12emplace_backIJRPKhRNS2_9ValueTypeEEEEvDpOT_
	movq	-112(%rbp), %rax
	leaq	-24(%rax), %r10
	jmp	.L4117
.L5491:
	movq	%rbx, %rsi
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-544(%rbp), %rdx
	jmp	.L4076
.L4400:
	movq	%xmm0, %rdx
	jmp	.L3339
.L5494:
	movq	-304(%rbp), %rax
	leaq	.LC124(%rip), %rdx
	movq	%r14, %rdi
	leaq	2(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-264(%rbp), %r15
	jmp	.L4063
.L5520:
	movzbl	16(%rcx), %eax
	movb	%al, 0(%r13,%r12)
	addq	$1, %r12
	cmpl	%r12d, %r8d
	jg	.L3640
	jmp	.L3637
.L3558:
	leaq	.LC7(%rip), %r9
	jmp	.L3569
.L3571:
	leaq	.LC7(%rip), %r8
	jmp	.L3582
.L4061:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movzbl	-754(%rbp), %edx
	xorl	%ecx, %ecx
	jmp	.L4060
.L4058:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4057
.L4055:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4054
.L5454:
	movl	$1, %esi
	cmpl	$2, %eax
	je	.L3276
	leaq	-1360(%rbp), %r12
	jmp	.L3308
.L4052:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4051
.L4049:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4048
.L4046:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4045
.L4043:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4042
.L4040:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4039
.L4037:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4036
.L4034:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4033
.L4031:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4030
.L4374:
	xorl	%r12d, %r12d
	xorl	%r8d, %r8d
	jmp	.L3207
.L4016:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4015
.L4028:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4027
.L4025:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4024
.L4022:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4021
.L4019:
	leaq	.LC123(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	movq	-296(%rbp), %rax
	xorl	%edx, %edx
	jmp	.L4018
.L5566:
	movq	-304(%rbp), %rsi
	movq	-128(%r8), %r8
	movq	%r14, %rdi
	xorl	%eax, %eax
	subl	-312(%rbp), %r8d
	leaq	.LC101(%rip), %rdx
	movl	$1, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3664
.L3885:
	leaq	8(%rax,%rcx,8), %rsi
.L3891:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$24, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %rsi
	jne	.L3891
	jmp	.L3888
.L3875:
	movq	%rax, %rcx
	leaq	16(%rdx,%rdi,8), %rdi
	movq	%rsi, %rax
.L3881:
	movq	(%rcx), %rsi
	addq	$8, %rax
	addq	$24, %rcx
	movq	%rsi, -8(%rax)
	cmpq	%rax, %rdi
	jne	.L3881
	jmp	.L3878
.L3939:
	leaq	8(%rax,%rcx,8), %rsi
.L3945:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$24, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %rsi
	jne	.L3945
	jmp	.L3942
.L3926:
	leaq	16(%r10,%r8,8), %rsi
.L3932:
	movq	(%rax), %rdx
	addq	$8, %rcx
	addq	$24, %rax
	movq	%rdx, -8(%rcx)
	cmpq	%rcx, %rsi
	jne	.L3932
	jmp	.L3929
.L5576:
	movq	%r13, -304(%rbp)
	jmp	.L4298
.L4082:
	movzbl	1(%rsi), %edi
	sall	$8, %ebx
	orl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L4080
.L5562:
	movl	%ebx, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L4080
.L5550:
	movq	-24(%rbx), %rax
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	leaq	8(%rax), %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	leaq	-72(%rbx), %rcx
	movq	%rax, 40(%rdx)
	movq	-32(%rbx), %rdx
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0
	movq	-176(%rbp), %rdx
	jmp	.L3455
.L5549:
	leaq	-184(%rbp), %r11
	movq	-32(%rbx), %rdx
	leaq	-72(%rbx), %rcx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface15MergeValuesIntoEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS3_7ControlEPNS1_5MergeINS3_5ValueEEE.isra.0
	movzbl	-136(%rbx), %eax
	jmp	.L3454
.L5551:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3252
.L5524:
	movq	%r9, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %r9
	jmp	.L3260
.L3715:
	movq	%r8, %rdx
.L3720:
	movq	(%rax), %rdi
	addq	$8, %rdx
	addq	$24, %rax
	movq	%rdi, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L3720
	jmp	.L3709
.L4228:
	leaq	3(%rsi), %r8
	cmpq	%rax, %r8
	jb	.L5593
	leaq	.LC30(%rip), %rcx
	movq	%r8, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addl	$3, -1512(%rbp)
	movl	$0, -1568(%rbp)
	jmp	.L4224
.L5573:
	movb	$1, -120(%rbx)
	movq	-304(%rbp), %rbx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	jmp	.L3125
.L5571:
	movq	-536(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movabsq	$-6148914691236517205, %rax
	movq	%rcx, %rbx
	subq	%r13, %rbx
	sarq	$3, %rbx
	imulq	%rax, %rbx
	movq	104(%rdx), %rax
	cmpq	%rax, %rbx
	ja	.L5594
.L4119:
	movq	96(%rdx), %rdx
	testq	%rbx, %rbx
	je	.L4128
	leaq	(%rdx,%rbx,8), %rsi
	leaq	16(%r13), %rax
	cmpq	%rsi, %rax
	setnb	%sil
	cmpq	%rdx, %rcx
	setbe	%cl
	orb	%cl, %sil
	je	.L5067
	leaq	-1(%rbx), %rcx
	cmpq	$22, %rcx
	jbe	.L5067
	movq	%rbx, %rsi
	movq	%rdx, %rcx
	shrq	%rsi
	salq	$4, %rsi
	addq	%rdx, %rsi
.L4127:
	movq	(%rax), %xmm0
	addq	$16, %rcx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rcx, %rsi
	jne	.L4127
	movq	%rbx, %rax
	andq	$-2, %rax
	andb	$1, %bl
	je	.L4128
	leaq	(%rax,%rax,2), %rcx
	movq	16(%r13,%rcx,8), %rcx
	movq	%rcx, (%rdx,%rax,8)
.L4128:
	movq	-176(%rbp), %rdi
	movl	%r12d, %esi
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder6SimdOpENS0_4wasm10WasmOpcodeEPKPNS1_4NodeE@PLT
	movq	-1488(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L4123
	cmpl	$-1, -168(%rbp)
	je	.L4123
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r10, -1488(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	-1488(%rbp), %r10
	movq	%rax, %rdx
.L4123:
	testq	%r10, %r10
	je	.L4131
	movq	%rdx, 16(%r10)
.L4131:
	movq	-544(%rbp), %r13
	jmp	.L4118
.L5067:
	imulq	$24, %r15, %rax
	movq	16(%r13,%rax), %rax
	movq	%rax, (%rdx,%r15,8)
	addq	$1, %r15
	cmpq	%r15, %rbx
	jne	.L5067
	jmp	.L4128
.L5561:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %r9
	jmp	.L3264
.L5575:
	movq	-176(%rbp), %r13
	movq	104(%r13), %rax
	cmpq	%rax, %r12
	ja	.L5595
.L3439:
	movq	96(%r13), %rsi
	testq	%r12, %r12
	je	.L3448
	imulq	$24, %r12, %rdx
	leaq	16(%rbx), %rax
	addq	%rbx, %rdx
	cmpq	%rdx, %rsi
	leaq	(%rsi,%r12,8), %rdx
	setnb	%cl
	cmpq	%rdx, %rax
	setnb	%dl
	orb	%dl, %cl
	je	.L5066
	leaq	-1(%r12), %rdx
	cmpq	$22, %rdx
	jbe	.L5066
	movq	%r12, %rcx
	movq	%rsi, %rdx
	shrq	%rcx
	salq	$4, %rcx
	addq	%rsi, %rcx
.L3447:
	movq	(%rax), %xmm0
	addq	$16, %rdx
	addq	$48, %rax
	movhps	-24(%rax), %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rcx
	jne	.L3447
	movq	%r12, %rax
	andq	$-2, %rax
	testb	$1, %r12b
	je	.L3448
	leaq	(%rax,%rax,2), %rdx
	movq	16(%rbx,%rdx,8), %rdx
	movq	%rdx, (%rsi,%rax,8)
.L3448:
	movq	-176(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler16WasmGraphBuilder6ReturnENS0_6VectorIPNS1_4NodeEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3443
	cmpl	$-1, -168(%rbp)
	je	.L3443
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
.L3443:
	movq	-304(%rbp), %rbx
	movq	-88(%rbp), %rdx
	movq	-296(%rbp), %rax
	movq	-264(%rbp), %r15
	leaq	1(%rbx), %r13
	cmpq	-80(%rbp), %rdx
	jne	.L4344
	jmp	.L3125
.L5066:
	imulq	$24, %r15, %rax
	movq	16(%rbx,%rax), %rax
	movq	%rax, (%rsi,%r15,8)
	addq	$1, %r15
	cmpq	%r15, %r12
	jne	.L5066
	jmp	.L3448
.L5565:
	testl	%ecx, %ecx
	jle	.L3661
	leal	-1(%rcx), %eax
	movq	%r15, -1568(%rbp)
	movl	$1, %r13d
	leaq	-296(%rbp), %rbx
	addq	$2, %rax
	movq	%rax, -1528(%rbp)
	jmp	.L3662
.L5597:
	cmpb	$2, -120(%r8)
	jne	.L5596
.L3673:
	addq	$1, %r13
	cmpq	%r13, -1528(%rbp)
	je	.L3669
	movq	-80(%rbp), %r8
.L3662:
	movq	-112(%rbp), %rdx
	movl	-132(%r8), %ecx
	movl	%r13d, %r15d
	movabsq	$-6148914691236517205, %rdi
	movq	%rdx, %rax
	subq	-120(%rbp), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rcx, %rax
	jbe	.L5597
	movq	-1520(%rbp), %rax
	movzbl	-16(%rdx), %ecx
	subq	$24, %rdx
	movq	(%rdx), %r12
	movq	%rdx, -112(%rbp)
	movzbl	-1(%rax,%r13), %r8d
	cmpb	%cl, %r8b
	je	.L3673
	movzbl	%r8b, %eax
	movzbl	%cl, %edi
	movl	%eax, %esi
	movl	%eax, -1544(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	cmpb	$10, %r8b
	setne	%sil
	cmpb	$10, %cl
	setne	%dl
	testb	%dl, %sil
	je	.L3673
	subb	$1, %al
	je	.L3673
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%rax, -1560(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, -1536(%rbp)
	cmpq	-296(%rbp), %r12
	jnb	.L3674
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	%rax, -1536(%rbp)
.L3674:
	movl	-1544(%rbp), %edi
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	movq	%rax, %r9
	cmpq	-296(%rbp), %rsi
	jnb	.L3675
	movq	%rbx, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-1544(%rbp), %r9
	movq	%rax, %rcx
.L3675:
	pushq	-1560(%rbp)
	movq	%r12, %rsi
	movl	%r15d, %r8d
	movq	%r14, %rdi
	pushq	-1536(%rbp)
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	popq	%rcx
	popq	%rsi
	jmp	.L3673
.L5596:
	movq	-304(%rbp), %rsi
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %rsi
	jnb	.L3672
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %rsi
	movq	%rax, %rcx
.L3672:
	leaq	.LC18(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3673
.L5586:
	movq	%r12, %rsi
	movq	%r11, -1488(%rbp)
	call	_ZN2v84base11SmallVectorINS_8internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5ValueELm8EE4GrowEm
	movq	-544(%rbp), %rax
	movq	-1488(%rbp), %r11
	jmp	.L4334
.L5555:
	movq	%rdx, -1544(%rbp)
	movq	%r10, -1536(%rbp)
	movb	%cl, -1520(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movzbl	-1520(%rbp), %ecx
	movq	-1528(%rbp), %rsi
	movq	%rax, %r8
	movq	-1536(%rbp), %r10
	movq	-296(%rbp), %rax
	movq	-1544(%rbp), %rdx
	jmp	.L4098
.L5556:
	movq	%r9, -1560(%rbp)
	movq	%rdx, -1544(%rbp)
	movq	%r8, -1536(%rbp)
	movq	%r10, -1528(%rbp)
	movq	%rsi, -1520(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-1520(%rbp), %rsi
	movq	-1528(%rbp), %r10
	movq	-1536(%rbp), %r8
	movq	-1544(%rbp), %rdx
	movq	%rax, %rcx
	movq	-1560(%rbp), %r9
	jmp	.L4113
.L5557:
	movq	%r9, -1488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1488(%rbp), %r9
	jmp	.L3409
.L5537:
	movq	%r9, -1488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1488(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L3417
.L5496:
	movq	%r12, %xmm4
	movq	-176(%rbp), %rdi
	movq	%rbx, %xmm0
	leaq	-544(%rbp), %rdx
	punpcklqdq	%xmm4, %xmm0
	leaq	-768(%rbp), %rsi
	movaps	%xmm0, -544(%rbp)
	call	_ZN2v88internal8compiler16WasmGraphBuilder17Simd8x16ShuffleOpEPKhPKPNS1_4NodeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4074
	cmpl	$-1, -168(%rbp)
	je	.L4074
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %rdx
.L4074:
	movq	%rdx, -8(%r13)
	movq	-264(%rbp), %r15
	jmp	.L4063
.L5592:
	cmpb	$2, -120(%r8)
	je	.L4515
	movq	-304(%rbp), %r9
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r9
	jnb	.L4071
	movq	%r9, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r9
	movq	%rax, %rcx
.L4071:
	leaq	.LC18(%rip), %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	xorl	%ebx, %ebx
	jmp	.L4065
.L5495:
	movq	-304(%rbp), %r9
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r9
	jnb	.L4066
	movq	%r9, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r9
	movq	%rax, %rcx
.L4066:
	leaq	.LC18(%rip), %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	xorl	%r12d, %r12d
	movq	-80(%rbp), %r8
	jmp	.L4067
.L5579:
	leaq	.LC0(%rip), %rcx
	cmpq	-296(%rbp), %r9
	jnb	.L3975
	movq	%r9, %rsi
	leaq	-296(%rbp), %rdi
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE16SafeOpcodeNameAtEPKh.isra.0.part.0
	movq	-304(%rbp), %r9
	movq	%rax, %rcx
.L3975:
	leaq	.LC18(%rip), %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	$0, -1544(%rbp)
	jmp	.L3976
.L4351:
	movq	%rsi, %rdx
.L3599:
	movq	(%rax), %rdi
	addq	$8, %rdx
	addq	$24, %rax
	movq	%rdi, -8(%rdx)
	cmpq	%rdx, %rcx
	jne	.L3599
	jmp	.L3596
.L5582:
	pushq	%r12
	leaq	-184(%rbp), %r11
	movl	-1324(%rbp), %r9d
	movq	%r14, %rsi
	pushq	-544(%rbp)
	movq	-1320(%rbp), %r8
	movq	%r11, %rdi
	movq	-1544(%rbp), %rcx
	movl	-1328(%rbp), %edx
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface12DoReturnCallEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEjPNS0_8compiler4NodeEPNS0_9SignatureINS1_9ValueTypeEEEjPKNS3_5ValueE
	popq	%r13
	popq	%r15
	jmp	.L3989
.L4586:
	movl	$2, %ecx
	movl	$65102, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4574:
	movl	$3, %ecx
	movl	$65090, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4536:
	movl	$1, %ecx
	movl	$65045, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4577:
	xorl	%ecx, %ecx
	movl	$65093, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L3210:
	leaq	8(%rcx,%rsi,8), %rsi
.L3214:
	movq	(%rax), %rdx
	addq	$8, %rcx
	addq	$24, %rax
	movq	%rdx, -8(%rcx)
	cmpq	%rcx, %rsi
	jne	.L3214
	jmp	.L3207
.L5366:
	call	__stack_chk_fail@PLT
.L3669:
	cmpq	$0, -264(%rbp)
	movq	-1568(%rbp), %r15
	jne	.L4337
	movq	-80(%rbp), %rax
	cmpb	$0, -120(%rax)
	je	.L3677
.L3692:
	movq	-1328(%rbp), %rdi
	leaq	-1360(%rbp), %rbx
	cmpq	$0, 56(%rdi)
	jne	.L3679
.L3678:
	movl	-1304(%rbp), %eax
	cmpl	-1300(%rbp), %eax
	ja	.L3679
	movq	-1312(%rbp), %rsi
	addl	$1, %eax
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	leaq	.LC32(%rip), %rcx
	movl	%eax, -1304(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-1328(%rbp), %rdi
	movl	-1360(%rbp), %eax
	addq	%rax, -1312(%rbp)
	cmpq	$0, 56(%rdi)
	je	.L3678
.L3679:
	movq	-1312(%rbp), %r13
	movq	%r14, %rdi
	subq	-1320(%rbp), %r13
	addl	$1, %r13d
	call	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE10EndControlEv
	jmp	.L3664
.L4620:
	cmpb	$6, %r9b
	sete	%al
	cmpb	$8, %sil
	sete	%dl
	testb	%al, %al
	je	.L4621
	testb	%dl, %dl
	je	.L4621
.L4454:
	movl	$8, %esi
	jmp	.L3649
.L4528:
	movl	$2, %ecx
	movl	$65024, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L5593:
	movzbl	3(%rsi), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, -1568(%rbp)
	testb	%cl, %cl
	js	.L4230
	addl	$4, -1512(%rbp)
	jmp	.L4224
.L4534:
	movl	$1, %ecx
	movl	$65043, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4538:
	movl	$2, %ecx
	movl	$65054, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4575:
	xorl	%ecx, %ecx
	movl	$65091, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4578:
	movl	$1, %ecx
	movl	$65094, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4561:
	xorl	%ecx, %ecx
	movl	$65077, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4573:
	movl	$2, %ecx
	movl	$65089, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4559:
	movl	$2, %ecx
	movl	$65075, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4572:
	movl	$2, %ecx
	movl	$65088, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4543:
	movl	$1, %ecx
	movl	$65059, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4584:
	xorl	%ecx, %ecx
	movl	$65100, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4579:
	movl	$2, %ecx
	movl	$65095, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4580:
	movl	$2, %ecx
	movl	$65096, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4555:
	movl	$1, %ecx
	movl	$65071, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4582:
	xorl	%ecx, %ecx
	movl	$65098, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4557:
	movl	$1, %ecx
	movl	$65073, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4581:
	movl	$3, %ecx
	movl	$65097, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4531:
	movl	$2, %ecx
	movl	$65040, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4570:
	xorl	%ecx, %ecx
	movl	$65086, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4569:
	movl	$1, %ecx
	movl	$65085, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4568:
	xorl	%ecx, %ecx
	movl	$65084, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4551:
	movl	$2, %ecx
	movl	$65067, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4566:
	movl	$2, %ecx
	movl	$65082, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4567:
	movl	$3, %ecx
	movl	$65083, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4564:
	movl	$1, %ecx
	movl	$65080, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4547:
	xorl	%ecx, %ecx
	movl	$65063, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4562:
	movl	$1, %ecx
	movl	$65078, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4565:
	movl	$2, %ecx
	movl	$65081, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4560:
	movl	$3, %ecx
	movl	$65076, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4541:
	movl	$3, %ecx
	movl	$65055, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4558:
	movl	$2, %ecx
	movl	$65074, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4563:
	xorl	%ecx, %ecx
	movl	$65079, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4556:
	xorl	%ecx, %ecx
	movl	$65072, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4535:
	xorl	%ecx, %ecx
	movl	$65044, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4554:
	xorl	%ecx, %ecx
	movl	$65070, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4553:
	movl	$3, %ecx
	movl	$65069, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4552:
	movl	$2, %ecx
	movl	$65068, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4549:
	xorl	%ecx, %ecx
	movl	$65065, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4550:
	movl	$1, %ecx
	movl	$65066, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4545:
	movl	$2, %ecx
	movl	$65061, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4548:
	movl	$1, %ecx
	movl	$65064, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4540:
	movl	$1, %ecx
	movl	$65057, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4546:
	movl	$3, %ecx
	movl	$65062, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4571:
	movl	$1, %ecx
	movl	$65087, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4544:
	movl	$2, %ecx
	movl	$65060, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4537:
	movl	$2, %ecx
	movl	$65046, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4542:
	xorl	%ecx, %ecx
	movl	$65058, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4533:
	xorl	%ecx, %ecx
	movl	$65042, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4539:
	xorl	%ecx, %ecx
	movl	$65056, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4530:
	movl	$3, %ecx
	movl	$65026, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L3677:
	cmpl	$0, -1440(%rbp)
	je	.L5598
	movl	-1440(%rbp), %eax
	movq	-1496(%rbp), %rdx
	movq	-176(%rbp), %rdi
	movq	-184(%rbp), %rbx
	leal	1(%rax), %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder6SwitchEjPNS1_4NodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3682
	cmpl	$-1, -168(%rbp)
	je	.L3682
	leaq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %r13
.L3682:
	movq	-192(%rbp), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L5599
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdi)
.L3684:
	movl	$2, (%r12)
	movq	48(%rbx), %rax
	leaq	24(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movl	$6, %ecx
	movq	%rdi, -1536(%rbp)
	leaq	8(%r12), %rdx
	movq	%rax, 48(%r12)
	movq	8(%rbx), %rax
	movq	%r15, -1568(%rbp)
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rbx, -1560(%rbp)
	movq	%rax, 16(%r12)
	movdqu	24(%rbx), %xmm4
	movups	%xmm4, 24(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	leaq	8(%rbx), %rax
	movq	%rax, -1528(%rbp)
	xorl	%eax, %eax
	movl	$1, (%rbx)
	movq	$0, 48(%rbx)
	movups	%xmm0, 8(%rbx)
	rep stosl
	movq	%r12, -184(%rbp)
	movq	-176(%rbp), %rax
	movq	%rdx, 24(%rax)
	movq	-176(%rbp), %rax
	leaq	16(%r12), %rdx
	movq	%rdx, 32(%rax)
	movq	-176(%rbp), %rax
	leaq	24(%r12), %rdx
	movl	%ecx, %ebx
	movq	%rdx, 40(%rax)
	leaq	-1360(%rbp), %rax
	movq	%rax, -1544(%rbp)
.L3691:
	cmpq	$0, -264(%rbp)
	jne	.L3685
	cmpl	%ebx, -1440(%rbp)
	jb	.L3685
	movq	-1488(%rbp), %r15
	movq	-1544(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	.LC32(%rip), %rcx
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%eax, -1496(%rbp)
	movl	-1360(%rbp), %eax
	addq	%rax, %r15
	movq	%r15, -1488(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface5SplitEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS2_6SsaEnvE.isra.0
	movq	-176(%rbp), %rdx
	leaq	8(%rax), %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, 24(%rdx)
	movq	-176(%rbp), %rdx
	leaq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, 32(%rdx)
	movq	-176(%rbp), %rdx
	movq	%rax, 40(%rdx)
	cmpl	%ebx, -1440(%rbp)
	je	.L5600
	movq	-176(%rbp), %rdi
	movq	%r13, %rdx
	movl	%ebx, %esi
	call	_ZN2v88internal8compiler16WasmGraphBuilder7IfValueEiPNS1_4NodeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3690
.L5353:
	cmpl	$-1, -168(%rbp)
	leaq	-184(%rbp), %r15
	je	.L3689
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface17CheckForExceptionEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEPNS0_8compiler4NodeE.part.0
	movq	%rax, %rdx
.L3689:
	movq	-184(%rbp), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	addl	$1, %ebx
	movq	%rdx, 8(%rax)
	movl	-1496(%rbp), %edx
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj
	jmp	.L3691
.L4585:
	movl	$1, %ecx
	movl	$65101, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4576:
	movl	$1, %ecx
	movl	$65092, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L5585:
	movzbl	4(%rcx), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%eax, %edx
	testb	%sil, %sil
	js	.L3497
	movl	%edx, %eax
	movl	$4, %ebx
	jmp	.L3491
.L4583:
	movl	$1, %ecx
	movl	$65099, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4532:
	movl	$3, %ecx
	movl	$65041, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L4529:
	movl	$2, %ecx
	movl	$65025, %r12d
	xorl	%r13d, %r13d
	jmp	.L4195
.L5519:
	movq	-1536(%rbp), %rsi
	movl	%edx, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rdi
	leaq	.LC98(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3630
.L4086:
	leaq	.LC7(%rip), %rdx
	jmp	.L4096
.L5574:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1488(%rbp), %r9
	jmp	.L3422
.L4621:
	cmpb	$7, %sil
	jne	.L4622
	testb	%al, %al
	je	.L4622
	movl	$7, %esi
	jmp	.L3649
.L4100:
	leaq	.LC7(%rip), %r9
	jmp	.L4110
.L3497:
	leaq	5(%rcx), %r13
	cmpq	%r13, %rdi
	ja	.L5601
	leaq	.LC66(%rip), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movl	$4, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3501
.L4230:
	leaq	4(%rsi), %r15
	cmpq	%rax, %r15
	jnb	.L4594
	movzbl	4(%rsi), %ebx
	testb	%bl, %bl
	js	.L4595
	movl	%ebx, %eax
	movl	$5, %r8d
	sall	$28, %eax
	orl	%eax, -1568(%rbp)
.L4232:
	addl	%r8d, -1512(%rbp)
	andb	$-16, %bl
	je	.L4224
	leaq	.LC27(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$0, -1568(%rbp)
	jmp	.L4224
.L3685:
	movq	-176(%rbp), %rax
	movq	-1560(%rbp), %rbx
	movq	-1528(%rbp), %rcx
	movq	-1568(%rbp), %r15
	movq	%rbx, -184(%rbp)
	addq	$16, %rbx
	movq	%rcx, 24(%rax)
	movq	-176(%rbp), %rax
	movq	%rbx, 32(%rax)
	movq	-1536(%rbp), %rbx
	movq	-176(%rbp), %rax
	movq	%rbx, 40(%rax)
.L3681:
	movabsq	$-1085102592571150095, %rbx
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	testl	%eax, %eax
	jle	.L3692
	leal	-1(%rax), %edi
	movq	-1512(%rbp), %r10
	movl	$1, %esi
	movq	%r15, %rcx
	movq	$-136, %rax
	jmp	.L3696
.L5602:
	movq	%rdx, %rcx
.L3696:
	movq	%rcx, %rdx
	movq	%rsi, %r8
	shrq	$6, %rdx
	salq	%cl, %r8
	testq	%r8, (%r10,%rdx,8)
	je	.L3693
	movq	-80(%rbp), %r8
	addq	%rax, %r8
	cmpb	$3, (%r8)
	leaq	24(%r8), %r9
	leaq	64(%r8), %rdx
	cmove	%r9, %rdx
	movb	$1, 32(%rdx)
.L3693:
	leaq	1(%rcx), %rdx
	subq	$136, %rax
	cmpq	%rcx, %rdi
	jne	.L5602
	jmp	.L3692
.L5395:
	movq	%rcx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L3712
.L4206:
	leaq	.LC129(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L4208
.L5583:
	movq	%r9, -1528(%rbp)
	movq	%r8, -1496(%rbp)
	movq	%rcx, -1488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1488(%rbp), %rcx
	movq	-1496(%rbp), %r8
	movq	-1528(%rbp), %r9
	movq	%rax, %rdx
	jmp	.L4280
.L5595:
	movq	0(%r13), %rdi
	leaq	5(%r12,%rax), %rcx
	leaq	0(,%rcx,8), %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L5603
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3441:
	movq	%rax, 96(%r13)
	movq	%rcx, 104(%r13)
	jmp	.L3439
.L4622:
	testb	%dil, %dil
	je	.L4623
	testb	%al, %al
	je	.L4623
	movl	$9, %esi
	jmp	.L3649
.L5546:
	leaq	6(%r15), %r8
	cmpq	%r8, %rax
	ja	.L5604
	leaq	.LC28(%rip), %rcx
	movq	%r8, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$4, -1512(%rbp)
.L4219:
	movl	-1512(%rbp), %ebx
	addq	$2, %rbx
	jmp	.L5349
.L5589:
	pushq	%rdi
	movl	-1328(%rbp), %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	-544(%rbp)
	movq	-1320(%rbp), %r8
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface12DoReturnCallEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEjPNS0_8compiler4NodeEPNS0_9SignatureINS1_9ValueTypeEEEjPKNS3_5ValueE
	popq	%r8
	popq	%r9
	jmp	.L3961
.L5594:
	movq	(%rdx), %rdi
	leaq	5(%rax,%rbx), %r8
	leaq	0(,%r8,8), %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%r9, %rsi
	ja	.L5605
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L4121:
	movq	%rax, 96(%rdx)
	movq	%r8, 104(%rdx)
	jmp	.L4119
.L5351:
	movq	%rcx, %rsi
	leaq	.LC108(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L5540:
	movq	%rcx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rcx
	jmp	.L3591
.L4623:
	leal	-7(%r9), %eax
	testb	$-3, %al
	jne	.L4624
	testb	%dl, %dl
	jne	.L4454
.L4624:
	movzbl	%r9b, %edi
	call	_ZN2v88internal4wasm10ValueTypes9IsSubTypeENS1_9ValueTypeES3_.part.0
	movl	$8, %esi
	testb	%al, %al
	cmovne	%r9d, %esi
	jmp	.L3649
.L5591:
	movq	%r8, -1528(%rbp)
	movq	%rcx, -1520(%rbp)
	movq	%rdx, -1512(%rbp)
	movl	%r11d, -1488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-1488(%rbp), %r11d
	movq	-1512(%rbp), %rdx
	movq	-1520(%rbp), %rcx
	movq	-1528(%rbp), %r8
	movq	%rax, %r10
	jmp	.L3922
.L5604:
	movzbl	6(%r15), %ebx
	testb	%bl, %bl
	js	.L5606
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, -1520(%rbp)
	andb	$-16, %bl
	jne	.L4218
	movl	$5, -1512(%rbp)
	movl	$7, %ebx
	jmp	.L4212
.L5598:
	movq	-1488(%rbp), %rsi
	leaq	-1360(%rbp), %r12
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	leaq	.LC32(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	leaq	-184(%rbp), %r11
	movq	%r14, %rsi
	movl	%eax, %edx
	movq	%r11, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_126WasmGraphBuildingInterface7BrOrRetEPNS1_15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ES3_EEj
	jmp	.L3681
.L5584:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L3209
.L4448:
	movq	$0, -1528(%rbp)
	xorl	%r13d, %r13d
	jmp	.L3637
.L5477:
	leaq	.LC130(%rip), %rdx
	movq	%r14, %rdi
	movl	$2, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc.constprop.0
	jmp	.L4136
.L5590:
	movq	%r8, -1520(%rbp)
	movq	%rcx, -1512(%rbp)
	movl	%r9d, -1488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-1488(%rbp), %r9d
	movq	-1512(%rbp), %rcx
	movq	-1520(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L3871
.L5563:
	movq	-1536(%rbp), %rsi
	movl	%r13d, %ecx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC99(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L3630
.L3145:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5601:
	movzbl	5(%rcx), %ebx
	testb	%bl, %bl
	jns	.L3499
	xorl	%eax, %eax
	leaq	.LC66(%rip), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC26(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	andb	$-16, %bl
	je	.L5345
.L3500:
	leaq	.LC27(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L5345:
	movl	$5, %ebx
	jmp	.L3501
.L3499:
	movl	%ebx, %eax
	sall	$28, %eax
	orl	%edx, %eax
	andb	$-16, %bl
	jne	.L3500
	movl	$5, %ebx
	jmp	.L3491
.L3661:
	cmpq	$0, -264(%rbp)
	je	.L3692
	jmp	.L4337
.L5560:
	cmpq	$15790320, %rax
	movl	$15790320, %ecx
	cmova	%rcx, %rax
	imulq	$136, %rax, %r12
	movq	%r12, %rsi
.L3238:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L5607
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3241:
	leaq	(%rax,%r12), %rcx
	movq	%rax, %xmm0
	leaq	136(%rax), %r12
	jmp	.L3239
.L5607:
	movq	%r8, -1488(%rbp)
	movq	%rdx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rdx
	movq	-1488(%rbp), %r8
	jmp	.L3241
.L5548:
	cmpq	$15790320, %rax
	movl	$15790320, %ecx
	cmova	%rcx, %rax
	imulq	$136, %rax, %rbx
	movq	%rbx, %rsi
.L3148:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L5608
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3151:
	leaq	(%rax,%rbx), %rcx
	movq	%rax, %xmm0
	leaq	136(%rax), %rbx
	jmp	.L3149
.L5608:
	movq	%r8, -1488(%rbp)
	movq	%rdx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rdx
	movq	-1488(%rbp), %r8
	jmp	.L3151
.L4594:
	xorl	%ebx, %ebx
	movl	$4, %r8d
.L4231:
	leaq	.LC30(%rip), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdx
	movl	%r8d, -1440(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-1440(%rbp), %r8d
	movl	$0, -1568(%rbp)
	jmp	.L4232
.L5547:
	cmpq	$15790320, %rax
	movl	$15790320, %ecx
	cmova	%rcx, %rax
	imulq	$136, %rax, %rbx
	movq	%rbx, %rsi
.L3335:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L5609
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3338:
	leaq	(%rax,%rbx), %rcx
	movq	%rax, %xmm0
	leaq	136(%rax), %rbx
	jmp	.L3336
.L5609:
	movq	%r8, -1488(%rbp)
	movq	%rdx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rdx
	movq	-1488(%rbp), %r8
	jmp	.L3338
.L4218:
	leaq	.LC27(%rip), %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movl	$7, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$5, -1512(%rbp)
	jmp	.L5349
.L3636:
	leaq	.LC44(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5476:
	cmpq	$15790320, %rdx
	movl	$15790320, %eax
	cmova	%rax, %rdx
	imulq	$136, %rdx, %rdx
	movq	%rdx, %rsi
	jmp	.L2922
.L5600:
	movq	-176(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler16WasmGraphBuilder9IfDefaultEPNS1_4NodeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L5353
.L3690:
	xorl	%edx, %edx
	leaq	-184(%rbp), %r15
	jmp	.L3689
.L3334:
	movl	$2147483520, %esi
	movl	$2147483520, %ebx
	jmp	.L3335
.L5606:
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC28(%rip), %rcx
	movq	%r14, %rdi
	leaq	.LC26(%rip), %rdx
	movq	%r8, -1440(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	andb	$-16, %bl
	movq	-1440(%rbp), %r8
	jne	.L4218
	movl	$5, -1512(%rbp)
	jmp	.L4219
.L3147:
	movl	$2147483520, %esi
	movl	$2147483520, %ebx
	jmp	.L3148
.L4595:
	movl	$5, %r8d
	jmp	.L4231
.L5599:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L3684
.L5367:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L5605:
	movq	%r8, -1520(%rbp)
	movq	%rdx, -1512(%rbp)
	movq	%rcx, -1496(%rbp)
	movq	%r10, -1488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1488(%rbp), %r10
	movq	-1496(%rbp), %rcx
	movq	-1512(%rbp), %rdx
	movq	-1520(%rbp), %r8
	jmp	.L4121
.L5603:
	movq	%rcx, -1440(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1440(%rbp), %rcx
	jmp	.L3441
.L4610:
	movq	-1520(%rbp), %r15
	jmp	.L3283
.L4515:
	xorl	%ebx, %ebx
	jmp	.L4065
.L3855:
	movq	-1320(%rbp), %rax
	movq	%rax, -1488(%rbp)
	jmp	.L3863
.L3906:
	movq	-1320(%rbp), %rax
	movq	%rax, -1488(%rbp)
	jmp	.L3914
.L5370:
	movq	%rdi, %rax
	jmp	.L4315
.L3237:
	movl	$2147483520, %esi
	movl	$2147483520, %r12d
	jmp	.L3238
.L3394:
	movl	$2147483520, %esi
	movl	$2147483520, %r9d
.L3395:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L5610
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3398:
	leaq	(%rax,%r9), %rcx
	movq	%rax, %xmm0
	leaq	136(%rax), %r9
	jmp	.L3396
.L5610:
	movq	%rdx, -1512(%rbp)
	movq	%r9, -1496(%rbp)
	movq	%r8, -1488(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1488(%rbp), %r8
	movq	-1496(%rbp), %r9
	movq	-1512(%rbp), %rdx
	jmp	.L3398
.L5572:
	cmpq	$15790320, %rax
	movl	$15790320, %ecx
	cmova	%rcx, %rax
	imulq	$136, %rax, %r9
	movq	%r9, %rsi
	jmp	.L3395
	.cfi_endproc
.LFE19766:
	.size	_ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE, .-_ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE:
.LFB24583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24583:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE, .-_GLOBAL__sub_I__ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm12BuildTFGraphEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPNS0_8compiler16WasmGraphBuilderEPS4_RKNS1_12FunctionBodyEPNSA_15NodeOriginTableE
	.section	.rodata.CSWTCH.1121,"a"
	.align 8
	.type	CSWTCH.1121, @object
	.size	CSWTCH.1121, 10
CSWTCH.1121:
	.byte	0
	.byte	4
	.byte	5
	.byte	12
	.byte	13
	.byte	14
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal4wasm7DecoderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm7DecoderE,"awG",@progbits,_ZTVN2v88internal4wasm7DecoderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm7DecoderE, @object
	.size	_ZTVN2v88internal4wasm7DecoderE, 40
_ZTVN2v88internal4wasm7DecoderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm7DecoderD1Ev
	.quad	_ZN2v88internal4wasm7DecoderD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEEE,"aw"
	.align 8
	.type	_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEEE, @object
	.size	_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEEE, 40
_ZTVN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED1Ev
	.quad	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEED0Ev
	.quad	_ZN2v88internal4wasm15WasmFullDecoderILNS1_7Decoder12ValidateFlagE1ENS1_12_GLOBAL__N_126WasmGraphBuildingInterfaceEE12onFirstErrorEv
	.section	.rodata._ZN2v88internal4wasmL16kAllWasmFeaturesE,"a"
	.align 8
	.type	_ZN2v88internal4wasmL16kAllWasmFeaturesE, @object
	.size	_ZN2v88internal4wasmL16kAllWasmFeaturesE, 13
_ZN2v88internal4wasmL16kAllWasmFeaturesE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.zero	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC40:
	.long	0
	.long	0
	.long	1
	.long	0
	.align 16
.LC42:
	.long	0
	.long	1
	.long	0
	.long	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
