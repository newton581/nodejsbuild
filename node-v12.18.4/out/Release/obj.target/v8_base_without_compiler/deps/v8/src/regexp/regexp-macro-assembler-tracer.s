	.file	"regexp-macro-assembler-tracer.cc"
	.text
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv,"axG",@progbits,_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv:
.LFB7987:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE7987:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv,"axG",@progbits,_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv:
.LFB7988:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE7988:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv:
.LFB19300:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*256(%rax)
	.cfi_endproc
.LFE19300:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	" AbortedCodeGeneration\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv:
.LFB19254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19254:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"label[%08x]: (Bind)\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE:
.LFB19256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC1(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19256:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	" AdvanceCurrentPosition(by=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi, @function
_ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi:
.LFB19257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC2(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19257:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi, .-_ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	" CheckGreedyLoop(label[%08x]);\n\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE:
.LFB19258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19258:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	" PopCurrentPosition();\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv:
.LFB19259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	.LC4(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	272(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19259:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	" PushCurrentPosition();\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv:
.LFB19260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	.LC5(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	296(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19260:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	" Backtrack();\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv:
.LFB19261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	.LC6(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19261:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE.str1.1,"aMS",@progbits,1
.LC7:
	.string	" GoTo(label[%08x]);\n\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE:
.LFB19262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC7(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	224(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19262:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE.str1.1,"aMS",@progbits,1
.LC8:
	.string	" PushBacktrack(label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE:
.LFB19263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC8(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	288(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19263:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv.str1.1,"aMS",@progbits,1
.LC9:
	.string	" [restart for global match]"
.LC10:
	.string	""
.LC11:
	.string	" Succeed();%s\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv:
.LFB19264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*344(%rax)
	leaq	.LC9(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	testb	%al, %al
	movl	%eax, %r12d
	leaq	.LC10(%rip), %rax
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer4FailEv.str1.1,"aMS",@progbits,1
.LC12:
	.string	" Fail();"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer4FailEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer4FailEv
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer4FailEv, @function
_ZN2v88internal26RegExpMacroAssemblerTracer4FailEv:
.LFB19265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	.LC12(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	208(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19265:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer4FailEv, .-_ZN2v88internal26RegExpMacroAssemblerTracer4FailEv
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi.str1.1,"aMS",@progbits,1
.LC13:
	.string	" PopRegister(register=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi, @function
_ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi:
.LFB19266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC13(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	280(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19266:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi, .-_ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"check stack limit"
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	" PushRegister(register=%d, %s);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE:
.LFB19267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC10(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC15(%rip), %rdi
	subq	$8, %rsp
	testl	%edx, %edx
	leaq	.LC14(%rip), %rdx
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	304(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19267:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE, .-_ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	" AdvanceRegister(register=%d, by=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii, @function
_ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii:
.LFB19268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC16(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19268:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii, .-_ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	" SetCurrentPositionFromEnd(by=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi, @function
_ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi:
.LFB19269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC17(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	328(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19269:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi, .-_ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	" SetRegister(register=%d, to=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii, @function
_ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii:
.LFB19270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC18(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	336(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19270:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii, .-_ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	" WriteCurrentPositionToRegister(register=%d,cp_offset=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii, @function
_ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii:
.LFB19271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC19(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	352(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19271:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii, .-_ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	" ClearRegister(from=%d, to=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii, @function
_ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii:
.LFB19272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC20(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	360(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19272:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii, .-_ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	" ReadCurrentPositionFromRegister(register=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi, @function
_ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi:
.LFB19273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC21(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	312(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19273:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi, .-_ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	" WriteStackPointerToRegister(register=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi, @function
_ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi:
.LFB19274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC22(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	368(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19274:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi, .-_ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	" ReadStackPointerFromRegister(register=%d);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi, @function
_ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi:
.LFB19275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	.LC23(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	320(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19275:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi, .-_ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	" CheckAtStart(cp_offset=%d, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE:
.LFB19284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC24(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	112(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19284:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	" CheckNotAtStart(cp_offset=%d, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE:
.LFB19285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC25(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19285:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	" CheckNotCharacterAfterMinusAnd(c=0x%04x, minus=%04x, mask=0x%04x, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE:
.LFB19289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	.LC26(%rip), %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movzwl	%cx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movzwl	%dx, %r13d
	movl	%r14d, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movzwl	%si, %r12d
	movl	%r13d, %edx
	pushq	%rbx
	movl	%r12d, %esi
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%r15), %rdi
	movq	%rbx, %r8
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	160(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19289:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"backward"
.LC28:
	.string	"forward"
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	" CheckNotBackReference(register=%d, %s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE:
.LFB19293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testb	%dl, %dl
	leaq	.LC28(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	leaq	.LC29(%rip), %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	leaq	.LC27(%rip), %rdx
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%r14), %rdi
	movzbl	%bl, %edx
	movq	%r13, %rcx
	popq	%rbx
	movl	%r12d, %esi
	popq	%r12
	movq	(%rdi), %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	128(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19293:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"unicode"
.LC31:
	.string	"non-unicode"
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	" CheckNotBackReferenceIgnoreCase(register=%d, %s %s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE:
.LFB19294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC31(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	leaq	.LC32(%rip), %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	testb	%cl, %cl
	leaq	.LC30(%rip), %rcx
	cmove	%rax, %rcx
	testb	%dl, %dl
	leaq	.LC28(%rip), %rax
	leaq	.LC27(%rip), %rdx
	cmove	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%r15), %rdi
	movzbl	%r12b, %ecx
	movzbl	%bl, %edx
	movq	%r14, %r8
	movl	%r13d, %esi
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19294:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	" CheckPosition(cp_offset=%d, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE:
.LFB19295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC33(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	192(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19295:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"true"
.LC35:
	.string	"false"
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	" CheckSpecialCharacterClass(type='%c', label[%08x]): %s;\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE:
.LFB19296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movzwl	%si, %r13d
	pushq	%r12
	movl	%r13d, %esi
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*200(%rax)
	leaq	.LC34(%rip), %rcx
	movl	%ebx, %edx
	movl	%r13d, %esi
	testb	%al, %al
	movl	%eax, %r12d
	leaq	.LC35(%rip), %rax
	cmove	%rax, %rcx
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19296:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	" IfRegisterLT(register=%d, number=%d, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE:
.LFB19297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	.LC37(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movl	%r12d, %esi
	popq	%r12
	popq	%r13
	movq	(%rdi), %rax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	240(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19297:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	" IfRegisterEqPos(register=%d, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE:
.LFB19298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	.LC38(%rip), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	248(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19298:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	" IfRegisterGE(register=%d, number=%d, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE:
.LFB19299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	.LC39(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movl	%r12d, %esi
	popq	%r12
	popq	%r13
	movq	(%rdi), %rax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	232(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE19299:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	" CheckCharacterLT(c=0x%04x%s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE:
.LFB19281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movzwl	%si, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leal	-32(%rsi), %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -46(%rbp)
	cmpw	$94, %dx
	ja	.L80
	movb	%sil, -43(%rbp)
	movl	$41, %eax
	movw	%ax, -42(%rbp)
	movl	$40, %eax
.L80:
	movb	%al, -44(%rbp)
	leaq	-44(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r12d, %ecx
	movl	%r13d, %esi
	leaq	.LC40(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19281:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	" CheckCharacterGT(c=0x%04x%s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE:
.LFB19282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movzwl	%si, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leal	-32(%rsi), %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -46(%rbp)
	cmpw	$94, %dx
	ja	.L86
	movb	%sil, -43(%rbp)
	movl	$41, %eax
	movw	%ax, -42(%rbp)
	movl	$40, %eax
.L86:
	movb	%al, -44(%rbp)
	leaq	-44(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r12d, %ecx
	movl	%r13d, %esi
	leaq	.LC41(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L90:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19282:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	" CheckCharacter(c=0x%04x%s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE:
.LFB19283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	leal	-32(%rsi), %edx
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -46(%rbp)
	cmpw	$94, %dx
	ja	.L92
	movb	%sil, -43(%rbp)
	movl	$41, %eax
	movw	%ax, -42(%rbp)
	movl	$40, %eax
.L92:
	movb	%al, -44(%rbp)
	leaq	-44(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r13d, %ecx
	movl	%r12d, %esi
	leaq	.LC42(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L96:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19283:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	" CheckNotCharacter(c=0x%04x%s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE:
.LFB19286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	leal	-32(%rsi), %edx
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -46(%rbp)
	cmpw	$94, %dx
	ja	.L98
	movb	%sil, -43(%rbp)
	movl	$41, %eax
	movw	%ax, -42(%rbp)
	movl	$40, %eax
.L98:
	movb	%al, -44(%rbp)
	leaq	-44(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r13d, %ecx
	movl	%r12d, %esi
	leaq	.LC43(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*144(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19286:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	" CheckCharacterAfterAnd(c=0x%04x%s, mask=0x%04x, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE:
.LFB19287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	leal	-32(%rsi), %edx
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -46(%rbp)
	cmpw	$94, %dx
	ja	.L104
	movb	%sil, -43(%rbp)
	movl	$41, %eax
	movw	%ax, -42(%rbp)
	movl	$40, %eax
.L104:
	movb	%al, -44(%rbp)
	leaq	-44(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movl	%r12d, %esi
	leaq	.LC44(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L108:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19287:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	" CheckNotCharacterAfterAnd(c=0x%04x%s, mask=0x%04x, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE:
.LFB19288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	leal	-32(%rsi), %edx
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -46(%rbp)
	cmpw	$94, %dx
	ja	.L110
	movb	%sil, -43(%rbp)
	movl	$41, %eax
	movw	%ax, -42(%rbp)
	movl	$40, %eax
.L110:
	movb	%al, -44(%rbp)
	leaq	-44(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movl	%r12d, %esi
	leaq	.LC45(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19288:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	" CheckCharacterInRange(from=0x%04x%s, to=0x%04x%s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE:
.LFB19290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movzwl	%dx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movzwl	%si, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	leal	-32(%rdx), %ecx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -52(%rbp)
	movw	%dx, -46(%rbp)
	cmpw	$94, %cx
	ja	.L116
	movb	%dl, -43(%rbp)
	movl	$41, %edx
	movl	$40, %eax
	movw	%dx, -42(%rbp)
.L116:
	leal	-32(%rsi), %edx
	movb	%al, -44(%rbp)
	xorl	%eax, %eax
	cmpw	$94, %dx
	ja	.L117
	movb	%sil, -49(%rbp)
	movl	$41, %eax
	movw	%ax, -48(%rbp)
	movl	$40, %eax
.L117:
	movb	%al, -50(%rbp)
	leaq	-50(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r12d, %r9d
	leaq	-44(%rbp), %r8
	movl	%r14d, %ecx
	movl	%r13d, %esi
	leaq	.LC46(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*168(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19290:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	" CheckCharacterNotInRange(from=0x%04x%s, to=%04x%s, label[%08x]);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE:
.LFB19291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movzwl	%dx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movzwl	%si, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	leal	-32(%rdx), %ecx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, -52(%rbp)
	movw	%dx, -46(%rbp)
	cmpw	$94, %cx
	ja	.L124
	movb	%dl, -43(%rbp)
	movl	$41, %edx
	movl	$40, %eax
	movw	%dx, -42(%rbp)
.L124:
	leal	-32(%rsi), %edx
	movb	%al, -44(%rbp)
	xorl	%eax, %eax
	cmpw	$94, %dx
	ja	.L125
	movb	%sil, -49(%rbp)
	movl	$41, %eax
	movw	%ax, -48(%rbp)
	movl	$40, %eax
.L125:
	movb	%al, -50(%rbp)
	leaq	-50(%rbp), %rdx
	xorl	%eax, %eax
	movl	%r12d, %r9d
	leaq	-44(%rbp), %r8
	movl	%r14d, %ecx
	movl	%r13d, %esi
	leaq	.LC47(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L130:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19291:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE.str1.1,"aMS",@progbits,1
.LC48:
	.string	" CheckBitInTable(label[%08x] "
.LC49:
	.string	"%c"
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"\n                                 "
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE.str1.1
.LC51:
	.string	");\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE:
.LFB19292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC49(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	%edx, %esi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	leaq	.LC48(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	movq	(%r12), %rax
	movl	%r15d, %ebx
	movq	%r13, %rdi
	cmpb	$1, 15(%r15,%rax)
	sbbl	%esi, %esi
	xorl	%eax, %eax
	andl	$31, %ebx
	andl	$-42, %esi
	addl	$88, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	$31, %ebx
	jne	.L133
	cmpl	$127, %r15d
	je	.L133
	leaq	.LC50(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L133:
	addq	$1, %r15
	cmpq	$128, %r15
	jne	.L134
	leaq	.LC51(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-56(%rbp), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	movq	184(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19292:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE, .-_ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracerD2Ev
	.type	_ZN2v88internal26RegExpMacroAssemblerTracerD2Ev, @function
_ZN2v88internal26RegExpMacroAssemblerTracerD2Ev:
.LFB19251:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal26RegExpMacroAssemblerTracerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal20RegExpMacroAssemblerD2Ev@PLT
	.cfi_endproc
.LFE19251:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracerD2Ev, .-_ZN2v88internal26RegExpMacroAssemblerTracerD2Ev
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracerD1Ev
	.set	_ZN2v88internal26RegExpMacroAssemblerTracerD1Ev,_ZN2v88internal26RegExpMacroAssemblerTracerD2Ev
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracerD0Ev
	.type	_ZN2v88internal26RegExpMacroAssemblerTracerD0Ev, @function
_ZN2v88internal26RegExpMacroAssemblerTracerD0Ev:
.LFB19253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal26RegExpMacroAssemblerTracerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal20RegExpMacroAssemblerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19253:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracerD0Ev, .-_ZN2v88internal26RegExpMacroAssemblerTracerD0Ev
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii.str1.1,"aMS",@progbits,1
.LC52:
	.string	" (unchecked)"
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii.str1.8,"aMS",@progbits,1
	.align 8
.LC53:
	.string	" LoadCurrentCharacter(cp_offset=%d, label[%08x]%s (%d chars) (eats at least %d));\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii, @function
_ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii:
.LFB19276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC52(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	testb	%cl, %cl
	leaq	.LC10(%rip), %rcx
	movq	%rdi, -56(%rbp)
	cmove	%rax, %rcx
	leaq	.LC53(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-56(%rbp), %r10
	movzbl	%bl, %ecx
	movl	%r15d, %r9d
	movl	%r14d, %r8d
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	32(%r10), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal20RegExpMacroAssembler20LoadCurrentCharacterEiPNS0_5LabelEbii@PLT
	.cfi_endproc
.LFE19276:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii, .-_ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC54:
	.string	" GetCode(%s);\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE:
.LFB19301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	-40(%rbp), %rsi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-32(%rbp), %rsi
	leaq	.LC54(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZdaPv@PLT
.L152:
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*216(%rax)
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L158
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L158:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19301:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE
	.section	.rodata._ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE.str1.1,"aMS",@progbits,1
.LC55:
	.string	"IA32"
.LC56:
	.string	"ARM"
.LC57:
	.string	"ARM64"
.LC58:
	.string	"MIPS"
.LC59:
	.string	"S390"
.LC60:
	.string	"PPC"
.LC61:
	.string	"X64"
.LC62:
	.string	"X87"
.LC63:
	.string	"Bytecode"
.LC64:
	.string	"RegExpMacroAssembler%s();\n"
	.section	.text._ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE
	.type	_ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE, @function
_ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE:
.LFB19248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	24(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal20RegExpMacroAssemblerC2EPNS0_7IsolateEPNS0_4ZoneE@PLT
	leaq	16+_ZTVN2v88internal26RegExpMacroAssemblerTracerE(%rip), %rax
	movq	%r12, 32(%rbx)
	leaq	_ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv(%rip), %rdx
	movq	%rax, (%rbx)
	movq	(%r12), %rax
	movq	256(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L160
	movq	32(%r12), %rdi
	movq	(%rdi), %rdx
	movq	256(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L161
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	256(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L168
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	256(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L161
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*256(%rax)
.L164:
	leaq	.LC56(%rip), %rdx
	leaq	.LC55(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rdx, %xmm1
	leaq	.LC57(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	leaq	.LC58(%rip), %rdx
	leaq	.LC61(%rip), %rbx
	movaps	%xmm0, -96(%rbp)
	movq	%rdx, %xmm2
	movq	%rcx, %xmm0
	leaq	.LC60(%rip), %rdx
	punpcklqdq	%xmm2, %xmm0
	leaq	.LC59(%rip), %rcx
	movq	%rdx, %xmm3
	movaps	%xmm0, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC62(%rip), %rdx
	leaq	.LC63(%rip), %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%rcx, -32(%rbp)
	movq	%rdx, %xmm4
	movaps	%xmm0, -64(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -48(%rbp)
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L169
	cltq
	leaq	.LC64(%rip), %rdi
	movq	-96(%rbp,%rax,8), %rsi
	addq	$80, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	call	*%rdx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r12, %rdi
.L168:
	call	*%rax
	jmp	.L164
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19248:
	.size	_ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE, .-_ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE
	.globl	_ZN2v88internal26RegExpMacroAssemblerTracerC1EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE
	.set	_ZN2v88internal26RegExpMacroAssemblerTracerC1EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE,_ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE, @function
_GLOBAL__sub_I__ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE:
.LFB23553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23553:
	.size	_GLOBAL__sub_I__ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE, .-_GLOBAL__sub_I__ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26RegExpMacroAssemblerTracerC2EPNS0_7IsolateEPNS0_20RegExpMacroAssemblerE
	.weak	_ZTVN2v88internal26RegExpMacroAssemblerTracerE
	.section	.data.rel.ro.local._ZTVN2v88internal26RegExpMacroAssemblerTracerE,"awG",@progbits,_ZTVN2v88internal26RegExpMacroAssemblerTracerE,comdat
	.align 8
	.type	_ZTVN2v88internal26RegExpMacroAssemblerTracerE, @object
	.size	_ZTVN2v88internal26RegExpMacroAssemblerTracerE, 392
_ZTVN2v88internal26RegExpMacroAssemblerTracerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracerD1Ev
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracerD0Ev
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer21AbortedCodeGenerationEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer17stack_limit_slackEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer16CanReadUnalignedEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer22AdvanceCurrentPositionEi
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer15AdvanceRegisterEii
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer9BacktrackEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer4BindEPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer14CheckCharacterEjPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer22CheckCharacterAfterAndEjjPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterGTEtPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer16CheckCharacterLTEtPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckGreedyLoopEPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer12CheckAtStartEiPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckNotAtStartEiPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckNotBackReferenceEibPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer31CheckNotBackReferenceIgnoreCaseEibbPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer17CheckNotCharacterEjPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer25CheckNotCharacterAfterAndEjjPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer30CheckNotCharacterAfterMinusAndEtttPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer21CheckCharacterInRangeEttPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer24CheckCharacterNotInRangeEttPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer15CheckBitInTableENS0_6HandleINS0_9ByteArrayEEEPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer13CheckPositionEiPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer26CheckSpecialCharacterClassEtPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer4FailEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer7GetCodeENS0_6HandleINS0_6StringEEE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer4GoToEPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterGEEiiPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer12IfRegisterLTEiiPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer15IfRegisterEqPosEiPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer14ImplementationEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer24LoadCurrentCharacterImplEiPNS0_5LabelEbii
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer18PopCurrentPositionEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer11PopRegisterEi
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer13PushBacktrackEPNS0_5LabelE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer19PushCurrentPositionEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer12PushRegisterEiNS0_20RegExpMacroAssembler14StackCheckFlagE
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer31ReadCurrentPositionFromRegisterEi
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer28ReadStackPointerFromRegisterEi
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer25SetCurrentPositionFromEndEi
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer11SetRegisterEii
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer7SucceedEv
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer30WriteCurrentPositionToRegisterEii
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer14ClearRegistersEii
	.quad	_ZN2v88internal26RegExpMacroAssemblerTracer27WriteStackPointerToRegisterEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
