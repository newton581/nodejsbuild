	.file	"thread-local-top.cc"
	.text
	.section	.text._ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE
	.type	_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE, @function
_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE:
.LFB8619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$19, %ecx
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-200(%rbp), %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rbx)
	movq	$0, -208(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movdqa	-48(%rbp), %xmm7
	movq	%rsi, 8(%rbx)
	movups	%xmm0, 32(%rbx)
	rep stosq
	movl	$6, -56(%rbp)
	movl	$-1, -200(%rbp)
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	movups	%xmm0, 48(%rbx)
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movups	%xmm0, 64(%rbx)
	movdqa	-208(%rbp), %xmm1
	movdqa	-64(%rbp), %xmm6
	movups	%xmm0, 80(%rbx)
	movups	%xmm2, 96(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm3, 112(%rbx)
	movups	%xmm4, 128(%rbx)
	movups	%xmm5, 144(%rbx)
	movups	%xmm6, 160(%rbx)
	movups	%xmm7, 176(%rbx)
	movaps	%xmm0, -224(%rbp)
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, 24(%rbx)
	call	_ZN2v88internal12trap_handler33GetThreadInWasmThreadLocalAddressEv@PLT
	movq	%rax, 184(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8619:
	.size	_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE, .-_ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE
	.section	.text._ZN2v88internal14ThreadLocalTop4FreeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14ThreadLocalTop4FreeEv
	.type	_ZN2v88internal14ThreadLocalTop4FreeEv, @function
_ZN2v88internal14ThreadLocalTop4FreeEv:
.LFB8620:
	.cfi_startproc
	endbr64
	cmpq	$0, 136(%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal7Isolate10PopPromiseEv@PLT
	cmpq	$0, 136(%rbx)
	jne	.L8
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE8620:
	.size	_ZN2v88internal14ThreadLocalTop4FreeEv, .-_ZN2v88internal14ThreadLocalTop4FreeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE:
.LFB9876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9876:
	.size	_GLOBAL__sub_I__ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14ThreadLocalTop10InitializeEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
