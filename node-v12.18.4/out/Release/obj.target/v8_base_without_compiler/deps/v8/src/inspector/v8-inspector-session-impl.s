	.file	"v8-inspector-session-impl.cc"
	.text
	.section	.text._ZN12v8_inspector18BinaryStringBuffer6stringEv,"axG",@progbits,_ZN12v8_inspector18BinaryStringBuffer6stringEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector18BinaryStringBuffer6stringEv
	.type	_ZN12v8_inspector18BinaryStringBuffer6stringEv, @function
_ZN12v8_inspector18BinaryStringBuffer6stringEv:
.LFB3696:
	.cfi_startproc
	endbr64
	leaq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE3696:
	.size	_ZN12v8_inspector18BinaryStringBuffer6stringEv, .-_ZN12v8_inspector18BinaryStringBuffer6stringEv
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv:
.LFB8545:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE8545:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv, .-_ZN12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl5stateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl5stateEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl5stateEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl5stateEv:
.LFB8604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	$0, 16(%rdi)
	movq	176(%rsi), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*80(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8604:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl5stateEv, .-_ZN12v8_inspector22V8InspectorSessionImpl5stateEv
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB12379:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L7
	cmpl	$3, %edx
	je	.L8
	cmpl	$1, %edx
	je	.L12
.L8:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12379:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS1_8String16EEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS1_8String16EEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS1_8String16EEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation:
.LFB12383:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L14
	cmpl	$3, %edx
	je	.L15
	cmpl	$1, %edx
	je	.L19
.L15:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12383:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS1_8String16EEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS1_8String16EEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB12414:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L21
	cmpl	$3, %edx
	je	.L22
	cmpl	$1, %edx
	je	.L26
.L22:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12414:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS1_18V8RuntimeAgentImplEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS1_18V8RuntimeAgentImplEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS1_18V8RuntimeAgentImplEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB12418:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L28
	cmpl	$3, %edx
	je	.L29
	cmpl	$1, %edx
	je	.L33
.L29:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE12418:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS1_18V8RuntimeAgentImplEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS1_18V8RuntimeAgentImplEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZN12v8_inspector18BinaryStringBufferD2Ev,"axG",@progbits,_ZN12v8_inspector18BinaryStringBufferD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector18BinaryStringBufferD2Ev
	.type	_ZN12v8_inspector18BinaryStringBufferD2Ev, @function
_ZN12v8_inspector18BinaryStringBufferD2Ev:
.LFB17037:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector18BinaryStringBufferE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L34
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE17037:
	.size	_ZN12v8_inspector18BinaryStringBufferD2Ev, .-_ZN12v8_inspector18BinaryStringBufferD2Ev
	.weak	_ZN12v8_inspector18BinaryStringBufferD1Ev
	.set	_ZN12v8_inspector18BinaryStringBufferD1Ev,_ZN12v8_inspector18BinaryStringBufferD2Ev
	.section	.text._ZN12v8_inspector18BinaryStringBufferD0Ev,"axG",@progbits,_ZN12v8_inspector18BinaryStringBufferD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector18BinaryStringBufferD0Ev
	.type	_ZN12v8_inspector18BinaryStringBufferD0Ev, @function
_ZN12v8_inspector18BinaryStringBufferD0Ev:
.LFB17039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector18BinaryStringBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE17039:
	.size	_ZN12v8_inspector18BinaryStringBufferD0Ev, .-_ZN12v8_inspector18BinaryStringBufferD0Ev
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl16setSkipAllPausesEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl16setSkipAllPausesEb
	.type	_ZN12v8_inspector22V8InspectorSessionImpl16setSkipAllPausesEb, @function
_ZN12v8_inspector22V8InspectorSessionImpl16setSkipAllPausesEb:
.LFB8647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	192(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rdi
	movq	(%r8), %rax
	movq	%r8, %rsi
	call	*208(%rax)
	movq	-56(%rbp), %rdi
	leaq	-40(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8647:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl16setSkipAllPausesEb, .-_ZN12v8_inspector22V8InspectorSessionImpl16setSkipAllPausesEb
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl6resumeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl6resumeEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl6resumeEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl6resumeEv:
.LFB8648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	192(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rdi
	movq	(%rsi), %rax
	call	*104(%rax)
	movq	-56(%rbp), %rdi
	leaq	-40(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8648:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl6resumeEv, .-_ZN12v8_inspector22V8InspectorSessionImpl6resumeEv
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl8stepOverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl8stepOverEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl8stepOverEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl8stepOverEv:
.LFB8649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	192(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rdi
	movq	(%rsi), %rax
	call	*240(%rax)
	movq	-56(%rbp), %rdi
	leaq	-40(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8649:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl8stepOverEv, .-_ZN12v8_inspector22V8InspectorSessionImpl8stepOverEv
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB6802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6802:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.rodata._ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE, @function
_ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE:
.LFB8544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8544:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE, .-_ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl22discardInjectedScriptsEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl22discardInjectedScriptsEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl22discardInjectedScriptsEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB12377:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %r8
	movq	(%r8), %rdi
	movl	(%rax), %esi
	jmp	_ZN12v8_inspector16InspectedContext21discardInjectedScriptEi@PLT
	.cfi_endproc
.LFE12377:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl22discardInjectedScriptsEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl22discardInjectedScriptsEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl17reportAllContextsEPNS0_18V8RuntimeAgentImplEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl17reportAllContextsEPNS0_18V8RuntimeAgentImplEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl17reportAllContextsEPNS0_18V8RuntimeAgentImplEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB12417:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rsi), %rsi
	movq	(%rax), %rdi
	jmp	_ZN12v8_inspector18V8RuntimeAgentImpl29reportExecutionContextCreatedEPNS_16InspectedContextE@PLT
	.cfi_endproc
.LFE12417:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl17reportAllContextsEPNS0_18V8RuntimeAgentImplEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl17reportAllContextsEPNS0_18V8RuntimeAgentImplEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl28schedulePauseOnNextStatementERKNS_10StringViewES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl28schedulePauseOnNextStatementERKNS_10StringViewES3_
	.type	_ZN12v8_inspector22V8InspectorSessionImpl28schedulePauseOnNextStatementERKNS_10StringViewES3_, @function
_ZN12v8_inspector22V8InspectorSessionImpl28schedulePauseOnNextStatementERKNS_10StringViewES3_:
.LFB8644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdx, %rsi
	subq	$72, %rsp
	movq	192(%rdi), %r14
	leaq	-96(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE@PLT
	movq	-96(%rbp), %rax
	movq	$0, -96(%rbp)
	testq	%rax, %rax
	je	.L65
	cmpl	$6, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L65:
	leaq	-80(%rbp), %r13
	movq	%r12, %rsi
	movq	%rax, -88(%rbp)
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r14, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl28schedulePauseOnNextStatementERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	movq	(%rdi), %rax
	call	*24(%rax)
.L67:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	call	*24(%rax)
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8644:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl28schedulePauseOnNextStatementERKNS_10StringViewES3_, .-_ZN12v8_inspector22V8InspectorSessionImpl28schedulePauseOnNextStatementERKNS_10StringViewES3_
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl26cancelPauseOnNextStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl26cancelPauseOnNextStatementEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl26cancelPauseOnNextStatementEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl26cancelPauseOnNextStatementEv:
.LFB8645:
	.cfi_startproc
	endbr64
	movq	192(%rdi), %rdi
	jmp	_ZN12v8_inspector19V8DebuggerAgentImpl26cancelPauseOnNextStatementEv@PLT
	.cfi_endproc
.LFE8645:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl26cancelPauseOnNextStatementEv, .-_ZN12v8_inspector22V8InspectorSessionImpl26cancelPauseOnNextStatementEv
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl12breakProgramERKNS_10StringViewES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl12breakProgramERKNS_10StringViewES3_
	.type	_ZN12v8_inspector22V8InspectorSessionImpl12breakProgramERKNS_10StringViewES3_, @function
_ZN12v8_inspector22V8InspectorSessionImpl12breakProgramERKNS_10StringViewES3_:
.LFB8646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdx, %rsi
	subq	$72, %rsp
	movq	192(%rdi), %r14
	leaq	-96(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE@PLT
	movq	-96(%rbp), %rax
	movq	$0, -96(%rbp)
	testq	%rax, %rax
	je	.L84
	cmpl	$6, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L84:
	leaq	-80(%rbp), %r13
	movq	%r12, %rsi
	movq	%rax, -88(%rbp)
	movq	%r13, %rdi
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r14, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl12breakProgramERKNS_8String16ESt10unique_ptrINS_8protocol15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	movq	(%rdi), %rax
	call	*24(%rax)
.L86:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	(%rdi), %rax
	call	*24(%rax)
.L83:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8646:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl12breakProgramERKNS_10StringViewES3_, .-_ZN12v8_inspector22V8InspectorSessionImpl12breakProgramERKNS_10StringViewES3_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB12413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%r8), %rdi
	movl	(%rax), %esi
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	testq	%rax, %rax
	je	.L101
	movq	%rax, %rdi
	movq	(%rbx), %rax
	movzbl	(%rax), %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector14InjectedScript31setCustomObjectFormatterEnabledEb@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12413:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl18releaseObjectGroupERKNS0_8String16EEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl18releaseObjectGroupERKNS0_8String16EEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl18releaseObjectGroupERKNS0_8String16EEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB12382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%r8), %rdi
	movl	(%rax), %esi
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	testq	%rax, %rax
	je	.L104
	movq	(%rbx), %rsi
	addq	$8, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector14InjectedScript18releaseObjectGroupERKNS_8String16E@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12382:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl18releaseObjectGroupERKNS0_8String16EEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl18releaseObjectGroupERKNS0_8String16EEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.rodata._ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"use_binary_protocol"
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE, @function
_ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE:
.LFB8600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$192, %rsp
	movq	8(%rsi), %rsi
	movq	16(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rbx)
	movaps	%xmm0, -192(%rbp)
	movq	$0, -176(%rbp)
	je	.L108
	cmpq	$1, %rsi
	jbe	.L109
	cmpb	$-40, (%rdi)
	je	.L137
.L109:
	leaq	-192(%rbp), %rdx
	call	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE@PLT
.L112:
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rdx
	subq	%rsi, %rdx
.L111:
	leaq	-208(%rbp), %rdi
	addq	$48, %r12
	leaq	-160(%rbp), %r13
	call	_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm@PLT
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-208(%rbp), %rsi
	movw	%dx, -144(%rbp)
	leaq	-144(%rbp), %rbx
	leaq	-212(%rbp), %rdx
	movq	%rbx, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E@PLT
	testb	%al, %al
	jne	.L138
.L113:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	movq	(%rdi), %rax
	call	*24(%rax)
.L118:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	leaq	-192(%rbp), %rdx
	call	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L138:
	xorl	%eax, %eax
	movl	-212(%rbp), %esi
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movw	%ax, -96(%rbp)
	leaq	-96(%rbp), %r14
	leaq	-112(%rbp), %r8
	movq	%r13, %rdx
	movq	-208(%rbp), %rax
	leaq	-200(%rbp), %rcx
	movq	%r14, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -208(%rbp)
	movq	%rax, -200(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE@PLT
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*24(%rax)
.L114:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L113
	call	_ZdlPv@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L137:
	cmpb	$90, 1(%rdi)
	jne	.L109
	movb	$1, 256(%r12)
	movq	176(%r12), %r14
	leaq	-112(%rbp), %r13
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rsi
	jmp	.L111
.L139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8600:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE, .-_ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE
	.section	.text._ZThn8_N12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE
	.type	_ZThn8_N12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE, @function
_ZThn8_N12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE:
.LFB17338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17338:
	.size	_ZThn8_N12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE, .-_ZThn8_N12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE
	.section	.text._ZThn8_N12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv
	.type	_ZThn8_N12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv, @function
_ZThn8_N12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv:
.LFB17341:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE17341:
	.size	_ZThn8_N12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv, .-_ZThn8_N12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD0Ev
	.type	_ZN12v8_inspector8protocol6Schema6DomainD0Ev, @function
_ZN12v8_inspector8protocol6Schema6DomainD0Ev:
.LFB5517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5517:
	.size	_ZN12v8_inspector8protocol6Schema6DomainD0Ev, .-_ZN12v8_inspector8protocol6Schema6DomainD0Ev
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_10StringViewE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_10StringViewE, @function
_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_10StringViewE:
.LFB8554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	leaq	-96(%rbp), %r12
	movq	%rdi, %xmm0
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-100(%rbp), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movl	20(%rbx), %eax
	movdqa	-128(%rbp), %xmm0
	movq	%r12, %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS1_8String16EEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation(%rip), %rcx
	movq	24(%rbx), %rdi
	movl	16(%rbx), %esi
	movl	%eax, -100(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl18releaseObjectGroupERKNS0_8String16EEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L148
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L148:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L155:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8554:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_10StringViewE, .-_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_10StringViewE
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.type	_ZN12v8_inspector8protocol6Schema6DomainD2Ev, @function
_ZN12v8_inspector8protocol6Schema6DomainD2Ev:
.LFB5515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L156
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5515:
	.size	_ZN12v8_inspector8protocol6Schema6DomainD2Ev, .-_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD1Ev
	.set	_ZN12v8_inspector8protocol6Schema6DomainD1Ev,_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev
	.type	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev, @function
_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev:
.LFB17343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	48(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L160
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17343:
	.size	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev, .-_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev
	.type	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev, @function
_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev:
.LFB17344:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	popq	%rbx
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE17344:
	.size	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev, .-_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, @function
_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev:
.LFB6800:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	24(%rdi), %r8
	addq	$40, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L168
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	ret
	.cfi_endproc
.LFE6800:
	.size	_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev, .-_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger11SearchMatchD1Ev,_ZN12v8_inspector8protocol8Debugger11SearchMatchD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev:
.LFB17339:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	movq	16(%rdi), %r8
	addq	$32, %rdi
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L170
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	ret
	.cfi_endproc
.LFE17339:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD1Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger11SearchMatchD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.type	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, @function
_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev:
.LFB17342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	addq	$32, %rdi
	subq	$8, %rsp
	movq	-16(%rdi), %r8
	movups	%xmm0, -40(%rdi)
	cmpq	%rdi, %r8
	je	.L173
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L173:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE17342:
	.size	_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev, .-_ZThn8_N12v8_inspector8protocol8Debugger11SearchMatchD0Ev
	.section	.text._ZN12v8_inspector18V8InspectorSession17canDispatchMethodERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector18V8InspectorSession17canDispatchMethodERKNS_10StringViewE
	.type	_ZN12v8_inspector18V8InspectorSession17canDispatchMethodERKNS_10StringViewE, @function
_ZN12v8_inspector18V8InspectorSession17canDispatchMethodERKNS_10StringViewE:
.LFB8506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector8protocol7Runtime8Metainfo13commandPrefixE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc@PLT
	testb	%al, %al
	je	.L180
.L177:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	leaq	_ZN12v8_inspector8protocol8Debugger8Metainfo13commandPrefixE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc@PLT
	testb	%al, %al
	jne	.L177
	leaq	_ZN12v8_inspector8protocol8Profiler8Metainfo13commandPrefixE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc@PLT
	testb	%al, %al
	jne	.L177
	leaq	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo13commandPrefixE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc@PLT
	testb	%al, %al
	jne	.L177
	leaq	_ZN12v8_inspector8protocol7Console8Metainfo13commandPrefixE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc@PLT
	testb	%al, %al
	jne	.L177
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	_ZN12v8_inspector8protocol6Schema8Metainfo13commandPrefixE(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector20stringViewStartsWithERKNS_10StringViewEPKc@PLT
	.cfi_endproc
.LFE8506:
	.size	_ZN12v8_inspector18V8InspectorSession17canDispatchMethodERKNS_10StringViewE, .-_ZN12v8_inspector18V8InspectorSession17canDispatchMethodERKNS_10StringViewE
	.section	.text._ZN12v8_inspector13V8ContextInfo18executionContextIdEN2v85LocalINS1_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector13V8ContextInfo18executionContextIdEN2v85LocalINS1_7ContextEEE
	.type	_ZN12v8_inspector13V8ContextInfo18executionContextIdEN2v85LocalINS1_7ContextEEE, @function
_ZN12v8_inspector13V8ContextInfo18executionContextIdEN2v85LocalINS1_7ContextEEE:
.LFB8507:
	.cfi_startproc
	endbr64
	jmp	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	.cfi_endproc
.LFE8507:
	.size	_ZN12v8_inspector13V8ContextInfo18executionContextIdEN2v85LocalINS1_7ContextEEE, .-_ZN12v8_inspector13V8ContextInfo18executionContextIdEN2v85LocalINS1_7ContextEEE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImplC2EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImplC2EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.type	_ZN12v8_inspector22V8InspectorSessionImplC2EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE, @function
_ZN12v8_inspector22V8InspectorSessionImplC2EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE:
.LFB8531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	48(%rbx), %r13
	leaq	8(%rbx), %r15
	subq	$120, %rsp
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	192+_ZTVN12v8_inspector22V8InspectorSessionImplE(%rip), %rax
	movl	%edx, 16(%rbx)
	leaq	-176(%rax), %rdi
	movq	%rax, %xmm1
	movl	%ecx, 20(%rbx)
	movq	%rdi, %xmm0
	movb	$0, 40(%rbx)
	movq	%r13, %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	%rsi, %xmm0
	movq	%r15, %rsi
	movhps	-160(%rbp), %xmm0
	movups	%xmm0, 24(%rbx)
	call	_ZN12v8_inspector8protocol14UberDispatcherC1EPNS0_15FrontendChannelE@PLT
	pxor	%xmm0, %xmm0
	cmpb	$0, (%r14)
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	jne	.L265
	movq	-152(%rbp), %rax
	leaq	-128(%rbp), %rdx
	movq	8(%rax), %rsi
	movq	16(%rax), %rdi
	call	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanItEEPSt6vectorIhSaIhEE@PLT
.L186:
	testl	%eax, %eax
	je	.L266
.L187:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r14, 176(%rbx)
.L190:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	call	_ZdlPv@PLT
	movq	176(%rbx), %r14
.L191:
	movb	$0, 256(%rbx)
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %r12
	leaq	.LC1(%rip), %rsi
	movq	$0, 248(%rbx)
	movq	%r12, %rdi
	movups	%xmm0, 184(%rbx)
	movups	%xmm0, 200(%rbx)
	movups	%xmm0, 216(%rbx)
	movups	%xmm0, 232(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	-80(%rbp), %r14
	leaq	256(%rbx), %rdx
	call	_ZNK12v8_inspector8protocol15DictionaryValue10getBooleanERKNS_8String16EPb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	leaq	_ZN12v8_inspector8protocol7Runtime8Metainfo10domainNameE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	176(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L267
.L193:
	movl	$104, %edi
	movq	%rcx, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector18V8RuntimeAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE@PLT
	movq	184(%rbx), %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L195
	movq	(%rdi), %rax
	call	*8(%rax)
.L195:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L196
	call	_ZdlPv@PLT
.L196:
	movq	184(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol7Runtime10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE@PLT
	leaq	_ZN12v8_inspector8protocol8Debugger8Metainfo10domainNameE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	176(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L268
.L197:
	movl	$480, %edi
	movq	%rcx, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE@PLT
	movq	192(%rbx), %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 192(%rbx)
	testq	%rdi, %rdi
	je	.L199
	movq	(%rdi), %rax
	call	*8(%rax)
.L199:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	192(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol8Debugger10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE@PLT
	leaq	_ZN12v8_inspector8protocol8Profiler8Metainfo10domainNameE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	176(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L269
.L201:
	movl	$128, %edi
	movq	%rcx, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector19V8ProfilerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE@PLT
	movq	208(%rbx), %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 208(%rbx)
	testq	%rdi, %rdi
	je	.L203
	movq	(%rdi), %rax
	call	*8(%rax)
.L203:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	208(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE@PLT
	leaq	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo10domainNameE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	176(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L270
.L205:
	movl	$48, %edi
	movq	%rcx, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector23V8HeapProfilerAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE@PLT
	movq	200(%rbx), %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 200(%rbx)
	testq	%rdi, %rdi
	je	.L207
	movq	(%rdi), %rax
	call	*8(%rax)
.L207:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	200(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE@PLT
	leaq	_ZN12v8_inspector8protocol7Console8Metainfo10domainNameE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	176(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L271
.L209:
	movl	$40, %edi
	movq	%rcx, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector18V8ConsoleAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE@PLT
	movq	216(%rbx), %rdi
	movq	-160(%rbp), %rax
	movq	%rax, 216(%rbx)
	testq	%rdi, %rdi
	je	.L211
	movq	(%rdi), %rax
	call	*8(%rax)
.L211:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	216(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol7Console10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE@PLT
	leaq	_ZN12v8_inspector8protocol6Schema8Metainfo10domainNameE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	176(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L272
.L213:
	movl	$24, %edi
	movq	%rcx, -160(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector17V8SchemaAgentImplC1EPNS_22V8InspectorSessionImplEPNS_8protocol15FrontendChannelEPNS3_15DictionaryValueE@PLT
	movq	224(%rbx), %rdi
	movq	%r12, 224(%rbx)
	testq	%rdi, %rdi
	je	.L215
	movq	(%rdi), %rax
	call	*8(%rax)
.L215:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	224(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE@PLT
	movq	-152(%rbp), %rax
	cmpq	$0, 8(%rax)
	jne	.L273
.L182:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L274
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	-128(%rbp), %rdi
	movq	-120(%rbp), %rsi
	subq	%rdi, %rsi
	je	.L187
.L185:
	leaq	-136(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm@PLT
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L187
	cmpl	$6, 8(%r14)
	movl	$0, %eax
	cmovne	%rax, %r14
	movq	%r14, 176(%rbx)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L265:
	movq	8(%r14), %rsi
	movq	16(%r14), %rdi
	cmpq	$1, %rsi
	jbe	.L184
	cmpb	$-40, (%rdi)
	je	.L275
.L184:
	leaq	-128(%rbp), %rdx
	call	_ZN12v8_inspector17ConvertJSONToCBOREN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L273:
	movq	184(%rbx), %rdi
	call	_ZN12v8_inspector18V8RuntimeAgentImpl7restoreEv@PLT
	movq	192(%rbx), %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl7restoreEv@PLT
	movq	200(%rbx), %rdi
	call	_ZN12v8_inspector23V8HeapProfilerAgentImpl7restoreEv@PLT
	movq	208(%rbx), %rdi
	call	_ZN12v8_inspector19V8ProfilerAgentImpl7restoreEv@PLT
	movq	216(%rbx), %rdi
	call	_ZN12v8_inspector18V8ConsoleAgentImpl7restoreEv@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L275:
	cmpb	$90, 1(%rdi)
	jne	.L184
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-160(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	176(%rbx), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %rcx
	testq	%rdi, %rdi
	je	.L197
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-160(%rbp), %rcx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L269:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-160(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	176(%rbx), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %rcx
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-160(%rbp), %rcx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-160(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	176(%rbx), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %rcx
	testq	%rdi, %rdi
	je	.L193
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-160(%rbp), %rcx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-160(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	176(%rbx), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %rcx
	testq	%rdi, %rdi
	je	.L205
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-160(%rbp), %rcx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-160(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	176(%rbx), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %rcx
	testq	%rdi, %rdi
	je	.L209
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-160(%rbp), %rcx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L272:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-160(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	176(%rbx), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-128(%rbp), %rdi
	movq	-160(%rbp), %rcx
	testq	%rdi, %rdi
	je	.L213
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-160(%rbp), %rcx
	jmp	.L213
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8531:
	.size	_ZN12v8_inspector22V8InspectorSessionImplC2EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE, .-_ZN12v8_inspector22V8InspectorSessionImplC2EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.globl	_ZN12v8_inspector22V8InspectorSessionImplC1EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.set	_ZN12v8_inspector22V8InspectorSessionImplC1EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE,_ZN12v8_inspector22V8InspectorSessionImplC2EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl6createEPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl6createEPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl6createEPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE, @function
_ZN12v8_inspector22V8InspectorSessionImpl6createEPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE:
.LFB8510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$264, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movl	%r15d, %ecx
	movq	%rax, %rbx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector22V8InspectorSessionImplC1EPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	movq	%rbx, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8510:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl6createEPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE, .-_ZN12v8_inspector22V8InspectorSessionImpl6createEPNS_15V8InspectorImplEiiPNS_11V8Inspector7ChannelERKNS_10StringViewE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl10agentStateERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl10agentStateERKNS_8String16E
	.type	_ZN12v8_inspector22V8InspectorSessionImpl10agentStateERKNS_8String16E, @function
_ZN12v8_inspector22V8InspectorSessionImpl10agentStateERKNS_8String16E:
.LFB8537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	176(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L286
.L278:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, -48(%rbp)
	movq	176(%rbx), %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L278
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L278
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8537:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl10agentStateERKNS_8String16E, .-_ZN12v8_inspector22V8InspectorSessionImpl10agentStateERKNS_8String16E
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, @function
_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE:
.LFB8538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-144(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	addq	$-128, %rsp
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*8(%rax)
	cmpb	$0, 256(%rbx)
	je	.L289
	movq	-144(%rbp), %r13
	movq	-136(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-128(%rbp), %r14
	movaps	%xmm0, -144(%rbp)
	movq	$0, -128(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector18BinaryStringBufferE(%rip), %rcx
	movq	%rbx, 16(%rax)
	subq	%r13, %rbx
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movq	%r14, 24(%rax)
	movb	$1, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	%r13, 48(%rax)
	movq	%rax, (%r12)
.L290:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	subq	$-128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	-144(%rbp), %rdi
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdx
	leaq	-80(%rbp), %r13
	movq	-136(%rbp), %rsi
	movaps	%xmm0, -112(%rbp)
	movq	$0, -96(%rbp)
	subq	%rdi, %rsi
	call	_ZN12v8_inspector17ConvertCBORToJSONEN30v8_inspector_protocol_encoding4spanIhEEPSt6vectorIhSaIhEE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%r13, %rdi
	subq	%rsi, %rdx
	call	_ZN12v8_inspector8String16C1EPKcm@PLT
	leaq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E@PLT
	movq	-152(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, (%r12)
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L290
	call	_ZdlPv@PLT
	jmp	.L290
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8538:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, .-_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, @function
_ZN12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE:
.LFB8541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	movq	%rdi, %rsi
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	32(%rdi), %r13
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	movq	16(%rax), %rbx
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	leaq	-56(%rbp), %rdx
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	movq	%r13, %rdi
	movq	%r14, %rdx
	movl	%r12d, %esi
	call	*%rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L304
	movq	(%rdi), %rax
	call	*8(%rax)
.L304:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	movq	(%rdi), %rax
	call	*24(%rax)
.L303:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L314:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8541:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, .-_ZN12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, @function
_ZN12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE:
.LFB8543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	leaq	-56(%rbp), %rdx
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %rbx
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*%rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L316
	movq	(%rdi), %rax
	call	*8(%rax)
.L316:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L315
	movq	(%rdi), %rax
	call	*24(%rax)
.L315:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L326
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L326:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8543:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, .-_ZN12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.section	.text._ZThn8_N12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.type	_ZThn8_N12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, @function
_ZThn8_N12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE:
.LFB17345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r13
	leaq	-48(%rbp), %rdx
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %rbx
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-8(%rdi), %rsi
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*%rbx
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L328
	movq	(%rdi), %rax
	call	*8(%rax)
.L328:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L327
	movq	(%rdi), %rax
	call	*24(%rax)
.L327:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L338:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17345:
	.size	_ZThn8_N12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, .-_ZThn8_N12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.section	.text._ZThn8_N12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.type	_ZThn8_N12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, @function
_ZThn8_N12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE:
.LFB17346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-56(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	leaq	-8(%rdi), %rsi
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	24(%rdi), %r13
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	movq	16(%rax), %rbx
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	leaq	-48(%rbp), %rdx
	movq	%rax, -48(%rbp)
	call	_ZN12v8_inspector22V8InspectorSessionImpl20serializeForFrontendESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	movq	%r13, %rdi
	movq	%r14, %rdx
	movl	%r12d, %esi
	call	*%rbx
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L340
	movq	(%rdi), %rax
	call	*8(%rax)
.L340:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L339
	movq	(%rdi), %rax
	call	*24(%rax)
.L339:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L350:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17346:
	.size	_ZThn8_N12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE, .-_ZThn8_N12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv:
.LFB8547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	232(%rdi), %r14
	movq	240(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %r14
	je	.L352
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L356:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L353
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*16(%rax)
	cmpq	%rbx, %r13
	jne	.L356
.L354:
	movq	%r14, 240(%r12)
.L352:
	movl	20(%r12), %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEvEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	24(%r12), %rdi
	leaq	-80(%rbp), %r13
	movq	%rcx, %xmm0
	movl	16(%r12), %esi
	movq	%r13, %rdx
	movl	%eax, -84(%rbp)
	leaq	-84(%rbp), %rax
	movq	%rax, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl22discardInjectedScriptsEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L351
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L351:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L363
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L356
	jmp	.L354
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8547:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv, .-_ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl5resetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl5resetEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl5resetEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl5resetEv:
.LFB8546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	192(%rdi), %rdi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl5resetEv@PLT
	movq	184(%r12), %rdi
	call	_ZN12v8_inspector18V8RuntimeAgentImpl5resetEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv
	.cfi_endproc
.LFE8546:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl5resetEv, .-_ZN12v8_inspector22V8InspectorSessionImpl5resetEv
	.section	.rodata._ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Cannot find context with specified id"
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE, @function
_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE:
.LFB8552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rcx)
	movl	16(%rsi), %esi
	movq	24(%rbx), %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L375
	movl	20(%rbx), %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L376
.L371:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L366:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	leaq	-80(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L366
	call	_ZdlPv@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L376:
	movl	20(%rbx), %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector16InspectedContext20createInjectedScriptEi@PLT
	movq	%rax, 0(%r13)
	cmpb	$0, 40(%rbx)
	je	.L371
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN12v8_inspector14InjectedScript31setCustomObjectFormatterEnabledEb@PLT
	jmp	.L371
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8552:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE, .-_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_10StringViewEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_10StringViewEb
	.type	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_10StringViewEb, @function
_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_10StringViewEb:
.LFB8593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%r8, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movl	%r9d, -180(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r14, %rdi
	leaq	-112(%rbp), %r14
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-176(%rbp), %rcx
	movl	%eax, %edx
	leaq	-88(%rbp), %r13
	call	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE
	movq	-104(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L379
	call	_ZdlPv@PLT
.L379:
	movq	-176(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L381
	xorl	%eax, %eax
	cmpb	$0, -180(%rbp)
	movq	%r14, %rdi
	movq	%r15, %rcx
	setne	%al
	movq	%rbx, %rdx
	leaq	-168(%rbp), %r9
	movq	$0, -168(%rbp)
	leal	1(%rax), %r8d
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
	movq	-104(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L383
	call	_ZdlPv@PLT
.L383:
	movq	-168(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L381
	addq	$8, %rsi
.L381:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rsi, (%r12)
	cmpq	%rax, %rdi
	je	.L378
	call	_ZdlPv@PLT
.L378:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L391
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L391:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8593:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_10StringViewEb, .-_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_10StringViewEb
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEPNS_18RemoteObjectIdBaseERPNS_14InjectedScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEPNS_18RemoteObjectIdBaseERPNS_14InjectedScriptE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEPNS_18RemoteObjectIdBaseERPNS_14InjectedScriptE, @function
_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEPNS_18RemoteObjectIdBaseERPNS_14InjectedScriptE:
.LFB8553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movl	(%rdx), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rcx)
	movl	16(%rsi), %esi
	movq	24(%rbx), %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L401
	movl	20(%rbx), %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L402
.L397:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
.L392:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	leaq	-80(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L392
	call	_ZdlPv@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L402:
	movl	20(%rbx), %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector16InspectedContext20createInjectedScriptEi@PLT
	movq	%rax, 0(%r13)
	cmpb	$0, 40(%rbx)
	je	.L397
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN12v8_inspector14InjectedScript31setCustomObjectFormatterEnabledEb@PLT
	jmp	.L397
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8553:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEPNS_18RemoteObjectIdBaseERPNS_14InjectedScriptE, .-_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEPNS_18RemoteObjectIdBaseERPNS_14InjectedScriptE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E
	.type	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E, @function
_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E:
.LFB8555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS1_8String16EEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-64(%rbp), %r12
	movq	%r12, %rdx
	subq	$72, %rsp
	movq	24(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	20(%rdi), %eax
	movq	%rsi, -64(%rbp)
	movl	16(%rdi), %esi
	movq	%r8, %rdi
	movl	%eax, -68(%rbp)
	leaq	-68(%rbp), %rax
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl18releaseObjectGroupERKNS0_8String16EEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L404
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L404:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L411:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8555:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E, .-_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_8String16E
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_
	.type	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_, @function
_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_:
.LFB8558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-176(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	leaq	-192(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector14RemoteObjectId5parseERKNS_8String16EPSt10unique_ptrIS0_St14default_deleteIS0_EE@PLT
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L413
	movl	%eax, (%r14)
	leaq	24(%r14), %rax
	leaq	-152(%rbp), %rdx
	movq	%rax, 8(%r14)
	movq	-168(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L482
.L435:
	movq	%rax, 8(%r14)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%r14)
.L436:
	movq	-160(%rbp), %rax
	movq	-192(%rbp), %rdi
	movq	%rax, 16(%r14)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%r14)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%r14)
.L446:
	testq	%rdi, %rdi
	je	.L412
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L483
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	movdqu	-152(%rbp), %xmm1
	movups	%xmm1, 24(%r14)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L413:
	movq	-192(%rbp), %rax
	leaq	-112(%rbp), %r15
	movq	%r12, %rsi
	movq	$0, -184(%rbp)
	movq	%r15, %rdi
	leaq	-184(%rbp), %rcx
	movl	(%rax), %edx
	call	_ZN12v8_inspector22V8InspectorSessionImpl18findInjectedScriptEiRPNS_14InjectedScriptE
	movl	-112(%rbp), %eax
	movq	-104(%rbp), %rdx
	leaq	-88(%rbp), %r8
	movq	-168(%rbp), %rdi
	movl	%eax, -176(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%r8, %rdx
	je	.L484
	leaq	-152(%rbp), %r12
	movq	-88(%rbp), %rcx
	cmpq	%r12, %rdi
	je	.L485
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	-152(%rbp), %rsi
	movq	%rdx, -168(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -160(%rbp)
	testq	%rdi, %rdi
	je	.L422
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
.L420:
	xorl	%r9d, %r9d
	movq	$0, -96(%rbp)
	movw	%r9w, (%rdi)
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	cmpq	%r8, %rdi
	je	.L423
	movq	%r8, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %r8
.L423:
	movl	-176(%rbp), %eax
	movq	-192(%rbp), %rdi
	testl	%eax, %eax
	je	.L424
	movl	%eax, (%r14)
	leaq	24(%r14), %rax
	movq	%rax, 8(%r14)
	movq	-168(%rbp), %rax
	cmpq	%r12, %rax
	je	.L486
	movq	%rax, 8(%r14)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%r14)
.L426:
	movq	-160(%rbp), %rax
	movq	%rax, 16(%r14)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%r14)
	movl	-128(%rbp), %eax
	movl	%eax, 48(%r14)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L485:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, -168(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -160(%rbp)
.L422:
	movq	%r8, -104(%rbp)
	leaq	-88(%rbp), %r8
	movq	%r8, %rdi
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L424:
	movq	-184(%rbp), %rsi
	movq	%rdi, %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%r8, -208(%rbp)
	call	_ZNK12v8_inspector14InjectedScript10findObjectERKNS_14RemoteObjectIdEPN2v85LocalINS4_5ValueEEE@PLT
	movl	-112(%rbp), %eax
	movq	-104(%rbp), %rdx
	movq	-208(%rbp), %r8
	movq	-168(%rbp), %rdi
	movl	%eax, -176(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%r8, %rdx
	je	.L487
	movq	-88(%rbp), %rcx
	cmpq	%r12, %rdi
	je	.L488
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	-152(%rbp), %rsi
	movq	%rdx, -168(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -160(%rbp)
	testq	%rdi, %rdi
	je	.L432
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
.L430:
	xorl	%ecx, %ecx
	movq	$0, -96(%rbp)
	movw	%cx, (%rdi)
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, -128(%rbp)
	cmpq	%r8, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movl	-176(%rbp), %eax
	testl	%eax, %eax
	je	.L434
	movl	%eax, (%r14)
	leaq	24(%r14), %rax
	movq	%rax, 8(%r14)
	movq	-168(%rbp), %rax
	cmpq	%r12, %rax
	jne	.L435
	movdqu	-152(%rbp), %xmm7
	movups	%xmm7, 24(%r14)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L484:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L418
	cmpq	$1, %rax
	je	.L489
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L418
	movq	%r8, %rsi
	movq	%r8, -208(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rax
	movq	-168(%rbp), %rdi
	movq	-208(%rbp), %r8
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L418:
	xorl	%r10d, %r10d
	movq	%rax, -160(%rbp)
	leaq	-152(%rbp), %r12
	movw	%r10w, (%rdi,%rdx)
	movq	-104(%rbp), %rdi
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L486:
	movdqu	-152(%rbp), %xmm4
	movups	%xmm4, 24(%r14)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, -168(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -160(%rbp)
.L432:
	movq	%r8, -104(%rbp)
	leaq	-88(%rbp), %r8
	movq	%r8, %rdi
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L434:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZNK12v8_inspector16InspectedContext7contextEv@PLT
	movq	-200(%rbp), %rcx
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L437
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-96(%rbp), %r13
	call	_ZNK12v8_inspector14InjectedScript15objectGroupNameERKNS_14RemoteObjectIdE@PLT
	movq	-112(%rbp), %rdx
	movq	(%rbx), %rdi
	movq	-104(%rbp), %rax
	cmpq	%r13, %rdx
	je	.L490
	leaq	16(%rbx), %rsi
	movq	-96(%rbp), %rcx
	cmpq	%rsi, %rdi
	je	.L491
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	16(%rbx), %rsi
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L443
	movq	%rdi, -112(%rbp)
	movq	%rsi, -96(%rbp)
.L441:
	xorl	%eax, %eax
	movq	$0, -104(%rbp)
	movw	%ax, (%rdi)
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movq	%rax, 32(%rbx)
	cmpq	%r13, %rdi
	je	.L437
	call	_ZdlPv@PLT
.L437:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-168(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L481
	call	_ZdlPv@PLT
.L481:
	movq	-192(%rbp), %rdi
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L487:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L428
	cmpq	$1, %rax
	je	.L492
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L428
	movq	%r8, %rsi
	movq	%r8, -208(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rax
	movq	-168(%rbp), %rdi
	movq	-208(%rbp), %r8
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L428:
	xorl	%esi, %esi
	movq	%rax, -160(%rbp)
	movw	%si, (%rdi,%rdx)
	movq	-104(%rbp), %rdi
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L489:
	movzwl	-88(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rax
	movq	-168(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L418
.L490:
	testq	%rax, %rax
	je	.L439
	cmpq	$1, %rax
	je	.L493
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L439
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-104(%rbp), %rax
	movq	(%rbx), %rdi
.L439:
	xorl	%edx, %edx
	movq	%rax, 8(%rbx)
	movw	%dx, (%rdi,%rax,2)
	movq	-112(%rbp), %rdi
	jmp	.L441
.L492:
	movzwl	-88(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rax
	movq	-168(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L428
.L491:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%rbx)
.L443:
	movq	%r13, -112(%rbp)
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L441
.L493:
	movzwl	-96(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-104(%rbp), %rax
	movq	(%rbx), %rdi
	jmp	.L439
.L483:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8558:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_, .-_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_
	.section	.rodata._ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"basic_string::_M_create"
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_
	.type	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_, @function
_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_:
.LFB8557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$216, %rsp
	movq	%rsi, -256(%rbp)
	movq	%rdx, %rsi
	movq	%rcx, -248(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	movl	$0, %r9d
	movw	%cx, -192(%rbp)
	cmovne	%r15, %r9
	movq	%r14, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%r9, -232(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	leaq	-112(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	-232(%rbp), %r9
	movq	-240(%rbp), %r8
	leaq	-144(%rbp), %r12
	movq	-248(%rbp), %rcx
	call	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectERKNS_8String16EPN2v85LocalINS4_5ValueEEEPNS5_INS4_7ContextEEEPS1_
	movq	-160(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L496
	call	_ZdlPv@PLT
.L496:
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jne	.L528
	testq	%rbx, %rbx
	je	.L510
	leaq	-216(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E@PLT
	movq	-216(%rbp), %rax
	movq	(%rbx), %rdi
	movq	$0, -216(%rbp)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L510
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L510
	movq	(%rdi), %rax
	call	*8(%rax)
.L510:
	movq	-104(%rbp), %r15
	movl	$1, %ebx
.L498:
	leaq	-88(%rbp), %rax
	cmpq	%rax, %r15
	je	.L512
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L512:
	movq	-208(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L494
	call	_ZdlPv@PLT
.L494:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$216, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	cmpq	$0, -256(%rbp)
	movq	-104(%rbp), %r15
	je	.L516
	movq	-96(%rbp), %rax
	movq	%r12, -160(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%r15, %rax
	addq	%rdx, %rax
	setne	%bl
	testq	%r15, %r15
	sete	%al
	andb	%al, %bl
	jne	.L530
	movq	%rdx, %r8
	movq	%r12, %rdi
	sarq	%r8
	cmpq	$14, %rdx
	ja	.L531
.L500:
	cmpq	$2, %rdx
	je	.L532
	testq	%rdx, %rdx
	je	.L503
	movq	%r15, %rsi
	movq	%r8, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	memmove@PLT
	movq	-160(%rbp), %rdi
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %rdx
.L503:
	xorl	%eax, %eax
	movq	%r8, -152(%rbp)
	movq	%r13, %rsi
	movw	%ax, (%rdi,%rdx)
	movq	-72(%rbp), %rax
	leaq	-216(%rbp), %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E@PLT
	movq	-256(%rbp), %rdx
	movq	-216(%rbp), %rax
	movq	$0, -216(%rbp)
	movq	(%rdx), %rdi
	movq	%rax, (%rdx)
	testq	%rdi, %rdi
	je	.L505
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L505
	movq	(%rdi), %rax
	call	*8(%rax)
.L505:
	movq	-160(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L507
	call	_ZdlPv@PLT
.L507:
	movq	-104(%rbp), %r15
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L516:
	xorl	%ebx, %ebx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L532:
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	-160(%rbp), %rdi
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L531:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L533
	leaq	2(%rdx), %rdi
	movq	%r8, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %r8
	movq	-232(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	%r8, -144(%rbp)
	jmp	.L500
.L529:
	call	__stack_chk_fail@PLT
.L533:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L530:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE8557:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_, .-_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb
	.type	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb, @function
_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb:
.LFB8594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movl	%r9d, -180(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movl	16(%rbx), %esi
	movq	24(%rbx), %rdi
	movl	%eax, %edx
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L551
	movl	20(%rbx), %esi
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	movq	-192(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L552
	leaq	-112(%rbp), %r11
	leaq	-88(%rbp), %rbx
	movq	%r11, %rdi
	movq	%r11, -192(%rbp)
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	movq	-192(%rbp), %r11
	cmpq	%rbx, %rdi
	je	.L542
.L546:
	movq	%r11, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-192(%rbp), %r11
.L541:
	testq	%r15, %r15
	je	.L538
.L542:
	xorl	%eax, %eax
	cmpb	$0, -180(%rbp)
	movq	%r11, %rdi
	movq	%r14, %rcx
	setne	%al
	leaq	-168(%rbp), %r9
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	$0, -168(%rbp)
	leal	1(%rax), %r8d
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
	movq	-104(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	-168(%rbp), %rax
	movq	%rax, (%r12)
.L534:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L553
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	leaq	-160(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L538
	call	_ZdlPv@PLT
.L538:
	movq	$0, (%r12)
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L552:
	movl	20(%rbx), %esi
	call	_ZN12v8_inspector16InspectedContext20createInjectedScriptEi@PLT
	cmpb	$0, 40(%rbx)
	movq	%rax, %r15
	jne	.L554
.L540:
	leaq	-112(%rbp), %r11
	leaq	-88(%rbp), %rbx
	movq	%r11, %rdi
	movq	%r11, -192(%rbp)
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	movq	-192(%rbp), %r11
	cmpq	%rbx, %rdi
	jne	.L546
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L554:
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN12v8_inspector14InjectedScript31setCustomObjectFormatterEnabledEb@PLT
	jmp	.L540
.L553:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8594:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb, .-_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_8String16Eb
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl9wrapTableEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl9wrapTableEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl9wrapTableEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE, @function
_ZN12v8_inspector22V8InspectorSessionImpl9wrapTableEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE:
.LFB8595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movl	16(%rbx), %esi
	movq	24(%rbx), %rdi
	movl	%eax, %edx
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L569
	movl	20(%rbx), %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L570
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L563
.L565:
	call	_ZdlPv@PLT
.L562:
	testq	%r14, %r14
	je	.L559
.L563:
	movq	-168(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector14InjectedScript9wrapTableEN2v85LocalINS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE@PLT
.L555:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	leaq	-160(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	$0, (%r12)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L570:
	movl	20(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector16InspectedContext20createInjectedScriptEi@PLT
	cmpb	$0, 40(%rbx)
	movq	%rax, %r14
	jne	.L572
.L561:
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L565
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L572:
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN12v8_inspector14InjectedScript31setCustomObjectFormatterEnabledEb@PLT
	jmp	.L561
.L571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8595:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl9wrapTableEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE, .-_ZN12v8_inspector22V8InspectorSessionImpl9wrapTableEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS1_10MaybeLocalINS1_5ArrayEEE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb
	.type	_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb, @function
_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb:
.LFB8596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-64(%rbp), %r12
	movq	%r12, %rdx
	subq	$88, %rsp
	movb	%sil, -84(%rbp)
	movq	24(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	20(%rdi), %eax
	movb	%sil, 40(%rdi)
	movl	16(%rdi), %esi
	movq	%r8, %rdi
	movl	%eax, -68(%rbp)
	leaq	-84(%rbp), %rax
	movq	%rax, %xmm0
	leaq	-68(%rbp), %rax
	movq	%rax, %xmm1
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	movaps	%xmm0, -64(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L573
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L573:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L580
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L580:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8596:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb, .-_ZN12v8_inspector22V8InspectorSessionImpl31setCustomObjectFormatterEnabledEb
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS_18V8RuntimeAgentImplE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS_18V8RuntimeAgentImplE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS_18V8RuntimeAgentImplE, @function
_ZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS_18V8RuntimeAgentImplE:
.LFB8598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS1_18V8RuntimeAgentImplEEUlPNS1_16InspectedContextEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-64(%rbp), %r12
	movq	%r12, %rdx
	subq	$72, %rsp
	movq	%rsi, -72(%rbp)
	movq	24(%rdi), %r8
	movl	16(%rdi), %esi
	movq	%r8, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector16InspectedContextEEZNS0_22V8InspectorSessionImpl17reportAllContextsEPNS0_18V8RuntimeAgentImplEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachContextEiRKSt8functionIFvPNS_16InspectedContextEEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L581
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L581:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L588
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L588:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8598:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS_18V8RuntimeAgentImplE, .-_ZN12v8_inspector22V8InspectorSessionImpl17reportAllContextsEPNS_18V8RuntimeAgentImplE
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl15inspectedObjectEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl15inspectedObjectEj
	.type	_ZN12v8_inspector22V8InspectorSessionImpl15inspectedObjectEj, @function
_ZN12v8_inspector22V8InspectorSessionImpl15inspectedObjectEj:
.LFB8643:
	.cfi_startproc
	endbr64
	movq	232(%rdi), %rdx
	movq	240(%rdi), %rax
	movl	%esi, %esi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	jnb	.L591
	movq	(%rdx,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8643:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl15inspectedObjectEj, .-_ZN12v8_inspector22V8InspectorSessionImpl15inspectedObjectEj
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm:
.LFB12476:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L625
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	%rdi, %r12
	subq	(%r14), %r12
	movq	%r12, %rax
	sarq	$3, %rax
	movq	%rax, -56(%rbp)
	subq	%rax, %r8
	movq	16(%r14), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	jb	.L594
	cmpq	$1, %rsi
	je	.L611
	leaq	-2(%rsi), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L596:
	movq	%rax, %r8
	addq	$1, %rax
	salq	$4, %r8
	movups	%xmm0, (%rdi,%r8)
	cmpq	%rax, %rdx
	ja	.L596
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rdi, %rdx
	cmpq	%r15, %rax
	je	.L597
.L595:
	movq	$0, (%rdx)
.L597:
	leaq	(%rdi,%r15,8), %rax
	movq	%rax, 8(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rsi, %r8
	jb	.L628
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	movq	%rax, %rbx
	cmovb	%rsi, %rbx
	addq	%rax, %rbx
	cmpq	%rdx, %rbx
	cmova	%rdx, %rbx
	leaq	0(,%rbx,8), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_Znwm@PLT
	movq	%rax, -64(%rbp)
	leaq	(%rax,%r12), %rdx
	cmpq	$1, %r15
	je	.L610
	leaq	-2(%r15), %rdi
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdi
	addq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L603:
	movq	%rax, %r8
	addq	$1, %rax
	salq	$4, %r8
	movups	%xmm0, (%rdx,%r8)
	cmpq	%rax, %rdi
	ja	.L603
	leaq	(%rdi,%rdi), %rax
	salq	$4, %rdi
	addq	%rdi, %rdx
	cmpq	%rax, %r15
	je	.L601
.L610:
	movq	$0, (%rdx)
.L601:
	movq	8(%r14), %r13
	movq	(%r14), %r12
	cmpq	%r12, %r13
	je	.L604
	movq	-64(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L608:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rbx)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L605
	movq	(%rdi), %rdx
	addq	$8, %r12
	addq	$8, %rbx
	call	*16(%rdx)
	cmpq	%r12, %r13
	jne	.L608
.L606:
	movq	(%r14), %r12
.L604:
	testq	%r12, %r12
	je	.L609
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L609:
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rbx
	addq	%r15, %rsi
	movq	%rbx, (%r14)
	leaq	(%rbx,%rsi,8), %rax
	addq	-72(%rbp), %rbx
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r12, %r13
	jne	.L608
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%rdi, %rdx
	jmp	.L595
.L628:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12476:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_default_appendEm
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB12974:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L648
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L639
	movq	16(%rdi), %rax
.L631:
	cmpq	%r15, %rax
	jb	.L651
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L635
.L654:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L652
	testq	%rdx, %rdx
	je	.L635
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L635:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L653
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L634
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L634:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L638
	call	_ZdlPv@PLT
.L638:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L635
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L652:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L635
.L653:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12974:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_:
.LFB14844:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L679
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L670
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L680
.L657:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L669:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L659
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L663:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L660
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*24(%rcx)
	cmpq	%r14, %r15
	jne	.L663
.L661:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L659:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L664
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L672
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L666:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L666
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L667
.L665:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L667:
	leaq	8(%rcx,%r9), %rcx
.L664:
	testq	%r12, %r12
	je	.L668
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L668:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L663
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L680:
	testq	%rdi, %rdi
	jne	.L658
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L670:
	movl	$8, %r13d
	jmp	.L657
.L672:
	movq	%rcx, %rdx
	jmp	.L665
.L658:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L657
.L679:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE14844:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB14855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$1152921504606846975, %rsi
	subq	$72, %rsp
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -104(%rbp)
	movq	%rax, -96(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rsi, %rax
	je	.L710
	movq	%r12, %r8
	subq	-72(%rbp), %r8
	testq	%rax, %rax
	je	.L697
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L711
.L683:
	movq	%r15, %rdi
	movq	%rdx, -112(%rbp)
	movq	%r8, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	%rax, -80(%rbp)
	addq	%rax, %r15
	movq	%r15, -88(%rbp)
	leaq	8(%rax), %r15
.L696:
	movq	(%rdx), %rax
	movq	-80(%rbp), %rcx
	movq	$0, (%rdx)
	movq	-72(%rbp), %r14
	movq	%rax, (%rcx,%r8)
	cmpq	%r14, %r12
	je	.L685
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rsi
	movq	%rcx, %r15
	leaq	-64(%rsi), %rdi
	movq	%rsi, %xmm5
	movq	%rdi, %xmm3
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm3, -64(%rbp)
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L713:
	movdqa	-64(%rbp), %xmm2
	movq	56(%r13), %rdi
	leaq	72(%r13), %rsi
	movups	%xmm2, 0(%r13)
	cmpq	%rsi, %rdi
	je	.L688
	call	_ZdlPv@PLT
.L688:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rsi
	cmpq	%rsi, %rdi
	je	.L689
	call	_ZdlPv@PLT
.L689:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L686:
	addq	$8, %r14
	addq	$8, %r15
	cmpq	%r14, %r12
	je	.L712
.L690:
	movq	(%r14), %rsi
	movq	$0, (%r14)
	movq	%rsi, (%r15)
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L686
	movq	0(%r13), %rsi
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L713
	addq	$8, %r14
	movq	%r13, %rdi
	addq	$8, %r15
	call	*%rsi
	cmpq	%r14, %r12
	jne	.L690
	.p2align 4,,10
	.p2align 3
.L712:
	movq	-80(%rbp), %rcx
	movq	%r12, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %r15
.L685:
	movq	-96(%rbp), %rax
	cmpq	%rax, %r12
	je	.L691
	subq	%r12, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L699
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L693:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%r15,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L693
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%r15,%rbx), %rdx
	addq	%r12, %rbx
	cmpq	%rax, %rsi
	je	.L694
.L692:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L694:
	leaq	8(%r15,%rdi), %r15
.L691:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L695
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L695:
	movq	-80(%rbp), %xmm0
	movq	-104(%rbp), %rax
	movq	%r15, %xmm4
	movq	-88(%rbp), %rbx
	punpcklqdq	%xmm4, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L684
	movq	$0, -88(%rbp)
	movl	$8, %r15d
	movq	$0, -80(%rbp)
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L697:
	movl	$8, %r15d
	jmp	.L683
.L699:
	movq	%r15, %rdx
	jmp	.L692
.L684:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	leaq	0(,%rsi,8), %r15
	jmp	.L683
.L710:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE14855:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv:
.LFB8632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	movq	$0, 16(%rdi)
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	movups	%xmm0, (%rdi)
	movl	$96, %edi
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -176(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm1
	leaq	_ZN12v8_inspector8protocol7Runtime8Metainfo10domainNameE(%rip), %rsi
	movq	%rax, %rbx
	leaq	16(%rax), %r13
	addq	$32, %rax
	movups	%xmm1, -32(%rax)
	leaq	56(%rbx), %r15
	movq	%rax, 16(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	xorl	%eax, %eax
	movw	%ax, 72(%rbx)
	movw	%r14w, 32(%rbx)
	leaq	-144(%rbp), %r14
	movq	$0, 24(%rbx)
	movq	%r14, %rdi
	movq	$0, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	leaq	-96(%rbp), %r13
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime8Metainfo7versionE(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, 48(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	8(%r12), %rsi
	movq	%rbx, -152(%rbp)
	movq	%rax, 88(%rbx)
	cmpq	16(%r12), %rsi
	je	.L715
	movq	$0, -152(%rbp)
	movq	%rbx, (%rsi)
	addq	$8, 8(%r12)
.L716:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L717
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L718
	movdqa	-176(%rbp), %xmm7
	movq	56(%r15), %rdi
	leaq	72(%r15), %rax
	movups	%xmm7, (%r15)
	cmpq	%rax, %rdi
	je	.L719
	call	_ZdlPv@PLT
.L719:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L720
	call	_ZdlPv@PLT
.L720:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L717:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L721
	call	_ZdlPv@PLT
.L721:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L722
	call	_ZdlPv@PLT
.L722:
	movl	$96, %edi
	call	_Znwm@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movq	%r14, %rdi
	leaq	32(%rax), %rdx
	movw	%r10w, 32(%rax)
	movdqa	-176(%rbp), %xmm3
	leaq	56(%rax), %r8
	movq	%rdx, 16(%rax)
	leaq	72(%rax), %rdx
	leaq	16(%rax), %r9
	movw	%r11w, 72(%rax)
	leaq	_ZN12v8_inspector8protocol8Debugger8Metainfo10domainNameE(%rip), %rsi
	movq	%rdx, 56(%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 64(%rax)
	movq	$0, 88(%rax)
	movups	%xmm3, (%rax)
	movq	%r8, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-200(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector8protocol8Debugger8Metainfo7versionE(%rip), %rsi
	movq	%rdx, 48(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-192(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-184(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	8(%r12), %rsi
	movq	%rdx, 88(%rax)
	movq	%rax, -152(%rbp)
	cmpq	16(%r12), %rsi
	je	.L723
	movq	$0, -152(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L724:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L725
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L726
	movdqa	-176(%rbp), %xmm7
	movq	56(%r8), %rdi
	leaq	72(%r8), %rax
	movups	%xmm7, (%r8)
	cmpq	%rax, %rdi
	je	.L727
	movq	%r8, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L727:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L728
	movq	%r8, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L728:
	movl	$96, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L725:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L729
	call	_ZdlPv@PLT
.L729:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L730
	call	_ZdlPv@PLT
.L730:
	movl	$96, %edi
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	movdqa	-176(%rbp), %xmm4
	movq	%r14, %rdi
	leaq	16(%rax), %r9
	leaq	32(%rax), %rdx
	movw	%r8w, 32(%rax)
	movq	%rdx, 16(%rax)
	leaq	72(%rax), %rdx
	leaq	56(%rax), %r8
	movq	%r9, -200(%rbp)
	xorl	%r9d, %r9d
	leaq	_ZN12v8_inspector8protocol8Profiler8Metainfo10domainNameE(%rip), %rsi
	movq	%rdx, 56(%rax)
	movw	%r9w, 72(%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 64(%rax)
	movq	$0, 88(%rax)
	movups	%xmm4, (%rax)
	movq	%r8, -192(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-200(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector8protocol8Profiler8Metainfo7versionE(%rip), %rsi
	movq	%rdx, 48(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-192(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-184(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	8(%r12), %rsi
	movq	%rdx, 88(%rax)
	movq	%rax, -152(%rbp)
	cmpq	16(%r12), %rsi
	je	.L731
	movq	$0, -152(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L732:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L733
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L734
	movdqa	-176(%rbp), %xmm0
	movq	56(%r8), %rdi
	leaq	72(%r8), %rax
	movups	%xmm0, (%r8)
	cmpq	%rax, %rdi
	je	.L735
	movq	%r8, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L735:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L736
	movq	%r8, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L736:
	movl	$96, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L733:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L737
	call	_ZdlPv@PLT
.L737:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L738
	call	_ZdlPv@PLT
.L738:
	movl	$96, %edi
	call	_Znwm@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqa	-176(%rbp), %xmm5
	leaq	32(%rax), %rdx
	movw	%si, 32(%rax)
	leaq	56(%rax), %r8
	movq	%rdx, 16(%rax)
	leaq	72(%rax), %rdx
	leaq	16(%rax), %r9
	movq	%rdx, 56(%rax)
	leaq	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo10domainNameE(%rip), %rsi
	movw	%di, 72(%rax)
	movq	%r14, %rdi
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 64(%rax)
	movq	$0, 88(%rax)
	movups	%xmm5, (%rax)
	movq	%r8, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-200(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo7versionE(%rip), %rsi
	movq	%rdx, 48(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-192(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-184(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	8(%r12), %rsi
	movq	%rdx, 88(%rax)
	movq	%rax, -152(%rbp)
	cmpq	16(%r12), %rsi
	je	.L739
	movq	$0, -152(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L740:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L741
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L742
	movdqa	-176(%rbp), %xmm2
	movq	56(%r8), %rdi
	leaq	72(%r8), %rax
	movups	%xmm2, (%r8)
	cmpq	%rax, %rdi
	je	.L743
	movq	%r8, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L743:
	movq	16(%r8), %rdi
	leaq	32(%r8), %rax
	cmpq	%rax, %rdi
	je	.L744
	movq	%r8, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L744:
	movl	$96, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L741:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L745
	call	_ZdlPv@PLT
.L745:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L746
	call	_ZdlPv@PLT
.L746:
	movl	$96, %edi
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	movdqa	-176(%rbp), %xmm6
	movq	%r14, %rdi
	leaq	32(%rax), %rdx
	movw	%cx, 72(%rax)
	leaq	56(%rax), %r8
	movq	%rdx, 16(%rax)
	xorl	%edx, %edx
	leaq	16(%rax), %r9
	leaq	_ZN12v8_inspector8protocol6Schema8Metainfo10domainNameE(%rip), %rsi
	movw	%dx, 32(%rax)
	leaq	72(%rax), %rdx
	movq	%rdx, 56(%rax)
	movq	$0, 24(%rax)
	movq	$0, 48(%rax)
	movq	$0, 64(%rax)
	movq	$0, 88(%rax)
	movups	%xmm6, (%rax)
	movq	%r8, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-200(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rdx
	movq	-184(%rbp), %rax
	movq	%r13, %rdi
	leaq	_ZN12v8_inspector8protocol6Schema8Metainfo7versionE(%rip), %rsi
	movq	%rdx, 48(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-192(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-184(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	8(%r12), %rsi
	movq	%rdx, 88(%rax)
	movq	%rax, -152(%rbp)
	cmpq	16(%r12), %rsi
	je	.L747
	movq	$0, -152(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L748:
	movq	-152(%rbp), %r13
	testq	%r13, %r13
	je	.L749
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L750
	movdqa	-176(%rbp), %xmm3
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	movups	%xmm3, 0(%r13)
	cmpq	%rax, %rdi
	je	.L751
	call	_ZdlPv@PLT
.L751:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L749:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L753
	call	_ZdlPv@PLT
.L753:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L772
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	leaq	-152(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	-152(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L723:
	leaq	-152(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L747:
	leaq	-152(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L739:
	leaq	-152(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema6DomainESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L718:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L742:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L734:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L725
.L772:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8632:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv, .-_ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl16supportedDomainsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl16supportedDomainsEv
	.type	_ZN12v8_inspector22V8InspectorSessionImpl16supportedDomainsEv, @function
_ZN12v8_inspector22V8InspectorSessionImpl16supportedDomainsEv:
.LFB8605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-80(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector22V8InspectorSessionImpl20supportedDomainsImplEv
	pxor	%xmm0, %xmm0
	movq	-80(%rbp), %r12
	movq	$0, 16(%r14)
	movups	%xmm0, (%r14)
	cmpq	%r12, -72(%rbp)
	je	.L774
	xorl	%r13d, %r13d
	leaq	-88(%rbp), %rbx
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L800:
	movq	$0, -88(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r14)
.L777:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L778
	movq	(%rdi), %rax
	call	*24(%rax)
.L778:
	movq	-72(%rbp), %r15
	movq	-80(%rbp), %r12
	addq	$1, %r13
	movq	%r15, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L799
.L781:
	leaq	(%r12,%r13,8), %rdx
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	movq	8(%r14), %rsi
	leaq	8(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, -88(%rbp)
	cmpq	16(%r14), %rsi
	jne	.L800
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol6Schema3API6DomainESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L802:
	movq	-80(%rbp), %r12
.L774:
	testq	%r12, %r12
	je	.L773
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L773:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L801
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	cmpq	%r12, %r15
	je	.L774
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rbx
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm3
	movq	%rdx, %xmm2
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -112(%rbp)
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L803:
	movdqa	-112(%rbp), %xmm1
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	movups	%xmm1, 0(%r13)
	cmpq	%rax, %rdi
	je	.L784
	call	_ZdlPv@PLT
.L784:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L785
	call	_ZdlPv@PLT
.L785:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L782:
	addq	$8, %r12
	cmpq	%r15, %r12
	je	.L802
.L786:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L782
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L803
	movq	%r13, %rdi
	call	*%rax
	jmp	.L782
.L801:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8605:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl16supportedDomainsEv, .-_ZN12v8_inspector22V8InspectorSessionImpl16supportedDomainsEv
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB14866:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L828
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L819
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L829
.L806:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L818:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L808
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L812:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L809
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*16(%rcx)
	cmpq	%r14, %r15
	jne	.L812
.L810:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L808:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L813
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L821
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L815:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L815
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L816
.L814:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L816:
	leaq	8(%rcx,%r9), %rcx
.L813:
	testq	%r12, %r12
	je	.L817
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L817:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L812
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L829:
	testq	%rdi, %rdi
	jne	.L807
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L819:
	movl	$8, %r13d
	jmp	.L806
.L821:
	movq	%rcx, %rdx
	jmp	.L814
.L807:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L806
.L828:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE14866:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl18addInspectedObjectESt10unique_ptrINS_18V8InspectorSession11InspectableESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl18addInspectedObjectESt10unique_ptrINS_18V8InspectorSession11InspectableESt14default_deleteIS3_EE
	.type	_ZN12v8_inspector22V8InspectorSessionImpl18addInspectedObjectESt10unique_ptrINS_18V8InspectorSession11InspectableESt14default_deleteIS3_EE, @function
_ZN12v8_inspector22V8InspectorSessionImpl18addInspectedObjectESt10unique_ptrINS_18V8InspectorSession11InspectableESt14default_deleteIS3_EE:
.LFB8641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	232(%rdi), %r15
	movq	240(%rdi), %rax
	cmpq	248(%rdi), %rax
	je	.L831
	cmpq	%rax, %r15
	je	.L854
	movq	-8(%rax), %rdx
	movq	$0, -8(%rax)
	movq	%rdx, (%rax)
	movq	240(%rdi), %r12
	leaq	8(%r12), %rax
	subq	$8, %r12
	movq	%rax, 240(%rdi)
	movq	%r12, %rax
	subq	%r15, %rax
	movq	%rax, %r14
	sarq	$3, %r14
	testq	%rax, %rax
	jle	.L838
	.p2align 4,,10
	.p2align 3
.L834:
	movq	-8(%r12), %rax
	movq	(%r12), %rdi
	subq	$8, %r12
	movq	$0, (%r12)
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L837
	movq	(%rdi), %rax
	call	*16(%rax)
.L837:
	subq	$1, %r14
	jne	.L834
.L838:
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	(%r15), %rdi
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L853
	movq	(%rdi), %rax
	call	*16(%rax)
.L853:
	movq	240(%rbx), %r12
.L833:
	movq	232(%rbx), %rax
	movq	%r12, %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	ja	.L844
.L830:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, (%r15)
	movq	240(%rdi), %rax
	leaq	8(%rax), %r12
	movq	%r12, 240(%rdi)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%rsi, %rdx
	leaq	232(%rdi), %rdi
	movq	%r15, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector18V8InspectorSession11InspectableESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L844:
	leaq	40(%rax), %r14
	cmpq	%r12, %r14
	je	.L830
	movq	%r14, %r13
	.p2align 4,,10
	.p2align 3
.L843:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L840
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*16(%rax)
	cmpq	%r13, %r12
	jne	.L843
.L841:
	movq	%r14, 240(%rbx)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L840:
	addq	$8, %r13
	cmpq	%r12, %r13
	jne	.L843
	jmp	.L841
	.cfi_endproc
.LFE8641:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl18addInspectedObjectESt10unique_ptrINS_18V8InspectorSession11InspectableESt14default_deleteIS3_EE, .-_ZN12v8_inspector22V8InspectorSessionImpl18addInspectedObjectESt10unique_ptrINS_18V8InspectorSession11InspectableESt14default_deleteIS3_EE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_:
.LFB14910:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L879
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L870
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L880
.L857:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L869:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L859
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L863:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L860
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*24(%rcx)
	cmpq	%r14, %r15
	jne	.L863
.L861:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L859:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L864
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L872
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L866:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L866
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L867
.L865:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L867:
	leaq	8(%rcx,%r9), %rcx
.L864:
	testq	%r12, %r12
	je	.L868
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L868:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L863
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L880:
	testq	%rdi, %rdi
	jne	.L858
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L870:
	movl	$8, %r13d
	jmp	.L857
.L872:
	movq	%rcx, %rdx
	jmp	.L865
.L858:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L857
.L879:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE14910:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	.section	.text._ZN12v8_inspector22V8InspectorSessionImpl19searchInTextByLinesERKNS_10StringViewES3_bb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImpl19searchInTextByLinesERKNS_10StringViewES3_bb
	.type	_ZN12v8_inspector22V8InspectorSessionImpl19searchInTextByLinesERKNS_10StringViewES3_bb, @function
_ZN12v8_inspector22V8InspectorSessionImpl19searchInTextByLinesERKNS_10StringViewES3_bb:
.LFB8650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rcx, %rsi
	leaq	-96(%rbp), %rcx
	pushq	%rbx
	movq	%rcx, %rdi
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -216(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	leaq	-144(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rdx, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movzbl	%r13b, %r9d
	movzbl	%bl, %r8d
	movq	%r12, %rsi
	movq	-216(%rbp), %rcx
	movq	-208(%rbp), %rdx
	leaq	-176(%rbp), %rdi
	call	_ZN12v8_inspector23searchInTextByLinesImplEPNS_18V8InspectorSessionERKNS_8String16ES4_bb@PLT
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L882
	call	_ZdlPv@PLT
.L882:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r14)
	movq	-176(%rbp), %r12
	movups	%xmm0, (%r14)
	cmpq	%r12, -168(%rbp)
	je	.L884
	xorl	%r13d, %r13d
	leaq	-184(%rbp), %rbx
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L909:
	movq	$0, -184(%rbp)
	movq	%rdx, (%rsi)
	addq	$8, 8(%r14)
.L887:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L888
	movq	(%rdi), %rax
	call	*24(%rax)
.L888:
	movq	-168(%rbp), %r15
	movq	-176(%rbp), %r12
	addq	$1, %r13
	movq	%r15, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L908
.L891:
	leaq	(%r12,%r13,8), %rax
	movq	(%rax), %rdx
	movq	$0, (%rax)
	movq	8(%r14), %rsi
	leaq	8(%rdx), %rax
	testq	%rdx, %rdx
	cmovne	%rax, %rdx
	movq	%rdx, -184(%rbp)
	cmpq	16(%r14), %rsi
	jne	.L909
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Debugger3API11SearchMatchESt14default_deleteIS5_EESaIS8_EE17_M_realloc_insertIJS8_EEEvN9__gnu_cxx17__normal_iteratorIPS8_SA_EEDpOT_
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L911:
	movq	-176(%rbp), %r12
.L884:
	testq	%r12, %r12
	je	.L881
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L881:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L910
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	cmpq	%r12, %r15
	je	.L884
	leaq	80+_ZTVN12v8_inspector8protocol8Debugger11SearchMatchE(%rip), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger11SearchMatchD0Ev(%rip), %rbx
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm3
	movq	%rdx, %xmm2
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -208(%rbp)
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L912:
	movdqa	-208(%rbp), %xmm1
	movq	24(%r13), %rdi
	leaq	40(%r13), %rax
	movups	%xmm1, 0(%r13)
	cmpq	%rax, %rdi
	je	.L894
	call	_ZdlPv@PLT
.L894:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L892:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L911
.L895:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L892
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L912
	movq	%r13, %rdi
	call	*%rax
	jmp	.L892
.L910:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8650:
	.size	_ZN12v8_inspector22V8InspectorSessionImpl19searchInTextByLinesERKNS_10StringViewES3_bb, .-_ZN12v8_inspector22V8InspectorSessionImpl19searchInTextByLinesERKNS_10StringViewES3_bb
	.section	.text._ZN12v8_inspector22V8InspectorSessionImplD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImplD2Ev
	.type	_ZN12v8_inspector22V8InspectorSessionImplD2Ev, @function
_ZN12v8_inspector22V8InspectorSessionImplD2Ev:
.LFB8534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	leaq	-72(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	192+_ZTVN12v8_inspector22V8InspectorSessionImplE(%rip), %rax
	leaq	-176(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	call	_ZN12v8_inspector22V8InspectorSessionImpl22discardInjectedScriptsEv
	movq	216(%rbx), %rsi
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-88(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L914
	call	_ZdlPv@PLT
.L914:
	movq	208(%rbx), %rsi
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	movq	-88(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L915
	call	_ZdlPv@PLT
.L915:
	movq	200(%rbx), %rsi
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	movq	-88(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L916
	call	_ZdlPv@PLT
.L916:
	movq	192(%rbx), %rsi
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-88(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L917
	call	_ZdlPv@PLT
.L917:
	movq	184(%rbx), %rsi
	movq	%r13, %rdi
	movq	(%rsi), %rax
	call	*40(%rax)
	movq	-88(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L918
	call	_ZdlPv@PLT
.L918:
	movq	24(%rbx), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15V8InspectorImpl10disconnectEPNS_22V8InspectorSessionImplE@PLT
	movq	240(%rbx), %r13
	movq	232(%rbx), %r12
	cmpq	%r12, %r13
	je	.L919
	.p2align 4,,10
	.p2align 3
.L923:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L920
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*16(%rax)
	cmpq	%r12, %r13
	jne	.L923
.L921:
	movq	232(%rbx), %r12
.L919:
	testq	%r12, %r12
	je	.L924
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L924:
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L925
	movq	(%rdi), %rax
	call	*8(%rax)
.L925:
	movq	216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L926
	movq	(%rdi), %rax
	call	*8(%rax)
.L926:
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L927
	movq	(%rdi), %rax
	call	*8(%rax)
.L927:
	movq	200(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L928
	movq	(%rdi), %rax
	call	*8(%rax)
.L928:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L929
	movq	(%rdi), %rax
	call	*8(%rax)
.L929:
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L930
	movq	(%rdi), %rax
	call	*8(%rax)
.L930:
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L931
	movq	(%rdi), %rax
	call	*24(%rax)
.L931:
	leaq	48(%rbx), %rdi
	call	_ZN12v8_inspector8protocol14UberDispatcherD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L958
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L923
	jmp	.L921
.L958:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8534:
	.size	_ZN12v8_inspector22V8InspectorSessionImplD2Ev, .-_ZN12v8_inspector22V8InspectorSessionImplD2Ev
	.set	.LTHUNK10,_ZN12v8_inspector22V8InspectorSessionImplD2Ev
	.section	.text._ZThn8_N12v8_inspector22V8InspectorSessionImplD1Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector22V8InspectorSessionImplD1Ev
	.type	_ZThn8_N12v8_inspector22V8InspectorSessionImplD1Ev, @function
_ZThn8_N12v8_inspector22V8InspectorSessionImplD1Ev:
.LFB17356:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK10
	.cfi_endproc
.LFE17356:
	.size	_ZThn8_N12v8_inspector22V8InspectorSessionImplD1Ev, .-_ZThn8_N12v8_inspector22V8InspectorSessionImplD1Ev
	.globl	_ZN12v8_inspector22V8InspectorSessionImplD1Ev
	.set	_ZN12v8_inspector22V8InspectorSessionImplD1Ev,_ZN12v8_inspector22V8InspectorSessionImplD2Ev
	.section	.text._ZN12v8_inspector22V8InspectorSessionImplD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector22V8InspectorSessionImplD0Ev
	.type	_ZN12v8_inspector22V8InspectorSessionImplD0Ev, @function
_ZN12v8_inspector22V8InspectorSessionImplD0Ev:
.LFB8536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector22V8InspectorSessionImplD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$264, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8536:
	.size	_ZN12v8_inspector22V8InspectorSessionImplD0Ev, .-_ZN12v8_inspector22V8InspectorSessionImplD0Ev
	.section	.text._ZThn8_N12v8_inspector22V8InspectorSessionImplD0Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector22V8InspectorSessionImplD0Ev
	.type	_ZThn8_N12v8_inspector22V8InspectorSessionImplD0Ev, @function
_ZThn8_N12v8_inspector22V8InspectorSessionImplD0Ev:
.LFB17340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector22V8InspectorSessionImplD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$264, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE17340:
	.size	_ZThn8_N12v8_inspector22V8InspectorSessionImplD0Ev, .-_ZThn8_N12v8_inspector22V8InspectorSessionImplD0Ev
	.weak	_ZTVN12v8_inspector18BinaryStringBufferE
	.section	.data.rel.ro.local._ZTVN12v8_inspector18BinaryStringBufferE,"awG",@progbits,_ZTVN12v8_inspector18BinaryStringBufferE,comdat
	.align 8
	.type	_ZTVN12v8_inspector18BinaryStringBufferE, @object
	.size	_ZTVN12v8_inspector18BinaryStringBufferE, 40
_ZTVN12v8_inspector18BinaryStringBufferE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector18BinaryStringBufferD1Ev
	.quad	_ZN12v8_inspector18BinaryStringBufferD0Ev
	.quad	_ZN12v8_inspector18BinaryStringBuffer6stringEv
	.weak	_ZTVN12v8_inspector22V8InspectorSessionImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector22V8InspectorSessionImplE,"awG",@progbits,_ZTVN12v8_inspector22V8InspectorSessionImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector22V8InspectorSessionImplE, @object
	.size	_ZTVN12v8_inspector22V8InspectorSessionImplE, 240
_ZTVN12v8_inspector22V8InspectorSessionImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector22V8InspectorSessionImplD1Ev
	.quad	_ZN12v8_inspector22V8InspectorSessionImplD0Ev
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl18addInspectedObjectESt10unique_ptrINS_18V8InspectorSession11InspectableESt14default_deleteIS3_EE
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl23dispatchProtocolMessageERKNS_10StringViewE
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl5stateEv
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl16supportedDomainsEv
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl28schedulePauseOnNextStatementERKNS_10StringViewES3_
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl26cancelPauseOnNextStatementEv
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl12breakProgramERKNS_10StringViewES3_
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl16setSkipAllPausesEb
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl6resumeEv
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl8stepOverEv
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl19searchInTextByLinesERKNS_10StringViewES3_bb
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl10wrapObjectEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKNS_10StringViewEb
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl12unwrapObjectEPSt10unique_ptrINS_12StringBufferESt14default_deleteIS2_EERKNS_10StringViewEPN2v85LocalINSA_5ValueEEEPNSB_INSA_7ContextEEES6_
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl18releaseObjectGroupERKNS_10StringViewE
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE
	.quad	_ZN12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv
	.quad	-8
	.quad	0
	.quad	_ZThn8_N12v8_inspector22V8InspectorSessionImplD1Ev
	.quad	_ZThn8_N12v8_inspector22V8InspectorSessionImplD0Ev
	.quad	_ZThn8_N12v8_inspector22V8InspectorSessionImpl20sendProtocolResponseEiSt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.quad	_ZThn8_N12v8_inspector22V8InspectorSessionImpl24sendProtocolNotificationESt10unique_ptrINS_8protocol12SerializableESt14default_deleteIS3_EE
	.quad	_ZThn8_N12v8_inspector22V8InspectorSessionImpl11fallThroughEiRKNS_8String16ERKNS_8protocol15ProtocolMessageE
	.quad	_ZThn8_N12v8_inspector22V8InspectorSessionImpl26flushProtocolNotificationsEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
