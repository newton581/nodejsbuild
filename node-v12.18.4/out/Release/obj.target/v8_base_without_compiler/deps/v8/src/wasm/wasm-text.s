	.file	"wasm-text.cc"
	.text
	.section	.text._ZN2v88internal4wasm7Decoder12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm7Decoder12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.type	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, @function
_ZN2v88internal4wasm7Decoder12onFirstErrorEv:
.LFB17981:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE17981:
	.size	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, .-_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.text._ZN2v88internal4wasm7DecoderD2Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD2Ev
	.type	_ZN2v88internal4wasm7DecoderD2Ev, @function
_ZN2v88internal4wasm7DecoderD2Ev:
.LFB22365:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L3
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
	.cfi_endproc
.LFE22365:
	.size	_ZN2v88internal4wasm7DecoderD2Ev, .-_ZN2v88internal4wasm7DecoderD2Ev
	.weak	_ZN2v88internal4wasm7DecoderD1Ev
	.set	_ZN2v88internal4wasm7DecoderD1Ev,_ZN2v88internal4wasm7DecoderD2Ev
	.section	.text._ZN2v88internal4wasm7DecoderD0Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD0Ev
	.type	_ZN2v88internal4wasm7DecoderD0Ev, @function
_ZN2v88internal4wasm7DecoderD0Ev:
.LFB22367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22367:
	.size	_ZN2v88internal4wasm7DecoderD0Ev, .-_ZN2v88internal4wasm7DecoderD0Ev
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0 < len"
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,"axG",@progbits,_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag:
.LFB18001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L35
.L8:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	$256, -328(%rbp)
	movq	%rdi, %r12
	movl	%esi, %ebx
	leaq	-320(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L37
	movq	-336(%rbp), %r14
	leaq	-400(%rbp), %r13
	movslq	%eax, %r15
	leaq	-416(%rbp), %rdi
	movq	%r13, -416(%rbp)
	testq	%r14, %r14
	je	.L38
	movq	%r15, -424(%rbp)
	cmpl	$15, %eax
	jg	.L39
	movq	%r13, %rdi
	cmpl	$1, %eax
	jne	.L13
	movzbl	(%r14), %eax
	movq	%r13, %rdx
	movb	%al, -400(%rbp)
	movl	$1, %eax
.L14:
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	-416(%rbp), %rax
	movl	%ebx, -384(%rbp)
	movq	%r14, -376(%rbp)
	cmpq	%r13, %rax
	je	.L40
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %rdx
	movq	%rax, -376(%rbp)
	movq	%r13, -416(%rbp)
	movq	48(%r12), %rdi
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	$0, -408(%rbp)
	movb	$0, -400(%rbp)
	movl	%ebx, 40(%r12)
	cmpq	%r14, %rax
	je	.L16
	leaq	64(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L41
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	64(%r12), %rsi
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%r12)
	testq	%rdi, %rdi
	je	.L22
	movq	%rdi, -376(%rbp)
	movq	%rsi, -360(%rbp)
.L20:
	movq	$0, -368(%rbp)
	movb	$0, (%rdi)
	movq	-376(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	-416(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	(%r12), %rax
	leaq	_ZN2v88internal4wasm7Decoder12onFirstErrorEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L8
	movq	%r12, %rdi
	call	*%rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	-424(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -416(%rbp)
	movq	%rax, %rdi
	movq	-424(%rbp), %rax
	movq	%rax, -400(%rbp)
.L13:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-424(%rbp), %rax
	movq	-416(%rbp), %rdx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L40:
	movq	-408(%rbp), %rdx
	movq	48(%r12), %rdi
	movl	%ebx, 40(%r12)
	movdqa	-400(%rbp), %xmm2
	movq	$0, -408(%rbp)
	movq	%rdx, -368(%rbp)
	movb	$0, -400(%rbp)
	movups	%xmm2, -360(%rbp)
.L16:
	testq	%rdx, %rdx
	je	.L18
	cmpq	$1, %rdx
	je	.L42
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
.L18:
	movq	%rdx, 56(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-376(%rbp), %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 56(%r12)
.L22:
	movq	%r14, -376(%rbp)
	leaq	-360(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L42:
	movzbl	-360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L36:
	call	__stack_chk_fail@PLT
.L38:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE18001:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEjPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz:
.LFB17979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L44
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L44:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17979:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.section	.rodata._ZN2v88internal4wasm7Decoder5errorEPKhPKc.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%s"
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKhPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKhPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKhPKc:
.LFB17977:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	subq	8(%rdi), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE17977:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.type	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh, @function
_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh:
.LFB20336:
	.cfi_startproc
	endbr64
	movzbl	1(%rcx), %edx
	movl	$1, (%rdi)
	movb	$0, 4(%rdi)
	leal	-64(%rdx), %eax
	movl	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	cmpb	$63, %al
	ja	.L50
	leaq	.L52(%rip), %rsi
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"aG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.align 4
	.align 4
.L52:
	.long	.L49-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L59-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L58-.L52
	.long	.L57-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L56-.L52
	.long	.L55-.L52
	.long	.L54-.L52
	.long	.L53-.L52
	.long	.L51-.L52
	.section	.text._ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh,"axG",@progbits,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC5ERKNS1_12WasmFeaturesEPS3_PKh,comdat
	.p2align 4,,10
	.p2align 3
.L50:
	movl	%edx, %esi
	movb	$10, 4(%rdi)
	andl	$127, %esi
	movl	%esi, %eax
	sall	$25, %eax
	sarl	$25, %eax
	testb	%dl, %dl
	js	.L66
.L65:
	movl	%eax, 8(%rdi)
.L49:
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movb	$1, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	movb	$9, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movb	$6, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movb	$7, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movb	$5, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movb	$4, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movb	$3, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movb	$2, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movzbl	2(%rcx), %edx
	movzbl	%sil, %esi
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%esi, %eax
	testb	%dl, %dl
	js	.L67
	sall	$18, %eax
	movl	$2, (%rdi)
	sarl	$18, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movzbl	3(%rcx), %esi
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L68
	sall	$11, %eax
	movl	$3, (%rdi)
	sarl	$11, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L68:
	movzbl	4(%rcx), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %eax
	testb	%sil, %sil
	js	.L69
	sall	$4, %eax
	movl	$4, (%rdi)
	sarl	$4, %eax
	jmp	.L65
.L69:
	movzbl	5(%rcx), %edx
	movl	$5, (%rdi)
	sall	$28, %edx
	orl	%edx, %eax
	jmp	.L65
	.cfi_endproc
.LFE20336:
	.size	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh, .-_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.weak	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	.set	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh,_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC2ERKNS1_12WasmFeaturesEPS3_PKh
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"<stmt>"
.LC5:
	.string	"<bot>"
.LC6:
	.string	"<unknown>"
.LC7:
	.string	"i64"
.LC8:
	.string	"i32"
.LC9:
	.string	"f64"
.LC10:
	.string	"anyref"
.LC11:
	.string	"funcref"
.LC12:
	.string	"nullref"
.LC13:
	.string	"exn"
.LC14:
	.string	"s128"
.LC15:
	.string	"f32"
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"../deps/v8/src/wasm/wasm-text.cc:43"
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE.str1.1
.LC17:
	.string	"func"
.LC18:
	.string	"_.+-*/\\^~=<>!?@#$%&|:'`"
.LC19:
	.string	" (param"
.LC20:
	.string	" (result"
.LC21:
	.string	"\n"
.LC22:
	.string	"(local"
.LC23:
	.string	")\n"
.LC24:
	.string	"vector::_M_realloc_insert"
.LC25:
	.string	" (type "
.LC26:
	.string	")"
.LC27:
	.string	" "
.LC28:
	.string	"end"
.LC29:
	.string	"br_table"
.LC30:
	.string	"invalid select type"
.LC31:
	.string	"i32.const "
.LC32:
	.string	"i64.const "
.LC33:
	.string	"f32.const "
.LC34:
	.string	"f64.const "
.LC35:
	.string	" offset="
.LC36:
	.string	" align="
.LC37:
	.string	"unreachable code"
.LC38:
	.string	" $"
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.type	_ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE, @function
_ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE:
.LFB18422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %edx
	pxor	%xmm0, %xmm0
	salq	$5, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	leaq	-288(%rbp), %rcx
	subq	$472, %rsp
	movq	%rsi, -416(%rbp)
	addq	136(%rdi), %rdx
	movq	%rcx, %rdi
	movq	%rdx, %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	movq	%rdx, -408(%rbp)
	leaq	.LC16(%rip), %rdx
	movq	%rax, -400(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rax, %rsi
	movq	%rcx, -504(%rbp)
	movups	%xmm0, -392(%rbp)
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movl	$4, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE@PLT
	movq	%rax, -432(%rbp)
	movq	%rdx, -424(%rbp)
	testq	%rdx, %rdx
	je	.L71
	movq	%rax, %r14
	addq	%rdx, %r14
	cmpq	%r14, %rax
	je	.L72
	movq	%rax, %r13
	leaq	.LC18(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L74:
	movsbl	0(%r13), %esi
	leal	-48(%rsi), %eax
	cmpb	$9, %al
	jbe	.L73
	movl	%esi, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L73
	movq	%r15, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L71
.L73:
	addq	$1, %r13
	cmpq	%r13, %r14
	jne	.L74
.L72:
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movslq	-424(%rbp), %rdx
	movq	-432(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
.L71:
	movq	-408(%rbp), %rax
	leaq	-336(%rbp), %r14
	movq	(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L470
	cmpq	$0, (%rax)
	jne	.L471
.L92:
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %rcx
	movq	-504(%rbp), %rax
	movl	$0, -336(%rbp)
	movq	-416(%rbp), %rdi
	movq	$0, -320(%rbp)
	movq	%rax, -328(%rbp)
	movq	$0, -312(%rbp)
	movq	(%rdi), %rsi
	movq	$0, -304(%rbp)
	movl	16(%rcx), %eax
	movl	%eax, %edx
	addl	20(%rcx), %edx
	movq	%r14, %rcx
	addq	%rsi, %rdx
	addq	%rax, %rsi
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal4wasm16BytecodeIteratorC1EPKhS4_PNS1_14BodyLocalDeclsE@PLT
	movq	-312(%rbp), %rax
	movl	$1, -424(%rbp)
	cmpq	%rax, -320(%rbp)
	je	.L109
	movl	$6, %edx
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %r14
	leaq	-368(%rbp), %r15
	leaq	.L113(%rip), %r13
	cmpq	%rax, %r14
	je	.L125
	movq	%r12, -408(%rbp)
	movq	%r14, %r12
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%rbx, %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$32, -368(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$10, (%r12)
	movq	%rax, %rdi
	ja	.L111
	movzbl	(%r12), %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE,"a",@progbits
	.align 4
	.align 4
.L113:
	.long	.L123-.L113
	.long	.L346-.L113
	.long	.L121-.L113
	.long	.L120-.L113
	.long	.L119-.L113
	.long	.L118-.L113
	.long	.L117-.L113
	.long	.L116-.L113
	.long	.L115-.L113
	.long	.L114-.L113
	.long	.L112-.L113
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
.L120:
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L122:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$1, %r12
	cmpq	%r12, %r14
	jne	.L124
	movq	-408(%rbp), %r12
.L125:
	movl	$2, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, -424(%rbp)
.L109:
	movq	-192(%rbp), %rax
	cmpq	%rax, -184(%rbp)
	jbe	.L126
	movl	$1, -416(%rbp)
	movabsq	$2314885530818453536, %r14
	movabsq	$2314885530818453536, %r15
	.p2align 4,,10
	.p2align 3
.L326:
	movzbl	(%rax), %r13d
	movl	%r13d, %edx
	movl	%r13d, %ecx
	andl	$-3, %edx
	cmpb	$5, %dl
	je	.L381
	cmpl	$11, %r13d
	jne	.L127
.L381:
	subl	$1, -416(%rbp)
.L127:
	movl	-416(%rbp), %edi
	movl	$64, %esi
	leal	(%rdi,%rdi), %edx
	cmpl	$64, %edx
	cmovge	%esi, %edx
	testq	%r12, %r12
	je	.L130
	movl	-176(%rbp), %esi
	subq	-200(%rbp), %rax
	movq	8(%r12), %r8
	addl	%eax, %esi
	cmpq	16(%r12), %r8
	je	.L131
	movl	-424(%rbp), %eax
	movl	%esi, (%r8)
	movl	%edx, 8(%r8)
	movl	%eax, 4(%r8)
	addq	$12, 8(%r12)
.L130:
	movslq	%edx, %rdx
	leaq	-128(%rbp), %rsi
	movq	%rbx, %rdi
	movb	%cl, -408(%rbp)
	movq	%r14, -128(%rbp)
	movq	%r15, -120(%rbp)
	movq	%r14, -112(%rbp)
	movq	%r15, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	%r15, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	%r15, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZNSo5writeEPKcl@PLT
	movzbl	-408(%rbp), %ecx
	cmpb	$-2, %cl
	ja	.L140
	leaq	.L142(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L142:
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L165-.L142
	.long	.L165-.L142
	.long	.L165-.L142
	.long	.L164-.L142
	.long	.L165-.L142
	.long	.L164-.L142
	.long	.L163-.L142
	.long	.L145-.L142
	.long	.L162-.L142
	.long	.L161-.L142
	.long	.L153-.L142
	.long	.L153-.L142
	.long	.L159-.L142
	.long	.L145-.L142
	.long	.L153-.L142
	.long	.L157-.L142
	.long	.L153-.L142
	.long	.L157-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L156-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L153-.L142
	.long	.L153-.L142
	.long	.L153-.L142
	.long	.L153-.L142
	.long	.L153-.L142
	.long	.L153-.L142
	.long	.L153-.L142
	.long	.L140-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L152-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L151-.L142
	.long	.L150-.L142
	.long	.L149-.L142
	.long	.L148-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L146-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L145-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L140-.L142
	.long	.L144-.L142
	.long	.L143-.L142
	.long	.L141-.L142
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.p2align 4,,10
	.p2align 3
.L145:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L418
.L325:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-368(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movl	$1, %edx
	movb	$10, -368(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-192(%rbp), %rdi
	movq	-184(%rbp), %rsi
	addl	$1, -424(%rbp)
	cmpq	%rsi, %rdi
	jb	.L472
.L126:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-160(%rbp), %rdi
	movq	%rax, -208(%rbp)
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L340
	call	_ZdlPv@PLT
.L340:
	movq	-504(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-512(%rbp), %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L473
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	-192(%rbp), %rax
	movzbl	1(%rax), %edx
	movl	%edx, %ecx
	andl	$127, %ecx
	movl	%ecx, -408(%rbp)
	movl	$2, %ecx
	testb	%dl, %dl
	js	.L474
.L252:
	addq	%rcx, %rax
	movzbl	(%rax), %edx
	movl	%edx, %ecx
	andl	$127, %ecx
	movl	%ecx, -448(%rbp)
	testb	%dl, %dl
	js	.L475
.L253:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L476
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L255:
	leaq	.LC35(%rip), %rsi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-448(%rbp), %esi
.L416:
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$7, %edx
	leaq	.LC36(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-408(%rbp), %ecx
	movl	$1, %esi
	movq	%r13, %rdi
	salq	%cl, %rsi
	call	_ZNSo9_M_insertIyEERSoT_@PLT
	leaq	-368(%rbp), %r8
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-192(%rbp), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	movl	%ecx, -408(%rbp)
	testb	%al, %al
	js	.L477
.L211:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L478
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L213:
	leaq	-368(%rbp), %r8
	movb	$32, -368(%rbp)
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r8, -448(%rbp)
	movq	%r8, %rsi
.L419:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-408(%rbp), %esi
	movq	%rax, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-448(%rbp), %r8
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	-368(%rbp), %r8
	movq	-192(%rbp), %rcx
	movq	-432(%rbp), %rdx
	leaq	_ZN2v88internal4wasmL16kAllWasmFeaturesE(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -408(%rbp)
	call	_ZN2v88internal4wasm18BlockTypeImmediateILNS1_7Decoder12ValidateFlagE0EEC1ERKNS1_12WasmFeaturesEPS3_PKh
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	-408(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L479
	movq	%rax, %rdi
	movq	%r8, -408(%rbp)
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-364(%rbp), %eax
	movq	-408(%rbp), %r8
	cmpb	$10, %al
	je	.L480
.L168:
	testb	%al, %al
	je	.L169
	movl	$1, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -408(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-364(%rbp), %eax
	movq	-408(%rbp), %r8
	cmpb	$10, %al
	jne	.L170
	movq	-352(%rbp), %rax
	movq	16(%rax), %rax
	movzbl	(%rax), %eax
.L170:
	cmpb	$10, %al
	ja	.L177
	leaq	.L328(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L328:
	.long	.L175-.L328
	.long	.L332-.L328
	.long	.L327-.L328
	.long	.L331-.L328
	.long	.L330-.L328
	.long	.L174-.L328
	.long	.L329-.L328
	.long	.L379-.L328
	.long	.L171-.L328
	.long	.L173-.L328
	.long	.L176-.L328
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
.L157:
	movq	-192(%rbp), %rax
	movzbl	1(%rax), %edx
	movl	%edx, %ecx
	andl	$127, %ecx
	movl	%ecx, -408(%rbp)
	movzbl	2(%rax), %ecx
	testb	%dl, %dl
	jns	.L211
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, -408(%rbp)
	movl	-408(%rbp), %edi
	testb	%cl, %cl
	jns	.L211
	movzbl	3(%rax), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %edi
	movl	%edi, -408(%rbp)
	testb	%cl, %cl
	jns	.L211
	movzbl	4(%rax), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %edi
	movl	%edi, -408(%rbp)
	testb	%cl, %cl
	jns	.L211
	movzbl	5(%rax), %eax
	sall	$28, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L164:
	movl	%r13d, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L481
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L191:
	addl	$1, -416(%rbp)
	leaq	-368(%rbp), %r8
	jmp	.L178
.L163:
	movq	-192(%rbp), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	js	.L482
.L205:
	movl	$8, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L424
.L206:
	movq	%rsi, %rdi
	movq	%rsi, -408(%rbp)
	call	strlen@PLT
	movq	-408(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L207:
	leaq	-368(%rbp), %r8
	movb	$32, -368(%rbp)
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r8, -408(%rbp)
	movq	%r8, %rsi
.L417:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-408(%rbp), %r8
	jmp	.L178
.L159:
	movq	-192(%rbp), %r13
	movl	$2, %edx
	movzbl	1(%r13), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	movl	%ecx, -448(%rbp)
	testb	%al, %al
	js	.L483
.L192:
	addq	%rdx, %r13
	leaq	.LC29(%rip), %rsi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, -464(%rbp)
	leaq	-368(%rbp), %r8
	movl	$0, -408(%rbp)
	movq	%r15, -456(%rbp)
	movq	%r13, %r15
	movq	%r12, %r13
	movq	%r8, %r12
.L193:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movb	$32, -368(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%rax, %rdi
	movzbl	(%r15), %eax
	movl	%eax, %esi
	andl	$127, %esi
	testb	%al, %al
	js	.L484
.L194:
	addq	%rdx, %r15
	call	_ZNSo9_M_insertImEERSoT_@PLT
	addl	$1, -408(%rbp)
	movl	-408(%rbp), %eax
	cmpl	%eax, -448(%rbp)
	jnb	.L193
	movq	%r12, %r8
	movq	-464(%rbp), %r14
	movq	%r13, %r12
	movq	-456(%rbp), %r15
	jmp	.L178
.L161:
	movl	$3, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-368(%rbp), %r8
	jmp	.L178
.L162:
	movq	-192(%rbp), %rax
	movzbl	1(%rax), %ecx
	movzbl	2(%rax), %edx
	movl	%ecx, %edi
	andl	$127, %edi
	movl	%edi, -408(%rbp)
	testb	%cl, %cl
	js	.L182
	leaq	1(%rax), %rcx
.L183:
	movl	%edx, %r13d
	andl	$127, %r13d
	testb	%dl, %dl
	js	.L485
.L187:
	movl	$10, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L426
.L286:
	movq	%rsi, %rdi
	movq	%rsi, -448(%rbp)
	call	strlen@PLT
	movq	-448(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L287:
	leaq	-368(%rbp), %r8
	movl	$1, %edx
	movq	%rbx, %rdi
	movb	$32, -368(%rbp)
	movq	%r8, %rsi
	movq	%r8, -448(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-408(%rbp), %esi
	movq	%rax, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-448(%rbp), %r8
	movl	$1, %edx
	movb	$32, -368(%rbp)
	movq	%rax, %rdi
	movq	%r8, -408(%rbp)
	movq	%r8, %rsi
	jmp	.L417
.L143:
	movq	-192(%rbp), %rdx
	movzwl	(%rdx), %eax
	rolw	$8, %ax
	addw	$768, %ax
	cmpw	$209, %ax
	ja	.L140
	leaq	.L303(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L303:
	.long	.L306-.L303
	.long	.L306-.L303
	.long	.L140-.L303
	.long	.L305-.L303
	.long	.L302-.L303
	.long	.L304-.L303
	.long	.L140-.L303
	.long	.L304-.L303
	.long	.L302-.L303
	.long	.L304-.L303
	.long	.L140-.L303
	.long	.L304-.L303
	.long	.L302-.L303
	.long	.L304-.L303
	.long	.L304-.L303
	.long	.L302-.L303
	.long	.L304-.L303
	.long	.L304-.L303
	.long	.L302-.L303
	.long	.L304-.L303
	.long	.L304-.L303
	.long	.L302-.L303
	.long	.L304-.L303
	.long	.L304-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L140-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.long	.L302-.L303
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
.L141:
	movq	-192(%rbp), %rdx
	movzwl	(%rdx), %eax
	rolw	$8, %ax
	movzwl	%ax, %edi
	cmpw	$-509, %ax
	jne	.L486
	movl	$65027, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L325
	.p2align 4,,10
	.p2align 3
.L418:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	leaq	-368(%rbp), %r8
	jmp	.L178
.L144:
	movq	-192(%rbp), %rdx
	movzwl	(%rdx), %eax
	rolw	$8, %ax
	addw	$1024, %ax
	cmpw	$17, %ax
	ja	.L140
	leaq	.L258(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L258:
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L260-.L258
	.long	.L260-.L258
	.long	.L262-.L258
	.long	.L262-.L258
	.long	.L261-.L258
	.long	.L260-.L258
	.long	.L259-.L258
	.long	.L257-.L258
	.long	.L257-.L258
	.long	.L257-.L258
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
.L260:
	movzbl	2(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	movl	%ecx, %r13d
	sall	$25, %r13d
	sarl	$25, %r13d
	testb	%al, %al
	js	.L487
.L299:
	movl	$252, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L206
.L424:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L207
.L146:
	movq	-192(%rbp), %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	js	.L488
.L249:
	movl	$210, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L206
	jmp	.L424
.L156:
	movq	-192(%rbp), %rsi
	movl	$2, %eax
	cmpb	$0, 1(%rsi)
	js	.L489
.L214:
	movzbl	(%rsi,%rax), %eax
	subl	$64, %eax
	cmpb	$63, %al
	ja	.L215
	leaq	.L217(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L217:
	.long	.L361-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L224-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L223-.L217
	.long	.L222-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L221-.L217
	.long	.L220-.L217
	.long	.L362-.L217
	.long	.L218-.L217
	.long	.L216-.L217
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$10, %r13d
.L225:
	movq	-432(%rbp), %rdi
	addq	$1, %rsi
	leaq	.LC30(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L341:
	movl	$28, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	je	.L490
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	strlen@PLT
	movq	-408(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L227:
	leaq	-368(%rbp), %r8
	movq	%rbx, %rdi
	movl	$1, %edx
	movb	$32, -368(%rbp)
	movq	%r8, %rsi
	movq	%r8, -408(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$10, %r13b
	movq	-408(%rbp), %r8
	movq	%rax, %rdi
	ja	.L232
	leaq	.L334(%rip), %rcx
	movslq	(%rcx,%r13,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L334:
	.long	.L232-.L334
	.long	.L338-.L334
	.long	.L333-.L334
	.long	.L337-.L334
	.long	.L336-.L334
	.long	.L231-.L334
	.long	.L335-.L334
	.long	.L380-.L334
	.long	.L228-.L334
	.long	.L230-.L334
	.long	.L233-.L334
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
.L150:
	movq	-192(%rbp), %rcx
	movzbl	1(%rcx), %eax
	movl	%eax, %edx
	andl	$127, %edx
	movq	%rdx, %r13
	salq	$57, %r13
	sarq	$57, %r13
	testb	%al, %al
	js	.L491
.L248:
	movl	$10, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	leaq	-368(%rbp), %r8
	jmp	.L178
.L151:
	movq	-192(%rbp), %rcx
	movzbl	1(%rcx), %eax
	movl	%eax, %edx
	andl	$127, %edx
	movl	%edx, %r13d
	sall	$25, %r13d
	sarl	$25, %r13d
	testb	%al, %al
	js	.L492
.L238:
	movl	$10, %edx
	leaq	.LC31(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZNSolsEi@PLT
	leaq	-368(%rbp), %r8
	jmp	.L178
.L148:
	movq	-192(%rbp), %rax
	movl	$10, %edx
	leaq	.LC34(%rip), %rsi
	movq	%rbx, %rdi
	movq	1(%rax), %xmm0
	movq	%xmm0, -408(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rdi
	movq	-408(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	leaq	-368(%rbp), %r8
	jmp	.L178
.L149:
	movq	-192(%rbp), %rax
	movl	$10, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rbx, %rdi
	movl	1(%rax), %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movd	%r13d, %xmm1
	cvtss2sd	%xmm1, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	leaq	-368(%rbp), %r8
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L472:
	call	_ZN2v88internal4wasm12OpcodeLengthEPKhS3_@PLT
	movq	-184(%rbp), %rdx
	movl	%eax, %eax
	addq	-192(%rbp), %rax
	movq	%rax, -192(%rbp)
	cmpq	%rdx, %rax
	jb	.L326
	movq	%rdx, -192(%rbp)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%r12), %r9
	movq	%r8, %r10
	movabsq	$-6148914691236517205, %rdi
	subq	%r9, %r10
	movq	%r10, %rax
	sarq	$2, %rax
	imulq	%rdi, %rax
	movabsq	$768614336404564650, %rdi
	cmpq	%rdi, %rax
	je	.L493
	testq	%rax, %rax
	movl	$1, %r11d
	cmovne	%rax, %r11
	addq	%r11, %rax
	jc	.L134
	testq	%rax, %rax
	je	.L494
	cmpq	%rdi, %rax
	cmova	%rdi, %rax
	leaq	(%rax,%rax,2), %r11
	salq	$2, %r11
.L135:
	movq	%r11, %rdi
	movq	%r10, -496(%rbp)
	movq	%r9, -488(%rbp)
	movq	%r8, -480(%rbp)
	movl	%esi, -472(%rbp)
	movb	%cl, -464(%rbp)
	movl	%edx, -448(%rbp)
	movq	%r11, -408(%rbp)
	call	_Znwm@PLT
	movq	-408(%rbp), %r11
	movl	-448(%rbp), %edx
	leaq	12(%rax), %rcx
	movl	-472(%rbp), %esi
	movq	-480(%rbp), %r8
	movq	%rcx, -408(%rbp)
	movq	-488(%rbp), %r9
	addq	%rax, %r11
	movzbl	-464(%rbp), %ecx
	movq	-496(%rbp), %r10
.L136:
	movl	-424(%rbp), %edi
	addq	%rax, %r10
	movl	%esi, (%r10)
	movl	%edi, 4(%r10)
	movl	%edx, 8(%r10)
	cmpq	%r9, %r8
	je	.L137
	movq	%rax, %rdi
	movq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%rsi), %r10
	addq	$12, %rsi
	addq	$12, %rdi
	movq	%r10, -12(%rdi)
	movl	-4(%rsi), %r10d
	movl	%r10d, -4(%rdi)
	cmpq	%rsi, %r8
	jne	.L138
	subq	$12, %r8
	subq	%r9, %r8
	shrq	$2, %r8
	leaq	24(%rax,%r8,4), %rdi
	movq	%rdi, -408(%rbp)
.L137:
	testq	%r9, %r9
	je	.L139
	movq	%r9, %rdi
	movq	%r11, -480(%rbp)
	movq	%rax, -472(%rbp)
	movb	%cl, -464(%rbp)
	movl	%edx, -448(%rbp)
	call	_ZdlPv@PLT
	movq	-480(%rbp), %r11
	movq	-472(%rbp), %rax
	movzbl	-464(%rbp), %ecx
	movl	-448(%rbp), %edx
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%rax, %xmm0
	movq	%r11, 16(%r12)
	movhps	-408(%rbp), %xmm0
	movups	%xmm0, (%r12)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L478:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L477:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, -408(%rbp)
	movl	-408(%rbp), %edi
	testb	%cl, %cl
	jns	.L211
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	testb	%cl, %cl
	jns	.L211
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	testb	%cl, %cl
	jns	.L211
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L486:
	cmpl	$65027, %edi
	jg	.L318
	leal	-65024(%rdi), %eax
	cmpl	$2, %eax
	ja	.L140
.L319:
	movzbl	2(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	movl	%ecx, -408(%rbp)
	movl	$3, %ecx
	testb	%al, %al
	js	.L495
.L320:
	addq	%rcx, %rdx
	movzbl	(%rdx), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	js	.L496
.L321:
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L430
.L309:
	movq	%rsi, %rdi
	movq	%rsi, -448(%rbp)
	call	strlen@PLT
	movq	-448(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L310:
	leaq	.LC35(%rip), %rsi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	jmp	.L416
.L331:
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%rbx, %rdi
	movq	%r8, -408(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %r8
.L169:
	addl	$1, -416(%rbp)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L318:
	leal	-65040(%rdi), %eax
	cmpl	$62, %eax
	jbe	.L319
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	.LC37(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L494:
	movq	$12, -408(%rbp)
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	jmp	.L136
.L261:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	andl	$127, %eax
	testb	%cl, %cl
	js	.L497
	sall	$25, %eax
	sarl	$25, %eax
	movl	%eax, -408(%rbp)
	movl	$2, %eax
.L284:
	addq	%rax, %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	js	.L498
.L285:
	movl	$252, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L286
	.p2align 4,,10
	.p2align 3
.L426:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L490:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L479:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movzbl	-364(%rbp), %eax
	movq	-408(%rbp), %r8
	cmpb	$10, %al
	jne	.L168
.L480:
	movl	$7, %edx
	leaq	.LC25(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -408(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-360(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$1, -416(%rbp)
	movq	-408(%rbp), %r8
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L476:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L481:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$253, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L325
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$252, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L325
	jmp	.L418
.L346:
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L122
.L118:
	movl	$4, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L122
.L119:
	movl	$3, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L122
.L121:
	movl	$3, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L122
.L123:
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L122
.L112:
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L122
.L114:
	movl	$3, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L122
.L115:
	movl	$7, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L122
.L116:
	movl	$7, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L122
.L117:
	movl	$6, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L122
.L257:
	movzbl	2(%rdx), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	jns	.L299
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L299
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L299
	movzbl	5(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L299
	movzbl	6(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$253, %edi
	movzbl	2(%rdx), %r13d
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	je	.L499
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	strlen@PLT
	movq	-408(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L315:
	leaq	-368(%rbp), %r8
	movl	$1, %edx
	movq	%rbx, %rdi
	movb	$32, -368(%rbp)
	movq	%r8, %rsi
	movq	%r8, -408(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %r8
	movl	$1, %edx
	movb	%r13b, -368(%rbp)
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %r8
	jmp	.L178
.L259:
	movzbl	2(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	movl	%ecx, -408(%rbp)
	movl	$2, %ecx
	testb	%al, %al
	js	.L500
.L295:
	addq	%rcx, %rdx
	movzbl	1(%rdx), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	js	.L501
.L296:
	movl	$252, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	testq	%rax, %rax
	je	.L502
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	strlen@PLT
	movq	-448(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L298:
	leaq	-368(%rbp), %r8
	movl	$1, %edx
	movq	%rbx, %rdi
	movb	$32, -368(%rbp)
	movq	%r8, %rsi
	movq	%r8, -448(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-448(%rbp), %r8
	movl	$1, %edx
	movb	$32, -368(%rbp)
	movq	%rax, %rdi
	movq	%r8, %rsi
	jmp	.L419
.L306:
	movzbl	1(%rdx), %eax
	movl	%eax, %ecx
	andl	$127, %ecx
	movl	%ecx, -408(%rbp)
	movl	$2, %ecx
	testb	%al, %al
	js	.L503
.L307:
	addq	%rcx, %rdx
	movzbl	(%rdx), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	js	.L504
.L308:
	movl	$253, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L309
.L430:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L310
.L305:
	movdqu	2(%rdx), %xmm2
	movl	$253, %edi
	movaps	%xmm2, -224(%rbp)
	call	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L505
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L312:
	movq	%r12, -408(%rbp)
	leaq	-224(%rbp), %r13
	leaq	-368(%rbp), %r8
	movq	%r14, -448(%rbp)
	movq	%r15, -440(%rbp)
	movq	%r13, %r15
	movq	%r8, %r13
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movzbl	(%r15), %r12d
	movb	$32, -368(%rbp)
	addq	$1, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	%r12b, -368(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, -432(%rbp)
	jne	.L313
	movq	-408(%rbp), %r12
	movq	-448(%rbp), %r14
	movq	%r13, %r8
	movq	-440(%rbp), %r15
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L492:
	movzbl	2(%rcx), %esi
	movzbl	%dl, %edx
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %edx
	movl	%edx, %r13d
	sall	$18, %r13d
	sarl	$18, %r13d
	testb	%sil, %sil
	jns	.L238
	movzbl	3(%rcx), %esi
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edx
	movl	%edx, %r13d
	sall	$11, %r13d
	sarl	$11, %r13d
	testb	%sil, %sil
	jns	.L238
	movzbl	4(%rcx), %esi
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edx
	movl	%edx, %r13d
	sall	$4, %r13d
	sarl	$4, %r13d
	testb	%sil, %sil
	jns	.L238
	movzbl	5(%rcx), %r13d
	sall	$28, %r13d
	orl	%edx, %r13d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L491:
	movzbl	2(%rcx), %esi
	movzbl	%dl, %edx
	movq	%rsi, %rax
	salq	$7, %rax
	andl	$16256, %eax
	orq	%rax, %rdx
	movq	%rdx, %r13
	salq	$50, %r13
	sarq	$50, %r13
	testb	%sil, %sil
	jns	.L248
	movzbl	3(%rcx), %esi
	movq	%rsi, %rax
	salq	$14, %rax
	andl	$2080768, %eax
	orq	%rax, %rdx
	movq	%rdx, %r13
	salq	$43, %r13
	sarq	$43, %r13
	testb	%sil, %sil
	jns	.L248
	movzbl	4(%rcx), %esi
	movq	%rsi, %rax
	salq	$21, %rax
	andl	$266338304, %eax
	orq	%rdx, %rax
	movq	%rax, %r13
	salq	$36, %r13
	sarq	$36, %r13
	testb	%sil, %sil
	jns	.L248
	movabsq	$34091302912, %rdi
	movzbl	5(%rcx), %esi
	movq	%rsi, %rdx
	salq	$28, %rdx
	andq	%rdi, %rdx
	orq	%rax, %rdx
	movq	%rdx, %r13
	salq	$29, %r13
	sarq	$29, %r13
	testb	%sil, %sil
	jns	.L248
	movabsq	$4363686772736, %rdi
	movzbl	6(%rcx), %esi
	movq	%rsi, %rax
	salq	$35, %rax
	andq	%rdi, %rax
	orq	%rax, %rdx
	movq	%rdx, %r13
	salq	$22, %r13
	sarq	$22, %r13
	testb	%sil, %sil
	jns	.L248
	movzbl	7(%rcx), %esi
	movl	$127, %edi
	salq	$42, %rdi
	movq	%rsi, %rax
	salq	$42, %rax
	andq	%rdi, %rax
	orq	%rdx, %rax
	testb	%sil, %sil
	js	.L506
	salq	$15, %rax
	sarq	$15, %rax
	movq	%rax, %r13
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L483:
	movzbl	2(%r13), %ecx
	movl	$3, %edx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, -448(%rbp)
	movl	-448(%rbp), %edi
	testb	%cl, %cl
	jns	.L192
	movzbl	3(%r13), %ecx
	movl	$4, %edx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edi
	movl	%edi, -448(%rbp)
	testb	%cl, %cl
	jns	.L192
	movzbl	4(%r13), %ecx
	movl	$5, %edx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edi
	movl	%edi, -448(%rbp)
	testb	%cl, %cl
	jns	.L192
	movzbl	5(%r13), %eax
	movl	$6, %edx
	sall	$28, %eax
	orl	%eax, %edi
	movl	%edi, -448(%rbp)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L482:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L205
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L205
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L205
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L489:
	cmpb	$0, 2(%rsi)
	movl	$3, %eax
	jns	.L214
	cmpb	$0, 3(%rsi)
	movl	$4, %eax
	jns	.L214
	movsbq	4(%rsi), %rax
	shrq	$63, %rax
	addq	$5, %rax
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L488:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L249
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L249
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L249
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L485:
	movzbl	2(%rcx), %edx
	movl	%edx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%dl, %dl
	jns	.L187
	movzbl	3(%rcx), %edx
	movl	%edx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%dl, %dl
	jns	.L187
	movzbl	4(%rcx), %edx
	movl	%edx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%dl, %dl
	jns	.L187
	movzbl	5(%rcx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L475:
	movzbl	1(%rax), %ecx
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, -448(%rbp)
	movl	-448(%rbp), %edi
	testb	%cl, %cl
	jns	.L253
	movzbl	2(%rax), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %edi
	movl	%edi, -448(%rbp)
	testb	%cl, %cl
	jns	.L253
	movzbl	3(%rax), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %edi
	movl	%edi, -448(%rbp)
	testb	%cl, %cl
	jns	.L253
	movzbl	4(%rax), %eax
	sall	$28, %eax
	orl	%eax, %edi
	movl	%edi, -448(%rbp)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L474:
	movzbl	2(%rax), %esi
	movl	$3, %ecx
	movl	%esi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, -408(%rbp)
	movl	-408(%rbp), %edi
	testb	%sil, %sil
	jns	.L252
	movzbl	3(%rax), %esi
	movl	$4, %ecx
	movl	%esi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %edi
	movl	%edi, -408(%rbp)
	testb	%sil, %sil
	jns	.L252
	movzbl	4(%rax), %esi
	movl	$5, %ecx
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %edi
	movl	%edi, -408(%rbp)
	testb	%sil, %sil
	jns	.L252
	movzbl	5(%rax), %edx
	movl	$6, %ecx
	sall	$28, %edx
	orl	%edx, %edi
	movl	%edi, -408(%rbp)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L182:
	movl	%edx, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, -408(%rbp)
	testb	%dl, %dl
	js	.L184
	movzbl	3(%rax), %edx
	leaq	2(%rax), %rcx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L484:
	movzbl	1(%r15), %r8d
	movl	$2, %edx
	movl	%r8d, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %esi
	testb	%r8b, %r8b
	jns	.L194
	movzbl	2(%r15), %r8d
	movl	$3, %edx
	movl	%r8d, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %esi
	testb	%r8b, %r8b
	jns	.L194
	movzbl	3(%r15), %r8d
	movl	$4, %edx
	movl	%r8d, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %esi
	testb	%r8b, %r8b
	jns	.L194
	movzbl	4(%r15), %eax
	movl	$5, %edx
	sall	$28, %eax
	orl	%eax, %esi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$9, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L122
.L470:
	movl	$7, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %rax
	movq	(%rax), %rdx
	movq	16(%rdx), %rcx
	movq	(%rdx), %rax
	leaq	(%rcx,%rax), %r13
	addq	8(%rdx), %rax
	addq	%rcx, %rax
	cmpq	%rax, %r13
	je	.L90
	movq	%r12, -424(%rbp)
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L91:
	movzbl	0(%r13), %r12d
	movq	%rbx, %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -336(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	cmpb	$10, %r12b
	ja	.L77
	leaq	.L79(%rip), %rsi
	movzbl	%r12b, %ecx
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L79:
	.long	.L89-.L79
	.long	.L343-.L79
	.long	.L87-.L79
	.long	.L86-.L79
	.long	.L85-.L79
	.long	.L84-.L79
	.long	.L83-.L79
	.long	.L82-.L79
	.long	.L81-.L79
	.long	.L80-.L79
	.long	.L78-.L79
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
.L86:
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L88:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$1, %r13
	cmpq	%r15, %r13
	jne	.L91
	movq	-424(%rbp), %r12
.L90:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movb	$41, -336(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %rax
	movq	(%rax), %rax
	cmpq	$0, (%rax)
	je	.L92
.L471:
	movl	$8, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %r13
	movq	(%rax), %rcx
	addq	%r13, %rcx
	cmpq	%r13, %rcx
	je	.L107
	movq	%r12, -424(%rbp)
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L108:
	movzbl	0(%r13), %r12d
	movq	%rbx, %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -336(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	cmpb	$10, %r12b
	ja	.L94
	leaq	.L96(%rip), %rsi
	movzbl	%r12b, %ecx
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.align 4
	.align 4
.L96:
	.long	.L106-.L96
	.long	.L344-.L96
	.long	.L104-.L96
	.long	.L103-.L96
	.long	.L102-.L96
	.long	.L101-.L96
	.long	.L100-.L96
	.long	.L99-.L96
	.long	.L98-.L96
	.long	.L97-.L96
	.long	.L95-.L96
	.section	.text._ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
.L343:
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L88
.L87:
	movl	$3, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L88
.L89:
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L88
.L78:
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L88
.L80:
	movl	$3, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L88
.L81:
	movl	$7, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L88
.L82:
	movl	$7, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L88
.L83:
	movl	$6, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L88
.L84:
	movl	$4, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L88
.L85:
	movl	$3, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L88
.L103:
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L105:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$1, %r13
	cmpq	%r15, %r13
	jne	.L108
	movq	-424(%rbp), %r12
.L107:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movb	$41, -336(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L92
.L344:
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L105
.L104:
	movl	$3, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L105
.L106:
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L105
.L95:
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L105
.L97:
	movl	$3, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L105
.L98:
	movl	$7, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L105
.L99:
	movl	$7, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L105
.L100:
	movl	$6, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L105
.L101:
	movl	$4, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L105
.L102:
	movl	$3, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L105
.L94:
	movl	$9, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L105
.L77:
	movl	$9, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L88
.L496:
	movzbl	1(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L321
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L321
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L321
	movzbl	4(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L495:
	movzbl	3(%rdx), %esi
	movl	$4, %ecx
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, -408(%rbp)
	movl	-408(%rbp), %r11d
	testb	%sil, %sil
	jns	.L320
	movzbl	4(%rdx), %esi
	movl	$5, %ecx
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r11d
	movl	%r11d, -408(%rbp)
	testb	%sil, %sil
	jns	.L320
	movzbl	5(%rdx), %esi
	movl	$6, %ecx
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r11d
	movl	%r11d, -408(%rbp)
	testb	%sil, %sil
	jns	.L320
	movzbl	6(%rdx), %eax
	movl	$7, %ecx
	sall	$28, %eax
	orl	%eax, %r11d
	movl	%r11d, -408(%rbp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L380:
	movl	$7, %edx
	leaq	.LC11(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r8, -408(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-408(%rbp), %r8
	jmp	.L178
.L333:
	movl	$3, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L229
.L337:
	movl	$3, %edx
	leaq	.LC15(%rip), %rsi
	jmp	.L229
.L336:
	movl	$3, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L229
.L335:
	movl	$6, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L229
.L228:
	movl	$7, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L229
.L230:
	movl	$3, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L229
.L231:
	movl	$4, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L229
.L233:
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L229
.L330:
	movl	$3, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L172
.L329:
	movl	$6, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L172
.L379:
	movl	$7, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L172
.L327:
	movl	$3, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L172
.L174:
	movl	$4, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L172
.L173:
	movl	$3, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L172
.L175:
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L172
.L176:
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L172
.L171:
	movl	$7, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L172
.L232:
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L229
.L502:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L298
.L501:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L296
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L296
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L296
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L500:
	movzbl	3(%rdx), %esi
	movl	$3, %ecx
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, -408(%rbp)
	movl	-408(%rbp), %edi
	testb	%sil, %sil
	jns	.L295
	movzbl	4(%rdx), %esi
	movl	$4, %ecx
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	testb	%sil, %sil
	jns	.L295
	movzbl	5(%rdx), %esi
	movl	$5, %ecx
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	testb	%sil, %sil
	jns	.L295
	movzbl	6(%rdx), %eax
	movl	$6, %ecx
	sall	$28, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L497:
	movzbl	3(%rdx), %esi
	movzbl	%al, %eax
	movl	%esi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %eax
	testb	%sil, %sil
	js	.L507
	sall	$18, %eax
	sarl	$18, %eax
	movl	%eax, -408(%rbp)
	movl	$3, %eax
	jmp	.L284
.L224:
	movl	$9, %r13d
	jmp	.L341
.L223:
	movl	$6, %r13d
	jmp	.L341
.L222:
	movl	$7, %r13d
	jmp	.L341
.L221:
	movl	$5, %r13d
	jmp	.L341
.L220:
	movl	$4, %r13d
	jmp	.L341
.L216:
	movl	$1, %r13d
	jmp	.L341
.L487:
	movzbl	3(%rdx), %esi
	movzbl	%cl, %ecx
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ecx
	movl	%ecx, %r13d
	sall	$18, %r13d
	sarl	$18, %r13d
	testb	%sil, %sil
	jns	.L299
	movzbl	4(%rdx), %esi
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ecx
	movl	%ecx, %r13d
	sall	$11, %r13d
	sarl	$11, %r13d
	testb	%sil, %sil
	jns	.L299
	movzbl	5(%rdx), %esi
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%ecx, %eax
	movl	%eax, %r13d
	sall	$4, %r13d
	sarl	$4, %r13d
	testb	%sil, %sil
	jns	.L299
	movzbl	6(%rdx), %r13d
	sall	$28, %r13d
	orl	%eax, %r13d
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L184:
	movzbl	3(%rax), %ecx
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, -408(%rbp)
	testb	%cl, %cl
	js	.L185
	movzbl	4(%rax), %edx
	leaq	3(%rax), %rcx
	jmp	.L183
.L505:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L312
.L499:
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L315
.L503:
	movzbl	2(%rdx), %esi
	movl	$3, %ecx
	movl	%esi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, -408(%rbp)
	movl	-408(%rbp), %edi
	testb	%sil, %sil
	jns	.L307
	movzbl	3(%rdx), %esi
	movl	$4, %ecx
	movl	%esi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	testb	%sil, %sil
	jns	.L307
	movzbl	4(%rdx), %esi
	movl	$5, %ecx
	movl	%esi, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	testb	%sil, %sil
	jns	.L307
	movzbl	5(%rdx), %eax
	movl	$6, %ecx
	sall	$28, %eax
	orl	%eax, %edi
	movl	%edi, -408(%rbp)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L504:
	movzbl	1(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L308
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L308
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L308
	movzbl	4(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L308
.L177:
	movl	$9, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L172
.L185:
	movzbl	4(%rax), %ecx
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, -408(%rbp)
	testb	%cl, %cl
	js	.L186
	movzbl	5(%rax), %edx
	leaq	4(%rax), %rcx
	jmp	.L183
.L498:
	movzbl	2(%rdx), %ecx
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L285
	movzbl	3(%rdx), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L285
	movzbl	4(%rdx), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	jns	.L285
	movzbl	5(%rdx), %eax
	sall	$28, %eax
	orl	%eax, %r13d
	jmp	.L285
.L338:
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L229
.L332:
	movl	$3, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L186:
	movzbl	5(%rax), %edx
	leaq	5(%rax), %rcx
	sall	$28, %edx
	orl	%edx, -408(%rbp)
	movzbl	6(%rax), %edx
	jmp	.L183
.L361:
	xorl	%r13d, %r13d
	jmp	.L225
.L362:
	movl	$3, %r13d
	jmp	.L341
.L218:
	movl	$2, %r13d
	jmp	.L341
.L507:
	movzbl	4(%rdx), %esi
	movl	%esi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %eax
	testb	%sil, %sil
	js	.L508
	sall	$11, %eax
	sarl	$11, %eax
	movl	%eax, -408(%rbp)
	movl	$4, %eax
	jmp	.L284
.L508:
	movzbl	5(%rdx), %esi
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %eax
	testb	%sil, %sil
	js	.L509
	sall	$4, %eax
	sarl	$4, %eax
	movl	%eax, -408(%rbp)
	movl	$5, %eax
	jmp	.L284
.L509:
	movzbl	6(%rdx), %ecx
	sall	$28, %ecx
	orl	%eax, %ecx
	movl	$6, %eax
	movl	%ecx, -408(%rbp)
	jmp	.L284
.L493:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L473:
	call	__stack_chk_fail@PLT
.L134:
	movabsq	$9223372036854775800, %r11
	jmp	.L135
.L506:
	movzbl	8(%rcx), %edx
	movl	$127, %esi
	salq	$49, %rsi
	movq	%rdx, %r13
	salq	$49, %r13
	andq	%rsi, %r13
	orq	%r13, %rax
	testb	%dl, %dl
	js	.L510
	salq	$8, %rax
	sarq	$8, %rax
	movq	%rax, %r13
	jmp	.L248
.L510:
	movzbl	9(%rcx), %edx
	movl	$127, %esi
	salq	$56, %rsi
	movq	%rdx, %r13
	salq	$56, %r13
	andq	%rsi, %r13
	orq	%rax, %r13
	testb	%dl, %dl
	js	.L511
	addq	%r13, %r13
	sarq	%r13
	jmp	.L248
.L511:
	movzbl	10(%rcx), %eax
	salq	$63, %rax
	orq	%rax, %r13
	jmp	.L248
	.cfi_endproc
.LFE18422:
	.size	_ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE, .-_ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE:
.LFB22409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22409:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE, .-_GLOBAL__sub_I__ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm13PrintWasmTextEPKNS1_10WasmModuleERKNS1_15ModuleWireBytesEjRSoPSt6vectorINS_5debug31WasmDisassemblyOffsetTableEntryESaISB_EE
	.weak	_ZTVN2v88internal4wasm7DecoderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm7DecoderE,"awG",@progbits,_ZTVN2v88internal4wasm7DecoderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm7DecoderE, @object
	.size	_ZTVN2v88internal4wasm7DecoderE, 40
_ZTVN2v88internal4wasm7DecoderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm7DecoderD1Ev
	.quad	_ZN2v88internal4wasm7DecoderD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.rodata._ZN2v88internal4wasmL16kAllWasmFeaturesE,"a"
	.align 8
	.type	_ZN2v88internal4wasmL16kAllWasmFeaturesE, @object
	.size	_ZN2v88internal4wasmL16kAllWasmFeaturesE, 13
_ZN2v88internal4wasmL16kAllWasmFeaturesE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.zero	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
