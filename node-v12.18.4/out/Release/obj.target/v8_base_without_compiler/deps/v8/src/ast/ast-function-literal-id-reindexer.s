	.file	"ast-function-literal-id-reindexer.cc"
	.text
	.section	.text._ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi
	.type	_ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi, @function
_ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi:
.LFB19272:
	.cfi_startproc
	endbr64
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movq	%rsi, (%rdi)
	movb	$0, 8(%rdi)
	movl	%edx, 28(%rdi)
	ret
	.cfi_endproc
.LFE19272:
	.size	_ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi, .-_ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi
	.globl	_ZN2v88internal29AstFunctionLiteralIdReindexerC1Emi
	.set	_ZN2v88internal29AstFunctionLiteralIdReindexerC1Emi,_ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi
	.section	.text._ZN2v88internal29AstFunctionLiteralIdReindexerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AstFunctionLiteralIdReindexerD2Ev
	.type	_ZN2v88internal29AstFunctionLiteralIdReindexerD2Ev, @function
_ZN2v88internal29AstFunctionLiteralIdReindexerD2Ev:
.LFB19275:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19275:
	.size	_ZN2v88internal29AstFunctionLiteralIdReindexerD2Ev, .-_ZN2v88internal29AstFunctionLiteralIdReindexerD2Ev
	.globl	_ZN2v88internal29AstFunctionLiteralIdReindexerD1Ev
	.set	_ZN2v88internal29AstFunctionLiteralIdReindexerD1Ev,_ZN2v88internal29AstFunctionLiteralIdReindexerD2Ev
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE, @function
_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE:
.LFB23062:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L24
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rsi), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L23
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L4
	movl	36(%r15), %edx
	testl	%edx, %edx
	jle	.L4
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L14:
	movq	24(%r15), %rax
	movq	(%rax,%r14,8), %r12
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L22
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jb	.L23
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L4
.L22:
	movl	20(%r12), %eax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L4
	addq	$1, %r13
	cmpl	%r13d, 20(%r12)
	jle	.L10
.L13:
	movq	8(%r12), %rax
	movq	(%rax,%r13,8), %rsi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	movq	-56(%rbp), %rsi
	jnb	.L12
.L23:
	movb	$1, 8(%rbx)
.L4:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	addq	$1, %r14
	cmpl	%r14d, 36(%r15)
	jg	.L14
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23062:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE, .-_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE, @function
_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE:
.LFB22439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.L29(%rip), %rbx
	subq	$8, %rsp
.L26:
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$57, %al
	ja	.L25
	movzbl	%al, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"aG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.align 4
	.align 4
.L29:
	.long	.L25-.L29
	.long	.L72-.L29
	.long	.L71-.L29
	.long	.L70-.L29
	.long	.L69-.L29
	.long	.L68-.L29
	.long	.L67-.L29
	.long	.L66-.L29
	.long	.L65-.L29
	.long	.L64-.L29
	.long	.L25-.L29
	.long	.L63-.L29
	.long	.L62-.L29
	.long	.L25-.L29
	.long	.L25-.L29
	.long	.L61-.L29
	.long	.L60-.L29
	.long	.L59-.L29
	.long	.L58-.L29
	.long	.L25-.L29
	.long	.L57-.L29
	.long	.L25-.L29
	.long	.L56-.L29
	.long	.L55-.L29
	.long	.L54-.L29
	.long	.L53-.L29
	.long	.L52-.L29
	.long	.L51-.L29
	.long	.L50-.L29
	.long	.L49-.L29
	.long	.L48-.L29
	.long	.L47-.L29
	.long	.L46-.L29
	.long	.L45-.L29
	.long	.L44-.L29
	.long	.L43-.L29
	.long	.L42-.L29
	.long	.L25-.L29
	.long	.L41-.L29
	.long	.L25-.L29
	.long	.L40-.L29
	.long	.L25-.L29
	.long	.L25-.L29
	.long	.L39-.L29
	.long	.L38-.L29
	.long	.L25-.L29
	.long	.L37-.L29
	.long	.L36-.L29
	.long	.L25-.L29
	.long	.L35-.L29
	.long	.L34-.L29
	.long	.L25-.L29
	.long	.L33-.L29
	.long	.L32-.L29
	.long	.L25-.L29
	.long	.L31-.L29
	.long	.L30-.L29
	.long	.L28-.L29
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE,comdat
	.p2align 4,,10
	.p2align 3
.L69:
	movq	32(%r13), %r14
	movzbl	8(%r12), %eax
	testq	%r14, %r14
	je	.L79
	testb	%al, %al
	jne	.L25
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L222
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	40(%r13), %r14
	testq	%r14, %r14
	je	.L223
.L195:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L224
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	48(%r13), %r14
	testq	%r14, %r14
	je	.L194
.L193:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L225
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
.L194:
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
.L25:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L190
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L226
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L190:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L31:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L188
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L227
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L188:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L32:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L186
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L228
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L186:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L33:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L184
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L229
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L184:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L179
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L230
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L179:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L34:
	movq	16(%r13), %rax
	movq	(%rax), %rbx
	movslq	12(%rax), %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%r13, %rbx
	je	.L25
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	movq	(%rbx), %r14
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L183
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L25
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L25
	movq	(%rbx), %r14
	movl	%eax, 24(%r12)
.L183:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L182
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L36:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L171
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L231
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L171
	movq	16(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L232
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	movl	24(%r12), %eax
	jne	.L175
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L233
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L178:
	subl	$1, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L37:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L169
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L234
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L169:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L39:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L162
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L235
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L162:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L40:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L160
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L236
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L160:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L38:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L164
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L237
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L164
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L238
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L168:
	subl	$1, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal29AstFunctionLiteralIdReindexer20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L158
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L239
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L158:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L44:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L150
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L240
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L150
	movq	16(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L241
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	movl	24(%r12), %eax
	jne	.L154
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L242
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L157:
	subl	$1, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L57:
	movq	8(%r13), %r14
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	jle	.L25
	movzbl	8(%r12), %edx
	xorl	%ebx, %ebx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L245:
	testb	%dl, %dl
	jne	.L25
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L243
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
.L219:
	movq	8(%r15), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L244
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	8(%r12), %edx
	testb	%dl, %dl
	jne	.L25
	addq	$1, %rbx
	cmpl	%ebx, 12(%r14)
	jle	.L25
.L104:
	movq	(%r14), %rax
	movq	(%rax,%rbx,8), %r15
	movq	(%r15), %r13
	andq	$-4, %r13
	movzbl	4(%r13), %eax
	andl	$63, %eax
	cmpb	$41, %al
	jne	.L245
	testb	%dl, %dl
	je	.L219
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L62:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L246
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	16(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L247
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L63:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L64:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L49:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L248
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L50:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L249
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L65:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE20VisitSwitchStatementEPNS0_15SwitchStatementE
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L145
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L250
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L145
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L251
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L149:
	subl	$1, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L46:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L140
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L252
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L140
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L253
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L144:
	subl	$1, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal29AstFunctionLiteralIdReindexer17VisitClassLiteralEPNS0_12ClassLiteralE
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L254
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L60:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	16(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L255
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L53:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L118
	movq	8(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L256
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
.L118:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L70:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	32(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L257
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L71:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L258
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	32(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L42:
	movq	8(%r13), %rsi
.L220:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L259
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L67:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	32(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L260
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	40(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L261
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L51:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	je	.L262
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L52:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L120
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L263
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L120
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L264
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L124:
	subl	$1, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L72:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L48:
	movl	36(%r13), %eax
	testl	%eax, %eax
	jle	.L25
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L139:
	movq	24(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	leal	1(%rax), %ecx
	movl	%ecx, 24(%r12)
	testb	%dl, %dl
	je	.L265
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L55:
	movl	36(%r13), %esi
	testl	%esi, %esi
	jle	.L25
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L112:
	movq	24(%r13), %rcx
	movq	(%rcx,%rbx,8), %r14
	leal	1(%rax), %ecx
	movl	%ecx, 24(%r12)
	testb	%dl, %dl
	je	.L266
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r13, %rsi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L54:
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L113
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L267
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L113
	movq	16(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L268
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L117:
	subl	$1, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L56:
	movl	36(%r13), %edi
	testl	%edi, %edi
	jle	.L25
	movl	24(%r12), %eax
	movzbl	8(%r12), %ecx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L109:
	movq	24(%r13), %rdx
	movq	(%rdx,%rbx,8), %r14
	leal	1(%rax), %edx
	movl	%edx, 24(%r12)
	testb	%cl, %cl
	je	.L269
.L105:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L68:
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	32(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L270
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	40(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L271
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%r12)
	jne	.L25
	movq	24(%r13), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L26
	movb	$1, 8(%r12)
	jmp	.L25
.L28:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L269:
	movq	(%r14), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L272
	movq	%r15, %rsi
	movq	%r12, %rdi
	andq	$-4, %rsi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	jne	.L105
	movq	8(%r14), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L273
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %ecx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%cl, %cl
	jne	.L25
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L109
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L273:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L274
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%dl, %dl
	jne	.L25
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L112
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L265:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L275
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	movzbl	8(%r12), %edx
	subl	$1, %eax
	movl	%eax, 24(%r12)
	testb	%dl, %dl
	jne	.L25
	addq	$1, %rbx
	cmpl	%ebx, 36(%r13)
	jg	.L139
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L244:
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L272:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L275:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L274:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L243:
	movb	$1, 8(%r12)
	jmp	.L25
.L267:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
.L113:
	movl	%eax, 24(%r12)
	jmp	.L25
.L231:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
.L171:
	movl	%eax, 24(%r12)
	jmp	.L25
.L240:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
.L150:
	movl	%eax, 24(%r12)
	jmp	.L25
.L252:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
.L140:
	movl	%eax, 24(%r12)
	jmp	.L25
.L237:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
.L164:
	movl	%eax, 24(%r12)
	jmp	.L25
.L263:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
.L120:
	movl	%eax, 24(%r12)
	jmp	.L25
.L250:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
.L145:
	movl	%eax, 24(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L249:
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L276
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L25
	movl	28(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L25
	xorl	%ebx, %ebx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L25
	addq	$1, %rbx
	cmpl	%ebx, 28(%r13)
	jle	.L25
.L132:
	movq	16(%r13), %rdx
	addl	$1, %eax
	movq	(%rdx,%rbx,8), %r14
	movl	%eax, 24(%r12)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L131
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L248:
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L277
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L25
	movl	28(%r13), %edx
	testl	%edx, %edx
	jle	.L25
	xorl	%ebx, %ebx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 24(%r12)
	jne	.L25
	addq	$1, %rbx
	cmpl	%ebx, 28(%r13)
	jle	.L25
.L136:
	movq	16(%r13), %rdx
	addl	$1, %eax
	movq	(%rdx,%rbx,8), %r14
	movl	%eax, 24(%r12)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L135
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L262:
	movq	8(%r13), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L278
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %eax
	cmpb	$0, 8(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 24(%r12)
	jne	.L25
	movq	32(%r13), %rcx
	cmpq	%rcx, 24(%r13)
	je	.L25
	movl	%eax, 24(%r12)
	xorl	%ebx, %ebx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movl	24(%r12), %edx
	cmpb	$0, 8(%r12)
	leal	-1(%rdx), %eax
	movl	%eax, 24(%r12)
	jne	.L25
	movq	32(%r13), %rax
	subq	24(%r13), %rax
	addq	$1, %rbx
	sarq	$4, %rax
	cmpq	%rbx, %rax
	jbe	.L25
	movl	%edx, 24(%r12)
.L128:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	24(%r13), %rax
	movq	(%rax), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jnb	.L127
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L79:
	movq	40(%r13), %r14
	testq	%r14, %r14
	je	.L82
	testb	%al, %al
	jne	.L25
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L247:
	movb	$1, 8(%r12)
	jmp	.L25
.L264:
	movb	$1, 8(%r12)
	jmp	.L124
.L253:
	movb	$1, 8(%r12)
	jmp	.L144
.L251:
	movb	$1, 8(%r12)
	jmp	.L149
.L261:
	movb	$1, 8(%r12)
	jmp	.L25
.L241:
	movb	$1, 8(%r12)
	movl	24(%r12), %eax
.L154:
	subl	$1, %eax
	movl	%eax, 24(%r12)
	jmp	.L25
.L238:
	movb	$1, 8(%r12)
	jmp	.L168
.L268:
	movb	$1, 8(%r12)
	jmp	.L117
.L271:
	movb	$1, 8(%r12)
	jmp	.L25
.L232:
	movb	$1, 8(%r12)
	movl	24(%r12), %eax
.L175:
	subl	$1, %eax
	movl	%eax, 24(%r12)
	jmp	.L25
.L82:
	movq	48(%r13), %r14
	testq	%r14, %r14
	je	.L85
	testb	%al, %al
	jne	.L25
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L270:
	movb	$1, 8(%r12)
	jmp	.L25
.L246:
	movb	$1, 8(%r12)
	jmp	.L25
.L257:
	movb	$1, 8(%r12)
	jmp	.L25
.L255:
	movb	$1, 8(%r12)
	jmp	.L25
.L226:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L190
.L259:
	movb	$1, 8(%r12)
	jmp	.L25
.L254:
	movb	$1, 8(%r12)
	jmp	.L25
.L260:
	movb	$1, 8(%r12)
	jmp	.L25
.L276:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
.L239:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L158
.L278:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
.L223:
	movq	48(%r13), %r14
	testq	%r14, %r14
	je	.L194
	jmp	.L193
.L256:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L118
.L236:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L160
.L235:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L162
.L234:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L169
.L277:
	subl	$1, 24(%r12)
	movb	$1, 8(%r12)
	jmp	.L25
.L227:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L188
.L228:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L186
.L258:
	movb	$1, 8(%r12)
	jmp	.L25
.L229:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L184
.L230:
	movl	24(%r12), %eax
	movb	$1, 8(%r12)
	subl	$1, %eax
	jmp	.L179
.L233:
	movb	$1, 8(%r12)
	jmp	.L178
.L242:
	movb	$1, 8(%r12)
	jmp	.L157
.L85:
	testb	%al, %al
	jne	.L25
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L222:
	movb	$1, 8(%r12)
	jmp	.L25
.L225:
	movb	$1, 8(%r12)
	jmp	.L25
.L224:
	movb	$1, 8(%r12)
	jmp	.L25
	.cfi_endproc
.LFE22439:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE, .-_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.section	.text._ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE,"axG",@progbits,_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE
	.type	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE, @function
_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE:
.LFB23061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 24(%rsi)
	je	.L289
	movl	24(%rdi), %eax
	leal	1(%rax), %edx
	movl	%edx, 24(%rdi)
	movq	24(%rsi), %rdx
	movq	96(%rdx), %r14
	leaq	88(%rdx), %r12
	movzbl	8(%rdi), %edx
	cmpq	%r12, %r14
	je	.L283
	testb	%dl, %dl
	je	.L288
.L287:
	movl	%eax, 24(%rbx)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L295
	movq	(%r12), %r12
	addq	$16, %r12
	cmpq	%r14, %r12
	je	.L296
.L288:
	movq	(%r12), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L285
	subl	$1, 24(%rbx)
	movb	$1, 8(%rbx)
.L279:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L283:
	.cfi_restore_state
	movl	%eax, 24(%rdi)
	testb	%dl, %dl
	jne	.L279
	.p2align 4,,10
	.p2align 3
.L289:
	movl	20(%r13), %eax
	testl	%eax, %eax
	jle	.L279
	cmpb	$0, 8(%rbx)
	jne	.L279
	xorl	%r12d, %r12d
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L279
	addq	$1, %r12
	cmpl	%r12d, 20(%r13)
	jle	.L279
.L291:
	movq	8(%r13), %rax
	movq	(%rax,%r12,8), %r14
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L290
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	subl	$1, 24(%rbx)
	jmp	.L289
.L295:
	movl	24(%rbx), %eax
	subl	$1, %eax
	jmp	.L287
	.cfi_endproc
.LFE23061:
	.size	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE, .-_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE10VisitBlockEPNS0_5BlockE
	.section	.text._ZN2v88internal29AstFunctionLiteralIdReindexer20VisitFunctionLiteralEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AstFunctionLiteralIdReindexer20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal29AstFunctionLiteralIdReindexer20VisitFunctionLiteralEPNS0_15FunctionLiteralE, @function
_ZN2v88internal29AstFunctionLiteralIdReindexer20VisitFunctionLiteralEPNS0_15FunctionLiteralE:
.LFB19278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %eax
	movq	40(%rsi), %rcx
	leal	1(%rax), %edx
	leaq	88(%rcx), %r13
	movl	%edx, 24(%rdi)
	movq	96(%rcx), %r14
	movzbl	8(%rdi), %ecx
	cmpq	%r14, %r13
	je	.L298
	testb	%cl, %cl
	je	.L304
.L307:
	movl	%eax, 24(%rbx)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L316
	movq	0(%r13), %r13
	addq	$16, %r13
	cmpq	%r13, %r14
	je	.L317
.L304:
	movq	0(%r13), %r15
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L300
	subl	$1, 24(%rbx)
	movb	$1, 8(%rbx)
.L301:
	movl	28(%rbx), %eax
	addl	%eax, 28(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movl	24(%rbx), %edx
	leal	-1(%rdx), %eax
	movl	%eax, 24(%rbx)
.L305:
	movq	40(%r12), %rcx
	cmpb	$0, 131(%rcx)
	js	.L301
	movl	%edx, 24(%rbx)
	movl	60(%r12), %edx
	testl	%edx, %edx
	jle	.L307
	movq	48(%r12), %rax
	xorl	%r13d, %r13d
	movq	(%rax), %r14
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	cmpb	$0, 8(%rbx)
	jne	.L316
	addq	$1, %r13
	cmpl	%r13d, 60(%r12)
	jle	.L316
	movq	48(%r12), %rax
	movq	(%rax,%r13,8), %r14
.L312:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%rbx), %rax
	jnb	.L308
	movb	$1, 8(%rbx)
.L316:
	movl	24(%rbx), %eax
	subl	$1, %eax
	jmp	.L307
.L298:
	movl	%eax, 24(%rdi)
	testb	%cl, %cl
	je	.L305
	jmp	.L301
	.cfi_endproc
.LFE19278:
	.size	_ZN2v88internal29AstFunctionLiteralIdReindexer20VisitFunctionLiteralEPNS0_15FunctionLiteralE, .-_ZN2v88internal29AstFunctionLiteralIdReindexer20VisitFunctionLiteralEPNS0_15FunctionLiteralE
	.section	.text._ZN2v88internal29AstFunctionLiteralIdReindexer7ReindexEPNS0_10ExpressionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AstFunctionLiteralIdReindexer7ReindexEPNS0_10ExpressionE
	.type	_ZN2v88internal29AstFunctionLiteralIdReindexer7ReindexEPNS0_10ExpressionE, @function
_ZN2v88internal29AstFunctionLiteralIdReindexer7ReindexEPNS0_10ExpressionE:
.LFB19277:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L325
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	(%r12), %rax
	jb	.L326
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movb	$1, 8(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19277:
	.size	_ZN2v88internal29AstFunctionLiteralIdReindexer7ReindexEPNS0_10ExpressionE, .-_ZN2v88internal29AstFunctionLiteralIdReindexer7ReindexEPNS0_10ExpressionE
	.section	.text._ZN2v88internal29AstFunctionLiteralIdReindexer17VisitClassLiteralEPNS0_12ClassLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AstFunctionLiteralIdReindexer17VisitClassLiteralEPNS0_12ClassLiteralE
	.type	_ZN2v88internal29AstFunctionLiteralIdReindexer17VisitClassLiteralEPNS0_12ClassLiteralE, @function
_ZN2v88internal29AstFunctionLiteralIdReindexer17VisitClassLiteralEPNS0_12ClassLiteralE:
.LFB19279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	32(%rsi), %r12
	movzbl	8(%rdi), %eax
	testq	%r12, %r12
	je	.L328
	testb	%al, %al
	je	.L368
.L347:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L333
.L372:
	cmpb	$0, 8(%r13)
	jne	.L335
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	0(%r13), %rax
	jb	.L364
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L333:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L335
	cmpb	$0, 8(%r13)
	jne	.L335
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	0(%r13), %rax
	jb	.L364
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	.p2align 4,,10
	.p2align 3
.L335:
	movq	48(%rbx), %r14
	xorl	%ebx, %ebx
	movl	12(%r14), %edx
	testl	%edx, %edx
	jg	.L337
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L369:
	cmpb	$0, 18(%r12)
	jne	.L338
.L339:
	andq	$-4, %rsi
	movzbl	8(%r13), %ecx
	movzbl	4(%rsi), %eax
	movq	%rsi, %r15
	andl	$63, %eax
	cmpb	$41, %al
	je	.L341
	testb	%cl, %cl
	jne	.L340
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	0(%r13), %rax
	jb	.L366
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	8(%r13), %ecx
.L341:
	testb	%cl, %cl
	jne	.L365
	movq	8(%r12), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	0(%r13), %rax
	jb	.L366
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
.L365:
	movl	12(%r14), %edx
.L340:
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jle	.L327
.L337:
	movq	(%r14), %rax
	movq	(%rax,%rbx,8), %r12
	movq	(%r12), %rsi
	testb	$3, %sil
	je	.L369
.L338:
	movq	8(%r12), %rax
	movl	4(%rax), %eax
	andl	$63, %eax
	cmpb	$38, %al
	je	.L339
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jg	.L337
.L327:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movb	$1, 8(%r13)
	movl	12(%r14), %edx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L368:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	0(%r13), %rax
	jb	.L370
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movzbl	8(%r13), %eax
.L328:
	testb	%al, %al
	jne	.L347
	movq	40(%rbx), %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	0(%r13), %rax
	jb	.L371
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19AstTraversalVisitorINS0_29AstFunctionLiteralIdReindexerEE25VisitNoStackOverflowCheckEPNS0_7AstNodeE
	movq	56(%rbx), %r12
	testq	%r12, %r12
	jne	.L372
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L364:
	movb	$1, 8(%r13)
	jmp	.L335
.L370:
	movb	$1, 8(%r13)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L371:
	movb	$1, 8(%r13)
	cmpq	$0, 56(%rbx)
	jne	.L335
	jmp	.L333
	.cfi_endproc
.LFE19279:
	.size	_ZN2v88internal29AstFunctionLiteralIdReindexer17VisitClassLiteralEPNS0_12ClassLiteralE, .-_ZN2v88internal29AstFunctionLiteralIdReindexer17VisitClassLiteralEPNS0_12ClassLiteralE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi, @function
_GLOBAL__sub_I__ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi:
.LFB23599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23599:
	.size	_GLOBAL__sub_I__ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi, .-_GLOBAL__sub_I__ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29AstFunctionLiteralIdReindexerC2Emi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
