	.file	"platform-embedded-file-writer-generic.cc"
	.text
	.section	.text._ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,"axG",@progbits,_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.type	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, @function
_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv:
.LFB3147:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE3147:
	.size	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, .-_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv:
.LFB3148:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3148:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric18DeclareFunctionEndEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric18DeclareFunctionEndEPKc
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric18DeclareFunctionEndEPKc, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric18DeclareFunctionEndEPKc:
.LFB3401:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3401:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric18DeclareFunctionEndEPKc, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric18DeclareFunctionEndEPKc
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FilePrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FilePrologueEv
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FilePrologueEv, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FilePrologueEv:
.LFB3403:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3403:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FilePrologueEv, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FilePrologueEv
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGenericD2Ev,"axG",@progbits,_ZN2v88internal33PlatformEmbeddedFileWriterGenericD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD2Ev
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD2Ev, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGenericD2Ev:
.LFB3938:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3938:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD2Ev, .-_ZN2v88internal33PlatformEmbeddedFileWriterGenericD2Ev
	.weak	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD1Ev
	.set	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD1Ev,_ZN2v88internal33PlatformEmbeddedFileWriterGenericD2Ev
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv:
.LFB3146:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	movl	$10, %edi
	jmp	fputc@PLT
	.cfi_endproc
.LFE3146:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	".section .data\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv:
.LFB3390:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$15, %edx
	movl	$1, %esi
	leaq	.LC0(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3390:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	".section .rodata\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv:
.LFB3391:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$17, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3391:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	".balign 32\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv:
.LFB3395:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$11, %edx
	movl	$1, %esi
	leaq	.LC2(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3395:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	".balign 8\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv:
.LFB3396:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$10, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3396:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc.str1.1,"aMS",@progbits,1
.LC4:
	.string	"// %s\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc:
.LFB3397:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC4(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3397:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc.str1.1,"aMS",@progbits,1
.LC5:
	.string	""
.LC6:
	.string	"%s%s:\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc:
.LFB3398:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %r8
	leaq	.LC5(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdx
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3398:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci.str1.1,"aMS",@progbits,1
.LC7:
	.string	".loc %d %d\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci:
.LFB3399:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	%ecx, %r8d
	leaq	.LC7(%rip), %rdx
	movl	%esi, %ecx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3399:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm.str1.1,"aMS",@progbits,1
.LC8:
	.string	"0x%lx"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm:
.LFB3402:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC8(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3402:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	".section .note.GNU-stack,\"\",%%progbits\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv:
.LFB3405:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	leaq	.LC9(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3405:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGenericD0Ev,"axG",@progbits,_ZN2v88internal33PlatformEmbeddedFileWriterGenericD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD0Ev
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD0Ev, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGenericD0Ev:
.LFB3940:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3940:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD0Ev, .-_ZN2v88internal33PlatformEmbeddedFileWriterGenericD0Ev
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	".section .text.hot.embedded\n"
.LC11:
	.string	".section .text\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv:
.LFB3389:
	.cfi_startproc
	endbr64
	cmpl	$1, 20(%rdi)
	movq	8(%rdi), %rcx
	je	.L20
	movl	$15, %edx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdi
	jmp	fwrite@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$28, %edx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3389:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc.str1.1,"aMS",@progbits,1
.LC13:
	.string	".file %d \"%s\"\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc:
.LFB3404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%rdx, %rdx
	je	.L35
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movl	%esi, %r12d
	movq	%rdx, %r15
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L36
	cmpq	$1, %rax
	jne	.L25
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L26:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r8
	addq	%rax, %r8
	cmpq	%r8, %rax
	je	.L27
	.p2align 4,,10
	.p2align 3
.L29:
	cmpb	$92, (%rax)
	jne	.L28
	movb	$47, (%rax)
.L28:
	addq	$1, %rax
	cmpq	%rax, %r8
	jne	.L29
	movq	-96(%rbp), %r8
.L27:
	movq	8(%rbx), %rdi
	movl	%r12d, %ecx
	leaq	.LC13(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L38
	movq	%r13, %rdx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L36:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L24:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC12(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L37:
	call	__stack_chk_fail@PLT
.L38:
	movq	%r13, %rdi
	jmp	.L24
	.cfi_endproc
.LFE3404:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE.str1.1,"aMS",@progbits,1
.LC14:
	.string	".byte"
.LC15:
	.string	".long"
.LC16:
	.string	".quad"
.LC17:
	.string	".octa"
.LC18:
	.string	"unreachable code"
.LC19:
	.string	"  %s "
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE:
.LFB3406:
	.cfi_startproc
	endbr64
	cmpl	$2, %esi
	je	.L42
	ja	.L41
	testl	%esi, %esi
	leaq	.LC15(%rip), %rcx
	leaq	.LC14(%rip), %rax
	cmove	%rax, %rcx
.L40:
	movq	8(%rdi), %rdi
	leaq	.LC19(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$3, %esi
	jne	.L48
	leaq	.LC17(%rip), %rcx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	.LC16(%rip), %rcx
	jmp	.L40
.L48:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3406:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc.str1.1,"aMS",@progbits,1
.LC20:
	.string	".type %s, %%function\n"
.LC21:
	.string	".type %s, @function\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc:
.LFB3400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L50
	movq	8(%rdi), %rdi
	movq	%rsi, %r8
	leaq	.LC5(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L51:
	cmpl	$1, 16(%rbx)
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	leaq	.LC20(%rip), %rdx
	jbe	.L54
	leaq	.LC21(%rip), %rdx
.L54:
	popq	%rbx
	movl	$1, %esi
	popq	%r12
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	call	*%rax
	jmp	.L51
	.cfi_endproc
.LFE3400:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_.str1.1,"aMS",@progbits,1
.LC22:
	.string	".global %s%s\n"
.LC23:
	.string	"  %s %s%s\n"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_:
.LFB3393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	.LC5(%rip), %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	.LC22(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	(%rbx), %rax
	leaq	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L56
	movq	8(%rbx), %rdi
	movq	%r13, %r8
	leaq	.LC5(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L57:
	call	_ZN2v88internal20PointerSizeDirectiveEv@PLT
	cmpl	$2, %eax
	je	.L60
	ja	.L59
	testl	%eax, %eax
	leaq	.LC15(%rip), %rcx
	leaq	.LC14(%rip), %rax
	cmove	%rax, %rcx
.L58:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %r9
	movl	$1, %esi
	popq	%rbx
	leaq	.LC5(%rip), %r8
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	leaq	.LC23(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L64
	leaq	.LC17(%rip), %rcx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC16(%rip), %rcx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L57
.L64:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3393:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_
	.section	.rodata._ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj.str1.1,"aMS",@progbits,1
.LC24:
	.string	"%d"
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj:
.LFB3392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	.LC5(%rip), %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	leaq	.LC22(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L66
	movq	8(%r12), %rdi
	movq	%r13, %r8
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rcx
	leaq	.LC6(%rip), %rdx
	call	__fprintf_chk@PLT
.L67:
	movq	(%r12), %rax
	leaq	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L68
	movq	8(%r12), %rdi
	leaq	.LC15(%rip), %rcx
	leaq	.LC19(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L69:
	movq	8(%r12), %rdi
	movl	%r14d, %ecx
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L70
	movq	8(%r12), %rsi
	addq	$8, %rsp
	movl	$10, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fputc@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L69
	.cfi_endproc
.LFE3392:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj
	.section	.text._ZN2v88internal33PlatformEmbeddedFileWriterGeneric19DeclareSymbolGlobalEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric19DeclareSymbolGlobalEPKc
	.type	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric19DeclareSymbolGlobalEPKc, @function
_ZN2v88internal33PlatformEmbeddedFileWriterGeneric19DeclareSymbolGlobalEPKc:
.LFB3394:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %r8
	leaq	.LC5(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rdx
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3394:
	.size	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric19DeclareSymbolGlobalEPKc, .-_ZN2v88internal33PlatformEmbeddedFileWriterGeneric19DeclareSymbolGlobalEPKc
	.weak	_ZTVN2v88internal33PlatformEmbeddedFileWriterGenericE
	.section	.data.rel.ro._ZTVN2v88internal33PlatformEmbeddedFileWriterGenericE,"awG",@progbits,_ZTVN2v88internal33PlatformEmbeddedFileWriterGenericE,comdat
	.align 8
	.type	_ZTVN2v88internal33PlatformEmbeddedFileWriterGenericE, @object
	.size	_ZTVN2v88internal33PlatformEmbeddedFileWriterGenericE, 200
_ZTVN2v88internal33PlatformEmbeddedFileWriterGenericE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD1Ev
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGenericD0Ev
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionTextEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric11SectionDataEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13SectionRoDataEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToCodeAlignmentEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20AlignToDataAlignmentEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric13DeclareUint32EPKcj
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric22DeclarePointerToSymbolEPKcS3_
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12DeclareLabelEPKc
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10SourceInfoEiPKci
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric20DeclareFunctionBeginEPKc
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric18DeclareFunctionEndEPKc
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric10HexLiteralEm
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric7CommentEPKc
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FilePrologueEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric23DeclareExternalFilenameEiPKc
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric12FileEpilogueEv
	.quad	_ZN2v88internal33PlatformEmbeddedFileWriterGeneric21IndentedDataDirectiveENS0_13DataDirectiveE
	.quad	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
