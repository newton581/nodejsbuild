	.file	"mksnapshot.cc"
	.text
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv:
.LFB10510:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	movl	$10, %edi
	jmp	fputc@PLT
	.cfi_endproc
.LFE10510:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.section	.rodata._ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Loading script for %s: %s\n"
.LC1:
	.string	"rb"
	.section	.rodata._ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Failed to open '%s': errno %d\n"
	.align 8
.LC3:
	.string	"Failed to read '%s': errno %d\n"
	.section	.text._ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0, @function
_ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0:
.LFB24490:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rdi, %rcx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rdi, -56(%rbp)
	movl	$1, %edi
	call	__printf_chk@PLT
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	testq	%rax, %rax
	je	.L14
	movl	$2, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	fseek@PLT
	movq	%r15, %rdi
	call	ftell@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	rewind@PLT
	leaq	1(%r13), %rdi
	call	_Znam@PLT
	movb	$0, (%rax,%r13)
	movq	%rax, %r14
	testq	%r13, %r13
	je	.L5
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r13, %rdx
	leaq	(%r14,%rbx), %rdi
	movq	%r15, %rcx
	movl	$1, %esi
	subq	%rbx, %rdx
	call	fread@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	ferror@PLT
	testl	%eax, %eax
	jne	.L15
	addq	%r12, %rbx
	cmpq	%rbx, %r13
	ja	.L7
.L5:
	movq	%r15, %rdi
	call	fclose@PLT
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__errno_location@PLT
	movq	-56(%rbp), %rcx
	leaq	.LC3(%rip), %rdx
	movl	(%rax), %r8d
.L13:
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movl	$1, %edi
	call	exit@PLT
.L14:
	call	__errno_location@PLT
	movq	-56(%rbp), %rcx
	leaq	.LC2(%rip), %rdx
	movl	(%rax), %r8d
	jmp	.L13
	.cfi_endproc
.LFE24490:
	.size	_ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0, .-_ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"The embedded blob starts here. Metadata comes first, followed"
	.align 8
.LC5:
	.string	"by builtin instruction streams."
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"v8_%s_embedded_blob_data_"
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.8
	.align 8
.LC7:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE,"axG",@progbits,_ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE
	.type	_ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE, @function
_ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE:
.LFB10526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	.LC4(%rip), %rsi
	call	*112(%rax)
	movq	(%r12), %rax
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	*112(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	movq	(%r12), %rax
	movl	$256, %esi
	movq	37360(%r13), %rcx
	leaq	-320(%rbp), %rdi
	leaq	.LC6(%rip), %rdx
	movq	$256, -328(%rbp)
	movq	72(%rax), %r14
	xorl	%eax, %eax
	movq	%rdi, -336(%rbp)
	leaq	-352(%rbp), %r13
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-336(%rbp), %r15
	leaq	-368(%rbp), %rax
	movq	%r13, -368(%rbp)
	movq	%rax, -392(%rbp)
	testq	%r15, %r15
	je	.L26
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -376(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L27
	cmpq	$1, %rax
	jne	.L20
	movzbl	(%r15), %edx
	movb	%dl, -352(%rbp)
	movq	%r13, %rdx
.L21:
	movq	%rax, -360(%rbp)
	movq	%r12, %rdi
	movb	$0, (%rdx,%rax)
	movq	-368(%rbp), %rsi
	call	*%r14
	movq	-368(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	(%rbx), %rsi
	movl	$12448, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L29
	movq	%r13, %rdx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L27:
	movq	-392(%rbp), %rdi
	leaq	-376(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -400(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-400(%rbp), %r8
	movq	%rax, -368(%rbp)
	movq	%rax, %rdi
	movq	-376(%rbp), %rax
	movq	%rax, -352(%rbp)
.L19:
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-376(%rbp), %rax
	movq	-368(%rbp), %rdx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L28:
	call	__stack_chk_fail@PLT
.L29:
	movq	%r13, %rdi
	jmp	.L19
	.cfi_endproc
.LFE10526:
	.size	_ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE, .-_ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"wb"
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Unable to open file \"%s\" for writing.\n"
	.align 8
.LC10:
	.string	"Autogenerated file. Do not edit."
	.align 8
.LC11:
	.string	"Source positions in the embedded blob refer to filenames by id."
	.align 8
.LC12:
	.string	"Assembly directives here map the id to a filename."
	.section	.text._ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE,"axG",@progbits,_ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE
	.type	_ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE, @function
_ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE:
.LFB10518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	37352(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L30
	movq	%rdi, %r12
	movq	%rsi, %r13
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L53
	movq	37376(%r12), %rdx
	movq	37368(%r12), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_@PLT
	movq	-64(%rbp), %r15
	leaq	.LC10(%rip), %rsi
	movq	(%r15), %rax
	movq	%r14, 8(%r15)
	movq	%r15, %rdi
	call	*112(%rax)
	movq	(%r15), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rcx
	movq	120(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L33
	movq	8(%r15), %rsi
	movl	$10, %edi
	call	fputc@PLT
.L34:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*128(%rax)
	movq	-64(%rbp), %r15
	leaq	.LC11(%rip), %rsi
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*112(%rax)
	movq	(%r15), %rax
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	call	*112(%rax)
	movq	(%r15), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rcx
	movq	120(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L35
	movq	8(%r15), %rsi
	movl	$10, %edi
	call	fputc@PLT
.L36:
	movq	37328(%r12), %rdx
	movq	37336(%r12), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	testl	%eax, %eax
	jle	.L37
	subl	$1, %eax
	xorl	%ebx, %ebx
	movq	%rax, -72(%rbp)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L54:
	movq	37328(%r12), %rdx
	movq	%rsi, %rbx
.L38:
	movq	(%r15), %r8
	leal	1(%rbx), %esi
	movq	(%rdx,%rbx,8), %rdx
	movq	%r15, %rdi
	call	*136(%r8)
	leaq	1(%rbx), %rsi
	cmpq	%rbx, -72(%rbp)
	jne	.L54
.L37:
	movq	-64(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZNK2v88internal18EmbeddedFileWriter20WriteMetadataSectionEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE
	movq	-64(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L40:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	testl	%eax, %eax
	je	.L39
	movl	%r15d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi@PLT
.L39:
	addl	$1, %r15d
	cmpl	$1553, %r15d
	jne	.L40
	movq	(%rbx), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rcx
	movq	120(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L41
	movq	8(%rbx), %rsi
	movl	$10, %edi
	call	fputc@PLT
.L42:
	movq	-64(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE@PLT
	movq	%r14, %rdi
	call	fclose@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
.L30:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L36
.L55:
	call	__stack_chk_fail@PLT
.L53:
	leaq	.LC9(%rip), %rdi
	movq	%r15, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	$1, %edi
	call	exit@PLT
	.cfi_endproc
.LFE10518:
	.size	_ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE, .-_ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB23101:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L71
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L60:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L58
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L56
.L59:
	movq	%rbx, %r12
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L59
.L56:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE23101:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_:
.LFB23105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$72, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%r14), %r14
	movq	%rax, %r12
	leaq	32(%rax), %r15
	addq	$48, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, 32(%r12)
	testq	%r14, %r14
	je	.L120
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L121
	cmpq	$1, %rax
	jne	.L78
	movzbl	(%r14), %edx
	movb	%dl, 48(%r12)
	movq	-88(%rbp), %rdx
.L79:
	movq	%rax, 40(%r12)
	movb	$0, (%rdx,%rax)
	movl	(%rbx), %eax
	movq	16(%r13), %rbx
	movl	%eax, 64(%r12)
	leaq	8(%r13), %rax
	movq	%rax, -72(%rbp)
	testq	%rbx, %rbx
	je	.L80
	movq	40(%r12), %r14
	movq	32(%r12), %r15
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	movq	%r15, %r12
	movq	%r14, %r15
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L86:
	movq	16(%rbx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L82
.L122:
	movq	%rax, %rbx
.L81:
	movq	40(%rbx), %r14
	movq	32(%rbx), %r11
	cmpq	%r14, %r15
	movq	%r14, %r13
	cmovbe	%r15, %r13
	testq	%r13, %r13
	je	.L83
	movq	%r11, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r11, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %r11
	testl	%eax, %eax
	jne	.L84
.L83:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L85
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L86
.L84:
	testl	%eax, %eax
	js	.L86
.L85:
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L122
.L82:
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	-104(%rbp), %r13
	movq	%r15, %r14
	movq	%rbx, %r9
	movq	%r12, %r15
	movq	-96(%rbp), %r12
	testb	%sil, %sil
	jne	.L123
.L89:
	testq	%rdx, %rdx
	je	.L90
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r9, -96(%rbp)
	movq	%rcx, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	jne	.L91
.L90:
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L92
	cmpq	$-2147483648, %rcx
	jl	.L93
	movl	%ecx, %eax
.L91:
	testl	%eax, %eax
	js	.L93
.L92:
	cmpq	%r15, -88(%rbp)
	je	.L99
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L99:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
.L98:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L124
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L125
	movq	-88(%rbp), %rdx
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L93:
	testq	%r9, %r9
	je	.L126
.L94:
	movl	$1, %edi
	cmpq	%r9, -72(%rbp)
	jne	.L127
.L95:
	movq	-72(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	movq	%r12, %rax
	movl	$1, %edx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L123:
	cmpq	24(%r13), %rbx
	je	.L128
.L100:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %r9
	movq	40(%rax), %rcx
	movq	32(%rax), %r11
	movq	%rax, %rbx
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r15, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L77:
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %rax
	movq	32(%r12), %rdx
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	movq	40(%r9), %rbx
	cmpq	%rbx, %r14
	movq	%rbx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L96
	movq	32(%r9), %rsi
	movq	%r15, %rdi
	movq	%r9, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L97
.L96:
	movq	%r14, %r8
	xorl	%edi, %edi
	subq	%rbx, %r8
	cmpq	$2147483647, %r8
	jg	.L95
	cmpq	$-2147483648, %r8
	jl	.L119
	movl	%r8d, %edi
.L97:
	shrl	$31, %edi
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	8(%r13), %rax
	cmpq	24(%r13), %rax
	jne	.L129
	leaq	8(%r13), %r9
.L119:
	movl	$1, %edi
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rbx, %r9
	jmp	.L94
.L124:
	call	__stack_chk_fail@PLT
.L125:
	movq	-88(%rbp), %rdi
	jmp	.L77
.L129:
	movq	32(%r12), %r15
	movq	40(%r12), %r14
	movq	%rax, %rbx
	jmp	.L100
.L126:
	xorl	%ebx, %ebx
	jmp	.L92
	.cfi_endproc
.LFE23105:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	.section	.rodata._ZZN12_GLOBAL__N_123MaybeSetCounterFunctionEPN2v87IsolateEENUlPKcE_4_FUNES4_.str1.1,"aMS",@progbits,1
.LC13:
	.string	"map::at"
	.section	.text._ZZN12_GLOBAL__N_123MaybeSetCounterFunctionEPN2v87IsolateEENUlPKcE_4_FUNES4_,"ax",@progbits
	.p2align 4
	.type	_ZZN12_GLOBAL__N_123MaybeSetCounterFunctionEPN2v87IsolateEENUlPKcE_4_FUNES4_, @function
_ZZN12_GLOBAL__N_123MaybeSetCounterFunctionEPN2v87IsolateEENUlPKcE_4_FUNES4_:
.LFB19824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	subq	$120, %rsp
	movq	_ZN12_GLOBAL__N_112counter_map_E(%rip), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -144(%rbp)
	movq	%rbx, -128(%rbp)
	testq	%rdi, %rdi
	je	.L168
	movq	%rdi, %r12
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L187
	cmpq	$1, %rax
	jne	.L134
	movzbl	(%r12), %edx
	movb	%dl, -112(%rbp)
	movq	%rbx, %rdx
.L135:
	leaq	8(%r14), %r9
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	16(%r14), %r14
	movq	%r9, %r13
	movq	-128(%rbp), %r8
	testq	%r14, %r14
	je	.L137
	movq	-120(%rbp), %r15
	movq	-128(%rbp), %r8
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L143:
	movq	24(%r14), %r14
	testq	%r14, %r14
	je	.L139
.L138:
	movq	40(%r14), %r12
	movq	%r15, %rdx
	cmpq	%r15, %r12
	cmovbe	%r12, %rdx
	testq	%rdx, %rdx
	je	.L140
	movq	32(%r14), %rdi
	movq	%r8, %rsi
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	testl	%eax, %eax
	jne	.L141
.L140:
	movq	%r12, %rax
	movl	$2147483648, %esi
	subq	%r15, %rax
	cmpq	%rsi, %rax
	jge	.L142
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L143
.L141:
	testl	%eax, %eax
	js	.L143
.L142:
	movq	%r14, %r13
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L138
.L139:
	cmpq	%r13, %r9
	je	.L137
	movq	40(%r13), %r12
	cmpq	%r12, %r15
	movq	%r12, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L145
	movq	32(%r13), %rsi
	movq	%r8, %rdi
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	testl	%eax, %eax
	jne	.L146
.L145:
	subq	%r12, %r15
	cmpq	$2147483647, %r15
	jg	.L137
	cmpq	$-2147483648, %r15
	jl	.L171
	movl	%r15d, %eax
.L146:
	testl	%eax, %eax
	cmovs	%r9, %r13
.L137:
	cmpq	%rbx, %r8
	je	.L147
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L147:
	movq	_ZN12_GLOBAL__N_112counter_map_E(%rip), %r14
	leaq	8(%r14), %rax
	cmpq	%rax, %r13
	je	.L188
.L148:
	movq	-144(%rbp), %r12
	leaq	-80(%rbp), %rbx
	leaq	-96(%rbp), %r13
	movq	%rbx, -96(%rbp)
	testq	%r12, %r12
	je	.L168
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L189
	cmpq	$1, %rax
	jne	.L150
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%rbx, %rdx
.L151:
	movq	%rax, -88(%rbp)
	leaq	8(%r14), %r12
	movb	$0, (%rdx,%rax)
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L160
	movq	-88(%rbp), %r15
	movq	-96(%rbp), %r13
	movq	%r12, %rcx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L158:
	movq	24(%r14), %r14
	testq	%r14, %r14
	je	.L154
.L153:
	movq	40(%r14), %r10
	movq	%r15, %rdx
	cmpq	%r15, %r10
	cmovbe	%r10, %rdx
	testq	%rdx, %rdx
	je	.L155
	movq	32(%r14), %rdi
	movq	%r13, %rsi
	movq	%rcx, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %rcx
	testl	%eax, %eax
	jne	.L156
.L155:
	movq	%r10, %rax
	movl	$2147483648, %esi
	subq	%r15, %rax
	cmpq	%rsi, %rax
	jge	.L157
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rax
	jle	.L158
.L156:
	testl	%eax, %eax
	js	.L158
.L157:
	movq	%r14, %rcx
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L153
.L154:
	cmpq	%rcx, %r12
	je	.L160
	movq	40(%rcx), %r12
	cmpq	%r12, %r15
	movq	%r12, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L161
	movq	32(%rcx), %rsi
	movq	%r13, %rdi
	movq	%rcx, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %rcx
	testl	%eax, %eax
	jne	.L162
.L161:
	subq	%r12, %r15
	cmpq	$2147483647, %r15
	jg	.L163
	cmpq	$-2147483648, %r15
	jl	.L160
	movl	%r15d, %eax
.L162:
	testl	%eax, %eax
	js	.L160
.L163:
	leaq	64(%rcx), %r12
	cmpq	%rbx, %r13
	je	.L130
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	testq	%rax, %rax
	jne	.L191
	movq	%rbx, %rdx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L150:
	testq	%r15, %r15
	jne	.L192
	movq	%rbx, %rdx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	-128(%rbp), %r15
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L133:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r13, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L149:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r14, %rdi
	leaq	-136(%rbp), %rdx
	leaq	-144(%rbp), %rsi
	movl	$0, -136(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE17_M_emplace_uniqueIJRPKciEEES6_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	movq	_ZN12_GLOBAL__N_112counter_map_E(%rip), %r14
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_out_of_rangePKc@PLT
.L171:
	movq	%r9, %r13
	jmp	.L137
.L190:
	call	__stack_chk_fail@PLT
.L191:
	movq	%rbx, %rdi
	jmp	.L133
.L192:
	movq	%rbx, %rdi
	jmp	.L149
	.cfi_endproc
.LFE19824:
	.size	_ZZN12_GLOBAL__N_123MaybeSetCounterFunctionEPN2v87IsolateEENUlPKcE_4_FUNES4_, .-_ZZN12_GLOBAL__N_123MaybeSetCounterFunctionEPN2v87IsolateEENUlPKcE_4_FUNES4_
	.section	.text._ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB23157:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L201
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L195:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L195
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE23157:
	.size	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.rodata.main.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"Usage: %s --startup_src=... --startup_blob=... [extras]\n"
	.section	.rodata.main.str1.1,"aMS",@progbits,1
.LC15:
	.string	"embedding"
.LC16:
	.string	"warm up"
	.section	.rodata.main.str1.8
	.align 8
.LC17:
	.string	"Creating snapshot took %0.3f ms\n"
	.align 8
.LC18:
	.string	"Warming up snapshot took %0.3f ms\n"
	.section	.rodata.main.str1.1
.LC19:
	.string	"blob.data"
.LC20:
	.string	"Check failed: %s."
	.section	.rodata.main.str1.8
	.align 8
.LC21:
	.string	"// Autogenerated snapshot file. Do not edit.\n\n"
	.section	.rodata.main.str1.1
.LC22:
	.string	"#include \"src/init/v8.h\"\n"
	.section	.rodata.main.str1.8
	.align 8
.LC23:
	.string	"#include \"src/base/platform/platform.h\"\n\n"
	.align 8
.LC24:
	.string	"#include \"src/snapshot/snapshot.h\"\n\n"
	.section	.rodata.main.str1.1
.LC25:
	.string	"namespace v8 {\n"
.LC26:
	.string	"namespace internal {\n\n"
	.section	.rodata.main.str1.8
	.align 8
.LC27:
	.string	"alignas(kPointerAlignment) static const byte blob_data[] = {\n"
	.section	.rodata.main.str1.1
.LC28:
	.string	"%u"
.LC29:
	.string	"};\n"
	.section	.rodata.main.str1.8
	.align 8
.LC30:
	.string	"static const int blob_size = %d;\n"
	.align 8
.LC31:
	.string	"static const v8::StartupData blob =\n"
	.align 8
.LC32:
	.string	"{ (const char*) blob_data, blob_size };\n"
	.align 8
.LC33:
	.string	"const v8::StartupData* Snapshot::DefaultSnapshotBlob() {\n"
	.section	.rodata.main.str1.1
.LC34:
	.string	"  return &blob;\n"
.LC35:
	.string	"}\n\n"
.LC36:
	.string	"}  // namespace internal\n"
.LC37:
	.string	"}  // namespace v8\n"
	.section	.rodata.main.str1.8
	.align 8
.LC38:
	.string	"Writing snapshot file failed.. Aborting.\n"
	.section	.text.startup.main,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB19826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-36864(%rsp), %r11
.LPSRL0:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL0
	subq	$664, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %edx
	movl	%edi, -37508(%rbp)
	leaq	-37508(%rbp), %rdi
	movq	%rsi, %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, _ZN2v88internal16FLAG_predictableE(%rip)
	call	_ZN2v88internal8FlagList23SetFlagsFromCommandLineEPiPPcb@PLT
	testl	%eax, %eax
	jg	.L205
	cmpl	$3, -37508(%rbp)
	jg	.L205
	cmpb	$0, _ZN2v88internal9FLAG_helpE(%rip)
	jne	.L205
	cmpb	$0, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	je	.L292
.L208:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	leaq	-37488(%rbp), %r13
	call	_ZN2v82V828InitializeICUDefaultLocationEPKcS2_@PLT
	leaq	-37496(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, -37488(%rbp)
	movq	%r13, %r8
	xorl	%esi, %esi
	call	_ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE@PLT
	movq	-37488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	movq	(%rdi), %rax
	call	*8(%rax)
.L209:
	movq	-37496(%rbp), %rdi
	leaq	-184(%rbp), %r14
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	movq	_ZN2v88internal17FLAG_startup_blobE(%rip), %rax
	movq	_ZN2v88internal16FLAG_startup_srcE(%rip), %r15
	pxor	%xmm0, %xmm0
	movq	%rax, -37560(%rbp)
	leaq	16+_ZTVN2v88internal18EmbeddedFileWriterE(%rip), %rax
	movq	%rax, -37440(%rbp)
	leaq	-37440(%rbp), %rax
	movq	%rax, -37528(%rbp)
	leaq	-37432(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L210:
	movups	%xmm0, (%rax)
	addq	$48, %rax
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %r14
	jne	.L210
	leaq	-152(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	_ZN2v88internalL23kDefaultEmbeddedVariantE(%rip), %rax
	movq	%rax, -80(%rbp)
	movq	_ZN2v88internal17FLAG_embedded_srcE(%rip), %rax
	movq	$0, -168(%rbp)
	movq	%rax, -88(%rbp)
	movq	_ZN2v88internal21FLAG_embedded_variantE(%rip), %rax
	movl	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -96(%rbp)
	movups	%xmm0, -184(%rbp)
	testq	%rax, %rax
	je	.L211
	movq	%rax, -80(%rbp)
.L211:
	movq	_ZN2v88internal16FLAG_target_archE(%rip), %xmm0
	cmpl	$1, -37508(%rbp)
	movq	$0, -37536(%rbp)
	movhps	_ZN2v88internal14FLAG_target_osE(%rip), %xmm0
	movups	%xmm0, -72(%rbp)
	jle	.L212
	movq	8(%rbx), %rdi
	movq	%rdi, -37536(%rbp)
	testq	%rdi, %rdi
	je	.L212
	cmpb	$0, (%rdi)
	je	.L241
	leaq	.LC15(%rip), %rsi
	call	_ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0
	movq	%rax, -37536(%rbp)
.L212:
	cmpl	$2, -37508(%rbp)
	movq	$0, -37520(%rbp)
	jle	.L213
	movq	16(%rbx), %rdi
	movq	%rdi, -37520(%rbp)
	testq	%rdi, %rdi
	je	.L213
	cmpb	$0, (%rdi)
	je	.L243
	leaq	.LC16(%rip), %rsi
	call	_ZN12_GLOBAL__N_112GetExtraCodeEPcPKc.part.0
	movq	%rax, -37520(%rbp)
.L213:
	call	_ZN2v88internal30DisableEmbeddedBlobRefcountingEv@PLT
	call	_ZN2v87Isolate8AllocateEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_native_code_countersE(%rip)
	movq	%rax, %rbx
	jne	.L293
.L214:
	movq	%r13, %rdi
	call	_ZN2v819ResourceConstraintsC1Ev@PLT
	leaq	37592(%rbx), %rdi
	movq	%r13, %rsi
	movq	$128, -37464(%rbp)
	call	_ZN2v88internal4Heap13ConfigureHeapERKNS_19ResourceConstraintsE@PLT
	movq	-37528(%rbp), %rax
	movq	%rax, 45776(%rbx)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-37536(%rbp), %rsi
	movq	%rbx, %rdx
	xorl	%edi, %edi
	movq	%rax, %r12
	call	_ZN2v88internal30CreateSnapshotDataBlobInternalENS_15SnapshotCreator20FunctionCodeHandlingEPKcPNS_7IsolateE@PLT
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	movq	%rdx, -37552(%rbp)
	movq	%rax, %rbx
	jne	.L294
.L215:
	movl	-37552(%rbp), %eax
	movq	%rbx, %r12
	movl	%eax, -37544(%rbp)
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, -37512(%rbp)
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	-37512(%rbp), %edx
	movq	-37528(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -37488(%rbp)
	movl	%edx, -37480(%rbp)
	call	_ZNK2v88internal18EmbeddedFileWriter22MaybeWriteEmbeddedFileEPKNS0_12EmbeddedDataE
	cmpq	$0, -37520(%rbp)
	je	.L216
	movl	-37552(%rbp), %r12d
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	-37520(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rax, -37544(%rbp)
	call	_ZN2v88internal30WarmUpSnapshotDataBlobInternalENS_11StartupDataEPKc@PLT
	cmpb	$0, _ZN2v88internal28FLAG_profile_deserializationE(%rip)
	movq	%rax, %r12
	jne	.L295
.L217:
	movl	%edx, -37544(%rbp)
	testq	%rbx, %rbx
	je	.L216
	movq	%rbx, %rdi
	call	_ZdaPv@PLT
.L216:
	movq	_ZN12_GLOBAL__N_112counter_map_E(%rip), %r13
	testq	%r13, %r13
	je	.L219
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_iESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L219:
	testq	%r12, %r12
	je	.L296
	testq	%r15, %r15
	je	.L221
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%r15, %rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L291
	movq	%rax, %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC21(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$25, %edx
	movl	$1, %esi
	leaq	.LC22(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC23(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC24(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$15, %edx
	movl	$1, %esi
	leaq	.LC25(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$22, %edx
	movl	$1, %esi
	leaq	.LC26(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$61, %edx
	movl	$1, %esi
	leaq	.LC27(%rip), %rdi
	call	fwrite@PLT
	movl	-37544(%rbp), %eax
	testl	%eax, %eax
	jle	.L223
	xorl	%r15d, %r15d
	subl	$1, %eax
	movq	%r14, -37552(%rbp)
	leaq	.LC28(%rip), %rbx
	movq	%r15, %r14
	movq	%rax, %r15
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L224:
	testq	%r14, %r14
	jne	.L225
.L226:
	movzbl	(%r12,%r14), %ecx
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	leaq	1(%r14), %rax
	cmpq	%r15, %r14
	je	.L297
	movq	%rax, %r14
.L227:
	movl	%r14d, %eax
	andl	$31, %eax
	cmpl	$31, %eax
	jne	.L224
	movq	%r13, %rsi
	movl	$10, %edi
	call	fputc@PLT
.L225:
	movq	%r13, %rsi
	movl	$44, %edi
	call	fputc@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L205:
	movq	(%rbx), %rdx
	leaq	.LC14(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	call	_ZN2v88internal8FlagList9PrintHelpEv@PLT
	movzbl	_ZN2v88internal9FLAG_helpE(%rip), %eax
	xorl	$1, %eax
	movzbl	%al, %eax
.L204:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L298
	addq	$37528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L297:
	.cfi_restore_state
	movq	-37552(%rbp), %r14
.L223:
	movq	%r13, %rsi
	movl	$10, %edi
	call	fputc@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	movl	$1, %esi
	leaq	.LC29(%rip), %rdi
	call	fwrite@PLT
	movl	-37544(%rbp), %ecx
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	%r13, %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC31(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC32(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$57, %edx
	movl	$1, %esi
	leaq	.LC33(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$16, %edx
	movl	$1, %esi
	leaq	.LC34(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	movl	$1, %esi
	leaq	.LC35(%rip), %rdi
	call	fwrite@PLT
	movq	%r13, %rcx
	movl	$25, %edx
	movl	$1, %esi
	leaq	.LC36(%rip), %rdi
	call	fwrite@PLT
	leaq	.LC37(%rip), %rdi
	movq	%r13, %rcx
	movl	$19, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movq	%r13, %rdi
	call	fclose@PLT
.L221:
	movq	-37560(%rbp), %rax
	testq	%rax, %rax
	je	.L228
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L299
	movslq	-37544(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %esi
	movq	%r13, %rdx
	call	fwrite@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	fclose@PLT
	cmpq	%rbx, %r13
	jne	.L300
.L228:
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	-37520(%rbp), %rax
	testq	%rax, %rax
	je	.L230
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZdlPvm@PLT
.L230:
	movq	-37536(%rbp), %rax
	testq	%rax, %rax
	je	.L231
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZdlPvm@PLT
.L231:
	movq	-112(%rbp), %rdi
	leaq	16+_ZTVN2v88internal18EmbeddedFileWriterE(%rip), %rax
	movq	%rax, -37440(%rbp)
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	-144(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L233
	leaq	-160(%rbp), %r12
.L234:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L234
.L233:
	movq	-37528(%rbp), %r12
	movq	%r14, %rbx
	subq	$16, %r12
	.p2align 4,,10
	.p2align 3
.L238:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L238
.L236:
	call	_ZN2v88internal23FreeCurrentEmbeddedBlobEv@PLT
	call	_ZN2v82V87DisposeEv@PLT
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	movq	-37496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L245
	movq	(%rdi), %rax
	call	*8(%rax)
	xorl	%eax, %eax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L235:
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L238
	jmp	.L236
.L292:
	movl	$1, %edi
	movb	$1, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	call	_ZN2v88internal11CpuFeatures9ProbeImplEb@PLT
	jmp	.L208
.L294:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r13, %rdi
	subq	%r12, %rax
	movq	%rax, -37488(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	.LC17(%rip), %rdi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L215
.L293:
	movl	$48, %edi
	call	_Znwm@PLT
	leaq	_ZZN12_GLOBAL__N_123MaybeSetCounterFunctionEPN2v87IsolateEENUlPKcE_4_FUNES4_(%rip), %rsi
	movq	%rbx, %rdi
	leaq	8(%rax), %rdx
	movl	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rdx, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, _ZN12_GLOBAL__N_112counter_map_E(%rip)
	call	_ZN2v87Isolate18SetCounterFunctionEPFPiPKcE@PLT
	jmp	.L214
.L245:
	xorl	%eax, %eax
	jmp	.L204
.L295:
	movq	%rdx, -37552(%rbp)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%r13, %rdi
	subq	-37544(%rbp), %rax
	movq	%rax, -37488(%rbp)
	call	_ZNK2v84base9TimeDelta15InMillisecondsFEv@PLT
	leaq	.LC18(%rip), %rdi
	movl	$1, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-37552(%rbp), %rdx
	jmp	.L217
.L241:
	movq	$0, -37536(%rbp)
	jmp	.L212
.L243:
	movq	$0, -37520(%rbp)
	jmp	.L213
.L296:
	leaq	.LC19(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L299:
	movq	-37560(%rbp), %rsi
.L291:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	$1, %edi
	call	exit@PLT
.L298:
	call	__stack_chk_fail@PLT
.L300:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-37560(%rbp), %rdi
	call	remove@PLT
	movl	$1, %edi
	call	exit@PLT
	.cfi_endproc
.LFE19826:
	.size	main, .-main
	.section	.text.startup._GLOBAL__sub_I_main,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB24481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24481:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.section	.bss._ZN12_GLOBAL__N_112counter_map_E,"aw",@nobits
	.align 8
	.type	_ZN12_GLOBAL__N_112counter_map_E, @object
	.size	_ZN12_GLOBAL__N_112counter_map_E, 8
_ZN12_GLOBAL__N_112counter_map_E:
	.zero	8
	.section	.rodata._ZN2v88internalL23kDefaultEmbeddedVariantE,"a"
	.align 8
	.type	_ZN2v88internalL23kDefaultEmbeddedVariantE, @object
	.size	_ZN2v88internalL23kDefaultEmbeddedVariantE, 8
_ZN2v88internalL23kDefaultEmbeddedVariantE:
	.string	"Default"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
