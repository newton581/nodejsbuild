	.file	"platform-embedded-file-writer-base.cc"
	.text
	.section	.text._ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,"axG",@progbits,_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.type	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, @function
_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv:
.LFB1936:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE1936:
	.size	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, .-_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv:
.LFB1937:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE1937:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv:
.LFB1935:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	movl	$10, %edi
	jmp	fputc@PLT
	.cfi_endproc
.LFE1935:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.section	.rodata._ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0x%lx%016lx"
.LC1:
	.string	"0x%lx"
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh:
.LFB3170:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rcx
	movq	(%rsi), %r8
	movq	8(%rdi), %rdi
	testq	%rcx, %rcx
	je	.L6
	leaq	.LC0(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r8, %rcx
	leaq	.LC1(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3170:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh
	.section	.text._ZN2v88internal20PointerSizeDirectiveEv,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20PointerSizeDirectiveEv
	.type	_ZN2v88internal20PointerSizeDirectiveEv, @function
_ZN2v88internal20PointerSizeDirectiveEv:
.LFB3168:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE3168:
	.size	_ZN2v88internal20PointerSizeDirectiveEv, .-_ZN2v88internal20PointerSizeDirectiveEv
	.section	.rodata._ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE
	.type	_ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE, @function
_ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE:
.LFB3169:
	.cfi_startproc
	endbr64
	cmpl	$3, %edi
	ja	.L9
	movl	%edi, %edi
	leaq	CSWTCH.67(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	ret
.L9:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3169:
	.size	_ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE, .-_ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE
	.section	.rodata._ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"arm"
.LC4:
	.string	"arm64"
.LC5:
	.string	"ia32"
.LC6:
	.string	"x64"
.LC7:
	.string	"aix"
.LC8:
	.string	"chromeos"
.LC9:
	.string	"fuchsia"
.LC10:
	.string	"ios"
.LC11:
	.string	"mac"
.LC12:
	.string	"win"
	.section	.text._ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_
	.type	_ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_, @function
_ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_:
.LFB3177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$3, %ebx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L14
	leaq	-80(%rbp), %r13
	movq	%rsi, %rdi
	movq	%rsi, -120(%rbp)
	leaq	-96(%rbp), %r15
	movq	%r13, -96(%rbp)
	call	strlen@PLT
	movq	-120(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -104(%rbp)
	movq	%rax, %rbx
	ja	.L60
	cmpq	$1, %rax
	jne	.L17
	movzbl	(%r8), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L18:
	movq	%rax, -88(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movb	$0, (%rdx,%rax)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L19
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movl	$1, %ebx
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L61
.L19:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	testq	%r12, %r12
	je	.L62
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	leaq	-96(%rbp), %r15
	movq	%r13, -96(%rbp)
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L63
	cmpq	$1, %rax
	jne	.L24
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L25:
	movq	%rax, -88(%rbp)
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	movb	$0, (%rdx,%rax)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L26
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L64
	movq	-96(%rbp), %rdi
	movl	$1, %r12d
	cmpq	%r13, %rdi
	je	.L21
.L58:
	call	_ZdlPv@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r15, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L16:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r15, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L23:
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal29PlatformEmbeddedFileWriterAIXE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movl	$0, 20(%rax)
	movq	%rax, (%r14)
.L13:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	$5, %r12d
.L21:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal33PlatformEmbeddedFileWriterGenericE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movl	%r12d, 20(%rax)
	movq	%rax, (%r14)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L24:
	testq	%rax, %rax
	jne	.L66
	movq	%r13, %rdx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%rax, %rax
	jne	.L67
	movq	%r13, %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movl	$2, %ebx
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L19
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	setne	%bl
	addl	$3, %ebx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L28
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L29
.L32:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal29PlatformEmbeddedFileWriterMacE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movl	$3, 20(%rax)
	movq	%rax, (%r14)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-96(%rbp), %rdi
	movl	$2, %r12d
	cmpq	%r13, %rdi
	jne	.L58
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L32
	movq	%r15, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-96(%rbp), %rdi
	testl	%eax, %eax
	je	.L33
	movl	$5, %r12d
	cmpq	%r13, %rdi
	jne	.L58
	jmp	.L21
.L33:
	cmpq	%r13, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal29PlatformEmbeddedFileWriterWinE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movl	$4, 20(%rax)
	movq	%rax, (%r14)
	jmp	.L13
.L65:
	call	__stack_chk_fail@PLT
.L66:
	movq	%r13, %rdi
	jmp	.L23
.L67:
	movq	%r13, %rdi
	jmp	.L16
	.cfi_endproc
.LFE3177:
	.size	_ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_, .-_ZN2v88internal29NewPlatformEmbeddedFileWriterEPKcS2_
	.section	.rodata.CSWTCH.67,"a"
	.align 16
	.type	CSWTCH.67, @object
	.size	CSWTCH.67, 16
CSWTCH.67:
	.long	1
	.long	4
	.long	8
	.long	16
	.weak	_ZTVN2v88internal30PlatformEmbeddedFileWriterBaseE
	.section	.data.rel.ro._ZTVN2v88internal30PlatformEmbeddedFileWriterBaseE,"awG",@progbits,_ZTVN2v88internal30PlatformEmbeddedFileWriterBaseE,comdat
	.align 8
	.type	_ZTVN2v88internal30PlatformEmbeddedFileWriterBaseE, @object
	.size	_ZTVN2v88internal30PlatformEmbeddedFileWriterBaseE, 200
_ZTVN2v88internal30PlatformEmbeddedFileWriterBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
