	.file	"embedded-file-writer.cc"
	.text
	.section	.text._ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,"axG",@progbits,_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.type	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, @function
_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv:
.LFB8638:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE8638:
	.size	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, .-_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.section	.text._ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi
	.type	_ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi, @function
_ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi:
.LFB17861:
	.cfi_startproc
	endbr64
	movq	37328(%rdi), %rax
	subl	$1, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE17861:
	.size	_ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi, .-_ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi
	.section	.text._ZNK2v88internal18EmbeddedFileWriter34GetExternallyCompiledFilenameCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18EmbeddedFileWriter34GetExternallyCompiledFilenameCountEv
	.type	_ZNK2v88internal18EmbeddedFileWriter34GetExternallyCompiledFilenameCountEv, @function
_ZNK2v88internal18EmbeddedFileWriter34GetExternallyCompiledFilenameCountEv:
.LFB17862:
	.cfi_startproc
	endbr64
	movq	37320(%rdi), %rax
	ret
	.cfi_endproc
.LFE17862:
	.size	_ZNK2v88internal18EmbeddedFileWriter34GetExternallyCompiledFilenameCountEv, .-_ZNK2v88internal18EmbeddedFileWriter34GetExternallyCompiledFilenameCountEv
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv:
.LFB8637:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	movl	$10, %edi
	jmp	fputc@PLT
	.cfi_endproc
.LFE8637:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.section	.rodata._ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE
	.type	_ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE, @function
_ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE:
.LFB17863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L7:
	movq	7(%rax), %rax
	movslq	11(%rax), %rcx
	leaq	15(%rax,%rcx), %rbx
	testb	%dl, %dl
	je	.L10
.L12:
	movq	7(%rsi), %rsi
.L11:
	leaq	15(%rsi), %r12
	subq	%r12, %rbx
	js	.L38
	je	.L14
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	memcpy@PLT
	movq	(%r15), %rdi
	movq	16(%r15), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rbx
	jbe	.L39
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	memcpy@PLT
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	addq	%r12, %rbx
	movq	%r12, (%r15)
	movq	%rbx, 16(%r15)
	movq	%rbx, 8(%r15)
.L18:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L23:
	addl	$1, %r13d
	addq	$24, %r15
	cmpl	$1553, %r13d
	je	.L40
.L22:
	movq	-56(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	23(%rax), %rax
	movq	%rax, %rdx
	movq	%rax, %rsi
	notq	%rdx
	andl	$1, %edx
	jne	.L7
	movq	-1(%rax), %rcx
	cmpw	$70, 11(%rcx)
	jne	.L7
	movslq	11(%rax), %rcx
	leaq	15(%rax,%rcx), %rbx
	testb	%dl, %dl
	jne	.L12
	.p2align 4,,10
	.p2align 3
.L10:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L12
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%r15), %rax
	movq	%rax, 8(%r15)
	jmp	.L23
.L40:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L39:
	movq	8(%r15), %rax
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%rdx, %rbx
	jbe	.L41
	testq	%rdx, %rdx
	jne	.L42
	movq	%rax, %rdi
	movq	%rbx, %rdx
	testq	%rbx, %rbx
	jne	.L43
.L15:
	addq	(%r15), %rbx
	movq	%rbx, 8(%r15)
	jmp	.L18
.L41:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memmove@PLT
	addq	(%r15), %rbx
	movq	%rbx, 8(%r15)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r14, %rsi
	call	memmove@PLT
	movq	8(%r15), %rdi
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	subq	(%r15), %rsi
	subq	%rsi, %rdx
	je	.L15
	addq	%r14, %rsi
.L21:
	call	memmove@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r14, %rsi
	jmp	.L21
	.cfi_endproc
.LFE17863:
	.size	_ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE, .-_ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"v8_%s_embedded_blob_"
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Pointer to the beginning of the embedded blob."
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.1
.LC3:
	.string	"v8_%s_embedded_blob_data_"
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.8
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.1
.LC5:
	.string	"v8_%s_embedded_blob_size_"
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE.str1.8
	.align 8
.LC6:
	.string	"The size of the embedded blob in bytes."
	.section	.text._ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE
	.type	_ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE, @function
_ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE:
.LFB17834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-624(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$256, %esi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-592(%rbp), %rdi
	subq	$648, %rsp
	movq	%rdx, -672(%rbp)
	movq	37360(%rbx), %rcx
	leaq	.LC1(%rip), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -608(%rbp)
	movq	$256, -600(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	0(%r13), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	*112(%rax)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*48(%rax)
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movl	$256, %esi
	movq	37360(%rbx), %rcx
	leaq	.LC3(%rip), %rdx
	movq	64(%rax), %rax
	movq	%r12, -336(%rbp)
	movq	$256, -328(%rbp)
	movq	%rax, -664(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-336(%rbp), %r15
	leaq	-640(%rbp), %rax
	movq	%r14, -640(%rbp)
	movq	%rax, -680(%rbp)
	testq	%r15, %r15
	je	.L58
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -648(%rbp)
	movq	%rax, %r9
	cmpq	$15, %rax
	ja	.L59
	cmpq	$1, %rax
	jne	.L48
	movzbl	(%r15), %edx
	movb	%dl, -624(%rbp)
	movq	%r14, %rdx
.L49:
	movq	%rax, -632(%rbp)
	movq	%r13, %rdi
	movb	$0, (%rdx,%rax)
	movq	-664(%rbp), %rax
	movq	-640(%rbp), %rdx
	movq	-608(%rbp), %rsi
	call	*%rax
	movq	-640(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	0(%r13), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %r14
	movq	120(%rax), %rax
	cmpq	%r14, %rax
	jne	.L51
	movq	8(%r13), %rsi
	movl	$10, %edi
	call	fputc@PLT
.L52:
	movq	37360(%rbx), %rcx
	movq	%r12, %rdi
	movl	$256, %esi
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rdx
	movq	%r12, -336(%rbp)
	movq	$256, -328(%rbp)
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	0(%r13), %rax
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	*112(%rax)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*48(%rax)
	movq	-672(%rbp), %rax
	movq	-336(%rbp), %rsi
	movq	%r13, %rdi
	movl	8(%rax), %edx
	movq	0(%r13), %rax
	call	*56(%rax)
	movq	0(%r13), %rax
	movq	120(%rax), %rax
	cmpq	%r14, %rax
	jne	.L53
	movq	8(%r13), %rsi
	movl	$10, %edi
	call	fputc@PLT
.L54:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L61
	movq	%r14, %rdx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-680(%rbp), %rdi
	leaq	-648(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -688(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-688(%rbp), %r9
	movq	%rax, -640(%rbp)
	movq	%rax, %rdi
	movq	-648(%rbp), %rax
	movq	%rax, -624(%rbp)
.L47:
	movq	%r9, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-648(%rbp), %rax
	movq	-640(%rbp), %rdx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L54
.L60:
	call	__stack_chk_fail@PLT
.L61:
	movq	%r14, %rdi
	jmp	.L47
	.cfi_endproc
.LFE17834:
	.size	_ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE, .-_ZNK2v88internal18EmbeddedFileWriter17WriteFileEpilogueEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataE
	.section	.rodata._ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj.str1.1,"aMS",@progbits,1
.LC7:
	.string	","
	.section	.text._ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj
	.type	_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj, @function
_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj:
.LFB17837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv(%rip), %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%rsi, -56(%rbp)
	movl	$3, -76(%rbp)
	movq	160(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L91
.L63:
	movl	-76(%rbp), %edi
	call	_ZN2v88internal17DataDirectiveSizeENS0_13DataDirectiveE@PLT
	movl	%eax, -64(%rbp)
	movl	%eax, %r12d
	cmpl	%eax, %r14d
	jbe	.L83
	leal	(%rax,%rax), %eax
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	cltq
	addq	$3, %rax
	movq	%rax, -72(%rbp)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	%r12d, %r14d
	jbe	.L66
.L65:
	testl	%r13d, %r13d
	je	.L89
	movq	8(%r15), %rdi
	leaq	.LC7(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movl	%ebx, %edx
	movl	%r12d, %ebx
	addl	%eax, %r13d
.L68:
	movq	(%r15), %rax
	movl	%edx, %esi
	movq	%r15, %rdi
	addq	-56(%rbp), %rsi
	call	*168(%rax)
	addl	-64(%rbp), %r12d
	addl	%eax, %r13d
	movslq	%r13d, %rax
	addq	-72(%rbp), %rax
	cmpq	$100, %rax
	jbe	.L69
	movq	8(%r15), %rsi
	movl	$10, %edi
	call	fputc@PLT
	cmpl	%r12d, %r14d
	jbe	.L90
.L89:
	movq	(%r15), %rax
	movl	%ebx, %edx
	movl	-76(%rbp), %esi
	movq	%r15, %rdi
	movl	%edx, -60(%rbp)
	movl	%r12d, %ebx
	call	*152(%rax)
	movl	-60(%rbp), %edx
	movl	%eax, %r13d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L66:
	testl	%r13d, %r13d
	je	.L90
	movq	(%r15), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	je	.L92
	movq	%r15, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L90:
	cmpl	%ebx, %r14d
	jbe	.L62
.L72:
	xorl	%r12d, %r12d
	leaq	.LC7(%rip), %r13
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	%ebx, %r14d
	jbe	.L93
.L80:
	testl	%r12d, %r12d
	je	.L79
	movq	8(%r15), %rdi
	movq	%r13, %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	addl	%eax, %r12d
.L77:
	movq	-56(%rbp), %rcx
	movl	%ebx, %eax
	movq	%r15, %rdi
	addl	$1, %ebx
	movzbl	(%rcx,%rax), %esi
	movq	(%r15), %rax
	call	*104(%rax)
	addl	%eax, %r12d
	movslq	%r12d, %rax
	addq	$5, %rax
	cmpq	$100, %rax
	jbe	.L78
	movq	8(%r15), %rsi
	movl	$10, %edi
	call	fputc@PLT
	cmpl	%ebx, %r14d
	jbe	.L62
.L79:
	movq	(%r15), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	*152(%rax)
	movl	%eax, %r12d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L93:
	testl	%r12d, %r12d
	je	.L62
	movq	(%r15), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L81
	movq	8(%r15), %rsi
	addq	$40, %rsp
	movl	$10, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fputc@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	8(%r15), %rsi
	movl	$10, %edi
	call	fputc@PLT
	cmpl	%ebx, %r14d
	ja	.L72
.L62:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	call	*%rax
	movl	%eax, -76(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$40, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L90
	.cfi_endproc
.LFE17837:
	.size	_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj, .-_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj
	.section	.rodata._ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Builtins_%s"
.LC9:
	.string	"%s_Builtins_%s"
.LC10:
	.string	"next_offset >= i"
.LC11:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi
	.type	_ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi, @function
_ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi:
.LFB17833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	_ZN2v88internalL23kDefaultEmbeddedVariantE(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	37360(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strcmp@PLT
	leaq	-320(%rbp), %rdx
	movl	%r15d, %edi
	movq	$256, -328(%rbp)
	movq	%rdx, -336(%rbp)
	testl	%eax, %eax
	jne	.L95
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	-336(%rbp), %rdi
	movq	-328(%rbp), %rsi
	leaq	.LC8(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
.L96:
	movq	0(%r13), %rax
	movq	-336(%rbp), %rsi
	movq	%r13, %rdi
	call	*88(%rax)
	movslq	%r15d, %rax
	movl	$1, %ecx
	leaq	(%rax,%rax,2), %rax
	leaq	8(%r12,%rax,8), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	leaq	-400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	subq	%rsi, %rdx
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal12EmbeddedData24InstructionSizeOfBuiltinEi@PLT
	testl	%eax, %eax
	jne	.L117
.L100:
	movq	0(%r13), %rax
	movq	-336(%rbp), %rsi
	movq	%r13, %rdi
	call	*96(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	call	_ZN2v88internal8Builtins4nameEi@PLT
	movq	37360(%r12), %rcx
	movq	-336(%rbp), %rdi
	leaq	.LC9(%rip), %rdx
	movq	-328(%rbp), %rsi
	movq	%rax, %r8
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L117:
	leal	32(%rax), %ebx
	andl	$-32, %ebx
	cmpl	$-1, -376(%rbp)
	movl	%ebx, %r14d
	jne	.L119
.L98:
	testl	%ebx, %ebx
	je	.L100
	xorl	%r15d, %r15d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L101:
	cmpl	%r15d, %r14d
	jb	.L106
.L105:
	movl	%r14d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	addq	-408(%rbp), %rsi
	subl	%r15d, %edx
	call	_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj
	cmpl	%ebx, %r14d
	jnb	.L100
	movl	%r14d, %r15d
.L99:
	cmpl	%r15d, %r14d
	jne	.L101
	movq	0(%r13), %rax
	movq	-360(%rbp), %rsi
	leaq	_ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi(%rip), %rdi
	movq	80(%rax), %r14
	movq	(%r12), %rax
	movq	%rsi, %rcx
	shrq	$21, %rsi
	shrq	%rcx
	andl	$1023, %esi
	movq	8(%rax), %rax
	andl	$1048575, %ecx
	cmpq	%rdi, %rax
	jne	.L102
	movq	37328(%r12), %rdx
	leal	-1(%rsi), %eax
	cltq
	movq	(%rdx,%rax,8), %rdx
.L103:
	movq	%r13, %rdi
	call	*%r14
	movq	-416(%rbp), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -376(%rbp)
	je	.L104
	movl	-368(%rbp), %r14d
	cmpl	%r15d, %r14d
	jnb	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	cmpl	%r15d, %ebx
	jb	.L106
	subl	%r15d, %ebx
	movl	%r15d, %esi
	movq	%r13, %rdi
	addq	-408(%rbp), %rsi
	movl	%ebx, %edx
	call	_ZN2v88internal18EmbeddedFileWriter35WriteBinaryContentsAsInlineAssemblyEPNS0_30PlatformEmbeddedFileWriterBaseEPKhj
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L119:
	movl	-368(%rbp), %r14d
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L102:
	movl	%ecx, -420(%rbp)
	movq	%r12, %rdi
	call	*%rax
	movq	-360(%rbp), %rsi
	movl	-420(%rbp), %ecx
	movq	%rax, %rdx
	shrq	$21, %rsi
	andl	$1023, %esi
	jmp	.L103
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17833:
	.size	_ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi, .-_ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi
	.section	.text._ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_
	.type	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_, @function
_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_:
.LFB20774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	(%r14), %r15
	movq	16(%rbx), %r12
	movq	%rax, %r13
	movq	%r15, 32(%rax)
	movl	8(%r14), %eax
	leaq	8(%rbx), %r14
	movl	%eax, 40(%r13)
	testq	%r12, %r12
	jne	.L122
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L123
.L140:
	movq	%rax, %r12
.L122:
	movq	32(%r12), %rcx
	cmpq	%r15, %rcx
	ja	.L139
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L140
.L123:
	testb	%dl, %dl
	jne	.L141
	cmpq	%r15, %rcx
	jnb	.L131
.L130:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L142
.L128:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	cmpq	%r12, 24(%rbx)
	je	.L130
.L132:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r15
	ja	.L130
	movq	%rax, %r12
.L131:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r14, %r12
	cmpq	24(%rbx), %r14
	jne	.L132
	movl	$1, %edi
	jmp	.L128
	.cfi_endproc
.LFE20774:
	.size	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_, .-_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_
	.section	.rodata._ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC12:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB20790:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L157
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L153
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L158
.L145:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L152:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L159
	testq	%r13, %r13
	jg	.L148
	testq	%r9, %r9
	jne	.L151
.L149:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L148
.L151:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L158:
	testq	%rsi, %rsi
	jne	.L146
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L149
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$8, %r14d
	jmp	.L145
.L157:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L146:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L145
	.cfi_endproc
.LFE20790:
	.size	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZN2v88internal18EmbeddedFileWriter37LookupOrAddExternallyCompiledFilenameEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18EmbeddedFileWriter37LookupOrAddExternallyCompiledFilenameEPKc
	.type	_ZN2v88internal18EmbeddedFileWriter37LookupOrAddExternallyCompiledFilenameEPKc, @function
_ZN2v88internal18EmbeddedFileWriter37LookupOrAddExternallyCompiledFilenameEPKc:
.LFB17838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%rsi, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	37296(%rdi), %rax
	testq	%rax, %rax
	je	.L161
	leaq	37288(%rdi), %rsi
	movq	%rsi, %rcx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L163
.L162:
	cmpq	%rdx, 32(%rax)
	jnb	.L173
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L162
.L163:
	cmpq	%rcx, %rsi
	je	.L161
	cmpq	%rdx, 32(%rcx)
	ja	.L161
	movl	40(%rcx), %r12d
.L160:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	37320(%rbx), %eax
	leaq	-48(%rbp), %rsi
	leaq	37280(%rbx), %rdi
	movq	%rdx, -48(%rbp)
	leal	1(%rax), %r12d
	movl	%r12d, -40(%rbp)
	call	_ZNSt8_Rb_treeIPKcSt4pairIKS1_iESt10_Select1stIS4_ESt4lessIS1_ESaIS4_EE17_M_emplace_uniqueIJS2_IS1_iEEEES2_ISt17_Rb_tree_iteratorIS4_EbEDpOT_
	movq	37336(%rbx), %rsi
	cmpq	37344(%rbx), %rsi
	je	.L167
	movq	-56(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 37336(%rbx)
	jmp	.L160
.L167:
	leaq	-56(%rbp), %rdx
	leaq	37328(%rbx), %rdi
	call	_ZNSt6vectorIPKcSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L160
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17838:
	.size	_ZN2v88internal18EmbeddedFileWriter37LookupOrAddExternallyCompiledFilenameEPKc, .-_ZN2v88internal18EmbeddedFileWriter37LookupOrAddExternallyCompiledFilenameEPKc
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi, @function
_GLOBAL__sub_I__ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi:
.LFB21844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21844:
	.size	_GLOBAL__sub_I__ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi, .-_GLOBAL__sub_I__ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal18EmbeddedFileWriter12WriteBuiltinEPNS0_30PlatformEmbeddedFileWriterBaseEPKNS0_12EmbeddedDataEi
	.weak	_ZTVN2v88internal18EmbeddedFileWriterE
	.section	.data.rel.ro.local._ZTVN2v88internal18EmbeddedFileWriterE,"awG",@progbits,_ZTVN2v88internal18EmbeddedFileWriterE,comdat
	.align 8
	.type	_ZTVN2v88internal18EmbeddedFileWriterE, @object
	.size	_ZTVN2v88internal18EmbeddedFileWriterE, 48
_ZTVN2v88internal18EmbeddedFileWriterE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal18EmbeddedFileWriter37LookupOrAddExternallyCompiledFilenameEPKc
	.quad	_ZNK2v88internal18EmbeddedFileWriter29GetExternallyCompiledFilenameEi
	.quad	_ZNK2v88internal18EmbeddedFileWriter34GetExternallyCompiledFilenameCountEv
	.quad	_ZN2v88internal18EmbeddedFileWriter31PrepareBuiltinSourcePositionMapEPNS0_8BuiltinsE
	.section	.rodata._ZN2v88internalL23kDefaultEmbeddedVariantE,"a"
	.align 8
	.type	_ZN2v88internalL23kDefaultEmbeddedVariantE, @object
	.size	_ZN2v88internalL23kDefaultEmbeddedVariantE, 8
_ZN2v88internalL23kDefaultEmbeddedVariantE:
	.string	"Default"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
