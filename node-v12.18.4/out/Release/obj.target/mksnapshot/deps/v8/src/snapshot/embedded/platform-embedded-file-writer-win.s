	.file	"platform-embedded-file-writer-win.cc"
	.text
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv:
.LFB3389:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3389:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc:
.LFB3408:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3408:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFunctionEPKc
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFunctionEPKc,_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv:
.LFB3410:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3410:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin15EndPdataSectionEv
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterWin15EndPdataSectionEv,_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin12FileEpilogueEv
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterWin12FileEpilogueEv,_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv
	.section	.text._ZNK2v88internal29PlatformEmbeddedFileWriterWin22ByteChunkDataDirectiveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal29PlatformEmbeddedFileWriterWin22ByteChunkDataDirectiveEv
	.type	_ZNK2v88internal29PlatformEmbeddedFileWriterWin22ByteChunkDataDirectiveEv, @function
_ZNK2v88internal29PlatformEmbeddedFileWriterWin22ByteChunkDataDirectiveEv:
.LFB3414:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE3414:
	.size	_ZNK2v88internal29PlatformEmbeddedFileWriterWin22ByteChunkDataDirectiveEv, .-_ZNK2v88internal29PlatformEmbeddedFileWriterWin22ByteChunkDataDirectiveEv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWinD2Ev,"axG",@progbits,_ZN2v88internal29PlatformEmbeddedFileWriterWinD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterWinD2Ev
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWinD2Ev, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWinD2Ev:
.LFB3949:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3949:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWinD2Ev, .-_ZN2v88internal29PlatformEmbeddedFileWriterWinD2Ev
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterWinD1Ev
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterWinD1Ev,_ZN2v88internal29PlatformEmbeddedFileWriterWinD2Ev
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv:
.LFB2764:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	movl	$10, %edi
	jmp	fputc@PLT
	.cfi_endproc
.LFE2764:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	".section .text\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv:
.LFB3390:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$15, %edx
	movl	$1, %esi
	leaq	.LC0(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3390:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	".section .data\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv:
.LFB3391:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$15, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3391:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	".section .rdata\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv:
.LFB3392:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$16, %edx
	movl	$1, %esi
	leaq	.LC2(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3392:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	".balign 32\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv:
.LFB3402:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$11, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3402:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	".balign 8\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv:
.LFB3403:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$10, %edx
	movl	$1, %esi
	leaq	.LC4(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3403:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc.str1.1,"aMS",@progbits,1
.LC5:
	.string	"// %s\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc:
.LFB3404:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3404:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc.str1.1,"aMS",@progbits,1
.LC6:
	.string	""
.LC7:
	.string	"%s%s:\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc:
.LFB3405:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %r8
	leaq	.LC6(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdx
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3405:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci.str1.1,"aMS",@progbits,1
.LC8:
	.string	".loc %d %d\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci:
.LFB3406:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	%ecx, %r8d
	leaq	.LC8(%rip), %rdx
	movl	%esi, %ecx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3406:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm.str1.1,"aMS",@progbits,1
.LC9:
	.string	"0x%lx"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm:
.LFB3409:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC9(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3409:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin14WriteByteChunkEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin14WriteByteChunkEPKh
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin14WriteByteChunkEPKh, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin14WriteByteChunkEPKh:
.LFB3415:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh@PLT
	.cfi_endproc
.LFE3415:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin14WriteByteChunkEPKh, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin14WriteByteChunkEPKh
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWinD0Ev,"axG",@progbits,_ZN2v88internal29PlatformEmbeddedFileWriterWinD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterWinD0Ev
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWinD0Ev, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWinD0Ev:
.LFB3951:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3951:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWinD0Ev, .-_ZN2v88internal29PlatformEmbeddedFileWriterWinD0Ev
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc.str1.1,"aMS",@progbits,1
.LC11:
	.string	".file %d \"%s\"\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc:
.LFB3411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%rdx, %rdx
	je	.L33
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movl	%esi, %r12d
	movq	%rdx, %r15
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L34
	cmpq	$1, %rax
	jne	.L23
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L24:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r8
	addq	%rax, %r8
	cmpq	%r8, %rax
	je	.L25
	.p2align 4,,10
	.p2align 3
.L27:
	cmpb	$92, (%rax)
	jne	.L26
	movb	$47, (%rax)
.L26:
	addq	$1, %rax
	cmpq	%rax, %r8
	jne	.L27
	movq	-96(%rbp), %r8
.L25:
	movq	8(%rbx), %rdi
	movl	%r12d, %ecx
	leaq	.LC11(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L36
	movq	%r13, %rdx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L22:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC10(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L35:
	call	__stack_chk_fail@PLT
.L36:
	movq	%r13, %rdi
	jmp	.L22
	.cfi_endproc
.LFE3411:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE.str1.1,"aMS",@progbits,1
.LC12:
	.string	".byte"
.LC13:
	.string	".long"
.LC14:
	.string	".quad"
.LC15:
	.string	".octa"
.LC16:
	.string	"unreachable code"
.LC17:
	.string	"  %s "
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE:
.LFB3413:
	.cfi_startproc
	endbr64
	cmpl	$2, %esi
	je	.L40
	ja	.L39
	testl	%esi, %esi
	leaq	.LC13(%rip), %rcx
	leaq	.LC12(%rip), %rax
	cmove	%rax, %rcx
.L38:
	movq	8(%rdi), %rdi
	leaq	.LC17(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$3, %esi
	jne	.L46
	leaq	.LC15(%rip), %rcx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	.LC14(%rip), %rcx
	jmp	.L38
.L46:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3413:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc.str1.1,"aMS",@progbits,1
.LC18:
	.string	".global %s%s\n"
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	".def %s%s; .scl 2; .type 32; .endef;\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc:
.LFB3407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L48
	movq	8(%rdi), %rdi
	movq	%rsi, %r8
	leaq	.LC6(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L49:
	cmpl	$1, 16(%rbx)
	movq	8(%rbx), %rdi
	movq	%r12, %r8
	leaq	.LC6(%rip), %rcx
	leaq	.LC18(%rip), %rdx
	je	.L52
	leaq	.LC19(%rip), %rdx
.L52:
	popq	%rbx
	movl	$1, %esi
	popq	%r12
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	call	*%rax
	jmp	.L49
	.cfi_endproc
.LFE3407:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"  %s %s%s\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_:
.LFB3394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	.LC6(%rip), %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	.LC18(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	(%rbx), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L54
	movq	8(%rbx), %rdi
	movq	%r13, %r8
	leaq	.LC6(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L55:
	call	_ZN2v88internal20PointerSizeDirectiveEv@PLT
	cmpl	$2, %eax
	je	.L58
	ja	.L57
	testl	%eax, %eax
	leaq	.LC13(%rip), %rcx
	leaq	.LC12(%rip), %rax
	cmove	%rax, %rcx
.L56:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %r9
	movl	$1, %esi
	popq	%rbx
	leaq	.LC6(%rip), %r8
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	leaq	.LC20(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L62
	leaq	.LC15(%rip), %rcx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	.LC14(%rip), %rcx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L55
.L62:
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3394:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj.str1.1,"aMS",@progbits,1
.LC21:
	.string	"%d"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj:
.LFB3393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	.LC6(%rip), %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	leaq	.LC18(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L64
	movq	8(%r12), %rdi
	movq	%r13, %r8
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	call	__fprintf_chk@PLT
.L65:
	movq	(%r12), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L66
	movq	8(%r12), %rdi
	leaq	.LC13(%rip), %rcx
	leaq	.LC17(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L67:
	movq	8(%r12), %rdi
	movl	%r14d, %ecx
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L68
	movq	8(%r12), %rsi
	addq	$8, %rsp
	movl	$10, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fputc@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L68:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L67
	.cfi_endproc
.LFE3393:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin17StartPdataSectionEv.str1.1,"aMS",@progbits,1
.LC22:
	.string	".section .pdata\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin17StartPdataSectionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartPdataSectionEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartPdataSectionEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartPdataSectionEv:
.LFB3395:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$16, %edx
	movl	$1, %esi
	leaq	.LC22(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3395:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartPdataSectionEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartPdataSectionEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin17StartXdataSectionEv.str1.1,"aMS",@progbits,1
.LC23:
	.string	".section .xdata\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin17StartXdataSectionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartXdataSectionEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartXdataSectionEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartXdataSectionEv:
.LFB3397:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$16, %edx
	movl	$1, %esi
	leaq	.LC23(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE3397:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartXdataSectionEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin17StartXdataSectionEv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin15EndXdataSectionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin15EndXdataSectionEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin15EndXdataSectionEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin15EndXdataSectionEv:
.LFB3972:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3972:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin15EndXdataSectionEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin15EndXdataSectionEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareRvaToSymbolEPKcm.str1.1,"aMS",@progbits,1
.LC24:
	.string	".rva %s + %lu\n"
.LC25:
	.string	".rva %s\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareRvaToSymbolEPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareRvaToSymbolEPKcm
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareRvaToSymbolEPKcm, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareRvaToSymbolEPKcm:
.LFB3400:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	movq	%rdx, %r8
	testq	%rdx, %rdx
	je	.L74
	leaq	.LC24(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	.LC25(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3400:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareRvaToSymbolEPKcm, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareRvaToSymbolEPKcm
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterWin19DeclareSymbolGlobalEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterWin19DeclareSymbolGlobalEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterWin19DeclareSymbolGlobalEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterWin19DeclareSymbolGlobalEPKc:
.LFB3401:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %r8
	leaq	.LC6(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rdx
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE3401:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterWin19DeclareSymbolGlobalEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterWin19DeclareSymbolGlobalEPKc
	.weak	_ZTVN2v88internal29PlatformEmbeddedFileWriterWinE
	.section	.data.rel.ro.local._ZTVN2v88internal29PlatformEmbeddedFileWriterWinE,"awG",@progbits,_ZTVN2v88internal29PlatformEmbeddedFileWriterWinE,comdat
	.align 8
	.type	_ZTVN2v88internal29PlatformEmbeddedFileWriterWinE, @object
	.size	_ZTVN2v88internal29PlatformEmbeddedFileWriterWinE, 200
_ZTVN2v88internal29PlatformEmbeddedFileWriterWinE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWinD1Ev
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWinD0Ev
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionTextEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin11SectionDataEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin13SectionRoDataEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToCodeAlignmentEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin20AlignToDataAlignmentEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin13DeclareUint32EPKcj
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin22DeclarePointerToSymbolEPKcS3_
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin12DeclareLabelEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin10SourceInfoEiPKci
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin20DeclareFunctionBeginEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin18DeclareFunctionEndEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin10HexLiteralEm
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin7CommentEPKc
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin12FilePrologueEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin23DeclareExternalFilenameEiPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin12FileEpilogueEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin21IndentedDataDirectiveENS0_13DataDirectiveE
	.quad	_ZNK2v88internal29PlatformEmbeddedFileWriterWin22ByteChunkDataDirectiveEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin14WriteByteChunkEPKh
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterWin19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
