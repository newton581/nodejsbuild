	.file	"platform-embedded-file-writer-mac.cc"
	.text
	.section	.text._ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,"axG",@progbits,_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.type	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, @function
_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv:
.LFB2765:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE2765:
	.size	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv, .-_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv:
.LFB2766:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2766:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac18DeclareFunctionEndEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac18DeclareFunctionEndEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac18DeclareFunctionEndEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac18DeclareFunctionEndEPKc:
.LFB2790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2790:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac18DeclareFunctionEndEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac18DeclareFunctionEndEPKc
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv:
.LFB2792:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2792:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac12FileEpilogueEv
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterMac12FileEpilogueEv,_ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMacD2Ev,"axG",@progbits,_ZN2v88internal29PlatformEmbeddedFileWriterMacD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterMacD2Ev
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMacD2Ev, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMacD2Ev:
.LFB3288:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3288:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMacD2Ev, .-_ZN2v88internal29PlatformEmbeddedFileWriterMacD2Ev
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterMacD1Ev
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterMacD1Ev,_ZN2v88internal29PlatformEmbeddedFileWriterMacD2Ev
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv:
.LFB2764:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	movl	$10, %edi
	jmp	fputc@PLT
	.cfi_endproc
.LFE2764:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	".text\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv:
.LFB2778:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$6, %edx
	movl	$1, %esi
	leaq	.LC0(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2778:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	".data\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv:
.LFB2779:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$6, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2779:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	".const_data\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv:
.LFB2780:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$12, %edx
	movl	$1, %esi
	leaq	.LC2(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2780:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	".balign 32\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv:
.LFB2784:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$11, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2784:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	".balign 8\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv:
.LFB2785:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$10, %edx
	movl	$1, %esi
	leaq	.LC4(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2785:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc.str1.1,"aMS",@progbits,1
.LC5:
	.string	"// %s\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc:
.LFB2786:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2786:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc.str1.1,"aMS",@progbits,1
.LC6:
	.string	"_%s:\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc:
.LFB2787:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC6(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2787:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci.str1.1,"aMS",@progbits,1
.LC7:
	.string	".loc %d %d\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci:
.LFB2788:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	%ecx, %r8d
	leaq	.LC7(%rip), %rdx
	movl	%esi, %ecx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2788:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm.str1.1,"aMS",@progbits,1
.LC8:
	.string	"0x%lx"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm:
.LFB2791:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC8(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2791:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc.str1.1,"aMS",@progbits,1
.LC9:
	.string	".file %d \"%s\"\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc:
.LFB2793:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	%esi, %ecx
	movq	%rdx, %r8
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2793:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMacD0Ev,"axG",@progbits,_ZN2v88internal29PlatformEmbeddedFileWriterMacD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterMacD0Ev
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMacD0Ev, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMacD0Ev:
.LFB3290:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3290:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMacD0Ev, .-_ZN2v88internal29PlatformEmbeddedFileWriterMacD0Ev
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE.str1.1,"aMS",@progbits,1
.LC10:
	.string	".byte"
.LC11:
	.string	".long"
.LC12:
	.string	".quad"
.LC13:
	.string	".octa"
.LC14:
	.string	"unreachable code"
.LC15:
	.string	"  %s "
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE:
.LFB2795:
	.cfi_startproc
	endbr64
	cmpl	$2, %esi
	je	.L22
	ja	.L21
	testl	%esi, %esi
	leaq	.LC11(%rip), %rcx
	leaq	.LC10(%rip), %rax
	cmove	%rax, %rcx
.L20:
	movq	8(%rdi), %rdi
	leaq	.LC15(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	cmpl	$3, %esi
	jne	.L28
	leaq	.LC13(%rip), %rcx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC12(%rip), %rcx
	jmp	.L20
.L28:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE2795:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac20DeclareFunctionBeginEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac20DeclareFunctionBeginEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac20DeclareFunctionBeginEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac20DeclareFunctionBeginEPKc:
.LFB2789:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc(%rip), %rdx
	movq	%rsi, %rcx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L30
	movq	8(%rdi), %rdi
	leaq	.LC6(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	jmp	*%rax
	.cfi_endproc
.LFE2789:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac20DeclareFunctionBeginEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac20DeclareFunctionBeginEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_.str1.1,"aMS",@progbits,1
.LC16:
	.string	".private_extern _%s\n"
.LC17:
	.string	"  %s _%s\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_:
.LFB2782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	leaq	.LC16(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	(%rbx), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L32
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	leaq	.LC6(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L33:
	call	_ZN2v88internal20PointerSizeDirectiveEv@PLT
	cmpl	$2, %eax
	je	.L36
	ja	.L35
	testl	%eax, %eax
	leaq	.LC11(%rip), %rcx
	leaq	.LC10(%rip), %rax
	cmove	%rax, %rcx
.L34:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	movq	%r12, %r8
	movl	$1, %esi
	popq	%rbx
	leaq	.LC17(%rip), %rdx
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L40
	leaq	.LC13(%rip), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC12(%rip), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L33
.L40:
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE2782:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj.str1.1,"aMS",@progbits,1
.LC18:
	.string	"%d"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj:
.LFB2781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	leaq	.LC16(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L42
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdx
	call	__fprintf_chk@PLT
.L43:
	movq	(%r12), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L44
	movq	8(%r12), %rdi
	leaq	.LC11(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L45:
	movq	8(%r12), %rdi
	movl	%r14d, %ecx
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L46
	movq	8(%r12), %rsi
	addq	$8, %rsp
	movl	$10, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fputc@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L45
	.cfi_endproc
.LFE2781:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterMac19DeclareSymbolGlobalEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterMac19DeclareSymbolGlobalEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterMac19DeclareSymbolGlobalEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterMac19DeclareSymbolGlobalEPKc:
.LFB2783:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC16(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2783:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterMac19DeclareSymbolGlobalEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterMac19DeclareSymbolGlobalEPKc
	.weak	_ZTVN2v88internal29PlatformEmbeddedFileWriterMacE
	.section	.data.rel.ro._ZTVN2v88internal29PlatformEmbeddedFileWriterMacE,"awG",@progbits,_ZTVN2v88internal29PlatformEmbeddedFileWriterMacE,comdat
	.align 8
	.type	_ZTVN2v88internal29PlatformEmbeddedFileWriterMacE, @object
	.size	_ZTVN2v88internal29PlatformEmbeddedFileWriterMacE, 200
_ZTVN2v88internal29PlatformEmbeddedFileWriterMacE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMacD1Ev
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMacD0Ev
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionTextEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac11SectionDataEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac13SectionRoDataEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToCodeAlignmentEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac20AlignToDataAlignmentEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac13DeclareUint32EPKcj
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac22DeclarePointerToSymbolEPKcS3_
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac12DeclareLabelEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac10SourceInfoEiPKci
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac20DeclareFunctionBeginEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac18DeclareFunctionEndEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac10HexLiteralEm
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac7CommentEPKc
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac12FilePrologueEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac23DeclareExternalFilenameEiPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac12FileEpilogueEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterMac21IndentedDataDirectiveENS0_13DataDirectiveE
	.quad	_ZNK2v88internal30PlatformEmbeddedFileWriterBase22ByteChunkDataDirectiveEv
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase14WriteByteChunkEPKh
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
