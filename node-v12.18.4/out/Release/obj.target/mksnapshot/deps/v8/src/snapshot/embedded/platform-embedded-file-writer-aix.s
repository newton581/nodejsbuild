	.file	"platform-embedded-file-writer-aix.cc"
	.text
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv:
.LFB2766:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2766:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX18DeclareFunctionEndEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX18DeclareFunctionEndEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX18DeclareFunctionEndEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX18DeclareFunctionEndEPKc:
.LFB2790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2790:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX18DeclareFunctionEndEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX18DeclareFunctionEndEPKc
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv:
.LFB2792:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2792:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FileEpilogueEv
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FileEpilogueEv,_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX23DeclareExternalFilenameEiPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX23DeclareExternalFilenameEiPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX23DeclareExternalFilenameEiPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX23DeclareExternalFilenameEiPKc:
.LFB2793:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2793:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX23DeclareExternalFilenameEiPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX23DeclareExternalFilenameEiPKc
	.section	.text._ZNK2v88internal29PlatformEmbeddedFileWriterAIX22ByteChunkDataDirectiveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal29PlatformEmbeddedFileWriterAIX22ByteChunkDataDirectiveEv
	.type	_ZNK2v88internal29PlatformEmbeddedFileWriterAIX22ByteChunkDataDirectiveEv, @function
_ZNK2v88internal29PlatformEmbeddedFileWriterAIX22ByteChunkDataDirectiveEv:
.LFB2796:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2796:
	.size	_ZNK2v88internal29PlatformEmbeddedFileWriterAIX22ByteChunkDataDirectiveEv, .-_ZNK2v88internal29PlatformEmbeddedFileWriterAIX22ByteChunkDataDirectiveEv
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIXD2Ev,"axG",@progbits,_ZN2v88internal29PlatformEmbeddedFileWriterAIXD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD2Ev
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD2Ev, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIXD2Ev:
.LFB3290:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3290:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD2Ev, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIXD2Ev
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD1Ev
	.set	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD1Ev,_ZN2v88internal29PlatformEmbeddedFileWriterAIXD2Ev
	.section	.text._ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,"axG",@progbits,_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.type	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, @function
_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv:
.LFB2764:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	movl	$10, %edi
	jmp	fputc@PLT
	.cfi_endproc
.LFE2764:
	.size	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv, .-_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	".csect .text[PR]\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv:
.LFB2778:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$17, %edx
	movl	$1, %esi
	leaq	.LC0(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2778:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	".csect .data[RW]\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv:
.LFB2779:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$17, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2779:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	".csect[RO]\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv:
.LFB2780:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$11, %edx
	movl	$1, %esi
	leaq	.LC2(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2780:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	".align 5\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv:
.LFB2784:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$9, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2784:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	".align 3\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv:
.LFB2785:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	$9, %edx
	movl	$1, %esi
	leaq	.LC4(%rip), %rdi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE2785:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc.str1.1,"aMS",@progbits,1
.LC5:
	.string	"// %s\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc:
.LFB2786:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2786:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci.str1.1,"aMS",@progbits,1
.LC6:
	.string	".xline %d, \"%s\"\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci:
.LFB2788:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rdx, %r8
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdx
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2788:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm.str1.1,"aMS",@progbits,1
.LC7:
	.string	"0x%lx"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm:
.LFB2791:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC7(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2791:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc.str1.1,"aMS",@progbits,1
.LC8:
	.string	".globl %s\n"
.LC9:
	.string	"%s:\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc:
.LFB2787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	leaq	.LC8(%rip), %rdx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	popq	%rbx
	leaq	.LC9(%rip), %rdx
	popq	%r12
	movl	$1, %esi
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2787:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE.str1.1,"aMS",@progbits,1
.LC10:
	.string	".long"
.LC11:
	.string	".llong"
.LC12:
	.string	".byte"
.LC13:
	.string	"unreachable code"
.LC14:
	.string	"  %s "
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE:
.LFB2795:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L21
	cmpl	$2, %esi
	je	.L22
	testl	%esi, %esi
	je	.L23
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	.LC11(%rip), %rcx
.L20:
	movq	8(%rdi), %rdi
	leaq	.LC14(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	.LC12(%rip), %rcx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC10(%rip), %rcx
	jmp	.L20
	.cfi_endproc
.LFE2795:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIXD0Ev,"axG",@progbits,_ZN2v88internal29PlatformEmbeddedFileWriterAIXD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD0Ev
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD0Ev, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIXD0Ev:
.LFB3292:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3292:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD0Ev, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIXD0Ev
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX14WriteByteChunkEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX14WriteByteChunkEPKh
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX14WriteByteChunkEPKh, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX14WriteByteChunkEPKh:
.LFB2797:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm(%rip), %rdx
	movl	(%rsi), %ecx
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L29
	movq	8(%rdi), %rdi
	leaq	.LC7(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rcx, %rsi
	jmp	*%rax
	.cfi_endproc
.LFE2797:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX14WriteByteChunkEPKh, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX14WriteByteChunkEPKh
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc.str1.1,"aMS",@progbits,1
.LC15:
	.string	".csect %s[DS]\n"
.LC16:
	.string	".llong .%s, 0, 0\n"
.LC17:
	.string	".%s:\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc:
.LFB2789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L31
	movq	8(%rdi), %rsi
	movl	$10, %edi
	call	fputc@PLT
.L32:
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	leaq	.LC8(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	leaq	.LC15(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	(%rbx), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L33
	movq	8(%rbx), %rcx
	movl	$17, %edx
	movl	$1, %esi
	leaq	.LC0(%rip), %rdi
	call	fwrite@PLT
.L34:
	movq	8(%rbx), %rdi
	movq	%r12, %rcx
	popq	%rbx
	leaq	.LC17(%rip), %rdx
	popq	%r12
	movl	$1, %esi
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	__fprintf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	*%rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L34
	.cfi_endproc
.LFE2789:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj.str1.1,"aMS",@progbits,1
.LC18:
	.string	".align 2\n"
.LC19:
	.string	"%d\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj:
.LFB2781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	leaq	.LC8(%rip), %rdx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	__fprintf_chk@PLT
	movq	8(%r12), %rcx
	movl	$9, %edx
	movl	$1, %esi
	leaq	.LC18(%rip), %rdi
	call	fwrite@PLT
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L37
	movq	8(%r12), %rdi
	leaq	.LC10(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L38:
	movq	8(%r12), %rdi
	movl	%r14d, %ecx
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L39
	movq	8(%r12), %rsi
	addq	$8, %rsp
	movl	$10, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fputc@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2781:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj
	.section	.rodata._ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"  %s %s\n"
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_:
.LFB2782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv(%rip), %rdx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L42
	movq	8(%rdi), %rcx
	movl	$9, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	call	fwrite@PLT
.L43:
	movq	(%r12), %rax
	leaq	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L44
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	8(%r12), %rdi
	movq	%r13, %rcx
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rdx
	movl	$1, %esi
	call	__fprintf_chk@PLT
.L45:
	call	_ZN2v88internal20PointerSizeDirectiveEv@PLT
	cmpl	$1, %eax
	je	.L48
	cmpl	$2, %eax
	je	.L49
	testl	%eax, %eax
	je	.L50
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC11(%rip), %rcx
.L46:
	movq	8(%r12), %rdi
	movq	%rbx, %r8
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	(%r12), %rax
	leaq	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L47
	movq	8(%r12), %rsi
	addq	$8, %rsp
	movl	$10, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	fputc@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	.LC12(%rip), %rcx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC10(%rip), %rcx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L42:
	call	*%rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2782:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_
	.section	.text._ZN2v88internal29PlatformEmbeddedFileWriterAIX19DeclareSymbolGlobalEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PlatformEmbeddedFileWriterAIX19DeclareSymbolGlobalEPKc
	.type	_ZN2v88internal29PlatformEmbeddedFileWriterAIX19DeclareSymbolGlobalEPKc, @function
_ZN2v88internal29PlatformEmbeddedFileWriterAIX19DeclareSymbolGlobalEPKc:
.LFB2783:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rsi, %rcx
	leaq	.LC8(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	jmp	__fprintf_chk@PLT
	.cfi_endproc
.LFE2783:
	.size	_ZN2v88internal29PlatformEmbeddedFileWriterAIX19DeclareSymbolGlobalEPKc, .-_ZN2v88internal29PlatformEmbeddedFileWriterAIX19DeclareSymbolGlobalEPKc
	.weak	_ZTVN2v88internal29PlatformEmbeddedFileWriterAIXE
	.section	.data.rel.ro.local._ZTVN2v88internal29PlatformEmbeddedFileWriterAIXE,"awG",@progbits,_ZTVN2v88internal29PlatformEmbeddedFileWriterAIXE,comdat
	.align 8
	.type	_ZTVN2v88internal29PlatformEmbeddedFileWriterAIXE, @object
	.size	_ZTVN2v88internal29PlatformEmbeddedFileWriterAIXE, 200
_ZTVN2v88internal29PlatformEmbeddedFileWriterAIXE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD1Ev
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIXD0Ev
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionTextEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX11SectionDataEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13SectionRoDataEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToCodeAlignmentEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20AlignToDataAlignmentEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX13DeclareUint32EPKcj
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX22DeclarePointerToSymbolEPKcS3_
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12DeclareLabelEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10SourceInfoEiPKci
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX20DeclareFunctionBeginEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX18DeclareFunctionEndEPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX10HexLiteralEm
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX7CommentEPKc
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase7NewlineEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FilePrologueEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX23DeclareExternalFilenameEiPKc
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX12FileEpilogueEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX21IndentedDataDirectiveENS0_13DataDirectiveE
	.quad	_ZNK2v88internal29PlatformEmbeddedFileWriterAIX22ByteChunkDataDirectiveEv
	.quad	_ZN2v88internal29PlatformEmbeddedFileWriterAIX14WriteByteChunkEPKh
	.quad	_ZN2v88internal30PlatformEmbeddedFileWriterBase19MaybeEmitUnwindDataEPKcS3_PKNS0_12EmbeddedDataEPKv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
