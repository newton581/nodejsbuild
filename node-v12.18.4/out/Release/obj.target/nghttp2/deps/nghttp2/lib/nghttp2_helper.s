	.file	"nghttp2_helper.c"
	.text
	.p2align 4
	.globl	nghttp2_put_uint16be
	.type	nghttp2_put_uint16be, @function
nghttp2_put_uint16be:
.LFB34:
	.cfi_startproc
	endbr64
	rolw	$8, %si
	movw	%si, (%rdi)
	ret
	.cfi_endproc
.LFE34:
	.size	nghttp2_put_uint16be, .-nghttp2_put_uint16be
	.p2align 4
	.globl	nghttp2_put_uint32be
	.type	nghttp2_put_uint32be, @function
nghttp2_put_uint32be:
.LFB35:
	.cfi_startproc
	endbr64
	bswap	%esi
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE35:
	.size	nghttp2_put_uint32be, .-nghttp2_put_uint32be
	.p2align 4
	.globl	nghttp2_get_uint16
	.type	nghttp2_get_uint16, @function
nghttp2_get_uint16:
.LFB36:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	rolw	$8, %ax
	ret
	.cfi_endproc
.LFE36:
	.size	nghttp2_get_uint16, .-nghttp2_get_uint16
	.p2align 4
	.globl	nghttp2_get_uint32
	.type	nghttp2_get_uint32, @function
nghttp2_get_uint32:
.LFB37:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	bswap	%eax
	ret
	.cfi_endproc
.LFE37:
	.size	nghttp2_get_uint32, .-nghttp2_get_uint32
	.p2align 4
	.globl	nghttp2_downcase
	.type	nghttp2_downcase, @function
nghttp2_downcase:
.LFB38:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L6
	addq	%rdi, %rsi
	leaq	DOWNCASE_TBL(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	movzbl	(%rdx,%rax), %eax
	movb	%al, -1(%rdi)
	cmpq	%rsi, %rdi
	jne	.L8
.L6:
	ret
	.cfi_endproc
.LFE38:
	.size	nghttp2_downcase, .-nghttp2_downcase
	.p2align 4
	.globl	nghttp2_adjust_local_window_size
	.type	nghttp2_adjust_local_window_size, @function
nghttp2_adjust_local_window_size:
.LFB39:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L14
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	movl	$0, %r8d
	cmovns	(%rsi), %r8d
	movl	%r8d, %r9d
	subl	%eax, %r9d
	jns	.L23
	subl	%r8d, %eax
	movl	$2147483647, %r9d
	movl	(%rdi), %r8d
	subl	%eax, %r9d
	cmpl	%r9d, %r8d
	jg	.L22
	addl	%eax, %r8d
	movl	%r8d, (%rdi)
	movl	(%rdx), %edi
	cmpl	%eax, %edi
	cmovle	%edi, %eax
	subl	%eax, %edi
	movl	%edi, (%rdx)
	movl	(%rsi), %edi
	testl	%edi, %edi
	leal	(%rdi,%rax), %edx
	cmovns	%eax, %edx
	movl	%edx, (%rsi)
	subl	%eax, (%rcx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%r9d, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%eax, %r9d
	addl	(%rdi), %r9d
	js	.L22
	movl	$-2147483648, %r8d
	subl	%eax, %r8d
	cmpl	%r8d, (%rsi)
	jl	.L22
	addl	$2147483647, %eax
	cmpl	%eax, (%rdx)
	jg	.L22
	movl	%r9d, (%rdi)
	movl	(%rcx), %eax
	addl	%eax, (%rsi)
	movl	(%rcx), %eax
	subl	%eax, (%rdx)
	xorl	%eax, %eax
	movl	$0, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$-524, %eax
	ret
	.cfi_endproc
.LFE39:
	.size	nghttp2_adjust_local_window_size, .-nghttp2_adjust_local_window_size
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_helper.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"delta >= 0"
	.text
	.p2align 4
	.globl	nghttp2_increase_local_window_size
	.type	nghttp2_increase_local_window_size, @function
nghttp2_increase_local_window_size:
.LFB40:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	js	.L31
	movl	$2147483647, %r9d
	movl	(%rdi), %r8d
	subl	%eax, %r9d
	cmpl	%r9d, %r8d
	jg	.L27
	addl	%eax, %r8d
	movl	%r8d, (%rdi)
	movl	(%rdx), %edi
	cmpl	%eax, %edi
	cmovle	%edi, %eax
	subl	%eax, %edi
	movl	%edi, (%rdx)
	addl	%eax, (%rsi)
	subl	%eax, (%rcx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$-524, %eax
	ret
.L31:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.4419(%rip), %rcx
	movl	$225, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE40:
	.size	nghttp2_increase_local_window_size, .-nghttp2_increase_local_window_size
	.p2align 4
	.globl	nghttp2_should_send_window_update
	.type	nghttp2_should_send_window_update, @function
nghttp2_should_send_window_update:
.LFB41:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L32
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%eax, %edi
	xorl	%eax, %eax
	sarl	%edi
	cmpl	%esi, %edi
	setle	%al
.L32:
	ret
	.cfi_endproc
.LFE41:
	.size	nghttp2_should_send_window_update, .-nghttp2_should_send_window_update
	.section	.rodata.str1.1
.LC2:
	.string	"Unknown error code"
.LC3:
	.string	"Out of memory"
.LC4:
	.string	"Out of buffer space"
.LC5:
	.string	"Unsupported SPDY version"
.LC6:
	.string	"Operation would block"
.LC7:
	.string	"Protocol error"
.LC8:
	.string	"Invalid frame octets"
.LC9:
	.string	"EOF"
.LC10:
	.string	"Data transfer deferred"
.LC11:
	.string	"No more Stream ID available"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Stream was already closed or invalid"
	.section	.rodata.str1.1
.LC13:
	.string	"Stream is closing"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"The transmission is not allowed for this stream"
	.section	.rodata.str1.1
.LC15:
	.string	"Stream ID is invalid"
.LC16:
	.string	"Invalid stream state"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Another DATA frame has already been deferred"
	.align 8
.LC18:
	.string	"request HEADERS is not allowed"
	.section	.rodata.str1.1
.LC19:
	.string	"GOAWAY has already been sent"
.LC20:
	.string	"Invalid header block"
.LC21:
	.string	"Invalid state"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"The user callback function failed due to the temporal error"
	.align 8
.LC23:
	.string	"The length of the frame is invalid"
	.align 8
.LC24:
	.string	"Header compression/decompression error"
	.section	.rodata.str1.1
.LC25:
	.string	"Flow control error"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"Insufficient buffer size given to function"
	.align 8
.LC27:
	.string	"Callback was paused by the application"
	.section	.rodata.str1.1
.LC28:
	.string	"Too many inflight SETTINGS"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"Server push is disabled by peer"
	.align 8
.LC30:
	.string	"DATA or HEADERS frame has already been submitted for the stream"
	.align 8
.LC31:
	.string	"The current session is closing"
	.align 8
.LC32:
	.string	"Invalid HTTP header field was received"
	.align 8
.LC33:
	.string	"Violation in HTTP messaging rule"
	.section	.rodata.str1.1
.LC34:
	.string	"Stream was refused"
.LC35:
	.string	"Internal error"
.LC36:
	.string	"Cancel"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"When a local endpoint expects to receive SETTINGS frame, it receives an other type of frame"
	.section	.rodata.str1.1
.LC38:
	.string	"Invalid argument"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"Received bad client magic byte string"
	.align 8
.LC40:
	.string	"Flooding was detected in this HTTP/2 session, and it must be closed"
	.align 8
.LC41:
	.string	"The user callback function failed"
	.align 8
.LC42:
	.string	"SETTINGS frame contained more than the maximum allowed entries"
	.section	.rodata.str1.1
.LC43:
	.string	"Success"
	.text
	.p2align 4
	.globl	nghttp2_strerror
	.type	nghttp2_strerror, @function
nghttp2_strerror:
.LFB42:
	.cfi_startproc
	endbr64
	cmpl	$-500, %edi
	jge	.L36
	cmpl	$-537, %edi
	jl	.L85
	addl	$537, %edi
	cmpl	$36, %edi
	ja	.L40
	leaq	.L42(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L42:
	.long	.L76-.L42
	.long	.L75-.L42
	.long	.L74-.L42
	.long	.L73-.L42
	.long	.L72-.L42
	.long	.L71-.L42
	.long	.L70-.L42
	.long	.L69-.L42
	.long	.L68-.L42
	.long	.L67-.L42
	.long	.L66-.L42
	.long	.L65-.L42
	.long	.L64-.L42
	.long	.L63-.L42
	.long	.L62-.L42
	.long	.L61-.L42
	.long	.L60-.L42
	.long	.L40-.L42
	.long	.L59-.L42
	.long	.L58-.L42
	.long	.L57-.L42
	.long	.L56-.L42
	.long	.L55-.L42
	.long	.L54-.L42
	.long	.L53-.L42
	.long	.L52-.L42
	.long	.L51-.L42
	.long	.L50-.L42
	.long	.L49-.L42
	.long	.L48-.L42
	.long	.L47-.L42
	.long	.L46-.L42
	.long	.L45-.L42
	.long	.L44-.L42
	.long	.L43-.L42
	.long	.L80-.L42
	.long	.L41-.L42
	.text
.L41:
	leaq	.LC38(%rip), %rax
	ret
.L80:
	leaq	.LC4(%rip), %rax
.L35:
	ret
.L40:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	.LC41(%rip), %rax
	cmpl	$-902, %edi
	je	.L35
	cmpl	$-901, %edi
	jl	.L86
	leaq	.LC3(%rip), %rdx
	leaq	.LC2(%rip), %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	testl	%edi, %edi
	leaq	.LC43(%rip), %rdx
	leaq	.LC2(%rip), %rax
	cmove	%rdx, %rax
	ret
.L86:
	leaq	.LC40(%rip), %rax
	cmpl	$-904, %edi
	je	.L35
	cmpl	$-903, %edi
	leaq	.LC39(%rip), %rdx
	leaq	.LC2(%rip), %rax
	cmove	%rdx, %rax
	ret
.L43:
	leaq	.LC5(%rip), %rax
	ret
.L44:
	leaq	.LC6(%rip), %rax
	ret
.L45:
	leaq	.LC7(%rip), %rax
	ret
.L46:
	leaq	.LC8(%rip), %rax
	ret
.L47:
	leaq	.LC9(%rip), %rax
	ret
.L48:
	leaq	.LC10(%rip), %rax
	ret
.L49:
	leaq	.LC11(%rip), %rax
	ret
.L50:
	leaq	.LC12(%rip), %rax
	ret
.L51:
	leaq	.LC13(%rip), %rax
	ret
.L52:
	leaq	.LC14(%rip), %rax
	ret
.L53:
	leaq	.LC15(%rip), %rax
	ret
.L54:
	leaq	.LC16(%rip), %rax
	ret
.L55:
	leaq	.LC17(%rip), %rax
	ret
.L56:
	leaq	.LC18(%rip), %rax
	ret
.L57:
	leaq	.LC19(%rip), %rax
	ret
.L58:
	leaq	.LC20(%rip), %rax
	ret
.L59:
	leaq	.LC21(%rip), %rax
	ret
.L60:
	leaq	.LC22(%rip), %rax
	ret
.L61:
	leaq	.LC23(%rip), %rax
	ret
.L62:
	leaq	.LC24(%rip), %rax
	ret
.L63:
	leaq	.LC25(%rip), %rax
	ret
.L64:
	leaq	.LC26(%rip), %rax
	ret
.L65:
	leaq	.LC27(%rip), %rax
	ret
.L66:
	leaq	.LC28(%rip), %rax
	ret
.L67:
	leaq	.LC29(%rip), %rax
	ret
.L68:
	leaq	.LC30(%rip), %rax
	ret
.L69:
	leaq	.LC31(%rip), %rax
	ret
.L70:
	leaq	.LC32(%rip), %rax
	ret
.L71:
	leaq	.LC33(%rip), %rax
	ret
.L72:
	leaq	.LC34(%rip), %rax
	ret
.L73:
	leaq	.LC35(%rip), %rax
	ret
.L74:
	leaq	.LC36(%rip), %rax
	ret
.L75:
	leaq	.LC37(%rip), %rax
	ret
.L76:
	leaq	.LC42(%rip), %rax
	ret
	.cfi_endproc
.LFE42:
	.size	nghttp2_strerror, .-nghttp2_strerror
	.p2align 4
	.globl	nghttp2_check_header_name
	.type	nghttp2_check_header_name, @function
nghttp2_check_header_name:
.LFB43:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L87
	cmpb	$58, (%rdi)
	je	.L99
.L89:
	addq	%rdi, %rsi
	leaq	VALID_HD_NAME_CHARS(%rip), %rdx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L101:
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	je	.L100
.L90:
	movzbl	(%rdi), %eax
	movl	(%rdx,%rax,4), %eax
	testl	%eax, %eax
	jne	.L101
.L87:
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	$1, %rsi
	je	.L87
	addq	$1, %rdi
	subq	$1, %rsi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE43:
	.size	nghttp2_check_header_name, .-nghttp2_check_header_name
	.p2align 4
	.globl	nghttp2_check_header_value
	.type	nghttp2_check_header_value, @function
nghttp2_check_header_value:
.LFB44:
	.cfi_startproc
	endbr64
	addq	%rdi, %rsi
	cmpq	%rsi, %rdi
	je	.L105
	leaq	VALID_HD_VALUE_CHARS(%rip), %rdx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L110:
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	je	.L105
.L104:
	movzbl	(%rdi), %eax
	movl	(%rdx,%rax,4), %eax
	testl	%eax, %eax
	jne	.L110
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE44:
	.size	nghttp2_check_header_value, .-nghttp2_check_header_value
	.p2align 4
	.globl	nghttp2_check_authority
	.type	nghttp2_check_authority, @function
nghttp2_check_authority:
.LFB45:
	.cfi_startproc
	endbr64
	addq	%rdi, %rsi
	cmpq	%rsi, %rdi
	je	.L114
	leaq	VALID_AUTHORITY_CHARS(%rip), %rdx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	je	.L114
.L113:
	movzbl	(%rdi), %eax
	cmpb	$0, (%rdx,%rax)
	jne	.L117
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE45:
	.size	nghttp2_check_authority, .-nghttp2_check_authority
	.p2align 4
	.globl	nghttp2_cpymem
	.type	nghttp2_cpymem, @function
nghttp2_cpymem:
.LFB46:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	jne	.L127
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	memcpy@PLT
	addq	$8, %rsp
	movq	%rax, %r8
	addq	%rbx, %r8
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE46:
	.size	nghttp2_cpymem, .-nghttp2_cpymem
	.section	.rodata.str1.1
.LC44:
	.string	"unknown"
.LC45:
	.string	"NO_ERROR"
.LC46:
	.string	"INTERNAL_ERROR"
.LC47:
	.string	"FLOW_CONTROL_ERROR"
.LC48:
	.string	"SETTINGS_TIMEOUT"
.LC49:
	.string	"STREAM_CLOSED"
.LC50:
	.string	"FRAME_SIZE_ERROR"
.LC51:
	.string	"REFUSED_STREAM"
.LC52:
	.string	"CANCEL"
.LC53:
	.string	"COMPRESSION_ERROR"
.LC54:
	.string	"CONNECT_ERROR"
.LC55:
	.string	"ENHANCE_YOUR_CALM"
.LC56:
	.string	"INADEQUATE_SECURITY"
.LC57:
	.string	"HTTP_1_1_REQUIRED"
.LC58:
	.string	"PROTOCOL_ERROR"
	.text
	.p2align 4
	.globl	nghttp2_http2_strerror
	.type	nghttp2_http2_strerror, @function
nghttp2_http2_strerror:
.LFB47:
	.cfi_startproc
	endbr64
	cmpl	$13, %edi
	ja	.L129
	leaq	.L131(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L131:
	.long	.L145-.L131
	.long	.L143-.L131
	.long	.L142-.L131
	.long	.L141-.L131
	.long	.L140-.L131
	.long	.L139-.L131
	.long	.L138-.L131
	.long	.L137-.L131
	.long	.L136-.L131
	.long	.L135-.L131
	.long	.L134-.L131
	.long	.L133-.L131
	.long	.L132-.L131
	.long	.L130-.L131
	.text
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	.LC58(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	.LC45(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	.LC57(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	.LC47(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	.LC48(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	.LC49(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	leaq	.LC50(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	.LC46(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	.LC55(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	.LC56(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	.LC51(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	.LC52(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	.LC53(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	.LC54(%rip), %rax
	ret
.L129:
	leaq	.LC44(%rip), %rax
	ret
	.cfi_endproc
.LFE47:
	.size	nghttp2_http2_strerror, .-nghttp2_http2_strerror
	.section	.rodata
	.align 32
	.type	__PRETTY_FUNCTION__.4419, @object
	.size	__PRETTY_FUNCTION__.4419, 35
__PRETTY_FUNCTION__.4419:
	.string	"nghttp2_increase_local_window_size"
	.align 32
	.type	VALID_AUTHORITY_CHARS, @object
	.size	VALID_AUTHORITY_CHARS, 256
VALID_AUTHORITY_CHARS:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001"
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001"
	.string	"\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 32
	.type	VALID_HD_VALUE_CHARS, @object
	.size	VALID_HD_VALUE_CHARS, 1024
VALID_HD_VALUE_CHARS:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.align 32
	.type	VALID_HD_NAME_CHARS, @object
	.size	VALID_HD_NAME_CHARS, 1024
VALID_HD_NAME_CHARS:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	0
	.long	1
	.long	1
	.long	0
	.long	1
	.long	1
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	1
	.long	0
	.long	1
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	DOWNCASE_TBL, @object
	.size	DOWNCASE_TBL, 256
DOWNCASE_TBL:
	.string	""
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\032\033\034\035\036\037 !\"#$%&"
	.ascii	"'()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[\\]^_`a"
	.ascii	"bcdefghijklmnopqrstuvwxyz{|}~\177\200\201\202\203\204\205\206"
	.ascii	"\207\210\211\212\213\214\215\216\217\220\221\222\223\224\225"
	.ascii	"\226\227\230\231\232\233\234\235\236\237\240\241\242\243\244"
	.ascii	"\245\246\247\250\251\252\253\254\255\256\257\260\261\262\263"
	.ascii	"\264\265\266\267\270\271\272\273\274\275\276\277\300\301\302"
	.ascii	"\303\304\305\306\307\310\311\312\313\314\315\316\317\320\321"
	.ascii	"\322\323\324\325\326\327\330\331\332\333\334\335\336\337\340"
	.ascii	"\341\342\343\344\345\346\347\350\351\352\353\354\355\356\357"
	.ascii	"\360\361\362\363\364\365\366\367\370\371\372\373\374\375\376"
	.ascii	"\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
