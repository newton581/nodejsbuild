	.file	"nghttp2_buf.c"
	.text
	.p2align 4
	.type	bufs_alloc_chain.part.0, @function
bufs_alloc_chain.part.0:
.LFB85:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$48, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r14
	movq	%rdi, %rbx
	movq	24(%rdi), %r13
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L5
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	testq	%r13, %r13
	jne	.L9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
.L3:
	movq	8(%rbx), %rax
	addq	$1, 40(%rbx)
	movq	%r12, (%rax)
	movq	56(%rbx), %rax
	movq	%r12, 8(%rbx)
	addq	%rax, %rcx
	addq	%rax, %rdx
	xorl	%eax, %eax
	movq	%rcx, 24(%r12)
	movq	%rdx, 32(%r12)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L4
	movq	40(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rax, 8(%r12)
	movq	24(%r12), %rcx
	movq	32(%r12), %rdx
	addq	%rax, %rsi
	addq	%rax, %rcx
	addq	%rax, %rdx
	subq	%rdi, %rsi
	addq	%r13, %rax
	movq	%rsi, 40(%r12)
	subq	%rdi, %rcx
	subq	%rdi, %rdx
	movq	%rax, 16(%r12)
	jmp	.L3
.L5:
	movl	$-901, %eax
	jmp	.L1
.L4:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L1
	.cfi_endproc
.LFE85:
	.size	bufs_alloc_chain.part.0, .-bufs_alloc_chain.part.0
	.p2align 4
	.globl	nghttp2_buf_init
	.type	nghttp2_buf_init, @function
nghttp2_buf_init:
.LFB54:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE54:
	.size	nghttp2_buf_init, .-nghttp2_buf_init
	.p2align 4
	.globl	nghttp2_buf_init2
	.type	nghttp2_buf_init2, @function
nghttp2_buf_init2:
.LFB55:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	testq	%rsi, %rsi
	jne	.L17
.L11:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	%rdx, %rdi
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L14
	movq	16(%rbx), %rdx
	movq	(%rbx), %rcx
	movq	%rax, (%rbx)
	addq	%rax, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 16(%rbx)
	movq	24(%rbx), %rdx
	addq	%rax, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 24(%rbx)
	movq	32(%rbx), %rdx
	addq	%rax, %rdx
	addq	%r12, %rax
	subq	%rcx, %rdx
	movq	%rax, 8(%rbx)
	movl	%r13d, %eax
	movq	%rdx, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	movl	$-901, %r13d
	jmp	.L11
	.cfi_endproc
.LFE55:
	.size	nghttp2_buf_init2, .-nghttp2_buf_init2
	.p2align 4
	.globl	nghttp2_buf_free
	.type	nghttp2_buf_free, @function
nghttp2_buf_free:
.LFB56:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L18
	movq	%rsi, %rdi
	movq	(%rbx), %rsi
	call	nghttp2_mem_free@PLT
	movq	$0, (%rbx)
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE56:
	.size	nghttp2_buf_free, .-nghttp2_buf_free
	.p2align 4
	.globl	nghttp2_buf_reserve
	.type	nghttp2_buf_reserve, @function
nghttp2_buf_reserve:
.LFB57:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r8
	movq	8(%rdi), %rax
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jb	.L29
.L24:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	addq	%rax, %rax
	movq	%rdx, %rdi
	cmpq	%rsi, %rax
	cmovnb	%rax, %rsi
	movq	%rsi, %r12
	movq	%rsi, %rdx
	movq	%r8, %rsi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L27
	movq	(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	%rax, (%rbx)
	subq	%rcx, %rdx
	addq	%rax, %rdx
	movq	%rdx, 16(%rbx)
	movq	24(%rbx), %rdx
	subq	%rcx, %rdx
	addq	%rax, %rdx
	movq	%rdx, 24(%rbx)
	movq	32(%rbx), %rdx
	subq	%rcx, %rdx
	addq	%rax, %rdx
	addq	%r12, %rax
	movq	%rax, 8(%rbx)
	movl	%r13d, %eax
	movq	%rdx, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	movl	$-901, %r13d
	jmp	.L24
	.cfi_endproc
.LFE57:
	.size	nghttp2_buf_reserve, .-nghttp2_buf_reserve
	.p2align 4
	.globl	nghttp2_buf_reset
	.type	nghttp2_buf_reset, @function
nghttp2_buf_reset:
.LFB58:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rax, 32(%rdi)
	movq	%rax, 24(%rdi)
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE58:
	.size	nghttp2_buf_reset, .-nghttp2_buf_reset
	.p2align 4
	.globl	nghttp2_buf_wrap_init
	.type	nghttp2_buf_wrap_init, @function
nghttp2_buf_wrap_init:
.LFB59:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	movq	%rsi, 24(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%rsi, (%rdi)
	addq	%rdx, %rsi
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE59:
	.size	nghttp2_buf_wrap_init, .-nghttp2_buf_wrap_init
	.p2align 4
	.globl	nghttp2_bufs_init
	.type	nghttp2_bufs_init, @function
nghttp2_bufs_init:
.LFB62:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L36
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$48, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	subq	$8, %rsp
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	testq	%r13, %r13
	jne	.L45
.L34:
	movq	%r12, %xmm0
	movq	%r14, %xmm1
	movq	%r15, 16(%rbx)
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movq	$0, 56(%rbx)
	movups	%xmm0, (%rbx)
	movq	%r13, %xmm0
	movq	$1, 40(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, 48(%rbx)
	movups	%xmm0, 24(%rbx)
.L32:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L35
	movq	24(%r12), %rdx
	movq	8(%r12), %rcx
	movq	%rax, 8(%r12)
	addq	%rax, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 24(%r12)
	movq	32(%r12), %rdx
	addq	%rax, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 32(%r12)
	movq	40(%r12), %rdx
	addq	%rax, %rdx
	addq	%r13, %rax
	subq	%rcx, %rdx
	movq	%rax, 16(%r12)
	movq	%rdx, 40(%r12)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-501, %eax
	ret
.L37:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$-901, %eax
	jmp	.L32
.L35:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L32
	.cfi_endproc
.LFE62:
	.size	nghttp2_bufs_init, .-nghttp2_bufs_init
	.p2align 4
	.globl	nghttp2_bufs_init2
	.type	nghttp2_bufs_init2, @function
nghttp2_bufs_init2:
.LFB63:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L50
	movq	%rsi, %rbx
	movq	%rcx, %r14
	cmpq	%rcx, %rsi
	jb	.L50
	movq	%rdi, %r13
	movl	$48, %esi
	movq	%r8, %rdi
	movq	%r8, %r15
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L51
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	testq	%rbx, %rbx
	jne	.L54
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L48:
	movq	%r12, %xmm0
	movq	%rcx, %xmm2
	movq	-56(%rbp), %rax
	movq	%r15, 16(%r13)
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, %xmm1
	movq	%r14, 56(%r13)
	movups	%xmm0, 0(%r13)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm1
	movq	%rax, %xmm3
	punpcklqdq	%xmm2, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, 24(%r12)
	movq	%rbx, %xmm0
	movq	$1, 40(%r13)
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 48(%r13)
	xorl	%eax, %eax
	movups	%xmm0, 24(%r13)
.L46:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L49
	movq	24(%r12), %rdx
	movq	32(%r12), %rcx
	movq	40(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rax, 8(%r12)
	addq	%rax, %rdx
	addq	%rax, %rcx
	addq	%rax, %rsi
	subq	%rdi, %rdx
	subq	%rdi, %rcx
	addq	%rbx, %rax
	subq	%rdi, %rsi
	movq	%rdx, 24(%r12)
	movq	%rcx, 32(%r12)
	movq	%rsi, 40(%r12)
	movq	%rax, 16(%r12)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$-501, %eax
	jmp	.L46
.L51:
	movl	$-901, %eax
	jmp	.L46
.L49:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L46
	.cfi_endproc
.LFE63:
	.size	nghttp2_bufs_init2, .-nghttp2_bufs_init2
	.p2align 4
	.globl	nghttp2_bufs_init3
	.type	nghttp2_bufs_init3, @function
nghttp2_bufs_init3:
.LFB64:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	movq	%rdx, -56(%rbp)
	sete	%dl
	cmpq	%rax, %rcx
	movq	%r8, -64(%rbp)
	seta	%al
	orb	%al, %dl
	jne	.L59
	movq	%rsi, %rbx
	cmpq	%r8, %rsi
	jb	.L59
	movq	%rdi, %r13
	movl	$48, %esi
	movq	%r9, %rdi
	movq	%rcx, %r15
	movq	%r9, %r14
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L60
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	testq	%rbx, %rbx
	jne	.L63
	xorl	%esi, %esi
	xorl	%edx, %edx
.L57:
	movq	-64(%rbp), %rax
	movq	%r12, %xmm0
	movq	%rsi, %xmm2
	movq	%r14, 16(%r13)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 0(%r13)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	punpcklqdq	%xmm1, %xmm1
	movq	%rax, 56(%r13)
	xorl	%eax, %eax
	paddq	%xmm1, %xmm0
	movups	%xmm0, 24(%r12)
	movq	%rbx, %xmm0
	movq	$1, 40(%r13)
	movhps	-56(%rbp), %xmm0
	movq	%r15, 48(%r13)
	movups	%xmm0, 24(%r13)
.L55:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L58
	movq	24(%r12), %rdx
	movq	32(%r12), %rsi
	movq	40(%r12), %rdi
	movq	8(%r12), %r8
	movq	%rax, 8(%r12)
	addq	%rax, %rdx
	addq	%rax, %rsi
	addq	%rax, %rdi
	subq	%r8, %rdx
	subq	%r8, %rsi
	addq	%rbx, %rax
	subq	%r8, %rdi
	movq	%rdx, 24(%r12)
	movq	%rsi, 32(%r12)
	movq	%rdi, 40(%r12)
	movq	%rax, 16(%r12)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$-501, %eax
	jmp	.L55
.L60:
	movl	$-901, %eax
	jmp	.L55
.L58:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L55
	.cfi_endproc
.LFE64:
	.size	nghttp2_bufs_init3, .-nghttp2_bufs_init3
	.p2align 4
	.globl	nghttp2_bufs_realloc
	.type	nghttp2_bufs_realloc, @function
nghttp2_bufs_realloc:
.LFB65:
	.cfi_startproc
	endbr64
	cmpq	%rsi, 56(%rdi)
	ja	.L70
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$48, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r14
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L71
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	testq	%r13, %r13
	jne	.L79
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L66:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L69
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r14, %r15
	movq	16(%rbx), %rdi
	movq	(%r14), %r14
	movq	8(%r15), %rsi
	movq	%rdi, -56(%rbp)
	call	nghttp2_mem_free@PLT
	movq	$0, 8(%r15)
	movq	-56(%rbp), %rdi
	movq	%r15, %rsi
	call	nghttp2_mem_free@PLT
	testq	%r14, %r14
	jne	.L68
	movq	24(%r12), %rdx
	movq	32(%r12), %rcx
.L69:
	movq	56(%rbx), %rax
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addq	%rax, %rdx
	addq	%rax, %rcx
	movups	%xmm0, (%rbx)
	xorl	%eax, %eax
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%r12)
	movq	%r13, 24(%rbx)
	movq	$1, 40(%rbx)
.L64:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L67
	movq	24(%r12), %rdx
	movq	32(%r12), %rcx
	movq	40(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rax, 8(%r12)
	addq	%rax, %rdx
	addq	%rax, %rcx
	addq	%rax, %rsi
	subq	%rdi, %rdx
	subq	%rdi, %rcx
	addq	%r13, %rax
	subq	%rdi, %rsi
	movq	%rdx, 24(%r12)
	movq	%rcx, 32(%r12)
	movq	%rsi, 40(%r12)
	movq	%rax, 16(%r12)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-501, %eax
	ret
.L71:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$-901, %eax
	jmp	.L64
.L67:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L64
	.cfi_endproc
.LFE65:
	.size	nghttp2_bufs_realloc, .-nghttp2_bufs_realloc
	.p2align 4
	.globl	nghttp2_bufs_free
	.type	nghttp2_bufs_free, @function
nghttp2_bufs_free:
.LFB66:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L92
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rbx, %r12
	movq	16(%r14), %r13
	movq	(%rbx), %rbx
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$0, 8(%r12)
	call	nghttp2_mem_free@PLT
	testq	%rbx, %rbx
	jne	.L83
.L82:
	movq	$0, (%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE66:
	.size	nghttp2_bufs_free, .-nghttp2_bufs_free
	.p2align 4
	.globl	nghttp2_bufs_wrap_init
	.type	nghttp2_bufs_wrap_init, @function
nghttp2_bufs_wrap_init:
.LFB67:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$48, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L97
	movq	%r12, 40(%rax)
	movq	%rax, %xmm0
	movq	%r12, 32(%rax)
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 24(%rax)
	movq	%r12, 8(%rax)
	addq	%r14, %r12
	movq	$0, (%rax)
	movq	%r12, 16(%rax)
	xorl	%eax, %eax
	movq	%r13, 16(%rbx)
	movq	$0, 56(%rbx)
	movq	%r14, 24(%rbx)
	movq	$1, 40(%rbx)
	movq	$1, 32(%rbx)
	movq	$1, 48(%rbx)
	movups	%xmm0, (%rbx)
.L95:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L95
	.cfi_endproc
.LFE67:
	.size	nghttp2_bufs_wrap_init, .-nghttp2_bufs_wrap_init
	.p2align 4
	.globl	nghttp2_bufs_wrap_init2
	.type	nghttp2_bufs_wrap_init2, @function
nghttp2_bufs_wrap_init2:
.LFB68:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L112
	movq	%rsi, %r14
	leaq	(%rdx,%rdx,2), %rsi
	movq	%rcx, %rdi
	movq	%rdx, %r12
	salq	$4, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, -48(%rbp)
	testq	%rax, %rax
	je	.L103
	movq	8(%r14), %rcx
	movq	(%r14), %rdx
	movq	$0, (%rax)
	movq	%rax, %r9
	movq	%rdx, 40(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 24(%rax)
	movq	%rdx, 8(%rax)
	addq	%rcx, %rdx
	movq	%rdx, 16(%rax)
	cmpq	$1, %r12
	jbe	.L104
	movq	%r12, %rdx
	leaq	48(%rax), %rsi
	leaq	16(%r14), %r8
	salq	$4, %rdx
	addq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L105:
	movq	8(%r8), %r11
	movq	(%r8), %rdi
	movq	%r9, %r10
	addq	$16, %r8
	movq	$0, (%rsi)
	movq	%rsi, %r9
	movq	%rdi, 40(%rsi)
	movq	%rdi, 32(%rsi)
	movq	%rdi, 24(%rsi)
	movq	%rdi, 8(%rsi)
	addq	%r11, %rdi
	movq	%rdi, 16(%rsi)
	movq	%rsi, (%r10)
	addq	$48, %rsi
	cmpq	%rdx, %r8
	jne	.L105
.L104:
	movq	%rax, %xmm0
	movq	%r13, 16(%rbx)
	xorl	%eax, %eax
	movq	$0, 56(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	$0, 24(%rbx)
	movq	%r12, 40(%rbx)
	movq	%r12, 32(%rbx)
	movq	%r12, 48(%rbx)
	movups	%xmm0, (%rbx)
.L99:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L113
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	$48, %esi
	movq	%rcx, %rdi
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L103
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 16(%rbx)
	movups	%xmm0, (%rbx)
	movdqa	.LC0(%rip), %xmm0
	movq	$0, 56(%rbx)
	movups	%xmm0, 24(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 40(%rbx)
	jmp	.L99
.L103:
	movl	$-901, %eax
	jmp	.L99
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE68:
	.size	nghttp2_bufs_wrap_init2, .-nghttp2_bufs_wrap_init2
	.p2align 4
	.globl	nghttp2_bufs_wrap_free
	.type	nghttp2_bufs_wrap_free, @function
nghttp2_bufs_wrap_free:
.LFB69:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L114
	movq	16(%rdi), %rdi
	jmp	nghttp2_mem_free@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	ret
	.cfi_endproc
.LFE69:
	.size	nghttp2_bufs_wrap_free, .-nghttp2_bufs_wrap_free
	.p2align 4
	.globl	nghttp2_bufs_seek_last_present
	.type	nghttp2_bufs_seek_last_present, @function
nghttp2_bufs_seek_last_present:
.LFB70:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.L124
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rax, 8(%rdi)
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L122
.L124:
	movq	24(%rax), %rdx
	cmpq	%rdx, 32(%rax)
	jne	.L129
.L122:
	ret
	.cfi_endproc
.LFE70:
	.size	nghttp2_bufs_seek_last_present, .-nghttp2_bufs_seek_last_present
	.p2align 4
	.globl	nghttp2_bufs_len
	.type	nghttp2_bufs_len, @function
nghttp2_bufs_len:
.LFB71:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L130
	.p2align 4,,10
	.p2align 3
.L132:
	movq	32(%rax), %rdx
	subq	24(%rax), %rdx
	movq	(%rax), %rax
	addq	%rdx, %r8
	testq	%rax, %rax
	jne	.L132
.L130:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE71:
	.size	nghttp2_bufs_len, .-nghttp2_bufs_len
	.p2align 4
	.globl	nghttp2_bufs_add
	.type	nghttp2_bufs_add, @function
nghttp2_bufs_add:
.LFB73:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L154
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	.p2align 4,,10
	.p2align 3
.L136:
	movq	32(%r12), %rdi
	movq	16(%r12), %rbx
	subq	%rdi, %rbx
	cmpq	%r13, %rbx
	cmova	%r13, %rbx
	testq	%rbx, %rbx
	jne	.L138
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L139
	movq	%rdx, 8(%r15)
	movq	%rdx, %r12
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r14, %rsi
	movq	%rbx, %rdx
	addq	%rbx, %r14
	call	nghttp2_cpymem@PLT
	movq	%rax, 32(%r12)
	subq	%rbx, %r13
	je	.L143
	movq	8(%r15), %r12
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%rdx, -56(%rbp)
	movq	40(%r15), %rax
	cmpq	%rax, 32(%r15)
	je	.L145
	movq	16(%r15), %rdi
	movl	$48, %esi
	movq	24(%r15), %r12
	movq	%rdi, -64(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L146
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	testq	%r12, %r12
	movq	-56(%rbp), %rdx
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	je	.L141
	movq	-64(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%rdi, -56(%rbp)
	call	nghttp2_mem_realloc@PLT
	movq	-56(%rbp), %rdi
	testq	%rax, %rax
	je	.L142
	movq	40(%rbx), %rsi
	movq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	movq	24(%rbx), %rdx
	movq	32(%rbx), %rcx
	addq	%rax, %rsi
	addq	%rax, %rdx
	addq	%rax, %rcx
	subq	%rdi, %rsi
	addq	%r12, %rax
	movq	%rsi, 40(%rbx)
	subq	%rdi, %rdx
	subq	%rdi, %rcx
	movq	%rax, 16(%rbx)
.L141:
	movq	56(%r15), %rax
	movq	8(%r15), %r12
	addq	$1, 40(%r15)
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	%rbx, (%r12)
	movq	%rbx, %r12
	movq	%rbx, 8(%r15)
	movq	%rdx, 24(%rbx)
	movq	%rcx, 32(%rbx)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%eax, %eax
.L135:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L145:
	.cfi_restore_state
	movl	$-502, %eax
	jmp	.L135
.L146:
	movl	$-901, %eax
	jmp	.L135
.L142:
	movq	%rbx, %rsi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L135
.L154:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE73:
	.size	nghttp2_bufs_add, .-nghttp2_bufs_add
	.p2align 4
	.globl	nghttp2_bufs_addb
	.type	nghttp2_bufs_addb, @function
nghttp2_bufs_addb:
.LFB75:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rax), %rdx
	cmpq	%rdx, 16(%rax)
	je	.L163
.L158:
	leaq	1(%rdx), %rcx
	movq	%rcx, 32(%rax)
	xorl	%eax, %eax
	movb	%r12b, (%rdx)
.L157:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L159
	movq	%rax, 8(%rdi)
	movq	32(%rax), %rdx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L159:
	movq	40(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L161
	call	bufs_alloc_chain.part.0
	testl	%eax, %eax
	jne	.L157
	movq	8(%rbx), %rax
	movq	32(%rax), %rdx
	jmp	.L158
.L161:
	movl	$-502, %eax
	jmp	.L157
	.cfi_endproc
.LFE75:
	.size	nghttp2_bufs_addb, .-nghttp2_bufs_addb
	.p2align 4
	.globl	nghttp2_bufs_addb_hold
	.type	nghttp2_bufs_addb_hold, @function
nghttp2_bufs_addb_hold:
.LFB76:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rax), %rdx
	cmpq	%rdx, 16(%rax)
	je	.L170
.L165:
	movb	%r12b, (%rdx)
	xorl	%eax, %eax
.L164:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L166
	movq	%rax, 8(%rdi)
	movq	32(%rax), %rdx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L166:
	movq	40(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L168
	call	bufs_alloc_chain.part.0
	testl	%eax, %eax
	jne	.L164
	movq	8(%rbx), %rax
	movq	32(%rax), %rdx
	jmp	.L165
.L168:
	movl	$-502, %eax
	jmp	.L164
	.cfi_endproc
.LFE76:
	.size	nghttp2_bufs_addb_hold, .-nghttp2_bufs_addb_hold
	.p2align 4
	.globl	nghttp2_bufs_orb
	.type	nghttp2_bufs_orb, @function
nghttp2_bufs_orb:
.LFB77:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rax), %rdx
	cmpq	%rdx, 16(%rax)
	je	.L177
.L172:
	leaq	1(%rdx), %rcx
	movq	%rcx, 32(%rax)
	xorl	%eax, %eax
	orb	%r12b, (%rdx)
.L171:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L173
	movq	%rax, 8(%rdi)
	movq	32(%rax), %rdx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L173:
	movq	40(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L175
	call	bufs_alloc_chain.part.0
	testl	%eax, %eax
	jne	.L171
	movq	8(%rbx), %rax
	movq	32(%rax), %rdx
	jmp	.L172
.L175:
	movl	$-502, %eax
	jmp	.L171
	.cfi_endproc
.LFE77:
	.size	nghttp2_bufs_orb, .-nghttp2_bufs_orb
	.p2align 4
	.globl	nghttp2_bufs_orb_hold
	.type	nghttp2_bufs_orb_hold, @function
nghttp2_bufs_orb_hold:
.LFB78:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdx), %rax
	cmpq	%rax, 16(%rdx)
	je	.L184
.L179:
	orb	%r12b, (%rax)
	xorl	%eax, %eax
.L178:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L180
	movq	%rax, 8(%rdi)
	movq	32(%rax), %rax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L180:
	movq	40(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L182
	call	bufs_alloc_chain.part.0
	testl	%eax, %eax
	jne	.L178
	movq	8(%rbx), %rax
	movq	32(%rax), %rax
	jmp	.L179
.L182:
	movl	$-502, %eax
	jmp	.L178
	.cfi_endproc
.LFE78:
	.size	nghttp2_bufs_orb_hold, .-nghttp2_bufs_orb_hold
	.p2align 4
	.globl	nghttp2_bufs_remove
	.type	nghttp2_bufs_remove, @function
nghttp2_bufs_remove:
.LFB79:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L190
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L187:
	movq	32(%rax), %rdx
	subq	24(%rax), %rdx
	movq	(%rax), %rax
	addq	%rdx, %r13
	testq	%rax, %rax
	jne	.L187
	testq	%r13, %r13
	je	.L185
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L192
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L188
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L189:
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdx
	subq	%rsi, %rdx
	call	nghttp2_cpymem@PLT
	movq	(%rbx), %rbx
	movq	%rax, %rdi
	testq	%rbx, %rbx
	jne	.L189
.L188:
	movq	%r14, (%r12)
	movq	%r13, %rax
.L185:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
.L192:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	$-901, %rax
	jmp	.L185
	.cfi_endproc
.LFE79:
	.size	nghttp2_bufs_remove, .-nghttp2_bufs_remove
	.p2align 4
	.globl	nghttp2_bufs_remove_copy
	.type	nghttp2_bufs_remove_copy, @function
nghttp2_bufs_remove_copy:
.LFB80:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L207
	movq	%rsi, %rdi
	movq	%rbx, %rdx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L205:
	movq	32(%rdx), %rax
	subq	24(%rdx), %rax
	movq	(%rdx), %rdx
	addq	%rax, %r12
	testq	%rdx, %rdx
	jne	.L205
	.p2align 4,,10
	.p2align 3
.L206:
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdx
	subq	%rsi, %rdx
	call	nghttp2_cpymem@PLT
	movq	(%rbx), %rbx
	movq	%rax, %rdi
	testq	%rbx, %rbx
	jne	.L206
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE80:
	.size	nghttp2_bufs_remove_copy, .-nghttp2_bufs_remove_copy
	.p2align 4
	.globl	nghttp2_bufs_reset
	.type	nghttp2_bufs_reset, @function
nghttp2_bufs_reset:
.LFB81:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %r8
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L212
	movq	56(%r14), %rsi
	movq	8(%rdi), %rax
	movq	%r8, %rcx
	movq	%rax, 40(%rdi)
	addq	%rsi, %rax
	movq	%rax, 24(%rdi)
	movq	%rax, 32(%rdi)
	movq	%rdi, %rax
	subq	$1, %rcx
	jne	.L214
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L215:
	movq	8(%rax), %rdx
	movq	%rdx, 40(%rax)
	addq	%rsi, %rdx
	movq	%rdx, 24(%rax)
	movq	%rdx, 32(%rax)
	subq	$1, %rcx
	je	.L213
.L214:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L215
.L212:
	popq	%rbx
	popq	%r12
	movq	%rdi, 8(%r14)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	(%rax), %rbx
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.L217
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%rbx, %r12
	movq	16(%r14), %r13
	movq	(%rbx), %rbx
	movq	8(%r12), %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$0, 8(%r12)
	call	nghttp2_mem_free@PLT
	testq	%rbx, %rbx
	jne	.L216
	movq	48(%r14), %r8
	movq	(%r14), %rdi
.L217:
	popq	%rbx
	popq	%r12
	movq	%r8, 40(%r14)
	popq	%r13
	movq	%rdi, 8(%r14)
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE81:
	.size	nghttp2_bufs_reset, .-nghttp2_bufs_reset
	.p2align 4
	.globl	nghttp2_bufs_advance
	.type	nghttp2_bufs_advance, @function
nghttp2_bufs_advance:
.LFB82:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L230
	movq	%r12, 8(%rdi)
	xorl	%eax, %eax
.L229:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	40(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L234
	movq	16(%rdi), %r15
	movq	24(%rdi), %r14
	movl	$48, %esi
	movq	%r15, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L235
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	testq	%r14, %r14
	je	.L232
	xorl	%esi, %esi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	nghttp2_mem_realloc@PLT
	testq	%rax, %rax
	je	.L233
	movq	40(%r13), %rcx
	movq	8(%r13), %rsi
	movq	%rax, 8(%r13)
	movq	24(%r13), %r12
	movq	32(%r13), %rdx
	addq	%rax, %rcx
	addq	%rax, %r12
	addq	%rax, %rdx
	subq	%rsi, %rcx
	addq	%r14, %rax
	movq	%rcx, 40(%r13)
	subq	%rsi, %r12
	subq	%rsi, %rdx
	movq	%rax, 16(%r13)
.L232:
	movq	8(%rbx), %rax
	addq	$1, 40(%rbx)
	movq	%r13, (%rax)
	movq	56(%rbx), %rax
	movq	%r13, 8(%rbx)
	addq	%rax, %r12
	addq	%rax, %rdx
	xorl	%eax, %eax
	movq	%r12, 24(%r13)
	movq	%rdx, 32(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L234:
	.cfi_restore_state
	movl	$-502, %eax
	jmp	.L229
.L235:
	movl	$-901, %eax
	jmp	.L229
.L233:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L229
	.cfi_endproc
.LFE82:
	.size	nghttp2_bufs_advance, .-nghttp2_bufs_advance
	.p2align 4
	.globl	nghttp2_bufs_next_present
	.type	nghttp2_bufs_next_present, @function
nghttp2_bufs_next_present:
.LFB83:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	xorl	%r8d, %r8d
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L239
	xorl	%r8d, %r8d
	movq	24(%rax), %rdx
	cmpq	%rdx, 32(%rax)
	setne	%r8b
.L239:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE83:
	.size	nghttp2_bufs_next_present, .-nghttp2_bufs_next_present
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	0
	.quad	1
	.align 16
.LC1:
	.quad	1
	.quad	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
