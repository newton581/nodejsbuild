	.file	"nghttp2_npn.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\002h2"
.LC1:
	.string	"\bhttp/1.1"
	.text
	.p2align 4
	.globl	nghttp2_select_next_protocol
	.type	nghttp2_select_next_protocol, @function
nghttp2_select_next_protocol:
.LFB32:
	.cfi_startproc
	endbr64
	cmpl	$2, %ecx
	jbe	.L12
	xorl	%r8d, %r8d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	movzbl	(%rax), %eax
	leal	1(%r8,%rax), %r8d
	leal	3(%r8), %eax
	cmpl	%eax, %ecx
	jb	.L16
.L6:
	movl	%r8d, %eax
	addq	%rdx, %rax
	cmpw	$26626, (%rax)
	jne	.L3
	cmpb	$50, 2(%rax)
	jne	.L3
	leal	1(%r8), %ecx
	movl	$1, %r9d
	addq	%rcx, %rdx
	movq	%rdx, (%rdi)
	movzbl	(%rax), %eax
	movb	%al, (%rsi)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$8, %ecx
	jbe	.L12
	movabsq	$3328493759640135688, %r9
	xorl	%r8d, %r8d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	movzbl	(%rax), %eax
	leal	1(%r8,%rax), %r8d
	leal	9(%r8), %eax
	cmpl	%eax, %ecx
	jb	.L12
.L10:
	movl	%r8d, %eax
	addq	%rdx, %rax
	cmpq	%r9, (%rax)
	jne	.L7
	cmpb	$49, 8(%rax)
	jne	.L7
	leal	1(%r8), %ecx
	xorl	%r9d, %r9d
	addq	%rcx, %rdx
	movq	%rdx, (%rdi)
	movzbl	(%rax), %eax
	movb	%al, (%rsi)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$-1, %r9d
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE32:
	.size	nghttp2_select_next_protocol, .-nghttp2_select_next_protocol
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
