	.file	"nghttp2_submit.c"
	.text
	.p2align 4
	.globl	nghttp2_submit_trailer
	.type	nghttp2_submit_trailer, @function
nghttp2_submit_trailer:
.LFB34:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L7
	leaq	-68(%rbp), %r15
	movq	%rdx, %r13
	movq	%rcx, %rbx
	movq	%rdi, -88(%rbp)
	leaq	2200(%rdi), %r14
	movq	%r15, %rdi
	movl	%esi, %r12d
	call	nghttp2_priority_spec_default_init@PLT
	leaq	-80(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	nghttp2_nv_array_copy@PLT
	testl	%eax, %eax
	js	.L1
	movq	-80(%rbp), %rax
	movl	$152, %esi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r9
	movl	%r12d, %edx
	movl	$3, %ecx
	movq	%r15, %r8
	movl	$5, %esi
	movq	%r13, %rdi
	movq	$0, 112(%r13)
	pushq	%rbx
	call	nghttp2_frame_headers_init@PLT
	movq	-88(%rbp), %rdi
	movq	%r13, %rsi
	call	nghttp2_session_add_item@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L13
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L14
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	nghttp2_nv_array_del@PLT
	movl	$-901, %eax
.L5:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	%eax, -88(%rbp)
	call	nghttp2_mem_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, -88(%rbp)
	call	nghttp2_frame_headers_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$-501, %eax
	jmp	.L1
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE34:
	.size	nghttp2_submit_trailer, .-nghttp2_submit_trailer
	.p2align 4
	.globl	nghttp2_submit_headers
	.type	nghttp2_submit_headers, @function
nghttp2_submit_headers:
.LFB35:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %edx
	je	.L39
	testl	%edx, %edx
	jle	.L23
.L18:
	movl	%esi, %eax
	andl	$1, %eax
	movb	%al, -88(%rbp)
	testq	%r15, %r15
	je	.L21
	movq	%r15, %rdi
	call	nghttp2_priority_spec_check_default@PLT
	testl	%eax, %eax
	jne	.L21
	movl	(%r15), %eax
	cmpl	$-1, %r12d
	je	.L40
	cmpl	%eax, %r12d
	je	.L23
.L24:
	movq	(%r15), %rax
	orb	$32, -88(%rbp)
	leaq	2200(%r13), %r14
	movq	%rax, -68(%rbp)
	movl	8(%r15), %eax
	leaq	-68(%rbp), %r15
	movq	%r15, %rdi
	movl	%eax, -60(%rbp)
	call	nghttp2_priority_spec_normalize_weight@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	-68(%rbp), %r15
	leaq	2200(%r13), %r14
	movq	%r15, %rdi
	call	nghttp2_priority_spec_default_init@PLT
.L20:
	movq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	nghttp2_nv_array_copy@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	js	.L15
	movq	-80(%rbp), %rax
	movl	$152, %esi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L32
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	nghttp2_outbound_item_init@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r11
	movzbl	-88(%rbp), %esi
	movq	%rdx, 112(%r11)
	orl	$4, %esi
	cmpl	$-1, %r12d
	jne	.L27
	movl	2376(%r13), %r10d
	testl	%r10d, %r10d
	js	.L33
	subq	$8, %rsp
	leal	2(%r10), %eax
	movq	-96(%rbp), %r9
	xorl	%ecx, %ecx
	pushq	%rbx
	movl	%r10d, %edx
	movq	%r11, %rdi
	movsbl	%sil, %esi
	movl	%eax, 2376(%r13)
	movq	%r15, %r8
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	nghttp2_frame_headers_init@PLT
	movq	-88(%rbp), %r11
	movq	%r13, %rdi
	movq	%r11, %rsi
	call	nghttp2_session_add_item@PLT
	popq	%rcx
	movq	-88(%rbp), %r11
	testl	%eax, %eax
	movl	-96(%rbp), %r10d
	movl	%eax, %ebx
	popq	%rsi
	je	.L15
.L29:
	movq	%r11, %rdi
	movq	%r14, %rsi
	movq	%r11, -88(%rbp)
	call	nghttp2_frame_headers_free@PLT
	movq	-88(%rbp), %r11
	movl	%ebx, %r10d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$-901, %r10d
.L26:
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	movl	%r10d, -104(%rbp)
	movq	%r11, -88(%rbp)
	call	nghttp2_nv_array_del@PLT
	movl	-104(%rbp), %r10d
	movq	-88(%rbp), %r11
.L28:
	movq	%r11, %rsi
	movq	%r14, %rdi
	movl	%r10d, -88(%rbp)
	call	nghttp2_mem_free@PLT
	movl	-88(%rbp), %r10d
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	leaq	-40(%rbp), %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	cmpb	$0, 2498(%rdi)
	je	.L18
	movl	$-505, %r10d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$-509, %r10d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	subq	$8, %rsp
	movq	-96(%rbp), %r9
	movl	%r12d, %edx
	movq	%r11, %rdi
	pushq	%rbx
	movsbl	%sil, %esi
	movq	%r15, %r8
	movl	$3, %ecx
	movq	%r11, -88(%rbp)
	call	nghttp2_frame_headers_init@PLT
	movq	-88(%rbp), %r11
	movq	%r13, %rdi
	movq	%r11, %rsi
	call	nghttp2_session_add_item@PLT
	movq	-88(%rbp), %r11
	movl	%eax, %r10d
	popq	%rax
	popq	%rdx
	testl	%r10d, %r10d
	je	.L15
	movl	%r10d, %ebx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	%eax, 2376(%r13)
	jne	.L24
.L23:
	movl	$-501, %r10d
	jmp	.L15
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE35:
	.size	nghttp2_submit_headers, .-nghttp2_submit_headers
	.p2align 4
	.globl	nghttp2_submit_ping
	.type	nghttp2_submit_ping, @function
nghttp2_submit_ping:
.LFB36:
	.cfi_startproc
	endbr64
	andl	$1, %esi
	jmp	nghttp2_session_add_ping@PLT
	.cfi_endproc
.LFE36:
	.size	nghttp2_submit_ping, .-nghttp2_submit_ping
	.p2align 4
	.globl	nghttp2_submit_priority
	.type	nghttp2_submit_priority, @function
nghttp2_submit_priority:
.LFB37:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L47
	testq	%rcx, %rcx
	je	.L47
	movl	%edx, %r15d
	cmpl	%edx, (%rcx)
	je	.L47
	movq	(%rcx), %rax
	leaq	-68(%rbp), %rbx
	leaq	2200(%rdi), %r14
	movq	%rdi, %r13
	movq	%rbx, %rdi
	movq	%rax, -68(%rbp)
	movl	8(%rcx), %eax
	movl	%eax, -60(%rbp)
	call	nghttp2_priority_spec_normalize_weight@PLT
	movl	$152, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L48
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	nghttp2_frame_priority_init@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_session_add_item@PLT
	testl	%eax, %eax
	jne	.L53
.L43:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L54
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -84(%rbp)
	call	nghttp2_frame_priority_free@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	-84(%rbp), %eax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$-501, %eax
	jmp	.L43
.L48:
	movl	$-901, %eax
	jmp	.L43
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE37:
	.size	nghttp2_submit_priority, .-nghttp2_submit_priority
	.p2align 4
	.globl	nghttp2_submit_rst_stream
	.type	nghttp2_submit_rst_stream, @function
nghttp2_submit_rst_stream:
.LFB38:
	.cfi_startproc
	endbr64
	movl	%edx, %esi
	movl	%ecx, %edx
	testl	%esi, %esi
	je	.L56
	jmp	nghttp2_session_add_rst_stream@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$-501, %eax
	ret
	.cfi_endproc
.LFE38:
	.size	nghttp2_submit_rst_stream, .-nghttp2_submit_rst_stream
	.p2align 4
	.globl	nghttp2_submit_goaway
	.type	nghttp2_submit_goaway, @function
nghttp2_submit_goaway:
.LFB39:
	.cfi_startproc
	endbr64
	movl	%edx, %esi
	movl	%ecx, %edx
	movq	%r8, %rcx
	movq	%r9, %r8
	testb	$1, 2499(%rdi)
	je	.L59
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%r9d, %r9d
	jmp	nghttp2_session_add_goaway@PLT
	.cfi_endproc
.LFE39:
	.size	nghttp2_submit_goaway, .-nghttp2_submit_goaway
	.p2align 4
	.globl	nghttp2_submit_shutdown_notice
	.type	nghttp2_submit_shutdown_notice, @function
nghttp2_submit_shutdown_notice:
.LFB40:
	.cfi_startproc
	endbr64
	cmpb	$0, 2498(%rdi)
	je	.L62
	xorl	%eax, %eax
	cmpb	$0, 2499(%rdi)
	je	.L64
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$2, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2147483647, %esi
	jmp	nghttp2_session_add_goaway@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$-519, %eax
	ret
	.cfi_endproc
.LFE40:
	.size	nghttp2_submit_shutdown_notice, .-nghttp2_submit_shutdown_notice
	.p2align 4
	.globl	nghttp2_submit_settings
	.type	nghttp2_submit_settings, @function
nghttp2_submit_settings:
.LFB41:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	nghttp2_session_add_settings@PLT
	.cfi_endproc
.LFE41:
	.size	nghttp2_submit_settings, .-nghttp2_submit_settings
	.p2align 4
	.globl	nghttp2_submit_push_promise
	.type	nghttp2_submit_push_promise, @function
nghttp2_submit_push_promise:
.LFB42:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -72(%rbp)
	movq	%r9, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L69
	movl	%edx, %esi
	movq	%rdi, %rbx
	movl	%edx, %r12d
	movq	%rcx, %r13
	leaq	2200(%rdi), %r15
	call	nghttp2_session_is_my_stream_id@PLT
	testl	%eax, %eax
	jne	.L69
	cmpb	$0, 2498(%rbx)
	je	.L72
	movl	2376(%rbx), %eax
	testl	%eax, %eax
	js	.L73
	movl	$152, %esi
	movq	%r15, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L74
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	movq	%r13, %rsi
	leaq	-64(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rax, 112(%r14)
	call	nghttp2_nv_array_copy@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L79
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r8
	movl	%r12d, %edx
	movl	$4, %esi
	movl	2376(%rbx), %r13d
	movq	%r14, %rdi
	leal	2(%r13), %eax
	movl	%r13d, %ecx
	movl	%eax, 2376(%rbx)
	call	nghttp2_frame_push_promise_init@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	nghttp2_session_add_item@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L80
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%ebx, %r13d
	call	nghttp2_frame_push_promise_free@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$-501, %r13d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$-509, %r13d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$-505, %r13d
	jmp	.L66
.L74:
	movl	$-901, %r13d
	jmp	.L66
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE42:
	.size	nghttp2_submit_push_promise, .-nghttp2_submit_push_promise
	.p2align 4
	.globl	nghttp2_submit_window_update
	.type	nghttp2_submit_window_update, @function
nghttp2_submit_window_update:
.LFB43:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%ecx, -36(%rbp)
	testl	%ecx, %ecx
	je	.L87
	movq	%rdi, %r12
	movl	%edx, %r13d
	testl	%edx, %edx
	jne	.L85
	leaq	2416(%rdi), %rdx
	leaq	2408(%rdi), %rsi
	leaq	-36(%rbp), %rcx
	leaq	2420(%rdi), %rdi
	call	nghttp2_adjust_local_window_size@PLT
	testl	%eax, %eax
	jne	.L82
	movl	-36(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L98
.L87:
	xorl	%eax, %eax
.L82:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movl	%edx, %esi
	call	nghttp2_session_get_stream@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L87
	leaq	-36(%rbp), %rcx
	leaq	208(%rax), %rdx
	leaq	200(%rax), %rsi
	leaq	212(%rax), %rdi
	call	nghttp2_adjust_local_window_size@PLT
	testl	%eax, %eax
	jne	.L82
	movl	-36(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L87
	subl	%ecx, 204(%rbx)
	movl	204(%rbx), %edx
	testl	%edx, %edx
	cmovns	204(%rbx), %eax
	movl	%eax, 204(%rbx)
.L89:
	movl	%r13d, %edx
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	nghttp2_session_add_window_update@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	subl	%ecx, 2412(%r12)
	movl	2412(%r12), %esi
	movl	%r13d, %eax
	testl	%esi, %esi
	cmovns	2412(%r12), %eax
	movl	%eax, 2412(%r12)
	jmp	.L89
	.cfi_endproc
.LFE43:
	.size	nghttp2_submit_window_update, .-nghttp2_submit_window_update
	.p2align 4
	.globl	nghttp2_session_set_local_window_size
	.type	nghttp2_session_set_local_window_size, @function
nghttp2_session_set_local_window_size:
.LFB44:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	js	.L109
	movq	%rdi, %r12
	movl	%edx, %r13d
	movl	%ecx, %ebx
	testl	%edx, %edx
	jne	.L101
	subl	2420(%rdi), %ebx
	movl	%ebx, -44(%rbp)
	je	.L105
	leaq	2416(%rdi), %rdx
	leaq	2408(%rdi), %rsi
	leaq	-44(%rbp), %rcx
	leaq	2420(%rdi), %rdi
	js	.L117
	call	nghttp2_increase_local_window_size@PLT
	testl	%eax, %eax
	jne	.L99
	movl	-44(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L104
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	nghttp2_session_add_window_update@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L101:
	movl	%edx, %esi
	call	nghttp2_session_get_stream@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L105
	subl	212(%rax), %ebx
	movl	%ebx, -44(%rbp)
	jne	.L118
.L105:
	xorl	%eax, %eax
.L99:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L119
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	leaq	208(%rax), %rdx
	leaq	200(%rax), %rsi
	leaq	212(%rax), %rdi
	leaq	-44(%rbp), %rcx
	js	.L117
	call	nghttp2_increase_local_window_size@PLT
	testl	%eax, %eax
	jne	.L99
	movl	-44(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L107
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	nghttp2_session_add_window_update@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	nghttp2_session_update_recv_connection_window_size@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L117:
	call	nghttp2_adjust_local_window_size@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_update_recv_stream_window_size@PLT
	jmp	.L99
.L109:
	movl	$-501, %eax
	jmp	.L99
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE44:
	.size	nghttp2_session_set_local_window_size, .-nghttp2_session_set_local_window_size
	.p2align 4
	.globl	nghttp2_submit_altsvc
	.type	nghttp2_submit_altsvc, @function
nghttp2_submit_altsvc:
.LFB45:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpb	$0, 2498(%rdi)
	movl	%edx, -60(%rbp)
	je	.L130
	movq	16(%rbp), %rax
	movq	%r8, %r12
	leaq	2(%r8,%rax), %rsi
	cmpq	$16384, %rsi
	ja	.L133
	movq	%rdi, %r13
	movq	%r9, %r14
	leaq	2200(%rdi), %rbx
	testl	%edx, %edx
	je	.L145
	testq	%r8, %r8
	jne	.L133
	movq	%rbx, %rdi
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L128
	movq	%rax, -56(%rbp)
.L129:
	leaq	1(%rax), %r15
	cmpq	$0, 16(%rbp)
	movb	$0, (%rax)
	movq	%r15, %rax
	jne	.L146
.L125:
	movb	$0, (%rax)
	movl	$152, %esi
	movq	%rbx, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L147
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	leaq	64(%r14), %rax
	movl	-60(%rbp), %esi
	movq	16(%rbp), %r9
	movb	$1, 96(%r14)
	movq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r15, %r8
	movq	%rax, 16(%r14)
	movq	%r12, %rcx
	call	nghttp2_frame_altsvc_init@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	nghttp2_session_add_item@PLT
	testl	%eax, %eax
	jne	.L148
.L120:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	testq	%r8, %r8
	je	.L133
	movq	%rbx, %rdi
	movq	%rcx, -72(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	-72(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, -56(%rbp)
	je	.L128
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	%rcx, %rsi
	call	nghttp2_cpymem@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L146:
	movq	16(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	nghttp2_cpymem@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, -56(%rbp)
	call	nghttp2_frame_altsvc_free@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	nghttp2_mem_free@PLT
	movl	-56(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-901, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movl	$-501, %eax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$-519, %eax
	jmp	.L120
.L147:
	movq	-56(%rbp), %rdi
	call	free@PLT
	movl	$-901, %eax
	jmp	.L120
	.cfi_endproc
.LFE45:
	.size	nghttp2_submit_altsvc, .-nghttp2_submit_altsvc
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_submit.c"
	.align 8
.LC1:
	.string	"(size_t)(p - (uint8_t *)ov_copy) == nov * sizeof(nghttp2_origin_entry) + len + nov"
	.text
	.p2align 4
	.globl	nghttp2_submit_origin
	.type	nghttp2_submit_origin, @function
nghttp2_submit_origin:
.LFB46:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	2200(%rdi), %rbx
	subq	$40, %rsp
	cmpb	$0, 2498(%rdi)
	movq	%rdi, -64(%rbp)
	movq	%rbx, -56(%rbp)
	je	.L157
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L158
	movq	%rdx, %r15
	leaq	-1(%rcx), %rdx
	cmpq	$4, %rdx
	jbe	.L159
	movq	%rdx, %rcx
	movq	%r15, %rax
	pxor	%xmm1, %xmm1
	shrq	%rcx
	salq	$5, %rcx
	addq	%r15, %rcx
	.p2align 4,,10
	.p2align 3
.L153:
	movdqu	8(%rax), %xmm0
	movdqu	24(%rax), %xmm2
	addq	$32, %rax
	punpcklqdq	%xmm2, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rcx, %rax
	jne	.L153
	movdqa	%xmm1, %xmm0
	andq	$-2, %rdx
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rax
.L152:
	movq	%rdx, %rcx
	salq	$4, %rcx
	addq	8(%r15,%rcx), %rax
	leaq	1(%rdx), %rcx
	cmpq	%rcx, %r12
	jbe	.L154
	salq	$4, %rcx
	addq	8(%r15,%rcx), %rax
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r12
	jbe	.L154
	salq	$4, %rcx
	addq	8(%r15,%rcx), %rax
	leaq	3(%rdx), %rcx
	cmpq	%rcx, %r12
	jbe	.L154
	salq	$4, %rcx
	addq	$4, %rdx
	addq	8(%r15,%rcx), %rax
	cmpq	%rdx, %r12
	jbe	.L154
	salq	$4, %rdx
	addq	8(%r15,%rdx), %rax
.L154:
	leaq	(%rax,%r12,2), %rdx
	cmpq	$16384, %rdx
	ja	.L160
	movq	%r12, %rbx
	movq	-56(%rbp), %rdi
	salq	$4, %rbx
	leaq	(%rbx,%r12), %rcx
	addq	%rcx, %rax
	movq	%rax, %rsi
	movq	%rax, -72(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L161
	leaq	(%rax,%rbx), %rdi
	movq	%rax, %r13
	addq	$8, %r15
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%r15), %rax
	movq	%rdi, 0(%r13)
	addq	$16, %r13
	addq	$16, %r15
	movq	-24(%r15), %rsi
	movq	%rax, -8(%r13)
	movq	-16(%r15), %rdx
	call	nghttp2_cpymem@PLT
	movb	$0, (%rax)
	leaq	1(%rax), %rdi
	cmpq	%rbx, %r13
	jne	.L155
	subq	%r14, %rdi
	cmpq	%rdi, -72(%rbp)
	jne	.L168
.L151:
	movq	-56(%rbp), %rdi
	movl	$152, %esi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L169
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	leaq	64(%rbx), %rax
	movb	$1, 96(%rbx)
	movq	%r14, %rsi
	movq	%rax, 16(%rbx)
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	nghttp2_frame_origin_init@PLT
	movq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	call	nghttp2_session_add_item@PLT
	testl	%eax, %eax
	jne	.L170
.L149:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L170:
	movq	-56(%rbp), %r14
	movq	%rbx, %rdi
	movl	%eax, -64(%rbp)
	movq	%r14, %rsi
	call	nghttp2_frame_origin_free@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	-64(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L152
.L160:
	movl	$-501, %eax
	jmp	.L149
.L157:
	movl	$-519, %eax
	jmp	.L149
.L169:
	movq	%r14, %rdi
	call	free@PLT
	movl	$-901, %eax
	jmp	.L149
.L161:
	movl	$-901, %eax
	jmp	.L149
.L168:
	leaq	__PRETTY_FUNCTION__.5309(%rip), %rcx
	movl	$628, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE46:
	.size	nghttp2_submit_origin, .-nghttp2_submit_origin
	.p2align 4
	.globl	nghttp2_submit_request
	.type	nghttp2_submit_request, @function
nghttp2_submit_request:
.LFB48:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L185
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	%rdx, %rsi
	movq	%rcx, %r13
	movq	%r8, %rbx
	testq	%r15, %r15
	je	.L173
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	call	nghttp2_priority_spec_check_default@PLT
	movq	-88(%rbp), %rsi
	testl	%eax, %eax
	je	.L206
.L173:
	movb	$1, -88(%rbp)
	testq	%rbx, %rbx
	je	.L175
	cmpq	$0, 8(%rbx)
	sete	-88(%rbp)
.L175:
	leaq	-68(%rbp), %r15
	movq	%rsi, -104(%rbp)
	leaq	2200(%r12), %r14
	movq	%r15, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	movq	-104(%rbp), %rsi
.L180:
	leaq	-80(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	nghttp2_nv_array_copy@PLT
	movl	%eax, %r10d
	testl	%eax, %eax
	js	.L171
	movq	-80(%rbp), %rax
	movl	$152, %esi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L187
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	nghttp2_outbound_item_init@PLT
	testq	%rbx, %rbx
	movq	-112(%rbp), %r11
	je	.L178
	cmpq	$0, 8(%rbx)
	je	.L178
	movdqu	(%rbx), %xmm0
	movups	%xmm0, 96(%r11)
.L178:
	movq	-96(%rbp), %rax
	movl	2376(%r12), %r10d
	movq	%rax, 112(%r11)
	testl	%r10d, %r10d
	js	.L188
	subq	$8, %rsp
	leal	2(%r10), %eax
	movzbl	-88(%rbp), %esi
	movq	-104(%rbp), %r9
	movl	%eax, 2376(%r12)
	movl	%r10d, %edx
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	pushq	%r13
	orl	$4, %esi
	movq	%r15, %r8
	movsbl	%sil, %esi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	nghttp2_frame_headers_init@PLT
	movq	-88(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	nghttp2_session_add_item@PLT
	movq	-88(%rbp), %r11
	movl	-96(%rbp), %r10d
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	jne	.L207
.L171:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	leaq	-40(%rbp), %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movl	(%r15), %eax
	cmpl	%eax, 2376(%r12)
	je	.L209
	movb	$33, -88(%rbp)
	testq	%rbx, %rbx
	je	.L182
	cmpq	$0, 8(%rbx)
	sete	%al
	orl	$32, %eax
	movb	%al, -88(%rbp)
.L182:
	movq	(%r15), %rax
	movq	%rsi, -104(%rbp)
	leaq	2200(%r12), %r14
	movq	%rax, -68(%rbp)
	movl	8(%r15), %eax
	leaq	-68(%rbp), %r15
	movq	%r15, %rdi
	movl	%eax, -60(%rbp)
	call	nghttp2_priority_spec_normalize_weight@PLT
	movq	-104(%rbp), %rsi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$-901, %r10d
.L177:
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movl	%r10d, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	nghttp2_nv_array_del@PLT
	movl	-96(%rbp), %r10d
	movq	-88(%rbp), %r11
.L179:
	movq	%r11, %rsi
	movq	%r14, %rdi
	movl	%r10d, -88(%rbp)
	call	nghttp2_mem_free@PLT
	movl	-88(%rbp), %r10d
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r11, %rdi
	movq	%r14, %rsi
	call	nghttp2_frame_headers_free@PLT
	movq	-88(%rbp), %r11
	movl	%ebx, %r10d
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$-509, %r10d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$-505, %r10d
	jmp	.L171
.L209:
	movl	$-501, %r10d
	jmp	.L171
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE48:
	.size	nghttp2_submit_request, .-nghttp2_submit_request
	.p2align 4
	.globl	nghttp2_submit_response
	.type	nghttp2_submit_response, @function
nghttp2_submit_response:
.LFB50:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -100(%rbp)
	movq	%rdx, %rsi
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	testl	%eax, %eax
	jle	.L219
	cmpb	$0, 2498(%rdi)
	movq	%rdi, %r12
	je	.L220
	movq	%r8, %rbx
	movl	$5, %r13d
	testq	%r8, %r8
	je	.L212
	xorl	%r13d, %r13d
	cmpq	$0, 8(%r8)
	sete	%r13b
	addl	$4, %r13d
.L212:
	leaq	-68(%rbp), %r15
	movq	%rsi, -96(%rbp)
	leaq	2200(%r12), %r14
	movq	%r15, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rcx
	leaq	-80(%rbp), %rdi
	call	nghttp2_nv_array_copy@PLT
	testl	%eax, %eax
	js	.L210
	movq	-80(%rbp), %rax
	movl	$152, %esi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L214
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	nghttp2_outbound_item_init@PLT
	testq	%rbx, %rbx
	movq	-112(%rbp), %r10
	je	.L215
	cmpq	$0, 8(%rbx)
	je	.L215
	movdqu	(%rbx), %xmm0
	movups	%xmm0, 96(%r10)
.L215:
	subq	$8, %rsp
	movl	-100(%rbp), %edx
	movq	-96(%rbp), %r9
	movl	$3, %ecx
	movq	$0, 112(%r10)
	movq	%r10, %rdi
	movq	%r15, %r8
	movl	%r13d, %esi
	pushq	-88(%rbp)
	movq	%r10, -88(%rbp)
	call	nghttp2_frame_headers_init@PLT
	movq	-88(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	nghttp2_session_add_item@PLT
	popq	%rdx
	movq	-88(%rbp), %r10
	testl	%eax, %eax
	popq	%rcx
	jne	.L217
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L228
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -88(%rbp)
	call	nghttp2_nv_array_del@PLT
	movq	-88(%rbp), %r10
	movl	$-901, %eax
.L216:
	movq	%r10, %rsi
	movq	%r14, %rdi
	movl	%eax, -88(%rbp)
	call	nghttp2_mem_free@PLT
	movl	-88(%rbp), %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r10, %rdi
	movq	%r14, %rsi
	movl	%eax, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	nghttp2_frame_headers_free@PLT
	movq	-88(%rbp), %r10
	movl	-96(%rbp), %eax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$-505, %eax
	jmp	.L210
.L219:
	movl	$-501, %eax
	jmp	.L210
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE50:
	.size	nghttp2_submit_response, .-nghttp2_submit_response
	.p2align 4
	.globl	nghttp2_submit_data
	.type	nghttp2_submit_data, @function
nghttp2_submit_data:
.LFB51:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%al, -52(%rbp)
	testl	%edx, %edx
	je	.L231
	leaq	2200(%rdi), %r15
	movq	%rdi, %r13
	movl	%edx, %ebx
	movq	%rcx, %r14
	movl	$152, %esi
	movq	%r15, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L232
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movzbl	-52(%rbp), %eax
	movdqu	(%r14), %xmm0
	xorl	%esi, %esi
	movb	$0, 113(%r12)
	movq	%r12, %rdi
	movl	%ebx, %edx
	movb	%al, 112(%r12)
	movups	%xmm0, 96(%r12)
	call	nghttp2_frame_data_init@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_session_add_item@PLT
	testl	%eax, %eax
	jne	.L237
.L229:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_data_free@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movl	$-501, %eax
	jmp	.L229
.L232:
	movl	$-901, %eax
	jmp	.L229
	.cfi_endproc
.LFE51:
	.size	nghttp2_submit_data, .-nghttp2_submit_data
	.p2align 4
	.globl	nghttp2_pack_settings_payload
	.type	nghttp2_pack_settings_payload, @function
nghttp2_pack_settings_payload:
.LFB52:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rcx, %rsi
	call	nghttp2_iv_check@PLT
	testl	%eax, %eax
	je	.L240
	leaq	(%r12,%r12,2), %rax
	addq	%rax, %rax
	cmpq	%rbx, %rax
	ja	.L241
	popq	%rbx
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	nghttp2_frame_pack_settings_payload@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movq	$-525, %rax
.L238:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	$-501, %rax
	jmp	.L238
	.cfi_endproc
.LFE52:
	.size	nghttp2_pack_settings_payload, .-nghttp2_pack_settings_payload
	.p2align 4
	.globl	nghttp2_submit_extension
	.type	nghttp2_submit_extension, @function
nghttp2_submit_extension:
.LFB53:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	movl	%ecx, -56(%rbp)
	cmpb	$9, %sil
	jbe	.L245
	cmpq	$0, 2160(%rdi)
	movq	%rdi, %r13
	je	.L246
	leaq	2200(%rdi), %r15
	movl	%esi, %ebx
	movl	$152, %esi
	movq	%r8, %r14
	movq	%r15, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L247
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movzbl	-52(%rbp), %edx
	movl	-56(%rbp), %ecx
	movzbl	%bl, %esi
	movq	%r12, %rdi
	movq	%r14, %r8
	call	nghttp2_frame_extension_init@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_session_add_item@PLT
	testl	%eax, %eax
	jne	.L252
.L243:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_extension_free@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movl	$-501, %eax
	jmp	.L243
.L246:
	movl	$-519, %eax
	jmp	.L243
.L247:
	movl	$-901, %eax
	jmp	.L243
	.cfi_endproc
.LFE53:
	.size	nghttp2_submit_extension, .-nghttp2_submit_extension
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.5309, @object
	.size	__PRETTY_FUNCTION__.5309, 22
__PRETTY_FUNCTION__.5309:
	.string	"nghttp2_submit_origin"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
