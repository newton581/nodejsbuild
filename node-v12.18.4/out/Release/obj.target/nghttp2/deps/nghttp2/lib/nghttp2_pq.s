	.file	"nghttp2_pq.c"
	.text
	.p2align 4
	.type	bubble_down, @function
bubble_down:
.LFB60:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	movq	0(%r13), %rdx
	leaq	(%rdx,%r14,8), %rsi
	leaq	(%rdx,%r12,8), %rdx
	movq	(%rsi), %rax
	movq	(%rdx), %rcx
	movq	%rcx, (%rsi)
	movq	%r14, (%rcx)
	movq	%r12, %r14
	movq	%rax, (%rdx)
	movq	%r12, (%rax)
.L6:
	leaq	(%r14,%r14), %r15
	movq	%r14, %r12
	leaq	1(%r15), %rbx
	addq	$3, %r15
.L4:
	cmpq	%rbx, 16(%r13)
	jbe	.L2
	movq	0(%r13), %rax
	movq	(%rax,%r12,8), %rsi
	movq	(%rax,%rbx,8), %rdi
	call	*32(%r13)
	testl	%eax, %eax
	cmovne	%rbx, %r12
	addq	$1, %rbx
	cmpq	%r15, %rbx
	jne	.L4
.L2:
	cmpq	%r12, %r14
	jne	.L13
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE60:
	.size	bubble_down, .-bubble_down
	.p2align 4
	.globl	nghttp2_pq_init
	.type	nghttp2_pq_init, @function
nghttp2_pq_init:
.LFB54:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rdx, 8(%rdi)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	movq	%rsi, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE54:
	.size	nghttp2_pq_init, .-nghttp2_pq_init
	.p2align 4
	.globl	nghttp2_pq_free
	.type	nghttp2_pq_free, @function
nghttp2_pq_free:
.LFB55:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rbx), %rsi
	movq	8(%rdi), %rdi
	call	nghttp2_mem_free@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE55:
	.size	nghttp2_pq_free, .-nghttp2_pq_free
	.p2align 4
	.globl	nghttp2_pq_push
	.type	nghttp2_pq_push, @function
nghttp2_pq_push:
.LFB58:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rax
	movq	16(%rdi), %r12
	movq	(%rdi), %rsi
	cmpq	%r12, %rax
	ja	.L18
	leaq	(%rax,%rax), %r12
	movl	$4, %eax
	movq	8(%rdi), %rdi
	movq	%r8, -56(%rbp)
	cmpq	$4, %r12
	cmovb	%rax, %r12
	leaq	0(,%r12,8), %rdx
	call	nghttp2_mem_realloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L22
	movq	%rax, (%rbx)
	movq	-56(%rbp), %r8
	movq	%r12, 24(%rbx)
	movq	16(%rbx), %r12
.L18:
	leaq	1(%r12), %rax
	movq	%r8, (%rsi,%r12,8)
	movq	%r12, (%r8)
	movq	%rax, 16(%rbx)
	testq	%r12, %r12
	jne	.L20
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%rbx), %rsi
	addq	%rsi, %r13
	addq	%rsi, %r14
	movq	0(%r13), %rax
	movq	(%r14), %rdx
	movq	%rdx, 0(%r13)
	movq	%r15, (%rdx)
	movq	%rax, (%r14)
	movq	%r12, (%rax)
	testq	%r15, %r15
	je	.L21
	movq	0(%r13), %r8
	movq	%r15, %r12
.L20:
	leaq	-1(%r12), %r15
	leaq	0(,%r12,8), %r14
	movq	%r8, %rdi
	shrq	%r15
	movq	(%rsi,%r15,8), %rsi
	leaq	0(,%r15,8), %r13
	call	*32(%rbx)
	testl	%eax, %eax
	jne	.L31
.L21:
	xorl	%eax, %eax
.L17:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L17
	.cfi_endproc
.LFE58:
	.size	nghttp2_pq_push, .-nghttp2_pq_push
	.p2align 4
	.globl	nghttp2_pq_top
	.type	nghttp2_pq_top, @function
nghttp2_pq_top:
.LFB59:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 16(%rdi)
	je	.L32
	movq	(%rdi), %rax
	movq	(%rax), %rax
.L32:
	ret
	.cfi_endproc
.LFE59:
	.size	nghttp2_pq_top, .-nghttp2_pq_top
	.p2align 4
	.globl	nghttp2_pq_pop
	.type	nghttp2_pq_pop, @function
nghttp2_pq_pop:
.LFB61:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	jne	.L37
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rdi), %rcx
	xorl	%esi, %esi
	movq	-8(%rcx,%rax,8), %rdx
	subq	$1, %rax
	movq	%rdx, (%rcx)
	movq	$0, (%rdx)
	movq	%rax, 16(%rdi)
	jmp	bubble_down
	.cfi_endproc
.LFE61:
	.size	nghttp2_pq_pop, .-nghttp2_pq_pop
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_pq.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"pq->q[item->index] == item"
	.text
	.p2align 4
	.globl	nghttp2_pq_remove
	.type	nghttp2_pq_remove, @function
nghttp2_pq_remove:
.LFB62:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdx
	movq	(%rsi), %rax
	leaq	(%rdx,%rax,8), %rsi
	cmpq	%rbx, (%rsi)
	jne	.L56
	movq	16(%rdi), %rcx
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L57
	leaq	-1(%rcx), %rdi
	cmpq	%rdi, %rax
	je	.L58
	movq	-8(%rdx,%rcx,8), %rcx
	movq	%rcx, (%rsi)
	movq	%rax, (%rcx)
	movq	(%rbx), %rax
	movq	%rdi, 16(%r12)
	movq	%rbx, %rdi
	movq	(%rdx,%rax,8), %rsi
	call	*32(%r12)
	testl	%eax, %eax
	jne	.L59
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L38
	movq	(%r12), %rax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%r12), %rax
	addq	%rax, %r14
	addq	%rax, %r13
	movq	(%r14), %rdx
	movq	0(%r13), %rcx
	movq	%rcx, (%r14)
	movq	%rbx, (%rcx)
	movq	%rdx, 0(%r13)
	movq	%r15, (%rdx)
	testq	%rbx, %rbx
	je	.L38
.L46:
	movq	%rbx, %r15
	leaq	-1(%rbx), %rbx
	shrq	%rbx
	movq	(%rax,%r15,8), %rdi
	leaq	0(,%r15,8), %r13
	movq	(%rax,%rbx,8), %rsi
	leaq	0(,%rbx,8), %r14
	call	*32(%r12)
	testl	%eax, %eax
	jne	.L60
.L38:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	(%rbx), %rsi
.L55:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	bubble_down
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L38
	movq	-8(%rdx,%rcx,8), %rax
	subq	$1, %rcx
	xorl	%esi, %esi
	movq	%rax, (%rdx)
	movq	$0, (%rax)
	movq	%rcx, 16(%rdi)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.4568(%rip), %rcx
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE62:
	.size	nghttp2_pq_remove, .-nghttp2_pq_remove
	.p2align 4
	.globl	nghttp2_pq_empty
	.type	nghttp2_pq_empty, @function
nghttp2_pq_empty:
.LFB63:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 16(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE63:
	.size	nghttp2_pq_empty, .-nghttp2_pq_empty
	.p2align 4
	.globl	nghttp2_pq_size
	.type	nghttp2_pq_size, @function
nghttp2_pq_size:
.LFB64:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE64:
	.size	nghttp2_pq_size, .-nghttp2_pq_size
	.p2align 4
	.globl	nghttp2_pq_update
	.type	nghttp2_pq_update, @function
nghttp2_pq_update:
.LFB65:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 16(%rdi)
	movq	%rsi, -56(%rbp)
	je	.L63
	movq	%rdi, %r14
	movq	%rdx, %r15
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%r14), %rax
	movq	%r15, %rsi
	movq	(%rax,%rbx,8), %rdi
	movq	-56(%rbp), %rax
	addq	$1, %rbx
	call	*%rax
	movq	16(%r14), %r12
	orl	%eax, %r13d
	cmpq	%rbx, %r12
	ja	.L65
	testl	%r13d, %r13d
	jne	.L76
.L63:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	subq	$1, %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	bubble_down
.L76:
	testq	%r12, %r12
	jne	.L66
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE65:
	.size	nghttp2_pq_update, .-nghttp2_pq_update
	.p2align 4
	.globl	nghttp2_pq_each
	.type	nghttp2_pq_each, @function
nghttp2_pq_each:
.LFB66:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L84
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$1, %rbx
	cmpq	%rbx, 16(%r12)
	jbe	.L87
.L80:
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	(%rax,%rbx,8), %rdi
	call	*%r13
	testl	%eax, %eax
	je	.L88
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE66:
	.size	nghttp2_pq_each, .-nghttp2_pq_each
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.4568, @object
	.size	__PRETTY_FUNCTION__.4568, 18
__PRETTY_FUNCTION__.4568:
	.string	"nghttp2_pq_remove"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
