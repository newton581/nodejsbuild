	.file	"nghttp2_mem.c"
	.text
	.p2align 4
	.type	default_realloc, @function
default_realloc:
.LFB23:
	.cfi_startproc
	endbr64
	jmp	realloc@PLT
	.cfi_endproc
.LFE23:
	.size	default_realloc, .-default_realloc
	.p2align 4
	.type	default_calloc, @function
default_calloc:
.LFB22:
	.cfi_startproc
	endbr64
	jmp	calloc@PLT
	.cfi_endproc
.LFE22:
	.size	default_calloc, .-default_calloc
	.p2align 4
	.type	default_free, @function
default_free:
.LFB21:
	.cfi_startproc
	endbr64
	jmp	free@PLT
	.cfi_endproc
.LFE21:
	.size	default_free, .-default_free
	.p2align 4
	.type	default_malloc, @function
default_malloc:
.LFB20:
	.cfi_startproc
	endbr64
	jmp	malloc@PLT
	.cfi_endproc
.LFE20:
	.size	default_malloc, .-default_malloc
	.p2align 4
	.globl	nghttp2_mem_default
	.type	nghttp2_mem_default, @function
nghttp2_mem_default:
.LFB24:
	.cfi_startproc
	endbr64
	leaq	mem_default(%rip), %rax
	ret
	.cfi_endproc
.LFE24:
	.size	nghttp2_mem_default, .-nghttp2_mem_default
	.p2align 4
	.globl	nghttp2_mem_malloc
	.type	nghttp2_mem_malloc, @function
nghttp2_mem_malloc:
.LFB25:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	8(%rdi), %rax
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE25:
	.size	nghttp2_mem_malloc, .-nghttp2_mem_malloc
	.p2align 4
	.globl	nghttp2_mem_free
	.type	nghttp2_mem_free, @function
nghttp2_mem_free:
.LFB26:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	16(%rdi), %rax
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE26:
	.size	nghttp2_mem_free, .-nghttp2_mem_free
	.p2align 4
	.globl	nghttp2_mem_free2
	.type	nghttp2_mem_free2, @function
nghttp2_mem_free2:
.LFB27:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	jmp	*%rax
	.cfi_endproc
.LFE27:
	.size	nghttp2_mem_free2, .-nghttp2_mem_free2
	.p2align 4
	.globl	nghttp2_mem_calloc
	.type	nghttp2_mem_calloc, @function
nghttp2_mem_calloc:
.LFB28:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	movq	(%rdi), %rdx
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE28:
	.size	nghttp2_mem_calloc, .-nghttp2_mem_calloc
	.p2align 4
	.globl	nghttp2_mem_realloc
	.type	nghttp2_mem_realloc, @function
nghttp2_mem_realloc:
.LFB29:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	32(%rdi), %rax
	movq	%rdx, %rsi
	movq	(%rdi), %rdx
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE29:
	.size	nghttp2_mem_realloc, .-nghttp2_mem_realloc
	.section	.data.rel.local,"aw"
	.align 32
	.type	mem_default, @object
	.size	mem_default, 40
mem_default:
	.quad	0
	.quad	default_malloc
	.quad	default_free
	.quad	default_calloc
	.quad	default_realloc
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
