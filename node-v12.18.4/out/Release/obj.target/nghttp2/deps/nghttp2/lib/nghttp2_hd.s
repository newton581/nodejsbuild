	.file	"nghttp2_hd.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dat"
.LC1:
	.string	"eta"
.LC2:
	.string	"lin"
.LC3:
	.string	"fro"
.LC4:
	.string	"hos"
.LC5:
	.string	"var"
.LC6:
	.string	"cooki"
.LC7:
	.string	"serve"
.LC8:
	.string	"accep"
.LC9:
	.string	"expec"
.LC10:
	.string	":metho"
.LC11:
	.string	":schem"
.LC12:
	.string	"upgrad"
.LC13:
	.string	"refres"
.LC14:
	.string	"refere"
.LC15:
	.string	":statu"
.LC16:
	.string	"expire"
.LC17:
	.string	"if-rang"
.LC18:
	.string	"if-matc"
.LC19:
	.string	"locatio"
.LC20:
	.string	"keep-aliv"
.LC21:
	.string	"set-cooki"
.LC22:
	.string	"connectio"
.LC23:
	.string	"user-agen"
.LC24:
	.string	":authorit"
.LC25:
	.string	"retry-afte"
.LC26:
	.string	"content-typ"
.LC27:
	.string	"max-forward"
.LC28:
	.string	"last-modifie"
.LC29:
	.string	"content-rang"
.LC30:
	.string	"if-none-matc"
.LC31:
	.string	"cache-contro"
.LC32:
	.string	"authorizatio"
.LC33:
	.string	"accept-range"
.LC34:
	.string	"content-lengt"
.LC35:
	.string	"accept-charse"
.LC36:
	.string	"accept-languag"
.LC37:
	.string	"accept-encodin"
.LC38:
	.string	"content-languag"
.LC39:
	.string	"www-authenticat"
.LC40:
	.string	"content-encodin"
.LC41:
	.string	"content-locatio"
.LC42:
	.string	"proxy-connectio"
.LC43:
	.string	"if-modified-sinc"
.LC44:
	.string	"transfer-encodin"
.LC45:
	.string	"proxy-authenticat"
.LC46:
	.string	"if-unmodified-sinc"
.LC47:
	.string	"content-dispositio"
.LC48:
	.string	"proxy-authorizatio"
.LC49:
	.string	"strict-transport-securit"
.LC50:
	.string	"access-control-allow-origi"
	.text
	.p2align 4
	.type	lookup_token, @function
lookup_token:
.LFB55:
	.cfi_startproc
	cmpq	$27, %rsi
	ja	.L171
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L171-.L4
	.long	.L171-.L4
	.long	.L23-.L4
	.long	.L22-.L4
	.long	.L21-.L4
	.long	.L20-.L4
	.long	.L19-.L4
	.long	.L18-.L4
	.long	.L17-.L4
	.long	.L16-.L4
	.long	.L15-.L4
	.long	.L14-.L4
	.long	.L13-.L4
	.long	.L12-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L171-.L4
	.long	.L171-.L4
	.long	.L171-.L4
	.long	.L171-.L4
	.long	.L171-.L4
	.long	.L5-.L4
	.long	.L171-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	cmpb	$101, 1(%rdi)
	jne	.L172
	cmpb	$116, (%rdi)
	movl	$61, %edx
	movl	$-1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpb	$110, 26(%rdi)
	jne	.L240
	movabsq	$7002372221548392047, %rdx
	xorq	8(%rdi), %rdx
	movabsq	$7146495123166290785, %rax
	xorq	(%rdi), %rax
	orq	%rax, %rdx
	jne	.L169
	movabsq	$7598257762395450476, %rax
	cmpq	%rax, 16(%rdi)
	je	.L243
.L169:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpb	$121, 24(%rdi)
	jne	.L238
	movabsq	$8390891584508289394, %rdx
	xorq	8(%rdi), %rdx
	movabsq	$8371475252671313011, %rax
	xorq	(%rdi), %rax
	orq	%rax, %rdx
	jne	.L167
	movabsq	$8388361629458592557, %rax
	cmpq	%rax, 16(%rdi)
	je	.L244
.L167:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movzbl	18(%rdi), %eax
	cmpb	$101, %al
	je	.L159
	cmpb	$110, %al
	jne	.L245
	movabsq	$8388362703419435364, %rdx
	xorq	8(%rdi), %rdx
	movabsq	$3275364211029340003, %rax
	xorq	(%rdi), %rax
	orq	%rax, %rdx
	jne	.L163
	cmpw	$28521, 16(%rdi)
	je	.L246
.L163:
	movabsq	$8386118574450632820, %rdx
	xorq	8(%rdi), %rdx
	movabsq	$8458091574913364592, %rax
	xorq	(%rdi), %rax
	orq	%rax, %rdx
	jne	.L165
	cmpw	$28521, 16(%rdi)
	je	.L247
.L165:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpb	$101, 17(%rdi)
	jne	.L233
	movabsq	$7017568593162627188, %rdx
	xorq	8(%rdi), %rdx
	movabsq	$8458091574913364592, %rax
	xorq	(%rdi), %rax
	orq	%rax, %rdx
	jne	.L157
	cmpb	$116, 16(%rdi)
	je	.L248
.L157:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	16(%rdi), %eax
	cmpb	$101, %al
	je	.L151
	cmpb	$103, %al
	jne	.L249
	movabsq	$7956000646299018541, %rcx
	xorq	8(%rdi), %rcx
	movl	$-1, %eax
	movabsq	$8243107338930713204, %rdx
	xorq	(%rdi), %rdx
	orq	%rdx, %rcx
	jne	.L1
	movl	$56, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	15(%rdi), %edx
	cmpb	$103, %dl
	je	.L139
	cmpb	$110, %dl
	je	.L140
	movl	$-1, %eax
	cmpb	$101, %dl
	je	.L250
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	9(%rdi), %eax
	cmpb	$116, %al
	je	.L85
	ja	.L86
	cmpb	$101, %al
	je	.L87
	cmpb	$110, %al
	jne	.L251
	movabsq	$7598807758576447331, %rax
	cmpq	%rax, (%rdi)
	je	.L252
.L94:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	cmpb	$108, 8(%rdi)
	jne	.L204
	movabsq	$8026381506781016122, %rax
	movl	$66, %edx
	cmpq	%rax, (%rdi)
	movl	$-1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	7(%rdi), %edx
	cmpb	$104, %dl
	je	.L77
	cmpb	$110, %dl
	je	.L78
	movl	$-1, %eax
	cmpb	$101, %dl
	jne	.L1
	cmpl	$1915577961, (%rdi)
	je	.L253
.L79:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movzbl	13(%rdi), %eax
	cmpb	$104, %al
	je	.L127
	cmpb	$116, %al
	jne	.L254
	movabsq	$7146496209793016673, %rax
	cmpq	%rax, (%rdi)
	je	.L255
.L131:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movzbl	5(%rdi), %edx
	cmpb	$114, %dl
	je	.L47
	cmpb	$116, %dl
	je	.L48
	movl	$-1, %eax
	cmpb	$101, %dl
	jne	.L1
	cmpl	$1802465123, (%rdi)
	je	.L256
.L49:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	3(%rdi), %eax
	subl	$101, %eax
	cmpb	$20, %al
	ja	.L176
	leaq	.L27(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L27:
	.long	.L32-.L27
	.long	.L176-.L27
	.long	.L31-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L30-.L27
	.long	.L176-.L27
	.long	.L29-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L28-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L176-.L27
	.long	.L26-.L27
	.text
	.p2align 4,,10
	.p2align 3
.L22:
	movzbl	2(%rdi), %eax
	cmpb	$97, %al
	je	.L24
	cmpb	$101, %al
	jne	.L257
	cmpw	$26465, (%rdi)
	movl	$20, %edx
	movl	$-1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	14(%rdi), %eax
	cmpb	$101, %al
	je	.L133
	cmpb	$103, %al
	jne	.L258
	movabsq	$7290611397868872545, %rax
	cmpq	%rax, (%rdi)
	je	.L259
.L137:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	11(%rdi), %eax
	cmpb	$101, %al
	je	.L102
	cmpb	$115, %al
	jne	.L260
	movabsq	$8607064322498650477, %rax
	cmpq	%rax, (%rdi)
	je	.L261
.L106:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpb	$114, 10(%rdi)
	jne	.L210
	movabsq	$7377227664244106610, %rax
	cmpq	%rax, (%rdi)
	je	.L262
.L100:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movzbl	6(%rdi), %eax
	subl	$100, %eax
	cmpb	$15, %al
	ja	.L192
	leaq	.L58(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L58:
	.long	.L62-.L58
	.long	.L61-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L60-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L192-.L58
	.long	.L59-.L58
	.long	.L57-.L58
	.text
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	12(%rdi), %eax
	subl	$100, %eax
	cmpb	$15, %al
	ja	.L214
	leaq	.L109(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L109:
	.long	.L114-.L109
	.long	.L113-.L109
	.long	.L214-.L109
	.long	.L214-.L109
	.long	.L112-.L109
	.long	.L214-.L109
	.long	.L214-.L109
	.long	.L214-.L109
	.long	.L111-.L109
	.long	.L214-.L109
	.long	.L110-.L109
	.long	.L214-.L109
	.long	.L214-.L109
	.long	.L214-.L109
	.long	.L214-.L109
	.long	.L108-.L109
	.text
	.p2align 4,,10
	.p2align 3
.L20:
	movzbl	4(%rdi), %edx
	cmpb	$104, %dl
	je	.L45
	cmpb	$119, %dl
	je	.L46
	movl	$-1, %eax
	cmpb	$101, %dl
	jne	.L1
	cmpl	$1735287154, (%rdi)
	movl	$49, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	movabsq	$3275364211029340003, %rax
	cmpq	%rax, (%rdi)
	je	.L263
.L141:
	movabsq	$7526769937478023031, %rax
	cmpq	%rax, (%rdi)
	je	.L264
.L143:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$-1, %eax
	ret
.L62:
	cmpl	$1952804154, (%rdi)
	je	.L265
.L63:
	movl	$-1, %eax
	ret
.L60:
	cmpl	$1919313266, (%rdi)
	je	.L266
.L69:
	movl	$-1, %eax
	ret
.L57:
	cmpl	$1635021626, (%rdi)
	je	.L267
.L73:
	cmpl	$1768978533, (%rdi)
	je	.L268
.L75:
	movl	$-1, %eax
	ret
.L61:
	cmpl	$1751348026, (%rdi)
	je	.L269
.L65:
	cmpl	$1919381621, (%rdi)
	je	.L270
.L67:
	movl	$-1, %eax
	ret
.L59:
	cmpl	$1701209458, (%rdi)
	je	.L271
.L71:
	movl	$-1, %eax
	ret
.L114:
	movabsq	$7237123168202350956, %rax
	cmpq	%rax, (%rdi)
	je	.L272
.L115:
	movl	$-1, %eax
	ret
.L108:
	movabsq	$8227360120361935713, %rax
	cmpq	%rax, (%rdi)
	je	.L273
.L125:
	movl	$-1, %eax
	ret
.L112:
	movabsq	$3271142129223427689, %rax
	cmpq	%rax, (%rdi)
	je	.L274
.L119:
	movl	$-1, %eax
	ret
.L110:
	movabsq	$8820707168001226081, %rax
	cmpq	%rax, (%rdi)
	je	.L275
.L123:
	movl	$-1, %eax
	ret
.L113:
	movabsq	$3275364211029340003, %rax
	cmpq	%rax, (%rdi)
	je	.L276
.L117:
	movl	$-1, %eax
	ret
.L111:
	movabsq	$8026308874470646115, %rax
	cmpq	%rax, (%rdi)
	je	.L277
.L121:
	movl	$-1, %eax
	ret
.L26:
	cmpw	$24950, (%rdi)
	je	.L278
.L43:
	movl	$-1, %eax
	ret
.L30:
	cmpw	$26988, (%rdi)
	je	.L279
.L37:
	movl	$-1, %eax
	ret
.L28:
	cmpw	$28520, (%rdi)
	je	.L280
.L41:
	movl	$-1, %eax
	ret
.L31:
	cmpw	$29797, (%rdi)
	je	.L281
.L35:
	movl	$-1, %eax
	ret
.L29:
	cmpw	$29286, (%rdi)
	je	.L282
.L39:
	movl	$-1, %eax
	ret
.L32:
	cmpw	$24932, (%rdi)
	je	.L283
.L33:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	cmpb	$121, %al
	jne	.L284
	movabsq	$7598258015748579642, %rax
	cmpq	%rax, (%rdi)
	je	.L285
.L98:
	movl	$1, %eax
.L99:
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	movabsq	$7164780000596747625, %rcx
	xorq	8(%rdi), %rcx
	movl	$-1, %eax
	movabsq	$7379539894159107689, %rdx
	xorq	(%rdi), %rdx
	orq	%rdx, %rcx
	jne	.L1
	movl	$39, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpw	$26998, (%rdi)
	movl	$59, %edx
	movl	$-1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	movabsq	$3275364211029340003, %rax
	cmpq	%rax, (%rdi)
	je	.L286
.L129:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	movabsq	$7795014556134368097, %rax
	cmpq	%rax, (%rdi)
	je	.L287
.L135:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	movabsq	$3275364211029340003, %rax
	cmpq	%rax, (%rdi)
	je	.L288
.L104:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	movabsq	$7598466905525544553, %rdx
	xorq	8(%rdi), %rdx
	movabsq	$7237123447387416169, %rax
	xorq	(%rdi), %rax
	orq	%rax, %rdx
	jne	.L161
	cmpw	$25454, 16(%rdi)
	je	.L289
.L161:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	cmpw	$25972, 8(%rdi)
	jne	.L100
	movl	$52, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	$1701012321, (%rdi)
	je	.L290
.L53:
	cmpl	$1701869669, (%rdi)
	je	.L291
.L55:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	cmpl	$1987208563, (%rdi)
	je	.L292
.L51:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	movabsq	$3275364211029340003, %rax
	cmpq	%rax, (%rdi)
	je	.L293
.L147:
	movabsq	$8026308960639218288, %rax
	cmpq	%rax, (%rdi)
	je	.L294
.L149:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	cmpl	$1869376609, (%rdi)
	movl	$21, %edx
	movl	$-1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
	cmpl	$1952542778, (%rdi)
	sete	%al
	leal	-1(,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	$1633906540, (%rdi)
	je	.L295
.L83:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	cmpl	$1831691881, (%rdi)
	je	.L296
.L81:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	movabsq	$3275364211029340003, %rax
	cmpq	%rax, (%rdi)
	je	.L297
.L145:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movabsq	$7596553519254300011, %rax
	cmpq	%rax, (%rdi)
	je	.L298
.L90:
	movabsq	$7741528756228220275, %rax
	cmpq	%rax, (%rdi)
	je	.L299
.L92:
	movl	$-1, %eax
	ret
.L85:
	movabsq	$7306915768252593013, %rax
	cmpq	%rax, (%rdi)
	je	.L300
.L96:
	movl	$-1, %eax
	ret
.L244:
	movl	$55, %eax
	ret
.L248:
	movl	$47, %eax
	ret
.L243:
	cmpw	$26983, 24(%rdi)
	jne	.L169
	movl	$19, %eax
	ret
.L259:
	cmpl	$1685021550, 8(%rdi)
	jne	.L137
	cmpw	$28265, 12(%rdi)
	jne	.L137
	movl	$15, %eax
	ret
.L261:
	cmpw	$29281, 8(%rdi)
	jne	.L106
	cmpb	$100, 10(%rdi)
	jne	.L106
	movl	$46, %eax
	ret
.L286:
	cmpl	$1735288172, 8(%rdi)
	jne	.L129
	cmpb	$116, 12(%rdi)
	jne	.L129
	movl	$27, %eax
	ret
.L287:
	cmpl	$1969712737, 8(%rdi)
	jne	.L135
	cmpw	$26465, 12(%rdi)
	jne	.L135
	movl	$16, %eax
	ret
.L288:
	cmpw	$31092, 8(%rdi)
	jne	.L104
	cmpb	$112, 10(%rdi)
	jne	.L104
	movl	$30, %eax
	ret
.L255:
	cmpl	$1936875880, 8(%rdi)
	jne	.L131
	cmpb	$101, 12(%rdi)
	jne	.L131
	movl	$14, %eax
	ret
.L295:
	cmpw	$26996, 4(%rdi)
	jne	.L83
	cmpb	$111, 6(%rdi)
	jne	.L83
	movl	$45, %eax
	ret
.L296:
	cmpw	$29793, 4(%rdi)
	jne	.L81
	cmpb	$99, 6(%rdi)
	jne	.L81
	movl	$38, %eax
	ret
.L297:
	cmpl	$1868787301, 8(%rdi)
	jne	.L145
	cmpw	$26980, 12(%rdi)
	jne	.L145
	cmpb	$110, 14(%rdi)
	jne	.L145
	movl	$25, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	cmpl	$1735287148, 8(%rdi)
	jne	.L141
	cmpw	$24949, 12(%rdi)
	jne	.L141
	cmpb	$103, 14(%rdi)
	jne	.L141
	movl	$26, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	cmpb	$112, 4(%rdi)
	jne	.L53
	movl	$18, %eax
	ret
.L253:
	cmpw	$28257, 4(%rdi)
	jne	.L79
	cmpb	$103, 6(%rdi)
	jne	.L79
	movl	$41, %eax
	ret
.L292:
	cmpb	$101, 4(%rdi)
	jne	.L51
	movl	$53, %eax
	ret
.L256:
	cmpb	$105, 4(%rdi)
	jne	.L49
	movl	$31, %eax
	ret
.L293:
	cmpl	$1633906540, 8(%rdi)
	jne	.L147
	cmpw	$26996, 12(%rdi)
	jne	.L147
	cmpb	$111, 14(%rdi)
	jne	.L147
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	cmpl	$1667591790, 8(%rdi)
	jne	.L149
	cmpw	$26996, 12(%rdi)
	jne	.L149
	cmpb	$111, 14(%rdi)
	jne	.L149
	movl	$64, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	cmpb	$111, 8(%rdi)
	jne	.L94
	movl	$62, %eax
	ret
.L300:
	cmpb	$110, 8(%rdi)
	jne	.L96
	movl	$57, %eax
	ret
.L291:
	cmpb	$99, 4(%rdi)
	jne	.L55
	movl	$34, %eax
	ret
.L298:
	cmpb	$118, 8(%rdi)
	jne	.L90
	movl	$63, %eax
	ret
.L264:
	cmpl	$1769238117, 8(%rdi)
	jne	.L143
	cmpw	$24931, 12(%rdi)
	jne	.L143
	cmpb	$116, 14(%rdi)
	jne	.L143
	movl	$60, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	xorl	%eax, %eax
	cmpb	$116, 8(%rdi)
	jne	.L98
	jmp	.L99
.L269:
	cmpw	$28005, 4(%rdi)
	jne	.L65
	movl	$5, %eax
	ret
.L265:
	cmpw	$28520, 4(%rdi)
	jne	.L63
	movl	$1, %eax
	ret
.L266:
	cmpw	$29541, 4(%rdi)
	jne	.L69
	movl	$51, %eax
	ret
.L267:
	cmpw	$30068, 4(%rdi)
	jne	.L73
	movl	$7, %eax
	ret
.L246:
	movl	$24, %eax
	ret
.L289:
	movl	$42, %eax
	ret
.L271:
	cmpw	$25970, 4(%rdi)
	jne	.L71
	movl	$50, %eax
	ret
.L299:
	cmpb	$105, 8(%rdi)
	jne	.L92
	movl	$54, %eax
	ret
.L279:
	cmpb	$110, 2(%rdi)
	jne	.L37
	movl	$44, %eax
	ret
.L281:
	cmpb	$97, 2(%rdi)
	jne	.L35
	movl	$33, %eax
	ret
.L278:
	cmpb	$114, 2(%rdi)
	jne	.L43
	movl	$58, %eax
	ret
.L280:
	cmpb	$115, 2(%rdi)
	jne	.L41
	movl	$37, %eax
	ret
.L277:
	cmpl	$1869771886, 8(%rdi)
	jne	.L121
	movl	$23, %eax
	ret
.L282:
	cmpb	$111, 2(%rdi)
	jne	.L39
	movl	$36, %eax
	ret
.L273:
	cmpl	$1701277281, 8(%rdi)
	jne	.L125
	movl	$17, %eax
	ret
.L275:
	cmpl	$1869182049, 8(%rdi)
	jne	.L123
	movl	$22, %eax
	ret
.L272:
	cmpl	$1701406313, 8(%rdi)
	jne	.L115
	movl	$43, %eax
	ret
.L274:
	cmpl	$1668571501, 8(%rdi)
	jne	.L119
	movl	$40, %eax
	ret
.L276:
	cmpl	$1735287154, 8(%rdi)
	jne	.L117
	movl	$29, %eax
	ret
.L283:
	cmpb	$116, 2(%rdi)
	jne	.L33
	movl	$32, %eax
	ret
.L247:
	movl	$48, %eax
	ret
.L270:
	cmpw	$25697, 4(%rdi)
	jne	.L67
	movl	$65, %eax
	ret
.L268:
	cmpw	$25970, 4(%rdi)
	jne	.L75
	movl	$35, %eax
	ret
.L240:
	movl	$-1, %eax
	ret
.L210:
	movl	$-1, %eax
	ret
.L204:
	movl	$-1, %eax
	ret
.L238:
	movl	$-1, %eax
	ret
.L233:
	movl	$-1, %eax
	ret
.L172:
	movl	$-1, %eax
	ret
.L251:
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE55:
	.size	lookup_token, .-lookup_token
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"../deps/nghttp2/lib/nghttp2_hd.c"
	.section	.rodata.str1.1
.LC52:
	.string	"enclen == len"
	.text
	.p2align 4
	.type	emit_string, @function
emit_string:
.LFB86:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	nghttp2_hd_huff_encode_count@PLT
	cmpq	%rax, %r14
	jbe	.L302
	movq	%rax, %rbx
	cmpq	$126, %rax
	jbe	.L315
	leaq	-127(%rax), %rax
	movl	$1, %r15d
	cmpq	$127, %rax
	ja	.L304
	movb	$-1, -80(%rbp)
	movl	$2, %r8d
	leaq	-79(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L305:
	movb	%al, (%rdx)
.L310:
	leaq	-80(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r13, %rdi
	call	nghttp2_bufs_add@PLT
	testl	%eax, %eax
	jne	.L301
	testl	%r15d, %r15d
	jne	.L324
	cmpq	%rbx, %r14
	jne	.L325
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_bufs_add@PLT
.L301:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L326
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	cmpq	$126, %r14
	jbe	.L317
	leaq	-127(%r14), %rax
	movq	%r14, %rbx
	xorl	%r15d, %r15d
	cmpq	$127, %rax
	jbe	.L327
.L304:
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L306:
	shrq	$7, %rsi
	movq	%rcx, %rdx
	addq	$1, %rcx
	cmpq	$127, %rsi
	ja	.L306
	leaq	2(%rdx), %r8
	cmpq	$16, %r8
	ja	.L319
	cmpl	$1, %r15d
	sbbl	%edx, %edx
	andl	$-128, %edx
	subl	$1, %edx
	movb	%dl, -80(%rbp)
	leaq	-79(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L311:
	movl	%eax, %ecx
	addq	$1, %rdx
	shrq	$7, %rax
	orl	$-128, %ecx
	movb	%cl, -1(%rdx)
	cmpq	$127, %rax
	ja	.L311
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_hd_huff_encode@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$1, %r15d
	movl	$-128, %eax
.L303:
	orl	%ebx, %eax
	movl	$1, %r8d
	movb	%al, -80(%rbp)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r14, %rbx
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	jmp	.L303
.L327:
	movb	$127, -80(%rbp)
	movl	$2, %r8d
	leaq	-79(%rbp), %rdx
	jmp	.L305
.L319:
	movl	$-523, %eax
	jmp	.L301
.L326:
	call	__stack_chk_fail@PLT
.L325:
	leaq	__PRETTY_FUNCTION__.5143(%rip), %rcx
	movl	$1004, %edx
	leaq	.LC51(%rip), %rsi
	leaq	.LC52(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE86:
	.size	emit_string, .-emit_string
	.section	.rodata.str1.1
.LC53:
	.string	"idx < ringbuf->len"
	.text
	.p2align 4
	.type	hd_ringbuf_get.part.0, @function
hd_ringbuf_get.part.0:
.LFB140:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.5005(%rip), %rcx
	movl	$605, %edx
	leaq	.LC51(%rip), %rsi
	leaq	.LC53(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE140:
	.size	hd_ringbuf_get.part.0, .-hd_ringbuf_get.part.0
	.section	.rodata.str1.1
.LC54:
	.string	"0"
	.text
	.p2align 4
	.type	pack_first_byte.part.0, @function
pack_first_byte.part.0:
.LFB143:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.5151(%rip), %rcx
	movl	$1020, %edx
	leaq	.LC51(%rip), %rsi
	leaq	.LC54(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE143:
	.size	pack_first_byte.part.0, .-pack_first_byte.part.0
	.p2align 4
	.type	emit_indname_block.isra.0, @function
emit_indname_block.isra.0:
.LFB149:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$1, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L333
	cmpq	$14, %rsi
	jbe	.L334
	movl	$-16, %edi
	movl	$15, %r10d
	movl	$15, %eax
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L333:
	cmpq	$62, %rsi
	jbe	.L351
	movl	$-64, %edi
	movl	$63, %r10d
	movl	$63, %eax
.L344:
	subq	%rax, %rsi
	cmpq	$127, %rsi
	jbe	.L335
	movq	%rsi, %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L336:
	shrq	$7, %rcx
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	$127, %rcx
	ja	.L336
	leaq	2(%rdx), %r9
	cmpq	$16, %r9
	ja	.L357
	cmpl	$2, %r8d
	ja	.L343
	leaq	CSWTCH.61(%rip), %rax
	movl	%r8d, %r8d
	andb	(%rax,%r8), %dil
	leaq	-63(%rbp), %rax
	orl	%r10d, %edi
	movb	%dil, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L339:
	movl	%esi, %edx
	addq	$1, %rax
	shrq	$7, %rsi
	orl	$-128, %edx
	movb	%dl, -1(%rax)
	cmpq	$127, %rsi
	ja	.L339
.L346:
	movb	%sil, (%rax)
.L341:
	leaq	-64(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	call	nghttp2_bufs_add@PLT
	testl	%eax, %eax
	jne	.L332
	movq	(%r12), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	emit_string
.L332:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L358
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movl	$-64, %eax
	xorl	%r8d, %r8d
.L342:
	movl	%r8d, %r8d
	leaq	CSWTCH.61(%rip), %rdx
	movl	$1, %r9d
	andb	(%rdx,%r8), %al
	orl	%eax, %esi
	movb	%sil, -64(%rbp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L334:
	cmpl	$2, %r8d
	ja	.L343
	movl	$-16, %eax
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L335:
	cmpl	$2, %r8d
	ja	.L343
	leaq	CSWTCH.61(%rip), %rax
	movl	%r8d, %r8d
	movl	$2, %r9d
	andb	(%rax,%r8), %dil
	leaq	-63(%rbp), %rax
	orl	%r10d, %edi
	movb	%dil, -64(%rbp)
	jmp	.L346
.L357:
	movl	$-523, %eax
	jmp	.L332
.L343:
	call	pack_first_byte.part.0
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE149:
	.size	emit_indname_block.isra.0, .-emit_indname_block.isra.0
	.p2align 4
	.type	hd_inflate_read_len.constprop.0, @function
hd_inflate_read_len.constprop.0:
.LFB158:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	184(%rdi), %rax
	movq	%rcx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	216(%rdi), %rbx
	movq	$0, 216(%rdi)
	movl	$0, (%rsi)
	testl	%eax, %eax
	jne	.L360
	movl	$1, %r10d
	movl	%r8d, %ecx
	movzbl	(%rdx), %eax
	sall	%cl, %r10d
	subl	$1, %r10d
	andl	%r10d, %eax
	cmpb	%al, %r10b
	je	.L361
	movl	$1, (%rsi)
	movzbl	%al, %r10d
	movl	$1, %eax
.L362:
	cmpq	%r9, %r10
	ja	.L365
	popq	%rbx
	popq	%r12
	movq	%r10, 184(%rdi)
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movl	%eax, %r10d
	cmpq	%rcx, %rdx
	je	.L364
	movq	%rdx, %r8
.L363:
	movzbl	(%r8), %r13d
	movl	%r13d, %eax
	andl	$127, %eax
	cmpq	$31, %rbx
	ja	.L365
	movl	$-1, %r12d
	movl	%ebx, %ecx
	movl	%r12d, %r15d
	shrl	%cl, %r15d
	cmpl	%r15d, %eax
	ja	.L365
	movl	%ebx, %ecx
	sall	%cl, %eax
	movl	%eax, %ecx
	notl	%ecx
	cmpl	%ecx, %r10d
	ja	.L365
	addl	%eax, %r10d
	testb	%r13b, %r13b
	jns	.L366
	leaq	1(%r8), %rax
	leaq	7(%rbx), %rcx
	cmpq	%rax, %r11
	je	.L367
	movzbl	1(%r8), %r14d
	movl	%r14d, %r13d
	andl	$127, %r13d
	cmpq	$31, %rcx
	ja	.L365
	shrl	%cl, %r12d
	cmpl	%r12d, %r13d
	ja	.L365
	sall	%cl, %r13d
	movl	%r13d, %r12d
	notl	%r12d
	cmpl	%r12d, %r10d
	ja	.L365
	addl	%r13d, %r10d
	testb	%r14b, %r14b
	jns	.L377
	leaq	2(%r8), %rax
	leaq	14(%rbx), %rcx
	cmpq	%rax, %r11
	je	.L367
	movzbl	2(%r8), %r14d
	movl	%r14d, %r12d
	andl	$127, %r12d
	cmpq	$31, %rcx
	ja	.L365
	movl	$-1, %r13d
	movl	%r13d, %r15d
	shrl	%cl, %r15d
	cmpl	%r15d, %r12d
	ja	.L365
	sall	%cl, %r12d
	movl	%r12d, %r15d
	notl	%r15d
	cmpl	%r15d, %r10d
	ja	.L365
	addl	%r12d, %r10d
	testb	%r14b, %r14b
	jns	.L377
	leaq	3(%r8), %rax
	leaq	21(%rbx), %rcx
	cmpq	%rax, %r11
	je	.L367
	movzbl	3(%r8), %r14d
	movl	%r14d, %r12d
	andl	$127, %r12d
	cmpq	$31, %rcx
	ja	.L365
	shrl	%cl, %r13d
	cmpl	%r13d, %r12d
	ja	.L365
	sall	%cl, %r12d
	movl	%r12d, %r13d
	notl	%r13d
	cmpl	%r13d, %r10d
	ja	.L365
	addl	%r12d, %r10d
	testb	%r14b, %r14b
	jns	.L377
	leaq	4(%r8), %rax
	leaq	28(%rbx), %rcx
	cmpq	%rax, %r11
	je	.L367
	movzbl	4(%r8), %r13d
	movl	%r13d, %r12d
	andl	$127, %r12d
	cmpq	$31, %rcx
	ja	.L365
	movl	$-1, %r14d
	shrl	%cl, %r14d
	cmpl	%r14d, %r12d
	ja	.L365
	sall	%cl, %r12d
	movl	%r12d, %r14d
	notl	%r14d
	cmpl	%r14d, %r10d
	ja	.L365
	addl	%r12d, %r10d
	testb	%r13b, %r13b
	jns	.L377
	leaq	5(%r8), %rax
	leaq	35(%rbx), %rcx
	cmpq	%rax, %r11
	je	.L367
.L365:
	popq	%rbx
	movq	$-523, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	leaq	1(%rdx), %r8
	movzbl	%r10b, %r10d
	cmpq	%r8, %r11
	jne	.L363
	movl	$1, %eax
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%rbx, 216(%rdi)
	cmpq	%r8, %r11
	je	.L382
.L371:
	movl	$1, (%rsi)
	leaq	1(%r8), %rax
	subq	%rdx, %rax
.L372:
	cmpq	$-1, %rax
	jne	.L362
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%rcx, 216(%rdi)
	subq	%rdx, %rax
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%rbx, 216(%rdi)
	xorl	%eax, %eax
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%rcx, 216(%rdi)
	movq	%rax, %r8
	jmp	.L371
.L382:
	movq	%r11, %rax
	subq	%rdx, %rax
	jmp	.L372
	.cfi_endproc
.LFE158:
	.size	hd_inflate_read_len.constprop.0, .-hd_inflate_read_len.constprop.0
	.p2align 4
	.type	hd_inflate_commit_newname, @function
hd_inflate_commit_newname:
.LFB113:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	152(%rdi), %r15
	cmpb	$0, 234(%rdi)
	movq	160(%rdi), %rax
	setne	-69(%rbp)
	movq	24(%r15), %rsi
	movq	16(%r15), %rdi
	movq	%rax, -64(%rbp)
	call	lookup_token
	cmpb	$0, 233(%r13)
	movl	%eax, -68(%rbp)
	jne	.L384
	movq	%r15, %xmm0
	movhps	-64(%rbp), %xmm0
.L385:
	movl	-68(%rbp), %eax
	movups	%xmm0, (%rbx)
	movl	%eax, 16(%rbx)
	movzbl	-69(%rbp), %eax
	movb	%al, 20(%rbx)
	xorl	%eax, %eax
	movups	%xmm0, 168(%r13)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 152(%r13)
.L383:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	32(%r13), %r14
	addq	24(%rax), %rsi
	leaq	32(%rsi), %rax
	movq	48(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	movq	40(%r13), %rax
	leaq	(%rax,%rdi), %rcx
	cmpq	%rsi, %rcx
	ja	.L386
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L388:
	movq	16(%r13), %rcx
	subq	$1, %rdx
	movq	0(%r13), %rsi
	addq	%rdx, %rcx
	andq	8(%r13), %rcx
	movq	(%rsi,%rcx,8), %r12
	movq	$-32, %rcx
	movq	(%r12), %r8
	movq	8(%r12), %rdi
	subq	24(%rdi), %rcx
	subq	24(%r8), %rcx
	movq	%rdx, 24(%r13)
	addq	%rcx, %rax
	movq	%rax, 40(%r13)
	call	nghttp2_rcbuf_decref@PLT
	movq	(%r12), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movq	40(%r13), %rax
	movq	-80(%rbp), %rdx
	movq	48(%r13), %rsi
	addq	%rax, %rdx
	cmpq	%rsi, %rdx
	jbe	.L387
.L386:
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L388
.L387:
	movq	-64(%rbp), %r12
	movq	%r15, %xmm0
	movq	%r12, %xmm3
	punpcklqdq	%xmm3, %xmm0
	cmpq	%rsi, -80(%rbp)
	ja	.L385
	movl	$80, %esi
	movq	%r14, %rdi
	movaps	%xmm0, -64(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L399
	movdqa	-64(%rbp), %xmm0
	movq	24(%r15), %xmm1
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movq	16(%r15), %xmm2
	movups	%xmm0, (%rax)
	movl	-68(%rbp), %eax
	movhps	24(%r12), %xmm1
	movhps	16(%r12), %xmm2
	movq	$0, 64(%r8)
	movl	%eax, 16(%r8)
	movzbl	-69(%rbp), %eax
	movl	$0, 76(%r8)
	movb	%al, 20(%r8)
	movb	%al, 56(%r8)
	movups	%xmm2, 24(%r8)
	movups	%xmm1, 40(%r8)
	movaps	%xmm0, -96(%rbp)
	call	nghttp2_rcbuf_incref@PLT
	movq	-64(%rbp), %r8
	movq	8(%r8), %rdi
	call	nghttp2_rcbuf_incref@PLT
	movq	24(%r13), %rax
	movq	8(%r13), %r15
	movq	-64(%rbp), %r8
	movdqa	-96(%rbp), %xmm0
	addq	$1, %rax
	leaq	1(%r15), %rcx
	cmpq	%rcx, %rax
	jbe	.L391
	movl	$1, %r15d
	cmpq	$1, %rax
	je	.L400
	.p2align 4,,10
	.p2align 3
.L393:
	addq	%r15, %r15
	cmpq	%r15, %rax
	ja	.L393
	leaq	0(,%r15,8), %rsi
.L392:
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L394
	movq	24(%r13), %rsi
	movq	0(%r13), %r9
	movdqa	-96(%rbp), %xmm0
	testq	%rsi, %rsi
	je	.L398
	movq	16(%r13), %rax
	movq	8(%r13), %r10
	movq	%rax, %rcx
	addq	%rax, %rsi
	negq	%rcx
	leaq	(%r11,%rcx,8), %rdi
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r10, %rdx
	andq	%rax, %rdx
	movq	(%r9,%rdx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L397
.L398:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r11, -96(%rbp)
	subq	$1, %r15
	movq	%r8, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	nghttp2_mem_free@PLT
	movq	-96(%rbp), %r11
	movq	24(%r13), %rax
	movq	%r15, 8(%r13)
	movq	-64(%rbp), %r8
	movdqa	-112(%rbp), %xmm0
	movq	$-1, %rcx
	movq	%r11, 0(%r13)
	addq	$1, %rax
.L396:
	movq	%rcx, 16(%r13)
	movq	%r8, (%r11,%r15,8)
	movq	%rax, 24(%r13)
	movl	56(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 56(%r13)
	movl	%eax, 72(%r8)
	movq	-80(%rbp), %rax
	movl	$0, 76(%r8)
	addq	%rax, 40(%r13)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L391:
	movq	16(%r13), %rdx
	movq	0(%r13), %r11
	leaq	-1(%rdx), %rcx
	andq	%rcx, %r15
	jmp	.L396
.L394:
	movq	8(%r8), %rdi
	movq	%r8, -64(%rbp)
	call	nghttp2_rcbuf_decref@PLT
	movq	-64(%rbp), %r8
	movq	(%r8), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	-64(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L400:
	movl	$8, %esi
	jmp	.L392
.L399:
	movl	$-901, %eax
	jmp	.L383
	.cfi_endproc
.LFE113:
	.size	hd_inflate_commit_newname, .-hd_inflate_commit_newname
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"INDEX_RANGE_VALID(context, idx)"
	.text
	.p2align 4
	.type	hd_inflate_commit_indname, @function
hd_inflate_commit_indname:
.LFB114:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	192(%rdi), %r12
	leaq	61(%rax), %rcx
	cmpq	%rcx, %r12
	jnb	.L440
	movq	%rdi, %r13
	movq	%rsi, %rbx
	cmpq	$60, %r12
	jbe	.L410
	subq	$61, %r12
	cmpq	%r12, %rax
	jbe	.L441
	movq	(%rdi), %rax
	addq	16(%rdi), %r12
	andq	8(%rdi), %r12
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %r12
	movl	16(%rax), %eax
	movl	%eax, -64(%rbp)
.L412:
	cmpb	$0, 234(%r13)
	movq	%r12, %rdi
	setne	-57(%rbp)
	call	nghttp2_rcbuf_incref@PLT
	movq	160(%r13), %rax
	movq	%r12, %xmm0
	cmpb	$0, 233(%r13)
	movq	%rax, -56(%rbp)
	movhps	-56(%rbp), %xmm0
	jne	.L442
.L426:
	movl	-64(%rbp), %eax
	movups	%xmm0, (%rbx)
	movl	%eax, 16(%rbx)
	movzbl	-57(%rbp), %eax
	movb	%al, 20(%rbx)
	movq	-56(%rbp), %rax
	movq	%r12, 168(%r13)
	movq	$0, 160(%r13)
	movq	%rax, 176(%r13)
	xorl	%eax, %eax
.L408:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	leaq	static_table(%rip), %rax
	salq	$7, %r12
	addq	%rax, %r12
	movl	120(%r12), %eax
	movl	%eax, -64(%rbp)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L442:
	movq	24(%rax), %rcx
	movq	24(%r12), %rax
	movq	48(%r13), %rsi
	movq	32(%r13), %r14
	leaq	32(%rcx,%rax), %rax
	movq	%rax, %rdx
	movq	%rax, -72(%rbp)
	movq	40(%r13), %rax
	leaq	(%rax,%rdx), %rcx
	cmpq	%rsi, %rcx
	ja	.L414
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L416:
	movq	16(%r13), %rcx
	subq	$1, %rdx
	movq	0(%r13), %rsi
	addq	%rdx, %rcx
	andq	8(%r13), %rcx
	movq	(%rsi,%rcx,8), %r15
	movq	$-32, %rcx
	movq	(%r15), %r8
	movq	8(%r15), %rdi
	subq	24(%rdi), %rcx
	subq	24(%r8), %rcx
	movq	%rdx, 24(%r13)
	addq	%rcx, %rax
	movq	%rax, 40(%r13)
	call	nghttp2_rcbuf_decref@PLT
	movq	(%r15), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movq	40(%r13), %rax
	movq	-72(%rbp), %rdi
	movq	48(%r13), %rsi
	leaq	(%rdi,%rax), %rdx
	cmpq	%rsi, %rdx
	jbe	.L415
.L414:
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L416
.L415:
	movq	-56(%rbp), %r15
	movq	%r12, %xmm0
	movq	%r15, %xmm3
	punpcklqdq	%xmm3, %xmm0
	cmpq	%rsi, -72(%rbp)
	ja	.L426
	movl	$80, %esi
	movq	%r14, %rdi
	movaps	%xmm0, -96(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L417
	movdqa	-96(%rbp), %xmm0
	movq	24(%r12), %xmm1
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movq	16(%r12), %xmm2
	movups	%xmm0, (%rax)
	movl	-64(%rbp), %eax
	movhps	24(%r15), %xmm1
	movhps	16(%r15), %xmm2
	movq	$0, 64(%r8)
	movl	%eax, 16(%r8)
	movzbl	-57(%rbp), %eax
	movl	$0, 76(%r8)
	movb	%al, 20(%r8)
	movb	%al, 56(%r8)
	movups	%xmm2, 24(%r8)
	movups	%xmm1, 40(%r8)
	movaps	%xmm0, -112(%rbp)
	call	nghttp2_rcbuf_incref@PLT
	movq	-96(%rbp), %r8
	movq	8(%r8), %rdi
	call	nghttp2_rcbuf_incref@PLT
	movq	24(%r13), %rax
	movq	8(%r13), %r15
	movq	-96(%rbp), %r8
	movdqa	-112(%rbp), %xmm0
	addq	$1, %rax
	leaq	1(%r15), %rcx
	cmpq	%rcx, %rax
	jbe	.L418
	movl	$1, %r15d
	cmpq	$1, %rax
	je	.L428
	.p2align 4,,10
	.p2align 3
.L420:
	addq	%r15, %r15
	cmpq	%r15, %rax
	ja	.L420
	leaq	0(,%r15,8), %rsi
.L419:
	movq	%r14, %rdi
	movq	%r8, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L421
	movq	24(%r13), %rsi
	movq	0(%r13), %r9
	movdqa	-112(%rbp), %xmm0
	testq	%rsi, %rsi
	je	.L425
	movq	16(%r13), %rax
	movq	8(%r13), %r10
	movq	%rax, %rcx
	addq	%rax, %rsi
	negq	%rcx
	leaq	(%r11,%rcx,8), %rdi
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rax, %rdx
	andq	%r10, %rdx
	movq	(%r9,%rdx,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L424
.L425:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r11, -112(%rbp)
	subq	$1, %r15
	movq	%r8, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	nghttp2_mem_free@PLT
	movq	-112(%rbp), %r11
	movq	24(%r13), %rax
	movq	%r15, 8(%r13)
	movq	-96(%rbp), %r8
	movdqa	-128(%rbp), %xmm0
	movq	$-1, %rcx
	movq	%r11, 0(%r13)
	addq	$1, %rax
.L423:
	movq	%rcx, 16(%r13)
	movq	%r8, (%r11,%r15,8)
	movq	%rax, 24(%r13)
	movl	56(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 56(%r13)
	movl	%eax, 72(%r8)
	movq	-72(%rbp), %rax
	movl	$0, 76(%r8)
	addq	%rax, 40(%r13)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L418:
	movq	16(%r13), %rsi
	movq	0(%r13), %r11
	leaq	-1(%rsi), %rcx
	andq	%rcx, %r15
	jmp	.L423
.L421:
	movq	8(%r8), %rdi
	movq	%r8, -56(%rbp)
	call	nghttp2_rcbuf_decref@PLT
	movq	-56(%rbp), %r8
	movq	(%r8), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	-56(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	nghttp2_mem_free@PLT
.L417:
	movq	%r12, %rdi
	call	nghttp2_rcbuf_decref@PLT
	movl	$-901, %eax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$8, %esi
	jmp	.L419
.L440:
	leaq	__PRETTY_FUNCTION__.5241(%rip), %rcx
	movl	$1296, %edx
	leaq	.LC51(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	call	__assert_fail@PLT
.L441:
	call	hd_ringbuf_get.part.0
	.cfi_endproc
.LFE114:
	.size	hd_inflate_commit_indname, .-hd_inflate_commit_indname
	.p2align 4
	.globl	nghttp2_hd_entry_init
	.type	nghttp2_hd_entry_init, @function
nghttp2_hd_entry_init:
.LFB56:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movdqu	(%rsi), %xmm2
	movups	%xmm2, (%rdi)
	movq	16(%rsi), %rax
	movq	(%rsi), %rdx
	movq	24(%rdx), %xmm0
	movq	16(%rdx), %xmm1
	movq	%rax, 16(%rdi)
	movq	8(%rsi), %rax
	movhps	16(%rax), %xmm1
	movhps	24(%rax), %xmm0
	movups	%xmm1, 24(%rdi)
	movups	%xmm0, 40(%rdi)
	movzbl	20(%rsi), %eax
	movq	$0, 64(%rdi)
	movb	%al, 56(%rdi)
	movl	$0, 76(%rdi)
	movq	(%rdi), %rdi
	call	nghttp2_rcbuf_incref@PLT
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_rcbuf_incref@PLT
	.cfi_endproc
.LFE56:
	.size	nghttp2_hd_entry_init, .-nghttp2_hd_entry_init
	.p2align 4
	.globl	nghttp2_hd_entry_free
	.type	nghttp2_hd_entry_free, @function
nghttp2_hd_entry_free:
.LFB57:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_rcbuf_decref@PLT
	.cfi_endproc
.LFE57:
	.size	nghttp2_hd_entry_free, .-nghttp2_hd_entry_free
	.p2align 4
	.globl	nghttp2_hd_deflate_init
	.type	nghttp2_hd_deflate_init, @function
nghttp2_hd_deflate_init:
.LFB73:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, 32(%rbx)
	movl	$1024, %esi
	movb	$0, 60(%rbx)
	movq	$4096, 48(%rbx)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L449
	leaq	72(%rbx), %rdi
	movl	%ebx, %eax
	movq	$127, 8(%rbx)
	movdqa	.LC56(%rip), %xmm0
	andq	$-8, %rdi
	movq	$0, 16(%rbx)
	subl	%edi, %eax
	movq	$0, 24(%rbx)
	leal	1088(%rax), %ecx
	xorl	%eax, %eax
	movq	$0, 40(%rbx)
	shrl	$3, %ecx
	movl	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 1080(%rbx)
	rep stosq
	movb	$0, 1104(%rbx)
	movups	%xmm0, 1088(%rbx)
.L447:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L449:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L447
	.cfi_endproc
.LFE73:
	.size	nghttp2_hd_deflate_init, .-nghttp2_hd_deflate_init
	.p2align 4
	.globl	nghttp2_hd_deflate_init2
	.type	nghttp2_hd_deflate_init2, @function
nghttp2_hd_deflate_init2:
.LFB74:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$1024, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	%rdx, 32(%rbx)
	movb	$0, 60(%rbx)
	movq	$4096, 48(%rbx)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L454
	leaq	72(%rbx), %rdi
	movl	%ebx, %eax
	movq	$127, 8(%rbx)
	andq	$-8, %rdi
	movq	$0, 16(%rbx)
	subl	%edi, %eax
	movq	$0, 24(%rbx)
	leal	1088(%rax), %ecx
	xorl	%eax, %eax
	movq	$0, 40(%rbx)
	shrl	$3, %ecx
	movl	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 1080(%rbx)
	rep stosq
	cmpq	$4095, %r12
	jbe	.L457
.L453:
	movq	%r12, 1088(%rbx)
	movb	%al, 1104(%rbx)
	movl	$4294967295, %eax
	movq	%rax, 1096(%rbx)
	xorl	%eax, %eax
.L451:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movq	%r12, 48(%rbx)
	movl	$1, %eax
	jmp	.L453
.L454:
	movl	$-901, %eax
	jmp	.L451
	.cfi_endproc
.LFE74:
	.size	nghttp2_hd_deflate_init2, .-nghttp2_hd_deflate_init2
	.p2align 4
	.globl	nghttp2_hd_inflate_init
	.type	nghttp2_hd_inflate_init, @function
nghttp2_hd_inflate_init:
.LFB75:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, 32(%rbx)
	movl	$1024, %esi
	movb	$0, 60(%rbx)
	movq	$4096, 48(%rbx)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L460
	movabsq	$4294967296, %rax
	movdqa	.LC56(%rip), %xmm0
	movq	$127, 8(%rbx)
	leaq	72(%rbx), %rdi
	movq	%rax, 224(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 40(%rbx)
	movl	$0, 56(%rbx)
	movups	%xmm0, 200(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 168(%rbx)
	call	nghttp2_buf_init@PLT
	leaq	112(%rbx), %rdi
	call	nghttp2_buf_init@PLT
	movb	$0, 234(%rbx)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	$0, 216(%rbx)
	movw	%ax, 232(%rbx)
	xorl	%eax, %eax
	movups	%xmm0, 152(%rbx)
	movups	%xmm0, 184(%rbx)
.L458:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L460:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L458
	.cfi_endproc
.LFE75:
	.size	nghttp2_hd_inflate_init, .-nghttp2_hd_inflate_init
	.p2align 4
	.globl	nghttp2_hd_deflate_free
	.type	nghttp2_hd_deflate_free, @function
nghttp2_hd_deflate_free:
.LFB77:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	movq	32(%rdi), %r14
	je	.L463
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L464:
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	addq	%r12, %rax
	andq	8(%rbx), %rax
	addq	$1, %r12
	movq	(%rdx,%rax,8), %r13
	movq	8(%r13), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	0(%r13), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	cmpq	24(%rbx), %r12
	jb	.L464
.L463:
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE77:
	.size	nghttp2_hd_deflate_free, .-nghttp2_hd_deflate_free
	.p2align 4
	.globl	nghttp2_hd_inflate_free
	.type	nghttp2_hd_inflate_free, @function
nghttp2_hd_inflate_free:
.LFB78:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	176(%rdi), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	168(%rbx), %rdi
	call	nghttp2_rcbuf_decref@PLT
	pxor	%xmm0, %xmm0
	movq	160(%rbx), %rdi
	movups	%xmm0, 168(%rbx)
	call	nghttp2_rcbuf_decref@PLT
	movq	152(%rbx), %rdi
	call	nghttp2_rcbuf_decref@PLT
	cmpq	$0, 24(%rbx)
	movq	32(%rbx), %r14
	je	.L468
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L469:
	movq	16(%rbx), %rax
	movq	(%rbx), %rdx
	addq	%r12, %rax
	andq	8(%rbx), %rax
	addq	$1, %r12
	movq	(%rdx,%rax,8), %r13
	movq	8(%r13), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	0(%r13), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	cmpq	24(%rbx), %r12
	jb	.L469
.L468:
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE78:
	.size	nghttp2_hd_inflate_free, .-nghttp2_hd_inflate_free
	.p2align 4
	.globl	nghttp2_hd_deflate_change_table_size
	.type	nghttp2_hd_deflate_change_table_size, @function
nghttp2_hd_deflate_change_table_size:
.LFB94:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	$-32, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	%rsi, 1088(%rdi)
	movq	%rdi, %rbx
	movq	40(%rdi), %rcx
	movq	32(%rdi), %r13
	movb	$1, 1104(%rdi)
	cmovbe	1088(%rdi), %rsi
	cmpq	%rsi, 1096(%rdi)
	movq	%rsi, %rax
	cmovbe	1096(%rdi), %rax
	movq	%rsi, 48(%rdi)
	movq	%rax, 1096(%rdi)
	cmpq	%rsi, %rcx
	jbe	.L482
	.p2align 4,,10
	.p2align 3
.L473:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L482
	movq	16(%rbx), %rdx
	subq	$1, %rax
	movq	(%rbx), %rsi
	addq	%rax, %rdx
	andq	8(%rbx), %rdx
	movq	(%rsi,%rdx,8), %r12
	movq	%r14, %rdx
	movq	8(%r12), %rdi
	movq	(%r12), %rsi
	subq	24(%rdi), %rdx
	subq	24(%rsi), %rdx
	movq	%rax, 24(%rbx)
	addq	%rcx, %rdx
	movq	%rdx, 40(%rbx)
	movl	76(%r12), %edx
	andl	$127, %edx
	movq	64(%rbx,%rdx,8), %rax
	testq	%rax, %rax
	je	.L475
	cmpq	%r12, %rax
	je	.L487
.L478:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L475
	cmpq	%rdx, %r12
	je	.L476
	movq	%rdx, %rax
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L487:
	leaq	64(%rbx,%rdx,8), %rax
.L479:
	movq	64(%r12), %rdx
	movq	%rdx, (%rax)
	movq	$0, 64(%r12)
.L475:
	call	nghttp2_rcbuf_decref@PLT
	movq	(%r12), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movq	40(%rbx), %rcx
	cmpq	48(%rbx), %rcx
	ja	.L473
.L482:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L476:
	.cfi_restore_state
	addq	$64, %rax
	jmp	.L479
	.cfi_endproc
.LFE94:
	.size	nghttp2_hd_deflate_change_table_size, .-nghttp2_hd_deflate_change_table_size
	.p2align 4
	.globl	nghttp2_hd_inflate_change_table_size
	.type	nghttp2_hd_inflate_change_table_size, @function
nghttp2_hd_inflate_change_table_size:
.LFB95:
	.cfi_startproc
	endbr64
	cmpl	$1, 228(%rdi)
	ja	.L494
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	%rsi, 48(%rdi)
	ja	.L500
.L490:
	movq	40(%rbx), %rcx
	movq	%rsi, 48(%rbx)
	movq	$-32, %r12
	movq	%rsi, 200(%rbx)
	movq	32(%rbx), %r13
	cmpq	%rcx, %rsi
	jb	.L491
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L493:
	movq	16(%rbx), %rdx
	subq	$1, %rax
	movq	(%rbx), %rsi
	addq	%rax, %rdx
	andq	8(%rbx), %rdx
	movq	(%rsi,%rdx,8), %r14
	movq	%r12, %rdx
	movq	(%r14), %rsi
	movq	8(%r14), %rdi
	subq	24(%rdi), %rdx
	subq	24(%rsi), %rdx
	movq	%rax, 24(%rbx)
	addq	%rcx, %rdx
	movq	%rdx, 40(%rbx)
	call	nghttp2_rcbuf_decref@PLT
	movq	(%r14), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movq	40(%rbx), %rcx
	cmpq	48(%rbx), %rcx
	jbe	.L492
.L491:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L493
.L492:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	movl	$0, 228(%rdi)
	movq	%rsi, 208(%rdi)
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$-519, %eax
	ret
	.cfi_endproc
.LFE95:
	.size	nghttp2_hd_inflate_change_table_size, .-nghttp2_hd_inflate_change_table_size
	.p2align 4
	.globl	nghttp2_hd_table_get
	.type	nghttp2_hd_table_get, @function
nghttp2_hd_table_get:
.LFB97:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	24(%rsi), %rcx
	movq	%rdi, %rax
	leaq	61(%rcx), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rdx, %rdi
	jbe	.L507
	cmpq	$60, %rdx
	jbe	.L503
	subq	$61, %rdx
	cmpq	%rdx, %rcx
	jbe	.L508
	movq	(%rsi), %rcx
	addq	16(%rsi), %rdx
	andq	8(%rsi), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rcx,%rdx,8), %rdx
	movdqu	(%rdx), %xmm0
	movq	16(%rdx), %rdx
	movups	%xmm0, (%rax)
	movq	%rdx, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	salq	$7, %rdx
	leaq	static_table(%rip), %rcx
	movb	$0, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	(%rcx,%rdx), %rsi
	leaq	40(%rcx,%rdx), %rdx
	movl	120(%rsi), %edi
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movl	%edi, 16(%rax)
	ret
.L507:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5241(%rip), %rcx
	movl	$1296, %edx
	leaq	.LC51(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	call	__assert_fail@PLT
.L508:
	call	hd_ringbuf_get.part.0
	.cfi_endproc
.LFE97:
	.size	nghttp2_hd_table_get, .-nghttp2_hd_table_get
	.p2align 4
	.globl	nghttp2_hd_deflate_hd_bufs
	.type	nghttp2_hd_deflate_hd_bufs, @function
nghttp2_hd_deflate_hd_bufs:
.LFB101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 60(%rdi)
	jne	.L638
	cmpb	$0, 1104(%rdi)
	movq	%rdi, %rbx
	movq	%rdx, %r12
	jne	.L723
.L511:
	leaq	24(%r12), %r13
	cmpq	$0, -144(%rbp)
	movq	$0, -128(%rbp)
	movq	%r13, %r15
	je	.L606
	.p2align 4,,10
	.p2align 3
.L605:
	movq	-8(%r15), %r14
	movq	-24(%r15), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	lookup_token
	movl	%eax, %r8d
	cmpl	$-1, %eax
	je	.L724
	cmpl	$60, %eax
	jg	.L640
	cltq
	leaq	static_table(%rip), %rsi
	salq	$7, %rax
	movl	124(%rsi,%rax), %r12d
	movl	%r12d, %eax
	andl	$127, %eax
.L530:
	cmpl	$22, %r8d
	je	.L531
	cmpl	$31, %r8d
	jne	.L532
	cmpq	$19, (%r15)
	jbe	.L531
	testb	$1, 8(%r15)
	jne	.L534
.L616:
	movabsq	$18051240873164800, %rdx
	btq	%r8, %rdx
	jnc	.L536
.L535:
	movl	$1, -120(%rbp)
.L537:
	movq	64(%rbx,%rax,8), %rcx
	movq	%rax, -160(%rbp)
	testq	%rcx, %rcx
	je	.L725
	xorl	%r10d, %r10d
	cmpl	$-1, %r8d
	je	.L645
	movq	%r14, -136(%rbp)
	movq	%r10, %r14
	movq	%r13, -168(%rbp)
	movq	%r15, %r13
	movq	%rbx, %r15
	movl	%r8d, %ebx
	movl	%r12d, -176(%rbp)
	movq	%rcx, %r12
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L538:
	movq	64(%r12), %r12
	testq	%r12, %r12
	je	.L726
.L541:
	cmpl	16(%r12), %ebx
	jne	.L538
	movq	8(%r12), %rax
	testq	%r14, %r14
	movq	0(%r13), %rdx
	cmove	%r12, %r14
	cmpq	%rdx, 24(%rax)
	jne	.L538
	movq	-16(%r13), %rsi
	movq	16(%rax), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L538
	movq	%r15, %rbx
	movq	%r12, %rcx
	movq	%r13, %r15
.L540:
	movl	56(%rbx), %eax
	addl	$60, %eax
	subl	72(%rcx), %eax
	leaq	1(%rax), %r13
	cmpq	$126, %r13
	jbe	.L558
	subq	$126, %rax
	cmpq	$127, %rax
	jbe	.L564
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L565:
	shrq	$7, %rsi
	movq	%rcx, %rdx
	addq	$1, %rcx
	cmpq	$127, %rsi
	ja	.L565
	leaq	2(%rdx), %r8
	cmpq	$16, %r8
	ja	.L597
	movb	$-1, -80(%rbp)
	leaq	-79(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L567:
	movl	%eax, %ecx
	addq	$1, %rdx
	shrq	$7, %rax
	orl	$-128, %ecx
	movb	%cl, -1(%rdx)
	cmpq	$127, %rax
	ja	.L567
.L611:
	movb	%al, (%rdx)
.L568:
	movq	-152(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r8, %rdx
	call	nghttp2_bufs_add@PLT
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	je	.L603
.L517:
	movb	$1, 60(%rbx)
.L509:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L727
	movl	-120(%rbp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L639
	movq	%r13, %rax
	leaq	(%r14,%r13), %rcx
	movl	$-2128831035, %r12d
	.p2align 4,,10
	.p2align 3
.L529:
	movzbl	(%rax), %edx
	addq	$1, %rax
	xorl	%edx, %r12d
	imull	$16777619, %r12d, %r12d
	cmpq	%rcx, %rax
	jne	.L529
	movl	%r12d, %eax
	andl	$127, %eax
.L528:
	testb	$1, 8(%r15)
	je	.L536
.L534:
	movq	64(%rbx,%rax,8), %r10
	movq	%rax, -160(%rbp)
	testq	%r10, %r10
	jne	.L728
.L701:
	cmpl	$60, %r8d
	jbe	.L626
.L722:
	movl	$2, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L601:
	movl	-120(%rbp), %eax
	leaq	CSWTCH.61(%rip), %rsi
	movq	-152(%rbp), %rdi
	movzbl	(%rsi,%rax), %esi
	call	nghttp2_bufs_addb@PLT
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	jne	.L517
	movq	-152(%rbp), %r14
	movq	-8(%r15), %rdx
	movq	-24(%r15), %rsi
	movq	%r14, %rdi
	call	emit_string
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	jne	.L517
	movq	-16(%r15), %rsi
	movq	(%r15), %rdx
	movq	%r14, %rdi
	call	emit_string
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	jne	.L517
	.p2align 4,,10
	.p2align 3
.L603:
	addq	$1, -128(%rbp)
	addq	$40, %r15
	movq	-128(%rbp), %rax
	cmpq	%rax, -144(%rbp)
	jne	.L605
.L606:
	movl	$0, -120(%rbp)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L532:
	testb	$1, 8(%r15)
	jne	.L534
	cmpl	$3, %r8d
	je	.L535
	cmpl	$20, %r8d
	je	.L535
	cmpl	$54, %r8d
	jbe	.L616
.L536:
	movq	(%r15), %rcx
	movq	48(%rbx), %rdx
	movl	$0, -120(%rbp)
	addq	%r14, %rcx
	leaq	(%rdx,%rdx,2), %rdx
	addq	$32, %rcx
	shrq	$2, %rdx
	cmpq	%rdx, %rcx
	jbe	.L537
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L531:
	movq	64(%rbx,%rax,8), %r10
	movq	%rax, -160(%rbp)
	testq	%r10, %r10
	je	.L626
.L550:
	cmpl	16(%r10), %r8d
	jne	.L729
	cmpl	$60, %r8d
	jbe	.L626
.L643:
	movl	$2, -120(%rbp)
.L627:
	movl	56(%rbx), %eax
	addl	$60, %eax
	movl	%eax, %esi
	subl	72(%r10), %esi
	movq	%rsi, -136(%rbp)
.L559:
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	jne	.L602
	movq	24(%rbx), %rdx
	movq	32(%rbx), %rcx
	leaq	61(%rdx), %rax
	cmpq	%rax, -136(%rbp)
	jnb	.L730
	movq	-136(%rbp), %rax
	cmpq	$60, %rax
	jbe	.L574
	subq	$61, %rax
	cmpq	%rax, %rdx
	jbe	.L731
	movq	(%rbx), %rdx
	addq	16(%rbx), %rax
	andq	8(%rbx), %rax
	movq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdi
.L576:
	movl	%r8d, -168(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	nghttp2_rcbuf_incref@PLT
	movl	-168(%rbp), %r8d
	movq	-120(%rbp), %rcx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L640:
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L645:
	movl	%r8d, -176(%rbp)
	movq	%r13, -168(%rbp)
	movq	%r15, %r13
	movq	%r10, %r15
	movq	%rbx, -136(%rbp)
	movl	%r12d, %ebx
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L547:
	cmpl	$-1, 16(%r12)
	jne	.L543
	cmpl	76(%r12), %ebx
	jne	.L543
	movq	(%r12), %rax
	cmpq	24(%rax), %r14
	je	.L732
	.p2align 4,,10
	.p2align 3
.L543:
	movq	64(%r12), %r12
	testq	%r12, %r12
	jne	.L547
	movl	%ebx, %r12d
	movq	%r15, %r10
	movq	%r13, %r15
	movl	-176(%rbp), %r8d
	movq	-168(%rbp), %r13
	movq	-136(%rbp), %rbx
.L622:
	testq	%r10, %r10
	jne	.L627
.L561:
	movl	-120(%rbp), %edx
	testl	%edx, %edx
	jne	.L601
	movq	32(%rbx), %rcx
	leaq	-112(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	%r8d, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	nghttp2_rcbuf_new2@PLT
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	jne	.L517
	movq	-168(%rbp), %rcx
	movl	-176(%rbp), %r8d
	movq	$-1, -136(%rbp)
.L571:
	movq	-16(%r15), %rsi
	movq	(%r15), %rdx
	leaq	-104(%rbp), %rdi
	movl	%r8d, -168(%rbp)
	call	nghttp2_rcbuf_new2@PLT
	movl	-168(%rbp), %r8d
	testl	%eax, %eax
	movl	%eax, -120(%rbp)
	jne	.L733
	movq	-112(%rbp), %rax
	movq	48(%rbx), %rcx
	movl	%r8d, -96(%rbp)
	movb	$0, -92(%rbp)
	movq	32(%rbx), %r13
	movq	24(%rax), %r14
	movq	-104(%rbp), %rax
	addq	24(%rax), %r14
	leaq	32(%r14), %rax
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	movq	40(%rbx), %rax
	leaq	(%rax,%rdi), %rdx
	cmpq	%rdx, %rcx
	jnb	.L579
	movl	%r12d, -176(%rbp)
	movq	%rdi, %r14
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L578:
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L710
	movq	16(%rbx), %rcx
	subq	$1, %rdx
	movq	(%rbx), %rax
	addq	%rdx, %rcx
	andq	8(%rbx), %rcx
	movq	(%rax,%rcx,8), %r12
	movq	$-32, %rax
	movq	8(%r12), %rdi
	movq	(%r12), %rcx
	subq	24(%rdi), %rax
	subq	24(%rcx), %rax
	movq	%rdx, 24(%rbx)
	movl	76(%r12), %edx
	leaq	(%rax,%r8), %rax
	movq	%rax, 40(%rbx)
	andl	$127, %edx
	movq	64(%rbx,%rdx,8), %rax
	testq	%rax, %rax
	je	.L581
	cmpq	%rax, %r12
	je	.L734
.L584:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L581
	cmpq	%r12, %rdx
	je	.L582
	movq	%rdx, %rax
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L734:
	leaq	64(%rbx,%rdx,8), %rax
.L585:
	movq	64(%r12), %rdx
	movq	%rdx, (%rax)
	movq	$0, 64(%r12)
.L581:
	call	nghttp2_rcbuf_decref@PLT
	movq	(%r12), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movq	40(%rbx), %r8
	movq	48(%rbx), %rcx
	leaq	(%r14,%r8), %rax
	cmpq	%rcx, %rax
	ja	.L578
.L710:
	movl	-176(%rbp), %r12d
.L579:
	cmpq	-168(%rbp), %rcx
	jb	.L587
	movl	$80, %esi
	movq	%r13, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L588
	movdqa	-112(%rbp), %xmm2
	movq	-112(%rbp), %rdi
	movq	%r10, -176(%rbp)
	movups	%xmm2, (%rax)
	movq	-96(%rbp), %rax
	movq	24(%rdi), %xmm0
	movq	16(%rdi), %xmm1
	movq	%rax, 16(%r10)
	movq	-104(%rbp), %rax
	movhps	16(%rax), %xmm1
	movhps	24(%rax), %xmm0
	movzbl	-92(%rbp), %eax
	movq	$0, 64(%r10)
	movl	$0, 76(%r10)
	movb	%al, 56(%r10)
	movups	%xmm1, 24(%r10)
	movups	%xmm0, 40(%r10)
	call	nghttp2_rcbuf_incref@PLT
	movq	-176(%rbp), %r10
	movq	8(%r10), %rdi
	call	nghttp2_rcbuf_incref@PLT
	movq	24(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	-176(%rbp), %r10
	addq	$1, %rax
	leaq	1(%rdx), %rcx
	cmpq	%rcx, %rax
	jbe	.L589
	movl	$1, %edx
	cmpq	$1, %rax
	je	.L644
	.p2align 4,,10
	.p2align 3
.L591:
	addq	%rdx, %rdx
	cmpq	%rdx, %rax
	ja	.L591
	leaq	0(,%rdx,8), %rsi
.L590:
	movq	%r13, %rdi
	movq	%r10, -184(%rbp)
	movq	%rdx, -176(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	-184(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L592
	movq	24(%rbx), %rsi
	movq	(%rbx), %r8
	movq	-176(%rbp), %rdx
	testq	%rsi, %rsi
	je	.L596
	movq	16(%rbx), %rax
	movq	8(%rbx), %r11
	movq	%rax, %rcx
	addq	%rax, %rsi
	negq	%rcx
	leaq	(%r9,%rcx,8), %rdi
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%r11, %rcx
	andq	%rax, %rcx
	movq	(%r8,%rcx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L595
.L596:
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r10, -192(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rdx, -176(%rbp)
	call	nghttp2_mem_free@PLT
	movq	-176(%rbp), %rdx
	movq	24(%rbx), %rax
	movq	$-1, %rcx
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	subq	$1, %rdx
	addq	$1, %rax
	movq	%r9, (%rbx)
	movq	%rdx, 8(%rbx)
.L594:
	movq	%rcx, 16(%rbx)
	movq	%r10, (%r9,%rdx,8)
	movq	%rax, 24(%rbx)
	movl	56(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 56(%rbx)
	movl	%eax, 72(%r10)
	movq	-160(%rbp), %rax
	movl	%r12d, 76(%r10)
	leaq	(%rbx,%rax,8), %rax
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L719
	movq	%rdx, 64(%r10)
.L719:
	movq	%r10, 64(%rax)
	movq	-168(%rbp), %rax
	addq	%rax, 40(%rbx)
	movq	-104(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	-112(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	cmpq	$-1, -136(%rbp)
	je	.L601
	.p2align 4,,10
	.p2align 3
.L602:
	movl	-120(%rbp), %r8d
	movq	-136(%rbp), %rsi
	leaq	-16(%r15), %rdx
	movq	%r15, %rcx
	movq	-152(%rbp), %rdi
	call	emit_indname_block.isra.0
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	je	.L603
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L729:
	movq	64(%r10), %r10
	testq	%r10, %r10
	jne	.L550
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L726:
	movl	%ebx, %r8d
	movq	%r14, %r10
	movq	%r15, %rbx
	movl	-176(%rbp), %r12d
	movq	%r13, %r15
	movq	-136(%rbp), %r14
	movq	-168(%rbp), %r13
	cmpl	$60, %r8d
	ja	.L622
.L634:
	movslq	%r8d, %rax
	leaq	static_table(%rip), %rsi
	movl	%r12d, -168(%rbp)
	movl	%r8d, %r13d
	movq	%rax, -136(%rbp)
	salq	$7, %rax
	movl	%r8d, %r12d
	leaq	56(%rsi,%rax), %r14
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L557:
	addl	$1, %r13d
	subq	$-128, %r14
	cmpl	$61, %r13d
	je	.L706
.L556:
	cmpl	64(%r14), %r12d
	jne	.L706
	movq	(%r15), %rdx
	cmpq	%rdx, 8(%r14)
	jne	.L557
	movq	-16(%r15), %rsi
	movq	(%r14), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L557
	movslq	%r13d, %r13
	addq	$1, %r13
.L558:
	orl	$-128, %r13d
	movl	$1, %r8d
	movb	%r13b, -80(%rbp)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L733:
	movq	-112(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L587:
	movq	-104(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	-112(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	cmpq	$-1, -136(%rbp)
	je	.L601
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L732:
	movq	16(%rax), %rdi
	movq	-168(%rbp), %rsi
	movq	%r14, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L543
	movq	8(%r12), %rax
	testq	%r15, %r15
	movq	0(%r13), %rdx
	cmove	%r12, %r15
	cmpq	%rdx, 24(%rax)
	jne	.L543
	movq	-16(%r13), %rsi
	movq	16(%rax), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L543
	movq	-136(%rbp), %rbx
	movq	%r13, %r15
	movq	%r12, %rcx
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L574:
	movq	%rax, %rdi
	leaq	static_table(%rip), %rax
	salq	$7, %rdi
	addq	%rax, %rdi
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L723:
	movl	$4294967295, %eax
	movq	1096(%rdi), %rdx
	movb	$0, 1104(%rdi)
	movq	%rax, 1096(%rdi)
	movq	48(%rdi), %rax
	cmpq	%rdx, %rax
	jbe	.L512
	cmpq	$30, %rdx
	jbe	.L513
	leaq	-31(%rdx), %rax
	cmpq	$127, %rax
	jbe	.L514
	movq	%rax, %rcx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L515:
	shrq	$7, %rcx
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	$127, %rcx
	ja	.L515
	leaq	2(%rsi), %r8
	cmpq	$16, %r8
	ja	.L597
	movb	$63, -80(%rbp)
	leaq	-79(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L518:
	movl	%eax, %ecx
	addq	$1, %rdx
	shrq	$7, %rax
	orl	$-128, %ecx
	movb	%cl, -1(%rdx)
	cmpq	$127, %rax
	ja	.L518
.L632:
	movb	%al, (%rdx)
.L621:
	movq	-152(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r8, %rdx
	call	nghttp2_bufs_add@PLT
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	jne	.L517
	movq	48(%rbx), %rax
.L512:
	cmpq	$30, %rax
	jbe	.L520
	subq	$31, %rax
	cmpq	$127, %rax
	jbe	.L521
	movq	%rax, %rcx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L522:
	shrq	$7, %rcx
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	$127, %rcx
	ja	.L522
	leaq	2(%rsi), %r8
	cmpq	$16, %r8
	ja	.L597
	movb	$63, -80(%rbp)
	leaq	-79(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L524:
	movl	%eax, %ecx
	addq	$1, %rdx
	shrq	$7, %rax
	orl	$-128, %ecx
	movb	%cl, -1(%rdx)
	cmpq	$127, %rax
	ja	.L524
.L618:
	movb	%al, (%rdx)
.L610:
	movq	-152(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r8, %rdx
	call	nghttp2_bufs_add@PLT
	movl	%eax, -120(%rbp)
	testl	%eax, %eax
	je	.L511
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L589:
	movq	16(%rbx), %rsi
	movq	(%rbx), %r9
	leaq	-1(%rsi), %rcx
	andq	%rcx, %rdx
	jmp	.L594
.L592:
	movq	8(%r10), %rdi
	movq	%r10, -120(%rbp)
	call	nghttp2_rcbuf_decref@PLT
	movq	-120(%rbp), %r10
	movq	(%r10), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	-120(%rbp), %r10
	movq	%r13, %rdi
	movq	%r10, %rsi
	call	nghttp2_mem_free@PLT
	movq	-104(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	-112(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
.L597:
	movl	$-523, -120(%rbp)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L706:
	movl	%r12d, %r8d
	movl	-168(%rbp), %r12d
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L639:
	movl	$69, %eax
	movl	$-2128831035, %r12d
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L513:
	movl	%edx, %eax
	movl	$1, %r8d
	orl	$32, %eax
	movb	%al, -80(%rbp)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L626:
	movslq	%r8d, %rax
	movl	$2, -120(%rbp)
	movq	%rax, -136(%rbp)
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L564:
	movb	$-1, -80(%rbp)
	movl	$2, %r8d
	leaq	-79(%rbp), %rdx
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L520:
	orl	$32, %eax
	movl	$1, %r8d
	movb	%al, -80(%rbp)
	jmp	.L610
.L728:
	cmpl	$-1, %r8d
	jne	.L550
	.p2align 4,,10
	.p2align 3
.L554:
	cmpl	$-1, 16(%r10)
	jne	.L552
	cmpl	%r12d, 76(%r10)
	jne	.L552
	movq	(%r10), %rax
	cmpq	24(%rax), %r14
	je	.L735
	.p2align 4,,10
	.p2align 3
.L552:
	movq	64(%r10), %r10
	testq	%r10, %r10
	jne	.L554
	jmp	.L722
.L735:
	movq	16(%rax), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	%r8d, -120(%rbp)
	movq	%r10, -136(%rbp)
	call	memcmp@PLT
	movl	-120(%rbp), %r8d
	movq	-136(%rbp), %r10
	testl	%eax, %eax
	je	.L643
	jmp	.L552
.L644:
	movl	$8, %esi
	jmp	.L590
.L521:
	movb	$63, -80(%rbp)
	movl	$2, %r8d
	leaq	-79(%rbp), %rdx
	jmp	.L618
.L514:
	movb	$63, -80(%rbp)
	movl	$2, %r8d
	leaq	-79(%rbp), %rdx
	jmp	.L632
.L638:
	movl	$-523, -120(%rbp)
	jmp	.L509
.L588:
	movq	-104(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	-112(%rbp), %rdi
	call	nghttp2_rcbuf_decref@PLT
	jmp	.L597
.L727:
	call	__stack_chk_fail@PLT
.L731:
	call	hd_ringbuf_get.part.0
.L730:
	leaq	__PRETTY_FUNCTION__.5241(%rip), %rcx
	movl	$1296, %edx
	leaq	.LC51(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	call	__assert_fail@PLT
.L725:
	cmpl	$60, %r8d
	ja	.L561
	jmp	.L634
.L582:
	addq	$64, %rax
	jmp	.L585
	.cfi_endproc
.LFE101:
	.size	nghttp2_hd_deflate_hd_bufs, .-nghttp2_hd_deflate_hd_bufs
	.p2align 4
	.globl	nghttp2_hd_deflate_hd
	.type	nghttp2_hd_deflate_hd, @function
nghttp2_hd_deflate_hd:
.LFB102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	32(%rdi), %rcx
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	nghttp2_bufs_wrap_init@PLT
	testl	%eax, %eax
	je	.L737
	cltq
.L736:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L743
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rsi
	call	nghttp2_hd_deflate_hd_bufs
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	nghttp2_bufs_len@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	nghttp2_bufs_wrap_free@PLT
	cmpl	$-502, %r12d
	je	.L741
	movslq	%r12d, %rax
	testl	%r12d, %r12d
	cmove	%r13, %rax
	jmp	.L736
.L741:
	movq	$-525, %rax
	jmp	.L736
.L743:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE102:
	.size	nghttp2_hd_deflate_hd, .-nghttp2_hd_deflate_hd
	.p2align 4
	.globl	nghttp2_hd_deflate_hd_vec
	.type	nghttp2_hd_deflate_hd_vec, @function
nghttp2_hd_deflate_hd_vec:
.LFB103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	32(%rdi), %rcx
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	nghttp2_bufs_wrap_init2@PLT
	testl	%eax, %eax
	je	.L745
	cltq
.L744:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L751
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rsi
	call	nghttp2_hd_deflate_hd_bufs
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	nghttp2_bufs_len@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	nghttp2_bufs_wrap_free@PLT
	cmpl	$-502, %r12d
	je	.L749
	movslq	%r12d, %rax
	testl	%r12d, %r12d
	cmove	%r13, %rax
	jmp	.L744
.L749:
	movq	$-525, %rax
	jmp	.L744
.L751:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE103:
	.size	nghttp2_hd_deflate_hd_vec, .-nghttp2_hd_deflate_hd_vec
	.p2align 4
	.globl	nghttp2_hd_deflate_bound
	.type	nghttp2_hd_deflate_bound, @function
nghttp2_hd_deflate_bound:
.LFB104:
	.cfi_startproc
	endbr64
	leaq	3(%rdx,%rdx,2), %r8
	salq	$2, %r8
	testq	%rdx, %rdx
	je	.L752
	addq	$16, %rsi
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rsi,%rax,8), %rdx
	.p2align 4,,10
	.p2align 3
.L754:
	movq	8(%rsi), %rax
	addq	$40, %rsi
	addq	-40(%rsi), %rax
	addq	%rax, %r8
	cmpq	%rsi, %rdx
	jne	.L754
.L752:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE104:
	.size	nghttp2_hd_deflate_bound, .-nghttp2_hd_deflate_bound
	.p2align 4
	.globl	nghttp2_hd_deflate_new
	.type	nghttp2_hd_deflate_new, @function
nghttp2_hd_deflate_new:
.LFB105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	nghttp2_mem_default@PLT
	movl	$1112, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L763
	movq	%r14, 32(%rax)
	movl	$1024, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	movb	$0, 60(%rax)
	movq	$4096, 48(%rax)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L766
	leaq	72(%r12), %rdi
	movl	%r12d, %eax
	movq	$127, 8(%r12)
	movq	$0, 16(%r12)
	andq	$-8, %rdi
	movq	$0, 24(%r12)
	subl	%edi, %eax
	movq	$0, 40(%r12)
	leal	1088(%rax), %ecx
	xorl	%eax, %eax
	movl	$0, 56(%r12)
	shrl	$3, %ecx
	movq	$0, 64(%r12)
	movq	$0, 1080(%r12)
	rep stosq
	cmpq	$4095, %rbx
	jbe	.L767
.L762:
	movb	%al, 1104(%r12)
	movl	$4294967295, %eax
	movq	%rax, 1096(%r12)
	xorl	%eax, %eax
	movq	%rbx, 1088(%r12)
	movq	%r12, 0(%r13)
.L759:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	movq	%rbx, 48(%r12)
	movl	$1, %eax
	jmp	.L762
.L766:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	popq	%rbx
	movl	$-901, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L763:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L759
	.cfi_endproc
.LFE105:
	.size	nghttp2_hd_deflate_new, .-nghttp2_hd_deflate_new
	.p2align 4
	.globl	nghttp2_hd_deflate_new2
	.type	nghttp2_hd_deflate_new2, @function
nghttp2_hd_deflate_new2:
.LFB106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	testq	%rdx, %rdx
	je	.L777
.L769:
	movl	$1112, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L773
	movq	%r14, 32(%rax)
	movl	$1024, %esi
	movq	%r14, %rdi
	movb	$0, 60(%rax)
	movq	$4096, 48(%rax)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L778
	leaq	72(%r12), %rdi
	movl	%r12d, %eax
	movq	$127, 8(%r12)
	movq	$0, 16(%r12)
	andq	$-8, %rdi
	movq	$0, 24(%r12)
	subl	%edi, %eax
	movq	$0, 40(%r12)
	leal	1088(%rax), %ecx
	xorl	%eax, %eax
	movl	$0, 56(%r12)
	shrl	$3, %ecx
	movq	$0, 64(%r12)
	movq	$0, 1080(%r12)
	rep stosq
	cmpq	$4095, %rbx
	jbe	.L779
.L772:
	movb	%al, 1104(%r12)
	movl	$4294967295, %eax
	movq	%rax, 1096(%r12)
	xorl	%eax, %eax
	movq	%rbx, 1088(%r12)
	movq	%r12, 0(%r13)
.L768:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	movq	%rbx, 48(%r12)
	movl	$1, %eax
	jmp	.L772
.L778:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	popq	%rbx
	movl	$-901, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	call	nghttp2_mem_default@PLT
	movq	%rax, %r14
	jmp	.L769
.L773:
	movl	$-901, %eax
	jmp	.L768
	.cfi_endproc
.LFE106:
	.size	nghttp2_hd_deflate_new2, .-nghttp2_hd_deflate_new2
	.p2align 4
	.globl	nghttp2_hd_deflate_del
	.type	nghttp2_hd_deflate_del, @function
nghttp2_hd_deflate_del:
.LFB107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$0, 24(%rdi)
	movq	32(%rdi), %r13
	je	.L781
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L782:
	movq	16(%r12), %rax
	movq	(%r12), %rdx
	addq	%rbx, %rax
	andq	8(%r12), %rax
	addq	$1, %rbx
	movq	(%rdx,%rax,8), %r14
	movq	8(%r14), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	(%r14), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	cmpq	24(%r12), %rbx
	jb	.L782
.L781:
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	popq	%rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE107:
	.size	nghttp2_hd_deflate_del, .-nghttp2_hd_deflate_del
	.p2align 4
	.globl	nghttp2_hd_inflate_hd_nv
	.type	nghttp2_hd_inflate_hd_nv, @function
nghttp2_hd_inflate_hd_nv:
.LFB117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rcx,%r8), %rbx
	subq	$72, %rsp
	movq	%rsi, -88(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%r9d, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movl	$0, -60(%rbp)
	movq	%rax, -72(%rbp)
	movzbl	60(%rdi), %eax
	movb	%al, -93(%rbp)
	testb	%al, %al
	jne	.L833
	movq	%rdi, %r15
	movq	176(%rdi), %rdi
	movq	%rdx, %r14
	movq	%rcx, %r13
	leaq	.L790(%rip), %r12
	call	nghttp2_rcbuf_decref@PLT
	movq	168(%r15), %rdi
	call	nghttp2_rcbuf_decref@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movups	%xmm0, 168(%r15)
	movl	$0, (%r14)
.L787:
	cmpq	%rbx, %r13
	setne	%cl
.L788:
	testb	$1, %al
	jne	.L830
	testb	%cl, %cl
	je	.L877
.L830:
	movl	228(%r15), %edx
	xorl	%eax, %eax
	cmpl	$12, %edx
	ja	.L788
	movl	%edx, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L790:
	.long	.L801-.L790
	.long	.L800-.L790
	.long	.L800-.L790
	.long	.L799-.L790
	.long	.L798-.L790
	.long	.L797-.L790
	.long	.L796-.L790
	.long	.L795-.L790
	.long	.L794-.L790
	.long	.L793-.L790
	.long	.L792-.L790
	.long	.L791-.L790
	.long	.L789-.L790
	.text
	.p2align 4,,10
	.p2align 3
.L793:
	movzbl	0(%r13), %eax
	movl	$10, 228(%r15)
	movq	$0, 184(%r15)
	movq	$0, 216(%r15)
	shrb	$7, %al
	movb	%al, 232(%r15)
.L792:
	leaq	-60(%rbp), %rsi
	movl	$65536, %r9d
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	$7, %r8d
	movq	%r15, %rdi
	movl	$0, -60(%rbp)
	call	hd_inflate_read_len.constprop.0
	movq	%rax, %r14
	testq	%rax, %rax
	js	.L809
	movl	-60(%rbp), %ecx
	addq	%rax, %r13
	testl	%ecx, %ecx
	je	.L810
	cmpb	$0, 232(%r15)
	leaq	160(%r15), %r14
	jne	.L878
	movq	184(%r15), %rax
	movq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movl	$12, 228(%r15)
	leaq	1(%rax), %rsi
	call	nghttp2_rcbuf_new@PLT
	movslq	%eax, %r14
.L824:
	testq	%r14, %r14
	jne	.L809
	movq	160(%r15), %rax
	leaq	112(%r15), %rdi
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	call	nghttp2_buf_wrap_init@PLT
	movl	$1, %eax
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L797:
	movzbl	0(%r13), %eax
	movl	$6, 228(%r15)
	movq	$0, 184(%r15)
	movq	$0, 216(%r15)
	shrb	$7, %al
	movb	%al, 232(%r15)
.L796:
	leaq	-60(%rbp), %rsi
	movl	$65536, %r9d
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	$7, %r8d
	movq	%r15, %rdi
	movl	$0, -60(%rbp)
	call	hd_inflate_read_len.constprop.0
	movq	%rax, %r14
	testq	%rax, %rax
	js	.L809
	movl	-60(%rbp), %esi
	addq	%rax, %r13
	testl	%esi, %esi
	je	.L810
	cmpb	$0, 232(%r15)
	leaq	152(%r15), %r14
	jne	.L879
	movq	184(%r15), %rax
	movq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movl	$8, 228(%r15)
	leaq	1(%rax), %rsi
	call	nghttp2_rcbuf_new@PLT
	movslq	%eax, %r14
.L821:
	testq	%r14, %r14
	jne	.L809
	movq	152(%r15), %rax
	leaq	72(%r15), %rdi
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	call	nghttp2_buf_wrap_init@PLT
	xorl	%eax, %eax
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L791:
	movq	184(%r15), %rcx
	subq	%r13, %rbx
	leaq	112(%r15), %rsi
	movl	$1, %r8d
	cmpq	%rcx, %rbx
	jnb	.L825
	movq	%rbx, %rcx
	xorl	%r8d, %r8d
.L825:
	leaq	64(%r15), %r12
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	nghttp2_hd_huff_decode@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	js	.L809
	movq	%r12, %rdi
	call	nghttp2_hd_huff_decode_failure_state@PLT
	testl	%eax, %eax
	jne	.L802
	movq	184(%r15), %rax
	addq	%r14, %r13
	subq	%r14, %rax
	movq	%rax, 184(%r15)
	testq	%rax, %rax
	je	.L880
.L810:
	movl	-92(%rbp), %eax
	testl	%eax, %eax
	je	.L872
.L802:
	movq	$-523, %r14
	.p2align 4,,10
	.p2align 3
.L809:
	movb	$1, 60(%r15)
.L785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L881
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	.cfi_restore_state
	movzbl	0(%r13), %eax
	movl	%eax, %ecx
	andl	$-32, %ecx
	cmpb	$32, %cl
	je	.L882
	testb	%al, %al
	js	.L883
	testb	$-17, %al
	je	.L807
	cmpb	$64, %al
	jne	.L884
.L807:
	movabsq	$21474836482, %rax
	movq	%rax, 224(%r15)
	movzbl	0(%r13), %eax
	shrb	$6, %al
	andl	$1, %eax
	movb	%al, 233(%r15)
	movzbl	0(%r13), %eax
	andl	$-16, %eax
	cmpb	$16, %al
	sete	234(%r15)
	addq	$1, %r13
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L801:
	movzbl	0(%r13), %eax
	andl	$-32, %eax
	cmpb	$32, %al
	jne	.L802
.L803:
	movabsq	$12884901889, %rax
	movq	%rax, 224(%r15)
.L805:
	movq	$0, 184(%r15)
	xorl	%eax, %eax
	movq	$0, 216(%r15)
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L789:
	subq	%r13, %rbx
	cmpq	%rbx, 184(%r15)
	movq	136(%r15), %rdi
	movq	%r13, %rsi
	cmovbe	184(%r15), %rbx
	movq	%rbx, %rdx
	movq	%rbx, %r14
	call	nghttp2_cpymem@PLT
	movq	184(%r15), %rdx
	movq	%rax, 136(%r15)
	subq	%rbx, %rdx
	movq	%rdx, 184(%r15)
	testq	%rbx, %rbx
	js	.L809
	addq	%rbx, %r13
	testq	%rdx, %rdx
	jne	.L810
.L876:
	movb	$0, (%rax)
	movq	160(%r15), %rdx
	movq	%r15, %rdi
	movq	136(%r15), %rax
	subq	128(%r15), %rax
	cmpl	$2, 224(%r15)
	movq	-88(%rbp), %rsi
	movq	%rax, 24(%rdx)
	je	.L885
	call	hd_inflate_commit_indname
	movslq	%eax, %r14
.L829:
	testq	%r14, %r14
	jne	.L809
.L871:
	movq	-104(%rbp), %rax
	movq	%r13, %r14
	subq	-80(%rbp), %r14
	movl	$2, 228(%r15)
	orl	$2, (%rax)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L795:
	movq	%rbx, %rax
	leaq	72(%r15), %rsi
	movq	184(%r15), %rcx
	movl	$1, %r8d
	subq	%r13, %rax
	cmpq	%rcx, %rax
	jb	.L886
.L822:
	leaq	64(%r15), %rdi
	movq	%r13, %rdx
	movq	%rdi, -112(%rbp)
	call	nghttp2_hd_huff_decode@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	js	.L809
	movq	-112(%rbp), %rdi
	call	nghttp2_hd_huff_decode_failure_state@PLT
	testl	%eax, %eax
	jne	.L802
	movq	184(%r15), %rdx
	addq	%r14, %r13
	subq	%r14, %rdx
	movq	%rdx, 184(%r15)
	testq	%rdx, %rdx
	jne	.L810
	movq	96(%r15), %rdx
	movb	$0, (%rdx)
	movq	152(%r15), %rcx
	movq	96(%r15), %rdx
	subq	88(%r15), %rdx
	movq	%rdx, 24(%rcx)
	movl	$9, 228(%r15)
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L798:
	cmpl	$1, 224(%r15)
	movl	$7, %r8d
	je	.L814
	cmpb	$1, 233(%r15)
	sbbq	%r8, %r8
	andq	$-2, %r8
	addq	$6, %r8
.L814:
	movq	24(%r15), %rax
	leaq	-60(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movl	$0, -60(%rbp)
	leaq	61(%rax), %r9
	call	hd_inflate_read_len.constprop.0
	movq	%rax, %r14
	testq	%rax, %rax
	js	.L809
	movl	-60(%rbp), %edi
	addq	%rax, %r13
	testl	%edi, %edi
	je	.L810
	movq	184(%r15), %rax
	testq	%rax, %rax
	je	.L802
	leaq	-1(%rax), %rdx
	cmpl	$1, 224(%r15)
	movq	%rdx, 192(%r15)
	je	.L887
.L869:
	movl	$9, 228(%r15)
	xorl	%eax, %eax
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L799:
	movq	208(%r15), %r9
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	cmpq	%r9, 200(%r15)
	cmovbe	200(%r15), %r9
	leaq	-60(%rbp), %rsi
	movl	$5, %r8d
	movl	$0, -60(%rbp)
	call	hd_inflate_read_len.constprop.0
	movq	%rax, %r14
	testq	%rax, %rax
	js	.L809
	movl	-60(%rbp), %r8d
	addq	%rax, %r13
	testl	%r8d, %r8d
	je	.L810
	movl	$4294967295, %eax
	movq	184(%r15), %rdx
	movq	%rax, 208(%r15)
	movq	32(%r15), %rax
	movq	%rdx, 48(%r15)
	movq	%rax, %rsi
	movq	40(%r15), %rax
	cmpq	%rax, %rdx
	jnb	.L812
	movq	%r13, -112(%rbp)
	movq	%rax, %rdi
	movq	%rsi, %r14
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L813:
	movq	16(%r15), %rcx
	subq	$1, %rdx
	movq	(%r15), %rax
	addq	%rdx, %rcx
	andq	8(%r15), %rcx
	movq	(%rax,%rcx,8), %r13
	movq	$-32, %rax
	movq	8(%r13), %r8
	movq	0(%r13), %rcx
	subq	24(%r8), %rax
	subq	24(%rcx), %rax
	movq	%rdx, 24(%r15)
	addq	%rdi, %rax
	movq	%r8, %rdi
	movq	%rax, 40(%r15)
	call	nghttp2_rcbuf_decref@PLT
	movq	0(%r13), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	nghttp2_mem_free@PLT
	movq	40(%r15), %rdi
	cmpq	48(%r15), %rdi
	jbe	.L853
.L811:
	movq	24(%r15), %rdx
	testq	%rdx, %rdx
	jne	.L813
.L853:
	movq	-112(%rbp), %r13
.L812:
	movl	$1, 228(%r15)
	xorl	%eax, %eax
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L794:
	movq	%rbx, %rdx
	movq	96(%r15), %rdi
	movq	%r13, %rsi
	subq	%r13, %rdx
	cmpq	%rdx, 184(%r15)
	cmovbe	184(%r15), %rdx
	movq	%rdx, -112(%rbp)
	call	nghttp2_cpymem@PLT
	movq	184(%r15), %rcx
	movq	-112(%rbp), %rdx
	movq	%rax, 96(%r15)
	subq	%rdx, %rcx
	movq	%rdx, %r14
	movq	%rcx, 184(%r15)
	testq	%rdx, %rdx
	js	.L809
	addq	%rdx, %r13
	testq	%rcx, %rcx
	jne	.L810
	movb	$0, (%rax)
	movq	152(%r15), %rdx
	movq	96(%r15), %rax
	subq	88(%r15), %rax
	movq	%rax, 24(%rdx)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L879:
	leaq	64(%r15), %rdi
	call	nghttp2_hd_huff_decode_context_init@PLT
	movq	184(%r15), %rax
	movq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movl	$7, 228(%r15)
	leaq	1(%rax,%rax), %rsi
	call	nghttp2_rcbuf_new@PLT
	movslq	%eax, %r14
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L878:
	leaq	64(%r15), %rdi
	call	nghttp2_hd_huff_decode_context_init@PLT
	movq	184(%r15), %rax
	movq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movl	$11, 228(%r15)
	leaq	1(%rax,%rax), %rsi
	call	nghttp2_rcbuf_new@PLT
	movslq	%eax, %r14
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L877:
	movl	-92(%rbp), %edx
	testl	%edx, %edx
	je	.L872
	movl	228(%r15), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L802
	movq	-104(%rbp), %rax
	orl	$1, (%rax)
	.p2align 4,,10
	.p2align 3
.L872:
	movq	%r13, %r14
	subq	-80(%rbp), %r14
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L886:
	movq	%rax, %rcx
	xorl	%r8d, %r8d
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L880:
	movq	136(%r15), %rax
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L883:
	movabsq	$17179869185, %rax
	movq	%rax, 224(%r15)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L884:
	movabsq	$17179869187, %rax
	movq	%rax, 224(%r15)
	movzbl	0(%r13), %eax
	shrb	$6, %al
	andl	$1, %eax
	movb	%al, 233(%r15)
	movzbl	0(%r13), %eax
	andl	$-16, %eax
	cmpb	$16, %al
	sete	234(%r15)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L882:
	cmpl	$2, %edx
	jne	.L803
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L885:
	call	hd_inflate_commit_newname
	movslq	%eax, %r14
	jmp	.L829
.L833:
	movq	$-523, %r14
	jmp	.L785
.L887:
	movq	24(%r15), %rcx
	leaq	61(%rcx), %rsi
	cmpq	%rsi, %rdx
	jnb	.L888
	cmpq	$60, %rdx
	jbe	.L817
	subq	$62, %rax
	cmpq	%rax, %rcx
	jbe	.L889
	movq	(%r15), %rdx
	addq	16(%r15), %rax
	andq	8(%r15), %rax
	movq	(%rdx,%rax,8), %rdx
	movzbl	20(%rdx), %ebx
	movq	(%rdx), %rax
	movq	8(%rdx), %rsi
	movl	16(%rdx), %ecx
	movb	%bl, -93(%rbp)
.L819:
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	-88(%rbp), %rax
	movzbl	-93(%rbp), %esi
	punpcklqdq	%xmm1, %xmm0
	movl	%ecx, 16(%rax)
	movb	%sil, 20(%rax)
	movups	%xmm0, (%rax)
	jmp	.L871
.L817:
	leaq	static_table(%rip), %rcx
	salq	$7, %rdx
	leaq	(%rdx,%rcx), %rax
	leaq	40(%rcx,%rdx), %rsi
	movl	120(%rax), %ecx
	jmp	.L819
.L881:
	call	__stack_chk_fail@PLT
.L888:
	leaq	__PRETTY_FUNCTION__.5241(%rip), %rcx
	movl	$1296, %edx
	leaq	.LC51(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	call	__assert_fail@PLT
.L889:
	call	hd_ringbuf_get.part.0
	.cfi_endproc
.LFE117:
	.size	nghttp2_hd_inflate_hd_nv, .-nghttp2_hd_inflate_hd_nv
	.p2align 4
	.globl	nghttp2_hd_inflate_hd2
	.type	nghttp2_hd_inflate_hd2, @function
nghttp2_hd_inflate_hd2:
.LFB116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	leaq	-48(%rbp), %rsi
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	nghttp2_hd_inflate_hd_nv
	testq	%rax, %rax
	js	.L890
	testb	$2, (%rbx)
	jne	.L897
.L890:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L898
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L897:
	.cfi_restore_state
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	24(%rcx), %xmm0
	movq	16(%rcx), %xmm1
	movhps	24(%rdx), %xmm0
	movhps	16(%rdx), %xmm1
	movzbl	-28(%rbp), %edx
	movups	%xmm1, (%r12)
	movb	%dl, 32(%r12)
	movups	%xmm0, 16(%r12)
	jmp	.L890
.L898:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE116:
	.size	nghttp2_hd_inflate_hd2, .-nghttp2_hd_inflate_hd2
	.p2align 4
	.globl	nghttp2_hd_inflate_hd
	.type	nghttp2_hd_inflate_hd, @function
nghttp2_hd_inflate_hd:
.LFB115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	leaq	-48(%rbp), %rsi
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	nghttp2_hd_inflate_hd_nv
	testq	%rax, %rax
	js	.L899
	testb	$2, (%rbx)
	jne	.L906
.L899:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L907
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	24(%rcx), %xmm0
	movq	16(%rcx), %xmm1
	movhps	24(%rdx), %xmm0
	movhps	16(%rdx), %xmm1
	movzbl	-28(%rbp), %edx
	movups	%xmm1, (%r12)
	movb	%dl, 32(%r12)
	movups	%xmm0, 16(%r12)
	jmp	.L899
.L907:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE115:
	.size	nghttp2_hd_inflate_hd, .-nghttp2_hd_inflate_hd
	.p2align 4
	.globl	nghttp2_hd_inflate_end_headers
	.type	nghttp2_hd_inflate_end_headers, @function
nghttp2_hd_inflate_end_headers:
.LFB118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	176(%rdi), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	168(%rbx), %rdi
	call	nghttp2_rcbuf_decref@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$1, 228(%rbx)
	movups	%xmm0, 168(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE118:
	.size	nghttp2_hd_inflate_end_headers, .-nghttp2_hd_inflate_end_headers
	.p2align 4
	.globl	nghttp2_hd_inflate_new
	.type	nghttp2_hd_inflate_new, @function
nghttp2_hd_inflate_new:
.LFB119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	nghttp2_mem_default@PLT
	movl	$240, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L913
	movq	%r13, 32(%rax)
	movl	$1024, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	movb	$0, 60(%rax)
	movq	$4096, 48(%rax)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L915
	movabsq	$4294967296, %rax
	leaq	72(%r12), %rdi
	movdqa	.LC56(%rip), %xmm0
	movq	$127, 8(%r12)
	movq	%rax, 224(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 40(%r12)
	movl	$0, 56(%r12)
	movups	%xmm0, 200(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 168(%r12)
	call	nghttp2_buf_init@PLT
	leaq	112(%r12), %rdi
	call	nghttp2_buf_init@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	$0, 216(%r12)
	movw	%ax, 232(%r12)
	xorl	%eax, %eax
	movb	$0, 234(%r12)
	movups	%xmm0, 152(%r12)
	movups	%xmm0, 184(%r12)
	movq	%r12, (%rbx)
.L910:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L915:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	addq	$8, %rsp
	movl	$-901, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L913:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L910
	.cfi_endproc
.LFE119:
	.size	nghttp2_hd_inflate_new, .-nghttp2_hd_inflate_new
	.p2align 4
	.globl	nghttp2_hd_inflate_new2
	.type	nghttp2_hd_inflate_new2, @function
nghttp2_hd_inflate_new2:
.LFB120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L923
.L917:
	movl	$240, %esi
	movq	%r13, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L920
	movq	%r13, 32(%rax)
	movl	$1024, %esi
	movq	%r13, %rdi
	movb	$0, 60(%rax)
	movq	$4096, 48(%rax)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L924
	movabsq	$4294967296, %rax
	leaq	72(%r12), %rdi
	movdqa	.LC56(%rip), %xmm0
	movq	$127, 8(%r12)
	movq	%rax, 224(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 40(%r12)
	movl	$0, 56(%r12)
	movups	%xmm0, 200(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 168(%r12)
	call	nghttp2_buf_init@PLT
	leaq	112(%r12), %rdi
	call	nghttp2_buf_init@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	$0, 216(%r12)
	movw	%ax, 232(%r12)
	xorl	%eax, %eax
	movb	$0, 234(%r12)
	movups	%xmm0, 152(%r12)
	movups	%xmm0, 184(%r12)
	movq	%r12, (%rbx)
.L916:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L924:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	addq	$8, %rsp
	movl	$-901, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	call	nghttp2_mem_default@PLT
	movq	%rax, %r13
	jmp	.L917
.L920:
	movl	$-901, %eax
	jmp	.L916
	.cfi_endproc
.LFE120:
	.size	nghttp2_hd_inflate_new2, .-nghttp2_hd_inflate_new2
	.p2align 4
	.globl	nghttp2_hd_inflate_del
	.type	nghttp2_hd_inflate_del, @function
nghttp2_hd_inflate_del:
.LFB121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %r14
	movq	176(%rdi), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	168(%r12), %rdi
	call	nghttp2_rcbuf_decref@PLT
	pxor	%xmm0, %xmm0
	movq	160(%r12), %rdi
	movups	%xmm0, 168(%r12)
	call	nghttp2_rcbuf_decref@PLT
	movq	152(%r12), %rdi
	call	nghttp2_rcbuf_decref@PLT
	cmpq	$0, 24(%r12)
	movq	32(%r12), %r15
	je	.L926
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L927:
	movq	16(%r12), %rax
	movq	(%r12), %rdx
	addq	%rbx, %rax
	andq	8(%r12), %rax
	addq	$1, %rbx
	movq	(%rdx,%rax,8), %r13
	movq	8(%r13), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	0(%r13), %rdi
	call	nghttp2_rcbuf_decref@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	cmpq	24(%r12), %rbx
	jb	.L927
.L926:
	movq	(%r12), %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE121:
	.size	nghttp2_hd_inflate_del, .-nghttp2_hd_inflate_del
	.p2align 4
	.globl	nghttp2_hd_emit_indname_block
	.type	nghttp2_hd_emit_indname_block, @function
nghttp2_hd_emit_indname_block:
.LFB122:
	.cfi_startproc
	endbr64
	movl	%ecx, %r8d
	leaq	24(%rdx), %rcx
	addq	$8, %rdx
	jmp	emit_indname_block.isra.0
	.cfi_endproc
.LFE122:
	.size	nghttp2_hd_emit_indname_block, .-nghttp2_hd_emit_indname_block
	.p2align 4
	.globl	nghttp2_hd_emit_newname_block
	.type	nghttp2_hd_emit_newname_block, @function
nghttp2_hd_emit_newname_block:
.LFB123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$2, %edx
	ja	.L932
	movl	%edx, %edx
	leaq	CSWTCH.61(%rip), %rax
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movzbl	(%rax,%rdx), %esi
	call	nghttp2_bufs_addb@PLT
	testl	%eax, %eax
	je	.L937
.L931:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_restore_state
	movq	16(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	emit_string
	testl	%eax, %eax
	jne	.L931
	movq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	emit_string
.L932:
	.cfi_restore_state
	call	pack_first_byte.part.0
	.cfi_endproc
.LFE123:
	.size	nghttp2_hd_emit_newname_block, .-nghttp2_hd_emit_newname_block
	.p2align 4
	.globl	nghttp2_hd_emit_table_size
	.type	nghttp2_hd_emit_table_size, @function
nghttp2_hd_emit_table_size:
.LFB124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	$30, %rsi
	jbe	.L939
	subq	$31, %rsi
	cmpq	$127, %rsi
	jbe	.L940
	movq	%rsi, %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L941:
	shrq	$7, %rcx
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	$127, %rcx
	ja	.L941
	addq	$2, %rdx
	cmpq	$16, %rdx
	ja	.L955
	movb	$63, -32(%rbp)
	leaq	-31(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L943:
	movl	%esi, %ecx
	addq	$1, %rax
	shrq	$7, %rsi
	orl	$-128, %ecx
	movb	%cl, -1(%rax)
	cmpq	$127, %rsi
	ja	.L943
.L945:
	movb	%sil, (%rax)
.L944:
	leaq	-32(%rbp), %rsi
	call	nghttp2_bufs_add@PLT
.L938:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L956
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore_state
	orl	$32, %esi
	movl	$1, %edx
	movb	%sil, -32(%rbp)
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L940:
	movb	$63, -32(%rbp)
	movl	$2, %edx
	leaq	-31(%rbp), %rax
	jmp	.L945
.L955:
	movl	$-523, %eax
	jmp	.L938
.L956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE124:
	.size	nghttp2_hd_emit_table_size, .-nghttp2_hd_emit_table_size
	.p2align 4
	.globl	nghttp2_hd_decode_length
	.type	nghttp2_hd_decode_length, @function
nghttp2_hd_decode_length:
.LFB125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	$0, (%rsi)
	movl	$0, (%rdx)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	testl	%ecx, %ecx
	jne	.L958
	movq	24(%rbp), %rcx
	movl	$1, %r10d
	movzbl	(%r9), %eax
	sall	%cl, %r10d
	subl	$1, %r10d
	andl	%r10d, %eax
	cmpb	%al, %r10b
	je	.L959
	movzbl	%al, %eax
	popq	%rbx
	popq	%r12
	movl	%eax, (%rdi)
	popq	%r13
	movl	$1, %eax
	popq	%r14
	popq	%r15
	movl	$1, (%rdx)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	.cfi_restore_state
	movl	%ecx, %r10d
	cmpq	%rbx, %r9
	je	.L970
	movq	%r9, %rax
.L962:
	movzbl	(%rax), %r13d
	movl	%r13d, %r11d
	andl	$127, %r11d
	cmpq	$31, %r8
	ja	.L992
	movl	$-1, %r12d
	movl	%r8d, %ecx
	movl	%r12d, %r15d
	shrl	%cl, %r15d
	cmpl	%r11d, %r15d
	jb	.L992
	movl	%r8d, %ecx
	sall	%cl, %r11d
	movl	%r11d, %ecx
	notl	%ecx
	cmpl	%ecx, %r10d
	ja	.L992
	addl	%r11d, %r10d
	testb	%r13b, %r13b
	jns	.L964
	leaq	1(%rax), %r11
	leaq	7(%r8), %rcx
	cmpq	%r11, %rbx
	je	.L989
	movzbl	1(%rax), %r14d
	movl	%r14d, %r13d
	andl	$127, %r13d
	cmpq	$31, %rcx
	ja	.L992
	shrl	%cl, %r12d
	cmpl	%r12d, %r13d
	ja	.L992
	sall	%cl, %r13d
	movl	%r13d, %r12d
	notl	%r12d
	cmpl	%r12d, %r10d
	ja	.L992
	addl	%r13d, %r10d
	testb	%r14b, %r14b
	jns	.L993
	leaq	2(%rax), %r11
	leaq	14(%r8), %rcx
	cmpq	%r11, %rbx
	je	.L989
	movzbl	2(%rax), %r14d
	movl	%r14d, %r12d
	andl	$127, %r12d
	cmpq	$31, %rcx
	ja	.L992
	movl	$-1, %r13d
	movl	%r13d, %r15d
	shrl	%cl, %r15d
	cmpl	%r15d, %r12d
	ja	.L992
	sall	%cl, %r12d
	movl	%r12d, %r15d
	notl	%r15d
	cmpl	%r15d, %r10d
	ja	.L992
	addl	%r12d, %r10d
	testb	%r14b, %r14b
	jns	.L993
	leaq	3(%rax), %r11
	leaq	21(%r8), %rcx
	cmpq	%r11, %rbx
	je	.L989
	movzbl	3(%rax), %r14d
	movl	%r14d, %r12d
	andl	$127, %r12d
	cmpq	$31, %rcx
	ja	.L992
	shrl	%cl, %r13d
	cmpl	%r13d, %r12d
	ja	.L992
	sall	%cl, %r12d
	movl	%r12d, %r13d
	notl	%r13d
	cmpl	%r13d, %r10d
	ja	.L992
	addl	%r12d, %r10d
	testb	%r14b, %r14b
	jns	.L993
	leaq	4(%rax), %r11
	leaq	28(%r8), %rcx
	cmpq	%r11, %rbx
	je	.L989
	movzbl	4(%rax), %r13d
	movl	%r13d, %r12d
	andl	$127, %r12d
	cmpq	$31, %rcx
	ja	.L992
	movl	$-1, %r14d
	shrl	%cl, %r14d
	cmpl	%r12d, %r14d
	jb	.L992
	sall	%cl, %r12d
	movl	%r12d, %r14d
	notl	%r14d
	cmpl	%r10d, %r14d
	jb	.L992
	addl	%r12d, %r10d
	testb	%r13b, %r13b
	jns	.L993
	leaq	5(%rax), %r11
	addq	$35, %r8
	movq	$-1, %rax
	cmpq	%r11, %rbx
	je	.L965
.L957:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	.cfi_restore_state
	leaq	1(%r9), %rax
	movzbl	%r10b, %r10d
	cmpq	%rax, %rbx
	jne	.L962
	movl	%r10d, (%rdi)
	movl	$1, %eax
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L964:
	movq	%r8, (%rsi)
	cmpq	%rax, %rbx
	je	.L997
.L968:
	addq	$1, %rax
	popq	%rbx
	popq	%r12
	movl	%r10d, (%rdi)
	popq	%r13
	subq	%r9, %rax
	popq	%r14
	movl	$1, (%rdx)
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L992:
	.cfi_restore_state
	popq	%rbx
	movq	$-1, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L989:
	.cfi_restore_state
	movq	%rcx, %r8
.L965:
	movq	%r11, %rax
	subq	%r9, %rax
.L963:
	movq	%r8, (%rsi)
.L969:
	movl	%r10d, (%rdi)
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L970:
	xorl	%eax, %eax
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L993:
	movq	%rcx, (%rsi)
	movq	%r11, %rax
	jmp	.L968
.L997:
	movq	%rbx, %rax
	subq	%r9, %rax
	jmp	.L969
	.cfi_endproc
.LFE125:
	.size	nghttp2_hd_decode_length, .-nghttp2_hd_decode_length
	.p2align 4
	.globl	nghttp2_hd_deflate_get_num_table_entries
	.type	nghttp2_hd_deflate_get_num_table_entries, @function
nghttp2_hd_deflate_get_num_table_entries:
.LFB127:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	addq	$61, %rax
	ret
	.cfi_endproc
.LFE127:
	.size	nghttp2_hd_deflate_get_num_table_entries, .-nghttp2_hd_deflate_get_num_table_entries
	.p2align 4
	.globl	nghttp2_hd_deflate_get_table_entry
	.type	nghttp2_hd_deflate_get_table_entry, @function
nghttp2_hd_deflate_get_table_entry:
.LFB128:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1004
	movq	24(%rdi), %rdx
	leaq	-1(%rsi), %rax
	leaq	61(%rdx), %rcx
	cmpq	%rcx, %rax
	jnb	.L1004
	cmpq	$60, %rax
	jbe	.L1001
	subq	$62, %rsi
	cmpq	%rsi, %rdx
	jbe	.L1008
	movq	(%rdi), %rax
	addq	16(%rdi), %rsi
	andq	8(%rdi), %rsi
	movq	(%rax,%rsi,8), %rax
	addq	$24, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	salq	$7, %rax
	leaq	static_table(%rip), %rdx
	leaq	80(%rdx,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1004:
	xorl	%eax, %eax
	ret
.L1008:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	hd_ringbuf_get.part.0
	.cfi_endproc
.LFE128:
	.size	nghttp2_hd_deflate_get_table_entry, .-nghttp2_hd_deflate_get_table_entry
	.p2align 4
	.globl	nghttp2_hd_deflate_get_dynamic_table_size
	.type	nghttp2_hd_deflate_get_dynamic_table_size, @function
nghttp2_hd_deflate_get_dynamic_table_size:
.LFB129:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE129:
	.size	nghttp2_hd_deflate_get_dynamic_table_size, .-nghttp2_hd_deflate_get_dynamic_table_size
	.p2align 4
	.globl	nghttp2_hd_deflate_get_max_dynamic_table_size
	.type	nghttp2_hd_deflate_get_max_dynamic_table_size, @function
nghttp2_hd_deflate_get_max_dynamic_table_size:
.LFB130:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE130:
	.size	nghttp2_hd_deflate_get_max_dynamic_table_size, .-nghttp2_hd_deflate_get_max_dynamic_table_size
	.p2align 4
	.globl	nghttp2_hd_inflate_get_num_table_entries
	.type	nghttp2_hd_inflate_get_num_table_entries, @function
nghttp2_hd_inflate_get_num_table_entries:
.LFB131:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	addq	$61, %rax
	ret
	.cfi_endproc
.LFE131:
	.size	nghttp2_hd_inflate_get_num_table_entries, .-nghttp2_hd_inflate_get_num_table_entries
	.p2align 4
	.globl	nghttp2_hd_inflate_get_table_entry
	.type	nghttp2_hd_inflate_get_table_entry, @function
nghttp2_hd_inflate_get_table_entry:
.LFB132:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1017
	movq	24(%rdi), %rdx
	leaq	-1(%rsi), %rax
	leaq	61(%rdx), %rcx
	cmpq	%rcx, %rax
	jnb	.L1017
	cmpq	$60, %rax
	jbe	.L1014
	subq	$62, %rsi
	cmpq	%rsi, %rdx
	jbe	.L1021
	movq	(%rdi), %rax
	addq	16(%rdi), %rsi
	andq	8(%rdi), %rsi
	movq	(%rax,%rsi,8), %rax
	addq	$24, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	salq	$7, %rax
	leaq	static_table(%rip), %rdx
	leaq	80(%rdx,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1017:
	xorl	%eax, %eax
	ret
.L1021:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	hd_ringbuf_get.part.0
	.cfi_endproc
.LFE132:
	.size	nghttp2_hd_inflate_get_table_entry, .-nghttp2_hd_inflate_get_table_entry
	.p2align 4
	.globl	nghttp2_hd_inflate_get_dynamic_table_size
	.type	nghttp2_hd_inflate_get_dynamic_table_size, @function
nghttp2_hd_inflate_get_dynamic_table_size:
.LFB133:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE133:
	.size	nghttp2_hd_inflate_get_dynamic_table_size, .-nghttp2_hd_inflate_get_dynamic_table_size
	.p2align 4
	.globl	nghttp2_hd_inflate_get_max_dynamic_table_size
	.type	nghttp2_hd_inflate_get_max_dynamic_table_size, @function
nghttp2_hd_inflate_get_max_dynamic_table_size:
.LFB134:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	ret
	.cfi_endproc
.LFE134:
	.size	nghttp2_hd_inflate_get_max_dynamic_table_size, .-nghttp2_hd_inflate_get_max_dynamic_table_size
	.section	.rodata
	.type	CSWTCH.61, @object
	.size	CSWTCH.61, 3
CSWTCH.61:
	.byte	64
	.byte	0
	.byte	16
	.align 8
	.type	__PRETTY_FUNCTION__.5143, @object
	.size	__PRETTY_FUNCTION__.5143, 12
__PRETTY_FUNCTION__.5143:
	.string	"emit_string"
	.align 16
	.type	__PRETTY_FUNCTION__.5151, @object
	.size	__PRETTY_FUNCTION__.5151, 16
__PRETTY_FUNCTION__.5151:
	.string	"pack_first_byte"
	.align 16
	.type	__PRETTY_FUNCTION__.5241, @object
	.size	__PRETTY_FUNCTION__.5241, 21
__PRETTY_FUNCTION__.5241:
	.string	"nghttp2_hd_table_get"
	.align 8
	.type	__PRETTY_FUNCTION__.5005, @object
	.size	__PRETTY_FUNCTION__.5005, 15
__PRETTY_FUNCTION__.5005:
	.string	"hd_ringbuf_get"
	.section	.rodata.str1.1
.LC57:
	.string	":authority"
.LC58:
	.string	""
.LC59:
	.string	":method"
.LC60:
	.string	"GET"
.LC61:
	.string	"POST"
.LC62:
	.string	":path"
.LC63:
	.string	"/"
.LC64:
	.string	"/index.html"
.LC65:
	.string	":scheme"
.LC66:
	.string	"http"
.LC67:
	.string	"https"
.LC68:
	.string	":status"
.LC69:
	.string	"200"
.LC70:
	.string	"204"
.LC71:
	.string	"206"
.LC72:
	.string	"304"
.LC73:
	.string	"400"
.LC74:
	.string	"404"
.LC75:
	.string	"500"
.LC76:
	.string	"accept-charset"
.LC77:
	.string	"accept-encoding"
.LC78:
	.string	"gzip, deflate"
.LC79:
	.string	"accept-language"
.LC80:
	.string	"accept-ranges"
.LC81:
	.string	"accept"
.LC82:
	.string	"access-control-allow-origin"
.LC83:
	.string	"age"
.LC84:
	.string	"allow"
.LC85:
	.string	"authorization"
.LC86:
	.string	"cache-control"
.LC87:
	.string	"content-disposition"
.LC88:
	.string	"content-encoding"
.LC89:
	.string	"content-language"
.LC90:
	.string	"content-length"
.LC91:
	.string	"content-location"
.LC92:
	.string	"content-range"
.LC93:
	.string	"content-type"
.LC94:
	.string	"cookie"
.LC95:
	.string	"date"
.LC96:
	.string	"etag"
.LC97:
	.string	"expect"
.LC98:
	.string	"expires"
.LC99:
	.string	"from"
.LC100:
	.string	"host"
.LC101:
	.string	"if-match"
.LC102:
	.string	"if-modified-since"
.LC103:
	.string	"if-none-match"
.LC104:
	.string	"if-range"
.LC105:
	.string	"if-unmodified-since"
.LC106:
	.string	"last-modified"
.LC107:
	.string	"link"
.LC108:
	.string	"location"
.LC109:
	.string	"max-forwards"
.LC110:
	.string	"proxy-authenticate"
.LC111:
	.string	"proxy-authorization"
.LC112:
	.string	"range"
.LC113:
	.string	"referer"
.LC114:
	.string	"refresh"
.LC115:
	.string	"retry-after"
.LC116:
	.string	"server"
.LC117:
	.string	"set-cookie"
.LC118:
	.string	"strict-transport-security"
.LC119:
	.string	"transfer-encoding"
.LC120:
	.string	"user-agent"
.LC121:
	.string	"vary"
.LC122:
	.string	"via"
.LC123:
	.string	"www-authenticate"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	static_table, @object
	.size	static_table, 7808
static_table:
	.quad	0
	.quad	0
	.quad	.LC57
	.quad	10
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC57
	.quad	.LC58
	.quad	10
	.quad	0
	.byte	0
	.zero	7
	.long	0
	.long	-1141242146
	.quad	0
	.quad	0
	.quad	.LC59
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC60
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC59
	.quad	.LC60
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	1
	.long	695666056
	.quad	0
	.quad	0
	.quad	.LC59
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC61
	.quad	4
	.long	-1
	.zero	4
	.quad	.LC59
	.quad	.LC61
	.quad	7
	.quad	4
	.byte	0
	.zero	7
	.long	1
	.long	695666056
	.quad	0
	.quad	0
	.quad	.LC62
	.quad	5
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC63
	.quad	1
	.long	-1
	.zero	4
	.quad	.LC62
	.quad	.LC63
	.quad	5
	.quad	1
	.byte	0
	.zero	7
	.long	3
	.long	-1002118610
	.quad	0
	.quad	0
	.quad	.LC62
	.quad	5
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC64
	.quad	11
	.long	-1
	.zero	4
	.quad	.LC62
	.quad	.LC64
	.quad	5
	.quad	11
	.byte	0
	.zero	7
	.long	3
	.long	-1002118610
	.quad	0
	.quad	0
	.quad	.LC65
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC66
	.quad	4
	.long	-1
	.zero	4
	.quad	.LC65
	.quad	.LC66
	.quad	7
	.quad	4
	.byte	0
	.zero	7
	.long	5
	.long	-1784489622
	.quad	0
	.quad	0
	.quad	.LC65
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC67
	.quad	5
	.long	-1
	.zero	4
	.quad	.LC65
	.quad	.LC67
	.quad	7
	.quad	5
	.byte	0
	.zero	7
	.long	5
	.long	-1784489622
	.quad	0
	.quad	0
	.quad	.LC68
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC69
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC68
	.quad	.LC69
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	7
	.long	-294678313
	.quad	0
	.quad	0
	.quad	.LC68
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC70
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC68
	.quad	.LC70
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	7
	.long	-294678313
	.quad	0
	.quad	0
	.quad	.LC68
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC71
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC68
	.quad	.LC71
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	7
	.long	-294678313
	.quad	0
	.quad	0
	.quad	.LC68
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC72
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC68
	.quad	.LC72
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	7
	.long	-294678313
	.quad	0
	.quad	0
	.quad	.LC68
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC73
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC68
	.quad	.LC73
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	7
	.long	-294678313
	.quad	0
	.quad	0
	.quad	.LC68
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC74
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC68
	.quad	.LC74
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	7
	.long	-294678313
	.quad	0
	.quad	0
	.quad	.LC68
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC75
	.quad	3
	.long	-1
	.zero	4
	.quad	.LC68
	.quad	.LC75
	.quad	7
	.quad	3
	.byte	0
	.zero	7
	.long	7
	.long	-294678313
	.quad	0
	.quad	0
	.quad	.LC76
	.quad	14
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC76
	.quad	.LC58
	.quad	14
	.quad	0
	.byte	0
	.zero	7
	.long	14
	.long	-630956952
	.quad	0
	.quad	0
	.quad	.LC77
	.quad	15
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC78
	.quad	13
	.long	-1
	.zero	4
	.quad	.LC77
	.quad	.LC78
	.quad	15
	.quad	13
	.byte	0
	.zero	7
	.long	15
	.long	-915318119
	.quad	0
	.quad	0
	.quad	.LC79
	.quad	15
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC79
	.quad	.LC58
	.quad	15
	.quad	0
	.byte	0
	.zero	7
	.long	16
	.long	1979086614
	.quad	0
	.quad	0
	.quad	.LC80
	.quad	13
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC80
	.quad	.LC58
	.quad	13
	.quad	0
	.byte	0
	.zero	7
	.long	17
	.long	1713753958
	.quad	0
	.quad	0
	.quad	.LC81
	.quad	6
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC81
	.quad	.LC58
	.quad	6
	.quad	0
	.byte	0
	.zero	7
	.long	18
	.long	136609321
	.quad	0
	.quad	0
	.quad	.LC82
	.quad	27
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC82
	.quad	.LC58
	.quad	27
	.quad	0
	.byte	0
	.zero	7
	.long	19
	.long	-1584170004
	.quad	0
	.quad	0
	.quad	.LC83
	.quad	3
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC83
	.quad	.LC58
	.quad	3
	.quad	0
	.byte	0
	.zero	7
	.long	20
	.long	742476188
	.quad	0
	.quad	0
	.quad	.LC84
	.quad	5
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC84
	.quad	.LC58
	.quad	5
	.quad	0
	.byte	0
	.zero	7
	.long	21
	.long	-1364088782
	.quad	0
	.quad	0
	.quad	.LC85
	.quad	13
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC85
	.quad	.LC58
	.quad	13
	.quad	0
	.byte	0
	.zero	7
	.long	22
	.long	-1858709570
	.quad	0
	.quad	0
	.quad	.LC86
	.quad	13
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC86
	.quad	.LC58
	.quad	13
	.quad	0
	.byte	0
	.zero	7
	.long	23
	.long	1355326669
	.quad	0
	.quad	0
	.quad	.LC87
	.quad	19
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC87
	.quad	.LC58
	.quad	19
	.quad	0
	.byte	0
	.zero	7
	.long	24
	.long	-405782948
	.quad	0
	.quad	0
	.quad	.LC88
	.quad	16
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC88
	.quad	.LC58
	.quad	16
	.quad	0
	.byte	0
	.zero	7
	.long	25
	.long	65203592
	.quad	0
	.quad	0
	.quad	.LC89
	.quad	16
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC89
	.quad	.LC58
	.quad	16
	.quad	0
	.byte	0
	.zero	7
	.long	26
	.long	24973587
	.quad	0
	.quad	0
	.quad	.LC90
	.quad	14
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC90
	.quad	.LC58
	.quad	14
	.quad	0
	.byte	0
	.zero	7
	.long	27
	.long	1308181789
	.quad	0
	.quad	0
	.quad	.LC91
	.quad	16
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC91
	.quad	.LC58
	.quad	16
	.quad	0
	.byte	0
	.zero	7
	.long	28
	.long	-1992602578
	.quad	0
	.quad	0
	.quad	.LC92
	.quad	13
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC92
	.quad	.LC58
	.quad	13
	.quad	0
	.byte	0
	.zero	7
	.long	29
	.long	-739444150
	.quad	0
	.quad	0
	.quad	.LC93
	.quad	12
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC93
	.quad	.LC58
	.quad	12
	.quad	0
	.byte	0
	.zero	7
	.long	30
	.long	-50919019
	.quad	0
	.quad	0
	.quad	.LC94
	.quad	6
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC94
	.quad	.LC58
	.quad	6
	.quad	0
	.byte	0
	.zero	7
	.long	31
	.long	2007449791
	.quad	0
	.quad	0
	.quad	.LC95
	.quad	4
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC95
	.quad	.LC58
	.quad	4
	.quad	0
	.byte	0
	.zero	7
	.long	32
	.long	-730669991
	.quad	0
	.quad	0
	.quad	.LC96
	.quad	4
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC96
	.quad	.LC58
	.quad	4
	.quad	0
	.byte	0
	.zero	7
	.long	33
	.long	113792960
	.quad	0
	.quad	0
	.quad	.LC97
	.quad	6
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC97
	.quad	.LC58
	.quad	6
	.quad	0
	.byte	0
	.zero	7
	.long	34
	.long	-1764070568
	.quad	0
	.quad	0
	.quad	.LC98
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC98
	.quad	.LC58
	.quad	7
	.quad	0
	.byte	0
	.zero	7
	.long	35
	.long	1049544579
	.quad	0
	.quad	0
	.quad	.LC99
	.quad	4
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC99
	.quad	.LC58
	.quad	4
	.quad	0
	.byte	0
	.zero	7
	.long	36
	.long	-1781694347
	.quad	0
	.quad	0
	.quad	.LC100
	.quad	4
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC100
	.quad	.LC58
	.quad	4
	.quad	0
	.byte	0
	.zero	7
	.long	37
	.long	-1342266001
	.quad	0
	.quad	0
	.quad	.LC101
	.quad	8
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC101
	.quad	.LC58
	.quad	8
	.quad	0
	.byte	0
	.zero	7
	.long	38
	.long	-697272598
	.quad	0
	.quad	0
	.quad	.LC102
	.quad	17
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC102
	.quad	.LC58
	.quad	17
	.quad	0
	.byte	0
	.zero	7
	.long	39
	.long	-2081916503
	.quad	0
	.quad	0
	.quad	.LC103
	.quad	13
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC103
	.quad	.LC58
	.quad	13
	.quad	0
	.byte	0
	.zero	7
	.long	40
	.long	-1758764681
	.quad	0
	.quad	0
	.quad	.LC104
	.quad	8
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC104
	.quad	.LC58
	.quad	8
	.quad	0
	.byte	0
	.zero	7
	.long	41
	.long	-1953989058
	.quad	0
	.quad	0
	.quad	.LC105
	.quad	19
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC105
	.quad	.LC58
	.quad	19
	.quad	0
	.byte	0
	.zero	7
	.long	42
	.long	-500152438
	.quad	0
	.quad	0
	.quad	.LC106
	.quad	13
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC106
	.quad	.LC58
	.quad	13
	.quad	0
	.byte	0
	.zero	7
	.long	43
	.long	-1068017045
	.quad	0
	.quad	0
	.quad	.LC107
	.quad	4
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC107
	.quad	.LC58
	.quad	4
	.quad	0
	.byte	0
	.zero	7
	.long	44
	.long	232457833
	.quad	0
	.quad	0
	.quad	.LC108
	.quad	8
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC108
	.quad	.LC58
	.quad	8
	.quad	0
	.byte	0
	.zero	7
	.long	45
	.long	200649126
	.quad	0
	.quad	0
	.quad	.LC109
	.quad	12
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC109
	.quad	.LC58
	.quad	12
	.quad	0
	.byte	0
	.zero	7
	.long	46
	.long	1826162134
	.quad	0
	.quad	0
	.quad	.LC110
	.quad	18
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC110
	.quad	.LC58
	.quad	18
	.quad	0
	.byte	0
	.zero	7
	.long	47
	.long	-1585521937
	.quad	0
	.quad	0
	.quad	.LC111
	.quad	19
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC111
	.quad	.LC58
	.quad	19
	.quad	0
	.byte	0
	.zero	7
	.long	48
	.long	-1608574789
	.quad	0
	.quad	0
	.quad	.LC112
	.quad	5
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC112
	.quad	.LC58
	.quad	5
	.quad	0
	.byte	0
	.zero	7
	.long	49
	.long	-86242094
	.quad	0
	.quad	0
	.quad	.LC113
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC113
	.quad	.LC58
	.quad	7
	.quad	0
	.byte	0
	.zero	7
	.long	50
	.long	-325387930
	.quad	0
	.quad	0
	.quad	.LC114
	.quad	7
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC114
	.quad	.LC58
	.quad	7
	.quad	0
	.byte	0
	.zero	7
	.long	51
	.long	-722311628
	.quad	0
	.quad	0
	.quad	.LC115
	.quad	11
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC115
	.quad	.LC58
	.quad	11
	.quad	0
	.byte	0
	.zero	7
	.long	52
	.long	-958786698
	.quad	0
	.quad	0
	.quad	.LC116
	.quad	6
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC116
	.quad	.LC58
	.quad	6
	.quad	0
	.byte	0
	.zero	7
	.long	53
	.long	1085029842
	.quad	0
	.quad	0
	.quad	.LC117
	.quad	10
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC117
	.quad	.LC58
	.quad	10
	.quad	0
	.byte	0
	.zero	7
	.long	54
	.long	1848371000
	.quad	0
	.quad	0
	.quad	.LC118
	.quad	25
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC118
	.quad	.LC58
	.quad	25
	.quad	0
	.byte	0
	.zero	7
	.long	55
	.long	-156819935
	.quad	0
	.quad	0
	.quad	.LC119
	.quad	17
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC119
	.quad	.LC58
	.quad	17
	.quad	0
	.byte	0
	.zero	7
	.long	56
	.long	-575376308
	.quad	0
	.quad	0
	.quad	.LC120
	.quad	10
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC120
	.quad	.LC58
	.quad	10
	.quad	0
	.byte	0
	.zero	7
	.long	57
	.long	606444526
	.quad	0
	.quad	0
	.quad	.LC121
	.quad	4
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC121
	.quad	.LC58
	.quad	4
	.quad	0
	.byte	0
	.zero	7
	.long	58
	.long	1085005381
	.quad	0
	.quad	0
	.quad	.LC122
	.quad	3
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC122
	.quad	.LC58
	.quad	3
	.quad	0
	.byte	0
	.zero	7
	.long	59
	.long	1762798611
	.quad	0
	.quad	0
	.quad	.LC123
	.quad	16
	.long	-1
	.zero	4
	.quad	0
	.quad	0
	.quad	.LC58
	.quad	0
	.long	-1
	.zero	4
	.quad	.LC123
	.quad	.LC58
	.quad	16
	.quad	0
	.byte	0
	.zero	7
	.long	60
	.long	779865858
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC56:
	.quad	4096
	.quad	4294967295
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
