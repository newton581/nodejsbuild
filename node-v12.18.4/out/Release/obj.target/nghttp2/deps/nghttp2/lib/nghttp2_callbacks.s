	.file	"nghttp2_callbacks.c"
	.text
	.p2align 4
	.globl	nghttp2_session_callbacks_new
	.type	nghttp2_session_callbacks_new, @function
nghttp2_session_callbacks_new:
.LFB20:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$184, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$1, %edi
	subq	$8, %rsp
	call	calloc@PLT
	cmpq	$1, %rax
	movq	%rax, (%rbx)
	sbbl	%eax, %eax
	addq	$8, %rsp
	andl	$-901, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
	.size	nghttp2_session_callbacks_new, .-nghttp2_session_callbacks_new
	.p2align 4
	.globl	nghttp2_session_callbacks_del
	.type	nghttp2_session_callbacks_del, @function
nghttp2_session_callbacks_del:
.LFB21:
	.cfi_startproc
	endbr64
	jmp	free@PLT
	.cfi_endproc
.LFE21:
	.size	nghttp2_session_callbacks_del, .-nghttp2_session_callbacks_del
	.p2align 4
	.globl	nghttp2_session_callbacks_set_send_callback
	.type	nghttp2_session_callbacks_set_send_callback, @function
nghttp2_session_callbacks_set_send_callback:
.LFB22:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE22:
	.size	nghttp2_session_callbacks_set_send_callback, .-nghttp2_session_callbacks_set_send_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_recv_callback
	.type	nghttp2_session_callbacks_set_recv_callback, @function
nghttp2_session_callbacks_set_recv_callback:
.LFB23:
	.cfi_startproc
	endbr64
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE23:
	.size	nghttp2_session_callbacks_set_recv_callback, .-nghttp2_session_callbacks_set_recv_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_frame_recv_callback
	.type	nghttp2_session_callbacks_set_on_frame_recv_callback, @function
nghttp2_session_callbacks_set_on_frame_recv_callback:
.LFB24:
	.cfi_startproc
	endbr64
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE24:
	.size	nghttp2_session_callbacks_set_on_frame_recv_callback, .-nghttp2_session_callbacks_set_on_frame_recv_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_invalid_frame_recv_callback
	.type	nghttp2_session_callbacks_set_on_invalid_frame_recv_callback, @function
nghttp2_session_callbacks_set_on_invalid_frame_recv_callback:
.LFB25:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE25:
	.size	nghttp2_session_callbacks_set_on_invalid_frame_recv_callback, .-nghttp2_session_callbacks_set_on_invalid_frame_recv_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_data_chunk_recv_callback
	.type	nghttp2_session_callbacks_set_on_data_chunk_recv_callback, @function
nghttp2_session_callbacks_set_on_data_chunk_recv_callback:
.LFB26:
	.cfi_startproc
	endbr64
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE26:
	.size	nghttp2_session_callbacks_set_on_data_chunk_recv_callback, .-nghttp2_session_callbacks_set_on_data_chunk_recv_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_before_frame_send_callback
	.type	nghttp2_session_callbacks_set_before_frame_send_callback, @function
nghttp2_session_callbacks_set_before_frame_send_callback:
.LFB27:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE27:
	.size	nghttp2_session_callbacks_set_before_frame_send_callback, .-nghttp2_session_callbacks_set_before_frame_send_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_frame_send_callback
	.type	nghttp2_session_callbacks_set_on_frame_send_callback, @function
nghttp2_session_callbacks_set_on_frame_send_callback:
.LFB28:
	.cfi_startproc
	endbr64
	movq	%rsi, 48(%rdi)
	ret
	.cfi_endproc
.LFE28:
	.size	nghttp2_session_callbacks_set_on_frame_send_callback, .-nghttp2_session_callbacks_set_on_frame_send_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_frame_not_send_callback
	.type	nghttp2_session_callbacks_set_on_frame_not_send_callback, @function
nghttp2_session_callbacks_set_on_frame_not_send_callback:
.LFB29:
	.cfi_startproc
	endbr64
	movq	%rsi, 56(%rdi)
	ret
	.cfi_endproc
.LFE29:
	.size	nghttp2_session_callbacks_set_on_frame_not_send_callback, .-nghttp2_session_callbacks_set_on_frame_not_send_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_stream_close_callback
	.type	nghttp2_session_callbacks_set_on_stream_close_callback, @function
nghttp2_session_callbacks_set_on_stream_close_callback:
.LFB30:
	.cfi_startproc
	endbr64
	movq	%rsi, 64(%rdi)
	ret
	.cfi_endproc
.LFE30:
	.size	nghttp2_session_callbacks_set_on_stream_close_callback, .-nghttp2_session_callbacks_set_on_stream_close_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_begin_headers_callback
	.type	nghttp2_session_callbacks_set_on_begin_headers_callback, @function
nghttp2_session_callbacks_set_on_begin_headers_callback:
.LFB31:
	.cfi_startproc
	endbr64
	movq	%rsi, 72(%rdi)
	ret
	.cfi_endproc
.LFE31:
	.size	nghttp2_session_callbacks_set_on_begin_headers_callback, .-nghttp2_session_callbacks_set_on_begin_headers_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_header_callback
	.type	nghttp2_session_callbacks_set_on_header_callback, @function
nghttp2_session_callbacks_set_on_header_callback:
.LFB32:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	ret
	.cfi_endproc
.LFE32:
	.size	nghttp2_session_callbacks_set_on_header_callback, .-nghttp2_session_callbacks_set_on_header_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_header_callback2
	.type	nghttp2_session_callbacks_set_on_header_callback2, @function
nghttp2_session_callbacks_set_on_header_callback2:
.LFB33:
	.cfi_startproc
	endbr64
	movq	%rsi, 88(%rdi)
	ret
	.cfi_endproc
.LFE33:
	.size	nghttp2_session_callbacks_set_on_header_callback2, .-nghttp2_session_callbacks_set_on_header_callback2
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_invalid_header_callback
	.type	nghttp2_session_callbacks_set_on_invalid_header_callback, @function
nghttp2_session_callbacks_set_on_invalid_header_callback:
.LFB34:
	.cfi_startproc
	endbr64
	movq	%rsi, 96(%rdi)
	ret
	.cfi_endproc
.LFE34:
	.size	nghttp2_session_callbacks_set_on_invalid_header_callback, .-nghttp2_session_callbacks_set_on_invalid_header_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_invalid_header_callback2
	.type	nghttp2_session_callbacks_set_on_invalid_header_callback2, @function
nghttp2_session_callbacks_set_on_invalid_header_callback2:
.LFB35:
	.cfi_startproc
	endbr64
	movq	%rsi, 104(%rdi)
	ret
	.cfi_endproc
.LFE35:
	.size	nghttp2_session_callbacks_set_on_invalid_header_callback2, .-nghttp2_session_callbacks_set_on_invalid_header_callback2
	.p2align 4
	.globl	nghttp2_session_callbacks_set_select_padding_callback
	.type	nghttp2_session_callbacks_set_select_padding_callback, @function
nghttp2_session_callbacks_set_select_padding_callback:
.LFB36:
	.cfi_startproc
	endbr64
	movq	%rsi, 112(%rdi)
	ret
	.cfi_endproc
.LFE36:
	.size	nghttp2_session_callbacks_set_select_padding_callback, .-nghttp2_session_callbacks_set_select_padding_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_data_source_read_length_callback
	.type	nghttp2_session_callbacks_set_data_source_read_length_callback, @function
nghttp2_session_callbacks_set_data_source_read_length_callback:
.LFB37:
	.cfi_startproc
	endbr64
	movq	%rsi, 120(%rdi)
	ret
	.cfi_endproc
.LFE37:
	.size	nghttp2_session_callbacks_set_data_source_read_length_callback, .-nghttp2_session_callbacks_set_data_source_read_length_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_begin_frame_callback
	.type	nghttp2_session_callbacks_set_on_begin_frame_callback, @function
nghttp2_session_callbacks_set_on_begin_frame_callback:
.LFB38:
	.cfi_startproc
	endbr64
	movq	%rsi, 128(%rdi)
	ret
	.cfi_endproc
.LFE38:
	.size	nghttp2_session_callbacks_set_on_begin_frame_callback, .-nghttp2_session_callbacks_set_on_begin_frame_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_send_data_callback
	.type	nghttp2_session_callbacks_set_send_data_callback, @function
nghttp2_session_callbacks_set_send_data_callback:
.LFB39:
	.cfi_startproc
	endbr64
	movq	%rsi, 136(%rdi)
	ret
	.cfi_endproc
.LFE39:
	.size	nghttp2_session_callbacks_set_send_data_callback, .-nghttp2_session_callbacks_set_send_data_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_pack_extension_callback
	.type	nghttp2_session_callbacks_set_pack_extension_callback, @function
nghttp2_session_callbacks_set_pack_extension_callback:
.LFB40:
	.cfi_startproc
	endbr64
	movq	%rsi, 144(%rdi)
	ret
	.cfi_endproc
.LFE40:
	.size	nghttp2_session_callbacks_set_pack_extension_callback, .-nghttp2_session_callbacks_set_pack_extension_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_unpack_extension_callback
	.type	nghttp2_session_callbacks_set_unpack_extension_callback, @function
nghttp2_session_callbacks_set_unpack_extension_callback:
.LFB41:
	.cfi_startproc
	endbr64
	movq	%rsi, 152(%rdi)
	ret
	.cfi_endproc
.LFE41:
	.size	nghttp2_session_callbacks_set_unpack_extension_callback, .-nghttp2_session_callbacks_set_unpack_extension_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_on_extension_chunk_recv_callback
	.type	nghttp2_session_callbacks_set_on_extension_chunk_recv_callback, @function
nghttp2_session_callbacks_set_on_extension_chunk_recv_callback:
.LFB42:
	.cfi_startproc
	endbr64
	movq	%rsi, 160(%rdi)
	ret
	.cfi_endproc
.LFE42:
	.size	nghttp2_session_callbacks_set_on_extension_chunk_recv_callback, .-nghttp2_session_callbacks_set_on_extension_chunk_recv_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_error_callback
	.type	nghttp2_session_callbacks_set_error_callback, @function
nghttp2_session_callbacks_set_error_callback:
.LFB43:
	.cfi_startproc
	endbr64
	movq	%rsi, 168(%rdi)
	ret
	.cfi_endproc
.LFE43:
	.size	nghttp2_session_callbacks_set_error_callback, .-nghttp2_session_callbacks_set_error_callback
	.p2align 4
	.globl	nghttp2_session_callbacks_set_error_callback2
	.type	nghttp2_session_callbacks_set_error_callback2, @function
nghttp2_session_callbacks_set_error_callback2:
.LFB44:
	.cfi_startproc
	endbr64
	movq	%rsi, 176(%rdi)
	ret
	.cfi_endproc
.LFE44:
	.size	nghttp2_session_callbacks_set_error_callback2, .-nghttp2_session_callbacks_set_error_callback2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
