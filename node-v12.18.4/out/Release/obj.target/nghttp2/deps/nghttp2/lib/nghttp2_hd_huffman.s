	.file	"nghttp2_hd_huffman.c"
	.text
	.p2align 4
	.globl	nghttp2_hd_huff_encode_count
	.type	nghttp2_hd_huff_encode_count, @function
nghttp2_hd_huff_encode_count:
.LFB57:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4
	addq	%rdi, %rsi
	xorl	%eax, %eax
	leaq	huff_sym_table(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L3:
	movzbl	(%rdi), %edx
	addq	$1, %rdi
	movl	(%rcx,%rdx,8), %edx
	addq	%rdx, %rax
	cmpq	%rdi, %rsi
	jne	.L3
	addq	$7, %rax
	shrq	$3, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE57:
	.size	nghttp2_hd_huff_encode_count, .-nghttp2_hd_huff_encode_count
	.p2align 4
	.globl	nghttp2_hd_huff_encode
	.type	nghttp2_hd_huff_encode, @function
nghttp2_hd_huff_encode:
.LFB58:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	(%rsi,%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	%rax, -56(%rbp)
	movq	16(%r9), %r8
	subq	32(%r9), %r8
	cmpq	%rax, %rsi
	je	.L8
	movq	%rdi, %r14
	movq	%rsi, %rbx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	leaq	huff_sym_table(%rip), %r12
.L12:
	movzbl	(%rbx), %esi
	movl	$32, %ecx
	addq	$1, %rbx
	subl	%r15d, %ecx
	movl	4(%r12,%rsi,8), %eax
	salq	%cl, %rax
	orq	%rax, %r13
	movl	(%r12,%rsi,8), %eax
	addq	%rax, %r15
	cmpq	$31, %r15
	jbe	.L9
	cmpq	$3, %r8
	ja	.L26
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r13, %rsi
	movq	%r14, %rdi
	shrq	$56, %rsi
	call	nghttp2_bufs_addb@PLT
	testl	%eax, %eax
	jne	.L7
	subq	$8, %r15
	salq	$8, %r13
	cmpq	$7, %r15
	ja	.L10
	movq	8(%r14), %r9
	movq	16(%r9), %r8
	subq	32(%r9), %r8
.L9:
	cmpq	%rbx, -56(%rbp)
	jne	.L12
	cmpq	$7, %r15
	ja	.L15
.L27:
	testq	%r15, %r15
	je	.L8
	movl	$8, %ecx
	movl	$1, %eax
	movq	%r13, %rdx
	movq	%r14, %rdi
	subl	%r15d, %ecx
	shrq	$56, %rdx
	addq	$24, %rsp
	sall	%cl, %eax
	popq	%rbx
	popq	%r12
	leal	-1(%rax), %esi
	popq	%r13
	popq	%r14
	orl	%esi, %edx
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%dl, %esi
	jmp	nghttp2_bufs_addb@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	subq	$8, %r15
	salq	$8, %r13
	cmpq	$7, %r15
	jbe	.L27
.L15:
	movq	%r13, %rsi
	movq	%r14, %rdi
	shrq	$56, %rsi
	call	nghttp2_bufs_addb@PLT
	testl	%eax, %eax
	je	.L28
.L7:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	32(%r9), %rcx
	movq	%r13, %rax
	subq	$4, %r8
	salq	$32, %r13
	shrq	$32, %rax
	subq	$32, %r15
	bswap	%eax
	movl	%eax, (%rcx)
	movq	8(%r14), %r9
	addq	$4, 32(%r9)
	jmp	.L9
.L8:
	xorl	%eax, %eax
	jmp	.L7
	.cfi_endproc
.LFE58:
	.size	nghttp2_hd_huff_encode, .-nghttp2_hd_huff_encode
	.p2align 4
	.globl	nghttp2_hd_huff_decode_context_init
	.type	nghttp2_hd_huff_decode_context_init, @function
nghttp2_hd_huff_decode_context_init:
.LFB59:
	.cfi_startproc
	endbr64
	movl	$16384, %eax
	movw	%ax, (%rdi)
	ret
	.cfi_endproc
.LFE59:
	.size	nghttp2_hd_huff_decode_context_init, .-nghttp2_hd_huff_decode_context_init
	.p2align 4
	.globl	nghttp2_hd_huff_decode
	.type	nghttp2_hd_huff_decode, @function
nghttp2_hd_huff_decode:
.LFB60:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	(%rdx,%rcx), %rbx
	subq	$24, %rsp
	movzwl	(%rdi), %r10d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, -42(%rbp)
	movw	%r10w, -44(%rbp)
	cmpq	%rbx, %rdx
	je	.L31
	leaq	huff_decode_table(%rip), %r11
	.p2align 4,,10
	.p2align 3
.L36:
	movzbl	(%rdx), %eax
	andl	$511, %r10d
	addq	$1, %rdx
	salq	$4, %r10
	movl	%eax, %r9d
	shrb	$4, %r9b
	andl	$15, %r9d
	addq	%r9, %r10
	movzwl	(%r11,%r10,4), %r9d
	testw	%r9w, %r9w
	jns	.L32
	movq	24(%rsi), %r12
	movzbl	2(%r11,%r10,4), %r10d
	leaq	1(%r12), %r13
	movq	%r13, 24(%rsi)
	movb	%r10b, (%r12)
.L32:
	movq	%r9, %r10
	movq	%rax, %r12
	andl	$511, %r9d
	andl	$15, %r12d
	andl	$511, %r10d
	salq	$4, %r9
	salq	$4, %r10
	addq	%r12, %r9
	addq	%r12, %r10
	cmpw	$0, (%r11,%r9,4)
	leaq	(%r11,%r10,4), %r10
	js	.L45
	movzwl	(%r10), %r10d
	cmpq	%rdx, %rbx
	jne	.L36
.L31:
	movw	%r10w, (%rdi)
	testl	%r8d, %r8d
	je	.L37
	testw	$16384, %r10w
	je	.L40
.L37:
	movq	%rcx, %rax
.L30:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L46
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	24(%rsi), %rax
	movzbl	2(%r11,%r9,4), %r9d
	leaq	1(%rax), %r12
	movq	%r12, 24(%rsi)
	movb	%r9b, (%rax)
	movzwl	(%r10), %r10d
	cmpq	%rdx, %rbx
	jne	.L36
	jmp	.L31
.L40:
	movq	$-523, %rax
	jmp	.L30
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE60:
	.size	nghttp2_hd_huff_decode, .-nghttp2_hd_huff_decode
	.p2align 4
	.globl	nghttp2_hd_huff_decode_failure_state
	.type	nghttp2_hd_huff_decode_failure_state, @function
nghttp2_hd_huff_decode_failure_state:
.LFB61:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$256, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE61:
	.size	nghttp2_hd_huff_decode_failure_state, .-nghttp2_hd_huff_decode_failure_state
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
