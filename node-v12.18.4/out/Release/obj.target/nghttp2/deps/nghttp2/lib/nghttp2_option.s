	.file	"nghttp2_option.c"
	.text
	.p2align 4
	.globl	nghttp2_option_new
	.type	nghttp2_option_new, @function
nghttp2_option_new:
.LFB20:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$104, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$1, %edi
	subq	$8, %rsp
	call	calloc@PLT
	cmpq	$1, %rax
	movq	%rax, (%rbx)
	sbbl	%eax, %eax
	addq	$8, %rsp
	andl	$-901, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
	.size	nghttp2_option_new, .-nghttp2_option_new
	.p2align 4
	.globl	nghttp2_option_del
	.type	nghttp2_option_del, @function
nghttp2_option_del:
.LFB21:
	.cfi_startproc
	endbr64
	jmp	free@PLT
	.cfi_endproc
.LFE21:
	.size	nghttp2_option_del, .-nghttp2_option_del
	.p2align 4
	.globl	nghttp2_option_set_no_auto_window_update
	.type	nghttp2_option_set_no_auto_window_update, @function
nghttp2_option_set_no_auto_window_update:
.LFB22:
	.cfi_startproc
	endbr64
	orl	$1, 32(%rdi)
	movl	%esi, 48(%rdi)
	ret
	.cfi_endproc
.LFE22:
	.size	nghttp2_option_set_no_auto_window_update, .-nghttp2_option_set_no_auto_window_update
	.p2align 4
	.globl	nghttp2_option_set_peer_max_concurrent_streams
	.type	nghttp2_option_set_peer_max_concurrent_streams, @function
nghttp2_option_set_peer_max_concurrent_streams:
.LFB23:
	.cfi_startproc
	endbr64
	orl	$2, 32(%rdi)
	movl	%esi, 36(%rdi)
	ret
	.cfi_endproc
.LFE23:
	.size	nghttp2_option_set_peer_max_concurrent_streams, .-nghttp2_option_set_peer_max_concurrent_streams
	.p2align 4
	.globl	nghttp2_option_set_no_recv_client_magic
	.type	nghttp2_option_set_no_recv_client_magic, @function
nghttp2_option_set_no_recv_client_magic:
.LFB24:
	.cfi_startproc
	endbr64
	orl	$4, 32(%rdi)
	movl	%esi, 52(%rdi)
	ret
	.cfi_endproc
.LFE24:
	.size	nghttp2_option_set_no_recv_client_magic, .-nghttp2_option_set_no_recv_client_magic
	.p2align 4
	.globl	nghttp2_option_set_no_http_messaging
	.type	nghttp2_option_set_no_http_messaging, @function
nghttp2_option_set_no_http_messaging:
.LFB25:
	.cfi_startproc
	endbr64
	orl	$8, 32(%rdi)
	movl	%esi, 56(%rdi)
	ret
	.cfi_endproc
.LFE25:
	.size	nghttp2_option_set_no_http_messaging, .-nghttp2_option_set_no_http_messaging
	.p2align 4
	.globl	nghttp2_option_set_max_reserved_remote_streams
	.type	nghttp2_option_set_max_reserved_remote_streams, @function
nghttp2_option_set_max_reserved_remote_streams:
.LFB26:
	.cfi_startproc
	endbr64
	orl	$16, 32(%rdi)
	movl	%esi, 40(%rdi)
	ret
	.cfi_endproc
.LFE26:
	.size	nghttp2_option_set_max_reserved_remote_streams, .-nghttp2_option_set_max_reserved_remote_streams
	.p2align 4
	.globl	nghttp2_option_set_user_recv_extension_type
	.type	nghttp2_option_set_user_recv_extension_type, @function
nghttp2_option_set_user_recv_extension_type:
.LFB28:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	cmpb	$9, %sil
	jbe	.L12
	movl	%esi, %eax
	andl	$7, %ecx
	movl	$1, %edx
	orl	$32, 32(%rdi)
	shrb	$3, %al
	sall	%cl, %edx
	movzbl	%al, %eax
	orb	%dl, 68(%rdi,%rax)
.L12:
	ret
	.cfi_endproc
.LFE28:
	.size	nghttp2_option_set_user_recv_extension_type, .-nghttp2_option_set_user_recv_extension_type
	.p2align 4
	.globl	nghttp2_option_set_builtin_recv_extension_type
	.type	nghttp2_option_set_builtin_recv_extension_type, @function
nghttp2_option_set_builtin_recv_extension_type:
.LFB29:
	.cfi_startproc
	endbr64
	cmpb	$10, %sil
	je	.L15
	cmpb	$12, %sil
	jne	.L18
	orl	$128, 32(%rdi)
	orl	$2, 44(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	orl	$128, 32(%rdi)
	orl	$1, 44(%rdi)
	ret
	.cfi_endproc
.LFE29:
	.size	nghttp2_option_set_builtin_recv_extension_type, .-nghttp2_option_set_builtin_recv_extension_type
	.p2align 4
	.globl	nghttp2_option_set_no_auto_ping_ack
	.type	nghttp2_option_set_no_auto_ping_ack, @function
nghttp2_option_set_no_auto_ping_ack:
.LFB30:
	.cfi_startproc
	endbr64
	orl	$64, 32(%rdi)
	movl	%esi, 60(%rdi)
	ret
	.cfi_endproc
.LFE30:
	.size	nghttp2_option_set_no_auto_ping_ack, .-nghttp2_option_set_no_auto_ping_ack
	.p2align 4
	.globl	nghttp2_option_set_max_send_header_block_length
	.type	nghttp2_option_set_max_send_header_block_length, @function
nghttp2_option_set_max_send_header_block_length:
.LFB31:
	.cfi_startproc
	endbr64
	orl	$256, 32(%rdi)
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE31:
	.size	nghttp2_option_set_max_send_header_block_length, .-nghttp2_option_set_max_send_header_block_length
	.p2align 4
	.globl	nghttp2_option_set_max_deflate_dynamic_table_size
	.type	nghttp2_option_set_max_deflate_dynamic_table_size, @function
nghttp2_option_set_max_deflate_dynamic_table_size:
.LFB32:
	.cfi_startproc
	endbr64
	orl	$512, 32(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE32:
	.size	nghttp2_option_set_max_deflate_dynamic_table_size, .-nghttp2_option_set_max_deflate_dynamic_table_size
	.p2align 4
	.globl	nghttp2_option_set_no_closed_streams
	.type	nghttp2_option_set_no_closed_streams, @function
nghttp2_option_set_no_closed_streams:
.LFB33:
	.cfi_startproc
	endbr64
	orl	$1024, 32(%rdi)
	movl	%esi, 64(%rdi)
	ret
	.cfi_endproc
.LFE33:
	.size	nghttp2_option_set_no_closed_streams, .-nghttp2_option_set_no_closed_streams
	.p2align 4
	.globl	nghttp2_option_set_max_outbound_ack
	.type	nghttp2_option_set_max_outbound_ack, @function
nghttp2_option_set_max_outbound_ack:
.LFB34:
	.cfi_startproc
	endbr64
	orl	$2048, 32(%rdi)
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE34:
	.size	nghttp2_option_set_max_outbound_ack, .-nghttp2_option_set_max_outbound_ack
	.p2align 4
	.globl	nghttp2_option_set_max_settings
	.type	nghttp2_option_set_max_settings, @function
nghttp2_option_set_max_settings:
.LFB35:
	.cfi_startproc
	endbr64
	orl	$4096, 32(%rdi)
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE35:
	.size	nghttp2_option_set_max_settings, .-nghttp2_option_set_max_settings
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
