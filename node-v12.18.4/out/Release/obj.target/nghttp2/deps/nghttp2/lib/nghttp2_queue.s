	.file	"nghttp2_queue.c"
	.text
	.p2align 4
	.globl	nghttp2_queue_init
	.type	nghttp2_queue_init, @function
nghttp2_queue_init:
.LFB31:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE31:
	.size	nghttp2_queue_init, .-nghttp2_queue_init
	.p2align 4
	.globl	nghttp2_queue_free
	.type	nghttp2_queue_free, @function
nghttp2_queue_free:
.LFB32:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rbx, %rdi
	movq	8(%rbx), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	jne	.L5
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE32:
	.size	nghttp2_queue_free, .-nghttp2_queue_free
	.p2align 4
	.globl	nghttp2_queue_push
	.type	nghttp2_queue_push, @function
nghttp2_queue_push:
.LFB33:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$16, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L20
	movq	8(%rbx), %rdx
	movq	%r12, (%rax)
	movq	$0, 8(%rax)
	testq	%rdx, %rdx
	je	.L19
	movq	%rax, 8(%rdx)
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
.L17:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%rax, %xmm0
	xorl	%eax, %eax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L17
	.cfi_endproc
.LFE33:
	.size	nghttp2_queue_push, .-nghttp2_queue_push
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_queue.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"front"
	.text
	.p2align 4
	.globl	nghttp2_queue_pop
	.type	nghttp2_queue_pop, @function
nghttp2_queue_pop:
.LFB34:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L28
	movq	8(%r8), %rax
	movq	%rax, (%rdi)
	cmpq	%r8, 8(%rdi)
	je	.L29
	movq	%r8, %rdi
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movq	$0, 8(%rdi)
	movq	%r8, %rdi
	jmp	free@PLT
.L28:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.3924(%rip), %rcx
	movl	$67, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE34:
	.size	nghttp2_queue_pop, .-nghttp2_queue_pop
	.section	.rodata.str1.1
.LC2:
	.string	"queue->front"
	.text
	.p2align 4
	.globl	nghttp2_queue_front
	.type	nghttp2_queue_front, @function
nghttp2_queue_front:
.LFB35:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L35
	movq	(%rax), %rax
	ret
.L35:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.3928(%rip), %rcx
	movl	$76, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE35:
	.size	nghttp2_queue_front, .-nghttp2_queue_front
	.section	.rodata.str1.1
.LC3:
	.string	"queue->back"
	.text
	.p2align 4
	.globl	nghttp2_queue_back
	.type	nghttp2_queue_back, @function
nghttp2_queue_back:
.LFB36:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L41
	movq	(%rax), %rax
	ret
.L41:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.3932(%rip), %rcx
	movl	$81, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE36:
	.size	nghttp2_queue_back, .-nghttp2_queue_back
	.p2align 4
	.globl	nghttp2_queue_empty
	.type	nghttp2_queue_empty, @function
nghttp2_queue_empty:
.LFB37:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE37:
	.size	nghttp2_queue_empty, .-nghttp2_queue_empty
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.3932, @object
	.size	__PRETTY_FUNCTION__.3932, 19
__PRETTY_FUNCTION__.3932:
	.string	"nghttp2_queue_back"
	.align 16
	.type	__PRETTY_FUNCTION__.3928, @object
	.size	__PRETTY_FUNCTION__.3928, 20
__PRETTY_FUNCTION__.3928:
	.string	"nghttp2_queue_front"
	.align 16
	.type	__PRETTY_FUNCTION__.3924, @object
	.size	__PRETTY_FUNCTION__.3924, 18
__PRETTY_FUNCTION__.3924:
	.string	"nghttp2_queue_pop"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
