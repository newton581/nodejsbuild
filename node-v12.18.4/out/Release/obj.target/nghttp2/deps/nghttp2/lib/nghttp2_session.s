	.file	"nghttp2_session.c"
	.text
	.p2align 4
	.type	session_inbound_frame_reset, @function
session_inbound_frame_reset:
.LFB77:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	2200(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	424(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	436(%rdi), %eax
	cmpb	$8, %al
	ja	.L2
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L12-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
.L2:
	movl	%eax, %edx
	shrb	$3, %dl
	movzbl	%dl, %edx
	movzbl	2501(%rdi,%rdx), %ecx
	movl	%eax, %edx
	andl	$7, %edx
	btl	%edx, %ecx
	jc	.L24
	cmpb	$10, %al
	je	.L14
	cmpb	$12, %al
	jne	.L12
	testb	$2, 2492(%rdi)
	je	.L12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_frame_origin_free@PLT
.L12:
	pxor	%xmm0, %xmm0
	leaq	652(%rbx), %rsi
	movl	$9, %edx
	movups	%xmm0, 424(%rbx)
	leaq	528(%rbx), %rdi
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
	leaq	568(%rbx), %r12
	movl	$2, 648(%rbx)
	movups	%xmm0, 488(%rbx)
	movups	%xmm0, 504(%rbx)
	call	nghttp2_buf_wrap_init@PLT
	addq	$9, 560(%rbx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_buf_free@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	nghttp2_buf_wrap_init@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 608(%rbx)
	movups	%xmm0, 632(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_frame_goaway_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	call	nghttp2_frame_window_update_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_frame_headers_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r12, %rdi
	call	nghttp2_frame_priority_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r12, %rdi
	call	nghttp2_frame_rst_stream_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_frame_settings_free@PLT
	movq	520(%rbx), %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 520(%rbx)
	movups	%xmm0, 616(%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_frame_push_promise_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r12, %rdi
	call	nghttp2_frame_ping_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	testb	$1, 2492(%rdi)
	je	.L12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_frame_altsvc_free@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, %rdi
	call	nghttp2_frame_extension_free@PLT
	jmp	.L12
	.cfi_endproc
.LFE77:
	.size	session_inbound_frame_reset, .-session_inbound_frame_reset
	.p2align 4
	.type	free_streams, @function
free_streams:
.LFB87:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	2200(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	176(%rdi), %r13
	testq	%r13, %r13
	je	.L26
	cmpb	$0, 144(%r13)
	jne	.L26
	cmpq	%r13, 344(%rsi)
	je	.L26
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
.L26:
	movq	%r12, %rdi
	call	nghttp2_stream_free@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE87:
	.size	free_streams, .-free_streams
	.p2align 4
	.type	session_call_error_callback, @function
session_call_error_callback:
.LFB69:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$232, %rsp
	movl	%esi, -268(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	testb	%al, %al
	je	.L32
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm7, -64(%rbp)
.L32:
	movq	%fs:40, %rax
	movq	%rax, -232(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 2184(%r15)
	je	.L47
.L33:
	leaq	16(%rbp), %rax
	leaq	-256(%rbp), %r9
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -248(%rbp)
	movq	%rbx, %r8
	leaq	-224(%rbp), %rax
	movq	$-1, %rcx
	movl	$1, %edx
	movq	%r9, -264(%rbp)
	leaq	2200(%r15), %r14
	movl	$24, -256(%rbp)
	movl	$48, -252(%rbp)
	movq	%rax, -240(%rbp)
	call	__vsnprintf_chk@PLT
	movq	-264(%rbp), %r9
	testl	%eax, %eax
	js	.L37
	addl	$1, %eax
	movq	%r14, %rdi
	movq	%r9, -264(%rbp)
	movslq	%eax, %r12
	movq	%r12, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L37
	leaq	16(%rbp), %rax
	movq	%rbx, %r8
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-264(%rbp), %r9
	movq	%rax, -248(%rbp)
	movq	$-1, %rcx
	movq	%r13, %rdi
	leaq	-224(%rbp), %rax
	movl	$24, -256(%rbp)
	movl	$48, -252(%rbp)
	movq	%rax, -240(%rbp)
	call	__vsnprintf_chk@PLT
	testl	%eax, %eax
	js	.L48
	movq	2192(%r15), %r11
	movq	2240(%r15), %r8
	movslq	%eax, %r9
	testq	%r11, %r11
	je	.L39
	movl	-268(%rbp), %esi
	movq	%r9, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	*%r11
	movl	%eax, %ebx
.L40:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	testl	%ebx, %ebx
	jne	.L42
.L34:
	xorl	%eax, %eax
.L31:
	movq	-232(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L49
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	cmpq	$0, 2192(%r15)
	jne	.L33
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r8, %rcx
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*2184(%r15)
	movl	%eax, %ebx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	xorl	%eax, %eax
	jmp	.L31
.L42:
	movl	$-902, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$-901, %eax
	jmp	.L31
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE69:
	.size	session_call_error_callback, .-session_call_error_callback
	.p2align 4
	.type	session_headers_add_pad, @function
session_headers_add_pad:
.LFB120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	$16384, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdx
	leaq	256(%rdx), %rax
	cmpq	$16384, %rax
	cmovbe	%rax, %r12
	cmpq	%r12, %rdx
	jnb	.L59
	movq	2128(%rdi), %r8
	testq	%r8, %r8
	je	.L55
	cmpq	%r12, %rax
	movq	2240(%rdi), %rcx
	cmovbe	%rax, %r12
	movq	%r12, %rdx
	call	*%r8
	movq	0(%r13), %rdx
	cmpq	%rdx, %rax
	jl	.L57
	cmpq	%r12, %rax
	jg	.L57
	movl	%eax, %r8d
	cmpl	$-900, %eax
	jge	.L60
.L50:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%rdx, %rax
	movl	%eax, %r8d
	cmpl	$-900, %eax
	jl	.L50
.L60:
	subq	%rdx, %rax
	movq	%rax, %r12
.L53:
	leaq	352(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	nghttp2_frame_add_pad@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L50
	movq	%r12, 16(%r13)
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$-902, %r8d
	jmp	.L50
	.cfi_endproc
.LFE120:
	.size	session_headers_add_pad, .-session_headers_add_pad
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_session.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"stream->closed_next == NULL"
.LC2:
	.string	"stream->closed_prev == NULL"
	.text
	.p2align 4
	.type	find_stream_on_goaway_func, @function
find_stream_on_goaway_func:
.LFB128:
	.cfi_startproc
	endbr64
	movl	192(%rdi), %eax
	movl	20(%rsi), %edx
	testl	%eax, %eax
	je	.L62
	movq	(%rsi), %r8
	movl	%eax, %ecx
	andl	$1, %ecx
	cmpb	$0, 2498(%r8)
	jne	.L82
	testl	%ecx, %ecx
	je	.L62
.L85:
	testl	%edx, %edx
	jne	.L79
.L65:
	cmpl	$5, 228(%rdi)
	je	.L79
	testb	$2, 236(%rdi)
	jne	.L79
	cmpl	16(%rsi), %eax
	jle	.L79
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, 160(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L83
	cmpq	$0, 152(%rdi)
	jne	.L84
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L68
	movq	%rax, 160(%rdi)
	movq	%rdi, 8(%rsi)
.L64:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore 6
	testl	%edx, %edx
	jne	.L65
.L79:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	$1, %ecx
	testl	%ecx, %ecx
	je	.L62
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	%rdi, 8(%rsi)
	jmp	.L64
.L84:
	leaq	__PRETTY_FUNCTION__.6660(%rip), %rcx
	movl	$2438, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
.L83:
	leaq	__PRETTY_FUNCTION__.6660(%rip), %rcx
	movl	$2437, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE128:
	.size	find_stream_on_goaway_func, .-find_stream_on_goaway_func
	.p2align 4
	.type	session_is_closing, @function
session_is_closing:
.LFB105:
	.cfi_startproc
	testb	$3, 2499(%rdi)
	je	.L87
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	nghttp2_map_size@PLT
	subq	2320(%rbx), %rax
	cmpq	2328(%rbx), %rax
	jne	.L90
	movzbl	2499(%rbx), %eax
	testb	$12, %al
	je	.L90
	testb	$2, %al
	jne	.L91
	cmpq	$0, 344(%rbx)
	jne	.L90
	cmpq	$0, 272(%rbx)
	je	.L99
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%eax, %eax
.L86:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	cmpq	$0, 296(%rbx)
	jne	.L90
	leaq	64(%rbx), %rdi
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	jne	.L92
	movl	2404(%rbx), %eax
	testl	%eax, %eax
	jg	.L90
.L92:
	cmpq	$0, 320(%rbx)
	je	.L91
	movl	2436(%rbx), %eax
	cmpq	%rax, 2288(%rbx)
	setnb	%al
	movzbl	%al, %eax
	jmp	.L86
	.cfi_endproc
.LFE105:
	.size	session_is_closing, .-session_is_closing
	.section	.rodata.str1.1
.LC7:
	.string	"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"
	.text
	.p2align 4
	.type	session_new, @function
session_new:
.LFB80:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	testq	%r9, %r9
	je	.L177
.L101:
	movl	$2536, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_mem_calloc@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L124
	movdqu	(%r12), %xmm1
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movl	$5, %ecx
	movl	$16, %r8d
	movups	%xmm1, 2200(%rax)
	movdqu	16(%r12), %xmm2
	movups	%xmm2, 2216(%rax)
	movq	32(%r12), %rdx
	movq	%rdx, 2232(%rax)
	movq	(%r15), %rdi
	xorl	%edx, %edx
	leaq	2200(%rdi), %r12
	addq	$32, %rdi
	pushq	%r12
	pushq	$0
	pushq	$0
	call	nghttp2_stream_init@PLT
	movq	(%r15), %rax
	addq	$32, %rsp
	movdqa	.LC3(%rip), %xmm0
	movabsq	$9223372034707292159, %rcx
	movl	$65535, 2420(%rax)
	movb	$0, 2499(%rax)
	movups	%xmm0, 2404(%rax)
	movq	(%r15), %rax
	movq	%rcx, 2396(%rax)
	movl	$-1, 2488(%rax)
	movb	$1, 2496(%rax)
	testl	%r13d, %r13d
	jne	.L178
.L103:
	movq	(%r15), %rdi
	movdqa	.LC4(%rip), %xmm0
	movl	$4096, %esi
	movabsq	$-4294950912, %rax
	movups	%xmm0, 2456(%rdi)
	movdqa	.LC5(%rip), %xmm0
	movq	%rax, 2472(%rdi)
	movups	%xmm0, 2428(%rdi)
	movdqa	.LC6(%rip), %xmm0
	movq	$200, 2312(%rdi)
	movq	%rax, 2444(%rdi)
	movq	$32, 2368(%rdi)
	movups	%xmm0, 2352(%rdi)
	testq	%r14, %r14
	je	.L104
	movl	32(%r14), %eax
	testb	$1, %al
	je	.L105
	movl	48(%r14), %r9d
	testl	%r9d, %r9d
	je	.L105
	orl	$1, 2484(%rdi)
.L105:
	testb	$2, %al
	je	.L106
	movl	36(%r14), %edx
	movl	%edx, 2436(%rdi)
.L106:
	testb	$16, %al
	je	.L107
	movl	40(%r14), %ecx
	movq	%rcx, 2312(%rdi)
.L107:
	testb	$4, %al
	je	.L108
	movl	52(%r14), %r8d
	testl	%r8d, %r8d
	je	.L108
	orl	$2, 2484(%rdi)
.L108:
	testb	$8, %al
	je	.L109
	movl	56(%r14), %esi
	testl	%esi, %esi
	je	.L109
	orl	$4, 2484(%rdi)
.L109:
	testb	$32, %al
	jne	.L179
	testb	$-128, %al
	je	.L111
.L182:
	movl	44(%r14), %edx
	movl	%edx, 2492(%rdi)
.L111:
	testb	$64, %al
	je	.L112
	movl	60(%r14), %ecx
	testl	%ecx, %ecx
	je	.L112
	orl	$8, 2484(%rdi)
.L112:
	testb	$1, %ah
	je	.L113
	movq	(%r14), %rdx
	movq	%rdx, 2360(%rdi)
.L113:
	movl	$4096, %esi
	testb	$2, %ah
	je	.L114
	movq	8(%r14), %rsi
.L114:
	testb	$4, %ah
	je	.L115
	movl	64(%r14), %edx
	testl	%edx, %edx
	je	.L115
	orl	$16, 2484(%rdi)
.L115:
	testb	$8, %ah
	je	.L116
	movq	16(%r14), %rdx
	movq	%rdx, 2352(%rdi)
.L116:
	testb	$16, %ah
	jne	.L180
.L104:
	addq	$664, %rdi
	movq	%r12, %rdx
	call	nghttp2_hd_deflate_init2@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L181
.L117:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
.L100:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	(%r15), %rax
	movb	$1, 2498(%rax)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L180:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L104
	movq	%rax, 2368(%rdi)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L179:
	movdqu	68(%r14), %xmm3
	movups	%xmm3, 2501(%rdi)
	movdqu	84(%r14), %xmm4
	movups	%xmm4, 2517(%rdi)
	movl	32(%r14), %eax
	movq	(%r15), %rdi
	testb	$-128, %al
	je	.L111
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L181:
	movq	(%r15), %rax
	movq	%r12, %rsi
	leaq	1776(%rax), %rdi
	call	nghttp2_hd_inflate_init@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L118
	movq	(%r15), %rdi
	movq	%r12, %rsi
	call	nghttp2_map_init@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L119
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	2360(%rdi), %rax
	addq	$16393, %rax
	cmpq	$16393, %rax
	jbe	.L120
	movabsq	$-11252131312498201, %rdx
	mulq	%rdx
	shrq	$14, %rdx
.L120:
	addq	$352, %rdi
	movq	%r12, %r9
	movl	$10, %r8d
	movl	$1, %ecx
	movl	$16394, %esi
	call	nghttp2_bufs_init3@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L183
	movq	(%r15), %r14
	movq	%r12, %rsi
	movq	344(%r14), %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	344(%r14), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	leaq	352(%r14), %rdi
	movq	$0, 344(%r14)
	call	nghttp2_bufs_reset@PLT
	movdqu	(%rbx), %xmm5
	movq	(%r15), %rax
	movl	$0, 416(%r14)
	movups	%xmm5, 2016(%rax)
	movdqu	16(%rbx), %xmm6
	movups	%xmm6, 2032(%rax)
	movdqu	32(%rbx), %xmm7
	movups	%xmm7, 2048(%rax)
	movdqu	48(%rbx), %xmm5
	movups	%xmm5, 2064(%rax)
	movdqu	64(%rbx), %xmm6
	movups	%xmm6, 2080(%rax)
	movdqu	80(%rbx), %xmm7
	movups	%xmm7, 2096(%rax)
	movdqu	96(%rbx), %xmm5
	movups	%xmm5, 2112(%rax)
	movdqu	112(%rbx), %xmm6
	movups	%xmm6, 2128(%rax)
	movdqu	128(%rbx), %xmm7
	movups	%xmm7, 2144(%rax)
	movdqu	144(%rbx), %xmm3
	movups	%xmm3, 2160(%rax)
	movdqu	160(%rbx), %xmm4
	movups	%xmm4, 2176(%rax)
	movq	176(%rbx), %rdx
	movq	-56(%rbp), %rbx
	movq	%rdx, 2192(%rax)
	movq	%rbx, 2240(%rax)
	movq	(%r15), %rdi
	call	session_inbound_frame_reset
	movl	nghttp2_enable_strict_preface(%rip), %r14d
	testl	%r14d, %r14d
	je	.L100
	movq	(%r15), %rdi
	testl	%r13d, %r13d
	je	.L122
	testb	$2, 2484(%rdi)
	jne	.L123
	movl	$0, 648(%rdi)
	xorl	%r14d, %r14d
	movq	$24, 632(%rdi)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L183:
	movq	(%r15), %rdi
	call	nghttp2_map_free@PLT
.L119:
	movq	(%r15), %rax
	leaq	1776(%rax), %rdi
	call	nghttp2_hd_inflate_free@PLT
.L118:
	movq	(%r15), %rax
	leaq	664(%rax), %rdi
	call	nghttp2_hd_deflate_free@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L177:
	call	nghttp2_mem_default@PLT
	movq	%rax, %r12
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$352, %rdi
	movl	$3, 64(%rdi)
	movl	$24, %edx
	xorl	%r14d, %r14d
	movl	$1, 296(%rdi)
	leaq	.LC7(%rip), %rsi
	call	nghttp2_bufs_add@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$1, 648(%rdi)
	xorl	%r14d, %r14d
	jmp	.L100
.L124:
	movl	$-901, %r14d
	jmp	.L100
	.cfi_endproc
.LFE80:
	.size	session_new, .-session_new
	.p2align 4
	.type	session_after_frame_sent2, @function
session_after_frame_sent2:
.LFB132:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	352(%rbx), %r13
	leaq	2200(%rbx), %r12
	subq	$16, %rsp
	movq	344(%rdi), %rdi
	movzbl	12(%rdi), %eax
	testb	%al, %al
	je	.L185
	andl	$-5, %eax
	cmpb	$1, %al
	jne	.L208
	movq	%r13, %rdi
	call	nghttp2_bufs_next_present@PLT
	testl	%eax, %eax
	je	.L190
	movq	360(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 360(%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	cmpb	$0, 113(%rdi)
	jne	.L208
	movb	$0, 114(%rdi)
	movl	8(%rdi), %esi
	movq	%rbx, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L190
	testb	$2, 236(%rax)
	jne	.L190
	cmpl	$5, 228(%rax)
	je	.L190
	movq	%rbx, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L191
	testb	$2, 237(%r14)
	jne	.L191
	movl	192(%r14), %edx
	movl	228(%r14), %ecx
	testl	%edx, %edx
	je	.L192
	andl	$1, %edx
	cmpb	$0, 2498(%rbx)
	je	.L193
	xorl	$1, %edx
.L193:
	testl	%edx, %edx
	jne	.L209
.L192:
	cmpl	$2, %ecx
	je	.L194
.L191:
	movq	%r14, %rdi
	call	nghttp2_stream_detach_item@PLT
	cmpl	$-900, %eax
	jl	.L184
.L190:
	movq	344(%rbx), %rdi
.L208:
	movq	%r12, %rsi
	call	nghttp2_outbound_item_free@PLT
	movq	344(%rbx), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	%r13, %rdi
	movq	$0, 344(%rbx)
	call	nghttp2_bufs_reset@PLT
	xorl	%eax, %eax
	movl	$0, 416(%rbx)
.L184:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	subl	$3, %ecx
	cmpl	$1, %ecx
	jbe	.L191
.L194:
	movq	$0, 344(%rbx)
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	%eax, -36(%rbp)
	call	nghttp2_outbound_item_free@PLT
	movq	344(%rbx), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	%r13, %rdi
	movq	$0, 344(%rbx)
	call	nghttp2_bufs_reset@PLT
	movl	-36(%rbp), %eax
	movl	$0, 416(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE132:
	.size	session_after_frame_sent2, .-session_after_frame_sent2
	.p2align 4
	.globl	nghttp2_is_fatal
	.type	nghttp2_is_fatal, @function
nghttp2_is_fatal:
.LFB61:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$-900, %edi
	setl	%al
	ret
	.cfi_endproc
.LFE61:
	.size	nghttp2_is_fatal, .-nghttp2_is_fatal
	.p2align 4
	.globl	nghttp2_session_is_my_stream_id
	.type	nghttp2_session_is_my_stream_id, @function
nghttp2_session_is_my_stream_id:
.LFB74:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L211
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L215
.L211:
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE74:
	.size	nghttp2_session_is_my_stream_id, .-nghttp2_session_is_my_stream_id
	.p2align 4
	.globl	nghttp2_session_get_stream
	.type	nghttp2_session_get_stream, @function
nghttp2_session_get_stream:
.LFB75:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L216
	testb	$2, 236(%rax)
	jne	.L218
	cmpl	$5, 228(%rax)
	movl	$0, %edx
	cmove	%rdx, %rax
.L216:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE75:
	.size	nghttp2_session_get_stream, .-nghttp2_session_get_stream
	.p2align 4
	.globl	nghttp2_session_get_stream_raw
	.type	nghttp2_session_get_stream_raw, @function
nghttp2_session_get_stream_raw:
.LFB76:
	.cfi_startproc
	endbr64
	jmp	nghttp2_map_find@PLT
	.cfi_endproc
.LFE76:
	.size	nghttp2_session_get_stream_raw, .-nghttp2_session_get_stream_raw
	.p2align 4
	.globl	nghttp2_session_client_new
	.type	nghttp2_session_client_new, @function
nghttp2_session_client_new:
.LFB81:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	nghttp2_mem_default@PLT
	movl	$2536, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	nghttp2_mem_calloc@PLT
	testq	%rax, %rax
	je	.L233
	movdqu	(%r12), %xmm1
	movq	%rax, %r13
	subq	$8, %rsp
	xorl	%r9d, %r9d
	leaq	32(%r13), %rdi
	movl	$16, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movups	%xmm1, 2200(%rax)
	movdqu	16(%r12), %xmm2
	movl	$5, %ecx
	leaq	664(%r13), %r14
	movups	%xmm2, 2216(%rax)
	movq	32(%r12), %rax
	leaq	2200(%r13), %r12
	movq	%rax, 2232(%r13)
	pushq	%r12
	pushq	$0
	pushq	$0
	call	nghttp2_stream_init@PLT
	addq	$32, %rsp
	movq	%r12, %rdx
	movq	%r14, %rdi
	movdqa	.LC8(%rip), %xmm0
	movl	$4096, %esi
	movabsq	$281470681743360, %rax
	movb	$0, 2499(%r13)
	movq	%rax, 2416(%r13)
	movabsq	$-4294950912, %rax
	movups	%xmm0, 2396(%r13)
	movdqa	.LC4(%rip), %xmm0
	movl	$0, 2412(%r13)
	movups	%xmm0, 2456(%r13)
	movdqa	.LC5(%rip), %xmm0
	movl	$-1, 2488(%r13)
	movups	%xmm0, 2428(%r13)
	movdqa	.LC6(%rip), %xmm0
	movb	$1, 2496(%r13)
	movq	%rax, 2472(%r13)
	movq	$200, 2312(%r13)
	movq	%rax, 2444(%r13)
	movq	$32, 2368(%r13)
	movups	%xmm0, 2352(%r13)
	call	nghttp2_hd_deflate_init2@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L236
.L227:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
.L225:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	leaq	1776(%r13), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	nghttp2_hd_inflate_init@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L228
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_map_init@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L229
	movq	2360(%r13), %rax
	movl	$1, %edx
	addq	$16393, %rax
	cmpq	$16393, %rax
	jbe	.L230
	movabsq	$-11252131312498201, %rdx
	mulq	%rdx
	shrq	$14, %rdx
.L230:
	leaq	352(%r13), %rax
	movq	%r12, %r9
	movl	$10, %r8d
	movl	$1, %ecx
	movl	$16394, %esi
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	nghttp2_bufs_init3@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L237
	movq	344(%r13), %rdi
	movq	%r12, %rsi
	call	nghttp2_outbound_item_free@PLT
	movq	344(%r13), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	-80(%rbp), %r14
	movq	$0, 344(%r13)
	movq	%r14, %rdi
	call	nghttp2_bufs_reset@PLT
	movdqu	(%rbx), %xmm3
	movq	%r13, %rdi
	movl	$0, 416(%r13)
	movups	%xmm3, 2016(%r13)
	movdqu	16(%rbx), %xmm4
	movups	%xmm4, 2032(%r13)
	movdqu	32(%rbx), %xmm5
	movups	%xmm5, 2048(%r13)
	movdqu	48(%rbx), %xmm6
	movups	%xmm6, 2064(%r13)
	movdqu	64(%rbx), %xmm7
	movups	%xmm7, 2080(%r13)
	movdqu	80(%rbx), %xmm3
	movups	%xmm3, 2096(%r13)
	movdqu	96(%rbx), %xmm4
	movups	%xmm4, 2112(%r13)
	movdqu	112(%rbx), %xmm5
	movups	%xmm5, 2128(%r13)
	movdqu	128(%rbx), %xmm6
	movups	%xmm6, 2144(%r13)
	movdqu	144(%rbx), %xmm7
	movups	%xmm7, 2160(%r13)
	movdqu	160(%rbx), %xmm3
	movups	%xmm3, 2176(%r13)
	movq	176(%rbx), %rax
	movq	%rax, 2192(%r13)
	movq	-64(%rbp), %rax
	movq	%rax, 2240(%r13)
	call	session_inbound_frame_reset
	movl	nghttp2_enable_strict_preface(%rip), %eax
	testl	%eax, %eax
	je	.L232
	movl	$24, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	movl	$1, 648(%r13)
	movl	$3, 416(%r13)
	call	nghttp2_bufs_add@PLT
.L232:
	movl	$1, 2376(%r13)
	movq	-56(%rbp), %rax
	movq	%r13, (%rax)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r13, %rdi
	call	nghttp2_map_free@PLT
.L229:
	movq	-72(%rbp), %rdi
	call	nghttp2_hd_inflate_free@PLT
.L228:
	movq	%r14, %rdi
	call	nghttp2_hd_deflate_free@PLT
	jmp	.L227
.L233:
	movl	$-901, %r15d
	jmp	.L225
	.cfi_endproc
.LFE81:
	.size	nghttp2_session_client_new, .-nghttp2_session_client_new
	.p2align 4
	.globl	nghttp2_session_client_new2
	.type	nghttp2_session_client_new2, @function
nghttp2_session_client_new2:
.LFB82:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	session_new
	testl	%eax, %eax
	jne	.L238
	movq	-32(%rbp), %rdx
	movl	$1, 2376(%rdx)
	movq	%rdx, (%rbx)
.L238:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L242
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L242:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE82:
	.size	nghttp2_session_client_new2, .-nghttp2_session_client_new2
	.p2align 4
	.globl	nghttp2_session_client_new3
	.type	nghttp2_session_client_new3, @function
nghttp2_session_client_new3:
.LFB83:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	session_new
	testl	%eax, %eax
	jne	.L243
	movq	-32(%rbp), %rdx
	movl	$1, 2376(%rdx)
	movq	%rdx, (%rbx)
.L243:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L247
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L247:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE83:
	.size	nghttp2_session_client_new3, .-nghttp2_session_client_new3
	.p2align 4
	.globl	nghttp2_session_server_new
	.type	nghttp2_session_server_new, @function
nghttp2_session_server_new:
.LFB84:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rdx, -56(%rbp)
	call	nghttp2_mem_default@PLT
	movl	$2536, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	nghttp2_mem_calloc@PLT
	testq	%rax, %rax
	je	.L257
	movdqu	(%r12), %xmm1
	movq	%rax, %r15
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	32(%r15), %rdi
	xorl	%r9d, %r9d
	movl	$16, %r8d
	movups	%xmm1, 2200(%rax)
	movdqu	16(%r12), %xmm2
	movl	$5, %ecx
	leaq	664(%r15), %r14
	movups	%xmm2, 2216(%rax)
	movq	32(%r12), %rax
	leaq	2200(%r15), %r12
	movq	%rax, 2232(%r15)
	pushq	%r12
	pushq	$0
	pushq	$0
	call	nghttp2_stream_init@PLT
	movdqa	.LC8(%rip), %xmm0
	addq	$32, %rsp
	movq	%r14, %rdi
	movabsq	$281470681743360, %rax
	movl	$1, %edx
	movl	$0, 2412(%r15)
	movl	$4096, %esi
	movups	%xmm0, 2396(%r15)
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, 2416(%r15)
	movabsq	$-4294950912, %rax
	movups	%xmm0, 2456(%r15)
	movdqa	.LC5(%rip), %xmm0
	movw	%dx, 2498(%r15)
	movq	%r12, %rdx
	movups	%xmm0, 2428(%r15)
	movdqa	.LC6(%rip), %xmm0
	movl	$-1, 2488(%r15)
	movb	$1, 2496(%r15)
	movq	%rax, 2472(%r15)
	movq	$200, 2312(%r15)
	movq	%rax, 2444(%r15)
	movq	$32, 2368(%r15)
	movups	%xmm0, 2352(%r15)
	call	nghttp2_hd_deflate_init2@PLT
	testl	%eax, %eax
	je	.L260
.L250:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, -56(%rbp)
	call	nghttp2_mem_free@PLT
	movl	-56(%rbp), %eax
.L248:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	leaq	1776(%r15), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	nghttp2_hd_inflate_init@PLT
	testl	%eax, %eax
	jne	.L251
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_map_init@PLT
	testl	%eax, %eax
	jne	.L252
	movq	2360(%r15), %rax
	movl	$1, %edx
	addq	$16393, %rax
	cmpq	$16393, %rax
	jbe	.L253
	movabsq	$-11252131312498201, %rdx
	mulq	%rdx
	shrq	$14, %rdx
.L253:
	leaq	352(%r15), %r11
	movq	%r12, %r9
	movl	$10, %r8d
	movl	$1, %ecx
	movq	%r11, %rdi
	movl	$16394, %esi
	movq	%r11, -72(%rbp)
	call	nghttp2_bufs_init3@PLT
	movq	-72(%rbp), %r11
	testl	%eax, %eax
	jne	.L261
	movq	344(%r15), %rdi
	movq	%r12, %rsi
	movl	%eax, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	nghttp2_outbound_item_free@PLT
	movq	344(%r15), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	-64(%rbp), %r11
	movq	$0, 344(%r15)
	movq	%r11, %rdi
	call	nghttp2_bufs_reset@PLT
	movdqu	(%rbx), %xmm3
	movq	-56(%rbp), %rax
	movq	%r15, %rdi
	movl	$0, 416(%r15)
	movups	%xmm3, 2016(%r15)
	movdqu	16(%rbx), %xmm4
	movups	%xmm4, 2032(%r15)
	movdqu	32(%rbx), %xmm5
	movups	%xmm5, 2048(%r15)
	movdqu	48(%rbx), %xmm6
	movups	%xmm6, 2064(%r15)
	movdqu	64(%rbx), %xmm7
	movups	%xmm7, 2080(%r15)
	movdqu	80(%rbx), %xmm3
	movups	%xmm3, 2096(%r15)
	movdqu	96(%rbx), %xmm4
	movups	%xmm4, 2112(%r15)
	movdqu	112(%rbx), %xmm5
	movups	%xmm5, 2128(%r15)
	movdqu	128(%rbx), %xmm6
	movups	%xmm6, 2144(%r15)
	movdqu	144(%rbx), %xmm7
	movups	%xmm7, 2160(%r15)
	movdqu	160(%rbx), %xmm3
	movups	%xmm3, 2176(%r15)
	movq	176(%rbx), %rdx
	movq	%rax, 2240(%r15)
	movq	%rdx, 2192(%r15)
	call	session_inbound_frame_reset
	movl	nghttp2_enable_strict_preface(%rip), %eax
	testl	%eax, %eax
	movl	-72(%rbp), %eax
	je	.L255
	testb	$2, 2484(%r15)
	jne	.L256
	movl	$0, 648(%r15)
	movq	$24, 632(%r15)
.L255:
	movl	$2, 2376(%r15)
	movq	%r15, 0(%r13)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	nghttp2_map_free@PLT
	movl	-56(%rbp), %eax
.L252:
	movq	-64(%rbp), %rdi
	movl	%eax, -56(%rbp)
	call	nghttp2_hd_inflate_free@PLT
	movl	-56(%rbp), %eax
.L251:
	movq	%r14, %rdi
	movl	%eax, -56(%rbp)
	call	nghttp2_hd_deflate_free@PLT
	movl	-56(%rbp), %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$1, 648(%r15)
	jmp	.L255
.L257:
	movl	$-901, %eax
	jmp	.L248
	.cfi_endproc
.LFE84:
	.size	nghttp2_session_server_new, .-nghttp2_session_server_new
	.p2align 4
	.globl	nghttp2_session_server_new2
	.type	nghttp2_session_server_new2, @function
nghttp2_session_server_new2:
.LFB85:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	session_new
	testl	%eax, %eax
	jne	.L262
	movq	-32(%rbp), %rdx
	movl	$2, 2376(%rdx)
	movq	%rdx, (%rbx)
.L262:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L266
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L266:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE85:
	.size	nghttp2_session_server_new2, .-nghttp2_session_server_new2
	.p2align 4
	.globl	nghttp2_session_server_new3
	.type	nghttp2_session_server_new3, @function
nghttp2_session_server_new3:
.LFB86:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	movq	%rcx, %r8
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	session_new
	testl	%eax, %eax
	jne	.L267
	movq	-32(%rbp), %rdx
	movl	$2, 2376(%rdx)
	movq	%rdx, (%rbx)
.L267:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L271
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L271:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE86:
	.size	nghttp2_session_server_new3, .-nghttp2_session_server_new3
	.p2align 4
	.globl	nghttp2_session_del
	.type	nghttp2_session_del, @function
nghttp2_session_del:
.LFB91:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L272
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	2200(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	2280(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L274
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rbx, %r14
	movq	%r12, %rdi
	movq	(%rbx), %rbx
	movq	8(%r14), %rsi
	call	nghttp2_mem_free@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	testq	%rbx, %rbx
	jne	.L275
.L274:
	leaq	32(%r13), %rdi
	call	nghttp2_stream_free@PLT
	movq	%r13, %rdx
	leaq	free_streams(%rip), %rsi
	movq	%r13, %rdi
	call	nghttp2_map_each_free@PLT
	movq	%r13, %rdi
	call	nghttp2_map_free@PLT
	movq	272(%r13), %rbx
	testq	%rbx, %rbx
	je	.L276
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rbx, %r14
	movq	%r12, %rsi
	movq	136(%rbx), %rbx
	movq	%r14, %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	testq	%rbx, %rbx
	jne	.L277
.L276:
	movq	296(%r13), %rbx
	testq	%rbx, %rbx
	je	.L278
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%rbx, %r14
	movq	%r12, %rsi
	movq	136(%rbx), %rbx
	movq	%r14, %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	testq	%rbx, %rbx
	jne	.L279
.L278:
	movq	320(%r13), %rbx
	testq	%rbx, %rbx
	je	.L280
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%rbx, %r14
	movq	%r12, %rsi
	movq	136(%rbx), %rbx
	movq	%r14, %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	testq	%rbx, %rbx
	jne	.L281
.L280:
	movq	344(%r13), %rdi
	movq	%r12, %rsi
	leaq	352(%r13), %r14
	call	nghttp2_outbound_item_free@PLT
	movq	344(%r13), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	%r14, %rdi
	movq	$0, 344(%r13)
	call	nghttp2_bufs_reset@PLT
	movq	%r13, %rdi
	movl	$0, 416(%r13)
	call	session_inbound_frame_reset
	leaq	664(%r13), %rdi
	call	nghttp2_hd_deflate_free@PLT
	leaq	1776(%r13), %rdi
	call	nghttp2_hd_inflate_free@PLT
	movq	%r14, %rdi
	call	nghttp2_bufs_free@PLT
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	nghttp2_mem_free@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	ret
	.cfi_endproc
.LFE91:
	.size	nghttp2_session_del, .-nghttp2_session_del
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"stream->state == NGHTTP2_STREAM_IDLE"
	.align 8
.LC10:
	.string	"nghttp2_stream_in_dep_tree(stream)"
	.section	.rodata.str1.1
.LC11:
	.string	"dep_stream"
	.text
	.p2align 4
	.globl	nghttp2_session_open_stream
	.type	nghttp2_session_open_stream, @function
nghttp2_session_open_stream:
.LFB95:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movl	%edx, -96(%rbp)
	movq	%r9, -104(%rbp)
	movb	%dl, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	2200(%rdi), %rax
	movq	%rax, -120(%rbp)
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L302
	cmpl	$5, 228(%rax)
	movq	%rax, %r12
	jne	.L370
	movq	%rax, %rdi
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L371
	movq	152(%r12), %rax
	movq	160(%r12), %rdx
	testq	%rax, %rax
	je	.L305
	movq	%rdx, 160(%rax)
.L306:
	testq	%rdx, %rdx
	je	.L307
	movq	%rax, 152(%rdx)
.L308:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 152(%r12)
	subq	$1, 2328(%r15)
	call	nghttp2_stream_dep_remove@PLT
	movl	%eax, -108(%rbp)
	testl	%eax, %eax
	jne	.L368
.L309:
	movl	(%rbx), %esi
	xorl	%r11d, %r11d
	testl	%esi, %esi
	jne	.L372
.L312:
	movzbl	-96(%rbp), %edx
	movzbl	-88(%rbp), %eax
	movl	4(%rbx), %r8d
	orl	$1, %edx
	cmpl	$4, %r13d
	cmove	%edx, %eax
	movl	-108(%rbp), %edx
	movb	%al, -88(%rbp)
	testl	%edx, %edx
	jne	.L373
	movb	%al, 236(%r12)
	movq	-104(%rbp), %rax
	movl	%r13d, 228(%r12)
	movl	%r8d, 216(%r12)
	movq	%rax, 168(%r12)
.L321:
	cmpl	$4, %r13d
	je	.L322
	cmpl	$5, %r13d
	je	.L323
	testl	%r14d, %r14d
	je	.L330
	andl	$1, %r14d
	cmpb	$0, 2498(%r15)
	je	.L331
	xorl	$1, %r14d
.L331:
	testl	%r14d, %r14d
	je	.L330
	addq	$1, 2288(%r15)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L323:
	movq	2272(%r15), %rax
	testq	%rax, %rax
	je	.L328
	movq	%r12, 160(%rax)
	movq	%rax, 152(%r12)
.L329:
	movl	(%rbx), %eax
	addq	$1, 2328(%r15)
	movq	%r12, 2272(%r15)
	testl	%eax, %eax
	je	.L374
.L332:
	testq	%r11, %r11
	jne	.L333
	leaq	__PRETTY_FUNCTION__.6434(%rip), %rcx
	movl	$1137, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r15, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L375
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	nghttp2_stream_in_dep_tree@PLT
	movq	-128(%rbp), %r11
	testl	%eax, %eax
	jne	.L312
.L318:
	leaq	-68(%rbp), %rbx
	movq	%r11, -128(%rbp)
	movq	%rbx, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	movq	-128(%rbp), %r11
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L373:
	movzbl	%al, %edx
	movl	2468(%r15), %eax
	movl	%r13d, %ecx
	movl	%r14d, %esi
	subq	$8, %rsp
	pushq	-120(%rbp)
	movl	2440(%r15), %r9d
	movq	%r12, %rdi
	pushq	-104(%rbp)
	pushq	%rax
	movq	%r11, -96(%rbp)
	call	nghttp2_stream_init@PLT
	addq	$32, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_map_insert@PLT
	movq	-96(%rbp), %r11
	testl	%eax, %eax
	je	.L321
	movq	%r12, %rdi
	call	nghttp2_stream_free@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L322:
	testl	%r14d, %r14d
	je	.L325
	andl	$1, %r14d
	cmpb	$0, 2498(%r15)
	je	.L326
	xorl	$1, %r14d
.L326:
	testl	%r14d, %r14d
	je	.L325
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%r11, -88(%rbp)
	call	nghttp2_stream_shutdown@PLT
	movq	-88(%rbp), %r11
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%r11, -88(%rbp)
	call	nghttp2_stream_shutdown@PLT
	addq	$1, 2304(%r15)
	movq	-88(%rbp), %r11
.L327:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L332
.L374:
	leaq	32(%r15), %r11
.L333:
	cmpb	$0, 8(%rbx)
	movq	%r12, %rsi
	movq	%r11, %rdi
	je	.L334
	call	nghttp2_stream_dep_insert@PLT
	testl	%eax, %eax
	jne	.L368
.L301:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	call	nghttp2_stream_dep_add@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L375:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L318
	movl	%eax, %edx
	andl	$1, %edx
	cmpb	$0, 2498(%r15)
	jne	.L377
	testl	%edx, %edx
	je	.L316
.L335:
	cmpl	2380(%r15), %eax
	jle	.L318
.L317:
	leaq	-68(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -128(%rbp)
	call	nghttp2_priority_spec_default_init@PLT
	movq	-128(%rbp), %rcx
	movl	(%rbx), %esi
	xorl	%r9d, %r9d
	movl	$5, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	nghttp2_session_open_stream
	movq	%rax, %r11
	testq	%rax, %rax
	jne	.L312
	movl	-108(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L368
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-120(%rbp), %rdi
	movq	%r12, %rsi
	call	nghttp2_mem_free@PLT
	.p2align 4,,10
	.p2align 3
.L368:
	xorl	%r12d, %r12d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%rax, 2272(%r15)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%rdx, 2264(%r15)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L330:
	addq	$1, 2296(%r15)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%r12, 2264(%r15)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$240, %esi
	leaq	2200(%r15), %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L368
	movl	$1, -108(%rbp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L377:
	testl	%edx, %edx
	je	.L335
.L316:
	cmpl	2384(%r15), %eax
	jle	.L318
	jmp	.L317
.L376:
	call	__stack_chk_fail@PLT
.L370:
	leaq	__PRETTY_FUNCTION__.6434(%rip), %rcx
	movl	$1032, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	__assert_fail@PLT
.L371:
	leaq	__PRETTY_FUNCTION__.6434(%rip), %rcx
	movl	$1033, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE95:
	.size	nghttp2_session_open_stream, .-nghttp2_session_open_stream
	.p2align 4
	.globl	nghttp2_session_add_item
	.type	nghttp2_session_add_item, @function
nghttp2_session_add_item:
.LFB93:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	8(%rsi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L379
	movq	%rax, %rdi
	movzbl	12(%r12), %eax
	testb	$2, 236(%rdi)
	jne	.L380
	movl	228(%rdi), %ecx
	cmpl	$5, %ecx
	je	.L380
	cmpb	$8, %al
	ja	.L408
	leaq	.L395(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L395:
	.long	.L382-.L395
	.long	.L387-.L395
	.long	.L408-.L395
	.long	.L389-.L395
	.long	.L388-.L395
	.long	.L391-.L395
	.long	.L388-.L395
	.long	.L408-.L395
	.long	.L393-.L395
	.text
	.p2align 4,,10
	.p2align 3
.L394:
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L408
	movb	$1, 2500(%rbx)
.L408:
	leaq	296(%rbx), %rdi
	movq	%r12, %rsi
	call	nghttp2_outbound_queue_push@PLT
	xorl	%eax, %eax
	movb	$1, 144(%r12)
.L378:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L409
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movzbl	12(%r12), %eax
.L380:
	cmpb	$8, %al
	ja	.L408
	leaq	.L396(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L396:
	.long	.L400-.L396
	.long	.L384-.L396
	.long	.L408-.L396
	.long	.L408-.L396
	.long	.L388-.L396
	.long	.L400-.L396
	.long	.L388-.L396
	.long	.L408-.L396
	.long	.L394-.L396
	.text
.L388:
	leaq	272(%rbx), %rdi
	movq	%r12, %rsi
	call	nghttp2_outbound_queue_push@PLT
	xorl	%eax, %eax
	movb	$1, 144(%r12)
	jmp	.L378
.L382:
	cmpq	$0, 176(%rdi)
	jne	.L398
	movq	%r12, %rsi
	call	nghttp2_stream_attach_item@PLT
	jmp	.L378
.L389:
	movl	$3, 228(%rdi)
	jmp	.L408
.L393:
	movb	$1, 239(%rdi)
	jmp	.L408
.L391:
	movl	192(%rdi), %esi
	leaq	-52(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r13, %rdi
	call	nghttp2_priority_spec_init@PLT
	movl	40(%r12), %esi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	112(%r12), %r9
	movl	$4, %r8d
	movq	%rbx, %rdi
	call	nghttp2_session_open_stream
	movq	%rax, %r8
	movl	$-901, %eax
	testq	%r8, %r8
	jne	.L408
	jmp	.L378
.L387:
	movl	56(%r12), %edx
	testl	%edx, %edx
	je	.L385
	cmpl	$4, %ecx
	jne	.L408
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L400:
	movl	$-510, %eax
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L384:
	movl	56(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L408
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	320(%rbx), %rdi
	movq	%r12, %rsi
	call	nghttp2_outbound_queue_push@PLT
	xorl	%eax, %eax
	movb	$1, 144(%r12)
	jmp	.L378
.L398:
	movl	$-529, %eax
	jmp	.L378
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE93:
	.size	nghttp2_session_add_item, .-nghttp2_session_add_item
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"headers_frame->hd.type == NGHTTP2_HEADERS"
	.text
	.p2align 4
	.globl	nghttp2_session_add_rst_stream
	.type	nghttp2_session_add_rst_stream, @function
nghttp2_session_add_rst_stream:
.LFB94:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	2200(%rdi), %rbx
	subq	$24, %rsp
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L411
	testb	$2, 236(%rax)
	jne	.L411
	cmpl	$3, 228(%rax)
	je	.L417
.L411:
	cmpb	$0, 2498(%r12)
	jne	.L413
	testl	%r14d, %r14d
	je	.L413
	testb	$1, %r14b
	je	.L413
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L413
	cmpb	$1, 12(%rax)
	jne	.L435
	movl	8(%rax), %edx
	cmpl	%r14d, %edx
	jg	.L413
	cmpl	%r14d, 2376(%r12)
	jbe	.L413
.L416:
	cmpl	%edx, %r14d
	jg	.L436
	jl	.L413
	cmpb	$0, 124(%rax)
	jne	.L413
	movl	%r15d, 120(%rax)
	movb	$1, 124(%rax)
	xorl	%eax, %eax
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L413:
	movl	$152, %esi
	movq	%rbx, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L418
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	%r15d, %edx
	call	nghttp2_frame_rst_stream_init@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L437
.L410:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_rst_stream_free@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L413
	movl	8(%rax), %edx
	jmp	.L416
.L418:
	movl	$-901, %eax
	jmp	.L410
.L435:
	leaq	__PRETTY_FUNCTION__.6414(%rip), %rcx
	movl	$970, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE94:
	.size	nghttp2_session_add_rst_stream, .-nghttp2_session_add_rst_stream
	.p2align 4
	.type	update_remote_initial_window_size_func, @function
update_remote_initial_window_size_func:
.LFB165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	12(%rsi), %edx
	movl	8(%rsi), %esi
	call	nghttp2_stream_update_remote_initial_window_size@PLT
	testl	%eax, %eax
	jne	.L447
	movl	%eax, %r13d
	movl	196(%r12), %eax
	testl	%eax, %eax
	jg	.L448
.L438:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	movq	%r12, %rdi
	call	nghttp2_stream_check_deferred_by_flow_control@PLT
	testl	%eax, %eax
	je	.L438
	movq	%r12, %rdi
	movl	$4, %esi
	call	nghttp2_stream_resume_deferred_item@PLT
	cmpl	$-901, %eax
	cmovle	%eax, %r13d
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	movl	192(%r12), %esi
	movq	(%rbx), %rdi
	addq	$8, %rsp
	movl	$3, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_session_add_rst_stream
	.cfi_endproc
.LFE165:
	.size	update_remote_initial_window_size_func, .-update_remote_initial_window_size_func
	.p2align 4
	.type	session_terminate_session.part.0, @function
session_terminate_session.part.0:
.LFB282:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	2200(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	%edx, -52(%rbp)
	movl	$15, 648(%rdi)
	testq	%rcx, %rcx
	je	.L450
	movq	%rcx, %rdi
	movq	%rcx, -64(%rbp)
	call	strlen@PLT
	testl	%r12d, %r12d
	movq	-64(%rbp), %r9
	movq	%rax, %r14
	je	.L451
.L457:
	movl	%r12d, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rbx)
	jne	.L467
.L452:
	testl	%eax, %eax
	jne	.L460
.L451:
	movq	%r9, -72(%rbp)
	testq	%r14, %r14
	je	.L459
	leaq	8(%r14), %rax
	cmpq	$16384, %rax
	ja	.L460
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L461
	movq	-72(%rbp), %r9
	movq	%r14, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
.L454:
	movl	$152, %esi
	movq	%r15, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L468
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movq	-64(%rbp), %rcx
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	cmpl	%r12d, 2396(%rbx)
	movq	%r13, %rdi
	movq	%r14, %r8
	cmovle	2396(%rbx), %esi
	call	nghttp2_frame_goaway_init@PLT
	movb	$1, 96(%r13)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L456
	orb	$1, 2499(%rbx)
.L449:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	xorl	$1, %eax
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L450:
	xorl	%r14d, %r14d
	testl	%esi, %esi
	jne	.L457
	movq	$0, -64(%rbp)
	xorl	%r14d, %r14d
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L459:
	movq	$0, -64(%rbp)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_goaway_free@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movl	$-501, %eax
	jmp	.L449
.L461:
	movl	$-901, %eax
	jmp	.L449
.L468:
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L449
	.cfi_endproc
.LFE282:
	.size	session_terminate_session.part.0, .-session_terminate_session.part.0
	.p2align 4
	.globl	nghttp2_session_terminate_session_with_reason
	.type	nghttp2_session_terminate_session_with_reason, @function
nghttp2_session_terminate_session_with_reason:
.LFB73:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	testb	$1, 2499(%rdi)
	jne	.L470
	movl	2388(%rdi), %r8d
	movl	%esi, %edx
	movl	%r8d, %esi
	jmp	session_terminate_session.part.0
	.p2align 4,,10
	.p2align 3
.L470:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE73:
	.size	nghttp2_session_terminate_session_with_reason, .-nghttp2_session_terminate_session_with_reason
	.p2align 4
	.type	session_update_consumed_size, @function
session_update_consumed_size:
.LFB192:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$2147483647, %edx
	pushq	%r13
	subq	%r9, %rdx
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	(%rsi), %rsi
	movl	16(%rbp), %edi
	cmpq	%rdx, %rsi
	ja	.L482
	addl	%esi, %r9d
	movl	%r9d, (%r12)
	testb	%cl, %cl
	je	.L483
.L475:
	xorl	%eax, %eax
.L471:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	cmpl	%r9d, (%r14)
	movl	%r9d, %ebx
	cmovle	(%r14), %ebx
	movl	%r8d, -52(%rbp)
	movl	%ebx, %esi
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L475
	leaq	2200(%r13), %rax
	movl	$152, %esi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L477
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	-52(%rbp), %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%ebx, %ecx
	movl	%r8d, %edx
	call	nghttp2_frame_window_update_init@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L476
	subl	%ebx, (%r14)
	subl	%ebx, (%r12)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L482:
	testb	$1, 2499(%r13)
	jne	.L475
	movl	2388(%r13), %esi
	addq	$24, %rsp
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	popq	%rbx
	movl	$3, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	session_terminate_session.part.0
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	-64(%rbp), %rdi
	movq	%r15, %rsi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L471
.L477:
	movl	$-901, %eax
	jmp	.L471
	.cfi_endproc
.LFE192:
	.size	session_update_consumed_size, .-session_update_consumed_size
	.p2align 4
	.type	update_local_initial_window_size_func, @function
update_local_initial_window_size_func:
.LFB167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	12(%rsi), %edx
	movl	8(%rsi), %esi
	call	nghttp2_stream_update_local_initial_window_size@PLT
	testl	%eax, %eax
	jne	.L495
	movl	%eax, %r12d
	movq	(%rbx), %rax
	testb	$1, 2484(%rax)
	jne	.L484
	cmpb	$0, 239(%r13)
	je	.L496
.L484:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movl	192(%r13), %esi
	movq	(%rbx), %rdi
	addq	$24, %rsp
	movl	$3, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	nghttp2_session_add_rst_stream
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movl	200(%r13), %esi
	movl	212(%r13), %edi
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L484
	movq	(%rbx), %rbx
	movl	200(%r13), %eax
	movl	$152, %esi
	movl	192(%r13), %r12d
	leaq	2200(%rbx), %r14
	movl	%eax, -52(%rbp)
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L490
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	-52(%rbp), %ecx
	movl	%r12d, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	nghttp2_frame_window_update_init@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	nghttp2_session_add_item
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L489
	movl	$0, 200(%r13)
	jmp	.L484
.L489:
	movq	%r15, %rdi
	call	nghttp2_frame_window_update_free@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	jmp	.L484
.L490:
	movl	$-901, %r12d
	jmp	.L484
	.cfi_endproc
.LFE167:
	.size	update_local_initial_window_size_func, .-update_local_initial_window_size_func
	.p2align 4
	.globl	nghttp2_session_terminate_session2
	.type	nghttp2_session_terminate_session2, @function
nghttp2_session_terminate_session2:
.LFB72:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$1, 2499(%rdi)
	jne	.L509
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	2200(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	$15, 648(%rdi)
	testl	%esi, %esi
	je	.L499
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	je	.L500
	xorl	$1, %eax
.L500:
	testl	%eax, %eax
	jne	.L504
.L499:
	movl	$152, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L512
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	movl	%r13d, %edx
	cmpl	%r12d, 2396(%rbx)
	cmovle	2396(%rbx), %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	call	nghttp2_frame_goaway_init@PLT
	movb	$1, 96(%r15)
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L502
	orb	$1, 2499(%rbx)
.L497:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_goaway_free@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L504:
	.cfi_restore_state
	movl	$-501, %eax
	jmp	.L497
.L512:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L497
	.cfi_endproc
.LFE72:
	.size	nghttp2_session_terminate_session2, .-nghttp2_session_terminate_session2
	.p2align 4
	.globl	nghttp2_session_terminate_session
	.type	nghttp2_session_terminate_session, @function
nghttp2_session_terminate_session:
.LFB71:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$1, 2499(%rdi)
	jne	.L525
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	2200(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	2388(%rdi), %r15d
	movl	$15, 648(%rdi)
	testl	%r15d, %r15d
	je	.L515
	movl	%r15d, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	je	.L516
	xorl	$1, %eax
.L516:
	testl	%eax, %eax
	jne	.L520
.L515:
	movl	$152, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L528
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%r13d, %edx
	cmpl	%r15d, 2396(%rbx)
	cmovle	2396(%rbx), %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	call	nghttp2_frame_goaway_init@PLT
	movb	$1, 96(%r12)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L518
	orb	$1, 2499(%rbx)
.L513:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_goaway_free@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L520:
	.cfi_restore_state
	movl	$-501, %eax
	jmp	.L513
.L528:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L513
	.cfi_endproc
.LFE71:
	.size	nghttp2_session_terminate_session, .-nghttp2_session_terminate_session
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"pri_spec->stream_id != stream->stream_id"
	.text
	.p2align 4
	.globl	nghttp2_session_reprioritize_stream
	.type	nghttp2_session_reprioritize_stream, @function
nghttp2_session_reprioritize_stream:
.LFB92:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	192(%rsi), %eax
	cmpl	%eax, (%rdx)
	je	.L563
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	movq	%rdx, %rbx
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L529
	movl	(%rbx), %esi
	testl	%esi, %esi
	jne	.L564
.L532:
	leaq	32(%r13), %r14
.L542:
	cmpq	%r14, 120(%r12)
	je	.L565
.L546:
	movq	%r12, %rdi
	call	nghttp2_stream_dep_remove_subtree@PLT
	movl	4(%rbx), %eax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%eax, 216(%r12)
	cmpb	$0, 8(%rbx)
	je	.L547
	call	nghttp2_stream_dep_insert_subtree@PLT
.L529:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L566
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	%r13, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L567
	movq	%rax, %rdi
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	jne	.L535
.L539:
	leaq	-52(%rbp), %rbx
	movq	%rbx, %rdi
	call	nghttp2_priority_spec_default_init@PLT
.L535:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L532
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_stream_dep_find_ancestor@PLT
	testl	%eax, %eax
	jne	.L543
.L545:
	testq	%r14, %r14
	jne	.L542
	leaq	__PRETTY_FUNCTION__.6384(%rip), %rcx
	movl	$818, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L565:
	cmpb	$0, 8(%rbx)
	jne	.L546
	movl	4(%rbx), %esi
	movq	%r12, %rdi
	call	nghttp2_stream_change_weight@PLT
	xorl	%eax, %eax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L547:
	call	nghttp2_stream_dep_add_subtree@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L567:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L539
	movl	%eax, %edx
	andl	$1, %edx
	cmpb	$0, 2498(%r13)
	jne	.L568
	testl	%edx, %edx
	je	.L537
.L549:
	cmpl	2380(%r13), %eax
	jle	.L539
.L538:
	leaq	-52(%rbp), %r14
	movq	%r14, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	movl	(%rbx), %esi
	movq	%r14, %rcx
	xorl	%r9d, %r9d
	movl	$5, %r8d
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	nghttp2_session_open_stream
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L535
	movl	$-901, %eax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%r14, %rdi
	call	nghttp2_stream_dep_remove_subtree@PLT
	movq	120(%r12), %rdi
	movq	%r14, %rsi
	call	nghttp2_stream_dep_add_subtree@PLT
	testl	%eax, %eax
	je	.L545
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L568:
	testl	%edx, %edx
	je	.L549
.L537:
	cmpl	2384(%r13), %eax
	jle	.L539
	jmp	.L538
.L566:
	call	__stack_chk_fail@PLT
.L563:
	leaq	__PRETTY_FUNCTION__.6384(%rip), %rcx
	movl	$778, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE92:
	.size	nghttp2_session_reprioritize_stream, .-nghttp2_session_reprioritize_stream
	.p2align 4
	.globl	nghttp2_session_close_stream
	.type	nghttp2_session_close_stream, @function
nghttp2_session_close_stream:
.LFB96:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	2200(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L592
	movq	%rax, %r12
	testb	$2, 236(%rax)
	jne	.L592
	cmpl	$5, 228(%rax)
	je	.L592
	movq	176(%rax), %r8
	testq	%r8, %r8
	movq	%r8, -56(%rbp)
	je	.L572
	movq	%rax, %rdi
	call	nghttp2_stream_detach_item@PLT
	testl	%eax, %eax
	jne	.L569
	movq	-56(%rbp), %r8
	cmpb	$0, 144(%r8)
	je	.L605
.L572:
	movq	2080(%r13), %rax
	testq	%rax, %rax
	je	.L577
	movq	2240(%r13), %rcx
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L606
.L577:
	movzbl	236(%r12), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testl	%ebx, %ebx
	je	.L607
	andl	$1, %ebx
	cmpb	$0, 2498(%r13)
	jne	.L608
	testb	%dl, %dl
	jne	.L609
.L581:
	testl	%ebx, %ebx
	je	.L579
	subq	$1, 2288(%r13)
	movzbl	236(%r12), %eax
.L582:
	orl	$2, %eax
	movb	%al, 236(%r12)
.L589:
	movq	%r12, %rdi
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L588
	movq	%r12, %rdi
	call	nghttp2_stream_dep_remove@PLT
	testl	%eax, %eax
	jne	.L569
.L588:
	movl	192(%r12), %esi
	movq	%r13, %rdi
	call	nghttp2_map_remove@PLT
	movq	%r12, %rdi
	call	nghttp2_stream_free@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	xorl	%eax, %eax
.L569:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	testl	%ebx, %ebx
	jne	.L582
.L578:
	subq	$1, 2304(%r13)
.L583:
	orb	$2, 236(%r12)
	testb	$16, 2484(%r13)
	jne	.L589
	cmpb	$0, 2498(%r13)
	je	.L589
	movq	%r12, %rdi
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L589
	movq	2256(%r13), %rax
	testq	%rax, %rax
	je	.L585
	movq	%r12, 160(%rax)
	movq	%rax, 152(%r12)
.L586:
	addq	$1, 2320(%r13)
	xorl	%eax, %eax
	movq	%r12, 2256(%r13)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L592:
	addq	$24, %rsp
	movl	$-501, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L578
.L579:
	subq	$1, 2296(%r13)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L608:
	xorl	$1, %ebx
	testb	%dl, %dl
	je	.L581
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L605:
	cmpq	344(%r13), %r8
	je	.L572
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	-56(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	nghttp2_mem_free@PLT
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r12, 2248(%r13)
	jmp	.L586
.L606:
	movl	$-902, %eax
	jmp	.L569
	.cfi_endproc
.LFE96:
	.size	nghttp2_session_close_stream, .-nghttp2_session_close_stream
	.p2align 4
	.globl	nghttp2_session_destroy_stream
	.type	nghttp2_session_destroy_stream, @function
nghttp2_session_destroy_stream:
.LFB97:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	2200(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L613
	movq	%r12, %rdi
	call	nghttp2_stream_dep_remove@PLT
	testl	%eax, %eax
	jne	.L610
.L613:
	movl	192(%r12), %esi
	movq	%r13, %rdi
	call	nghttp2_map_remove@PLT
	movq	%r12, %rdi
	call	nghttp2_stream_free@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	xorl	%eax, %eax
.L610:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE97:
	.size	nghttp2_session_destroy_stream, .-nghttp2_session_destroy_stream
	.p2align 4
	.globl	nghttp2_session_keep_closed_stream
	.type	nghttp2_session_keep_closed_stream, @function
nghttp2_session_keep_closed_stream:
.LFB98:
	.cfi_startproc
	endbr64
	movq	2256(%rdi), %rax
	testq	%rax, %rax
	je	.L619
	movq	%rsi, 160(%rax)
	movq	%rax, 152(%rsi)
	addq	$1, 2320(%rdi)
	movq	%rsi, 2256(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	addq	$1, 2320(%rdi)
	movq	%rsi, 2248(%rdi)
	movq	%rsi, 2256(%rdi)
	ret
	.cfi_endproc
.LFE98:
	.size	nghttp2_session_keep_closed_stream, .-nghttp2_session_keep_closed_stream
	.p2align 4
	.globl	nghttp2_session_keep_idle_stream
	.type	nghttp2_session_keep_idle_stream, @function
nghttp2_session_keep_idle_stream:
.LFB99:
	.cfi_startproc
	endbr64
	movq	2272(%rdi), %rax
	testq	%rax, %rax
	je	.L622
	movq	%rsi, 160(%rax)
	movq	%rax, 152(%rsi)
	addq	$1, 2328(%rdi)
	movq	%rsi, 2272(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	addq	$1, 2328(%rdi)
	movq	%rsi, 2264(%rdi)
	movq	%rsi, 2272(%rdi)
	ret
	.cfi_endproc
.LFE99:
	.size	nghttp2_session_keep_idle_stream, .-nghttp2_session_keep_idle_stream
	.p2align 4
	.globl	nghttp2_session_detach_idle_stream
	.type	nghttp2_session_detach_idle_stream, @function
nghttp2_session_detach_idle_stream:
.LFB100:
	.cfi_startproc
	endbr64
	movq	152(%rsi), %rax
	movq	160(%rsi), %rdx
	testq	%rax, %rax
	je	.L625
	movq	%rdx, 160(%rax)
	testq	%rdx, %rdx
	je	.L627
.L629:
	movq	%rax, 152(%rdx)
.L628:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 152(%rsi)
	subq	$1, 2328(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%rdx, 2264(%rdi)
	testq	%rdx, %rdx
	jne	.L629
.L627:
	movq	%rax, 2272(%rdi)
	jmp	.L628
	.cfi_endproc
.LFE100:
	.size	nghttp2_session_detach_idle_stream, .-nghttp2_session_detach_idle_stream
	.section	.rodata.str1.1
.LC14:
	.string	"head_stream"
	.text
	.p2align 4
	.globl	nghttp2_session_adjust_closed_stream
	.type	nghttp2_session_adjust_closed_stream, @function
nghttp2_session_adjust_closed_stream:
.LFB101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	2464(%rdi), %ebx
	cmpl	$-1, %ebx
	jne	.L632
	movl	2488(%rdi), %ebx
.L632:
	movq	2320(%r14), %rax
	testq	%rax, %rax
	je	.L640
	leaq	2200(%r14), %r13
	.p2align 4,,10
	.p2align 3
.L641:
	addq	2296(%r14), %rax
	cmpq	%rbx, %rax
	jbe	.L640
	movq	2248(%r14), %r12
	testq	%r12, %r12
	je	.L651
	movq	%r12, %rdi
	movq	160(%r12), %r15
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L638
	movq	%r12, %rdi
	call	nghttp2_stream_dep_remove@PLT
	testl	%eax, %eax
	jne	.L630
.L638:
	movl	192(%r12), %esi
	movq	%r14, %rdi
	call	nghttp2_map_remove@PLT
	movq	%r12, %rdi
	call	nghttp2_stream_free@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movq	%r15, 2248(%r14)
	testq	%r15, %r15
	je	.L652
	movq	2320(%r14), %rax
	movq	$0, 152(%r15)
	subq	$1, %rax
	movq	%rax, 2320(%r14)
	jne	.L641
.L640:
	xorl	%eax, %eax
.L630:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	movq	2320(%r14), %rax
	movq	$0, 2256(%r14)
	subq	$1, %rax
	movq	%rax, 2320(%r14)
	jne	.L641
	jmp	.L640
.L651:
	leaq	__PRETTY_FUNCTION__.6476(%rip), %rcx
	movl	$1346, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE101:
	.size	nghttp2_session_adjust_closed_stream, .-nghttp2_session_adjust_closed_stream
	.section	.rodata.str1.1
.LC15:
	.string	"head"
	.text
	.p2align 4
	.globl	nghttp2_session_adjust_idle_stream
	.type	nghttp2_session_adjust_idle_stream, @function
nghttp2_session_adjust_idle_stream:
.LFB102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$100, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	2464(%rdi), %eax
	cmpl	%eax, 2488(%rdi)
	cmovbe	2488(%rdi), %eax
	cmpl	$100, %eax
	ja	.L654
	cmpl	$16, %eax
	movl	$16, %r14d
	cmovnb	%eax, %r14d
	movl	%r14d, %r14d
.L654:
	cmpq	2328(%rbx), %r14
	jnb	.L655
	leaq	2200(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L663:
	movq	2264(%rbx), %r12
	testq	%r12, %r12
	je	.L668
	movq	%r12, %rdi
	movq	160(%r12), %r13
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L661
	movq	%r12, %rdi
	call	nghttp2_stream_dep_remove@PLT
	testl	%eax, %eax
	jne	.L653
.L661:
	movl	192(%r12), %esi
	movq	%rbx, %rdi
	call	nghttp2_map_remove@PLT
	movq	%r12, %rdi
	call	nghttp2_stream_free@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movq	%r13, 2264(%rbx)
	testq	%r13, %r13
	je	.L669
	movq	2328(%rbx), %rax
	movq	$0, 152(%r13)
	subq	$1, %rax
	movq	%rax, 2328(%rbx)
	cmpq	%rax, %r14
	jb	.L663
.L655:
	xorl	%eax, %eax
.L653:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movq	2328(%rbx), %rax
	movq	$0, 2272(%rbx)
	subq	$1, %rax
	movq	%rax, 2328(%rbx)
	cmpq	%r14, %rax
	ja	.L663
	jmp	.L655
.L668:
	leaq	__PRETTY_FUNCTION__.6487(%rip), %rcx
	movl	$1390, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE102:
	.size	nghttp2_session_adjust_idle_stream, .-nghttp2_session_adjust_idle_stream
	.p2align 4
	.globl	nghttp2_session_close_stream_if_shut_rdwr
	.type	nghttp2_session_close_stream_if_shut_rdwr, @function
nghttp2_session_close_stream_if_shut_rdwr:
.LFB103:
	.cfi_startproc
	endbr64
	movzbl	237(%rsi), %eax
	andl	$3, %eax
	cmpb	$3, %al
	je	.L672
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	movl	192(%rsi), %esi
	xorl	%edx, %edx
	jmp	nghttp2_session_close_stream
	.cfi_endproc
.LFE103:
	.size	nghttp2_session_close_stream_if_shut_rdwr, .-nghttp2_session_close_stream_if_shut_rdwr
	.p2align 4
	.globl	nghttp2_session_check_request_allowed
	.type	nghttp2_session_check_request_allowed, @function
nghttp2_session_check_request_allowed:
.LFB107:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L679
	movl	2376(%rdi), %edx
	testl	%edx, %edx
	js	.L679
	testb	$8, 2499(%rdi)
	je	.L682
.L679:
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	session_is_closing
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE107:
	.size	nghttp2_session_check_request_allowed, .-nghttp2_session_check_request_allowed
	.p2align 4
	.globl	nghttp2_session_get_next_ob_item
	.type	nghttp2_session_get_next_ob_item, @function
nghttp2_session_get_next_ob_item:
.LFB124:
	.cfi_startproc
	endbr64
	movq	272(%rdi), %rax
	testq	%rax, %rax
	je	.L687
.L683:
	ret
	.p2align 4,,10
	.p2align 3
.L687:
	movq	296(%rdi), %rax
	testq	%rax, %rax
	jne	.L683
	movl	2436(%rdi), %edx
	cmpq	%rdx, 2288(%rdi)
	jnb	.L685
	movq	320(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L685
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	movl	2404(%rdi), %edx
	testl	%edx, %edx
	jle	.L683
	addq	$32, %rdi
	jmp	nghttp2_stream_next_outbound_item@PLT
	.cfi_endproc
.LFE124:
	.size	nghttp2_session_get_next_ob_item, .-nghttp2_session_get_next_ob_item
	.p2align 4
	.globl	nghttp2_session_pop_next_ob_item
	.type	nghttp2_session_pop_next_ob_item, @function
nghttp2_session_pop_next_ob_item:
.LFB125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	272(%rdi), %r12
	testq	%r12, %r12
	jne	.L697
	movq	296(%rdi), %r12
	testq	%r12, %r12
	jne	.L698
	movl	2436(%rdi), %eax
	cmpq	%rax, 2288(%rdi)
	jnb	.L692
	movq	320(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L692
	addq	$320, %rdi
	movq	%rbx, %r12
	call	nghttp2_outbound_queue_pop@PLT
	movb	$0, 144(%rbx)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L698:
	addq	$296, %rdi
	call	nghttp2_outbound_queue_pop@PLT
	movb	$0, 144(%r12)
.L688:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movl	2404(%rdi), %eax
	testl	%eax, %eax
	jle	.L688
	popq	%rbx
	addq	$32, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	nghttp2_stream_next_outbound_item@PLT
	.p2align 4,,10
	.p2align 3
.L697:
	.cfi_restore_state
	addq	$272, %rdi
	call	nghttp2_outbound_queue_pop@PLT
	movq	%r12, %rax
	movb	$0, 144(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE125:
	.size	nghttp2_session_pop_next_ob_item, .-nghttp2_session_pop_next_ob_item
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"request HEADERS: stream_id == 0"
	.align 8
.LC17:
	.string	"request HEADERS: client received request"
	.align 8
.LC18:
	.string	"request HEADERS: invalid stream_id"
	.section	.rodata.str1.1
.LC19:
	.string	"HEADERS: stream closed"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"request HEADERS: max concurrent streams exceeded"
	.align 8
.LC21:
	.string	"request HEADERS: depend on itself"
	.text
	.p2align 4
	.globl	nghttp2_session_on_request_headers_received
	.type	nghttp2_session_on_request_headers_received, @function
nghttp2_session_on_request_headers_received:
.LFB156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	8(%rsi), %esi
	movq	%rdi, %r12
	testl	%esi, %esi
	je	.L766
	cmpb	$0, 2498(%rdi)
	jne	.L706
	testb	$1, %sil
	jne	.L767
	cmpl	2384(%rdi), %esi
	jle	.L702
.L708:
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L712
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L711
.L712:
	testb	$1, 2499(%r12)
	jne	.L702
	movl	2388(%r12), %esi
	leaq	.LC17(%rip), %rcx
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L706:
	testb	$1, %sil
	je	.L713
	cmpl	2384(%rdi), %esi
	jg	.L768
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L702
	testb	$1, 237(%rax)
	je	.L702
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L722
	movq	2240(%r12), %rcx
	movl	$-510, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L711
.L722:
	testb	$1, 2499(%r12)
	jne	.L702
	movl	2388(%r12), %esi
	leaq	.LC19(%rip), %rcx
	movl	$5, %edx
	movq	%r12, %rdi
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jl	.L699
	.p2align 4,,10
	.p2align 3
.L702:
	movl	$-103, %eax
.L699:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L704
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L711
.L704:
	testb	$1, 2499(%r12)
	jne	.L702
	movl	2388(%r12), %esi
	leaq	.LC16(%rip), %rcx
.L759:
	movl	$1, %edx
	movq	%r12, %rdi
	call	session_terminate_session.part.0
.L764:
	cmpl	$-900, %eax
	jge	.L702
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	.cfi_restore_state
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L719
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L719
.L711:
	movl	$-902, %eax
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L767:
	cmpl	2380(%rdi), %esi
	jg	.L708
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L719:
	testb	$1, 2499(%r12)
	jne	.L702
	movl	2388(%r12), %esi
	leaq	.LC18(%rip), %rcx
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L768:
	movq	2296(%rdi), %rax
	movl	2464(%rdi), %edx
	movl	%esi, 2384(%rdi)
	cmpq	%rdx, %rax
	jnb	.L769
	movzbl	2499(%rdi), %edx
	testb	$5, %dl
	jne	.L702
	cmpl	24(%r13), %esi
	je	.L770
	movl	2488(%rdi), %edx
	cmpq	%rdx, %rax
	jnb	.L771
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	leaq	24(%r13), %rcx
	movl	$1, %r8d
	call	nghttp2_session_open_stream
	testq	%rax, %rax
	je	.L730
	movq	%r12, %rdi
	call	nghttp2_session_adjust_closed_stream
	cmpl	$-900, %eax
	jl	.L699
	movl	2384(%r12), %eax
	movq	2088(%r12), %rcx
	movl	%eax, 2388(%r12)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L699
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rcx
	cmpl	$-521, %eax
	je	.L699
	testl	%eax, %eax
	je	.L699
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L771:
	movl	$7, %edx
	call	nghttp2_session_add_rst_stream
	testl	%eax, %eax
	jne	.L764
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L702
	movq	2240(%r12), %rcx
	movl	$-533, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L702
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L769:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L725
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L711
.L725:
	testb	$1, 2499(%r12)
	jne	.L702
	movl	2388(%r12), %esi
	leaq	.LC20(%rip), %rcx
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L770:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L727
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L711
	movzbl	2499(%r12), %edx
.L727:
	andl	$1, %edx
	jne	.L702
	movl	2388(%r12), %esi
	leaq	.LC21(%rip), %rcx
	jmp	.L759
.L730:
	movl	$-901, %eax
	jmp	.L699
	.cfi_endproc
.LFE156:
	.size	nghttp2_session_on_request_headers_received, .-nghttp2_session_on_request_headers_received
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"stream->state == NGHTTP2_STREAM_OPENING && nghttp2_session_is_my_stream_id(session, frame->hd.stream_id)"
	.text
	.p2align 4
	.globl	nghttp2_session_on_response_headers_received
	.type	nghttp2_session_on_response_headers_received, @function
nghttp2_session_on_response_headers_received:
.LFB157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpl	$1, 228(%rdx)
	jne	.L773
	movl	8(%rsi), %eax
	testl	%eax, %eax
	je	.L773
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L796
.L774:
	testl	%eax, %eax
	je	.L773
	testb	$1, 237(%rdx)
	jne	.L797
	movq	2088(%rdi), %rax
	movl	$2, 228(%rdx)
	testq	%rax, %rax
	je	.L784
	movq	2240(%rdi), %rdx
	call	*%rax
	cmpl	$-521, %eax
	je	.L772
	testl	%eax, %eax
	jne	.L780
.L784:
	xorl	%eax, %eax
.L772:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	xorl	$1, %eax
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L773:
	leaq	__PRETTY_FUNCTION__.6943(%rip), %rcx
	movl	$3928, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L797:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L781
	movq	%rdi, -8(%rbp)
	movq	2240(%rdi), %rcx
	movl	$-510, %edx
	call	*%rax
	movq	-8(%rbp), %rdi
	testl	%eax, %eax
	jne	.L780
.L781:
	testb	$1, 2499(%rdi)
	jne	.L778
	movl	2388(%rdi), %esi
	leaq	.LC19(%rip), %rcx
	movl	$5, %edx
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jl	.L772
.L778:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$-103, %eax
	ret
.L780:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	movl	$-902, %eax
	ret
	.cfi_endproc
.LFE157:
	.size	nghttp2_session_on_response_headers_received, .-nghttp2_session_on_response_headers_received
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"stream->state == NGHTTP2_STREAM_RESERVED"
	.align 8
.LC24:
	.string	"push response HEADERS: stream_id == 0"
	.align 8
.LC25:
	.string	"HEADERS: no HEADERS allowed from client in reserved state"
	.align 8
.LC26:
	.string	"push response HEADERS: max concurrent streams exceeded"
	.text
	.p2align 4
	.globl	nghttp2_session_on_push_response_headers_received
	.type	nghttp2_session_on_push_response_headers_received, @function
nghttp2_session_on_push_response_headers_received:
.LFB158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	$4, 228(%rdx)
	jne	.L843
	movq	%rsi, %r13
	movl	8(%rsi), %esi
	movq	%rdi, %r12
	testl	%esi, %esi
	je	.L844
	cmpb	$0, 2498(%rdi)
	jne	.L845
	movq	%rdx, %rbx
	movq	2296(%rdi), %rax
	movl	2464(%rdi), %edx
	cmpq	%rdx, %rax
	jnb	.L846
	testb	$5, 2499(%rdi)
	jne	.L802
	movl	2488(%rdi), %edx
	cmpq	%rdx, %rax
	jnb	.L847
	movq	%rbx, %rdi
	call	nghttp2_stream_promise_fulfilled@PLT
	movl	192(%rbx), %eax
	testl	%eax, %eax
	je	.L817
	andl	$1, %eax
	cmpb	$0, 2498(%r12)
	jne	.L848
.L818:
	testl	%eax, %eax
	je	.L817
.L819:
	movq	2088(%r12), %rax
	addq	$1, 2296(%r12)
	testq	%rax, %rax
	je	.L821
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	cmpl	$-521, %eax
	je	.L798
	testl	%eax, %eax
	jne	.L804
.L821:
	xorl	%eax, %eax
.L798:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	subq	$1, 2304(%r12)
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L847:
	movl	$7, %edx
	call	nghttp2_session_add_rst_stream
	testl	%eax, %eax
	jne	.L841
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L802
	movq	2240(%r12), %rcx
	movl	$-533, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L804
.L802:
	addq	$8, %rsp
	movl	$-103, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_restore_state
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L805
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L804
.L805:
	testb	$1, 2499(%r12)
	jne	.L802
	movl	2388(%r12), %esi
	leaq	.LC24(%rip), %rcx
.L839:
	movl	$1, %edx
	movq	%r12, %rdi
	call	session_terminate_session.part.0
.L841:
	cmpl	$-900, %eax
	jge	.L802
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L848:
	.cfi_restore_state
	xorl	$1, %eax
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L845:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L810
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L804
.L810:
	testb	$1, 2499(%r12)
	jne	.L802
	movl	2388(%r12), %esi
	leaq	.LC25(%rip), %rcx
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L846:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L814
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L804
.L814:
	testb	$1, 2499(%r12)
	jne	.L802
	movl	2388(%r12), %esi
	leaq	.LC26(%rip), %rcx
	jmp	.L839
.L804:
	movl	$-902, %eax
	jmp	.L798
.L843:
	leaq	__PRETTY_FUNCTION__.6950(%rip), %rcx
	movl	$3958, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE158:
	.size	nghttp2_session_on_push_response_headers_received, .-nghttp2_session_on_push_response_headers_received
	.section	.rodata.str1.1
.LC27:
	.string	"HEADERS: stream_id == 0"
	.text
	.p2align 4
	.globl	nghttp2_session_on_headers_received
	.type	nghttp2_session_on_headers_received, @function
nghttp2_session_on_headers_received:
.LFB159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	8(%rsi), %eax
	testl	%eax, %eax
	je	.L890
	testb	$1, 237(%rdx)
	jne	.L891
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L892
	movl	228(%rdx), %edx
	testl	%eax, %eax
	jne	.L893
.L862:
	cmpl	$3, %edx
	je	.L852
.L887:
	movq	2088(%rdi), %rax
	testq	%rax, %rax
	je	.L863
	movq	2240(%rdi), %rdx
	call	*%rax
	cmpl	$-521, %eax
	je	.L894
	testl	%eax, %eax
	jne	.L859
.L863:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	cmpl	$2, %edx
	je	.L887
.L852:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$-103, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L892:
	.cfi_restore_state
	xorl	$1, %eax
	movl	228(%rdx), %edx
	testl	%eax, %eax
	je	.L862
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L890:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L854
	movq	%rdi, -8(%rbp)
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	call	*%rax
	movq	-8(%rbp), %rdi
	testl	%eax, %eax
	jne	.L859
.L854:
	testb	$1, 2499(%rdi)
	jne	.L852
	movl	2388(%rdi), %esi
	leaq	.LC27(%rip), %rcx
	movl	$1, %edx
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jge	.L852
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore_state
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L860
	movq	%rdi, -8(%rbp)
	movq	2240(%rdi), %rcx
	movl	$-510, %edx
	call	*%rax
	movq	-8(%rbp), %rdi
	testl	%eax, %eax
	jne	.L859
.L860:
	testb	$1, 2499(%rdi)
	jne	.L852
	movl	2388(%rdi), %esi
	leaq	.LC19(%rip), %rcx
	movl	$5, %edx
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jge	.L852
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L859:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$-902, %eax
	ret
.L894:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	movl	$-521, %eax
	ret
	.cfi_endproc
.LFE159:
	.size	nghttp2_session_on_headers_received, .-nghttp2_session_on_headers_received
	.section	.rodata.str1.1
.LC28:
	.string	"HEADERS: could not unpack"
	.text
	.p2align 4
	.type	session_process_headers_frame, @function
session_process_headers_frame:
.LFB160:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	424(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	544(%rdi), %rsi
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	nghttp2_frame_unpack_headers_payload@PLT
	testl	%eax, %eax
	je	.L896
	testb	$1, 2499(%r12)
	jne	.L897
	movl	2388(%r12), %esi
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%r12
	leaq	.LC28(%rip), %rcx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	session_terminate_session.part.0
	.p2align 4,,10
	.p2align 3
.L896:
	.cfi_restore_state
	movl	432(%r12), %esi
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L898
	testb	$2, 236(%rax)
	jne	.L898
	movl	228(%rax), %eax
	cmpl	$5, %eax
	je	.L898
	cmpl	$4, %eax
	je	.L917
	cmpl	$1, %eax
	jne	.L901
	movl	432(%r12), %eax
	testl	%eax, %eax
	je	.L901
	andl	$1, %eax
	cmpb	$0, 2498(%r12)
	je	.L902
	xorl	$1, %eax
.L902:
	testl	%eax, %eax
	jne	.L918
.L901:
	movl	$3, 480(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	nghttp2_session_on_headers_received
	.p2align 4,,10
	.p2align 3
.L897:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_restore_state
	movl	$2, 480(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	nghttp2_session_on_push_response_headers_received
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movl	$1, 480(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	nghttp2_session_on_response_headers_received
	.p2align 4,,10
	.p2align 3
.L898:
	.cfi_restore_state
	movl	$0, 480(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_session_on_request_headers_received
	.cfi_endproc
.LFE160:
	.size	session_process_headers_frame, .-session_process_headers_frame
	.section	.rodata.str1.1
.LC29:
	.string	"PRIORITY: stream_id == 0"
.LC30:
	.string	"depend on itself"
	.text
	.p2align 4
	.globl	nghttp2_session_on_priority_received
	.type	nghttp2_session_on_priority_received, @function
nghttp2_session_on_priority_received:
.LFB161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	8(%rsi), %esi
	movq	%rdi, %r12
	testl	%esi, %esi
	je	.L952
	cmpl	16(%r13), %esi
	je	.L953
	cmpb	$0, 2498(%rdi)
	jne	.L927
.L950:
	movq	2032(%r12), %rax
	testq	%rax, %rax
	je	.L922
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L928
.L922:
	xorl	%eax, %eax
.L919:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L927:
	.cfi_restore_state
	call	nghttp2_map_find@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L954
	leaq	16(%r13), %rdx
	movq	%r12, %rdi
	call	nghttp2_session_reprioritize_stream
	cmpl	$-900, %eax
	jl	.L919
.L948:
	movq	%r12, %rdi
	call	nghttp2_session_adjust_idle_stream
	cmpl	$-900, %eax
	jge	.L950
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L952:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L924
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L928
.L924:
	testb	$1, 2499(%r12)
	jne	.L922
	movl	2388(%r12), %esi
	leaq	.LC29(%rip), %rcx
.L951:
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	session_terminate_session.part.0
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	movl	8(%r13), %esi
	testl	%esi, %esi
	je	.L922
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%r12)
	jne	.L955
	testl	%eax, %eax
	jne	.L935
.L931:
	cmpl	2384(%r12), %esi
	jle	.L922
.L932:
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	leaq	16(%r13), %rcx
	movl	$5, %r8d
	movq	%r12, %rdi
	call	nghttp2_session_open_stream
	testq	%rax, %rax
	jne	.L948
	movl	$-901, %eax
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L953:
	testb	$1, 2499(%rdi)
	jne	.L922
	movl	2388(%rdi), %esi
	leaq	.LC30(%rip), %rcx
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L955:
	testl	%eax, %eax
	jne	.L931
.L935:
	cmpl	2380(%r12), %esi
	jg	.L932
	jmp	.L922
.L928:
	movl	$-902, %eax
	jmp	.L919
	.cfi_endproc
.LFE161:
	.size	nghttp2_session_on_priority_received, .-nghttp2_session_on_priority_received
	.section	.rodata.str1.1
.LC31:
	.string	"RST_STREAM: stream_id == 0"
.LC32:
	.string	"RST_STREAM: stream in idle"
	.text
	.p2align 4
	.globl	nghttp2_session_on_rst_stream_received
	.type	nghttp2_session_on_rst_stream_received, @function
nghttp2_session_on_rst_stream_received:
.LFB163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	8(%rsi), %esi
	movq	%rdi, %r12
	testl	%esi, %esi
	je	.L985
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L986
	testl	%eax, %eax
	jne	.L974
.L964:
	cmpl	2384(%r12), %esi
	jg	.L965
.L966:
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L971
	testb	$2, 236(%rax)
	jne	.L971
	cmpl	$5, 228(%rax)
	je	.L971
	movl	$1, %esi
	call	nghttp2_stream_shutdown@PLT
.L971:
	movq	2032(%r12), %rax
	testq	%rax, %rax
	je	.L973
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L969
.L973:
	movl	16(%r13), %edx
	movl	8(%r13), %esi
	movq	%r12, %rdi
	call	nghttp2_session_close_stream
	cmpl	$-900, %eax
	jl	.L956
.L959:
	xorl	%eax, %eax
.L956:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L986:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L964
.L974:
	cmpl	2380(%r12), %esi
	jle	.L966
.L965:
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L970
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L969
.L970:
	testb	$1, 2499(%r12)
	jne	.L959
	movl	2388(%r12), %esi
	leaq	.LC32(%rip), %rcx
.L984:
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	session_terminate_session.part.0
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L961
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L969
.L961:
	testb	$1, 2499(%r12)
	jne	.L959
	movl	2388(%r12), %esi
	leaq	.LC31(%rip), %rcx
	jmp	.L984
.L969:
	movl	$-902, %eax
	jmp	.L956
	.cfi_endproc
.LFE163:
	.size	nghttp2_session_on_rst_stream_received, .-nghttp2_session_on_rst_stream_received
	.p2align 4
	.globl	nghttp2_session_update_local_settings
	.type	nghttp2_session_update_local_settings, @function
nghttp2_session_update_local_settings:
.LFB169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L988
	leaq	4(%rsi), %rax
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r13
	leaq	(%rax,%rdx,8), %rcx
	xorl	%edi, %edi
	movl	$4294967295, %esi
	xorl	%r12d, %r12d
	movl	$-1, %r15d
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1020:
	cmpl	$4, %edx
	jne	.L991
	movl	(%rax), %r15d
.L991:
	addq	$8, %rax
	cmpq	%rcx, %rax
	je	.L1019
.L992:
	movl	-4(%rax), %edx
	cmpl	$1, %edx
	jne	.L1020
	movl	(%rax), %r12d
	movl	$1, %edi
	cmpl	%r12d, %esi
	cmova	%r12, %rsi
	addq	$8, %rax
	cmpq	%rcx, %rax
	jne	.L992
.L1019:
	testb	%dil, %dil
	je	.L993
	leaq	1776(%r14), %rdi
	cmpl	%esi, %r12d
	jbe	.L996
	movq	%rdi, -88(%rbp)
	call	nghttp2_hd_inflate_change_table_size@PLT
	movq	-88(%rbp), %rdi
	testl	%eax, %eax
	jne	.L987
.L996:
	movl	%r12d, %esi
	call	nghttp2_hd_inflate_change_table_size@PLT
	testl	%eax, %eax
	jne	.L987
.L993:
	cmpl	$-1, %r15d
	je	.L997
	movl	2468(%r14), %eax
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r14, -80(%rbp)
	leaq	update_local_initial_window_size_func(%rip), %rsi
	movl	%r15d, -72(%rbp)
	movl	%eax, -68(%rbp)
	call	nghttp2_map_each@PLT
	testl	%eax, %eax
	jne	.L987
.L997:
	xorl	%eax, %eax
	leaq	.L1000(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L1007:
	cmpl	$8, (%rbx,%rax,8)
	ja	.L998
	movl	(%rbx,%rax,8), %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L1000:
	.long	.L998-.L1000
	.long	.L1006-.L1000
	.long	.L1005-.L1000
	.long	.L1004-.L1000
	.long	.L1003-.L1000
	.long	.L1002-.L1000
	.long	.L1001-.L1000
	.long	.L998-.L1000
	.long	.L999-.L1000
	.text
	.p2align 4,,10
	.p2align 3
.L999:
	movl	4(%rbx,%rax,8), %edx
	movl	%edx, 2480(%r14)
	.p2align 4,,10
	.p2align 3
.L998:
	addq	$1, %rax
	cmpq	%rax, %r13
	ja	.L1007
.L988:
	xorl	%eax, %eax
.L987:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1021
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	.cfi_restore_state
	movl	4(%rbx,%rax,8), %edx
	movl	%edx, 2476(%r14)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	4(%rbx,%rax,8), %edx
	movl	%edx, 2472(%r14)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1005:
	movl	4(%rbx,%rax,8), %edx
	movl	%edx, 2460(%r14)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1006:
	movl	4(%rbx,%rax,8), %edx
	movl	%edx, 2456(%r14)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1003:
	movl	4(%rbx,%rax,8), %edx
	movl	%edx, 2468(%r14)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	4(%rbx,%rax,8), %edx
	movl	%edx, 2464(%r14)
	jmp	.L998
.L1021:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE169:
	.size	nghttp2_session_update_local_settings, .-nghttp2_session_update_local_settings
	.section	.rodata.str1.1
.LC33:
	.string	"SETTINGS: stream_id != 0"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"SETTINGS: ACK and payload != 0"
	.section	.rodata.str1.1
.LC35:
	.string	"SETTINGS: unexpected ACK"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"SETTINGS: invalid SETTINGS_ENBLE_PUSH"
	.align 8
.LC37:
	.string	"SETTINGS: server attempted to enable push"
	.align 8
.LC38:
	.string	"SETTINGS: too large SETTINGS_INITIAL_WINDOW_SIZE"
	.align 8
.LC39:
	.string	"SETTINGS: invalid SETTINGS_MAX_FRAME_SIZE"
	.align 8
.LC40:
	.string	"SETTINGS: invalid SETTINGS_ENABLE_CONNECT_PROTOCOL"
	.align 8
.LC41:
	.string	"SETTINGS: server attempted to disable SETTINGS_ENABLE_CONNECT_PROTOCOL"
	.section	.rodata.str1.1
.LC42:
	.string	"nghttp2_is_fatal(rv)"
	.text
	.p2align 4
	.globl	nghttp2_session_on_settings_received
	.type	nghttp2_session_on_settings_received, @function
nghttp2_session_on_settings_received:
.LFB170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movl	8(%rsi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	2200(%rdi), %rax
	movq	%rax, -96(%rbp)
	testl	%esi, %esi
	jne	.L1138
	testb	$1, 13(%r13)
	je	.L1029
	cmpq	$0, 16(%r13)
	jne	.L1139
	movq	2280(%rdi), %r12
	testq	%r12, %r12
	je	.L1140
	movq	16(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	2200(%r15), %rbx
	call	nghttp2_session_update_local_settings
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	movl	%eax, -84(%rbp)
	movq	(%r12), %rax
	movq	%rax, 2280(%r15)
	call	nghttp2_mem_free@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	nghttp2_mem_free@PLT
	movl	-84(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1093
	cmpl	$-900, %r9d
	jl	.L1022
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1043
	movl	%r9d, -84(%rbp)
	movl	%r9d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	2240(%r15), %rcx
	call	*%rax
	movl	-84(%rbp), %r9d
	testl	%eax, %eax
	jne	.L1033
.L1043:
	addl	$533, %r9d
	movl	$2, %edx
	cmpl	$28, %r9d
	ja	.L1042
	leaq	CSWTCH.113(%rip), %rax
	movl	(%rax,%r9,4), %edx
.L1042:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1029:
	cmpb	$0, 2424(%rdi)
	je	.L1141
.L1044:
	movq	16(%r13), %rdx
	testq	%rdx, %rdx
	je	.L1089
	xorl	%r12d, %r12d
	leaq	.L1050(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	24(%r13), %rax
	leaq	(%rax,%r12,8), %rbx
	cmpl	$8, (%rbx)
	ja	.L1048
	movl	(%rbx), %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1050:
	.long	.L1048-.L1050
	.long	.L1056-.L1050
	.long	.L1055-.L1050
	.long	.L1054-.L1050
	.long	.L1053-.L1050
	.long	.L1052-.L1050
	.long	.L1051-.L1050
	.long	.L1048-.L1050
	.long	.L1049-.L1050
	.text
	.p2align 4,,10
	.p2align 3
.L1049:
	movl	4(%rbx), %eax
	cmpl	$1, %eax
	ja	.L1142
	cmpb	$0, 2498(%r15)
	jne	.L1085
	movl	2452(%r15), %edx
	testl	%edx, %edx
	je	.L1085
	testb	$1, %al
	je	.L1143
.L1085:
	movl	%eax, 2452(%r15)
	movq	16(%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L1048:
	addq	$1, %r12
	cmpq	%rdx, %r12
	jb	.L1045
.L1089:
	movl	-84(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1144
.L1093:
	movq	2032(%r15), %rax
	testq	%rax, %rax
	je	.L1025
	movq	2240(%r15), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1025:
	xorl	%r9d, %r9d
.L1022:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1145
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1053:
	.cfi_restore_state
	movl	4(%rbx), %eax
	testl	%eax, %eax
	js	.L1146
	movl	%eax, -72(%rbp)
	movl	2440(%r15), %eax
	leaq	-80(%rbp), %rdx
	movq	%r15, %rdi
	leaq	update_remote_initial_window_size_func(%rip), %rsi
	movq	%r15, -80(%rbp)
	movl	%eax, -68(%rbp)
	call	nghttp2_map_each@PLT
	movl	%eax, %r9d
	cmpl	$-900, %eax
	jl	.L1022
	testl	%eax, %eax
	jne	.L1147
	movl	4(%rbx), %eax
	movl	%eax, 2440(%r15)
	movq	16(%r13), %rdx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1054:
	movl	4(%rbx), %eax
	movl	%eax, 2436(%r15)
	movq	16(%r13), %rdx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1051:
	movl	4(%rbx), %eax
	movl	%eax, 2448(%r15)
	movq	16(%r13), %rdx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1052:
	movl	4(%rbx), %eax
	leal	-16384(%rax), %edx
	cmpl	$16760831, %edx
	ja	.L1148
	movl	%eax, 2444(%r15)
	movq	16(%r13), %rdx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1055:
	movl	4(%rbx), %eax
	cmpl	$1, %eax
	ja	.L1149
	cmpb	$0, 2498(%r15)
	jne	.L1065
	testb	$1, %al
	jne	.L1150
.L1065:
	movl	%eax, 2432(%r15)
	movq	16(%r13), %rdx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	4(%rbx), %esi
	leaq	664(%r15), %rdi
	call	nghttp2_hd_deflate_change_table_size@PLT
	movl	%eax, %r9d
	testl	%eax, %eax
	jne	.L1151
	movl	4(%rbx), %eax
	movl	%eax, 2428(%r15)
	movq	16(%r13), %rdx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1027
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1027:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC33(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1141:
	movl	$-1, 2436(%rdi)
	movb	$1, 2424(%rdi)
	jmp	.L1044
.L1144:
	movq	%r15, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L1093
	movq	2352(%r15), %rax
	cmpq	%rax, 2344(%r15)
	jnb	.L1099
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	nghttp2_iv_check@PLT
	testl	%eax, %eax
	je	.L1090
	movq	-96(%rbp), %rdi
	movl	$152, %esi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1100
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_frame_settings_init@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	je	.L1091
	cmpl	$-900, %eax
	jge	.L1152
	movq	-96(%rbp), %rbx
	movq	%r12, %rdi
	movl	%eax, -84(%rbp)
	movq	%rbx, %rsi
	call	nghttp2_frame_settings_free@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	nghttp2_mem_free@PLT
	movl	-84(%rbp), %r9d
	jmp	.L1022
.L1139:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1034
	movq	2240(%rdi), %rcx
	movl	$-522, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1034:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC34(%rip), %rcx
	movl	$6, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1140:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1038
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L1038
.L1033:
	movl	$-902, %r9d
	jmp	.L1022
.L1038:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC35(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1151:
	cmpl	$-900, %eax
	jl	.L1022
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1060
	movq	2240(%r15), %rcx
	movl	$-523, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1060:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	xorl	%ecx, %ecx
	movl	$9, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1142:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1084
	movq	2240(%r15), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1084:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC40(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1149:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1064
	movq	2240(%r15), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1064:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC36(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1146:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1072
	movq	2240(%r15), %rcx
	movl	$-524, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1072:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC38(%rip), %rcx
	movl	$3, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1147:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1076
	movq	2240(%r15), %rcx
	movl	$-524, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1076:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	xorl	%ecx, %ecx
	movl	$3, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1148:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1080
	movq	2240(%r15), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1080:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC39(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1150:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1068
	movq	2240(%r15), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1068:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC37(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1091:
	addq	$1, 2344(%r15)
	jmp	.L1093
.L1143:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1088
	movq	2240(%r15), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1088:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	leaq	.LC41(%rip), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1090:
	movq	2040(%r15), %rax
	testq	%rax, %rax
	je	.L1096
	movq	2240(%r15), %rcx
	movl	$-534, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1033
.L1096:
	testb	$1, 2499(%r15)
	jne	.L1025
	movl	2388(%r15), %esi
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r9d
	jmp	.L1022
.L1145:
	call	__stack_chk_fail@PLT
.L1099:
	movl	$-904, %r9d
	jmp	.L1022
.L1152:
	leaq	__PRETTY_FUNCTION__.7416(%rip), %rcx
	movl	$7049, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
.L1100:
	movl	$-901, %r9d
	jmp	.L1022
	.cfi_endproc
.LFE170:
	.size	nghttp2_session_on_settings_received, .-nghttp2_session_on_settings_received
	.section	.rodata.str1.1
.LC43:
	.string	"PUSH_PROMISE: stream_id == 0"
.LC44:
	.string	"PUSH_PROMISE: push disabled"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"PUSH_PROMISE: invalid stream_id"
	.align 8
.LC46:
	.string	"PUSH_PROMISE: invalid promised_stream_id"
	.section	.rodata.str1.1
.LC47:
	.string	"PUSH_PROMISE: stream in idle"
.LC48:
	.string	"PUSH_PROMISE: stream closed"
	.text
	.p2align 4
	.globl	nghttp2_session_on_push_promise_received
	.type	nghttp2_session_on_push_promise_received, @function
nghttp2_session_on_push_promise_received:
.LFB172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	testl	%eax, %eax
	je	.L1222
	cmpb	$0, 2498(%rdi)
	jne	.L1160
	movl	2460(%rdi), %edx
	testl	%edx, %edx
	je	.L1160
	testb	$1, %al
	je	.L1223
	movzbl	2499(%rdi), %ecx
	testb	$5, %cl
	jne	.L1156
	movl	40(%rsi), %edx
	testl	%edx, %edx
	je	.L1170
	movl	%edx, %r14d
	andl	$1, %r14d
	jne	.L1170
	cmpl	2384(%rdi), %edx
	jle	.L1170
	cmpl	2380(%rdi), %eax
	jle	.L1224
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1176
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	call	*%rax
	testl	%eax, %eax
	jne	.L1164
	movzbl	2499(%r12), %ecx
.L1176:
	andl	$1, %ecx
	jne	.L1156
	movl	2388(%r12), %esi
	leaq	.LC47(%rip), %rcx
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1165
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1164
.L1165:
	testb	$1, 2499(%r12)
	jne	.L1156
	movl	2388(%r12), %esi
	leaq	.LC44(%rip), %rcx
.L1216:
	movl	$1, %edx
.L1221:
	movq	%r12, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r14d
	cmpl	$-900, %eax
	jge	.L1156
.L1153:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1225
	addq	$32, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	.cfi_restore_state
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1173
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1164
	movzbl	2499(%r12), %ecx
.L1173:
	andl	$1, %ecx
	jne	.L1156
	movl	2388(%r12), %esi
	leaq	.LC46(%rip), %rcx
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1158
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	call	*%rax
	testl	%eax, %eax
	jne	.L1164
.L1158:
	testb	$1, 2499(%r12)
	je	.L1208
.L1156:
	movl	$-103, %r14d
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	2388(%r12), %esi
	leaq	.LC43(%rip), %rcx
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1169
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	call	*%rax
	testl	%eax, %eax
	jne	.L1164
.L1169:
	testb	$1, 2499(%r12)
	jne	.L1156
	movl	2388(%r12), %esi
	leaq	.LC45(%rip), %rcx
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1224:
	movl	%edx, 2384(%rdi)
	movl	8(%rsi), %esi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L1174
	testb	$2, 236(%rax)
	jne	.L1174
	movl	228(%rax), %edx
	subl	$3, %edx
	andl	$-3, %edx
	je	.L1174
	cmpb	$0, 2496(%r12)
	je	.L1174
	movq	2312(%r12), %rdx
	cmpq	%rdx, 2304(%r12)
	jnb	.L1174
	testb	$1, 237(%rax)
	jne	.L1226
	movl	192(%rax), %esi
	leaq	-52(%rbp), %r15
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r15, %rdi
	call	nghttp2_priority_spec_init@PLT
	movl	40(%r13), %esi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	$4, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	nghttp2_session_open_stream
	testq	%rax, %rax
	je	.L1183
	movl	2384(%r12), %eax
	movl	%eax, 2388(%r12)
	movq	2088(%r12), %rax
	testq	%rax, %rax
	je	.L1153
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r14d
	cmpl	$-521, %eax
	je	.L1153
	testl	%eax, %eax
	je	.L1153
.L1164:
	movl	$-902, %r14d
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1174:
	movl	40(%r13), %esi
	movl	$8, %edx
	movq	%r12, %rdi
	call	nghttp2_session_add_rst_stream
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1153
	jmp	.L1156
.L1226:
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1181
	movq	2240(%r12), %rcx
	movl	$-510, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1164
.L1181:
	testb	$1, 2499(%r12)
	jne	.L1156
	movl	2388(%r12), %esi
	leaq	.LC48(%rip), %rcx
	movl	$5, %edx
	jmp	.L1221
.L1225:
	call	__stack_chk_fail@PLT
.L1183:
	movl	$-901, %r14d
	jmp	.L1153
	.cfi_endproc
.LFE172:
	.size	nghttp2_session_on_push_promise_received, .-nghttp2_session_on_push_promise_received
	.section	.rodata.str1.1
.LC49:
	.string	"PING: stream_id != 0"
	.text
	.p2align 4
	.globl	nghttp2_session_on_ping_received
	.type	nghttp2_session_on_ping_received, @function
nghttp2_session_on_ping_received:
.LFB174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jne	.L1249
	testb	$8, 2484(%rdi)
	je	.L1250
.L1236:
	movq	2032(%r12), %rax
	testq	%rax, %rax
	je	.L1230
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1232
.L1230:
	xorl	%eax, %eax
.L1227:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1250:
	.cfi_restore_state
	testb	$1, 13(%rsi)
	jne	.L1236
	call	session_is_closing
	testl	%eax, %eax
	jne	.L1236
	movq	2352(%r12), %rax
	leaq	16(%r13), %rbx
	leaq	2200(%r12), %r14
	cmpq	%rax, 2344(%r12)
	jnb	.L1239
	movl	$152, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1240
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	call	nghttp2_frame_ping_init@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L1251
	addq	$1, 2344(%r12)
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1233
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	call	*%rax
	testl	%eax, %eax
	jne	.L1232
.L1233:
	testb	$1, 2499(%r12)
	jne	.L1230
	movl	2388(%r12), %esi
	addq	$24, %rsp
	movq	%r12, %rdi
	leaq	.LC49(%rip), %rcx
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	session_terminate_session.part.0
.L1232:
	.cfi_restore_state
	movl	$-902, %eax
	jmp	.L1227
.L1251:
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_ping_free@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L1227
.L1239:
	movl	$-904, %eax
	jmp	.L1227
.L1240:
	movl	$-901, %eax
	jmp	.L1227
	.cfi_endproc
.LFE174:
	.size	nghttp2_session_on_ping_received, .-nghttp2_session_on_ping_received
	.section	.rodata.str1.1
.LC50:
	.string	"GOAWAY: stream_id != 0"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"GOAWAY: invalid last_stream_id"
	.section	.rodata.str1.1
.LC52:
	.string	"rv == 0"
	.text
	.p2align 4
	.globl	nghttp2_session_on_goaway_received
	.type	nghttp2_session_on_goaway_received, @function
nghttp2_session_on_goaway_received:
.LFB176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jne	.L1289
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jle	.L1259
	movl	%eax, %edx
	andl	$1, %edx
	cmpb	$0, 2498(%rdi)
	je	.L1260
	xorl	$1, %edx
.L1260:
	testl	%edx, %edx
	je	.L1261
.L1259:
	cmpl	2400(%r12), %eax
	jg	.L1261
	orb	$8, 2499(%r12)
	movl	16(%r13), %eax
	movl	%eax, 2400(%r12)
	movq	2032(%r12), %rax
	testq	%rax, %rax
	je	.L1270
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1265
.L1270:
	movl	16(%r13), %eax
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r12, -64(%rbp)
	leaq	find_stream_on_goaway_func(%rip), %rsi
	movq	$0, -56(%rbp)
	movl	%eax, -48(%rbp)
	movl	$0, -44(%rbp)
	call	nghttp2_map_each@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1290
	movq	-56(%rbp), %rdx
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	160(%rdx), %r13
	movl	192(%rdx), %esi
	movq	%r12, %rdi
	movq	$0, 160(%rdx)
	movl	$7, %edx
	call	nghttp2_session_close_stream
	cmpl	$-900, %eax
	jl	.L1291
	movq	%r13, %rdx
.L1271:
	testq	%rdx, %rdx
	jne	.L1274
.L1252:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1292
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1261:
	.cfi_restore_state
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1266
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1265
.L1266:
	testb	$1, 2499(%r12)
	jne	.L1255
	movl	2388(%r12), %esi
	leaq	.LC51(%rip), %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r14d
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1291:
	testq	%r13, %r13
	je	.L1277
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	%r13, %rdx
	movq	160(%r13), %r13
	movq	$0, 160(%rdx)
	testq	%r13, %r13
	jne	.L1273
.L1277:
	movl	%eax, %r14d
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1257
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	call	*%rax
	testl	%eax, %eax
	jne	.L1265
.L1257:
	testb	$1, 2499(%r12)
	jne	.L1255
	movl	2388(%r12), %esi
	leaq	.LC50(%rip), %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r14d
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1255:
	xorl	%r14d, %r14d
	jmp	.L1252
.L1265:
	movl	$-902, %r14d
	jmp	.L1252
.L1292:
	call	__stack_chk_fail@PLT
.L1290:
	leaq	__PRETTY_FUNCTION__.6670(%rip), %rcx
	movl	$2463, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC52(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE176:
	.size	nghttp2_session_on_goaway_received, .-nghttp2_session_on_goaway_received
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"WINDOW_UPDATE: window_size_increment == 0"
	.section	.rodata.str1.1
.LC54:
	.string	"WINDOW_UPDATE to idle stream"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"WINDOW_UPADATE to reserved stream"
	.text
	.p2align 4
	.globl	nghttp2_session_on_window_update_received
	.type	nghttp2_session_on_window_update_received, @function
nghttp2_session_on_window_update_received:
.LFB180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	8(%rsi), %esi
	testl	%esi, %esi
	je	.L1360
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L1361
	testl	%eax, %eax
	jne	.L1327
.L1307:
	cmpl	2384(%r12), %esi
	jg	.L1308
.L1309:
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1297
	testb	$2, 236(%rax)
	jne	.L1297
	movl	228(%rax), %eax
	cmpl	$5, %eax
	je	.L1297
	cmpl	$4, %eax
	je	.L1362
.L1313:
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L1359
	movl	$2147483647, %edx
	movl	196(%r14), %ecx
	subl	%eax, %edx
	cmpl	%ecx, %edx
	jl	.L1363
	addl	%ecx, %eax
	movl	%eax, 196(%r14)
	testl	%eax, %eax
	jg	.L1324
.L1326:
	movq	2032(%r12), %rax
	testq	%rax, %rax
	jne	.L1325
	.p2align 4,,10
	.p2align 3
.L1297:
	xorl	%eax, %eax
.L1293:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1360:
	.cfi_restore_state
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L1359
	movl	$2147483647, %edx
	movl	2404(%rdi), %ecx
	subl	%eax, %edx
	cmpl	%ecx, %edx
	jl	.L1364
	addl	%ecx, %eax
	movl	%eax, 2404(%rdi)
	movq	2032(%rdi), %rax
	testq	%rax, %rax
	je	.L1297
.L1325:
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L1297
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1361:
	testl	%eax, %eax
	jne	.L1307
.L1327:
	cmpl	2380(%r12), %esi
	jle	.L1309
.L1308:
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1312
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1304
.L1312:
	testb	$1, 2499(%r12)
	jne	.L1297
	movl	2388(%r12), %esi
	leaq	.LC54(%rip), %rcx
.L1356:
	movl	$1, %edx
.L1355:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	session_terminate_session.part.0
	.p2align 4,,10
	.p2align 3
.L1359:
	.cfi_restore_state
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1322
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1304
.L1322:
	testb	$1, 2499(%r12)
	jne	.L1297
	movl	2388(%r12), %esi
	leaq	.LC53(%rip), %rcx
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1362:
	movl	192(%r14), %eax
	testl	%eax, %eax
	je	.L1314
	andl	$1, %eax
	cmpb	$0, 2498(%r12)
	jne	.L1365
.L1315:
	testl	%eax, %eax
	jne	.L1313
.L1314:
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1318
	movq	2240(%r12), %rcx
	movl	$-505, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L1318
.L1304:
	movl	$-902, %eax
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1318:
	testb	$1, 2499(%r12)
	jne	.L1297
	movl	2388(%r12), %esi
	leaq	.LC55(%rip), %rcx
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1364:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1305
	movq	2240(%rdi), %rcx
	movl	$-524, %edx
	movq	%r13, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L1304
.L1305:
	testb	$1, 2499(%r12)
	jne	.L1297
	movl	2388(%r12), %esi
	xorl	%ecx, %ecx
	movl	$3, %edx
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	%r14, %rdi
	call	nghttp2_stream_check_deferred_by_flow_control@PLT
	testl	%eax, %eax
	je	.L1326
	movl	$4, %esi
	movq	%r14, %rdi
	call	nghttp2_stream_resume_deferred_item@PLT
	cmpl	$-900, %eax
	jge	.L1326
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1365:
	xorl	$1, %eax
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1363:
	movl	8(%r13), %esi
	movl	$3, %edx
	movq	%r12, %rdi
	call	nghttp2_session_add_rst_stream
	testl	%eax, %eax
	jne	.L1293
	movq	2040(%r12), %rax
	testq	%rax, %rax
	je	.L1297
	movq	2240(%r12), %rcx
	movl	$-524, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L1297
	jmp	.L1304
	.cfi_endproc
.LFE180:
	.size	nghttp2_session_on_window_update_received, .-nghttp2_session_on_window_update_received
	.p2align 4
	.globl	nghttp2_session_on_altsvc_received
	.type	nghttp2_session_on_altsvc_received, @function
nghttp2_session_on_altsvc_received:
.LFB182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	16(%rsi), %rbx
	movl	8(%rsi), %esi
	movq	8(%rbx), %rax
	testl	%esi, %esi
	jne	.L1367
	testq	%rax, %rax
	je	.L1368
.L1375:
	cmpq	$0, 24(%rbx)
	je	.L1368
	movq	2032(%rdi), %rax
	testq	%rax, %rax
	je	.L1371
	movq	2240(%rdi), %rdx
	movq	%r12, %rsi
	call	*%rax
	testl	%eax, %eax
	jne	.L1374
.L1371:
	xorl	%eax, %eax
.L1366:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1367:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L1373
.L1368:
	movq	2040(%rdi), %rax
	testq	%rax, %rax
	je	.L1371
	movq	2240(%rdi), %rcx
	movl	$-505, %edx
	movq	%r12, %rsi
	call	*%rax
	testl	%eax, %eax
	je	.L1371
.L1374:
	movl	$-902, %eax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	%rdi, -24(%rbp)
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L1371
	testb	$2, 236(%rax)
	jne	.L1371
	movl	228(%rax), %eax
	movq	-24(%rbp), %rdi
	subl	$3, %eax
	andl	$-3, %eax
	jne	.L1375
	jmp	.L1371
	.cfi_endproc
.LFE182:
	.size	nghttp2_session_on_altsvc_received, .-nghttp2_session_on_altsvc_received
	.p2align 4
	.globl	nghttp2_session_on_origin_received
	.type	nghttp2_session_on_origin_received, @function
nghttp2_session_on_origin_received:
.LFB183:
	.cfi_startproc
	endbr64
	movq	2032(%rdi), %rax
	testq	%rax, %rax
	je	.L1414
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	2240(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	testl	%eax, %eax
	jne	.L1415
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1414:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
.L1415:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$-902, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE183:
	.size	nghttp2_session_on_origin_received, .-nghttp2_session_on_origin_received
	.p2align 4
	.globl	nghttp2_session_on_data_received
	.type	nghttp2_session_on_data_received, @function
nghttp2_session_on_data_received:
.LFB187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	8(%rsi), %esi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L1417
	movq	%rax, %r12
	testb	$2, 236(%rax)
	jne	.L1417
	movl	228(%rax), %eax
	subl	$3, %eax
	andl	$-3, %eax
	je	.L1417
	testb	$4, 2484(%r13)
	je	.L1435
.L1418:
	movq	2032(%r13), %rax
	testq	%rax, %rax
	je	.L1423
.L1421:
	movq	2240(%r13), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1436
.L1423:
	testb	$1, 13(%rbx)
	je	.L1417
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_stream_shutdown@PLT
	movzbl	237(%r12), %eax
	andl	$3, %eax
	cmpb	$3, %al
	je	.L1437
	.p2align 4,,10
	.p2align 3
.L1417:
	xorl	%eax, %eax
.L1416:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1435:
	.cfi_restore_state
	testb	$1, 13(%rbx)
	jne	.L1438
	movq	2032(%r13), %rax
	testq	%rax, %rax
	jne	.L1421
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	%r12, %rdi
	call	nghttp2_http_on_remote_end_stream@PLT
	testl	%eax, %eax
	je	.L1418
	movl	192(%r12), %esi
	movl	$1, %edx
	movq	%r13, %rdi
	call	nghttp2_session_add_rst_stream
	cmpl	$-900, %eax
	jl	.L1416
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_stream_shutdown@PLT
	xorl	%eax, %eax
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1437:
	movl	192(%r12), %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	nghttp2_session_close_stream
	cmpl	$-900, %eax
	jge	.L1417
	jmp	.L1416
.L1436:
	movl	$-902, %eax
	jmp	.L1416
	.cfi_endproc
.LFE187:
	.size	nghttp2_session_on_data_received, .-nghttp2_session_on_data_received
	.p2align 4
	.globl	nghttp2_session_update_recv_stream_window_size
	.type	nghttp2_session_update_recv_stream_window_size, @function
nghttp2_session_update_recv_stream_window_size:
.LFB190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	212(%rsi), %edi
	movl	200(%rsi), %esi
	movl	%edi, %eax
	subl	%edx, %eax
	cmpl	%eax, %esi
	jg	.L1440
	movl	$2147483647, %eax
	subl	%edx, %eax
	cmpl	%eax, %esi
	jg	.L1440
	addl	%edx, %esi
	movl	%esi, 200(%rbx)
	testl	%ecx, %ecx
	je	.L1443
	testb	$1, 2484(%r12)
	jne	.L1443
	cmpb	$0, 239(%rbx)
	je	.L1450
	.p2align 4,,10
	.p2align 3
.L1443:
	xorl	%eax, %eax
.L1439:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L1443
	movl	192(%rbx), %eax
	leaq	2200(%r12), %r13
	movl	$152, %esi
	movl	200(%rbx), %r15d
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1445
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	-52(%rbp), %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r15d, %ecx
	call	nghttp2_frame_window_update_init@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L1444
	movl	$0, 200(%rbx)
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1440:
	movl	192(%rbx), %esi
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$3, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	nghttp2_session_add_rst_stream
.L1444:
	.cfi_restore_state
	movq	%r14, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	jmp	.L1439
.L1445:
	movl	$-901, %eax
	jmp	.L1439
	.cfi_endproc
.LFE190:
	.size	nghttp2_session_update_recv_stream_window_size, .-nghttp2_session_update_recv_stream_window_size
	.p2align 4
	.globl	nghttp2_session_update_recv_connection_window_size
	.type	nghttp2_session_update_recv_connection_window_size, @function
nghttp2_session_update_recv_connection_window_size:
.LFB191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	2420(%rdi), %edi
	movl	2408(%r12), %eax
	movl	%edi, %edx
	subl	%esi, %edx
	cmpl	%edx, %eax
	jg	.L1452
	movl	$2147483647, %edx
	subl	%esi, %edx
	cmpl	%edx, %eax
	jg	.L1452
	addl	%eax, %esi
	movl	%esi, 2408(%r12)
	testb	$1, 2484(%r12)
	je	.L1453
.L1455:
	xorl	%eax, %eax
.L1451:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1453:
	.cfi_restore_state
	cmpb	$0, 2500(%r12)
	jne	.L1455
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L1455
	leaq	2200(%r12), %r14
	movl	$152, %esi
	movl	2408(%r12), %r15d
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1457
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%r15d, %ecx
	call	nghttp2_frame_window_update_init@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L1456
	movl	$0, 2408(%r12)
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	%r13, %rdi
	movl	%eax, -36(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1452:
	testb	$1, 2499(%r12)
	jne	.L1455
	movl	2388(%r12), %esi
	addq	$16, %rsp
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movl	$3, %edx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	session_terminate_session.part.0
.L1457:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L1451
	.cfi_endproc
.LFE191:
	.size	nghttp2_session_update_recv_connection_window_size, .-nghttp2_session_update_recv_connection_window_size
	.section	.rodata.str1.1
.LC56:
	.string	"0"
	.text
	.p2align 4
	.type	session_after_frame_sent1, @function
session_after_frame_sent1:
.LFB131:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	344(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	12(%r13), %eax
	testb	%al, %al
	je	.L1560
	andl	$-5, %eax
	cmpb	$1, %al
	je	.L1474
	movq	2064(%r12), %rax
	testq	%rax, %rax
	je	.L1476
.L1475:
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1472
.L1476:
	cmpb	$8, 12(%r13)
	ja	.L1517
	movzbl	12(%r13), %eax
	leaq	.L1479(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1479:
	.long	.L1517-.L1479
	.long	.L1483-.L1479
	.long	.L1482-.L1479
	.long	.L1481-.L1479
	.long	.L1517-.L1479
	.long	.L1517-.L1479
	.long	.L1517-.L1479
	.long	.L1480-.L1479
	.long	.L1478-.L1479
	.text
	.p2align 4,,10
	.p2align 3
.L1465:
	movl	%eax, 2404(%r12)
.L1467:
	movq	2064(%r12), %rax
	testq	%rax, %rax
	je	.L1517
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1472
.L1517:
	xorl	%r14d, %r14d
.L1462:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1561
	leaq	-32(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	.cfi_restore_state
	leaq	352(%rdi), %rdi
	call	nghttp2_bufs_next_present@PLT
	testl	%eax, %eax
	jne	.L1517
	movq	2064(%r12), %rax
	testq	%rax, %rax
	jne	.L1475
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1560:
	movl	8(%r13), %esi
	call	nghttp2_map_find@PLT
	movq	%rax, %rbx
	movl	2404(%r12), %eax
	subl	0(%r13), %eax
	testq	%rbx, %rbx
	je	.L1465
	testb	$2, 236(%rbx)
	jne	.L1465
	cmpl	$5, 228(%rbx)
	je	.L1465
	movl	%eax, 2404(%r12)
	movq	0(%r13), %rax
	subl	%eax, 196(%rbx)
	cmpb	$0, 113(%r13)
	je	.L1467
	movq	%rbx, %rdi
	call	nghttp2_stream_detach_item@PLT
	movl	%eax, %r14d
	cmpl	$-900, %eax
	jl	.L1462
	movq	2064(%r12), %rax
	testq	%rax, %rax
	je	.L1473
	movq	2240(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1472
.L1473:
	testb	$1, 13(%r13)
	je	.L1517
	movl	$2, %esi
	movq	%rbx, %rdi
	call	nghttp2_stream_shutdown@PLT
	movzbl	237(%rbx), %eax
	andl	$3, %eax
	cmpb	$3, %al
	jne	.L1517
	movl	192(%rbx), %esi
	xorl	%edx, %edx
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1478:
	movl	8(%r13), %esi
	testl	%esi, %esi
	jne	.L1510
	movb	$0, 2500(%r12)
	testb	$1, 2484(%r12)
	je	.L1511
	movl	2420(%r12), %eax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	leaq	2408(%r12), %rdx
	leaq	2412(%r12), %rsi
	xorl	%r8d, %r8d
	pushq	%rax
.L1555:
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	session_update_consumed_size
	movl	%eax, %r14d
	popq	%rax
	popq	%rdx
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1480:
	movzbl	96(%r13), %edx
	testb	$2, %dl
	jne	.L1517
	movzbl	2499(%r12), %eax
	leaq	find_stream_on_goaway_func(%rip), %rsi
	movq	%r12, %rdi
	movq	%r12, -64(%rbp)
	movq	$0, -56(%rbp)
	movl	%eax, %ecx
	orl	$2, %ecx
	andl	$1, %edx
	leaq	-64(%rbp), %rdx
	cmovne	%ecx, %eax
	orl	$4, %eax
	movb	%al, 2499(%r12)
	movl	16(%r13), %eax
	movl	$1, -44(%rbp)
	movl	%eax, -48(%rbp)
	call	nghttp2_map_each@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1562
	movq	-56(%rbp), %rax
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%rbx, %rax
.L1505:
	testq	%rax, %rax
	je	.L1462
	movq	160(%rax), %rbx
	movl	192(%rax), %esi
	movl	$7, %edx
	movq	%r12, %rdi
	movq	$0, 160(%rax)
	call	nghttp2_session_close_stream
	cmpl	$-900, %eax
	jge	.L1519
	testq	%rbx, %rbx
	je	.L1506
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	%rbx, %rdx
	movq	160(%rbx), %rbx
	movq	$0, 160(%rdx)
	testq	%rbx, %rbx
	jne	.L1507
.L1506:
	movl	%eax, %r14d
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1481:
	movl	16(%r13), %edx
	movl	8(%r13), %esi
.L1554:
	movq	%r12, %rdi
	call	nghttp2_session_close_stream
	movl	%eax, %r14d
.L1552:
	cmpl	$-900, %r14d
	jge	.L1517
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1482:
	cmpb	$0, 2498(%r12)
	jne	.L1517
	movl	8(%r13), %esi
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1563
	leaq	16(%r13), %rdx
	movq	%r12, %rdi
	call	nghttp2_session_reprioritize_stream
	movl	%eax, %r14d
	cmpl	$-900, %eax
	jl	.L1462
.L1502:
	movq	%r12, %rdi
	call	nghttp2_session_adjust_idle_stream
	movl	%eax, %r14d
	cmpl	$-900, %eax
	jge	.L1517
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1483:
	movl	8(%r13), %esi
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1517
	movzbl	236(%rax), %edx
	testb	$2, %dl
	jne	.L1517
	cmpl	$5, 228(%rax)
	je	.L1517
	movl	56(%r13), %eax
	cmpl	$2, %eax
	je	.L1484
	ja	.L1485
	testl	%eax, %eax
	je	.L1564
.L1487:
	movl	$2, 228(%rbx)
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1485:
	cmpl	$3, %eax
	jne	.L1565
.L1488:
	testb	$1, 13(%r13)
	jne	.L1558
.L1494:
	movzbl	237(%rbx), %eax
	andl	$3, %eax
	cmpb	$3, %al
	je	.L1495
.L1497:
	cmpq	$0, 104(%r13)
	je	.L1517
	movl	8(%r13), %edx
	leaq	96(%r13), %rcx
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_submit_data@PLT
	movl	%eax, %r14d
	cmpl	$-900, %eax
	jge	.L1517
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1564:
	movl	$1, 228(%rbx)
	testb	$1, 13(%r13)
	je	.L1494
.L1558:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	nghttp2_stream_shutdown@PLT
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1511:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	nghttp2_session_update_recv_connection_window_size
	movl	%eax, %r14d
	jmp	.L1552
.L1472:
	movl	$-902, %r14d
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L1517
	testb	$2, 236(%rax)
	jne	.L1517
	cmpl	$5, 228(%rax)
	je	.L1517
	movb	$0, 239(%rax)
	testb	$1, 237(%rax)
	jne	.L1517
	testb	$1, 2484(%r12)
	je	.L1513
	movl	192(%rax), %r8d
	leaq	200(%rax), %rdx
	subq	$8, %rsp
	xorl	%r9d, %r9d
	leaq	204(%rax), %rsi
	movl	212(%rax), %eax
	pushq	%rax
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1563:
	movl	8(%r13), %esi
	testl	%esi, %esi
	je	.L1517
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%r12)
	jne	.L1566
	testl	%eax, %eax
	je	.L1500
.L1515:
	cmpl	2380(%r12), %esi
	jle	.L1517
.L1501:
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	leaq	16(%r13), %rcx
	movl	$5, %r8d
	movq	%r12, %rdi
	call	nghttp2_session_open_stream
	testq	%rax, %rax
	jne	.L1502
	movl	$-901, %r14d
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1484:
	andl	$-2, %edx
	movb	%dl, 236(%rbx)
	addq	$1, 2288(%r12)
	jmp	.L1487
.L1495:
	movl	192(%rbx), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	nghttp2_session_close_stream
	movl	%eax, %r14d
	cmpl	$-900, %eax
	jge	.L1497
	jmp	.L1462
.L1513:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_update_recv_stream_window_size
	movl	%eax, %r14d
	jmp	.L1552
.L1566:
	testl	%eax, %eax
	je	.L1515
.L1500:
	cmpl	2384(%r12), %esi
	jle	.L1517
	jmp	.L1501
.L1561:
	call	__stack_chk_fail@PLT
.L1562:
	leaq	__PRETTY_FUNCTION__.6670(%rip), %rcx
	movl	$2463, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC52(%rip), %rdi
	call	__assert_fail@PLT
.L1565:
	leaq	__PRETTY_FUNCTION__.6705(%rip), %rcx
	movl	$2662, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE131:
	.size	session_after_frame_sent1, .-session_after_frame_sent1
	.p2align 4
	.globl	nghttp2_session_want_read
	.type	nghttp2_session_want_read, @function
nghttp2_session_want_read:
.LFB206:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$2, 2499(%rdi)
	je	.L1575
	ret
	.p2align 4,,10
	.p2align 3
.L1575:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	nghttp2_map_size@PLT
	subq	2320(%rbx), %rax
	movq	%rax, %rdx
	movl	$1, %eax
	cmpq	2328(%rbx), %rdx
	jne	.L1567
	xorl	%eax, %eax
	testb	$12, 2499(%rbx)
	sete	%al
.L1567:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE206:
	.size	nghttp2_session_want_read, .-nghttp2_session_want_read
	.p2align 4
	.globl	nghttp2_session_want_write
	.type	nghttp2_session_want_write, @function
nghttp2_session_want_write:
.LFB207:
	.cfi_startproc
	endbr64
	testb	$2, 2499(%rdi)
	jne	.L1586
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 344(%rdi)
	je	.L1579
.L1580:
	movl	$1, %eax
.L1576:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1579:
	.cfi_restore_state
	cmpq	$0, 272(%rdi)
	jne	.L1580
	cmpq	$0, 296(%rdi)
	jne	.L1580
	leaq	64(%rdi), %rdi
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	jne	.L1581
	movl	2404(%rbx), %eax
	testl	%eax, %eax
	jg	.L1580
.L1581:
	xorl	%eax, %eax
	cmpq	$0, 320(%rbx)
	je	.L1576
	movl	2436(%rbx), %eax
	cmpq	%rax, 2288(%rbx)
	setb	%al
	movzbl	%al, %eax
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1586:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE207:
	.size	nghttp2_session_want_write, .-nghttp2_session_want_write
	.p2align 4
	.globl	nghttp2_session_add_ping
	.type	nghttp2_session_add_ping, @function
nghttp2_session_add_ping:
.LFB208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movzbl	%sil, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	2200(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	andl	$1, %ebx
	je	.L1588
	movq	2352(%rdi), %rax
	cmpq	%rax, 2344(%rdi)
	jnb	.L1591
.L1588:
	movl	$152, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1592
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movq	-56(%rbp), %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	nghttp2_frame_ping_init@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L1600
	testb	%bl, %bl
	je	.L1587
	addq	$1, 2344(%r12)
.L1587:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%eax, -56(%rbp)
	call	nghttp2_frame_ping_free@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	movl	-56(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1592:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L1587
.L1591:
	movl	$-904, %eax
	jmp	.L1587
	.cfi_endproc
.LFE208:
	.size	nghttp2_session_add_ping, .-nghttp2_session_add_ping
	.p2align 4
	.globl	nghttp2_session_add_goaway
	.type	nghttp2_session_add_goaway, @function
nghttp2_session_add_goaway:
.LFB209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	2200(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	movl	%r9d, -56(%rbp)
	testl	%esi, %esi
	je	.L1602
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L1618
.L1603:
	testl	%eax, %eax
	jne	.L1609
.L1602:
	testq	%r13, %r13
	je	.L1608
	leaq	8(%r13), %rax
	cmpq	$16384, %rax
	ja	.L1609
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1610
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
.L1605:
	movl	$152, %esi
	movq	%r15, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1619
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	movq	-64(%rbp), %rcx
	movl	-52(%rbp), %edx
	movq	%r14, %rdi
	cmpl	%ebx, 2396(%r12)
	movq	%r13, %r8
	cmovle	2396(%r12), %ebx
	movl	%ebx, %esi
	call	nghttp2_frame_goaway_init@PLT
	movzbl	-56(%rbp), %eax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	%al, 96(%r14)
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L1620
.L1601:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1618:
	.cfi_restore_state
	xorl	$1, %eax
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1608:
	movq	$0, -64(%rbp)
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_goaway_free@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1609:
	.cfi_restore_state
	movl	$-501, %eax
	jmp	.L1601
.L1610:
	movl	$-901, %eax
	jmp	.L1601
.L1619:
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	$-901, %eax
	jmp	.L1601
	.cfi_endproc
.LFE209:
	.size	nghttp2_session_add_goaway, .-nghttp2_session_add_goaway
	.p2align 4
	.globl	nghttp2_session_add_window_update
	.type	nghttp2_session_add_window_update, @function
nghttp2_session_add_window_update:
.LFB210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	2200(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movl	%esi, -52(%rbp)
	movl	$152, %esi
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L1623
	movq	%rax, %rdi
	movq	%rax, %r13
	call	nghttp2_outbound_item_init@PLT
	movzbl	-52(%rbp), %esi
	movq	%r13, %rdi
	movl	%r14d, %ecx
	movl	%ebx, %edx
	call	nghttp2_frame_window_update_init@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L1628
.L1621:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1628:
	.cfi_restore_state
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1623:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L1621
	.cfi_endproc
.LFE210:
	.size	nghttp2_session_add_window_update, .-nghttp2_session_add_window_update
	.p2align 4
	.globl	nghttp2_session_add_settings
	.type	nghttp2_session_add_settings, @function
nghttp2_session_add_settings:
.LFB212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	2200(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movl	%eax, -68(%rbp)
	movl	%esi, %eax
	andl	$1, %eax
	movq	%rdi, -56(%rbp)
	movb	%al, -64(%rbp)
	je	.L1630
	testq	%rcx, %rcx
	jne	.L1633
	movq	2352(%rdi), %rcx
	cmpq	%rcx, 2344(%rdi)
	jnb	.L1659
.L1630:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	nghttp2_iv_check@PLT
	testl	%eax, %eax
	je	.L1633
	movl	$152, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1680
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	jne	.L1682
.L1634:
	cmpb	$0, -64(%rbp)
	jne	.L1635
	movl	$24, %esi
	movq	%r14, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1636
	testq	%rbx, %rbx
	jne	.L1683
	movq	$0, 8(%rax)
.L1638:
	movq	%rbx, 16(%r8)
	movq	%r12, %rdi
	movq	$0, (%r8)
	movq	%r8, -64(%rbp)
	call	nghttp2_outbound_item_init@PLT
	movl	-68(%rbp), %esi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	nghttp2_frame_settings_init@PLT
	movq	-56(%rbp), %rdi
	movq	%r12, %rsi
	call	nghttp2_session_add_item
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L1684
	movq	-56(%rbp), %rax
	movq	2280(%rax), %rax
	testq	%rax, %rax
	je	.L1644
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	%rax, %rdx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1645
.L1646:
	movq	%r8, (%rdx)
.L1643:
	testq	%rbx, %rbx
	je	.L1681
	leaq	-8(,%rbx,8), %rdx
	movq	%rbx, %rsi
	leaq	0(%r13,%rdx), %rax
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1648:
	subq	$8, %rax
	subq	$1, %rsi
	je	.L1649
.L1650:
	cmpl	$3, (%rax)
	jne	.L1648
	movl	4(%rax), %eax
	movq	-56(%rbp), %rcx
	movl	%eax, 2488(%rcx)
.L1649:
	leaq	0(%r13,%rdx), %rax
	movq	%rbx, %rsi
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1651:
	subq	$8, %rax
	subq	$1, %rsi
	je	.L1652
.L1653:
	cmpl	$2, (%rax)
	jne	.L1651
	movl	4(%rax), %eax
	movq	-56(%rbp), %rcx
	movb	%al, 2496(%rcx)
.L1652:
	addq	%rdx, %r13
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1654:
	subq	$8, %r13
	subq	$1, %rbx
	je	.L1681
.L1655:
	cmpl	$8, 0(%r13)
	jne	.L1654
	movl	4(%r13), %eax
	movq	-56(%rbp), %rcx
	movb	%al, 2497(%rcx)
.L1681:
	xorl	%r15d, %r15d
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1635:
	movq	%r12, %rdi
	call	nghttp2_outbound_item_init@PLT
	movl	-68(%rbp), %esi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	nghttp2_frame_settings_init@PLT
	movq	-56(%rbp), %rdi
	movq	%r12, %rsi
	call	nghttp2_session_add_item
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L1685
	cmpl	$-900, %r15d
	jge	.L1642
.L1641:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_frame_settings_free@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
.L1629:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1682:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	nghttp2_frame_iv_copy@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1634
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
.L1680:
	movl	$-901, %r15d
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1683:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	nghttp2_frame_iv_copy@PLT
	movq	-64(%rbp), %r8
	movq	%rax, 8(%r8)
	testq	%rax, %rax
	jne	.L1638
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
.L1636:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	-56(%rbp), %rax
	addq	$1, 2344(%rax)
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1684:
	cmpl	$-900, %eax
	jge	.L1642
	movq	8(%r8), %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	nghttp2_mem_free@PLT
	movq	-56(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	nghttp2_mem_free@PLT
	jmp	.L1641
.L1644:
	movq	-56(%rbp), %rax
	leaq	2280(%rax), %rdx
	jmp	.L1646
.L1633:
	movl	$-501, %r15d
	jmp	.L1629
.L1659:
	movl	$-904, %r15d
	jmp	.L1629
.L1642:
	leaq	__PRETTY_FUNCTION__.7416(%rip), %rcx
	movl	$7049, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE212:
	.size	nghttp2_session_add_settings, .-nghttp2_session_add_settings
	.section	.rodata.str1.1
.LC57:
	.string	"bufs->head == bufs->cur"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"&session->aob.framebufs == bufs"
	.align 8
.LC59:
	.string	"nghttp2_buf_avail(buf) >= datamax"
	.text
	.p2align 4
	.globl	nghttp2_session_pack_data
	.type	nghttp2_session_pack_data, @function
nghttp2_session_pack_data:
.LFB213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	8(%rsi), %r11
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r11, (%rsi)
	jne	.L1715
	movq	2136(%rdi), %rax
	movq	%rdi, %r12
	movq	%rcx, %r13
	movq	%r8, %rbx
	movq	%r9, %r14
	leaq	8(%r11), %r15
	testq	%rax, %rax
	je	.L1716
	subq	$8, %rsp
	movl	2404(%rdi), %ecx
	movl	192(%r9), %edx
	movq	%r11, -88(%rbp)
	movzbl	12(%r13), %esi
	movl	2444(%rdi), %r9d
	pushq	2240(%rdi)
	movl	196(%r14), %r8d
	call	*%rax
	movslq	196(%r14), %rdx
	movslq	2404(%r12), %rcx
	popq	%rsi
	movq	-88(%rbp), %r11
	cmpq	%rdx, %rcx
	popq	%rdi
	cmovg	%rdx, %rcx
	movslq	2444(%r12), %rdx
	cmpq	%rdx, %rcx
	cmovg	%rdx, %rcx
	cmpq	%rax, %rcx
	cmovg	%rax, %rcx
	testq	%rcx, %rcx
	jle	.L1697
	movq	16(%r11), %rax
	subq	32(%r11), %rax
	cmpq	%rcx, %rax
	jb	.L1717
.L1692:
	subq	$8, %rsp
	movq	16(%r15), %rdx
	movq	%rcx, -72(%rbp)
	leaq	-60(%rbp), %r8
	movl	8(%r13), %esi
	movl	$0, -60(%rbp)
	movq	%rbx, %r9
	movq	%r12, %rdi
	pushq	2240(%r12)
	call	*8(%rbx)
	popq	%rdx
	movq	%rax, %r8
	leaq	526(%rax), %rax
	popq	%rcx
	cmpq	$18, %rax
	ja	.L1718
	movl	$262177, %edx
	movl	%r8d, %r12d
	btq	%rax, %rdx
	jnc	.L1697
.L1686:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1719
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1718:
	.cfi_restore_state
	testq	%r8, %r8
	js	.L1697
	movq	-72(%rbp), %rcx
	cmpq	%r8, %rcx
	jb	.L1697
	movq	16(%r15), %rax
	leaq	-9(%rax), %rdi
	addq	%r8, %rax
	movq	%rax, %xmm1
	movq	%rdi, %xmm0
	movl	-60(%rbp), %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r15)
	movb	$0, 13(%r13)
	testb	$1, %al
	je	.L1700
	movb	$1, 17(%rbx)
	testb	$1, 16(%rbx)
	je	.L1700
	testb	$2, %al
	jne	.L1700
	movb	$1, 13(%r13)
	.p2align 4,,10
	.p2align 3
.L1700:
	testb	$4, %al
	je	.L1702
	cmpq	$0, 2152(%r12)
	je	.L1697
	movb	$1, 18(%rbx)
.L1702:
	leaq	256(%r8), %rdx
	movq	%r8, 0(%r13)
	cmpq	%rcx, %rdx
	movq	$0, 16(%r13)
	cmovbe	%rdx, %rcx
	cmpq	%r8, %rcx
	jbe	.L1706
	movq	2128(%r12), %rax
	testq	%rax, %rax
	je	.L1706
	cmpq	%rcx, %rdx
	movq	%r8, -88(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmova	%rcx, %rdx
	movq	2240(%r12), %rcx
	movq	%rdx, -72(%rbp)
	call	*%rax
	cmpq	0(%r13), %rax
	jl	.L1697
	movq	-72(%rbp), %rdx
	movq	-88(%rbp), %r8
	cmpq	%rdx, %rax
	jg	.L1697
.L1703:
	movl	%eax, %r12d
	cmpl	$-900, %eax
	jl	.L1686
	subq	%r8, %rax
	movq	16(%r15), %rdi
	movq	%r13, %rsi
	movq	%rax, 16(%r13)
	call	nghttp2_frame_pack_frame_hd@PLT
	movzbl	18(%rbx), %ecx
	movq	16(%r13), %rdx
	movq	%r13, %rsi
	movq	-80(%rbp), %rdi
	call	nghttp2_frame_add_pad@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L1686
	movq	176(%r14), %rax
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	%rax, 184(%r14)
	call	nghttp2_stream_reschedule@PLT
	cmpq	$0, 0(%r13)
	jne	.L1686
	movl	-60(%rbp), %eax
	andl	$3, %eax
	cmpl	$3, %eax
	movl	$-535, %eax
	cmove	%eax, %r12d
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1717:
	leaq	352(%r12), %rdx
	leaq	10(%rcx), %rsi
	movq	%rcx, -96(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r11, -104(%rbp)
	call	nghttp2_bufs_realloc@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1720
	cmpq	-80(%rbp), %rdx
	jne	.L1721
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdx
	movq	16(%rdx), %rax
	leaq	8(%rdx), %r15
	subq	32(%rdx), %rax
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	%r8, %rax
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	16(%r11), %rax
	movq	%rdx, %rcx
	subq	32(%r11), %rax
.L1689:
	cmpq	%rax, %rcx
	jbe	.L1692
	leaq	__PRETTY_FUNCTION__.7440(%rip), %rcx
	movl	$7149, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC59(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	-104(%rbp), %r11
	movq	-72(%rbp), %rcx
	movq	16(%r11), %rax
	subq	32(%r11), %rax
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1697:
	movl	$-902, %r12d
	jmp	.L1686
.L1719:
	call	__stack_chk_fail@PLT
.L1715:
	leaq	__PRETTY_FUNCTION__.7440(%rip), %rcx
	movl	$7104, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC57(%rip), %rdi
	call	__assert_fail@PLT
.L1721:
	leaq	__PRETTY_FUNCTION__.7440(%rip), %rcx
	movl	$7140, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC58(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE213:
	.size	nghttp2_session_pack_data, .-nghttp2_session_pack_data
	.section	.rodata.str1.1
.LC60:
	.string	"stream->item == item"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"session->remote_window_size > 0"
	.align 8
.LC62:
	.string	"session->last_sent_stream_id < frame->hd.stream_id"
	.align 8
.LC63:
	.string	"session->obq_flood_counter_ > 0"
	.align 8
.LC64:
	.string	"session->last_sent_stream_id + 2 <= frame->push_promise.promised_stream_id"
	.align 8
.LC65:
	.string	"session->callbacks.pack_extension_callback"
	.section	.rodata.str1.1
.LC66:
	.string	"buf->pos == buf->last"
	.text
	.p2align 4
	.type	nghttp2_session_mem_send_internal, @function
nghttp2_session_mem_send_internal:
.LFB134:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movl	%edx, -60(%rbp)
	call	nghttp2_session_adjust_idle_stream
	cmpl	$-900, %eax
	jl	.L1869
	movl	416(%r14), %eax
	leaq	2200(%r14), %r12
	leaq	.L1740(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1731:
	cmpl	$2, %eax
	je	.L1726
.L2024:
	ja	.L1727
	testl	%eax, %eax
	je	.L1728
.L1995:
	movq	360(%r14), %rdx
	movq	24(%rdx), %rax
	cmpq	32(%rdx), %rax
	jne	.L1851
	movl	-60(%rbp), %eax
	movq	%r14, %rdi
	testl	%eax, %eax
	jne	.L2020
	call	session_after_frame_sent1
	testl	%eax, %eax
	js	.L2023
	movq	%r14, %rdi
.L2020:
	call	session_after_frame_sent2
	testl	%eax, %eax
	js	.L1856
.L1829:
	movl	416(%r14), %eax
	cmpl	$2, %eax
	jne	.L2024
.L1726:
	movq	344(%r14), %r13
	movq	%r14, %rdi
	movl	8(%r13), %esi
	call	nghttp2_map_find@PLT
	movq	344(%r14), %r10
	testq	%rax, %rax
	je	.L1859
	testb	$2, 236(%rax)
	jne	.L1859
	cmpl	$5, 228(%rax)
	movq	%rax, -56(%rbp)
	je	.L1859
	movq	360(%r14), %rax
	movq	(%r10), %rcx
	movq	%r10, %rsi
	movq	%r14, %rdi
	movq	2240(%r14), %r9
	subq	16(%r10), %rcx
	leaq	96(%r10), %r8
	movq	24(%rax), %rdx
	call	*2152(%r14)
	movl	%eax, %r15d
	cmpl	$-503, %eax
	jge	.L1860
	cmpl	$-526, %eax
	jl	.L1892
	leal	526(%rax), %ecx
	movl	$1, %eax
	movq	-56(%rbp), %r11
	salq	%cl, %rax
	testl	$4194337, %eax
	jne	.L1861
.L1892:
	movq	$-902, %rax
.L1722:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1727:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L1731
	movq	360(%r14), %rdx
	movq	24(%rdx), %rax
	cmpq	32(%rdx), %rax
	jne	.L1870
.L2005:
	movq	344(%r14), %rdi
	movq	%r12, %rsi
.L2006:
	call	nghttp2_outbound_item_free@PLT
	movq	344(%r14), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	leaq	352(%r14), %rdi
	movq	$0, 344(%r14)
	call	nghttp2_bufs_reset@PLT
	movl	$0, 416(%r14)
.L1728:
	movq	272(%r14), %r13
	testq	%r13, %r13
	jne	.L2025
	movq	296(%r14), %r13
	testq	%r13, %r13
	jne	.L2026
	movl	2436(%r14), %eax
	cmpq	%rax, 2288(%r14)
	jnb	.L1735
	movq	320(%r14), %r13
	testq	%r13, %r13
	jne	.L2027
.L1735:
	movl	2404(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L1736
.L1737:
	xorl	%eax, %eax
.L2030:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1736:
	.cfi_restore_state
	leaq	32(%r14), %rdi
	call	nghttp2_stream_next_outbound_item@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1737
	.p2align 4,,10
	.p2align 3
.L1733:
	movzbl	12(%r13), %eax
	cmpb	$9, %al
	ja	.L1738
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1740:
	.long	.L1749-.L1740
	.long	.L1748-.L1740
	.long	.L1747-.L1740
	.long	.L1746-.L1740
	.long	.L1745-.L1740
	.long	.L1744-.L1740
	.long	.L1743-.L1740
	.long	.L1742-.L1740
	.long	.L1741-.L1740
	.long	.L1739-.L1740
	.text
	.p2align 4,,10
	.p2align 3
.L1860:
	testl	%eax, %eax
	jne	.L1892
.L1862:
	movq	%r14, %rdi
	call	session_after_frame_sent1
	testl	%eax, %eax
	js	.L2028
	movq	%r14, %rdi
	call	session_after_frame_sent2
	testl	%eax, %eax
	js	.L2029
	cmpl	$-526, %r15d
	jne	.L1829
	xorl	%eax, %eax
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2025:
	leaq	272(%r14), %rdi
	call	nghttp2_outbound_queue_pop@PLT
	movb	$0, 144(%r13)
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	%r12, %rsi
	movq	%r10, %rdi
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2026:
	leaq	296(%r14), %rdi
	call	nghttp2_outbound_queue_pop@PLT
	movb	$0, 144(%r13)
	jmp	.L1733
.L1861:
	cmpl	$-521, %r15d
	jne	.L1863
	movq	%r11, %rdi
	call	nghttp2_stream_detach_item@PLT
	cmpl	$-900, %eax
	jl	.L1869
	movl	8(%r13), %esi
	movl	$2, %edx
	movq	%r14, %rdi
	call	nghttp2_session_add_rst_stream
	cmpl	$-900, %eax
	jge	.L2005
	.p2align 4,,10
	.p2align 3
.L1869:
	cltq
.L2051:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1741:
	.cfi_restore_state
	movq	%r14, %rdi
	movl	8(%r13), %r15d
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	testl	%r15d, %r15d
	jne	.L2031
.L1807:
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_window_update@PLT
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1742:
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_goaway@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L1785
	movl	16(%r13), %eax
	movl	%eax, 2396(%r14)
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	352(%r14), %rax
	cmpb	$0, 12(%r13)
	movq	%r13, 344(%r14)
	movq	%rax, 360(%r14)
	je	.L1839
	movq	2056(%r14), %rax
	testq	%rax, %rax
	je	.L1840
	movq	2240(%r14), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
	cmpl	$-535, %eax
	je	.L2032
	testl	%eax, %eax
	jne	.L1892
.L1840:
	movl	$1, 416(%r14)
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1748:
	movl	56(%r13), %r8d
	movl	8(%r13), %esi
	testl	%r8d, %r8d
	jne	.L1765
	movq	112(%r13), %r9
	leaq	24(%r13), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	nghttp2_session_open_stream
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2033
	cmpb	$0, 124(%r13)
	jne	.L2010
	testb	$8, 2499(%r14)
	jne	.L2017
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2017
	testb	$4, 2484(%r14)
	je	.L2034
.L1771:
	movq	48(%r13), %rdx
	movq	40(%r13), %rsi
	leaq	664(%r14), %r15
	movq	%r15, %rdi
	call	nghttp2_hd_deflate_bound@PLT
	addq	$5, %rax
	cmpq	2360(%r14), %rax
	ja	.L2016
	movq	%r15, %rdx
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_headers@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L1785
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	session_headers_add_pad
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L1785
	movl	56(%r13), %edi
	testl	%edi, %edi
	jne	.L1787
	movl	8(%r13), %eax
	cmpl	%eax, 2380(%r14)
	jge	.L2035
	movl	%eax, 2380(%r14)
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1749:
	movl	8(%r13), %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1885
	testb	$2, 236(%rax)
	jne	.L1885
	cmpl	$5, 228(%rax)
	je	.L1885
	cmpq	%r13, 176(%rax)
	jne	.L2036
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2037
	testb	$2, 237(%r15)
	jne	.L1886
	movl	192(%r15), %eax
	testl	%eax, %eax
	je	.L1752
	andl	$1, %eax
	cmpb	$0, 2498(%r14)
	jne	.L2038
.L1753:
	testl	%eax, %eax
	je	.L1752
	movl	228(%r15), %eax
	cmpl	$3, %eax
	je	.L1887
	cmpl	$4, %eax
	je	.L2039
.L1754:
	movslq	2404(%r14), %rdx
	movl	$16384, %esi
	movslq	2444(%r14), %rax
	cmpl	$16384, %edx
	movq	%rdx, %rcx
	cmovg	%rsi, %rdx
	cmpq	%rdx, %rax
	cmovg	%rdx, %rax
	movslq	196(%r15), %rdx
	cmpq	%rdx, %rax
	cmovle	%rax, %rdx
	testq	%rdx, %rdx
	jg	.L2040
	testl	%ecx, %ecx
	jle	.L2041
	movq	%r15, %rdi
	movl	$4, %esi
	call	nghttp2_stream_defer_item@PLT
	movl	%eax, %r15d
	cmpl	$-900, %eax
	jge	.L2042
.L1879:
	movzbl	12(%r13), %eax
	testl	%r15d, %r15d
	jns	.L1787
.L1761:
	testb	%al, %al
	je	.L1835
	movq	2072(%r14), %r9
	testq	%r9, %r9
	je	.L1831
	cmpl	$-899, %r15d
	jb	.L1831
	cmpb	$8, %al
	jne	.L1881
.L1831:
	cmpb	$1, %al
	jne	.L2043
	movl	56(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L1835
	cmpb	$0, 124(%r13)
	movl	8(%r13), %esi
	movl	$7, %edx
	je	.L1836
	movl	120(%r13), %edx
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1744:
	movl	8(%r13), %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movzbl	2498(%r14), %ecx
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1793
	testb	$2, 236(%rax)
	jne	.L1793
	cmpl	$5, 228(%rax)
	je	.L1793
	testb	%cl, %cl
	je	.L2019
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	testb	$2, 237(%r15)
	jne	.L2018
	movl	2432(%r14), %esi
	testl	%esi, %esi
	je	.L2044
	cmpl	$3, 228(%r15)
	je	.L2010
	testb	$8, 2499(%r14)
	jne	.L2017
	leaq	664(%r14), %r15
	movq	32(%r13), %rdx
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	nghttp2_hd_deflate_bound@PLT
	cmpq	2360(%r14), %rax
	ja	.L2016
	movq	%r15, %rdx
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_push_promise@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L2045
	.p2align 4,,10
	.p2align 3
.L1785:
	cmpl	$-526, %r15d
	je	.L1737
	cmpl	$-508, %r15d
	je	.L1829
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1745:
	testb	$1, 13(%r13)
	je	.L1790
	movq	2344(%r14), %rax
	testq	%rax, %rax
	je	.L2046
	subq	$1, %rax
	movq	%r14, %rdi
	movq	%rax, 2344(%r14)
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
.L1790:
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_settings@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L1787
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1743:
	testb	$1, 13(%r13)
	je	.L1803
	movq	2344(%r14), %rax
	testq	%rax, %rax
	je	.L2047
	subq	$1, %rax
	movq	%rax, 2344(%r14)
.L1803:
	testb	$1, 2499(%r14)
	jne	.L1805
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_ping@PLT
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_rst_stream@PLT
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_priority@PLT
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L2027:
	leaq	320(%r14), %rdi
	call	nghttp2_outbound_queue_pop@PLT
	movb	$0, 144(%r13)
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1752:
	movl	228(%r15), %eax
	cmpl	$2, %eax
	je	.L1754
	xorl	%edx, %edx
	cmpl	$3, %eax
	sete	%dl
	leal	-514(%rdx,%rdx,2), %edx
.L1750:
	movl	8(%r13), %esi
	movq	%r14, %rdi
	movl	%edx, -56(%rbp)
	call	nghttp2_map_find@PLT
	movl	-56(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1998
	movl	%edx, -56(%rbp)
	call	nghttp2_stream_detach_item@PLT
	movl	%eax, %r15d
	cmpl	$-900, %eax
	jl	.L1879
	movl	-56(%rbp), %edx
	cmpl	$-526, %edx
	je	.L1737
.L1998:
	movzbl	12(%r13), %eax
	movl	%edx, %r15d
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L2043:
	cmpb	$5, %al
	jne	.L1835
	movl	40(%r13), %esi
	movl	$2, %edx
.L1836:
	testl	%esi, %esi
	je	.L1835
	movq	%r14, %rdi
	call	nghttp2_session_close_stream
	cmpl	$-900, %eax
	jl	.L1869
	.p2align 4,,10
	.p2align 3
.L1835:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	344(%r14), %rdi
	movq	%r12, %rsi
	call	nghttp2_outbound_item_free@PLT
	movq	344(%r14), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	leaq	352(%r14), %rdi
	movq	$0, 344(%r14)
	call	nghttp2_bufs_reset@PLT
	movl	$0, 416(%r14)
	cmpl	$-523, %r15d
	je	.L2048
.L1837:
	cmpl	$-900, %r15d
	jge	.L1829
	movslq	%r15d, %rax
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1839:
	cmpb	$0, 114(%r13)
	je	.L1840
	movl	$2, 416(%r14)
	jmp	.L1726
.L1738:
	cmpb	$0, 96(%r13)
	je	.L2049
	cmpb	$10, %al
	je	.L1820
	cmpb	$12, %al
	jne	.L2050
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_origin@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L1787
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L2048:
	testb	$1, 2499(%r14)
	jne	.L1728
	movl	2388(%r14), %esi
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r14, %rdi
	call	session_terminate_session.part.0
	movl	%eax, %r15d
	jmp	.L1837
.L2032:
	movq	2072(%r14), %rax
	testq	%rax, %rax
	je	.L1847
	movq	2240(%r14), %rcx
	movl	$-535, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L1892
.L1847:
	movzbl	12(%r13), %eax
	cmpb	$1, %al
	je	.L1843
	cmpb	$5, %al
	jne	.L2005
	movl	40(%r13), %esi
	movl	$2, %edx
.L1849:
	testl	%esi, %esi
	je	.L2005
	movq	%r14, %rdi
	call	nghttp2_session_close_stream
	cmpl	$-900, %eax
	jge	.L2005
	cltq
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1772
	testb	$2, 236(%rax)
	jne	.L1772
	movl	228(%rax), %eax
	cmpl	$5, %eax
	je	.L1772
	movq	%r14, %rdi
	cmpl	$4, %eax
	je	.L2052
	call	session_is_closing
	testl	%eax, %eax
	jne	.L1779
	testb	$2, 237(%r15)
	jne	.L1779
	cmpb	$0, 2498(%r14)
	je	.L1779
	movl	192(%r15), %eax
	testl	%eax, %eax
	je	.L1780
	testb	$1, %al
	je	.L1779
.L1780:
	cmpl	$1, 228(%r15)
	je	.L2053
.L1779:
	movl	$3, 56(%r13)
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	testb	$2, 237(%r15)
	jne	.L2018
	movl	228(%r15), %eax
	cmpl	$2, %eax
	je	.L1771
	cmpl	$3, %eax
	je	.L2010
	movl	192(%r15), %eax
	testl	%eax, %eax
	je	.L2012
	andl	$1, %eax
	cmpb	$0, 2498(%r14)
	je	.L1784
	xorl	$1, %eax
.L1784:
	testl	%eax, %eax
	jne	.L1771
.L2012:
	movzbl	12(%r13), %eax
	movl	$-514, %r15d
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L2031:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2007
	testb	$2, 236(%rax)
	jne	.L2007
	movl	228(%rax), %edx
	cmpl	$5, %edx
	je	.L2007
	cmpl	$3, %edx
	je	.L2010
	movl	192(%rax), %eax
	cmpl	$4, %edx
	jne	.L1807
	testl	%eax, %eax
	je	.L1807
	andl	$1, %eax
	cmpb	$0, 2498(%r14)
	je	.L1812
	xorl	$1, %eax
.L1812:
	testl	%eax, %eax
	je	.L1807
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L1820:
	movq	%r14, %rdi
	movl	8(%r13), %r15d
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	testl	%r15d, %r15d
	je	.L1824
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2007
	testb	$2, 236(%rax)
	jne	.L2007
	movl	228(%rax), %eax
	cmpl	$5, %eax
	je	.L2007
	cmpl	$3, %eax
	jne	.L1824
.L2010:
	movzbl	12(%r13), %eax
	movl	$-511, %r15d
	jmp	.L1761
.L2045:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	session_headers_add_pad
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L1785
	movl	2380(%r14), %eax
	movl	40(%r13), %edx
	addl	$1, %eax
	cmpl	%edx, %eax
	jge	.L2054
	movl	%edx, 2380(%r14)
	jmp	.L1787
.L1805:
	movq	2072(%r14), %r9
	movl	$-530, %r15d
	testq	%r9, %r9
	je	.L1835
.L1881:
	movq	2240(%r14), %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%r9
	testl	%eax, %eax
	jne	.L1832
	movzbl	12(%r13), %eax
	jmp	.L1831
.L2040:
	leaq	352(%r14), %r11
	movq	%r15, %r9
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	96(%r13), %r8
	movq	%r11, %rsi
	movq	%r11, -56(%rbp)
	call	nghttp2_session_pack_data
	cmpl	$-526, %eax
	je	.L1737
	cmpl	$-508, %eax
	je	.L2055
	cmpl	$-521, %eax
	je	.L2056
	testl	%eax, %eax
	je	.L1787
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	call	nghttp2_stream_detach_item@PLT
	movl	-56(%rbp), %edx
	movl	%eax, %r15d
	cmpl	$-900, %eax
	cmovge	%edx, %r15d
	jmp	.L1879
.L2038:
	xorl	$1, %eax
	jmp	.L1753
.L1843:
	movl	56(%r13), %edx
	testl	%edx, %edx
	jne	.L2005
	movl	8(%r13), %esi
	movl	$7, %edx
	jmp	.L1849
.L1772:
	movl	$3, 56(%r13)
.L2007:
	movzbl	12(%r13), %eax
	movl	$-510, %r15d
	jmp	.L1761
.L2049:
	movq	%r14, %rdi
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	movq	2160(%r14), %rax
	testq	%rax, %rax
	je	.L2057
	movq	352(%r14), %r15
	movl	$16384, %ecx
	movq	2240(%r14), %r8
	movq	%r14, %rdi
	movq	32(%r15), %rsi
	movq	16(%r15), %rdx
	subq	%rsi, %rdx
	cmpq	$16384, %rdx
	cmova	%rcx, %rdx
	movq	%r13, %rcx
	movq	%rdx, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %rdx
	cmpq	$-535, %rax
	je	.L2058
	testq	%rax, %rax
	js	.L1895
	cmpq	%rax, %rdx
	jb	.L1895
	movq	%rax, 0(%r13)
	movq	24(%r15), %rdx
	cmpq	32(%r15), %rdx
	jne	.L2059
	addq	%rdx, %rax
	leaq	-9(%rdx), %rdi
	movq	%r13, %rsi
	movq	%rax, 32(%r15)
	movq	%rdi, 24(%r15)
	call	nghttp2_frame_pack_frame_hd@PLT
	jmp	.L1787
.L1824:
	leaq	352(%r14), %rdi
	movq	%r13, %rsi
	call	nghttp2_frame_pack_altsvc@PLT
	jmp	.L1787
.L1885:
	movl	$-510, %edx
	jmp	.L1750
.L1793:
	cmpb	$1, %cl
	movzbl	12(%r13), %eax
	sbbl	%r15d, %r15d
	andl	$5, %r15d
	subl	$510, %r15d
	jmp	.L1761
.L2053:
	movl	$1, 56(%r13)
	jmp	.L1771
.L1851:
	movq	-72(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	32(%rdx), %rcx
	movq	%rcx, %rax
	subq	24(%rdx), %rax
	movq	%rcx, 24(%rdx)
	jmp	.L1722
.L1870:
	movq	-72(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	32(%rdx), %rcx
	movq	%rcx, %rbx
	movq	%rcx, 24(%rdx)
	subq	%rax, %rbx
	movq	%rbx, %rax
	jmp	.L1722
.L1856:
	cmpl	$-900, %eax
	jl	.L1869
	leaq	__PRETTY_FUNCTION__.6767(%rip), %rcx
	movl	$3102, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	nghttp2_http_record_request_method@PLT
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1739:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$2281, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2009:
	movzbl	12(%r13), %eax
	movl	$-530, %r15d
	jmp	.L1761
.L2042:
	movq	$0, 344(%r14)
	movq	%r12, %rsi
	xorl	%edi, %edi
	jmp	.L2006
.L2039:
	movl	$-514, %edx
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1863:
	cmpl	$-504, %r15d
	jne	.L1862
	xorl	%eax, %eax
	jmp	.L2030
.L2029:
	cmpl	$-900, %eax
	jl	.L1869
	leaq	__PRETTY_FUNCTION__.6767(%rip), %rcx
	movl	$3172, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2028:
	cmpl	$-900, %eax
	jl	.L1869
	leaq	__PRETTY_FUNCTION__.6767(%rip), %rcx
	movl	$3167, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2023:
	cmpl	$-900, %eax
	jl	.L1869
	leaq	__PRETTY_FUNCTION__.6767(%rip), %rcx
	movl	$3095, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2052:
	call	session_is_closing
	testl	%eax, %eax
	jne	.L2009
	testb	$2, 237(%r15)
	jne	.L2018
	cmpb	$0, 2498(%r14)
	je	.L2019
	cmpl	$4, 228(%r15)
	jne	.L2019
	testb	$8, 2499(%r14)
	jne	.L2017
	movq	112(%r13), %rax
	movl	$2, 56(%r13)
	testq	%rax, %rax
	je	.L1771
	movq	%rax, 168(%r15)
	jmp	.L1771
.L2055:
	movq	%r15, %rdi
	movl	$8, %esi
	call	nghttp2_stream_defer_item@PLT
	movl	%eax, %r15d
	cmpl	$-900, %eax
	jl	.L1879
	movq	$0, 344(%r14)
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	nghttp2_outbound_item_free@PLT
	movq	344(%r14), %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	-56(%rbp), %r11
	movq	$0, 344(%r14)
	movq	%r11, %rdi
	call	nghttp2_bufs_reset@PLT
	movl	$0, 416(%r14)
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	%r15, %rdi
	call	nghttp2_stream_detach_item@PLT
	movl	%eax, %r15d
	cmpl	$-900, %eax
	jl	.L1879
	movl	8(%r13), %esi
	movl	$2, %edx
	movq	%r14, %rdi
	call	nghttp2_session_add_rst_stream
	movl	%eax, %r15d
	cmpl	$-900, %eax
	jl	.L1879
	movzbl	12(%r13), %eax
	movl	$-521, %r15d
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L2016:
	movzbl	12(%r13), %eax
	movl	$-522, %r15d
	jmp	.L1761
.L2017:
	movzbl	12(%r13), %eax
	movl	$-516, %r15d
	jmp	.L1761
.L2037:
	movl	$-530, %edx
	jmp	.L1750
.L2018:
	movzbl	12(%r13), %eax
	movl	$-512, %r15d
	jmp	.L1761
.L2044:
	movzbl	12(%r13), %eax
	movl	$-528, %r15d
	jmp	.L1761
.L2019:
	movzbl	12(%r13), %eax
	movl	$-505, %r15d
	jmp	.L1761
.L1886:
	movl	$-512, %edx
	jmp	.L1750
.L1832:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_outbound_item_free@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_mem_free@PLT
	movq	$-902, %rax
	jmp	.L1722
.L1895:
	movzbl	12(%r13), %eax
	movl	$-902, %r15d
	jmp	.L1761
.L2047:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$2253, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC63(%rip), %rdi
	call	__assert_fail@PLT
.L1887:
	movl	$-511, %edx
	jmp	.L1750
.L2050:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$2322, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	call	__assert_fail@PLT
.L2046:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC63(%rip), %rdi
	call	__assert_fail@PLT
.L2036:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$1993, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC60(%rip), %rdi
	call	__assert_fail@PLT
.L2058:
	movzbl	12(%r13), %eax
	movl	$-535, %r15d
	jmp	.L1761
.L2057:
	leaq	__PRETTY_FUNCTION__.6603(%rip), %rcx
	movl	$1942, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC65(%rip), %rdi
	call	__assert_fail@PLT
.L2035:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$2167, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC62(%rip), %rdi
	call	__assert_fail@PLT
.L2059:
	leaq	__PRETTY_FUNCTION__.6603(%rip), %rcx
	movl	$1961, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC66(%rip), %rdi
	call	__assert_fail@PLT
.L2033:
	movzbl	12(%r13), %eax
	movl	$-901, %r15d
	jmp	.L1761
.L2041:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$2022, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC61(%rip), %rdi
	call	__assert_fail@PLT
.L2054:
	leaq	__PRETTY_FUNCTION__.6614(%rip), %rcx
	movl	$2245, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC64(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE134:
	.size	nghttp2_session_mem_send_internal, .-nghttp2_session_mem_send_internal
	.p2align 4
	.globl	nghttp2_session_mem_send
	.type	nghttp2_session_mem_send, @function
nghttp2_session_mem_send:
.LFB135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	$0, (%rsi)
	call	nghttp2_session_mem_send_internal
	movq	%rax, %r12
	testq	%rax, %rax
	jle	.L2060
	cmpq	$0, 344(%r13)
	je	.L2060
	movq	%r13, %rdi
	call	session_after_frame_sent1
	testl	%eax, %eax
	js	.L2064
.L2060:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2064:
	.cfi_restore_state
	cmpl	$-900, %eax
	jge	.L2065
	movslq	%eax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2065:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.6782(%rip), %rcx
	movl	$3226, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE135:
	.size	nghttp2_session_mem_send, .-nghttp2_session_mem_send
	.p2align 4
	.globl	nghttp2_session_send
	.type	nghttp2_session_send, @function
nghttp2_session_send:
.LFB136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	jmp	.L2070
	.p2align 4,,10
	.p2align 3
.L2067:
	xorl	%ecx, %ecx
	movq	2240(%r12), %r8
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	-48(%rbp), %rsi
	call	*2016(%r12)
	testq	%rax, %rax
	js	.L2074
	movq	360(%r12), %rdx
	subq	%rax, %rbx
	subq	%rbx, 24(%rdx)
.L2070:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_mem_send_internal
	movq	%rax, %rbx
	testq	%rax, %rax
	jg	.L2067
.L2066:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2075
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2074:
	.cfi_restore_state
	cmpq	$-504, %rax
	jne	.L2072
	movq	360(%r12), %rax
	subq	%rbx, 24(%rax)
	xorl	%eax, %eax
	jmp	.L2066
.L2072:
	movl	$-902, %eax
	jmp	.L2066
.L2075:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE136:
	.size	nghttp2_session_send, .-nghttp2_session_send
	.p2align 4
	.globl	nghttp2_session_get_stream_user_data
	.type	nghttp2_session_get_stream_user_data, @function
nghttp2_session_get_stream_user_data:
.LFB214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2076
	testb	$2, 236(%rax)
	jne	.L2079
	cmpl	$5, 228(%rax)
	je	.L2079
	movq	168(%rax), %rax
.L2076:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2079:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE214:
	.size	nghttp2_session_get_stream_user_data, .-nghttp2_session_get_stream_user_data
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"frame->hd.type == NGHTTP2_HEADERS"
	.text
	.p2align 4
	.globl	nghttp2_session_set_stream_user_data
	.type	nghttp2_session_set_stream_user_data, @function
nghttp2_session_set_stream_user_data:
.LFB215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2085
	testb	$2, 236(%rax)
	jne	.L2085
	cmpl	$5, 228(%rax)
	je	.L2085
	movq	%r13, 168(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2085:
	.cfi_restore_state
	cmpb	$0, 2498(%r12)
	jne	.L2096
	testl	%ebx, %ebx
	je	.L2096
	testb	$1, %bl
	je	.L2096
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L2096
	cmpb	$1, 12(%rax)
	jne	.L2101
	movl	8(%rax), %edx
	cmpl	%ebx, %edx
	jg	.L2096
	cmpl	%ebx, 2376(%r12)
	jbe	.L2096
.L2089:
	cmpl	%ebx, %edx
	jl	.L2102
	jg	.L2096
	movq	%r13, 112(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2096:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-501, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2102:
	.cfi_restore_state
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L2096
	movl	8(%rax), %edx
	jmp	.L2089
.L2101:
	leaq	__PRETTY_FUNCTION__.7454(%rip), %rcx
	movl	$7261, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC67(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE215:
	.size	nghttp2_session_set_stream_user_data, .-nghttp2_session_set_stream_user_data
	.p2align 4
	.globl	nghttp2_session_resume_data
	.type	nghttp2_session_resume_data, @function
nghttp2_session_resume_data:
.LFB216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2106
	movq	%rax, %r12
	testb	$2, 236(%rax)
	jne	.L2106
	cmpl	$5, 228(%rax)
	je	.L2106
	movq	%rax, %rdi
	call	nghttp2_stream_check_deferred_item@PLT
	testl	%eax, %eax
	je	.L2106
	movq	%r12, %rdi
	movl	$8, %esi
	call	nghttp2_stream_resume_deferred_item@PLT
	movl	$0, %edx
	cmpl	$-900, %eax
	cmovge	%edx, %eax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2106:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-501, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE216:
	.size	nghttp2_session_resume_data, .-nghttp2_session_resume_data
	.p2align 4
	.globl	nghttp2_session_get_outbound_queue_size
	.type	nghttp2_session_get_outbound_queue_size, @function
nghttp2_session_get_outbound_queue_size:
.LFB217:
	.cfi_startproc
	endbr64
	movq	312(%rdi), %rax
	addq	288(%rdi), %rax
	addq	336(%rdi), %rax
	ret
	.cfi_endproc
.LFE217:
	.size	nghttp2_session_get_outbound_queue_size, .-nghttp2_session_get_outbound_queue_size
	.p2align 4
	.globl	nghttp2_session_get_stream_effective_recv_data_length
	.type	nghttp2_session_get_stream_effective_recv_data_length, @function
nghttp2_session_get_stream_effective_recv_data_length:
.LFB218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2117
	testb	$2, 236(%rax)
	jne	.L2117
	cmpl	$5, 228(%rax)
	je	.L2117
	movl	200(%rax), %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%edx, %edx
	movl	$0, %edx
	cmovns	200(%rax), %edx
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2117:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE218:
	.size	nghttp2_session_get_stream_effective_recv_data_length, .-nghttp2_session_get_stream_effective_recv_data_length
	.p2align 4
	.globl	nghttp2_session_get_stream_effective_local_window_size
	.type	nghttp2_session_get_stream_effective_local_window_size, @function
nghttp2_session_get_stream_effective_local_window_size:
.LFB219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2123
	testb	$2, 236(%rax)
	jne	.L2123
	cmpl	$5, 228(%rax)
	je	.L2123
	movl	212(%rax), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2123:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE219:
	.size	nghttp2_session_get_stream_effective_local_window_size, .-nghttp2_session_get_stream_effective_local_window_size
	.p2align 4
	.globl	nghttp2_session_get_stream_local_window_size
	.type	nghttp2_session_get_stream_local_window_size, @function
nghttp2_session_get_stream_local_window_size:
.LFB220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2129
	testb	$2, 236(%rax)
	jne	.L2129
	cmpl	$5, 228(%rax)
	je	.L2129
	movl	212(%rax), %edx
	subl	200(%rax), %edx
	movl	%edx, %eax
	movl	$0, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmovs	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2129:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE220:
	.size	nghttp2_session_get_stream_local_window_size, .-nghttp2_session_get_stream_local_window_size
	.p2align 4
	.globl	nghttp2_session_get_effective_recv_data_length
	.type	nghttp2_session_get_effective_recv_data_length, @function
nghttp2_session_get_effective_recv_data_length:
.LFB221:
	.cfi_startproc
	endbr64
	movl	2408(%rdi), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovns	2408(%rdi), %eax
	ret
	.cfi_endproc
.LFE221:
	.size	nghttp2_session_get_effective_recv_data_length, .-nghttp2_session_get_effective_recv_data_length
	.p2align 4
	.globl	nghttp2_session_get_effective_local_window_size
	.type	nghttp2_session_get_effective_local_window_size, @function
nghttp2_session_get_effective_local_window_size:
.LFB222:
	.cfi_startproc
	endbr64
	movl	2420(%rdi), %eax
	ret
	.cfi_endproc
.LFE222:
	.size	nghttp2_session_get_effective_local_window_size, .-nghttp2_session_get_effective_local_window_size
	.p2align 4
	.globl	nghttp2_session_get_local_window_size
	.type	nghttp2_session_get_local_window_size, @function
nghttp2_session_get_local_window_size:
.LFB223:
	.cfi_startproc
	endbr64
	movl	2420(%rdi), %eax
	subl	2408(%rdi), %eax
	ret
	.cfi_endproc
.LFE223:
	.size	nghttp2_session_get_local_window_size, .-nghttp2_session_get_local_window_size
	.p2align 4
	.globl	nghttp2_session_get_stream_remote_window_size
	.type	nghttp2_session_get_stream_remote_window_size, @function
nghttp2_session_get_stream_remote_window_size:
.LFB224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2138
	testb	$2, 236(%rax)
	jne	.L2138
	cmpl	$5, 228(%rax)
	je	.L2138
	movl	196(%rax), %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%edx, %edx
	movl	$0, %edx
	cmovns	196(%rax), %edx
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2138:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE224:
	.size	nghttp2_session_get_stream_remote_window_size, .-nghttp2_session_get_stream_remote_window_size
	.p2align 4
	.globl	nghttp2_session_get_remote_window_size
	.type	nghttp2_session_get_remote_window_size, @function
nghttp2_session_get_remote_window_size:
.LFB225:
	.cfi_startproc
	endbr64
	movl	2404(%rdi), %eax
	ret
	.cfi_endproc
.LFE225:
	.size	nghttp2_session_get_remote_window_size, .-nghttp2_session_get_remote_window_size
	.p2align 4
	.globl	nghttp2_session_get_remote_settings
	.type	nghttp2_session_get_remote_settings, @function
nghttp2_session_get_remote_settings:
.LFB226:
	.cfi_startproc
	endbr64
	cmpl	$8, %esi
	ja	.L2142
	leaq	.L2144(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2144:
	.long	.L2142-.L2144
	.long	.L2150-.L2144
	.long	.L2149-.L2144
	.long	.L2148-.L2144
	.long	.L2147-.L2144
	.long	.L2146-.L2144
	.long	.L2145-.L2144
	.long	.L2142-.L2144
	.long	.L2143-.L2144
	.text
	.p2align 4,,10
	.p2align 3
.L2143:
	movl	2452(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2150:
	movl	2428(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2149:
	movl	2432(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2148:
	movl	2436(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2147:
	movl	2440(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2146:
	movl	2444(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2145:
	movl	2448(%rdi), %eax
	ret
.L2142:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.7512(%rip), %rcx
	movl	$7402, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE226:
	.size	nghttp2_session_get_remote_settings, .-nghttp2_session_get_remote_settings
	.p2align 4
	.globl	nghttp2_session_get_local_settings
	.type	nghttp2_session_get_local_settings, @function
nghttp2_session_get_local_settings:
.LFB227:
	.cfi_startproc
	endbr64
	cmpl	$8, %esi
	ja	.L2155
	leaq	.L2157(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2157:
	.long	.L2155-.L2157
	.long	.L2163-.L2157
	.long	.L2162-.L2157
	.long	.L2161-.L2157
	.long	.L2160-.L2157
	.long	.L2159-.L2157
	.long	.L2158-.L2157
	.long	.L2155-.L2157
	.long	.L2156-.L2157
	.text
	.p2align 4,,10
	.p2align 3
.L2156:
	movl	2480(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2163:
	movl	2456(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2162:
	movl	2460(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2161:
	movl	2464(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2160:
	movl	2468(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2159:
	movl	2472(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2158:
	movl	2476(%rdi), %eax
	ret
.L2155:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.7524(%rip), %rcx
	movl	$7425, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE227:
	.size	nghttp2_session_get_local_settings, .-nghttp2_session_get_local_settings
	.section	.rodata.str1.1
.LC68:
	.string	"stream"
	.text
	.p2align 4
	.globl	nghttp2_session_upgrade
	.type	nghttp2_session_upgrade, @function
nghttp2_session_upgrade:
.LFB229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	2200(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L2168
	cmpl	$1, 2376(%rdi)
	jne	.L2171
.L2169:
	movabsq	$-6148914691236517205, %rdx
	movq	%r14, %rax
	movabsq	$3074457345618258602, %rcx
	imulq	%rdx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L2172
	movq	%r14, %rax
	mulq	%rdx
	shrq	$2, %rdx
	cmpq	2368(%r12), %rdx
	ja	.L2173
	leaq	-152(%rbp), %rsi
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r9, %rdx
	leaq	-160(%rbp), %rdi
	call	nghttp2_frame_unpack_settings_payload2@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L2167
	cmpb	$0, 2498(%r12)
	jne	.L2187
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	nghttp2_submit_settings@PLT
	movl	%eax, %r13d
.L2176:
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	call	nghttp2_mem_free@PLT
	testl	%r13d, %r13d
	jne	.L2167
	leaq	-140(%rbp), %r14
	movq	%r14, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	movl	$0, %eax
	movq	%r12, %rdi
	movq	%r14, %rcx
	cmpb	$0, 2498(%r12)
	movl	$1, %r8d
	movl	$1, %esi
	cmovne	%rax, %rbx
	xorl	%edx, %edx
	movq	%rbx, %r9
	call	nghttp2_session_open_stream
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2178
	cmpb	$0, 2498(%r12)
	jne	.L2188
	movl	$2, %esi
	call	nghttp2_stream_shutdown@PLT
	addl	$2, 2376(%r12)
	movl	$1, 2380(%r12)
.L2180:
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2181
	testb	$2, 236(%rax)
	jne	.L2181
	cmpl	$5, 228(%rax)
	je	.L2181
	orw	$1024, 234(%rax)
.L2167:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2189
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2187:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	nghttp2_frame_hd_init@PLT
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movq	-160(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-152(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	nghttp2_session_on_settings_received
	movl	%eax, %r13d
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2168:
	movl	2384(%rdi), %eax
	testl	%eax, %eax
	jle	.L2169
.L2171:
	movl	$-505, %r13d
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2188:
	movl	$1, %esi
	call	nghttp2_stream_shutdown@PLT
	movabsq	$4294967297, %rax
	movq	%rax, 2384(%r12)
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2173:
	movl	$-537, %r13d
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2172:
	movl	$-501, %r13d
	jmp	.L2167
.L2178:
	movl	$-901, %r13d
	jmp	.L2167
.L2189:
	call	__stack_chk_fail@PLT
.L2181:
	leaq	__PRETTY_FUNCTION__.7546(%rip), %rcx
	movl	$7513, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC68(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE229:
	.size	nghttp2_session_upgrade, .-nghttp2_session_upgrade
	.p2align 4
	.globl	nghttp2_session_upgrade2
	.type	nghttp2_session_upgrade2, @function
nghttp2_session_upgrade2:
.LFB230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	2200(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movl	%ecx, -164(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L2191
	cmpl	$1, 2376(%rdi)
	jne	.L2194
.L2192:
	movabsq	$-6148914691236517205, %rdx
	movq	%r15, %rax
	movabsq	$3074457345618258602, %rcx
	imulq	%rdx, %rax
	rorq	%rax
	cmpq	%rcx, %rax
	ja	.L2195
	movq	%r15, %rax
	mulq	%rdx
	shrq	$2, %rdx
	cmpq	2368(%r12), %rdx
	ja	.L2196
	leaq	-152(%rbp), %rsi
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r10, %rdx
	leaq	-160(%rbp), %rdi
	call	nghttp2_frame_unpack_settings_payload2@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L2190
	cmpb	$0, 2498(%r12)
	jne	.L2211
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	nghttp2_submit_settings@PLT
	movl	%eax, %r13d
.L2199:
	movq	-160(%rbp), %rsi
	movq	%r14, %rdi
	call	nghttp2_mem_free@PLT
	testl	%r13d, %r13d
	jne	.L2190
	leaq	-140(%rbp), %r15
	movq	%r15, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	movl	$0, %eax
	movq	%r12, %rdi
	movq	%r15, %rcx
	cmpb	$0, 2498(%r12)
	movl	$1, %r8d
	movl	$1, %esi
	cmovne	%rax, %rbx
	xorl	%edx, %edx
	movq	%rbx, %r9
	call	nghttp2_session_open_stream
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2201
	cmpb	$0, 2498(%r12)
	jne	.L2212
	movl	$2, %esi
	call	nghttp2_stream_shutdown@PLT
	addl	$2, 2376(%r12)
	movl	$1, 2380(%r12)
.L2203:
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2204
	testb	$2, 236(%rax)
	jne	.L2204
	cmpl	$5, 228(%rax)
	je	.L2204
	movl	-164(%rbp), %edx
	testl	%edx, %edx
	jne	.L2205
	xorl	%r13d, %r13d
.L2190:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2213
	addq	$136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2211:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	nghttp2_frame_hd_init@PLT
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movq	-160(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-152(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	nghttp2_session_on_settings_received
	movl	%eax, %r13d
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L2191:
	movl	2384(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L2192
.L2194:
	movl	$-505, %r13d
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2212:
	movl	$1, %esi
	call	nghttp2_stream_shutdown@PLT
	movabsq	$4294967297, %rax
	movq	%rax, 2384(%r12)
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2205:
	orw	$256, 234(%rax)
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2196:
	movl	$-537, %r13d
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2195:
	movl	$-501, %r13d
	jmp	.L2190
.L2201:
	movl	$-901, %r13d
	jmp	.L2190
.L2213:
	call	__stack_chk_fail@PLT
.L2204:
	leaq	__PRETTY_FUNCTION__.7556(%rip), %rcx
	movl	$7542, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC68(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE230:
	.size	nghttp2_session_upgrade2, .-nghttp2_session_upgrade2
	.p2align 4
	.globl	nghttp2_session_get_stream_local_close
	.type	nghttp2_session_get_stream_local_close, @function
nghttp2_session_get_stream_local_close:
.LFB231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2218
	testb	$2, 236(%rax)
	jne	.L2218
	cmpl	$5, 228(%rax)
	je	.L2218
	movzbl	237(%rax), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrb	%al
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2218:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE231:
	.size	nghttp2_session_get_stream_local_close, .-nghttp2_session_get_stream_local_close
	.p2align 4
	.globl	nghttp2_session_get_stream_remote_close
	.type	nghttp2_session_get_stream_remote_close, @function
nghttp2_session_get_stream_remote_close:
.LFB232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2224
	testb	$2, 236(%rax)
	jne	.L2224
	cmpl	$5, 228(%rax)
	je	.L2224
	movzbl	237(%rax), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2224:
	.cfi_restore_state
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE232:
	.size	nghttp2_session_get_stream_remote_close, .-nghttp2_session_get_stream_remote_close
	.p2align 4
	.globl	nghttp2_session_consume
	.type	nghttp2_session_consume, @function
nghttp2_session_consume:
.LFB233:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L2241
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	testb	$1, 2484(%rdi)
	je	.L2242
	movslq	2412(%rdi), %rax
	movl	$2147483647, %r14d
	movl	%esi, %r15d
	movq	%rdx, %r13
	subq	%rdx, %r14
	movq	%rax, %rbx
	cmpq	%r14, %rax
	ja	.L2257
	addl	%edx, %ebx
	movl	2420(%rdi), %edi
	cmpb	$0, 2500(%r12)
	movl	%ebx, 2412(%r12)
	je	.L2258
.L2230:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2236
	testb	$2, 236(%rax)
	jne	.L2236
	cmpl	$5, 228(%rax)
	je	.L2236
	movslq	204(%rax), %rax
	cmpq	%rax, %r14
	jb	.L2259
	addl	%eax, %r13d
	cmpb	$0, 239(%rbx)
	movl	212(%rbx), %edi
	movl	%r13d, 204(%rbx)
	movl	192(%rbx), %r15d
	je	.L2260
.L2236:
	xorl	%eax, %eax
.L2226:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2258:
	.cfi_restore_state
	cmpl	%ebx, 2408(%r12)
	cmovle	2408(%r12), %ebx
	movl	%ebx, %esi
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L2230
	leaq	2200(%r12), %rax
	movl	$152, %esi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L2239
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	nghttp2_outbound_item_init@PLT
	movq	-56(%rbp), %r9
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%ebx, %ecx
	movq	%r9, %rdi
	call	nghttp2_frame_window_update_init@PLT
	movq	-56(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	nghttp2_session_add_item
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L2234
	subl	%ebx, 2408(%r12)
	subl	%ebx, 2412(%r12)
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2257:
	testb	$1, 2499(%rdi)
	jne	.L2230
	movl	2388(%rdi), %esi
	xorl	%ecx, %ecx
	movl	$3, %edx
	call	session_terminate_session.part.0
.L2231:
	cmpl	$-900, %eax
	jge	.L2230
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2260:
	.cfi_restore_state
	cmpl	%r13d, 200(%rbx)
	cmovle	200(%rbx), %r13d
	movl	%r13d, %esi
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L2236
	leaq	2200(%r12), %rax
	movl	$152, %esi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2239
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r13d, %ecx
	movl	%r15d, %edx
	call	nghttp2_frame_window_update_init@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L2240
	subl	%r13d, 200(%rbx)
	subl	%r13d, 204(%rbx)
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2259:
	testb	$1, 2499(%r12)
	jne	.L2236
	movl	2388(%r12), %esi
	xorl	%ecx, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	call	session_terminate_session.part.0
.L2238:
	cmpl	$-900, %eax
	jl	.L2226
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2242:
	movl	$-519, %eax
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2234:
	movq	%r9, %rdi
	movl	%eax, -68(%rbp)
	movq	%r9, -56(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdi
	movq	%r9, %rsi
	call	nghttp2_mem_free@PLT
	movl	-68(%rbp), %eax
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2241:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-501, %eax
	ret
.L2240:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rdi
	movl	%eax, -64(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	-56(%rbp), %rdi
	movq	%r14, %rsi
	call	nghttp2_mem_free@PLT
	movl	-64(%rbp), %eax
	jmp	.L2238
.L2239:
	movl	$-901, %eax
	jmp	.L2226
	.cfi_endproc
.LFE233:
	.size	nghttp2_session_consume, .-nghttp2_session_consume
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"DATA: stream in half-closed(remote)"
	.section	.rodata.str1.1
.LC70:
	.string	"DATA: stream not opened"
.LC71:
	.string	"DATA: stream closed"
.LC72:
	.string	"DATA: stream_id == 0"
.LC73:
	.string	"DATA: stream in idle"
.LC74:
	.string	"DATA: stream in reserved"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"Remote peer returned unexpected data while we expected SETTINGS frame.  Perhaps, peer does not support HTTP/2 properly."
	.section	.rodata.str1.1
.LC76:
	.string	"SETTINGS expected"
.LC77:
	.string	"too large frame size"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"DATA: insufficient padding space"
	.align 8
.LC79:
	.string	"HEADERS: insufficient padding space"
	.align 8
.LC80:
	.string	"SETTINGS: too many setting entries"
	.align 8
.LC81:
	.string	"PUSH_PROMISE: insufficient padding space"
	.section	.rodata.str1.1
.LC82:
	.string	"CONTINUATION: unexpected"
.LC83:
	.string	"HEADERS: invalid padding"
.LC84:
	.string	"PUSH_PROMISE: invalid padding"
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"PUSH_PROMISE: could not unpack"
	.align 8
.LC86:
	.string	"Invalid HTTP header field was received: frame type: %u, stream: %d, name: [%.*s], value: [%.*s]"
	.align 8
.LC87:
	.string	"Ignoring received invalid HTTP header field: frame type: %u, stream: %d, name: [%.*s], value: [%.*s]"
	.section	.rodata.str1.1
.LC88:
	.string	"!session->server"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"iframe->state == NGHTTP2_IB_IGN_ALL"
	.align 8
.LC90:
	.string	"nghttp2_buf_avail(&iframe->lbuf) > 0"
	.align 8
.LC91:
	.string	"unexpected non-CONTINUATION frame or stream_id is invalid"
	.section	.rodata.str1.1
.LC92:
	.string	"DATA: invalid padding"
.LC93:
	.string	"i < iframe->niv"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB94:
	.text
.LHOTB94:
	.p2align 4
	.globl	nghttp2_session_mem_recv
	.type	nghttp2_session_mem_recv, @function
nghttp2_session_mem_recv:
.LFB203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	nghttp2_session_adjust_idle_stream
	cmpl	$-900, %eax
	jl	.L2562
	testb	$2, 2499(%r15)
	je	.L2871
.L2276:
	movq	-128(%rbp), %rax
.L2261:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2872
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2871:
	.cfi_restore_state
	movq	%r15, %rdi
	call	nghttp2_map_size@PLT
	subq	2320(%r15), %rax
	cmpq	2328(%r15), %rax
	je	.L2873
.L2266:
	movq	-136(%rbp), %r14
	movq	-128(%rbp), %rax
	movq	%r15, %rbx
	addq	%r14, %rax
	movq	%rax, -120(%rbp)
	leaq	424(%r15), %rax
	movq	%rax, -144(%rbp)
	leaq	2200(%r15), %rax
	movq	%rax, -192(%rbp)
	movl	648(%r15), %eax
	movq	%r14, %r15
	movq	%rbx, %r14
.L2270:
	cmpl	$18, %eax
	ja	.L2579
	leaq	.L2273(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2273:
	.long	.L2288-.L2273
	.long	.L2287-.L2273
	.long	.L2286-.L2273
	.long	.L2285-.L2273
	.long	.L2820-.L2273
	.long	.L2820-.L2273
	.long	.L2284-.L2273
	.long	.L2810-.L2273
	.long	.L2282-.L2273
	.long	.L2281-.L2273
	.long	.L2280-.L2273
	.long	.L2280-.L2273
	.long	.L2279-.L2273
	.long	.L2580-.L2273
	.long	.L2277-.L2273
	.long	.L2276-.L2273
	.long	.L2275-.L2273
	.long	.L2274-.L2273
	.long	.L2272-.L2273
	.text
.L2561:
	movq	2032(%r14), %rax
	testq	%rax, %rax
	je	.L2563
	movq	2240(%r14), %rdx
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L2563
.L2861:
	movl	$-902, %eax
	.p2align 4,,10
	.p2align 3
.L2562:
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2873:
	testb	$12, 2499(%r15)
	jne	.L2276
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	552(%r14), %rdi
	movq	560(%r14), %rbx
	movq	%r15, %rsi
	movq	-120(%rbp), %rax
	subq	%rdi, %rbx
	subq	%r15, %rax
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	movq	%rbx, %rdx
	addq	%rbx, %r15
	call	nghttp2_cpymem@PLT
	movq	%rax, 552(%r14)
	cmpq	%rax, 560(%r14)
	jne	.L2564
	movq	544(%r14), %rsi
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	nghttp2_frame_unpack_frame_hd@PLT
	movq	-96(%rbp), %rdx
	cmpb	$9, -84(%rbp)
	movq	%rdx, 632(%r14)
	jne	.L2512
	movl	432(%r14), %eax
	cmpl	%eax, -88(%rbp)
	jne	.L2512
	movzbl	-83(%rbp), %eax
	addq	%rdx, 424(%r14)
	andl	$4, %eax
	orb	%al, 437(%r14)
	cmpl	$10, 648(%r14)
	je	.L2874
.L2514:
	movl	$5, 648(%r14)
	movq	%r15, -184(%rbp)
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2274:
	movq	-120(%rbp), %rax
	movq	632(%r14), %rdx
	movq	592(%r14), %rdi
	subq	%r15, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L2875
.L2559:
	testq	%rdx, %rdx
	je	.L2560
	cmpq	%rdi, 576(%r14)
	jne	.L2606
	leaq	__PRETTY_FUNCTION__.7329(%rip), %rcx
	movl	$6745, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2282:
	movq	552(%r14), %rdi
	movq	-120(%rbp), %rax
	movq	%r15, %rsi
	movl	$1, %ebx
	movq	560(%r14), %r12
	subq	%r15, %rax
	subq	%rdi, %r12
	cmpq	%rax, %r12
	cmova	%rax, %r12
	movq	%r12, %rdx
	addq	%r12, %r15
	call	nghttp2_cpymem@PLT
	movq	632(%r14), %rdx
	movq	%rax, 552(%r14)
	subq	%r12, %rdx
	movq	%rdx, 632(%r14)
	cmpq	%rax, 560(%r14)
	je	.L2876
	.p2align 4,,10
	.p2align 3
.L2271:
	cmpq	-120(%rbp), %r15
	jne	.L2613
	testb	%bl, %bl
	jne	.L2564
.L2613:
	movl	648(%r14), %eax
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2285:
	movq	552(%r14), %rdi
	movq	560(%r14), %rbx
	movq	%r15, %rsi
	movq	-120(%rbp), %rax
	subq	%rdi, %rbx
	subq	%r15, %rax
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	movq	%rbx, %rdx
	addq	%rbx, %r15
	call	nghttp2_cpymem@PLT
	movq	632(%r14), %rdx
	movq	%rax, 552(%r14)
	subq	%rbx, %rdx
	movq	%rdx, 632(%r14)
	cmpq	%rax, 560(%r14)
	jne	.L2564
	cmpb	$10, 436(%r14)
	ja	.L2846
	movzbl	436(%r14), %eax
	leaq	.L2364(%rip), %rbx
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2364:
	.long	.L2846-.L2364
	.long	.L2371-.L2364
	.long	.L2370-.L2364
	.long	.L2369-.L2364
	.long	.L2846-.L2364
	.long	.L2368-.L2364
	.long	.L2367-.L2364
	.long	.L2366-.L2364
	.long	.L2365-.L2364
	.long	.L2846-.L2364
	.long	.L2363-.L2364
	.text
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	552(%r14), %rdi
	movq	560(%r14), %r13
	movq	%r15, %rsi
	movq	-120(%rbp), %rdx
	subq	%rdi, %r13
	subq	%r15, %rdx
	cmpq	%rdx, %r13
	cmova	%rdx, %r13
	movq	%r13, %rdx
	leaq	(%r15,%r13), %r12
	call	nghttp2_cpymem@PLT
	subq	%r13, 632(%r14)
	movq	%rax, 552(%r14)
	cmpq	%rax, 560(%r14)
	jne	.L2877
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_update_recv_connection_window_size
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
	movl	432(%r14), %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	nghttp2_session_consume
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
	movl	432(%r14), %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rsi
	movq	632(%r14), %rax
	testq	%rsi, %rsi
	je	.L2520
	testb	$2, 236(%rsi)
	jne	.L2520
	cmpl	$5, 228(%rsi)
	je	.L2520
	movl	$1, %ecx
	testq	%rax, %rax
	jne	.L2521
	movzbl	437(%r14), %eax
	andl	$1, %eax
	subl	%eax, %ecx
.L2521:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	nghttp2_session_update_recv_stream_window_size
	cmpl	$-900, %eax
	jl	.L2562
	movq	632(%r14), %rax
.L2520:
	movq	544(%r14), %rdx
	movzbl	(%rdx), %ecx
	leaq	1(%rcx), %rdx
	cmpq	%rax, %rcx
	ja	.L2878
	movq	%rdx, 640(%r14)
	movq	%rdx, 440(%r14)
	movl	$13, 648(%r14)
.L2278:
	movl	432(%r14), %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2525
	testb	$2, 236(%rax)
	jne	.L2525
	cmpl	$5, 228(%rax)
	je	.L2525
	movq	-120(%rbp), %r13
	movq	632(%r14), %rax
	subq	%r12, %r13
	cmpq	%rax, %r13
	cmova	%rax, %r13
	subq	%r13, %rax
	leaq	(%r12,%r13), %r15
	movq	%rax, 632(%r14)
	testq	%r13, %r13
	jne	.L2879
.L2527:
	movl	$1, %ebx
	testq	%rax, %rax
	jne	.L2271
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_data_received
	cmpl	$-900, %eax
	jl	.L2562
.L2847:
	movq	%r14, %rdi
	call	session_inbound_frame_reset
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2287:
	movq	552(%r14), %rdi
	movq	560(%r14), %rbx
	movq	%r15, %rsi
	movq	-120(%rbp), %rax
	subq	%rdi, %rbx
	subq	%r15, %rax
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	movq	%rbx, %rdx
	addq	%rbx, %r15
	call	nghttp2_cpymem@PLT
	movq	%rax, 552(%r14)
	movq	%rax, %rdi
	cmpq	%rax, 560(%r14)
	jne	.L2564
	movq	544(%r14), %rax
	cmpb	$4, 3(%rax)
	jne	.L2291
	testb	$1, 4(%rax)
	jne	.L2291
	movl	$2, 648(%r14)
	movq	%rdi, %rbx
.L2289:
	movq	-120(%rbp), %rax
	subq	%rdi, %rbx
	movq	%r15, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	movq	%rbx, %rdx
	addq	%rbx, %r15
	call	nghttp2_cpymem@PLT
	movq	%rax, 552(%r14)
	cmpq	%rax, 560(%r14)
	jne	.L2564
	movq	544(%r14), %rsi
	movq	-144(%rbp), %rdi
	call	nghttp2_frame_unpack_frame_hd@PLT
	movq	424(%r14), %rsi
	movl	2472(%r14), %edx
	movq	%rsi, 632(%r14)
	cmpq	%rdx, %rsi
	ja	.L2880
	movzbl	436(%r14), %edx
	cmpb	$9, %dl
	ja	.L2296
	leaq	.L2298(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L2298:
	.long	.L2306-.L2298
	.long	.L2305-.L2298
	.long	.L2304-.L2298
	.long	.L2299-.L2298
	.long	.L2303-.L2298
	.long	.L2302-.L2298
	.long	.L2301-.L2298
	.long	.L2300-.L2298
	.long	.L2299-.L2298
	.long	.L2297-.L2298
	.text
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	552(%r14), %rdi
	movq	560(%r14), %rbx
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	-120(%rbp), %rcx
	movq	632(%r14), %rax
	subq	%r15, %rcx
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	subq	%rcx, %rax
	leaq	(%r15,%rcx), %r12
	movq	%rax, 632(%r14)
	testq	%rcx, %rcx
	jne	.L2881
.L2549:
	movq	%r12, %r15
	movl	$1, %ebx
	testq	%rax, %rax
	jne	.L2271
	leaq	-80(%rbp), %r8
	movq	$0, -80(%rbp)
	movq	%r14, %rdi
	movq	2240(%r14), %rcx
	movq	-144(%rbp), %rdx
	movq	%r8, %rsi
	call	*2168(%r14)
	cmpl	$-535, %eax
	je	.L2552
	testl	%eax, %eax
	jne	.L2553
	movq	-80(%rbp), %rax
	movq	%rax, 440(%r14)
	movq	2032(%r14), %rax
	testq	%rax, %rax
	je	.L2552
	movq	2240(%r14), %rdx
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L2553
.L2552:
	movq	%r14, %rdi
	movq	%r12, %r15
	movl	$1, %ebx
	call	session_inbound_frame_reset
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	632(%r14), %rbx
	movq	-128(%rbp), %rax
	leaq	24+.LC7(%rip), %rdi
	movq	%r15, %rsi
	cmpq	%rax, %rbx
	movq	%rax, %r12
	cmovbe	%rbx, %r12
	subq	%rbx, %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2581
	movq	%rbx, %rax
	addq	%r12, %r15
	movl	$1, %ebx
	subq	%r12, %rax
	movq	%rax, 632(%r14)
	testq	%rax, %rax
	jne	.L2271
	movq	%r14, %rdi
	call	session_inbound_frame_reset
	movl	$1, 648(%r14)
	jmp	.L2271
.L2363:
	movq	544(%r14), %rdi
	call	nghttp2_get_uint16@PLT
	movzwl	%ax, %eax
	cmpq	%rax, 632(%r14)
	jb	.L2842
	movq	424(%r14), %rax
	cmpq	$2, %rax
	ja	.L2882
.L2402:
	movl	$16, 648(%r14)
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	-120(%rbp), %rax
	movq	632(%r14), %rdx
	movq	592(%r14), %r12
	subq	%r15, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L2883
.L2556:
	testq	%rdx, %rdx
	je	.L2557
	cmpq	%r12, 576(%r14)
	je	.L2884
.L2606:
	movl	$1, %ebx
	jmp	.L2271
.L2366:
	movq	424(%r14), %r12
	subq	$8, %r12
	jne	.L2885
.L2399:
	movl	$9, 648(%r14)
	.p2align 4,,10
	.p2align 3
.L2281:
	movq	-120(%rbp), %rax
	movq	632(%r14), %rdx
	movq	592(%r14), %rdi
	subq	%r15, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L2886
.L2508:
	testq	%rdx, %rdx
	je	.L2509
	cmpq	%rdi, 576(%r14)
	jne	.L2606
	leaq	__PRETTY_FUNCTION__.7329(%rip), %rcx
	movl	$6355, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2820:
	movq	%r15, -184(%rbp)
.L2269:
	movq	632(%r14), %r15
	movq	-120(%rbp), %rax
	subq	-184(%rbp), %rax
	movq	640(%r14), %rsi
	cmpq	%r15, %rax
	movq	-144(%rbp), %rdi
	movq	%r15, %rbx
	cmova	%r15, %rax
	subq	%rax, %rbx
	movq	%rax, -200(%rbp)
	movq	%rax, %r13
	call	nghttp2_frame_trail_padlen@PLT
	cmpq	%rax, %rbx
	jnb	.L2403
	movq	%r13, %rdx
	movq	640(%r14), %rsi
	subq	%r15, %rdx
	addq	%rax, %rdx
	cmpq	%r13, %rdx
	ja	.L2404
	subq	%rax, %r15
	movq	%r15, %rbx
.L2405:
	cmpq	$-1, %rbx
	je	.L2404
	movq	-144(%rbp), %rdi
	call	nghttp2_frame_trail_padlen@PLT
	testb	$4, 437(%r14)
	je	.L2406
	movq	632(%r14), %rdx
	movq	%rdx, %rcx
	subq	%r15, %rcx
	cmpq	%rax, %rcx
	je	.L2887
.L2406:
	testq	%rbx, %rbx
	jle	.L2576
	movl	$0, -156(%rbp)
.L2572:
	movl	648(%r14), %eax
	movl	432(%r14), %esi
	movq	%r14, %rdi
	movl	%eax, -160(%rbp)
	call	nghttp2_map_find@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L2888
	movq	%rax, %rbx
	movzbl	436(%r14), %eax
	testb	$2, 236(%rbx)
	jne	.L2409
	cmpl	$5, 228(%rbx)
	je	.L2409
	cmpb	$5, %al
	je	.L2411
	movl	$0, -204(%rbp)
	cmpb	$1, %al
	jne	.L2412
	cmpb	$0, 2498(%r14)
	movl	480(%r14), %eax
	jne	.L2889
	cmpl	$3, %eax
	jne	.L2412
	movzwl	234(%rbx), %eax
	shrw	$14, %ax
	xorl	$1, %eax
	andl	$1, %eax
	movl	%eax, -204(%rbp)
	.p2align 4,,10
	.p2align 3
.L2412:
	leaq	-100(%rbp), %rax
	leaq	-80(%rbp), %r12
	movq	$0, -168(%rbp)
	movq	-184(%rbp), %rbx
	leaq	1776(%r14), %r13
	movq	%rax, -152(%rbp)
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	-152(%rbp), %rdx
	movq	%r15, %r8
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movl	-156(%rbp), %r9d
	movq	%r12, %rdi
	movl	$0, -100(%rbp)
	call	nghttp2_hd_inflate_hd_nv@PLT
	movl	%eax, %edx
	cmpl	$-900, %eax
	jl	.L2816
	testq	%rax, %rax
	js	.L2890
	addq	%rax, -168(%rbp)
	addq	%rax, %rbx
	subq	%rax, %r15
	movl	-100(%rbp), %eax
	cmpl	$4, -160(%rbp)
	jne	.L2420
	testb	$2, %al
	je	.L2420
	movq	-176(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2420
	testb	$4, 2484(%r14)
	jne	.L2421
	movl	-204(%rbp), %r8d
	movq	-144(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	nghttp2_http_on_header@PLT
	cmpl	$-105, %eax
	je	.L2891
	cmpl	$-531, %eax
	je	.L2838
	testl	%eax, %eax
	jne	.L2444
.L2421:
	movq	2104(%r14), %rax
	testq	%rax, %rax
	je	.L2439
	movq	-80(%rbp), %rdx
	movq	2240(%r14), %r9
	movq	%r14, %rdi
	movzbl	-60(%rbp), %r8d
	movq	-72(%rbp), %rcx
	movq	-144(%rbp), %rsi
	call	*%rax
	movl	%eax, %edx
.L2440:
	cmpl	$-526, %edx
	je	.L2442
	cmpl	$-521, %edx
	je	.L2442
	testl	%edx, %edx
	jne	.L2861
.L2444:
	movl	-100(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L2420:
	testb	$1, %al
	jne	.L2892
	testb	$2, %al
	jne	.L2449
	testq	%r15, %r15
	jne	.L2449
	cmpl	$15, 648(%r14)
	movq	-184(%rbp), %r15
	je	.L2276
.L2806:
	movq	-200(%rbp), %rax
	movq	632(%r14), %rdx
	subq	%rax, %rdx
	addq	%rax, %r15
	movq	%rdx, 632(%r14)
.L2455:
	testq	%rdx, %rdx
	jne	.L2606
	testb	$4, 437(%r14)
	je	.L2893
	cmpl	$4, 648(%r14)
	jne	.L2846
	movl	432(%r14), %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2846
	testb	$2, 236(%rax)
	jne	.L2846
	movl	228(%rax), %eax
	subl	$3, %eax
	andl	$-3, %eax
	je	.L2846
	testb	$4, 2484(%r14)
	jne	.L2461
	movzbl	436(%r14), %eax
	cmpb	$5, %al
	je	.L2894
	cmpb	$1, %al
	jne	.L2895
	movl	480(%r14), %eax
	cmpl	$2, %eax
	ja	.L2465
	testl	%eax, %eax
	jne	.L2472
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	nghttp2_http_on_request_headers@PLT
.L2470:
	testl	%eax, %eax
	jne	.L2476
	testb	$1, 437(%r14)
	jne	.L2896
.L2461:
	movq	2032(%r14), %rax
	testq	%rax, %rax
	je	.L2487
	movq	2240(%r14), %rdx
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L2861
.L2487:
	cmpb	$1, 436(%r14)
	jne	.L2846
	testb	$1, 437(%r14)
	je	.L2846
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_stream_shutdown@PLT
	movzbl	237(%r12), %eax
	andl	$3, %eax
	cmpb	$3, %al
	jne	.L2846
	movl	192(%r12), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	nghttp2_session_close_stream
	cmpl	$-900, %eax
	jl	.L2562
.L2846:
	movq	%r14, %rdi
	movl	$1, %ebx
	call	session_inbound_frame_reset
	jmp	.L2271
.L2311:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2830
	movl	$5, %edx
	leaq	.LC71(%rip), %rcx
	testb	$1, 237(%rax)
	jne	.L2307
.L2830:
	cmpl	$15, 648(%r14)
	je	.L2276
.L2541:
	movl	$14, 648(%r14)
	.p2align 4,,10
	.p2align 3
.L2277:
	movq	-120(%rbp), %rbx
	movq	632(%r14), %rax
	subq	%r15, %rbx
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	subq	%rbx, %rax
	addq	%rbx, %r15
	movq	%rax, 632(%r14)
	testq	%rbx, %rbx
	jne	.L2897
.L2545:
	movl	$1, %ebx
	testq	%rax, %rax
	jne	.L2271
	jmp	.L2847
	.p2align 4,,10
	.p2align 3
.L2890:
	cmpl	$4, 648(%r14)
	movq	-184(%rbp), %r15
	jne	.L2418
	cmpq	$0, -176(%rbp)
	jne	.L2898
.L2418:
	testb	$1, 2499(%r14)
	jne	.L2570
	movl	2388(%r14), %esi
	xorl	%ecx, %ecx
	movl	$9, %edx
	movq	%r14, %rdi
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jl	.L2562
.L2570:
	cmpl	$15, 648(%r14)
	je	.L2276
	movq	-200(%rbp), %rax
	movq	632(%r14), %rdx
	subq	%rax, %rdx
	addq	%rax, %r15
	movq	%rdx, 632(%r14)
.L2456:
	testq	%rdx, %rdx
	je	.L2846
.L2457:
	movl	$6, 648(%r14)
.L2284:
	movq	-120(%rbp), %rdx
	movq	632(%r14), %rax
	movl	$1, %ebx
	subq	%r15, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	subq	%rdx, %rax
	addq	%rdx, %r15
	movq	%rax, 632(%r14)
	testq	%rax, %rax
	jne	.L2271
	movzbl	436(%r14), %ecx
	cmpb	$9, %cl
	ja	.L2846
	movl	$1, %eax
	salq	%cl, %rax
	testl	$546, %eax
	je	.L2846
	movb	$1, 1836(%r14)
	jmp	.L2846
	.p2align 4,,10
	.p2align 3
.L2403:
	movq	-200(%rbp), %r15
	movq	640(%r14), %rsi
	movq	%r15, %rbx
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2881:
	movq	2176(%r14), %r9
	testq	%r9, %r9
	je	.L2549
	movq	2240(%r14), %r8
	movq	-144(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	*%r9
	cmpl	$-535, %eax
	je	.L2899
	testl	%eax, %eax
	jne	.L2553
	movq	632(%r14), %rax
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2810:
	movq	%r14, %r15
.L2283:
	testb	$1, 2499(%r15)
	jne	.L2491
	movl	2388(%r15), %esi
	xorl	%ecx, %ecx
	movl	$6, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r15)
	je	.L2276
	.p2align 4,,10
	.p2align 3
.L2491:
	leaq	__PRETTY_FUNCTION__.7329(%rip), %rcx
	movl	$6301, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC89(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2299:
	movb	$0, 437(%r14)
	cmpq	$4, %rsi
	je	.L2831
	.p2align 4,,10
	.p2align 3
.L2842:
	movl	$7, 648(%r14)
	movq	%r14, %r15
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2886:
	movq	%rax, %rdx
	movq	%r15, %rsi
	addq	%rbx, %r15
	call	nghttp2_cpymem@PLT
	movq	632(%r14), %rdx
	movq	%rax, 592(%r14)
	movq	%rax, %rdi
	subq	%rbx, %rdx
	movq	%rdx, 632(%r14)
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2560:
	movq	584(%r14), %rsi
	movq	-192(%rbp), %rcx
	subq	%rsi, %rdi
	movq	%rdi, %rdx
	movq	-144(%rbp), %rdi
	call	nghttp2_frame_unpack_origin_payload@PLT
	testl	%eax, %eax
	je	.L2561
	cmpl	$-900, %eax
	jl	.L2562
	.p2align 4,,10
	.p2align 3
.L2563:
	cmpl	$15, 648(%r14)
	jne	.L2846
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2897:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_update_recv_connection_window_size
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
	testb	$1, 2484(%r14)
	jne	.L2547
.L2844:
	movq	632(%r14), %rax
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2883:
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	%rbx, %r15
	call	nghttp2_cpymem@PLT
	movq	632(%r14), %rdx
	movq	%rax, 592(%r14)
	movq	%rax, %r12
	subq	%rbx, %rdx
	movq	%rdx, 632(%r14)
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2557:
	movq	544(%r14), %rdi
	movq	584(%r14), %r13
	call	nghttp2_get_uint16@PLT
	movq	-144(%rbp), %rbx
	movq	%r12, %rcx
	movq	%r13, %rdx
	subq	%r13, %rcx
	movzwl	%ax, %esi
	movq	%rbx, %rdi
	call	nghttp2_frame_unpack_altsvc_payload@PLT
	xorl	%esi, %esi
	leaq	568(%r14), %rdi
	xorl	%edx, %edx
	call	nghttp2_buf_wrap_init@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_altsvc_received
	cmpl	$-900, %eax
	jge	.L2846
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2509:
	movq	584(%r14), %rdx
	movq	-144(%rbp), %rbx
	movq	%rdi, %rcx
	movq	544(%r14), %rsi
	subq	%rdx, %rcx
	movq	%rbx, %rdi
	call	nghttp2_frame_unpack_goaway_payload@PLT
	xorl	%esi, %esi
	leaq	568(%r14), %rdi
	xorl	%edx, %edx
	call	nghttp2_buf_wrap_init@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_goaway_received
	cmpl	$-900, %eax
	jge	.L2563
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2875:
	movq	%rax, %rdx
	movq	%r15, %rsi
	addq	%rbx, %r15
	call	nghttp2_cpymem@PLT
	movq	632(%r14), %rdx
	movq	%rax, 592(%r14)
	movq	%rax, %rdi
	subq	%rbx, %rdx
	movq	%rdx, 632(%r14)
	jmp	.L2559
	.p2align 4,,10
	.p2align 3
.L2874:
	movq	2144(%r14), %rax
	movl	$4, 648(%r14)
	testq	%rax, %rax
	je	.L2820
	movq	2240(%r14), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L2613
.L2553:
	movq	$-902, %rax
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2876:
	testq	%r12, %r12
	jne	.L2900
.L2492:
	testq	%rdx, %rdx
	je	.L2502
	leaq	528(%r14), %rdi
	movl	$1, %ebx
	call	nghttp2_buf_reset@PLT
	addq	$6, 560(%r14)
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2301:
	andb	$1, 437(%r14)
	cmpq	$8, %rsi
	jne	.L2842
.L2832:
	movl	$3, 648(%r14)
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	$8, 560(%r14)
.L2332:
	movl	648(%r14), %eax
	cmpl	$7, %eax
	ja	.L2357
	cmpl	$4, %eax
	ja	.L2606
	xorl	%ebx, %ebx
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2302:
	movzbl	437(%r14), %eax
	movl	%eax, %edx
	andl	$12, %edx
	movb	%dl, 437(%r14)
	testb	$8, %al
	je	.L2347
	testq	%rsi, %rsi
	jne	.L2833
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC81(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L2850:
	movl	$1, %edx
	movq	%r15, %rdi
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jge	.L2276
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2300:
	movb	$0, 437(%r14)
	cmpq	$7, %rsi
	jbe	.L2842
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L2303:
	movabsq	$-6148914691236517205, %rcx
	movq	%rsi, %rdx
	movzbl	437(%r14), %eax
	movabsq	$3074457345618258602, %rdi
	imulq	%rcx, %rdx
	andl	$1, %eax
	movb	%al, 437(%r14)
	rorq	%rdx
	cmpq	%rdi, %rdx
	ja	.L2842
	testb	%al, %al
	je	.L2341
	testq	%rsi, %rsi
	jne	.L2842
	movl	$8, 648(%r14)
.L2343:
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
.L2346:
	movl	648(%r14), %eax
	cmpl	$7, %eax
	ja	.L2566
	cmpl	$4, %eax
	ja	.L2270
.L2607:
	movl	$1, %ebx
.L2358:
	movq	2144(%r14), %rax
	testq	%rax, %rax
	je	.L2845
	movq	2240(%r14), %rdx
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L2553
.L2845:
	xorl	$1, %ebx
	andl	$1, %ebx
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2306:
	movl	432(%r14), %r12d
	andb	$9, 437(%r14)
	testl	%r12d, %r12d
	je	.L2583
	movl	%r12d, %ebx
	andl	$1, %ebx
	cmpb	$0, 2498(%r14)
	jne	.L2901
	testl	%ebx, %ebx
	je	.L2309
.L2575:
	cmpl	2380(%r14), %r12d
	jle	.L2310
.L2585:
	movl	$1, %edx
	leaq	.LC73(%rip), %rcx
.L2307:
	testb	$1, 2499(%r14)
	jne	.L2830
	movl	2388(%r14), %esi
	movq	%r14, %rdi
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jge	.L2830
	cmpl	$15, 648(%r14)
	je	.L2276
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC82(%rip), %rcx
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2304:
	movb	$0, 437(%r14)
	cmpq	$5, %rsi
	jne	.L2842
	movl	$3, 648(%r14)
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	$5, 560(%r14)
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2305:
	movzbl	437(%r14), %edx
	movl	%edx, %eax
	andl	$45, %eax
	andl	$8, %edx
	movb	%al, 437(%r14)
	je	.L2327
	testq	%rsi, %rsi
	jne	.L2833
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC79(%rip), %rcx
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2525:
	movl	$14, 648(%r14)
	movq	%r12, %r15
	jmp	.L2277
.L2369:
	movq	-144(%rbp), %rbx
	movq	544(%r14), %rsi
	movq	%rbx, %rdi
	call	nghttp2_frame_unpack_rst_stream_payload@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_rst_stream_received
	cmpl	$-900, %eax
	jge	.L2563
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2370:
	movq	-144(%rbp), %rbx
	movq	544(%r14), %rsi
	movq	%rbx, %rdi
	call	nghttp2_frame_unpack_priority_payload@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_priority_received
	cmpl	$-900, %eax
	jge	.L2563
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2371:
	cmpq	$0, 640(%r14)
	jne	.L2372
	movzbl	437(%r14), %eax
	testb	$8, %al
	jne	.L2902
.L2372:
	movq	%r14, %rdi
	call	session_process_headers_frame
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
	cmpl	$-521, %eax
	je	.L2903
	cmpl	$-103, %eax
	movq	%r15, -184(%rbp)
	sete	%al
	movzbl	%al, %eax
	addl	$4, %eax
	movl	%eax, 648(%r14)
	jmp	.L2269
.L2365:
	movq	-144(%rbp), %rbx
	movq	544(%r14), %rsi
	movq	%rbx, %rdi
	call	nghttp2_frame_unpack_window_update_payload@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_window_update_received
	cmpl	$-900, %eax
	jge	.L2563
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	-144(%rbp), %rbx
	movq	544(%r14), %rsi
	movq	%rbx, %rdi
	call	nghttp2_frame_unpack_ping_payload@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_ping_received
	cmpl	$-900, %eax
	jge	.L2563
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2368:
	cmpq	$0, 640(%r14)
	jne	.L2385
	testb	$8, 437(%r14)
	je	.L2385
	movq	544(%r14), %rax
	movzbl	(%rax), %ecx
	leaq	1(%rcx), %rax
	cmpq	%rcx, %rdx
	jb	.L2836
	leaq	4(%rax), %rsi
	leaq	1(%rdx), %rcx
	movq	%rax, 640(%r14)
	cmpq	%rcx, %rsi
	ja	.L2836
	movq	%rax, 440(%r14)
	cmpq	$3, %rdx
	jbe	.L2842
	movl	$3, 648(%r14)
	leaq	528(%r14), %rdi
	movl	$1, %ebx
	call	nghttp2_buf_reset@PLT
	addq	$4, 560(%r14)
	jmp	.L2271
.L2580:
	movq	%r15, %r12
	jmp	.L2278
	.p2align 4,,10
	.p2align 3
.L2566:
	leal	-14(%rax), %edx
	cmpl	$1, %edx
	ja	.L2607
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2564:
	movq	%r15, %rax
	subq	-136(%rbp), %rax
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2442:
	testl	%edx, %edx
	je	.L2444
.L2815:
	movl	%edx, %eax
	cmpl	$-900, %edx
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
	cmpl	$-526, %edx
	je	.L2904
	cmpl	$-521, %edx
	jne	.L2905
.L2434:
	movq	-168(%rbp), %rbx
	addq	%rbx, -184(%rbp)
	subq	%rbx, 632(%r14)
	cmpb	$5, 436(%r14)
	je	.L2906
	movl	432(%r14), %esi
.L2453:
	movl	$2, %edx
	movq	%r14, %rdi
	call	nghttp2_session_add_rst_stream
	cmpl	$-900, %eax
	jl	.L2562
	movl	$5, 648(%r14)
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	2096(%r14), %rax
	testq	%rax, %rax
	je	.L2444
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movzbl	-60(%rbp), %esi
	movq	24(%rcx), %r9
	movq	16(%rcx), %r8
	movq	24(%rdx), %rcx
	movq	16(%rdx), %rdx
	pushq	2240(%r14)
	pushq	%rsi
	movq	-144(%rbp), %rsi
	call	*%rax
	popq	%r10
	popq	%r11
	movl	%eax, %edx
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2404:
	movq	-144(%rbp), %rdi
	call	nghttp2_frame_trail_padlen@PLT
	testb	$4, 437(%r14)
	je	.L2576
	cmpq	632(%r14), %rax
	je	.L2907
.L2576:
	movq	-184(%rbp), %r15
	movq	632(%r14), %rdx
.L2407:
	movq	-200(%rbp), %rax
	subq	%rax, %rdx
	addq	%rax, %r15
	movq	%rdx, 632(%r14)
	jmp	.L2455
.L2357:
	subl	$14, %eax
	movl	$1, %ebx
	cmpl	$1, %eax
	jbe	.L2271
	xorl	%ebx, %ebx
	jmp	.L2358
.L2899:
	movl	$6, 648(%r14)
	movq	%r12, %r15
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_update_recv_connection_window_size
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
	cmpq	$0, 632(%r14)
	movl	$1, %ecx
	jne	.L2529
	movzbl	437(%r14), %eax
	andl	$1, %eax
	subl	%eax, %ecx
.L2529:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	nghttp2_session_update_recv_stream_window_size
	cmpl	$-900, %eax
	jl	.L2562
	movq	632(%r14), %rcx
	movq	640(%r14), %rsi
	movq	-144(%rbp), %rdi
	movq	%rcx, -152(%rbp)
	call	nghttp2_frame_trail_padlen@PLT
	movq	-152(%rbp), %rcx
	movq	%r13, %r8
	cmpq	%rax, %rcx
	jnb	.L2531
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r13
	jb	.L2532
	leaq	(%rcx,%r13), %r8
	subq	%rax, %r8
	cmpq	$-1, %r8
	je	.L2532
	subq	%r8, %r13
	testq	%r13, %r13
	jle	.L2535
.L2574:
	movl	432(%r14), %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, -152(%rbp)
	call	nghttp2_session_consume
	movq	-152(%rbp), %r8
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
.L2535:
	testq	%r8, %r8
	jle	.L2843
	testb	$4, 2484(%r14)
	je	.L2908
.L2538:
	movq	2048(%r14), %rax
	testq	%rax, %rax
	je	.L2843
	movl	432(%r14), %edx
	movq	2240(%r14), %r9
	movq	%r12, %rcx
	movq	%r14, %rdi
	movzbl	437(%r14), %esi
	call	*%rax
	cmpl	$-526, %eax
	je	.L2564
	cmpl	$-900, %eax
	jl	.L2553
.L2843:
	movq	632(%r14), %rax
	jmp	.L2527
.L2888:
	movzbl	436(%r14), %eax
.L2409:
	cmpb	$5, %al
	je	.L2411
.L2414:
	movl	$0, -204(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L2412
.L2891:
	movq	2120(%r14), %rax
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	testq	%rax, %rax
	je	.L2423
	movq	2240(%r14), %r9
	movzbl	-60(%rbp), %r8d
	movq	%r14, %rdi
	movq	-144(%rbp), %rsi
	call	*%rax
	movl	%eax, %edx
.L2424:
	cmpl	$-526, %edx
	je	.L2427
	cmpl	$-521, %edx
	je	.L2838
	testl	%edx, %edx
	jne	.L2861
.L2428:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	subq	$8, %rsp
	movq	%r14, %rdi
	movzbl	436(%r14), %ecx
	movl	$-531, %esi
	movq	24(%rax), %r9
	pushq	16(%rdx)
	movq	24(%rdx), %rdx
	pushq	%rdx
	leaq	.LC87(%rip), %rdx
	pushq	16(%rax)
	movl	432(%r14), %r8d
	xorl	%eax, %eax
	call	session_call_error_callback
	addq	$32, %rsp
	movl	%eax, %edx
	cmpl	$-900, %eax
	jge	.L2444
	.p2align 4,,10
	.p2align 3
.L2816:
	movl	%edx, %eax
	cltq
	jmp	.L2261
.L2296:
	movl	%edx, %eax
	shrb	$3, %al
	movzbl	%al, %eax
	movzbl	2501(%r14,%rax), %ecx
	movl	%edx, %eax
	andl	$7, %eax
	btl	%eax, %ecx
	jnc	.L2349
	cmpq	$0, 2168(%r14)
	je	.L2457
	movl	$18, 648(%r14)
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L2892:
	movq	%r12, %rdi
	movq	-184(%rbp), %r15
	call	nghttp2_hd_inflate_end_headers@PLT
	cmpl	$15, 648(%r14)
	jne	.L2806
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	624(%r14), %rax
	movq	520(%r14), %rsi
	movq	616(%r14), %r8
	testq	%rax, %rax
	je	.L2503
	leaq	-8(%rsi,%rax,8), %rdi
	movl	4(%rdi), %ecx
	cmpl	$-1, %ecx
	je	.L2503
	testq	%r8, %r8
	je	.L2504
	movq	%rsi, %rax
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L2909:
	addq	$1, %rdx
	addq	$8, %rax
	cmpq	%r8, %rdx
	je	.L2504
.L2506:
	cmpl	$1, (%rax)
	jne	.L2909
	cmpl	4(%rax), %ecx
	je	.L2503
	movq	(%rdi), %rcx
	movq	(%rax), %rdi
	leaq	1(%r8), %rdx
	movq	%rdx, 616(%r14)
	movq	%rdi, (%rsi,%r8,8)
	movq	%rdx, %r8
	movq	%rcx, (%rax)
.L2503:
	movq	-144(%rbp), %rbx
	movq	%r8, %rdx
	movq	%rbx, %rdi
	call	nghttp2_frame_unpack_settings_payload@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	$0, 520(%r14)
	movq	%r14, %rdi
	movups	%xmm0, 616(%r14)
	call	nghttp2_session_on_settings_received
	cmpl	$-900, %eax
	jge	.L2563
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2411:
	movl	464(%r14), %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L2414
	testb	$2, 236(%rax)
	jne	.L2414
	cmpl	$5, 228(%rax)
	je	.L2414
	movl	$0, -204(%rbp)
	jmp	.L2412
.L2887:
	testq	%rbx, %rbx
	jns	.L2837
	movq	-184(%rbp), %r15
	jmp	.L2407
.L2905:
	movq	-200(%rbp), %rsi
	movq	632(%r14), %rdx
	movq	-184(%rbp), %r15
	subq	%rsi, %rdx
	movq	%rdx, 632(%r14)
	addq	%rsi, %r15
	cmpl	$-523, %eax
	je	.L2456
	jmp	.L2455
.L2531:
	cmpq	$-1, %r13
	jne	.L2535
	jmp	.L2843
.L2547:
	movl	2420(%r14), %eax
	subq	$8, %rsp
	movzbl	2500(%r14), %ecx
	leaq	2408(%r14), %rdx
	leaq	2412(%r14), %rsi
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	pushq	%rax
	call	session_update_consumed_size
	popq	%rdx
	popq	%rcx
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	jne	.L2844
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2833:
	leaq	528(%r14), %rdi
	xorl	%ebx, %ebx
	call	nghttp2_buf_reset@PLT
	addq	$1, 560(%r14)
	movl	$3, 648(%r14)
	jmp	.L2358
.L2900:
	leaq	-80(%rbp), %r8
	movq	544(%r14), %rsi
	movq	%r8, %rdi
	call	nghttp2_frame_unpack_settings_entry@PLT
	movl	-80(%rbp), %ecx
	cmpl	$6, %ecx
	jg	.L2493
	testl	%ecx, %ecx
	jg	.L2494
.L2495:
	movq	616(%r14), %rax
	movq	520(%r14), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 616(%r14)
	movq	-80(%rbp), %rcx
	movq	%rcx, (%rdx,%rax,8)
	movq	632(%r14), %rdx
	jmp	.L2492
.L2493:
	cmpl	$8, %ecx
	jne	.L2495
.L2494:
	movq	520(%r14), %rdi
	movq	616(%r14), %rsi
	xorl	%edx, %edx
	movq	%rdi, %rax
	testq	%rsi, %rsi
	jne	.L2500
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2498:
	addq	$1, %rdx
	addq	$8, %rax
	cmpq	%rsi, %rdx
	je	.L2497
.L2500:
	cmpl	(%rax), %ecx
	jne	.L2498
	movq	-80(%rbp), %rcx
	movq	%rcx, (%rax)
	cmpq	%rsi, %rdx
	je	.L2497
.L2499:
	cmpl	$1, -80(%rbp)
	je	.L2910
.L2501:
	movq	632(%r14), %rdx
	jmp	.L2492
.L2893:
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	$9, 560(%r14)
	cmpl	$4, 648(%r14)
	movq	$0, 640(%r14)
	je	.L2911
	movl	$11, 648(%r14)
	movl	$1, %ebx
	jmp	.L2271
.L2327:
	movzbl	%al, %edi
	call	nghttp2_frame_priority_len@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2330
	cmpq	%rax, 632(%r14)
	jb	.L2842
	movl	$3, 648(%r14)
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	%rbx, 560(%r14)
	jmp	.L2332
.L2349:
	cmpb	$10, %dl
	je	.L2351
	cmpb	$12, %dl
	jne	.L2457
	testb	$2, 2492(%r14)
	je	.L2457
	leaq	488(%r14), %rax
	cmpb	$0, 2498(%r14)
	movq	%rax, 440(%r14)
	jne	.L2457
	movl	432(%r14), %ebx
	testl	%ebx, %ebx
	jne	.L2457
	testb	$-16, 437(%r14)
	jne	.L2457
	movb	$0, 437(%r14)
	testq	%rsi, %rsi
	jne	.L2912
	movl	$1, %ebx
.L2355:
	movl	$17, 648(%r14)
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2347:
	cmpq	$3, %rsi
	jbe	.L2842
.L2831:
	movl	$3, 648(%r14)
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	$4, 560(%r14)
	jmp	.L2332
.L2889:
	cmpl	$3, %eax
	sete	%al
	movzbl	%al, %eax
	movl	%eax, -204(%rbp)
	jmp	.L2412
.L2906:
	movl	464(%r14), %esi
	jmp	.L2453
.L2385:
	movq	544(%r14), %rsi
	movq	-144(%rbp), %rdi
	call	nghttp2_frame_unpack_push_promise_payload@PLT
	testl	%eax, %eax
	je	.L2391
	testb	$1, 2499(%r14)
	jne	.L2913
	movl	2388(%r14), %esi
	leaq	.LC85(%rip), %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	call	session_terminate_session.part.0
.L2394:
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$15, 648(%r14)
	je	.L2276
	cmpl	$-521, %eax
	je	.L2914
	cmpl	$-103, %eax
	je	.L2514
.L2393:
	movl	$4, 648(%r14)
	movq	%r15, -184(%rbp)
	jmp	.L2269
.L2583:
	movl	$1, %edx
	leaq	.LC72(%rip), %rcx
	jmp	.L2307
.L2901:
	testl	%ebx, %ebx
	je	.L2575
.L2309:
	cmpl	2384(%r14), %r12d
	jg	.L2585
.L2310:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	je	.L2311
	testb	$2, 236(%rax)
	jne	.L2311
	movl	228(%rax), %esi
	cmpl	$5, %esi
	je	.L2311
	testb	$1, 237(%rax)
	jne	.L2915
	cmpb	$0, 2498(%r14)
	je	.L2316
	xorl	$1, %ebx
.L2316:
	testl	%ebx, %ebx
	je	.L2317
	cmpl	$3, %esi
	je	.L2830
	movl	$1, %edx
	leaq	.LC70(%rip), %rcx
	cmpl	$2, %esi
	jne	.L2307
	cmpl	$15, 648(%r14)
	je	.L2276
.L2319:
	testb	$8, 437(%r14)
	je	.L2324
	cmpq	$0, 424(%r14)
	jne	.L2916
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC78(%rip), %rcx
	jmp	.L2850
.L2324:
	movl	$13, 648(%r14)
	jmp	.L2607
.L2907:
	xorl	%r15d, %r15d
.L2837:
	movl	$1, -156(%rbp)
	jmp	.L2572
.L2423:
	movq	2112(%r14), %rax
	movq	24(%rcx), %r9
	movq	16(%rcx), %r8
	movq	24(%rdx), %r11
	movq	16(%rdx), %r10
	testq	%rax, %rax
	je	.L2429
	movzbl	-60(%rbp), %edx
	movq	%r11, %rcx
	movq	%r14, %rdi
	pushq	2240(%r14)
	movq	-144(%rbp), %rsi
	pushq	%rdx
	movq	%r10, %rdx
	call	*%rax
	movl	%eax, %edx
	popq	%rax
	popq	%rcx
	jmp	.L2424
.L2341:
	movq	2352(%r14), %rax
	cmpq	%rax, 2344(%r14)
	jnb	.L2917
	movl	$8, 648(%r14)
	testq	%rsi, %rsi
	je	.L2343
	movq	%rsi, %rax
	mulq	%rcx
	shrq	$2, %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 624(%r14)
	cmpq	2368(%r14), %rdx
	ja	.L2918
	movq	-192(%rbp), %rdi
	leaq	0(,%rax,8), %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, 520(%r14)
	testq	%rax, %rax
	je	.L2356
	movq	624(%r14), %rdx
	leaq	528(%r14), %rdi
	movabsq	$-4294967295, %rsi
	movq	%rsi, -8(%rax,%rdx,8)
	call	nghttp2_buf_reset@PLT
	addq	$6, 560(%r14)
	jmp	.L2332
.L2898:
	movq	-176(%rbp), %rax
	cmpl	$3, 228(%rax)
	je	.L2418
	movl	192(%rax), %esi
	movl	$9, %edx
	movq	%r14, %rdi
	call	nghttp2_session_add_rst_stream
	cmpl	$-900, %eax
	jge	.L2418
	cltq
	jmp	.L2261
.L2908:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%r8, -152(%rbp)
	call	nghttp2_http_on_data_chunk@PLT
	movq	-152(%rbp), %r8
	testl	%eax, %eax
	je	.L2538
	testb	$1, 2484(%r14)
	je	.L2539
	movzbl	2500(%r14), %ecx
	pushq	%rdi
	movq	%r8, %r9
	leaq	2408(%r14), %rdx
	movl	2420(%r14), %eax
	xorl	%r8d, %r8d
	leaq	2412(%r14), %rsi
	movq	%r14, %rdi
	pushq	%rax
	call	session_update_consumed_size
	popq	%r8
	popq	%r9
	cmpl	$-900, %eax
	jl	.L2562
	cmpl	$14, 648(%r14)
	je	.L2276
.L2539:
	movl	432(%r14), %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	nghttp2_session_add_rst_stream
	cmpl	$-900, %eax
	jge	.L2541
	cltq
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2291:
	xorl	%eax, %eax
	leaq	.LC75(%rip), %rdx
	movq	%r14, %rdi
	movq	%r14, %r15
	movl	$-536, %esi
	call	session_call_error_callback
	cmpl	$-900, %eax
	jl	.L2562
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC76(%rip), %rcx
	jmp	.L2850
.L2512:
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC91(%rip), %rcx
	jmp	.L2850
.L2880:
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC77(%rip), %rcx
	movl	$6, %edx
	movq	%r14, %rdi
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jge	.L2276
	cltq
	jmp	.L2261
.L2532:
	xorl	%r8d, %r8d
	testq	%r13, %r13
	jg	.L2574
	jmp	.L2843
.L2885:
	movq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, 608(%r14)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2356
	leaq	568(%r14), %rdi
	movq	%r12, %rdx
	call	nghttp2_buf_wrap_init@PLT
	jmp	.L2399
.L2391:
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	nghttp2_session_on_push_promise_received
	jmp	.L2394
.L2911:
	movl	$10, 648(%r14)
	movl	$1, %ebx
	jmp	.L2271
.L2330:
	movq	2144(%r14), %rax
	testq	%rax, %rax
	je	.L2372
	movq	2240(%r14), %rdx
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L2372
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2351:
	testb	$1, 2492(%r14)
	je	.L2457
	leaq	488(%r14), %rax
	cmpb	$0, 2498(%r14)
	movb	$0, 437(%r14)
	movq	%rax, 440(%r14)
	jne	.L2457
	cmpq	$1, %rsi
	jbe	.L2842
	movl	$3, 648(%r14)
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	$2, 560(%r14)
	jmp	.L2346
.L2427:
	cmpl	$-521, %edx
	je	.L2838
	testl	%edx, %edx
	je	.L2428
	jmp	.L2815
	.p2align 4,,10
	.p2align 3
.L2916:
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	$1, 560(%r14)
	movl	$12, 648(%r14)
	jmp	.L2607
.L2902:
	movzbl	%al, %edi
	call	nghttp2_frame_priority_len@PLT
	movq	%rax, %rbx
	movq	544(%r14), %rax
	movzbl	(%rax), %ecx
	movq	632(%r14), %rax
	leaq	1(%rcx), %rdx
	cmpq	%rax, %rcx
	ja	.L2835
	leaq	(%rbx,%rdx), %rsi
	leaq	1(%rax), %rcx
	movq	%rdx, 640(%r14)
	cmpq	%rcx, %rsi
	ja	.L2835
	movq	%rdx, 440(%r14)
	testq	%rbx, %rbx
	je	.L2377
	cmpq	%rax, %rbx
	ja	.L2842
	movl	$3, 648(%r14)
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	addq	%rbx, 560(%r14)
	movl	$1, %ebx
	jmp	.L2271
.L2903:
	movl	432(%r14), %esi
.L2854:
	movl	$2, %edx
	movq	%r14, %rdi
	call	nghttp2_session_add_rst_stream
	cmpl	$-900, %eax
	jge	.L2514
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2581:
	movq	$-903, %rax
	jmp	.L2261
.L2838:
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
.L2429:
	movq	24(%rcx), %rax
	subq	$8, %rsp
	movq	24(%rdx), %r9
	movq	%r14, %rdi
	movzbl	436(%r14), %r10d
	pushq	16(%rcx)
	movl	$-531, %esi
	pushq	%rax
	xorl	%eax, %eax
	pushq	16(%rdx)
	movl	432(%r14), %r8d
	movl	%r10d, %ecx
	leaq	.LC86(%rip), %rdx
	call	session_call_error_callback
	addq	$32, %rsp
	cmpl	$-900, %eax
	jl	.L2562
	movq	-176(%rbp), %rax
	movl	$1, %edx
	movq	%r14, %rdi
	movl	192(%rax), %esi
	call	nghttp2_session_add_rst_stream
	testl	%eax, %eax
	jne	.L2432
	movq	2040(%r14), %rax
	testq	%rax, %rax
	je	.L2435
	movq	2240(%r14), %rcx
	movq	-144(%rbp), %rsi
	movl	$-531, %edx
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L2861
.L2435:
	cmpl	$15, 648(%r14)
	jne	.L2434
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	%r12, %rax
	subq	-136(%rbp), %rax
	jmp	.L2261
.L2465:
	cmpl	$3, %eax
	jne	.L2919
	testb	$64, 235(%r12)
	je	.L2471
	cmpb	$0, 2498(%r14)
	jne	.L2920
.L2472:
	movq	%r12, %rdi
	call	nghttp2_http_on_response_headers@PLT
	jmp	.L2470
.L2882:
	movq	-192(%rbp), %rdi
	leaq	-2(%rax), %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, 608(%r14)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2356
	movq	424(%r14), %rdx
	leaq	568(%r14), %rdi
	call	nghttp2_buf_wrap_init@PLT
	jmp	.L2402
.L2497:
	leaq	1(%rsi), %rax
	movq	%rax, 616(%r14)
	movq	-80(%rbp), %rax
	movq	%rax, (%rdi,%rsi,8)
	jmp	.L2499
.L2915:
	movl	$5, %edx
	leaq	.LC69(%rip), %rcx
	jmp	.L2307
.L2317:
	cmpl	$4, %esi
	je	.L2589
	movl	648(%r14), %eax
	cmpl	$3, %esi
	je	.L2320
	cmpl	$15, %eax
	jne	.L2319
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2910:
	movq	624(%r14), %rax
	movl	-76(%rbp), %edx
	leaq	-8(%rdi,%rax,8), %rax
	cmpl	4(%rax), %edx
	jnb	.L2501
	movl	%edx, 4(%rax)
	jmp	.L2501
.L2904:
	movq	-168(%rbp), %rax
	movq	%r14, %r15
	movq	-184(%rbp), %r14
	subq	%rax, 632(%r15)
	addq	%r14, %rax
	subq	-136(%rbp), %rax
	jmp	.L2261
.L2914:
	movl	464(%r14), %esi
	jmp	.L2854
.L2913:
	cmpl	$15, 648(%r14)
	jne	.L2393
	jmp	.L2276
.L2896:
	movq	%r12, %rdi
	call	nghttp2_http_on_remote_end_stream@PLT
.L2463:
	testl	%eax, %eax
	je	.L2461
.L2476:
	cmpb	$5, 436(%r14)
	je	.L2921
	movl	432(%r14), %esi
.L2477:
	movl	$1, %edx
	movq	%r14, %rdi
	call	nghttp2_session_add_rst_stream
	testl	%eax, %eax
	jne	.L2478
	movq	2040(%r14), %rax
	testq	%rax, %rax
	je	.L2481
	movq	2240(%r14), %rcx
	movq	-144(%rbp), %rsi
	movl	$-532, %edx
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L2861
.L2481:
	cmpb	$1, 436(%r14)
	jne	.L2846
	testb	$1, 437(%r14)
	je	.L2846
	movl	$1, %esi
	movq	%r12, %rdi
	call	nghttp2_stream_shutdown@PLT
	jmp	.L2846
.L2878:
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC92(%rip), %rcx
	jmp	.L2850
.L2377:
	leaq	528(%r14), %rdi
	call	nghttp2_buf_reset@PLT
	jmp	.L2372
.L2872:
	call	__stack_chk_fail@PLT
.L2589:
	movl	$1, %edx
	leaq	.LC74(%rip), %rcx
	jmp	.L2307
.L2478:
	cmpl	$-900, %eax
	jge	.L2481
	cltq
	jmp	.L2261
.L2432:
	cmpl	$-900, %eax
	jge	.L2435
	cltq
	jmp	.L2261
.L2921:
	movl	464(%r14), %esi
	jmp	.L2477
.L2894:
	movl	464(%r14), %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2461
	testb	$2, 236(%rax)
	jne	.L2461
	cmpl	$5, 228(%rax)
	je	.L2461
	movq	-144(%rbp), %rsi
	call	nghttp2_http_on_request_headers@PLT
	jmp	.L2463
.L2356:
	movq	$-901, %rax
	jmp	.L2261
.L2918:
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC80(%rip), %rcx
	movl	$11, %edx
	movq	%r14, %rdi
	call	session_terminate_session.part.0
	cmpl	$-900, %eax
	jge	.L2276
	cltq
	jmp	.L2261
.L2836:
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC84(%rip), %rcx
	jmp	.L2850
.L2471:
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	nghttp2_http_on_trailer_headers@PLT
	jmp	.L2470
.L2504:
	leaq	__PRETTY_FUNCTION__.7070(%rip), %rcx
	movl	$4572, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC93(%rip), %rdi
	call	__assert_fail@PLT
.L2912:
	movq	-192(%rbp), %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, 608(%r14)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2356
	movq	632(%r14), %rdx
	leaq	568(%r14), %rdi
	call	nghttp2_buf_wrap_init@PLT
	jmp	.L2355
.L2919:
	leaq	__PRETTY_FUNCTION__.6922(%rip), %rcx
	movl	$3775, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	call	__assert_fail@PLT
.L2320:
	cmpl	$15, %eax
	jne	.L2541
	jmp	.L2276
.L2835:
	movq	%r14, %r15
	testb	$1, 2499(%r14)
	jne	.L2276
	movl	2388(%r14), %esi
	leaq	.LC83(%rip), %rcx
	jmp	.L2850
.L2884:
	leaq	__PRETTY_FUNCTION__.7329(%rip), %rcx
	movl	$6716, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	call	__assert_fail@PLT
.L2917:
	movq	$-904, %rax
	jmp	.L2261
.L2920:
	leaq	__PRETTY_FUNCTION__.6922(%rip), %rcx
	movl	$3768, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC88(%rip), %rdi
	call	__assert_fail@PLT
.L2895:
	leaq	__PRETTY_FUNCTION__.6922(%rip), %rcx
	movl	$3757, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC67(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	nghttp2_session_mem_recv.cold, @function
nghttp2_session_mem_recv.cold:
.LFSB203:
.L2579:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, %ebx
	jmp	.L2271
	.cfi_endproc
.LFE203:
	.text
	.size	nghttp2_session_mem_recv, .-nghttp2_session_mem_recv
	.section	.text.unlikely
	.size	nghttp2_session_mem_recv.cold, .-nghttp2_session_mem_recv.cold
.LCOLDE94:
	.text
.LHOTE94:
	.section	.rodata.str1.1
.LC95:
	.string	"proclen == readlen"
	.text
	.p2align 4
	.globl	nghttp2_session_recv
	.type	nghttp2_session_recv, @function
nghttp2_session_recv:
.LFB204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL0:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL0
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r13
	leaq	-16432(%rbp), %r12
	jmp	.L2927
	.p2align 4,,10
	.p2align 3
.L2946:
	cmpq	$16384, %rax
	jg	.L2944
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	nghttp2_session_mem_recv
	testq	%rax, %rax
	js	.L2922
	cmpq	%rbx, %rax
	jne	.L2945
.L2927:
	movq	2240(%r13), %r8
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$16384, %edx
	call	*2024(%r13)
	movq	%rax, %rbx
	testq	%rax, %rax
	jg	.L2946
	jns	.L2926
	cmpq	$-504, %rax
	je	.L2926
	cmpq	$-507, %rax
	movl	$-902, %edx
	movl	$-507, %eax
	cmovne	%edx, %eax
.L2922:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2947
	addq	$16408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2944:
	.cfi_restore_state
	movl	$-902, %eax
	jmp	.L2922
.L2945:
	leaq	__PRETTY_FUNCTION__.7350(%rip), %rcx
	movl	$6787, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC95(%rip), %rdi
	call	__assert_fail@PLT
.L2947:
	call	__stack_chk_fail@PLT
.L2926:
	testq	%rbx, %rbx
	je	.L2935
	cmpq	$-504, %rbx
	je	.L2935
	cmpq	$-507, %rbx
	movl	$-902, %eax
	movl	$-507, %edx
	cmove	%edx, %eax
	jmp	.L2922
.L2935:
	xorl	%eax, %eax
	jmp	.L2922
	.cfi_endproc
.LFE204:
	.size	nghttp2_session_recv, .-nghttp2_session_recv
	.p2align 4
	.globl	nghttp2_session_consume_connection
	.type	nghttp2_session_consume_connection, @function
nghttp2_session_consume_connection:
.LFB234:
	.cfi_startproc
	endbr64
	testb	$1, 2484(%rdi)
	je	.L2955
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483647, %edx
	subq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movslq	2412(%rdi), %rcx
	cmpq	%rdx, %rcx
	ja	.L2965
	addl	%ecx, %esi
	movl	2420(%rdi), %edi
	cmpb	$0, 2500(%r12)
	movl	%esi, 2412(%r12)
	je	.L2966
.L2953:
	xorl	%eax, %eax
.L2948:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2966:
	.cfi_restore_state
	cmpl	%esi, 2408(%r12)
	movl	%esi, %ebx
	cmovle	2408(%r12), %ebx
	movl	%ebx, %esi
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L2953
	leaq	2200(%r12), %r13
	movl	$152, %esi
	movq	%r13, %rdi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2956
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%ebx, %ecx
	call	nghttp2_frame_window_update_init@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L2954
	subl	%ebx, 2408(%r12)
	subl	%ebx, 2412(%r12)
	jmp	.L2948
	.p2align 4,,10
	.p2align 3
.L2965:
	testb	$1, 2499(%rdi)
	jne	.L2953
	movl	2388(%rdi), %esi
	xorl	%ecx, %ecx
	movl	$3, %edx
	call	session_terminate_session.part.0
.L2952:
	cmpl	$-900, %eax
	jge	.L2953
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2955:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$-519, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2954:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%r14, %rdi
	movl	%eax, -36(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	nghttp2_mem_free@PLT
	movl	-36(%rbp), %eax
	jmp	.L2952
.L2956:
	movl	$-901, %eax
	jmp	.L2948
	.cfi_endproc
.LFE234:
	.size	nghttp2_session_consume_connection, .-nghttp2_session_consume_connection
	.p2align 4
	.globl	nghttp2_session_consume_stream
	.type	nghttp2_session_consume_stream, @function
nghttp2_session_consume_stream:
.LFB235:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L2974
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testb	$1, 2484(%rdi)
	je	.L2975
	movq	%rdx, %r13
	call	nghttp2_map_find@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2970
	testb	$2, 236(%rax)
	jne	.L2970
	cmpl	$5, 228(%rax)
	je	.L2970
	movslq	204(%rax), %rcx
	movl	$2147483647, %eax
	subq	%r13, %rax
	movq	%rcx, %rdx
	cmpq	%rax, %rcx
	ja	.L2985
	addl	%r13d, %edx
	cmpb	$0, 239(%rbx)
	movl	212(%rbx), %edi
	movl	%edx, 204(%rbx)
	movl	192(%rbx), %r15d
	je	.L2986
.L2970:
	xorl	%eax, %eax
.L2967:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2985:
	.cfi_restore_state
	testb	$1, 2499(%r12)
	jne	.L2970
	movl	2388(%r12), %esi
	xorl	%ecx, %ecx
	movl	$3, %edx
	movq	%r12, %rdi
	call	session_terminate_session.part.0
.L2972:
	cmpl	$-900, %eax
	jge	.L2970
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2986:
	.cfi_restore_state
	cmpl	%edx, 200(%rbx)
	cmovle	200(%rbx), %edx
	movl	%edx, %esi
	movl	%edx, %r13d
	call	nghttp2_should_send_window_update@PLT
	testl	%eax, %eax
	je	.L2970
	leaq	2200(%r12), %rax
	movl	$152, %esi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2976
	movq	%rax, %rdi
	call	nghttp2_outbound_item_init@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r13d, %ecx
	movl	%r15d, %edx
	call	nghttp2_frame_window_update_init@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	nghttp2_session_add_item
	testl	%eax, %eax
	jne	.L2973
	subl	%r13d, 200(%rbx)
	subl	%r13d, 204(%rbx)
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2975:
	movl	$-519, %eax
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2974:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-501, %eax
	ret
.L2973:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	nghttp2_frame_window_update_free@PLT
	movq	-56(%rbp), %rdi
	movq	%r14, %rsi
	call	nghttp2_mem_free@PLT
	movl	-60(%rbp), %eax
	jmp	.L2972
.L2976:
	movl	$-901, %eax
	jmp	.L2967
	.cfi_endproc
.LFE235:
	.size	nghttp2_session_consume_stream, .-nghttp2_session_consume_stream
	.p2align 4
	.globl	nghttp2_session_set_next_stream_id
	.type	nghttp2_session_set_next_stream_id, @function
nghttp2_session_set_next_stream_id:
.LFB236:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L2994
	cmpl	%esi, 2376(%rdi)
	ja	.L2994
	movl	%esi, %eax
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L2995
	testl	%eax, %eax
	je	.L2994
.L2990:
	movl	%esi, 2376(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2995:
	testl	%eax, %eax
	je	.L2990
.L2994:
	movl	$-501, %eax
	ret
	.cfi_endproc
.LFE236:
	.size	nghttp2_session_set_next_stream_id, .-nghttp2_session_set_next_stream_id
	.p2align 4
	.globl	nghttp2_session_get_next_stream_id
	.type	nghttp2_session_get_next_stream_id, @function
nghttp2_session_get_next_stream_id:
.LFB237:
	.cfi_startproc
	endbr64
	movl	2376(%rdi), %eax
	ret
	.cfi_endproc
.LFE237:
	.size	nghttp2_session_get_next_stream_id, .-nghttp2_session_get_next_stream_id
	.p2align 4
	.globl	nghttp2_session_get_last_proc_stream_id
	.type	nghttp2_session_get_last_proc_stream_id, @function
nghttp2_session_get_last_proc_stream_id:
.LFB238:
	.cfi_startproc
	endbr64
	movl	2388(%rdi), %eax
	ret
	.cfi_endproc
.LFE238:
	.size	nghttp2_session_get_last_proc_stream_id, .-nghttp2_session_get_last_proc_stream_id
	.p2align 4
	.globl	nghttp2_session_find_stream
	.type	nghttp2_session_find_stream, @function
nghttp2_session_find_stream:
.LFB239:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L2999
	leaq	32(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2999:
	jmp	nghttp2_map_find@PLT
	.cfi_endproc
.LFE239:
	.size	nghttp2_session_find_stream, .-nghttp2_session_find_stream
	.p2align 4
	.globl	nghttp2_session_get_root_stream
	.type	nghttp2_session_get_root_stream, @function
nghttp2_session_get_root_stream:
.LFB240:
	.cfi_startproc
	endbr64
	leaq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE240:
	.size	nghttp2_session_get_root_stream, .-nghttp2_session_get_root_stream
	.p2align 4
	.globl	nghttp2_session_check_server_session
	.type	nghttp2_session_check_server_session, @function
nghttp2_session_check_server_session:
.LFB241:
	.cfi_startproc
	endbr64
	movzbl	2498(%rdi), %eax
	ret
	.cfi_endproc
.LFE241:
	.size	nghttp2_session_check_server_session, .-nghttp2_session_check_server_session
	.p2align 4
	.globl	nghttp2_session_change_stream_priority
	.type	nghttp2_session_change_stream_priority, @function
nghttp2_session_change_stream_priority:
.LFB242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L3007
	movq	%rdx, %rbx
	cmpl	%esi, (%rdx)
	je	.L3007
	movq	%rdi, %r13
	call	nghttp2_map_find@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3007
	movq	(%rbx), %rax
	movq	%rax, -80(%rbp)
	movl	8(%rbx), %eax
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	movl	%eax, -72(%rbp)
	call	nghttp2_priority_spec_normalize_weight@PLT
	movl	192(%r12), %eax
	cmpl	%eax, -80(%rbp)
	je	.L3043
	movq	%r12, %rdi
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	je	.L3009
	movl	-80(%rbp), %esi
	movq	%rbx, %r15
	testl	%esi, %esi
	jne	.L3044
.L3010:
	leaq	32(%r13), %r14
.L3019:
	cmpq	%r14, 120(%r12)
	je	.L3045
.L3024:
	movq	%r12, %rdi
	call	nghttp2_stream_dep_remove_subtree@PLT
	movl	4(%r15), %eax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%eax, 216(%r12)
	cmpb	$0, 8(%r15)
	je	.L3025
	call	nghttp2_stream_dep_insert_subtree@PLT
.L3026:
	testl	%eax, %eax
	jne	.L3022
.L3004:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3046
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3044:
	.cfi_restore_state
	movq	%r13, %rdi
	call	nghttp2_map_find@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3047
	movq	%rax, %rdi
	call	nghttp2_stream_in_dep_tree@PLT
	testl	%eax, %eax
	jne	.L3013
.L3017:
	leaq	-68(%rbp), %r15
	movq	%r15, %rdi
	call	nghttp2_priority_spec_default_init@PLT
.L3013:
	movl	(%r15), %eax
	testl	%eax, %eax
	je	.L3010
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	nghttp2_stream_dep_find_ancestor@PLT
	testl	%eax, %eax
	jne	.L3020
.L3023:
	testq	%r14, %r14
	jne	.L3019
	leaq	__PRETTY_FUNCTION__.6384(%rip), %rcx
	movl	$818, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	%r14, %rdi
	call	nghttp2_stream_dep_remove_subtree@PLT
	movq	120(%r12), %rdi
	movq	%r14, %rsi
	call	nghttp2_stream_dep_add_subtree@PLT
	testl	%eax, %eax
	je	.L3023
	.p2align 4,,10
	.p2align 3
.L3022:
	cmpl	$-900, %eax
	jl	.L3004
	.p2align 4,,10
	.p2align 3
.L3009:
	xorl	%eax, %eax
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3045:
	cmpb	$0, 8(%r15)
	jne	.L3024
	movl	4(%r15), %esi
	movq	%r12, %rdi
	call	nghttp2_stream_change_weight@PLT
	xorl	%eax, %eax
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3025:
	call	nghttp2_stream_dep_add_subtree@PLT
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3047:
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L3017
	movl	%eax, %edx
	andl	$1, %edx
	cmpb	$0, 2498(%r13)
	jne	.L3048
	testl	%edx, %edx
	je	.L3015
.L3027:
	cmpl	2380(%r13), %eax
	jle	.L3017
.L3016:
	leaq	-68(%rbp), %r14
	movq	%r14, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	movl	-80(%rbp), %esi
	movq	%r14, %rcx
	xorl	%r9d, %r9d
	movl	$5, %r8d
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	nghttp2_session_open_stream
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3049
	movq	%rbx, %r15
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L3007:
	movl	$-501, %eax
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3048:
	testl	%edx, %edx
	je	.L3027
.L3015:
	cmpl	2384(%r13), %eax
	jle	.L3017
	jmp	.L3016
.L3046:
	call	__stack_chk_fail@PLT
.L3043:
	leaq	__PRETTY_FUNCTION__.6384(%rip), %rcx
	movl	$778, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	call	__assert_fail@PLT
.L3049:
	movl	$-901, %eax
	jmp	.L3004
	.cfi_endproc
.LFE242:
	.size	nghttp2_session_change_stream_priority, .-nghttp2_session_change_stream_priority
	.p2align 4
	.globl	nghttp2_session_create_idle_stream
	.type	nghttp2_session_create_idle_stream, @function
nghttp2_session_create_idle_stream:
.LFB243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L3053
	movl	%esi, %r12d
	movq	%rdx, %rbx
	cmpl	%esi, (%rdx)
	je	.L3053
	movl	%esi, %eax
	movq	%rdi, %r14
	andl	$1, %eax
	cmpb	$0, 2498(%rdi)
	jne	.L3061
	testl	%eax, %eax
	je	.L3055
.L3057:
	cmpl	2380(%r14), %r12d
	jle	.L3053
.L3056:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	nghttp2_map_find@PLT
	testq	%rax, %rax
	jne	.L3053
	movq	(%rbx), %rax
	leaq	-52(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -52(%rbp)
	movl	8(%rbx), %eax
	movl	%eax, -44(%rbp)
	call	nghttp2_priority_spec_normalize_weight@PLT
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	$5, %r8d
	movq	%r13, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	nghttp2_session_open_stream
	cmpq	$1, %rax
	sbbl	%eax, %eax
	andl	$-901, %eax
.L3050:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3062
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3061:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L3057
.L3055:
	cmpl	2384(%r14), %r12d
	jg	.L3056
.L3053:
	movl	$-501, %eax
	jmp	.L3050
.L3062:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE243:
	.size	nghttp2_session_create_idle_stream, .-nghttp2_session_create_idle_stream
	.p2align 4
	.globl	nghttp2_session_get_hd_inflate_dynamic_table_size
	.type	nghttp2_session_get_hd_inflate_dynamic_table_size, @function
nghttp2_session_get_hd_inflate_dynamic_table_size:
.LFB244:
	.cfi_startproc
	endbr64
	addq	$1776, %rdi
	jmp	nghttp2_hd_inflate_get_dynamic_table_size@PLT
	.cfi_endproc
.LFE244:
	.size	nghttp2_session_get_hd_inflate_dynamic_table_size, .-nghttp2_session_get_hd_inflate_dynamic_table_size
	.p2align 4
	.globl	nghttp2_session_get_hd_deflate_dynamic_table_size
	.type	nghttp2_session_get_hd_deflate_dynamic_table_size, @function
nghttp2_session_get_hd_deflate_dynamic_table_size:
.LFB245:
	.cfi_startproc
	endbr64
	addq	$664, %rdi
	jmp	nghttp2_hd_deflate_get_dynamic_table_size@PLT
	.cfi_endproc
.LFE245:
	.size	nghttp2_session_get_hd_deflate_dynamic_table_size, .-nghttp2_session_get_hd_deflate_dynamic_table_size
	.p2align 4
	.globl	nghttp2_session_set_user_data
	.type	nghttp2_session_set_user_data, @function
nghttp2_session_set_user_data:
.LFB246:
	.cfi_startproc
	endbr64
	movq	%rsi, 2240(%rdi)
	ret
	.cfi_endproc
.LFE246:
	.size	nghttp2_session_set_user_data, .-nghttp2_session_set_user_data
	.section	.rodata
	.align 32
	.type	CSWTCH.113, @object
	.size	CSWTCH.113, 116
CSWTCH.113:
	.long	7
	.long	1
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	3
	.long	9
	.long	6
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	5
	.long	2
	.long	2
	.long	2
	.long	2
	.long	1
	.align 16
	.type	__PRETTY_FUNCTION__.7556, @object
	.size	__PRETTY_FUNCTION__.7556, 25
__PRETTY_FUNCTION__.7556:
	.string	"nghttp2_session_upgrade2"
	.align 16
	.type	__PRETTY_FUNCTION__.7546, @object
	.size	__PRETTY_FUNCTION__.7546, 24
__PRETTY_FUNCTION__.7546:
	.string	"nghttp2_session_upgrade"
	.align 32
	.type	__PRETTY_FUNCTION__.7524, @object
	.size	__PRETTY_FUNCTION__.7524, 35
__PRETTY_FUNCTION__.7524:
	.string	"nghttp2_session_get_local_settings"
	.align 32
	.type	__PRETTY_FUNCTION__.7512, @object
	.size	__PRETTY_FUNCTION__.7512, 36
__PRETTY_FUNCTION__.7512:
	.string	"nghttp2_session_get_remote_settings"
	.align 32
	.type	__PRETTY_FUNCTION__.7454, @object
	.size	__PRETTY_FUNCTION__.7454, 37
__PRETTY_FUNCTION__.7454:
	.string	"nghttp2_session_set_stream_user_data"
	.align 16
	.type	__PRETTY_FUNCTION__.7440, @object
	.size	__PRETTY_FUNCTION__.7440, 26
__PRETTY_FUNCTION__.7440:
	.string	"nghttp2_session_pack_data"
	.align 16
	.type	__PRETTY_FUNCTION__.7416, @object
	.size	__PRETTY_FUNCTION__.7416, 29
__PRETTY_FUNCTION__.7416:
	.string	"nghttp2_session_add_settings"
	.align 16
	.type	__PRETTY_FUNCTION__.7350, @object
	.size	__PRETTY_FUNCTION__.7350, 21
__PRETTY_FUNCTION__.7350:
	.string	"nghttp2_session_recv"
	.align 16
	.type	__PRETTY_FUNCTION__.7070, @object
	.size	__PRETTY_FUNCTION__.7070, 31
__PRETTY_FUNCTION__.7070:
	.string	"session_process_settings_frame"
	.align 32
	.type	__PRETTY_FUNCTION__.6922, @object
	.size	__PRETTY_FUNCTION__.6922, 36
__PRETTY_FUNCTION__.6922:
	.string	"session_after_header_block_received"
	.align 16
	.type	__PRETTY_FUNCTION__.7329, @object
	.size	__PRETTY_FUNCTION__.7329, 25
__PRETTY_FUNCTION__.7329:
	.string	"nghttp2_session_mem_recv"
	.align 32
	.type	__PRETTY_FUNCTION__.6950, @object
	.size	__PRETTY_FUNCTION__.6950, 50
__PRETTY_FUNCTION__.6950:
	.string	"nghttp2_session_on_push_response_headers_received"
	.align 32
	.type	__PRETTY_FUNCTION__.6943, @object
	.size	__PRETTY_FUNCTION__.6943, 45
__PRETTY_FUNCTION__.6943:
	.string	"nghttp2_session_on_response_headers_received"
	.align 16
	.type	__PRETTY_FUNCTION__.6660, @object
	.size	__PRETTY_FUNCTION__.6660, 27
__PRETTY_FUNCTION__.6660:
	.string	"find_stream_on_goaway_func"
	.align 16
	.type	__PRETTY_FUNCTION__.6670, @object
	.size	__PRETTY_FUNCTION__.6670, 31
__PRETTY_FUNCTION__.6670:
	.string	"session_close_stream_on_goaway"
	.align 16
	.type	__PRETTY_FUNCTION__.6705, @object
	.size	__PRETTY_FUNCTION__.6705, 26
__PRETTY_FUNCTION__.6705:
	.string	"session_after_frame_sent1"
	.align 16
	.type	__PRETTY_FUNCTION__.6603, @object
	.size	__PRETTY_FUNCTION__.6603, 23
__PRETTY_FUNCTION__.6603:
	.string	"session_pack_extension"
	.align 16
	.type	__PRETTY_FUNCTION__.6614, @object
	.size	__PRETTY_FUNCTION__.6614, 19
__PRETTY_FUNCTION__.6614:
	.string	"session_prep_frame"
	.align 32
	.type	__PRETTY_FUNCTION__.6767, @object
	.size	__PRETTY_FUNCTION__.6767, 34
__PRETTY_FUNCTION__.6767:
	.string	"nghttp2_session_mem_send_internal"
	.align 16
	.type	__PRETTY_FUNCTION__.6782, @object
	.size	__PRETTY_FUNCTION__.6782, 25
__PRETTY_FUNCTION__.6782:
	.string	"nghttp2_session_mem_send"
	.align 32
	.type	__PRETTY_FUNCTION__.6487, @object
	.size	__PRETTY_FUNCTION__.6487, 35
__PRETTY_FUNCTION__.6487:
	.string	"nghttp2_session_adjust_idle_stream"
	.align 32
	.type	__PRETTY_FUNCTION__.6476, @object
	.size	__PRETTY_FUNCTION__.6476, 37
__PRETTY_FUNCTION__.6476:
	.string	"nghttp2_session_adjust_closed_stream"
	.align 16
	.type	__PRETTY_FUNCTION__.6434, @object
	.size	__PRETTY_FUNCTION__.6434, 28
__PRETTY_FUNCTION__.6434:
	.string	"nghttp2_session_open_stream"
	.align 16
	.type	__PRETTY_FUNCTION__.6414, @object
	.size	__PRETTY_FUNCTION__.6414, 31
__PRETTY_FUNCTION__.6414:
	.string	"nghttp2_session_add_rst_stream"
	.align 32
	.type	__PRETTY_FUNCTION__.6384, @object
	.size	__PRETTY_FUNCTION__.6384, 36
__PRETTY_FUNCTION__.6384:
	.string	"nghttp2_session_reprioritize_stream"
	.globl	nghttp2_enable_strict_preface
	.data
	.align 4
	.type	nghttp2_enable_strict_preface, @object
	.size	nghttp2_enable_strict_preface, 4
nghttp2_enable_strict_preface:
	.long	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	65535
	.long	0
	.long	0
	.long	0
	.align 16
.LC4:
	.long	4096
	.long	1
	.long	-1
	.long	65535
	.align 16
.LC5:
	.long	4096
	.long	1
	.long	100
	.long	65535
	.align 16
.LC6:
	.quad	1000
	.quad	65536
	.align 16
.LC8:
	.long	2147483647
	.long	2147483647
	.long	65535
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
