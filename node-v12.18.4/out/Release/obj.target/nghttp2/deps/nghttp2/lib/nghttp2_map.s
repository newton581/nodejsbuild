	.file	"nghttp2_map.c"
	.text
	.p2align 4
	.globl	nghttp2_map_init
	.type	nghttp2_map_init, @function
nghttp2_map_init:
.LFB31:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, 8(%rbx)
	movl	$256, %esi
	movl	$256, 24(%rbx)
	call	nghttp2_mem_calloc@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L3
	movq	$0, 16(%rbx)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L1
	.cfi_endproc
.LFE31:
	.size	nghttp2_map_init, .-nghttp2_map_init
	.p2align 4
	.globl	nghttp2_map_free
	.type	nghttp2_map_free, @function
nghttp2_map_free:
.LFB32:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE32:
	.size	nghttp2_map_free, .-nghttp2_map_free
	.p2align 4
	.globl	nghttp2_map_each_free
	.type	nghttp2_map_each_free, @function
nghttp2_map_each_free:
.LFB33:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	24(%rdi), %esi
	movq	%rdi, -56(%rbp)
	testl	%esi, %esi
	je	.L7
	movq	(%rdi), %rcx
	movq	%rdx, %r12
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%r14d, %r13d
	salq	$3, %r13
	leaq	(%rcx,%r13), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r15, %rdi
	movq	(%r15), %r15
	movq	%r12, %rsi
	call	*%rbx
	testq	%r15, %r15
	jne	.L10
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rcx
	movl	24(%rdx), %esi
	leaq	(%rcx,%r13), %rax
.L9:
	addl	$1, %r14d
	movq	$0, (%rax)
	cmpl	%esi, %r14d
	jb	.L11
.L7:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33:
	.size	nghttp2_map_each_free, .-nghttp2_map_each_free
	.p2align 4
	.globl	nghttp2_map_each
	.type	nghttp2_map_each, @function
nghttp2_map_each:
.LFB34:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %edx
	testl	%edx, %edx
	je	.L24
.L20:
	movq	0(%r13), %rcx
	movl	%r14d, %eax
	movq	(%rcx,%rax,8), %r15
	testq	%r15, %r15
	jne	.L23
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L34
.L23:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*%r12
	testl	%eax, %eax
	je	.L35
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movl	24(%r13), %edx
.L22:
	addl	$1, %r14d
	cmpl	%edx, %r14d
	jb	.L20
.L24:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE34:
	.size	nghttp2_map_each, .-nghttp2_map_each
	.p2align 4
	.globl	nghttp2_map_entry_init
	.type	nghttp2_map_entry_init, @function
nghttp2_map_entry_init:
.LFB35:
	.cfi_startproc
	endbr64
	movl	%esi, 8(%rdi)
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE35:
	.size	nghttp2_map_entry_init, .-nghttp2_map_entry_init
	.p2align 4
	.globl	nghttp2_map_insert
	.type	nghttp2_map_insert, @function
nghttp2_map_insert:
.LFB39:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %edx
	movq	16(%rdi), %rax
	leal	(%rdx,%rdx), %r15d
	leaq	4(,%rax,4), %rcx
	leal	(%r15,%rdx), %eax
	cmpq	%rax, %rcx
	ja	.L38
	movq	(%rdi), %r14
	leal	-1(%rdx), %r13d
.L39:
	movl	8(%r12), %ecx
	movl	%ecx, %edx
	movl	%ecx, %eax
	shrl	$12, %eax
	shrl	$20, %edx
	xorl	%eax, %edx
	xorl	%ecx, %edx
	movl	%edx, %eax
	movl	%edx, %esi
	shrl	$4, %esi
	shrl	$7, %eax
	xorl	%esi, %eax
	xorl	%eax, %edx
	andl	%r13d, %edx
	leaq	(%r14,%rdx,8), %rsi
	movq	(%rsi), %rdx
	movq	%rdx, %rax
	testq	%rdx, %rdx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	8(%rax), %ecx
	je	.L53
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L48
	movq	%rdx, (%r12)
.L64:
	movq	%r12, (%rsi)
	xorl	%eax, %eax
	addq	$1, 16(%rbx)
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	%r15d, %esi
	movl	$8, %edx
	call	nghttp2_mem_calloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L50
	movl	24(%rbx), %eax
	movq	(%rbx), %r8
	leal	-1(%r15), %r13d
	testl	%eax, %eax
	je	.L44
	subl	$1, %eax
	movq	%r8, %rdi
	leaq	8(%r8,%rax,8), %r9
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdi), %rax
.L63:
	testq	%rax, %rax
	je	.L66
.L42:
	movq	%rax, %rcx
	movq	(%rax), %rax
	movl	8(%rcx), %esi
	movq	$0, (%rcx)
	movl	%esi, %edx
	movl	%esi, %r10d
	shrl	$12, %r10d
	shrl	$20, %edx
	xorl	%r10d, %edx
	xorl	%esi, %edx
	movl	%edx, %r10d
	movl	%edx, %r11d
	shrl	$4, %r11d
	shrl	$7, %r10d
	xorl	%r11d, %r10d
	xorl	%r10d, %edx
	andl	%r13d, %edx
	leaq	(%r14,%rdx,8), %r10
	movq	(%r10), %r11
	movq	%r11, %rdx
	testq	%r11, %r11
	je	.L65
.L45:
	cmpl	8(%rdx), %esi
	je	.L63
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L45
	movq	%r11, (%rcx)
.L65:
	movq	%rcx, (%r10)
	testq	%rax, %rax
	jne	.L42
.L66:
	addq	$8, %rdi
	cmpq	%r9, %rdi
	jne	.L43
.L44:
	movq	8(%rbx), %rdi
	movq	%r8, %rsi
	call	nghttp2_mem_free@PLT
	movl	%r15d, 24(%rbx)
	movq	%r14, (%rbx)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$8, %rsp
	movl	$-501, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L37
	.cfi_endproc
.LFE39:
	.size	nghttp2_map_insert, .-nghttp2_map_insert
	.p2align 4
	.globl	nghttp2_map_find
	.type	nghttp2_map_find, @function
nghttp2_map_find:
.LFB40:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movl	%esi, %edx
	shrl	$12, %edx
	shrl	$20, %eax
	xorl	%edx, %eax
	xorl	%esi, %eax
	movl	%eax, %ecx
	movl	%eax, %edx
	shrl	$4, %edx
	shrl	$7, %ecx
	xorl	%edx, %ecx
	movl	24(%rdi), %edx
	xorl	%ecx, %eax
	subl	$1, %edx
	andl	%edx, %eax
	movq	(%rdi), %rdx
	movq	(%rdx,%rax,8), %rax
	testq	%rax, %rax
	jne	.L69
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L67
.L69:
	cmpl	%esi, 8(%rax)
	jne	.L74
.L67:
	ret
	.cfi_endproc
.LFE40:
	.size	nghttp2_map_find, .-nghttp2_map_find
	.p2align 4
	.globl	nghttp2_map_remove
	.type	nghttp2_map_remove, @function
nghttp2_map_remove:
.LFB41:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movl	%esi, %edx
	shrl	$12, %edx
	shrl	$20, %eax
	xorl	%edx, %eax
	xorl	%esi, %eax
	movl	%eax, %ecx
	movl	%eax, %edx
	shrl	$4, %edx
	shrl	$7, %ecx
	xorl	%edx, %ecx
	movl	24(%rdi), %edx
	xorl	%ecx, %eax
	subl	$1, %edx
	andl	%edx, %eax
	movq	(%rdi), %rdx
	leaq	(%rdx,%rax,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L79
	movq	(%rax), %rdx
	cmpl	%esi, 8(%rax)
	je	.L77
.L81:
	movq	%rax, %rcx
	testq	%rdx, %rdx
	je	.L79
	movq	%rdx, %rax
	movq	(%rax), %rdx
	cmpl	%esi, 8(%rax)
	jne	.L81
.L77:
	movq	%rdx, (%rcx)
	xorl	%eax, %eax
	subq	$1, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$-501, %eax
	ret
	.cfi_endproc
.LFE41:
	.size	nghttp2_map_remove, .-nghttp2_map_remove
	.p2align 4
	.globl	nghttp2_map_size
	.type	nghttp2_map_size, @function
nghttp2_map_size:
.LFB42:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE42:
	.size	nghttp2_map_size, .-nghttp2_map_size
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
