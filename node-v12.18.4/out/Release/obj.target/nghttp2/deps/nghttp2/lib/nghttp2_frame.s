	.file	"nghttp2_frame.c"
	.text
	.p2align 4
	.type	nv_compar, @function
nv_compar:
.LFB120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rsi), %rdx
	movq	%rdi, %rbx
	movq	16(%rdi), %r8
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	cmpq	%r8, %rdx
	je	.L12
.L6:
	jbe	.L7
	movq	%r8, %rdx
	call	memcmp@PLT
	movl	$-1, %edx
	testl	%eax, %eax
	cmove	%edx, %eax
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	call	memcmp@PLT
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1
	movq	24(%r12), %rdx
	movq	24(%rbx), %r8
	movq	8(%r12), %rsi
	movq	8(%rbx), %rdi
	cmpq	%r8, %rdx
	jne	.L6
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	memcmp@PLT
	.cfi_endproc
.LFE120:
	.size	nv_compar, .-nv_compar
	.p2align 4
	.type	frame_pack_headers_shared.isra.0, @function
frame_pack_headers_shared.isra.0:
.LFB129:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	movq	%r13, %rdx
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rax
	pushq	%rbx
	shrq	$40, %rdx
	shrq	$32, %rax
	movl	%edx, %r14d
	andl	$-5, %r14d
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	%rax, -56(%rbp)
	movq	24(%rcx), %rbx
	movq	32(%rcx), %rsi
	leaq	-9(%rbx), %rdi
	subq	%rbx, %rsi
	cmpq	(%r15), %rcx
	movq	%rdi, 24(%rcx)
	cmove	%edx, %r14d
	sall	$8, %esi
	call	nghttp2_put_uint32be@PLT
	movzbl	-56(%rbp), %eax
	movb	%r14b, -5(%rbx)
	movl	%r13d, %esi
	leaq	-4(%rbx), %rdi
	movb	%al, -6(%rbx)
	call	nghttp2_put_uint32be@PLT
	movq	(%r12), %rax
	movq	(%r15), %r12
	cmpq	%r12, %rax
	je	.L15
	movq	(%rax), %r15
	movl	%r13d, %r14d
	cmpq	%r15, %r12
	je	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	movq	24(%r15), %rbx
	movq	32(%r15), %rsi
	leaq	-9(%rbx), %rdi
	subq	%rbx, %rsi
	movq	%rdi, 24(%r15)
	sall	$8, %esi
	call	nghttp2_put_uint32be@PLT
	movb	$9, -6(%rbx)
	leaq	-4(%rbx), %rdi
	movl	%r14d, %esi
	movb	$0, -5(%rbx)
	call	nghttp2_put_uint32be@PLT
	movq	(%r15), %r15
	cmpq	%r15, %r12
	jne	.L17
.L16:
	movq	24(%r15), %rbx
	movq	32(%r15), %rsi
	leaq	-9(%rbx), %rdi
	subq	%rbx, %rsi
	movq	%rdi, 24(%r15)
	sall	$8, %esi
	call	nghttp2_put_uint32be@PLT
	movb	$9, -6(%rbx)
	leaq	-4(%rbx), %rdi
	movl	%r13d, %esi
	movb	$4, -5(%rbx)
	call	nghttp2_put_uint32be@PLT
.L15:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE129:
	.size	frame_pack_headers_shared.isra.0, .-frame_pack_headers_shared.isra.0
	.p2align 4
	.globl	nghttp2_frame_pack_frame_hd
	.type	nghttp2_frame_pack_frame_hd, @function
nghttp2_frame_pack_frame_hd:
.LFB57:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rsi), %esi
	movq	%rdi, %rbx
	sall	$8, %esi
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%r12), %eax
	leaq	5(%rbx), %rdi
	movb	%al, 3(%rbx)
	movzbl	13(%r12), %eax
	movb	%al, 4(%rbx)
	movl	8(%r12), %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_put_uint32be@PLT
	.cfi_endproc
.LFE57:
	.size	nghttp2_frame_pack_frame_hd, .-nghttp2_frame_pack_frame_hd
	.p2align 4
	.globl	nghttp2_frame_unpack_frame_hd
	.type	nghttp2_frame_unpack_frame_hd, @function
nghttp2_frame_unpack_frame_hd:
.LFB58:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	nghttp2_get_uint32@PLT
	leaq	5(%rbx), %rdi
	shrl	$8, %eax
	movq	%rax, (%r12)
	movzbl	3(%rbx), %eax
	movb	%al, 12(%r12)
	movzbl	4(%rbx), %eax
	movb	%al, 13(%r12)
	call	nghttp2_get_uint32@PLT
	popq	%rbx
	movb	$0, 14(%r12)
	andl	$2147483647, %eax
	movl	%eax, 8(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE58:
	.size	nghttp2_frame_unpack_frame_hd, .-nghttp2_frame_unpack_frame_hd
	.p2align 4
	.globl	nghttp2_frame_hd_init
	.type	nghttp2_frame_hd_init, @function
nghttp2_frame_hd_init:
.LFB59:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movb	%dl, 12(%rdi)
	movb	%cl, 13(%rdi)
	movl	%r8d, 8(%rdi)
	movb	$0, 14(%rdi)
	ret
	.cfi_endproc
.LFE59:
	.size	nghttp2_frame_hd_init, .-nghttp2_frame_hd_init
	.p2align 4
	.globl	nghttp2_frame_headers_init
	.type	nghttp2_frame_headers_init, @function
nghttp2_frame_headers_init:
.LFB60:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	$0, (%rdi)
	movb	$1, 12(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movb	%sil, 13(%rdi)
	movq	16(%rbp), %rax
	movl	%edx, 8(%rdi)
	movb	$0, 14(%rdi)
	movq	$0, 16(%rdi)
	movq	%r9, 40(%rdi)
	movq	%rax, 48(%rdi)
	movl	%ecx, 56(%rdi)
	testq	%r8, %r8
	je	.L26
	movq	(%r8), %rax
	movq	%rax, 24(%rdi)
	movl	8(%r8), %eax
	movl	%eax, 32(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$24, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_priority_spec_default_init@PLT
	.cfi_endproc
.LFE60:
	.size	nghttp2_frame_headers_init, .-nghttp2_frame_headers_init
	.p2align 4
	.globl	nghttp2_frame_headers_free
	.type	nghttp2_frame_headers_free, @function
nghttp2_frame_headers_free:
.LFB61:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	40(%r8), %rsi
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE61:
	.size	nghttp2_frame_headers_free, .-nghttp2_frame_headers_free
	.p2align 4
	.globl	nghttp2_frame_priority_init
	.type	nghttp2_frame_priority_init, @function
nghttp2_frame_priority_init:
.LFB62:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	movb	$0, 14(%rdi)
	movw	%ax, 12(%rdi)
	movq	(%rdx), %rax
	movq	$5, (%rdi)
	movq	%rax, 16(%rdi)
	movl	8(%rdx), %eax
	movl	%esi, 8(%rdi)
	movl	%eax, 24(%rdi)
	ret
	.cfi_endproc
.LFE62:
	.size	nghttp2_frame_priority_init, .-nghttp2_frame_priority_init
	.p2align 4
	.globl	nghttp2_frame_priority_free
	.type	nghttp2_frame_priority_free, @function
nghttp2_frame_priority_free:
.LFB63:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE63:
	.size	nghttp2_frame_priority_free, .-nghttp2_frame_priority_free
	.p2align 4
	.globl	nghttp2_frame_rst_stream_init
	.type	nghttp2_frame_rst_stream_init, @function
nghttp2_frame_rst_stream_init:
.LFB64:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	movq	$4, (%rdi)
	movl	%esi, 8(%rdi)
	movw	%ax, 12(%rdi)
	movb	$0, 14(%rdi)
	movl	%edx, 16(%rdi)
	ret
	.cfi_endproc
.LFE64:
	.size	nghttp2_frame_rst_stream_init, .-nghttp2_frame_rst_stream_init
	.p2align 4
	.globl	nghttp2_frame_rst_stream_free
	.type	nghttp2_frame_rst_stream_free, @function
nghttp2_frame_rst_stream_free:
.LFB131:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE131:
	.size	nghttp2_frame_rst_stream_free, .-nghttp2_frame_rst_stream_free
	.p2align 4
	.globl	nghttp2_frame_settings_init
	.type	nghttp2_frame_settings_init, @function
nghttp2_frame_settings_init:
.LFB66:
	.cfi_startproc
	endbr64
	leaq	(%rcx,%rcx,2), %rax
	movb	$4, 12(%rdi)
	addq	%rax, %rax
	movb	%sil, 13(%rdi)
	movq	%rax, (%rdi)
	movl	$0, 8(%rdi)
	movb	$0, 14(%rdi)
	movq	%rcx, 16(%rdi)
	movq	%rdx, 24(%rdi)
	ret
	.cfi_endproc
.LFE66:
	.size	nghttp2_frame_settings_init, .-nghttp2_frame_settings_init
	.p2align 4
	.globl	nghttp2_frame_settings_free
	.type	nghttp2_frame_settings_free, @function
nghttp2_frame_settings_free:
.LFB67:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	24(%r8), %rsi
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE67:
	.size	nghttp2_frame_settings_free, .-nghttp2_frame_settings_free
	.p2align 4
	.globl	nghttp2_frame_push_promise_init
	.type	nghttp2_frame_push_promise_init, @function
nghttp2_frame_push_promise_init:
.LFB68:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movb	$5, 12(%rdi)
	movb	%sil, 13(%rdi)
	movl	%edx, 8(%rdi)
	movb	$0, 14(%rdi)
	movq	$0, 16(%rdi)
	movq	%r8, 24(%rdi)
	movq	%r9, 32(%rdi)
	movl	%ecx, 40(%rdi)
	movb	$0, 44(%rdi)
	ret
	.cfi_endproc
.LFE68:
	.size	nghttp2_frame_push_promise_init, .-nghttp2_frame_push_promise_init
	.p2align 4
	.globl	nghttp2_frame_push_promise_free
	.type	nghttp2_frame_push_promise_free, @function
nghttp2_frame_push_promise_free:
.LFB69:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	24(%r8), %rsi
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE69:
	.size	nghttp2_frame_push_promise_free, .-nghttp2_frame_push_promise_free
	.p2align 4
	.globl	nghttp2_frame_ping_init
	.type	nghttp2_frame_ping_init, @function
nghttp2_frame_ping_init:
.LFB70:
	.cfi_startproc
	endbr64
	movq	$8, (%rdi)
	movb	$6, 12(%rdi)
	movb	%sil, 13(%rdi)
	movl	$0, 8(%rdi)
	movb	$0, 14(%rdi)
	testq	%rdx, %rdx
	je	.L40
	movq	(%rdx), %rax
	movq	%rax, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE70:
	.size	nghttp2_frame_ping_init, .-nghttp2_frame_ping_init
	.p2align 4
	.globl	nghttp2_frame_ping_free
	.type	nghttp2_frame_ping_free, @function
nghttp2_frame_ping_free:
.LFB133:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE133:
	.size	nghttp2_frame_ping_free, .-nghttp2_frame_ping_free
	.p2align 4
	.globl	nghttp2_frame_goaway_init
	.type	nghttp2_frame_goaway_init, @function
nghttp2_frame_goaway_init:
.LFB72:
	.cfi_startproc
	endbr64
	leaq	8(%r8), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$7, %eax
	movw	%ax, 12(%rdi)
	movb	$0, 14(%rdi)
	movl	%esi, 16(%rdi)
	movl	%edx, 20(%rdi)
	movq	%rcx, 24(%rdi)
	movq	%r8, 32(%rdi)
	movb	$0, 40(%rdi)
	ret
	.cfi_endproc
.LFE72:
	.size	nghttp2_frame_goaway_init, .-nghttp2_frame_goaway_init
	.p2align 4
	.globl	nghttp2_frame_goaway_free
	.type	nghttp2_frame_goaway_free, @function
nghttp2_frame_goaway_free:
.LFB73:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	24(%r8), %rsi
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE73:
	.size	nghttp2_frame_goaway_free, .-nghttp2_frame_goaway_free
	.p2align 4
	.globl	nghttp2_frame_window_update_init
	.type	nghttp2_frame_window_update_init, @function
nghttp2_frame_window_update_init:
.LFB74:
	.cfi_startproc
	endbr64
	movq	$4, (%rdi)
	movb	$8, 12(%rdi)
	movb	%sil, 13(%rdi)
	movl	%edx, 8(%rdi)
	movb	$0, 14(%rdi)
	movl	%ecx, 16(%rdi)
	movb	$0, 20(%rdi)
	ret
	.cfi_endproc
.LFE74:
	.size	nghttp2_frame_window_update_init, .-nghttp2_frame_window_update_init
	.p2align 4
	.globl	nghttp2_frame_window_update_free
	.type	nghttp2_frame_window_update_free, @function
nghttp2_frame_window_update_free:
.LFB135:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE135:
	.size	nghttp2_frame_window_update_free, .-nghttp2_frame_window_update_free
	.p2align 4
	.globl	nghttp2_frame_trail_padlen
	.type	nghttp2_frame_trail_padlen, @function
nghttp2_frame_trail_padlen:
.LFB76:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L47
	movzbl	13(%rdi), %eax
	shrb	$3, %al
	andl	$1, %eax
	subq	%rax, %rsi
	movq	%rsi, %rax
.L47:
	ret
	.cfi_endproc
.LFE76:
	.size	nghttp2_frame_trail_padlen, .-nghttp2_frame_trail_padlen
	.p2align 4
	.globl	nghttp2_frame_data_init
	.type	nghttp2_frame_data_init, @function
nghttp2_frame_data_init:
.LFB77:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movb	$0, 12(%rdi)
	movb	%sil, 13(%rdi)
	movl	%edx, 8(%rdi)
	movb	$0, 14(%rdi)
	movq	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE77:
	.size	nghttp2_frame_data_init, .-nghttp2_frame_data_init
	.p2align 4
	.globl	nghttp2_frame_data_free
	.type	nghttp2_frame_data_free, @function
nghttp2_frame_data_free:
.LFB139:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE139:
	.size	nghttp2_frame_data_free, .-nghttp2_frame_data_free
	.p2align 4
	.globl	nghttp2_frame_extension_init
	.type	nghttp2_frame_extension_init, @function
nghttp2_frame_extension_init:
.LFB79:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movb	%sil, 12(%rdi)
	movb	%dl, 13(%rdi)
	movl	%ecx, 8(%rdi)
	movb	$0, 14(%rdi)
	movq	%r8, 16(%rdi)
	ret
	.cfi_endproc
.LFE79:
	.size	nghttp2_frame_extension_init, .-nghttp2_frame_extension_init
	.p2align 4
	.globl	nghttp2_frame_extension_free
	.type	nghttp2_frame_extension_free, @function
nghttp2_frame_extension_free:
.LFB137:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE137:
	.size	nghttp2_frame_extension_free, .-nghttp2_frame_extension_free
	.p2align 4
	.globl	nghttp2_frame_altsvc_init
	.type	nghttp2_frame_altsvc_init, @function
nghttp2_frame_altsvc_init:
.LFB81:
	.cfi_startproc
	endbr64
	leaq	2(%rcx,%r9), %rax
	movl	%esi, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$10, %eax
	movw	%ax, 12(%rdi)
	movq	16(%rdi), %rax
	movb	$0, 14(%rdi)
	movq	%rdx, (%rax)
	movq	%rcx, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%r9, 24(%rax)
	ret
	.cfi_endproc
.LFE81:
	.size	nghttp2_frame_altsvc_init, .-nghttp2_frame_altsvc_init
	.p2align 4
	.globl	nghttp2_frame_altsvc_free
	.type	nghttp2_frame_altsvc_free, @function
nghttp2_frame_altsvc_free:
.LFB82:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L56
	movq	(%rax), %rsi
	jmp	nghttp2_mem_free@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	ret
	.cfi_endproc
.LFE82:
	.size	nghttp2_frame_altsvc_free, .-nghttp2_frame_altsvc_free
	.p2align 4
	.globl	nghttp2_frame_origin_init
	.type	nghttp2_frame_origin_init, @function
nghttp2_frame_origin_init:
.LFB83:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L64
	leaq	-1(%rdx), %rcx
	cmpq	$3, %rcx
	jbe	.L65
	movq	%rcx, %r8
	movdqa	.LC0(%rip), %xmm2
	movq	%rsi, %rax
	pxor	%xmm1, %xmm1
	shrq	%r8
	salq	$5, %r8
	addq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L61:
	movdqu	8(%rax), %xmm0
	movdqu	24(%rax), %xmm3
	addq	$32, %rax
	punpcklqdq	%xmm3, %xmm0
	paddq	%xmm2, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%r8, %rax
	jne	.L61
	movdqa	%xmm1, %xmm0
	andq	$-2, %rcx
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rax
.L60:
	movq	%rcx, %r8
	salq	$4, %r8
	movq	8(%rsi,%r8), %r8
	leaq	2(%rax,%r8), %rax
	leaq	1(%rcx), %r8
	cmpq	%r8, %rdx
	jbe	.L59
	salq	$4, %r8
	movq	8(%rsi,%r8), %r8
	leaq	2(%rax,%r8), %rax
	leaq	2(%rcx), %r8
	cmpq	%r8, %rdx
	jbe	.L59
	salq	$4, %r8
	addq	$3, %rcx
	movq	8(%rsi,%r8), %r8
	leaq	2(%rax,%r8), %rax
	cmpq	%rcx, %rdx
	jbe	.L59
	salq	$4, %rcx
	movq	8(%rsi,%rcx), %rcx
	leaq	2(%rax,%rcx), %rax
.L59:
	movq	%rax, (%rdi)
	movl	$12, %eax
	movw	%ax, 12(%rdi)
	movq	16(%rdi), %rax
	movl	$0, 8(%rdi)
	movb	$0, 14(%rdi)
	movq	%rsi, 8(%rax)
	movq	%rdx, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%eax, %eax
	jmp	.L59
.L65:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L60
	.cfi_endproc
.LFE83:
	.size	nghttp2_frame_origin_init, .-nghttp2_frame_origin_init
	.p2align 4
	.globl	nghttp2_frame_origin_free
	.type	nghttp2_frame_origin_free, @function
nghttp2_frame_origin_free:
.LFB84:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	16(%r8), %rax
	testq	%rax, %rax
	je	.L67
	movq	8(%rax), %rsi
	jmp	nghttp2_mem_free@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	ret
	.cfi_endproc
.LFE84:
	.size	nghttp2_frame_origin_free, .-nghttp2_frame_origin_free
	.p2align 4
	.globl	nghttp2_frame_priority_len
	.type	nghttp2_frame_priority_len, @function
nghttp2_frame_priority_len:
.LFB85:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	salq	$58, %rax
	sarq	$63, %rax
	andl	$5, %eax
	ret
	.cfi_endproc
.LFE85:
	.size	nghttp2_frame_priority_len, .-nghttp2_frame_priority_len
	.p2align 4
	.globl	nghttp2_frame_headers_payload_nv_offset
	.type	nghttp2_frame_headers_payload_nv_offset, @function
nghttp2_frame_headers_payload_nv_offset:
.LFB86:
	.cfi_startproc
	endbr64
	movzbl	13(%rdi), %eax
	salq	$58, %rax
	sarq	$63, %rax
	andl	$5, %eax
	ret
	.cfi_endproc
.LFE86:
	.size	nghttp2_frame_headers_payload_nv_offset, .-nghttp2_frame_headers_payload_nv_offset
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/nghttp2/lib/nghttp2_frame.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"bufs->head == bufs->cur"
	.text
	.p2align 4
	.globl	nghttp2_frame_pack_headers
	.type	nghttp2_frame_pack_headers, @function
nghttp2_frame_pack_headers:
.LFB88:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	cmpq	%r14, (%rdi)
	jne	.L87
	movzbl	13(%rsi), %eax
	movq	48(%rsi), %rcx
	movq	%rdx, %rdi
	movq	%rsi, %rbx
	movq	40(%rsi), %rdx
	movq	%r12, %rsi
	andl	$32, %eax
	cmpb	$1, %al
	sbbq	%r13, %r13
	notq	%r13
	andq	$-5, %r13
	cmpb	$1, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$5, %eax
	addq	24(%r14), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%r14)
	call	nghttp2_hd_deflate_hd_bufs@PLT
	cmpl	$-502, %eax
	je	.L88
	addq	24(%r14), %r13
	movq	%r13, 24(%r14)
	testl	%eax, %eax
	jne	.L75
	testb	$32, 13(%rbx)
	jne	.L89
.L80:
	movq	$0, 16(%rbx)
	movq	%r12, %rdi
	call	nghttp2_bufs_len@PLT
	movq	8(%rbx), %rdx
	leaq	8(%r12), %rsi
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	movq	(%rbx), %rax
	movq	%rdx, %rcx
	popq	%rbx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	frame_pack_headers_shared.isra.0
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	24(%rbx), %esi
	movq	%r13, %rdi
	call	nghttp2_put_uint32be@PLT
	cmpb	$0, 32(%rbx)
	je	.L81
	orb	$-128, 0(%r13)
.L81:
	movzbl	28(%rbx), %eax
	subl	$1, %eax
	movb	%al, 4(%r13)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L88:
	addq	%r13, 24(%r14)
	movl	$-523, %eax
.L75:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5691(%rip), %rcx
	movl	$342, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE88:
	.size	nghttp2_frame_pack_headers, .-nghttp2_frame_pack_headers
	.p2align 4
	.globl	nghttp2_frame_pack_priority_spec
	.type	nghttp2_frame_pack_priority_spec, @function
nghttp2_frame_pack_priority_spec:
.LFB89:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rsi), %esi
	movq	%rdi, %rbx
	call	nghttp2_put_uint32be@PLT
	cmpb	$0, 8(%r12)
	je	.L91
	orb	$-128, (%rbx)
.L91:
	movl	4(%r12), %eax
	subl	$1, %eax
	movb	%al, 4(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE89:
	.size	nghttp2_frame_pack_priority_spec, .-nghttp2_frame_pack_priority_spec
	.p2align 4
	.globl	nghttp2_frame_unpack_priority_spec
	.type	nghttp2_frame_unpack_priority_spec, @function
nghttp2_frame_unpack_priority_spec:
.LFB90:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	nghttp2_get_uint32@PLT
	movsbl	(%rbx), %ecx
	movzbl	4(%rbx), %edx
	movq	%r12, %rdi
	andl	$2147483647, %eax
	popq	%rbx
	popq	%r12
	shrl	$31, %ecx
	addl	$1, %edx
	movl	%eax, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_priority_spec_init@PLT
	.cfi_endproc
.LFE90:
	.size	nghttp2_frame_unpack_priority_spec, .-nghttp2_frame_unpack_priority_spec
	.p2align 4
	.globl	nghttp2_frame_unpack_headers_payload
	.type	nghttp2_frame_unpack_headers_payload, @function
nghttp2_frame_unpack_headers_payload:
.LFB91:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testb	$32, 13(%rdi)
	je	.L96
	movq	%rsi, %r12
	movq	%rsi, %rdi
	call	nghttp2_get_uint32@PLT
	movsbl	(%r12), %ecx
	movzbl	4(%r12), %edx
	movq	%r13, %rdi
	andl	$2147483647, %eax
	shrl	$31, %ecx
	addl	$1, %edx
	movl	%eax, %esi
	call	nghttp2_priority_spec_init@PLT
.L97:
	movq	$0, 40(%rbx)
	xorl	%eax, %eax
	movq	$0, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%r13, %rdi
	call	nghttp2_priority_spec_default_init@PLT
	jmp	.L97
	.cfi_endproc
.LFE91:
	.size	nghttp2_frame_unpack_headers_payload, .-nghttp2_frame_unpack_headers_payload
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"nghttp2_buf_avail(buf) >= NGHTTP2_PRIORITY_SPECLEN"
	.text
	.p2align 4
	.globl	nghttp2_frame_pack_priority
	.type	nghttp2_frame_pack_priority, @function
nghttp2_frame_pack_priority:
.LFB92:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r13
	movq	8(%rdi), %rax
	cmpq	%rax, %r13
	jne	.L104
	movq	16(%r13), %rax
	subq	32(%r13), %rax
	cmpq	$4, %rax
	jbe	.L105
	movq	24(%r13), %r12
	movq	%rsi, %rbx
	movl	(%rsi), %esi
	leaq	-9(%r12), %rdi
	sall	$8, %esi
	movq	%rdi, 24(%r13)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%rbx), %eax
	leaq	-4(%r12), %rdi
	movb	%al, -6(%r12)
	movzbl	13(%rbx), %eax
	movb	%al, -5(%r12)
	movl	8(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movq	32(%r13), %r12
	movl	16(%rbx), %esi
	movq	%r12, %rdi
	call	nghttp2_put_uint32be@PLT
	cmpb	$0, 24(%rbx)
	je	.L102
	orb	$-128, (%r12)
.L102:
	movl	20(%rbx), %eax
	subl	$1, %eax
	movb	%al, 4(%r12)
	xorl	%eax, %eax
	addq	$5, 32(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5712(%rip), %rcx
	movl	$417, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	__assert_fail@PLT
.L104:
	leaq	__PRETTY_FUNCTION__.5712(%rip), %rcx
	movl	$413, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE92:
	.size	nghttp2_frame_pack_priority, .-nghttp2_frame_pack_priority
	.p2align 4
	.globl	nghttp2_frame_unpack_priority_payload
	.type	nghttp2_frame_unpack_priority_payload, @function
nghttp2_frame_unpack_priority_payload:
.LFB93:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	nghttp2_get_uint32@PLT
	movsbl	(%r12), %ecx
	movzbl	4(%r12), %edx
	leaq	16(%rbx), %rdi
	andl	$2147483647, %eax
	popq	%rbx
	popq	%r12
	shrl	$31, %ecx
	addl	$1, %edx
	movl	%eax, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_priority_spec_init@PLT
	.cfi_endproc
.LFE93:
	.size	nghttp2_frame_unpack_priority_payload, .-nghttp2_frame_unpack_priority_payload
	.section	.rodata.str1.1
.LC4:
	.string	"nghttp2_buf_avail(buf) >= 4"
	.text
	.p2align 4
	.globl	nghttp2_frame_pack_rst_stream
	.type	nghttp2_frame_pack_rst_stream, @function
nghttp2_frame_pack_rst_stream:
.LFB94:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	8(%rdi), %rax
	cmpq	%rax, %r12
	jne	.L112
	movq	16(%r12), %rax
	subq	32(%r12), %rax
	cmpq	$3, %rax
	jbe	.L113
	movq	24(%r12), %r13
	movq	%rsi, %rbx
	movl	(%rsi), %esi
	leaq	-9(%r13), %rdi
	sall	$8, %esi
	movq	%rdi, 24(%r12)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%rbx), %eax
	leaq	-4(%r13), %rdi
	movb	%al, -6(%r13)
	movzbl	13(%rbx), %eax
	movb	%al, -5(%r13)
	movl	8(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movl	16(%rbx), %esi
	movq	32(%r12), %rdi
	call	nghttp2_put_uint32be@PLT
	addq	$4, 32(%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L112:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5722(%rip), %rcx
	movl	$439, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
.L113:
	leaq	__PRETTY_FUNCTION__.5722(%rip), %rcx
	movl	$443, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE94:
	.size	nghttp2_frame_pack_rst_stream, .-nghttp2_frame_pack_rst_stream
	.p2align 4
	.globl	nghttp2_frame_unpack_rst_stream_payload
	.type	nghttp2_frame_unpack_rst_stream_payload, @function
nghttp2_frame_unpack_rst_stream_payload:
.LFB95:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	nghttp2_get_uint32@PLT
	movl	%eax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE95:
	.size	nghttp2_frame_unpack_rst_stream_payload, .-nghttp2_frame_unpack_rst_stream_payload
	.p2align 4
	.globl	nghttp2_frame_pack_settings
	.type	nghttp2_frame_pack_settings, @function
nghttp2_frame_pack_settings:
.LFB96:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	8(%rdi), %rax
	cmpq	%rax, %r12
	jne	.L127
	movq	%rsi, %rbx
	movq	(%rsi), %rsi
	movq	16(%r12), %rax
	subq	32(%r12), %rax
	cmpq	%rsi, %rax
	jb	.L121
	movq	24(%r12), %r13
	sall	$8, %esi
	leaq	-9(%r13), %rdi
	movq	%rdi, 24(%r12)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%rbx), %eax
	leaq	-4(%r13), %rdi
	movb	%al, -6(%r13)
	movzbl	13(%rbx), %eax
	movb	%al, -5(%r13)
	movl	8(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movq	16(%rbx), %r14
	movq	24(%rbx), %rax
	movq	32(%r12), %rbx
	testq	%r14, %r14
	je	.L119
	leaq	4(%rax), %r13
	leaq	4(%rax,%r14,8), %r15
	.p2align 4,,10
	.p2align 3
.L120:
	movzwl	-4(%r13), %esi
	movq	%rbx, %rdi
	addq	$8, %r13
	call	nghttp2_put_uint16be@PLT
	movl	-8(%r13), %esi
	leaq	2(%rbx), %rdi
	addq	$6, %rbx
	call	nghttp2_put_uint32be@PLT
	cmpq	%r15, %r13
	jne	.L120
	movq	32(%r12), %rbx
.L119:
	leaq	(%r14,%r14,2), %rax
	leaq	(%rbx,%rax,2), %rax
	movq	%rax, 32(%r12)
	xorl	%eax, %eax
.L116:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$-522, %eax
	jmp	.L116
.L127:
	leaq	__PRETTY_FUNCTION__.5732(%rip), %rcx
	movl	$463, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE96:
	.size	nghttp2_frame_pack_settings, .-nghttp2_frame_pack_settings
	.p2align 4
	.globl	nghttp2_frame_pack_settings_payload
	.type	nghttp2_frame_pack_settings_payload, @function
nghttp2_frame_pack_settings_payload:
.LFB97:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdx, %rdx
	je	.L129
	leaq	4(%rsi), %rbx
	movq	%rdi, %r12
	leaq	(%rbx,%rdx,8), %r14
	.p2align 4,,10
	.p2align 3
.L130:
	movzwl	-4(%rbx), %esi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	nghttp2_put_uint16be@PLT
	movl	-8(%rbx), %esi
	leaq	2(%r12), %rdi
	addq	$6, %r12
	call	nghttp2_put_uint32be@PLT
	cmpq	%r14, %rbx
	jne	.L130
.L129:
	leaq	0(%r13,%r13,2), %rax
	popq	%rbx
	popq	%r12
	addq	%rax, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE97:
	.size	nghttp2_frame_pack_settings_payload, .-nghttp2_frame_pack_settings_payload
	.p2align 4
	.globl	nghttp2_frame_unpack_settings_payload
	.type	nghttp2_frame_unpack_settings_payload, @function
nghttp2_frame_unpack_settings_payload:
.LFB98:
	.cfi_startproc
	endbr64
	movq	%rsi, 24(%rdi)
	movq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE98:
	.size	nghttp2_frame_unpack_settings_payload, .-nghttp2_frame_unpack_settings_payload
	.p2align 4
	.globl	nghttp2_frame_unpack_settings_entry
	.type	nghttp2_frame_unpack_settings_entry, @function
nghttp2_frame_unpack_settings_entry:
.LFB99:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	nghttp2_get_uint16@PLT
	leaq	2(%rbx), %rdi
	movzwl	%ax, %eax
	movl	%eax, (%r12)
	call	nghttp2_get_uint32@PLT
	popq	%rbx
	movl	%eax, 4(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE99:
	.size	nghttp2_frame_unpack_settings_entry, .-nghttp2_frame_unpack_settings_entry
	.p2align 4
	.globl	nghttp2_frame_unpack_settings_payload2
	.type	nghttp2_frame_unpack_settings_payload2, @function
nghttp2_frame_unpack_settings_payload2:
.LFB100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movabsq	$-6148914691236517205, %rdx
	mulq	%rdx
	subq	$8, %rsp
	shrq	$2, %rdx
	movq	%rdx, (%rsi)
	cmpq	$5, %rcx
	ja	.L140
	movq	$0, (%rdi)
	xorl	%eax, %eax
.L139:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%r8, %rdi
	leaq	0(,%rdx,8), %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L144
	cmpq	$0, 0(%r13)
	je	.L142
	xorl	%r12d, %r12d
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%r14), %rax
.L143:
	movq	%rbx, %rdi
	leaq	(%rax,%r12,8), %r15
	addq	$1, %r12
	call	nghttp2_get_uint16@PLT
	leaq	2(%rbx), %rdi
	addq	$6, %rbx
	movzwl	%ax, %eax
	movl	%eax, (%r15)
	call	nghttp2_get_uint32@PLT
	movl	%eax, 4(%r15)
	cmpq	%r12, 0(%r13)
	ja	.L146
.L142:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L144:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L139
	.cfi_endproc
.LFE100:
	.size	nghttp2_frame_unpack_settings_payload2, .-nghttp2_frame_unpack_settings_payload2
	.p2align 4
	.globl	nghttp2_frame_pack_push_promise
	.type	nghttp2_frame_pack_push_promise, @function
nghttp2_frame_pack_push_promise:
.LFB101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	cmpq	%r13, (%rdi)
	jne	.L152
	movq	24(%r13), %rax
	movq	32(%rsi), %rcx
	movq	%rdx, %rdi
	movq	%rsi, %rbx
	movq	24(%rsi), %rdx
	movq	%r12, %rsi
	addq	$4, %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%r13)
	call	nghttp2_hd_deflate_hd_bufs@PLT
	cmpl	$-502, %eax
	je	.L153
	movq	24(%r13), %rcx
	leaq	-4(%rcx), %rdi
	movq	%rdi, 24(%r13)
	testl	%eax, %eax
	jne	.L147
	movl	40(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movq	$0, 16(%rbx)
	movq	%r12, %rdi
	call	nghttp2_bufs_len@PLT
	movq	8(%rbx), %rdx
	leaq	8(%r12), %rsi
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	movq	(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	movq	%rdx, %rcx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	frame_pack_headers_shared.isra.0
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	subq	$4, 24(%r13)
	movl	$-523, %eax
.L147:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L152:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5771(%rip), %rcx
	movl	$542, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE101:
	.size	nghttp2_frame_pack_push_promise, .-nghttp2_frame_pack_push_promise
	.p2align 4
	.globl	nghttp2_frame_unpack_push_promise_payload
	.type	nghttp2_frame_unpack_push_promise_payload, @function
nghttp2_frame_unpack_push_promise_payload:
.LFB102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	nghttp2_get_uint32@PLT
	movq	$0, 24(%rbx)
	andl	$2147483647, %eax
	movq	$0, 32(%rbx)
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE102:
	.size	nghttp2_frame_unpack_push_promise_payload, .-nghttp2_frame_unpack_push_promise_payload
	.section	.rodata.str1.1
.LC5:
	.string	"nghttp2_buf_avail(buf) >= 8"
	.text
	.p2align 4
	.globl	nghttp2_frame_pack_ping
	.type	nghttp2_frame_pack_ping, @function
nghttp2_frame_pack_ping:
.LFB103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	8(%rdi), %rax
	cmpq	%rax, %r12
	jne	.L160
	movq	16(%r12), %rax
	subq	32(%r12), %rax
	cmpq	$7, %rax
	jbe	.L161
	movq	24(%r12), %r13
	movq	%rsi, %rbx
	movl	(%rsi), %esi
	leaq	-9(%r13), %rdi
	sall	$8, %esi
	movq	%rdi, 24(%r12)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%rbx), %eax
	leaq	-4(%r13), %rdi
	movb	%al, -6(%r13)
	movzbl	13(%rbx), %eax
	movb	%al, -5(%r13)
	movl	8(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movq	32(%r12), %rdi
	leaq	16(%rbx), %rsi
	movl	$8, %edx
	call	nghttp2_cpymem@PLT
	movq	%rax, 32(%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L160:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5781(%rip), %rcx
	movl	$582, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
.L161:
	leaq	__PRETTY_FUNCTION__.5781(%rip), %rcx
	movl	$586, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE103:
	.size	nghttp2_frame_pack_ping, .-nghttp2_frame_pack_ping
	.p2align 4
	.globl	nghttp2_frame_unpack_ping_payload
	.type	nghttp2_frame_unpack_ping_payload, @function
nghttp2_frame_unpack_ping_payload:
.LFB104:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE104:
	.size	nghttp2_frame_unpack_ping_payload, .-nghttp2_frame_unpack_ping_payload
	.p2align 4
	.globl	nghttp2_frame_pack_goaway
	.type	nghttp2_frame_pack_goaway, @function
nghttp2_frame_pack_goaway:
.LFB105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	cmpq	8(%rdi), %r12
	jne	.L167
	movq	24(%r12), %r13
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movl	(%rsi), %esi
	leaq	-9(%r13), %rdi
	sall	$8, %esi
	movq	%rdi, 24(%r12)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%rbx), %eax
	leaq	-4(%r13), %rdi
	movb	%al, -6(%r13)
	movzbl	13(%rbx), %eax
	movb	%al, -5(%r13)
	movl	8(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movl	16(%rbx), %esi
	movq	32(%r12), %rdi
	call	nghttp2_put_uint32be@PLT
	movq	32(%r12), %rax
	movl	20(%rbx), %esi
	leaq	4(%rax), %rdi
	movq	%rdi, 32(%r12)
	call	nghttp2_put_uint32be@PLT
	movq	32(%rbx), %rdx
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	addq	$4, 32(%r12)
	call	nghttp2_bufs_add@PLT
	cmpl	$-502, %eax
	je	.L168
.L163:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	movl	$-522, %eax
	jmp	.L163
.L167:
	leaq	__PRETTY_FUNCTION__.5792(%rip), %rcx
	movl	$607, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE105:
	.size	nghttp2_frame_pack_goaway, .-nghttp2_frame_pack_goaway
	.p2align 4
	.globl	nghttp2_frame_unpack_goaway_payload
	.type	nghttp2_frame_unpack_goaway_payload, @function
nghttp2_frame_unpack_goaway_payload:
.LFB106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	nghttp2_get_uint32@PLT
	leaq	4(%r12), %rdi
	andl	$2147483647, %eax
	movl	%eax, 16(%rbx)
	call	nghttp2_get_uint32@PLT
	movq	%r14, 24(%rbx)
	movq	%r13, 32(%rbx)
	movl	%eax, 20(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE106:
	.size	nghttp2_frame_unpack_goaway_payload, .-nghttp2_frame_unpack_goaway_payload
	.p2align 4
	.globl	nghttp2_frame_unpack_goaway_payload2
	.type	nghttp2_frame_unpack_goaway_payload2, @function
nghttp2_frame_unpack_goaway_payload2:
.LFB107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$8, %rdx
	jbe	.L174
	leaq	-8(%rdx), %rbx
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L175
	leaq	8(%r12), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L174:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
.L172:
	movq	%r12, %rdi
	call	nghttp2_get_uint32@PLT
	leaq	4(%r12), %rdi
	andl	$2147483647, %eax
	movl	%eax, 16(%r13)
	call	nghttp2_get_uint32@PLT
	movq	%r14, 24(%r13)
	movq	%rbx, 32(%r13)
	movl	%eax, 20(%r13)
	xorl	%eax, %eax
.L171:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L175:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L171
	.cfi_endproc
.LFE107:
	.size	nghttp2_frame_unpack_goaway_payload2, .-nghttp2_frame_unpack_goaway_payload2
	.p2align 4
	.globl	nghttp2_frame_pack_window_update
	.type	nghttp2_frame_pack_window_update, @function
nghttp2_frame_pack_window_update:
.LFB108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	8(%rdi), %rax
	cmpq	%rax, %r12
	jne	.L181
	movq	16(%r12), %rax
	subq	32(%r12), %rax
	cmpq	$3, %rax
	jbe	.L182
	movq	24(%r12), %r13
	movq	%rsi, %rbx
	movl	(%rsi), %esi
	leaq	-9(%r13), %rdi
	sall	$8, %esi
	movq	%rdi, 24(%r12)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%rbx), %eax
	leaq	-4(%r13), %rdi
	movb	%al, -6(%r13)
	movzbl	13(%rbx), %eax
	movb	%al, -5(%r13)
	movl	8(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movl	16(%rbx), %esi
	movq	32(%r12), %rdi
	call	nghttp2_put_uint32be@PLT
	addq	$4, 32(%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L181:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5812(%rip), %rcx
	movl	$681, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
.L182:
	leaq	__PRETTY_FUNCTION__.5812(%rip), %rcx
	movl	$685, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE108:
	.size	nghttp2_frame_pack_window_update, .-nghttp2_frame_pack_window_update
	.p2align 4
	.globl	nghttp2_frame_unpack_window_update_payload
	.type	nghttp2_frame_unpack_window_update_payload, @function
nghttp2_frame_unpack_window_update_payload:
.LFB109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	nghttp2_get_uint32@PLT
	andl	$2147483647, %eax
	movl	%eax, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE109:
	.size	nghttp2_frame_unpack_window_update_payload, .-nghttp2_frame_unpack_window_update_payload
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"nghttp2_buf_avail(buf) >= 2 + altsvc->origin_len + altsvc->field_value_len"
	.section	.rodata.str1.1
.LC7:
	.string	"rv == 0"
	.text
	.p2align 4
	.globl	nghttp2_frame_pack_altsvc
	.type	nghttp2_frame_pack_altsvc, @function
nghttp2_frame_pack_altsvc:
.LFB110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %r12
	movq	(%rdi), %r13
	movq	24(%r12), %rax
	addq	8(%r12), %rax
	movq	16(%r13), %rdx
	addq	$2, %rax
	subq	32(%r13), %rdx
	cmpq	%rax, %rdx
	jb	.L190
	movq	24(%r13), %r14
	movq	%rdi, %r15
	movq	%rsi, %rbx
	movl	(%rsi), %esi
	leaq	-9(%r14), %rdi
	sall	$8, %esi
	movq	%rdi, 24(%r13)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%rbx), %eax
	leaq	-4(%r14), %rdi
	movb	%al, -6(%r14)
	movzbl	13(%rbx), %eax
	movb	%al, -5(%r14)
	movl	8(%rbx), %esi
	call	nghttp2_put_uint32be@PLT
	movzwl	8(%r12), %esi
	movq	32(%r13), %rdi
	call	nghttp2_put_uint16be@PLT
	addq	$2, 32(%r13)
	movq	8(%r12), %rdx
	movq	%r15, %rdi
	movq	(%r12), %rsi
	call	nghttp2_bufs_add@PLT
	testl	%eax, %eax
	jne	.L191
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movq	%r15, %rdi
	call	nghttp2_bufs_add@PLT
	testl	%eax, %eax
	jne	.L192
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5824(%rip), %rcx
	movl	$715, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	__assert_fail@PLT
.L192:
	leaq	__PRETTY_FUNCTION__.5824(%rip), %rcx
	movl	$731, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	__assert_fail@PLT
.L191:
	leaq	__PRETTY_FUNCTION__.5824(%rip), %rcx
	movl	$727, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE110:
	.size	nghttp2_frame_pack_altsvc, .-nghttp2_frame_pack_altsvc
	.p2align 4
	.globl	nghttp2_frame_unpack_altsvc_payload
	.type	nghttp2_frame_unpack_altsvc_payload, @function
nghttp2_frame_unpack_altsvc_payload:
.LFB111:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	subq	%rsi, %rcx
	movq	%rdx, (%rax)
	addq	%rsi, %rdx
	movq	%rsi, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rcx, 24(%rax)
	ret
	.cfi_endproc
.LFE111:
	.size	nghttp2_frame_unpack_altsvc_payload, .-nghttp2_frame_unpack_altsvc_payload
	.p2align 4
	.globl	nghttp2_frame_unpack_altsvc_payload2
	.type	nghttp2_frame_unpack_altsvc_payload2, @function
nghttp2_frame_unpack_altsvc_payload2:
.LFB112:
	.cfi_startproc
	endbr64
	movl	$6, %eax
	cmpq	$1, %rdx
	jbe	.L199
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$2, %rbx
	subq	$8, %rsp
	call	nghttp2_get_uint16@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movzwl	%ax, %r14d
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L197
	movq	%rbx, %rdx
	leaq	2(%r15), %rsi
	movq	%rax, %rdi
	subq	%r14, %rbx
	call	nghttp2_cpymem@PLT
	movq	16(%r13), %rax
	movq	%r12, (%rax)
	addq	%r14, %r12
	movq	%r14, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rbx, 24(%rax)
	xorl	%eax, %eax
.L194:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L197:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$-901, %eax
	jmp	.L194
	.cfi_endproc
.LFE112:
	.size	nghttp2_frame_unpack_altsvc_payload2, .-nghttp2_frame_unpack_altsvc_payload2
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"nghttp2_buf_len(buf) == NGHTTP2_FRAME_HDLEN + frame->hd.length"
	.text
	.p2align 4
	.globl	nghttp2_frame_pack_origin
	.type	nghttp2_frame_pack_origin, @function
nghttp2_frame_pack_origin:
.LFB113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	16(%rsi), %r15
	movq	(%rsi), %rsi
	movq	16(%r14), %rax
	subq	32(%r14), %rax
	cmpq	%rsi, %rax
	jb	.L207
	movq	24(%r14), %rbx
	sall	$8, %esi
	leaq	-9(%rbx), %rdi
	movq	%rdi, 24(%r14)
	call	nghttp2_put_uint32be@PLT
	movzbl	12(%r12), %eax
	leaq	-4(%rbx), %rdi
	movb	%al, -6(%rbx)
	movzbl	13(%r12), %eax
	movb	%al, -5(%rbx)
	movl	8(%r12), %esi
	call	nghttp2_put_uint32be@PLT
	cmpq	$0, (%r15)
	movq	32(%r14), %rdi
	je	.L206
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r13, %rbx
	addq	$1, %r13
	salq	$4, %rbx
	addq	8(%r15), %rbx
	movzwl	8(%rbx), %esi
	call	nghttp2_put_uint16be@PLT
	movq	32(%r14), %rax
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	2(%rax), %rdi
	movq	%rdi, 32(%r14)
	call	nghttp2_cpymem@PLT
	movq	%rax, 32(%r14)
	movq	%rax, %rdi
	cmpq	%r13, (%r15)
	ja	.L205
.L206:
	movq	(%r12), %rax
	subq	24(%r14), %rdi
	addq	$9, %rax
	cmpq	%rax, %rdi
	jne	.L211
	xorl	%eax, %eax
.L202:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L207:
	.cfi_restore_state
	movl	$-522, %eax
	jmp	.L202
.L211:
	leaq	__PRETTY_FUNCTION__.5852(%rip), %rcx
	movl	$804, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE113:
	.size	nghttp2_frame_pack_origin, .-nghttp2_frame_pack_origin
	.p2align 4
	.globl	nghttp2_frame_unpack_origin_payload
	.type	nghttp2_frame_unpack_origin_payload, @function
nghttp2_frame_unpack_origin_payload:
.LFB114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	(%rsi,%rdx), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rsi
	je	.L219
	movq	%rsi, %rbx
	movq	%rsi, %r15
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	cmpq	$1, %rdx
	jg	.L215
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$1, %rax
	jle	.L216
.L215:
	movq	%r15, %rdi
	addq	$2, %r15
	call	nghttp2_get_uint16@PLT
	movzwl	%ax, %eax
	testq	%rax, %rax
	je	.L217
	movq	%r14, %rdx
	subq	%r15, %rdx
	cmpq	%rax, %rdx
	jb	.L216
	addq	%rax, %r15
	leaq	1(%r13,%rax), %r13
	addq	$1, %r12
.L217:
	cmpq	%r15, %r14
	jne	.L218
	testq	%r12, %r12
	je	.L219
	movq	%r12, %rdx
	movq	-64(%rbp), %rdi
	salq	$4, %rdx
	leaq	(%rdx,%r13), %rsi
	movq	%rdx, -72(%rbp)
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L224
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rdx
	movq	%r13, 8(%rax)
	leaq	0(%r13,%rdx), %r14
	movq	%r12, (%rax)
	.p2align 4,,10
	.p2align 3
.L221:
	cmpq	%r15, %rbx
	je	.L235
.L222:
	movq	%rbx, %rdi
	addq	$2, %rbx
	call	nghttp2_get_uint16@PLT
	movzwl	%ax, %r12d
	testq	%r12, %r12
	je	.L221
	movq	%r14, 0(%r13)
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r12, 8(%r13)
	addq	%r12, %rbx
	addq	$16, %r13
	call	nghttp2_cpymem@PLT
	movb	$0, (%rax)
	leaq	1(%rax), %r14
	cmpq	%r15, %rbx
	jne	.L222
.L235:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movl	$-522, %eax
.L212:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L212
	.cfi_endproc
.LFE114:
	.size	nghttp2_frame_unpack_origin_payload, .-nghttp2_frame_unpack_origin_payload
	.p2align 4
	.globl	nghttp2_frame_iv_copy
	.type	nghttp2_frame_iv_copy, @function
nghttp2_frame_iv_copy:
.LFB115:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	salq	$3, %rsi
	je	.L244
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	call	nghttp2_mem_malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L236
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
.L236:
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE115:
	.size	nghttp2_frame_iv_copy, .-nghttp2_frame_iv_copy
	.p2align 4
	.globl	nghttp2_nv_equal
	.type	nghttp2_nv_equal, @function
nghttp2_nv_equal:
.LFB116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rdx
	cmpq	16(%rsi), %rdx
	je	.L253
.L247:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	24(%rdi), %r14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpq	24(%rsi), %r14
	jne	.L247
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L247
	movq	8(%r12), %rsi
	movq	8(%rbx), %rdi
	movq	%r14, %rdx
	xorl	%r13d, %r13d
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%r13b
	jmp	.L247
	.cfi_endproc
.LFE116:
	.size	nghttp2_nv_equal, .-nghttp2_nv_equal
	.p2align 4
	.globl	nghttp2_nv_array_del
	.type	nghttp2_nv_array_del, @function
nghttp2_nv_array_del:
.LFB117:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	jmp	nghttp2_mem_free@PLT
	.cfi_endproc
.LFE117:
	.size	nghttp2_nv_array_del, .-nghttp2_nv_array_del
	.p2align 4
	.globl	nghttp2_nv_compare_name
	.type	nghttp2_nv_compare_name, @function
nghttp2_nv_compare_name:
.LFB119:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rdx
	movq	16(%rdi), %r8
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	cmpq	%r8, %rdx
	je	.L262
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jbe	.L257
	movq	%r8, %rdx
	call	memcmp@PLT
	movl	$-1, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	call	memcmp@PLT
	movl	$1, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore 6
	jmp	memcmp@PLT
	.cfi_endproc
.LFE119:
	.size	nghttp2_nv_compare_name, .-nghttp2_nv_compare_name
	.p2align 4
	.globl	nghttp2_nv_array_sort
	.type	nghttp2_nv_array_sort, @function
nghttp2_nv_array_sort:
.LFB121:
	.cfi_startproc
	endbr64
	leaq	nv_compar(%rip), %rcx
	movl	$40, %edx
	jmp	qsort@PLT
	.cfi_endproc
.LFE121:
	.size	nghttp2_nv_array_sort, .-nghttp2_nv_array_sort
	.p2align 4
	.globl	nghttp2_nv_array_copy
	.type	nghttp2_nv_array_copy, @function
nghttp2_nv_array_copy:
.LFB122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdx, %rdx
	je	.L287
	leaq	(%rdx,%rdx,4), %r14
	leaq	16(%rsi), %rax
	movq	%rsi, %rbx
	movq	%rcx, %rdi
	salq	$3, %r14
	xorl	%esi, %esi
	leaq	(%rax,%r14), %r8
	.p2align 4,,10
	.p2align 3
.L269:
	movzbl	16(%rax), %edx
	testb	$2, %dl
	jne	.L267
	movq	(%rax), %r9
	leaq	1(%rsi,%r9), %rsi
.L267:
	andl	$4, %edx
	jne	.L268
	movq	8(%rax), %rdx
	leaq	1(%rsi,%rdx), %rsi
.L268:
	addq	$40, %rax
	cmpq	%r8, %rax
	jne	.L269
	addq	%r14, %rsi
	call	nghttp2_mem_malloc@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L277
	addq	%rax, %r14
	movq	%r14, %r13
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L289:
	movq	8(%rbx), %rax
	addq	$40, %r12
	movq	%rdx, -16(%r12)
	addq	$40, %rbx
	movq	%rax, -32(%r12)
	cmpq	%r12, %r14
	je	.L288
.L276:
	movzbl	32(%rbx), %eax
	movb	%al, 32(%r12)
	movzbl	32(%rbx), %eax
	movq	16(%rbx), %rdx
	testb	$2, %al
	je	.L270
	movq	(%rbx), %rcx
	movq	%rdx, 16(%r12)
	movq	%rcx, (%r12)
.L271:
	movq	24(%rbx), %rdx
	testb	$4, %al
	jne	.L289
	testq	%rdx, %rdx
	jne	.L290
.L275:
	movq	%r13, 8(%r12)
	addq	$40, %r12
	addq	$40, %rbx
	movq	%rdx, -16(%r12)
	movb	$0, 0(%r13,%rdx)
	movq	-16(%rbx), %rax
	leaq	1(%r13,%rax), %r13
	cmpq	%r12, %r14
	jne	.L276
.L288:
	xorl	%eax, %eax
.L264:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L291
.L272:
	movq	%r13, (%r12)
	movq	%rdx, 16(%r12)
	movb	$0, 0(%r13,%rdx)
	movq	16(%r12), %rsi
	movq	(%r12), %rdi
	call	nghttp2_downcase@PLT
	movq	16(%rbx), %rax
	leaq	1(%r13,%rax), %r13
	movzbl	32(%rbx), %eax
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L290:
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	24(%rbx), %rdx
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L291:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	16(%rbx), %rdx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L287:
	movq	$0, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L277:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L264
	.cfi_endproc
.LFE122:
	.size	nghttp2_nv_array_copy, .-nghttp2_nv_array_copy
	.p2align 4
	.globl	nghttp2_iv_check
	.type	nghttp2_iv_check, @function
nghttp2_iv_check:
.LFB123:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L300
	xorl	%edx, %edx
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L312:
	cmpl	$4, %eax
	jne	.L298
	movl	4(%rdi,%rdx,8), %eax
	testl	%eax, %eax
	js	.L303
	.p2align 4,,10
	.p2align 3
.L298:
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	je	.L300
.L299:
	movl	(%rdi,%rdx,8), %eax
	cmpl	$5, %eax
	je	.L294
	jg	.L295
	cmpl	$2, %eax
	jne	.L312
	cmpl	$1, 4(%rdi,%rdx,8)
	jbe	.L298
.L303:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	cmpl	$8, %eax
	jne	.L298
	cmpl	$1, 4(%rdi,%rdx,8)
	jbe	.L298
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	movl	4(%rdi,%rdx,8), %eax
	subl	$16384, %eax
	cmpl	$16760831, %eax
	jbe	.L298
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE123:
	.size	nghttp2_iv_check, .-nghttp2_iv_check
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"nghttp2_buf_avail(buf) >= padlen - 1"
	.text
	.p2align 4
	.globl	nghttp2_frame_add_pad
	.type	nghttp2_frame_add_pad, @function
nghttp2_frame_add_pad:
.LFB125:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L321
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-1(%rdx), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	16(%rbx), %rax
	subq	32(%rbx), %rax
	cmpq	%r15, %rax
	jb	.L324
	movq	%rsi, %r13
	movq	24(%rbx), %rsi
	movl	$9, %edx
	movl	%ecx, %r14d
	leaq	-1(%rsi), %rdi
	call	memmove@PLT
	movq	24(%rbx), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, 24(%rbx)
	orb	$8, 3(%rax)
	movq	24(%rbx), %rdi
	call	nghttp2_get_uint32@PLT
	movq	24(%rbx), %rdi
	shrl	$8, %eax
	leal	(%rax,%r12), %esi
	movzbl	3(%rdi), %eax
	sall	$8, %esi
	addl	%eax, %esi
	call	nghttp2_put_uint32be@PLT
	testl	%r14d, %r14d
	je	.L325
.L316:
	addq	%r12, 0(%r13)
	xorl	%eax, %eax
	orb	$8, 13(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	24(%rbx), %rax
	movq	%r15, %rdx
	xorl	%esi, %esi
	movb	%r15b, 9(%rax)
	movq	32(%rbx), %rdi
	call	memset@PLT
	addq	%r15, 32(%rbx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
.L324:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	__PRETTY_FUNCTION__.5956(%rip), %rcx
	movl	$1124, %edx
	leaq	.LC1(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE125:
	.size	nghttp2_frame_add_pad, .-nghttp2_frame_add_pad
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.5956, @object
	.size	__PRETTY_FUNCTION__.5956, 22
__PRETTY_FUNCTION__.5956:
	.string	"nghttp2_frame_add_pad"
	.align 16
	.type	__PRETTY_FUNCTION__.5852, @object
	.size	__PRETTY_FUNCTION__.5852, 26
__PRETTY_FUNCTION__.5852:
	.string	"nghttp2_frame_pack_origin"
	.align 16
	.type	__PRETTY_FUNCTION__.5824, @object
	.size	__PRETTY_FUNCTION__.5824, 26
__PRETTY_FUNCTION__.5824:
	.string	"nghttp2_frame_pack_altsvc"
	.align 32
	.type	__PRETTY_FUNCTION__.5812, @object
	.size	__PRETTY_FUNCTION__.5812, 33
__PRETTY_FUNCTION__.5812:
	.string	"nghttp2_frame_pack_window_update"
	.align 16
	.type	__PRETTY_FUNCTION__.5792, @object
	.size	__PRETTY_FUNCTION__.5792, 26
__PRETTY_FUNCTION__.5792:
	.string	"nghttp2_frame_pack_goaway"
	.align 16
	.type	__PRETTY_FUNCTION__.5781, @object
	.size	__PRETTY_FUNCTION__.5781, 24
__PRETTY_FUNCTION__.5781:
	.string	"nghttp2_frame_pack_ping"
	.align 32
	.type	__PRETTY_FUNCTION__.5771, @object
	.size	__PRETTY_FUNCTION__.5771, 32
__PRETTY_FUNCTION__.5771:
	.string	"nghttp2_frame_pack_push_promise"
	.align 16
	.type	__PRETTY_FUNCTION__.5732, @object
	.size	__PRETTY_FUNCTION__.5732, 28
__PRETTY_FUNCTION__.5732:
	.string	"nghttp2_frame_pack_settings"
	.align 16
	.type	__PRETTY_FUNCTION__.5722, @object
	.size	__PRETTY_FUNCTION__.5722, 30
__PRETTY_FUNCTION__.5722:
	.string	"nghttp2_frame_pack_rst_stream"
	.align 16
	.type	__PRETTY_FUNCTION__.5712, @object
	.size	__PRETTY_FUNCTION__.5712, 28
__PRETTY_FUNCTION__.5712:
	.string	"nghttp2_frame_pack_priority"
	.align 16
	.type	__PRETTY_FUNCTION__.5691, @object
	.size	__PRETTY_FUNCTION__.5691, 27
__PRETTY_FUNCTION__.5691:
	.string	"nghttp2_frame_pack_headers"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	2
	.quad	2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
