	.file	"nghttp2_rcbuf.c"
	.text
	.p2align 4
	.globl	nghttp2_rcbuf_new
	.type	nghttp2_rcbuf_new, @function
nghttp2_rcbuf_new:
.LFB31:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	addq	$40, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L3
	movq	%rax, 0(%r13)
	movq	(%rbx), %rdx
	addq	$40, %rax
	movq	16(%rbx), %rcx
	movq	%rdx, -40(%rax)
	movq	0(%r13), %rdx
	movq	%rcx, 8(%rdx)
	movq	%r12, 24(%rdx)
	movl	$1, 32(%rdx)
	movq	%rax, 16(%rdx)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L1
	.cfi_endproc
.LFE31:
	.size	nghttp2_rcbuf_new, .-nghttp2_rcbuf_new
	.p2align 4
	.globl	nghttp2_rcbuf_new2
	.type	nghttp2_rcbuf_new2, @function
nghttp2_rcbuf_new2:
.LFB32:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rcx, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	41(%rdx), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	call	nghttp2_mem_malloc@PLT
	testq	%rax, %rax
	je	.L8
	movq	%rax, (%r14)
	movq	(%rbx), %rdx
	leaq	40(%rax), %rdi
	movq	%r13, %rsi
	movq	16(%rbx), %rcx
	movq	%rdx, (%rax)
	movq	(%r14), %rdx
	movq	%rcx, 8(%rdx)
	movq	%rdi, 16(%rdx)
	movl	$1, 32(%rdx)
	movq	%r12, 24(%rdx)
	movq	%r12, %rdx
	call	nghttp2_cpymem@PLT
	movb	$0, (%rax)
	xorl	%eax, %eax
.L6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movl	$-901, %eax
	jmp	.L6
	.cfi_endproc
.LFE32:
	.size	nghttp2_rcbuf_new2, .-nghttp2_rcbuf_new2
	.p2align 4
	.globl	nghttp2_rcbuf_del
	.type	nghttp2_rcbuf_del, @function
nghttp2_rcbuf_del:
.LFB33:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rdx
	jmp	nghttp2_mem_free2@PLT
	.cfi_endproc
.LFE33:
	.size	nghttp2_rcbuf_del, .-nghttp2_rcbuf_del
	.p2align 4
	.globl	nghttp2_rcbuf_incref
	.type	nghttp2_rcbuf_incref, @function
nghttp2_rcbuf_incref:
.LFB34:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	cmpl	$-1, %eax
	je	.L11
	addl	$1, %eax
	movl	%eax, 32(%rdi)
.L11:
	ret
	.cfi_endproc
.LFE34:
	.size	nghttp2_rcbuf_incref, .-nghttp2_rcbuf_incref
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_rcbuf.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"rcbuf->ref > 0"
	.text
	.p2align 4
	.globl	nghttp2_rcbuf_decref
	.type	nghttp2_rcbuf_decref, @function
nghttp2_rcbuf_decref:
.LFB35:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L16
	movl	32(%rdi), %eax
	movq	%rdi, %rsi
	cmpl	$-1, %eax
	je	.L16
	testl	%eax, %eax
	jle	.L27
	subl	$1, %eax
	movl	%eax, 32(%rdi)
	je	.L28
.L16:
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	8(%rdi), %rdi
	movq	(%rsi), %rdx
	jmp	nghttp2_mem_free2@PLT
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.3974(%rip), %rcx
	movl	$88, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE35:
	.size	nghttp2_rcbuf_decref, .-nghttp2_rcbuf_decref
	.p2align 4
	.globl	nghttp2_rcbuf_get_buf
	.type	nghttp2_rcbuf_get_buf, @function
nghttp2_rcbuf_get_buf:
.LFB36:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE36:
	.size	nghttp2_rcbuf_get_buf, .-nghttp2_rcbuf_get_buf
	.p2align 4
	.globl	nghttp2_rcbuf_is_static
	.type	nghttp2_rcbuf_is_static, @function
nghttp2_rcbuf_is_static:
.LFB37:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$-1, 32(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE37:
	.size	nghttp2_rcbuf_is_static, .-nghttp2_rcbuf_is_static
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.3974, @object
	.size	__PRETTY_FUNCTION__.3974, 21
__PRETTY_FUNCTION__.3974:
	.string	"nghttp2_rcbuf_decref"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
