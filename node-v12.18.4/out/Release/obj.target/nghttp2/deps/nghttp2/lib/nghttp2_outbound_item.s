	.file	"nghttp2_outbound_item.c"
	.text
	.p2align 4
	.globl	nghttp2_outbound_item_init
	.type	nghttp2_outbound_item_init, @function
nghttp2_outbound_item_init:
.LFB31:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movb	$0, 144(%rdi)
	movq	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	ret
	.cfi_endproc
.LFE31:
	.size	nghttp2_outbound_item_init, .-nghttp2_outbound_item_init
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_outbound_item.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"0"
	.text
	.p2align 4
	.globl	nghttp2_outbound_item_free
	.type	nghttp2_outbound_item_free, @function
nghttp2_outbound_item_free:
.LFB32:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3
	movzbl	12(%rdi), %eax
	cmpb	$8, %al
	ja	.L5
	leaq	.L7(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L7:
	.long	.L15-.L7
	.long	.L14-.L7
	.long	.L13-.L7
	.long	.L12-.L7
	.long	.L11-.L7
	.long	.L10-.L7
	.long	.L9-.L7
	.long	.L8-.L7
	.long	.L6-.L7
	.text
	.p2align 4,,10
	.p2align 3
.L8:
	jmp	nghttp2_frame_goaway_free@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	jmp	nghttp2_frame_window_update_free@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	jmp	nghttp2_frame_data_free@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	jmp	nghttp2_frame_headers_free@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	jmp	nghttp2_frame_priority_free@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	jmp	nghttp2_frame_rst_stream_free@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	jmp	nghttp2_frame_settings_free@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	jmp	nghttp2_frame_push_promise_free@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	jmp	nghttp2_frame_ping_free@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	ret
.L5:
	cmpb	$0, 96(%rdi)
	je	.L24
	cmpb	$10, %al
	je	.L17
	cmpb	$12, %al
	jne	.L25
	jmp	nghttp2_frame_origin_free@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	jmp	nghttp2_frame_altsvc_free@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	jmp	nghttp2_frame_extension_free@PLT
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.4589(%rip), %rcx
	movl	$93, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE32:
	.size	nghttp2_outbound_item_free, .-nghttp2_outbound_item_free
	.p2align 4
	.globl	nghttp2_outbound_queue_init
	.type	nghttp2_outbound_queue_init, @function
nghttp2_outbound_queue_init:
.LFB33:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE33:
	.size	nghttp2_outbound_queue_init, .-nghttp2_outbound_queue_init
	.p2align 4
	.globl	nghttp2_outbound_queue_push
	.type	nghttp2_outbound_queue_push, @function
nghttp2_outbound_queue_push:
.LFB34:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L28
	movq	%rsi, 136(%rax)
	addq	$1, 16(%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rsi, %xmm0
	addq	$1, 16(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE34:
	.size	nghttp2_outbound_queue_push, .-nghttp2_outbound_queue_push
	.p2align 4
	.globl	nghttp2_outbound_queue_pop
	.type	nghttp2_outbound_queue_pop, @function
nghttp2_outbound_queue_pop:
.LFB35:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L30
	movq	136(%rax), %rdx
	movq	%rdx, (%rdi)
	movq	$0, 136(%rax)
	testq	%rdx, %rdx
	je	.L36
.L32:
	subq	$1, 16(%rdi)
.L30:
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	$0, 8(%rdi)
	jmp	.L32
	.cfi_endproc
.LFE35:
	.size	nghttp2_outbound_queue_pop, .-nghttp2_outbound_queue_pop
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.4589, @object
	.size	__PRETTY_FUNCTION__.4589, 27
__PRETTY_FUNCTION__.4589:
	.string	"nghttp2_outbound_item_free"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
