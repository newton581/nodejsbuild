	.file	"nghttp2_stream.c"
	.text
	.p2align 4
	.type	stream_less, @function
stream_less:
.LFB54:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdx
	movq	72(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L5
	subq	%rdx, %rax
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	setbe	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	88(%rsi), %rax
	cmpq	%rax, 88(%rdi)
	setb	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE54:
	.size	stream_less, .-stream_less
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_stream.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"stream->sib_prev == NULL"
	.text
	.p2align 4
	.type	insert_link_dep.part.0, @function
insert_link_dep.part.0:
.LFB106:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	__PRETTY_FUNCTION__.5845(%rip), %rcx
	movl	$653, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE106:
	.size	insert_link_dep.part.0, .-insert_link_dep.part.0
	.section	.rodata.str1.1
.LC2:
	.string	"stream->queued"
	.text
	.p2align 4
	.type	stream_obq_remove, @function
stream_obq_remove:
.LFB62:
	.cfi_startproc
	cmpb	$0, 238(%rdi)
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %r12
	testq	%r12, %r12
	je	.L8
.L12:
	leaq	32(%r12), %r13
	leaq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	nghttp2_pq_remove@PLT
	cmpb	$0, 238(%rbx)
	je	.L27
	movb	$0, 238(%rbx)
	pxor	%xmm0, %xmm0
	cmpq	$0, 176(%r12)
	movl	$0, 220(%rbx)
	movq	$0, 184(%rbx)
	movups	%xmm0, 88(%rbx)
	je	.L11
	testb	$12, 236(%r12)
	jne	.L11
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r13, %rdi
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	je	.L8
	movq	120(%r12), %rax
	movq	%r12, %rbx
	testq	%rax, %rax
	je	.L8
	movq	%rax, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L27:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	__PRETTY_FUNCTION__.5720(%rip), %rcx
	movl	$190, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE62:
	.size	stream_obq_remove, .-stream_obq_remove
	.p2align 4
	.globl	nghttp2_stream_init
	.type	nghttp2_stream_init, @function
nghttp2_stream_init:
.LFB55:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%edx, -52(%rbp)
	call	nghttp2_map_entry_init@PLT
	movq	32(%rbp), %rdx
	leaq	32(%rbx), %rdi
	leaq	stream_less(%rip), %rsi
	call	nghttp2_pq_init@PLT
	movq	24(%rbp), %rax
	movl	-52(%rbp), %r10d
	pxor	%xmm0, %xmm0
	movdqa	.LC3(%rip), %xmm1
	movl	%r12d, 192(%rbx)
	movq	%rax, 168(%rbx)
	movl	16(%rbp), %eax
	movl	%r15d, 228(%rbx)
	movl	%eax, 212(%rbx)
	xorl	%eax, %eax
	movl	%r14d, 196(%rbx)
	movl	%r13d, 216(%rbx)
	movb	%r10b, 236(%rbx)
	movq	$0, 176(%rbx)
	movq	$0, 200(%rbx)
	movl	$0, 208(%rbx)
	movl	$65535, 232(%rbx)
	movb	$0, 237(%rbx)
	movw	%ax, 238(%rbx)
	movq	$0, 220(%rbx)
	movq	$0, 184(%rbx)
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	movups	%xmm1, 72(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE55:
	.size	nghttp2_stream_init, .-nghttp2_stream_init
	.p2align 4
	.globl	nghttp2_stream_free
	.type	nghttp2_stream_free, @function
nghttp2_stream_free:
.LFB56:
	.cfi_startproc
	endbr64
	addq	$32, %rdi
	jmp	nghttp2_pq_free@PLT
	.cfi_endproc
.LFE56:
	.size	nghttp2_stream_free, .-nghttp2_stream_free
	.p2align 4
	.globl	nghttp2_stream_shutdown
	.type	nghttp2_stream_shutdown, @function
nghttp2_stream_shutdown:
.LFB57:
	.cfi_startproc
	endbr64
	orb	%sil, 237(%rdi)
	ret
	.cfi_endproc
.LFE57:
	.size	nghttp2_stream_shutdown, .-nghttp2_stream_shutdown
	.p2align 4
	.globl	nghttp2_stream_reschedule
	.type	nghttp2_stream_reschedule, @function
nghttp2_stream_reschedule:
.LFB64:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$0, 238(%rdi)
	je	.L41
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L32
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	24(%rbx), %r14
	leaq	32(%r12), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	nghttp2_pq_remove@PLT
	movq	184(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	220(%rbx), %edx
	movl	216(%rbx), %ecx
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	addq	88(%r12), %rax
	movq	%rax, 96(%rbx)
	movq	104(%r12), %rax
	movl	%edx, 220(%rbx)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r12)
	movq	%rax, 112(%rbx)
	call	nghttp2_pq_push@PLT
	movq	184(%rbx), %rax
	movq	%r12, %rbx
	movq	%rax, 184(%r12)
	movq	120(%r12), %r12
	testq	%r12, %r12
	jne	.L35
.L32:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5733(%rip), %rcx
	movl	$228, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE64:
	.size	nghttp2_stream_reschedule, .-nghttp2_stream_reschedule
	.p2align 4
	.globl	nghttp2_stream_change_weight
	.type	nghttp2_stream_change_weight, @function
nghttp2_stream_change_weight:
.LFB65:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	216(%rdi), %r12d
	cmpl	%esi, %r12d
	je	.L42
	movq	120(%rdi), %r13
	movl	%esi, 216(%rdi)
	movq	%rdi, %rbx
	testq	%r13, %r13
	je	.L42
	subl	%r12d, %esi
	addl	%esi, 224(%r13)
	cmpb	$0, 238(%rdi)
	jne	.L51
.L42:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	leaq	24(%rdi), %r15
	leaq	32(%r13), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	nghttp2_pq_remove@PLT
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	184(%rbx), %rsi
	addl	220(%rbx), %r12d
	salq	$8, %rsi
	movq	%rsi, %rax
	divq	%rcx
	movq	%r12, %rax
	subq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	leaq	(%rdx,%rsi), %r12
	xorl	%edx, %edx
	movl	216(%rbx), %esi
	movq	%r12, %rax
	divq	%rcx
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	%r12, %rax
	divq	%rsi
	addq	96(%rbx), %rax
	movq	%rax, %rsi
	subq	%rcx, %rsi
	movl	%edx, 220(%rbx)
	movq	88(%r13), %rdx
	addq	%rdx, %rcx
	subq	%rax, %rcx
	movl	$4294967295, %eax
	cmpq	%rax, %rcx
	cmovbe	%rdx, %rsi
	movq	%rsi, 96(%rbx)
	addq	$8, %rsp
	movq	%r15, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	nghttp2_pq_push@PLT
	.cfi_endproc
.LFE65:
	.size	nghttp2_stream_change_weight, .-nghttp2_stream_change_weight
	.p2align 4
	.globl	nghttp2_stream_dep_distributed_weight
	.type	nghttp2_stream_dep_distributed_weight, @function
nghttp2_stream_dep_distributed_weight:
.LFB67:
	.cfi_startproc
	endbr64
	movl	216(%rdi), %eax
	imull	%esi, %eax
	movl	$1, %esi
	cltd
	idivl	224(%rdi)
	testl	%eax, %eax
	cmovle	%esi, %eax
	ret
	.cfi_endproc
.LFE67:
	.size	nghttp2_stream_dep_distributed_weight, .-nghttp2_stream_dep_distributed_weight
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"(stream->flags & NGHTTP2_STREAM_FLAG_DEFERRED_ALL) == 0"
	.section	.rodata.str1.1
.LC5:
	.string	"stream->item == NULL"
	.text
	.p2align 4
	.globl	nghttp2_stream_attach_item
	.type	nghttp2_stream_attach_item, @function
nghttp2_stream_attach_item:
.LFB71:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testb	$12, 236(%rdi)
	jne	.L67
	cmpq	$0, 176(%rdi)
	movq	%rdi, %r13
	jne	.L68
	movq	120(%rdi), %r12
	movq	%rsi, 176(%rdi)
	movq	%rdi, %rbx
	testq	%r12, %r12
	jne	.L56
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	movq	184(%rbx), %rax
	movl	220(%rbx), %edx
	leaq	24(%rbx), %rsi
	leaq	32(%r12), %rdi
	movl	216(%rbx), %ecx
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	addq	88(%r12), %rax
	movq	%rax, 96(%rbx)
	movq	104(%r12), %rax
	movl	%edx, 220(%rbx)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r12)
	movq	%rax, 112(%rbx)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	jne	.L58
	movb	$1, 238(%rbx)
	movq	120(%r12), %rax
	movq	%r12, %rbx
	testq	%rax, %rax
	je	.L59
	movq	%rax, %r12
.L56:
	cmpb	$0, 238(%rbx)
	je	.L60
.L59:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	$0, 176(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L68:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5770(%rip), %rcx
	movl	$482, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	__assert_fail@PLT
.L67:
	leaq	__PRETTY_FUNCTION__.5770(%rip), %rcx
	movl	$481, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE71:
	.size	nghttp2_stream_attach_item, .-nghttp2_stream_attach_item
	.p2align 4
	.globl	nghttp2_stream_detach_item
	.type	nghttp2_stream_detach_item, @function
nghttp2_stream_detach_item:
.LFB72:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	andb	$-13, 236(%rdi)
	addq	$32, %rdi
	movq	$0, 144(%rdi)
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	jne	.L75
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r12, %rdi
	call	stream_obq_remove
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE72:
	.size	nghttp2_stream_detach_item, .-nghttp2_stream_detach_item
	.section	.rodata.str1.1
.LC6:
	.string	"stream->item"
	.text
	.p2align 4
	.globl	nghttp2_stream_defer_item
	.type	nghttp2_stream_defer_item, @function
nghttp2_stream_defer_item:
.LFB73:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpq	$0, 176(%rdi)
	je	.L83
	orb	%sil, 236(%rdi)
	movq	%rdi, %r12
	leaq	32(%rdi), %rdi
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	jne	.L84
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	%r12, %rdi
	call	stream_obq_remove
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L83:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.5778(%rip), %rcx
	movl	$511, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE73:
	.size	nghttp2_stream_defer_item, .-nghttp2_stream_defer_item
	.p2align 4
	.globl	nghttp2_stream_resume_deferred_item
	.type	nghttp2_stream_resume_deferred_item, @function
nghttp2_stream_resume_deferred_item:
.LFB74:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 176(%rdi)
	je	.L97
	notl	%esi
	andb	236(%rdi), %sil
	movq	%rdi, %rbx
	movb	%sil, 236(%rdi)
	andl	$12, %esi
	je	.L87
.L90:
	xorl	%eax, %eax
.L85:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	120(%rdi), %r12
	testq	%r12, %r12
	jne	.L89
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	movq	184(%rbx), %rax
	movl	220(%rbx), %edx
	leaq	24(%rbx), %rsi
	leaq	32(%r12), %rdi
	movl	216(%rbx), %ecx
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	addq	88(%r12), %rax
	movq	%rax, 96(%rbx)
	movq	104(%r12), %rax
	movl	%edx, 220(%rbx)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r12)
	movq	%rax, 112(%rbx)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	jne	.L85
	movb	$1, 238(%rbx)
	movq	120(%r12), %rax
	movq	%r12, %rbx
	testq	%rax, %rax
	je	.L90
	movq	%rax, %r12
.L89:
	cmpb	$0, 238(%rbx)
	je	.L91
	jmp	.L90
.L97:
	leaq	__PRETTY_FUNCTION__.5783(%rip), %rcx
	movl	$522, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE74:
	.size	nghttp2_stream_resume_deferred_item, .-nghttp2_stream_resume_deferred_item
	.p2align 4
	.globl	nghttp2_stream_check_deferred_item
	.type	nghttp2_stream_check_deferred_item, @function
nghttp2_stream_check_deferred_item:
.LFB75:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 176(%rdi)
	je	.L98
	xorl	%eax, %eax
	testb	$12, 236(%rdi)
	setne	%al
.L98:
	ret
	.cfi_endproc
.LFE75:
	.size	nghttp2_stream_check_deferred_item, .-nghttp2_stream_check_deferred_item
	.p2align 4
	.globl	nghttp2_stream_check_deferred_by_flow_control
	.type	nghttp2_stream_check_deferred_by_flow_control, @function
nghttp2_stream_check_deferred_by_flow_control:
.LFB76:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 176(%rdi)
	je	.L101
	movzbl	236(%rdi), %eax
	shrb	$2, %al
	andl	$1, %eax
.L101:
	ret
	.cfi_endproc
.LFE76:
	.size	nghttp2_stream_check_deferred_by_flow_control, .-nghttp2_stream_check_deferred_by_flow_control
	.p2align 4
	.globl	nghttp2_stream_update_remote_initial_window_size
	.type	nghttp2_stream_update_remote_initial_window_size, @function
nghttp2_stream_update_remote_initial_window_size:
.LFB78:
	.cfi_startproc
	endbr64
	movslq	%esi, %r8
	movslq	196(%rdi), %rsi
	movslq	%edx, %rdx
	movl	$2147483648, %eax
	addq	%r8, %rsi
	subq	%rdx, %rsi
	movl	$4294967295, %edx
	addq	%rsi, %rax
	cmpq	%rdx, %rax
	ja	.L106
	movl	%esi, 196(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE78:
	.size	nghttp2_stream_update_remote_initial_window_size, .-nghttp2_stream_update_remote_initial_window_size
	.p2align 4
	.globl	nghttp2_stream_update_local_initial_window_size
	.type	nghttp2_stream_update_local_initial_window_size, @function
nghttp2_stream_update_local_initial_window_size:
.LFB79:
	.cfi_startproc
	endbr64
	movslq	%esi, %r8
	movslq	212(%rdi), %rsi
	movslq	%edx, %rdx
	movl	$2147483648, %eax
	addq	%r8, %rsi
	subq	%rdx, %rsi
	movl	$4294967295, %edx
	addq	%rsi, %rax
	cmpq	%rdx, %rax
	ja	.L109
	movl	%esi, 212(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE79:
	.size	nghttp2_stream_update_local_initial_window_size, .-nghttp2_stream_update_local_initial_window_size
	.p2align 4
	.globl	nghttp2_stream_promise_fulfilled
	.type	nghttp2_stream_promise_fulfilled, @function
nghttp2_stream_promise_fulfilled:
.LFB80:
	.cfi_startproc
	endbr64
	andb	$-2, 236(%rdi)
	movl	$2, 228(%rdi)
	ret
	.cfi_endproc
.LFE80:
	.size	nghttp2_stream_promise_fulfilled, .-nghttp2_stream_promise_fulfilled
	.p2align 4
	.globl	nghttp2_stream_dep_find_ancestor
	.type	nghttp2_stream_dep_find_ancestor, @function
nghttp2_stream_dep_find_ancestor:
.LFB81:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L115
	cmpq	%rsi, %rdi
	jne	.L113
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L114:
	cmpq	%rdi, %rsi
	je	.L117
.L113:
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L114
.L115:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE81:
	.size	nghttp2_stream_dep_find_ancestor, .-nghttp2_stream_dep_find_ancestor
	.p2align 4
	.globl	nghttp2_stream_dep_insert
	.type	nghttp2_stream_dep_insert, @function
nghttp2_stream_dep_insert:
.LFB82:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	224(%rdi), %eax
	movq	128(%rdi), %r13
	movl	%eax, 224(%rsi)
	movl	216(%rsi), %eax
	movl	%eax, 224(%rdi)
	testq	%r13, %r13
	je	.L119
	leaq	32(%rdi), %rax
	movq	%rax, -64(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L120:
	movq	144(%r13), %r13
	testq	%r13, %r13
	je	.L144
.L123:
	cmpb	$0, 238(%r13)
	movq	%rbx, 120(%r13)
	je	.L120
	movq	-64(%rbp), %rdi
	leaq	24(%r13), %rsi
	movq	%r13, %r15
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	call	nghttp2_pq_remove@PLT
	movb	$0, 238(%r13)
	movq	-56(%rbp), %rsi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L145:
	movb	$1, 238(%r15)
	movq	120(%r14), %rax
	testq	%rax, %rax
	je	.L120
	cmpb	$0, 238(%r14)
	jne	.L120
	leaq	24(%r14), %rsi
	movq	%r14, %r15
	movq	%rax, %r14
.L122:
	movq	184(%r15), %rax
	movl	220(%r15), %edx
	movl	216(%r15), %edi
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rdi
	leaq	32(%r14), %rdi
	addq	88(%r14), %rax
	movq	%rax, 96(%r15)
	movq	104(%r14), %rax
	movl	%edx, 220(%r15)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r14)
	movq	%rax, 112(%r15)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	je	.L145
.L118:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	cmpq	$0, 176(%rbx)
	je	.L124
	testb	$12, 236(%rbx)
	je	.L125
.L124:
	leaq	32(%rbx), %rdi
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	je	.L125
.L127:
	movq	128(%r12), %rax
	movq	%rax, 128(%rbx)
.L119:
	movq	%rbx, 128(%r12)
	xorl	%eax, %eax
	movq	%r12, 120(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%rbx, %r13
	movq	%r12, %r14
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rax, %r14
.L126:
	cmpb	$0, 238(%r13)
	jne	.L127
	movq	184(%r13), %rax
	movl	220(%r13), %edx
	leaq	24(%r13), %rsi
	leaq	32(%r14), %rdi
	movl	216(%r13), %ecx
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	addq	88(%r14), %rax
	movq	%rax, 96(%r13)
	movq	104(%r14), %rax
	movl	%edx, 220(%r13)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r14)
	movq	%rax, 112(%r13)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	jne	.L118
	movb	$1, 238(%r13)
	movq	120(%r14), %rax
	movq	%r14, %r13
	testq	%rax, %rax
	jne	.L129
	movq	128(%r12), %rax
	movq	%rax, 128(%rbx)
	jmp	.L119
	.cfi_endproc
.LFE82:
	.size	nghttp2_stream_dep_insert, .-nghttp2_stream_dep_insert
	.p2align 4
	.globl	nghttp2_stream_dep_add
	.type	nghttp2_stream_dep_add, @function
nghttp2_stream_dep_add:
.LFB89:
	.cfi_startproc
	endbr64
	movl	216(%rsi), %eax
	addl	%eax, 224(%rdi)
	movq	128(%rdi), %rax
	testq	%rax, %rax
	je	.L153
	cmpq	$0, 136(%rsi)
	jne	.L154
	movq	%rax, 144(%rsi)
	movq	%rsi, 136(%rax)
.L153:
	movq	%rsi, 128(%rdi)
	movq	%rdi, 120(%rsi)
	ret
.L154:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	insert_link_dep.part.0
	.cfi_endproc
.LFE89:
	.size	nghttp2_stream_dep_add, .-nghttp2_stream_dep_add
	.section	.rodata.str1.1
.LC7:
	.string	"stream->dep_prev"
.LC8:
	.string	"prev"
	.text
	.p2align 4
	.globl	nghttp2_stream_dep_remove
	.type	nghttp2_stream_dep_remove, @function
nghttp2_stream_dep_remove:
.LFB90:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	216(%rdi), %eax
	movq	128(%rdi), %r14
	movq	120(%rdi), %r13
	movl	%eax, %ebx
	negl	%ebx
	testq	%r14, %r14
	jne	.L162
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L157:
	movq	144(%r14), %r14
	testq	%r14, %r14
	je	.L156
.L198:
	movl	216(%r15), %eax
.L162:
	imull	216(%r14), %eax
	movl	$1, %ecx
	cltd
	idivl	224(%r15)
	testl	%eax, %eax
	cmovle	%ecx, %eax
	addl	%eax, %ebx
	cmpb	$0, 238(%r14)
	movl	%eax, 216(%r14)
	je	.L157
	leaq	24(%r14), %rsi
	leaq	32(%r15), %rdi
	movq	%r14, %r12
	movq	%rsi, -56(%rbp)
	call	nghttp2_pq_remove@PLT
	testq	%r13, %r13
	movq	-56(%rbp), %rsi
	movb	$0, 238(%r14)
	jne	.L158
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	24(%r13), %rsi
	movq	%r13, %r12
	movq	%rax, %r13
.L158:
	movq	184(%r12), %rax
	movl	220(%r12), %edx
	movl	216(%r12), %edi
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rdi
	leaq	32(%r13), %rdi
	addq	88(%r13), %rax
	movq	%rax, 96(%r12)
	movq	104(%r13), %rax
	movl	%edx, 220(%r12)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r13)
	movq	%rax, 112(%r12)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	jne	.L155
	movb	$1, 238(%r12)
	movq	120(%r13), %rax
	testq	%rax, %rax
	je	.L161
	cmpb	$0, 238(%r13)
	je	.L197
.L161:
	movq	144(%r14), %r14
	movq	120(%r15), %r13
	testq	%r14, %r14
	jne	.L198
.L156:
	testq	%r13, %r13
	je	.L199
	addl	%ebx, 224(%r13)
	cmpb	$0, 238(%r15)
	jne	.L200
	movq	136(%r15), %rdx
	movq	128(%r15), %rax
	testq	%rdx, %rdx
	je	.L171
	testq	%rax, %rax
	je	.L166
.L203:
	movq	120(%r15), %rcx
	movq	%rax, 144(%rdx)
	movq	%rdx, 136(%rax)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rcx, 120(%rdx)
	movq	144(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L167
	movq	144(%r15), %rcx
	testq	%rcx, %rcx
	je	.L169
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%rax, %rdx
	movq	144(%rax), %rax
	testq	%rax, %rax
	jne	.L168
	movq	%rcx, 144(%rdx)
	movq	%rdx, 136(%rcx)
.L169:
	movl	$0, 224(%r15)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movups	%xmm0, 120(%r15)
	movups	%xmm0, 136(%r15)
.L155:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	%r15, %rdi
	call	stream_obq_remove
	movq	136(%r15), %rdx
	testq	%rdx, %rdx
	jne	.L201
	movq	120(%r15), %r13
	movq	128(%r15), %rax
	testq	%r13, %r13
	je	.L202
.L171:
	movq	144(%r15), %rsi
	testq	%rax, %rax
	je	.L172
	movq	%rax, 128(%r13)
	movq	%rax, %rdx
	movq	%r13, 120(%rax)
	movq	120(%r15), %rcx
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%rcx, 120(%rdx)
	movq	144(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L173
	testq	%rsi, %rsi
	je	.L169
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rax, %rdx
	movq	144(%rax), %rax
	testq	%rax, %rax
	jne	.L174
	movq	%rsi, 144(%rdx)
	movq	%rdx, 136(%rsi)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L201:
	movq	128(%r15), %rax
	testq	%rax, %rax
	jne	.L203
.L166:
	movq	144(%r15), %rax
	movq	%rax, 144(%rdx)
	testq	%rax, %rax
	je	.L169
	movq	%rdx, 136(%rax)
	jmp	.L169
.L172:
	testq	%rsi, %rsi
	je	.L175
	movq	$0, 136(%rsi)
	movq	%rsi, 128(%r13)
	movq	%r13, 120(%rsi)
	jmp	.L169
.L175:
	movq	$0, 128(%r13)
	jmp	.L169
.L202:
	leaq	__PRETTY_FUNCTION__.5859(%rip), %rcx
	movl	$704, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	call	__assert_fail@PLT
.L199:
	leaq	__PRETTY_FUNCTION__.5874(%rip), %rcx
	movl	$777, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE90:
	.size	nghttp2_stream_dep_remove, .-nghttp2_stream_dep_remove
	.p2align 4
	.globl	nghttp2_stream_dep_insert_subtree
	.type	nghttp2_stream_dep_insert_subtree, @function
nghttp2_stream_dep_insert_subtree:
.LFB91:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	224(%rdi), %eax
	movq	128(%rdi), %r15
	addl	%eax, 224(%rsi)
	movl	216(%rsi), %eax
	movq	%rsi, 128(%rdi)
	movl	%eax, 224(%rdi)
	movq	%rdi, 120(%rsi)
	testq	%r15, %r15
	je	.L213
	movq	128(%rsi), %rax
	testq	%rax, %rax
	je	.L206
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%rax, %rdx
	movq	144(%rax), %rax
	testq	%rax, %rax
	jne	.L207
	movq	%r15, 144(%rdx)
	movq	%rdx, 136(%r15)
.L208:
	leaq	32(%rbx), %rax
	movq	%rax, -64(%rbp)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L209:
	movq	144(%r15), %r15
	testq	%r15, %r15
	je	.L213
.L212:
	cmpb	$0, 238(%r15)
	movq	%r14, 120(%r15)
	je	.L209
	movq	-64(%rbp), %rdi
	leaq	24(%r15), %rsi
	movq	%r15, %r13
	movq	%r14, %r12
	movq	%rsi, -56(%rbp)
	call	nghttp2_pq_remove@PLT
	movb	$0, 238(%r15)
	movq	-56(%rbp), %rsi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L233:
	cmpb	$0, 238(%r12)
	jne	.L209
	leaq	24(%r12), %rsi
	movq	%r12, %r13
	movq	%rax, %r12
.L211:
	movq	184(%r13), %rax
	movl	220(%r13), %edx
	movl	216(%r13), %edi
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rdi
	leaq	32(%r12), %rdi
	addq	88(%r12), %rax
	movq	%rax, 96(%r13)
	movq	104(%r12), %rax
	movl	%edx, 220(%r13)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r12)
	movq	%rax, 112(%r13)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	jne	.L204
	movb	$1, 238(%r13)
	movq	120(%r12), %rax
	testq	%rax, %rax
	jne	.L233
	movq	144(%r15), %r15
	testq	%r15, %r15
	jne	.L212
	.p2align 4,,10
	.p2align 3
.L213:
	cmpq	$0, 176(%r14)
	je	.L214
	testb	$12, 236(%r14)
	je	.L216
.L214:
	leaq	32(%r14), %rdi
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	je	.L216
.L217:
	xorl	%eax, %eax
.L204:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	%rax, %rbx
.L216:
	cmpb	$0, 238(%r14)
	jne	.L217
	movq	184(%r14), %rax
	movl	220(%r14), %edx
	leaq	32(%rbx), %rdi
	movl	216(%r14), %esi
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	24(%r14), %rsi
	addq	88(%rbx), %rax
	movq	%rax, 96(%r14)
	movq	104(%rbx), %rax
	movl	%edx, 220(%r14)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%rbx)
	movq	%rax, 112(%r14)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	jne	.L204
	movb	$1, 238(%r14)
	movq	120(%rbx), %rax
	movq	%rbx, %r14
	testq	%rax, %rax
	jne	.L234
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%r15, 128(%rsi)
	movq	%rsi, 120(%r15)
	jmp	.L208
	.cfi_endproc
.LFE91:
	.size	nghttp2_stream_dep_insert_subtree, .-nghttp2_stream_dep_insert_subtree
	.p2align 4
	.globl	nghttp2_stream_dep_add_subtree
	.type	nghttp2_stream_dep_add_subtree, @function
nghttp2_stream_dep_add_subtree:
.LFB92:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	216(%rsi), %eax
	movq	%rsi, %rbx
	addl	%eax, 224(%rdi)
	movq	128(%rdi), %rax
	testq	%rax, %rax
	je	.L236
	cmpq	$0, 136(%rsi)
	jne	.L253
	movq	%rax, 144(%rsi)
	movq	%rsi, 136(%rax)
	movq	%rsi, 128(%r12)
	movq	%rdi, 120(%rbx)
.L238:
	cmpq	$0, 176(%rbx)
	je	.L239
	testb	$12, 236(%rbx)
	je	.L242
.L239:
	leaq	32(%rbx), %rdi
	call	nghttp2_pq_empty@PLT
	testl	%eax, %eax
	je	.L242
.L243:
	xorl	%eax, %eax
.L235:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movq	%rax, %r12
.L242:
	cmpb	$0, 238(%rbx)
	jne	.L243
	movq	184(%rbx), %rax
	movl	220(%rbx), %edx
	leaq	24(%rbx), %rsi
	leaq	32(%r12), %rdi
	movl	216(%rbx), %ecx
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	addq	88(%r12), %rax
	movq	%rax, 96(%rbx)
	movq	104(%r12), %rax
	movl	%edx, 220(%rbx)
	leaq	1(%rax), %rdx
	movq	%rdx, 104(%r12)
	movq	%rax, 112(%rbx)
	call	nghttp2_pq_push@PLT
	testl	%eax, %eax
	jne	.L235
	movb	$1, 238(%rbx)
	movq	120(%r12), %rax
	movq	%r12, %rbx
	testq	%rax, %rax
	jne	.L254
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rsi, 128(%r12)
	movq	%rdi, 120(%rbx)
	jmp	.L238
.L253:
	call	insert_link_dep.part.0
	.cfi_endproc
.LFE92:
	.size	nghttp2_stream_dep_add_subtree, .-nghttp2_stream_dep_add_subtree
	.p2align 4
	.globl	nghttp2_stream_dep_remove_subtree
	.type	nghttp2_stream_dep_remove_subtree, @function
nghttp2_stream_dep_remove_subtree:
.LFB93:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	120(%rdi), %rax
	testq	%rax, %rax
	je	.L266
	movq	136(%rdi), %rcx
	movq	144(%rdi), %rdx
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	je	.L257
	movq	%rdx, 144(%rcx)
	testq	%rdx, %rdx
	je	.L259
	movq	%rcx, 136(%rdx)
.L259:
	movl	216(%rbx), %edx
	subl	%edx, 224(%rax)
	cmpb	$0, 238(%rbx)
	jne	.L267
.L261:
	pxor	%xmm0, %xmm0
	movq	$0, 120(%rbx)
	movups	%xmm0, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	stream_obq_remove
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%rdx, 128(%rax)
	testq	%rdx, %rdx
	je	.L259
	movq	%rax, 120(%rdx)
	movq	$0, 136(%rdx)
	jmp	.L259
.L266:
	leaq	__PRETTY_FUNCTION__.5896(%rip), %rcx
	movl	$889, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE93:
	.size	nghttp2_stream_dep_remove_subtree, .-nghttp2_stream_dep_remove_subtree
	.p2align 4
	.globl	nghttp2_stream_in_dep_tree
	.type	nghttp2_stream_in_dep_tree, @function
nghttp2_stream_in_dep_tree:
.LFB94:
	.cfi_startproc
	endbr64
	cmpq	$0, 120(%rdi)
	movl	$1, %eax
	je	.L273
.L268:
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	cmpq	$0, 128(%rdi)
	jne	.L268
	cmpq	$0, 136(%rdi)
	jne	.L268
	xorl	%eax, %eax
	cmpq	$0, 144(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE94:
	.size	nghttp2_stream_in_dep_tree, .-nghttp2_stream_in_dep_tree
	.p2align 4
	.globl	nghttp2_stream_next_outbound_item
	.type	nghttp2_stream_next_outbound_item, @function
nghttp2_stream_next_outbound_item:
.LFB95:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
.L279:
	movq	176(%rdi), %rax
	testq	%rax, %rax
	je	.L275
	testb	$12, 236(%rdi)
	jne	.L275
	movq	120(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L276
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%rcx, %rdx
.L276:
	movq	96(%rdi), %rcx
	movq	%rdx, %rdi
	movq	%rcx, 88(%rdx)
	movq	120(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L280
.L274:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	addq	$32, %rdi
	call	nghttp2_pq_top@PLT
	testq	%rax, %rax
	je	.L274
	leaq	-24(%rax), %rdi
	jmp	.L279
	.cfi_endproc
.LFE95:
	.size	nghttp2_stream_next_outbound_item, .-nghttp2_stream_next_outbound_item
	.p2align 4
	.globl	nghttp2_stream_get_state
	.type	nghttp2_stream_get_state, @function
nghttp2_stream_get_state:
.LFB96:
	.cfi_startproc
	endbr64
	movzbl	236(%rdi), %edx
	movl	$7, %eax
	testb	$2, %dl
	jne	.L290
	movzbl	237(%rdi), %ecx
	movl	%ecx, %esi
	andl	$1, %esi
	andl	$1, %edx
	je	.L292
	movl	$3, %eax
	testb	%sil, %sil
	jne	.L290
	andl	$2, %ecx
	movl	$4, %eax
	jne	.L301
.L293:
	xorl	%eax, %eax
	cmpl	$5, 228(%rdi)
	setne	%al
	addl	$1, %eax
.L290:
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$6, %eax
	testb	%sil, %sil
	jne	.L290
	andl	$2, %ecx
	movl	$5, %eax
	je	.L293
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	ret
	.cfi_endproc
.LFE96:
	.size	nghttp2_stream_get_state, .-nghttp2_stream_get_state
	.p2align 4
	.globl	nghttp2_stream_get_parent
	.type	nghttp2_stream_get_parent, @function
nghttp2_stream_get_parent:
.LFB97:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE97:
	.size	nghttp2_stream_get_parent, .-nghttp2_stream_get_parent
	.p2align 4
	.globl	nghttp2_stream_get_next_sibling
	.type	nghttp2_stream_get_next_sibling, @function
nghttp2_stream_get_next_sibling:
.LFB98:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %rax
	ret
	.cfi_endproc
.LFE98:
	.size	nghttp2_stream_get_next_sibling, .-nghttp2_stream_get_next_sibling
	.p2align 4
	.globl	nghttp2_stream_get_previous_sibling
	.type	nghttp2_stream_get_previous_sibling, @function
nghttp2_stream_get_previous_sibling:
.LFB99:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	ret
	.cfi_endproc
.LFE99:
	.size	nghttp2_stream_get_previous_sibling, .-nghttp2_stream_get_previous_sibling
	.p2align 4
	.globl	nghttp2_stream_get_first_child
	.type	nghttp2_stream_get_first_child, @function
nghttp2_stream_get_first_child:
.LFB100:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	ret
	.cfi_endproc
.LFE100:
	.size	nghttp2_stream_get_first_child, .-nghttp2_stream_get_first_child
	.p2align 4
	.globl	nghttp2_stream_get_weight
	.type	nghttp2_stream_get_weight, @function
nghttp2_stream_get_weight:
.LFB101:
	.cfi_startproc
	endbr64
	movl	216(%rdi), %eax
	ret
	.cfi_endproc
.LFE101:
	.size	nghttp2_stream_get_weight, .-nghttp2_stream_get_weight
	.p2align 4
	.globl	nghttp2_stream_get_sum_dependency_weight
	.type	nghttp2_stream_get_sum_dependency_weight, @function
nghttp2_stream_get_sum_dependency_weight:
.LFB102:
	.cfi_startproc
	endbr64
	movl	224(%rdi), %eax
	ret
	.cfi_endproc
.LFE102:
	.size	nghttp2_stream_get_sum_dependency_weight, .-nghttp2_stream_get_sum_dependency_weight
	.p2align 4
	.globl	nghttp2_stream_get_stream_id
	.type	nghttp2_stream_get_stream_id, @function
nghttp2_stream_get_stream_id:
.LFB103:
	.cfi_startproc
	endbr64
	movl	192(%rdi), %eax
	ret
	.cfi_endproc
.LFE103:
	.size	nghttp2_stream_get_stream_id, .-nghttp2_stream_get_stream_id
	.section	.rodata
	.align 32
	.type	__PRETTY_FUNCTION__.5896, @object
	.size	__PRETTY_FUNCTION__.5896, 34
__PRETTY_FUNCTION__.5896:
	.string	"nghttp2_stream_dep_remove_subtree"
	.align 8
	.type	__PRETTY_FUNCTION__.5859, @object
	.size	__PRETTY_FUNCTION__.5859, 11
__PRETTY_FUNCTION__.5859:
	.string	"unlink_dep"
	.align 16
	.type	__PRETTY_FUNCTION__.5874, @object
	.size	__PRETTY_FUNCTION__.5874, 26
__PRETTY_FUNCTION__.5874:
	.string	"nghttp2_stream_dep_remove"
	.align 16
	.type	__PRETTY_FUNCTION__.5845, @object
	.size	__PRETTY_FUNCTION__.5845, 16
__PRETTY_FUNCTION__.5845:
	.string	"insert_link_dep"
	.align 32
	.type	__PRETTY_FUNCTION__.5783, @object
	.size	__PRETTY_FUNCTION__.5783, 36
__PRETTY_FUNCTION__.5783:
	.string	"nghttp2_stream_resume_deferred_item"
	.align 16
	.type	__PRETTY_FUNCTION__.5778, @object
	.size	__PRETTY_FUNCTION__.5778, 26
__PRETTY_FUNCTION__.5778:
	.string	"nghttp2_stream_defer_item"
	.align 16
	.type	__PRETTY_FUNCTION__.5720, @object
	.size	__PRETTY_FUNCTION__.5720, 18
__PRETTY_FUNCTION__.5720:
	.string	"stream_obq_remove"
	.align 16
	.type	__PRETTY_FUNCTION__.5770, @object
	.size	__PRETTY_FUNCTION__.5770, 27
__PRETTY_FUNCTION__.5770:
	.string	"nghttp2_stream_attach_item"
	.align 16
	.type	__PRETTY_FUNCTION__.5733, @object
	.size	__PRETTY_FUNCTION__.5733, 26
__PRETTY_FUNCTION__.5733:
	.string	"nghttp2_stream_reschedule"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	-1
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
