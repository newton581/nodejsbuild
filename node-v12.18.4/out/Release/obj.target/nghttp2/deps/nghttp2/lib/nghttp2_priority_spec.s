	.file	"nghttp2_priority_spec.c"
	.text
	.p2align 4
	.globl	nghttp2_priority_spec_init
	.type	nghttp2_priority_spec_init, @function
nghttp2_priority_spec_init:
.LFB20:
	.cfi_startproc
	endbr64
	testl	%ecx, %ecx
	movl	%esi, (%rdi)
	movl	%edx, 4(%rdi)
	setne	8(%rdi)
	ret
	.cfi_endproc
.LFE20:
	.size	nghttp2_priority_spec_init, .-nghttp2_priority_spec_init
	.p2align 4
	.globl	nghttp2_priority_spec_default_init
	.type	nghttp2_priority_spec_default_init, @function
nghttp2_priority_spec_default_init:
.LFB21:
	.cfi_startproc
	endbr64
	movabsq	$68719476736, %rax
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE21:
	.size	nghttp2_priority_spec_default_init, .-nghttp2_priority_spec_default_init
	.p2align 4
	.globl	nghttp2_priority_spec_check_default
	.type	nghttp2_priority_spec_check_default, @function
nghttp2_priority_spec_check_default:
.LFB22:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L6
	cmpl	$16, 4(%rdi)
	je	.L7
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	cmpb	$0, 8(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE22:
	.size	nghttp2_priority_spec_check_default, .-nghttp2_priority_spec_check_default
	.p2align 4
	.globl	nghttp2_priority_spec_normalize_weight
	.type	nghttp2_priority_spec_normalize_weight, @function
nghttp2_priority_spec_normalize_weight:
.LFB23:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jle	.L11
	cmpl	$256, %eax
	jle	.L8
	movl	$256, 4(%rdi)
.L8:
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, 4(%rdi)
	ret
	.cfi_endproc
.LFE23:
	.size	nghttp2_priority_spec_normalize_weight, .-nghttp2_priority_spec_normalize_weight
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
