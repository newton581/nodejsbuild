	.file	"nghttp2_http.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/nghttp2/lib/nghttp2_http.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"nv->name->len > 0"
.LC2:
	.string	"CONNECT"
.LC3:
	.string	"OPTIONS"
	.text
	.p2align 4
	.globl	nghttp2_http_on_header
	.type	nghttp2_http_on_header, @function
nghttp2_http_on_header:
.LFB64:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	24(%rax), %rsi
	movq	16(%rax), %rdi
	call	nghttp2_check_header_name@PLT
	testl	%eax, %eax
	jne	.L2
	movq	(%r12), %rax
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L261
	movq	16(%rax), %rax
	cmpb	$58, (%rax)
	je	.L260
	addq	%rax, %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L264:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L261
.L7:
	movzbl	(%rax), %esi
	leal	-65(%rsi), %edx
	cmpb	$25, %dl
	ja	.L264
.L260:
	movl	$-531, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movq	8(%r12), %rdx
	movl	16(%r12), %eax
	movq	24(%rdx), %rsi
	movq	16(%rdx), %rdi
	testl	%eax, %eax
	je	.L135
	cmpl	$37, %eax
	je	.L135
	cmpl	$5, %eax
	jne	.L11
	testq	%rsi, %rsi
	je	.L15
	movzbl	(%rdi), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L15
	addq	%rdi, %rsi
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	je	.L16
	movabsq	$288063250384289792, %rcx
	.p2align 4,,10
	.p2align 3
.L18:
	movzbl	(%rdi), %edx
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L17
	cmpb	$57, %dl
	jbe	.L265
.L15:
	movq	(%r12), %rax
	cmpq	$0, 24(%rax)
	je	.L266
	movq	16(%rax), %rax
	cmpb	$58, (%rax)
	je	.L260
.L261:
	orw	$64, 234(%rbx)
	movl	$-105, %r8d
.L1:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	call	nghttp2_check_header_value@PLT
.L10:
	testl	%eax, %eax
	je	.L15
.L16:
	movq	(%r12), %rax
	cmpb	$0, 2498(%r13)
	movq	16(%rax), %rcx
	movzbl	(%rcx), %edx
	jne	.L19
	cmpb	$5, 12(%r14)
	je	.L267
	cmpb	$58, %dl
	je	.L268
	movl	16(%r12), %eax
	cmpl	$56, %eax
	je	.L260
	jg	.L119
	cmpl	$7, %eax
	je	.L120
	cmpl	$27, %eax
	jne	.L122
.L121:
	movzwl	232(%rbx), %eax
	cmpw	$204, %ax
	je	.L269
	leal	-100(%rax), %edx
	cmpw	$99, %dx
	jbe	.L260
	subw	$200, %ax
	cmpw	$99, %ax
	ja	.L92
	testb	$-128, 234(%rbx)
	jne	.L262
.L92:
	cmpq	$-1, 72(%rbx)
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %rdx
	movq	16(%rax), %r8
	testq	%rdx, %rdx
	je	.L260
	movzbl	(%r8), %eax
	leal	-48(%rax), %esi
	cmpb	$9, %sil
	ja	.L260
	subl	$48, %eax
	leaq	1(%r8), %rdi
	xorl	%esi, %esi
	addq	%rdx, %r8
	movabsq	$922337203685477580, %r10
	cltq
	movabsq	$9223372036854775807, %r9
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L94:
	movzbl	(%rdi), %edx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	ja	.L260
	cmpq	%r10, %rax
	jg	.L260
	leaq	(%rax,%rax,4), %rax
	addq	$1, %rdi
	leaq	(%rax,%rax), %rsi
	movzbl	%dl, %eax
	movq	%r9, %rdx
	subl	$48, %eax
	cltq
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jg	.L260
.L126:
	addq	%rsi, %rax
	cmpq	%r8, %rdi
	jne	.L94
	movq	%rax, 72(%rbx)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L135:
	call	nghttp2_check_authority@PLT
	jmp	.L10
.L119:
	cmpl	$61, %eax
	je	.L123
	subl	$62, %eax
	cmpl	$3, %eax
	jbe	.L260
.L122:
	orw	$64, 234(%rbx)
	xorl	%r8d, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L265:
	btq	%rdx, %rcx
	jnc	.L15
.L17:
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	jne	.L18
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%edi, %edi
	cmpb	$0, 2497(%r13)
	setne	%dil
.L20:
	cmpb	$58, %dl
	je	.L270
	cmpl	$66, 16(%r12)
	ja	.L35
	movl	16(%r12), %eax
	leaq	.L115(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L115:
	.long	.L24-.L115
	.long	.L29-.L115
	.long	.L35-.L115
	.long	.L40-.L115
	.long	.L35-.L115
	.long	.L47-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L70-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L65-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L260-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L35-.L115
	.long	.L74-.L115
	.long	.L260-.L115
	.long	.L260-.L115
	.long	.L260-.L115
	.long	.L260-.L115
	.long	.L61-.L115
	.text
.L47:
	movzwl	234(%rbx), %r13d
.L48:
	testb	$8, %r13b
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %r8
	movq	16(%rax), %rdi
	testq	%r8, %r8
	je	.L260
	movq	%rdi, %rax
	leaq	(%r8,%rdi), %rsi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L260
.L51:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L140
	cmpb	$9, %dl
	je	.L140
	orl	$8, %r13d
	movw	%r13w, 234(%rbx)
	cmpq	$4, %r8
	je	.L271
	cmpq	$5, %r8
	jne	.L257
	movzbl	(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$104, %al
	jne	.L257
	movzbl	1(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$116, %al
	jne	.L257
	movzbl	2(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$116, %al
	jne	.L257
	movzbl	3(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$112, %al
	jne	.L257
	movzbl	4(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$115, %al
	jne	.L257
.L52:
	orw	$8192, %r13w
	movw	%r13w, 234(%rbx)
.L257:
	movzbl	(%rcx), %edx
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%r8d, %r8d
	cmpb	$58, %dl
	je	.L1
	orw	$64, 234(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L267:
	xorl	%edi, %edi
	jmp	.L20
.L123:
	movq	8(%r12), %rax
	cmpq	$8, 24(%rax)
	jne	.L260
	movq	16(%rax), %rax
	movzbl	(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$116, %cl
	jne	.L260
	movzbl	1(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$114, %cl
	jne	.L260
	movzbl	2(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$97, %cl
	jne	.L260
	movzbl	3(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$105, %cl
	jne	.L260
	movzbl	4(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$108, %cl
	jne	.L260
	movzbl	5(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$101, %cl
	jne	.L260
	movzbl	6(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$114, %cl
	jne	.L260
	movzbl	7(%rax), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	cmpb	$115, %al
	jne	.L260
.L89:
	xorl	%r8d, %r8d
	cmpb	$58, %dl
	je	.L1
	jmp	.L122
.L120:
	movzwl	234(%rbx), %esi
.L124:
	testb	$32, %sil
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %r9
	movq	16(%rax), %rdi
	testq	%r9, %r9
	je	.L260
	movq	%rdi, %rax
	leaq	(%r9,%rdi), %r8
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L143:
	addq	$1, %rax
	cmpq	%r8, %rax
	je	.L260
.L87:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L143
	cmpb	$9, %dl
	je	.L143
	orl	$32, %esi
	movw	%si, 234(%rbx)
	cmpq	$3, %r9
	jne	.L260
	movzbl	(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	ja	.L88
	movzbl	1(%rdi), %edx
	subl	$48, %eax
	cltq
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	ja	.L88
	leaq	(%rax,%rax,4), %rsi
	movzbl	%dl, %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rsi,2), %rdx
	movzbl	2(%rdi), %eax
	leal	-48(%rax), %esi
	cmpb	$9, %sil
	ja	.L88
	subl	$48, %eax
	leaq	(%rdx,%rdx,4), %rdx
	cltq
	leaq	(%rax,%rdx,2), %rax
	movw	%ax, 232(%rbx)
	cmpw	$101, %ax
	je	.L260
.L258:
	movzbl	(%rcx), %edx
	jmp	.L89
.L269:
	cmpq	$-1, 72(%rbx)
	jne	.L260
	movq	8(%r12), %rax
	cmpq	$1, 24(%rax)
	jne	.L260
	movq	16(%rax), %rax
	cmpb	$48, (%rax)
	jne	.L260
	movq	$0, 72(%rbx)
.L262:
	movl	$-106, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	$61, %eax
	je	.L74
	cmpl	$66, %eax
	jne	.L260
.L61:
	testl	%edi, %edi
	je	.L260
	movzwl	234(%rbx), %edi
	testw	%di, %di
	js	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %rsi
	movq	16(%rax), %rax
	testq	%rsi, %rsi
	je	.L260
	addq	%rax, %rsi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L141:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L260
.L64:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L141
	cmpb	$9, %dl
	je	.L141
	orw	$-32768, %di
	movw	%di, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L74:
	movq	8(%r12), %rax
	cmpq	$8, 24(%rax)
	jne	.L260
	movq	16(%rax), %rax
	movzbl	(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$116, %cl
	jne	.L260
	movzbl	1(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$114, %cl
	jne	.L260
	movzbl	2(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$97, %cl
	jne	.L260
	movzbl	3(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$105, %cl
	jne	.L260
	movzbl	4(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$108, %cl
	jne	.L260
	movzbl	5(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$101, %cl
	jne	.L260
	movzbl	6(%rax), %ecx
	leal	-65(%rcx), %edi
	leal	32(%rcx), %esi
	cmpb	$26, %dil
	cmovb	%esi, %ecx
	cmpb	$114, %cl
	jne	.L260
	movzbl	7(%rax), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %eax
	cmpb	$115, %al
	je	.L35
	jmp	.L260
.L70:
	cmpq	$-1, 72(%rbx)
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %rdx
	movq	16(%rax), %r8
	testq	%rdx, %rdx
	je	.L260
	movzbl	(%r8), %eax
	leal	-48(%rax), %esi
	cmpb	$9, %sil
	ja	.L260
	subl	$48, %eax
	leaq	1(%r8), %rdi
	xorl	%esi, %esi
	addq	%rdx, %r8
	movabsq	$922337203685477580, %r9
	cltq
	movabsq	$9223372036854775807, %r10
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L72:
	movzbl	(%rdi), %edx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	ja	.L260
	cmpq	%r9, %rax
	jg	.L260
	leaq	(%rax,%rax,4), %rax
	addq	$1, %rdi
	leaq	(%rax,%rax), %rsi
	movzbl	%dl, %eax
	movq	%r10, %rdx
	subl	$48, %eax
	cltq
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	jg	.L260
.L125:
	addq	%rsi, %rax
	cmpq	%r8, %rdi
	jne	.L72
	movq	%rax, 72(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L65:
	movzwl	234(%rbx), %r13d
.L66:
	testb	$16, %r13b
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %rsi
	movq	16(%rax), %rax
	testq	%rsi, %rsi
	je	.L260
	addq	%rax, %rsi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L260
.L69:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L142
	cmpb	$9, %dl
	je	.L142
	orl	$16, %r13d
	movw	%r13w, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L40:
	movzwl	234(%rbx), %r13d
.L41:
	testb	$2, %r13b
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %r8
	movq	16(%rax), %rdi
	testq	%r8, %r8
	je	.L260
	movq	%rdi, %rax
	leaq	(%r8,%rdi), %rsi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L138:
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L260
.L44:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L138
	cmpb	$9, %dl
	je	.L138
	movl	%r13d, %eax
	orl	$2, %eax
	movw	%ax, 234(%rbx)
	movzbl	(%rdi), %eax
	cmpb	$47, %al
	je	.L272
	cmpq	$1, %r8
	jne	.L257
	cmpb	$42, %al
	jne	.L257
	orw	$4098, %r13w
	movw	%r13w, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L24:
	movzwl	234(%rbx), %r13d
.L25:
	testb	$1, %r13b
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %rsi
	movq	16(%rax), %rax
	testq	%rsi, %rsi
	je	.L260
	addq	%rax, %rsi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L260
.L28:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L136
	cmpb	$9, %dl
	je	.L136
	orl	$1, %r13d
	movw	%r13w, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L29:
	movzwl	234(%rbx), %r13d
.L30:
	testb	$4, %r13b
	jne	.L260
	movq	8(%r12), %rax
	movq	24(%rax), %rdi
	movq	16(%rax), %r8
	testq	%rdi, %rdi
	je	.L260
	movq	%r8, %rax
	leaq	(%rdi,%r8), %rsi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L137:
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L260
.L33:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L137
	cmpb	$9, %dl
	je	.L137
	movl	%r13d, %eax
	orl	$4, %eax
	movw	%ax, 234(%rbx)
	cmpq	$4, %rdi
	je	.L104
	cmpq	$7, %rdi
	jne	.L257
	movzbl	6(%r8), %eax
	cmpb	$83, %al
	je	.L36
	cmpb	$84, %al
	jne	.L257
	movl	$7, %edx
	movq	%r8, %rsi
	leaq	.LC2(%rip), %rdi
	movq	%rcx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	testl	%eax, %eax
	jne	.L257
	testb	$1, 192(%rbx)
	je	.L260
	orb	$-124, %r13b
	movw	%r13w, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L270:
	testl	%r15d, %r15d
	jne	.L260
	movzwl	234(%rbx), %r13d
	testb	$64, %r13b
	jne	.L260
	movl	16(%r12), %eax
	cmpl	$37, %eax
	jg	.L116
	movl	$-531, %r8d
	testl	%eax, %eax
	js	.L1
	cmpl	$37, %eax
	ja	.L260
	leaq	.L117(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L117:
	.long	.L25-.L117
	.long	.L30-.L117
	.long	.L260-.L117
	.long	.L41-.L117
	.long	.L260-.L117
	.long	.L48-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L70-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L260-.L117
	.long	.L66-.L117
	.text
.L268:
	testl	%r15d, %r15d
	jne	.L260
	movzwl	234(%rbx), %esi
	testb	$64, %sil
	jne	.L260
	movl	16(%r12), %eax
	cmpl	$27, %eax
	je	.L121
	cmpl	$61, %eax
	je	.L123
	cmpl	$7, %eax
	jne	.L260
	jmp	.L124
.L272:
	orw	$2050, %r13w
	movw	%r13w, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L271:
	movzbl	(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$104, %al
	jne	.L257
	movzbl	1(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$116, %al
	jne	.L257
	movzbl	2(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$116, %al
	jne	.L257
	movzbl	3(%rdi), %eax
	leal	-65(%rax), %esi
	leal	32(%rax), %edx
	cmpb	$26, %sil
	cmovb	%edx, %eax
	cmpb	$112, %al
	je	.L52
	jmp	.L257
.L104:
	cmpl	$1145128264, (%r8)
	jne	.L257
	orw	$260, %r13w
	movw	%r13w, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L36:
	movl	$7, %edx
	movq	%r8, %rsi
	leaq	.LC3(%rip), %rdi
	movq	%rcx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	testl	%eax, %eax
	jne	.L257
	orw	$516, %r13w
	movw	%r13w, 234(%rbx)
	movzbl	(%rcx), %edx
	jmp	.L35
.L88:
	movw	$-1, 232(%rbx)
	movl	$-531, %r8d
	jmp	.L1
.L266:
	leaq	__PRETTY_FUNCTION__.5801(%rip), %rcx
	movl	$373, %edx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE64:
	.size	nghttp2_http_on_header, .-nghttp2_http_on_header
	.p2align 4
	.globl	nghttp2_http_on_request_headers
	.type	nghttp2_http_on_request_headers, @function
nghttp2_http_on_request_headers:
.LFB65:
	.cfi_startproc
	endbr64
	movzwl	234(%rdi), %eax
	movl	%eax, %edx
	testw	%ax, %ax
	js	.L274
	testb	$-128, %al
	jne	.L291
.L274:
	andl	$14, %edx
	cmpw	$14, %dx
	jne	.L281
	testb	$17, %al
	je	.L281
	testw	%ax, %ax
	js	.L292
.L277:
	testb	$32, %ah
	jne	.L293
.L276:
	xorl	%r8d, %r8d
	cmpb	$5, 12(%rsi)
	jne	.L273
	andw	$1920, %ax
	movq	$-1, 72(%rdi)
	movw	%ax, 234(%rdi)
.L273:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	testb	$8, %ah
	jne	.L276
	movl	%eax, %edx
	movl	$-1, %r8d
	andw	$4608, %dx
	cmpw	$4608, %dx
	je	.L276
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	andl	$11, %edx
	cmpw	$1, %dx
	jne	.L281
	movq	$-1, 72(%rdi)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L292:
	movl	%eax, %edx
	andw	$129, %dx
	cmpw	$129, %dx
	je	.L277
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$-1, %r8d
	jmp	.L273
	.cfi_endproc
.LFE65:
	.size	nghttp2_http_on_request_headers, .-nghttp2_http_on_request_headers
	.p2align 4
	.globl	nghttp2_http_on_response_headers
	.type	nghttp2_http_on_response_headers, @function
nghttp2_http_on_response_headers:
.LFB66:
	.cfi_startproc
	endbr64
	movzwl	234(%rdi), %eax
	testb	$32, %al
	je	.L299
	movzwl	232(%rdi), %edx
	leal	-100(%rdx), %ecx
	cmpw	$99, %cx
	jbe	.L307
	movl	%eax, %ecx
	andb	$-65, %ch
	cmpw	$204, %dx
	movw	%cx, 234(%rdi)
	setne	%cl
	cmpw	$304, %dx
	setne	%dl
	testb	%dl, %cl
	je	.L297
	testb	$1, %ah
	je	.L308
.L297:
	movq	$0, 72(%rdi)
.L306:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	andw	$1920, %ax
	movq	$-1, 72(%rdi)
	orb	$64, %ah
	movw	%ax, 234(%rdi)
	movl	$-1, %eax
	movw	%ax, 232(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	testw	$1152, %ax
	je	.L306
	movq	$-1, 72(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE66:
	.size	nghttp2_http_on_response_headers, .-nghttp2_http_on_response_headers
	.p2align 4
	.globl	nghttp2_http_on_trailer_headers
	.type	nghttp2_http_on_trailer_headers, @function
nghttp2_http_on_trailer_headers:
.LFB67:
	.cfi_startproc
	endbr64
	movzbl	13(%rsi), %eax
	andl	$1, %eax
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE67:
	.size	nghttp2_http_on_trailer_headers, .-nghttp2_http_on_trailer_headers
	.p2align 4
	.globl	nghttp2_http_on_remote_end_stream
	.type	nghttp2_http_on_remote_end_stream, @function
nghttp2_http_on_remote_end_stream:
.LFB68:
	.cfi_startproc
	endbr64
	testb	$64, 235(%rdi)
	jne	.L312
	movq	72(%rdi), %rdx
	xorl	%eax, %eax
	cmpq	$-1, %rdx
	je	.L310
	xorl	%eax, %eax
	cmpq	%rdx, 80(%rdi)
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$-1, %eax
.L310:
	ret
	.cfi_endproc
.LFE68:
	.size	nghttp2_http_on_remote_end_stream, .-nghttp2_http_on_remote_end_stream
	.p2align 4
	.globl	nghttp2_http_on_data_chunk
	.type	nghttp2_http_on_data_chunk, @function
nghttp2_http_on_data_chunk:
.LFB69:
	.cfi_startproc
	endbr64
	addq	80(%rdi), %rsi
	movq	%rsi, 80(%rdi)
	testb	$64, 235(%rdi)
	jne	.L317
	movq	72(%rdi), %rdx
	cmpq	%rdx, %rsi
	setg	%cl
	xorl	%eax, %eax
	cmpq	$-1, %rdx
	setne	%al
	andl	%ecx, %eax
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE69:
	.size	nghttp2_http_on_data_chunk, .-nghttp2_http_on_data_chunk
	.section	.rodata.str1.1
.LC4:
	.string	":metho"
	.text
	.p2align 4
	.globl	nghttp2_http_record_request_method
	.type	nghttp2_http_record_request_method, @function
nghttp2_http_record_request_method:
.LFB70:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	cmpb	$1, %al
	je	.L319
	cmpb	$5, %al
	jne	.L336
	movq	24(%rsi), %rax
	movq	32(%rsi), %rsi
.L322:
	testq	%rsi, %rsi
	je	.L318
	xorl	%edx, %edx
.L327:
	cmpq	$7, 16(%rax)
	jne	.L323
	movq	(%rax), %rcx
	cmpb	$100, 6(%rcx)
	jne	.L323
	cmpl	$1952804154, (%rcx)
	je	.L337
	.p2align 4,,10
	.p2align 3
.L323:
	addq	$1, %rdx
	addq	$40, %rax
	cmpq	%rdx, %rsi
	jne	.L327
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	cmpw	$28520, 4(%rcx)
	jne	.L323
	movq	24(%rax), %rdx
	cmpq	$7, %rdx
	je	.L338
	cmpq	$4, %rdx
	jne	.L318
	movq	8(%rax), %rax
	cmpl	$1145128264, (%rax)
	jne	.L318
	orw	$256, 234(%rdi)
.L318:
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	movq	40(%rsi), %rax
	movq	48(%rsi), %rsi
	jmp	.L322
.L338:
	movq	8(%rax), %rax
	cmpl	$1313754947, (%rax)
	jne	.L318
	cmpw	$17221, 4(%rax)
	jne	.L318
	cmpb	$84, 6(%rax)
	jne	.L318
	orw	$128, 234(%rdi)
	ret
	.cfi_endproc
.LFE70:
	.size	nghttp2_http_record_request_method, .-nghttp2_http_record_request_method
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.5801, @object
	.size	__PRETTY_FUNCTION__.5801, 23
__PRETTY_FUNCTION__.5801:
	.string	"nghttp2_http_on_header"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
