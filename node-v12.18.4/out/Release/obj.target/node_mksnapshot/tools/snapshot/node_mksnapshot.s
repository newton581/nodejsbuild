	.file	"node_mksnapshot.cc"
	.text
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev:
.LFB6727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L2
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L6
.L4:
	movq	0(%r13), %r12
.L2:
	testq	%r12, %r12
	je	.L1
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L6
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6727:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	.set	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC5ERKS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_:
.LFB6999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r12
	subq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	movq	$0, 16(%rdi)
	sarq	$5, %rax
	movups	%xmm0, (%rdi)
	je	.L33
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L34
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%rax, %rbx
.L12:
	movq	%rbx, %xmm0
	addq	%rbx, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 16(%r13)
	movups	%xmm0, 0(%r13)
	movq	8(%r14), %rax
	movq	(%r14), %r15
	movq	%rax, -80(%rbp)
	cmpq	%r15, %rax
	je	.L14
	leaq	-64(%rbp), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L37:
	movzbl	(%r8), %eax
	movb	%al, 16(%rbx)
.L19:
	movq	%r12, 8(%rbx)
	addq	$32, %r15
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r15, -80(%rbp)
	je	.L14
.L20:
	leaq	16(%rbx), %rdi
	movq	8(%r15), %r12
	movq	%rdi, (%rbx)
	movq	(%r15), %r8
	movq	%r8, %rax
	addq	%r12, %rax
	je	.L15
	testq	%r8, %r8
	je	.L35
.L15:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L36
	cmpq	$1, %r12
	je	.L37
	testq	%r12, %r12
	je	.L19
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
.L17:
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rbx, 8(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L12
.L35:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L38:
	call	__stack_chk_fail@PLT
.L34:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6999:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC1ERKS7_
	.set	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC1ERKS7_,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC2ERKS7_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"--random_seed=42"
.LC2:
	.string	"Usage: "
.LC3:
	.string	" <path/to/output.cc>\n"
.LC4:
	.string	"Cannot open "
.LC5:
	.string	"\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB6406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	leaq	.LC1(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$728, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v82V818SetFlagsFromStringEPKc@PLT
	cmpl	$1, %r12d
	jle	.L49
	leaq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	movl	$20, %edx
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	leaq	-464(%rbp), %rdi
	call	_ZNKSt12__basic_fileIcE7is_openEv@PLT
	testb	%al, %al
	je	.L50
	leaq	-61(%rbp), %rax
	leaq	-688(%rbp), %rdi
	movb	$0, -57(%rbp)
	movl	$1, %esi
	leaq	-624(%rbp), %rdx
	movl	$1701080942, -61(%rbp)
	movq	$0, -616(%rbp)
	movq	%rax, -624(%rbp)
	call	_ZN4node24InitializeOncePerProcessEiPPc@PLT
	cmpb	$0, -632(%rbp)
	jne	.L51
	movl	-688(%rbp), %r13d
	testl	%r13d, %r13d
	jne	.L52
	leaq	-720(%rbp), %r14
	leaq	-656(%rbp), %rbx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-680(%rbp), %r15
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC1ERKS7_
	leaq	-752(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -760(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEC1ERKS7_
	movq	-760(%rbp), %r8
	movq	%r14, %rdx
	leaq	-608(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN4node15SnapshotBuilder8GenerateESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS7_EES9_@PLT
	movq	-760(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r14, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	-600(%rbp), %rdx
	movq	-608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE5closeEv@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	call	_ZN4node22TearDownOncePerProcessEv@PLT
	movq	%rbx, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r15, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L43:
	movq	%r12, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev@PLT
.L39:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$728, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	movl	$12, %edx
	leaq	.LC4(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	movl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	jmp	.L43
.L49:
	movl	$7, %edx
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	movl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	jmp	.L39
.L51:
	leaq	_ZZ4mainE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L52:
	leaq	_ZZ4mainE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6406:
	.size	main, .-main
	.p2align 4
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB7776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7776:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../tools/snapshot/node_mksnapshot.cc:43"
	.section	.rodata.str1.1
.LC7:
	.string	"(result.exit_code) == (0)"
.LC8:
	.string	"int main(int, char**)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZ4mainE4args_0, @object
	.size	_ZZ4mainE4args_0, 24
_ZZ4mainE4args_0:
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"../tools/snapshot/node_mksnapshot.cc:42"
	.section	.rodata.str1.1
.LC10:
	.string	"!result.early_return"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ4mainE4args, @object
	.size	_ZZ4mainE4args, 24
_ZZ4mainE4args:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
