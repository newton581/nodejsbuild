	.file	"task-queue.cc"
	.text
	.section	.text._ZN2v88platform9TaskQueueC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform9TaskQueueC2Ev
	.type	_ZN2v88platform9TaskQueueC2Ev, @function
_ZN2v88platform9TaskQueueC2Ev:
.LFB3823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v84base9SemaphoreC1Ei@PLT
	leaq	32(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, 72(%rbx)
	movq	$8, 80(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 136(%rbx)
	call	_Znwm@PLT
	movq	80(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, 72(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 112(%rbx)
	movq	%rax, %xmm0
	leaq	512(%rax), %rdx
	movq	%rax, (%r12)
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 144(%rbx)
	movq	%rdx, 104(%rbx)
	movq	%rax, 128(%rbx)
	movq	%rdx, 136(%rbx)
	movq	%rax, 120(%rbx)
	movb	$0, 152(%rbx)
	movups	%xmm0, 88(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3823:
	.size	_ZN2v88platform9TaskQueueC2Ev, .-_ZN2v88platform9TaskQueueC2Ev
	.globl	_ZN2v88platform9TaskQueueC1Ev
	.set	_ZN2v88platform9TaskQueueC1Ev,_ZN2v88platform9TaskQueueC2Ev
	.section	.rodata._ZN2v88platform9TaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88platform9TaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform9TaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.type	_ZN2v88platform9TaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, @function
_ZN2v88platform9TaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE:
.LFB3828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	136(%r12), %rcx
	movq	120(%r12), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L5
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movq	%rdx, (%rax)
	addq	$8, 120(%r12)
.L6:
	movq	%r12, %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	144(%r12), %r14
	movq	112(%r12), %rsi
	movabsq	$1152921504606846975, %r8
	subq	128(%r12), %rax
	movq	%r14, %r15
	sarq	$3, %rax
	subq	%rsi, %r15
	movq	%r15, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	104(%r12), %rax
	subq	88(%r12), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%r8, %rax
	je	.L15
	movq	72(%r12), %r9
	movq	80(%r12), %rdx
	movq	%r14, %rax
	subq	%r9, %rax
	movq	%rdx, %rcx
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L16
.L8:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%rbx), %rdx
	movq	120(%r12), %rax
	movq	$0, (%rbx)
	movq	%rdx, (%rax)
	movq	144(%r12), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 144(%r12)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 128(%r12)
	movq	%rdx, 136(%r12)
	movq	%rax, 120(%r12)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	2(%rdi), %r10
	leaq	(%r10,%r10), %rax
	cmpq	%rax, %rdx
	ja	.L17
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%r8, %r14
	ja	.L18
	leaq	0(,%r14,8), %rdi
	movq	%r10, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r10
	movq	112(%r12), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r10, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	144(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L13
	movq	%r8, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	%rax, %r8
.L13:
	movq	72(%r12), %rdi
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 80(%r12)
	movq	-64(%rbp), %r8
	movq	%rax, 72(%r12)
.L11:
	movq	%r8, 112(%r12)
	movq	(%r8), %rax
	leaq	(%r8,%r15), %r14
	movq	(%r8), %xmm0
	movq	%r14, 144(%r12)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%r12)
	movq	(%r14), %rax
	movq	%rax, 128(%r12)
	addq	$512, %rax
	movq	%rax, 136(%r12)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	subq	%r10, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r9,%rdx,8), %r8
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L10
	cmpq	%r14, %rsi
	je	.L11
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%r14, %rsi
	je	.L11
	leaq	8(%r15), %rdi
	movq	%r8, -56(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
	jmp	.L11
.L15:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L18:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE3828:
	.size	_ZN2v88platform9TaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, .-_ZN2v88platform9TaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.section	.text._ZN2v88platform9TaskQueue7GetNextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform9TaskQueue7GetNextEv
	.type	_ZN2v88platform9TaskQueue7GetNextEv, @function
_ZN2v88platform9TaskQueue7GetNextEv:
.LFB3829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L20:
	cmpb	$0, 152(%r12)
	jne	.L35
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base9Semaphore4WaitEv@PLT
.L27:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%r12), %rax
	cmpq	120(%r12), %rax
	je	.L20
	movq	(%rax), %rbx
	movq	$0, (%rax)
	movq	104(%r12), %rax
	movq	88(%r12), %rdx
	subq	$8, %rax
	movq	(%rdx), %rdi
	cmpq	%rax, %rdx
	je	.L21
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	addq	$8, 88(%r12)
.L23:
	movq	%rbx, (%r14)
.L25:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	movq	$0, (%r14)
	jmp	.L25
.L21:
	testq	%rdi, %rdi
	je	.L24
	movq	(%rdi), %rax
	call	*8(%rax)
.L24:
	movq	96(%r12), %rdi
	call	_ZdlPv@PLT
	movq	112(%r12), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 112(%r12)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%r12)
	movq	%rdx, 104(%r12)
	movq	%rax, 88(%r12)
	jmp	.L23
	.cfi_endproc
.LFE3829:
	.size	_ZN2v88platform9TaskQueue7GetNextEv, .-_ZN2v88platform9TaskQueue7GetNextEv
	.section	.text._ZN2v88platform9TaskQueue9TerminateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform9TaskQueue9TerminateEv
	.type	_ZN2v88platform9TaskQueue9TerminateEv, @function
_ZN2v88platform9TaskQueue9TerminateEv:
.LFB3830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, %rdi
	movb	$1, 152(%r12)
	call	_ZN2v84base9Semaphore6SignalEv@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE3830:
	.size	_ZN2v88platform9TaskQueue9TerminateEv, .-_ZN2v88platform9TaskQueue9TerminateEv
	.section	.text._ZN2v88platform9TaskQueue30BlockUntilQueueEmptyForTestingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform9TaskQueue30BlockUntilQueueEmptyForTestingEv
	.type	_ZN2v88platform9TaskQueue30BlockUntilQueueEmptyForTestingEv, @function
_ZN2v88platform9TaskQueue30BlockUntilQueueEmptyForTestingEv:
.LFB3831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L39:
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$5000, %edi
	call	_ZN2v84base2OS5SleepENS0_9TimeDeltaE@PLT
.L40:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rax
	movq	%r12, %rdi
	cmpq	%rax, 120(%rbx)
	jne	.L39
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE3831:
	.size	_ZN2v88platform9TaskQueue30BlockUntilQueueEmptyForTestingEv, .-_ZN2v88platform9TaskQueue30BlockUntilQueueEmptyForTestingEv
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.type	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, @function
_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_:
.LFB4357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L43
	.p2align 4,,10
	.p2align 3
.L48:
	movq	0(%r13), %rbx
	leaq	512(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L47
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L48
.L65:
	movq	24(%r15), %rdx
.L43:
	movq	(%r15), %rbx
	cmpq	%rax, %rdx
	je	.L49
	movq	16(%r15), %r12
	cmpq	%r12, %rbx
	je	.L54
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L50
.L54:
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	je	.L42
	.p2align 4,,10
	.p2align 3
.L52:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L52
.L42:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L47
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L48
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L52
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L50
	jmp	.L54
.L49:
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L42
.L60:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L58
.L66:
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	je	.L42
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L66
.L58:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L60
	jmp	.L42
	.cfi_endproc
.LFE4357:
	.size	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, .-_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.section	.text._ZN2v88platform9TaskQueueD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform9TaskQueueD2Ev
	.type	_ZN2v88platform9TaskQueueD2Ev, @function
_ZN2v88platform9TaskQueueD2Ev:
.LFB3826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movdqu	120(%r12), %xmm0
	leaq	72(%r12), %rdi
	movdqu	136(%r12), %xmm1
	movdqu	88(%r12), %xmm2
	movdqu	104(%r12), %xmm3
	leaq	-112(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm3, -64(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	144(%r12), %rax
	movq	112(%r12), %rbx
	leaq	8(%rax), %r14
	cmpq	%rbx, %r14
	jbe	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r14
	ja	.L70
	movq	72(%r12), %rdi
.L69:
	call	_ZdlPv@PLT
.L68:
	movq	%r13, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v84base9SemaphoreD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3826:
	.size	_ZN2v88platform9TaskQueueD2Ev, .-_ZN2v88platform9TaskQueueD2Ev
	.globl	_ZN2v88platform9TaskQueueD1Ev
	.set	_ZN2v88platform9TaskQueueD1Ev,_ZN2v88platform9TaskQueueD2Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
