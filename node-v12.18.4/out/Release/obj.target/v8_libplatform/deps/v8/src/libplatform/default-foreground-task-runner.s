	.file	"default-foreground-task-runner.cc"
	.text
	.section	.text._ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB3945:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3945:
	.size	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd,"axG",@progbits,_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.type	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd, @function
_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd:
.LFB3946:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3946:
	.size	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd, .-_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.section	.text._ZNK2v810TaskRunner23NonNestableTasksEnabledEv,"axG",@progbits,_ZNK2v810TaskRunner23NonNestableTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.type	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv, @function
_ZNK2v810TaskRunner23NonNestableTasksEnabledEv:
.LFB3947:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3947:
	.size	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv, .-_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.section	.text._ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv,"axG",@progbits,_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.type	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv, @function
_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv:
.LFB3948:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3948:
	.size	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv, .-_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner16IdleTasksEnabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner16IdleTasksEnabledEv
	.type	_ZN2v88platform27DefaultForegroundTaskRunner16IdleTasksEnabledEv, @function
_ZN2v88platform27DefaultForegroundTaskRunner16IdleTasksEnabledEv:
.LFB4746:
	.cfi_startproc
	endbr64
	cmpl	$1, 184(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE4746:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner16IdleTasksEnabledEv, .-_ZN2v88platform27DefaultForegroundTaskRunner16IdleTasksEnabledEv
	.section	.rodata._ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"IdleTaskSupport::kEnabled == idle_task_support_"
	.section	.rodata._ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE.str1.8
	.align 8
.LC2:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE
	.type	_ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE, @function
_ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE:
.LFB4744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$1, 184(%rdi)
	jne	.L20
	leaq	16(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 8(%rbx)
	jne	.L11
	movq	256(%rbx), %rcx
	movq	240(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L10
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	addq	$8, 240(%rbx)
.L11:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	264(%rbx), %r14
	movq	232(%rbx), %rsi
	movabsq	$1152921504606846975, %r8
	subq	248(%rbx), %rax
	movq	%r14, %r15
	sarq	$3, %rax
	subq	%rsi, %r15
	movq	%r15, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	224(%rbx), %rax
	subq	208(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%r8, %rax
	je	.L21
	movq	192(%rbx), %r9
	movq	200(%rbx), %rdx
	movq	%r14, %rax
	subq	%r9, %rax
	movq	%rdx, %rcx
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L22
.L13:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	240(%rbx), %rax
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	264(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 264(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 248(%rbx)
	movq	%rdx, 256(%rbx)
	movq	%rax, 240(%rbx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	2(%rdi), %r10
	leaq	(%r10,%r10), %rax
	cmpq	%rax, %rdx
	jbe	.L14
	subq	%r10, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r9,%rdx,8), %r8
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L15
	cmpq	%r14, %rsi
	je	.L16
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r8, 232(%rbx)
	movq	(%r8), %rax
	leaq	(%r8,%r15), %r14
	movq	(%r8), %xmm0
	movq	%r14, 264(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 216(%rbx)
	movq	(%r14), %rax
	movq	%rax, 248(%rbx)
	addq	$512, %rax
	movq	%rax, 256(%rbx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%r8, %r14
	ja	.L23
	leaq	0(,%r14,8), %rdi
	movq	%r10, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r10
	movq	232(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r10, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	264(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L18
	movq	%r8, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	%rax, %r8
.L18:
	movq	192(%rbx), %rdi
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 200(%rbx)
	movq	-64(%rbp), %r8
	movq	%rax, 192(%rbx)
	jmp	.L16
.L15:
	cmpq	%r14, %rsi
	je	.L16
	leaq	8(%r15), %rdi
	movq	%r8, -56(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
	jmp	.L16
.L23:
	call	_ZSt17__throw_bad_allocv@PLT
.L21:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4744:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE, .-_ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunnerC2ENS0_15IdleTaskSupportEPFdvE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunnerC2ENS0_15IdleTaskSupportEPFdvE
	.type	_ZN2v88platform27DefaultForegroundTaskRunnerC2ENS0_15IdleTaskSupportEPFdvE, @function
_ZN2v88platform27DefaultForegroundTaskRunnerC2ENS0_15IdleTaskSupportEPFdvE:
.LFB4732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform27DefaultForegroundTaskRunnerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	movq	%rax, -16(%rdi)
	movb	$0, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	56(%rbx), %rdi
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, 104(%rbx)
	movq	$8, 112(%rbx)
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	movups	%xmm0, 168(%rbx)
	call	_Znwm@PLT
	movq	112(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, 104(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 144(%rbx)
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	leaq	512(%rax), %rdx
	movq	%r12, 176(%rbx)
	movq	%rax, %xmm1
	movq	%rdx, 136(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	%rdx, 168(%rbx)
	movl	%r14d, 184(%rbx)
	movq	%rax, (%r12)
	movq	%rax, 160(%rbx)
	movq	%rax, 152(%rbx)
	movq	$0, 192(%rbx)
	movq	$8, 200(%rbx)
	movups	%xmm1, 120(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 224(%rbx)
	movups	%xmm0, 240(%rbx)
	movups	%xmm0, 256(%rbx)
	call	_Znwm@PLT
	movq	200(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, 192(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, 232(%rbx)
	movq	%rax, %xmm1
	leaq	512(%rax), %rdx
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm1
	movq	%r12, 264(%rbx)
	movq	%r13, 304(%rbx)
	movq	%rdx, 224(%rbx)
	movq	%rax, 248(%rbx)
	movq	%rdx, 256(%rbx)
	movq	%rax, 240(%rbx)
	movq	$0, 288(%rbx)
	movups	%xmm1, 208(%rbx)
	movups	%xmm0, 272(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4732:
	.size	_ZN2v88platform27DefaultForegroundTaskRunnerC2ENS0_15IdleTaskSupportEPFdvE, .-_ZN2v88platform27DefaultForegroundTaskRunnerC2ENS0_15IdleTaskSupportEPFdvE
	.globl	_ZN2v88platform27DefaultForegroundTaskRunnerC1ENS0_15IdleTaskSupportEPFdvE
	.set	_ZN2v88platform27DefaultForegroundTaskRunnerC1ENS0_15IdleTaskSupportEPFdvE,_ZN2v88platform27DefaultForegroundTaskRunnerC2ENS0_15IdleTaskSupportEPFdvE
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner27MonotonicallyIncreasingTimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner27MonotonicallyIncreasingTimeEv
	.type	_ZN2v88platform27DefaultForegroundTaskRunner27MonotonicallyIncreasingTimeEv, @function
_ZN2v88platform27DefaultForegroundTaskRunner27MonotonicallyIncreasingTimeEv:
.LFB4737:
	.cfi_startproc
	endbr64
	jmp	*304(%rdi)
	.cfi_endproc
.LFE4737:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner27MonotonicallyIncreasingTimeEv, .-_ZN2v88platform27DefaultForegroundTaskRunner27MonotonicallyIncreasingTimeEv
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner20PopTaskFromIdleQueueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner20PopTaskFromIdleQueueEv
	.type	_ZN2v88platform27DefaultForegroundTaskRunner20PopTaskFromIdleQueueEv, @function
_ZN2v88platform27DefaultForegroundTaskRunner20PopTaskFromIdleQueueEv:
.LFB4749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	16(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	208(%rbx), %rax
	cmpq	240(%rbx), %rax
	je	.L41
	movq	(%rax), %r14
	movq	$0, (%rax)
	movq	224(%rbx), %rax
	movq	208(%rbx), %rdx
	subq	$8, %rax
	movq	(%rdx), %rdi
	cmpq	%rax, %rdx
	je	.L30
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	call	*8(%rax)
.L31:
	addq	$8, 208(%rbx)
.L32:
	movq	%r14, (%r12)
.L29:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	$0, (%r12)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	testq	%rdi, %rdi
	je	.L33
	movq	(%rdi), %rax
	call	*8(%rax)
.L33:
	movq	216(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	232(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 232(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 216(%rbx)
	movq	%rdx, 224(%rbx)
	movq	%rax, 208(%rbx)
	jmp	.L32
	.cfi_endproc
.LFE4749:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner20PopTaskFromIdleQueueEv, .-_ZN2v88platform27DefaultForegroundTaskRunner20PopTaskFromIdleQueueEv
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner17WaitForTaskLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner17WaitForTaskLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE
	.type	_ZN2v88platform27DefaultForegroundTaskRunner17WaitForTaskLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE, @function
_ZN2v88platform27DefaultForegroundTaskRunner17WaitForTaskLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE:
.LFB4750:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rsi
	addq	$56, %rdi
	jmp	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	.cfi_endproc
.LFE4750:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner17WaitForTaskLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE, .-_ZN2v88platform27DefaultForegroundTaskRunner17WaitForTaskLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.type	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, @function
_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_:
.LFB5482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movq	0(%r13), %rbx
	leaq	512(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L48
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L49
.L66:
	movq	24(%r15), %rdx
.L44:
	movq	(%r15), %rbx
	cmpq	%rax, %rdx
	je	.L50
	movq	16(%r15), %r12
	cmpq	%r12, %rbx
	je	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L51
.L55:
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	je	.L43
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L53
.L43:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L48
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L49
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L53
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L51
	jmp	.L55
.L50:
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L43
.L61:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L59
.L67:
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	je	.L43
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L67
.L59:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L61
	jmp	.L43
	.cfi_endproc
.LFE5482:
	.size	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, .-_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.type	_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, @function
_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_:
.LFB5491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L69
	.p2align 4,,10
	.p2align 3
.L74:
	movq	0(%r13), %rbx
	leaq	512(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L73
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L74
.L91:
	movq	24(%r15), %rdx
.L69:
	movq	(%r15), %rbx
	cmpq	%rax, %rdx
	je	.L75
	movq	16(%r15), %r12
	cmpq	%r12, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L79
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L76
.L80:
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	je	.L68
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L78
.L68:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L73
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L74
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L78
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L76
	jmp	.L80
.L75:
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L68
.L86:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L84
.L92:
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	je	.L68
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L92
.L84:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L86
	jmp	.L68
	.cfi_endproc
.LFE5491:
	.size	_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, .-_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunnerD2Ev,"axG",@progbits,_ZN2v88platform27DefaultForegroundTaskRunnerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88platform27DefaultForegroundTaskRunnerD2Ev
	.type	_ZN2v88platform27DefaultForegroundTaskRunnerD2Ev, @function
_ZN2v88platform27DefaultForegroundTaskRunnerD2Ev:
.LFB5900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	280(%rdi), %r13
	movq	272(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88platform27DefaultForegroundTaskRunnerE(%rip), %rax
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L94
	.p2align 4,,10
	.p2align 3
.L98:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L95
	movq	(%rdi), %rax
	addq	$16, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L98
.L96:
	movq	272(%rbx), %r12
.L94:
	testq	%r12, %r12
	je	.L99
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L99:
	movdqu	240(%rbx), %xmm0
	leaq	-128(%rbp), %r15
	leaq	-96(%rbp), %r14
	movdqu	256(%rbx), %xmm1
	movdqu	208(%rbx), %xmm2
	leaq	192(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	movdqu	224(%rbx), %xmm3
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L100
	movq	264(%rbx), %rax
	movq	232(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L102
	movq	192(%rbx), %rdi
.L101:
	call	_ZdlPv@PLT
.L100:
	movdqu	120(%rbx), %xmm6
	leaq	104(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	movdqu	152(%rbx), %xmm4
	movdqu	168(%rbx), %xmm5
	movdqu	136(%rbx), %xmm7
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm7, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L103
	movq	176(%rbx), %rax
	movq	144(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L104
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L105
	movq	104(%rbx), %rdi
.L104:
	call	_ZdlPv@PLT
.L103:
	leaq	56(%rbx), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	leaq	16(%rbx), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L98
	jmp	.L96
.L119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5900:
	.size	_ZN2v88platform27DefaultForegroundTaskRunnerD2Ev, .-_ZN2v88platform27DefaultForegroundTaskRunnerD2Ev
	.weak	_ZN2v88platform27DefaultForegroundTaskRunnerD1Ev
	.set	_ZN2v88platform27DefaultForegroundTaskRunnerD1Ev,_ZN2v88platform27DefaultForegroundTaskRunnerD2Ev
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunnerD0Ev,"axG",@progbits,_ZN2v88platform27DefaultForegroundTaskRunnerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88platform27DefaultForegroundTaskRunnerD0Ev
	.type	_ZN2v88platform27DefaultForegroundTaskRunnerD0Ev, @function
_ZN2v88platform27DefaultForegroundTaskRunnerD0Ev:
.LFB5902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	280(%rdi), %rbx
	movq	272(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88platform27DefaultForegroundTaskRunnerE(%rip), %rax
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L122
	movq	(%rdi), %rax
	addq	$16, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L125
.L123:
	movq	272(%r12), %r13
.L121:
	testq	%r13, %r13
	je	.L126
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L126:
	movdqu	240(%r12), %xmm0
	leaq	-128(%rbp), %r15
	movdqu	256(%r12), %xmm1
	leaq	-96(%rbp), %r14
	movdqu	208(%r12), %xmm2
	leaq	192(%r12), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	movdqu	224(%r12), %xmm3
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v88IdleTaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	264(%r12), %rax
	movq	232(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L129
	movq	192(%r12), %rdi
.L128:
	call	_ZdlPv@PLT
.L127:
	movdqu	152(%r12), %xmm4
	movq	%r15, %rdx
	movq	%r14, %rsi
	movdqu	168(%r12), %xmm5
	movdqu	120(%r12), %xmm6
	leaq	104(%r12), %rdi
	movdqu	136(%r12), %xmm7
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	104(%r12), %rdi
	testq	%rdi, %rdi
	je	.L130
	movq	176(%r12), %rax
	movq	144(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L132
	movq	104(%r12), %rdi
.L131:
	call	_ZdlPv@PLT
.L130:
	leaq	56(%r12), %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movl	$312, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	addq	$16, %r13
	cmpq	%r13, %rbx
	jne	.L125
	jmp	.L123
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5902:
	.size	_ZN2v88platform27DefaultForegroundTaskRunnerD0Ev, .-_ZN2v88platform27DefaultForegroundTaskRunnerD0Ev
	.section	.text._ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_,"axG",@progbits,_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_,comdat
	.p2align 4
	.weak	_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_
	.type	_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_, @function
_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_:
.LFB5527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rax, %r15
	pushq	%r14
	shrq	$63, %r15
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	addq	%rax, %r15
	pushq	%r12
	sarq	%r15
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	movsd	(%rcx), %xmm1
	cmpq	%rdx, %rsi
	jg	.L155
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L163:
	movsd	%xmm0, (%rsi)
	movq	8(%r13), %rax
	movq	$0, 8(%r13)
	movq	8(%rsi), %rdi
	movq	%rax, 8(%rsi)
	testq	%rdi, %rdi
	je	.L152
	movq	(%rdi), %rax
	call	*8(%rax)
	movsd	(%rbx), %xmm1
.L152:
	leaq	-1(%r15), %rdx
	movq	%r15, %rsi
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	cmpq	%r15, %r12
	jge	.L162
	movq	%rax, %r15
.L155:
	movq	%r15, %r13
	salq	$4, %rsi
	salq	$4, %r13
	addq	%r14, %rsi
	addq	%r14, %r13
	movsd	0(%r13), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L163
.L149:
	movq	8(%rbx), %rax
	movq	8(%rsi), %rdi
	movsd	%xmm1, (%rsi)
	movq	$0, 8(%rbx)
	movq	%rax, 8(%rsi)
	testq	%rdi, %rdi
	je	.L164
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	%r13, %rsi
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L164:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	salq	$4, %rsi
	addq	%rdi, %rsi
	jmp	.L149
	.cfi_endproc
.LFE5527:
	.size	_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_, .-_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_:
.LFB5653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1(%rdx), %rax
	movq	%rax, %rbx
	shrq	$63, %rbx
	addq	%rax, %rbx
	sarq	%rbx
	cmpq	%rbx, %rsi
	jge	.L174
	movq	%rsi, %rdx
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r14, %rdx
.L167:
	leaq	1(%rdx), %rax
	leaq	(%rax,%rax), %r14
	salq	$5, %rax
	leaq	-1(%r14), %rdi
	addq	%r15, %rax
	movq	%rdi, %rcx
	movsd	(%rax), %xmm0
	salq	$4, %rcx
	addq	%r15, %rcx
	movsd	(%rcx), %xmm1
	comisd	%xmm1, %xmm0
	jbe	.L168
	movq	%rcx, %rax
	movapd	%xmm1, %xmm0
	movq	%rdi, %r14
.L168:
	salq	$4, %rdx
	addq	%r15, %rdx
	movsd	%xmm0, (%rdx)
	movq	8(%rax), %rcx
	movq	$0, 8(%rax)
	movq	8(%rdx), %rdi
	movq	%rcx, 8(%rdx)
	testq	%rdi, %rdi
	je	.L170
	movq	(%rdi), %rax
	call	*8(%rax)
.L170:
	cmpq	%rbx, %r14
	jl	.L185
.L166:
	testb	$1, %r12b
	jne	.L171
	subq	$2, %r12
	movq	%r12, %rax
	shrq	$63, %rax
	addq	%rax, %r12
	sarq	%r12
	cmpq	%r14, %r12
	je	.L186
.L171:
	movq	8(%r13), %rax
	movsd	0(%r13), %xmm0
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	$0, 8(%r13)
	movq	-104(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	leaq	-81(%rbp), %r8
	movq	%rax, -72(%rbp)
	movsd	%xmm0, -80(%rbp)
	call	_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L165
	movq	(%rdi), %rax
	call	*8(%rax)
.L165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	leaq	1(%r14,%r14), %rdx
	movq	%r14, %rsi
	movq	%rdx, %rax
	salq	$4, %rsi
	movq	%rdx, %r14
	salq	$4, %rax
	addq	%r15, %rsi
	addq	%r15, %rax
	movsd	(%rax), %xmm0
	movsd	%xmm0, (%rsi)
	movq	8(%rax), %rcx
	movq	$0, 8(%rax)
	movq	8(%rsi), %rdi
	movq	%rcx, 8(%rsi)
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rsi, %r14
	jmp	.L166
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5653:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv
	.type	_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv, @function
_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv:
.LFB4734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movb	$1, 8(%rbx)
	movq	120(%rbx), %rax
	cmpq	152(%rbx), %rax
	jne	.L189
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L231:
	testq	%rdi, %rdi
	je	.L194
	movq	(%rdi), %rax
	call	*8(%rax)
.L194:
	movq	120(%rbx), %rax
	addq	$8, %rax
	movq	%rax, 120(%rbx)
	cmpq	%rax, 152(%rbx)
	je	.L192
.L189:
	movq	136(%rbx), %rsi
	movq	(%rax), %rdi
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rax
	jne	.L231
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	call	*8(%rax)
.L196:
	movq	128(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	144(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 144(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 128(%rbx)
	movq	%rdx, 136(%rbx)
	movq	%rax, 120(%rbx)
	cmpq	%rax, 152(%rbx)
	jne	.L189
	.p2align 4,,10
	.p2align 3
.L192:
	movq	280(%rbx), %rax
	movq	272(%rbx), %r13
	leaq	-80(%rbp), %r14
	cmpq	%r13, %rax
	je	.L191
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	$16, %rdx
	jg	.L232
.L199:
	leaq	-16(%rax), %rdx
	movq	%rdx, 280(%rbx)
	movq	-8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L202
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	280(%rbx), %rax
	movq	272(%rbx), %r13
	cmpq	%r13, %rax
	jne	.L190
.L191:
	movq	208(%rbx), %rax
	cmpq	%rax, 240(%rbx)
	jne	.L197
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L233:
	testq	%rdi, %rdi
	je	.L205
	movq	(%rdi), %rax
	call	*8(%rax)
.L205:
	movq	208(%rbx), %rax
	addq	$8, %rax
	movq	%rax, 208(%rbx)
	cmpq	%rax, 240(%rbx)
	je	.L198
.L197:
	movq	224(%rbx), %rcx
	movq	(%rax), %rdi
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L233
	testq	%rdi, %rdi
	je	.L207
	movq	(%rdi), %rax
	call	*8(%rax)
.L207:
	movq	216(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	232(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 232(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 216(%rbx)
	movq	%rdx, 224(%rbx)
	movq	%rax, 208(%rbx)
	cmpq	%rax, 240(%rbx)
	jne	.L197
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	-8(%rax), %rdx
	movq	$0, -8(%rax)
	leaq	-16(%rax), %r15
	movsd	0(%r13), %xmm1
	movsd	-16(%rax), %xmm0
	movsd	%xmm1, -16(%rax)
	movq	8(%r13), %rcx
	movq	$0, 8(%r13)
	movq	-8(%rax), %rdi
	movq	%rcx, -8(%rax)
	testq	%rdi, %rdi
	je	.L200
	movq	(%rdi), %rax
	movq	%rdx, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	call	*8(%rax)
	movq	-96(%rbp), %rdx
	movsd	-88(%rbp), %xmm0
.L200:
	movq	%rdx, -72(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r14, %rcx
	subq	%r13, %rdx
	xorl	%esi, %esi
	movsd	%xmm0, -80(%rbp)
	sarq	$4, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*8(%rax)
.L201:
	movq	280(%rbx), %rax
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L202:
	movq	272(%rbx), %r13
	movq	%rdx, %rax
	cmpq	%r13, %rdx
	jne	.L190
	jmp	.L191
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4734:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv, .-_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner29PopTaskFromDelayedQueueLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner29PopTaskFromDelayedQueueLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE
	.type	_ZN2v88platform27DefaultForegroundTaskRunner29PopTaskFromDelayedQueueLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE, @function
_ZN2v88platform27DefaultForegroundTaskRunner29PopTaskFromDelayedQueueLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE:
.LFB4748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	280(%rsi), %rax
	cmpq	%rax, 272(%rsi)
	je	.L257
	movq	%rsi, %rbx
	call	*304(%rsi)
	movq	272(%rbx), %rax
	movsd	(%rax), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L255
.L257:
	movq	$0, (%r12)
.L235:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	8(%rax), %r13
	movq	$0, 8(%rax)
	movq	280(%rbx), %rax
	movq	272(%rbx), %r14
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	$16, %rdx
	jg	.L259
.L240:
	leaq	-16(%rax), %rdx
	movq	%rdx, 280(%rbx)
	movq	-8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L243
	movq	(%rdi), %rax
	call	*8(%rax)
.L243:
	movq	%r13, (%r12)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L259:
	movq	-8(%rax), %rdx
	movq	$0, -8(%rax)
	leaq	-16(%rax), %r15
	movsd	(%r14), %xmm1
	movsd	-16(%rax), %xmm0
	movsd	%xmm1, -16(%rax)
	movq	8(%r14), %rcx
	movq	$0, 8(%r14)
	movq	-8(%rax), %rdi
	movq	%rcx, -8(%rax)
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	movq	%rdx, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	call	*8(%rax)
	movq	-96(%rbp), %rdx
	movsd	-88(%rbp), %xmm0
.L241:
	movq	%rdx, -72(%rbp)
	movq	%r15, %rdx
	movq	%r14, %rdi
	leaq	-80(%rbp), %rcx
	subq	%r14, %rdx
	xorl	%esi, %esi
	movsd	%xmm0, -80(%rbp)
	sarq	$4, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*8(%rax)
.L242:
	movq	280(%rbx), %rax
	jmp	.L240
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4748:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner29PopTaskFromDelayedQueueLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE, .-_ZN2v88platform27DefaultForegroundTaskRunner29PopTaskFromDelayedQueueLockedERKNS_4base9LockGuardINS2_5MutexELNS2_12NullBehaviorE0EEE
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	.type	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_, @function
_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_:
.LFB5659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L269
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L270
.L262:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L271
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L272
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L267
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L267:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L265:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L271:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L264
	cmpq	%r14, %rsi
	je	.L265
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L264:
	cmpq	%r14, %rsi
	je	.L265
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L265
.L269:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L272:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5659:
	.size	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_, .-_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner14PostTaskLockedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EERKNS_4base9LockGuardINS7_5MutexELNS7_12NullBehaviorE0EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner14PostTaskLockedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EERKNS_4base9LockGuardINS7_5MutexELNS7_12NullBehaviorE0EEE
	.type	_ZN2v88platform27DefaultForegroundTaskRunner14PostTaskLockedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EERKNS_4base9LockGuardINS7_5MutexELNS7_12NullBehaviorE0EEE, @function
_ZN2v88platform27DefaultForegroundTaskRunner14PostTaskLockedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EERKNS_4base9LockGuardINS7_5MutexELNS7_12NullBehaviorE0EEE:
.LFB4735:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	je	.L280
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	168(%rdi), %rax
	movq	152(%rdi), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L275
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, (%rdx)
	addq	$8, 152(%rdi)
.L276:
	addq	$8, %rsp
	leaq	56(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	leaq	104(%rdi), %rdi
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	jmp	.L276
	.cfi_endproc
.LFE4735:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner14PostTaskLockedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EERKNS_4base9LockGuardINS7_5MutexELNS7_12NullBehaviorE0EEE, .-_ZN2v88platform27DefaultForegroundTaskRunner14PostTaskLockedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EERKNS_4base9LockGuardINS7_5MutexELNS7_12NullBehaviorE0EEE
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.type	_ZN2v88platform27DefaultForegroundTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, @function
_ZN2v88platform27DefaultForegroundTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE:
.LFB4736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	(%r12), %rdi
	movq	$0, (%r12)
	cmpb	$0, 8(%rbx)
	movq	%rdi, -48(%rbp)
	je	.L291
.L282:
	testq	%rdi, %rdi
	je	.L285
	movq	(%rdi), %rax
	call	*8(%rax)
.L285:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	152(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L283
	movq	$0, -48(%rbp)
	movq	%rdi, (%rdx)
	addq	$8, 152(%rbx)
.L284:
	leaq	56(%rbx), %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	movq	-48(%rbp), %rdi
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	-48(%rbp), %rsi
	leaq	104(%rbx), %rdi
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	jmp	.L284
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4736:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, .-_ZN2v88platform27DefaultForegroundTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE
	.type	_ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE, @function
_ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE:
.LFB4747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	16(%rsi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movl	%edx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	272(%rbx), %rax
	cmpq	%rax, 280(%rbx)
	je	.L300
	call	*304(%rbx)
	movq	272(%rbx), %rax
	movsd	(%rax), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L356
	.p2align 4,,10
	.p2align 3
.L300:
	movq	120(%rbx), %rax
	cmpq	152(%rbx), %rax
	jne	.L302
.L354:
	cmpb	$0, -84(%rbp)
	leaq	56(%rbx), %r14
	je	.L357
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	movq	120(%rbx), %rax
	cmpq	%rax, 152(%rbx)
	je	.L315
.L302:
	movq	(%rax), %r14
	movq	$0, (%rax)
	movq	136(%rbx), %rax
	movq	120(%rbx), %rdx
	subq	$8, %rax
	movq	(%rdx), %rdi
	cmpq	%rax, %rdx
	je	.L316
	testq	%rdi, %rdi
	je	.L317
	movq	(%rdi), %rax
	call	*8(%rax)
.L317:
	addq	$8, 120(%rbx)
.L318:
	movq	%r14, 0(%r13)
	jmp	.L314
.L357:
	movq	$0, 0(%r13)
.L314:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	8(%rax), %r15
	movq	$0, 8(%rax)
	movq	280(%rbx), %rax
	movq	272(%rbx), %r14
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	$16, %rdx
	jg	.L359
.L295:
	leaq	-16(%rax), %rdx
	movq	%rdx, 280(%rbx)
	movq	-8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L298
	movq	(%rdi), %rax
	call	*8(%rax)
.L298:
	testq	%r15, %r15
	je	.L300
	leaq	56(%rbx), %r14
.L299:
	cmpb	$0, 8(%rbx)
	movq	%r15, -80(%rbp)
	jne	.L303
.L361:
	movq	168(%rbx), %rax
	movq	152(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L304
	movq	$0, -80(%rbp)
	movq	%r15, (%rdx)
	addq	$8, 152(%rbx)
.L305:
	movq	%r14, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	movq	-80(%rbp), %r15
	testq	%r15, %r15
	jne	.L303
.L306:
	movq	272(%rbx), %rax
	cmpq	%rax, 280(%rbx)
	je	.L300
	call	*304(%rbx)
	movq	272(%rbx), %rax
	movsd	(%rax), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L300
	movq	8(%rax), %r15
	movq	$0, 8(%rax)
	movq	280(%rbx), %rax
	movq	272(%rbx), %r9
	movq	%rax, %rdx
	subq	%r9, %rdx
	cmpq	$16, %rdx
	jg	.L360
.L307:
	leaq	-16(%rax), %rdx
	movq	%rdx, 280(%rbx)
	movq	-8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L310
	movq	(%rdi), %rax
	call	*8(%rax)
	testq	%r15, %r15
	jne	.L299
.L312:
	movq	120(%rbx), %rax
	cmpq	%rax, 152(%rbx)
	je	.L354
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L310:
	testq	%r15, %r15
	je	.L312
	cmpb	$0, 8(%rbx)
	movq	%r15, -80(%rbp)
	je	.L361
.L303:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	-80(%rbp), %rsi
	leaq	104(%rbx), %rdi
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-8(%rax), %rcx
	movq	$0, -8(%rax)
	leaq	-16(%rax), %rdx
	movsd	(%r9), %xmm1
	movsd	-16(%rax), %xmm0
	movsd	%xmm1, -16(%rax)
	movq	8(%r9), %rsi
	movq	$0, 8(%r9)
	movq	-8(%rax), %rdi
	movq	%rsi, -8(%rax)
	testq	%rdi, %rdi
	je	.L308
	movq	(%rdi), %rax
	movq	%rcx, -120(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r9, -96(%rbp)
	movsd	%xmm0, -112(%rbp)
	call	*8(%rax)
	movq	-120(%rbp), %rcx
	movsd	-112(%rbp), %xmm0
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %r9
.L308:
	subq	%r9, %rdx
	movq	%rcx, -72(%rbp)
	movq	%r9, %rdi
	leaq	-80(%rbp), %rcx
	sarq	$4, %rdx
	xorl	%esi, %esi
	movsd	%xmm0, -80(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L309
	movq	(%rdi), %rax
	call	*8(%rax)
.L309:
	movq	280(%rbx), %rax
	jmp	.L307
.L316:
	testq	%rdi, %rdi
	je	.L319
	movq	(%rdi), %rax
	call	*8(%rax)
.L319:
	movq	128(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	144(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 144(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 128(%rbx)
	movq	%rdx, 136(%rbx)
	movq	%rax, 120(%rbx)
	jmp	.L318
.L359:
	movq	-8(%rax), %rcx
	movq	$0, -8(%rax)
	leaq	-16(%rax), %rdx
	movsd	(%r14), %xmm1
	movsd	-16(%rax), %xmm0
	movsd	%xmm1, -16(%rax)
	movq	8(%r14), %rsi
	movq	$0, 8(%r14)
	movq	-8(%rax), %rdi
	movq	%rsi, -8(%rax)
	testq	%rdi, %rdi
	je	.L296
	movq	(%rdi), %rax
	movq	%rcx, -112(%rbp)
	movq	%rdx, -96(%rbp)
	movsd	%xmm0, -104(%rbp)
	call	*8(%rax)
	movq	-112(%rbp), %rcx
	movsd	-104(%rbp), %xmm0
	movq	-96(%rbp), %rdx
.L296:
	subq	%r14, %rdx
	movq	%rcx, -72(%rbp)
	movq	%r14, %rdi
	leaq	-80(%rbp), %rcx
	sarq	$4, %rdx
	xorl	%esi, %esi
	movsd	%xmm0, -80(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops15_Iter_comp_iterINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_T2_
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L297
	movq	(%rdi), %rax
	call	*8(%rax)
.L297:
	movq	280(%rbx), %rax
	jmp	.L295
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4747:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE, .-_ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE
	.section	.rodata._ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB5661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L383
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L375
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L384
.L364:
	movq	%rsi, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, -64(%rbp)
	leaq	16(%rax), %rsi
.L374:
	movq	-56(%rbp), %r15
	movsd	(%rdx), %xmm0
	leaq	(%r15,%rcx), %rax
	movq	8(%rdx), %rcx
	movq	$0, 8(%rdx)
	movsd	%xmm0, (%rax)
	movq	%rcx, 8(%rax)
	cmpq	%r14, %rbx
	je	.L366
	movq	%r14, %r13
	.p2align 4,,10
	.p2align 3
.L370:
	movsd	0(%r13), %xmm0
	movsd	%xmm0, (%r15)
	movq	8(%r13), %rcx
	movq	$0, 8(%r13)
	movq	%rcx, 8(%r15)
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L367
	movq	(%rdi), %rcx
	addq	$16, %r13
	addq	$16, %r15
	call	*8(%rcx)
	cmpq	%r13, %rbx
	jne	.L370
.L368:
	movq	-56(%rbp), %rsi
	movq	%rbx, %rax
	subq	%r14, %rax
	leaq	16(%rsi,%rax), %rsi
.L366:
	cmpq	%r12, %rbx
	je	.L371
	movq	%rbx, %rax
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L372:
	movsd	(%rax), %xmm0
	movq	8(%rax), %rcx
	addq	$16, %rax
	addq	$16, %rdx
	movsd	%xmm0, -16(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%r12, %rax
	jne	.L372
	subq	%rbx, %rax
	addq	%rax, %rsi
.L371:
	testq	%r14, %r14
	je	.L373
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rsi
.L373:
	movq	-56(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%rsi, %xmm1
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm1, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	addq	$16, %r13
	addq	$16, %r15
	cmpq	%r13, %rbx
	jne	.L370
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L384:
	testq	%r8, %r8
	jne	.L365
	movq	$0, -64(%rbp)
	movl	$16, %esi
	movq	$0, -56(%rbp)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L375:
	movl	$16, %esi
	jmp	.L364
.L365:
	cmpq	%rdi, %r8
	movq	%rdi, %rsi
	cmovbe	%r8, %rsi
	salq	$4, %rsi
	jmp	.L364
.L383:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5661:
	.size	_ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN2v88platform27DefaultForegroundTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform27DefaultForegroundTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.type	_ZN2v88platform27DefaultForegroundTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, @function
_ZN2v88platform27DefaultForegroundTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd:
.LFB4738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$88, %rsp
	movsd	%xmm0, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 8(%rbx)
	jne	.L391
	call	*304(%rbx)
	addsd	-104(%rbp), %xmm0
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	280(%rbx), %rsi
	movq	%rax, -72(%rbp)
	movsd	%xmm0, -80(%rbp)
	cmpq	288(%rbx), %rsi
	je	.L388
	movsd	%xmm0, (%rsi)
	movq	-72(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	%rax, 8(%rsi)
	movq	280(%rbx), %rax
	leaq	16(%rax), %rsi
	movq	%rsi, 280(%rbx)
.L389:
	movq	-8(%rsi), %rax
	movsd	-16(%rsi), %xmm0
	leaq	-64(%rbp), %rcx
	xorl	%edx, %edx
	movq	272(%rbx), %rdi
	movq	$0, -8(%rsi)
	leaq	-81(%rbp), %r8
	movq	%rax, -56(%rbp)
	subq	%rdi, %rsi
	movsd	%xmm0, -64(%rbp)
	sarq	$4, %rsi
	subq	$1, %rsi
	call	_ZSt11__push_heapIN9__gnu_cxx17__normal_iteratorIPSt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS5_EEESt6vectorIS9_SaIS9_EEEElS9_NS0_5__ops14_Iter_comp_valINS4_8platform27DefaultForegroundTaskRunner19DelayedEntryCompareEEEEvT_T0_SM_T1_RT2_
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L390
	movq	(%rdi), %rax
	call	*8(%rax)
.L390:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L391
	movq	(%rdi), %rax
	call	*8(%rax)
.L391:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L400
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	leaq	-80(%rbp), %rdx
	leaq	272(%rbx), %rdi
	call	_ZNSt6vectorISt4pairIdSt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	movq	280(%rbx), %rsi
	jmp	.L389
.L400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4738:
	.size	_ZN2v88platform27DefaultForegroundTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, .-_ZN2v88platform27DefaultForegroundTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.weak	_ZTVN2v88platform27DefaultForegroundTaskRunnerE
	.section	.data.rel.ro.local._ZTVN2v88platform27DefaultForegroundTaskRunnerE,"awG",@progbits,_ZTVN2v88platform27DefaultForegroundTaskRunnerE,comdat
	.align 8
	.type	_ZTVN2v88platform27DefaultForegroundTaskRunnerE, @object
	.size	_ZTVN2v88platform27DefaultForegroundTaskRunnerE, 96
_ZTVN2v88platform27DefaultForegroundTaskRunnerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform27DefaultForegroundTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.quad	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	_ZN2v88platform27DefaultForegroundTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.quad	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.quad	_ZN2v88platform27DefaultForegroundTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE
	.quad	_ZN2v88platform27DefaultForegroundTaskRunner16IdleTasksEnabledEv
	.quad	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.quad	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.quad	_ZN2v88platform27DefaultForegroundTaskRunnerD1Ev
	.quad	_ZN2v88platform27DefaultForegroundTaskRunnerD0Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
