	.file	"tracing-controller.cc"
	.text
	.section	.text._ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.type	_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv, @function
_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv:
.LFB4192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4192:
	.size	_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv, .-_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.section	.text._ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv
	.type	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv, @function
_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv:
.LFB4193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v84base11ThreadTicks3NowEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4193:
	.size	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv, .-_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv
	.section	.text._ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.type	_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE, @function
_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE:
.LFB4205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	40(%rbx), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	32(%rbx), %r14
	divq	%rcx
	leaq	0(,%rdx,8), %rax
	leaq	(%r14,%rax), %r15
	movq	%rax, -56(%rbp)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L7
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	8(%rdi), %rsi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L7
	movq	8(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L7
	movq	%r9, %rdi
.L9:
	cmpq	%r12, %rsi
	jne	.L27
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L28
	testq	%rsi, %rsi
	je	.L11
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L11
	movq	%r10, (%r14,%rdx,8)
	movq	(%rdi), %rsi
.L11:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 56(%rbx)
.L7:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L15
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L11
	movq	%r10, (%r14,%rdx,8)
	movq	-56(%rbp), %r15
	addq	32(%rbx), %r15
	movq	(%r15), %rax
.L10:
	leaq	48(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L29
.L12:
	movq	$0, (%r15)
	movq	(%rdi), %rsi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r10, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, 48(%rbx)
	jmp	.L12
	.cfi_endproc
.LFE4205:
	.size	_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE, .-_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.section	.text._ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl
	.type	_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl, @function
_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl:
.LFB4195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$88, %rsp
	movq	32(%rbp), %rax
	movl	%esi, -76(%rbp)
	movq	%rax, -88(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L31
	call	_ZN2v84base11ThreadTicks3NowEv@PLT
	movq	%rax, -72(%rbp)
.L32:
	movq	$0, -64(%rbp)
	movzbl	88(%r15), %eax
	testb	%al, %al
	je	.L34
	movq	8(%r15), %rdi
	leaq	-64(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	testq	%rax, %rax
	movq	%rax, -120(%rbp)
	je	.L34
	movq	24(%r15), %r10
	movq	%r10, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	64(%rbp), %edx
	subq	$8, %rsp
	pushq	-72(%rbp)
	pushq	72(%rbp)
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	-120(%rbp), %rax
	pushq	%rdx
	movl	24(%rbp), %edx
	movsbl	-76(%rbp), %esi
	pushq	-112(%rbp)
	pushq	-104(%rbp)
	movq	%rax, %rdi
	pushq	-96(%rbp)
	pushq	-88(%rbp)
	pushq	%rdx
	movq	%r12, %rdx
	pushq	16(%rbp)
	call	_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll@PLT
	movq	-128(%rbp), %r10
	addq	$80, %rsp
	movq	%r10, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
.L34:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-64(%rbp), %rax
	jne	.L41
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	*%rax
	movq	%rax, -72(%rbp)
	jmp	.L32
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4195:
	.size	_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl, .-_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl
	.section	.text._ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L43
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, %r13
.L44:
	movq	(%rbx), %rax
	leaq	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L45
	call	_ZN2v84base11ThreadTicks3NowEv@PLT
	movq	%rax, %r14
.L46:
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L42
	popq	%rbx
	movq	%r14, %rdx
	popq	%r12
	movq	%r13, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88platform7tracing11TraceObject14UpdateDurationEll@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	movq	%rax, %r14
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L43:
	call	*%rax
	movq	%rax, %r13
	jmp	.L44
	.cfi_endproc
.LFE4196:
	.size	_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj
	.type	_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj, @function
_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj:
.LFB4194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$88, %rsp
	movq	32(%rbp), %rax
	movq	%rdx, -80(%rbp)
	leaq	_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv(%rip), %rdx
	movq	%rax, -88(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L50
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	movq	%rax, -72(%rbp)
.L51:
	movq	(%r12), %rax
	leaq	_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl(%rip), %rdx
	movsbl	%bl, %ebx
	movq	32(%rax), %r11
	cmpq	%rdx, %r11
	jne	.L52
	movq	72(%rax), %rax
	leaq	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L53
	call	_ZN2v84base11ThreadTicks3NowEv@PLT
	movq	%rax, -120(%rbp)
.L54:
	movq	$0, -64(%rbp)
	movzbl	88(%r12), %eax
	testb	%al, %al
	jne	.L64
.L56:
	movq	-64(%rbp), %rax
.L49:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L65
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	8(%r12), %rdi
	leaq	-64(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	je	.L56
	movq	24(%r12), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	64(%rbp), %eax
	subq	$8, %rsp
	pushq	-120(%rbp)
	pushq	-72(%rbp)
	movq	%r15, %r9
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	-80(%rbp), %rdx
	pushq	%rax
	movl	%ebx, %esi
	movl	24(%rbp), %eax
	pushq	-112(%rbp)
	pushq	-104(%rbp)
	pushq	-96(%rbp)
	pushq	-88(%rbp)
	pushq	%rax
	movq	-128(%rbp), %rax
	pushq	16(%rbp)
	movq	%rax, %rdi
	call	_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll@PLT
	addq	$80, %rsp
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L52:
	movl	64(%rbp), %eax
	pushq	-72(%rbp)
	movq	%r15, %r9
	movq	%r14, %r8
	movq	-80(%rbp), %rdx
	movq	%r13, %rcx
	movl	%ebx, %esi
	movq	%r12, %rdi
	pushq	%rax
	movl	24(%rbp), %eax
	pushq	-112(%rbp)
	pushq	-104(%rbp)
	pushq	-96(%rbp)
	pushq	-88(%rbp)
	pushq	%rax
	pushq	16(%rbp)
	call	*%r11
	addq	$64, %rsp
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L50:
	call	*%rax
	movq	%rax, -72(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, -120(%rbp)
	jmp	.L54
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4194:
	.size	_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj, .-_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj
	.section	.rodata._ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"__metadata"
	.section	.text._ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc:
.LFB4203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	_ZN2v88platform7tracing16g_category_indexE(%rip), %r13
	testq	%r13, %r13
	je	.L67
	xorl	%ebx, %ebx
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %r12
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L68:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L67
.L70:
	movq	(%r12,%rbx,8), %rdi
	movq	%r14, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L68
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %rax
	addq	%rax, %rbx
.L66:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	24(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	_ZN2v88platform7tracing16g_category_indexE(%rip), %r13
	testq	%r13, %r13
	je	.L71
	xorl	%ebx, %ebx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L93
.L74:
	movq	(%r12,%rbx,8), %rdi
	movq	%r14, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L72
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %rax
	addq	%rax, %rbx
.L73:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	1+_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %rbx
	cmpq	$200, %r13
	je	.L73
.L71:
	movq	%r14, %rdi
	call	strdup@PLT
	movq	%rax, (%r12,%r13,8)
	movq	%rax, %rbx
	movq	-56(%rbp), %rax
	movzbl	88(%rax), %eax
	testb	%al, %al
	jne	.L94
.L75:
	movq	-56(%rbp), %rcx
	movzbl	88(%rcx), %edx
	testb	%dl, %dl
	jne	.L76
	movzbl	%al, %eax
.L77:
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %rbx
	addq	%r13, %rbx
	addq	$1, %r13
	movb	%al, (%rbx)
	movq	%r13, _ZN2v88platform7tracing16g_category_indexE(%rip)
	jmp	.L73
.L76:
	movl	$11, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%rbx, %rsi
	movzbl	%al, %eax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	movl	$1, %edx
	cmove	%edx, %eax
	jmp	.L77
.L94:
	movq	-56(%rbp), %rax
	movq	%rbx, %rsi
	movq	16(%rax), %rdi
	call	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc@PLT
	jmp	.L75
	.cfi_endproc
.LFE4203:
	.size	_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v88platform7tracing17TracingControllerC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingControllerC2Ev
	.type	_ZN2v88platform7tracing17TracingControllerC2Ev, @function
_ZN2v88platform7tracing17TracingControllerC2Ev:
.LFB4185:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88platform7tracing17TracingControllerE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	80(%rdi), %rax
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rax, 32(%rdi)
	movq	$1, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movl	$0x3f800000, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movb	$0, 88(%rdi)
	ret
	.cfi_endproc
.LFE4185:
	.size	_ZN2v88platform7tracing17TracingControllerC2Ev, .-_ZN2v88platform7tracing17TracingControllerC2Ev
	.globl	_ZN2v88platform7tracing17TracingControllerC1Ev
	.set	_ZN2v88platform7tracing17TracingControllerC1Ev,_ZN2v88platform7tracing17TracingControllerC2Ev
	.section	.text._ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE
	.type	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE, @function
_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE:
.LFB4191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	%rsi, 8(%rbx)
	testq	%rdi, %rdi
	je	.L97
	movq	(%rdi), %rax
	call	*8(%rax)
.L97:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	24(%rbx), %r13
	movq	%r12, 24(%rbx)
	testq	%r13, %r13
	je	.L96
	movq	%r13, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4191:
	.size	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE, .-_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE
	.section	.text._ZN2v88platform7tracing17TracingController20GetCategoryGroupNameEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController20GetCategoryGroupNameEPKh
	.type	_ZN2v88platform7tracing17TracingController20GetCategoryGroupNameEPKh, @function
_ZN2v88platform7tracing17TracingController20GetCategoryGroupNameEPKh:
.LFB4197:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %rax
	subq	%rax, %rdi
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.cfi_endproc
.LFE4197:
	.size	_ZN2v88platform7tracing17TracingController20GetCategoryGroupNameEPKh, .-_ZN2v88platform7tracing17TracingController20GetCategoryGroupNameEPKh
	.section	.text._ZN2v88platform7tracing17TracingController30UpdateCategoryGroupEnabledFlagEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController30UpdateCategoryGroupEnabledFlagEm
	.type	_ZN2v88platform7tracing17TracingController30UpdateCategoryGroupEnabledFlagEm, @function
_ZN2v88platform7tracing17TracingController30UpdateCategoryGroupEnabledFlagEm:
.LFB4201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rax,%rsi,8), %r13
	movzbl	88(%rdi), %eax
	testb	%al, %al
	jne	.L113
.L105:
	movzbl	88(%rbx), %edx
	testb	%dl, %dl
	jne	.L106
	movzbl	%al, %eax
.L107:
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %rdx
	movb	%al, (%rdx,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	$11, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%r13, %rsi
	movzbl	%al, %eax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	movl	$1, %edx
	cmove	%edx, %eax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L113:
	movq	16(%rdi), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc@PLT
	jmp	.L105
	.cfi_endproc
.LFE4201:
	.size	_ZN2v88platform7tracing17TracingController30UpdateCategoryGroupEnabledFlagEm, .-_ZN2v88platform7tracing17TracingController30UpdateCategoryGroupEnabledFlagEm
	.section	.text._ZN2v88platform7tracing17TracingController31UpdateCategoryGroupEnabledFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController31UpdateCategoryGroupEnabledFlagsEv
	.type	_ZN2v88platform7tracing17TracingController31UpdateCategoryGroupEnabledFlagsEv, @function
_ZN2v88platform7tracing17TracingController31UpdateCategoryGroupEnabledFlagsEv:
.LFB4202:
	.cfi_startproc
	endbr64
	movq	_ZN2v88platform7tracing16g_category_indexE(%rip), %rax
	testq	%rax, %rax
	je	.L126
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	88(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %r12
	pushq	%rbx
	leaq	(%r12,%rax,8), %r15
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L131:
	movzbl	%al, %eax
.L118:
	movb	%al, 0(%r13)
	addq	$8, %r12
	addq	$1, %r13
	cmpq	%r15, %r12
	je	.L129
.L120:
	movq	(%r12), %rsi
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L130
.L116:
	movzbl	(%r14), %ecx
	testb	%cl, %cl
	je	.L131
	movl	$11, %ecx
	leaq	.LC0(%rip), %rdi
	movzbl	%al, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	movl	$1, %ecx
	cmove	%ecx, %eax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L130:
	movq	16(%rbx), %rdi
	movq	%rsi, -56(%rbp)
	call	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc@PLT
	movq	-56(%rbp), %rsi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L129:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE4202:
	.size	_ZN2v88platform7tracing17TracingController31UpdateCategoryGroupEnabledFlagsEv, .-_ZN2v88platform7tracing17TracingController31UpdateCategoryGroupEnabledFlagsEv
	.section	.text._ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_,"axG",@progbits,_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_
	.type	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_, @function
_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_:
.LFB4845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rdx
	movq	(%rdi), %r13
	cmpq	8(%rdi), %rdx
	je	.L133
	cmpq	$1, %rdx
	je	.L178
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L142
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %rcx
.L135:
	movq	%rcx, (%r12)
	movq	8(%r14), %rax
	movq	16(%r12), %rbx
	movq	%rax, 8(%r12)
	movq	24(%r14), %rax
	movq	%rax, 24(%r12)
	movdqu	32(%r14), %xmm0
	movq	$0, 16(%r12)
	movups	%xmm0, 32(%r12)
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L137
.L138:
	testq	%rbx, %rbx
	je	.L143
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rbx, %r15
	movq	8(%r14), %rdx
	movq	%rdx, 8(%rbx)
	movq	%rax, %rbx
.L144:
	movq	%r15, 16(%r12)
	movq	8(%r15), %rax
	xorl	%edx, %edx
	leaq	16(%r12), %rcx
	divq	8(%r12)
	movq	(%r12), %rax
	movq	%rcx, (%rax,%rdx,8)
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L150
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L137
.L149:
	movq	%rcx, %r15
.L150:
	testq	%rbx, %rbx
	je	.L145
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rbx, %rcx
	movq	8(%r14), %rdx
	movq	%rdx, 8(%rbx)
	movq	%rax, %rbx
.L146:
	movq	%rcx, (%r15)
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	(%r12), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpq	$0, (%rax)
	jne	.L147
	movq	%r15, (%rax)
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L149
	.p2align 4,,10
	.p2align 3
.L137:
	testq	%r13, %r13
	je	.L151
	addq	$48, %r12
	cmpq	%r12, %r13
	je	.L151
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L151:
	testq	%rbx, %rbx
	je	.L132
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L152
.L132:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	8(%r14), %rax
	movq	%rax, 8(%r15)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	8(%r14), %rax
	movq	%rax, 8(%rcx)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L133:
	salq	$3, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	memset@PLT
	movq	24(%r14), %rax
	cmpq	$0, (%r12)
	movq	16(%r12), %rbx
	movq	%rax, 24(%r12)
	movdqu	32(%r14), %xmm1
	movq	$0, 16(%r12)
	movups	%xmm1, 32(%r12)
	jne	.L139
	movq	8(%r12), %rdx
	cmpq	$1, %rdx
	je	.L179
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L142
	leaq	0(,%rdx,8), %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %rcx
.L141:
	movq	%rcx, (%r12)
.L139:
	movq	16(%r14), %r14
	xorl	%r13d, %r13d
	testq	%r14, %r14
	jne	.L138
	jmp	.L151
.L178:
	movq	$0, 48(%rdi)
	leaq	48(%rdi), %rcx
	jmp	.L135
.L179:
	movq	$0, 48(%r12)
	leaq	48(%r12), %rcx
	jmp	.L141
.L142:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4845:
	.size	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_, .-_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_
	.section	.text._ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE
	.type	_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE, @function
_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE:
.LFB4198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	16(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 16(%rdi)
	testq	%r14, %r14
	je	.L181
	movq	16(%r14), %r13
	movq	8(%r14), %r12
	cmpq	%r12, %r13
	je	.L182
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L183
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L186
	movq	8(%r14), %r12
.L182:
	testq	%r12, %r12
	je	.L187
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L187:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L181:
	leaq	-64(%rbp), %rax
	movq	$1, -104(%rbp)
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %r15
	movq	%rax, -112(%rbp)
	leaq	88(%rbx), %r14
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %r12
	movq	%rax, -152(%rbp)
	movq	24(%rbx), %rax
	movq	$0, -96(%rbp)
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movb	$1, 88(%rbx)
	movq	_ZN2v88platform7tracing16g_category_indexE(%rip), %rax
	leaq	(%r15,%rax,8), %r13
	testq	%rax, %rax
	jne	.L195
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L222:
	movzbl	%al, %eax
.L193:
	movb	%al, (%r12)
	addq	$8, %r15
	addq	$1, %r12
	cmpq	%r15, %r13
	je	.L194
.L195:
	movq	(%r15), %rsi
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L221
.L191:
	movzbl	(%r14), %ecx
	testb	%cl, %cl
	je	.L222
	movl	$11, %ecx
	leaq	.LC0(%rip), %rdi
	movzbl	%al, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	movl	$1, %ecx
	cmove	%ecx, %eax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L221:
	movq	16(%rbx), %rdi
	movq	%rsi, -136(%rbp)
	call	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc@PLT
	movq	-136(%rbp), %rsi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L183:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L186
	movq	8(%r14), %r12
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	32(%rbx), %rsi
	leaq	-113(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	call	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_
	movq	-144(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L190
	.p2align 4,,10
	.p2align 3
.L189:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L189
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L190
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L198
.L190:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, -88(%rbp)
	movq	-112(%rbp), %rdi
	movq	$0, -96(%rbp)
	cmpq	-152(%rbp), %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L223:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4198:
	.size	_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE, .-_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE
	.section	.text._ZN2v88platform7tracing17TracingController11StopTracingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController11StopTracingEv
	.type	_ZN2v88platform7tracing17TracingController11StopTracingEv, @function
_ZN2v88platform7tracing17TracingController11StopTracingEv:
.LFB4200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	88(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	movb	$1, -114(%rbp)
	lock cmpxchgb	%dl, (%r15)
	je	.L226
	movb	%al, -114(%rbp)
.L224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	_ZN2v88platform7tracing16g_category_indexE(%rip), %rax
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %r13
	movq	%rdi, %rbx
	leaq	_ZN2v88platform7tracing24g_category_group_enabledE(%rip), %r14
	leaq	0(%r13,%rax,8), %r12
	testq	%rax, %rax
	jne	.L234
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L257:
	movzbl	%al, %eax
.L232:
	movb	%al, (%r14)
	addq	$8, %r13
	addq	$1, %r14
	cmpq	%r13, %r12
	je	.L233
.L234:
	movq	0(%r13), %rsi
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L256
.L230:
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	je	.L257
	movl	$11, %ecx
	leaq	.LC0(%rip), %rdi
	movzbl	%al, %eax
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	movl	$1, %ecx
	cmove	%ecx, %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L256:
	movq	16(%rbx), %rdi
	movq	%rsi, -136(%rbp)
	call	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc@PLT
	movq	-136(%rbp), %rsi
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L233:
	movq	24(%rbx), %r12
	leaq	-112(%rbp), %r14
	leaq	-64(%rbp), %r13
	movq	$1, -104(%rbp)
	movq	%r13, -112(%rbp)
	movq	%r12, %rdi
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	-113(%rbp), %rdx
	leaq	32(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE18_M_assign_elementsIRKSG_ZNSG_aSESJ_EUlRKNS5_17_ReuseOrAllocNodeISaINS5_10_Hash_nodeIS3_Lb0EEEEEEPKSM_E0_EEvOT_RKT0_
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L229
	.p2align 4,,10
	.p2align 3
.L228:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L228
.L229:
	movq	24(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L235
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L236
.L235:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-112(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	$0, -96(%rbp)
	cmpq	%r13, %rdi
	je	.L224
	call	_ZdlPv@PLT
	jmp	.L224
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4200:
	.size	_ZN2v88platform7tracing17TracingController11StopTracingEv, .-_ZN2v88platform7tracing17TracingController11StopTracingEv
	.section	.text._ZN2v88platform7tracing17TracingControllerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingControllerD2Ev
	.type	_ZN2v88platform7tracing17TracingControllerD2Ev, @function
_ZN2v88platform7tracing17TracingControllerD2Ev:
.LFB4188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform7tracing17TracingControllerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	call	_ZN2v88platform7tracing17TracingController11StopTracingEv
	movq	24(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	_ZN2v88platform7tracing16g_category_indexE(%rip), %rax
	leaq	_ZN2v88platform7tracing17g_category_groupsE(%rip), %rcx
	leaq	24(%rcx), %r12
	leaq	-1(%rax), %rdx
	leaq	(%rcx,%rax,8), %rbx
	cmpq	$2, %rdx
	jbe	.L262
	.p2align 4,,10
	.p2align 3
.L263:
	movq	-8(%rbx), %rdi
	movq	$0, -8(%rbx)
	subq	$8, %rbx
	call	free@PLT
	cmpq	%rbx, %r12
	jne	.L263
.L262:
	movq	$3, _ZN2v88platform7tracing16g_category_indexE(%rip)
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	48(%r13), %rbx
	testq	%rbx, %rbx
	je	.L260
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L261
.L260:
	movq	40(%r13), %rax
	movq	32(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	32(%r13), %rdi
	leaq	80(%r13), %rax
	movq	$0, 56(%r13)
	movq	$0, 48(%r13)
	cmpq	%rax, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L266
	movq	%r12, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L266:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L267
	movq	16(%r14), %rbx
	movq	8(%r14), %r12
	cmpq	%r12, %rbx
	je	.L268
	.p2align 4,,10
	.p2align 3
.L272:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L269
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L272
	movq	8(%r14), %r12
.L268:
	testq	%r12, %r12
	je	.L273
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L273:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L267:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L258
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	movq	8(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L272
	movq	8(%r14), %r12
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L258:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4188:
	.size	_ZN2v88platform7tracing17TracingControllerD2Ev, .-_ZN2v88platform7tracing17TracingControllerD2Ev
	.globl	_ZN2v88platform7tracing17TracingControllerD1Ev
	.set	_ZN2v88platform7tracing17TracingControllerD1Ev,_ZN2v88platform7tracing17TracingControllerD2Ev
	.section	.text._ZN2v88platform7tracing17TracingControllerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingControllerD0Ev
	.type	_ZN2v88platform7tracing17TracingControllerD0Ev, @function
_ZN2v88platform7tracing17TracingControllerD0Ev:
.LFB4190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88platform7tracing17TracingControllerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4190:
	.size	_ZN2v88platform7tracing17TracingControllerD0Ev, .-_ZN2v88platform7tracing17TracingControllerD0Ev
	.section	.text._ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB5067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L297
	movq	(%rbx), %r8
.L298:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L307
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L308:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L321
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L322
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L300:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L302
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L304:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L305:
	testq	%rsi, %rsi
	je	.L302
.L303:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L304
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L310
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L303
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L306
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L306:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L307:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L309
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L309:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rdx, %rdi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L321:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L300
.L322:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5067:
	.size	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.text._ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.type	_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE, @function
_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE:
.LFB4204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	40(%rbx), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	32(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L324
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L336:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L324
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r13
	jne	.L324
.L326:
	cmpq	%r12, %rdi
	jne	.L336
.L325:
	movzbl	88(%rbx), %eax
	movq	%r14, %rdi
	testb	%al, %al
	je	.L337
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	16(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	32(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v817TracingController18TraceStateObserverES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	jmp	.L325
	.cfi_endproc
.LFE4204:
	.size	_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE, .-_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.weak	_ZTVN2v88platform7tracing17TracingControllerE
	.section	.data.rel.ro.local._ZTVN2v88platform7tracing17TracingControllerE,"awG",@progbits,_ZTVN2v88platform7tracing17TracingControllerE,comdat
	.align 8
	.type	_ZTVN2v88platform7tracing17TracingControllerE, @object
	.size	_ZTVN2v88platform7tracing17TracingControllerE, 96
_ZTVN2v88platform7tracing17TracingControllerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform7tracing17TracingControllerD1Ev
	.quad	_ZN2v88platform7tracing17TracingControllerD0Ev
	.quad	_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc
	.quad	_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj
	.quad	_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl
	.quad	_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm
	.quad	_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.quad	_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.quad	_ZN2v88platform7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.quad	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv
	.globl	_ZN2v88platform7tracing16g_category_indexE
	.section	.data._ZN2v88platform7tracing16g_category_indexE,"aw"
	.align 8
	.type	_ZN2v88platform7tracing16g_category_indexE, @object
	.size	_ZN2v88platform7tracing16g_category_indexE, 8
_ZN2v88platform7tracing16g_category_indexE:
	.quad	3
	.globl	_ZN2v88platform7tracing24g_category_group_enabledE
	.section	.bss._ZN2v88platform7tracing24g_category_group_enabledE,"aw",@nobits
	.align 32
	.type	_ZN2v88platform7tracing24g_category_group_enabledE, @object
	.size	_ZN2v88platform7tracing24g_category_group_enabledE, 200
_ZN2v88platform7tracing24g_category_group_enabledE:
	.zero	200
	.globl	_ZN2v88platform7tracing17g_category_groupsE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"toplevel"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"tracing categories exhausted; must increase kMaxCategoryGroups"
	.section	.data.rel.local._ZN2v88platform7tracing17g_category_groupsE,"aw"
	.align 32
	.type	_ZN2v88platform7tracing17g_category_groupsE, @object
	.size	_ZN2v88platform7tracing17g_category_groupsE, 1600
_ZN2v88platform7tracing17g_category_groupsE:
	.quad	.LC2
	.quad	.LC3
	.quad	.LC0
	.zero	1576
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
