	.file	"trace-object.cc"
	.text
	.section	.text._ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll
	.type	_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll, @function
_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll:
.LFB4157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm1
	movq	%rcx, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$104, %rsp
	movq	32(%rbp), %r11
	movq	56(%rbp), %rax
	movq	%r8, -72(%rbp)
	movhps	16(%rbp), %xmm1
	movq	40(%rbp), %r14
	movq	48(%rbp), %r12
	movq	%rcx, -80(%rbp)
	movq	%r11, -136(%rbp)
	movq	72(%rbp), %xmm0
	movq	%rax, -144(%rbp)
	movhps	-72(%rbp), %xmm2
	movhps	80(%rbp), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movl	%eax, (%r15)
	call	_ZN2v84base2OS18GetCurrentThreadIdEv@PLT
	movdqa	-128(%rbp), %xmm0
	cmpl	$2, 24(%rbp)
	movb	%bl, 8(%r15)
	movl	%eax, 4(%r15)
	movl	64(%rbp), %eax
	movl	$2, %edx
	movdqa	-96(%rbp), %xmm2
	cmovle	24(%rbp), %edx
	movq	%r13, 32(%r15)
	movl	%eax, %ebx
	movl	%eax, 128(%r15)
	movdqa	-112(%rbp), %xmm1
	movl	24(%rbp), %eax
	movl	%edx, 56(%r15)
	movl	%edx, %r13d
	andl	$1, %ebx
	movups	%xmm0, 136(%r15)
	pxor	%xmm0, %xmm0
	movups	%xmm2, 16(%r15)
	movups	%xmm1, 40(%r15)
	movups	%xmm0, 152(%r15)
	testl	%eax, %eax
	jle	.L2
	movq	-136(%rbp), %r11
	movq	(%r11), %rax
	movq	%rax, 64(%r15)
	movq	(%r12), %rax
	movq	%rax, 88(%r15)
	movzbl	(%r14), %eax
	movb	%al, 80(%r15)
	cmpb	$8, (%r14)
	je	.L82
.L3:
	cmpl	$1, %r13d
	jle	.L5
.L86:
	movq	8(%r11), %rax
	movq	%rax, 72(%r15)
	movq	8(%r12), %rax
	movq	%rax, 96(%r15)
	movzbl	1(%r14), %eax
	movb	%al, 81(%r15)
	cmpb	$8, 1(%r14)
	je	.L83
.L5:
	testl	%ebx, %ebx
	jne	.L8
.L87:
	testl	%r13d, %r13d
	jle	.L1
	xorl	%r14d, %r14d
.L9:
	movzbl	80(%r15), %eax
	cmpb	$7, %al
	sete	-58(%rbp)
	jne	.L21
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
.L21:
	cmpl	$1, %r13d
	je	.L24
	movzbl	81(%r15), %eax
	cmpb	$7, %al
	sete	-57(%rbp)
	jne	.L24
	movq	96(%r15), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
.L24:
	testq	%r14, %r14
	je	.L1
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.L27
.L37:
	call	_ZdaPv@PLT
.L27:
	movq	%r14, %rdi
	call	_Znam@PLT
	movq	%rax, 120(%r15)
	movq	%rax, %r12
	testl	%ebx, %ebx
	jne	.L38
	movl	56(%r15), %r13d
	testl	%r13d, %r13d
	jle	.L1
.L32:
	xorl	%ebx, %ebx
	leaq	-58(%rbp), %rcx
.L35:
	cmpb	$0, (%rcx,%rbx)
	je	.L34
	movq	88(%r15,%rbx,8), %r14
	testq	%r14, %r14
	je	.L34
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	1(%rax), %rdx
	movq	%rdx, -72(%rbp)
	call	strncpy@PLT
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	%r12, 88(%r15,%rbx,8)
	addq	%rdx, %r12
.L34:
	addq	$1, %rbx
	cmpl	%ebx, %r13d
	jg	.L35
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testl	%ebx, %ebx
	je	.L1
.L8:
	cmpq	$0, -80(%rbp)
	je	.L85
	movq	-80(%rbp), %rdi
	call	strlen@PLT
	cmpq	$0, -72(%rbp)
	leaq	1(%rax), %r14
	je	.L14
.L11:
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
.L14:
	testl	%r13d, %r13d
	jle	.L15
.L13:
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
.L16:
	cmpb	$6, 80(%r15)
	jne	.L17
	movb	$7, 80(%r15)
.L17:
	cmpl	$1, %r13d
	je	.L9
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
.L19:
	cmpb	$6, 81(%r15)
	jne	.L9
	movb	$7, 81(%r15)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L82:
	movq	-144(%rbp), %rsi
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	104(%r15), %rdi
	movq	%rax, 104(%r15)
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	movq	%r11, -96(%rbp)
	call	*8(%rax)
	movl	56(%r15), %r13d
	movq	-96(%rbp), %r11
	cmpl	$1, %r13d
	jg	.L86
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L15:
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L37
	movq	%r14, %rdi
	call	_Znam@PLT
	movq	%rax, 120(%r15)
	movq	%rax, %r12
.L38:
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L29
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	1(%rax), %rbx
	movq	%rbx, %rdx
	call	strncpy@PLT
	movq	%r12, 16(%r15)
	addq	%rbx, %r12
.L29:
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L30
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	1(%rax), %rbx
	movq	%rbx, %rdx
	call	strncpy@PLT
	movq	%r12, 24(%r15)
	addq	%rbx, %r12
.L30:
	movl	56(%r15), %r13d
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	jle	.L1
.L31:
	movq	64(%r15,%rbx,8), %r14
	testq	%r14, %r14
	je	.L33
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	1(%rax), %rdx
	movq	%rdx, -72(%rbp)
	call	strncpy@PLT
	movq	-72(%rbp), %rdx
	movq	%r12, 64(%r15,%rbx,8)
	addq	%rdx, %r12
.L33:
	addq	$1, %rbx
	cmpl	%ebx, %r13d
	jg	.L31
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L83:
	movq	-144(%rbp), %rsi
	movq	8(%rsi), %rax
	movq	$0, 8(%rsi)
	movq	112(%r15), %rdi
	movq	%rax, 112(%r15)
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*8(%rax)
.L80:
	movl	56(%r15), %r13d
	testl	%ebx, %ebx
	jne	.L8
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L85:
	xorl	%r14d, %r14d
	cmpq	$0, -72(%rbp)
	jne	.L11
	testl	%r13d, %r13d
	jle	.L1
	movq	-80(%rbp), %r14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L4:
	movl	56(%r15), %r13d
	jmp	.L3
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4157:
	.size	_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll, .-_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll
	.section	.text._ZN2v88platform7tracing11TraceObjectD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceObjectD2Ev
	.type	_ZN2v88platform7tracing11TraceObjectD2Ev, @function
_ZN2v88platform7tracing11TraceObjectD2Ev:
.LFB4159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	_ZdaPv@PLT
.L89:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	call	*8(%rax)
.L90:
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L88
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4159:
	.size	_ZN2v88platform7tracing11TraceObjectD2Ev, .-_ZN2v88platform7tracing11TraceObjectD2Ev
	.globl	_ZN2v88platform7tracing11TraceObjectD1Ev
	.set	_ZN2v88platform7tracing11TraceObjectD1Ev,_ZN2v88platform7tracing11TraceObjectD2Ev
	.section	.text._ZN2v88platform7tracing11TraceObject14UpdateDurationEll,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceObject14UpdateDurationEll
	.type	_ZN2v88platform7tracing11TraceObject14UpdateDurationEll, @function
_ZN2v88platform7tracing11TraceObject14UpdateDurationEll:
.LFB4161:
	.cfi_startproc
	endbr64
	movdqu	136(%rdi), %xmm2
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	psubq	%xmm2, %xmm0
	movups	%xmm0, 152(%rdi)
	ret
	.cfi_endproc
.LFE4161:
	.size	_ZN2v88platform7tracing11TraceObject14UpdateDurationEll, .-_ZN2v88platform7tracing11TraceObject14UpdateDurationEll
	.section	.text._ZN2v88platform7tracing11TraceObject20InitializeForTestingEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjiillmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceObject20InitializeForTestingEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjiillmm
	.type	_ZN2v88platform7tracing11TraceObject20InitializeForTestingEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjiillmm, @function
_ZN2v88platform7tracing11TraceObject20InitializeForTestingEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjiillmm:
.LFB4162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movb	%sil, 8(%rdi)
	movq	%r8, -8(%rbp)
	movl	72(%rbp), %eax
	movq	%rdx, 32(%rdi)
	movl	%eax, (%rdi)
	movl	80(%rbp), %eax
	movhps	-8(%rbp), %xmm0
	movl	%eax, 4(%rdi)
	movl	24(%rbp), %eax
	movups	%xmm0, 16(%rdi)
	movq	%r9, %xmm0
	movhps	16(%rbp), %xmm0
	movl	%eax, 56(%rdi)
	movl	64(%rbp), %eax
	movups	%xmm0, 40(%rdi)
	movq	88(%rbp), %xmm0
	movl	%eax, 128(%rdi)
	movhps	96(%rbp), %xmm0
	movups	%xmm0, 136(%rdi)
	movq	104(%rbp), %xmm0
	movhps	112(%rbp), %xmm0
	movups	%xmm0, 152(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4162:
	.size	_ZN2v88platform7tracing11TraceObject20InitializeForTestingEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjiillmm, .-_ZN2v88platform7tracing11TraceObject20InitializeForTestingEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjiillmm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
