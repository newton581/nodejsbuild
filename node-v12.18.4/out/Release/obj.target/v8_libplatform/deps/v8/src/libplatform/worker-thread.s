	.file	"worker-thread.cc"
	.text
	.section	.text._ZN2v88platform12WorkerThreadD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform12WorkerThreadD2Ev
	.type	_ZN2v88platform12WorkerThreadD2Ev, @function
_ZN2v88platform12WorkerThreadD2Ev:
.LFB3710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform12WorkerThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v84base6Thread4JoinEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6ThreadD2Ev@PLT
	.cfi_endproc
.LFE3710:
	.size	_ZN2v88platform12WorkerThreadD2Ev, .-_ZN2v88platform12WorkerThreadD2Ev
	.globl	_ZN2v88platform12WorkerThreadD1Ev
	.set	_ZN2v88platform12WorkerThreadD1Ev,_ZN2v88platform12WorkerThreadD2Ev
	.section	.text._ZN2v88platform12WorkerThreadD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform12WorkerThreadD0Ev
	.type	_ZN2v88platform12WorkerThreadD0Ev, @function
_ZN2v88platform12WorkerThreadD0Ev:
.LFB3712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform12WorkerThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3712:
	.size	_ZN2v88platform12WorkerThreadD0Ev, .-_ZN2v88platform12WorkerThreadD0Ev
	.section	.text._ZN2v88platform12WorkerThread3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform12WorkerThread3RunEv
	.type	_ZN2v88platform12WorkerThread3RunEv, @function
_ZN2v88platform12WorkerThread3RunEv:
.LFB3713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-32(%rbp), %r12
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L9:
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88platform9TaskQueue7GetNextEv@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3713:
	.size	_ZN2v88platform12WorkerThread3RunEv, .-_ZN2v88platform12WorkerThread3RunEv
	.section	.rodata._ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"V8 WorkerThread"
.LC1:
	.string	"Start()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE
	.type	_ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE, @function
_ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE:
.LFB3707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	-48(%rbp), %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movl	$0, -40(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v84base6ThreadC2ERKNS1_7OptionsE@PLT
	leaq	16+_ZTVN2v88platform12WorkerThreadE(%rip), %rax
	movq	%rbx, 48(%r12)
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base6Thread5StartEv@PLT
	testb	%al, %al
	je	.L20
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3707:
	.size	_ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE, .-_ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE
	.globl	_ZN2v88platform12WorkerThreadC1EPNS0_9TaskQueueE
	.set	_ZN2v88platform12WorkerThreadC1EPNS0_9TaskQueueE,_ZN2v88platform12WorkerThreadC2EPNS0_9TaskQueueE
	.weak	_ZTVN2v88platform12WorkerThreadE
	.section	.data.rel.ro.local._ZTVN2v88platform12WorkerThreadE,"awG",@progbits,_ZTVN2v88platform12WorkerThreadE,comdat
	.align 8
	.type	_ZTVN2v88platform12WorkerThreadE, @object
	.size	_ZTVN2v88platform12WorkerThreadE, 40
_ZTVN2v88platform12WorkerThreadE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform12WorkerThreadD1Ev
	.quad	_ZN2v88platform12WorkerThreadD0Ev
	.quad	_ZN2v88platform12WorkerThread3RunEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
