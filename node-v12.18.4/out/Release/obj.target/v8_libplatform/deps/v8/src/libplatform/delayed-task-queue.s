	.file	"delayed-task-queue.cc"
	.text
	.section	.text._ZN2v88platform16DelayedTaskQueueC2EPFdvE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueueC2EPFdvE
	.type	_ZN2v88platform16DelayedTaskQueueC2EPFdvE, @function
_ZN2v88platform16DelayedTaskQueueC2EPFdvE:
.LFB4138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base17ConditionVariableC1Ev@PLT
	leaq	48(%rbx), %rdi
	call	_ZN2v84base5MutexC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, 88(%rbx)
	movq	$8, 96(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	call	_Znwm@PLT
	movq	96(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, 88(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 128(%rbx)
	movq	%rax, %xmm0
	movq	%rax, (%r12)
	leaq	512(%rax), %rdx
	movq	%rax, 144(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 136(%rbx)
	leaq	176(%rbx), %rax
	movq	%r12, 160(%rbx)
	movq	%r13, 224(%rbx)
	movq	%rdx, 120(%rbx)
	movq	%rdx, 152(%rbx)
	movl	$0, 176(%rbx)
	movq	$0, 184(%rbx)
	movq	%rax, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	$0, 208(%rbx)
	movb	$0, 216(%rbx)
	movups	%xmm0, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4138:
	.size	_ZN2v88platform16DelayedTaskQueueC2EPFdvE, .-_ZN2v88platform16DelayedTaskQueueC2EPFdvE
	.globl	_ZN2v88platform16DelayedTaskQueueC1EPFdvE
	.set	_ZN2v88platform16DelayedTaskQueueC1EPFdvE,_ZN2v88platform16DelayedTaskQueueC2EPFdvE
	.section	.text._ZN2v88platform16DelayedTaskQueue27MonotonicallyIncreasingTimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueue27MonotonicallyIncreasingTimeEv
	.type	_ZN2v88platform16DelayedTaskQueue27MonotonicallyIncreasingTimeEv, @function
_ZN2v88platform16DelayedTaskQueue27MonotonicallyIncreasingTimeEv:
.LFB4143:
	.cfi_startproc
	endbr64
	jmp	*224(%rdi)
	.cfi_endproc
.LFE4143:
	.size	_ZN2v88platform16DelayedTaskQueue27MonotonicallyIncreasingTimeEv, .-_ZN2v88platform16DelayedTaskQueue27MonotonicallyIncreasingTimeEv
	.section	.text._ZN2v88platform16DelayedTaskQueue13AppendDelayedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueue13AppendDelayedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.type	_ZN2v88platform16DelayedTaskQueue13AppendDelayedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, @function
_ZN2v88platform16DelayedTaskQueue13AppendDelayedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd:
.LFB4145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	48(%r12), %r13
	subq	$24, %rsp
	movsd	%xmm0, -40(%rbp)
	call	*224(%rdi)
	addsd	-40(%rbp), %xmm0
	movq	%r13, %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	$48, %edi
	call	_Znwm@PLT
	movsd	-40(%rbp), %xmm0
	leaq	176(%r12), %rcx
	movq	%rax, %rsi
	movsd	%xmm0, 32(%rax)
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	184(%r12), %rdx
	movq	%rax, 40(%rsi)
	testq	%rdx, %rdx
	jne	.L7
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L8
.L19:
	movq	%rax, %rdx
.L7:
	movsd	32(%rdx), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L18
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L19
.L8:
	movl	$1, %edi
	cmpq	%rdx, %rcx
	je	.L6
	xorl	%edi, %edi
	comisd	%xmm0, %xmm1
	seta	%dil
.L6:
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	%r12, %rdi
	addq	$1, 208(%r12)
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	%rcx, %rdx
	movl	$1, %edi
	jmp	.L6
	.cfi_endproc
.LFE4145:
	.size	_ZN2v88platform16DelayedTaskQueue13AppendDelayedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, .-_ZN2v88platform16DelayedTaskQueue13AppendDelayedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.section	.text._ZN2v88platform16DelayedTaskQueue23PopTaskFromDelayedQueueEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueue23PopTaskFromDelayedQueueEd
	.type	_ZN2v88platform16DelayedTaskQueue23PopTaskFromDelayedQueueEd, @function
_ZN2v88platform16DelayedTaskQueue23PopTaskFromDelayedQueueEd:
.LFB4147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$0, 208(%rsi)
	je	.L32
	movq	192(%rsi), %rdi
	movq	%rsi, %rbx
	movsd	32(%rdi), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L30
.L32:
	popq	%rbx
	movq	%r12, %rax
	movq	$0, (%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	40(%rdi), %r14
	movq	$0, 40(%rdi)
	leaq	176(%rsi), %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	40(%rax), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L25
	movq	(%rdi), %rax
	call	*8(%rax)
.L25:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	subq	$1, 208(%rbx)
	movq	%r12, %rax
	popq	%rbx
	movq	%r14, (%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4147:
	.size	_ZN2v88platform16DelayedTaskQueue23PopTaskFromDelayedQueueEd, .-_ZN2v88platform16DelayedTaskQueue23PopTaskFromDelayedQueueEd
	.section	.text._ZN2v88platform16DelayedTaskQueue9TerminateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueue9TerminateEv
	.type	_ZN2v88platform16DelayedTaskQueue9TerminateEv, @function
_ZN2v88platform16DelayedTaskQueue9TerminateEv:
.LFB4148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, %rdi
	movb	$1, 216(%r12)
	call	_ZN2v84base17ConditionVariable9NotifyAllEv@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE4148:
	.size	_ZN2v88platform16DelayedTaskQueue9TerminateEv, .-_ZN2v88platform16DelayedTaskQueue9TerminateEv
	.section	.text._ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB4633:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L50
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L39:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L35
.L38:
	movq	%rbx, %r12
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L38
.L35:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE4633:
	.size	_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.type	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, @function
_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_:
.LFB4714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L54
	.p2align 4,,10
	.p2align 3
.L59:
	movq	0(%r13), %rbx
	leaq	512(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L58
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L59
.L76:
	movq	24(%r15), %rdx
.L54:
	movq	(%r15), %rbx
	cmpq	%rax, %rdx
	je	.L60
	movq	16(%r15), %r12
	cmpq	%r12, %rbx
	je	.L65
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L64
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L61
.L65:
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	je	.L53
	.p2align 4,,10
	.p2align 3
.L63:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L67
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L63
.L53:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L58
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L59
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L63
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L61
	jmp	.L65
.L60:
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L53
.L71:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L69
.L77:
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	je	.L53
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L77
.L69:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L71
	jmp	.L53
	.cfi_endproc
.LFE4714:
	.size	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, .-_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.section	.text._ZN2v88platform16DelayedTaskQueueD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueueD2Ev
	.type	_ZN2v88platform16DelayedTaskQueueD2Ev, @function
_ZN2v88platform16DelayedTaskQueueD2Ev:
.LFB4141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	48(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	leaq	168(%r12), %r15
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	184(%r12), %r13
	testq	%r13, %r13
	je	.L83
.L79:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIdSt4pairIKdSt10unique_ptrIN2v84TaskESt14default_deleteIS4_EEESt10_Select1stIS8_ESt4lessIdESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L83
.L84:
	movq	%rbx, %r13
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L84
.L83:
	movdqu	104(%r12), %xmm2
	leaq	88(%r12), %rdi
	leaq	-128(%rbp), %rdx
	movdqu	136(%r12), %xmm0
	movdqu	152(%r12), %xmm1
	movdqu	120(%r12), %xmm3
	leaq	-96(%rbp), %rsi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L81
	movq	160(%r12), %rax
	movq	128(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L86
	movq	88(%r12), %rdi
.L85:
	call	_ZdlPv@PLT
.L81:
	movq	%r14, %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v84base17ConditionVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L98:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4141:
	.size	_ZN2v88platform16DelayedTaskQueueD2Ev, .-_ZN2v88platform16DelayedTaskQueueD2Ev
	.globl	_ZN2v88platform16DelayedTaskQueueD1Ev
	.set	_ZN2v88platform16DelayedTaskQueueD1Ev,_ZN2v88platform16DelayedTaskQueueD2Ev
	.section	.rodata._ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	.type	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_, @function
_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_:
.LFB4813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L108
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L109
.L101:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L110
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L111
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L106
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L106:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L104:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L110:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L103
	cmpq	%r14, %rsi
	je	.L104
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L103:
	cmpq	%r14, %rsi
	je	.L104
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L104
.L108:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L111:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4813:
	.size	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_, .-_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	.section	.text._ZN2v88platform16DelayedTaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.type	_ZN2v88platform16DelayedTaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, @function
_ZN2v88platform16DelayedTaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE:
.LFB4144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	48(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	152(%r12), %rax
	movq	136(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L113
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	%rax, (%rdx)
	addq	$8, 136(%r12)
.L114:
	movq	%r12, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyOneEv@PLT
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	88(%r12), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	jmp	.L114
	.cfi_endproc
.LFE4144:
	.size	_ZN2v88platform16DelayedTaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, .-_ZN2v88platform16DelayedTaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.section	.text._ZN2v88platform16DelayedTaskQueue7GetNextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform16DelayedTaskQueue7GetNextEv
	.type	_ZN2v88platform16DelayedTaskQueue7GetNextEv, @function
_ZN2v88platform16DelayedTaskQueue7GetNextEv:
.LFB4146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-72(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	176(%r15), %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	48(%rsi), %rax
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	call	*224(%r15)
	cmpq	$0, 208(%r15)
	movsd	%xmm0, -88(%rbp)
	je	.L178
	movq	192(%r15), %rdi
	movsd	32(%rdi), %xmm0
	comisd	-88(%rbp), %xmm0
	jbe	.L172
.L178:
	movq	$0, -72(%rbp)
.L118:
	movq	104(%r15), %rax
	cmpq	%rax, 136(%r15)
	jne	.L179
	cmpb	$0, 216(%r15)
	jne	.L180
	cmpq	$0, 208(%r15)
	je	.L138
	movq	192(%r15), %rax
	movq	-104(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movsd	32(%rax), %xmm0
	subsd	-88(%rbp), %xmm0
	mulsd	.LC1(%rip), %xmm0
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v84base17ConditionVariable7WaitForEPNS0_5MutexERKNS0_9TimeDeltaE@PLT
.L139:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L138:
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v84base17ConditionVariable4WaitEPNS0_5MutexE@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L172:
	movq	40(%rdi), %r14
	movq	$0, 40(%rdi)
	movq	%r12, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	40(%rax), %rdi
	movq	%rax, %r9
	testq	%rdi, %rdi
	je	.L121
	movq	%rax, -96(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-96(%rbp), %r9
.L121:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	subq	$1, 208(%r15)
	movq	%r14, -72(%rbp)
	testq	%r14, %r14
	je	.L118
	leaq	88(%r15), %rbx
	.p2align 4,,10
	.p2align 3
.L131:
	movq	152(%r15), %rax
	movq	136(%r15), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L122
	movq	$0, -72(%rbp)
	movq	%r14, (%rcx)
	addq	$8, 136(%r15)
.L123:
	cmpq	$0, 208(%r15)
	je	.L177
	movq	192(%r15), %rdi
	movsd	32(%rdi), %xmm0
	comisd	-88(%rbp), %xmm0
	jbe	.L173
.L177:
	movq	-72(%rbp), %rdi
	movq	$0, -72(%rbp)
	testq	%rdi, %rdi
	je	.L118
.L125:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	jne	.L131
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE16_M_push_back_auxIJS5_EEEvDpOT_
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L173:
	movq	40(%rdi), %r14
	movq	$0, 40(%rdi)
	movq	%r12, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	40(%rax), %rdi
	movq	%rax, %r9
	testq	%rdi, %rdi
	je	.L128
	movq	%rax, -96(%rbp)
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-96(%rbp), %r9
.L128:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rdi
	subq	$1, 208(%r15)
	movq	%r14, -72(%rbp)
	testq	%rdi, %rdi
	jne	.L125
	testq	%r14, %r14
	jne	.L131
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L179:
	movq	(%rax), %rbx
	movq	$0, (%rax)
	movq	120(%r15), %rax
	movq	104(%r15), %rdx
	subq	$8, %rax
	movq	(%rdx), %rdi
	cmpq	%rax, %rdx
	je	.L132
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	call	*8(%rax)
.L133:
	addq	$8, 104(%r15)
.L134:
	movq	-112(%rbp), %rax
	movq	%rbx, (%rax)
.L136:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	call	*8(%rax)
.L142:
	movq	-104(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	movq	-112(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L180:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v84base17ConditionVariable9NotifyAllEv@PLT
	movq	-112(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L136
.L132:
	testq	%rdi, %rdi
	je	.L135
	movq	(%rdi), %rax
	call	*8(%rax)
.L135:
	movq	112(%r15), %rdi
	call	_ZdlPv@PLT
	movq	128(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 128(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 112(%r15)
	movq	%rdx, 120(%r15)
	movq	%rax, 104(%r15)
	jmp	.L134
.L181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4146:
	.size	_ZN2v88platform16DelayedTaskQueue7GetNextEv, .-_ZN2v88platform16DelayedTaskQueue7GetNextEv
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1093567616
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
