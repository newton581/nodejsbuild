	.file	"default-platform.cc"
	.text
	.section	.text._ZN2v88Platform24OnCriticalMemoryPressureEv,"axG",@progbits,_ZN2v88Platform24OnCriticalMemoryPressureEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform24OnCriticalMemoryPressureEv
	.type	_ZN2v88Platform24OnCriticalMemoryPressureEv, @function
_ZN2v88Platform24OnCriticalMemoryPressureEv:
.LFB4376:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4376:
	.size	_ZN2v88Platform24OnCriticalMemoryPressureEv, .-_ZN2v88Platform24OnCriticalMemoryPressureEv
	.section	.text._ZN2v88Platform24OnCriticalMemoryPressureEm,"axG",@progbits,_ZN2v88Platform24OnCriticalMemoryPressureEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform24OnCriticalMemoryPressureEm
	.type	_ZN2v88Platform24OnCriticalMemoryPressureEm, @function
_ZN2v88Platform24OnCriticalMemoryPressureEm:
.LFB4377:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4377:
	.size	_ZN2v88Platform24OnCriticalMemoryPressureEm, .-_ZN2v88Platform24OnCriticalMemoryPressureEm
	.section	.text._ZN2v88Platform19DumpWithoutCrashingEv,"axG",@progbits,_ZN2v88Platform19DumpWithoutCrashingEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform19DumpWithoutCrashingEv
	.type	_ZN2v88Platform19DumpWithoutCrashingEv, @function
_ZN2v88Platform19DumpWithoutCrashingEv:
.LFB4384:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4384:
	.size	_ZN2v88Platform19DumpWithoutCrashingEv, .-_ZN2v88Platform19DumpWithoutCrashingEv
	.section	.text._ZN2v88Platform11AddCrashKeyEiPKcm,"axG",@progbits,_ZN2v88Platform11AddCrashKeyEiPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform11AddCrashKeyEiPKcm
	.type	_ZN2v88Platform11AddCrashKeyEiPKcm, @function
_ZN2v88Platform11AddCrashKeyEiPKcm:
.LFB4385:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4385:
	.size	_ZN2v88Platform11AddCrashKeyEiPKcm, .-_ZN2v88Platform11AddCrashKeyEiPKcm
	.section	.text._ZN2v88platform15DefaultPlatform16IdleTasksEnabledEPNS_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform16IdleTasksEnabledEPNS_7IsolateE
	.type	_ZN2v88platform15DefaultPlatform16IdleTasksEnabledEPNS_7IsolateE, @function
_ZN2v88platform15DefaultPlatform16IdleTasksEnabledEPNS_7IsolateE:
.LFB5068:
	.cfi_startproc
	endbr64
	cmpl	$1, 52(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE5068:
	.size	_ZN2v88platform15DefaultPlatform16IdleTasksEnabledEPNS_7IsolateE, .-_ZN2v88platform15DefaultPlatform16IdleTasksEnabledEPNS_7IsolateE
	.section	.text._ZN2v88platform15DefaultPlatform20GetTracingControllerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform20GetTracingControllerEv
	.type	_ZN2v88platform15DefaultPlatform20GetTracingControllerEv, @function
_ZN2v88platform15DefaultPlatform20GetTracingControllerEv:
.LFB5071:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE5071:
	.size	_ZN2v88platform15DefaultPlatform20GetTracingControllerEv, .-_ZN2v88platform15DefaultPlatform20GetTracingControllerEv
	.section	.text._ZN2v88platform15DefaultPlatform21NumberOfWorkerThreadsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform21NumberOfWorkerThreadsEv
	.type	_ZN2v88platform15DefaultPlatform21NumberOfWorkerThreadsEv, @function
_ZN2v88platform15DefaultPlatform21NumberOfWorkerThreadsEv:
.LFB5073:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE5073:
	.size	_ZN2v88platform15DefaultPlatform21NumberOfWorkerThreadsEv, .-_ZN2v88platform15DefaultPlatform21NumberOfWorkerThreadsEv
	.section	.text._ZN2v88platform15DefaultPlatform20GetStackTracePrinterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform20GetStackTracePrinterEv
	.type	_ZN2v88platform15DefaultPlatform20GetStackTracePrinterEv, @function
_ZN2v88platform15DefaultPlatform20GetStackTracePrinterEv:
.LFB5074:
	.cfi_startproc
	endbr64
	leaq	_ZN2v88platform12_GLOBAL__N_115PrintStackTraceEv(%rip), %rax
	ret
	.cfi_endproc
.LFE5074:
	.size	_ZN2v88platform15DefaultPlatform20GetStackTracePrinterEv, .-_ZN2v88platform15DefaultPlatform20GetStackTracePrinterEv
	.section	.text._ZN2v88platform15DefaultPlatform16GetPageAllocatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform16GetPageAllocatorEv
	.type	_ZN2v88platform15DefaultPlatform16GetPageAllocatorEv, @function
_ZN2v88platform15DefaultPlatform16GetPageAllocatorEv:
.LFB5075:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	ret
	.cfi_endproc
.LFE5075:
	.size	_ZN2v88platform15DefaultPlatform16GetPageAllocatorEv, .-_ZN2v88platform15DefaultPlatform16GetPageAllocatorEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB6527:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6527:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB6530:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r8
	movq	%r8, %rdi
	movq	64(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6530:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB6534:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6534:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB6537:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r8
	movq	%r8, %rdi
	movq	64(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6537:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v817TracingControllerD0Ev,"axG",@progbits,_ZN2v817TracingControllerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingControllerD0Ev
	.type	_ZN2v817TracingControllerD0Ev, @function
_ZN2v817TracingControllerD0Ev:
.LFB5715:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5715:
	.size	_ZN2v817TracingControllerD0Ev, .-_ZN2v817TracingControllerD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB6536:
	.cfi_startproc
	endbr64
	movl	$344, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6536:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB6529:
	.cfi_startproc
	endbr64
	movl	$328, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6529:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB6531:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE6531:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB6538:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE6538:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB6539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L20
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L20:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6539:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB6532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L24
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L24:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6532:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN2v88platform12_GLOBAL__N_119DefaultTimeFunctionEv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88platform12_GLOBAL__N_119DefaultTimeFunctionEv, @function
_ZN2v88platform12_GLOBAL__N_119DefaultTimeFunctionEv:
.LFB5031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC0(%rip), %xmm0
	ret
	.cfi_endproc
.LFE5031:
	.size	_ZN2v88platform12_GLOBAL__N_119DefaultTimeFunctionEv, .-_ZN2v88platform12_GLOBAL__N_119DefaultTimeFunctionEv
	.section	.text._ZN2v88platform15DefaultPlatform22CurrentClockTimeMillisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform22CurrentClockTimeMillisEv
	.type	_ZN2v88platform15DefaultPlatform22CurrentClockTimeMillisEv, @function
_ZN2v88platform15DefaultPlatform22CurrentClockTimeMillisEv:
.LFB5070:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base2OS17TimeCurrentMillisEv@PLT
	.cfi_endproc
.LFE5070:
	.size	_ZN2v88platform15DefaultPlatform22CurrentClockTimeMillisEv, .-_ZN2v88platform15DefaultPlatform22CurrentClockTimeMillisEv
	.section	.text._ZN2v88platform12_GLOBAL__N_115PrintStackTraceEv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88platform12_GLOBAL__N_115PrintStackTraceEv, @function
_ZN2v88platform12_GLOBAL__N_115PrintStackTraceEv:
.LFB4963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-528(%rbp), %r12
	movq	%r12, %rdi
	subq	$520, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5debug10StackTraceC1Ev@PLT
	movq	%r12, %rdi
	call	_ZNK2v84base5debug10StackTrace5PrintEv@PLT
	call	_ZN2v84base5debug22DisableSignalStackDumpEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base5debug10StackTraceD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$520, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4963:
	.size	_ZN2v88platform12_GLOBAL__N_115PrintStackTraceEv, .-_ZN2v88platform12_GLOBAL__N_115PrintStackTraceEv
	.section	.text._ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv
	.type	_ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv, @function
_ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv:
.LFB5069:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.L36
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L36:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC0(%rip), %xmm0
	ret
	.cfi_endproc
.LFE5069:
	.size	_ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv, .-_ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv
	.section	.text._ZN2v88platform15DefaultPlatform22CallOnForegroundThreadEPNS_7IsolateEPNS_4TaskE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform22CallOnForegroundThreadEPNS_7IsolateEPNS_4TaskE
	.type	_ZN2v88platform15DefaultPlatform22CallOnForegroundThreadEPNS_7IsolateEPNS_4TaskE, @function
_ZN2v88platform15DefaultPlatform22CallOnForegroundThreadEPNS_7IsolateEPNS_4TaskE:
.LFB5065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movq	%rsi, %rdx
	pushq	%rbx
	movq	%r8, %rsi
	leaq	-48(%rbp), %rdi
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	call	*48(%rax)
	movq	-48(%rbp), %rdi
	leaq	-56(%rbp), %rsi
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	%r12, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*8(%rax)
.L40:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L39
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L43
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L53
	.p2align 4,,10
	.p2align 3
.L39:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L39
.L53:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L46
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L47:
	cmpl	$1, %eax
	jne	.L39
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L46:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L47
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5065:
	.size	_ZN2v88platform15DefaultPlatform22CallOnForegroundThreadEPNS_7IsolateEPNS_4TaskE, .-_ZN2v88platform15DefaultPlatform22CallOnForegroundThreadEPNS_7IsolateEPNS_4TaskE
	.section	.text._ZN2v88platform15DefaultPlatform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE
	.type	_ZN2v88platform15DefaultPlatform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE, @function
_ZN2v88platform15DefaultPlatform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE:
.LFB5067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movq	%rsi, %rdx
	pushq	%rbx
	movq	%r8, %rsi
	leaq	-48(%rbp), %rdi
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	call	*48(%rax)
	movq	-48(%rbp), %rdi
	leaq	-56(%rbp), %rsi
	movq	(%rdi), %rax
	movq	%r12, -56(%rbp)
	call	*32(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	call	*8(%rax)
.L56:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L55
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L59
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L69
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L55
.L69:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L62
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L63:
	cmpl	$1, %eax
	jne	.L55
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L62:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L63
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5067:
	.size	_ZN2v88platform15DefaultPlatform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE, .-_ZN2v88platform15DefaultPlatform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE
	.section	.text._ZN2v88platform15DefaultPlatform29CallDelayedOnForegroundThreadEPNS_7IsolateEPNS_4TaskEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform29CallDelayedOnForegroundThreadEPNS_7IsolateEPNS_4TaskEd
	.type	_ZN2v88platform15DefaultPlatform29CallDelayedOnForegroundThreadEPNS_7IsolateEPNS_4TaskEd, @function
_ZN2v88platform15DefaultPlatform29CallDelayedOnForegroundThreadEPNS_7IsolateEPNS_4TaskEd:
.LFB5066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movq	%rsi, %rdx
	pushq	%rbx
	movq	%r8, %rsi
	leaq	-48(%rbp), %rdi
	subq	$64, %rsp
	.cfi_offset 3, -32
	movsd	%xmm0, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	call	*48(%rax)
	movq	-48(%rbp), %rdi
	movsd	-72(%rbp), %xmm0
	leaq	-56(%rbp), %rsi
	movq	(%rdi), %rax
	movq	%r12, -56(%rbp)
	call	*16(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	movq	(%rdi), %rax
	call	*8(%rax)
.L72:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L71
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L75
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L85
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L71
.L85:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L78
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L79:
	cmpl	$1, %eax
	jne	.L71
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L78:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L79
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5066:
	.size	_ZN2v88platform15DefaultPlatform29CallDelayedOnForegroundThreadEPNS_7IsolateEPNS_4TaskEd, .-_ZN2v88platform15DefaultPlatform29CallDelayedOnForegroundThreadEPNS_7IsolateEPNS_4TaskEd
	.section	.text._ZN2v88platform20SetTracingControllerEPNS_8PlatformEPNS0_7tracing17TracingControllerE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88platform20SetTracingControllerEPNS_8PlatformEPNS0_7tracing17TracingControllerE
	.type	_ZN2v88platform20SetTracingControllerEPNS_8PlatformEPNS0_7tracing17TracingControllerE, @function
_ZN2v88platform20SetTracingControllerEPNS_8PlatformEPNS0_7tracing17TracingControllerE:
.LFB4973:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %r8
	movq	%rsi, 120(%rdi)
	testq	%r8, %r8
	je	.L87
	movq	(%r8), %rax
	leaq	_ZN2v817TracingControllerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L89
	movl	$8, %esi
	movq	%r8, %rdi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE4973:
	.size	_ZN2v88platform20SetTracingControllerEPNS_8PlatformEPNS0_7tracing17TracingControllerE, .-_ZN2v88platform20SetTracingControllerEPNS_8PlatformEPNS0_7tracing17TracingControllerE
	.section	.text._ZN2v88platform15DefaultPlatformC2ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatformC2ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE
	.type	_ZN2v88platform15DefaultPlatformC2ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE, @function
_ZN2v88platform15DefaultPlatformC2ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE:
.LFB5000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform15DefaultPlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	80(%rbx), %rax
	movl	%r13d, 52(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%rbx)
	movl	$24, %edi
	movq	%rax, 104(%rbx)
	movq	(%r12), %rax
	movl	$0, 48(%rbx)
	movq	$0, (%r12)
	movl	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 112(%rbx)
	movq	%rax, 120(%rbx)
	movups	%xmm0, 56(%rbx)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v84base13PageAllocatorC1Ev@PLT
	cmpq	$0, 120(%rbx)
	movq	%r12, 128(%rbx)
	movq	$0, 136(%rbx)
	je	.L99
.L90:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88platform7tracing17TracingControllerC1Ev@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE@PLT
	movq	120(%rbx), %rdi
	movq	%r12, 120(%rbx)
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingControllerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L94
	addq	$8, %rsp
	movl	$8, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5000:
	.size	_ZN2v88platform15DefaultPlatformC2ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE, .-_ZN2v88platform15DefaultPlatformC2ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE
	.globl	_ZN2v88platform15DefaultPlatformC1ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE
	.set	_ZN2v88platform15DefaultPlatformC1ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE,_ZN2v88platform15DefaultPlatformC2ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE
	.section	.text._ZN2v88platform15DefaultPlatform17SetThreadPoolSizeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform17SetThreadPoolSizeEi
	.type	_ZN2v88platform15DefaultPlatform17SetThreadPoolSizeEi, @function
_ZN2v88platform15DefaultPlatform17SetThreadPoolSizeEi:
.LFB5028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	testl	%ebx, %ebx
	jle	.L103
.L101:
	testl	%ebx, %ebx
	movl	$1, %esi
	movq	%r13, %rdi
	cmovle	%esi, %ebx
	movl	$8, %esi
	cmpl	$8, %ebx
	cmovg	%esi, %ebx
	movl	%ebx, 48(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	call	_ZN2v84base7SysInfo18NumberOfProcessorsEv@PLT
	leal	-1(%rax), %ebx
	jmp	.L101
	.cfi_endproc
.LFE5028:
	.size	_ZN2v88platform15DefaultPlatform17SetThreadPoolSizeEi, .-_ZN2v88platform15DefaultPlatform17SetThreadPoolSizeEi
	.section	.text._ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv
	.type	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv, @function
_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv:
.LFB5032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpq	$0, 56(%rbx)
	je	.L116
.L105:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	136(%rbx), %r15
	leaq	_ZN2v88platform12_GLOBAL__N_119DefaultTimeFunctionEv(%rip), %rax
	movl	$344, %edi
	testq	%r15, %r15
	cmove	%rax, %r15
	call	_Znwm@PLT
	movl	48(%rbx), %esi
	movq	%rax, %r13
	movq	%r15, %rdx
	movabsq	$4294967297, %rax
	leaq	16(%r13), %r14
	movq	%rax, 8(%r13)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	call	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC1EjPFdvE@PLT
	movq	64(%rbx), %r15
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	testq	%r15, %r15
	je	.L105
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L109
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L110:
	cmpl	$1, %eax
	jne	.L105
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L112
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L113:
	cmpl	$1, %eax
	jne	.L105
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L109:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L112:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L113
	.cfi_endproc
.LFE5032:
	.size	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv, .-_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv
	.section	.text._ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE
	.type	_ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE, @function
_ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE:
.LFB4966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %ecx
	je	.L127
.L118:
	movq	(%r12), %rax
	movq	$0, (%r12)
	movl	$144, %edi
	movq	%rax, -48(%rbp)
	call	_Znwm@PLT
	leaq	-48(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88platform15DefaultPlatformC1ENS0_15IdleTaskSupportESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L119
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingControllerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L120
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L119:
	leaq	8(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	testl	%ebx, %ebx
	jle	.L128
.L121:
	testl	%ebx, %ebx
	movl	$1, %esi
	movq	%r14, %rdi
	cmovle	%esi, %ebx
	movl	$8, %esi
	cmpl	$8, %ebx
	cmovg	%esi, %ebx
	movl	%ebx, 48(%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv
	movq	%r12, 0(%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	call	_ZN2v84base7SysInfo18NumberOfProcessorsEv@PLT
	leal	-1(%rax), %ebx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L127:
	call	_ZN2v84base5debug27EnableInProcessStackDumpingEv@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L120:
	call	*%rax
	jmp	.L119
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4966:
	.size	_ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE, .-_ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE
	.section	.text._ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.type	_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, @function
_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE:
.LFB5063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv
	movq	56(%r12), %rdi
	movq	(%rbx), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%rdx, -32(%rbp)
	call	*%rax
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L130
	movq	(%rdi), %rax
	call	*8(%rax)
.L130:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L137:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5063:
	.size	_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, .-_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.section	.text._ZN2v88platform15DefaultPlatform25CallDelayedOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform25CallDelayedOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.type	_ZN2v88platform15DefaultPlatform25CallDelayedOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, @function
_ZN2v88platform15DefaultPlatform25CallDelayedOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd:
.LFB5064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$32, %rsp
	movsd	%xmm0, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv
	movq	56(%r12), %rdi
	movq	(%rbx), %rdx
	leaq	-32(%rbp), %rsi
	movsd	-40(%rbp), %xmm0
	movq	(%rdi), %rax
	movq	%rdx, -32(%rbp)
	movq	16(%rax), %rax
	movq	$0, (%rbx)
	call	*%rax
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L138
	movq	(%rdi), %rax
	call	*8(%rax)
.L138:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L145:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5064:
	.size	_ZN2v88platform15DefaultPlatform25CallDelayedOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, .-_ZN2v88platform15DefaultPlatform25CallDelayedOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.section	.text._ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB4380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	leaq	_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE(%rip), %rdx
	movq	56(%rax), %rax
	movq	$0, (%rsi)
	cmpq	%rdx, %rax
	jne	.L147
	call	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv
	movq	56(%r12), %rdi
	movq	-40(%rbp), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	$0, -40(%rbp)
	movq	%rdx, -32(%rbp)
	call	*%rax
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L149
	movq	(%rdi), %rax
	call	*8(%rax)
.L149:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L146
	movq	(%rdi), %rax
	call	*8(%rax)
.L146:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	leaq	-40(%rbp), %rsi
	call	*%rax
	jmp	.L149
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4380:
	.size	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB4378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	leaq	_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE(%rip), %rdx
	movq	56(%rax), %rax
	movq	$0, (%rsi)
	cmpq	%rdx, %rax
	jne	.L161
	call	_ZN2v88platform15DefaultPlatform37EnsureBackgroundTaskRunnerInitializedEv
	movq	56(%r12), %rdi
	movq	-40(%rbp), %rdx
	leaq	-32(%rbp), %rsi
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	$0, -40(%rbp)
	movq	%rdx, -32(%rbp)
	call	*%rax
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L163
	movq	(%rdi), %rax
	call	*8(%rax)
.L163:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rax
	call	*8(%rax)
.L160:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leaq	-40(%rbp), %rsi
	call	*%rax
	jmp	.L163
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4378:
	.size	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZN2v88platform15DefaultPlatform25SetTimeFunctionForTestingEPFdvE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform25SetTimeFunctionForTestingEPFdvE
	.type	_ZN2v88platform15DefaultPlatform25SetTimeFunctionForTestingEPFdvE, @function
_ZN2v88platform15DefaultPlatform25SetTimeFunctionForTestingEPFdvE:
.LFB5033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, 136(%rbx)
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE5033:
	.size	_ZN2v88platform15DefaultPlatform25SetTimeFunctionForTestingEPFdvE, .-_ZN2v88platform15DefaultPlatform25SetTimeFunctionForTestingEPFdvE
	.section	.text._ZN2v88platform15DefaultPlatform15PumpMessageLoopEPNS_7IsolateENS0_19MessageLoopBehaviorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform15PumpMessageLoopEPNS_7IsolateENS0_19MessageLoopBehaviorE
	.type	_ZN2v88platform15DefaultPlatform15PumpMessageLoopEPNS_7IsolateENS0_19MessageLoopBehaviorE, @function
_ZN2v88platform15DefaultPlatform15PumpMessageLoopEPNS_7IsolateENS0_19MessageLoopBehaviorE:
.LFB5034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%r13), %rax
	testq	%rax, %rax
	je	.L177
	leaq	80(%r13), %rcx
	movq	%rcx, %rdx
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L179
.L178:
	cmpq	%rbx, 32(%rax)
	jnb	.L208
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L178
.L179:
	cmpq	%rdx, %rcx
	je	.L177
	cmpq	%rbx, 32(%rdx)
	ja	.L177
	movq	48(%rdx), %r13
	movq	40(%rdx), %r15
	testq	%r13, %r13
	je	.L184
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L185
	lock addl	$1, 8(%r13)
.L186:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-64(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	_ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L194
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L195
.L196:
	movq	(%rdi), %rax
	movl	$1, %r12d
	call	*8(%rax)
	testq	%r13, %r13
	je	.L176
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
.L195:
	movl	$1, %r12d
.L194:
	testq	%rbx, %rbx
	je	.L189
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L209
	.p2align 4,,10
	.p2align 3
.L176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L176
.L209:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L191
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L192:
	cmpl	$1, %eax
	jne	.L176
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-64(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	_ZN2v88platform27DefaultForegroundTaskRunner16PopTaskFromQueueENS0_19MessageLoopBehaviorE@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L196
	movl	$1, %r12d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L185:
	addl	$1, 8(%r13)
	jmp	.L186
.L191:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L192
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5034:
	.size	_ZN2v88platform15DefaultPlatform15PumpMessageLoopEPNS_7IsolateENS0_19MessageLoopBehaviorE, .-_ZN2v88platform15DefaultPlatform15PumpMessageLoopEPNS_7IsolateENS0_19MessageLoopBehaviorE
	.section	.text._ZN2v88platform15PumpMessageLoopEPNS_8PlatformEPNS_7IsolateENS0_19MessageLoopBehaviorE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88platform15PumpMessageLoopEPNS_8PlatformEPNS_7IsolateENS0_19MessageLoopBehaviorE
	.type	_ZN2v88platform15PumpMessageLoopEPNS_8PlatformEPNS_7IsolateENS0_19MessageLoopBehaviorE, @function
_ZN2v88platform15PumpMessageLoopEPNS_8PlatformEPNS_7IsolateENS0_19MessageLoopBehaviorE:
.LFB4971:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88platform15DefaultPlatform15PumpMessageLoopEPNS_7IsolateENS0_19MessageLoopBehaviorE
	.cfi_endproc
.LFE4971:
	.size	_ZN2v88platform15PumpMessageLoopEPNS_8PlatformEPNS_7IsolateENS0_19MessageLoopBehaviorE, .-_ZN2v88platform15PumpMessageLoopEPNS_8PlatformEPNS_7IsolateENS0_19MessageLoopBehaviorE
	.section	.text._ZN2v88platform15DefaultPlatform20SetTracingControllerESt10unique_ptrINS_17TracingControllerESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform20SetTracingControllerESt10unique_ptrINS_17TracingControllerESt14default_deleteIS3_EE
	.type	_ZN2v88platform15DefaultPlatform20SetTracingControllerESt10unique_ptrINS_17TracingControllerESt14default_deleteIS3_EE, @function
_ZN2v88platform15DefaultPlatform20SetTracingControllerESt10unique_ptrINS_17TracingControllerESt14default_deleteIS3_EE:
.LFB5072:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	120(%rdi), %r8
	movq	$0, (%rsi)
	movq	%rax, 120(%rdi)
	testq	%r8, %r8
	je	.L212
	movq	(%r8), %rax
	leaq	_ZN2v817TracingControllerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L214
	movl	$8, %esi
	movq	%r8, %rdi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE5072:
	.size	_ZN2v88platform15DefaultPlatform20SetTracingControllerESt10unique_ptrINS_17TracingControllerESt14default_deleteIS3_EE, .-_ZN2v88platform15DefaultPlatform20SetTracingControllerESt10unique_ptrINS_17TracingControllerESt14default_deleteIS3_EE
	.section	.text._ZN2v817TracingControllerD2Ev,"axG",@progbits,_ZN2v817TracingControllerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingControllerD2Ev
	.type	_ZN2v817TracingControllerD2Ev, @function
_ZN2v817TracingControllerD2Ev:
.LFB5713:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5713:
	.size	_ZN2v817TracingControllerD2Ev, .-_ZN2v817TracingControllerD2Ev
	.weak	_ZN2v817TracingControllerD1Ev
	.set	_ZN2v817TracingControllerD1Ev,_ZN2v817TracingControllerD2Ev
	.section	.text._ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB5753:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L236
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	je	.L223
.L218:
	movq	24(%rbx), %rsi
	movq	%rbx, %r14
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	48(%r14), %r12
	movq	16(%rbx), %rbx
	testq	%r12, %r12
	je	.L225
	lock subl	$1, 8(%r12)
	je	.L239
.L225:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	movq	%rbx, %r14
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	48(%r14), %r12
	movq	16(%rbx), %rbx
	testq	%r12, %r12
	je	.L221
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	je	.L222
.L221:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L223
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r12)
	jne	.L225
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L222:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	cmpl	$1, %eax
	jne	.L221
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE5753:
	.size	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88platform15DefaultPlatformD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatformD2Ev
	.type	_ZN2v88platform15DefaultPlatformD2Ev, @function
_ZN2v88platform15DefaultPlatformD2Ev:
.LFB5025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform15DefaultPlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	leaq	8(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZN2v88platform30DefaultWorkerThreadsTaskRunner9TerminateEv@PLT
.L241:
	movq	96(%r13), %r12
	leaq	80(%r13), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	jne	.L252
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L297:
	lock addl	$1, (%rbx)
	call	_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv@PLT
	testq	%r14, %r14
	je	.L295
.L273:
	movl	$-1, %edx
	lock xaddl	%edx, (%rbx)
	cmpl	$1, %edx
	je	.L296
.L248:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -56(%rbp)
	je	.L242
.L252:
	movq	48(%r12), %r15
	movq	40(%r12), %rdi
	testq	%r15, %r15
	je	.L243
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	8(%r15), %rbx
	testq	%r14, %r14
	jne	.L297
	addl	$1, 8(%r15)
	call	_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv@PLT
	testq	%r14, %r14
	jne	.L273
.L295:
	movl	8(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r15)
	cmpl	$1, %edx
	jne	.L248
.L296:
	movq	(%r15), %rdx
	movq	%r15, %rdi
	call	*16(%rdx)
	testq	%r14, %r14
	je	.L249
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L250:
	cmpl	$1, %eax
	jne	.L248
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L243:
	call	_ZN2v88platform27DefaultForegroundTaskRunner9TerminateEv@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L242:
	movq	-64(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	128(%r13), %rdi
	testq	%rdi, %rdi
	je	.L253
	movq	(%rdi), %rax
	call	*8(%rax)
.L253:
	movq	120(%r13), %rdi
	testq	%rdi, %rdi
	je	.L254
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingControllerD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L255
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L254:
	movq	88(%r13), %r12
	leaq	72(%r13), %rbx
	testq	%r12, %r12
	je	.L256
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L262
.L257:
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	48(%r14), %r15
	movq	16(%r12), %r12
	testq	%r15, %r15
	je	.L264
	lock subl	$1, 8(%r15)
	je	.L298
.L264:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L257
.L256:
	movq	64(%r13), %r12
	testq	%r12, %r12
	je	.L267
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L268
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
	cmpl	$1, %edx
	je	.L299
.L267:
	movq	-64(%rbp), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	48(%r14), %r15
	movq	16(%r12), %r12
	testq	%r15, %r15
	je	.L260
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	je	.L261
.L260:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L262
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L249:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L268:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %edx
	jne	.L267
.L299:
	movq	(%r12), %rdx
	movq	%r12, %rdi
	call	*16(%rdx)
	testq	%r14, %r14
	je	.L271
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L272:
	cmpl	$1, %eax
	jne	.L267
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L298:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r15)
	jne	.L264
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L261:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	cmpl	$1, %eax
	jne	.L260
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L255:
	call	*%rax
	jmp	.L254
.L271:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L272
	.cfi_endproc
.LFE5025:
	.size	_ZN2v88platform15DefaultPlatformD2Ev, .-_ZN2v88platform15DefaultPlatformD2Ev
	.globl	_ZN2v88platform15DefaultPlatformD1Ev
	.set	_ZN2v88platform15DefaultPlatformD1Ev,_ZN2v88platform15DefaultPlatformD2Ev
	.section	.text._ZN2v88platform15DefaultPlatformD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatformD0Ev
	.type	_ZN2v88platform15DefaultPlatformD0Ev, @function
_ZN2v88platform15DefaultPlatformD0Ev:
.LFB5027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88platform15DefaultPlatformD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$144, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5027:
	.size	_ZN2v88platform15DefaultPlatformD0Ev, .-_ZN2v88platform15DefaultPlatformD0Ev
	.section	.text._ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_
	.type	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_, @function
_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_:
.LFB5802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$56, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	leaq	8(%rbx), %rcx
	movq	%rax, %r12
	movq	0(%r13), %rax
	movq	(%rax), %r14
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movq	%r14, 32(%r12)
	cmpq	%r15, %rcx
	je	.L362
	movq	%r15, %r13
	movq	32(%r15), %r15
	cmpq	%r15, %r14
	jnb	.L315
	movq	24(%rbx), %r15
	cmpq	%r13, %r15
	je	.L342
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpq	%r14, 32(%rax)
	jnb	.L317
	cmpq	$0, 24(%rax)
	je	.L305
.L342:
	movq	%r13, %rax
.L337:
	testq	%rax, %rax
	setne	%al
.L336:
	cmpq	%r13, %rcx
	je	.L350
	testb	%al, %al
	je	.L363
.L350:
	movl	$1, %edi
.L327:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	jbe	.L313
	cmpq	%r13, 32(%rbx)
	je	.L359
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpq	%r14, 32(%rax)
	jbe	.L325
	cmpq	$0, 24(%r13)
	je	.L326
	movq	%rax, %r13
	movl	$1, %edi
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%rdx, %r13
	testb	%dil, %dil
	jne	.L306
.L311:
	cmpq	%r14, %rsi
	jb	.L314
	movq	%rdx, %r13
.L313:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	cmpq	$0, 40(%rbx)
	je	.L304
	movq	32(%rbx), %rax
	cmpq	%r14, 32(%rax)
	jb	.L305
.L304:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L307
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L365:
	movq	16(%rdx), %rax
	movl	$1, %edi
.L310:
	testq	%rax, %rax
	je	.L308
	movq	%rax, %rdx
.L307:
	movq	32(%rdx), %rsi
	cmpq	%r14, %rsi
	ja	.L365
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%rax, %r13
	xorl	%eax, %eax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L317:
	movq	16(%rbx), %r13
	testq	%r13, %r13
	jne	.L319
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L367:
	movq	16(%r13), %rax
	movl	$1, %esi
.L322:
	testq	%rax, %rax
	je	.L320
	movq	%rax, %r13
.L319:
	movq	32(%r13), %rdx
	cmpq	%rdx, %r14
	jb	.L367
	movq	24(%r13), %rax
	xorl	%esi, %esi
	jmp	.L322
.L366:
	movq	%rcx, %r13
.L318:
	cmpq	%r13, %r15
	je	.L345
.L361:
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	%r13, %rdi
	movq	32(%rax), %rdx
	movq	%rax, %r13
.L333:
	cmpq	%rdx, %r14
	jbe	.L313
.L334:
	movq	%rdi, %r13
.L314:
	testq	%r13, %r13
	je	.L313
.L359:
	xorl	%eax, %eax
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L363:
	movq	32(%r13), %r15
.L326:
	xorl	%edi, %edi
	cmpq	%r15, %r14
	setb	%dil
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r15, %rdx
.L306:
	cmpq	%rdx, 24(%rbx)
	je	.L340
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	32(%rax), %rsi
	movq	%rdx, %r13
	movq	%rax, %rdx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%r13, %rdi
	testb	%sil, %sil
	je	.L333
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L325:
	movq	16(%rbx), %r13
	testq	%r13, %r13
	jne	.L329
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L369:
	movq	16(%r13), %rax
	movl	$1, %esi
.L332:
	testq	%rax, %rax
	je	.L330
	movq	%rax, %r13
.L329:
	movq	32(%r13), %rdx
	cmpq	%rdx, %r14
	jb	.L369
	movq	24(%r13), %rax
	xorl	%esi, %esi
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r13, %rdi
	testb	%sil, %sil
	je	.L333
.L328:
	cmpq	%r13, 24(%rbx)
	jne	.L361
	movq	%r13, %rdi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%rdx, %r13
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%r15, %rdi
	jmp	.L334
.L368:
	movq	%rcx, %r13
	jmp	.L328
	.cfi_endproc
.LFE5802:
	.size	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_, .-_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_
	.section	.text._ZN2v88platform15DefaultPlatform12RunIdleTasksEPNS_7IsolateEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform12RunIdleTasksEPNS_7IsolateEd
	.type	_ZN2v88platform15DefaultPlatform12RunIdleTasksEPNS_7IsolateEd, @function
_ZN2v88platform15DefaultPlatform12RunIdleTasksEPNS_7IsolateEd:
.LFB5046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$48, %rsp
	movq	%rsi, -72(%rbp)
	movsd	%xmm0, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L371
	leaq	80(%rbx), %rdi
	movq	-72(%rbp), %rcx
	movq	%rdx, %rax
	movq	%rdi, %rsi
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L373
.L372:
	cmpq	%rcx, 32(%rax)
	jnb	.L432
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L372
.L373:
	cmpq	%rsi, %rdi
	je	.L371
	cmpq	%rcx, 32(%rsi)
	ja	.L371
	movq	%rdi, %rsi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L433
.L376:
	cmpq	%rcx, 32(%rdx)
	jnb	.L434
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L376
.L433:
	cmpq	%rsi, %rdi
	je	.L381
	cmpq	%rcx, 32(%rsi)
	jbe	.L382
.L381:
	leaq	-72(%rbp), %rax
	leaq	-48(%rbp), %rcx
	leaq	72(%rbx), %rdi
	leaq	-49(%rbp), %r8
	movq	%rax, -48(%rbp)
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_
	movq	%rax, %rsi
.L382:
	movq	48(%rsi), %r14
	movq	40(%rsi), %r13
	testq	%r14, %r14
	je	.L383
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L384
	lock addl	$1, 8(%r14)
.L383:
	movq	%r12, %rdi
	leaq	_ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv(%rip), %r12
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rax
	movq	120(%rax), %rax
	cmpq	%r12, %rax
	jne	.L386
	movq	136(%rbx), %rax
	testq	%rax, %rax
	je	.L387
	call	*%rax
.L388:
	addsd	-80(%rbp), %xmm0
	movsd	%xmm0, -80(%rbp)
.L397:
	movq	(%rbx), %rax
	movq	120(%rax), %rax
	cmpq	%r12, %rax
	jne	.L389
.L435:
	movq	136(%rbx), %rax
	testq	%rax, %rax
	je	.L390
	call	*%rax
.L391:
	movsd	-80(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L422
.L436:
	leaq	-48(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88platform27DefaultForegroundTaskRunner20PopTaskFromIdleQueueEv@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L422
	movq	(%rdi), %rax
	movsd	-80(%rbp), %xmm0
	call	*16(%rax)
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L397
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%rbx), %rax
	movq	120(%rax), %rax
	cmpq	%r12, %rax
	je	.L435
.L389:
	movq	%rbx, %rdi
	call	*%rax
	movsd	-80(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L436
	.p2align 4,,10
	.p2align 3
.L422:
	testq	%r14, %r14
	je	.L370
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L403
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L437
	.p2align 4,,10
	.p2align 3
.L370:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC0(%rip), %xmm0
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L403:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L370
.L437:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L405
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L406:
	cmpl	$1, %eax
	jne	.L370
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L384:
	addl	$1, 8(%r14)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L386:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L387:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC0(%rip), %xmm0
	jmp	.L388
.L405:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L406
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5046:
	.size	_ZN2v88platform15DefaultPlatform12RunIdleTasksEPNS_7IsolateEd, .-_ZN2v88platform15DefaultPlatform12RunIdleTasksEPNS_7IsolateEd
	.section	.text._ZN2v88platform12RunIdleTasksEPNS_8PlatformEPNS_7IsolateEd,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88platform12RunIdleTasksEPNS_8PlatformEPNS_7IsolateEd
	.type	_ZN2v88platform12RunIdleTasksEPNS_8PlatformEPNS_7IsolateEd, @function
_ZN2v88platform12RunIdleTasksEPNS_8PlatformEPNS_7IsolateEd:
.LFB4972:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88platform15DefaultPlatform12RunIdleTasksEPNS_7IsolateEd
	.cfi_endproc
.LFE4972:
	.size	_ZN2v88platform12RunIdleTasksEPNS_8PlatformEPNS_7IsolateEd, .-_ZN2v88platform12RunIdleTasksEPNS_8PlatformEPNS_7IsolateEd
	.section	.text._ZN2v88platform15DefaultPlatform23GetForegroundTaskRunnerEPNS_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform15DefaultPlatform23GetForegroundTaskRunnerEPNS_7IsolateE
	.type	_ZN2v88platform15DefaultPlatform23GetForegroundTaskRunnerEPNS_7IsolateE, @function
_ZN2v88platform15DefaultPlatform23GetForegroundTaskRunnerEPNS_7IsolateE:
.LFB5049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	80(%rbx), %r15
	subq	$72, %rsp
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	88(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L441
	movq	-88(%rbp), %rcx
	movq	%r15, %rsi
	movq	%rdx, %rax
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L443
.L442:
	cmpq	%rcx, 32(%rax)
	jnb	.L489
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L442
.L443:
	cmpq	%rsi, %r15
	je	.L441
	cmpq	%rcx, 32(%rsi)
	jbe	.L446
.L441:
	movq	136(%rbx), %rdx
	leaq	_ZN2v88platform12_GLOBAL__N_119DefaultTimeFunctionEv(%rip), %rax
	movl	$328, %edi
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	movq	%rdx, -96(%rbp)
	call	_Znwm@PLT
	movl	52(%rbx), %esi
	movq	-96(%rbp), %rdx
	movq	%rax, %r8
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r8)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r8), %r14
	movq	%rax, (%r8)
	movq	%r14, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88platform27DefaultForegroundTaskRunnerC1ENS0_15IdleTaskSupportEPFdvE@PLT
	movq	-88(%rbp), %rcx
	movl	$56, %edi
	movq	%rcx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %xmm1
	movq	-104(%rbp), %r8
	movq	88(%rbx), %r14
	movq	%rax, %rsi
	movq	%rcx, %xmm0
	movq	%r8, 48(%rax)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 32(%rax)
	testq	%r14, %r14
	jne	.L449
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L491:
	movq	16(%r14), %rax
	movl	$1, %edi
	testq	%rax, %rax
	je	.L450
.L492:
	movq	%rax, %r14
.L449:
	movq	32(%r14), %rdx
	cmpq	%rcx, %rdx
	ja	.L491
	movq	24(%r14), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	jne	.L492
.L450:
	testb	%dil, %dil
	jne	.L493
	cmpq	%rcx, %rdx
	jb	.L472
.L473:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L457
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r8)
.L458:
	cmpl	$1, %eax
	je	.L494
.L460:
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L493:
	cmpq	%r14, 96(%rbx)
	je	.L472
.L474:
	movq	%r14, %rdi
	movq	%r8, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rsi
	cmpq	32(%rax), %rcx
	movq	-112(%rbp), %r8
	jbe	.L473
.L472:
	movl	$1, %edi
	cmpq	%r14, %r15
	jne	.L495
.L455:
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 112(%rbx)
.L456:
	movq	88(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L478
	movq	-88(%rbp), %rcx
.L446:
	movq	%r15, %rsi
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L466
.L465:
	cmpq	%rcx, 32(%rdx)
	jnb	.L496
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L465
.L466:
	cmpq	%rsi, %r15
	je	.L464
	cmpq	%rcx, 32(%rsi)
	ja	.L464
.L469:
	movq	40(%rsi), %rax
	movq	%rax, (%r12)
	movq	48(%rsi), %rax
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L470
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L471
	lock addl	$1, 8(%rax)
.L470:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L497
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	-88(%rbp), %rax
	leaq	-64(%rbp), %rcx
	leaq	72(%rbx), %rdi
	leaq	-65(%rbp), %r8
	movq	%rax, -64(%rbp)
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_St10shared_ptrINS0_8platform27DefaultForegroundTaskRunnerEEESt10_Select1stIS9_ESt4lessIS2_ESaIS9_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESK_IJEEEEESt17_Rb_tree_iteratorIS9_ESt23_Rb_tree_const_iteratorIS9_EDpOT_
	movq	%rax, %rsi
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L471:
	addl	$1, 8(%rax)
	jmp	.L470
.L457:
	movl	8(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r8)
	jmp	.L458
.L495:
	xorl	%edi, %edi
	cmpq	32(%r14), %rcx
	setb	%dil
	jmp	.L455
.L490:
	movq	%r15, %r14
	cmpq	96(%rbx), %r15
	jne	.L474
	movl	$1, %edi
	jmp	.L455
.L494:
	movq	(%r8), %rax
	movq	%rsi, -104(%rbp)
	movq	%r8, %rdi
	movq	%r8, -96(%rbp)
	call	*16(%rax)
	testq	%r14, %r14
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rsi
	je	.L461
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r8)
.L462:
	cmpl	$1, %eax
	jne	.L460
	movq	(%r8), %rax
	movq	%rsi, -96(%rbp)
	movq	%r8, %rdi
	call	*24(%rax)
	movq	-96(%rbp), %rsi
	jmp	.L460
.L461:
	movl	12(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r8)
	jmp	.L462
.L497:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5049:
	.size	_ZN2v88platform15DefaultPlatform23GetForegroundTaskRunnerEPNS_7IsolateE, .-_ZN2v88platform15DefaultPlatform23GetForegroundTaskRunnerEPNS_7IsolateE
	.weak	_ZTVN2v88platform15DefaultPlatformE
	.section	.data.rel.ro.local._ZTVN2v88platform15DefaultPlatformE,"awG",@progbits,_ZTVN2v88platform15DefaultPlatformE,comdat
	.align 8
	.type	_ZTVN2v88platform15DefaultPlatformE, @object
	.size	_ZTVN2v88platform15DefaultPlatformE, 184
_ZTVN2v88platform15DefaultPlatformE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform15DefaultPlatformD1Ev
	.quad	_ZN2v88platform15DefaultPlatformD0Ev
	.quad	_ZN2v88platform15DefaultPlatform16GetPageAllocatorEv
	.quad	_ZN2v88Platform24OnCriticalMemoryPressureEv
	.quad	_ZN2v88Platform24OnCriticalMemoryPressureEm
	.quad	_ZN2v88platform15DefaultPlatform21NumberOfWorkerThreadsEv
	.quad	_ZN2v88platform15DefaultPlatform23GetForegroundTaskRunnerEPNS_7IsolateE
	.quad	_ZN2v88platform15DefaultPlatform18CallOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.quad	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	_ZN2v88platform15DefaultPlatform25CallDelayedOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.quad	_ZN2v88platform15DefaultPlatform22CallOnForegroundThreadEPNS_7IsolateEPNS_4TaskE
	.quad	_ZN2v88platform15DefaultPlatform29CallDelayedOnForegroundThreadEPNS_7IsolateEPNS_4TaskEd
	.quad	_ZN2v88platform15DefaultPlatform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE
	.quad	_ZN2v88platform15DefaultPlatform16IdleTasksEnabledEPNS_7IsolateE
	.quad	_ZN2v88platform15DefaultPlatform27MonotonicallyIncreasingTimeEv
	.quad	_ZN2v88platform15DefaultPlatform22CurrentClockTimeMillisEv
	.quad	_ZN2v88platform15DefaultPlatform20GetStackTracePrinterEv
	.quad	_ZN2v88platform15DefaultPlatform20GetTracingControllerEv
	.quad	_ZN2v88Platform19DumpWithoutCrashingEv
	.quad	_ZN2v88Platform11AddCrashKeyEiPKcm
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform30DefaultWorkerThreadsTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88platform27DefaultForegroundTaskRunnerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.globl	_ZN2v88platform15DefaultPlatform18kMaxThreadPoolSizeE
	.section	.rodata._ZN2v88platform15DefaultPlatform18kMaxThreadPoolSizeE,"a"
	.align 4
	.type	_ZN2v88platform15DefaultPlatform18kMaxThreadPoolSizeE, @object
	.size	_ZN2v88platform15DefaultPlatform18kMaxThreadPoolSizeE, 4
_ZN2v88platform15DefaultPlatform18kMaxThreadPoolSizeE:
	.long	8
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1093567616
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
