	.file	"trace-buffer.cc"
	.text
	.section	.text._ZN2v88platform7tracing21TraceBufferRingBuffer16GetEventByHandleEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing21TraceBufferRingBuffer16GetEventByHandleEm
	.type	_ZN2v88platform7tracing21TraceBufferRingBuffer16GetEventByHandleEm, @function
_ZN2v88platform7tracing21TraceBufferRingBuffer16GetEventByHandleEm:
.LFB4041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	48(%rbx), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	64(%rbx), %rsi
	salq	$6, %rcx
	divq	%rcx
	movq	72(%rbx), %rcx
	subq	%rsi, %rcx
	sarq	$3, %rcx
	movq	%rdx, %rdi
	shrq	$6, %rdi
	cmpq	%rdi, %rcx
	jbe	.L4
	movq	(%rsi,%rdi,8), %r13
	testq	%r13, %r13
	je	.L2
	cmpl	%eax, 10760(%r13)
	jne	.L4
	andl	$63, %edx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	8(%r13,%rax,8), %r13
.L2:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L2
	.cfi_endproc
.LFE4041:
	.size	_ZN2v88platform7tracing21TraceBufferRingBuffer16GetEventByHandleEm, .-_ZN2v88platform7tracing21TraceBufferRingBuffer16GetEventByHandleEm
	.section	.text._ZN2v88platform7tracing21TraceBufferRingBuffer5FlushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing21TraceBufferRingBuffer5FlushEv
	.type	_ZN2v88platform7tracing21TraceBufferRingBuffer5FlushEv, @function
_ZN2v88platform7tracing21TraceBufferRingBuffer5FlushEv:
.LFB4042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rax, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 96(%r13)
	jne	.L11
	movq	88(%r13), %rcx
	movl	$0, %eax
	leaq	1(%rcx), %rbx
	cmpq	%rbx, 48(%r13)
	cmovbe	%rax, %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	cmpq	%rcx, %rbx
	je	.L11
.L22:
	addq	$1, %rbx
	movl	$0, %eax
	cmpq	48(%r13), %rbx
	cmovnb	%rax, %rbx
.L13:
	movq	64(%r13), %rax
	leaq	(%rax,%rbx,8), %r12
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L14
	cmpq	$0, (%rsi)
	je	.L14
	movl	$8, %r14d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L15:
	movq	56(%r13), %rdi
	addq	%r14, %rsi
	addq	$1, %r15
	addq	$168, %r14
	movq	(%rdi), %rcx
	call	*16(%rcx)
	movq	(%r12), %rsi
	cmpq	(%rsi), %r15
	jb	.L15
	movq	88(%r13), %rcx
	cmpq	%rcx, %rbx
	jne	.L22
.L11:
	movq	56(%r13), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movb	$1, 96(%r13)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4042:
	.size	_ZN2v88platform7tracing21TraceBufferRingBuffer5FlushEv, .-_ZN2v88platform7tracing21TraceBufferRingBuffer5FlushEv
	.section	.text._ZN2v88platform7tracing21TraceBufferRingBuffer13AddTraceEventEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing21TraceBufferRingBuffer13AddTraceEventEPm
	.type	_ZN2v88platform7tracing21TraceBufferRingBuffer13AddTraceEventEPm, @function
_ZN2v88platform7tracing21TraceBufferRingBuffer13AddTraceEventEPm:
.LFB4038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 96(%rbx)
	je	.L24
	movq	64(%rbx), %r14
	xorl	%eax, %eax
.L25:
	movl	100(%rbx), %r15d
	movq	%rax, 88(%rbx)
	movb	$0, 96(%rbx)
	movq	(%r14), %rax
	leal	1(%r15), %edx
	movl	%edx, 100(%rbx)
	testq	%rax, %rax
	je	.L27
	movq	$0, (%rax)
	movl	%r15d, 10760(%rax)
.L35:
	movq	88(%rbx), %rdx
	movq	64(%rbx), %rax
	leaq	(%rax,%rdx,8), %rcx
	movq	(%rcx), %rax
	movq	(%rax), %rdx
.L26:
	leaq	1(%rdx), %rsi
	movq	%r13, %rdi
	movq	%rsi, (%rax)
	leaq	(%rdx,%rdx,4), %rsi
	leaq	(%rdx,%rsi,4), %rsi
	leaq	8(%rax,%rsi,8), %r14
	movq	(%rcx), %rax
	movl	10760(%rax), %eax
	imulq	48(%rbx), %rax
	addq	88(%rbx), %rax
	salq	$6, %rax
	addq	%rdx, %rax
	movq	%rax, (%r12)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	88(%rbx), %rsi
	movq	64(%rbx), %r14
	leaq	0(,%rsi,8), %rdi
	leaq	(%r14,%rdi), %rcx
	movq	(%rcx), %rax
	movq	(%rax), %rdx
	cmpq	$64, %rdx
	jne	.L26
	leaq	1(%rsi), %rax
	cmpq	%rax, 48(%rbx)
	ja	.L36
	xorl	%eax, %eax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	8(%r14,%rdi), %r14
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$10768, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	leaq	8(%rax), %rdx
	leaq	10760(%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L28:
	pxor	%xmm0, %xmm0
	movl	$0, 56(%rdx)
	addq	$168, %rdx
	movups	%xmm0, -64(%rdx)
	movq	$0, -48(%rdx)
	cmpq	%rcx, %rdx
	jne	.L28
	movl	%r15d, 10760(%rax)
	movq	(%r14), %rdi
	movq	%rax, (%r14)
	movq	%rdi, -56(%rbp)
	testq	%rdi, %rdi
	je	.L35
	leaq	10760(%rdi), %r15
	leaq	8(%rdi), %r14
	.p2align 4,,10
	.p2align 3
.L30:
	subq	$168, %r15
	movq	%r15, %rdi
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	cmpq	%r14, %r15
	jne	.L30
	movq	-56(%rbp), %rdi
	movl	$10768, %esi
	call	_ZdlPvm@PLT
	jmp	.L35
	.cfi_endproc
.LFE4038:
	.size	_ZN2v88platform7tracing21TraceBufferRingBuffer13AddTraceEventEPm, .-_ZN2v88platform7tracing21TraceBufferRingBuffer13AddTraceEventEPm
	.section	.text._ZN2v88platform7tracing21TraceBufferRingBufferD2Ev,"axG",@progbits,_ZN2v88platform7tracing21TraceBufferRingBufferD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88platform7tracing21TraceBufferRingBufferD2Ev
	.type	_ZN2v88platform7tracing21TraceBufferRingBufferD2Ev, @function
_ZN2v88platform7tracing21TraceBufferRingBufferD2Ev:
.LFB4953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform7tracing21TraceBufferRingBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	64(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r14
	je	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L39
	leaq	-160(%r13), %rax
	leaq	10592(%r13), %r15
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r15, %rdi
	subq	$168, %r15
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	cmpq	%r15, -56(%rbp)
	jne	.L40
	movl	$10768, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L39:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L41
	movq	64(%rbx), %r12
.L38:
	testq	%r12, %r12
	je	.L42
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L42:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	call	*8(%rax)
.L43:
	addq	$24, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE4953:
	.size	_ZN2v88platform7tracing21TraceBufferRingBufferD2Ev, .-_ZN2v88platform7tracing21TraceBufferRingBufferD2Ev
	.weak	_ZN2v88platform7tracing21TraceBufferRingBufferD1Ev
	.set	_ZN2v88platform7tracing21TraceBufferRingBufferD1Ev,_ZN2v88platform7tracing21TraceBufferRingBufferD2Ev
	.section	.text._ZN2v88platform7tracing21TraceBufferRingBufferD0Ev,"axG",@progbits,_ZN2v88platform7tracing21TraceBufferRingBufferD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88platform7tracing21TraceBufferRingBufferD0Ev
	.type	_ZN2v88platform7tracing21TraceBufferRingBufferD0Ev, @function
_ZN2v88platform7tracing21TraceBufferRingBufferD0Ev:
.LFB4955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform7tracing21TraceBufferRingBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	72(%rdi), %rbx
	movq	64(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L57
	.p2align 4,,10
	.p2align 3
.L60:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L58
	leaq	-160(%r14), %rax
	leaq	10592(%r14), %r15
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r15, %rdi
	subq	$168, %r15
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	cmpq	%r15, -56(%rbp)
	jne	.L59
	movl	$10768, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L58:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L60
	movq	64(%r12), %r13
.L57:
	testq	%r13, %r13
	je	.L61
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L61:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	(%rdi), %rax
	call	*8(%rax)
.L62:
	leaq	8(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4955:
	.size	_ZN2v88platform7tracing21TraceBufferRingBufferD0Ev, .-_ZN2v88platform7tracing21TraceBufferRingBufferD0Ev
	.section	.text._ZNK2v88platform7tracing21TraceBufferRingBuffer10MakeHandleEmjm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88platform7tracing21TraceBufferRingBuffer10MakeHandleEmjm
	.type	_ZNK2v88platform7tracing21TraceBufferRingBuffer10MakeHandleEmjm, @function
_ZNK2v88platform7tracing21TraceBufferRingBuffer10MakeHandleEmjm:
.LFB4043:
	.cfi_startproc
	endbr64
	movl	%edx, %edx
	imulq	48(%rdi), %rdx
	addq	%rsi, %rdx
	salq	$6, %rdx
	leaq	(%rdx,%rcx), %rax
	ret
	.cfi_endproc
.LFE4043:
	.size	_ZNK2v88platform7tracing21TraceBufferRingBuffer10MakeHandleEmjm, .-_ZNK2v88platform7tracing21TraceBufferRingBuffer10MakeHandleEmjm
	.section	.text._ZNK2v88platform7tracing21TraceBufferRingBuffer13ExtractHandleEmPmPjS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88platform7tracing21TraceBufferRingBuffer13ExtractHandleEmPmPjS3_
	.type	_ZNK2v88platform7tracing21TraceBufferRingBuffer13ExtractHandleEmPmPjS3_, @function
_ZNK2v88platform7tracing21TraceBufferRingBuffer13ExtractHandleEmPmPjS3_:
.LFB4044:
	.cfi_startproc
	endbr64
	movq	%rcx, %r10
	movq	48(%rdi), %rcx
	movq	%rdx, %r9
	movq	%rsi, %rax
	xorl	%edx, %edx
	salq	$6, %rcx
	divq	%rcx
	xorl	%edx, %edx
	movl	%eax, (%r10)
	movq	48(%rdi), %rcx
	movq	%rsi, %rax
	salq	$6, %rcx
	divq	%rcx
	movq	%rdx, %rsi
	movq	%rdx, %rax
	shrq	$6, %rax
	andl	$63, %esi
	movq	%rax, (%r9)
	movq	%rsi, (%r8)
	ret
	.cfi_endproc
.LFE4044:
	.size	_ZNK2v88platform7tracing21TraceBufferRingBuffer13ExtractHandleEmPmPjS3_, .-_ZNK2v88platform7tracing21TraceBufferRingBuffer13ExtractHandleEmPmPjS3_
	.section	.text._ZNK2v88platform7tracing21TraceBufferRingBuffer14NextChunkIndexEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88platform7tracing21TraceBufferRingBuffer14NextChunkIndexEm
	.type	_ZNK2v88platform7tracing21TraceBufferRingBuffer14NextChunkIndexEm, @function
_ZNK2v88platform7tracing21TraceBufferRingBuffer14NextChunkIndexEm:
.LFB4045:
	.cfi_startproc
	endbr64
	leaq	1(%rsi), %rax
	movl	$0, %edx
	cmpq	%rax, 48(%rdi)
	cmovbe	%rdx, %rax
	ret
	.cfi_endproc
.LFE4045:
	.size	_ZNK2v88platform7tracing21TraceBufferRingBuffer14NextChunkIndexEm, .-_ZNK2v88platform7tracing21TraceBufferRingBuffer14NextChunkIndexEm
	.section	.text._ZN2v88platform7tracing16TraceBufferChunkC2Ej,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing16TraceBufferChunkC2Ej
	.type	_ZN2v88platform7tracing16TraceBufferChunkC2Ej, @function
_ZN2v88platform7tracing16TraceBufferChunkC2Ej:
.LFB4050:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	leaq	8(%rdi), %rax
	leaq	10760(%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L80:
	pxor	%xmm0, %xmm0
	movl	$0, 56(%rax)
	addq	$168, %rax
	movups	%xmm0, -64(%rax)
	movq	$0, -48(%rax)
	cmpq	%rdx, %rax
	jne	.L80
	movl	%esi, 10760(%rdi)
	ret
	.cfi_endproc
.LFE4050:
	.size	_ZN2v88platform7tracing16TraceBufferChunkC2Ej, .-_ZN2v88platform7tracing16TraceBufferChunkC2Ej
	.globl	_ZN2v88platform7tracing16TraceBufferChunkC1Ej
	.set	_ZN2v88platform7tracing16TraceBufferChunkC1Ej,_ZN2v88platform7tracing16TraceBufferChunkC2Ej
	.section	.text._ZN2v88platform7tracing16TraceBufferChunk5ResetEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing16TraceBufferChunk5ResetEj
	.type	_ZN2v88platform7tracing16TraceBufferChunk5ResetEj, @function
_ZN2v88platform7tracing16TraceBufferChunk5ResetEj:
.LFB4052:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movl	%esi, 10760(%rdi)
	ret
	.cfi_endproc
.LFE4052:
	.size	_ZN2v88platform7tracing16TraceBufferChunk5ResetEj, .-_ZN2v88platform7tracing16TraceBufferChunk5ResetEj
	.section	.text._ZN2v88platform7tracing16TraceBufferChunk13AddTraceEventEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing16TraceBufferChunk13AddTraceEventEPm
	.type	_ZN2v88platform7tracing16TraceBufferChunk13AddTraceEventEPm, @function
_ZN2v88platform7tracing16TraceBufferChunk13AddTraceEventEPm:
.LFB4053:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, (%rdi)
	leaq	(%rax,%rax,4), %rdx
	movq	%rax, (%rsi)
	leaq	(%rax,%rdx,4), %rax
	leaq	8(%rdi,%rax,8), %rax
	ret
	.cfi_endproc
.LFE4053:
	.size	_ZN2v88platform7tracing16TraceBufferChunk13AddTraceEventEPm, .-_ZN2v88platform7tracing16TraceBufferChunk13AddTraceEventEPm
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm
	.type	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm, @function
_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm:
.LFB4616:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L121
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %rcx
	movq	%rcx, %r13
	subq	(%rdi), %r13
	movq	%r13, %rax
	sarq	$3, %rax
	movq	%rax, -80(%rbp)
	subq	%rax, %rsi
	movq	16(%rdi), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jb	.L86
	cmpq	$1, %rbx
	je	.L102
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L88
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L89
.L87:
	movq	$0, (%rdx)
.L89:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 8(%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L124
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	movq	%rdi, %rax
	cmovb	%rbx, %rax
	addq	%rdi, %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	leaq	(%rax,%r13), %rdx
	cmpq	$1, %rbx
	je	.L101
	leaq	-2(%rbx), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rdx,%rsi)
	cmpq	%rax, %rcx
	ja	.L95
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L93
.L101:
	movq	$0, (%rdx)
.L93:
	movq	8(%r12), %rax
	movq	(%r12), %r15
	movq	%rax, -72(%rbp)
	cmpq	%r15, %rax
	je	.L96
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r15)
	movq	%rax, (%rcx)
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L97
	leaq	-160(%r13), %rax
	leaq	10592(%r13), %r14
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r14, %rdi
	subq	$168, %r14
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	cmpq	-56(%rbp), %r14
	jne	.L98
	movl	$10768, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L97:
	addq	$8, -64(%rbp)
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L99
	movq	(%r12), %r15
.L96:
	testq	%r15, %r15
	je	.L100
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L100:
	movq	-88(%rbp), %rdi
	addq	-80(%rbp), %rbx
	leaq	(%rdi,%rbx,8), %rax
	movq	%rdi, (%r12)
	movq	%rax, 8(%r12)
	movq	-96(%rbp), %rax
	addq	%rdi, %rax
	movq	%rax, 16(%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	%rcx, %rdx
	jmp	.L87
.L124:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4616:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm, .-_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm
	.section	.text._ZN2v88platform7tracing21TraceBufferRingBufferC2EmPNS1_11TraceWriterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing21TraceBufferRingBufferC2EmPNS1_11TraceWriterE
	.type	_ZN2v88platform7tracing21TraceBufferRingBufferC2EmPNS1_11TraceWriterE, @function
_ZN2v88platform7tracing21TraceBufferRingBufferC2EmPNS1_11TraceWriterE:
.LFB4036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform7tracing21TraceBufferRingBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	%r12, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movb	$1, 96(%rbx)
	movl	$1, 100(%rbx)
	movq	%r13, 56(%rbx)
	testq	%r12, %r12
	jne	.L128
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm
	.cfi_endproc
.LFE4036:
	.size	_ZN2v88platform7tracing21TraceBufferRingBufferC2EmPNS1_11TraceWriterE, .-_ZN2v88platform7tracing21TraceBufferRingBufferC2EmPNS1_11TraceWriterE
	.globl	_ZN2v88platform7tracing21TraceBufferRingBufferC1EmPNS1_11TraceWriterE
	.set	_ZN2v88platform7tracing21TraceBufferRingBufferC1EmPNS1_11TraceWriterE,_ZN2v88platform7tracing21TraceBufferRingBufferC2EmPNS1_11TraceWriterE
	.section	.text._ZN2v88platform7tracing11TraceBuffer27CreateTraceBufferRingBufferEmPNS1_11TraceWriterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceBuffer27CreateTraceBufferRingBufferEmPNS1_11TraceWriterE
	.type	_ZN2v88platform7tracing11TraceBuffer27CreateTraceBufferRingBufferEmPNS1_11TraceWriterE, @function
_ZN2v88platform7tracing11TraceBuffer27CreateTraceBufferRingBufferEmPNS1_11TraceWriterE:
.LFB4054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$104, %edi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88platform7tracing21TraceBufferRingBufferC1EmPNS1_11TraceWriterE
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4054:
	.size	_ZN2v88platform7tracing11TraceBuffer27CreateTraceBufferRingBufferEmPNS1_11TraceWriterE, .-_ZN2v88platform7tracing11TraceBuffer27CreateTraceBufferRingBufferEmPNS1_11TraceWriterE
	.weak	_ZTVN2v88platform7tracing21TraceBufferRingBufferE
	.section	.data.rel.ro.local._ZTVN2v88platform7tracing21TraceBufferRingBufferE,"awG",@progbits,_ZTVN2v88platform7tracing21TraceBufferRingBufferE,comdat
	.align 8
	.type	_ZTVN2v88platform7tracing21TraceBufferRingBufferE, @object
	.size	_ZTVN2v88platform7tracing21TraceBufferRingBufferE, 56
_ZTVN2v88platform7tracing21TraceBufferRingBufferE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform7tracing21TraceBufferRingBufferD1Ev
	.quad	_ZN2v88platform7tracing21TraceBufferRingBufferD0Ev
	.quad	_ZN2v88platform7tracing21TraceBufferRingBuffer13AddTraceEventEPm
	.quad	_ZN2v88platform7tracing21TraceBufferRingBuffer16GetEventByHandleEm
	.quad	_ZN2v88platform7tracing21TraceBufferRingBuffer5FlushEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
