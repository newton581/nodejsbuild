	.file	"trace-config.cc"
	.text
	.section	.rodata._ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc
	.type	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc, @function
_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc:
.LFB3882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-464(%rbp), %rbx
	subq	$520, %rsp
	movq	%rdi, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-480(%rbp), %rax
	movq	%rbx, -480(%rbp)
	movq	%rax, -504(%rbp)
	testq	%rsi, %rsi
	je	.L7
	movq	%rsi, %rdi
	movq	%rsi, %r14
	call	strlen@PLT
	movq	%rax, -488(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L44
	cmpq	$1, %rax
	jne	.L5
	movzbl	(%r14), %edx
	movb	%dl, -464(%rbp)
	movq	%rbx, %rdx
.L6:
	movq	%rax, -472(%rbp)
	leaq	-320(%rbp), %r13
	movq	.LC1(%rip), %xmm1
	leaq	-448(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	%r13, %rdi
	movhps	.LC2(%rip), %xmm1
	movaps	%xmm1, -528(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rax
	leaq	-432(%rbp,%rax), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	pxor	%xmm0, %xmm0
	movdqa	-528(%rbp), %xmm1
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-480(%rbp), %r9
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-472(%rbp), %r8
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movq	%r9, %rax
	movl	$0, -360(%rbp)
	addq	%r8, %rax
	setne	%dl
	testq	%r9, %r9
	sete	%al
	andb	%al, %dl
	movb	%dl, -528(%rbp)
	jne	.L7
	movq	%r8, -488(%rbp)
	cmpq	$15, %r8
	ja	.L45
	cmpq	$1, %r8
	jne	.L10
	movzbl	(%r9), %eax
	movb	%al, -336(%rbp)
	movq	-544(%rbp), %rax
.L11:
	movq	%r8, -344(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movb	$0, (%rax,%r8)
	leaq	-424(%rbp), %r8
	movq	-352(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -512(%rbp)
	movl	$24, -360(%rbp)
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	-512(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L41
.L42:
	call	_ZdlPv@PLT
.L41:
	movl	-288(%rbp), %eax
	testl	%eax, %eax
	jne	.L14
	movq	-504(%rbp), %rsi
	movl	$44, %edx
	movq	%r14, %rdi
	movq	%rbx, -480(%rbp)
	movq	$0, -472(%rbp)
	movb	$0, -464(%rbp)
	call	_ZSt7getlineIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EES4_@PLT
	movq	-536(%rbp), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	cmpq	%r8, %rcx
	je	.L15
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %r15
	testq	%rdx, %rdx
	je	.L16
	movq	%rbx, -512(%rbp)
	movq	%rcx, %r12
	movq	%r8, %rbx
	movq	%r14, -560(%rbp)
	movq	%rdx, %r14
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$32, %r12
	cmpq	%r12, %rbx
	je	.L46
.L19:
	cmpq	8(%r12), %r14
	jne	.L17
	movq	(%r12), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L17
	movq	-512(%rbp), %rbx
	movq	%r15, %rdi
.L18:
	cmpq	%rbx, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movb	$1, -528(%rbp)
.L14:
	movq	.LC1(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC3(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	-552(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	movzbl	-528(%rbp), %eax
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	addq	$32, %rcx
	cmpq	%rcx, %r8
	je	.L20
.L16:
	cmpq	$0, 8(%rcx)
	jne	.L21
	movq	%r15, %rdi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L5:
	testq	%rax, %rax
	jne	.L48
	movq	%rbx, %rdx
	jmp	.L6
.L15:
	movq	-480(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	%rbx, %r15
	je	.L41
	movq	%r15, %rdi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-512(%rbp), %rbx
	movq	-560(%rbp), %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L10:
	testq	%r8, %r8
	jne	.L49
	movq	-544(%rbp), %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	-352(%rbp), %rdi
	leaq	-488(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -560(%rbp)
	movq	%r8, -512(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-512(%rbp), %r8
	movq	-560(%rbp), %r9
	movq	%rax, -352(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -336(%rbp)
.L9:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %r8
	movq	-352(%rbp), %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L44:
	movq	-504(%rbp), %rdi
	leaq	-488(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -464(%rbp)
.L4:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %rax
	movq	-480(%rbp), %rdx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L49:
	movq	-544(%rbp), %rdi
	jmp	.L9
.L47:
	call	__stack_chk_fail@PLT
.L48:
	movq	%rbx, %rdi
	jmp	.L4
	.cfi_endproc
.LFE3882:
	.size	_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc, .-_ZNK2v88platform7tracing11TraceConfig22IsCategoryGroupEnabledEPKc
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB4451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L77
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L68
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L78
.L52:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L67:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L79
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L55:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L56
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L76:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L80
.L60:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L57
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L56:
	cmpq	%r12, %rbx
	je	.L61
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L81
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L65
.L63:
	subq	%rbx, %r12
	addq	%r12, %r8
.L61:
	testq	%r15, %r15
	je	.L66
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L66:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L65
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L78:
	testq	%r8, %r8
	jne	.L53
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$32, %esi
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L79:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L55
.L53:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L52
.L77:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4451:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN2v88platform7tracing11TraceConfig24CreateDefaultTraceConfigEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceConfig24CreateDefaultTraceConfigEv
	.type	_ZN2v88platform7tracing11TraceConfig24CreateDefaultTraceConfigEv, @function
_ZN2v88platform7tracing11TraceConfig24CreateDefaultTraceConfigEv:
.LFB3881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$14454, %ecx
	xorl	%esi, %esi
	andb	$-4, 4(%rax)
	leaq	8(%rax), %rdi
	leaq	-64(%rbp), %rdx
	movq	%rax, %r12
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	movq	%rbx, -64(%rbp)
	movw	%cx, -48(%rbp)
	movq	$2, -56(%rbp)
	movb	$0, -46(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3881:
	.size	_ZN2v88platform7tracing11TraceConfig24CreateDefaultTraceConfigEv, .-_ZN2v88platform7tracing11TraceConfig24CreateDefaultTraceConfigEv
	.section	.text._ZN2v88platform7tracing11TraceConfig19AddIncludedCategoryEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceConfig19AddIncludedCategoryEPKc
	.type	_ZN2v88platform7tracing11TraceConfig19AddIncludedCategoryEPKc, @function
_ZN2v88platform7tracing11TraceConfig19AddIncludedCategoryEPKc:
.LFB3883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -96(%rbp)
	testq	%rsi, %rsi
	je	.L100
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	-96(%rbp), %r14
	movq	%rsi, %r15
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L101
	cmpq	$1, %rax
	jne	.L91
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L92:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L93
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	%r13, %rax
	je	.L102
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L95:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 16(%rbx)
.L87:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L104
	movq	%r13, %rdx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	8(%rbx), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L87
	call	_ZdlPv@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r14, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L90:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L95
.L103:
	call	__stack_chk_fail@PLT
.L104:
	movq	%r13, %rdi
	jmp	.L90
	.cfi_endproc
.LFE3883:
	.size	_ZN2v88platform7tracing11TraceConfig19AddIncludedCategoryEPKc, .-_ZN2v88platform7tracing11TraceConfig19AddIncludedCategoryEPKc
	.section	.data.rel.ro,"aw"
	.align 8
.LC1:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC2:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC3:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
