	.file	"default-worker-threads-task-runner.cc"
	.text
	.section	.text._ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB2231:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2231:
	.size	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd,"axG",@progbits,_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.type	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd, @function
_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd:
.LFB2232:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2232:
	.size	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd, .-_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.section	.text._ZNK2v810TaskRunner23NonNestableTasksEnabledEv,"axG",@progbits,_ZNK2v810TaskRunner23NonNestableTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.type	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv, @function
_ZNK2v810TaskRunner23NonNestableTasksEnabledEv:
.LFB2233:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2233:
	.size	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv, .-_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.section	.text._ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv,"axG",@progbits,_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.type	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv, @function
_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv:
.LFB2234:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2234:
	.size	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv, .-_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner16IdleTasksEnabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner16IdleTasksEnabledEv
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner16IdleTasksEnabledEv, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner16IdleTasksEnabledEv:
.LFB4068:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4068:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner16IdleTasksEnabledEv, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner16IdleTasksEnabledEv
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE:
.LFB4063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 8(%rbx)
	jne	.L10
	movq	(%r12), %rax
	movq	$0, (%r12)
	leaq	56(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88platform16DelayedTaskQueue6AppendESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	call	*8(%rax)
.L10:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4063:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd:
.LFB4064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$40, %rsp
	movsd	%xmm0, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	cmpb	$0, 8(%rbx)
	movsd	-56(%rbp), %xmm0
	jne	.L20
	movq	(%r12), %rax
	movq	$0, (%r12)
	leaq	56(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88platform16DelayedTaskQueue13AppendDelayedESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	movq	(%rdi), %rax
	call	*8(%rax)
.L20:
	movq	%r13, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4064:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.section	.rodata._ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE:
.LFB4067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4067:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThread3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThread3RunEv
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThread3RunEv, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThread3RunEv:
.LFB4077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	48(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS18GetCurrentThreadIdEv@PLT
	movl	%eax, 320(%r12)
	leaq	-32(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L32:
	movq	48(%rbx), %rax
	movq	%r12, %rdi
	leaq	56(%rax), %rsi
	call	_ZN2v88platform16DelayedTaskQueue7GetNextEv@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4077:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThread3RunEv, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThread3RunEv
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD2Ev
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD2Ev, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD2Ev:
.LFB4074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v84base6Thread4JoinEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base6ThreadD2Ev@PLT
	.cfi_endproc
.LFE4074:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD2Ev, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD2Ev
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD1Ev
	.set	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD1Ev,_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD2Ev
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev:
.LFB4076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4076:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunnerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD2Ev
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD2Ev, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD2Ev:
.LFB4057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	296(%rdi), %r13
	movq	288(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L44
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %r14
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r14, (%r15)
	movq	%r15, %rdi
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	%r15, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	movl	$56, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L45:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L57
.L47:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L45
	movq	(%r15), %rax
	leaq	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.L58
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r13
	jne	.L47
	.p2align 4,,10
	.p2align 3
.L57:
	movq	288(%rbx), %r12
.L44:
	testq	%r12, %r12
	je	.L48
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L48:
	leaq	56(%rbx), %rdi
	call	_ZN2v88platform16DelayedTaskQueueD1Ev@PLT
	addq	$8, %rsp
	leaq	16(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE4057:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD2Ev, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD2Ev
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD1Ev
	.set	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD1Ev,_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD2Ev
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunnerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD0Ev
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD0Ev, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD0Ev:
.LFB4059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	296(%rdi), %rbx
	movq	288(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L60
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %r14
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r14, (%r15)
	movq	%r15, %rdi
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	%r15, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	movl	$56, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L61:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L73
.L63:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L61
	movq	(%r15), %rax
	leaq	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.L74
	addq	$8, %r13
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L63
	.p2align 4,,10
	.p2align 3
.L73:
	movq	288(%r12), %r13
.L60:
	testq	%r13, %r13
	je	.L64
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L64:
	leaq	56(%r12), %rdi
	call	_ZN2v88platform16DelayedTaskQueueD1Ev@PLT
	leaq	16(%r12), %rdi
	call	_ZN2v84base5MutexD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$328, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4059:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD0Ev, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD0Ev
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner27MonotonicallyIncreasingTimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner27MonotonicallyIncreasingTimeEv
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner27MonotonicallyIncreasingTimeEv, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner27MonotonicallyIncreasingTimeEv:
.LFB4060:
	.cfi_startproc
	endbr64
	jmp	*312(%rdi)
	.cfi_endproc
.LFE4060:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner27MonotonicallyIncreasingTimeEv, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner27MonotonicallyIncreasingTimeEv
	.section	.text._ZNK2v88platform30DefaultWorkerThreadsTaskRunner24RunsTasksOnCurrentThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88platform30DefaultWorkerThreadsTaskRunner24RunsTasksOnCurrentThreadEv
	.type	_ZNK2v88platform30DefaultWorkerThreadsTaskRunner24RunsTasksOnCurrentThreadEv, @function
_ZNK2v88platform30DefaultWorkerThreadsTaskRunner24RunsTasksOnCurrentThreadEv:
.LFB4061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	320(%rdi), %ebx
	call	_ZN2v84base2OS18GetCurrentThreadIdEv@PLT
	cmpl	%eax, %ebx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4061:
	.size	_ZNK2v88platform30DefaultWorkerThreadsTaskRunner24RunsTasksOnCurrentThreadEv, .-_ZNK2v88platform30DefaultWorkerThreadsTaskRunner24RunsTasksOnCurrentThreadEv
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner9TerminateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner9TerminateEv
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner9TerminateEv, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner9TerminateEv:
.LFB4062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$24, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movb	$1, 8(%rbx)
	leaq	56(%rbx), %rdi
	call	_ZN2v88platform16DelayedTaskQueue9TerminateEv@PLT
	movq	288(%rbx), %rax
	movq	296(%rbx), %r12
	movq	%rax, -56(%rbp)
	cmpq	%r12, %rax
	je	.L79
	movq	%rax, %r14
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	%r13, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	movl	$56, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L80:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L88
.L82:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L80
	movq	0(%r13), %rdx
	leaq	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev(%rip), %rax
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L89
	addq	$8, %r14
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r14, %r12
	jne	.L82
	.p2align 4,,10
	.p2align 3
.L88:
	movq	-56(%rbp), %rax
	movq	%rax, 296(%rbx)
.L79:
	movl	$0, 320(%rbx)
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE4062:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner9TerminateEv, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner9TerminateEv
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner7GetNextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner7GetNextEv
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner7GetNextEv, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner7GetNextEv:
.LFB4069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$56, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88platform16DelayedTaskQueue7GetNextEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4069:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner7GetNextEv, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner7GetNextEv
	.section	.rodata._ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"V8 DefaultWorkerThreadsTaskRunner WorkerThread"
	.section	.rodata._ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Start()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_:
.LFB4071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	-48(%rbp), %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rax
	movl	$0, -40(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZN2v84base6ThreadC2ERKNS1_7OptionsE@PLT
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %rax
	movq	%rbx, 48(%r12)
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base6Thread5StartEv@PLT
	testb	%al, %al
	je	.L98
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4071:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC1EPS1_
	.set	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC1EPS1_,_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadC2EPS1_
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB4618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$1152921504606846975, %rsi
	subq	$56, %rsp
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -56(%rbp)
	cmpq	%rsi, %rax
	je	.L127
	movq	%rdx, %r15
	movq	%r13, %rdx
	subq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L114
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L128
.L102:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, -64(%rbp)
	addq	%rax, %r14
	movq	%r14, -72(%rbp)
	leaq	8(%rax), %r14
.L113:
	movq	(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r15)
	movq	-56(%rbp), %r15
	movq	%rax, (%rcx,%rdx)
	cmpq	%r15, %r13
	je	.L104
	movq	%rcx, %r14
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L105:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L129
.L107:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L105
	movq	(%r12), %rsi
	leaq	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev(%rip), %rax
	movq	8(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L130
	addq	$8, %r15
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rsi
	cmpq	%r15, %r13
	jne	.L107
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-64(%rbp), %rcx
	movq	%r13, %rax
	subq	-56(%rbp), %rax
	leaq	8(%rcx,%rax), %r14
.L104:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r13
	je	.L108
	subq	%r13, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L116
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L110:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L110
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L111
.L109:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L111:
	leaq	8(%r14,%rdi), %r14
.L108:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L112
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L112:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%r14, %xmm2
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L103
	movq	$0, -72(%rbp)
	movl	$8, %r14d
	movq	$0, -64(%rbp)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$8, %r14d
	jmp	.L102
.L116:
	movq	%r14, %rdx
	jmp	.L109
.L103:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	leaq	0(,%rsi,8), %r14
	jmp	.L102
.L127:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4618:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN2v88platform30DefaultWorkerThreadsTaskRunnerC2EjPFdvE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC2EjPFdvE
	.type	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC2EjPFdvE, @function
_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC2EjPFdvE:
.LFB4054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	addq	$16, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE(%rip), %rax
	movb	$0, -8(%rdi)
	movq	%rax, -16(%rdi)
	call	_ZN2v84base5MutexC1Ev@PLT
	leaq	56(%r15), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88platform16DelayedTaskQueueC1EPFdvE@PLT
	leaq	288(%r15), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 304(%r15)
	movq	%rax, -96(%rbp)
	movq	%rbx, 312(%r15)
	movl	$0, 320(%r15)
	movl	%r14d, 324(%r15)
	movups	%xmm0, 288(%r15)
	testl	%r14d, %r14d
	je	.L131
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r14
	leaq	16+_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE(%rip), %r13
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L149:
	movq	$0, -80(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 296(%r15)
.L135:
	movq	-80(%rbp), %r12
	testq	%r12, %r12
	je	.L136
	movq	(%r12), %rax
	leaq	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L137
	movq	%r13, (%r12)
	movq	%r12, %rdi
	call	_ZN2v84base6Thread4JoinEv@PLT
	movq	%r12, %rdi
	call	_ZN2v84base6ThreadD2Ev@PLT
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L136:
	addl	$1, %ebx
	cmpl	%ebx, -84(%rbp)
	je	.L131
.L138:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	movl	$0, -72(%rbp)
	movq	%rax, %r12
	leaq	.LC1(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v84base6ThreadC2ERKNS1_7OptionsE@PLT
	movq	%r13, (%r12)
	movq	%r12, %rdi
	movq	%r15, 48(%r12)
	call	_ZN2v84base6Thread5StartEv@PLT
	testb	%al, %al
	je	.L148
	movq	%r12, -80(%rbp)
	movq	296(%r15), %rsi
	cmpq	304(%r15), %rsi
	jne	.L149
	movq	-96(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorISt10unique_ptrIN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	*%rax
	cmpl	%ebx, -84(%rbp)
	jne	.L138
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4054:
	.size	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC2EjPFdvE, .-_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC2EjPFdvE
	.globl	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC1EjPFdvE
	.set	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC1EjPFdvE,_ZN2v88platform30DefaultWorkerThreadsTaskRunnerC2EjPFdvE
	.weak	_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE
	.section	.data.rel.ro.local._ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE,"awG",@progbits,_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE,comdat
	.align 8
	.type	_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE, @object
	.size	_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE, 96
_ZTVN2v88platform30DefaultWorkerThreadsTaskRunnerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunner8PostTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EE
	.quad	_ZN2v810TaskRunner19PostNonNestableTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS3_EEd
	.quad	_ZN2v810TaskRunner26PostNonNestableDelayedTaskESt10unique_ptrINS_4TaskESt14default_deleteIS2_EEd
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12PostIdleTaskESt10unique_ptrINS_8IdleTaskESt14default_deleteIS3_EE
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunner16IdleTasksEnabledEv
	.quad	_ZNK2v810TaskRunner23NonNestableTasksEnabledEv
	.quad	_ZNK2v810TaskRunner30NonNestableDelayedTasksEnabledEv
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD1Ev
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunnerD0Ev
	.weak	_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE
	.section	.data.rel.ro.local._ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE,"awG",@progbits,_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE,comdat
	.align 8
	.type	_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE, @object
	.size	_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE, 40
_ZTVN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD1Ev
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThreadD0Ev
	.quad	_ZN2v88platform30DefaultWorkerThreadsTaskRunner12WorkerThread3RunEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
