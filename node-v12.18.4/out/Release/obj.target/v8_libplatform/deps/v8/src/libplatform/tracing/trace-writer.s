	.file	"trace-writer.cc"
	.text
	.section	.text._ZN2v88platform7tracing15JSONTraceWriter5FlushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriter5FlushEv
	.type	_ZN2v88platform7tracing15JSONTraceWriter5FlushEv, @function
_ZN2v88platform7tracing15JSONTraceWriter5FlushEv:
.LFB4295:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4295:
	.size	_ZN2v88platform7tracing15JSONTraceWriter5FlushEv, .-_ZN2v88platform7tracing15JSONTraceWriter5FlushEv
	.section	.rodata._ZN2v88platform7tracing15JSONTraceWriterD2Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"]}"
	.section	.text._ZN2v88platform7tracing15JSONTraceWriterD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriterD2Ev
	.type	_ZN2v88platform7tracing15JSONTraceWriterD2Ev, @function
_ZN2v88platform7tracing15JSONTraceWriterD2Ev:
.LFB4291:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88platform7tracing15JSONTraceWriterE(%rip), %rax
	movl	$2, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE4291:
	.size	_ZN2v88platform7tracing15JSONTraceWriterD2Ev, .-_ZN2v88platform7tracing15JSONTraceWriterD2Ev
	.globl	_ZN2v88platform7tracing15JSONTraceWriterD1Ev
	.set	_ZN2v88platform7tracing15JSONTraceWriterD1Ev,_ZN2v88platform7tracing15JSONTraceWriterD2Ev
	.section	.text._ZN2v88platform7tracing15JSONTraceWriterD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriterD0Ev
	.type	_ZN2v88platform7tracing15JSONTraceWriterD0Ev, @function
_ZN2v88platform7tracing15JSONTraceWriterD0Ev:
.LFB4293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform7tracing15JSONTraceWriterE(%rip), %rax
	movl	$2, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4293:
	.size	_ZN2v88platform7tracing15JSONTraceWriterD0Ev, .-_ZN2v88platform7tracing15JSONTraceWriterD0Ev
	.section	.rodata._ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"true"
.LC2:
	.string	"false"
.LC5:
	.string	".0"
.LC6:
	.string	"\"NaN\""
.LC8:
	.string	"\"-Infinity\""
.LC9:
	.string	"\"Infinity\""
.LC10:
	.string	"\""
.LC11:
	.string	"\"nullptr\""
.LC12:
	.string	"\\b"
.LC13:
	.string	"\\f"
.LC14:
	.string	"\\n"
.LC15:
	.string	"\\r"
.LC16:
	.string	"\\t"
.LC17:
	.string	"\\\""
.LC18:
	.string	"\\\\"
.LC19:
	.string	"unreachable code"
	.section	.text._ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE
	.type	_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE, @function
_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE:
.LFB4272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$7, %sil
	ja	.L7
	leaq	.L9(%rip), %rcx
	movq	%rdx, %r12
	movzbl	%sil, %edx
	movq	%rdi, %r14
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE,"a",@progbits
	.align 4
	.align 4
.L9:
	.long	.L7-.L9
	.long	.L14-.L9
	.long	.L13-.L9
	.long	.L12-.L9
	.long	.L11-.L9
	.long	.L10-.L9
	.long	.L8-.L9
	.long	.L8-.L9
	.section	.text._ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rdi), %r14
	testq	%r12, %r12
	je	.L65
	movq	%r12, %rdi
	leaq	.L42(%rip), %r13
	call	strlen@PLT
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	(%r15,%r12), %rbx
	testq	%r15, %r15
	je	.L50
	.p2align 4,,10
	.p2align 3
.L51:
	movzbl	(%r12), %edx
	cmpb	$34, %dl
	jg	.L39
	cmpb	$7, %dl
	jle	.L40
	leal	-8(%rdx), %eax
	cmpb	$26, %al
	ja	.L40
	movzbl	%al, %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE
	.align 4
	.align 4
.L42:
	.long	.L47-.L42
	.long	.L46-.L42
	.long	.L45-.L42
	.long	.L40-.L42
	.long	.L44-.L42
	.long	.L43-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L40-.L42
	.long	.L41-.L42
	.section	.text._ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE
.L41:
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$1, %r12
	cmpq	%r12, %rbx
	jne	.L51
.L50:
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L49
.L44:
	movl	$2, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L49
.L45:
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L49
.L46:
	movl	$2, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L49
.L47:
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L49
.L40:
	movb	%dl, -497(%rbp)
	leaq	-497(%rbp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L14:
	cmpb	$1, %r12b
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rax
	movq	8(%rdi), %rdi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testb	%r12b, %r12b
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	movq	8(%rdi), %rdi
	movq	%r12, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
	movq	8(%rdi), %rdi
	movq	%r12, %rsi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movsd	.LC4(%rip), %xmm2
	movq	%r12, %xmm1
	andpd	.LC3(%rip), %xmm1
	leaq	-480(%rbp), %rbx
	movq	%rbx, -496(%rbp)
	leaq	-496(%rbp), %r13
	movq	%r12, %xmm0
	ucomisd	%xmm1, %xmm2
	movb	$0, -480(%rbp)
	movq	$0, -488(%rbp)
	jnb	.L67
	ucomisd	%xmm0, %xmm0
	jp	.L68
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L69
	movl	$10, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	.LC9(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L32:
	movq	8(%r14), %rdi
	movq	-488(%rbp), %rdx
	movq	-496(%rbp), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-496(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L6
	call	_ZdlPv@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	movq	8(%rdi), %r13
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L39:
	cmpb	$92, %dl
	jne	.L40
	movl	$2, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L67:
	movq	.LC20(%rip), %xmm2
	movq	%r12, -544(%rbp)
	leaq	-320(%rbp), %r12
	leaq	-432(%rbp), %r15
	movq	%r12, %rdi
	movhps	.LC21(%rip), %xmm2
	movaps	%xmm2, -528(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm1, %xmm1
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm1, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm1, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm1, %xmm1
	movdqa	-528(%rbp), %xmm2
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm2, -432(%rbp)
	movaps	%xmm1, -416(%rbp)
	movaps	%xmm1, -400(%rbp)
	movaps	%xmm1, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movsd	-544(%rbp), %xmm0
	movq	%r15, %rdi
	leaq	-448(%rbp), %r15
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r15, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L18
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L19
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L20:
	movq	-464(%rbp), %rax
	movq	-496(%rbp), %rdi
	movq	-456(%rbp), %rdx
	cmpq	%r15, %rax
	je	.L70
	movq	-448(%rbp), %rcx
	cmpq	%rbx, %rdi
	je	.L71
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	-480(%rbp), %rsi
	movq	%rax, -496(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -488(%rbp)
	testq	%rdi, %rdi
	je	.L26
	movq	%rdi, -464(%rbp)
	movq	%rsi, -448(%rbp)
.L24:
	movq	$0, -456(%rbp)
	movb	$0, (%rdi)
	movq	-464(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	xorl	%edx, %edx
	movl	$46, %esi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEcm@PLT
	cmpq	$-1, %rax
	je	.L72
.L29:
	movq	.LC20(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC22(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L32
.L18:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$9, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$11, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L19:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L70:
	testq	%rdx, %rdx
	je	.L22
	cmpq	$1, %rdx
	je	.L73
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-456(%rbp), %rdx
	movq	-496(%rbp), %rdi
.L22:
	movq	%rdx, -488(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-464(%rbp), %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	%rax, -496(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -488(%rbp)
.L26:
	movq	%r15, -464(%rbp)
	leaq	-448(%rbp), %r15
	movq	%r15, %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%edx, %edx
	movl	$101, %esi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEcm@PLT
	cmpq	$-1, %rax
	jne	.L29
	xorl	%edx, %edx
	movl	$69, %esi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEcm@PLT
	cmpq	$-1, %rax
	jne	.L29
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	.LC6(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L32
.L73:
	movzbl	-448(%rbp), %eax
	movb	%al, (%rdi)
	movq	-456(%rbp), %rdx
	movq	-496(%rbp), %rdi
	jmp	.L22
.L7:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4272:
	.size	_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE, .-_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE
	.section	.rodata._ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE.str1.1,"aMS",@progbits,1
.LC23:
	.string	","
.LC24:
	.string	"{\"pid\":"
.LC25:
	.string	",\"tid\":"
.LC26:
	.string	",\"ts\":"
.LC27:
	.string	",\"tts\":"
.LC28:
	.string	",\"ph\":\""
.LC29:
	.string	"\",\"cat\":\""
.LC30:
	.string	"\",\"name\":\""
.LC31:
	.string	"\",\"dur\":"
.LC32:
	.string	",\"tdur\":"
.LC33:
	.string	",\"bind_id\":\"0x"
.LC34:
	.string	",\"flow_in\":true"
.LC35:
	.string	",\"flow_out\":true"
.LC36:
	.string	",\"scope\":\""
.LC37:
	.string	",\"id\":\"0x"
.LC38:
	.string	",\"args\":{"
.LC39:
	.string	"\":"
.LC40:
	.string	"}}"
	.section	.text._ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE
	.type	_ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE, @function
_ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE:
.LFB4294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 16(%rdi)
	jne	.L107
.L75:
	movb	$1, 16(%r14)
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	0(%r13), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$7, %edx
	leaq	.LC25(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%r13), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$6, %edx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	136(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movl	$7, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	144(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movl	$7, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	8(%r13), %eax
	movq	%r12, %rdi
	leaq	-97(%rbp), %rsi
	movl	$1, %edx
	movb	%al, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC29(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r13), %rdi
	call	_ZN2v88platform7tracing17TracingController20GetCategoryGroupNameEPKh@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L108
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L77:
	movl	$10, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r13), %r15
	testq	%r15, %r15
	je	.L109
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L79:
	movl	$8, %edx
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	152(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$8, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	160(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	128(%r13), %eax
	movq	8(%r14), %r12
	testb	$3, %ah
	jne	.L80
.L81:
	testb	$2, %al
	je	.L83
	cmpq	$0, 24(%r13)
	je	.L84
	movl	$10, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%r13), %r15
	testq	%r15, %r15
	je	.L110
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L86:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %r12
.L84:
	movq	%r12, %rdi
	movl	$9, %edx
	leaq	.LC37(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	movq	40(%r13), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	addq	-24(%rax), %rbx
	movl	24(%rbx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rbx)
	movq	8(%r14), %r12
.L83:
	movl	$9, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	56(%r13), %eax
	testl	%eax, %eax
	jle	.L87
	leaq	-96(%rbp), %rax
	leaq	88(%r13), %r12
	xorl	%ebx, %ebx
	movq	%rax, -128(%rbp)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L92:
	movq	(%r12), %rdx
	movq	%r14, %rdi
	call	_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEhNS1_11TraceObject8ArgValueE
.L94:
	movq	8(%r14), %rdi
	addq	$1, %rbx
	addq	$8, %r12
	cmpl	%ebx, 56(%r13)
	jle	.L89
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L88:
	movq	8(%r14), %r15
	leaq	.LC10(%rip), %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L111
	movq	%rsi, %rdi
	movq	%rsi, -120(%rbp)
	call	strlen@PLT
	movq	-120(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L91:
	leaq	.LC39(%rip), %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	80(%r13,%rbx), %esi
	cmpb	$8, %sil
	jne	.L92
	movq	16(%r12), %rdi
	leaq	-80(%rbp), %r15
	movq	-128(%rbp), %rsi
	movb	$0, -80(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r14), %rdi
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L94
	call	_ZdlPv@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%r15), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r14), %rdi
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$2, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	$14, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	movq	48(%r13), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	addq	-24(%rax), %rbx
	movl	24(%rbx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rbx)
	movl	128(%r13), %eax
	testb	$1, %ah
	jne	.L113
.L82:
	movq	8(%r14), %r12
	testb	$2, %ah
	je	.L81
	movq	%r12, %rdi
	movl	$16, %edx
	leaq	.LC35(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	128(%r13), %eax
	movq	8(%r14), %r12
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %r12
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L108:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L77
.L110:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L113:
	movq	8(%r14), %rdi
	movl	$15, %edx
	leaq	.LC34(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	128(%r13), %eax
	jmp	.L82
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4294:
	.size	_ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE, .-_ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE
	.section	.text._ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEPNS_24ConvertableToTraceFormatE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEPNS_24ConvertableToTraceFormatE
	.type	_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEPNS_24ConvertableToTraceFormatE, @function
_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEPNS_24ConvertableToTraceFormatE:
.LFB4276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	-48(%rbp), %r12
	leaq	-64(%rbp), %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, -64(%rbp)
	movq	$0, -56(%rbp)
	movb	$0, -48(%rbp)
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L118:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4276:
	.size	_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEPNS_24ConvertableToTraceFormatE, .-_ZN2v88platform7tracing15JSONTraceWriter14AppendArgValueEPNS_24ConvertableToTraceFormatE
	.section	.rodata._ZN2v88platform7tracing15JSONTraceWriterC2ERSo.str1.1,"aMS",@progbits,1
.LC41:
	.string	"{\""
.LC42:
	.string	"\":["
	.section	.text._ZN2v88platform7tracing15JSONTraceWriterC2ERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriterC2ERSo
	.type	_ZN2v88platform7tracing15JSONTraceWriterC2ERSo, @function
_ZN2v88platform7tracing15JSONTraceWriterC2ERSo:
.LFB4278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 8(%rdi)
	leaq	.LC41(%rip), %rsi
	movabsq	$7311107346843923060, %rax
	movq	%rax, -48(%rbp)
	movl	$29806, %eax
	movw	%ax, -40(%rbp)
	leaq	16+_ZTVN2v88platform7tracing15JSONTraceWriterE(%rip), %rax
	movq	%rax, (%rdi)
	movb	$0, 16(%rdi)
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movb	$115, -38(%rbp)
	movq	$11, -56(%rbp)
	movb	$0, -37(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC42(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4278:
	.size	_ZN2v88platform7tracing15JSONTraceWriterC2ERSo, .-_ZN2v88platform7tracing15JSONTraceWriterC2ERSo
	.globl	_ZN2v88platform7tracing15JSONTraceWriterC1ERSo
	.set	_ZN2v88platform7tracing15JSONTraceWriterC1ERSo,_ZN2v88platform7tracing15JSONTraceWriterC2ERSo
	.section	.text._ZN2v88platform7tracing15JSONTraceWriterC2ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing15JSONTraceWriterC2ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88platform7tracing15JSONTraceWriterC2ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88platform7tracing15JSONTraceWriterC2ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88platform7tracing15JSONTraceWriterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movl	$2, %edx
	movq	%rsi, 8(%rdi)
	leaq	.LC41(%rip), %rsi
	movq	%rax, (%rdi)
	movb	$0, 16(%rdi)
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	popq	%rbx
	popq	%r12
	movl	$3, %edx
	movq	%rax, %rdi
	leaq	.LC42(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE4288:
	.size	_ZN2v88platform7tracing15JSONTraceWriterC2ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88platform7tracing15JSONTraceWriterC2ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.globl	_ZN2v88platform7tracing15JSONTraceWriterC1ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.set	_ZN2v88platform7tracing15JSONTraceWriterC1ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,_ZN2v88platform7tracing15JSONTraceWriterC2ERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSo
	.type	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSo, @function
_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSo:
.LFB4296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88platform7tracing15JSONTraceWriterC1ERSo
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4296:
	.size	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSo, .-_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSo
	.section	.text._ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	%r13, %rdi
	movl	$2, %edx
	leaq	.LC41(%rip), %rsi
	movq	%rax, %r12
	leaq	16+_ZTVN2v88platform7tracing15JSONTraceWriterE(%rip), %rax
	movq	%r13, 8(%r12)
	movq	%rax, (%r12)
	movb	$0, 16(%r12)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC42(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4297:
	.size	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.weak	_ZTVN2v88platform7tracing15JSONTraceWriterE
	.section	.data.rel.ro.local._ZTVN2v88platform7tracing15JSONTraceWriterE,"awG",@progbits,_ZTVN2v88platform7tracing15JSONTraceWriterE,comdat
	.align 8
	.type	_ZTVN2v88platform7tracing15JSONTraceWriterE, @object
	.size	_ZTVN2v88platform7tracing15JSONTraceWriterE, 48
_ZTVN2v88platform7tracing15JSONTraceWriterE:
	.quad	0
	.quad	0
	.quad	_ZN2v88platform7tracing15JSONTraceWriterD1Ev
	.quad	_ZN2v88platform7tracing15JSONTraceWriterD0Ev
	.quad	_ZN2v88platform7tracing15JSONTraceWriter16AppendTraceEventEPNS1_11TraceObjectE
	.quad	_ZN2v88platform7tracing15JSONTraceWriter5FlushEv
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	4294967295
	.long	2146435071
	.section	.data.rel.ro,"aw"
	.align 8
.LC20:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC21:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC22:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
