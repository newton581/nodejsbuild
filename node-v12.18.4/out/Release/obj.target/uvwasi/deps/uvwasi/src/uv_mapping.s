	.file	"uv_mapping.c"
	.text
	.p2align 4
	.globl	uvwasi__translate_uv_error
	.hidden	uvwasi__translate_uv_error
	.type	uvwasi__translate_uv_error, @function
uvwasi__translate_uv_error:
.LFB54:
	.cfi_startproc
	endbr64
	leal	125(%rdi), %eax
	cmpl	$125, %eax
	ja	.L2
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L60-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L59-.L4
	.long	.L58-.L4
	.long	.L2-.L4
	.long	.L57-.L4
	.long	.L56-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L55-.L4
	.long	.L54-.L4
	.long	.L53-.L4
	.long	.L52-.L4
	.long	.L51-.L4
	.long	.L2-.L4
	.long	.L50-.L4
	.long	.L49-.L4
	.long	.L48-.L4
	.long	.L47-.L4
	.long	.L46-.L4
	.long	.L2-.L4
	.long	.L45-.L4
	.long	.L2-.L4
	.long	.L44-.L4
	.long	.L43-.L4
	.long	.L42-.L4
	.long	.L41-.L4
	.long	.L40-.L4
	.long	.L39-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L38-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L37-.L4
	.long	.L36-.L4
	.long	.L35-.L4
	.long	.L2-.L4
	.long	.L34-.L4
	.long	.L2-.L4
	.long	.L33-.L4
	.long	.L2-.L4
	.long	.L32-.L4
	.long	.L31-.L4
	.long	.L30-.L4
	.long	.L29-.L4
	.long	.L28-.L4
	.long	.L27-.L4
	.long	.L26-.L4
	.long	.L2-.L4
	.long	.L25-.L4
	.long	.L24-.L4
	.long	.L23-.L4
	.long	.L22-.L4
	.long	.L21-.L4
	.long	.L20-.L4
	.long	.L19-.L4
	.long	.L18-.L4
	.long	.L17-.L4
	.long	.L2-.L4
	.long	.L16-.L4
	.long	.L15-.L4
	.long	.L14-.L4
	.long	.L13-.L4
	.long	.L2-.L4
	.long	.L12-.L4
	.long	.L2-.L4
	.long	.L61-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%edi, %edi
	movl	$52, %eax
	cmovg	%edi, %eax
	ret
.L3:
	xorl	%eax, %eax
	ret
.L5:
	movl	$63, %eax
	ret
.L6:
	movl	$44, %eax
	ret
.L7:
	movl	$71, %eax
	ret
.L8:
	movl	$27, %eax
	ret
.L9:
	movl	$29, %eax
	ret
.L10:
	movl	$60, %eax
	ret
.L12:
	movl	$8, %eax
	ret
.L13:
	movl	$6, %eax
	ret
.L14:
	movl	$48, %eax
	ret
.L16:
	movl	$21, %eax
	ret
.L17:
	movl	$10, %eax
	ret
.L18:
	movl	$20, %eax
	ret
.L19:
	movl	$75, %eax
	ret
.L20:
	movl	$43, %eax
	ret
.L21:
	movl	$54, %eax
	ret
.L22:
	movl	$31, %eax
	ret
.L23:
	movl	$28, %eax
	ret
.L24:
	movl	$41, %eax
	ret
.L25:
	movl	$33, %eax
	ret
.L26:
	movl	$74, %eax
	ret
.L27:
	movl	$22, %eax
	ret
.L28:
	movl	$51, %eax
	ret
.L29:
	movl	$70, %eax
	ret
.L30:
	movl	$69, %eax
	ret
.L31:
	movl	$34, %eax
	ret
.L32:
	movl	$64, %eax
	ret
.L33:
	movl	$68, %eax
	ret
.L34:
	movl	$37, %eax
	ret
.L35:
	movl	$52, %eax
	ret
.L36:
	movl	$55, %eax
	ret
.L37:
	movl	$32, %eax
	ret
.L38:
	movl	$65, %eax
	ret
.L39:
	movl	$57, %eax
	ret
.L40:
	movl	$17, %eax
	ret
.L41:
	movl	$35, %eax
	ret
.L42:
	movl	$67, %eax
	ret
.L43:
	movl	$50, %eax
	ret
.L44:
	movl	$66, %eax
	ret
.L45:
	movl	$58, %eax
	ret
.L46:
	movl	$5, %eax
	ret
.L47:
	movl	$3, %eax
	ret
.L48:
	movl	$4, %eax
	ret
.L49:
	movl	$38, %eax
	ret
.L50:
	movl	$40, %eax
	ret
.L51:
	movl	$13, %eax
	ret
.L52:
	movl	$15, %eax
	ret
.L53:
	movl	$42, %eax
	ret
.L54:
	movl	$30, %eax
	ret
.L55:
	movl	$53, %eax
	ret
.L56:
	movl	$73, %eax
	ret
.L57:
	movl	$14, %eax
	ret
.L58:
	movl	$23, %eax
	ret
.L59:
	movl	$7, %eax
	ret
.L60:
	movl	$11, %eax
	ret
.L61:
	movl	$1, %eax
	ret
.L15:
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE54:
	.size	uvwasi__translate_uv_error, .-uvwasi__translate_uv_error
	.p2align 4
	.globl	uvwasi__translate_to_uv_signal
	.hidden	uvwasi__translate_to_uv_signal
	.type	uvwasi__translate_to_uv_signal, @function
uvwasi__translate_to_uv_signal:
.LFB55:
	.cfi_startproc
	endbr64
	subl	$1, %edi
	movl	$-1, %eax
	cmpb	$29, %dil
	ja	.L64
	movzbl	%dil, %edi
	leaq	CSWTCH.2(%rip), %rax
	movl	(%rax,%rdi,4), %eax
.L64:
	ret
	.cfi_endproc
.LFE55:
	.size	uvwasi__translate_to_uv_signal, .-uvwasi__translate_to_uv_signal
	.p2align 4
	.globl	uvwasi__timespec_to_timestamp
	.hidden	uvwasi__timespec_to_timestamp
	.type	uvwasi__timespec_to_timestamp, @function
uvwasi__timespec_to_timestamp:
.LFB56:
	.cfi_startproc
	endbr64
	imulq	$1000000000, (%rdi), %rax
	addq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE56:
	.size	uvwasi__timespec_to_timestamp, .-uvwasi__timespec_to_timestamp
	.p2align 4
	.globl	uvwasi__stat_to_filetype
	.hidden	uvwasi__stat_to_filetype
	.type	uvwasi__stat_to_filetype, @function
uvwasi__stat_to_filetype:
.LFB57:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	andl	$61440, %eax
	cmpq	$32768, %rax
	je	.L70
	cmpq	$16384, %rax
	je	.L71
	cmpq	$8192, %rax
	je	.L72
	cmpq	$40960, %rax
	je	.L73
	cmpq	$49152, %rax
	je	.L74
	cmpq	$4096, %rax
	je	.L74
	cmpq	$24576, %rax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$6, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$7, %eax
	ret
	.cfi_endproc
.LFE57:
	.size	uvwasi__stat_to_filetype, .-uvwasi__stat_to_filetype
	.p2align 4
	.globl	uvwasi__stat_to_filestat
	.hidden	uvwasi__stat_to_filestat
	.type	uvwasi__stat_to_filestat, @function
uvwasi__stat_to_filestat:
.LFB58:
	.cfi_startproc
	endbr64
	movdqu	(%rdi), %xmm0
	movdqu	48(%rdi), %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rsi)
	movdqu	16(%rdi), %xmm0
	movdqu	48(%rdi), %xmm2
	shufpd	$2, %xmm2, %xmm0
	movups	%xmm0, 24(%rsi)
	movq	8(%rdi), %rax
	andl	$61440, %eax
	cmpq	$32768, %rax
	je	.L77
	cmpq	$16384, %rax
	je	.L78
	cmpq	$8192, %rax
	je	.L79
	cmpq	$40960, %rax
	je	.L80
	cmpq	$49152, %rax
	je	.L81
	cmpq	$4096, %rax
	je	.L81
	cmpq	$24576, %rax
	sete	%al
.L76:
	movb	%al, 16(%rsi)
	imulq	$1000000000, 96(%rdi), %rax
	addq	104(%rdi), %rax
	movq	%rax, 40(%rsi)
	imulq	$1000000000, 112(%rdi), %rax
	addq	120(%rdi), %rax
	movq	%rax, 48(%rsi)
	imulq	$1000000000, 128(%rdi), %rax
	addq	136(%rdi), %rax
	movq	%rax, 56(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$6, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$4, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$3, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$2, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$7, %eax
	jmp	.L76
	.cfi_endproc
.LFE58:
	.size	uvwasi__stat_to_filestat, .-uvwasi__stat_to_filestat
	.p2align 4
	.globl	uvwasi__get_filetype_by_fd
	.hidden	uvwasi__get_filetype_by_fd
	.type	uvwasi__get_filetype_by_fd, @function
uvwasi__get_filetype_by_fd:
.LFB59:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	%edi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-480(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edi, %r12d
	xorl	%edi, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%r14, %rsi
	subq	$448, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uv_fs_fstat@PLT
	testl	%eax, %eax
	jne	.L98
	movq	-360(%rbp), %rax
	andl	$61440, %eax
	cmpq	$32768, %rax
	je	.L91
	cmpq	$16384, %rax
	je	.L92
	cmpq	$8192, %rax
	je	.L93
	cmpq	$40960, %rax
	je	.L94
	cmpq	$4096, %rax
	je	.L95
	cmpq	$49152, %rax
	je	.L95
	cmpq	$24576, %rax
	sete	%al
.L87:
	movb	%al, (%rbx)
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	cmpb	$6, (%rbx)
	je	.L99
.L82:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$448, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	movl	%r12d, %edi
	call	uv_guess_handle@PLT
	cmpl	$14, %eax
	je	.L100
	movb	$0, (%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$448, %rsp
	movl	%r13d, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movl	$2, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L99:
	movl	%r12d, %edi
	call	uv_guess_handle@PLT
	cmpl	$15, %eax
	jne	.L82
	movb	$5, (%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L100:
	movb	$2, (%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$6, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$4, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$3, %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$7, %eax
	jmp	.L87
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE59:
	.size	uvwasi__get_filetype_by_fd, .-uvwasi__get_filetype_by_fd
	.section	.rodata
	.align 32
	.type	CSWTCH.2, @object
	.size	CSWTCH.2, 120
CSWTCH.2:
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	31
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
