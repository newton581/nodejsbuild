	.file	"uvwasi.c"
	.text
	.p2align 4
	.type	default_realloc, @function
default_realloc:
.LFB93:
	.cfi_startproc
	endbr64
	jmp	realloc@PLT
	.cfi_endproc
.LFE93:
	.size	default_realloc, .-default_realloc
	.p2align 4
	.type	default_calloc, @function
default_calloc:
.LFB92:
	.cfi_startproc
	endbr64
	jmp	calloc@PLT
	.cfi_endproc
.LFE92:
	.size	default_calloc, .-default_calloc
	.p2align 4
	.type	default_free, @function
default_free:
.LFB91:
	.cfi_startproc
	endbr64
	jmp	free@PLT
	.cfi_endproc
.LFE91:
	.size	default_free, .-default_free
	.p2align 4
	.type	default_malloc, @function
default_malloc:
.LFB90:
	.cfi_startproc
	endbr64
	jmp	malloc@PLT
	.cfi_endproc
.LFE90:
	.size	default_malloc, .-default_malloc
	.p2align 4
	.globl	uvwasi__malloc
	.hidden	uvwasi__malloc
	.type	uvwasi__malloc, @function
uvwasi__malloc:
.LFB94:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	64(%r8), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	jmp	*%rdx
	.cfi_endproc
.LFE94:
	.size	uvwasi__malloc, .-uvwasi__malloc
	.p2align 4
	.globl	uvwasi__free
	.hidden	uvwasi__free
	.type	uvwasi__free, @function
uvwasi__free:
.LFB95:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	64(%r8), %rax
	movq	16(%rax), %rdx
	movq	(%rax), %rsi
	jmp	*%rdx
	.cfi_endproc
.LFE95:
	.size	uvwasi__free, .-uvwasi__free
	.p2align 4
	.globl	uvwasi__calloc
	.hidden	uvwasi__calloc
	.type	uvwasi__calloc, @function
uvwasi__calloc:
.LFB96:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	64(%r8), %rax
	movq	24(%rax), %rcx
	movq	(%rax), %rdx
	jmp	*%rcx
	.cfi_endproc
.LFE96:
	.size	uvwasi__calloc, .-uvwasi__calloc
	.p2align 4
	.globl	uvwasi__realloc
	.hidden	uvwasi__realloc
	.type	uvwasi__realloc, @function
uvwasi__realloc:
.LFB97:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	64(%r8), %rax
	movq	32(%rax), %rcx
	movq	(%rax), %rdx
	jmp	*%rcx
	.cfi_endproc
.LFE97:
	.size	uvwasi__realloc, .-uvwasi__realloc
	.p2align 4
	.globl	uvwasi_init
	.hidden	uvwasi_init
	.type	uvwasi_init, @function
uvwasi_init:
.LFB101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$28, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$936, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L10
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L10
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L10
	movq	56(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rbx
	leaq	default_allocator(%rip), %rax
	movups	%xmm0, 16(%rdi)
	testq	%rdx, %rdx
	movups	%xmm0, 40(%rdi)
	movl	16(%rsi), %ecx
	cmovne	%rdx, %rax
	movq	$0, (%rdi)
	movl	%ecx, -952(%rbp)
	movq	%rax, 64(%rdi)
	testl	%ecx, %ecx
	je	.L14
	movq	24(%rsi), %rax
	leal	-1(%rcx), %edx
	xorl	%r15d, %r15d
	leaq	8(%rax), %r13
	leaq	0(%r13,%rdx,8), %r14
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L78:
	addq	$8, %r13
.L15:
	movq	(%rax), %rdi
	call	strlen@PLT
	leal	1(%r15,%rax), %r15d
	movq	%r13, %rax
	cmpq	%r13, %r14
	jne	.L78
	movl	-952(%rbp), %eax
	movl	%r15d, 32(%rbx)
	movl	%eax, 8(%rbx)
	testl	%r15d, %r15d
	je	.L17
	movq	64(%rbx), %rax
	movl	%r15d, %edi
	movq	(%rax), %rsi
	call	*8(%rax)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L19
	movq	64(%rbx), %rax
	movl	16(%r12), %edi
	movl	$8, %esi
	movq	(%rax), %rdx
	call	*24(%rax)
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L19
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L17
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L20:
	movq	24(%r12), %rax
	movl	%ecx, %r8d
	movl	%ecx, -968(%rbp)
	movl	%r15d, %r14d
	movq	%r8, -960(%rbp)
	movq	(%rax,%r8,8), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -952(%rbp)
	call	strlen@PLT
	movq	24(%rbx), %rdi
	movq	-952(%rbp), %rsi
	leal	1(%rax), %edx
	addq	%r14, %rdi
	movq	%rdx, %r13
	call	memcpy@PLT
	movq	16(%rbx), %rax
	addq	24(%rbx), %r14
	addl	%r13d, %r15d
	movq	-960(%rbp), %r8
	movl	-968(%rbp), %ecx
	movq	%r14, (%rax,%r8,8)
	addl	$1, %ecx
	cmpl	%ecx, 16(%r12)
	ja	.L20
.L17:
	movq	32(%r12), %r14
	testq	%r14, %r14
	je	.L21
	movq	(%r14), %rdi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	testq	%rdi, %rdi
	je	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	call	strlen@PLT
	leal	1(%r13), %r8d
	movl	%r13d, %ecx
	movq	(%r14,%r8,8), %rdi
	leal	1(%r15,%rax), %r15d
	movq	%r8, %r13
	testq	%rdi, %rdi
	jne	.L22
	movl	%ecx, -960(%rbp)
	movq	%r8, -952(%rbp)
	movl	%r8d, 36(%rbx)
	movl	%r15d, 56(%rbx)
	testl	%r15d, %r15d
	je	.L23
	movq	64(%rbx), %rax
	movl	%r15d, %edi
	movq	(%rax), %rsi
	call	*8(%rax)
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	je	.L19
	movq	64(%rbx), %rax
	movq	-952(%rbp), %r8
	movl	$8, %esi
	movq	(%rax), %rdx
	movq	%r8, %rdi
	call	*24(%rax)
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L19
	testl	%r13d, %r13d
	je	.L23
	movl	-960(%rbp), %ecx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	leaq	8(,%rcx,8), %rax
	movq	%rax, -968(%rbp)
	.p2align 4,,10
	.p2align 3
.L25:
	movq	32(%r12), %rax
	movq	(%rax,%r13), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -960(%rbp)
	call	strlen@PLT
	movl	%r14d, %r8d
	movq	-960(%rbp), %rsi
	movq	%r8, %rdi
	leal	1(%rax), %edx
	addq	48(%rbx), %rdi
	movq	%r8, -952(%rbp)
	movq	%rdx, %r15
	call	memcpy@PLT
	movq	40(%rbx), %rax
	movq	-952(%rbp), %r8
	addl	%r15d, %r14d
	addq	48(%rbx), %r8
	movq	%r8, (%rax,%r13)
	addq	$8, %r13
	cmpq	%r13, -968(%rbp)
	jne	.L25
.L23:
	movl	4(%r12), %edx
	testl	%edx, %edx
	je	.L26
	movq	8(%r12), %rax
	subl	$1, %edx
	salq	$4, %rdx
	leaq	16(%rax,%rdx), %rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L79:
	cmpq	$0, (%rax)
	je	.L38
	addq	$16, %rax
	cmpq	%rdx, %rax
	je	.L26
.L28:
	cmpq	$0, 8(%rax)
	jne	.L79
.L38:
	movl	$28, %r13d
.L27:
	movq	(%rbx), %rsi
	movq	%rbx, %rdi
	call	uvwasi_fd_table_free@PLT
	movq	64(%rbx), %rax
	movq	24(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	48(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	40(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	$0, (%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 40(%rbx)
.L10:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$936, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	$0, 36(%rbx)
	movl	$0, 56(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$48, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	uvwasi_fd_table_init@PLT
	movl	%eax, %r13d
	testw	%ax, %ax
	jne	.L27
	movl	4(%r12), %eax
	leaq	-944(%rbp), %r14
	movl	$0, -952(%rbp)
	leaq	-496(%rbp), %r15
	testl	%eax, %eax
	jne	.L29
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-848(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movl	$438, %r8d
	movq	%r15, %rsi
	call	uv_fs_open@PLT
	testl	%eax, %eax
	js	.L81
	movq	-960(%rbp), %rdx
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	movq	-848(%rbp), %r8
	movq	(%rbx), %rsi
	movq	(%rax,%rdx), %rcx
	movl	-408(%rbp), %edx
	call	uvwasi_fd_table_insert_preopen@PLT
	movq	%r14, %rdi
	movl	%eax, -960(%rbp)
	call	uv_fs_req_cleanup@PLT
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	-960(%rbp), %eax
	testw	%ax, %ax
	jne	.L40
	addl	$1, -952(%rbp)
	movl	-952(%rbp), %eax
	cmpl	%eax, 4(%r12)
	jbe	.L10
.L29:
	movl	-952(%rbp), %eax
	xorl	%edi, %edi
	movq	%r14, %rsi
	salq	$4, %rax
	movq	%rax, %rcx
	movq	%rax, -960(%rbp)
	movq	8(%r12), %rax
	movq	8(%rax,%rcx), %rdx
	xorl	%ecx, %ecx
	call	uv_fs_realpath@PLT
	testl	%eax, %eax
	je	.L30
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$0, 8(%rdi)
	movl	$0, 32(%rdi)
	jmp	.L17
.L81:
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	jmp	.L27
.L40:
	movl	%eax, %r13d
	jmp	.L27
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE101:
	.size	uvwasi_init, .-uvwasi_init
	.p2align 4
	.globl	uvwasi_destroy
	.hidden	uvwasi_destroy
	.type	uvwasi_destroy, @function
uvwasi_destroy:
.LFB102:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L88
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rsi
	call	uvwasi_fd_table_free@PLT
	movq	64(%rbx), %rax
	movq	24(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	48(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	40(%rbx), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	pxor	%xmm0, %xmm0
	movq	$0, (%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE102:
	.size	uvwasi_destroy, .-uvwasi_destroy
	.p2align 4
	.globl	uvwasi_embedder_remap_fd
	.hidden	uvwasi_embedder_remap_fd
	.type	uvwasi_embedder_remap_fd, @function
uvwasi_embedder_remap_fd:
.LFB103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L94
	movq	(%rdi), %rdi
	movl	%edx, %ebx
	xorl	%r8d, %r8d
	leaq	-32(%rbp), %rdx
	xorl	%ecx, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L96
.L91:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	-32(%rbp), %rdi
	movl	%ebx, 4(%rdi)
	addq	$64, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$28, %r12d
	jmp	.L91
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE103:
	.size	uvwasi_embedder_remap_fd, .-uvwasi_embedder_remap_fd
	.p2align 4
	.globl	uvwasi_args_get
	.hidden	uvwasi_args_get
	.type	uvwasi_args_get, @function
uvwasi_args_get:
.LFB104:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	testq	%rsi, %rsi
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L106
	testq	%rdi, %rdi
	je	.L106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rdi), %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%r9d, %r9d
	je	.L100
	movl	%r9d, %ecx
	leaq	24(%rdi), %r10
	movq	16(%rdi), %rdx
	leal	-1(%r9), %eax
	leaq	(%rsi,%rcx,8), %rcx
	cmpq	%rcx, %r10
	leaq	32(%rdi), %rcx
	setnb	%r10b
	cmpq	%rcx, %rsi
	setnb	%cl
	orb	%cl, %r10b
	je	.L101
	leaq	15(%rdx), %rcx
	subq	%rsi, %rcx
	cmpq	$30, %rcx
	seta	%r10b
	cmpl	$2, %eax
	seta	%cl
	testb	%cl, %r10b
	je	.L101
	movl	%r9d, %eax
	movq	24(%rdi), %xmm2
	movq	%r8, %xmm1
	shrl	%eax
	punpcklqdq	%xmm1, %xmm1
	leal	-1(%rax), %ecx
	punpcklqdq	%xmm2, %xmm2
	xorl	%eax, %eax
	addq	$1, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L102:
	movdqu	(%rdx,%rax), %xmm0
	psubq	%xmm2, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L102
	movl	%r9d, %eax
	andl	$-2, %eax
	andl	$1, %r9d
	je	.L100
	movq	(%rdx,%rax,8), %rcx
	addq	%r8, %rcx
	movq	%rcx, %rdx
	subq	24(%rdi), %rdx
	movq	%rdx, (%rsi,%rax,8)
.L100:
	movl	32(%rdi), %edx
	movq	24(%rdi), %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore 6
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	%eax, %r9d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%rdx,%rax,8), %rcx
	addq	%r8, %rcx
	subq	24(%rdi), %rcx
	movq	%rcx, (%rsi,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %r9
	jne	.L104
	jmp	.L100
	.cfi_endproc
.LFE104:
	.size	uvwasi_args_get, .-uvwasi_args_get
	.p2align 4
	.globl	uvwasi_args_sizes_get
	.hidden	uvwasi_args_sizes_get
	.type	uvwasi_args_sizes_get, @function
uvwasi_args_sizes_get:
.LFB105:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	sete	%cl
	testq	%rdx, %rdx
	sete	%al
	orb	%al, %cl
	jne	.L124
	testq	%rdi, %rdi
	je	.L124
	movl	8(%rdi), %eax
	movl	%eax, (%rsi)
	movl	32(%rdi), %eax
	movl	%eax, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE105:
	.size	uvwasi_args_sizes_get, .-uvwasi_args_sizes_get
	.p2align 4
	.globl	uvwasi_clock_res_get
	.hidden	uvwasi_clock_res_get
	.type	uvwasi_clock_res_get, @function
uvwasi_clock_res_get:
.LFB106:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L130
	testq	%rdx, %rdx
	je	.L130
	cmpl	$2, %esi
	je	.L127
	jbe	.L128
	cmpl	$3, %esi
	jne	.L130
	movq	%rdx, %rdi
	jmp	uvwasi__clock_getres_thread_cputime@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movq	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%rdx, %rdi
	jmp	uvwasi__clock_getres_process_cputime@PLT
	.cfi_endproc
.LFE106:
	.size	uvwasi_clock_res_get, .-uvwasi_clock_res_get
	.p2align 4
	.globl	uvwasi_clock_time_get
	.hidden	uvwasi_clock_time_get
	.type	uvwasi_clock_time_get, @function
uvwasi_clock_time_get:
.LFB107:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L141
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	testq	%rcx, %rcx
	je	.L138
	cmpl	$2, %esi
	je	.L133
	ja	.L134
	testl	%esi, %esi
	je	.L145
	call	uv_hrtime@PLT
	movq	%rax, (%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	cmpl	$3, %esi
	jne	.L138
	addq	$8, %rsp
	movq	%rcx, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__clock_gettime_thread_cputime@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$28, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rcx, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__clock_gettime_realtime@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rcx, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__clock_gettime_process_cputime@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE107:
	.size	uvwasi_clock_time_get, .-uvwasi_clock_time_get
	.p2align 4
	.globl	uvwasi_environ_get
	.hidden	uvwasi_environ_get
	.type	uvwasi_environ_get, @function
uvwasi_environ_get:
.LFB108:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	testq	%rsi, %rsi
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L154
	testq	%rdi, %rdi
	je	.L154
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	36(%rdi), %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%r9d, %r9d
	je	.L148
	movl	%r9d, %ecx
	leaq	48(%rdi), %r10
	movq	40(%rdi), %rdx
	leal	-1(%r9), %eax
	leaq	(%rsi,%rcx,8), %rcx
	cmpq	%rcx, %r10
	leaq	56(%rdi), %rcx
	setnb	%r10b
	cmpq	%rcx, %rsi
	setnb	%cl
	orb	%cl, %r10b
	je	.L149
	leaq	15(%rdx), %rcx
	subq	%rsi, %rcx
	cmpq	$30, %rcx
	seta	%r10b
	cmpl	$2, %eax
	seta	%cl
	testb	%cl, %r10b
	je	.L149
	movl	%r9d, %eax
	movq	48(%rdi), %xmm2
	movq	%r8, %xmm1
	shrl	%eax
	punpcklqdq	%xmm1, %xmm1
	leal	-1(%rax), %ecx
	punpcklqdq	%xmm2, %xmm2
	xorl	%eax, %eax
	addq	$1, %rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L150:
	movdqu	(%rdx,%rax), %xmm0
	psubq	%xmm2, %xmm0
	paddq	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L150
	movl	%r9d, %eax
	andl	$-2, %eax
	andl	$1, %r9d
	je	.L148
	movq	(%rdx,%rax,8), %rcx
	addq	%r8, %rcx
	movq	%rcx, %rdx
	subq	48(%rdi), %rdx
	movq	%rdx, (%rsi,%rax,8)
.L148:
	movl	56(%rdi), %edx
	movq	48(%rdi), %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore 6
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	%eax, %r9d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L152:
	movq	(%rdx,%rax,8), %rcx
	addq	%r8, %rcx
	subq	48(%rdi), %rcx
	movq	%rcx, (%rsi,%rax,8)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %r9
	jne	.L152
	jmp	.L148
	.cfi_endproc
.LFE108:
	.size	uvwasi_environ_get, .-uvwasi_environ_get
	.p2align 4
	.globl	uvwasi_environ_sizes_get
	.hidden	uvwasi_environ_sizes_get
	.type	uvwasi_environ_sizes_get, @function
uvwasi_environ_sizes_get:
.LFB109:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	sete	%cl
	testq	%rdx, %rdx
	sete	%al
	orb	%al, %cl
	jne	.L172
	testq	%rdi, %rdi
	je	.L172
	movl	36(%rdi), %eax
	movl	%eax, (%rsi)
	movl	56(%rdi), %eax
	movl	%eax, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE109:
	.size	uvwasi_environ_sizes_get, .-uvwasi_environ_sizes_get
	.p2align 4
	.globl	uvwasi_fd_advise
	.hidden	uvwasi_fd_advise
	.type	uvwasi_fd_advise, @function
uvwasi_fd_advise:
.LFB110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	je	.L177
	cmpb	$5, %r8b
	ja	.L177
	movzbl	%r8b, %eax
	movq	(%rdi), %rdi
	movq	%rdx, %r13
	xorl	%r8d, %r8d
	leaq	CSWTCH.172(%rip), %rdx
	movl	$128, %ecx
	movl	(%rdx,%rax,4), %r14d
	leaq	-48(%rbp), %rdx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L173
	movq	-48(%rbp), %rax
	movl	%r14d, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	4(%rax), %edi
	call	posix_fadvise64@PLT
	testl	%eax, %eax
	jne	.L188
.L175:
	movq	-48(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$28, %r12d
.L173:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movl	%eax, %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L175
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE110:
	.size	uvwasi_fd_advise, .-uvwasi_fd_advise
	.p2align 4
	.globl	uvwasi_fd_allocate
	.hidden	uvwasi_fd_allocate
	.type	uvwasi_fd_allocate, @function
uvwasi_fd_allocate:
.LFB111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rdi
	movq	%rdx, %rbx
	movq	%rcx, %r13
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$256, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L199
.L190:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$488, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	-504(%rbp), %rax
	leaq	-496(%rbp), %r14
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movl	4(%rax), %edx
	call	uv_fs_fstat@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	movq	-328(%rbp), %rax
	movq	%rax, -520(%rbp)
	call	uv_fs_req_cleanup@PLT
	testl	%r15d, %r15d
	jne	.L201
	movq	-504(%rbp), %rdi
	leaq	(%rbx,%r13), %rcx
	cmpq	-520(%rbp), %rcx
	ja	.L202
.L193:
	addq	$64, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$28, %r12d
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L202:
	movl	4(%rdi), %edx
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	uv_fs_ftruncate@PLT
	testl	%eax, %eax
	jne	.L194
.L198:
	movq	-504(%rbp), %rdi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L201:
	movl	%r15d, %edi
	call	uvwasi__translate_uv_error@PLT
	movq	-504(%rbp), %rdi
	movl	%eax, %r12d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L198
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE111:
	.size	uvwasi_fd_allocate, .-uvwasi_fd_allocate
	.p2align 4
	.globl	uvwasi_fd_close
	.hidden	uvwasi_fd_close
	.type	uvwasi_fd_close, @function
uvwasi_fd_close:
.LFB112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$464, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L208
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	%esi, %r13d
	call	uvwasi_fd_table_lock@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	-488(%rbp), %rdx
	movl	%r13d, %esi
	call	uvwasi_fd_table_get_nolock@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L210
.L205:
	movq	(%rbx), %rdi
	call	uvwasi_fd_table_unlock@PLT
.L203:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	addq	$464, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	-488(%rbp), %rax
	leaq	-480(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	movl	4(%rax), %edx
	call	uv_fs_close@PLT
	movl	%eax, %r12d
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r12d, %r12d
	jne	.L212
	movq	(%rbx), %rsi
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	uvwasi_fd_table_remove_nolock@PLT
	movl	%eax, %r12d
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$28, %r12d
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L212:
	movl	%r12d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L205
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE112:
	.size	uvwasi_fd_close, .-uvwasi_fd_close
	.p2align 4
	.globl	uvwasi_fd_datasync
	.hidden	uvwasi_fd_datasync
	.type	uvwasi_fd_datasync, @function
uvwasi_fd_datasync:
.LFB113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$472, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L216
	movq	(%rdi), %rdi
	leaq	-488(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L221
.L213:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	-488(%rbp), %rax
	leaq	-480(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	movl	4(%rax), %edx
	call	uv_fs_fdatasync@PLT
	movl	%eax, %r13d
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L213
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$28, %r12d
	jmp	.L213
.L222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE113:
	.size	uvwasi_fd_datasync, .-uvwasi_fd_datasync
	.p2align 4
	.globl	uvwasi_fd_fdstat_get
	.hidden	uvwasi_fd_fdstat_get
	.type	uvwasi_fd_fdstat_get, @function
uvwasi_fd_fdstat_get:
.LFB114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L227
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L227
	movq	(%rdi), %rdi
	leaq	-32(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L229
.L223:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	-32(%rbp), %rax
	movl	$3, %esi
	movzbl	32(%rax), %edx
	movb	%dl, (%rbx)
	movdqu	40(%rax), %xmm0
	movl	4(%rax), %edi
	xorl	%eax, %eax
	movups	%xmm0, 8(%rbx)
	call	fcntl64@PLT
	testl	%eax, %eax
	js	.L231
	movw	%ax, 2(%rbx)
	movq	-32(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$28, %r12d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L231:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	movq	-32(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L223
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE114:
	.size	uvwasi_fd_fdstat_get, .-uvwasi_fd_fdstat_get
	.p2align 4
	.globl	uvwasi_fd_fdstat_set_flags
	.hidden	uvwasi_fd_fdstat_set_flags
	.type	uvwasi_fd_fdstat_set_flags, @function
uvwasi_fd_fdstat_set_flags:
.LFB115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rdi
	movl	%edx, %ebx
	xorl	%r8d, %r8d
	leaq	-32(%rbp), %rdx
	movl	$8, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L232
	movl	%ebx, %ecx
	movl	%ebx, %r9d
	andl	$1, %ecx
	cmpw	$1, %cx
	sbbl	%edx, %edx
	andl	$-1024, %edx
	addl	$7168, %edx
	cmpw	$1, %cx
	sbbl	%eax, %eax
	andl	$-1024, %eax
	addl	$1055744, %eax
	cmpw	$1, %cx
	sbbl	%edi, %edi
	andl	$-1024, %edi
	addl	$1053696, %edi
	cmpw	$1, %cx
	sbbl	%r8d, %r8d
	andl	$-1024, %r8d
	addl	$3072, %r8d
	cmpw	$1, %cx
	sbbl	%esi, %esi
	sall	$31, %ecx
	andl	$-1024, %esi
	sarl	$31, %ecx
	andl	$1024, %ecx
	addl	$5120, %esi
	andw	$2, %r9w
	cmove	%ecx, %esi
	cmove	%r8d, %edx
	movl	%ebx, %ecx
	andw	$4, %cx
	cmove	%edi, %eax
	cmove	%esi, %edx
	testb	$8, %bl
	movl	$4, %esi
	cmovne	%eax, %edx
	andl	$16, %ebx
	cmovne	%eax, %edx
	movq	-32(%rbp), %rax
	movl	4(%rax), %edi
	xorl	%eax, %eax
	call	fcntl64@PLT
	testl	%eax, %eax
	js	.L247
.L239:
	movq	-32(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
.L232:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$28, %r12d
	jmp	.L232
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE115:
	.size	uvwasi_fd_fdstat_set_flags, .-uvwasi_fd_fdstat_set_flags
	.p2align 4
	.globl	uvwasi_fd_fdstat_set_rights
	.hidden	uvwasi_fd_fdstat_set_rights
	.type	uvwasi_fd_fdstat_set_rights, @function
uvwasi_fd_fdstat_set_rights:
.LFB116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L253
	movq	(%rdi), %rdi
	movq	%rdx, %rbx
	movq	%rcx, %r13
	leaq	-48(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L249
	movq	-48(%rbp), %rdi
	movq	40(%rdi), %rax
	orq	%rbx, %rax
	cmpq	40(%rdi), %rax
	ja	.L255
	movq	48(%rdi), %rax
	orq	%r13, %rax
	cmpq	48(%rdi), %rax
	ja	.L255
	movq	%rbx, %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rdi)
.L251:
	addq	$64, %rdi
	call	uv_mutex_unlock@PLT
.L249:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	$76, %r12d
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$28, %r12d
	jmp	.L249
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE116:
	.size	uvwasi_fd_fdstat_set_rights, .-uvwasi_fd_fdstat_set_rights
	.p2align 4
	.globl	uvwasi_fd_filestat_get
	.hidden	uvwasi_fd_filestat_get
	.type	uvwasi_fd_filestat_get, @function
uvwasi_fd_filestat_get:
.LFB117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$472, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L263
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L263
	movq	(%rdi), %rdi
	leaq	-488(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$2097152, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L265
.L258:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	-488(%rbp), %rax
	leaq	-480(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	movl	4(%rax), %edx
	call	uv_fs_fstat@PLT
	testl	%eax, %eax
	jne	.L267
	leaq	-368(%rbp), %rdi
	movq	%r13, %rsi
	call	uvwasi__stat_to_filestat@PLT
.L261:
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$28, %r12d
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L267:
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L261
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE117:
	.size	uvwasi_fd_filestat_get, .-uvwasi_fd_filestat_get
	.p2align 4
	.globl	uvwasi_fd_filestat_set_size
	.hidden	uvwasi_fd_filestat_set_size
	.type	uvwasi_fd_filestat_set_size, @function
uvwasi_fd_filestat_set_size:
.LFB118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$472, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L271
	movq	(%rdi), %rdi
	movq	%rdx, %r13
	xorl	%r8d, %r8d
	leaq	-488(%rbp), %rdx
	movl	$4194304, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L276
.L268:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	-488(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%edi, %edi
	leaq	-480(%rbp), %r14
	movl	4(%rax), %edx
	movq	%r14, %rsi
	call	uv_fs_ftruncate@PLT
	movl	%eax, %r13d
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L268
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$28, %r12d
	jmp	.L268
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE118:
	.size	uvwasi_fd_filestat_set_size, .-uvwasi_fd_filestat_set_size
	.p2align 4
	.globl	uvwasi_fd_filestat_set_times
	.hidden	uvwasi_fd_filestat_set_times
	.type	uvwasi_fd_filestat_set_times, @function
uvwasi_fd_filestat_set_times:
.LFB119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	$28, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$464, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L278
	andl	$65520, %r8d
	jne	.L278
	movq	(%rdi), %rdi
	movq	%rdx, %r12
	movq	%rcx, %rbx
	leaq	-488(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$8388608, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r13d
	testw	%ax, %ax
	je	.L291
.L278:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$464, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	testq	%rbx, %rbx
	js	.L280
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rbx, %xmm1
	testq	%r12, %r12
	js	.L282
.L293:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
.L283:
	movq	-488(%rbp), %rax
	leaq	-480(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	movl	4(%rax), %edx
	call	uv_fs_futime@PLT
	movl	%eax, %r12d
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r12d, %r12d
	je	.L278
	movl	%r12d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r13d
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%rbx, %rax
	andl	$1, %ebx
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	testq	%r12, %r12
	jns	.L293
.L282:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L283
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE119:
	.size	uvwasi_fd_filestat_set_times, .-uvwasi_fd_filestat_set_times
	.p2align 4
	.globl	uvwasi_fd_pread
	.hidden	uvwasi_fd_pread
	.type	uvwasi_fd_pread, @function
uvwasi_fd_pread:
.LFB120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%dl
	testq	%r9, %r9
	sete	%al
	orb	%al, %dl
	jne	.L302
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L302
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movq	%r9, %r12
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	je	.L305
.L294:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movq	64(%rbx), %rax
	movl	%r13d, %edi
	salq	$4, %rdi
	movq	(%rax), %rsi
	call	*8(%rax)
	movq	%rax, -544(%rbp)
	testq	%rax, %rax
	je	.L296
	testl	%r13d, %r13d
	je	.L298
	movq	%rax, %rcx
	movq	%rax, %r8
	leal	-1(%r13), %eax
	addq	$8, %r14
	addq	$1, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	%rax, -536(%rbp)
	.p2align 4,,10
	.p2align 3
.L299:
	movq	-8(%r14), %rdi
	movl	(%r14), %esi
	movq	%r8, -520(%rbp)
	addq	$16, %r14
	call	uv_buf_init@PLT
	movq	-520(%rbp), %r8
	movq	%rax, (%r8)
	addq	$16, %r8
	movq	%rdx, -8(%r8)
	cmpq	%r8, -536(%rbp)
	jne	.L299
.L298:
	movq	-504(%rbp), %rax
	subq	$8, %rsp
	movl	%r13d, %r8d
	xorl	%edi, %edi
	leaq	-496(%rbp), %r11
	movq	-528(%rbp), %r9
	movq	-544(%rbp), %rcx
	movl	4(%rax), %edx
	pushq	$0
	movq	%r11, %rsi
	movq	%r11, -520(%rbp)
	call	uv_fs_read@PLT
	movl	%eax, %r14d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	-520(%rbp), %r11
	movq	-408(%rbp), %r13
	movq	%r11, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	64(%rbx), %rax
	movq	-544(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	popq	%rax
	popq	%rdx
	testl	%r14d, %r14d
	js	.L307
	movl	%r13d, (%r12)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$28, %r15d
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L296:
	movq	-504(%rbp), %rax
	movl	$48, %r15d
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L307:
	movl	%r14d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r15d
	jmp	.L294
.L306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE120:
	.size	uvwasi_fd_pread, .-uvwasi_fd_pread
	.p2align 4
	.globl	uvwasi_fd_prestat_get
	.hidden	uvwasi_fd_prestat_get
	.type	uvwasi_fd_prestat_get, @function
uvwasi_fd_prestat_get:
.LFB121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L312
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L312
	movq	(%rdi), %rdi
	leaq	-48(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L308
	movq	-48(%rbp), %r13
	cmpl	$1, 56(%r13)
	jne	.L313
	movb	$0, (%rbx)
	movq	8(%r13), %rdi
	call	strlen@PLT
	addl	$1, %eax
	movl	%eax, 4(%rbx)
.L310:
	leaq	64(%r13), %rdi
	call	uv_mutex_unlock@PLT
.L308:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	movl	$28, %r12d
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$28, %r12d
	jmp	.L310
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE121:
	.size	uvwasi_fd_prestat_get, .-uvwasi_fd_prestat_get
	.p2align 4
	.globl	uvwasi_fd_prestat_dir_name
	.hidden	uvwasi_fd_prestat_dir_name
	.type	uvwasi_fd_prestat_dir_name, @function
uvwasi_fd_prestat_dir_name:
.LFB122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L320
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L320
	movq	(%rdi), %rdi
	movl	%ecx, %ebx
	leaq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L316
	movq	-64(%rbp), %r13
	cmpl	$1, 56(%r13)
	jne	.L321
	movq	8(%r13), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbx
	jnb	.L324
	leaq	64(%r13), %rdi
	movl	$42, %r12d
	call	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movl	$28, %r12d
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L321:
	movl	$8, %r12d
.L318:
	leaq	64(%r13), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L318
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE122:
	.size	uvwasi_fd_prestat_dir_name, .-uvwasi_fd_prestat_dir_name
	.p2align 4
	.globl	uvwasi_fd_pwrite
	.hidden	uvwasi_fd_pwrite
	.type	uvwasi_fd_pwrite, @function
uvwasi_fd_pwrite:
.LFB123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%dl
	testq	%r9, %r9
	sete	%al
	orb	%al, %dl
	jne	.L334
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L334
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$68, %ecx
	movq	%r9, %r12
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	je	.L337
.L326:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	64(%rbx), %rax
	movl	%r13d, %edi
	salq	$4, %rdi
	movq	(%rax), %rsi
	call	*8(%rax)
	movq	%rax, -544(%rbp)
	testq	%rax, %rax
	je	.L328
	testl	%r13d, %r13d
	je	.L330
	movq	%rax, %rcx
	movq	%rax, %r8
	leal	-1(%r13), %eax
	addq	$8, %r14
	addq	$1, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	%rax, -536(%rbp)
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-8(%r14), %rdi
	movl	(%r14), %esi
	movq	%r8, -520(%rbp)
	addq	$16, %r14
	call	uv_buf_init@PLT
	movq	-520(%rbp), %r8
	movq	%rax, (%r8)
	addq	$16, %r8
	movq	%rdx, -8(%r8)
	cmpq	%r8, -536(%rbp)
	jne	.L331
.L330:
	movq	-504(%rbp), %rax
	subq	$8, %rsp
	movl	%r13d, %r8d
	xorl	%edi, %edi
	leaq	-496(%rbp), %r11
	movq	-528(%rbp), %r9
	movq	-544(%rbp), %rcx
	movl	4(%rax), %edx
	pushq	$0
	movq	%r11, %rsi
	movq	%r11, -520(%rbp)
	call	uv_fs_write@PLT
	movl	%eax, %r14d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	-520(%rbp), %r11
	movq	-408(%rbp), %r13
	movq	%r11, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	64(%rbx), %rax
	movq	-544(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	popq	%rax
	popq	%rdx
	testl	%r14d, %r14d
	js	.L339
	movl	%r13d, (%r12)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$28, %r15d
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L328:
	movq	-504(%rbp), %rax
	movl	$48, %r15d
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L339:
	movl	%r14d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r15d
	jmp	.L326
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE123:
	.size	uvwasi_fd_pwrite, .-uvwasi_fd_pwrite
	.p2align 4
	.globl	uvwasi_fd_read
	.hidden	uvwasi_fd_read
	.type	uvwasi_fd_read, @function
uvwasi_fd_read:
.LFB124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L348
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L348
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	movq	%r8, %r12
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$2, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	je	.L351
.L340:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L352
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movq	64(%rbx), %rax
	movl	%r13d, %edi
	salq	$4, %rdi
	movq	(%rax), %rsi
	call	*8(%rax)
	movq	%rax, -536(%rbp)
	testq	%rax, %rax
	je	.L342
	testl	%r13d, %r13d
	je	.L344
	leaq	8(%r14), %rcx
	leal	-1(%r13), %r14d
	movq	%rax, %r8
	addq	$1, %r14
	salq	$4, %r14
	addq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L345:
	movq	-8(%rcx), %rdi
	movl	(%rcx), %esi
	movq	%r8, -528(%rbp)
	movq	%rcx, -520(%rbp)
	call	uv_buf_init@PLT
	movq	-528(%rbp), %r8
	movq	-520(%rbp), %rcx
	movq	%rax, (%r8)
	addq	$16, %r8
	addq	$16, %rcx
	movq	%rdx, -8(%r8)
	cmpq	%r8, %r14
	jne	.L345
.L344:
	movq	-504(%rbp), %rax
	subq	$8, %rsp
	movl	%r13d, %r8d
	xorl	%edi, %edi
	leaq	-496(%rbp), %r11
	movq	-536(%rbp), %rcx
	movq	$-1, %r9
	movl	4(%rax), %edx
	pushq	$0
	movq	%r11, %rsi
	movq	%r11, -520(%rbp)
	call	uv_fs_read@PLT
	movl	%eax, %r14d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	-520(%rbp), %r11
	movq	-408(%rbp), %r13
	movq	%r11, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	64(%rbx), %rax
	movq	-536(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	popq	%rax
	popq	%rdx
	testl	%r14d, %r14d
	js	.L353
	movl	%r13d, (%r12)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L348:
	movl	$28, %r15d
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-504(%rbp), %rax
	movl	$48, %r15d
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L353:
	movl	%r14d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r15d
	jmp	.L340
.L352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE124:
	.size	uvwasi_fd_read, .-uvwasi_fd_read
	.p2align 4
	.globl	uvwasi_fd_readdir
	.hidden	uvwasi_fd_readdir
	.type	uvwasi_fd_readdir, @function
uvwasi_fd_readdir:
.LFB125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%dl
	testq	%r9, %r9
	sete	%al
	orb	%al, %dl
	jne	.L381
	testq	%rdi, %rdi
	je	.L381
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	movq	%r8, %r15
	leaq	-552(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$16384, %ecx
	movq	%r9, %rbx
	call	uvwasi_fd_table_get@PLT
	movw	%ax, -578(%rbp)
	testw	%ax, %ax
	je	.L403
.L354:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	movzwl	-578(%rbp), %eax
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movq	-552(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	16(%rax), %rdx
	leaq	-496(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -592(%rbp)
	call	uv_fs_opendir@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L405
	movq	-400(%rbp), %r14
	leaq	-512(%rbp), %rax
	movq	-592(%rbp), %rdi
	movq	%rax, -600(%rbp)
	movq	%rax, (%r14)
	movq	$1, 8(%r14)
	call	uv_fs_req_cleanup@PLT
	testq	%r15, %r15
	jne	.L406
.L357:
	movl	$0, (%rbx)
	movq	%r12, -568(%rbp)
.L358:
	movq	-592(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rdx
	call	uv_fs_readdir@PLT
	testl	%eax, %eax
	je	.L360
	js	.L402
	subl	$1, %eax
	movq	-600(%rbp), %r15
	salq	$4, %rax
	addq	-592(%rbp), %rax
	movq	%rax, -576(%rbp)
	.p2align 4,,10
	.p2align 3
.L377:
	movq	48(%r14), %rdi
	call	telldir@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	js	.L407
	movq	(%r15), %rdi
	call	strlen@PLT
	cmpl	$7, 8(%r15)
	movq	%r12, -544(%rbp)
	movq	$0, -536(%rbp)
	movl	%eax, -528(%rbp)
	ja	.L362
	movl	8(%r15), %edx
	leaq	.L364(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L364:
	.long	.L362-.L364
	.long	.L369-.L364
	.long	.L368-.L364
	.long	.L367-.L364
	.long	.L362-.L364
	.long	.L366-.L364
	.long	.L365-.L364
	.long	.L363-.L364
	.text
	.p2align 4,,10
	.p2align 3
.L363:
	movb	$1, -524(%rbp)
	.p2align 4,,10
	.p2align 3
.L370:
	movl	(%rbx), %esi
	movl	%r13d, %edx
	movl	$24, %r9d
	leaq	-544(%rbp), %r10
	subl	%esi, %edx
	movq	%rsi, %rdi
	addq	-568(%rbp), %rsi
	movl	%edx, %r8d
	cmpl	$24, %edx
	cmova	%r9, %r8
	movl	%r8d, %r11d
	cmpl	$8, %r8d
	jnb	.L371
	andl	$4, %r8d
	jne	.L408
	testl	%r11d, %r11d
	je	.L372
	movzbl	(%r10), %edi
	movb	%dil, (%rsi)
	testb	$2, %r11b
	jne	.L397
.L401:
	movl	(%rbx), %edi
.L372:
	cmpl	$24, %edx
	movl	$24, %esi
	movl	%r13d, %r8d
	cmova	%esi, %edx
	movq	%r15, %rsi
	addl	%edi, %edx
	subl	%edx, %r8d
	movl	%edx, (%rbx)
	cmpq	%rax, %r8
	cmovbe	%r8, %rax
	addq	$16, %r15
	movq	%rax, %r12
	movq	-568(%rbp), %rax
	leaq	(%rax,%rdx), %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
	addl	%r12d, (%rbx)
	cmpq	%r15, -576(%rbp)
	jne	.L377
	movq	-592(%rbp), %rdi
	call	uv_fs_req_cleanup@PLT
	cmpl	%r13d, (%rbx)
	jb	.L358
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-592(%rbp), %rbx
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	uv_fs_closedir@PLT
	movl	%eax, %r12d
	movq	-552(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%rbx, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r12d, %r12d
	je	.L354
	movl	%r12d, %edi
	call	uvwasi__translate_uv_error@PLT
	movw	%ax, -578(%rbp)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L365:
	movb	$2, -524(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L366:
	movb	$6, -524(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L367:
	movb	$7, -524(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L368:
	movb	$3, -524(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L369:
	movb	$4, -524(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L362:
	movb	$0, -524(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L371:
	movq	-544(%rbp), %rdi
	movq	%rdi, (%rsi)
	movl	%r8d, %edi
	movq	-8(%r10,%rdi), %r9
	movq	%r9, -8(%rsi,%rdi)
	leaq	8(%rsi), %r9
	andq	$-8, %r9
	subq	%r9, %rsi
	leal	(%r8,%rsi), %r11d
	subq	%rsi, %r10
	andl	$-8, %r11d
	cmpl	$8, %r11d
	jb	.L401
	movl	%r11d, %esi
	xorl	%edi, %edi
	andl	$-8, %esi
.L375:
	movl	%edi, %ecx
	addl	$8, %edi
	movq	(%r10,%rcx), %r8
	movq	%r8, (%r9,%rcx)
	cmpl	%esi, %edi
	jb	.L375
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$28, %eax
	movw	%ax, -578(%rbp)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L397:
	movzwl	-2(%r10,%r11), %edi
	movw	%di, -2(%rsi,%r11)
	movl	(%rbx), %edi
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L406:
	movq	48(%r14), %rdi
	movq	%r15, %rsi
	call	seekdir@PLT
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L407:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
.L402:
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movq	-592(%rbp), %rdi
	movw	%ax, -578(%rbp)
	call	uv_fs_req_cleanup@PLT
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L408:
	movl	(%r10), %edi
	movl	%edi, (%rsi)
	movl	-4(%r10,%r11), %edi
	movl	%edi, -4(%rsi,%r11)
	movl	(%rbx), %edi
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L405:
	movq	-552(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movl	%r14d, %edi
	call	uvwasi__translate_uv_error@PLT
	movw	%ax, -578(%rbp)
	jmp	.L354
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE125:
	.size	uvwasi_fd_readdir, .-uvwasi_fd_readdir
	.p2align 4
	.globl	uvwasi_fd_renumber
	.hidden	uvwasi_fd_renumber
	.type	uvwasi_fd_renumber, @function
uvwasi_fd_renumber:
.LFB126:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	testq	%rdi, %rdi
	je	.L410
	movq	(%rdi), %rsi
	jmp	uvwasi_fd_table_renumber@PLT
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE126:
	.size	uvwasi_fd_renumber, .-uvwasi_fd_renumber
	.p2align 4
	.globl	uvwasi_fd_seek
	.hidden	uvwasi_fd_seek
	.type	uvwasi_fd_seek, @function
uvwasi_fd_seek:
.LFB127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L417
	movq	%r8, %rbx
	testq	%r8, %r8
	je	.L417
	movq	(%rdi), %rdi
	movq	%rdx, %r13
	movl	%ecx, %r14d
	leaq	-48(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$4, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L422
.L411:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L423
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	-48(%rbp), %rdi
	movl	4(%rdi), %r8d
	cmpb	$1, %r14b
	je	.L418
	cmpb	$2, %r14b
	je	.L419
	xorl	%edx, %edx
	testb	%r14b, %r14b
	jne	.L424
.L413:
	movq	%r13, %rsi
	movl	%r8d, %edi
	call	lseek64@PLT
	cmpq	$-1, %rax
	je	.L425
	movq	%rax, (%rbx)
	movq	-48(%rbp), %rdi
.L414:
	addq	$64, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$28, %r12d
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$2, %edx
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$1, %edx
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L424:
	movl	$28, %r12d
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L425:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %r12d
	jmp	.L414
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE127:
	.size	uvwasi_fd_seek, .-uvwasi_fd_seek
	.p2align 4
	.globl	uvwasi_fd_sync
	.hidden	uvwasi_fd_sync
	.type	uvwasi_fd_sync, @function
uvwasi_fd_sync:
.LFB128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$472, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L429
	movq	(%rdi), %rdi
	leaq	-488(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$16, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L434
.L426:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	-488(%rbp), %rax
	leaq	-480(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	movl	4(%rax), %edx
	call	uv_fs_fsync@PLT
	movl	%eax, %r13d
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L426
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L429:
	movl	$28, %r12d
	jmp	.L426
.L435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE128:
	.size	uvwasi_fd_sync, .-uvwasi_fd_sync
	.p2align 4
	.globl	uvwasi_fd_tell
	.hidden	uvwasi_fd_tell
	.type	uvwasi_fd_tell, @function
uvwasi_fd_tell:
.LFB129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L441
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L441
	movq	(%rdi), %rdi
	leaq	-32(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$32, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L443
.L436:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L444
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	-32(%rbp), %rax
	xorl	%esi, %esi
	movl	$1, %edx
	movl	4(%rax), %edi
	call	lseek64@PLT
	cmpq	$-1, %rax
	je	.L445
	movq	%rax, (%rbx)
.L439:
	movq	-32(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$28, %r12d
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L445:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L439
.L444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE129:
	.size	uvwasi_fd_tell, .-uvwasi_fd_tell
	.p2align 4
	.globl	uvwasi_fd_write
	.hidden	uvwasi_fd_write
	.type	uvwasi_fd_write, @function
uvwasi_fd_write:
.LFB130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L454
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L454
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	movq	%r8, %r12
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$64, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	je	.L457
.L446:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L458
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movq	64(%rbx), %rax
	movl	%r13d, %edi
	salq	$4, %rdi
	movq	(%rax), %rsi
	call	*8(%rax)
	movq	%rax, -536(%rbp)
	testq	%rax, %rax
	je	.L448
	testl	%r13d, %r13d
	je	.L450
	leaq	8(%r14), %rcx
	leal	-1(%r13), %r14d
	movq	%rax, %r8
	addq	$1, %r14
	salq	$4, %r14
	addq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-8(%rcx), %rdi
	movl	(%rcx), %esi
	movq	%r8, -528(%rbp)
	movq	%rcx, -520(%rbp)
	call	uv_buf_init@PLT
	movq	-528(%rbp), %r8
	movq	-520(%rbp), %rcx
	movq	%rax, (%r8)
	addq	$16, %r8
	addq	$16, %rcx
	movq	%rdx, -8(%r8)
	cmpq	%r8, %r14
	jne	.L451
.L450:
	movq	-504(%rbp), %rax
	subq	$8, %rsp
	movl	%r13d, %r8d
	xorl	%edi, %edi
	leaq	-496(%rbp), %r11
	movq	-536(%rbp), %rcx
	movq	$-1, %r9
	movl	4(%rax), %edx
	pushq	$0
	movq	%r11, %rsi
	movq	%r11, -520(%rbp)
	call	uv_fs_write@PLT
	movl	%eax, %r14d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	-520(%rbp), %r11
	movq	-408(%rbp), %r13
	movq	%r11, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	64(%rbx), %rax
	movq	-536(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	popq	%rax
	popq	%rdx
	testl	%r14d, %r14d
	js	.L459
	movl	%r13d, (%r12)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$28, %r15d
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L448:
	movq	-504(%rbp), %rax
	movl	$48, %r15d
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L459:
	movl	%r14d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r15d
	jmp	.L446
.L458:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE130:
	.size	uvwasi_fd_write, .-uvwasi_fd_write
	.p2align 4
	.globl	uvwasi_path_create_directory
	.hidden	uvwasi_path_create_directory
	.type	uvwasi_path_create_directory, @function
uvwasi_path_create_directory:
.LFB131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$464, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L464
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L464
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	leaq	-488(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$512, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L469
.L460:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L470
	addq	$464, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	-488(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	leaq	-496(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L471
.L462:
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L464:
	movl	$28, %r12d
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L471:
	movq	-496(%rbp), %rdx
	leaq	-480(%rbp), %r14
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movq	%r14, %rsi
	movl	$511, %ecx
	call	uv_fs_mkdir@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	movq	64(%rbx), %rax
	movq	-496(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	testl	%r13d, %r13d
	je	.L462
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L462
.L470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE131:
	.size	uvwasi_path_create_directory, .-uvwasi_path_create_directory
	.p2align 4
	.globl	uvwasi_path_filestat_get
	.hidden	uvwasi_path_filestat_get
	.type	uvwasi_path_filestat_get, @function
uvwasi_path_filestat_get:
.LFB132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -516(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	sete	%dl
	testq	%r9, %r9
	sete	%al
	orb	%al, %dl
	jne	.L477
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L477
	movq	(%rdi), %rdi
	movq	%rcx, %r13
	movl	%r8d, %r15d
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$262144, %ecx
	movq	%r9, %r14
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L479
.L472:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L480
	addq	$488, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movl	-516(%rbp), %r10d
	movl	%r15d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-504(%rbp), %rsi
	leaq	-512(%rbp), %r8
	movl	%r10d, %r9d
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L481
.L474:
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$28, %r12d
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L481:
	movq	-512(%rbp), %rdx
	leaq	-496(%rbp), %r15
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	call	uv_fs_stat@PLT
	movq	-512(%rbp), %rdi
	movl	%eax, %r13d
	movq	64(%rbx), %rax
	movq	(%rax), %rsi
	call	*16(%rax)
	testl	%r13d, %r13d
	jne	.L482
	leaq	-384(%rbp), %rdi
	movq	%r14, %rsi
	call	uvwasi__stat_to_filestat@PLT
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L474
.L480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE132:
	.size	uvwasi_path_filestat_get, .-uvwasi_path_filestat_get
	.p2align 4
	.globl	uvwasi_path_filestat_set_times
	.hidden	uvwasi_path_filestat_set_times
	.type	uvwasi_path_filestat_set_times, @function
uvwasi_path_filestat_set_times:
.LFB133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$28, %r12d
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movl	%edx, -516(%rbp)
	movl	24(%rbp), %eax
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	je	.L483
	testq	%r13, %r13
	je	.L483
	testl	$65520, %eax
	jne	.L483
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	%r8d, %r15d
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$1048576, %ecx
	movq	%r9, %r14
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L497
.L483:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L498
	addq	$488, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	movl	-516(%rbp), %r10d
	movl	%r15d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-504(%rbp), %rsi
	leaq	-512(%rbp), %r8
	movl	%r10d, %r9d
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L499
.L485:
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L499:
	cmpq	$0, 16(%rbp)
	js	.L486
	pxor	%xmm1, %xmm1
	cvtsi2sdq	16(%rbp), %xmm1
.L487:
	testq	%r14, %r14
	js	.L488
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r14, %xmm0
.L489:
	movq	-512(%rbp), %rdx
	leaq	-496(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	uv_fs_utime@PLT
	movq	-512(%rbp), %rdi
	movl	%eax, %r13d
	movq	64(%rbx), %rax
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L485
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r14, %rax
	andl	$1, %r14d
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%r14, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L486:
	movq	16(%rbp), %rdx
	movq	16(%rbp), %rax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	andl	$1, %eax
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L487
.L498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE133:
	.size	uvwasi_path_filestat_set_times, .-uvwasi_path_filestat_set_times
	.p2align 4
	.globl	uvwasi_path_link
	.hidden	uvwasi_path_link
	.type	uvwasi_path_link, @function
uvwasi_path_link:
.LFB134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -536(%rbp)
	movq	16(%rbp), %r15
	movl	%r8d, -540(%rbp)
	movl	%r9d, -532(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	sete	%dl
	testq	%r15, %r15
	sete	%al
	orb	%al, %dl
	jne	.L519
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L519
	movq	(%rdi), %rdi
	movl	%esi, %r12d
	movl	%r9d, %r14d
	movq	%rcx, %r13
	call	uvwasi_fd_table_lock@PLT
	leaq	-512(%rbp), %rdx
	xorl	%r8d, %r8d
	cmpl	%r14d, %r12d
	je	.L528
	movq	(%rbx), %rdi
	movl	$2048, %ecx
	movl	%r12d, %esi
	call	uvwasi_fd_table_get_nolock@PLT
	movl	%eax, %r14d
	testw	%ax, %ax
	jne	.L527
	movl	-532(%rbp), %esi
	movq	(%rbx), %rdi
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$4096, %ecx
	call	uvwasi_fd_table_get_nolock@PLT
	movl	%eax, %r14d
	testw	%ax, %ax
	jne	.L529
	movq	(%rbx), %rdi
	call	uvwasi_fd_table_unlock@PLT
	movl	-536(%rbp), %r9d
	movq	%r13, %rdx
	movl	-540(%rbp), %ecx
	movq	-512(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-528(%rbp), %r8
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r14d
	testw	%ax, %ax
	jne	.L524
	movl	24(%rbp), %ecx
	movq	-504(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r15, %rdx
	leaq	-520(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r14d
	testw	%ax, %ax
	jne	.L524
	leaq	-496(%rbp), %r15
	movq	-520(%rbp), %rcx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movq	-528(%rbp), %rdx
	movq	%r15, %rsi
	call	uv_fs_link@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L524
.L515:
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r14d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	cmpl	-532(%rbp), %r12d
	jne	.L508
.L506:
	movq	64(%rbx), %rax
	movq	-528(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	-520(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
.L500:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L530
	addq	$504, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movl	$6144, %ecx
	movl	%r12d, %esi
	call	uvwasi_fd_table_get_nolock@PLT
	movq	(%rbx), %rdi
	movl	%eax, %r14d
	movq	-512(%rbp), %rax
	movq	%rax, -504(%rbp)
	call	uvwasi_fd_table_unlock@PLT
	testw	%r14w, %r14w
	jne	.L500
	movl	-536(%rbp), %r9d
	movl	-540(%rbp), %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-512(%rbp), %rsi
	leaq	-528(%rbp), %r8
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r14d
	testw	%ax, %ax
	je	.L531
.L525:
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L519:
	movl	$28, %r14d
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L529:
	movq	-512(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
.L527:
	movq	(%rbx), %rdi
	call	uvwasi_fd_table_unlock@PLT
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
.L508:
	movq	-512(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L531:
	movl	24(%rbp), %ecx
	movq	-504(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r15, %rdx
	leaq	-520(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r14d
	testw	%ax, %ax
	jne	.L525
	leaq	-496(%rbp), %r15
	movq	-520(%rbp), %rcx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movq	-528(%rbp), %rdx
	movq	%r15, %rsi
	call	uv_fs_link@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L525
	jmp	.L515
.L530:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE134:
	.size	uvwasi_path_link, .-uvwasi_path_link
	.p2align 4
	.globl	uvwasi_path_open
	.hidden	uvwasi_path_open
	.type	uvwasi_path_open, @function
uvwasi_path_open:
.LFB135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$520, %rsp
	movq	40(%rbp), %r14
	movl	32(%rbp), %eax
	movl	%r8d, -548(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testq	%rcx, %rcx
	sete	%cl
	testq	%r14, %r14
	sete	%dl
	orb	%dl, %cl
	jne	.L550
	testq	%rbx, %rbx
	je	.L550
	movq	16(%rbp), %r10
	andl	$4194625, %r10d
	jne	.L581
	movl	$65600, %r12d
	movl	$65536, %r15d
	movl	$64, %edx
	xorl	%edi, %edi
.L534:
	movl	%r9d, %ecx
	movq	16(%rbp), %r8
	orq	24(%rbp), %r8
	andw	$1, %cx
	cmove	%edi, %edx
	cmove	%r15d, %r12d
	cmpw	$1, %cx
	movl	%r9d, %edi
	sbbq	%rcx, %rcx
	andq	$-1024, %rcx
	addq	$9216, %rcx
	andw	$2, %di
	cmove	%edx, %r12d
	movw	%di, -554(%rbp)
	movl	%r12d, %edx
	orb	$-128, %dl
	testb	$4, %r9b
	cmovne	%edx, %r12d
	andl	$8, %r9d
	je	.L538
	orl	$512, %r12d
	orq	$524288, %rcx
.L538:
	movl	%r12d, %edx
	orb	$4, %dh
	testb	$1, %al
	cmovne	%edx, %r12d
	testb	$2, %al
	je	.L540
	orl	$4096, %r12d
	orq	$1, %r8
.L540:
	movl	%r12d, %edx
	orb	$8, %dh
	testb	$4, %al
	cmovne	%edx, %r12d
	testb	$8, %al
	je	.L542
	orl	$1052672, %r12d
	orq	$16, %r8
.L542:
	testb	$16, %al
	je	.L543
	orl	$1052672, %r12d
	orq	$16, %r8
.L543:
	testq	%r10, %r10
	je	.L544
	movq	%r8, %rax
	orq	$4, %rax
	testl	$1536, %r12d
	cmove	%rax, %r8
.L544:
	movq	(%rbx), %rdi
	leaq	-512(%rbp), %rdx
	movl	%r11d, -552(%rbp)
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r8d
	testw	%ax, %ax
	jne	.L532
	movl	-552(%rbp), %r11d
	movl	-548(%rbp), %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-512(%rbp), %rsi
	leaq	-536(%rbp), %r8
	movl	%r11d, %r9d
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r8d
	testw	%ax, %ax
	jne	.L582
	movq	-536(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%r12d, %ecx
	leaq	-496(%rbp), %r13
	movl	$438, %r8d
	movq	%r13, %rsi
	call	uv_fs_open@PLT
	movl	%eax, %r15d
	movq	-512(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r13, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r15d, %r15d
	js	.L583
	leaq	-537(%rbp), %rsi
	movl	%r15d, %edi
	call	uvwasi__get_filetype_by_fd@PLT
	movl	%eax, %r8d
	testw	%ax, %ax
	jne	.L547
	cmpw	$0, -554(%rbp)
	movzbl	-537(%rbp), %edx
	je	.L548
	cmpb	$3, %dl
	je	.L548
	movl	$54, %r8d
.L547:
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movl	%r8d, -548(%rbp)
	call	uv_fs_close@PLT
	movq	%r13, %rdi
	call	uv_fs_req_cleanup@PLT
.L580:
	movq	64(%rbx), %rax
	movq	-536(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movl	-548(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L584
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	movq	16(%rbp), %rcx
	andl	$16386, %ecx
	cmpq	$1, %rcx
	sbbl	%r12d, %r12d
	addl	$65602, %r12d
	cmpq	$1, %rcx
	sbbl	%r15d, %r15d
	addl	$65538, %r15d
	cmpq	$1, %rcx
	sbbl	%edx, %edx
	addl	$66, %edx
	cmpq	$1, %rcx
	sbbl	%edi, %edi
	addl	$2, %edi
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L550:
	movl	$28, %r8d
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L583:
	movq	64(%rbx), %rax
	movq	-536(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movl	%r15d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r8d
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	-520(%rbp), %r8
	leaq	-528(%rbp), %rcx
	movl	%r12d, %esi
	movl	%r15d, %edi
	call	uvwasi__get_rights@PLT
	movl	%eax, %r8d
	testw	%ax, %ax
	jne	.L547
	leaq	-504(%rbp), %rax
	movq	(%rbx), %rsi
	movl	%r15d, %edx
	movq	%rbx, %rdi
	movq	-536(%rbp), %rcx
	movzbl	-537(%rbp), %r9d
	pushq	%rax
	movq	24(%rbp), %rax
	andq	-520(%rbp), %rax
	pushq	$0
	pushq	%rax
	movq	16(%rbp), %rax
	movq	%rcx, %r8
	andq	-528(%rbp), %rax
	pushq	%rax
	call	uvwasi_fd_table_insert@PLT
	addq	$32, %rsp
	movl	%eax, %r8d
	testw	%ax, %ax
	jne	.L547
	movq	-504(%rbp), %rax
	movl	%r8d, -548(%rbp)
	movl	(%rax), %edx
	leaq	64(%rax), %rdi
	movl	%edx, (%r14)
	call	uv_mutex_unlock@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L582:
	movq	-512(%rbp), %rax
	movl	%r8d, -548(%rbp)
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movl	-548(%rbp), %r8d
	jmp	.L532
.L584:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE135:
	.size	uvwasi_path_open, .-uvwasi_path_open
	.p2align 4
	.globl	uvwasi_path_readlink
	.hidden	uvwasi_path_readlink
	.type	uvwasi_path_readlink, @function
uvwasi_path_readlink:
.LFB136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, -520(%rbp)
	movq	16(%rbp), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L592
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L592
	movq	%r8, %rbx
	testq	%r8, %r8
	je	.L592
	testq	%r13, %r13
	je	.L592
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	xorl	%r8d, %r8d
	movl	%ecx, %r15d
	leaq	-504(%rbp), %rdx
	movl	$32768, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r8d
	testw	%ax, %ax
	je	.L594
.L585:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L595
	addq	$488, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	movq	-504(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	%r15d, %ecx
	movq	%r14, %rdx
	leaq	-512(%rbp), %r8
	movq	%r12, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r8d
	testw	%ax, %ax
	jne	.L596
	movq	-512(%rbp), %rdx
	leaq	-496(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r15, %rsi
	movl	%r8d, -524(%rbp)
	call	uv_fs_readlink@PLT
	movl	%eax, %r14d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	64(%r12), %rax
	movq	-512(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	testl	%r14d, %r14d
	movl	-524(%rbp), %r8d
	jne	.L597
	movq	-400(%rbp), %r9
	movl	-520(%rbp), %r12d
	movl	%r8d, -524(%rbp)
	movq	%r9, %rdi
	movq	%r12, %rsi
	movq	%r9, -520(%rbp)
	call	strnlen@PLT
	movq	-520(%rbp), %r9
	movl	-524(%rbp), %r8d
	cmpq	%rax, %r12
	movq	%rax, %r14
	jbe	.L598
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -520(%rbp)
	call	memcpy@PLT
	movb	$0, (%rbx,%r14)
	addl	$1, %r14d
	movq	%r15, %rdi
	movl	%r14d, 0(%r13)
	call	uv_fs_req_cleanup@PLT
	movl	-520(%rbp), %r8d
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$28, %r8d
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L596:
	movq	-504(%rbp), %rax
	movl	%r8d, -520(%rbp)
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movl	-520(%rbp), %r8d
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	%r14d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r8d
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	$42, %r8d
	jmp	.L585
.L595:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE136:
	.size	uvwasi_path_readlink, .-uvwasi_path_readlink
	.p2align 4
	.globl	uvwasi_path_remove_directory
	.hidden	uvwasi_path_remove_directory
	.type	uvwasi_path_remove_directory, @function
uvwasi_path_remove_directory:
.LFB137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$464, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L603
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L603
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	leaq	-488(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$33554432, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L608
.L599:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	addq	$464, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	movq	-488(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	leaq	-496(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L610
	movq	-496(%rbp), %rdx
	leaq	-480(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	uv_fs_rmdir@PLT
	movl	%eax, %r13d
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	64(%rbx), %rax
	movq	-496(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L599
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L603:
	movl	$28, %r12d
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L610:
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L599
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE137:
	.size	uvwasi_path_remove_directory, .-uvwasi_path_remove_directory
	.p2align 4
	.globl	uvwasi_path_rename
	.hidden	uvwasi_path_rename
	.type	uvwasi_path_rename, @function
uvwasi_path_rename:
.LFB138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -536(%rbp)
	movl	%r8d, -532(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%dl
	testq	%r9, %r9
	sete	%al
	orb	%al, %dl
	jne	.L630
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L630
	movq	(%rdi), %rdi
	movl	%r8d, %r15d
	movl	%esi, %r12d
	movq	%r9, %r14
	call	uvwasi_fd_table_lock@PLT
	leaq	-512(%rbp), %rdx
	xorl	%r8d, %r8d
	cmpl	%r15d, %r12d
	je	.L639
	movq	(%rbx), %rdi
	movl	$65536, %ecx
	movl	%r12d, %esi
	call	uvwasi_fd_table_get_nolock@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	jne	.L638
	movl	-532(%rbp), %esi
	movq	(%rbx), %rdi
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$131072, %ecx
	call	uvwasi_fd_table_get_nolock@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	jne	.L640
	movq	(%rbx), %rdi
	call	uvwasi_fd_table_unlock@PLT
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	-536(%rbp), %ecx
	movq	-512(%rbp), %rsi
	leaq	-528(%rbp), %r8
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	jne	.L635
	movl	16(%rbp), %ecx
	movq	-504(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	leaq	-520(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	jne	.L635
	leaq	-496(%rbp), %r14
	movq	-520(%rbp), %rcx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movq	-528(%rbp), %rdx
	movq	%r14, %rsi
	call	uv_fs_rename@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L635
.L626:
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r15d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	cmpl	-532(%rbp), %r12d
	jne	.L619
.L617:
	movq	64(%rbx), %rax
	movq	-528(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	64(%rbx), %rax
	movq	-520(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
.L611:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	addq	$504, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movl	$196608, %ecx
	movl	%r12d, %esi
	call	uvwasi_fd_table_get_nolock@PLT
	movq	(%rbx), %rdi
	movl	%eax, %r15d
	movq	-512(%rbp), %rax
	movq	%rax, -504(%rbp)
	call	uvwasi_fd_table_unlock@PLT
	testw	%r15w, %r15w
	jne	.L611
	movl	-536(%rbp), %ecx
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-512(%rbp), %rsi
	leaq	-528(%rbp), %r8
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	je	.L642
.L636:
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$28, %r15d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L640:
	movq	-512(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
.L638:
	movq	(%rbx), %rdi
	call	uvwasi_fd_table_unlock@PLT
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L635:
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
.L619:
	movq	-512(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L642:
	movl	16(%rbp), %ecx
	movq	-504(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	leaq	-520(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r15d
	testw	%ax, %ax
	jne	.L636
	leaq	-496(%rbp), %r14
	movq	-520(%rbp), %rcx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movq	-528(%rbp), %rdx
	movq	%r14, %rsi
	call	uv_fs_rename@PLT
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L636
	jmp	.L626
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE138:
	.size	uvwasi_path_rename, .-uvwasi_path_rename
	.p2align 4
	.globl	uvwasi_path_symlink
	.hidden	uvwasi_path_symlink
	.type	uvwasi_path_symlink, @function
uvwasi_path_symlink:
.LFB139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L647
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L647
	movq	(%rdi), %rdi
	movl	%ecx, %esi
	movq	%r8, %r13
	leaq	-504(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$16777216, %ecx
	movl	%r9d, %r15d
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L652
.L643:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L653
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	movq	-504(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	%r15d, %ecx
	movq	%r13, %rdx
	leaq	-512(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L654
	movq	-512(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	leaq	-496(%rbp), %r15
	xorl	%edi, %edi
	movq	%r15, %rsi
	call	uv_fs_symlink@PLT
	movl	%eax, %r13d
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	64(%rbx), %rax
	movq	-512(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L643
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L647:
	movl	$28, %r12d
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L654:
	movq	-504(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L643
.L653:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE139:
	.size	uvwasi_path_symlink, .-uvwasi_path_symlink
	.p2align 4
	.globl	uvwasi_path_unlink_file
	.hidden	uvwasi_path_unlink_file
	.type	uvwasi_path_unlink_file, @function
uvwasi_path_unlink_file:
.LFB140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$464, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L659
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L659
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	%ecx, %r13d
	leaq	-488(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$67108864, %ecx
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L664
.L655:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L665
	addq	$464, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	-488(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	%r13d, %ecx
	movq	%r14, %rdx
	leaq	-496(%rbp), %r8
	movq	%rbx, %rdi
	call	uvwasi__resolve_path@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L666
	movq	-496(%rbp), %rdx
	leaq	-480(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	uv_fs_unlink@PLT
	movl	%eax, %r13d
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	movq	64(%rbx), %rax
	movq	-496(%rbp), %rdi
	movq	(%rax), %rsi
	call	*16(%rax)
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	je	.L655
	movl	%r13d, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L659:
	movl	$28, %r12d
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L666:
	movq	-488(%rbp), %rax
	leaq	64(%rax), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L655
.L665:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE140:
	.size	uvwasi_path_unlink_file, .-uvwasi_path_unlink_file
	.p2align 4
	.globl	uvwasi_poll_oneoff
	.hidden	uvwasi_poll_oneoff
	.type	uvwasi_poll_oneoff, @function
uvwasi_poll_oneoff:
.LFB141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1192, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L689
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L689
	testq	%rdx, %rdx
	movq	%rdx, -1192(%rbp)
	movl	%ecx, %r13d
	sete	%dl
	testl	%ecx, %ecx
	sete	%al
	orb	%al, %dl
	jne	.L689
	movq	%r8, %r15
	testq	%r8, %r8
	je	.L689
	movl	$0, (%r8)
	leaq	-1120(%rbp), %r14
	movl	%ecx, %edx
	movq	%r14, %rsi
	call	uvwasi__poll_oneoff_state_init@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L667
	movq	$0, -1200(%rbp)
	movq	-1192(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
.L676:
	movdqu	(%rbx), %xmm0
	movdqu	16(%rbx), %xmm1
	movdqu	32(%rbx), %xmm2
	movaps	%xmm0, -1168(%rbp)
	movzbl	-1160(%rbp), %eax
	movaps	%xmm1, -1152(%rbp)
	movaps	%xmm2, -1136(%rbp)
	testb	%al, %al
	je	.L669
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L702
	leaq	-1168(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r9, -1216(%rbp)
	movl	%edx, -1204(%rbp)
	movl	%ecx, -1192(%rbp)
	call	uvwasi__poll_oneoff_state_add_fdevent@PLT
	movl	-1192(%rbp), %ecx
	movl	-1204(%rbp), %edx
	testw	%ax, %ax
	movq	-1216(%rbp), %r9
	movl	%eax, %r12d
	jne	.L670
.L674:
	addl	$1, %edx
	addq	$48, %rbx
	cmpl	%edx, %r13d
	ja	.L676
	cmpl	$1, %ecx
	jne	.L679
	movq	-1200(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r9, -1192(%rbp)
	call	uvwasi__poll_oneoff_state_set_timer@PLT
	movq	-1192(%rbp), %r9
	testw	%ax, %ax
	movl	%eax, %r12d
	jne	.L670
.L679:
	movq	%r14, %rdi
	movq	%r9, -1192(%rbp)
	call	uvwasi__poll_oneoff_run@PLT
	movq	-1192(%rbp), %r9
	testw	%ax, %ax
	movl	%eax, %r12d
	jne	.L670
	movl	-72(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L680
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L670
	subl	$1, %eax
	movq	-1112(%rbp), %rsi
	movl	(%r15), %ecx
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	leaq	8(%rsi), %rdx
	leaq	56(%rsi,%rax), %r11
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L703:
	movl	$1, %esi
	movw	%si, 24(%rax)
.L682:
	addl	$1, %ecx
	movl	%ecx, (%r15)
.L684:
	addq	$48, %rdx
	cmpq	%r11, %rdx
	je	.L670
.L685:
	movl	%ecx, %eax
	movq	(%rdx), %rsi
	salq	$5, %rax
	addq	%r9, %rax
	movq	%rsi, (%rax)
	movzwl	10(%rdx), %esi
	movw	%si, 8(%rax)
	movzbl	8(%rdx), %edi
	movq	$0, 16(%rax)
	movb	%dil, 10(%rax)
	xorl	%edi, %edi
	movw	%di, 24(%rax)
	testw	%si, %si
	jne	.L682
	movl	32(%rdx), %esi
	testb	$4, %sil
	jne	.L703
	andl	$3, %esi
	jne	.L682
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L689:
	movl	$28, %r12d
.L667:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L704
	addq	$1192, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	movl	$28, %r12d
.L670:
	movq	%r14, %rdi
	call	uvwasi__poll_oneoff_state_cleanup@PLT
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L669:
	cmpw	$1, -1128(%rbp)
	movq	-1144(%rbp), %rax
	je	.L705
.L673:
	testl	%ecx, %ecx
	je	.L692
	movl	$1, %ecx
	cmpq	-1200(%rbp), %rax
	jnb	.L674
.L692:
	movq	-1168(%rbp), %rcx
	movq	%rax, -1200(%rbp)
	movq	%rcx, -1224(%rbp)
	movl	$1, %ecx
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L705:
	leaq	-1176(%rbp), %rdi
	movq	%r9, -1216(%rbp)
	movl	%edx, -1204(%rbp)
	movl	%ecx, -1192(%rbp)
	call	uvwasi__clock_gettime_realtime@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	jne	.L670
	movq	-1144(%rbp), %rax
	movq	-1216(%rbp), %r9
	movl	-1204(%rbp), %edx
	movl	-1192(%rbp), %ecx
	subq	-1176(%rbp), %rax
	jmp	.L673
.L680:
	movq	-1224(%rbp), %rax
	xorl	%r8d, %r8d
	movb	$0, 10(%r9)
	movw	%r8w, 8(%r9)
	movq	%rax, (%r9)
	movl	$1, (%r15)
	jmp	.L670
.L704:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE141:
	.size	uvwasi_poll_oneoff, .-uvwasi_poll_oneoff
	.p2align 4
	.globl	uvwasi_proc_exit
	.hidden	uvwasi_proc_exit
	.type	uvwasi_proc_exit, @function
uvwasi_proc_exit:
.LFB142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	exit@PLT
	.cfi_endproc
.LFE142:
	.size	uvwasi_proc_exit, .-uvwasi_proc_exit
	.p2align 4
	.globl	uvwasi_proc_raise
	.hidden	uvwasi_proc_raise
	.type	uvwasi_proc_raise, @function
uvwasi_proc_raise:
.LFB143:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L710
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	uvwasi__translate_to_uv_signal@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L711
	call	uv_os_getpid@PLT
	movl	%r12d, %esi
	movl	%eax, %edi
	call	uv_kill@PLT
	movl	%eax, %edi
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L718
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error@PLT
	.p2align 4,,10
	.p2align 3
.L710:
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	movl	$52, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE143:
	.size	uvwasi_proc_raise, .-uvwasi_proc_raise
	.p2align 4
	.globl	uvwasi_random_get
	.hidden	uvwasi_random_get
	.type	uvwasi_random_get, @function
uvwasi_random_get:
.LFB144:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L721
	testq	%rsi, %rsi
	je	.L721
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	xorl	%edi, %edi
	movq	%rsi, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_random@PLT
	movl	%eax, %edi
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L728
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore 6
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error@PLT
	.cfi_endproc
.LFE144:
	.size	uvwasi_random_get, .-uvwasi_random_get
	.p2align 4
	.globl	uvwasi_sched_yield
	.hidden	uvwasi_sched_yield
	.type	uvwasi_sched_yield, @function
uvwasi_sched_yield:
.LFB145:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L731
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	sched_yield@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L738
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	.cfi_restore 6
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movl	%eax, %edi
	jmp	uvwasi__translate_uv_error@PLT
	.cfi_endproc
.LFE145:
	.size	uvwasi_sched_yield, .-uvwasi_sched_yield
	.p2align 4
	.globl	uvwasi_sock_recv
	.hidden	uvwasi_sock_recv
	.type	uvwasi_sock_recv, @function
uvwasi_sock_recv:
.LFB146:
	.cfi_startproc
	endbr64
	movl	$58, %eax
	ret
	.cfi_endproc
.LFE146:
	.size	uvwasi_sock_recv, .-uvwasi_sock_recv
	.p2align 4
	.globl	uvwasi_sock_send
	.hidden	uvwasi_sock_send
	.type	uvwasi_sock_send, @function
uvwasi_sock_send:
.LFB147:
	.cfi_startproc
	endbr64
	movl	$58, %eax
	ret
	.cfi_endproc
.LFE147:
	.size	uvwasi_sock_send, .-uvwasi_sock_send
	.p2align 4
	.globl	uvwasi_sock_shutdown
	.hidden	uvwasi_sock_shutdown
	.type	uvwasi_sock_shutdown, @function
uvwasi_sock_shutdown:
.LFB148:
	.cfi_startproc
	endbr64
	movl	$58, %eax
	ret
	.cfi_endproc
.LFE148:
	.size	uvwasi_sock_shutdown, .-uvwasi_sock_shutdown
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UVWASI_UNKNOWN_ERROR"
.LC1:
	.string	"UVWASI_E2BIG"
.LC2:
	.string	"UVWASI_EADDRINUSE"
.LC3:
	.string	"UVWASI_EADDRNOTAVAIL"
.LC4:
	.string	"UVWASI_EAFNOSUPPORT"
.LC5:
	.string	"UVWASI_EAGAIN"
.LC6:
	.string	"UVWASI_EALREADY"
.LC7:
	.string	"UVWASI_EBADF"
.LC8:
	.string	"UVWASI_EBADMSG"
.LC9:
	.string	"UVWASI_EBUSY"
.LC10:
	.string	"UVWASI_ECANCELED"
.LC11:
	.string	"UVWASI_ECHILD"
.LC12:
	.string	"UVWASI_ECONNABORTED"
.LC13:
	.string	"UVWASI_ECONNREFUSED"
.LC14:
	.string	"UVWASI_ECONNRESET"
.LC15:
	.string	"UVWASI_EDEADLK"
.LC16:
	.string	"UVWASI_EDESTADDRREQ"
.LC17:
	.string	"UVWASI_EDOM"
.LC18:
	.string	"UVWASI_EDQUOT"
.LC19:
	.string	"UVWASI_EEXIST"
.LC20:
	.string	"UVWASI_EFAULT"
.LC21:
	.string	"UVWASI_EFBIG"
.LC22:
	.string	"UVWASI_EHOSTUNREACH"
.LC23:
	.string	"UVWASI_EIDRM"
.LC24:
	.string	"UVWASI_EILSEQ"
.LC25:
	.string	"UVWASI_EINPROGRESS"
.LC26:
	.string	"UVWASI_EINTR"
.LC27:
	.string	"UVWASI_EINVAL"
.LC28:
	.string	"UVWASI_EIO"
.LC29:
	.string	"UVWASI_EISCONN"
.LC30:
	.string	"UVWASI_EISDIR"
.LC31:
	.string	"UVWASI_ELOOP"
.LC32:
	.string	"UVWASI_EMFILE"
.LC33:
	.string	"UVWASI_EMLINK"
.LC34:
	.string	"UVWASI_EMSGSIZE"
.LC35:
	.string	"UVWASI_EMULTIHOP"
.LC36:
	.string	"UVWASI_ENAMETOOLONG"
.LC37:
	.string	"UVWASI_ENETDOWN"
.LC38:
	.string	"UVWASI_ENETRESET"
.LC39:
	.string	"UVWASI_ENETUNREACH"
.LC40:
	.string	"UVWASI_ENFILE"
.LC41:
	.string	"UVWASI_ENOBUFS"
.LC42:
	.string	"UVWASI_ENODEV"
.LC43:
	.string	"UVWASI_ENOENT"
.LC44:
	.string	"UVWASI_ENOEXEC"
.LC45:
	.string	"UVWASI_ENOLCK"
.LC46:
	.string	"UVWASI_ENOLINK"
.LC47:
	.string	"UVWASI_ENOMEM"
.LC48:
	.string	"UVWASI_ENOMSG"
.LC49:
	.string	"UVWASI_ENOPROTOOPT"
.LC50:
	.string	"UVWASI_ENOSPC"
.LC51:
	.string	"UVWASI_ENOSYS"
.LC52:
	.string	"UVWASI_ENOTCONN"
.LC53:
	.string	"UVWASI_ENOTDIR"
.LC54:
	.string	"UVWASI_ENOTEMPTY"
.LC55:
	.string	"UVWASI_ENOTRECOVERABLE"
.LC56:
	.string	"UVWASI_ENOTSOCK"
.LC57:
	.string	"UVWASI_ENOTSUP"
.LC58:
	.string	"UVWASI_ENOTTY"
.LC59:
	.string	"UVWASI_ENXIO"
.LC60:
	.string	"UVWASI_EOVERFLOW"
.LC61:
	.string	"UVWASI_EOWNERDEAD"
.LC62:
	.string	"UVWASI_EPERM"
.LC63:
	.string	"UVWASI_EPIPE"
.LC64:
	.string	"UVWASI_EPROTO"
.LC65:
	.string	"UVWASI_EPROTONOSUPPORT"
.LC66:
	.string	"UVWASI_EPROTOTYPE"
.LC67:
	.string	"UVWASI_ERANGE"
.LC68:
	.string	"UVWASI_EROFS"
.LC69:
	.string	"UVWASI_ESPIPE"
.LC70:
	.string	"UVWASI_ESRCH"
.LC71:
	.string	"UVWASI_ESTALE"
.LC72:
	.string	"UVWASI_ETIMEDOUT"
.LC73:
	.string	"UVWASI_ETXTBSY"
.LC74:
	.string	"UVWASI_EXDEV"
.LC75:
	.string	"UVWASI_ENOTCAPABLE"
.LC76:
	.string	"UVWASI_ESUCCESS"
.LC77:
	.string	"UVWASI_EACCES"
	.text
	.p2align 4
	.globl	uvwasi_embedder_err_code_to_string
	.hidden	uvwasi_embedder_err_code_to_string
	.type	uvwasi_embedder_err_code_to_string, @function
uvwasi_embedder_err_code_to_string:
.LFB149:
	.cfi_startproc
	endbr64
	movzwl	%di, %edx
	cmpw	$76, %di
	ja	.L743
	leaq	.L745(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L745:
	.long	.L821-.L745
	.long	.L822-.L745
	.long	.L819-.L745
	.long	.L818-.L745
	.long	.L817-.L745
	.long	.L816-.L745
	.long	.L815-.L745
	.long	.L814-.L745
	.long	.L813-.L745
	.long	.L812-.L745
	.long	.L811-.L745
	.long	.L810-.L745
	.long	.L809-.L745
	.long	.L808-.L745
	.long	.L807-.L745
	.long	.L806-.L745
	.long	.L805-.L745
	.long	.L804-.L745
	.long	.L803-.L745
	.long	.L802-.L745
	.long	.L801-.L745
	.long	.L800-.L745
	.long	.L799-.L745
	.long	.L798-.L745
	.long	.L797-.L745
	.long	.L796-.L745
	.long	.L795-.L745
	.long	.L794-.L745
	.long	.L793-.L745
	.long	.L792-.L745
	.long	.L791-.L745
	.long	.L790-.L745
	.long	.L789-.L745
	.long	.L788-.L745
	.long	.L787-.L745
	.long	.L786-.L745
	.long	.L785-.L745
	.long	.L784-.L745
	.long	.L783-.L745
	.long	.L782-.L745
	.long	.L781-.L745
	.long	.L780-.L745
	.long	.L779-.L745
	.long	.L778-.L745
	.long	.L777-.L745
	.long	.L776-.L745
	.long	.L775-.L745
	.long	.L774-.L745
	.long	.L773-.L745
	.long	.L772-.L745
	.long	.L771-.L745
	.long	.L770-.L745
	.long	.L769-.L745
	.long	.L768-.L745
	.long	.L767-.L745
	.long	.L766-.L745
	.long	.L765-.L745
	.long	.L764-.L745
	.long	.L763-.L745
	.long	.L762-.L745
	.long	.L761-.L745
	.long	.L760-.L745
	.long	.L759-.L745
	.long	.L758-.L745
	.long	.L757-.L745
	.long	.L756-.L745
	.long	.L755-.L745
	.long	.L754-.L745
	.long	.L753-.L745
	.long	.L752-.L745
	.long	.L751-.L745
	.long	.L750-.L745
	.long	.L749-.L745
	.long	.L748-.L745
	.long	.L747-.L745
	.long	.L746-.L745
	.long	.L744-.L745
	.text
	.p2align 4,,10
	.p2align 3
.L819:
	leaq	.LC77(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	leaq	.LC45(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L776:
	leaq	.LC44(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	leaq	.LC43(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	leaq	.LC42(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L779:
	leaq	.LC41(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	leaq	.LC40(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	leaq	.LC39(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	leaq	.LC38(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	leaq	.LC37(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L784:
	leaq	.LC36(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	leaq	.LC35(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	leaq	.LC34(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	.LC75(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	.LC74(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	leaq	.LC76(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L808:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L771:
	leaq	.LC49(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	leaq	.LC48(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	leaq	.LC47(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	leaq	.LC46(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L751:
	leaq	.LC69(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	leaq	.LC68(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	leaq	.LC67(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L754:
	leaq	.LC66(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	.LC65(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	.LC64(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	.LC63(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	leaq	.LC62(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L747:
	leaq	.LC73(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L748:
	leaq	.LC72(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	leaq	.LC71(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L750:
	leaq	.LC70(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	leaq	.LC33(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L788:
	leaq	.LC32(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	.LC31(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	leaq	.LC30(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	leaq	.LC29(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	leaq	.LC28(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	leaq	.LC27(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	.LC26(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	leaq	.LC25(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	leaq	.LC24(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	leaq	.LC21(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L802:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	leaq	.LC61(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	leaq	.LC60(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	leaq	.LC59(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	.LC58(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	leaq	.LC57(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	leaq	.LC56(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	leaq	.LC55(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	leaq	.LC54(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	leaq	.LC53(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	leaq	.LC52(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	leaq	.LC51(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	leaq	.LC50(%rip), %rax
	ret
.L743:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE149:
	.size	uvwasi_embedder_err_code_to_string, .-uvwasi_embedder_err_code_to_string
	.section	.rodata
	.align 16
	.type	CSWTCH.172, @object
	.size	CSWTCH.172, 24
CSWTCH.172:
	.long	0
	.long	2
	.long	1
	.long	3
	.long	4
	.long	5
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	default_allocator, @object
	.size	default_allocator, 40
default_allocator:
	.quad	0
	.quad	default_malloc
	.quad	default_free
	.quad	default_calloc
	.quad	default_realloc
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
