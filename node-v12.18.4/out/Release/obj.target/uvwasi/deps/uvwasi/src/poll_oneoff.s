	.file	"poll_oneoff.c"
	.text
	.p2align 4
	.type	poll_cb, @function
poll_cb:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	uv_poll_stop@PLT
	movq	%rbx, %rdi
	call	uv_handle_get_data@PLT
	movl	%r13d, 40(%rax)
	testl	%r12d, %r12d
	je	.L2
	movl	$29, %edx
	movw	%dx, 18(%rax)
.L2:
	movq	8(%rbx), %rdi
	call	uv_loop_get_data@PLT
	addl	$1, 1048(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE54:
	.size	poll_cb, .-poll_cb
	.p2align 4
	.type	timeout_cb, @function
timeout_cb:
.LFB55:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rdi
	call	uv_loop_get_data@PLT
	movl	1044(%rax), %edx
	testl	%edx, %edx
	je	.L8
	movq	%rax, %r12
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L10:
	movl	%ebx, %eax
	addl	$1, %ebx
	leaq	(%rax,%rax,4), %rdi
	salq	$5, %rdi
	addq	16(%r12), %rdi
	call	uv_poll_stop@PLT
	cmpl	%ebx, 1044(%r12)
	ja	.L10
.L8:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE55:
	.size	timeout_cb, .-timeout_cb
	.p2align 4
	.globl	uvwasi__poll_oneoff_state_init
	.hidden	uvwasi__poll_oneoff_state_init
	.type	uvwasi__poll_oneoff_state_init, @function
uvwasi__poll_oneoff_state_init:
.LFB56:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L19
	movq	$0, 16(%rsi)
	pxor	%xmm0, %xmm0
	leaq	184(%rsi), %r14
	movq	%rdi, %r12
	movl	$0, 1048(%rsi)
	movq	%r14, %rdi
	movl	%edx, %r13d
	movq	$0, 176(%rsi)
	movq	$0, 1032(%rsi)
	movq	$0, 1040(%rsi)
	movups	%xmm0, (%rsi)
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L28
	testl	%r13d, %r13d
	je	.L18
	movl	%r13d, %r15d
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	uvwasi__calloc@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L17
	movl	$160, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	uvwasi__calloc@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L17
.L18:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	uv_loop_set_data@PLT
	movq	%r12, (%rbx)
	xorl	%eax, %eax
	movl	%r13d, 1032(%rbx)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$28, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	%r14, %rdi
	call	uv_loop_close@PLT
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	uvwasi__free@PLT
	movq	16(%rbx), %rsi
	movq	(%rbx), %rdi
	call	uvwasi__free@PLT
	movl	$48, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$8, %rsp
	movl	%eax, %edi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE56:
	.size	uvwasi__poll_oneoff_state_init, .-uvwasi__poll_oneoff_state_init
	.p2align 4
	.globl	uvwasi__poll_oneoff_state_cleanup
	.hidden	uvwasi__poll_oneoff_state_cleanup
	.type	uvwasi__poll_oneoff_state_cleanup, @function
uvwasi__poll_oneoff_state_cleanup:
.LFB57:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L38
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	1036(%rdi), %edi
	testl	%edi, %edi
	jne	.L52
.L31:
	movl	1040(%rbx), %edx
	xorl	%r12d, %r12d
	testl	%edx, %edx
	je	.L36
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%r12d, %eax
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	8(%rbx), %rax
	movl	32(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L35
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L35
	addq	$64, %rdi
	call	uv_mutex_unlock@PLT
	movl	1040(%rbx), %edx
.L35:
	addl	$1, %r12d
	cmpl	%edx, %r12d
	jb	.L32
.L36:
	movl	1044(%rbx), %esi
	xorl	%r12d, %r12d
	testl	%esi, %esi
	je	.L34
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%r12d, %eax
	xorl	%esi, %esi
	addl	$1, %r12d
	leaq	(%rax,%rax,4), %rdi
	salq	$5, %rdi
	addq	16(%rbx), %rdi
	call	uv_close@PLT
	cmpl	%r12d, 1044(%rbx)
	ja	.L33
.L34:
	leaq	184(%rbx), %r12
	movl	$2, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	movl	$0, 1032(%rbx)
	movq	$0, 1040(%rbx)
	call	uvwasi__free@PLT
	movq	(%rbx), %rdi
	movq	16(%rbx), %rsi
	call	uvwasi__free@PLT
	movq	$0, 16(%rbx)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, (%rbx)
	call	uv_loop_close@PLT
	testl	%eax, %eax
	jne	.L37
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movl	$0, 1036(%rbx)
	leaq	24(%rbx), %rdi
	xorl	%esi, %esi
	movq	$0, 176(%rbx)
	call	uv_close@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	popq	%rbx
	.cfi_restore 3
	movl	%eax, %edi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE57:
	.size	uvwasi__poll_oneoff_state_cleanup, .-uvwasi__poll_oneoff_state_cleanup
	.p2align 4
	.globl	uvwasi__poll_oneoff_state_set_timer
	.hidden	uvwasi__poll_oneoff_state_set_timer
	.type	uvwasi__poll_oneoff_state_set_timer, @function
uvwasi__poll_oneoff_state_set_timer:
.LFB58:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L56
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	24(%rdi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	184(%rdi), %rdi
	call	uv_timer_init@PLT
	testl	%eax, %eax
	jne	.L61
	movabsq	$4835703278458516699, %rdx
	movq	%r12, %rax
	movl	$1, 1036(%rbx)
	mulq	%rdx
	xorl	%eax, %eax
	shrq	$18, %rdx
	movq	%rdx, 176(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	popq	%rbx
	.cfi_restore 3
	movl	%eax, %edi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error@PLT
	.cfi_endproc
.LFE58:
	.size	uvwasi__poll_oneoff_state_set_timer, .-uvwasi__poll_oneoff_state_set_timer
	.p2align 4
	.globl	uvwasi__poll_oneoff_state_add_fdevent
	.hidden	uvwasi__poll_oneoff_state_add_fdevent
	.type	uvwasi__poll_oneoff_state_add_fdevent, @function
uvwasi__poll_oneoff_state_add_fdevent:
.LFB59:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L74
	movl	1040(%rdi), %edx
	movq	%rsi, %r13
	movq	8(%rdi), %rax
	movq	%rdi, %r12
	movzbl	8(%r13), %r14d
	movl	16(%rsi), %esi
	leaq	(%rdx,%rdx,2), %rbx
	movq	%rdx, %rdi
	salq	$4, %rbx
	addq	%rax, %rbx
	cmpb	$1, %r14b
	je	.L81
	cmpb	$2, %r14b
	jne	.L74
	movl	$6, 36(%rbx)
	movl	$134217792, %r9d
.L65:
	movl	$0, 32(%rbx)
	testl	%edi, %edi
	je	.L66
	leal	-1(%rdi), %edx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	leaq	48(%rax,%rdx), %rcx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$48, %rax
	cmpq	%rax, %rcx
	je	.L66
.L69:
	movq	(%rax), %rdx
	cmpl	%esi, (%rdx)
	jne	.L67
	movq	24(%rax), %rax
	movzwl	18(%rbx), %r15d
	movq	%rdx, (%rbx)
	movl	$1, 32(%rbx)
	movq	%rax, 24(%rbx)
.L68:
	movb	%r14b, 16(%rbx)
	movq	0(%r13), %rax
	addl	$1, %edi
	movw	%r15w, 18(%rbx)
	xorl	%r15d, %r15d
	movq	%rax, 8(%rbx)
	movl	$0, 40(%rbx)
	movl	%edi, 1040(%r12)
.L62:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movl	$28, %r15d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%r12), %rax
	xorl	%r8d, %r8d
	movq	%r9, %rcx
	movq	%rbx, %rdx
	movq	(%rax), %rdi
	call	uvwasi_fd_table_get@PLT
	movl	%eax, %r15d
	cmpw	$8, %ax
	je	.L82
	testw	%ax, %ax
	jne	.L62
	movl	1044(%r12), %eax
	leaq	184(%r12), %rdi
	leaq	(%rax,%rax,4), %r8
	movq	(%rbx), %rax
	salq	$5, %r8
	addq	16(%r12), %r8
	movl	4(%rax), %edx
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	call	uv_poll_init@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L83
	movl	36(%rbx), %esi
	movq	%r8, %rdi
	leaq	poll_cb(%rip), %rdx
	movq	%r8, -56(%rbp)
	call	uv_poll_start@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L84
	movl	1040(%r12), %eax
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	leaq	(%rax,%rax,2), %rsi
	salq	$4, %rsi
	addq	8(%r12), %rsi
	call	uv_handle_set_data@PLT
	movq	-56(%rbp), %r8
	movl	1040(%r12), %edi
	movq	%r8, 24(%rbx)
	addl	$1, 1044(%r12)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$5, 36(%rbx)
	movl	$134217730, %r9d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L82:
	movq	$0, (%rbx)
	movl	1040(%r12), %edi
	jmp	.L68
.L84:
	movq	(%rbx), %rdi
	movl	%eax, -60(%rbp)
	addq	$64, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	uv_close@PLT
	movl	-60(%rbp), %eax
.L80:
	addq	$24, %rsp
	movl	%eax, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error@PLT
.L83:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movl	%eax, -56(%rbp)
	addq	$64, %rdi
	call	uv_mutex_unlock@PLT
	movl	-56(%rbp), %eax
	jmp	.L80
	.cfi_endproc
.LFE59:
	.size	uvwasi__poll_oneoff_state_add_fdevent, .-uvwasi__poll_oneoff_state_add_fdevent
	.p2align 4
	.globl	uvwasi__poll_oneoff_run
	.hidden	uvwasi__poll_oneoff_run
	.type	uvwasi__poll_oneoff_run, @function
uvwasi__poll_oneoff_run:
.LFB60:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$1, 1036(%rdi)
	movq	%rdi, %rbx
	je	.L93
.L87:
	xorl	%esi, %esi
	leaq	184(%rbx), %rdi
	call	uv_run@PLT
	testl	%eax, %eax
	jne	.L92
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	176(%rdi), %rdx
	leaq	24(%rdi), %r12
	xorl	%ecx, %ecx
	leaq	timeout_cb(%rip), %rsi
	movq	%r12, %rdi
	call	uv_timer_start@PLT
	testl	%eax, %eax
	jne	.L92
	movl	1040(%rbx), %eax
	testl	%eax, %eax
	je	.L87
	movq	%r12, %rdi
	call	uv_unref@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L92:
	popq	%rbx
	movl	%eax, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uvwasi__translate_uv_error@PLT
	.cfi_endproc
.LFE60:
	.size	uvwasi__poll_oneoff_run, .-uvwasi__poll_oneoff_run
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
