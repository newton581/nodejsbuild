	.file	"clocks.c"
	.text
	.p2align 4
	.globl	uvwasi__clock_gettime_realtime
	.hidden	uvwasi__clock_gettime_realtime
	.type	uvwasi__clock_gettime_realtime, @function
uvwasi__clock_gettime_realtime:
.LFB54:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-48(%rbp), %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_gettimeofday@PLT
	testl	%eax, %eax
	jne	.L7
	imull	$1000, -40(%rbp), %edx
	imulq	$1000000000, -48(%rbp), %rax
	movslq	%edx, %rdx
	addq	%rdx, %rax
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L1:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L8
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	jmp	.L1
.L8:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE54:
	.size	uvwasi__clock_gettime_realtime, .-uvwasi__clock_gettime_realtime
	.p2align 4
	.globl	uvwasi__clock_gettime_process_cputime
	.hidden	uvwasi__clock_gettime_process_cputime
	.type	uvwasi__clock_gettime_process_cputime, @function
uvwasi__clock_gettime_process_cputime:
.LFB55:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$2, %edi
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L14
	imulq	$1000000000, -48(%rbp), %rax
	addq	-40(%rbp), %rax
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L9:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L15
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	jmp	.L9
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE55:
	.size	uvwasi__clock_gettime_process_cputime, .-uvwasi__clock_gettime_process_cputime
	.p2align 4
	.globl	uvwasi__clock_gettime_thread_cputime
	.hidden	uvwasi__clock_gettime_thread_cputime
	.type	uvwasi__clock_gettime_thread_cputime, @function
uvwasi__clock_gettime_thread_cputime:
.LFB56:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$3, %edi
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L21
	imulq	$1000000000, -48(%rbp), %rax
	addq	-40(%rbp), %rax
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L16:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L22
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	jmp	.L16
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE56:
	.size	uvwasi__clock_gettime_thread_cputime, .-uvwasi__clock_gettime_thread_cputime
	.p2align 4
	.globl	uvwasi__clock_getres_process_cputime
	.hidden	uvwasi__clock_getres_process_cputime
	.type	uvwasi__clock_getres_process_cputime, @function
uvwasi__clock_getres_process_cputime:
.LFB57:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$2, %edi
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	clock_getres@PLT
	movl	%eax, %r8d
	movl	$1000000, %eax
	testl	%r8d, %r8d
	jne	.L24
	imulq	$1000000000, -48(%rbp), %rax
	addq	-40(%rbp), %rax
.L24:
	movq	%rax, (%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE57:
	.size	uvwasi__clock_getres_process_cputime, .-uvwasi__clock_getres_process_cputime
	.p2align 4
	.globl	uvwasi__clock_getres_thread_cputime
	.hidden	uvwasi__clock_getres_thread_cputime
	.type	uvwasi__clock_getres_thread_cputime, @function
uvwasi__clock_getres_thread_cputime:
.LFB60:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$3, %edi
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	clock_gettime@PLT
	testl	%eax, %eax
	jne	.L35
	imulq	$1000000000, -48(%rbp), %rax
	addq	-40(%rbp), %rax
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L30:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L36
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	jmp	.L30
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE60:
	.size	uvwasi__clock_getres_thread_cputime, .-uvwasi__clock_getres_thread_cputime
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
