	.file	"path_resolver.c"
	.text
	.p2align 4
	.type	uvwasi__is_path_sandboxed, @function
uvwasi__is_path_sandboxed:
.LFB70:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L2
	testl	%r12d, %r12d
	je	.L2
	cmpb	$47, (%rdx)
	je	.L26
.L2:
	cmpl	$1, %r12d
	je	.L27
.L4:
	movq	%rbx, %rdi
	call	strstr@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	cmpq	%r8, %rbx
	je	.L28
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	subl	%r12d, %r13d
	movl	$1, %eax
	cmpl	$1, %r13d
	jle	.L1
	addq	%rbx, %r12
	cmpb	$47, (%r12)
	je	.L29
.L6:
	movl	$1, %eax
	cmpl	$2, %r13d
	je	.L30
	jle	.L1
	cmpb	$46, (%r12)
	jne	.L1
	cmpb	$46, 1(%r12)
	jne	.L1
	xorl	%eax, %eax
	cmpb	$47, 2(%r12)
	setne	%al
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	cmpb	$46, (%rsi)
	jne	.L4
	movl	$1, %eax
	cmpl	$2, %r13d
	je	.L31
	jbe	.L1
	cmpb	$46, (%rbx)
	jne	.L1
	cmpb	$46, 1(%rbx)
	jne	.L1
	xorl	%eax, %eax
	cmpb	$47, 2(%rbx)
	setne	%al
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	call	strstr@PLT
	cmpq	%rax, %rbx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	cmpb	$46, (%r12)
	jne	.L1
	xorl	%eax, %eax
	cmpb	$46, 1(%r12)
	setne	%al
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$1, %r12
	subl	$1, %r13d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L31:
	cmpb	$46, (%rbx)
	jne	.L1
	xorl	%eax, %eax
	cmpb	$46, 1(%rbx)
	setne	%al
	jmp	.L1
	.cfi_endproc
.LFE70:
	.size	uvwasi__is_path_sandboxed, .-uvwasi__is_path_sandboxed
	.p2align 4
	.globl	uvwasi__normalize_path
	.hidden	uvwasi__normalize_path
	.type	uvwasi__normalize_path, @function
uvwasi__normalize_path:
.LFB69:
	.cfi_startproc
	endbr64
	movl	$42, %eax
	cmpl	%ecx, %esi
	ja	.L99
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L34
	testl	%esi, %esi
	je	.L34
	movzbl	(%rdi), %eax
	movb	%al, -50(%rbp)
	cmpb	$47, %al
	je	.L102
	movb	$0, (%rdx)
	xorl	%r13d, %r13d
	movb	$46, -50(%rbp)
.L60:
	movl	%r13d, %eax
	movq	%r12, %r14
	andl	$1, %eax
	movb	%al, -49(%rbp)
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r15, %rbx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L104:
	addq	$1, %rbx
	testb	%al, %al
	je	.L103
.L39:
	movzbl	(%rbx), %eax
	cmpb	$47, %al
	jne	.L104
	movq	%rbx, %r13
	subq	%r15, %r13
	jne	.L58
	cmpq	%r14, %r12
	jne	.L41
	cmpb	$0, -49(%rbp)
	je	.L41
	movb	$47, (%r14)
	addq	$1, %r14
.L41:
	movb	$0, (%r14)
.L42:
	leaq	1(%rbx), %r15
	jmp	.L56
.L34:
	movb	$0, (%r12)
	movb	$46, -50(%rbp)
	testq	%r15, %r15
	je	.L37
	xorl	%r13d, %r13d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%ebx, %ebx
.L58:
	cmpq	$1, %r13
	je	.L105
	cmpq	$2, %r13
	je	.L106
.L45:
	cmpq	%r14, %r12
	je	.L54
	cmpb	$47, -1(%r14)
	je	.L54
	movb	$47, (%r14)
	addq	$1, %r14
.L54:
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	addq	%r13, %r14
	call	memcpy@PLT
	movb	$0, (%r14)
.L44:
	testq	%rbx, %rbx
	jne	.L42
	xorl	%eax, %eax
	cmpq	%r12, %r14
	je	.L37
.L32:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L66
	movb	$0, (%r14)
	xorl	%eax, %eax
	cmpq	%r12, %r14
	jne	.L32
.L37:
	movzbl	-50(%rbp), %eax
	movb	$0, 1(%r12)
	movb	%al, (%r12)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	cmpb	$46, (%r15)
	jne	.L45
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L106:
	cmpb	$46, (%r15)
	jne	.L45
	cmpb	$46, 1(%r15)
	jne	.L45
	cmpb	$47, (%r14)
	je	.L46
	cmpq	%r14, %r12
	je	.L47
	movq	%r14, %rax
	jmp	.L49
.L108:
	cmpq	%rax, %r12
	je	.L107
.L49:
	movzbl	-1(%rax), %edx
	subq	$1, %rax
	cmpb	$47, %dl
	jne	.L108
	leaq	-3(%r14), %rsi
	cmpq	%rsi, %rax
	je	.L109
.L62:
	cmpq	%rax, %r12
	jne	.L52
	cmpb	$47, %dl
	je	.L57
.L52:
	movb	$0, (%rax)
	movq	%rax, %r14
	jmp	.L44
.L99:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L102:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$0, (%rdx)
	movl	$1, %r13d
	jmp	.L60
.L107:
	leaq	-2(%r14), %rsi
	cmpq	%rsi, %r12
	jne	.L50
	cmpb	$46, %dl
	jne	.L50
	cmpb	$46, 1(%r12)
	jne	.L52
.L51:
	cmpb	$47, -1(%r14)
	je	.L47
	movb	$47, (%r14)
	addq	$1, %r14
.L47:
	movl	$11822, %eax
	movw	%ax, (%r14)
	leaq	2(%r14), %rax
	jmp	.L52
.L109:
	cmpb	$46, 1(%rax)
	je	.L110
.L53:
	cmpq	%rax, %r12
	jne	.L52
.L57:
	addq	$1, %rax
	jmp	.L52
.L50:
	leaq	-3(%r14), %rcx
	cmpq	%rcx, %rax
	je	.L52
	jmp	.L62
.L46:
	movq	%r14, %rax
	cmpq	%r14, %r12
	je	.L47
	jmp	.L52
.L110:
	cmpb	$46, 2(%rax)
	jne	.L53
	jmp	.L51
	.cfi_endproc
.LFE69:
	.size	uvwasi__normalize_path, .-uvwasi__normalize_path
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s/%s"
	.text
	.p2align 4
	.globl	uvwasi__resolve_path
	.hidden	uvwasi__resolve_path
	.type	uvwasi__resolve_path, @function
uvwasi__resolve_path:
.LFB74:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$1, %r9d
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$520, %rsp
	movq	%rsi, -504(%rbp)
	movq	%r8, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r9d, -528(%rbp)
	movl	$32, -540(%rbp)
	movq	$0, -512(%rbp)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L123:
	cmpb	$47, (%r8)
	je	.L126
.L125:
	movb	$47, (%r12)
	addq	$1, %r12
.L126:
	movslq	%ebx, %rbx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rbx, %rdx
	addq	%rbx, %r12
	call	memcpy@PLT
.L124:
	movl	-528(%rbp), %eax
	movb	$0, (%r12)
	testl	%eax, %eax
	je	.L131
.L163:
	leaq	-496(%rbp), %r12
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	uv_fs_readlink@PLT
	testl	%eax, %eax
	jne	.L158
	subl	$1, -540(%rbp)
	je	.L159
	movq	-400(%rbp), %rdi
	call	strlen@PLT
	movq	-512(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -520(%rbp)
	movl	%eax, %ebx
	call	uvwasi__free@PLT
	movq	-520(%rbp), %rax
	movq	%r14, %rdi
	leal	1(%rax), %edx
	movq	%rdx, %rsi
	movq	%rdx, -512(%rbp)
	call	uvwasi__malloc@PLT
	movq	-512(%rbp), %rdx
	testq	%rax, %rax
	je	.L160
	movq	-400(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -512(%rbp)
	call	memcpy@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-512(%rbp), %r10
.L112:
	testq	%r10, %r10
	je	.L113
	testl	%ebx, %ebx
	je	.L113
	cmpb	$47, (%r10)
	je	.L161
.L113:
	movq	-504(%rbp), %rax
	movq	%r10, -520(%rbp)
	movq	24(%rax), %rdi
	call	strlen@PLT
	movq	%r14, %rdi
	addl	%eax, %ebx
	movq	%rax, -536(%rbp)
	leal	2(%rbx), %esi
	movl	%ebx, -524(%rbp)
	movslq	%esi, %rbx
	movq	%rbx, %rsi
	call	uvwasi__malloc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L137
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	uvwasi__malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L118
	movq	-520(%rbp), %r10
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	-504(%rbp), %rax
	movq	$-1, %rcx
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	movq	%r13, %rdi
	pushq	%r10
	movq	24(%rax), %r9
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jle	.L162
	movl	-524(%rbp), %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	addl	$1, %esi
	movl	%esi, %ecx
	call	uvwasi__normalize_path
	testw	%ax, %ax
	jne	.L120
	movq	%r12, %rdi
	call	strlen@PLT
	movl	-536(%rbp), %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%eax, %ebx
	movq	-504(%rbp), %rax
	movq	24(%rax), %rdx
	call	uvwasi__is_path_sandboxed
	testl	%eax, %eax
	je	.L121
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r12, %r13
	call	uvwasi__free@PLT
.L117:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	movq	-504(%rbp), %rax
	movq	24(%rax), %r15
	movq	16(%rax), %rdi
	call	strlen@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movl	%eax, %edx
	cmpl	$1, %eax
	jne	.L122
	xorl	%edx, %edx
	cmpb	$46, (%r15)
	setne	%dl
.L122:
	subl	%edx, %ebx
	movq	%r14, %rdi
	movl	%edx, -520(%rbp)
	leal	2(%rbx,%r12), %esi
	call	uvwasi__malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L137
	movq	-504(%rbp), %rax
	movslq	-520(%rbp), %rdx
	movslq	%r12d, %r12
	movq	%r15, %rdi
	movq	16(%rax), %rsi
	leaq	0(%r13,%rdx), %r8
	movq	%r12, %rdx
	addq	%r15, %r12
	movq	%r8, -520(%rbp)
	call	memcpy@PLT
	cmpl	$1, %ebx
	movq	-520(%rbp), %r8
	jg	.L123
	jne	.L124
	cmpb	$47, (%r8)
	jne	.L125
	movl	-528(%rbp), %eax
	movb	$0, (%r12)
	testl	%eax, %eax
	jne	.L163
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-552(%rbp), %rax
	movq	%r15, (%rax)
	xorl	%eax, %eax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$48, %eax
.L115:
	movl	%eax, -504(%rbp)
	movq	-552(%rbp), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	$0, (%rax)
	call	uvwasi__free@PLT
	movl	-504(%rbp), %eax
.L134:
	movq	-512(%rbp), %rsi
	movq	%r14, %rdi
	movl	%eax, -504(%rbp)
	call	uvwasi__free@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-504(%rbp), %eax
	jne	.L164
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	uvwasi__free@PLT
	movl	$48, %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L161:
	leal	1(%rbx), %esi
	movq	%r14, %rdi
	movq	%r10, -520(%rbp)
	movslq	%esi, %rsi
	call	uvwasi__malloc@PLT
	movq	-520(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L165
	movl	%ebx, %ecx
	movq	%rax, %rdx
	movl	%ebx, %esi
	movq	%r10, %rdi
	call	uvwasi__normalize_path
	testw	%ax, %ax
	jne	.L116
	movq	-504(%rbp), %rax
	movq	24(%rax), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rdx
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	uvwasi__is_path_sandboxed
	testl	%eax, %eax
	jne	.L117
.L156:
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	uvwasi__free@PLT
	movl	$76, %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%eax, -504(%rbp)
	call	uvwasi__free@PLT
.L157:
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	uvwasi__free@PLT
	movl	-504(%rbp), %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L162:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	uv_translate_sys_error@PLT
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	testw	%ax, %ax
	jne	.L120
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	call	uvwasi__free@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L165:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	movl	$48, %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%eax, -504(%rbp)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	$32, %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L158:
	cmpl	$-22, %eax
	je	.L138
	cmpl	$-2, %eax
	jne	.L129
.L138:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-552(%rbp), %rax
	movq	%r15, (%rax)
	xorl	%eax, %eax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	$48, %eax
	movq	$0, -512(%rbp)
	jmp	.L115
.L129:
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movq	%r12, %rdi
	movl	%eax, -504(%rbp)
	call	uv_fs_req_cleanup@PLT
	movl	-504(%rbp), %eax
	testw	%ax, %ax
	je	.L131
	jmp	.L115
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE74:
	.size	uvwasi__resolve_path, .-uvwasi__resolve_path
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
