	.file	"fd_table.c"
	.text
	.p2align 4
	.globl	uvwasi_fd_table_insert
	.hidden	uvwasi_fd_table_insert
	.type	uvwasi_fd_table_insert, @function
uvwasi_fd_table_insert:
.LFB78:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	%edx, -84(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%rcx, -64(%rbp)
	call	strlen@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	movq	%r15, %rdi
	leaq	107(%rax,%rbx,2), %rsi
	movq	%rax, %r12
	call	uvwasi__malloc@PLT
	testq	%rax, %rax
	je	.L15
	leaq	104(%rax), %rcx
	movq	-64(%rbp), %r10
	movq	%rax, %r13
	leaq	105(%rax,%rbx), %rax
	leaq	1(%rax,%r12), %rdi
	movq	%rbx, %rdx
	movq	%rcx, -64(%rbp)
	movq	%r10, %rsi
	movq	%rdi, -80(%rbp)
	movq	%rcx, %rdi
	movq	%rax, -72(%rbp)
	call	memcpy@PLT
	movq	%r14, %rsi
	movq	-72(%rbp), %r14
	movq	%r12, %rdx
	movb	$0, 104(%r13,%rbx)
	movq	%r14, %rdi
	call	memcpy@PLT
	movb	$0, (%r14,%r12)
	movq	-80(%rbp), %rdx
	movl	%ebx, %ecx
	movq	-64(%rbp), %rdi
	movl	%ebx, %esi
	call	uvwasi__normalize_path@PLT
	movl	%eax, %r12d
	movq	-56(%rbp), %rax
	leaq	16(%rax), %r14
	testw	%r12w, %r12w
	jne	.L23
	movq	%r14, %rdi
	call	uv_rwlock_wrlock@PLT
	movq	-56(%rbp), %rax
	movl	8(%rax), %edx
	cmpl	%edx, 12(%rax)
	jnb	.L5
	testl	%edx, %edx
	je	.L7
	movq	(%rax), %rax
	xorl	%ebx, %ebx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L24:
	addl	$1, %ebx
	addq	$8, %rax
	cmpl	%ebx, %edx
	je	.L7
.L12:
	cmpq	$0, (%rax)
	movq	%rax, %rcx
	jne	.L24
.L10:
	movq	%r13, (%rcx)
	leaq	64(%r13), %r15
	movq	%r15, %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L25
	movl	-84(%rbp), %eax
	movq	-64(%rbp), %xmm0
	movl	%ebx, 0(%r13)
	cmpq	$0, 40(%rbp)
	movl	%eax, 4(%r13)
	movq	-80(%rbp), %rax
	movhps	-72(%rbp), %xmm0
	movups	%xmm0, 8(%r13)
	movq	16(%rbp), %xmm0
	movq	%rax, 24(%r13)
	movzbl	-88(%rbp), %eax
	movhps	24(%rbp), %xmm0
	movb	%al, 32(%r13)
	movl	32(%rbp), %eax
	movups	%xmm0, 40(%r13)
	movl	%eax, 56(%r13)
	je	.L14
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	40(%rbp), %rax
	movq	%r13, (%rax)
.L14:
	movq	-56(%rbp), %rax
	addl	$1, 12(%rax)
.L4:
	movq	%r14, %rdi
	call	uv_rwlock_wrunlock@PLT
.L1:
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	uvwasi__free@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	addl	%edx, %edx
	movq	(%rax), %rsi
	movq	%r15, %rdi
	movl	%edx, -96(%rbp)
	salq	$3, %rdx
	call	uvwasi__realloc@PLT
	movl	-96(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L26
	movq	-56(%rbp), %rax
	movl	8(%rax), %ebx
	cmpl	%ebx, %ecx
	jbe	.L27
	movl	%ecx, %eax
	movl	%ebx, %r15d
	xorl	%esi, %esi
	movl	%ecx, -100(%rbp)
	subl	%ebx, %eax
	leaq	(%r9,%r15,8), %rdi
	movq	%r9, -96(%rbp)
	subl	$1, %eax
	leaq	8(,%rax,8), %rdx
	call	memset@PLT
	movq	-96(%rbp), %r9
	movl	-100(%rbp), %ecx
.L11:
	movq	-56(%rbp), %rax
	movl	%ecx, 8(%rax)
	leaq	(%r9,%r15,8), %rcx
	movq	%r9, (%rax)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$48, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$51, %r12d
	call	uvwasi__free@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movl	%eax, %r12d
	jmp	.L4
.L26:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$48, %r12d
	call	uvwasi__free@PLT
	jmp	.L4
.L27:
	movl	%ebx, %r15d
	jmp	.L11
	.cfi_endproc
.LFE78:
	.size	uvwasi_fd_table_insert, .-uvwasi_fd_table_insert
	.p2align 4
	.type	uvwasi__insert_stdio, @function
uvwasi__insert_stdio:
.LFB77:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-81(%rbp), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uvwasi__get_filetype_by_fd@PLT
	testw	%ax, %ax
	je	.L33
.L28:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L34
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movzbl	-81(%rbp), %edx
	leaq	-72(%rbp), %rcx
	leaq	-64(%rbp), %r8
	movl	%r15d, %edi
	movl	$2, %esi
	call	uvwasi__get_rights@PLT
	testw	%ax, %ax
	jne	.L28
	leaq	-80(%rbp), %rax
	movzbl	-81(%rbp), %r9d
	movq	%r12, %r8
	movq	%r12, %rcx
	pushq	%rax
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	pushq	$0
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	call	uvwasi_fd_table_insert
	addq	$32, %rsp
	testw	%ax, %ax
	jne	.L28
	movq	-80(%rbp), %rdi
	movl	$8, %edx
	cmpl	%ebx, (%rdi)
	cmovne	%edx, %eax
	addq	$64, %rdi
	movl	%eax, -104(%rbp)
	movl	%eax, -100(%rbp)
	call	uv_mutex_unlock@PLT
	movl	-100(%rbp), %eax
	jmp	.L28
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE77:
	.size	uvwasi__insert_stdio, .-uvwasi__insert_stdio
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<stdin>"
.LC1:
	.string	"<stdout>"
.LC2:
	.string	"<stderr>"
	.text
	.p2align 4
	.globl	uvwasi_fd_table_init
	.hidden	uvwasi_fd_table_init
	.type	uvwasi_fd_table_init, @function
uvwasi_fd_table_init:
.LFB79:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$28, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L35
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L35
	cmpl	$2, (%rsi)
	jbe	.L35
	movl	$72, %esi
	movq	%rdi, %r13
	call	uvwasi__malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L48
	movl	(%rbx), %esi
	movl	$0, 12(%rax)
	movl	$8, %edx
	movq	%r13, %rdi
	movl	%esi, 8(%rax)
	call	uvwasi__calloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L56
	leaq	16(%r12), %rax
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	uv_rwlock_init@PLT
	testl	%eax, %eax
	jne	.L57
	movl	40(%rbx), %edx
	leaq	.LC0(%rip), %r8
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	uvwasi__insert_stdio
	movl	%eax, %r15d
	testw	%ax, %ax
	je	.L58
.L39:
	movl	8(%r12), %eax
	movq	(%r12), %r8
	testl	%eax, %eax
	je	.L40
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%ebx, %edx
	movq	(%r8,%rdx,8), %r14
	testq	%r14, %r14
	je	.L41
	leaq	64(%r14), %rdi
	addl	$1, %ebx
	call	uv_mutex_destroy@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	uvwasi__free@PLT
	movl	8(%r12), %eax
	movq	(%r12), %r8
	cmpl	%eax, %ebx
	jb	.L44
.L40:
	testq	%r8, %r8
	je	.L45
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	uvwasi__free@PLT
	movq	$0, (%r12)
	movq	-56(%rbp), %rdi
	movq	$0, 8(%r12)
	call	uv_rwlock_destroy@PLT
.L45:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	uvwasi__free@PLT
.L35:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	44(%rbx), %edx
	leaq	.LC1(%rip), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	call	uvwasi__insert_stdio
	movl	%eax, %r15d
	testw	%ax, %ax
	jne	.L39
	movl	48(%rbx), %edx
	leaq	.LC2(%rip), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$2, %ecx
	call	uvwasi__insert_stdio
	movl	%eax, %r15d
	testw	%ax, %ax
	jne	.L39
	movq	%r12, 0(%r13)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$48, %r15d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L41:
	addl	$1, %ebx
	cmpl	%eax, %ebx
	jb	.L44
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%eax, %edi
	call	uvwasi__translate_uv_error@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	uvwasi__free@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	uvwasi__free@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$48, %r15d
	call	uvwasi__free@PLT
	jmp	.L35
	.cfi_endproc
.LFE79:
	.size	uvwasi_fd_table_init, .-uvwasi_fd_table_init
	.p2align 4
	.globl	uvwasi_fd_table_free
	.hidden	uvwasi_fd_table_free
	.type	uvwasi_fd_table_free, @function
uvwasi_fd_table_free:
.LFB80:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L74
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L59
	movl	8(%rsi), %edx
	movq	%rdi, %r14
	movq	(%rsi), %rsi
	testl	%edx, %edx
	je	.L61
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L65:
	movl	%ebx, %eax
	movq	(%rsi,%rax,8), %r12
	testq	%r12, %r12
	je	.L62
	leaq	64(%r12), %rdi
	addl	$1, %ebx
	call	uv_mutex_destroy@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	movl	8(%r13), %edx
	movq	0(%r13), %rsi
	cmpl	%edx, %ebx
	jb	.L65
.L61:
	testq	%rsi, %rsi
	je	.L66
.L77:
	movq	%r14, %rdi
	call	uvwasi__free@PLT
	movq	$0, 0(%r13)
	leaq	16(%r13), %rdi
	movq	$0, 8(%r13)
	call	uv_rwlock_destroy@PLT
.L66:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rsi
	popq	%r12
	.cfi_restore 12
	movq	%r14, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uvwasi__free@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	addl	$1, %ebx
	cmpl	%edx, %ebx
	jb	.L65
	testq	%rsi, %rsi
	jne	.L77
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L59:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE80:
	.size	uvwasi_fd_table_free, .-uvwasi_fd_table_free
	.p2align 4
	.globl	uvwasi_fd_table_insert_preopen
	.hidden	uvwasi_fd_table_insert_preopen
	.type	uvwasi_fd_table_insert_preopen, @function
uvwasi_fd_table_insert_preopen:
.LFB81:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	sete	%dl
	testq	%r8, %r8
	sete	%al
	orb	%al, %dl
	jne	.L81
	movq	%rsi, %r14
	testq	%rsi, %rsi
	je	.L81
	movq	%rdi, %r13
	leaq	-73(%rbp), %rsi
	movl	%r15d, %edi
	movq	%rcx, %r12
	movq	%r8, %rbx
	call	uvwasi__get_filetype_by_fd@PLT
	testw	%ax, %ax
	jne	.L78
	cmpb	$3, -73(%rbp)
	movl	$54, %eax
	je	.L84
.L78:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L85
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	$28, %eax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%esi, %esi
	leaq	-72(%rbp), %rcx
	leaq	-64(%rbp), %r8
	movl	$3, %edx
	movl	%r15d, %edi
	call	uvwasi__get_rights@PLT
	testw	%ax, %ax
	jne	.L78
	pushq	$0
	movl	$3, %r9d
	movq	%rbx, %r8
	movq	%r12, %rcx
	pushq	$1
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	pushq	$268435455
	pushq	$264240792
	call	uvwasi_fd_table_insert
	addq	$32, %rsp
	jmp	.L78
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE81:
	.size	uvwasi_fd_table_insert_preopen, .-uvwasi_fd_table_insert_preopen
	.p2align 4
	.globl	uvwasi_fd_table_get
	.hidden	uvwasi_fd_table_get
	.type	uvwasi_fd_table_get, @function
uvwasi_fd_table_get:
.LFB82:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	movq	%r8, -64(%rbp)
	testq	%rdi, %rdi
	je	.L89
	leaq	16(%rdi), %r14
	movq	%rdi, %rbx
	movq	%rdx, %r13
	movl	%esi, %r12d
	movq	%r14, %rdi
	movl	$28, %r15d
	call	uv_rwlock_wrlock@PLT
	testq	%r13, %r13
	je	.L88
	movl	$8, %r15d
	cmpl	8(%rbx), %r12d
	jnb	.L88
	movq	(%rbx), %rcx
	movl	%r12d, %eax
	movq	(%rcx,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.L88
	cmpl	(%rbx), %r12d
	jne	.L88
	movq	40(%rbx), %rax
	movl	$76, %r15d
	notq	%rax
	testq	%rax, -56(%rbp)
	jne	.L88
	movq	48(%rbx), %rax
	notq	%rax
	testq	%rax, -64(%rbp)
	jne	.L88
	leaq	64(%rbx), %rdi
	xorl	%r15d, %r15d
	call	uv_mutex_lock@PLT
	movq	%rbx, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r14, %rdi
	call	uv_rwlock_wrunlock@PLT
.L86:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	$28, %r15d
	jmp	.L86
	.cfi_endproc
.LFE82:
	.size	uvwasi_fd_table_get, .-uvwasi_fd_table_get
	.p2align 4
	.globl	uvwasi_fd_table_get_nolock
	.hidden	uvwasi_fd_table_get_nolock
	.type	uvwasi_fd_table_get_nolock, @function
uvwasi_fd_table_get_nolock:
.LFB83:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L107
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L100
	movl	$8, %eax
	cmpl	%esi, 8(%rdi)
	jbe	.L98
	movq	(%rdi), %rdi
	movl	%esi, %edx
	movq	(%rdi,%rdx,8), %r12
	testq	%r12, %r12
	je	.L98
	cmpl	(%r12), %esi
	jne	.L98
	movq	40(%r12), %rdx
	movl	$76, %eax
	notq	%rdx
	testq	%rcx, %rdx
	jne	.L98
	movq	48(%r12), %rdx
	notq	%rdx
	testq	%r8, %rdx
	jne	.L98
	leaq	64(%r12), %rdi
	call	uv_mutex_lock@PLT
	movq	%r12, (%rbx)
	xorl	%eax, %eax
.L98:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	popq	%rbx
	movl	$28, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE83:
	.size	uvwasi_fd_table_get_nolock, .-uvwasi_fd_table_get_nolock
	.p2align 4
	.globl	uvwasi_fd_table_remove_nolock
	.hidden	uvwasi_fd_table_remove_nolock
	.type	uvwasi_fd_table_remove_nolock, @function
uvwasi_fd_table_remove_nolock:
.LFB84:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L113
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	cmpl	%edx, 8(%rsi)
	jbe	.L111
	movq	(%rsi), %rcx
	movl	%edx, %r14d
	movq	(%rcx,%r14,8), %r13
	testq	%r13, %r13
	je	.L111
	cmpl	%edx, 0(%r13)
	je	.L121
.L111:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	64(%r13), %rdi
	call	uv_mutex_destroy@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	uvwasi__free@PLT
	movq	(%rbx), %rax
	movq	$0, (%rax,%r14,8)
	xorl	%eax, %eax
	subl	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE84:
	.size	uvwasi_fd_table_remove_nolock, .-uvwasi_fd_table_remove_nolock
	.p2align 4
	.globl	uvwasi_fd_table_renumber
	.hidden	uvwasi_fd_table_renumber
	.type	uvwasi_fd_table_renumber, @function
uvwasi_fd_table_renumber:
.LFB85:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L127
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L127
	cmpl	%ecx, %edx
	movl	%ecx, -504(%rbp)
	movl	%edx, %ebx
	je	.L122
	leaq	16(%rsi), %r14
	movq	%rdi, %r13
	movq	%r14, %rdi
	call	uv_rwlock_wrlock@PLT
	movl	-504(%rbp), %ecx
	movl	$8, %eax
	cmpl	%ecx, %ebx
	movl	%ecx, %edx
	cmovnb	%ebx, %edx
	cmpl	%edx, 8(%r12)
	ja	.L134
.L124:
	movq	%r14, %rdi
	movl	%eax, -504(%rbp)
	call	uv_rwlock_wrunlock@PLT
	movl	-504(%rbp), %eax
.L122:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L135
	addq	$504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	(%r12), %rdx
	movl	%ebx, %r10d
	movl	%ecx, %r8d
	movq	(%rdx,%r10,8), %r15
	leaq	(%rdx,%r8,8), %rdx
	testq	%r15, %r15
	je	.L124
	movq	(%rdx), %r9
	cmpl	%ebx, (%r15)
	jne	.L131
	testq	%r9, %r9
	je	.L131
	cmpl	%ecx, (%r9)
	jne	.L124
	leaq	64(%r15), %rax
	movq	%r8, -544(%rbp)
	movq	%rax, %rdi
	movq	%r10, -536(%rbp)
	movq	%rax, -512(%rbp)
	movq	%r9, -504(%rbp)
	call	uv_mutex_lock@PLT
	movq	-504(%rbp), %r9
	addq	$64, %r9
	movq	%r9, %rdi
	movq	%r9, -528(%rbp)
	call	uv_mutex_lock@PLT
	leaq	-496(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movl	4(%r15), %edx
	movq	%r11, %rsi
	movq	%r11, -520(%rbp)
	call	uv_fs_close@PLT
	movq	-520(%rbp), %r11
	movl	%eax, -504(%rbp)
	movq	%r11, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	-504(%rbp), %eax
	movq	-528(%rbp), %r9
	movq	-536(%rbp), %r10
	movq	-544(%rbp), %r8
	testl	%eax, %eax
	jne	.L136
	movq	(%r12), %rax
	movq	%r8, -504(%rbp)
	movq	(%rax,%r8,8), %rdi
	movq	%rdi, (%rax,%r10,8)
	addq	$64, %rdi
	movl	%ebx, -64(%rdi)
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	movq	-504(%rbp), %r8
	movq	-512(%rbp), %rbx
	movq	$0, (%rax,%r8,8)
	subl	$1, 12(%r12)
	movq	%rbx, %rdi
	call	uv_mutex_unlock@PLT
	movq	%rbx, %rdi
	call	uv_mutex_destroy@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	uvwasi__free@PLT
	xorl	%eax, %eax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$28, %eax
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$8, %eax
	jmp	.L124
.L136:
	movq	%r9, %rdi
	call	uv_mutex_unlock@PLT
	movq	-512(%rbp), %rdi
	call	uv_mutex_unlock@PLT
	movl	-504(%rbp), %edi
	call	uvwasi__translate_uv_error@PLT
	jmp	.L124
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE85:
	.size	uvwasi_fd_table_renumber, .-uvwasi_fd_table_renumber
	.p2align 4
	.globl	uvwasi_fd_table_lock
	.hidden	uvwasi_fd_table_lock
	.type	uvwasi_fd_table_lock, @function
uvwasi_fd_table_lock:
.LFB86:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L139
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_rwlock_wrlock@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore 6
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE86:
	.size	uvwasi_fd_table_lock, .-uvwasi_fd_table_lock
	.p2align 4
	.globl	uvwasi_fd_table_unlock
	.hidden	uvwasi_fd_table_unlock
	.type	uvwasi_fd_table_unlock, @function
uvwasi_fd_table_unlock:
.LFB87:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_rwlock_wrunlock@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore 6
	movl	$28, %eax
	ret
	.cfi_endproc
.LFE87:
	.size	uvwasi_fd_table_unlock, .-uvwasi_fd_table_unlock
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
