	.file	"wasi_serdes.c"
	.text
	.p2align 4
	.globl	uvwasi_serdes_write_uint64_t
	.hidden	uvwasi_serdes_write_uint64_t
	.type	uvwasi_serdes_write_uint64_t, @function
uvwasi_serdes_write_uint64_t:
.LFB0:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE0:
	.size	uvwasi_serdes_write_uint64_t, .-uvwasi_serdes_write_uint64_t
	.p2align 4
	.globl	uvwasi_serdes_write_uint32_t
	.hidden	uvwasi_serdes_write_uint32_t
	.type	uvwasi_serdes_write_uint32_t, @function
uvwasi_serdes_write_uint32_t:
.LFB1:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	shrl	$16, %edx
	movb	%dl, 2(%rdi,%rsi)
	movb	%dh, 3(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE1:
	.size	uvwasi_serdes_write_uint32_t, .-uvwasi_serdes_write_uint32_t
	.p2align 4
	.globl	uvwasi_serdes_write_uint16_t
	.hidden	uvwasi_serdes_write_uint16_t
	.type	uvwasi_serdes_write_uint16_t, @function
uvwasi_serdes_write_uint16_t:
.LFB2:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE2:
	.size	uvwasi_serdes_write_uint16_t, .-uvwasi_serdes_write_uint16_t
	.p2align 4
	.globl	uvwasi_serdes_write_uint8_t
	.hidden	uvwasi_serdes_write_uint8_t
	.type	uvwasi_serdes_write_uint8_t, @function
uvwasi_serdes_write_uint8_t:
.LFB3:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE3:
	.size	uvwasi_serdes_write_uint8_t, .-uvwasi_serdes_write_uint8_t
	.p2align 4
	.globl	uvwasi_serdes_read_uint64_t
	.hidden	uvwasi_serdes_read_uint64_t
	.type	uvwasi_serdes_read_uint64_t, @function
uvwasi_serdes_read_uint64_t:
.LFB4:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE4:
	.size	uvwasi_serdes_read_uint64_t, .-uvwasi_serdes_read_uint64_t
	.p2align 4
	.globl	uvwasi_serdes_read_uint32_t
	.hidden	uvwasi_serdes_read_uint32_t
	.type	uvwasi_serdes_read_uint32_t, @function
uvwasi_serdes_read_uint32_t:
.LFB5:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE5:
	.size	uvwasi_serdes_read_uint32_t, .-uvwasi_serdes_read_uint32_t
	.p2align 4
	.globl	uvwasi_serdes_read_uint16_t
	.hidden	uvwasi_serdes_read_uint16_t
	.type	uvwasi_serdes_read_uint16_t, @function
uvwasi_serdes_read_uint16_t:
.LFB6:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE6:
	.size	uvwasi_serdes_read_uint16_t, .-uvwasi_serdes_read_uint16_t
	.p2align 4
	.globl	uvwasi_serdes_read_uint8_t
	.hidden	uvwasi_serdes_read_uint8_t
	.type	uvwasi_serdes_read_uint8_t, @function
uvwasi_serdes_read_uint8_t:
.LFB7:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE7:
	.size	uvwasi_serdes_read_uint8_t, .-uvwasi_serdes_read_uint8_t
	.p2align 4
	.globl	uvwasi_serdes_write_advice_t
	.hidden	uvwasi_serdes_write_advice_t
	.type	uvwasi_serdes_write_advice_t, @function
uvwasi_serdes_write_advice_t:
.LFB83:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE83:
	.size	uvwasi_serdes_write_advice_t, .-uvwasi_serdes_write_advice_t
	.p2align 4
	.globl	uvwasi_serdes_write_clockid_t
	.hidden	uvwasi_serdes_write_clockid_t
	.type	uvwasi_serdes_write_clockid_t, @function
uvwasi_serdes_write_clockid_t:
.LFB147:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	shrl	$16, %edx
	movb	%dl, 2(%rdi,%rsi)
	movb	%dh, 3(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE147:
	.size	uvwasi_serdes_write_clockid_t, .-uvwasi_serdes_write_clockid_t
	.p2align 4
	.globl	uvwasi_serdes_write_device_t
	.hidden	uvwasi_serdes_write_device_t
	.type	uvwasi_serdes_write_device_t, @function
uvwasi_serdes_write_device_t:
.LFB10:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE10:
	.size	uvwasi_serdes_write_device_t, .-uvwasi_serdes_write_device_t
	.p2align 4
	.globl	uvwasi_serdes_write_dircookie_t
	.hidden	uvwasi_serdes_write_dircookie_t
	.type	uvwasi_serdes_write_dircookie_t, @function
uvwasi_serdes_write_dircookie_t:
.LFB165:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE165:
	.size	uvwasi_serdes_write_dircookie_t, .-uvwasi_serdes_write_dircookie_t
	.p2align 4
	.globl	uvwasi_serdes_write_errno_t
	.hidden	uvwasi_serdes_write_errno_t
	.type	uvwasi_serdes_write_errno_t, @function
uvwasi_serdes_write_errno_t:
.LFB127:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE127:
	.size	uvwasi_serdes_write_errno_t, .-uvwasi_serdes_write_errno_t
	.p2align 4
	.globl	uvwasi_serdes_write_eventrwflags_t
	.hidden	uvwasi_serdes_write_eventrwflags_t
	.type	uvwasi_serdes_write_eventrwflags_t, @function
uvwasi_serdes_write_eventrwflags_t:
.LFB111:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE111:
	.size	uvwasi_serdes_write_eventrwflags_t, .-uvwasi_serdes_write_eventrwflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_eventtype_t
	.hidden	uvwasi_serdes_write_eventtype_t
	.type	uvwasi_serdes_write_eventtype_t, @function
uvwasi_serdes_write_eventtype_t:
.LFB85:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE85:
	.size	uvwasi_serdes_write_eventtype_t, .-uvwasi_serdes_write_eventtype_t
	.p2align 4
	.globl	uvwasi_serdes_write_exitcode_t
	.hidden	uvwasi_serdes_write_exitcode_t
	.type	uvwasi_serdes_write_exitcode_t, @function
uvwasi_serdes_write_exitcode_t:
.LFB149:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	shrl	$16, %edx
	movb	%dl, 2(%rdi,%rsi)
	movb	%dh, 3(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE149:
	.size	uvwasi_serdes_write_exitcode_t, .-uvwasi_serdes_write_exitcode_t
	.p2align 4
	.globl	uvwasi_serdes_write_fd_t
	.hidden	uvwasi_serdes_write_fd_t
	.type	uvwasi_serdes_write_fd_t, @function
uvwasi_serdes_write_fd_t:
.LFB151:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	shrl	$16, %edx
	movb	%dl, 2(%rdi,%rsi)
	movb	%dh, 3(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE151:
	.size	uvwasi_serdes_write_fd_t, .-uvwasi_serdes_write_fd_t
	.p2align 4
	.globl	uvwasi_serdes_write_fdflags_t
	.hidden	uvwasi_serdes_write_fdflags_t
	.type	uvwasi_serdes_write_fdflags_t, @function
uvwasi_serdes_write_fdflags_t:
.LFB113:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE113:
	.size	uvwasi_serdes_write_fdflags_t, .-uvwasi_serdes_write_fdflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_filesize_t
	.hidden	uvwasi_serdes_write_filesize_t
	.type	uvwasi_serdes_write_filesize_t, @function
uvwasi_serdes_write_filesize_t:
.LFB167:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE167:
	.size	uvwasi_serdes_write_filesize_t, .-uvwasi_serdes_write_filesize_t
	.p2align 4
	.globl	uvwasi_serdes_write_filetype_t
	.hidden	uvwasi_serdes_write_filetype_t
	.type	uvwasi_serdes_write_filetype_t, @function
uvwasi_serdes_write_filetype_t:
.LFB95:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE95:
	.size	uvwasi_serdes_write_filetype_t, .-uvwasi_serdes_write_filetype_t
	.p2align 4
	.globl	uvwasi_serdes_write_fstflags_t
	.hidden	uvwasi_serdes_write_fstflags_t
	.type	uvwasi_serdes_write_fstflags_t, @function
uvwasi_serdes_write_fstflags_t:
.LFB115:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE115:
	.size	uvwasi_serdes_write_fstflags_t, .-uvwasi_serdes_write_fstflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_inode_t
	.hidden	uvwasi_serdes_write_inode_t
	.type	uvwasi_serdes_write_inode_t, @function
uvwasi_serdes_write_inode_t:
.LFB169:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE169:
	.size	uvwasi_serdes_write_inode_t, .-uvwasi_serdes_write_inode_t
	.p2align 4
	.globl	uvwasi_serdes_write_linkcount_t
	.hidden	uvwasi_serdes_write_linkcount_t
	.type	uvwasi_serdes_write_linkcount_t, @function
uvwasi_serdes_write_linkcount_t:
.LFB171:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE171:
	.size	uvwasi_serdes_write_linkcount_t, .-uvwasi_serdes_write_linkcount_t
	.p2align 4
	.globl	uvwasi_serdes_write_lookupflags_t
	.hidden	uvwasi_serdes_write_lookupflags_t
	.type	uvwasi_serdes_write_lookupflags_t, @function
uvwasi_serdes_write_lookupflags_t:
.LFB153:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	shrl	$16, %edx
	movb	%dl, 2(%rdi,%rsi)
	movb	%dh, 3(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE153:
	.size	uvwasi_serdes_write_lookupflags_t, .-uvwasi_serdes_write_lookupflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_oflags_t
	.hidden	uvwasi_serdes_write_oflags_t
	.type	uvwasi_serdes_write_oflags_t, @function
uvwasi_serdes_write_oflags_t:
.LFB117:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE117:
	.size	uvwasi_serdes_write_oflags_t, .-uvwasi_serdes_write_oflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_preopentype_t
	.hidden	uvwasi_serdes_write_preopentype_t
	.type	uvwasi_serdes_write_preopentype_t, @function
uvwasi_serdes_write_preopentype_t:
.LFB87:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE87:
	.size	uvwasi_serdes_write_preopentype_t, .-uvwasi_serdes_write_preopentype_t
	.p2align 4
	.globl	uvwasi_serdes_write_riflags_t
	.hidden	uvwasi_serdes_write_riflags_t
	.type	uvwasi_serdes_write_riflags_t, @function
uvwasi_serdes_write_riflags_t:
.LFB119:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE119:
	.size	uvwasi_serdes_write_riflags_t, .-uvwasi_serdes_write_riflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_rights_t
	.hidden	uvwasi_serdes_write_rights_t
	.type	uvwasi_serdes_write_rights_t, @function
uvwasi_serdes_write_rights_t:
.LFB173:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE173:
	.size	uvwasi_serdes_write_rights_t, .-uvwasi_serdes_write_rights_t
	.p2align 4
	.globl	uvwasi_serdes_write_roflags_t
	.hidden	uvwasi_serdes_write_roflags_t
	.type	uvwasi_serdes_write_roflags_t, @function
uvwasi_serdes_write_roflags_t:
.LFB121:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE121:
	.size	uvwasi_serdes_write_roflags_t, .-uvwasi_serdes_write_roflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_sdflags_t
	.hidden	uvwasi_serdes_write_sdflags_t
	.type	uvwasi_serdes_write_sdflags_t, @function
uvwasi_serdes_write_sdflags_t:
.LFB89:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE89:
	.size	uvwasi_serdes_write_sdflags_t, .-uvwasi_serdes_write_sdflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_siflags_t
	.hidden	uvwasi_serdes_write_siflags_t
	.type	uvwasi_serdes_write_siflags_t, @function
uvwasi_serdes_write_siflags_t:
.LFB123:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE123:
	.size	uvwasi_serdes_write_siflags_t, .-uvwasi_serdes_write_siflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_signal_t
	.hidden	uvwasi_serdes_write_signal_t
	.type	uvwasi_serdes_write_signal_t, @function
uvwasi_serdes_write_signal_t:
.LFB91:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE91:
	.size	uvwasi_serdes_write_signal_t, .-uvwasi_serdes_write_signal_t
	.p2align 4
	.globl	uvwasi_serdes_write_size_t
	.hidden	uvwasi_serdes_write_size_t
	.type	uvwasi_serdes_write_size_t, @function
uvwasi_serdes_write_size_t:
.LFB155:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	shrl	$16, %edx
	movb	%dl, 2(%rdi,%rsi)
	movb	%dh, 3(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE155:
	.size	uvwasi_serdes_write_size_t, .-uvwasi_serdes_write_size_t
	.p2align 4
	.globl	uvwasi_serdes_write_subclockflags_t
	.hidden	uvwasi_serdes_write_subclockflags_t
	.type	uvwasi_serdes_write_subclockflags_t, @function
uvwasi_serdes_write_subclockflags_t:
.LFB125:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	movb	%dh, 1(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE125:
	.size	uvwasi_serdes_write_subclockflags_t, .-uvwasi_serdes_write_subclockflags_t
	.p2align 4
	.globl	uvwasi_serdes_write_timestamp_t
	.hidden	uvwasi_serdes_write_timestamp_t
	.type	uvwasi_serdes_write_timestamp_t, @function
uvwasi_serdes_write_timestamp_t:
.LFB175:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE175:
	.size	uvwasi_serdes_write_timestamp_t, .-uvwasi_serdes_write_timestamp_t
	.p2align 4
	.globl	uvwasi_serdes_write_userdata_t
	.hidden	uvwasi_serdes_write_userdata_t
	.type	uvwasi_serdes_write_userdata_t, @function
uvwasi_serdes_write_userdata_t:
.LFB177:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movb	%dl, (%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 1(%rdi,%rsi)
	movl	%edx, %eax
	shrq	$32, %rdx
	shrl	$16, %eax
	movb	%dl, 4(%rdi,%rsi)
	movb	%al, 2(%rdi,%rsi)
	movb	%ah, 3(%rdi,%rsi)
	movl	%edx, %eax
	shrl	$16, %edx
	shrw	$8, %ax
	movb	%dl, 6(%rdi,%rsi)
	movb	%al, 5(%rdi,%rsi)
	movb	%dh, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE177:
	.size	uvwasi_serdes_write_userdata_t, .-uvwasi_serdes_write_userdata_t
	.p2align 4
	.globl	uvwasi_serdes_write_whence_t
	.hidden	uvwasi_serdes_write_whence_t
	.type	uvwasi_serdes_write_whence_t, @function
uvwasi_serdes_write_whence_t:
.LFB93:
	.cfi_startproc
	endbr64
	movb	%dl, (%rdi,%rsi)
	ret
	.cfi_endproc
.LFE93:
	.size	uvwasi_serdes_write_whence_t, .-uvwasi_serdes_write_whence_t
	.p2align 4
	.globl	uvwasi_serdes_write_fdstat_t
	.hidden	uvwasi_serdes_write_fdstat_t
	.type	uvwasi_serdes_write_fdstat_t, @function
uvwasi_serdes_write_fdstat_t:
.LFB37:
	.cfi_startproc
	endbr64
	movzbl	(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	movzwl	2(%rdx), %eax
	movb	%al, 2(%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 3(%rdi,%rsi)
	movq	8(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 8(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 9(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 12(%rdi,%rsi)
	movb	%cl, 10(%rdi,%rsi)
	movb	%ch, 11(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 14(%rdi,%rsi)
	movb	%cl, 13(%rdi,%rsi)
	movb	%ah, 15(%rdi,%rsi)
	movq	16(%rdx), %rax
	movl	%eax, %edx
	movb	%al, 16(%rdi,%rsi)
	shrw	$8, %dx
	movb	%dl, 17(%rdi,%rsi)
	movl	%eax, %edx
	shrq	$32, %rax
	shrl	$16, %edx
	movb	%al, 20(%rdi,%rsi)
	movb	%dl, 18(%rdi,%rsi)
	movb	%dh, 19(%rdi,%rsi)
	movl	%eax, %edx
	shrl	$16, %eax
	shrw	$8, %dx
	movb	%al, 22(%rdi,%rsi)
	movb	%dl, 21(%rdi,%rsi)
	movb	%ah, 23(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE37:
	.size	uvwasi_serdes_write_fdstat_t, .-uvwasi_serdes_write_fdstat_t
	.p2align 4
	.globl	uvwasi_serdes_write_filestat_t
	.hidden	uvwasi_serdes_write_filestat_t
	.type	uvwasi_serdes_write_filestat_t, @function
uvwasi_serdes_write_filestat_t:
.LFB38:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, (%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 1(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 4(%rdi,%rsi)
	movb	%cl, 2(%rdi,%rsi)
	movb	%ch, 3(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 6(%rdi,%rsi)
	movb	%cl, 5(%rdi,%rsi)
	movb	%ah, 7(%rdi,%rsi)
	movq	8(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 8(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 9(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 12(%rdi,%rsi)
	movb	%cl, 10(%rdi,%rsi)
	movb	%ch, 11(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 14(%rdi,%rsi)
	movb	%cl, 13(%rdi,%rsi)
	movb	%ah, 15(%rdi,%rsi)
	movzbl	16(%rdx), %eax
	movb	%al, 16(%rdi,%rsi)
	movq	24(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 24(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 25(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 28(%rdi,%rsi)
	movb	%cl, 26(%rdi,%rsi)
	movb	%ch, 27(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 30(%rdi,%rsi)
	movb	%cl, 29(%rdi,%rsi)
	movb	%ah, 31(%rdi,%rsi)
	movq	32(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 32(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 33(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%cl, 34(%rdi,%rsi)
	movb	%ch, 35(%rdi,%rsi)
	movl	%eax, %ecx
	movb	%al, 36(%rdi,%rsi)
	shrw	$8, %cx
	shrl	$16, %eax
	movb	%cl, 37(%rdi,%rsi)
	movb	%al, 38(%rdi,%rsi)
	movb	%ah, 39(%rdi,%rsi)
	movq	40(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 40(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 41(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 44(%rdi,%rsi)
	movb	%cl, 42(%rdi,%rsi)
	movb	%ch, 43(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 46(%rdi,%rsi)
	movb	%cl, 45(%rdi,%rsi)
	movb	%ah, 47(%rdi,%rsi)
	movq	48(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 48(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 49(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 52(%rdi,%rsi)
	movb	%cl, 50(%rdi,%rsi)
	movb	%ch, 51(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 54(%rdi,%rsi)
	movb	%cl, 53(%rdi,%rsi)
	movb	%ah, 55(%rdi,%rsi)
	movq	56(%rdx), %rax
	movl	%eax, %edx
	movb	%al, 56(%rdi,%rsi)
	shrw	$8, %dx
	movb	%dl, 57(%rdi,%rsi)
	movl	%eax, %edx
	shrl	$16, %edx
	shrq	$32, %rax
	movb	%dl, 58(%rdi,%rsi)
	movb	%dh, 59(%rdi,%rsi)
	movl	%eax, %edx
	movb	%al, 60(%rdi,%rsi)
	shrw	$8, %dx
	shrl	$16, %eax
	movb	%dl, 61(%rdi,%rsi)
	movb	%al, 62(%rdi,%rsi)
	movb	%ah, 63(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE38:
	.size	uvwasi_serdes_write_filestat_t, .-uvwasi_serdes_write_filestat_t
	.p2align 4
	.globl	uvwasi_serdes_write_prestat_t
	.hidden	uvwasi_serdes_write_prestat_t
	.type	uvwasi_serdes_write_prestat_t, @function
uvwasi_serdes_write_prestat_t:
.LFB39:
	.cfi_startproc
	endbr64
	movzbl	(%rdx), %eax
	movb	%al, (%rdi,%rsi)
	movl	4(%rdx), %eax
	movb	%al, 4(%rdi,%rsi)
	movb	%ah, 5(%rdi,%rsi)
	shrl	$16, %eax
	movb	%al, 6(%rdi,%rsi)
	movb	%ah, 7(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE39:
	.size	uvwasi_serdes_write_prestat_t, .-uvwasi_serdes_write_prestat_t
	.p2align 4
	.globl	uvwasi_serdes_write_event_t
	.hidden	uvwasi_serdes_write_event_t
	.type	uvwasi_serdes_write_event_t, @function
uvwasi_serdes_write_event_t:
.LFB40:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, (%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 1(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 4(%rdi,%rsi)
	movb	%cl, 2(%rdi,%rsi)
	movb	%ch, 3(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 6(%rdi,%rsi)
	movb	%cl, 5(%rdi,%rsi)
	movb	%ah, 7(%rdi,%rsi)
	movzwl	8(%rdx), %eax
	movb	%al, 8(%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 9(%rdi,%rsi)
	movzbl	10(%rdx), %eax
	movb	%al, 10(%rdi,%rsi)
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L44
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	16(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 16(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 17(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 20(%rdi,%rsi)
	movb	%cl, 18(%rdi,%rsi)
	movb	%ch, 19(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 22(%rdi,%rsi)
	movb	%cl, 21(%rdi,%rsi)
	movb	%ah, 23(%rdi,%rsi)
	movzwl	24(%rdx), %eax
	movb	%al, 24(%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 25(%rdi,%rsi)
	ret
	.cfi_endproc
.LFE40:
	.size	uvwasi_serdes_write_event_t, .-uvwasi_serdes_write_event_t
	.p2align 4
	.globl	uvwasi_serdes_write_subscription_t
	.hidden	uvwasi_serdes_write_subscription_t
	.type	uvwasi_serdes_write_subscription_t, @function
uvwasi_serdes_write_subscription_t:
.LFB41:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, (%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 1(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 4(%rdi,%rsi)
	movb	%cl, 2(%rdi,%rsi)
	movb	%ch, 3(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 6(%rdi,%rsi)
	movb	%cl, 5(%rdi,%rsi)
	movb	%ah, 7(%rdi,%rsi)
	movzbl	8(%rdx), %eax
	movb	%al, 8(%rdi,%rsi)
	testb	%al, %al
	je	.L46
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L50
	movl	16(%rdx), %eax
	movb	%al, 16(%rdi,%rsi)
	movb	%ah, 17(%rdi,%rsi)
	shrl	$16, %eax
	movb	%al, 18(%rdi,%rsi)
	movb	%ah, 19(%rdi,%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movl	16(%rdx), %eax
	movb	%al, 16(%rdi,%rsi)
	movb	%ah, 17(%rdi,%rsi)
	shrl	$16, %eax
	movb	%al, 18(%rdi,%rsi)
	movb	%ah, 19(%rdi,%rsi)
	movq	24(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 24(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 25(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 28(%rdi,%rsi)
	movb	%cl, 26(%rdi,%rsi)
	movb	%ch, 27(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 30(%rdi,%rsi)
	movb	%cl, 29(%rdi,%rsi)
	movb	%ah, 31(%rdi,%rsi)
	movq	32(%rdx), %rax
	movl	%eax, %ecx
	movb	%al, 32(%rdi,%rsi)
	shrw	$8, %cx
	movb	%cl, 33(%rdi,%rsi)
	movl	%eax, %ecx
	shrq	$32, %rax
	shrl	$16, %ecx
	movb	%al, 36(%rdi,%rsi)
	movb	%cl, 34(%rdi,%rsi)
	movb	%ch, 35(%rdi,%rsi)
	movl	%eax, %ecx
	shrl	$16, %eax
	shrw	$8, %cx
	movb	%al, 38(%rdi,%rsi)
	movb	%cl, 37(%rdi,%rsi)
	movb	%ah, 39(%rdi,%rsi)
	movzwl	40(%rdx), %eax
	movb	%al, 40(%rdi,%rsi)
	shrw	$8, %ax
	movb	%al, 41(%rdi,%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	ret
	.cfi_endproc
.LFE41:
	.size	uvwasi_serdes_write_subscription_t, .-uvwasi_serdes_write_subscription_t
	.p2align 4
	.globl	uvwasi_serdes_read_advice_t
	.hidden	uvwasi_serdes_read_advice_t
	.type	uvwasi_serdes_read_advice_t, @function
uvwasi_serdes_read_advice_t:
.LFB97:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE97:
	.size	uvwasi_serdes_read_advice_t, .-uvwasi_serdes_read_advice_t
	.p2align 4
	.globl	uvwasi_serdes_read_clockid_t
	.hidden	uvwasi_serdes_read_clockid_t
	.type	uvwasi_serdes_read_clockid_t, @function
uvwasi_serdes_read_clockid_t:
.LFB43:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE43:
	.size	uvwasi_serdes_read_clockid_t, .-uvwasi_serdes_read_clockid_t
	.p2align 4
	.globl	uvwasi_serdes_read_device_t
	.hidden	uvwasi_serdes_read_device_t
	.type	uvwasi_serdes_read_device_t, @function
uvwasi_serdes_read_device_t:
.LFB44:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE44:
	.size	uvwasi_serdes_read_device_t, .-uvwasi_serdes_read_device_t
	.p2align 4
	.globl	uvwasi_serdes_read_dircookie_t
	.hidden	uvwasi_serdes_read_dircookie_t
	.type	uvwasi_serdes_read_dircookie_t, @function
uvwasi_serdes_read_dircookie_t:
.LFB179:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE179:
	.size	uvwasi_serdes_read_dircookie_t, .-uvwasi_serdes_read_dircookie_t
	.p2align 4
	.globl	uvwasi_serdes_read_errno_t
	.hidden	uvwasi_serdes_read_errno_t
	.type	uvwasi_serdes_read_errno_t, @function
uvwasi_serdes_read_errno_t:
.LFB145:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE145:
	.size	uvwasi_serdes_read_errno_t, .-uvwasi_serdes_read_errno_t
	.p2align 4
	.globl	uvwasi_serdes_read_eventrwflags_t
	.hidden	uvwasi_serdes_read_eventrwflags_t
	.type	uvwasi_serdes_read_eventrwflags_t, @function
uvwasi_serdes_read_eventrwflags_t:
.LFB129:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE129:
	.size	uvwasi_serdes_read_eventrwflags_t, .-uvwasi_serdes_read_eventrwflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_eventtype_t
	.hidden	uvwasi_serdes_read_eventtype_t
	.type	uvwasi_serdes_read_eventtype_t, @function
uvwasi_serdes_read_eventtype_t:
.LFB99:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE99:
	.size	uvwasi_serdes_read_eventtype_t, .-uvwasi_serdes_read_eventtype_t
	.p2align 4
	.globl	uvwasi_serdes_read_exitcode_t
	.hidden	uvwasi_serdes_read_exitcode_t
	.type	uvwasi_serdes_read_exitcode_t, @function
uvwasi_serdes_read_exitcode_t:
.LFB157:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE157:
	.size	uvwasi_serdes_read_exitcode_t, .-uvwasi_serdes_read_exitcode_t
	.p2align 4
	.globl	uvwasi_serdes_read_fd_t
	.hidden	uvwasi_serdes_read_fd_t
	.type	uvwasi_serdes_read_fd_t, @function
uvwasi_serdes_read_fd_t:
.LFB159:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE159:
	.size	uvwasi_serdes_read_fd_t, .-uvwasi_serdes_read_fd_t
	.p2align 4
	.globl	uvwasi_serdes_read_fdflags_t
	.hidden	uvwasi_serdes_read_fdflags_t
	.type	uvwasi_serdes_read_fdflags_t, @function
uvwasi_serdes_read_fdflags_t:
.LFB131:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE131:
	.size	uvwasi_serdes_read_fdflags_t, .-uvwasi_serdes_read_fdflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_filesize_t
	.hidden	uvwasi_serdes_read_filesize_t
	.type	uvwasi_serdes_read_filesize_t, @function
uvwasi_serdes_read_filesize_t:
.LFB181:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE181:
	.size	uvwasi_serdes_read_filesize_t, .-uvwasi_serdes_read_filesize_t
	.p2align 4
	.globl	uvwasi_serdes_read_filetype_t
	.hidden	uvwasi_serdes_read_filetype_t
	.type	uvwasi_serdes_read_filetype_t, @function
uvwasi_serdes_read_filetype_t:
.LFB109:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE109:
	.size	uvwasi_serdes_read_filetype_t, .-uvwasi_serdes_read_filetype_t
	.p2align 4
	.globl	uvwasi_serdes_read_fstflags_t
	.hidden	uvwasi_serdes_read_fstflags_t
	.type	uvwasi_serdes_read_fstflags_t, @function
uvwasi_serdes_read_fstflags_t:
.LFB133:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE133:
	.size	uvwasi_serdes_read_fstflags_t, .-uvwasi_serdes_read_fstflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_inode_t
	.hidden	uvwasi_serdes_read_inode_t
	.type	uvwasi_serdes_read_inode_t, @function
uvwasi_serdes_read_inode_t:
.LFB183:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE183:
	.size	uvwasi_serdes_read_inode_t, .-uvwasi_serdes_read_inode_t
	.p2align 4
	.globl	uvwasi_serdes_read_linkcount_t
	.hidden	uvwasi_serdes_read_linkcount_t
	.type	uvwasi_serdes_read_linkcount_t, @function
uvwasi_serdes_read_linkcount_t:
.LFB185:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE185:
	.size	uvwasi_serdes_read_linkcount_t, .-uvwasi_serdes_read_linkcount_t
	.p2align 4
	.globl	uvwasi_serdes_read_lookupflags_t
	.hidden	uvwasi_serdes_read_lookupflags_t
	.type	uvwasi_serdes_read_lookupflags_t, @function
uvwasi_serdes_read_lookupflags_t:
.LFB161:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE161:
	.size	uvwasi_serdes_read_lookupflags_t, .-uvwasi_serdes_read_lookupflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_oflags_t
	.hidden	uvwasi_serdes_read_oflags_t
	.type	uvwasi_serdes_read_oflags_t, @function
uvwasi_serdes_read_oflags_t:
.LFB135:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE135:
	.size	uvwasi_serdes_read_oflags_t, .-uvwasi_serdes_read_oflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_preopentype_t
	.hidden	uvwasi_serdes_read_preopentype_t
	.type	uvwasi_serdes_read_preopentype_t, @function
uvwasi_serdes_read_preopentype_t:
.LFB101:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE101:
	.size	uvwasi_serdes_read_preopentype_t, .-uvwasi_serdes_read_preopentype_t
	.p2align 4
	.globl	uvwasi_serdes_read_riflags_t
	.hidden	uvwasi_serdes_read_riflags_t
	.type	uvwasi_serdes_read_riflags_t, @function
uvwasi_serdes_read_riflags_t:
.LFB137:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE137:
	.size	uvwasi_serdes_read_riflags_t, .-uvwasi_serdes_read_riflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_rights_t
	.hidden	uvwasi_serdes_read_rights_t
	.type	uvwasi_serdes_read_rights_t, @function
uvwasi_serdes_read_rights_t:
.LFB187:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE187:
	.size	uvwasi_serdes_read_rights_t, .-uvwasi_serdes_read_rights_t
	.p2align 4
	.globl	uvwasi_serdes_read_roflags_t
	.hidden	uvwasi_serdes_read_roflags_t
	.type	uvwasi_serdes_read_roflags_t, @function
uvwasi_serdes_read_roflags_t:
.LFB139:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE139:
	.size	uvwasi_serdes_read_roflags_t, .-uvwasi_serdes_read_roflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_sdflags_t
	.hidden	uvwasi_serdes_read_sdflags_t
	.type	uvwasi_serdes_read_sdflags_t, @function
uvwasi_serdes_read_sdflags_t:
.LFB103:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE103:
	.size	uvwasi_serdes_read_sdflags_t, .-uvwasi_serdes_read_sdflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_siflags_t
	.hidden	uvwasi_serdes_read_siflags_t
	.type	uvwasi_serdes_read_siflags_t, @function
uvwasi_serdes_read_siflags_t:
.LFB141:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE141:
	.size	uvwasi_serdes_read_siflags_t, .-uvwasi_serdes_read_siflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_signal_t
	.hidden	uvwasi_serdes_read_signal_t
	.type	uvwasi_serdes_read_signal_t, @function
uvwasi_serdes_read_signal_t:
.LFB105:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE105:
	.size	uvwasi_serdes_read_signal_t, .-uvwasi_serdes_read_signal_t
	.p2align 4
	.globl	uvwasi_serdes_read_size_t
	.hidden	uvwasi_serdes_read_size_t
	.type	uvwasi_serdes_read_size_t, @function
uvwasi_serdes_read_size_t:
.LFB163:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE163:
	.size	uvwasi_serdes_read_size_t, .-uvwasi_serdes_read_size_t
	.p2align 4
	.globl	uvwasi_serdes_read_subclockflags_t
	.hidden	uvwasi_serdes_read_subclockflags_t
	.type	uvwasi_serdes_read_subclockflags_t, @function
uvwasi_serdes_read_subclockflags_t:
.LFB143:
	.cfi_startproc
	endbr64
	movzbl	1(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%rdi,%rsi), %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE143:
	.size	uvwasi_serdes_read_subclockflags_t, .-uvwasi_serdes_read_subclockflags_t
	.p2align 4
	.globl	uvwasi_serdes_read_timestamp_t
	.hidden	uvwasi_serdes_read_timestamp_t
	.type	uvwasi_serdes_read_timestamp_t, @function
uvwasi_serdes_read_timestamp_t:
.LFB189:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE189:
	.size	uvwasi_serdes_read_timestamp_t, .-uvwasi_serdes_read_timestamp_t
	.p2align 4
	.globl	uvwasi_serdes_read_userdata_t
	.hidden	uvwasi_serdes_read_userdata_t
	.type	uvwasi_serdes_read_userdata_t, @function
uvwasi_serdes_read_userdata_t:
.LFB191:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movzbl	3(%rdi,%rsi), %edx
	salq	$32, %rax
	movl	%edx, %ecx
	movzbl	2(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzbl	1(%rdi,%rsi), %ecx
	sall	$16, %edx
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %edx
	orq	%rdx, %rax
	ret
	.cfi_endproc
.LFE191:
	.size	uvwasi_serdes_read_userdata_t, .-uvwasi_serdes_read_userdata_t
	.p2align 4
	.globl	uvwasi_serdes_read_whence_t
	.hidden	uvwasi_serdes_read_whence_t
	.type	uvwasi_serdes_read_whence_t, @function
uvwasi_serdes_read_whence_t:
.LFB107:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	ret
	.cfi_endproc
.LFE107:
	.size	uvwasi_serdes_read_whence_t, .-uvwasi_serdes_read_whence_t
	.p2align 4
	.globl	uvwasi_serdes_read_fdstat_t
	.hidden	uvwasi_serdes_read_fdstat_t
	.type	uvwasi_serdes_read_fdstat_t, @function
uvwasi_serdes_read_fdstat_t:
.LFB71:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movb	%al, (%rdx)
	movzbl	3(%rdi,%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movw	%ax, 2(%r9)
	movzbl	15(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	14(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	13(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	12(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	salq	$32, %rax
	movq	%rax, %rcx
	movzbl	11(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	10(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	9(%rdi,%rsi), %edx
	sall	$16, %eax
	sall	$8, %edx
	movl	%edx, %r8d
	movzbl	8(%rdi,%rsi), %edx
	addq	$16, %rsi
	orl	%r8d, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	orq	%rcx, %rax
	movq	%rax, 8(%r9)
	call	uvwasi_serdes_read_uint64_t
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, 16(%r9)
	ret
	.cfi_endproc
.LFE71:
	.size	uvwasi_serdes_read_fdstat_t, .-uvwasi_serdes_read_fdstat_t
	.p2align 4
	.globl	uvwasi_serdes_read_filestat_t
	.hidden	uvwasi_serdes_read_filestat_t
	.type	uvwasi_serdes_read_filestat_t, @function
uvwasi_serdes_read_filestat_t:
.LFB72:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uvwasi_serdes_read_uint64_t
	movq	%rax, (%r10)
	leaq	8(%rsi), %rsi
	call	uvwasi_serdes_read_uint64_t
	movq	%rax, 8(%r10)
	movzbl	16(%rdi,%r9), %eax
	leaq	24(%r9), %rsi
	movb	%al, 16(%r10)
	call	uvwasi_serdes_read_uint64_t
	movq	%rax, 24(%r10)
	leaq	32(%r9), %rsi
	call	uvwasi_serdes_read_uint64_t
	movq	%rax, 32(%r10)
	leaq	40(%r9), %rsi
	call	uvwasi_serdes_read_uint64_t
	movq	%rax, 40(%r10)
	leaq	48(%r9), %rsi
	call	uvwasi_serdes_read_uint64_t
	movq	%rax, 48(%r10)
	leaq	56(%r9), %rsi
	call	uvwasi_serdes_read_uint64_t
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, 56(%r10)
	ret
	.cfi_endproc
.LFE72:
	.size	uvwasi_serdes_read_filestat_t, .-uvwasi_serdes_read_filestat_t
	.p2align 4
	.globl	uvwasi_serdes_read_prestat_t
	.hidden	uvwasi_serdes_read_prestat_t
	.type	uvwasi_serdes_read_prestat_t, @function
uvwasi_serdes_read_prestat_t:
.LFB73:
	.cfi_startproc
	endbr64
	movzbl	(%rdi,%rsi), %eax
	movb	%al, (%rdx)
	movzbl	7(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %ecx
	movzbl	6(%rdi,%rsi), %eax
	orl	%ecx, %eax
	movzbl	5(%rdi,%rsi), %ecx
	sall	$16, %eax
	sall	$8, %ecx
	movl	%ecx, %r8d
	movzbl	4(%rdi,%rsi), %ecx
	orl	%r8d, %ecx
	movzwl	%cx, %ecx
	orl	%ecx, %eax
	movl	%eax, 4(%rdx)
	ret
	.cfi_endproc
.LFE73:
	.size	uvwasi_serdes_read_prestat_t, .-uvwasi_serdes_read_prestat_t
	.p2align 4
	.globl	uvwasi_serdes_read_event_t
	.hidden	uvwasi_serdes_read_event_t
	.type	uvwasi_serdes_read_event_t, @function
uvwasi_serdes_read_event_t:
.LFB74:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	movq	%rdx, %r10
	movq	%rsi, %r9
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	salq	$32, %rax
	movq	%rax, %rcx
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %esi
	movzbl	(%rdi,%r9), %edx
	sall	$8, %esi
	orl	%esi, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	orq	%rcx, %rax
	movq	%rax, (%r10)
	movzbl	9(%rdi,%r9), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	8(%rdi,%r9), %eax
	orl	%edx, %eax
	movw	%ax, 8(%r10)
	movzbl	10(%rdi,%r9), %eax
	movb	%al, 10(%r10)
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L91
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%r9), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uvwasi_serdes_read_uint64_t
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, 16(%r10)
	movzbl	25(%rdi,%r9), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	24(%rdi,%r9), %eax
	orl	%edx, %eax
	movw	%ax, 24(%r10)
	ret
	.cfi_endproc
.LFE74:
	.size	uvwasi_serdes_read_event_t, .-uvwasi_serdes_read_event_t
	.p2align 4
	.globl	uvwasi_serdes_read_subscription_t
	.hidden	uvwasi_serdes_read_subscription_t
	.type	uvwasi_serdes_read_subscription_t, @function
uvwasi_serdes_read_subscription_t:
.LFB75:
	.cfi_startproc
	endbr64
	movzbl	7(%rdi,%rsi), %eax
	movq	%rdx, %r10
	movq	%rsi, %r9
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	6(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	5(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	4(%rdi,%rsi), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	salq	$32, %rax
	movq	%rax, %rcx
	movzbl	3(%rdi,%rsi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi,%rsi), %eax
	orl	%edx, %eax
	movzbl	1(%rdi,%rsi), %edx
	sall	$16, %eax
	movl	%edx, %esi
	movzbl	(%rdi,%r9), %edx
	sall	$8, %esi
	orl	%esi, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	orq	%rcx, %rax
	movq	%rax, (%r10)
	movzbl	8(%rdi,%r9), %eax
	movb	%al, 8(%r10)
	testb	%al, %al
	je	.L93
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L100
	movzbl	19(%rdi,%r9), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	18(%rdi,%r9), %eax
	orl	%edx, %eax
	movzbl	17(%rdi,%r9), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	16(%rdi,%r9), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movl	%eax, 16(%r10)
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	19(%rdi,%r9), %eax
	leaq	24(%r9), %rsi
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	18(%rdi,%r9), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	orl	%edx, %eax
	movzbl	17(%rdi,%r9), %edx
	sall	$16, %eax
	movl	%edx, %ecx
	movzbl	16(%rdi,%r9), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movl	%eax, 16(%r10)
	call	uvwasi_serdes_read_uint64_t
	movq	%rax, 24(%r10)
	leaq	32(%r9), %rsi
	call	uvwasi_serdes_read_uint64_t
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, 32(%r10)
	movzbl	41(%rdi,%r9), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	40(%rdi,%r9), %eax
	orl	%edx, %eax
	movw	%ax, 40(%r10)
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE75:
	.size	uvwasi_serdes_read_subscription_t, .-uvwasi_serdes_read_subscription_t
	.p2align 4
	.globl	uvwasi_serdes_read_ciovec_t
	.hidden	uvwasi_serdes_read_ciovec_t
	.type	uvwasi_serdes_read_ciovec_t, @function
uvwasi_serdes_read_ciovec_t:
.LFB76:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rdx), %r9d
	movzbl	2(%rdi,%rdx), %r8d
	sall	$8, %r9d
	orl	%r9d, %r8d
	movl	%r8d, %r9d
	movzbl	1(%rdi,%rdx), %r8d
	sall	$16, %r9d
	movl	%r8d, %eax
	movzbl	(%rdi,%rdx), %r8d
	sall	$8, %eax
	orl	%eax, %r8d
	movzbl	7(%rdi,%rdx), %eax
	movzwl	%r8w, %r8d
	sall	$8, %eax
	orl	%r9d, %r8d
	movl	$61, %r9d
	movl	%eax, %r10d
	movzbl	6(%rdi,%rdx), %eax
	orl	%r10d, %eax
	movzbl	5(%rdi,%rdx), %r10d
	movzbl	4(%rdi,%rdx), %edx
	sall	$16, %eax
	sall	$8, %r10d
	orl	%r10d, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movl	%eax, 8(%rcx)
	cmpq	%rsi, %r8
	jnb	.L101
	subq	%r8, %rsi
	cmpq	%rsi, %rax
	ja	.L101
	addq	%r8, %rdi
	xorl	%r9d, %r9d
	movq	%rdi, (%rcx)
.L101:
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE76:
	.size	uvwasi_serdes_read_ciovec_t, .-uvwasi_serdes_read_ciovec_t
	.p2align 4
	.globl	uvwasi_serdes_read_iovec_t
	.hidden	uvwasi_serdes_read_iovec_t
	.type	uvwasi_serdes_read_iovec_t, @function
uvwasi_serdes_read_iovec_t:
.LFB77:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi,%rdx), %r9d
	movzbl	2(%rdi,%rdx), %r8d
	sall	$8, %r9d
	orl	%r9d, %r8d
	movl	%r8d, %r9d
	movzbl	1(%rdi,%rdx), %r8d
	sall	$16, %r9d
	movl	%r8d, %eax
	movzbl	(%rdi,%rdx), %r8d
	sall	$8, %eax
	orl	%eax, %r8d
	movzbl	7(%rdi,%rdx), %eax
	movzwl	%r8w, %r8d
	sall	$8, %eax
	orl	%r9d, %r8d
	movl	$61, %r9d
	movl	%eax, %r10d
	movzbl	6(%rdi,%rdx), %eax
	orl	%r10d, %eax
	movzbl	5(%rdi,%rdx), %r10d
	movzbl	4(%rdi,%rdx), %edx
	sall	$16, %eax
	sall	$8, %r10d
	orl	%r10d, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movl	%eax, 8(%rcx)
	cmpq	%rsi, %r8
	jnb	.L105
	subq	%r8, %rsi
	cmpq	%rsi, %rax
	ja	.L105
	addq	%r8, %rdi
	xorl	%r9d, %r9d
	movq	%rdi, (%rcx)
.L105:
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE77:
	.size	uvwasi_serdes_read_iovec_t, .-uvwasi_serdes_read_iovec_t
	.p2align 4
	.globl	uvwasi_serdes_readv_ciovec_t
	.hidden	uvwasi_serdes_readv_ciovec_t
	.type	uvwasi_serdes_readv_ciovec_t, @function
uvwasi_serdes_readv_ciovec_t:
.LFB78:
	.cfi_startproc
	endbr64
	testl	%r8d, %r8d
	je	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi,%rdx), %rax
	leaq	(%rdi,%rdx), %r9
	leal	-1(%r8), %edx
	leaq	(%rax,%rdx,8), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	.p2align 4,,10
	.p2align 3
.L112:
	movzbl	3(%r9), %edx
	movzbl	1(%r9), %r8d
	movl	%edx, %eax
	movzbl	2(%r9), %edx
	sall	$8, %eax
	orl	%eax, %edx
	movl	%r8d, %eax
	movzbl	(%r9), %r8d
	sall	$8, %eax
	sall	$16, %edx
	orl	%eax, %r8d
	movzbl	7(%r9), %eax
	movzwl	%r8w, %r8d
	sall	$8, %eax
	orl	%r8d, %edx
	movl	%eax, %r11d
	movzbl	6(%r9), %eax
	orl	%r11d, %eax
	movzbl	5(%r9), %r11d
	sall	$16, %eax
	movl	%r11d, %ebx
	movzbl	4(%r9), %r11d
	sall	$8, %ebx
	orl	%ebx, %r11d
	movzwl	%r11w, %r11d
	orl	%r11d, %eax
	movl	%eax, 8(%rcx)
	cmpq	%rdx, %rsi
	jbe	.L114
	movq	%rsi, %r8
	subq	%rdx, %r8
	cmpq	%r8, %rax
	jbe	.L123
.L114:
	movl	$61, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	addq	%rdi, %rdx
	addq	$8, %r9
	addq	$16, %rcx
	movq	%rdx, -16(%rcx)
	cmpq	%r9, %r10
	jne	.L112
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L120:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE78:
	.size	uvwasi_serdes_readv_ciovec_t, .-uvwasi_serdes_readv_ciovec_t
	.p2align 4
	.globl	uvwasi_serdes_readv_iovec_t
	.hidden	uvwasi_serdes_readv_iovec_t
	.type	uvwasi_serdes_readv_iovec_t, @function
uvwasi_serdes_readv_iovec_t:
.LFB79:
	.cfi_startproc
	endbr64
	testl	%r8d, %r8d
	je	.L135
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi,%rdx), %rax
	leaq	(%rdi,%rdx), %r9
	leal	-1(%r8), %edx
	leaq	(%rax,%rdx,8), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	.p2align 4,,10
	.p2align 3
.L127:
	movzbl	3(%r9), %edx
	movzbl	1(%r9), %r8d
	movl	%edx, %eax
	movzbl	2(%r9), %edx
	sall	$8, %eax
	orl	%eax, %edx
	movl	%r8d, %eax
	movzbl	(%r9), %r8d
	sall	$8, %eax
	sall	$16, %edx
	orl	%eax, %r8d
	movzbl	7(%r9), %eax
	movzwl	%r8w, %r8d
	sall	$8, %eax
	orl	%r8d, %edx
	movl	%eax, %r11d
	movzbl	6(%r9), %eax
	orl	%r11d, %eax
	movzbl	5(%r9), %r11d
	sall	$16, %eax
	movl	%r11d, %ebx
	movzbl	4(%r9), %r11d
	sall	$8, %ebx
	orl	%ebx, %r11d
	movzwl	%r11w, %r11d
	orl	%r11d, %eax
	movl	%eax, 8(%rcx)
	cmpq	%rdx, %rsi
	jbe	.L129
	movq	%rsi, %r8
	subq	%rdx, %r8
	cmpq	%r8, %rax
	jbe	.L138
.L129:
	movl	$61, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	addq	%rdi, %rdx
	addq	$8, %r9
	addq	$16, %rcx
	movq	%rdx, -16(%rcx)
	cmpq	%r9, %r10
	jne	.L127
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L135:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE79:
	.size	uvwasi_serdes_readv_iovec_t, .-uvwasi_serdes_readv_iovec_t
	.p2align 4
	.globl	uvwasi_serdes_check_bounds
	.hidden	uvwasi_serdes_check_bounds
	.type	uvwasi_serdes_check_bounds, @function
uvwasi_serdes_check_bounds:
.LFB80:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	jbe	.L139
	subq	%rdi, %rsi
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	setnb	%al
.L139:
	ret
	.cfi_endproc
.LFE80:
	.size	uvwasi_serdes_check_bounds, .-uvwasi_serdes_check_bounds
	.p2align 4
	.globl	uvwasi_serdes_check_array_bounds
	.hidden	uvwasi_serdes_check_array_bounds
	.type	uvwasi_serdes_check_array_bounds, @function
uvwasi_serdes_check_array_bounds:
.LFB81:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	xorl	%r9d, %r9d
	cmpq	%rdi, %rsi
	jbe	.L142
	movq	%rcx, %r10
	imulq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%r8
	cmpq	%rcx, %rax
	je	.L146
.L142:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	subq	%rdi, %rsi
	xorl	%r9d, %r9d
	cmpq	%rsi, %r10
	setbe	%r9b
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE81:
	.size	uvwasi_serdes_check_array_bounds, .-uvwasi_serdes_check_array_bounds
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
