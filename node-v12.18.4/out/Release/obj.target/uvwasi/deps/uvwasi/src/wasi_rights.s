	.file	"wasi_rights.c"
	.text
	.p2align 4
	.globl	uvwasi__get_rights
	.hidden	uvwasi__get_rights
	.type	uvwasi__get_rights, @function
uvwasi__get_rights:
.LFB54:
	.cfi_startproc
	endbr64
	movl	$28, %r9d
	testb	%dl, %dl
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	cmpb	$6, %dl
	ja	.L3
	leaq	.L5(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L3-.L5
	.long	.L9-.L5
	.long	.L8-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.long	.L4-.L5
	.text
.L8:
	movq	%r8, -24(%rbp)
	call	uv_guess_handle@PLT
	movq	-24(%rbp), %r8
	cmpl	$14, %eax
	je	.L20
.L9:
	movq	$536870911, (%r12)
	movq	$536870911, (%r8)
.L10:
	andl	$3, %ebx
	jne	.L12
	andq	$-65, (%r12)
	xorl	%r9d, %r9d
.L1:
	addq	$16, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%r9d, %r9d
	cmpl	$1, %ebx
	jne	.L1
	andq	$-3, (%r12)
	addq	$16, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	%r9d, %eax
	ret
.L3:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	$0, (%r12)
	movq	$0, (%r8)
	jmp	.L10
.L7:
	movq	$264240792, (%r12)
	movq	$268435455, (%r8)
	jmp	.L10
.L6:
	movq	$148898303, (%r12)
	movq	$0, (%r8)
	jmp	.L10
.L4:
	movq	$404750410, (%r12)
	movq	$536870911, (%r8)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L20:
	movq	$136314954, (%r12)
	movq	$0, (%r8)
	jmp	.L10
	.cfi_endproc
.LFE54:
	.size	uvwasi__get_rights, .-uvwasi__get_rights
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
