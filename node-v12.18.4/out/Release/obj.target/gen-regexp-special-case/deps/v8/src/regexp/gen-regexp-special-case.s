	.file	"gen-regexp-special-case.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1338:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE, @function
_GLOBAL__sub_I__ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE:
.LFB3762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE3762:
	.size	_GLOBAL__sub_I__ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE, .-_GLOBAL__sub_I__ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE
	.section	.rodata._ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icu::UnicodeSet "
.LC1:
	.string	"() {\n"
.LC2:
	.string	"  icu::UnicodeSet set;\n"
.LC3:
	.string	"  set.add(0x"
.LC4:
	.string	");\n"
.LC5:
	.string	", 0x"
.LC6:
	.string	"  set.freeze();\n"
.LC7:
	.string	"  return set;\n"
.LC8:
	.string	"}\n"
	.section	.text._ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE
	.type	_ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE, @function
_ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$16, %edx
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r13, %r13
	je	.L14
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L7:
	movl	$5, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %r15
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$4, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
.L13:
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC4(%rip), %rsi
	addl	$1, %ebx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L11:
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%ebx, %eax
	jle	.L8
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	$12, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	cmpl	%eax, %r13d
	jne	.L9
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r14, %rdi
	movl	$16, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rdi
	movl	$14, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r14, %rdi
	movl	$2, %edx
	popq	%rbx
	leaq	.LC8(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L7
	.cfi_endproc
.LFE3148:
	.size	_ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE, .-_ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE
	.section	.rodata._ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"[\\p{Lu}]"
.LC10:
	.string	"U_SUCCESS(status)"
.LC11:
	.string	"Check failed: %s."
.LC12:
	.string	"current.getRangeCount() > 0"
.LC13:
	.string	"BuildIgnoreSet"
.LC14:
	.string	"BuildSpecialAddSet"
	.section	.text._ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE
	.type	_ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE, @function
_ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$128, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-1296(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1088(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-256(%rbp), %rbx
	subq	$1320, %rsp
	movq	%rdi, -1352(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	$56319, %edx
	movl	$55296, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	leaq	-880(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1336(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	-672(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1344(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	movl	$0, -1300(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKc@PLT
	leaq	-464(%rbp), %rax
	leaq	-1300(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-1300(%rbp), %eax
	testl	%eax, %eax
	jle	.L16
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L17:
	addl	$1, %r15d
	cmpl	$65536, %r15d
	je	.L35
.L16:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L17
	movl	%r15d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6710UnicodeSet3setEii@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	movq	-1328(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	movq	%rbx, %rdi
	movl	$0, -1316(%rbp)
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%r14d, %eax
	jle	.L18
.L36:
	cmpl	$2, %r14d
	je	.L18
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	addl	$1, %r14d
	movl	%eax, -1320(%rbp)
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	-1320(%rbp), %ecx
	movl	-1316(%rbp), %edx
	movq	%rbx, %rdi
	subl	%eax, %ecx
	leal	1(%rdx,%rcx), %eax
	movl	%eax, -1316(%rbp)
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%r14d, %eax
	jg	.L36
.L18:
	cmpl	$1, -1316(%rbp)
	jle	.L20
	movq	-1328(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	movq	-1336(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	testl	%eax, %eax
	jle	.L37
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%eax, %edi
	call	u_toupper_67@PLT
	movq	-1336(%rbp), %rdi
	movl	%eax, %r14d
	movl	%eax, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEi@PLT
	movq	-1344(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
.L20:
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	cmpl	$65536, %r15d
	jne	.L16
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-1336(%rbp), %rbx
	xorl	%esi, %esi
	movl	$127, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movq	-1344(%rbp), %r15
	movq	-1352(%rbp), %r14
	leaq	.LC13(%rip), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE
	movq	%rbx, %rdx
	leaq	.LC14(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8PrintSetERSt14basic_ofstreamIcSt11char_traitsIcEEPKcRKN6icu_6710UnicodeSetE
	movq	-1328(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$1320, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	leaq	.LC12(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L34:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3149:
	.size	_ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE, .-_ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE
	.section	.rodata._ZN2v88internal11WriteHeaderEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"// Automatically generated by regexp/gen-regexp-special-case.cc\n"
	.align 8
.LC16:
	.string	"// The following functions are used to build icu::UnicodeSet\n"
	.align 8
.LC17:
	.string	"// for specical cases different between Unicode and ECMA262.\n"
	.section	.rodata._ZN2v88internal11WriteHeaderEPKc.str1.1,"aMS",@progbits,1
.LC18:
	.string	"#ifdef V8_INTL_SUPPORT\n"
	.section	.rodata._ZN2v88internal11WriteHeaderEPKc.str1.8
	.align 8
.LC19:
	.string	"#include \"src/regexp/special-case.h\"\n\n"
	.section	.rodata._ZN2v88internal11WriteHeaderEPKc.str1.1
.LC20:
	.string	"#include \"unicode/uniset.h\"\n"
.LC21:
	.string	"namespace v8 {\n"
.LC22:
	.string	"namespace internal {\n\n"
.LC23:
	.string	"\n"
.LC24:
	.string	"}  // namespace internal\n"
.LC25:
	.string	"}  // namespace v8\n"
.LC26:
	.string	"#endif  // V8_INTL_SUPPORT\n"
	.section	.text._ZN2v88internal11WriteHeaderEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11WriteHeaderEPKc
	.type	_ZN2v88internal11WriteHeaderEPKc, @function
_ZN2v88internal11WriteHeaderEPKc:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-328(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-576(%rbp), %r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -592(%rbp)
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rax, -328(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	%rbx, -576(%rbp)
	movq	$0, -112(%rbp)
	movw	%ax, -104(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	leaq	40(%rax), %r15
	movq	%rax, -576(%rbp)
	movq	%r15, -328(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-592(%rbp), %r8
	movq	%r13, %rdi
	movl	$16, %edx
	movq	%r8, %rsi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	%r12, %rdi
	testq	%rax, %rax
	movq	-576(%rbp), %rax
	je	.L48
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L41:
	movq	-576(%rbp), %rdx
	movq	-24(%rdx), %rcx
	addq	%r12, %rcx
	movl	24(%rcx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rcx)
	movq	-24(%rdx), %rcx
	addq	%r12, %rcx
	cmpb	$0, 225(%rcx)
	je	.L49
.L42:
	movb	$48, 224(%rcx)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r12, %rdi
	movq	.LC27(%rip), %xmm0
	movq	%rax, %xmm1
	movq	-24(%rdx), %rax
	leaq	.LC15(%rip), %rsi
	movl	$64, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -592(%rbp)
	movq	$4, -560(%rbp,%rax)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$61, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$61, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$38, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$28, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$22, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal12PrintSpecialERSt14basic_ofstreamIcSt11char_traitsIcEE
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$25, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$19, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$27, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movdqa	-592(%rbp), %xmm0
	movq	%r13, %rdi
	movq	%r15, -328(%rbp)
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	-464(%rbp), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-512(%rbp), %rdi
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -576(%rbp)
	movq	-24(%rbx), %rax
	movq	%r14, %rdi
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rbx
	movq	%rbx, -576(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -328(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	240(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L51
	cmpb	$0, 56(%rdi)
	je	.L52
.L44:
	movb	$1, 225(%rcx)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L48:
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rcx, -600(%rbp)
	movq	%rdi, -592(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-592(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-600(%rbp), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L45
	movb	$1, 225(%rcx)
	movq	-576(%rbp), %rdx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rcx, -592(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-576(%rbp), %rdx
	movq	-592(%rbp), %rcx
	jmp	.L44
.L50:
	call	__stack_chk_fail@PLT
.L51:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE3150:
	.size	_ZN2v88internal11WriteHeaderEPKc, .-_ZN2v88internal11WriteHeaderEPKc
	.section	.rodata.main.str1.1,"aMS",@progbits,1
.LC28:
	.string	"Usage: "
.LC29:
	.string	" <output filename>\n"
	.section	.text.startup.main,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB3151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpl	$2, %edi
	jne	.L56
	movq	8(%rsi), %rdi
	call	_ZN2v88internal11WriteHeaderEPKc
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	movl	$7, %edx
	leaq	.LC28(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	.LC29(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$1, %edi
	call	exit@PLT
	.cfi_endproc
.LFE3151:
	.size	main, .-main
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC27:
	.quad	_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE+24
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
