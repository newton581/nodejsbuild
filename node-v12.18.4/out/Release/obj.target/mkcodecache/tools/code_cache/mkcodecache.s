	.file	"mkcodecache.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"--random_seed=42"
.LC1:
	.string	"Usage: "
.LC2:
	.string	" <path/to/output.cc>\n"
.LC3:
	.string	"Cannot open "
.LC4:
	.string	"\n"
.LC5:
	.string	"--random_seed=0"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB6207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	leaq	.LC0(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$728, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v82V818SetFlagsFromStringEPKc@PLT
	cmpl	$1, %r12d
	jle	.L22
	leaq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev@PLT
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	movl	$20, %edx
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	leaq	-464(%rbp), %rdi
	call	_ZNKSt12__basic_fileIcE7is_openEv@PLT
	testb	%al, %al
	je	.L23
	xorl	%esi, %esi
	leaq	_ZN4node11per_process18enabled_debug_listE(%rip), %rdi
	leaq	-720(%rbp), %r13
	call	_ZN4node16EnabledDebugList5ParseEPNS_11EnvironmentE@PLT
	leaq	-760(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, -720(%rbp)
	movq	%r13, %r8
	xorl	%esi, %esi
	call	_ZN2v88platform18NewDefaultPlatformEiNS0_15IdleTaskSupportENS0_21InProcessStackDumpingESt10unique_ptrINS_17TracingControllerESt14default_deleteIS4_EE@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	call	*8(%rax)
.L6:
	movq	-760(%rbp), %rdi
	leaq	-752(%rbp), %rbx
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	leaq	-712(%rbp), %rdi
	movq	$0, -720(%rbp)
	call	_ZN2v819ResourceConstraintsC1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -672(%rbp)
	movups	%xmm0, -664(%rbp)
	movaps	%xmm0, -640(%rbp)
	movq	$0, -648(%rbp)
	movw	$1, -624(%rbp)
	call	_ZN2v811ArrayBuffer9Allocator19NewDefaultAllocatorEv@PLT
	movq	%r13, %rdi
	movq	%rax, -640(%rbp)
	movq	%rax, %r14
	call	_ZN2v87Isolate3NewERKNS0_12CreateParamsE@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	%rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	pushq	$0
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v87Context5EnterEv@PLT
	leaq	.LC5(%rip), %rdi
	call	_ZN2v82V818SetFlagsFromStringEPKc@PLT
	leaq	-608(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE@PLT
	movq	-600(%rbp), %rdx
	movq	-608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE5closeEv@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	testq	%r14, %r14
	je	.L8
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L8:
	movq	-760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
.L9:
	xorl	%r13d, %r13d
.L5:
	movq	%r12, %rdi
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	movl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	jmp	.L5
.L22:
	movl	$7, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	movl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	jmp	.L1
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6207:
	.size	main, .-main
	.p2align 4
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7653:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
