	.file	"cache_builder.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.text
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB7804:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L2
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L2:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L4
.L24:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L24
.L4:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L6
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L7:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L8
	testb	$4, %al
	jne	.L26
	testl	%eax, %eax
	je	.L9
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L27
.L9:
	movq	(%r12), %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L9
	andl	$-8, %eax
	xorl	%edx, %edx
.L12:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L12
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L9
.L27:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L9
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7804:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB7779:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L29
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L30
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L30:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L31
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L32
.L31:
	testq	%rbx, %rbx
	je	.L32
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L32:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L33
	call	__stack_chk_fail@PLT
.L29:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L33:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7779:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata.str1.1
.LC2:
	.string	"%.2fB"
.LC3:
	.string	"%.2fKB"
.LC4:
	.string	"%.2fMB"
	.text
	.p2align 4
	.type	_ZN4node13native_moduleL10FormatSizeEm, @function
_ZN4node13native_moduleL10FormatSizeEm:
.LFB6237:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	cmpq	$1023, %rsi
	jbe	.L70
	cmpq	$1048575, %rsi
	jbe	.L71
	shrq	$20, %rsi
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %r13
	cvtsi2sdq	%rsi, %xmm0
	leaq	.LC4(%rip), %r8
.L69:
	movl	$64, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r13, %rbx
	movl	$64, %esi
	movl	$1, %eax
	call	__snprintf_chk@PLT
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
.L43:
	movl	(%rbx), %ecx
	addq	$4, %rbx
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L43
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rbx), %rcx
	cmove	%rcx, %rbx
	movl	%edx, %edi
	addb	%dl, %dil
	sbbq	$3, %rbx
	subq	%r13, %rbx
	movq	%rbx, -120(%rbp)
	cmpq	$15, %rbx
	ja	.L72
	cmpq	$1, %rbx
	jne	.L47
	movzbl	-112(%rbp), %edx
	movb	%dl, 16(%r12)
.L48:
	movq	%rbx, 8(%r12)
	movb	$0, (%rax,%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	shrq	$10, %rsi
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %r13
	cvtsi2sdq	%rsi, %xmm0
	leaq	.LC3(%rip), %r8
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L47:
	testq	%rbx, %rbx
	je	.L48
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L70:
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %r13
	leaq	.LC2(%rip), %r8
	cvtsi2sdq	%rsi, %xmm0
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%edx, %edx
	leaq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
.L46:
	movl	%ebx, %edx
	cmpl	$8, %ebx
	jnb	.L49
	andl	$4, %ebx
	jne	.L74
	testl	%edx, %edx
	je	.L50
	movzbl	0(%r13), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	jne	.L75
.L50:
	movq	-120(%rbp), %rbx
	movq	(%r12), %rax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-112(%rbp), %rdx
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rsi, %rax
	leal	(%rbx,%rax), %edx
	subq	%rax, %r13
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L50
	andl	$-8, %edx
	xorl	%eax, %eax
.L53:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	0(%r13,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L53
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L74:
	movl	0(%r13), %ecx
	movl	%ecx, (%rax)
	movl	-4(%r13,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L50
.L75:
	movzwl	-2(%r13,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L50
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6237:
	.size	_ZN4node13native_moduleL10FormatSizeEm, .-_ZN4node13native_moduleL10FormatSizeEm
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L76
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L78
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L80
.L76:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L80
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB6032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L83
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L112
	cmpq	$1, %rax
	jne	.L86
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L87:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L82:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L87
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L85:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L83:
	cmpb	$37, 1(%rax)
	jne	.L114
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L115
	cmpq	$1, %r13
	jne	.L92
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L93:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L95
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L116
.L95:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L97:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L117
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L99:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L82
	call	_ZdlPv@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L92:
	testq	%r13, %r13
	jne	.L118
	movq	%rbx, %rax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L91:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L117:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L97
.L114:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L113:
	call	__stack_chk_fail@PLT
.L118:
	movq	%rbx, %rdi
	jmp	.L91
	.cfi_endproc
.LFE6032:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB6655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L126
	movq	16(%rdi), %r10
.L120:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L121
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L127
	movq	16(%r9), %r10
.L122:
	cmpq	%r10, %rax
	jbe	.L129
.L121:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L123:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L130
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L125:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L130:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$15, %r10d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L127:
	movl	$15, %r10d
	jmp	.L122
	.cfi_endproc
.LFE6655:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E:
.LFB7145:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L135:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L133
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L131
.L134:
	movq	%rbx, %r12
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L134
.L131:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE7145:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_:
.LFB7151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$72, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%r15), %r14
	movq	8(%r15), %r15
	movq	%rax, %r12
	leaq	32(%rax), %rdi
	addq	$48, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, 32(%r12)
	movq	%r14, %rax
	addq	%r15, %rax
	setne	%dl
	testq	%r14, %r14
	sete	%al
	testb	%al, %dl
	jne	.L195
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L196
	cmpq	$1, %r15
	jne	.L153
	movzbl	(%r14), %eax
	movb	%al, 48(%r12)
	movq	-88(%rbp), %rax
.L154:
	movq	%r15, 40(%r12)
	movb	$0, (%rax,%r15)
	movq	(%rbx), %rax
	movq	16(%r13), %rbx
	movq	%rax, 64(%r12)
	leaq	8(%r13), %rax
	movq	%rax, -72(%rbp)
	testq	%rbx, %rbx
	je	.L155
	movq	40(%r12), %r14
	movq	32(%r12), %r15
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	movq	%r15, %r12
	movq	%r14, %r15
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L161:
	movq	16(%rbx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L157
.L197:
	movq	%rax, %rbx
.L156:
	movq	40(%rbx), %r14
	movq	32(%rbx), %r11
	cmpq	%r14, %r15
	movq	%r14, %r13
	cmovbe	%r15, %r13
	testq	%r13, %r13
	je	.L158
	movq	%r11, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r11, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %r11
	testl	%eax, %eax
	jne	.L159
.L158:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L160
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L161
.L159:
	testl	%eax, %eax
	js	.L161
.L160:
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L197
.L157:
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	-104(%rbp), %r13
	movq	%r15, %r14
	movq	%rbx, %r9
	movq	%r12, %r15
	movq	-96(%rbp), %r12
	testb	%sil, %sil
	jne	.L198
.L164:
	testq	%rdx, %rdx
	je	.L165
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r9, -96(%rbp)
	movq	%rcx, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	jne	.L166
.L165:
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L167
	cmpq	$-2147483648, %rcx
	jl	.L168
	movl	%ecx, %eax
.L166:
	testl	%eax, %eax
	js	.L168
.L167:
	cmpq	%r15, -88(%rbp)
	je	.L174
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L174:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
.L173:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L199
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L200
	movq	-88(%rbp), %rax
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L168:
	testq	%r9, %r9
	je	.L201
.L169:
	movl	$1, %edi
	cmpq	%r9, -72(%rbp)
	jne	.L202
.L170:
	movq	-72(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	movq	%r12, %rax
	movl	$1, %edx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L198:
	cmpq	%rbx, 24(%r13)
	je	.L203
.L175:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r14, %rdx
	movq	%rbx, %r9
	movq	40(%rax), %rcx
	movq	32(%rax), %r11
	movq	%rax, %rbx
	cmpq	%r14, %rcx
	cmovbe	%rcx, %rdx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L152:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	32(%r12), %rax
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L202:
	movq	40(%r9), %rbx
	cmpq	%rbx, %r14
	movq	%rbx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L171
	movq	32(%r9), %rsi
	movq	%r15, %rdi
	movq	%r9, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L172
.L171:
	movq	%r14, %r8
	xorl	%edi, %edi
	subq	%rbx, %r8
	cmpq	$2147483647, %r8
	jg	.L170
	cmpq	$-2147483648, %r8
	jl	.L194
	movl	%r8d, %edi
.L172:
	shrl	$31, %edi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	8(%r13), %rax
	cmpq	24(%r13), %rax
	jne	.L204
	leaq	8(%r13), %r9
.L194:
	movl	$1, %edi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%rbx, %r9
	jmp	.L169
.L199:
	call	__stack_chk_fail@PLT
.L195:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L204:
	movq	32(%r12), %r15
	movq	40(%r12), %r14
	movq	%rax, %rbx
	jmp	.L175
.L200:
	movq	-88(%rbp), %rdi
	jmp	.L152
.L201:
	xorl	%ebx, %ebx
	jmp	.L167
	.cfi_endproc
.LFE7151:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB7390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L206
	testq	%rsi, %rsi
	je	.L222
.L206:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L223
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L209
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L210:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L210
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L208:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L210
.L222:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7390:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB7472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L226
	testq	%r14, %r14
	je	.L242
.L226:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L243
	cmpq	$1, %r13
	jne	.L229
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L230:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L230
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L228:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L230
.L242:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7472:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB7478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L249
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L247:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L247
	.cfi_endproc
.LFE7478:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.rodata._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(null)"
.LC6:
	.string	"lz"
.LC7:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB7692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L251
	leaq	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L251:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC6(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-184(%rbp), %r9
.L252:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L252
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L253
	cmpb	$99, %dl
	jg	.L254
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L255
	cmpb	$88, %dl
	je	.L256
	jmp	.L253
.L254:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L253
	leaq	.L258(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L258:
	.long	.L257-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L257-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L257-.L258
	.long	.L260-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L257-.L258
	.long	.L253-.L258
	.long	.L257-.L258
	.long	.L253-.L258
	.long	.L253-.L258
	.long	.L257-.L258
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L255:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L292
	jmp	.L289
.L253:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L289
.L292:
	call	_ZdlPv@PLT
	jmp	.L289
.L257:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L272
	leaq	.LC5(%rip), %rsi
.L272:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L284
	jmp	.L269
.L256:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L274
	leaq	.LC5(%rip), %rsi
.L274:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L269
.L284:
	call	_ZdlPv@PLT
	jmp	.L269
.L260:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC7(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L277
	leaq	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L277:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L269:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L289:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L250
	call	_ZdlPv@PLT
.L250:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L280
	call	__stack_chk_fail@PLT
.L280:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7692:
	.size	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB7664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L294
	leaq	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L294:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC6(%rip), %rbx
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %r9
.L295:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -216(%rbp)
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	movq	-216(%rbp), %r8
	jne	.L295
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L296
	cmpb	$99, %dl
	jg	.L297
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L298
	cmpb	$88, %dl
	je	.L299
	jmp	.L296
.L297:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L296
	leaq	.L301(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L301:
	.long	.L300-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L300-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L300-.L301
	.long	.L303-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L300-.L301
	.long	.L296-.L301
	.long	.L300-.L301
	.long	.L296-.L301
	.long	.L296-.L301
	.long	.L300-.L301
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L298:
	movq	-184(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L332
	jmp	.L308
.L296:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	leaq	-144(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L308
.L332:
	call	_ZdlPv@PLT
.L308:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L328
	jmp	.L307
.L300:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L315
	leaq	.LC5(%rip), %rsi
.L315:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L327
	jmp	.L312
.L299:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L317
	leaq	.LC5(%rip), %rsi
.L317:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L318
	call	_ZdlPv@PLT
.L318:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L312
.L327:
	call	_ZdlPv@PLT
	jmp	.L312
.L303:
	movq	(%r8), %r9
	leaq	-80(%rbp), %rbx
	xorl	%eax, %eax
	leaq	.LC7(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%rbx, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L320
	leaq	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L320:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L312:
	movq	-184(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L307
.L328:
	call	_ZdlPv@PLT
.L307:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L323
	call	__stack_chk_fail@PLT
.L323:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7664:
	.size	_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB7613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L335
	leaq	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L335:
	movq	%rax, %r10
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	movq	%r9, -216(%rbp)
	leaq	.LC6(%rip), %rbx
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-216(%rbp), %r9
	movq	-184(%rbp), %r10
	movq	%r13, -216(%rbp)
	movq	%r10, %r15
	movq	%r9, %r12
.L336:
	movq	%r15, %rax
	movq	%rbx, %rdi
	movq	%r15, -184(%rbp)
	leaq	1(%r15), %r15
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L336
	movl	%r13d, %edx
	movq	%r12, %r9
	movq	-216(%rbp), %r13
	movq	%r15, %r10
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L337
	cmpb	$99, %dl
	jg	.L338
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r15
	movq	%rax, -216(%rbp)
	je	.L339
	cmpb	$88, %dl
	je	.L340
	jmp	.L337
.L338:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L337
	leaq	.L342(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L342:
	.long	.L341-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L341-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L341-.L342
	.long	.L344-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L341-.L342
	.long	.L337-.L342
	.long	.L341-.L342
	.long	.L337-.L342
	.long	.L337-.L342
	.long	.L341-.L342
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L339:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L373
	jmp	.L349
.L337:
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L349
.L373:
	call	_ZdlPv@PLT
.L349:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L369
	jmp	.L348
.L341:
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L356
	leaq	.LC5(%rip), %rsi
.L356:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L368
	jmp	.L353
.L340:
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L358
	leaq	.LC5(%rip), %rsi
.L358:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L353
.L368:
	call	_ZdlPv@PLT
	jmp	.L353
.L344:
	leaq	-80(%rbp), %r15
	movq	(%r9), %r9
	xorl	%eax, %eax
	leaq	.LC7(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r15, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L361
	leaq	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L361:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L353:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %rcx
	movq	%r12, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L348
.L369:
	call	_ZdlPv@PLT
.L348:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L364
	call	__stack_chk_fail@PLT
.L364:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7613:
	.size	_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB7486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L376
	call	__stack_chk_fail@PLT
.L376:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7486:
	.size	_ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_:
.LFB7343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJPKcS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L378
	call	_ZdlPv@PLT
.L378:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L380
	call	__stack_chk_fail@PLT
.L380:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7343:
	.size	_ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"#include <cinttypes>\n#include \"node_native_module_env.h\"\n\n// This file is generated by mkcodecache (tools/code_cache/mkcodecache.cc)\n\nnamespace node {\nnamespace native_module {\n\nconst bool has_code_cache = true;\n\n"
	.align 8
.LC9:
	.string	"void NativeModuleEnv::InitializeCodeCache() {\n  NativeModuleCacheMap& code_cache =\n      *NativeModuleLoader::GetInstance()->code_cache();\n  CHECK(code_cache.empty());\n  auto policy = v8::ScriptCompiler::CachedData::BufferPolicy::BufferNotOwned;\n"
	.section	.rodata.str1.1
.LC10:
	.string	"static const uint8_t "
.LC14:
	.string	"[] = {\n"
.LC15:
	.string	"};"
.LC16:
	.string	"\n\n"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Generated cache for %s, size = %s, total = %s\n"
	.align 8
.LC18:
	.string	"\n}\n\n}  // namespace native_module\n}  // namespace node\n"
	.section	.rodata.str1.1
.LC19:
	.string	"  code_cache.emplace(\n"
.LC20:
	.string	"    \""
.LC21:
	.string	"\",\n"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"    std::make_unique<v8::ScriptCompiler::CachedData>(\n"
	.section	.rodata.str1.1
.LC23:
	.string	"      "
.LC24:
	.string	",\n"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"      static_cast<int>(arraysize("
	.section	.rodata.str1.1
.LC26:
	.string	")), policy\n"
.LC27:
	.string	"    )\n"
.LC28:
	.string	"  );"
	.section	.text.unlikely
.LCOLDB32:
	.text
.LHOTB32:
	.p2align 4
	.type	_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE, @function
_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE:
.LFB6240:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-976(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-864(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-992(%rbp), %rbx
	subq	$1224, %rsp
	movq	%rdi, -1232(%rbp)
	movq	.LC29(%rip), %xmm5
	movq	%r12, %rdi
	movq	%rsi, -1240(%rbp)
	movhps	.LC30(%rip), %xmm5
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm5, -1216(%rbp)
	movq	%r12, -1264(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -640(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -632(%rbp)
	movups	%xmm0, -616(%rbp)
	movq	%rax, -864(%rbp)
	movq	-24(%r14), %rax
	movq	$0, -648(%rbp)
	movq	%r14, -992(%rbp)
	movq	%rcx, -992(%rbp,%rax)
	movq	$0, -984(%rbp)
	addq	-24(%r14), %rbx
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -976(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r15, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	movdqa	-1216(%rbp), %xmm5
	movq	-24(%rax), %rax
	movq	%rbx, -992(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -992(%rbp)
	addq	$80, %rax
	movq	%rax, -864(%rbp)
	leaq	-912(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm5, -976(%rbp)
	movaps	%xmm0, -960(%rbp)
	movaps	%xmm0, -944(%rbp)
	movaps	%xmm0, -928(%rbp)
	movq	%rax, -1248(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-968(%rbp), %rsi
	movq	%rax, -968(%rbp)
	leaq	-880(%rbp), %rax
	movq	%rax, -1256(%rbp)
	movq	%rax, -896(%rbp)
	movl	$24, -904(%rbp)
	movq	$0, -888(%rbp)
	movb	$0, -880(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$213, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rax
	movq	24(%r13), %r13
	addq	$8, %rax
	movq	%rax, -1192(%rbp)
	cmpq	%r13, %rax
	je	.L437
	leaq	-592(%rbp), %rax
	movq	%r15, -1224(%rbp)
	leaq	-576(%rbp), %r12
	movq	%r13, %r15
	movq	%rax, -1152(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -1176(%rbp)
	leaq	-512(%rbp), %rax
	movq	%rax, -1160(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, -1168(%rbp)
	leaq	-568(%rbp), %rax
	movq	$0, -1144(%rbp)
	movq	%rax, -1200(%rbp)
	.p2align 4,,10
	.p2align 3
.L383:
	movq	64(%r15), %rax
	movq	-1176(%rbp), %rbx
	movslq	8(%rax), %rcx
	movq	%rax, -1184(%rbp)
	movq	%rbx, %rdi
	movq	(%rax), %rax
	addq	%rcx, -1144(%rbp)
	movq	%rcx, -1096(%rbp)
	movq	%rax, -1120(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm2, %xmm2
	xorl	%esi, %esi
	movq	%rax, -464(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm2, -232(%rbp)
	movups	%xmm2, -216(%rbp)
	movw	%ax, -240(%rbp)
	movq	-24(%r14), %rax
	movq	$0, -248(%rbp)
	movq	%r14, -592(%rbp)
	movq	%rcx, -592(%rbp,%rax)
	movq	-1152(%rbp), %rax
	movq	$0, -584(%rbp)
	addq	-24(%r14), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	addq	%r12, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm2, %xmm2
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-1216(%rbp), %xmm3
	movq	-1160(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -592(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -592(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -576(%rbp)
	movaps	%xmm2, -560(%rbp)
	movaps	%xmm2, -544(%rbp)
	movaps	%xmm2, -528(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, %rdi
	movq	-1200(%rbp), %rsi
	movq	%rax, -568(%rbp)
	movq	-1168(%rbp), %rax
	movl	$24, -504(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -488(%rbp)
	movb	$0, -480(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$21, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r15), %rdx
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	cmpq	$63, %rdx
	ja	.L440
	testq	%rdx, %rdx
	je	.L394
	leaq	-1(%rdx), %rax
	movq	32(%r15), %rcx
	cmpq	$14, %rax
	jbe	.L481
	movdqu	(%rcx), %xmm0
	pand	.LC11(%rip), %xmm0
	movq	%rdx, %rax
	movdqa	.LC13(%rip), %xmm1
	movdqu	(%rcx), %xmm4
	shrq	$4, %rax
	pcmpeqb	.LC12(%rip), %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm4, %xmm0
	por	%xmm1, %xmm0
	movaps	%xmm0, -192(%rbp)
	cmpq	$1, %rax
	je	.L393
	movdqu	16(%rcx), %xmm0
	pand	.LC11(%rip), %xmm0
	movdqa	.LC13(%rip), %xmm1
	movdqu	16(%rcx), %xmm6
	pcmpeqb	.LC12(%rip), %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm6, %xmm0
	por	%xmm1, %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpq	$2, %rax
	je	.L393
	movdqu	32(%rcx), %xmm0
	pand	.LC11(%rip), %xmm0
	movdqa	.LC13(%rip), %xmm1
	movdqu	32(%rcx), %xmm7
	pcmpeqb	.LC12(%rip), %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm7, %xmm0
	por	%xmm1, %xmm0
	movaps	%xmm0, -160(%rbp)
.L393:
	movq	%rdx, %rax
	andq	$-16, %rax
	testb	$15, %dl
	je	.L394
.L392:
	movzbl	(%rcx,%rax), %edi
	movl	$95, %esi
	movl	%edi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	leaq	1(%rax), %r8
	cmove	%esi, %edi
	movb	%dil, -192(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	1(%rcx,%rax), %edi
	leaq	2(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -191(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	2(%rcx,%rax), %edi
	leaq	3(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -190(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	3(%rcx,%rax), %edi
	leaq	4(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -189(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	4(%rcx,%rax), %edi
	leaq	5(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -188(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	5(%rcx,%rax), %edi
	leaq	6(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -187(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	6(%rcx,%rax), %edi
	leaq	7(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -186(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	7(%rcx,%rax), %edi
	leaq	8(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -185(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	8(%rcx,%rax), %edi
	leaq	9(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -184(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	9(%rcx,%rax), %edi
	leaq	10(%rax), %r8
	movl	%edi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%esi, %edi
	movb	%dil, -183(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	10(%rcx,%rax), %esi
	movl	$95, %edi
	leaq	11(%rax), %r8
	movl	%esi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%edi, %esi
	movb	%sil, -182(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	11(%rcx,%rax), %esi
	leaq	12(%rax), %r8
	movl	%esi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%edi, %esi
	movb	%sil, -181(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	12(%rcx,%rax), %esi
	leaq	13(%rax), %r8
	movl	%esi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%edi, %esi
	movb	%sil, -180(%rbp,%rax)
	cmpq	%r8, %rdx
	jbe	.L394
	movzbl	13(%rcx,%rax), %esi
	movl	%esi, %r9d
	andl	$-3, %r9d
	cmpb	$45, %r9b
	cmove	%edi, %esi
	movb	%sil, -179(%rbp,%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L394
	movzbl	14(%rcx,%rax), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpb	$45, %dl
	cmove	%edi, %eax
	movb	%al, -192(%rbp,%rsi)
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	-1008(%rbp), %rax
	leaq	-192(%rbp), %r13
	movq	%rax, -1104(%rbp)
	movq	%r13, %rbx
	movq	%rax, -1024(%rbp)
.L388:
	movl	(%rbx), %edx
	addq	$4, %rbx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L388
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rbx), %rdx
	cmove	%rdx, %rbx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rbx
	subq	%r13, %rbx
	movq	%rbx, -1064(%rbp)
	cmpq	$15, %rbx
	ja	.L555
	cmpq	$1, %rbx
	jne	.L412
	movzbl	-192(%rbp), %eax
	leaq	-1024(%rbp), %rcx
	movq	%rcx, -1112(%rbp)
	movb	%al, -1008(%rbp)
	movq	-1104(%rbp), %rax
.L413:
	movq	%rbx, -1016(%rbp)
	movq	%r12, %rdi
	movb	$0, (%rax,%rbx)
	movq	-1016(%rbp), %rdx
	movq	-1024(%rbp), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1024(%rbp), %rdi
	cmpq	-1104(%rbp), %rdi
	je	.L420
	call	_ZdlPv@PLT
.L420:
	cmpq	$0, -1096(%rbp)
	je	.L426
	movq	-1096(%rbp), %rax
	xorl	%r13d, %r13d
	movl	$10, %ebx
	subq	$1, %rax
	movq	%rax, -1128(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, -1136(%rbp)
	.p2align 4,,10
	.p2align 3
.L428:
	movq	-1120(%rbp), %rax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	movq	-1112(%rbp), %rdi
	movzbl	(%rax,%r13), %r8d
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-1016(%rbp), %rdx
	movq	-1024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	-1128(%rbp), %r13
	movl	$1, %edx
	movq	-1136(%rbp), %rsi
	movq	%rax, %rdi
	movl	$44, %eax
	cmove	%ebx, %eax
	movb	%al, -1064(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1024(%rbp), %rdi
	cmpq	-1104(%rbp), %rdi
	je	.L425
	call	_ZdlPv@PLT
	addq	$1, %r13
	cmpq	%r13, -1096(%rbp)
	jne	.L428
.L426:
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-528(%rbp), %rax
	leaq	-1040(%rbp), %rbx
	movq	$0, -1048(%rbp)
	movq	%rbx, -1056(%rbp)
	leaq	-1056(%rbp), %rdi
	movb	$0, -1040(%rbp)
	testq	%rax, %rax
	je	.L556
	movq	-544(%rbp), %r8
	movq	-536(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L429
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L430:
	movq	.LC29(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-496(%rbp), %rdi
	movq	%rax, -592(%rbp)
	addq	$80, %rax
	movhps	.LC31(%rip), %xmm0
	movq	%rax, -464(%rbp)
	movaps	%xmm0, -576(%rbp)
	cmpq	-1168(%rbp), %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	-1160(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1176(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -592(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%r14), %rax
	movq	%r14, -592(%rbp)
	movq	%rcx, -592(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -464(%rbp)
	movq	$0, -584(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1048(%rbp), %rdx
	movq	-1056(%rbp), %rsi
	movq	-1224(%rbp), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1184(%rbp), %rax
	movq	-1112(%rbp), %rdi
	movslq	8(%rax), %rsi
	call	_ZN4node13native_moduleL10FormatSizeEm
	movq	-1152(%rbp), %rdi
	movq	-1144(%rbp), %rsi
	call	_ZN4node13native_moduleL10FormatSizeEm
	movq	-1024(%rbp), %rax
	movq	-592(%rbp), %rdi
	cmpb	$0, 50+_ZN4node11per_process18enabled_debug_listE(%rip)
	movq	%rdi, -1064(%rbp)
	movq	%rax, -1072(%rbp)
	movq	32(%r15), %rax
	movq	%rax, -1080(%rbp)
	jne	.L547
.L432:
	cmpq	%r12, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movq	-1024(%rbp), %rdi
	cmpq	-1104(%rbp), %rdi
	je	.L434
	call	_ZdlPv@PLT
.L434:
	movq	-1056(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L435
	call	_ZdlPv@PLT
.L435:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -1192(%rbp)
	jne	.L383
	movq	-1224(%rbp), %r15
.L437:
	movl	$246, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1240(%rbp), %rax
	movq	24(%rax), %r12
	cmpq	%r12, -1192(%rbp)
	je	.L385
	movl	$95, %r13d
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L551:
	cmpq	$1, %rbx
	jne	.L466
	movzbl	-128(%rbp), %eax
	movb	%al, -576(%rbp)
	movq	%r8, %rax
.L467:
	movq	%rbx, -584(%rbp)
	movl	$22, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r15, %rdi
	movb	$0, (%rax,%rbx)
	movq	%r8, -1096(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$5, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r12), %rdx
	movq	32(%r12), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$54, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$33, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	$4, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-592(%rbp), %rdi
	movq	-1096(%rbp), %r8
	cmpq	%r8, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movl	$2, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -1192(%rbp)
	je	.L385
.L384:
	movq	40(%r12), %rdx
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	$63, %rdx
	ja	.L440
	testq	%rdx, %rdx
	je	.L448
	leaq	-1(%rdx), %rax
	movq	32(%r12), %rcx
	cmpq	$14, %rax
	jbe	.L499
	movdqu	(%rcx), %xmm0
	pand	.LC11(%rip), %xmm0
	movq	%rdx, %rax
	movdqa	.LC13(%rip), %xmm1
	movdqu	(%rcx), %xmm7
	shrq	$4, %rax
	pcmpeqb	.LC12(%rip), %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm7, %xmm0
	por	%xmm1, %xmm0
	movaps	%xmm0, -128(%rbp)
	cmpq	$1, %rax
	je	.L447
	movdqu	16(%rcx), %xmm0
	pand	.LC11(%rip), %xmm0
	movdqa	.LC13(%rip), %xmm1
	movdqu	16(%rcx), %xmm6
	pcmpeqb	.LC12(%rip), %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm6, %xmm0
	por	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	cmpq	$2, %rax
	je	.L447
	movdqu	32(%rcx), %xmm0
	pand	.LC11(%rip), %xmm0
	movdqa	.LC13(%rip), %xmm1
	movdqu	32(%rcx), %xmm4
	pcmpeqb	.LC12(%rip), %xmm0
	pand	%xmm0, %xmm1
	pandn	%xmm4, %xmm0
	por	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
.L447:
	movq	%rdx, %rax
	andq	$-16, %rax
	testb	$15, %dl
	je	.L448
.L446:
	movzbl	(%rcx,%rax), %esi
	movl	%esi, %edi
	andl	$-3, %edi
	cmpb	$45, %dil
	leaq	1(%rax), %rdi
	cmove	%r13d, %esi
	movb	%sil, -128(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	1(%rcx,%rax), %esi
	leaq	2(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -127(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	2(%rcx,%rax), %esi
	leaq	3(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -126(%rbp,%rax)
	cmpq	%rdi, %rdx
	jbe	.L448
	movzbl	3(%rcx,%rax), %esi
	leaq	4(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -125(%rbp,%rax)
	cmpq	%rdi, %rdx
	jbe	.L448
	movzbl	4(%rcx,%rax), %esi
	leaq	5(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -124(%rbp,%rax)
	cmpq	%rdi, %rdx
	jbe	.L448
	movzbl	5(%rcx,%rax), %esi
	leaq	6(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -123(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	6(%rcx,%rax), %esi
	leaq	7(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -122(%rbp,%rax)
	cmpq	%rdi, %rdx
	jbe	.L448
	movzbl	7(%rcx,%rax), %esi
	leaq	8(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -121(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	8(%rcx,%rax), %esi
	leaq	9(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -120(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	9(%rcx,%rax), %esi
	leaq	10(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -119(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	10(%rcx,%rax), %esi
	leaq	11(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -118(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	11(%rcx,%rax), %esi
	leaq	12(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -117(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	12(%rcx,%rax), %esi
	leaq	13(%rax), %rdi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -116(%rbp,%rax)
	cmpq	%rdx, %rdi
	jnb	.L448
	movzbl	13(%rcx,%rax), %esi
	movl	%esi, %r8d
	andl	$-3, %r8d
	cmpb	$45, %r8b
	cmove	%r13d, %esi
	movb	%sil, -115(%rbp,%rax)
	leaq	14(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L448
	movzbl	14(%rcx,%rax), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpb	$45, %dl
	cmove	%r13d, %eax
	movb	%al, -128(%rbp,%rsi)
	.p2align 4,,10
	.p2align 3
.L448:
	leaq	-576(%rbp), %r8
	leaq	-128(%rbp), %rcx
	movq	%r8, -592(%rbp)
	movq	%rcx, %rbx
.L442:
	movl	(%rbx), %edx
	addq	$4, %rbx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L442
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rbx), %rdx
	cmove	%rdx, %rbx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rbx
	subq	%rcx, %rbx
	movq	%rbx, -1064(%rbp)
	cmpq	$15, %rbx
	jbe	.L551
	xorl	%edx, %edx
	leaq	-1064(%rbp), %rsi
	leaq	-592(%rbp), %rdi
	movq	%r8, -1104(%rbp)
	movq	%rcx, -1096(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1064(%rbp), %rdx
	movq	-1096(%rbp), %rcx
	movq	%rax, -592(%rbp)
	movq	-1104(%rbp), %r8
	movq	%rdx, -576(%rbp)
.L465:
	movl	%ebx, %edx
	cmpl	$8, %ebx
	jnb	.L468
	andl	$4, %ebx
	jne	.L557
	testl	%edx, %edx
	jne	.L558
.L469:
	movq	-1064(%rbp), %rbx
	movq	-592(%rbp), %rax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L425:
	addq	$1, %r13
	cmpq	%r13, -1096(%rbp)
	jne	.L428
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L429:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L412:
	leaq	-1024(%rbp), %rcx
	movq	-1104(%rbp), %rax
	movq	%rcx, -1112(%rbp)
	testq	%rbx, %rbx
	je	.L413
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L555:
	leaq	-1024(%rbp), %rax
	xorl	%edx, %edx
	leaq	-1064(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -1112(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1064(%rbp), %rdx
	movq	%rax, -1024(%rbp)
	movq	%rdx, -1008(%rbp)
.L411:
	movl	%ebx, %edx
	cmpl	$8, %ebx
	jnb	.L414
	andl	$4, %ebx
	jne	.L559
	testl	%edx, %edx
	jne	.L560
.L415:
	movq	-1064(%rbp), %rbx
	movq	-1024(%rbp), %rax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L556:
	leaq	-496(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L440:
	leaq	_ZZN4node13native_moduleL10GetDefNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L414:
	movq	0(%r13), %rdx
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	leaq	8(%rax), %rcx
	andq	$-8, %rcx
	subq	%rcx, %rax
	leal	(%rbx,%rax), %edx
	subq	%rax, %r13
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L415
	andl	$-8, %edx
	xorl	%eax, %eax
.L418:
	movl	%eax, %esi
	addl	$8, %eax
	movq	0(%r13,%rsi), %rdi
	movq	%rdi, (%rcx,%rsi)
	cmpl	%edx, %eax
	jb	.L418
	movq	-1064(%rbp), %rbx
	movq	-1024(%rbp), %rax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L560:
	movzbl	0(%r13), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L415
	movzwl	-2(%r13,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L385:
	movl	$55, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1232(%rbp), %rbx
	leaq	16(%rbx), %rax
	movq	$0, 8(%rbx)
	movq	%rax, (%rbx)
	movq	-928(%rbp), %rax
	movb	$0, 16(%rbx)
	testq	%rax, %rax
	je	.L561
	movq	-944(%rbp), %r8
	movq	-936(%rbp), %rcx
	movq	%rbx, %rdi
	cmpq	%r8, %rax
	ja	.L562
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L477:
	movq	.LC29(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-896(%rbp), %rdi
	movq	%rax, -992(%rbp)
	addq	$80, %rax
	movhps	.LC31(%rip), %xmm0
	movq	%rax, -864(%rbp)
	movaps	%xmm0, -976(%rbp)
	cmpq	-1256(%rbp), %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movq	-1248(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -968(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	-1264(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rbx, -992(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -976(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -976(%rbp,%rax)
	movq	-24(%r14), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r14, -992(%rbp)
	movq	%rbx, -992(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -864(%rbp)
	movq	$0, -984(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L563
	movq	-1232(%rbp), %rax
	addq	$1224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	%r8, %rax
	testq	%rbx, %rbx
	je	.L467
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%rcx,%rdx), %rsi
	movq	%rsi, -8(%rax,%rdx)
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rax
	leal	(%rbx,%rax), %edx
	subq	%rax, %rcx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L469
	andl	$-8, %edx
	xorl	%eax, %eax
.L472:
	movl	%eax, %edi
	addl	$8, %eax
	movq	(%rcx,%rdi), %r9
	movq	%r9, (%rsi,%rdi)
	cmpl	%edx, %eax
	jb	.L472
	movq	-1064(%rbp), %rbx
	movq	-592(%rbp), %rax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L481:
	xorl	%eax, %eax
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L558:
	movzbl	(%rcx), %esi
	movb	%sil, (%rax)
	testb	$2, %dl
	je	.L469
	movzwl	-2(%rcx,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L562:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L499:
	xorl	%eax, %eax
	jmp	.L446
.L559:
	movl	0(%r13), %ecx
	movl	%ecx, (%rax)
	movl	-4(%r13,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L415
.L561:
	movq	%rbx, %rdi
	leaq	-896(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L477
.L557:
	movl	(%rcx), %esi
	movl	%esi, (%rax)
	movl	-4(%rcx,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L469
.L563:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE.cold, @function
_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE.cold:
.LFSB6240:
.L547:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-1072(%rbp), %rcx
	leaq	-1080(%rbp), %rdx
	leaq	-1064(%rbp), %r8
	leaq	.LC17(%rip), %rsi
	call	_ZN4node7FPrintFIJPKcS2_S2_EEEvP8_IO_FILES2_DpOT_
	movq	-592(%rbp), %rdi
	jmp	.L432
	.cfi_endproc
.LFE6240:
	.text
	.size	_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE, .-_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE
	.section	.text.unlikely
	.size	_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE.cold, .-_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE.cold
.LCOLDE32:
	.text
.LHOTE32:
	.section	.rodata.str1.1
.LC33:
	.string	"Failed to compile "
.LC34:
	.string	"\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE
	.type	_ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE, @function
_ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE:
.LFB6241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-156(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -176(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN4node13native_module18NativeModuleLoader12GetModuleIdsB5cxx11Ev@PLT
	movq	-136(%rbp), %r12
	leaq	-104(%rbp), %rax
	movq	-144(%rbp), %r14
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	cmpq	%r12, %r14
	jne	.L571
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L568:
	addq	$32, %r14
	cmpq	%r14, %r12
	je	.L572
.L571:
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZN4node13native_module18NativeModuleLoader13CanBeRequiredEPKc@PLT
	testb	%al, %al
	je	.L568
	movq	(%r14), %rdx
	movq	-168(%rbp), %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdi
	call	_ZN4node13native_module18NativeModuleLoader15CompileAsModuleEN2v85LocalINS2_7ContextEEEPKcPNS1_6ResultE@PLT
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNK4node13native_module18NativeModuleLoader12GetCodeCacheEPKc@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L601
	movq	%r14, %rsi
	leaq	-152(%rbp), %rdx
	movq	%r13, %rdi
	addq	$32, %r14
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE17_M_emplace_uniqueIJRS7_RSB_EEES6_ISt17_Rb_tree_iteratorISC_EbEDpOT_
	cmpq	%r14, %r12
	jne	.L571
.L572:
	movq	-176(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node13native_moduleL17GenerateCodeCacheERKSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPN2v814ScriptCompiler10CachedDataESt4lessIS7_ESaISt4pairIKS7_SB_EEE
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L566
.L567:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_PN2v814ScriptCompiler10CachedDataEESt10_Select1stISC_ESt4lessIS5_ESaISC_EE8_M_eraseEPSt13_Rb_tree_nodeISC_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L575
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L566
.L576:
	movq	%rbx, %r12
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L576
.L566:
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L573
	.p2align 4,,10
	.p2align 3
.L574:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L577
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L574
.L578:
	movq	-144(%rbp), %r12
.L573:
	testq	%r12, %r12
	je	.L564
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L564:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L602
	movq	-176(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L574
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L601:
	movl	$18, %edx
	leaq	.LC33(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r14), %rdx
	movq	(%r14), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L568
.L602:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6241:
	.size	_ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE, .-_ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE, @function
_GLOBAL__sub_I__ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE:
.LFB7735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE7735:
	.size	_GLOBAL__sub_I__ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE, .-_GLOBAL__sub_I__ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node13native_module16CodeCacheBuilder8GenerateB5cxx11EN2v85LocalINS2_7ContextEEE
	.weak	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC35:
	.string	"../src/debug_utils-inl.h:113"
.LC36:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.rodata.str1.1
.LC38:
	.string	"../src/debug_utils-inl.h:76"
.LC39:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC37
	.weak	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*; Args = {const char*}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC40
	.weak	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.weak	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*; Args = {const char*, const char*}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC41
	.weak	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPKcJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC41
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"../tools/code_cache/cache_builder.cc:22"
	.section	.rodata.str1.1
.LC43:
	.string	"(size) < (sizeof(buf))"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"std::string node::native_module::GetDefName(const string&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node13native_moduleL10GetDefNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZN4node13native_moduleL10GetDefNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZN4node13native_moduleL10GetDefNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC45:
	.string	"../src/debug_utils-inl.h:67"
.LC46:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.byte	-3
	.align 16
.LC12:
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.align 16
.LC13:
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.section	.data.rel.ro,"aw"
	.align 8
.LC29:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC30:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC31:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
