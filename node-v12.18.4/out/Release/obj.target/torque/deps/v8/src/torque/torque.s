	.file	"torque.cc"
	.text
	.section	.rodata._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB12481:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L2
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L2:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L4
.L24:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L24
.L4:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L6
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L7:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L8
	testb	$4, %al
	jne	.L26
	testl	%eax, %eax
	je	.L9
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L27
.L9:
	movq	(%r12), %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L9
	andl	$-8, %eax
	xorl	%edx, %edx
.L12:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L12
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L9
.L27:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L9
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12481:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text._ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE
	.type	_ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE, @function
_ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE:
.LFB6520:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	testl	%esi, %esi
	jne	.L29
	movabsq	$4981092742960672596, %rcx
	movl	$1919906418, 24(%rdi)
	movq	%rcx, 16(%rdi)
	movq	$12, 8(%rdi)
	movb	$0, 28(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movabsq	$8246765057735551308, %rcx
	movl	$29295, %edx
	movq	$10, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE6520:
	.size	_ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE, .-_ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE
	.section	.text._ZN2v88internal6torque21TorqueCompilerOptionsD2Ev,"axG",@progbits,_ZN2v88internal6torque21TorqueCompilerOptionsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev
	.type	_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev, @function
_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev:
.LFB6526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	48(%rbx), %rax
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L31
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6526:
	.size	_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev, .-_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev
	.weak	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev
	.set	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev,_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev:
.LFB6872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L36
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L37
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L40
.L38:
	movq	0(%r13), %r12
.L36:
	testq	%r12, %r12
	je	.L35
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L40
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6872:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	.set	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.section	.text._ZN2v88internal6torque13SourceFileMapD2Ev,"axG",@progbits,_ZN2v88internal6torque13SourceFileMapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13SourceFileMapD2Ev
	.type	_ZN2v88internal6torque13SourceFileMapD2Ev, @function
_ZN2v88internal6torque13SourceFileMapD2Ev:
.LFB7912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	40(%rbx), %rax
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L46
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L49
.L47:
	movq	(%rbx), %r12
.L45:
	testq	%r12, %r12
	je	.L43
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L49
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7912:
	.size	_ZN2v88internal6torque13SourceFileMapD2Ev, .-_ZN2v88internal6torque13SourceFileMapD2Ev
	.weak	_ZN2v88internal6torque13SourceFileMapD1Ev
	.set	_ZN2v88internal6torque13SourceFileMapD1Ev,_ZN2v88internal6torque13SourceFileMapD2Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8414:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L67
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L56:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L54
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L52
.L55:
	movq	%rbx, %r12
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L55
.L52:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8414:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8488:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L78
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L72:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L72
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE8488:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9081:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9081
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movabsq	$288230376151711743, %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movq	%rdi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdx, %rax
	je	.L129
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r15, %r13
	testq	%rax, %rax
	je	.L108
	leaq	(%rax,%rax), %rdi
	movq	%rdi, -80(%rbp)
	cmpq	%rdi, %rax
	jbe	.L130
	movabsq	$9223372036854775776, %rax
	movq	%rax, -80(%rbp)
.L83:
	movq	-80(%rbp), %rdi
.LEHB0:
	call	_Znwm@PLT
.LEHE0:
	movq	%rax, -72(%rbp)
.L106:
	addq	-72(%rbp), %r13
	leaq	16(%r13), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, 0(%r13)
	movq	(%r14), %rax
	movq	8(%r14), %r14
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	addq	%r14, %rdi
	je	.L85
	testq	%rax, %rax
	je	.L131
.L85:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L132
	cmpq	$1, %r14
	jne	.L88
	movq	-104(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 16(%r13)
.L89:
	movq	-88(%rbp), %rax
	movq	%r14, 8(%r13)
	movb	$0, (%rax,%r14)
	cmpq	%r15, %rbx
	je	.L110
	movq	-72(%rbp), %rdx
	movq	%r15, %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L128:
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L133
.L94:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	jne	.L91
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L130:
	testq	%rdi, %rdi
	jne	.L84
	movq	$0, -72(%rbp)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%rbx, %r13
	subq	%r15, %r13
	addq	-72(%rbp), %r13
.L90:
	addq	$32, %r13
	cmpq	%r12, %rbx
	je	.L95
	movq	%r13, %rdx
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L134
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -16(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L99
.L97:
	subq	%rbx, %r12
	addq	%r12, %r13
.L95:
	testq	%r15, %r15
	je	.L100
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L100:
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	%r13, %xmm3
	movq	%rax, %xmm0
	addq	-80(%rbp), %rax
	movq	%rax, 16(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L99
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L108:
	movq	$32, -80(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L88:
	testq	%r14, %r14
	je	.L89
	movq	-88(%rbp), %rdi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB1:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r13)
.L87:
	movq	-104(%rbp), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	0(%r13), %rax
	movq	-64(%rbp), %r14
	movq	%rax, -88(%rbp)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-72(%rbp), %r13
	jmp	.L90
.L131:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE1:
.L84:
	movq	-80(%rbp), %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	salq	$5, %rdx
	movq	%rdx, -80(%rbp)
	jmp	.L83
.L135:
	call	__stack_chk_fail@PLT
.L129:
	leaq	.LC1(%rip), %rdi
.LEHB2:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE2:
.L111:
	endbr64
	movq	%rax, %rdi
.L101:
	call	__cxa_begin_catch@PLT
	cmpq	$0, -72(%rbp)
	je	.L136
	movq	-72(%rbp), %rdi
	call	_ZdlPv@PLT
.L105:
.LEHB3:
	call	__cxa_rethrow@PLT
.LEHE3:
.L136:
	movq	0(%r13), %rdi
	cmpq	%rdi, -88(%rbp)
	je	.L105
	call	_ZdlPv@PLT
	jmp	.L105
.L112:
	endbr64
	movq	%rax, %r12
.L104:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB4:
	call	_Unwind_Resume@PLT
.LEHE4:
	.cfi_endproc
.LFE9081:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"aG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 4
.LLSDA9081:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9081-.LLSDATTD9081
.LLSDATTD9081:
	.byte	0x1
	.uleb128 .LLSDACSE9081-.LLSDACSB9081
.LLSDACSB9081:
	.uleb128 .LEHB0-.LFB9081
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB9081
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L111-.LFB9081
	.uleb128 0x1
	.uleb128 .LEHB2-.LFB9081
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB9081
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L112-.LFB9081
	.uleb128 0
	.uleb128 .LEHB4-.LFB9081
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
.LLSDACSE9081:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT9081:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB9093:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L141:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L139
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L137
.L140:
	movq	%rbx, %r12
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L140
.L137:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE9093:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB9095:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L170
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L159:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L157
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L155
.L158:
	movq	%rbx, %r12
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L158
.L155:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE9095:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB9906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L173
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	%rdx, %xmm2
	movq	%rax, %xmm3
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	%rsi, %rbx
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -64(%rbp)
.L178:
	movq	24(%rbx), %rsi
	movq	-72(%rbp), %rdi
	movq	%rbx, %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16(%rbx), %rbx
	movdqa	-64(%rbp), %xmm0
	movq	%rax, 432(%r15)
	movq	528(%r15), %rdi
	addq	$80, %rax
	movq	%rax, 560(%r15)
	leaq	544(%r15), %rax
	movups	%xmm0, 448(%r15)
	cmpq	%rax, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r15), %rdi
	movq	%rax, 456(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r15)
	movq	-24(%r14), %rax
	leaq	560(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 432(%r15,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 448(%r15)
	movq	-24(%r13), %rax
	movq	%rdx, 448(%r15,%rax)
	movq	%r12, 432(%r15)
	movq	-24(%r12), %rax
	movq	%rcx, 432(%r15,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 560(%r15)
	movq	$0, 440(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-64(%rbp), %xmm1
	movq	136(%r15), %rdi
	movq	%rax, 40(%r15)
	addq	$80, %rax
	movq	%rax, 168(%r15)
	leaq	152(%r15), %rax
	movups	%xmm1, 56(%r15)
	cmpq	%rax, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r15), %rdi
	movq	%rax, 64(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r15)
	movq	-24(%r14), %rax
	leaq	168(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, 40(%r15,%rax)
	movq	%r13, 56(%r15)
	movq	-24(%r13), %rax
	movq	%rsi, 56(%r15,%rax)
	movq	%r12, 40(%r15)
	movq	-24(%r12), %rax
	movq	%rcx, 40(%r15,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, 48(%r15)
	movq	%rax, 168(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L178
.L173:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9906:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E:
.LFB11579:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L191
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L185:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L185
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE11579:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB8162:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L208
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
.L198:
	movq	24(%r12), %rsi
	movq	%r12, %r13
	movq	%r14, %rdi
	leaq	40(%r13), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r13), %rbx
	movq	16(%r12), %r12
	testq	%rbx, %rbx
	je	.L196
.L197:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L197
.L196:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L198
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE8162:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.text._ZN2v88internal6torque20TorqueCompilerResultD2Ev,"axG",@progbits,_ZN2v88internal6torque20TorqueCompilerResultD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20TorqueCompilerResultD2Ev
	.type	_ZN2v88internal6torque20TorqueCompilerResultD2Ev, @function
_ZN2v88internal6torque20TorqueCompilerResultD2Ev:
.LFB6550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	184(%rdi), %r13
	movq	176(%rdi), %r12
	cmpq	%r12, %r13
	je	.L212
	.p2align 4,,10
	.p2align 3
.L216:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L213
	call	_ZdlPv@PLT
	addq	$64, %r12
	cmpq	%r12, %r13
	jne	.L216
.L214:
	movq	176(%rbx), %r12
.L212:
	testq	%r12, %r12
	je	.L217
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L217:
	movq	168(%rbx), %r12
	leaq	64(%rbx), %rax
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L218
	movq	216(%r12), %r14
	movq	208(%r12), %r13
	cmpq	%r13, %r14
	je	.L219
	.p2align 4,,10
	.p2align 3
.L223:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L220
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r14, %r13
	jne	.L223
.L221:
	movq	208(%r12), %r13
.L219:
	testq	%r13, %r13
	je	.L224
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L224:
	movq	192(%r12), %r14
	movq	184(%r12), %r13
	cmpq	%r13, %r14
	je	.L225
	.p2align 4,,10
	.p2align 3
.L229:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L226
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r14, %r13
	jne	.L229
.L227:
	movq	184(%r12), %r13
.L225:
	testq	%r13, %r13
	je	.L230
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L230:
	movq	168(%r12), %r14
	movq	160(%r12), %r13
	cmpq	%r13, %r14
	je	.L231
	.p2align 4,,10
	.p2align 3
.L235:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L232
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %r14
	jne	.L235
.L233:
	movq	160(%r12), %r13
.L231:
	testq	%r13, %r13
	je	.L236
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L236:
	movq	144(%r12), %r14
	movq	136(%r12), %r13
	cmpq	%r13, %r14
	je	.L237
	.p2align 4,,10
	.p2align 3
.L241:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %r14
	jne	.L241
.L239:
	movq	136(%r12), %r13
.L237:
	testq	%r13, %r13
	je	.L242
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L242:
	movq	96(%r12), %rax
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rcx
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L250
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-56(%rbp), %rax
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	96(%r13), %r14
	leaq	80(%r13), %r15
	movq	%rax, -56(%rbp)
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	%rax, 8(%r13)
	testq	%r14, %r14
	je	.L249
.L246:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L246
.L249:
	movq	-64(%rbp), %rax
	movq	48(%r13), %r15
	movq	%rax, 8(%r13)
	leaq	32(%r13), %rax
	movq	%rax, -80(%rbp)
	testq	%r15, %r15
	je	.L247
.L248:
	movq	-80(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r14
	cmpq	%rax, %rdi
	je	.L251
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L247
.L252:
	movq	%r14, %r15
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	jne	.L262
.L261:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L254
.L259:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L218:
	movq	160(%rbx), %r13
	testq	%r13, %r13
	je	.L263
	movq	208(%r13), %rdi
	testq	%rdi, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	176(%r13), %rsi
	leaq	160(%r13), %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	144(%r13), %r14
	movq	136(%r13), %r12
	cmpq	%r12, %r14
	je	.L265
	.p2align 4,,10
	.p2align 3
.L269:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L266
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r14
	jne	.L269
.L267:
	movq	136(%r13), %r12
.L265:
	testq	%r12, %r12
	je	.L270
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L270:
	movq	120(%r13), %r14
	movq	112(%r13), %r12
	cmpq	%r12, %r14
	je	.L271
	.p2align 4,,10
	.p2align 3
.L275:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L272
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r14
	jne	.L275
.L273:
	movq	112(%r13), %r12
.L271:
	testq	%r12, %r12
	je	.L276
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L276:
	movq	80(%r13), %r15
	leaq	64(%r13), %rax
	movq	%rax, -64(%rbp)
	testq	%r15, %r15
	je	.L281
.L277:
	movq	24(%r15), %rsi
	movq	-64(%rbp), %rdi
	movq	%r15, %r14
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %r12
	movq	16(%r15), %r15
	leaq	40(%r14), %rax
	testq	%r12, %r12
	je	.L282
.L280:
	movq	24(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%r12, %r12
	jne	.L280
.L282:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L277
.L281:
	movq	48(%r13), %r14
	movq	40(%r13), %r12
	cmpq	%r12, %r14
	je	.L278
	.p2align 4,,10
	.p2align 3
.L279:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L283
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r14
	jne	.L279
.L284:
	movq	40(%r13), %r12
.L278:
	testq	%r12, %r12
	je	.L286
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L286:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movl	$240, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L263:
	movq	128(%rbx), %r12
	leaq	112(%rbx), %r14
	testq	%r12, %r12
	je	.L292
.L288:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %r13
	testq	%rdi, %rdi
	je	.L291
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L292
.L293:
	movq	%r13, %r12
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L213:
	addq	$64, %r12
	cmpq	%r12, %r13
	jne	.L216
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L293
.L292:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L289
.L290:
	movq	-72(%rbp), %rdi
	movq	24(%r12), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %r13
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L289
.L297:
	movq	%r13, %r12
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L297
.L289:
	cmpb	$0, (%rbx)
	jne	.L406
.L211:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L223
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L266:
	addq	$32, %r12
	cmpq	%r12, %r14
	jne	.L269
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L252
.L247:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	jne	.L243
.L250:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r12), %rdi
	leaq	128(%r12), %rax
	movq	$0, 104(%r12)
	movq	$0, 96(%r12)
	cmpq	%rax, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	16(%r12), %r14
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -64(%rbp)
	testq	%r14, %r14
	je	.L259
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r14, %r13
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	(%r14), %r14
	movq	80(%r13), %rdi
	movq	%rax, 8(%r13)
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movq	-64(%rbp), %rax
	movq	48(%r13), %r15
	movq	%rax, 8(%r13)
	leaq	32(%r13), %rax
	movq	%rax, -80(%rbp)
	testq	%r15, %r15
	je	.L261
.L258:
	movq	-80(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	16(%r15), %rax
	movq	32(%r15), %rdi
	movq	%rax, -56(%rbp)
	leaq	48(%r15), %rax
	cmpq	%rax, %rdi
	je	.L260
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -56(%rbp)
	je	.L261
.L262:
	movq	-56(%rbp), %r15
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L272:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L275
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L279
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L238:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L241
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L226:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L229
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L232:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L235
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L406:
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L299
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L300
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L303
.L301:
	movq	8(%rbx), %r12
.L299:
	testq	%r12, %r12
	je	.L211
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L303
	jmp	.L301
	.cfi_endproc
.LFE6550:
	.size	_ZN2v88internal6torque20TorqueCompilerResultD2Ev, .-_ZN2v88internal6torque20TorqueCompilerResultD2Ev
	.weak	_ZN2v88internal6torque20TorqueCompilerResultD1Ev
	.set	_ZN2v88internal6torque20TorqueCompilerResultD1Ev,_ZN2v88internal6torque20TorqueCompilerResultD2Ev
	.section	.rodata._ZN2v88internal6torque11WrappedMainEiPPKc.str1.1,"aMS",@progbits,1
.LC3:
	.string	"-o"
.LC4:
	.string	"-v8-root"
.LC5:
	.string	"-m32"
.LC6:
	.string	"basic_string::basic_string"
	.section	.rodata._ZN2v88internal6torque11WrappedMainEiPPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.align 8
.LC8:
	.string	"Unexpected command-line argument \""
	.section	.rodata._ZN2v88internal6torque11WrappedMainEiPPKc.str1.1
.LC9:
	.string	"\", expected a .tq file.\n"
.LC10:
	.string	"basic_string::append"
.LC11:
	.string	":"
.LC12:
	.string	": "
.LC13:
	.string	"\n"
	.section	.text.unlikely._ZN2v88internal6torque11WrappedMainEiPPKc,"ax",@progbits
.LCOLDB14:
	.section	.text._ZN2v88internal6torque11WrappedMainEiPPKc,"ax",@progbits
.LHOTB14:
	.p2align 4
	.globl	_ZN2v88internal6torque11WrappedMainEiPPKc
	.type	_ZN2v88internal6torque11WrappedMainEiPPKc, @function
_ZN2v88internal6torque11WrappedMainEiPPKc:
.LFB6521:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6521
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-480(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$1, %ebx
	subq	$728, %rsp
	movq	%rsi, -704(%rbp)
	xorl	%esi, %esi
	movl	%edi, -712(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-400(%rbp), %rax
	movb	$0, -400(%rbp)
	movq	%rax, -760(%rbp)
	movq	%rax, -416(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, -752(%rbp)
	movq	%rax, -384(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -696(%rbp)
	leaq	-640(%rbp), %rax
	movq	$0, -408(%rbp)
	movq	$0, -376(%rbp)
	movb	$0, -368(%rbp)
	movw	%si, -352(%rbp)
	movb	$0, -350(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -728(%rbp)
	movaps	%xmm0, -672(%rbp)
	cmpl	$1, %edi
	jle	.L712
	.p2align 4,,10
	.p2align 3
.L408:
	movq	-704(%rbp), %rcx
	movslq	%ebx, %rax
	leaq	0(,%rax,8), %r15
	movq	(%rcx,%rax,8), %r14
	movq	-696(%rbp), %rax
	movq	%rax, -480(%rbp)
	testq	%r14, %r14
	je	.L713
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -640(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L714
	cmpq	$1, %rax
	jne	.L413
	movzbl	(%r14), %edx
	movb	%dl, -464(%rbp)
	movq	-696(%rbp), %rdx
.L414:
	movq	%rax, -472(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movb	$0, (%rdx,%rax)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L415
	movq	-704(%rbp), %rax
	leaq	-416(%rbp), %r14
	addl	$1, %ebx
	movq	8(%rax,%r15), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	-408(%rbp), %rdx
	movq	%rax, %r8
	leaq	-672(%rbp), %rax
	movq	%rax, -720(%rbp)
.LEHB5:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE5:
.L416:
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L449
.L726:
	call	_ZdlPv@PLT
	addl	$1, %ebx
	cmpl	%ebx, -712(%rbp)
	jg	.L408
.L450:
	movq	-416(%rbp), %r13
	leaq	-320(%rbp), %rax
	movq	-408(%rbp), %r12
	movq	%rax, -720(%rbp)
	movq	%rax, -336(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L452
	testq	%r13, %r13
	je	.L715
.L452:
	movq	%r12, -640(%rbp)
	cmpq	$15, %r12
	ja	.L716
	cmpq	$1, %r12
	jne	.L455
	movzbl	0(%r13), %eax
	movb	%al, -320(%rbp)
	movq	-720(%rbp), %rax
.L409:
	movq	%r12, -328(%rbp)
	movb	$0, (%rax,%r12)
	movq	-384(%rbp), %r13
	leaq	-288(%rbp), %rax
	movq	-376(%rbp), %r12
	movq	%rax, -704(%rbp)
	movq	%rax, -304(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L456
	testq	%r13, %r13
	je	.L717
.L456:
	movq	%r12, -640(%rbp)
	cmpq	$15, %r12
	ja	.L718
	cmpq	$1, %r12
	jne	.L459
	movzbl	0(%r13), %eax
	movb	%al, -288(%rbp)
	movq	-704(%rbp), %rax
.L460:
	movq	%r12, -296(%rbp)
	pxor	%xmm0, %xmm0
	movb	$0, (%rax,%r12)
	movzwl	-352(%rbp), %eax
	movq	-664(%rbp), %r15
	movq	-672(%rbp), %r13
	movq	$0, -624(%rbp)
	movw	%ax, -272(%rbp)
	movzbl	-350(%rbp), %eax
	movq	%r15, %rbx
	movaps	%xmm0, -640(%rbp)
	subq	%r13, %rbx
	movb	%al, -270(%rbp)
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L719
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L720
	movq	%rbx, %rdi
.LEHB6:
	call	_Znwm@PLT
.LEHE6:
	movq	%rax, -712(%rbp)
	movq	-664(%rbp), %r15
	movq	-672(%rbp), %r13
.L462:
	movq	-712(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -624(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -640(%rbp)
	cmpq	%r13, %r15
	je	.L467
	leaq	-680(%rbp), %rax
	movq	%rax, -696(%rbp)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L469:
	cmpq	$1, %r12
	jne	.L471
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L472:
	addq	$32, %r13
	movq	%r12, 8(%rbx)
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, %r15
	je	.L467
.L473:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L468
	testq	%r14, %r14
	je	.L721
.L468:
	movq	%r12, -680(%rbp)
	cmpq	$15, %r12
	jbe	.L469
	movq	-696(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB7:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE7:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-680(%rbp), %rax
	movq	%rax, 16(%rbx)
.L470:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-680(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L415:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L417
	movq	-704(%rbp), %rax
	addl	$1, %ebx
	movq	8(%rax,%r15), %r13
	leaq	-448(%rbp), %rax
	leaq	-432(%rbp), %r15
	movq	%rax, -744(%rbp)
	movq	%r15, -448(%rbp)
	testq	%r13, %r13
	je	.L722
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -736(%rbp)
	movq	%rax, -640(%rbp)
	cmpq	$15, %rax
	ja	.L723
	cmpq	$1, %rax
	jne	.L421
	movzbl	0(%r13), %edx
	movb	%dl, -432(%rbp)
	movq	%r15, %rdx
.L422:
	movq	%rax, -440(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-448(%rbp), %rdx
	movq	-384(%rbp), %rdi
	cmpq	%r15, %rdx
	je	.L724
	movq	-440(%rbp), %rax
	movq	-432(%rbp), %rcx
	cmpq	-752(%rbp), %rdi
	je	.L725
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	-368(%rbp), %rsi
	movq	%rdx, -384(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -376(%rbp)
	testq	%rdi, %rdi
	je	.L428
	movq	%rdi, -448(%rbp)
	movq	%rsi, -432(%rbp)
.L426:
	movq	$0, -440(%rbp)
	movb	$0, (%rdi)
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L416
	call	_ZdlPv@PLT
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	jne	.L726
	.p2align 4,,10
	.p2align 3
.L449:
	addl	$1, %ebx
	cmpl	%ebx, -712(%rbp)
	jg	.L408
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L413:
	testq	%rax, %rax
	jne	.L727
	movq	-696(%rbp), %rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-728(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
.LEHB8:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, -464(%rbp)
.L412:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %rax
	movq	-480(%rbp), %rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE8:
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L430
	movb	$1, -350(%rbp)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L421:
	cmpq	$0, -736(%rbp)
	jne	.L728
	movq	%r15, %rdx
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L723:
	movq	-728(%rbp), %rsi
	movq	-744(%rbp), %rdi
	leaq	-672(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -720(%rbp)
	leaq	-416(%rbp), %r14
.LEHB9:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, -432(%rbp)
.L420:
	movq	-736(%rbp), %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %rax
	movq	-448(%rbp), %rdx
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-664(%rbp), %r13
	cmpq	-656(%rbp), %r13
	je	.L431
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	movq	-480(%rbp), %rax
	movq	-472(%rbp), %r15
	movq	%rax, %rcx
	movq	%rax, -736(%rbp)
	addq	%r15, %rcx
	je	.L432
	testq	%rax, %rax
	je	.L729
.L432:
	movq	%r15, -640(%rbp)
	cmpq	$15, %r15
	ja	.L730
	movq	0(%r13), %rdi
	cmpq	$1, %r15
	jne	.L435
	movq	-736(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdi)
	movq	-640(%rbp), %r15
	movq	0(%r13), %rdi
.L436:
	movq	%r15, 8(%r13)
	movb	$0, (%rdi,%r15)
	movq	-664(%rbp), %rax
	addq	$32, %rax
	movq	%rax, -664(%rbp)
.L437:
	leaq	-320(%rbp), %rsi
	movl	$29742, %ecx
	movb	$113, -318(%rbp)
	movq	%rsi, -336(%rbp)
	movw	%cx, -320(%rbp)
	movq	$3, -328(%rbp)
	movb	$0, -317(%rbp)
	movq	-24(%rax), %rdx
	movq	%rsi, -720(%rbp)
	cmpq	$2, %rdx
	ja	.L731
.L591:
	leaq	-672(%rbp), %rax
	movl	$34, %edx
	leaq	.LC8(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	movq	%rax, -720(%rbp)
	leaq	-416(%rbp), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-664(%rbp), %rax
	leaq	_ZSt4cerr(%rip), %rdi
	movq	-24(%rax), %rdx
	movq	-32(%rax), %rsi
	leaq	-672(%rbp), %rax
	movq	%rax, -720(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	leaq	.LC9(%rip), %rsi
	leaq	-672(%rbp), %rax
	movq	%rax, -720(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-672(%rbp), %rax
	movq	%rax, -720(%rbp)
	call	_ZN2v84base2OS5AbortEv@PLT
	.p2align 4,,10
	.p2align 3
.L724:
	movq	-440(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L424
	cmpq	$1, %rdx
	je	.L732
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-440(%rbp), %rdx
	movq	-384(%rbp), %rdi
.L424:
	movq	%rdx, -376(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-448(%rbp), %rdi
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	-672(%rbp), %rax
	leaq	.LC2(%rip), %rdi
	movq	%rax, -720(%rbp)
	leaq	-416(%rbp), %r14
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE9:
	.p2align 4,,10
	.p2align 3
.L725:
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%rdx, -384(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -376(%rbp)
.L428:
	movq	%r15, -448(%rbp)
	leaq	-432(%rbp), %r15
	movq	%r15, %rdi
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L435:
	testq	%r15, %r15
	je	.L436
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L455:
	testq	%r12, %r12
	jne	.L733
	movq	-720(%rbp), %rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L732:
	movzbl	-432(%rbp), %eax
	movb	%al, (%rdi)
	movq	-440(%rbp), %rdx
	movq	-384(%rbp), %rdi
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L471:
	testq	%r12, %r12
	je	.L472
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	-640(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%rbx, -632(%rbp)
	leaq	-336(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdi, -744(%rbp)
	movq	%rdx, -736(%rbp)
	movq	%rax, -696(%rbp)
.LEHB10:
	call	_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE@PLT
.LEHE10:
	movq	-632(%rbp), %rbx
	movq	-640(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L482
	.p2align 4,,10
	.p2align 3
.L486:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L483
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L486
.L484:
	movq	-640(%rbp), %r12
.L482:
	testq	%r12, %r12
	je	.L487
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L487:
	movq	-304(%rbp), %rdi
	cmpq	-704(%rbp), %rdi
	je	.L488
	call	_ZdlPv@PLT
.L488:
	movq	-336(%rbp), %rdi
	cmpq	-720(%rbp), %rdi
	je	.L489
	call	_ZdlPv@PLT
.L489:
	movq	-240(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	-248(%rbp), %r13
	movq	$0, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	movq	%r15, %rbx
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L734
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L735
	movq	%rbx, %rdi
.LEHB11:
	call	_Znwm@PLT
.LEHE11:
	movq	%rax, -704(%rbp)
	movq	-240(%rbp), %r15
	movq	-248(%rbp), %r13
.L491:
	movq	-704(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -320(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -336(%rbp)
	cmpq	%r13, %r15
	jne	.L499
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L495:
	cmpq	$1, %r12
	jne	.L497
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L498:
	addq	$32, %r13
	movq	%r12, 8(%rbx)
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, %r15
	je	.L493
.L499:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L494
	testq	%r14, %r14
	je	.L736
.L494:
	movq	%r12, -640(%rbp)
	cmpq	$15, %r12
	jbe	.L495
	movq	-696(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB12:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE12:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, 16(%rbx)
.L496:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L483:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L486
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L497:
	testq	%r12, %r12
	je	.L498
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L493:
	movq	-224(%rbp), %r13
	leaq	-296(%rbp), %rax
	movq	-216(%rbp), %r12
	movq	%rbx, -328(%rbp)
	movq	%rax, -728(%rbp)
	movq	%rax, -312(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L502
	testq	%r13, %r13
	je	.L737
.L502:
	movq	%r12, -640(%rbp)
	cmpq	$15, %r12
	ja	.L738
	cmpq	$1, %r12
	jne	.L512
	movzbl	0(%r13), %eax
	movb	%al, -296(%rbp)
	movq	-728(%rbp), %rax
.L513:
	movq	%r12, -304(%rbp)
	movb	$0, (%rax,%r12)
.LEHB13:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
.LEHE13:
	movq	-736(%rbp), %rsi
	leaq	-432(%rbp), %r15
	movq	%rsi, (%rax)
	movq	-72(%rbp), %rax
	leaq	-448(%rbp), %rsi
	movq	-80(%rbp), %rbx
	movq	%rsi, -720(%rbp)
	movq	%rax, -704(%rbp)
	cmpq	%rbx, %rax
	je	.L517
	.p2align 4,,10
	.p2align 3
.L568:
	cmpb	$0, 32(%rbx)
	je	.L519
	movl	44(%rbx), %r8d
	movl	36(%rbx), %eax
	movl	$16, %edx
	leaq	.LC0(%rip), %rcx
	movq	vsnprintf@GOTPCREL(%rip), %r12
	movq	-720(%rbp), %rdi
	movl	40(%rbx), %r13d
	addl	$1, %r8d
	movl	%eax, -640(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rsi
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	leaq	-544(%rbp), %rax
	leal	1(%r13), %r8d
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -696(%rbp)
	movl	$16, %edx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movl	-640(%rbp), %edi
.LEHB14:
	call	_ZN2v88internal6torque13SourceFileMap14PathFromV8RootB5cxx11ENS1_8SourceIdE@PLT
.LEHE14:
	leaq	-560(%rbp), %r14
	movq	8(%rax), %r12
	movq	%r14, -576(%rbp)
	movq	(%rax), %r13
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L520
	testq	%r13, %r13
	je	.L739
.L520:
	movq	%r12, -680(%rbp)
	cmpq	$15, %r12
	ja	.L740
	cmpq	$1, %r12
	jne	.L523
	movzbl	0(%r13), %eax
	movb	%al, -560(%rbp)
	movq	%r14, %rax
.L524:
	movq	%r12, -568(%rbp)
	movb	$0, (%rax,%r12)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -568(%rbp)
	je	.L741
	leaq	-576(%rbp), %r12
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
.LEHB15:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE15:
	movq	-576(%rbp), %r9
	movq	-568(%rbp), %r8
	movl	$15, %eax
	movq	-536(%rbp), %rdx
	movq	%rax, %rdi
	movq	-544(%rbp), %rsi
	cmpq	%r14, %r9
	cmovne	-560(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L742
	leaq	-528(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-528(%rbp), %rax
	movq	%rdi, -712(%rbp)
	cmpq	%rax, %rcx
	jbe	.L743
.L531:
	movq	%r12, %rdi
.LEHB16:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE16:
.L533:
	leaq	-496(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -512(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L744
	movq	%rcx, -512(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -496(%rbp)
.L535:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -504(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -504(%rbp)
	je	.L745
	leaq	-512(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
.LEHB17:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE17:
	leaq	-464(%rbp), %rsi
	leaq	16(%rax), %rdx
	movq	%rsi, -480(%rbp)
	movq	(%rax), %rcx
	movq	%rsi, -696(%rbp)
	cmpq	%rdx, %rcx
	je	.L746
	movq	%rcx, -480(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -464(%rbp)
.L538:
	movq	8(%rax), %rcx
	movq	%rcx, -472(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movl	$15, %eax
	movq	-472(%rbp), %r8
	movq	-440(%rbp), %rdx
	movq	-480(%rbp), %r9
	movq	%rax, %rdi
	cmpq	-696(%rbp), %r9
	cmovne	-464(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	movq	-448(%rbp), %rsi
	cmpq	%rdi, %rcx
	jbe	.L540
	cmpq	%r15, %rsi
	cmovne	-432(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L747
.L540:
	leaq	-480(%rbp), %rdi
.LEHB18:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE18:
.L542:
	leaq	-592(%rbp), %r12
	leaq	16(%rax), %rdx
	movq	%r12, -608(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L748
	movq	%rcx, -608(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -592(%rbp)
.L544:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -600(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	-512(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L546
	call	_ZdlPv@PLT
.L546:
	movq	-576(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	-544(%rbp), %rdi
	cmpq	-712(%rbp), %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	-600(%rbp), %rdx
	movq	-608(%rbp), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
.LEHB19:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE19:
	movq	-608(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r12, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
.LEHB20:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE20:
.L519:
	movl	56(%rbx), %edx
	movq	%r15, -448(%rbp)
	testl	%edx, %edx
	jne	.L563
	movabsq	$4981092742960672596, %rax
	movl	$1919906418, 8(%r15)
	movl	$12, %edx
	movq	%rax, (%r15)
	movq	$12, -440(%rbp)
	movb	$0, -420(%rbp)
.L564:
	movq	%r15, %rsi
	leaq	_ZSt4cerr(%rip), %rdi
.LEHB21:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE21:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L565
	call	_ZdlPv@PLT
	addq	$64, %rbx
	cmpq	%rbx, -704(%rbp)
	jne	.L568
.L567:
	movq	-80(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L749
.L517:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	-280(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-312(%rbp), %rdi
	cmpq	-728(%rbp), %rdi
	je	.L569
	call	_ZdlPv@PLT
.L569:
	movq	-328(%rbp), %rbx
	movq	-336(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L570
	.p2align 4,,10
	.p2align 3
.L574:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L571
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L574
.L572:
	movq	-336(%rbp), %r12
.L570:
	testq	%r12, %r12
	je	.L575
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L575:
	movq	-744(%rbp), %rdi
	call	_ZN2v88internal6torque20TorqueCompilerResultD1Ev
	movq	-664(%rbp), %rbx
	movq	-672(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L576
	.p2align 4,,10
	.p2align 3
.L580:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L577
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L580
.L578:
	movq	-672(%rbp), %r12
.L576:
	testq	%r12, %r12
	je	.L581
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L581:
	movq	-384(%rbp), %rdi
	cmpq	-752(%rbp), %rdi
	je	.L582
	call	_ZdlPv@PLT
.L582:
	movq	-416(%rbp), %rdi
	cmpq	-760(%rbp), %rdi
	je	.L583
	call	_ZdlPv@PLT
.L583:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L750
	addq	$728, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	movabsq	$8246765057735551308, %rax
	movl	$10, %edx
	movq	%rax, (%r15)
	movl	$29295, %eax
	movw	%ax, 8(%r15)
	movq	$10, -440(%rbp)
	movb	$0, -422(%rbp)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L577:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L580
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L571:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L574
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L565:
	addq	$64, %rbx
	cmpq	%rbx, -704(%rbp)
	jne	.L568
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L523:
	testq	%r12, %r12
	jne	.L751
	movq	%r14, %rax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L742:
	leaq	-528(%rbp), %rax
	movq	%rax, -712(%rbp)
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L740:
	leaq	-680(%rbp), %rsi
	leaq	-576(%rbp), %rdi
	xorl	%edx, %edx
.LEHB22:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE22:
	movq	%rax, -576(%rbp)
	movq	%rax, %rdi
	movq	-680(%rbp), %rax
	movq	%rax, -560(%rbp)
.L522:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-680(%rbp), %r12
	movq	-576(%rbp), %rax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L744:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -496(%rbp)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L748:
	movdqu	16(%rax), %xmm4
	movaps	%xmm4, -592(%rbp)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L746:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -464(%rbp)
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L747:
	movq	-720(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB23:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE23:
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L743:
	movq	-696(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB24:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE24:
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	-240(%rbp), %rcx
	leaq	-3(%rdx), %r15
	movq	%rcx, -256(%rbp)
	leaq	-256(%rbp), %rdi
	movq	-32(%rax), %rsi
	movq	%rcx, -736(%rbp)
	movq	-24(%rax), %rcx
	cmpq	%rcx, %r15
	ja	.L752
	leaq	3(%rcx), %r13
	subq	%rdx, %r13
	addq	%rsi, %rcx
	setne	%al
	addq	%rsi, %r15
	sete	%r14b
	andb	%al, %r14b
	jne	.L753
	movq	%r13, -640(%rbp)
	cmpq	$15, %r13
	ja	.L754
	cmpq	$1, %r13
	jne	.L443
	movzbl	(%r15), %eax
	movb	%al, -240(%rbp)
	movq	-736(%rbp), %rax
.L444:
	movq	%r13, -248(%rbp)
	movb	$0, (%rax,%r13)
	movq	-248(%rbp), %rdx
	movq	-256(%rbp), %r13
	cmpq	-328(%rbp), %rdx
	je	.L445
.L446:
	cmpq	-736(%rbp), %r13
	je	.L447
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L447:
	movq	-336(%rbp), %rdi
	cmpq	-720(%rbp), %rdi
	jne	.L755
.L448:
	testb	%r14b, %r14b
	jne	.L416
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L431:
	leaq	-672(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -720(%rbp)
	leaq	-416(%rbp), %r14
.LEHB25:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
.LEHE25:
	movq	-664(%rbp), %rax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L459:
	testq	%r12, %r12
	jne	.L756
	movq	-704(%rbp), %rax
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L512:
	testq	%r12, %r12
	jne	.L757
	movq	-728(%rbp), %rax
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L718:
	leaq	-640(%rbp), %rsi
	leaq	-304(%rbp), %rdi
	xorl	%edx, %edx
.LEHB26:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE26:
	movq	%rax, -304(%rbp)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, -288(%rbp)
.L458:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %r12
	movq	-304(%rbp), %rax
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L716:
	leaq	-336(%rbp), %rax
	leaq	-640(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
.LEHB27:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE27:
	movq	%rax, -336(%rbp)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, -320(%rbp)
.L454:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %r12
	movq	-336(%rbp), %rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L738:
	movq	-696(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	xorl	%edx, %edx
.LEHB28:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE28:
	movq	%rax, -312(%rbp)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, -296(%rbp)
.L511:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %r12
	movq	-312(%rbp), %rax
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L730:
	movq	-728(%rbp), %rsi
	leaq	-672(%rbp), %rax
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, -720(%rbp)
	leaq	-416(%rbp), %r14
.LEHB29:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE29:
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, 16(%r13)
.L434:
	movq	-736(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-640(%rbp), %r15
	movq	0(%r13), %rdi
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L719:
	movq	$0, -712(%rbp)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L734:
	movq	$0, -704(%rbp)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$1, %r14d
	testq	%rdx, %rdx
	je	.L446
	movq	-336(%rbp), %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%r14b
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L443:
	testq	%r13, %r13
	jne	.L758
	movq	-736(%rbp), %rax
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L754:
	movq	-728(%rbp), %rsi
	xorl	%edx, %edx
.LEHB30:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE30:
	movq	%rax, -256(%rbp)
	movq	%rax, %rdi
	movq	-640(%rbp), %rax
	movq	%rax, -240(%rbp)
.L442:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-640(%rbp), %r13
	movq	-256(%rbp), %rax
	jmp	.L444
.L712:
	leaq	-320(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -336(%rbp)
	movq	%rax, -720(%rbp)
	jmp	.L409
.L755:
	call	_ZdlPv@PLT
	jmp	.L448
.L736:
	leaq	.LC2(%rip), %rdi
.LEHB31:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE31:
.L741:
	leaq	.LC10(%rip), %rdi
.LEHB32:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE32:
.L745:
	leaq	.LC10(%rip), %rdi
.LEHB33:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE33:
.L721:
	leaq	.LC2(%rip), %rdi
.LEHB34:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE34:
.L757:
	movq	-728(%rbp), %rdi
	jmp	.L511
.L737:
	leaq	.LC2(%rip), %rdi
.LEHB35:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE35:
.L739:
	leaq	.LC2(%rip), %rdi
.LEHB36:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE36:
.L735:
.LEHB37:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE37:
.L720:
.LEHB38:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE38:
.L733:
	movq	-720(%rbp), %rdi
	jmp	.L454
.L756:
	movq	-704(%rbp), %rdi
	jmp	.L458
.L749:
.LEHB39:
	call	_ZN2v84base2OS5AbortEv@PLT
.LEHE39:
.L750:
	call	__stack_chk_fail@PLT
.L751:
	movq	%r14, %rdi
	jmp	.L522
.L752:
	movq	%r15, %rdx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
.LEHB40:
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L753:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE40:
.L728:
	movq	%r15, %rdi
	jmp	.L420
.L729:
	leaq	-672(%rbp), %rax
	leaq	.LC2(%rip), %rdi
	movq	%rax, -720(%rbp)
	leaq	-416(%rbp), %r14
.LEHB41:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE41:
.L758:
	movq	-736(%rbp), %rdi
	jmp	.L442
.L727:
	movq	-696(%rbp), %rdi
	jmp	.L412
.L715:
	leaq	.LC2(%rip), %rdi
.LEHB42:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE42:
.L717:
	leaq	.LC2(%rip), %rdi
.LEHB43:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE43:
.L628:
	endbr64
	movq	%rax, %r12
	jmp	.L528
.L609:
	endbr64
	movq	%rax, %r12
	jmp	.L584
.L608:
	endbr64
	movq	%rax, %r12
	jmp	.L586
.L625:
	endbr64
	movq	%rax, %r12
	jmp	.L555
.L613:
	endbr64
	movq	%rax, %r12
	jmp	.L509
.L622:
	endbr64
	movq	%rax, %rdi
	jmp	.L503
.L626:
	endbr64
	movq	%rax, %r12
	jmp	.L553
.L627:
	endbr64
	movq	%rax, %r12
	jmp	.L551
.L615:
	endbr64
	movq	%rax, %r12
	jmp	.L559
.L623:
	endbr64
	movq	%rax, %rbx
	jmp	.L561
.L620:
	endbr64
	movq	%rax, %r12
	jmp	.L515
.L611:
	endbr64
	movq	%rax, %r12
	leaq	-336(%rbp), %rax
	movq	%rax, -736(%rbp)
	jmp	.L481
.L612:
	endbr64
	movq	%rax, %r12
	jmp	.L588
.L618:
	endbr64
	movq	%rax, %rdi
	jmp	.L475
.L619:
	endbr64
	movq	%rax, %r12
	jmp	.L518
.L624:
	endbr64
	movq	%rax, %r12
	leaq	-528(%rbp), %rax
	movq	%rax, -712(%rbp)
	jmp	.L530
.L614:
	endbr64
	movq	%rax, %r12
	jmp	.L589
.L616:
	endbr64
	movq	%rax, %r12
	jmp	.L463
.L610:
	endbr64
	movq	%rax, %r12
	leaq	-672(%rbp), %rax
	leaq	-416(%rbp), %r14
	movq	%rax, -720(%rbp)
	jmp	.L465
	.section	.gcc_except_table._ZN2v88internal6torque11WrappedMainEiPPKc,"a",@progbits
	.align 4
.LLSDA6521:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6521-.LLSDATTD6521
.LLSDATTD6521:
	.byte	0x1
	.uleb128 .LLSDACSE6521-.LLSDACSB6521
.LLSDACSB6521:
	.uleb128 .LEHB5-.LFB6521
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L608-.LFB6521
	.uleb128 0
	.uleb128 .LEHB6-.LFB6521
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L611-.LFB6521
	.uleb128 0
	.uleb128 .LEHB7-.LFB6521
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L618-.LFB6521
	.uleb128 0x1
	.uleb128 .LEHB8-.LFB6521
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L610-.LFB6521
	.uleb128 0
	.uleb128 .LEHB9-.LFB6521
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L608-.LFB6521
	.uleb128 0
	.uleb128 .LEHB10-.LFB6521
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L612-.LFB6521
	.uleb128 0
	.uleb128 .LEHB11-.LFB6521
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L613-.LFB6521
	.uleb128 0
	.uleb128 .LEHB12-.LFB6521
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L622-.LFB6521
	.uleb128 0x1
	.uleb128 .LEHB13-.LFB6521
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L619-.LFB6521
	.uleb128 0
	.uleb128 .LEHB14-.LFB6521
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L624-.LFB6521
	.uleb128 0
	.uleb128 .LEHB15-.LFB6521
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L628-.LFB6521
	.uleb128 0
	.uleb128 .LEHB16-.LFB6521
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L625-.LFB6521
	.uleb128 0
	.uleb128 .LEHB17-.LFB6521
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L626-.LFB6521
	.uleb128 0
	.uleb128 .LEHB18-.LFB6521
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L627-.LFB6521
	.uleb128 0
	.uleb128 .LEHB19-.LFB6521
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L623-.LFB6521
	.uleb128 0
	.uleb128 .LEHB20-.LFB6521
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L615-.LFB6521
	.uleb128 0
	.uleb128 .LEHB21-.LFB6521
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L614-.LFB6521
	.uleb128 0
	.uleb128 .LEHB22-.LFB6521
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L624-.LFB6521
	.uleb128 0
	.uleb128 .LEHB23-.LFB6521
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L627-.LFB6521
	.uleb128 0
	.uleb128 .LEHB24-.LFB6521
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L625-.LFB6521
	.uleb128 0
	.uleb128 .LEHB25-.LFB6521
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L608-.LFB6521
	.uleb128 0
	.uleb128 .LEHB26-.LFB6521
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L616-.LFB6521
	.uleb128 0
	.uleb128 .LEHB27-.LFB6521
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L610-.LFB6521
	.uleb128 0
	.uleb128 .LEHB28-.LFB6521
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L620-.LFB6521
	.uleb128 0
	.uleb128 .LEHB29-.LFB6521
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L608-.LFB6521
	.uleb128 0
	.uleb128 .LEHB30-.LFB6521
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L609-.LFB6521
	.uleb128 0
	.uleb128 .LEHB31-.LFB6521
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L622-.LFB6521
	.uleb128 0x1
	.uleb128 .LEHB32-.LFB6521
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L628-.LFB6521
	.uleb128 0
	.uleb128 .LEHB33-.LFB6521
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L626-.LFB6521
	.uleb128 0
	.uleb128 .LEHB34-.LFB6521
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L618-.LFB6521
	.uleb128 0x1
	.uleb128 .LEHB35-.LFB6521
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L620-.LFB6521
	.uleb128 0
	.uleb128 .LEHB36-.LFB6521
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L624-.LFB6521
	.uleb128 0
	.uleb128 .LEHB37-.LFB6521
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L613-.LFB6521
	.uleb128 0
	.uleb128 .LEHB38-.LFB6521
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L611-.LFB6521
	.uleb128 0
	.uleb128 .LEHB39-.LFB6521
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L615-.LFB6521
	.uleb128 0
	.uleb128 .LEHB40-.LFB6521
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L609-.LFB6521
	.uleb128 0
	.uleb128 .LEHB41-.LFB6521
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L608-.LFB6521
	.uleb128 0
	.uleb128 .LEHB42-.LFB6521
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L610-.LFB6521
	.uleb128 0
	.uleb128 .LEHB43-.LFB6521
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L616-.LFB6521
	.uleb128 0
.LLSDACSE6521:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6521:
	.section	.text._ZN2v88internal6torque11WrappedMainEiPPKc
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque11WrappedMainEiPPKc
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6521
	.type	_ZN2v88internal6torque11WrappedMainEiPPKc.cold, @function
_ZN2v88internal6torque11WrappedMainEiPPKc.cold:
.LFSB6521:
.L528:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-576(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L529
	call	_ZdlPv@PLT
.L529:
	leaq	-528(%rbp), %rax
	movq	%rax, -712(%rbp)
.L530:
	movq	-544(%rbp), %rdi
	cmpq	-712(%rbp), %rdi
	je	.L589
	call	_ZdlPv@PLT
.L589:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L759
.L559:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	-280(%rbp), %rdx
	movq	-736(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque13SourceFileMapD1Ev
.L509:
	movq	-744(%rbp), %rdi
	leaq	-416(%rbp), %r14
	call	_ZN2v88internal6torque20TorqueCompilerResultD1Ev
	leaq	-672(%rbp), %rax
	movq	%rax, -720(%rbp)
.L465:
	movq	-720(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev
	movq	%r12, %rdi
.LEHB44:
	call	_Unwind_Resume@PLT
.LEHE44:
.L584:
	movq	-336(%rbp), %rdi
	cmpq	-720(%rbp), %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	leaq	-672(%rbp), %rax
	leaq	-416(%rbp), %r14
	movq	%rax, -720(%rbp)
.L586:
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L465
	call	_ZdlPv@PLT
	jmp	.L465
.L759:
	call	_ZdlPv@PLT
	jmp	.L559
.L551:
	movq	-480(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L553
	call	_ZdlPv@PLT
.L553:
	movq	-512(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L555
	call	_ZdlPv@PLT
.L555:
	movq	-576(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L530
	call	_ZdlPv@PLT
	jmp	.L530
.L503:
	call	__cxa_begin_catch@PLT
	movq	-704(%rbp), %r12
.L506:
	cmpq	%rbx, %r12
	jne	.L760
.LEHB45:
	call	__cxa_rethrow@PLT
.LEHE45:
.L760:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	addq	$32, %r12
	jmp	.L506
.L621:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L509
	call	_ZdlPv@PLT
	jmp	.L509
.L561:
	movq	-608(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L562
	call	_ZdlPv@PLT
.L562:
	movq	%rbx, %r12
	jmp	.L559
.L515:
	movq	-736(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	jmp	.L509
.L617:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L480
	call	_ZdlPv@PLT
.L480:
	leaq	-336(%rbp), %rax
	movq	%rax, -736(%rbp)
.L481:
	movq	-736(%rbp), %rdi
	leaq	-416(%rbp), %r14
	call	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev
	leaq	-672(%rbp), %rax
	movq	%rax, -720(%rbp)
	jmp	.L465
.L588:
	movq	-696(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	jmp	.L481
.L475:
	call	__cxa_begin_catch@PLT
	movq	-712(%rbp), %r12
.L478:
	cmpq	%rbx, %r12
	jne	.L761
.LEHB46:
	call	__cxa_rethrow@PLT
.LEHE46:
.L518:
	movq	-736(%rbp), %rdi
	call	_ZN2v88internal6torque13SourceFileMapD1Ev
	jmp	.L509
.L463:
	movq	-336(%rbp), %rdi
	cmpq	-720(%rbp), %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	leaq	-672(%rbp), %rax
	leaq	-416(%rbp), %r14
	movq	%rax, -720(%rbp)
	jmp	.L465
.L761:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L477
	call	_ZdlPv@PLT
.L477:
	addq	$32, %r12
	jmp	.L478
	.cfi_endproc
.LFE6521:
	.section	.gcc_except_table._ZN2v88internal6torque11WrappedMainEiPPKc
	.align 4
.LLSDAC6521:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6521-.LLSDATTDC6521
.LLSDATTDC6521:
	.byte	0x1
	.uleb128 .LLSDACSEC6521-.LLSDACSBC6521
.LLSDACSBC6521:
	.uleb128 .LEHB44-.LCOLDB14
	.uleb128 .LEHE44-.LEHB44
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB45-.LCOLDB14
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L621-.LCOLDB14
	.uleb128 0
	.uleb128 .LEHB46-.LCOLDB14
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L617-.LCOLDB14
	.uleb128 0
.LLSDACSEC6521:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6521:
	.section	.text.unlikely._ZN2v88internal6torque11WrappedMainEiPPKc
	.section	.text._ZN2v88internal6torque11WrappedMainEiPPKc
	.size	_ZN2v88internal6torque11WrappedMainEiPPKc, .-_ZN2v88internal6torque11WrappedMainEiPPKc
	.section	.text.unlikely._ZN2v88internal6torque11WrappedMainEiPPKc
	.size	_ZN2v88internal6torque11WrappedMainEiPPKc.cold, .-_ZN2v88internal6torque11WrappedMainEiPPKc.cold
.LCOLDE14:
	.section	.text._ZN2v88internal6torque11WrappedMainEiPPKc
.LHOTE14:
	.section	.text.startup.main,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB6599:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal6torque11WrappedMainEiPPKc
	.cfi_endproc
.LFE6599:
	.size	main, .-main
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE, @function
_GLOBAL__sub_I__ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE:
.LFB12110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12110:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE, .-_GLOBAL__sub_I__ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque14ErrorPrefixForB5cxx11ENS1_13TorqueMessage4KindE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
