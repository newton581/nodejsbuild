	.file	"metablock.c"
	.text
	.p2align 4
	.type	BlockSplitterFinishBlockLiteral, @function
BlockSplitterFinishBlockLiteral:
.LFB102:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2232, %rsp
	movq	24(%rdi), %rdx
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	cmovnb	8(%rdi), %rax
	movq	40(%rdi), %r15
	movq	%rax, 64(%rdi)
	testq	%rdx, %rdx
	jne	.L2
	movq	24(%r14), %rcx
	pxor	%xmm1, %xmm1
	movl	%eax, (%rcx)
	movq	16(%r14), %rax
	movq	%r15, %rcx
	movb	$0, (%rax)
	movq	(%rdi), %rax
	leaq	(%r15,%rax,4), %rsi
	andl	$1, %eax
	movq	%rax, %r12
	jne	.L69
	movapd	%xmm1, %xmm2
	cmpq	%rsi, %r15
	jb	.L13
.L4:
	movq	72(%rbx), %rax
	movq	48(%rbx), %rdx
	unpcklpd	%xmm1, %xmm1
	addq	$1, 24(%rbx)
	addq	$1, %rax
	movaps	%xmm1, 96(%rbx)
	addq	$1, (%r14)
	movq	%rax, 72(%rbx)
	cmpq	(%rdx), %rax
	jb	.L97
.L20:
	movq	$0, 64(%rbx)
.L21:
	testl	%r13d, %r13d
	je	.L1
	movq	48(%rbx), %rax
	movq	(%r14), %rdx
	movq	%rdx, (%rax)
	movq	24(%rbx), %rax
	movq	%rax, 8(%r14)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$2232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	movq	%rdi, %rcx
.L13:
	movl	(%rcx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rcx), %rdi
	cvtsi2sdq	%rax, %xmm3
	leaq	(%rax,%r12), %rdx
	cmpq	$255, %rax
	ja	.L7
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rax,4), %xmm0
.L8:
	mulsd	%xmm0, %xmm3
	movq	%rdi, %rcx
	subsd	%xmm3, %xmm2
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rax, %rax
	je	.L21
	movq	72(%rdi), %rcx
	movq	(%rdi), %rax
	pxor	%xmm1, %xmm1
	movq	%rcx, %rdx
	movq	%rax, %r12
	salq	$6, %rdx
	addq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r15, %rdx
	leaq	(%rdx,%rax,4), %rsi
	andl	$1, %r12d
	jne	.L74
	movapd	%xmm1, %xmm2
	cmpq	%rsi, %rdx
	jb	.L32
	movapd	%xmm1, %xmm4
.L23:
	leaq	-2160(%rbp), %rdi
	leaq	-2144(%rbp), %r10
	xorl	%edx, %edx
	movq	%rdi, -2184(%rbp)
	movq	%r10, %r12
	leaq	-2176(%rbp), %r11
.L58:
	movq	%rcx, %rsi
	movq	80(%rbx,%rdx,8), %r9
	movq	%r12, %rdi
	movq	%r12, %r8
	salq	$6, %rsi
	addq	%rcx, %rsi
	movl	$130, %ecx
	salq	$4, %rsi
	addq	%r15, %rsi
	rep movsq
	movq	%r9, %rcx
	salq	$6, %rcx
	addq	%rcx, %r9
	salq	$4, %r9
	leaq	(%r15,%r9), %rsi
	movq	1024(%rsi), %rcx
	addq	%rcx, 1024(%r12)
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L39:
	movdqu	(%rsi,%rcx), %xmm0
	paddd	(%r12,%rcx), %xmm0
	movaps	%xmm0, (%r12,%rcx)
	addq	$16, %rcx
	cmpq	$1024, %rcx
	jne	.L39
	leaq	(%r12,%rax,4), %rsi
	andl	$1, %eax
	jne	.L79
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	cmpq	%rsi, %r12
	jb	.L50
.L41:
	movsd	%xmm2, (%r11,%rdx,8)
	subsd	%xmm4, %xmm2
	subsd	96(%rbx,%rdx,8), %xmm2
	movq	-2184(%rbp), %rax
	addq	$1040, %r12
	movsd	%xmm2, (%rax,%rdx,8)
	cmpq	$1, %rdx
	jne	.L99
	movq	24(%rbx), %rax
	movq	(%r14), %rdx
	movsd	-2160(%rbp), %xmm0
	movsd	-2152(%rbp), %xmm1
	movq	24(%r14), %rcx
	movq	64(%rbx), %rdi
	leaq	0(,%rax,4), %rsi
	cmpq	$255, %rdx
	ja	.L59
	movsd	16(%rbx), %xmm2
	comisd	%xmm2, %xmm0
	jbe	.L59
	comisd	%xmm2, %xmm1
	jbe	.L59
	movl	%edi, (%rcx,%rax,4)
	movq	16(%r14), %rcx
	movb	%dl, (%rcx,%rax)
	movq	80(%rbx), %rax
	movsd	96(%rbx), %xmm0
	movq	%rax, 88(%rbx)
	movq	(%r14), %rax
	addq	$1, 24(%rbx)
	movzbl	%al, %edx
	addq	$1, %rax
	movsd	%xmm0, 104(%rbx)
	movq	%rdx, 80(%rbx)
	movsd	%xmm4, 96(%rbx)
	movq	%rax, (%r14)
	movq	72(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	48(%rbx), %rax
	movq	%rdx, 72(%rbx)
	cmpq	(%rax), %rdx
	jb	.L100
.L62:
	movq	8(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	$0, 112(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L69:
	movapd	%xmm1, %xmm2
.L3:
	movl	(%rcx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rcx), %rdi
	cvtsi2sdq	%rax, %xmm3
	leaq	(%rax,%rdx), %r12
	cmpq	$255, %rax
	ja	.L11
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L12:
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm2
	cmpq	%rdi, %rsi
	ja	.L71
	testq	%r12, %r12
	je	.L14
	js	.L15
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r12, %xmm1
.L16:
	cmpq	$255, %r12
	ja	.L17
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L18:
	mulsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
.L14:
	maxsd	%xmm2, %xmm1
	jmp	.L4
.L7:
	movapd	%xmm3, %xmm0
	movq	%rdi, -2208(%rbp)
	movq	%rsi, -2200(%rbp)
	movq	%rdx, -2192(%rbp)
	movsd	%xmm1, -2224(%rbp)
	movsd	%xmm2, -2216(%rbp)
	movsd	%xmm3, -2184(%rbp)
	call	log2@PLT
	movsd	-2224(%rbp), %xmm1
	movsd	-2216(%rbp), %xmm2
	movq	-2208(%rbp), %rdi
	movq	-2200(%rbp), %rsi
	movq	-2192(%rbp), %rdx
	movsd	-2184(%rbp), %xmm3
	jmp	.L8
.L74:
	movapd	%xmm1, %xmm2
	xorl	%r12d, %r12d
.L22:
	movl	(%rdx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rdx), %rcx
	cvtsi2sdq	%rax, %xmm3
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L30
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L31:
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm2
	cmpq	%rcx, %rsi
	ja	.L76
	testq	%r12, %r12
	je	.L77
	js	.L34
	pxor	%xmm4, %xmm4
	cvtsi2sdq	%r12, %xmm4
.L35:
	cmpq	$255, %r12
	ja	.L36
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L37:
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm2
.L33:
	maxsd	%xmm2, %xmm4
	movq	72(%rbx), %rcx
	movq	(%rbx), %rax
	jmp	.L23
.L97:
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	salq	$4, %rax
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 1016(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 1024(%r15)
	movq	%rax, 1032(%r15)
	jmp	.L20
.L81:
	movq	%rdi, %r8
.L50:
	movl	(%r8), %ecx
	pxor	%xmm2, %xmm2
	leaq	4(%r8), %rdi
	cvtsi2sdq	%rcx, %xmm2
	addq	%rcx, %rax
	cmpq	$255, %rcx
	ja	.L44
	leaq	kLog2Table(%rip), %r9
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r9,%rcx,4), %xmm0
.L45:
	mulsd	%xmm0, %xmm2
	movq	%rdi, %r8
	subsd	%xmm2, %xmm3
.L40:
	movl	(%r8), %ecx
	pxor	%xmm2, %xmm2
	leaq	4(%r8), %rdi
	cvtsi2sdq	%rcx, %xmm2
	addq	%rcx, %rax
	cmpq	$255, %rcx
	ja	.L48
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r8,%rcx,4), %xmm0
.L49:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm3
	cmpq	%rdi, %rsi
	ja	.L81
	testq	%rax, %rax
	je	.L82
	js	.L52
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L53:
	cmpq	$255, %rax
	ja	.L54
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L55:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm3
.L51:
	maxsd	%xmm3, %xmm2
	jmp	.L41
.L59:
	subsd	.LC2(%rip), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L101
	addl	%edi, -4(%rcx,%rsi)
	movq	80(%rbx), %rcx
	movq	%r10, %rsi
	movsd	-2176(%rbp), %xmm0
	movq	%rcx, %rax
	salq	$6, %rax
	addq	%rcx, %rax
	movl	$130, %ecx
	salq	$4, %rax
	addq	%r15, %rax
	movq	%rax, %rdi
	rep movsq
	movsd	%xmm0, 96(%rbx)
	cmpq	$1, %rdx
	jne	.L66
	movsd	%xmm0, 104(%rbx)
.L66:
	movq	72(%rbx), %rdx
	movq	$0, 64(%rbx)
	movq	%rdx, %rax
	salq	$6, %rax
	addq	%rdx, %rax
	salq	$4, %rax
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 1016(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 1024(%r15)
	movq	%rax, 1032(%r15)
	movq	112(%rbx), %rax
	addq	$1, %rax
	movq	%rax, 112(%rbx)
	cmpq	$1, %rax
	jbe	.L21
	movq	8(%rbx), %rax
	addq	%rax, 56(%rbx)
	jmp	.L21
.L76:
	movq	%rcx, %rdx
.L32:
	movl	(%rdx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rdx), %rcx
	cvtsi2sdq	%rax, %xmm3
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L26
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L27:
	mulsd	%xmm0, %xmm3
	movq	%rcx, %rdx
	subsd	%xmm3, %xmm2
	jmp	.L22
.L11:
	movapd	%xmm3, %xmm0
	movq	%rdi, -2208(%rbp)
	movq	%rsi, -2192(%rbp)
	movsd	%xmm1, -2216(%rbp)
	movsd	%xmm2, -2200(%rbp)
	movsd	%xmm3, -2184(%rbp)
	call	log2@PLT
	movsd	-2216(%rbp), %xmm1
	movq	-2208(%rbp), %rdi
	movsd	-2200(%rbp), %xmm2
	movq	-2192(%rbp), %rsi
	movsd	-2184(%rbp), %xmm3
	jmp	.L12
.L26:
	movapd	%xmm3, %xmm0
	movq	%rcx, -2200(%rbp)
	movq	%rsi, -2192(%rbp)
	movsd	%xmm1, -2216(%rbp)
	movsd	%xmm2, -2208(%rbp)
	movsd	%xmm3, -2184(%rbp)
	call	log2@PLT
	movsd	-2216(%rbp), %xmm1
	movsd	-2208(%rbp), %xmm2
	movq	-2200(%rbp), %rcx
	movq	-2192(%rbp), %rsi
	movsd	-2184(%rbp), %xmm3
	jmp	.L27
.L17:
	movapd	%xmm1, %xmm0
	movsd	%xmm2, -2192(%rbp)
	movsd	%xmm1, -2184(%rbp)
	call	log2@PLT
	movsd	-2192(%rbp), %xmm2
	movsd	-2184(%rbp), %xmm1
	jmp	.L18
.L15:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L16
.L30:
	movapd	%xmm3, %xmm0
	movq	%rcx, -2208(%rbp)
	movq	%rsi, -2192(%rbp)
	movsd	%xmm1, -2216(%rbp)
	movsd	%xmm2, -2200(%rbp)
	movsd	%xmm3, -2184(%rbp)
	call	log2@PLT
	movsd	-2216(%rbp), %xmm1
	movq	-2208(%rbp), %rcx
	movsd	-2200(%rbp), %xmm2
	movq	-2192(%rbp), %rsi
	movsd	-2184(%rbp), %xmm3
	jmp	.L31
.L44:
	movapd	%xmm2, %xmm0
	movq	%r10, -2264(%rbp)
	movq	%r11, -2256(%rbp)
	movq	%rdx, -2248(%rbp)
	movq	%rax, -2224(%rbp)
	movq	%rdi, -2216(%rbp)
	movq	%rsi, -2208(%rbp)
	movsd	%xmm1, -2240(%rbp)
	movsd	%xmm3, -2232(%rbp)
	movsd	%xmm4, -2200(%rbp)
	movsd	%xmm2, -2192(%rbp)
	call	log2@PLT
	movq	-2264(%rbp), %r10
	movq	-2256(%rbp), %r11
	movq	-2248(%rbp), %rdx
	movsd	-2240(%rbp), %xmm1
	movsd	-2232(%rbp), %xmm3
	movq	-2224(%rbp), %rax
	movq	-2216(%rbp), %rdi
	movq	-2208(%rbp), %rsi
	movsd	-2200(%rbp), %xmm4
	movsd	-2192(%rbp), %xmm2
	jmp	.L45
.L101:
	movq	16(%r14), %rdx
	movl	%edi, (%rcx,%rax,4)
	leaq	-1104(%rbp), %rsi
	movzbl	-2(%rdx,%rax), %ecx
	movb	%cl, (%rdx,%rax)
	movq	88(%rbx), %rax
	movdqa	80(%rbx), %xmm0
	movq	%rax, %rdx
	salq	$6, %rdx
	shufpd	$1, %xmm0, %xmm0
	movaps	%xmm0, 80(%rbx)
	movsd	-2168(%rbp), %xmm0
	addq	%rax, %rdx
	movl	$130, %eax
	salq	$4, %rdx
	movq	%rax, %rcx
	addq	%r15, %rdx
	movq	%rdx, %rdi
	rep movsq
	addq	$1, 24(%rbx)
	movq	$0, 64(%rbx)
	movq	%rcx, %rax
	movq	72(%rbx), %rcx
	movhpd	96(%rbx), %xmm0
	movaps	%xmm0, 96(%rbx)
	movq	%rcx, %rdx
	salq	$6, %rdx
	addq	%rcx, %rdx
	salq	$4, %rdx
	addq	%rdx, %r15
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 1016(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 1024(%r15)
	movq	%rax, 1032(%r15)
	movq	8(%rbx), %rax
	movq	$0, 112(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L21
.L79:
	movapd	%xmm1, %xmm3
	xorl	%eax, %eax
	jmp	.L40
.L36:
	movapd	%xmm4, %xmm0
	movsd	%xmm1, -2200(%rbp)
	movsd	%xmm2, -2192(%rbp)
	movsd	%xmm4, -2184(%rbp)
	call	log2@PLT
	movsd	-2200(%rbp), %xmm1
	movsd	-2192(%rbp), %xmm2
	movsd	-2184(%rbp), %xmm4
	jmp	.L37
.L34:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm4, %xmm4
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm4
	addsd	%xmm4, %xmm4
	jmp	.L35
.L48:
	movapd	%xmm2, %xmm0
	movq	%r10, -2264(%rbp)
	movq	%r11, -2256(%rbp)
	movq	%rdx, -2248(%rbp)
	movq	%rdi, -2232(%rbp)
	movq	%rax, -2216(%rbp)
	movq	%rsi, -2208(%rbp)
	movsd	%xmm1, -2240(%rbp)
	movsd	%xmm3, -2224(%rbp)
	movsd	%xmm4, -2200(%rbp)
	movsd	%xmm2, -2192(%rbp)
	call	log2@PLT
	movq	-2264(%rbp), %r10
	movq	-2256(%rbp), %r11
	movq	-2248(%rbp), %rdx
	movsd	-2240(%rbp), %xmm1
	movq	-2232(%rbp), %rdi
	movsd	-2224(%rbp), %xmm3
	movq	-2216(%rbp), %rax
	movq	-2208(%rbp), %rsi
	movsd	-2200(%rbp), %xmm4
	movsd	-2192(%rbp), %xmm2
	jmp	.L49
.L54:
	movapd	%xmm2, %xmm0
	movq	%r10, -2240(%rbp)
	movq	%r11, -2232(%rbp)
	movq	%rdx, -2224(%rbp)
	movsd	%xmm1, -2216(%rbp)
	movsd	%xmm3, -2208(%rbp)
	movsd	%xmm4, -2200(%rbp)
	movsd	%xmm2, -2192(%rbp)
	call	log2@PLT
	movq	-2240(%rbp), %r10
	movq	-2232(%rbp), %r11
	movq	-2224(%rbp), %rdx
	movsd	-2216(%rbp), %xmm1
	movsd	-2208(%rbp), %xmm3
	movsd	-2200(%rbp), %xmm4
	movsd	-2192(%rbp), %xmm2
	jmp	.L55
.L52:
	movq	%rax, %rcx
	movq	%rax, %rsi
	pxor	%xmm2, %xmm2
	shrq	%rcx
	andl	$1, %esi
	orq	%rsi, %rcx
	cvtsi2sdq	%rcx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L53
.L77:
	movapd	%xmm1, %xmm4
	jmp	.L33
.L82:
	movapd	%xmm1, %xmm2
	jmp	.L51
.L100:
	movq	%rdx, %rax
	salq	$6, %rax
	addq	%rdx, %rax
	salq	$4, %rax
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 1016(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 1024(%r15)
	movq	%rax, 1032(%r15)
	jmp	.L62
.L98:
	call	__stack_chk_fail@PLT
.L99:
	movq	72(%rbx), %rcx
	movq	(%rbx), %rax
	movl	$1, %edx
	jmp	.L58
	.cfi_endproc
.LFE102:
	.size	BlockSplitterFinishBlockLiteral, .-BlockSplitterFinishBlockLiteral
	.p2align 4
	.type	BlockSplitterFinishBlockCommand, @function
BlockSplitterFinishBlockCommand:
.LFB105:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1720, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rdx
	movq	32(%rdi), %r14
	movq	40(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	movq	%rdi, %rbx
	cmovnb	8(%rdi), %rax
	movl	%esi, %r13d
	movq	%rax, 64(%rdi)
	testq	%rdx, %rdx
	jne	.L103
	movq	24(%r14), %rcx
	pxor	%xmm1, %xmm1
	movl	%eax, (%rcx)
	movq	16(%r14), %rax
	movq	%r15, %rcx
	movb	$0, (%rax)
	movq	(%rdi), %rax
	leaq	(%r15,%rax,4), %rsi
	andl	$1, %eax
	movq	%rax, %r12
	jne	.L170
	movapd	%xmm1, %xmm2
	cmpq	%rsi, %r15
	jb	.L114
.L105:
	movq	72(%rbx), %rax
	movq	48(%rbx), %rdx
	unpcklpd	%xmm1, %xmm1
	addq	$1, 24(%rbx)
	addq	$1, %rax
	movaps	%xmm1, 96(%rbx)
	addq	$1, (%r14)
	movq	%rax, 72(%rbx)
	cmpq	(%rdx), %rax
	jb	.L197
.L121:
	movq	$0, 64(%rbx)
.L122:
	testl	%r13d, %r13d
	je	.L102
	movq	48(%rbx), %rax
	movq	(%r14), %rdx
	movq	%rdx, (%rax)
	movq	24(%rbx), %rax
	movq	%rax, 8(%r14)
.L102:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$5816, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L172:
	.cfi_restore_state
	movq	%rdi, %rcx
.L114:
	movl	(%rcx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rcx), %rdi
	cvtsi2sdq	%rax, %xmm3
	leaq	(%rax,%r12), %rdx
	cmpq	$255, %rax
	ja	.L108
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rax,4), %xmm0
.L109:
	mulsd	%xmm0, %xmm3
	movq	%rdi, %rcx
	subsd	%xmm3, %xmm2
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L103:
	testq	%rax, %rax
	je	.L122
	movq	72(%rdi), %rcx
	movq	(%rdi), %rax
	pxor	%xmm1, %xmm1
	imulq	$2832, %rcx, %rdx
	movq	%rax, %r12
	addq	%r15, %rdx
	leaq	(%rdx,%rax,4), %rsi
	andl	$1, %r12d
	jne	.L175
	movapd	%xmm1, %xmm2
	cmpq	%rsi, %rdx
	jb	.L133
	movapd	%xmm1, %xmm4
.L124:
	leaq	-5728(%rbp), %r9
	xorl	%edx, %edx
	leaq	-5760(%rbp), %r11
	movq	%r9, %r12
	leaq	-5744(%rbp), %r10
.L159:
	imulq	$2832, %rcx, %rsi
	movq	%r12, %rdi
	movl	$354, %ecx
	movq	%r12, %r8
	addq	%r15, %rsi
	rep movsq
	imulq	$2832, 80(%rbx,%rdx,8), %rsi
	addq	%r15, %rsi
	movq	2816(%rsi), %rcx
	addq	%rcx, 2816(%r12)
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L140:
	movdqu	(%rsi,%rcx), %xmm0
	paddd	(%r12,%rcx), %xmm0
	movaps	%xmm0, (%r12,%rcx)
	addq	$16, %rcx
	cmpq	$2816, %rcx
	jne	.L140
	leaq	(%r12,%rax,4), %rcx
	andl	$1, %eax
	jne	.L180
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	cmpq	%rcx, %r12
	jb	.L151
.L142:
	movsd	%xmm2, (%r11,%rdx,8)
	subsd	%xmm4, %xmm2
	subsd	96(%rbx,%rdx,8), %xmm2
	addq	$2832, %r12
	movsd	%xmm2, (%r10,%rdx,8)
	cmpq	$1, %rdx
	jne	.L199
	movq	24(%rbx), %rax
	movq	(%r14), %rdx
	movsd	-5744(%rbp), %xmm0
	movsd	-5736(%rbp), %xmm1
	movq	24(%r14), %rcx
	movq	64(%rbx), %rdi
	leaq	0(,%rax,4), %rsi
	cmpq	$255, %rdx
	ja	.L160
	movsd	16(%rbx), %xmm2
	comisd	%xmm2, %xmm0
	jbe	.L160
	comisd	%xmm2, %xmm1
	jbe	.L160
	movl	%edi, (%rcx,%rax,4)
	movq	16(%r14), %rcx
	movb	%dl, (%rcx,%rax)
	movq	80(%rbx), %rax
	movsd	96(%rbx), %xmm0
	movq	%rax, 88(%rbx)
	movq	(%r14), %rax
	addq	$1, 24(%rbx)
	movzbl	%al, %edx
	addq	$1, %rax
	movsd	%xmm0, 104(%rbx)
	movq	%rdx, 80(%rbx)
	movq	48(%rbx), %rdx
	movsd	%xmm4, 96(%rbx)
	movq	%rax, (%r14)
	movq	72(%rbx), %rax
	addq	$1, %rax
	movq	%rax, 72(%rbx)
	cmpq	(%rdx), %rax
	jb	.L200
.L163:
	movq	8(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	$0, 112(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L170:
	movapd	%xmm1, %xmm2
.L104:
	movl	(%rcx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rcx), %rdi
	cvtsi2sdq	%rax, %xmm3
	leaq	(%rax,%rdx), %r12
	cmpq	$255, %rax
	ja	.L112
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L113:
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm2
	cmpq	%rdi, %rsi
	ja	.L172
	testq	%r12, %r12
	je	.L115
	js	.L116
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r12, %xmm1
.L117:
	cmpq	$255, %r12
	ja	.L118
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L119:
	mulsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
.L115:
	maxsd	%xmm2, %xmm1
	jmp	.L105
.L108:
	movapd	%xmm3, %xmm0
	movq	%rdi, -5792(%rbp)
	movq	%rsi, -5784(%rbp)
	movq	%rdx, -5776(%rbp)
	movsd	%xmm1, -5808(%rbp)
	movsd	%xmm2, -5800(%rbp)
	movsd	%xmm3, -5768(%rbp)
	call	log2@PLT
	movsd	-5808(%rbp), %xmm1
	movsd	-5800(%rbp), %xmm2
	movq	-5792(%rbp), %rdi
	movq	-5784(%rbp), %rsi
	movq	-5776(%rbp), %rdx
	movsd	-5768(%rbp), %xmm3
	jmp	.L109
.L175:
	movapd	%xmm1, %xmm2
	xorl	%r12d, %r12d
.L123:
	movl	(%rdx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rdx), %rcx
	cvtsi2sdq	%rax, %xmm3
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L131
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L132:
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm2
	cmpq	%rcx, %rsi
	ja	.L177
	testq	%r12, %r12
	je	.L178
	js	.L135
	pxor	%xmm4, %xmm4
	cvtsi2sdq	%r12, %xmm4
.L136:
	cmpq	$255, %r12
	ja	.L137
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L138:
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm2
.L134:
	maxsd	%xmm2, %xmm4
	movq	72(%rbx), %rcx
	movq	(%rbx), %rax
	jmp	.L124
.L197:
	imulq	$2832, %rax, %rax
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 2808(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2816(%r15)
	movq	%rax, 2824(%r15)
	jmp	.L121
.L182:
	movq	%rsi, %r8
.L151:
	movl	(%r8), %edi
	pxor	%xmm2, %xmm2
	leaq	4(%r8), %rsi
	cvtsi2sdq	%rdi, %xmm2
	addq	%rdi, %rax
	cmpq	$255, %rdi
	ja	.L145
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r8,%rdi,4), %xmm0
.L146:
	mulsd	%xmm0, %xmm2
	movq	%rsi, %r8
	subsd	%xmm2, %xmm3
.L141:
	movl	(%r8), %edi
	pxor	%xmm2, %xmm2
	leaq	4(%r8), %rsi
	cvtsi2sdq	%rdi, %xmm2
	addq	%rdi, %rax
	cmpq	$255, %rdi
	ja	.L149
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r8,%rdi,4), %xmm0
.L150:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm3
	cmpq	%rsi, %rcx
	ja	.L182
	testq	%rax, %rax
	je	.L183
	js	.L153
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L154:
	cmpq	$255, %rax
	ja	.L155
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L156:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm3
.L152:
	maxsd	%xmm3, %xmm2
	jmp	.L142
.L160:
	subsd	.LC2(%rip), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L201
	imulq	$2832, 80(%rbx), %rax
	movsd	-5760(%rbp), %xmm0
	addl	%edi, -4(%rcx,%rsi)
	movl	$354, %ecx
	movq	%r9, %rsi
	addq	%r15, %rax
	movq	%rax, %rdi
	rep movsq
	movsd	%xmm0, 96(%rbx)
	cmpq	$1, %rdx
	jne	.L167
	movsd	%xmm0, 104(%rbx)
.L167:
	imulq	$2832, 72(%rbx), %rax
	movq	$0, 64(%rbx)
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 2808(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2816(%r15)
	movq	%rax, 2824(%r15)
	movq	112(%rbx), %rax
	addq	$1, %rax
	movq	%rax, 112(%rbx)
	cmpq	$1, %rax
	jbe	.L122
	movq	8(%rbx), %rax
	addq	%rax, 56(%rbx)
	jmp	.L122
.L177:
	movq	%rcx, %rdx
.L133:
	movl	(%rdx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rdx), %rcx
	cvtsi2sdq	%rax, %xmm3
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L127
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L128:
	mulsd	%xmm0, %xmm3
	movq	%rcx, %rdx
	subsd	%xmm3, %xmm2
	jmp	.L123
.L112:
	movapd	%xmm3, %xmm0
	movq	%rdi, -5792(%rbp)
	movq	%rsi, -5776(%rbp)
	movsd	%xmm1, -5800(%rbp)
	movsd	%xmm2, -5784(%rbp)
	movsd	%xmm3, -5768(%rbp)
	call	log2@PLT
	movsd	-5800(%rbp), %xmm1
	movq	-5792(%rbp), %rdi
	movsd	-5784(%rbp), %xmm2
	movq	-5776(%rbp), %rsi
	movsd	-5768(%rbp), %xmm3
	jmp	.L113
.L127:
	movapd	%xmm3, %xmm0
	movq	%rcx, -5784(%rbp)
	movq	%rsi, -5776(%rbp)
	movsd	%xmm1, -5800(%rbp)
	movsd	%xmm2, -5792(%rbp)
	movsd	%xmm3, -5768(%rbp)
	call	log2@PLT
	movsd	-5800(%rbp), %xmm1
	movsd	-5792(%rbp), %xmm2
	movq	-5784(%rbp), %rcx
	movq	-5776(%rbp), %rsi
	movsd	-5768(%rbp), %xmm3
	jmp	.L128
.L118:
	movapd	%xmm1, %xmm0
	movsd	%xmm2, -5776(%rbp)
	movsd	%xmm1, -5768(%rbp)
	call	log2@PLT
	movsd	-5776(%rbp), %xmm2
	movsd	-5768(%rbp), %xmm1
	jmp	.L119
.L116:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L117
.L131:
	movapd	%xmm3, %xmm0
	movq	%rcx, -5792(%rbp)
	movq	%rsi, -5776(%rbp)
	movsd	%xmm1, -5800(%rbp)
	movsd	%xmm2, -5784(%rbp)
	movsd	%xmm3, -5768(%rbp)
	call	log2@PLT
	movsd	-5800(%rbp), %xmm1
	movq	-5792(%rbp), %rcx
	movsd	-5784(%rbp), %xmm2
	movq	-5776(%rbp), %rsi
	movsd	-5768(%rbp), %xmm3
	jmp	.L132
.L145:
	movapd	%xmm2, %xmm0
	movq	%r9, -5848(%rbp)
	movq	%r10, -5840(%rbp)
	movq	%r11, -5832(%rbp)
	movq	%rdx, -5824(%rbp)
	movq	%rax, -5800(%rbp)
	movq	%rsi, -5792(%rbp)
	movq	%rcx, -5784(%rbp)
	movsd	%xmm1, -5816(%rbp)
	movsd	%xmm3, -5808(%rbp)
	movsd	%xmm4, -5776(%rbp)
	movsd	%xmm2, -5768(%rbp)
	call	log2@PLT
	movq	-5848(%rbp), %r9
	movq	-5840(%rbp), %r10
	movq	-5832(%rbp), %r11
	movq	-5824(%rbp), %rdx
	movsd	-5816(%rbp), %xmm1
	movsd	-5808(%rbp), %xmm3
	movq	-5800(%rbp), %rax
	movq	-5792(%rbp), %rsi
	movq	-5784(%rbp), %rcx
	movsd	-5776(%rbp), %xmm4
	movsd	-5768(%rbp), %xmm2
	jmp	.L146
.L201:
	movq	16(%r14), %rdx
	movl	%edi, (%rcx,%rax,4)
	leaq	-2896(%rbp), %rsi
	movzbl	-2(%rdx,%rax), %ecx
	movb	%cl, (%rdx,%rax)
	movq	88(%rbx), %rdx
	movl	$354, %eax
	movdqa	80(%rbx), %xmm0
	movq	%rax, %rcx
	imulq	$2832, %rdx, %rdx
	shufpd	$1, %xmm0, %xmm0
	movaps	%xmm0, 80(%rbx)
	movsd	-5752(%rbp), %xmm0
	addq	%r15, %rdx
	movq	%rdx, %rdi
	imulq	$2832, 72(%rbx), %rdx
	rep movsq
	addq	$1, 24(%rbx)
	movq	$0, 64(%rbx)
	addq	%rdx, %r15
	leaq	8(%r15), %rdi
	andq	$-8, %rdi
	movq	%rcx, %rax
	movq	%r15, %rcx
	movhpd	96(%rbx), %xmm0
	subq	%rdi, %rcx
	movaps	%xmm0, 96(%rbx)
	addl	$2816, %ecx
	movq	$0, (%r15)
	movq	$0, 2808(%r15)
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2816(%r15)
	movq	%rax, 2824(%r15)
	movq	8(%rbx), %rax
	movq	$0, 112(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L122
.L180:
	movapd	%xmm1, %xmm3
	xorl	%eax, %eax
	jmp	.L141
.L137:
	movapd	%xmm4, %xmm0
	movsd	%xmm1, -5784(%rbp)
	movsd	%xmm2, -5776(%rbp)
	movsd	%xmm4, -5768(%rbp)
	call	log2@PLT
	movsd	-5784(%rbp), %xmm1
	movsd	-5776(%rbp), %xmm2
	movsd	-5768(%rbp), %xmm4
	jmp	.L138
.L135:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm4, %xmm4
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm4
	addsd	%xmm4, %xmm4
	jmp	.L136
.L149:
	movapd	%xmm2, %xmm0
	movq	%r9, -5848(%rbp)
	movq	%r10, -5840(%rbp)
	movq	%r11, -5832(%rbp)
	movq	%rdx, -5824(%rbp)
	movq	%rsi, -5808(%rbp)
	movq	%rax, -5792(%rbp)
	movq	%rcx, -5784(%rbp)
	movsd	%xmm1, -5816(%rbp)
	movsd	%xmm3, -5800(%rbp)
	movsd	%xmm4, -5776(%rbp)
	movsd	%xmm2, -5768(%rbp)
	call	log2@PLT
	movq	-5848(%rbp), %r9
	movq	-5840(%rbp), %r10
	movq	-5832(%rbp), %r11
	movq	-5824(%rbp), %rdx
	movsd	-5816(%rbp), %xmm1
	movq	-5808(%rbp), %rsi
	movsd	-5800(%rbp), %xmm3
	movq	-5792(%rbp), %rax
	movq	-5784(%rbp), %rcx
	movsd	-5776(%rbp), %xmm4
	movsd	-5768(%rbp), %xmm2
	jmp	.L150
.L155:
	movapd	%xmm2, %xmm0
	movq	%r9, -5824(%rbp)
	movq	%r10, -5816(%rbp)
	movq	%r11, -5808(%rbp)
	movq	%rdx, -5800(%rbp)
	movsd	%xmm1, -5792(%rbp)
	movsd	%xmm3, -5784(%rbp)
	movsd	%xmm4, -5776(%rbp)
	movsd	%xmm2, -5768(%rbp)
	call	log2@PLT
	movq	-5824(%rbp), %r9
	movq	-5816(%rbp), %r10
	movq	-5808(%rbp), %r11
	movq	-5800(%rbp), %rdx
	movsd	-5792(%rbp), %xmm1
	movsd	-5784(%rbp), %xmm3
	movsd	-5776(%rbp), %xmm4
	movsd	-5768(%rbp), %xmm2
	jmp	.L156
.L153:
	movq	%rax, %rcx
	movq	%rax, %rsi
	pxor	%xmm2, %xmm2
	shrq	%rcx
	andl	$1, %esi
	orq	%rsi, %rcx
	cvtsi2sdq	%rcx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L154
.L178:
	movapd	%xmm1, %xmm4
	jmp	.L134
.L183:
	movapd	%xmm1, %xmm2
	jmp	.L152
.L200:
	imulq	$2832, %rax, %rax
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 2808(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2816(%r15)
	movq	%rax, 2824(%r15)
	jmp	.L163
.L198:
	call	__stack_chk_fail@PLT
.L199:
	movq	72(%rbx), %rcx
	movq	(%rbx), %rax
	movl	$1, %edx
	jmp	.L159
	.cfi_endproc
.LFE105:
	.size	BlockSplitterFinishBlockCommand, .-BlockSplitterFinishBlockCommand
	.p2align 4
	.type	BlockSplitterFinishBlockDistance, @function
BlockSplitterFinishBlockDistance:
.LFB108:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$440, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rdx
	movq	32(%rdi), %r14
	movq	40(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	movq	%rdi, %rbx
	cmovnb	8(%rdi), %rax
	movl	%esi, %r13d
	movq	%rax, 64(%rdi)
	testq	%rdx, %rdx
	jne	.L203
	movq	24(%r14), %rcx
	pxor	%xmm1, %xmm1
	movl	%eax, (%rcx)
	movq	16(%r14), %rax
	movq	%r15, %rcx
	movb	$0, (%rax)
	movq	(%rdi), %rax
	leaq	(%r15,%rax,4), %rsi
	andl	$1, %eax
	movq	%rax, %r12
	jne	.L270
	movapd	%xmm1, %xmm2
	cmpq	%rsi, %r15
	jb	.L214
.L205:
	movq	72(%rbx), %rax
	movq	48(%rbx), %rdx
	unpcklpd	%xmm1, %xmm1
	addq	$1, 24(%rbx)
	addq	$1, %rax
	movaps	%xmm1, 96(%rbx)
	addq	$1, (%r14)
	movq	%rax, 72(%rbx)
	cmpq	(%rdx), %rax
	jb	.L297
.L221:
	movq	$0, 64(%rbx)
.L222:
	testl	%r13d, %r13d
	je	.L202
	movq	48(%rbx), %rax
	movq	(%r14), %rdx
	movq	%rdx, (%rax)
	movq	24(%rbx), %rax
	movq	%rax, 8(%r14)
.L202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$4536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L272:
	.cfi_restore_state
	movq	%rdi, %rcx
.L214:
	movl	(%rcx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rcx), %rdi
	cvtsi2sdq	%rax, %xmm3
	leaq	(%rax,%r12), %rdx
	cmpq	$255, %rax
	ja	.L208
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rax,4), %xmm0
.L209:
	mulsd	%xmm0, %xmm3
	movq	%rdi, %rcx
	subsd	%xmm3, %xmm2
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L203:
	testq	%rax, %rax
	je	.L222
	movq	72(%rdi), %rcx
	movq	(%rdi), %rax
	pxor	%xmm1, %xmm1
	imulq	$2192, %rcx, %rdx
	movq	%rax, %r12
	addq	%r15, %rdx
	leaq	(%rdx,%rax,4), %rsi
	andl	$1, %r12d
	jne	.L275
	movapd	%xmm1, %xmm2
	cmpq	%rsi, %rdx
	jb	.L233
	movapd	%xmm1, %xmm4
.L224:
	leaq	-4448(%rbp), %r9
	xorl	%edx, %edx
	leaq	-4480(%rbp), %r11
	movq	%r9, %r12
	leaq	-4464(%rbp), %r10
.L259:
	imulq	$2192, %rcx, %rsi
	movq	%r12, %rdi
	movl	$274, %ecx
	movq	%r12, %r8
	addq	%r15, %rsi
	rep movsq
	imulq	$2192, 80(%rbx,%rdx,8), %rsi
	addq	%r15, %rsi
	movq	2176(%rsi), %rcx
	addq	%rcx, 2176(%r12)
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L240:
	movdqu	(%rsi,%rcx), %xmm0
	paddd	(%r12,%rcx), %xmm0
	movaps	%xmm0, (%r12,%rcx)
	addq	$16, %rcx
	cmpq	$2176, %rcx
	jne	.L240
	leaq	(%r12,%rax,4), %rcx
	andl	$1, %eax
	jne	.L280
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	cmpq	%rcx, %r12
	jb	.L251
.L242:
	movsd	%xmm2, (%r11,%rdx,8)
	subsd	%xmm4, %xmm2
	subsd	96(%rbx,%rdx,8), %xmm2
	addq	$2192, %r12
	movsd	%xmm2, (%r10,%rdx,8)
	cmpq	$1, %rdx
	jne	.L299
	movq	24(%rbx), %rax
	movq	(%r14), %rdx
	movsd	-4464(%rbp), %xmm0
	movsd	-4456(%rbp), %xmm1
	movq	24(%r14), %rcx
	movq	64(%rbx), %rdi
	leaq	0(,%rax,4), %rsi
	cmpq	$255, %rdx
	ja	.L260
	movsd	16(%rbx), %xmm2
	comisd	%xmm2, %xmm0
	jbe	.L260
	comisd	%xmm2, %xmm1
	jbe	.L260
	movl	%edi, (%rcx,%rax,4)
	movq	16(%r14), %rcx
	movb	%dl, (%rcx,%rax)
	movq	80(%rbx), %rax
	movsd	96(%rbx), %xmm0
	movq	%rax, 88(%rbx)
	movq	(%r14), %rax
	addq	$1, 24(%rbx)
	movzbl	%al, %edx
	addq	$1, %rax
	movsd	%xmm0, 104(%rbx)
	movq	%rdx, 80(%rbx)
	movq	48(%rbx), %rdx
	movsd	%xmm4, 96(%rbx)
	movq	%rax, (%r14)
	movq	72(%rbx), %rax
	addq	$1, %rax
	movq	%rax, 72(%rbx)
	cmpq	(%rdx), %rax
	jb	.L300
.L263:
	movq	8(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	$0, 112(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L270:
	movapd	%xmm1, %xmm2
.L204:
	movl	(%rcx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rcx), %rdi
	cvtsi2sdq	%rax, %xmm3
	leaq	(%rax,%rdx), %r12
	cmpq	$255, %rax
	ja	.L212
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L213:
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm2
	cmpq	%rdi, %rsi
	ja	.L272
	testq	%r12, %r12
	je	.L215
	js	.L216
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r12, %xmm1
.L217:
	cmpq	$255, %r12
	ja	.L218
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L219:
	mulsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
.L215:
	maxsd	%xmm2, %xmm1
	jmp	.L205
.L208:
	movapd	%xmm3, %xmm0
	movq	%rdi, -4512(%rbp)
	movq	%rsi, -4504(%rbp)
	movq	%rdx, -4496(%rbp)
	movsd	%xmm1, -4528(%rbp)
	movsd	%xmm2, -4520(%rbp)
	movsd	%xmm3, -4488(%rbp)
	call	log2@PLT
	movsd	-4528(%rbp), %xmm1
	movsd	-4520(%rbp), %xmm2
	movq	-4512(%rbp), %rdi
	movq	-4504(%rbp), %rsi
	movq	-4496(%rbp), %rdx
	movsd	-4488(%rbp), %xmm3
	jmp	.L209
.L275:
	movapd	%xmm1, %xmm2
	xorl	%r12d, %r12d
.L223:
	movl	(%rdx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rdx), %rcx
	cvtsi2sdq	%rax, %xmm3
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L231
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L232:
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm2
	cmpq	%rcx, %rsi
	ja	.L277
	testq	%r12, %r12
	je	.L278
	js	.L235
	pxor	%xmm4, %xmm4
	cvtsi2sdq	%r12, %xmm4
.L236:
	cmpq	$255, %r12
	ja	.L237
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L238:
	mulsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm2
.L234:
	maxsd	%xmm2, %xmm4
	movq	72(%rbx), %rcx
	movq	(%rbx), %rax
	jmp	.L224
.L297:
	imulq	$2192, %rax, %rax
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 2168(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2176(%r15)
	movq	%rax, 2184(%r15)
	jmp	.L221
.L282:
	movq	%rsi, %r8
.L251:
	movl	(%r8), %edi
	pxor	%xmm2, %xmm2
	leaq	4(%r8), %rsi
	cvtsi2sdq	%rdi, %xmm2
	addq	%rdi, %rax
	cmpq	$255, %rdi
	ja	.L245
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r8,%rdi,4), %xmm0
.L246:
	mulsd	%xmm0, %xmm2
	movq	%rsi, %r8
	subsd	%xmm2, %xmm3
.L241:
	movl	(%r8), %edi
	pxor	%xmm2, %xmm2
	leaq	4(%r8), %rsi
	cvtsi2sdq	%rdi, %xmm2
	addq	%rdi, %rax
	cmpq	$255, %rdi
	ja	.L249
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r8,%rdi,4), %xmm0
.L250:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm3
	cmpq	%rsi, %rcx
	ja	.L282
	testq	%rax, %rax
	je	.L283
	js	.L253
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L254:
	cmpq	$255, %rax
	ja	.L255
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L256:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm3
.L252:
	maxsd	%xmm3, %xmm2
	jmp	.L242
.L260:
	subsd	.LC2(%rip), %xmm0
	comisd	%xmm1, %xmm0
	ja	.L301
	imulq	$2192, 80(%rbx), %rax
	movsd	-4480(%rbp), %xmm0
	addl	%edi, -4(%rcx,%rsi)
	movl	$274, %ecx
	movq	%r9, %rsi
	addq	%r15, %rax
	movq	%rax, %rdi
	rep movsq
	movsd	%xmm0, 96(%rbx)
	cmpq	$1, %rdx
	jne	.L267
	movsd	%xmm0, 104(%rbx)
.L267:
	imulq	$2192, 72(%rbx), %rax
	movq	$0, 64(%rbx)
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 2168(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2176(%r15)
	movq	%rax, 2184(%r15)
	movq	112(%rbx), %rax
	addq	$1, %rax
	movq	%rax, 112(%rbx)
	cmpq	$1, %rax
	jbe	.L222
	movq	8(%rbx), %rax
	addq	%rax, 56(%rbx)
	jmp	.L222
.L277:
	movq	%rcx, %rdx
.L233:
	movl	(%rdx), %eax
	pxor	%xmm3, %xmm3
	leaq	4(%rdx), %rcx
	cvtsi2sdq	%rax, %xmm3
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L227
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L228:
	mulsd	%xmm0, %xmm3
	movq	%rcx, %rdx
	subsd	%xmm3, %xmm2
	jmp	.L223
.L212:
	movapd	%xmm3, %xmm0
	movq	%rdi, -4512(%rbp)
	movq	%rsi, -4496(%rbp)
	movsd	%xmm1, -4520(%rbp)
	movsd	%xmm2, -4504(%rbp)
	movsd	%xmm3, -4488(%rbp)
	call	log2@PLT
	movsd	-4520(%rbp), %xmm1
	movq	-4512(%rbp), %rdi
	movsd	-4504(%rbp), %xmm2
	movq	-4496(%rbp), %rsi
	movsd	-4488(%rbp), %xmm3
	jmp	.L213
.L227:
	movapd	%xmm3, %xmm0
	movq	%rcx, -4504(%rbp)
	movq	%rsi, -4496(%rbp)
	movsd	%xmm1, -4520(%rbp)
	movsd	%xmm2, -4512(%rbp)
	movsd	%xmm3, -4488(%rbp)
	call	log2@PLT
	movsd	-4520(%rbp), %xmm1
	movsd	-4512(%rbp), %xmm2
	movq	-4504(%rbp), %rcx
	movq	-4496(%rbp), %rsi
	movsd	-4488(%rbp), %xmm3
	jmp	.L228
.L218:
	movapd	%xmm1, %xmm0
	movsd	%xmm2, -4496(%rbp)
	movsd	%xmm1, -4488(%rbp)
	call	log2@PLT
	movsd	-4496(%rbp), %xmm2
	movsd	-4488(%rbp), %xmm1
	jmp	.L219
.L216:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L217
.L231:
	movapd	%xmm3, %xmm0
	movq	%rcx, -4512(%rbp)
	movq	%rsi, -4496(%rbp)
	movsd	%xmm1, -4520(%rbp)
	movsd	%xmm2, -4504(%rbp)
	movsd	%xmm3, -4488(%rbp)
	call	log2@PLT
	movsd	-4520(%rbp), %xmm1
	movq	-4512(%rbp), %rcx
	movsd	-4504(%rbp), %xmm2
	movq	-4496(%rbp), %rsi
	movsd	-4488(%rbp), %xmm3
	jmp	.L232
.L245:
	movapd	%xmm2, %xmm0
	movq	%r9, -4568(%rbp)
	movq	%r10, -4560(%rbp)
	movq	%r11, -4552(%rbp)
	movq	%rdx, -4544(%rbp)
	movq	%rax, -4520(%rbp)
	movq	%rsi, -4512(%rbp)
	movq	%rcx, -4504(%rbp)
	movsd	%xmm1, -4536(%rbp)
	movsd	%xmm3, -4528(%rbp)
	movsd	%xmm4, -4496(%rbp)
	movsd	%xmm2, -4488(%rbp)
	call	log2@PLT
	movq	-4568(%rbp), %r9
	movq	-4560(%rbp), %r10
	movq	-4552(%rbp), %r11
	movq	-4544(%rbp), %rdx
	movsd	-4536(%rbp), %xmm1
	movsd	-4528(%rbp), %xmm3
	movq	-4520(%rbp), %rax
	movq	-4512(%rbp), %rsi
	movq	-4504(%rbp), %rcx
	movsd	-4496(%rbp), %xmm4
	movsd	-4488(%rbp), %xmm2
	jmp	.L246
.L301:
	movq	16(%r14), %rdx
	movl	%edi, (%rcx,%rax,4)
	leaq	-2256(%rbp), %rsi
	movzbl	-2(%rdx,%rax), %ecx
	movb	%cl, (%rdx,%rax)
	movq	88(%rbx), %rdx
	movl	$274, %eax
	movdqa	80(%rbx), %xmm0
	movq	%rax, %rcx
	imulq	$2192, %rdx, %rdx
	shufpd	$1, %xmm0, %xmm0
	movaps	%xmm0, 80(%rbx)
	movsd	-4472(%rbp), %xmm0
	addq	%r15, %rdx
	movq	%rdx, %rdi
	imulq	$2192, 72(%rbx), %rdx
	rep movsq
	addq	$1, 24(%rbx)
	movq	$0, 64(%rbx)
	addq	%rdx, %r15
	leaq	8(%r15), %rdi
	andq	$-8, %rdi
	movq	%rcx, %rax
	movq	%r15, %rcx
	movhpd	96(%rbx), %xmm0
	subq	%rdi, %rcx
	movaps	%xmm0, 96(%rbx)
	addl	$2176, %ecx
	movq	$0, (%r15)
	movq	$0, 2168(%r15)
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2176(%r15)
	movq	%rax, 2184(%r15)
	movq	8(%rbx), %rax
	movq	$0, 112(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L222
.L280:
	movapd	%xmm1, %xmm3
	xorl	%eax, %eax
	jmp	.L241
.L237:
	movapd	%xmm4, %xmm0
	movsd	%xmm1, -4504(%rbp)
	movsd	%xmm2, -4496(%rbp)
	movsd	%xmm4, -4488(%rbp)
	call	log2@PLT
	movsd	-4504(%rbp), %xmm1
	movsd	-4496(%rbp), %xmm2
	movsd	-4488(%rbp), %xmm4
	jmp	.L238
.L235:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm4, %xmm4
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm4
	addsd	%xmm4, %xmm4
	jmp	.L236
.L249:
	movapd	%xmm2, %xmm0
	movq	%r9, -4568(%rbp)
	movq	%r10, -4560(%rbp)
	movq	%r11, -4552(%rbp)
	movq	%rdx, -4544(%rbp)
	movq	%rsi, -4528(%rbp)
	movq	%rax, -4512(%rbp)
	movq	%rcx, -4504(%rbp)
	movsd	%xmm1, -4536(%rbp)
	movsd	%xmm3, -4520(%rbp)
	movsd	%xmm4, -4496(%rbp)
	movsd	%xmm2, -4488(%rbp)
	call	log2@PLT
	movq	-4568(%rbp), %r9
	movq	-4560(%rbp), %r10
	movq	-4552(%rbp), %r11
	movq	-4544(%rbp), %rdx
	movsd	-4536(%rbp), %xmm1
	movq	-4528(%rbp), %rsi
	movsd	-4520(%rbp), %xmm3
	movq	-4512(%rbp), %rax
	movq	-4504(%rbp), %rcx
	movsd	-4496(%rbp), %xmm4
	movsd	-4488(%rbp), %xmm2
	jmp	.L250
.L255:
	movapd	%xmm2, %xmm0
	movq	%r9, -4544(%rbp)
	movq	%r10, -4536(%rbp)
	movq	%r11, -4528(%rbp)
	movq	%rdx, -4520(%rbp)
	movsd	%xmm1, -4512(%rbp)
	movsd	%xmm3, -4504(%rbp)
	movsd	%xmm4, -4496(%rbp)
	movsd	%xmm2, -4488(%rbp)
	call	log2@PLT
	movq	-4544(%rbp), %r9
	movq	-4536(%rbp), %r10
	movq	-4528(%rbp), %r11
	movq	-4520(%rbp), %rdx
	movsd	-4512(%rbp), %xmm1
	movsd	-4504(%rbp), %xmm3
	movsd	-4496(%rbp), %xmm4
	movsd	-4488(%rbp), %xmm2
	jmp	.L256
.L253:
	movq	%rax, %rcx
	movq	%rax, %rsi
	pxor	%xmm2, %xmm2
	shrq	%rcx
	andl	$1, %esi
	orq	%rsi, %rcx
	cvtsi2sdq	%rcx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L254
.L278:
	movapd	%xmm1, %xmm4
	jmp	.L234
.L283:
	movapd	%xmm1, %xmm2
	jmp	.L252
.L300:
	imulq	$2192, %rax, %rax
	addq	%rax, %r15
	xorl	%eax, %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	$0, (%r15)
	movq	$0, 2168(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2176(%r15)
	movq	%rax, 2184(%r15)
	jmp	.L263
.L298:
	call	__stack_chk_fail@PLT
.L299:
	movq	72(%rbx), %rcx
	movq	(%rbx), %rax
	movl	$1, %edx
	jmp	.L259
	.cfi_endproc
.LFE108:
	.size	BlockSplitterFinishBlockDistance, .-BlockSplitterFinishBlockDistance
	.p2align 4
	.type	ContextBlockSplitterFinishBlock, @function
ContextBlockSplitterFinishBlock:
.LFB111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$520, %rsp
	movq	48(%rdi), %r10
	movq	8(%rdi), %r13
	movl	%edx, -404(%rbp)
	movq	24(%rdi), %rdx
	movq	56(%rdi), %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	112(%rdi), %rax
	movq	%rax, -416(%rbp)
	movq	80(%rdi), %rax
	cmpq	%rdx, %rax
	jnb	.L303
	movq	%rdx, 80(%rdi)
	movq	%rdx, %rax
.L303:
	cmpq	$0, 40(%rbx)
	jne	.L304
	movq	24(%r10), %rdx
	movl	%eax, (%rdx)
	movq	16(%r10), %rax
	movb	$0, (%rax)
	testq	%r13, %r13
	je	.L325
	leaq	(%rbx,%r13,8), %rsi
	xorl	%r14d, %r14d
	pxor	%xmm3, %xmm3
	movq	%rbx, %rdi
	movq	%r10, -424(%rbp)
	movq	%r14, %rbx
	leaq	kLog2Table(%rip), %rcx
	movq	%rsi, %r15
	movq	%r13, -416(%rbp)
	movq	%r11, %r14
.L324:
	movq	(%rdi), %rax
	movq	%r14, %rdx
	leaq	(%r14,%rax,4), %r13
	andl	$1, %eax
	movq	%rax, %r12
	jne	.L394
	movapd	%xmm3, %xmm1
	movapd	%xmm3, %xmm2
	cmpq	%r13, %r14
	jb	.L318
.L309:
	movsd	%xmm1, 112(%rdi,%rbx,8)
	addq	$1040, %r14
	movsd	%xmm1, 112(%r15,%rbx,8)
	addq	$1, %rbx
	cmpq	%rbx, -416(%rbp)
	jne	.L324
	movq	-424(%rbp), %r10
	movq	-416(%rbp), %r13
	movq	%rdi, %rbx
.L325:
	movq	88(%rbx), %rax
	movq	64(%rbx), %rdx
	addq	$1, 40(%rbx)
	addq	%r13, %rax
	addq	$1, (%r10)
	movq	%rax, 88(%rbx)
	cmpq	(%rdx), %rax
	jnb	.L307
	movq	8(%rbx), %rcx
	movq	56(%rbx), %rsi
	testq	%rcx, %rcx
	je	.L307
	movq	%rax, %rdx
	movsd	.LC1(%rip), %xmm0
	salq	$6, %rdx
	addq	%rax, %rdx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$4, %rdx
	salq	$6, %rcx
	addq	%rsi, %rdx
	addq	%rcx, %rax
	salq	$4, %rax
	addq	%rax, %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$1040, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	%xmm0, -8(%rdx)
	cmpq	%rsi, %rdx
	jne	.L327
.L307:
	movq	$0, 80(%rbx)
.L326:
	movl	-404(%rbp), %eax
	testl	%eax, %eax
	jne	.L498
.L302:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L499
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L326
	movq	$0, -424(%rbp)
	movq	%r13, %rax
	addq	%rax, %rax
	jne	.L500
.L328:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	testq	%r13, %r13
	je	.L329
	movq	%r13, %rax
	xorl	%r8d, %r8d
	movq	%r10, -480(%rbp)
	pxor	%xmm3, %xmm3
	salq	$6, %rax
	movq	%r13, -440(%rbp)
	addq	%r13, %rax
	movq	%r15, -488(%rbp)
	movq	%r8, %r15
	salq	$4, %rax
	movq	%rax, -456(%rbp)
	leaq	0(,%r13,8), %rax
	movq	%r11, %r13
	movq	%rax, -448(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, -432(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -464(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rax, -472(%rbp)
.L367:
	movq	88(%rbx), %rdx
	movq	(%rbx), %rax
	addq	%r15, %rdx
	movq	%rax, %r12
	movq	%rdx, %r14
	salq	$6, %r14
	addq	%rdx, %r14
	salq	$4, %r14
	addq	%r13, %r14
	movq	%r14, %rdx
	leaq	(%r14,%rax,4), %rcx
	andl	$1, %r12d
	jne	.L399
	movapd	%xmm3, %xmm1
	movapd	%xmm3, %xmm2
	cmpq	%rcx, %r14
	jb	.L340
.L331:
	movq	-432(%rbp), %rdi
	movq	%r15, %r12
	xorl	%r9d, %r9d
	leaq	0(,%r15,8), %r10
	salq	$6, %r12
	movsd	%xmm1, (%rdi,%r15,8)
	addq	%r15, %r12
	movq	%r9, %rdi
	movq	%r14, %r9
	salq	$4, %r12
	movq	%rdi, %r14
	addq	-424(%rbp), %r12
.L366:
	movq	96(%rbx,%r14,8), %r8
	movl	$130, %ecx
	movq	%r12, %rdi
	movq	%r9, %rsi
	rep movsq
	movq	%r12, %rdx
	addq	%r15, %r8
	movq	%r8, %rcx
	salq	$6, %rcx
	addq	%rcx, %r8
	salq	$4, %r8
	leaq	0(%r13,%r8), %rsi
	movq	1024(%rsi), %rcx
	addq	%rcx, 1024(%r12)
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L347:
	movdqu	(%rsi,%rcx), %xmm0
	movdqu	(%r12,%rcx), %xmm5
	paddd	%xmm5, %xmm0
	movups	%xmm0, (%r12,%rcx)
	addq	$16, %rcx
	cmpq	$1024, %rcx
	jne	.L347
	leaq	(%r12,%rax,4), %rcx
	andl	$1, %eax
	jne	.L404
	movapd	%xmm3, %xmm2
	movapd	%xmm3, %xmm4
	cmpq	%r12, %rcx
	ja	.L358
.L349:
	movq	-464(%rbp), %rax
	addq	-456(%rbp), %r12
	movsd	%xmm2, (%rax,%r10)
	subsd	%xmm1, %xmm2
	movq	-472(%rbp), %rax
	movapd	%xmm2, %xmm0
	subsd	112(%rbx,%r10), %xmm0
	addsd	(%rax,%r14,8), %xmm0
	addq	-448(%rbp), %r10
	movsd	%xmm0, (%rax,%r14,8)
	cmpq	$1, %r14
	jne	.L501
	addq	$1, %r15
	cmpq	%r15, -440(%rbp)
	jne	.L367
	movq	-480(%rbp), %r10
	movq	40(%rbx), %rcx
	movq	%r13, %r11
	movq	-488(%rbp), %r15
	movq	-440(%rbp), %r13
	movq	(%r10), %r12
	movq	24(%r10), %rax
	leaq	0(,%rcx,4), %rdx
	movsd	-400(%rbp), %xmm0
	movsd	-392(%rbp), %xmm1
	movl	80(%rbx), %esi
	cmpq	16(%rbx), %r12
	jnb	.L368
.L391:
	movsd	32(%rbx), %xmm2
	comisd	%xmm2, %xmm0
	jbe	.L368
	comisd	%xmm2, %xmm1
	jbe	.L368
	movl	%esi, (%rax,%rdx)
	movq	16(%r10), %rax
	movb	%r12b, (%rax,%rcx)
	movq	(%r10), %rcx
	movq	%rcx, %rax
	imulq	%r13, %rax
	movq	%rax, %xmm0
	movhps	96(%rbx), %xmm0
	movaps	%xmm0, 96(%rbx)
	testq	%r13, %r13
	je	.L376
	leaq	128(,%r13,8), %rdx
	leaq	-16(%rbx,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%rdx, -416(%rbp)
	leaq	128(%rbx), %rdx
	setnb	%sil
	cmpq	%rdx, %rax
	setnb	%dl
	orb	%dl, %sil
	je	.L374
	leaq	-1(%r13), %rdx
	cmpq	$1, %rdx
	jbe	.L374
	movapd	112(%rbx), %xmm6
	movq	%r13, %rdx
	shrq	%rdx
	movups	%xmm6, (%rax)
	movapd	-384(%rbp), %xmm7
	movaps	%xmm7, 112(%rbx)
	cmpq	$1, %rdx
	je	.L375
	movapd	128(%rbx), %xmm6
	movups	%xmm6, 16(%rax)
	movapd	-368(%rbp), %xmm7
	movaps	%xmm7, 128(%rbx)
	cmpq	$2, %rdx
	je	.L375
	movapd	144(%rbx), %xmm6
	movups	%xmm6, 32(%rax)
	movapd	-352(%rbp), %xmm7
	movaps	%xmm7, 144(%rbx)
	cmpq	$3, %rdx
	je	.L375
	movapd	160(%rbx), %xmm6
	movups	%xmm6, 48(%rax)
	movapd	-336(%rbp), %xmm7
	movaps	%xmm7, 160(%rbx)
	cmpq	$4, %rdx
	je	.L375
	movapd	176(%rbx), %xmm6
	movups	%xmm6, 64(%rax)
	movapd	-320(%rbp), %xmm7
	movaps	%xmm7, 176(%rbx)
	cmpq	$5, %rdx
	je	.L375
	movapd	192(%rbx), %xmm6
	movups	%xmm6, 80(%rax)
	movapd	-304(%rbp), %xmm7
	movaps	%xmm7, 192(%rbx)
.L375:
	movq	%r13, %rax
	andq	$-2, %rax
	testb	$1, %r13b
	je	.L376
	movq	-416(%rbp), %rdi
	leaq	0(%r13,%rax), %rsi
	leaq	(%rdi,%rax,8), %rdx
	movsd	(%rdx), %xmm0
	movsd	%xmm0, (%rdi,%rsi,8)
	movsd	-384(%rbp,%rax,8), %xmm0
	movsd	%xmm0, (%rdx)
.L376:
	movq	88(%rbx), %rax
	movq	64(%rbx), %rdx
	addq	$1, %rcx
	addq	$1, 40(%rbx)
	addq	%r13, %rax
	movq	%rcx, (%r10)
	movq	%rax, 88(%rbx)
	cmpq	(%rdx), %rax
	jnb	.L496
	movq	8(%rbx), %rcx
	movq	56(%rbx), %rsi
	testq	%rcx, %rcx
	je	.L496
	movq	%rax, %rdx
	movsd	.LC1(%rip), %xmm0
	salq	$6, %rdx
	addq	%rax, %rdx
	addq	%rcx, %rax
	movq	%rax, %rcx
	salq	$4, %rdx
	salq	$6, %rcx
	addq	%rsi, %rdx
	addq	%rcx, %rax
	salq	$4, %rax
	addq	%rax, %rsi
	xorl	%eax, %eax
.L380:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$1040, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	%xmm0, -8(%rdx)
	cmpq	%rsi, %rdx
	jne	.L380
.L496:
	movq	24(%rbx), %rax
	movq	$0, 80(%rbx)
	movq	$0, 320(%rbx)
	movq	%rax, 72(%rbx)
.L379:
	movq	-424(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r10, -416(%rbp)
	call	BrotliFree
	movq	-416(%rbp), %r10
	jmp	.L326
.L396:
	movq	%r9, %rdx
.L318:
	leaq	4(%rdx), %r9
	movl	(%rdx), %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	addq	%rdx, %r12
	cmpq	$255, %rdx
	ja	.L312
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
.L313:
	mulsd	%xmm0, %xmm1
	movq	%r9, %rdx
	subsd	%xmm1, %xmm2
.L308:
	leaq	4(%rdx), %r9
	movl	(%rdx), %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	addq	%rdx, %r12
	cmpq	$255, %rdx
	ja	.L316
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
.L317:
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	cmpq	%r9, %r13
	ja	.L396
	testq	%r12, %r12
	je	.L397
	js	.L320
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r12, %xmm1
.L321:
	cmpq	$255, %r12
	ja	.L322
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%r12,4), %xmm0
.L323:
	mulsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
.L319:
	maxsd	%xmm2, %xmm1
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L394:
	xorl	%r12d, %r12d
	movapd	%xmm3, %xmm2
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L498:
	imulq	(%r10), %r13
	movq	64(%rbx), %rax
	movq	%r13, (%rax)
	movq	40(%rbx), %rax
	movq	%rax, 8(%r10)
	jmp	.L302
.L316:
	movapd	%xmm1, %xmm0
	movq	%rdi, -464(%rbp)
	movq	%r9, -448(%rbp)
	movsd	%xmm3, -456(%rbp)
	movsd	%xmm2, -440(%rbp)
	movsd	%xmm1, -432(%rbp)
	call	log2@PLT
	movq	-464(%rbp), %rdi
	movsd	-456(%rbp), %xmm3
	leaq	kLog2Table(%rip), %rcx
	movq	-448(%rbp), %r9
	movsd	-440(%rbp), %xmm2
	movsd	-432(%rbp), %xmm1
	jmp	.L317
.L399:
	movapd	%xmm3, %xmm2
	xorl	%r12d, %r12d
.L330:
	movl	(%rdx), %eax
	pxor	%xmm1, %xmm1
	leaq	4(%rdx), %rsi
	cvtsi2sdq	%rax, %xmm1
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L338
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L339:
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	cmpq	%rsi, %rcx
	ja	.L401
	testq	%r12, %r12
	je	.L402
	js	.L342
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r12, %xmm1
.L343:
	cmpq	$255, %r12
	ja	.L344
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L345:
	mulsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
.L341:
	maxsd	%xmm2, %xmm1
	movq	(%rbx), %rax
	jmp	.L331
.L500:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r11, -440(%rbp)
	salq	$6, %rsi
	movq	%r10, -432(%rbp)
	addq	%r13, %rsi
	salq	$5, %rsi
	call	BrotliAllocate
	movq	-440(%rbp), %r11
	movq	-432(%rbp), %r10
	movq	%rax, -424(%rbp)
	jmp	.L328
.L406:
	movq	%rsi, %rdx
.L358:
	leaq	4(%rdx), %rsi
	movl	(%rdx), %edx
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	addq	%rdx, %rax
	cmpq	$255, %rdx
	ja	.L352
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rdx,4), %xmm0
.L353:
	mulsd	%xmm0, %xmm2
	movq	%rsi, %rdx
	subsd	%xmm2, %xmm4
.L348:
	leaq	4(%rdx), %rsi
	movl	(%rdx), %edx
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	addq	%rdx, %rax
	cmpq	$255, %rdx
	ja	.L356
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rdx,4), %xmm0
.L357:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm4
	cmpq	%rsi, %rcx
	ja	.L406
	testq	%rax, %rax
	je	.L407
	js	.L360
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L361:
	cmpq	$255, %rax
	ja	.L362
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L363:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm4
.L359:
	maxsd	%xmm4, %xmm2
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L312:
	movapd	%xmm1, %xmm0
	movq	%rdi, -464(%rbp)
	movq	%r9, -440(%rbp)
	movsd	%xmm3, -456(%rbp)
	movsd	%xmm2, -448(%rbp)
	movsd	%xmm1, -432(%rbp)
	call	log2@PLT
	movq	-464(%rbp), %rdi
	movsd	-456(%rbp), %xmm3
	leaq	kLog2Table(%rip), %rcx
	movsd	-448(%rbp), %xmm2
	movq	-440(%rbp), %r9
	movsd	-432(%rbp), %xmm1
	jmp	.L313
.L401:
	movq	%rsi, %rdx
.L340:
	movl	(%rdx), %eax
	pxor	%xmm1, %xmm1
	leaq	4(%rdx), %rsi
	cvtsi2sdq	%rax, %xmm1
	addq	%rax, %r12
	cmpq	$255, %rax
	ja	.L334
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L335:
	mulsd	%xmm0, %xmm1
	movq	%rsi, %rdx
	subsd	%xmm1, %xmm2
	jmp	.L330
.L322:
	movapd	%xmm1, %xmm0
	movq	%rdi, -456(%rbp)
	movsd	%xmm3, -448(%rbp)
	movsd	%xmm2, -440(%rbp)
	movsd	%xmm1, -432(%rbp)
	call	log2@PLT
	movq	-456(%rbp), %rdi
	movsd	-448(%rbp), %xmm3
	leaq	kLog2Table(%rip), %rcx
	movsd	-440(%rbp), %xmm2
	movsd	-432(%rbp), %xmm1
	jmp	.L323
.L320:
	movq	%r12, %rdx
	movq	%r12, %r9
	pxor	%xmm1, %xmm1
	shrq	%rdx
	andl	$1, %r9d
	orq	%r9, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L321
.L334:
	movapd	%xmm1, %xmm0
	movq	%rsi, -512(%rbp)
	movq	%rcx, -504(%rbp)
	movsd	%xmm3, -528(%rbp)
	movsd	%xmm2, -520(%rbp)
	movsd	%xmm1, -496(%rbp)
	call	log2@PLT
	movsd	-528(%rbp), %xmm3
	movsd	-520(%rbp), %xmm2
	movq	-512(%rbp), %rsi
	movq	-504(%rbp), %rcx
	movsd	-496(%rbp), %xmm1
	jmp	.L335
.L338:
	movapd	%xmm1, %xmm0
	movq	%rsi, -520(%rbp)
	movq	%rcx, -504(%rbp)
	movsd	%xmm3, -528(%rbp)
	movsd	%xmm2, -512(%rbp)
	movsd	%xmm1, -496(%rbp)
	call	log2@PLT
	movsd	-528(%rbp), %xmm3
	movq	-520(%rbp), %rsi
	movsd	-512(%rbp), %xmm2
	movq	-504(%rbp), %rcx
	movsd	-496(%rbp), %xmm1
	jmp	.L339
.L352:
	movapd	%xmm2, %xmm0
	movq	%r10, -560(%rbp)
	movq	%r9, -552(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rsi, -520(%rbp)
	movq	%rcx, -512(%rbp)
	movsd	%xmm3, -544(%rbp)
	movsd	%xmm4, -536(%rbp)
	movsd	%xmm1, -504(%rbp)
	movsd	%xmm2, -496(%rbp)
	call	log2@PLT
	movq	-560(%rbp), %r10
	movq	-552(%rbp), %r9
	movsd	-544(%rbp), %xmm3
	movsd	-536(%rbp), %xmm4
	movq	-528(%rbp), %rax
	movq	-520(%rbp), %rsi
	movq	-512(%rbp), %rcx
	movsd	-504(%rbp), %xmm1
	movsd	-496(%rbp), %xmm2
	jmp	.L353
.L404:
	movapd	%xmm3, %xmm4
	xorl	%eax, %eax
	jmp	.L348
.L344:
	movapd	%xmm1, %xmm0
	movsd	%xmm3, -512(%rbp)
	movsd	%xmm2, -504(%rbp)
	movsd	%xmm1, -496(%rbp)
	call	log2@PLT
	movsd	-512(%rbp), %xmm3
	movsd	-504(%rbp), %xmm2
	movsd	-496(%rbp), %xmm1
	jmp	.L345
.L342:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L343
.L397:
	movapd	%xmm3, %xmm1
	jmp	.L319
.L329:
	movq	40(%rbx), %rcx
	movq	80(%rbx), %rdi
	movq	(%r10), %r12
	movq	24(%r10), %rax
	leaq	0(,%rcx,4), %rdx
	movl	%edi, %esi
	cmpq	16(%rbx), %r12
	jnb	.L502
	pxor	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L391
.L356:
	movapd	%xmm2, %xmm0
	movq	%r10, -560(%rbp)
	movq	%r9, -552(%rbp)
	movq	%rsi, -536(%rbp)
	movq	%rax, -520(%rbp)
	movq	%rcx, -512(%rbp)
	movsd	%xmm3, -544(%rbp)
	movsd	%xmm4, -528(%rbp)
	movsd	%xmm1, -504(%rbp)
	movsd	%xmm2, -496(%rbp)
	call	log2@PLT
	movq	-560(%rbp), %r10
	movq	-552(%rbp), %r9
	movsd	-544(%rbp), %xmm3
	movq	-536(%rbp), %rsi
	movsd	-528(%rbp), %xmm4
	movq	-520(%rbp), %rax
	movq	-512(%rbp), %rcx
	movsd	-504(%rbp), %xmm1
	movsd	-496(%rbp), %xmm2
	jmp	.L357
.L362:
	movapd	%xmm2, %xmm0
	movq	%r10, -536(%rbp)
	movq	%r9, -528(%rbp)
	movsd	%xmm3, -520(%rbp)
	movsd	%xmm4, -512(%rbp)
	movsd	%xmm1, -504(%rbp)
	movsd	%xmm2, -496(%rbp)
	call	log2@PLT
	movq	-536(%rbp), %r10
	movq	-528(%rbp), %r9
	movsd	-520(%rbp), %xmm3
	movsd	-512(%rbp), %xmm4
	movsd	-504(%rbp), %xmm1
	movsd	-496(%rbp), %xmm2
	jmp	.L363
.L368:
	subsd	.LC2(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jbe	.L491
	movl	%esi, (%rax,%rdx)
	movq	16(%r10), %rax
	movzbl	-2(%rax,%rcx), %edx
	movb	%dl, (%rax,%rcx)
	movdqa	96(%rbx), %xmm0
	movq	104(%rbx), %rax
	shufpd	$1, %xmm0, %xmm0
	movaps	%xmm0, 96(%rbx)
	testq	%r13, %r13
	je	.L383
	movq	%r13, %r9
	leaq	0(,%r13,8), %rdx
	movsd	.LC1(%rip), %xmm0
	xorl	%r8d, %r8d
	salq	$6, %r9
	leaq	(%rbx,%rdx), %r14
	leaq	-272(%rbp,%rdx), %r12
	addq	%r13, %r9
	salq	$4, %r9
	addq	-424(%rbp), %r9
	jmp	.L384
.L503:
	movq	96(%rbx), %rax
.L384:
	addq	%r8, %rax
	movq	%r9, %rsi
	addq	$1040, %r9
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movl	$130, %eax
	salq	$4, %rdx
	movq	%rax, %rcx
	addq	%r11, %rdx
	movq	%rdx, %rdi
	rep movsq
	movsd	112(%rbx,%r8,8), %xmm1
	movsd	%xmm1, 112(%r14,%r8,8)
	movsd	(%r12,%r8,8), %xmm1
	movq	%rcx, %rax
	movq	88(%rbx), %rcx
	movsd	%xmm1, 112(%rbx,%r8,8)
	addq	%r8, %rcx
	addq	$1, %r8
	movq	%rcx, %rdx
	salq	$6, %rdx
	addq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, 1016(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movsd	%xmm0, 1032(%rdx)
	movq	$0, 1024(%rdx)
	cmpq	%r8, %r13
	jne	.L503
.L383:
	addq	$1, 40(%rbx)
	jmp	.L496
.L491:
	addl	%esi, -4(%rax,%rdx)
	testq	%r13, %r13
	je	.L388
	leaq	-272(%rbp), %rax
	leaq	(%rbx,%r13,8), %r14
	xorl	%r8d, %r8d
	movq	%r15, -416(%rbp)
	movq	-424(%rbp), %r9
	movsd	.LC1(%rip), %xmm0
	movq	%rax, %r15
	jmp	.L389
.L504:
	movq	(%r10), %r12
.L389:
	movq	96(%rbx), %rax
	movq	%r9, %rsi
	movsd	(%r15,%r8,8), %xmm1
	addq	%r8, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movl	$130, %eax
	salq	$4, %rdx
	movq	%rax, %rcx
	addq	%r11, %rdx
	movq	%rdx, %rdi
	rep movsq
	movsd	%xmm1, 112(%rbx,%r8,8)
	movq	%rcx, %rax
	cmpq	$1, %r12
	jne	.L386
	movsd	%xmm1, 112(%r14,%r8,8)
.L386:
	movq	88(%rbx), %rcx
	addq	$1040, %r9
	addq	%r8, %rcx
	addq	$1, %r8
	movq	%rcx, %rdx
	salq	$6, %rdx
	addq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, 1016(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movsd	%xmm0, 1032(%rdx)
	movq	$0, 1024(%rdx)
	cmpq	%r8, %r13
	jne	.L504
	movq	-416(%rbp), %r15
.L388:
	movq	320(%rbx), %rax
	movq	$0, 80(%rbx)
	addq	$1, %rax
	movq	%rax, 320(%rbx)
	cmpq	$1, %rax
	jbe	.L379
	movq	24(%rbx), %rax
	addq	%rax, 72(%rbx)
	jmp	.L379
.L502:
	addl	%edi, -4(%rax,%rdx)
	jmp	.L388
.L360:
	movq	%rax, %rdx
	movq	%rax, %rcx
	pxor	%xmm2, %xmm2
	shrq	%rdx
	andl	$1, %ecx
	orq	%rcx, %rdx
	cvtsi2sdq	%rdx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L361
.L402:
	movapd	%xmm3, %xmm1
	jmp	.L341
.L407:
	movapd	%xmm3, %xmm2
	jmp	.L359
.L374:
	movsd	112(%rbx), %xmm0
	movq	-416(%rbp), %rax
	movsd	%xmm0, (%rax,%r13,8)
	movsd	-384(%rbp), %xmm0
	movsd	%xmm0, 112(%rbx)
	cmpq	$1, %r13
	jbe	.L376
	movsd	120(%rbx), %xmm0
	movsd	%xmm0, 8(%rax,%r13,8)
	movsd	-376(%rbp), %xmm0
	movsd	%xmm0, 120(%rbx)
	cmpq	$2, %r13
	je	.L376
	movsd	128(%rbx), %xmm0
	movsd	%xmm0, 16(%rax,%r13,8)
	movsd	-368(%rbp), %xmm0
	movsd	%xmm0, 128(%rbx)
	cmpq	$3, %r13
	je	.L376
	movsd	136(%rbx), %xmm0
	movsd	%xmm0, 24(%rax,%r13,8)
	movsd	-360(%rbp), %xmm0
	movsd	%xmm0, 136(%rbx)
	cmpq	$4, %r13
	je	.L376
	movsd	144(%rbx), %xmm0
	movsd	%xmm0, 32(%rax,%r13,8)
	movsd	-352(%rbp), %xmm0
	movsd	%xmm0, 144(%rbx)
	cmpq	$5, %r13
	je	.L376
	movsd	152(%rbx), %xmm0
	movsd	%xmm0, 40(%rax,%r13,8)
	movsd	-344(%rbp), %xmm0
	movsd	%xmm0, 152(%rbx)
	cmpq	$6, %r13
	je	.L376
	movsd	160(%rbx), %xmm0
	movsd	%xmm0, 48(%rax,%r13,8)
	movsd	-336(%rbp), %xmm0
	movsd	%xmm0, 160(%rbx)
	cmpq	$7, %r13
	je	.L376
	movsd	168(%rbx), %xmm0
	movsd	%xmm0, 56(%rax,%r13,8)
	movsd	-328(%rbp), %xmm0
	movsd	%xmm0, 168(%rbx)
	cmpq	$8, %r13
	je	.L376
	movsd	176(%rbx), %xmm0
	movsd	%xmm0, 64(%rax,%r13,8)
	movsd	-320(%rbp), %xmm0
	movsd	%xmm0, 176(%rbx)
	cmpq	$9, %r13
	je	.L376
	movsd	184(%rbx), %xmm0
	movsd	%xmm0, 72(%rax,%r13,8)
	movsd	-312(%rbp), %xmm0
	movsd	%xmm0, 184(%rbx)
	cmpq	$10, %r13
	je	.L376
	movsd	192(%rbx), %xmm0
	movsd	%xmm0, 80(%rax,%r13,8)
	movsd	-304(%rbp), %xmm0
	movsd	%xmm0, 192(%rbx)
	cmpq	$11, %r13
	je	.L376
	movsd	200(%rbx), %xmm0
	movsd	%xmm0, 88(%rax,%r13,8)
	movsd	-296(%rbp), %xmm0
	movsd	%xmm0, 200(%rbx)
	cmpq	$12, %r13
	je	.L376
	movsd	208(%rbx), %xmm0
	movsd	%xmm0, 96(%rax,%r13,8)
	movsd	-288(%rbp), %xmm0
	movsd	%xmm0, 208(%rbx)
	jmp	.L376
.L501:
	movq	(%rbx), %rax
	movl	$1, %r14d
	jmp	.L366
.L499:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE111:
	.size	ContextBlockSplitterFinishBlock, .-ContextBlockSplitterFinishBlock
	.p2align 4
	.type	ComputeDistanceCost, @function
ComputeDistanceCost:
.LFB99:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rdi, %r9
	movl	$272, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-2256(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %r12
	movq	(%r10), %r13
	movq	%r8, -2272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2280(%rbp)
	movq	$0, -80(%rbp)
	rep stosq
	movq	.LC1(%rip), %rax
	movq	%rax, -72(%rbp)
	testq	%rsi, %rsi
	je	.L515
	leaq	4(%r9), %rax
	xorl	%r11d, %r11d
	pxor	%xmm1, %xmm1
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L512:
	testl	$33554431, (%rax)
	je	.L507
	cmpw	$127, 8(%rax)
	jbe	.L507
	movzwl	10(%rax), %r9d
	movl	%r9d, %edi
	andw	$1023, %di
	cmpq	%r13, %r12
	je	.L508
	movl	4(%rdx), %ecx
	movzwl	%di, %edi
	leal	16(%rcx), %ebx
	cmpl	%ebx, %edi
	jb	.L509
	movl	$-16, %r15d
	movl	(%rdx), %r14d
	subl	%ecx, %r15d
	addl	%edi, %r15d
	movl	%r14d, %ecx
	movl	%r15d, %edi
	shrl	%cl, %edi
	movl	%r9d, %ecx
	andl	$1, %edi
	shrw	$10, %cx
	addl	$2, %edi
	sall	%cl, %edi
	movl	4(%rax), %ecx
	leal	-4(%rdi,%rcx), %edi
	movl	%r14d, %ecx
	sall	%cl, %edi
	movl	%edi, %r9d
	movl	$-1, %edi
	sall	%cl, %edi
	notl	%edi
	andl	%r15d, %edi
	addl	%ebx, %edi
	addl	%r9d, %edi
.L509:
	movl	%edi, %r9d
	cmpq	16(%r10), %r9
	ja	.L516
	movl	4(%r10), %r15d
	movl	(%r10), %r14d
	leaq	16(%r15), %rcx
	movq	%r15, %rbx
	cmpq	%rcx, %r9
	jb	.L525
	leal	2(%r14), %ecx
	movl	$1, %edi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	movl	%r14d, %edi
	subq	%r15, %rcx
	leaq	-16(%r9,%rcx), %r15
	bsrl	%r15d, %ecx
	leal	-1(%rcx), %r9d
	movq	%r9, %rcx
	subq	%rdi, %r9
	movq	%r15, %rdi
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	andl	$1, %ecx
	leaq	-2(%rcx,%r9,2), %rdi
	movl	%r14d, %ecx
	sall	$10, %r9d
	salq	%cl, %rdi
	movq	%rdi, -2264(%rbp)
	movl	$1, %edi
	sall	%cl, %edi
	movl	%edi, %ecx
	movzwl	-2264(%rbp), %edi
	subl	$1, %ecx
	andq	%r15, %rcx
	leal	16(%rbx,%rcx), %ebx
	addl	%ebx, %edi
	orl	%edi, %r9d
	andw	$1023, %di
.L508:
	movl	%r9d, %ecx
	pxor	%xmm0, %xmm0
	movzwl	%di, %edi
	addq	$1, %r11
	shrw	$10, %cx
	addl	$1, -2256(%rbp,%rdi,4)
	movzwl	%cx, %ecx
	cvtsi2sdl	%ecx, %xmm0
	movl	$1, %ecx
	addsd	%xmm0, %xmm1
.L507:
	addq	$1, %r8
	addq	$16, %rax
	cmpq	%r8, %rsi
	jne	.L512
	testb	%cl, %cl
	je	.L506
	movq	%r11, -80(%rbp)
.L506:
	movq	-2280(%rbp), %rdi
	movsd	%xmm1, -2264(%rbp)
	call	BrotliPopulationCostDistance
	movsd	-2264(%rbp), %xmm1
	movq	-2272(%rbp), %rax
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
	movl	$1, %eax
.L505:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L526
	addq	$2248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movl	%edi, %r9d
	andw	$1023, %di
	jmp	.L508
.L516:
	xorl	%eax, %eax
	jmp	.L505
.L515:
	pxor	%xmm1, %xmm1
	jmp	.L506
.L526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE99:
	.size	ComputeDistanceCost, .-ComputeDistanceCost
	.p2align 4
	.type	InitBlockSplitterDistance.constprop.0, @function
InitBlockSplitterDistance.constprop.0:
.LFB119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	$9, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	1(%rdx), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$257, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movdqa	.LC3(%rip), %xmm0
	movq	%r9, -64(%rbp)
	cmpq	$257, %r15
	movq	%r9, 48(%rsi)
	movq	.LC4(%rip), %rax
	cmovbe	%r15, %r12
	movaps	%xmm0, (%rsi)
	movdqa	.LC5(%rip), %xmm0
	movq	%r8, -56(%rbp)
	movups	%xmm0, 56(%rsi)
	movq	32(%rcx), %r9
	movq	%rax, 16(%rsi)
	movq	$0, 24(%rsi)
	movq	%rcx, 32(%rsi)
	movq	$0, 72(%rsi)
	movq	$0, 112(%rsi)
	cmpq	%r9, %r15
	jbe	.L528
	testq	%r9, %r9
	jne	.L530
	movq	%r15, %r9
.L529:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliAllocate
	movq	32(%r13), %rdx
	movq	-72(%rbp), %r9
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	jne	.L549
.L531:
	movq	16(%r13), %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliFree
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %r9
	movq	%rax, 16(%r13)
	movq	%r9, 32(%r13)
.L528:
	movq	40(%r13), %r9
	cmpq	%r9, %r15
	jbe	.L532
	testq	%r9, %r9
	jne	.L534
	movq	%r15, %r9
.L533:
	leaq	0(,%r9,4), %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliAllocate
	movq	40(%r13), %rdx
	movq	-72(%rbp), %r9
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	jne	.L550
.L535:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliFree
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %r9
	movq	%rax, 24(%r13)
	movq	%r9, 40(%r13)
.L532:
	movq	32(%rbx), %rax
	imulq	$2192, %r12, %rsi
	movq	%r14, %rdi
	movq	%r15, 8(%rax)
	movq	-64(%rbp), %rax
	movq	%r12, (%rax)
	call	BrotliAllocate
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	xorl	%eax, %eax
	subq	%rdi, %rcx
	movq	%rdx, 40(%rbx)
	addl	$2176, %ecx
	movq	$0, (%rdx)
	movq	$0, 2168(%rdx)
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2176(%rdx)
	movq	%rax, 2184(%rdx)
	movaps	%xmm0, 80(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	addq	%r9, %r9
	cmpq	%r9, %r15
	jbe	.L529
	addq	%r9, %r9
	cmpq	%r9, %r15
	ja	.L530
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L534:
	addq	%r9, %r9
	cmpq	%r9, %r15
	jbe	.L533
	addq	%r9, %r9
	cmpq	%r9, %r15
	ja	.L534
	jmp	.L533
.L550:
	movq	24(%r13), %rsi
	salq	$2, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	jmp	.L535
.L549:
	movq	16(%r13), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	jmp	.L531
	.cfi_endproc
.LFE119:
	.size	InitBlockSplitterDistance.constprop.0, .-InitBlockSplitterDistance.constprop.0
	.p2align 4
	.type	InitBlockSplitterCommand.constprop.0, @function
InitBlockSplitterCommand.constprop.0:
.LFB118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	shrq	$10, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	1(%rdx), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$257, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movdqa	.LC6(%rip), %xmm0
	movq	%r9, -64(%rbp)
	cmpq	$257, %r15
	movq	%r9, 48(%rsi)
	movq	.LC7(%rip), %rax
	cmovbe	%r15, %r12
	movaps	%xmm0, (%rsi)
	movdqa	.LC8(%rip), %xmm0
	movq	%r8, -56(%rbp)
	movups	%xmm0, 56(%rsi)
	movq	32(%rcx), %r9
	movq	%rax, 16(%rsi)
	movq	$0, 24(%rsi)
	movq	%rcx, 32(%rsi)
	movq	$0, 72(%rsi)
	movq	$0, 112(%rsi)
	cmpq	%r9, %r15
	jbe	.L552
	testq	%r9, %r9
	jne	.L554
	movq	%r15, %r9
.L553:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliAllocate
	movq	32(%r13), %rdx
	movq	-72(%rbp), %r9
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	jne	.L573
.L555:
	movq	16(%r13), %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliFree
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %r9
	movq	%rax, 16(%r13)
	movq	%r9, 32(%r13)
.L552:
	movq	40(%r13), %r9
	cmpq	%r9, %r15
	jbe	.L556
	testq	%r9, %r9
	jne	.L558
	movq	%r15, %r9
.L557:
	leaq	0(,%r9,4), %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliAllocate
	movq	40(%r13), %rdx
	movq	-72(%rbp), %r9
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	jne	.L574
.L559:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	movq	%r9, -72(%rbp)
	call	BrotliFree
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %r9
	movq	%rax, 24(%r13)
	movq	%r9, 40(%r13)
.L556:
	movq	32(%rbx), %rax
	imulq	$2832, %r12, %rsi
	movq	%r14, %rdi
	movq	%r15, 8(%rax)
	movq	-64(%rbp), %rax
	movq	%r12, (%rax)
	call	BrotliAllocate
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	xorl	%eax, %eax
	subq	%rdi, %rcx
	movq	%rdx, 40(%rbx)
	addl	$2816, %ecx
	movq	$0, (%rdx)
	movq	$0, 2808(%rdx)
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2816(%rdx)
	movq	%rax, 2824(%rdx)
	movaps	%xmm0, 80(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	addq	%r9, %r9
	cmpq	%r9, %r15
	jbe	.L553
	addq	%r9, %r9
	cmpq	%r9, %r15
	ja	.L554
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L558:
	addq	%r9, %r9
	cmpq	%r9, %r15
	jbe	.L557
	addq	%r9, %r9
	cmpq	%r9, %r15
	ja	.L558
	jmp	.L557
.L574:
	movq	24(%r13), %rsi
	salq	$2, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	jmp	.L559
.L573:
	movq	16(%r13), %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	jmp	.L555
	.cfi_endproc
.LFE118:
	.size	InitBlockSplitterCommand.constprop.0, .-InitBlockSplitterCommand.constprop.0
	.p2align 4
	.globl	BrotliInitDistanceParams
	.hidden	BrotliInitDistanceParams
	.type	BrotliInitDistanceParams, @function
BrotliInitDistanceParams:
.LFB97:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %r8d
	movl	%esi, 56(%rdi)
	leal	1(%rsi), %ecx
	leal	16(%rdx), %eax
	movl	%edx, 60(%rdi)
	testl	%r8d, %r8d
	jne	.L576
	movl	$24, %r9d
	movl	$1, %r8d
	sall	%cl, %r9d
	leal	26(%rsi), %ecx
	addl	%eax, %r9d
	movl	%r8d, %eax
	sall	%cl, %eax
	leal	2(%rsi), %ecx
	addl	%edx, %eax
	sall	%cl, %r8d
	subl	%r8d, %eax
.L577:
	movl	%r9d, 64(%rdi)
	movq	%rax, 72(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	movl	$62, %r9d
	sall	%cl, %r9d
	leaq	bound.4732(%rip), %rcx
	addl	%eax, %r9d
	movl	%esi, %eax
	movl	(%rcx,%rax,4), %r8d
	cmpl	%edx, %r8d
	jbe	.L578
	addl	$2147483644, %edx
	movl	%r9d, 64(%rdi)
	movl	%edx, %eax
	subl	%r8d, %eax
	movq	%rax, 72(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	movl	%esi, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	leal	(%rax,%r8), %ecx
	movl	$2147483644, %eax
	cmpl	%edx, %ecx
	ja	.L577
	addl	$1610612732, %edx
	movl	%r9d, 64(%rdi)
	movl	%edx, %eax
	subl	%r8d, %eax
	movq	%rax, 72(%rdi)
	ret
	.cfi_endproc
.LFE97:
	.size	BrotliInitDistanceParams, .-BrotliInitDistanceParams
	.p2align 4
	.globl	BrotliBuildMetaBlock
	.hidden	BrotliBuildMetaBlock
	.type	BrotliBuildMetaBlock, @function
BrotliBuildMetaBlock:
.LFB100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r14d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movq	%rdi, -416(%rbp)
	movq	%rsi, -424(%rbp)
	movdqu	48(%r8), %xmm4
	movl	%eax, -444(%rbp)
	movq	48(%rbp), %rax
	movq	%rdx, -432(%rbp)
	movdqu	(%r8), %xmm7
	movq	%rcx, -440(%rbp)
	movdqu	16(%r8), %xmm6
	movl	%r9d, -400(%rbp)
	movdqu	32(%r8), %xmm5
	movq	%rax, -408(%rbp)
	movdqu	64(%r8), %xmm3
	movdqu	80(%r8), %xmm2
	movq	24(%rbp), %r12
	movdqu	96(%r8), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm7, -320(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movdqu	112(%r8), %xmm0
	movaps	%xmm4, -144(%rbp)
	movsd	.LC9(%rip), %xmm4
	movq	$0, -368(%rbp)
	movl	$1, -380(%rbp)
	movq	%r12, -376(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movsd	%xmm4, -344(%rbp)
.L581:
	movq	-368(%rbp), %rax
	movl	%eax, -360(%rbp)
	movl	%eax, %ebx
	leal	1(%rax), %edx
	cmpl	$15, %r13d
	ja	.L678
	movq	%rax, %rdi
	movl	%edx, %ecx
	movl	$24, %eax
	sall	%cl, %eax
	leal	26(%rdi), %ecx
	leaq	-136(%rbp), %r14
	addl	$16, %eax
	leaq	-264(%rbp), %r12
	leaq	-328(%rbp), %r8
	movl	%eax, -388(%rbp)
	movl	$1, %eax
	movl	%eax, %esi
	movl	%eax, %r11d
	sall	%cl, %esi
	movl	%edi, %ecx
	addl	$2, %ecx
	sall	%cl, %r11d
	movl	%edi, %ecx
	sall	%cl, %eax
	movl	%edx, %ecx
	subl	%r11d, %esi
	movl	%eax, -396(%rbp)
	movl	$62, %eax
	sall	%cl, %eax
	movl	%esi, -392(%rbp)
	movq	%r12, %rsi
	movl	%r13d, %r12d
	addl	$16, %eax
	movq	%rsi, %r13
	movl	%eax, -384(%rbp)
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%rax, %r15
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L680:
	movl	-388(%rbp), %esi
	movl	-392(%rbp), %edi
	leal	(%rsi,%rax), %ecx
	leal	(%rdi,%rax), %edx
.L583:
	movl	%ecx, -128(%rbp)
	movl	-360(%rbp), %edi
	movq	%rdx, -120(%rbp)
	cmpl	%edi, -264(%rbp)
	jne	.L585
	cmpl	%eax, -260(%rbp)
	movl	$0, %eax
	cmovne	-380(%rbp), %eax
	movl	%eax, -380(%rbp)
.L585:
	movq	32(%rbp), %rsi
	movq	-376(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r8, -352(%rbp)
	call	ComputeDistanceCost
	testl	%eax, %eax
	je	.L586
	movsd	-328(%rbp), %xmm0
	comisd	-344(%rbp), %xmm0
	ja	.L586
	movdqu	-136(%rbp), %xmm2
	movq	-120(%rbp), %rax
	addl	$1, %r12d
	cmpl	$16, %r12d
	movq	-352(%rbp), %r8
	movq	%rax, 72(%r14)
	movups	%xmm2, 56(%r14)
	je	.L679
	movsd	%xmm0, -344(%rbp)
.L587:
	movl	%r12d, %eax
	movl	%ebx, %ecx
	movl	-164(%rbp), %r9d
	movl	%ebx, -136(%rbp)
	sall	%cl, %eax
	movl	%eax, -132(%rbp)
	testl	%r9d, %r9d
	je	.L680
	movl	-384(%rbp), %esi
	leaq	bound.4732(%rip), %rdi
	leal	(%rsi,%rax), %ecx
	movq	-368(%rbp), %rsi
	movl	(%rdi,%rsi,4), %esi
	cmpl	%esi, %eax
	jnb	.L584
	movl	%eax, %edx
	subl	%esi, %edx
	addl	$2147483644, %edx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L584:
	movl	-396(%rbp), %edi
	movl	$2147483644, %edx
	addl	%esi, %edi
	cmpl	%edi, %eax
	jb	.L583
	movl	%eax, %edx
	subl	%esi, %edx
	addl	$1610612732, %edx
	jmp	.L583
.L586:
	movl	%r12d, %r13d
	movq	%r14, %r15
	testl	%r12d, %r12d
	jne	.L678
.L589:
	addq	$1, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	$4, %rax
	jne	.L581
	movl	-380(%rbp), %edi
	movq	-376(%rbp), %r12
	movq	32(%rbp), %r13
	testl	%edi, %edi
	jne	.L681
.L592:
	movq	56(%r15), %rax
	cmpq	%rax, -264(%rbp)
	je	.L598
	testq	%r13, %r13
	je	.L598
	movl	-260(%rbp), %eax
	movl	-264(%rbp), %ebx
	movl	$-16, %edx
	subl	%eax, %edx
	leal	16(%rax), %r14d
	movl	%ebx, %ecx
	movl	$-1, %eax
	movl	%edx, -344(%rbp)
	sall	%cl, %eax
	movq	%r13, %rdx
	movl	%ebx, -352(%rbp)
	notl	%eax
	salq	$4, %rdx
	movl	%eax, -360(%rbp)
	leaq	(%rdx,%r12), %r10
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L602:
	testl	$33554431, 4(%rax)
	je	.L599
	cmpw	$127, 12(%rax)
	jbe	.L599
	movzwl	14(%rax), %r9d
	movl	60(%r15), %edi
	movl	%r9d, %esi
	movq	%rdi, %rdx
	andl	$1023, %esi
	cmpl	%r14d, %esi
	jb	.L600
	movl	-352(%rbp), %ebx
	addl	-344(%rbp), %esi
	movl	%esi, %r8d
	andl	-360(%rbp), %esi
	movl	%ebx, %ecx
	addl	%r14d, %esi
	shrl	%cl, %r8d
	movl	%r9d, %ecx
	andl	$1, %r8d
	shrw	$10, %cx
	addl	$2, %r8d
	sall	%cl, %r8d
	movl	8(%rax), %ecx
	leal	-4(%r8,%rcx), %r8d
	movl	%ebx, %ecx
	sall	%cl, %r8d
	addl	%r8d, %esi
.L600:
	movl	%esi, %r8d
	leaq	16(%rdi), %rcx
	cmpq	%rcx, %r8
	jb	.L682
	movl	56(%r15), %r9d
	movl	$1, %ebx
	leal	2(%r9), %ecx
	salq	%cl, %rbx
	movq	%rbx, %rcx
	movl	$1, %ebx
	subq	%rdi, %rcx
	leaq	-16(%r8,%rcx), %rsi
	bsrl	%esi, %r11d
	movq	%rsi, %rdi
	leal	-1(%r11), %r8d
	movl	%r8d, %ecx
	movq	%r8, %r11
	shrq	%cl, %rdi
	movl	%r9d, %ecx
	subq	%rcx, %r8
	movl	%r9d, %ecx
	andl	$1, %edi
	sall	%cl, %ebx
	movl	%ebx, %ecx
	leaq	-2(%rdi,%r8,2), %rbx
	addq	$2, %rdi
	sall	$10, %r8d
	subl	$1, %ecx
	andq	%rsi, %rcx
	leal	16(%rdx,%rcx), %edx
	movl	%r9d, %ecx
	salq	%cl, %rbx
	movl	%r11d, %ecx
	salq	%cl, %rdi
	addl	%ebx, %edx
	movl	%r9d, %ecx
	subq	%rdi, %rsi
	orl	%edx, %r8d
	shrq	%cl, %rsi
	movw	%r8w, 14(%rax)
	movl	%esi, 8(%rax)
	.p2align 4,,10
	.p2align 3
.L599:
	addq	$16, %rax
	cmpq	%r10, %rax
	jne	.L602
.L598:
	movq	-408(%rbp), %rax
	movq	-440(%rbp), %r9
	movq	%r13, %rdx
	movq	-432(%rbp), %r8
	movq	-424(%rbp), %rcx
	leaq	48(%rax), %rsi
	leaq	96(%rax), %rbx
	movq	-416(%rbp), %rdi
	pushq	%rbx
	pushq	%rsi
	pushq	%rax
	pushq	%r15
	movq	%rsi, -368(%rbp)
	movq	%r12, %rsi
	call	BrotliSplitBlock
	movl	24(%r15), %esi
	addq	$32, %rsp
	testl	%esi, %esi
	jne	.L683
	movq	-408(%rbp), %rax
	movq	$0, -352(%rbp)
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L608
	movq	-416(%rbp), %rdi
	leaq	0(,%rax,4), %rsi
	call	BrotliAllocate
	movq	%rax, -352(%rbp)
	movq	%rax, %rsi
	movq	-408(%rbp), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L608
	leaq	-1(%r14), %rax
	cmpq	$2, %rax
	jbe	.L632
	movq	%r14, %rdx
	movd	40(%rbp), %xmm7
	movq	%rsi, %rax
	shrq	$2, %rdx
	salq	$4, %rdx
	pshufd	$0, %xmm7, %xmm0
	addq	%rsi, %rdx
.L605:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L605
	movq	%r14, %rax
	andq	$-4, %rax
	testb	$3, %r14b
	je	.L606
.L604:
	movq	-352(%rbp), %rdi
	movl	40(%rbp), %esi
	leaq	1(%rax), %rdx
	movl	%esi, (%rdi,%rax,4)
	cmpq	%rdx, %r14
	jbe	.L606
	addq	$2, %rax
	movl	%esi, (%rdi,%rdx,4)
	cmpq	%rax, %r14
	jbe	.L606
	movl	%esi, (%rdi,%rax,4)
.L606:
	salq	$6, %r14
	testq	%r14, %r14
	je	.L608
.L687:
	movq	%r14, %rsi
	movq	-416(%rbp), %rdi
	salq	$6, %rsi
	addq	%r14, %rsi
	salq	$4, %rsi
	movq	%rsi, -344(%rbp)
	call	BrotliAllocate
	movq	-344(%rbp), %rcx
	movsd	.LC1(%rip), %xmm0
	movq	%rax, -360(%rbp)
	movq	%rax, %rdx
	leaq	(%rcx,%rax), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L609:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$1040, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	%xmm0, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L609
.L627:
	movq	-408(%rbp), %rax
	movq	$0, -344(%rbp)
	movq	96(%rax), %rax
	movq	%rax, %rsi
	movq	%rax, -376(%rbp)
	salq	$2, %rsi
	jne	.L684
.L610:
	movq	-408(%rbp), %rsi
	movq	48(%rsi), %rax
	movq	%rax, 200(%rsi)
	testq	%rax, %rax
	jne	.L685
	movq	$0, 192(%rsi)
	xorl	%edx, %edx
.L613:
	movzbl	-444(%rbp), %eax
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-344(%rbp)
	movq	-408(%rbp), %rbx
	movq	-424(%rbp), %r9
	pushq	%rdx
	pushq	-360(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rbx, %rdx
	pushq	-352(%rbp)
	pushq	%rax
	movzbl	-400(%rbp), %eax
	pushq	%rax
	pushq	-440(%rbp)
	pushq	-432(%rbp)
	call	BrotliBuildHistogramsWithContext
	movq	-352(%rbp), %rsi
	movq	-416(%rbp), %rdi
	addq	$64, %rsp
	call	BrotliFree
	movq	(%rbx), %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	movq	%rdx, 152(%rbx)
	jne	.L686
	movq	-408(%rbp), %rax
	xorl	%r8d, %r8d
	movq	$0, 144(%rax)
	movq	$0, 184(%rax)
	xorl	%eax, %eax
.L616:
	movq	-408(%rbp), %rbx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	-360(%rbp), %r14
	movl	$256, %ecx
	movq	%r8, 176(%rbx)
	leaq	184(%rbx), %r9
	movq	%r14, %rsi
	movq	-416(%rbp), %rbx
	pushq	%rax
	movq	%rbx, %rdi
	call	BrotliClusterHistogramsLiteral
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movl	24(%r15), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	je	.L620
	movq	-408(%rbp), %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L620
	movq	144(%rbx), %rcx
	movq	%rax, %r8
	leaq	-4(,%rax,4), %rdi
	salq	$8, %r8
	leaq	(%rcx,%r8), %rdx
	leaq	-4(%rcx,%rax,4), %rsi
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L637:
	movd	(%rsi), %xmm3
	leaq	-256(%rdx), %r9
	pshufd	$0, %xmm3, %xmm0
	movups	%xmm0, -256(%rdx)
	movups	%xmm0, -240(%rdx)
	movups	%xmm0, -224(%rdx)
	movups	%xmm0, -208(%rdx)
	movups	%xmm0, -192(%rdx)
	movups	%xmm0, -176(%rdx)
	movups	%xmm0, -160(%rdx)
	movups	%xmm0, -144(%rdx)
	movups	%xmm0, -128(%rdx)
	movups	%xmm0, -112(%rdx)
	movups	%xmm0, -96(%rdx)
	movups	%xmm0, -80(%rdx)
	movups	%xmm0, -64(%rdx)
	movups	%xmm0, -48(%rdx)
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
.L623:
	subq	$4, %rdi
	subq	$256, %r8
	movq	%r9, %rdx
	subq	$4, %rsi
	cmpq	$-4, %rdi
	je	.L620
.L625:
	movq	%rdi, %rcx
	leaq	4(%rdi), %rax
	salq	$6, %rcx
	cmpq	%rax, %rcx
	jge	.L637
	cmpq	%r8, %rdi
	jge	.L637
	leaq	-256(%rdx), %r9
	movq	%r9, %rax
	.p2align 4,,10
	.p2align 3
.L624:
	movl	(%rsi), %ecx
	addq	$4, %rax
	movl	%ecx, -4(%rax)
	cmpq	%rdx, %rax
	jne	.L624
	jmp	.L623
.L679:
	movq	%r14, %r15
	movl	$15, %r9d
.L588:
	shrl	%r9d
	movsd	%xmm0, -344(%rbp)
	movl	%r9d, %r13d
	jmp	.L589
.L678:
	movsd	-344(%rbp), %xmm0
	leal	-1(%r13), %r9d
	jmp	.L588
.L682:
	movw	%si, 14(%rax)
	movl	$0, 8(%rax)
	jmp	.L599
.L683:
	movq	-408(%rbp), %rax
	movq	$0, -352(%rbp)
	movq	(%rax), %r14
	testq	%r14, %r14
	jne	.L687
.L608:
	movq	$0, -360(%rbp)
	xorl	%r14d, %r14d
	jmp	.L627
.L620:
	movq	-408(%rbp), %rbx
	movq	96(%rbx), %rax
	leaq	0(,%rax,4), %rdx
	movq	%rdx, 168(%rbx)
	testq	%rdx, %rdx
	jne	.L688
	movq	$0, 160(%rbx)
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movq	$0, 216(%rbx)
.L626:
	movq	-408(%rbp), %rbx
	subq	$8, %rsp
	movl	$256, %ecx
	movq	-344(%rbp), %r15
	movq	%r8, 208(%rbx)
	leaq	216(%rbx), %r9
	movq	%r15, %rsi
	movq	-416(%rbp), %rbx
	pushq	%rax
	movq	%rbx, %rdi
	call	BrotliClusterHistogramsDistance
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L689
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L685:
	.cfi_restore_state
	imulq	$2832, %rax, %rsi
	movq	-416(%rbp), %rdi
	call	BrotliAllocate
	movq	-408(%rbp), %rsi
	movq	%rax, %rdx
	movq	200(%rsi), %rax
	movq	%rdx, 192(%rsi)
	testq	%rax, %rax
	je	.L613
	imulq	$2832, %rax, %rax
	movsd	.LC1(%rip), %xmm0
	leaq	(%rdx,%rax), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L614:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$2832, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	%xmm0, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L614
	movq	-408(%rbp), %rax
	movq	192(%rax), %rdx
	jmp	.L613
.L684:
	imulq	$2192, %rsi, %rsi
	movq	-416(%rbp), %rdi
	movq	%rsi, -376(%rbp)
	call	BrotliAllocate
	movq	-376(%rbp), %rsi
	movsd	.LC1(%rip), %xmm0
	movq	%rax, -344(%rbp)
	movq	%rax, %rdx
	addq	%rax, %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L611:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$2192, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	%xmm0, -8(%rdx)
	cmpq	%rsi, %rdx
	jne	.L611
	jmp	.L610
.L686:
	movq	-416(%rbp), %r13
	salq	$8, %rax
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	BrotliAllocate
	movq	-408(%rbp), %rbx
	movq	152(%rbx), %rdx
	movq	%rax, 144(%rbx)
	movq	%rdx, 184(%rbx)
	testq	%rdx, %rdx
	je	.L634
	movq	%rdx, %rsi
	movq	%r13, %rdi
	salq	$6, %rsi
	addq	%rdx, %rsi
	salq	$4, %rsi
	call	BrotliAllocate
	movq	%rax, %r8
	movq	144(%rbx), %rax
	jmp	.L616
.L681:
	leaq	-264(%rbp), %rdx
	leaq	-328(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, %rcx
	call	ComputeDistanceCost
	movsd	-344(%rbp), %xmm5
	comisd	-328(%rbp), %xmm5
	jbe	.L592
	movdqu	-264(%rbp), %xmm6
	movq	-248(%rbp), %rax
	movups	%xmm6, 56(%r15)
	movq	%rax, 72(%r15)
	jmp	.L592
.L688:
	movq	-416(%rbp), %r15
	salq	$4, %rax
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	BrotliAllocate
	movq	-408(%rbp), %rbx
	movq	168(%rbx), %rdx
	movq	%rax, 160(%rbx)
	movq	%rdx, 216(%rbx)
	testq	%rdx, %rdx
	je	.L635
	imulq	$2192, %rdx, %rsi
	movq	%r15, %rdi
	call	BrotliAllocate
	movq	168(%rbx), %rdx
	movq	%rax, %r8
	movq	160(%rbx), %rax
	jmp	.L626
.L632:
	xorl	%eax, %eax
	jmp	.L604
.L635:
	xorl	%r8d, %r8d
	jmp	.L626
.L689:
	call	__stack_chk_fail@PLT
.L634:
	xorl	%r8d, %r8d
	jmp	.L616
	.cfi_endproc
.LFE100:
	.size	BrotliBuildMetaBlock, .-BrotliBuildMetaBlock
	.p2align 4
	.globl	BrotliBuildMetaBlockGreedy
	.hidden	BrotliBuildMetaBlockGreedy
	.type	BrotliBuildMetaBlockGreedy, @function
BrotliBuildMetaBlockGreedy:
.LFB115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$712, %rsp
	movq	40(%rbp), %rax
	movq	16(%rbp), %r15
	movq	%rdi, -680(%rbp)
	movq	32(%rbp), %r11
	movq	%rax, -648(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$1, 24(%rbp)
	je	.L691
	cmpq	$0, 48(%rbp)
	movl	%r8d, %r10d
	movl	%r9d, %r12d
	movl	$1, %ecx
	je	.L693
	movq	48(%rbp), %rax
	leaq	-1(%rax), %rdx
	cmpq	$4, %rdx
	jbe	.L754
	movq	%rdx, %rcx
	movq	-648(%rbp), %rdi
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	shrq	$2, %rcx
	salq	$6, %rcx
	movq	%rdi, %rax
	addq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L717:
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm4
	addq	$64, %rax
	movdqu	-32(%rax), %xmm1
	movdqu	-16(%rax), %xmm5
	shufps	$136, %xmm4, %xmm0
	shufps	$136, %xmm5, %xmm1
	shufps	$136, %xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	punpckldq	%xmm3, %xmm0
	punpckhdq	%xmm3, %xmm1
	paddq	%xmm1, %xmm0
	paddq	%xmm0, %xmm2
	cmpq	%rax, %rcx
	jne	.L717
	movdqa	%xmm2, %xmm0
	andq	$-4, %rdx
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm2
	movq	%xmm2, %rax
.L716:
	movq	-648(%rbp), %rdi
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	leaq	1(%rdx), %rcx
	cmpq	%rcx, 48(%rbp)
	jbe	.L718
	salq	$4, %rcx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	leaq	2(%rdx), %rcx
	cmpq	%rcx, 48(%rbp)
	jbe	.L718
	salq	$4, %rcx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	leaq	3(%rdx), %rcx
	cmpq	%rcx, 48(%rbp)
	jbe	.L718
	salq	$4, %rcx
	addq	$4, %rdx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	cmpq	%rdx, 48(%rbp)
	jbe	.L718
	salq	$4, %rdx
	movl	(%rdi,%rdx), %edx
	addq	%rdx, %rax
.L718:
	shrq	$9, %rax
	leaq	1(%rax), %rcx
.L693:
	movq	24(%rbp), %rax
	xorl	%edx, %edx
	movq	.LC11(%rip), %rdi
	movq	$256, -384(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movq	$512, -360(%rbp)
	movq	%rax, -376(%rbp)
	movl	$256, %eax
	divq	24(%rbp)
	movq	%rdi, -352(%rbp)
	movq	-688(%rbp), %rdi
	movq	$0, -344(%rbp)
	movq	32(%rdi), %r8
	movq	%rdi, -336(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -64(%rbp)
	movups	%xmm0, -312(%rbp)
	leaq	184(%rdi), %rdx
	movq	%rdx, -320(%rbp)
	movq	%rax, -368(%rbp)
	addq	$1, %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	movq	%rax, -656(%rbp)
	cmpq	%rcx, %r8
	jnb	.L719
	testq	%r8, %r8
	je	.L755
	.p2align 4,,10
	.p2align 3
.L721:
	addq	%r8, %r8
	cmpq	%rcx, %r8
	jb	.L721
.L720:
	movq	-680(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r11, -712(%rbp)
	movb	%r10b, -704(%rbp)
	movq	%rcx, -696(%rbp)
	movq	%r8, -672(%rbp)
	call	BrotliAllocate
	movq	-672(%rbp), %r8
	movq	-696(%rbp), %rcx
	movq	%rax, -664(%rbp)
	movq	-688(%rbp), %rax
	movzbl	-704(%rbp), %r10d
	movq	-712(%rbp), %r11
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L800
.L722:
	movq	-688(%rbp), %rax
	movq	-680(%rbp), %rdi
	movq	%r11, -712(%rbp)
	movb	%r10b, -704(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -696(%rbp)
	movq	%r8, -672(%rbp)
	call	BrotliFree
	movq	-688(%rbp), %rax
	movq	-664(%rbp), %rdi
	movq	-672(%rbp), %r8
	movq	-712(%rbp), %r11
	movq	%rdi, 16(%rax)
	movzbl	-704(%rbp), %r10d
	movq	%r8, 32(%rax)
	movq	-696(%rbp), %rcx
.L719:
	movq	-688(%rbp), %rax
	movq	40(%rax), %r8
	cmpq	%rcx, %r8
	jnb	.L723
	testq	%r8, %r8
	je	.L756
	.p2align 4,,10
	.p2align 3
.L725:
	addq	%r8, %r8
	cmpq	%rcx, %r8
	jb	.L725
.L724:
	movq	-680(%rbp), %rdi
	leaq	0(,%r8,4), %rsi
	movq	%r11, -712(%rbp)
	movb	%r10b, -704(%rbp)
	movq	%rcx, -696(%rbp)
	movq	%r8, -672(%rbp)
	call	BrotliAllocate
	movq	-672(%rbp), %r8
	movq	-696(%rbp), %rcx
	movq	%rax, -664(%rbp)
	movq	-688(%rbp), %rax
	movzbl	-704(%rbp), %r10d
	movq	-712(%rbp), %r11
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L801
.L726:
	movq	-688(%rbp), %rax
	movq	-680(%rbp), %rdi
	movq	%r11, -712(%rbp)
	movb	%r10b, -704(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -696(%rbp)
	movq	%r8, -672(%rbp)
	call	BrotliFree
	movq	-688(%rbp), %rax
	movq	-664(%rbp), %rdi
	movq	-672(%rbp), %r8
	movq	-712(%rbp), %r11
	movq	%rdi, 24(%rax)
	movzbl	-704(%rbp), %r10d
	movq	%r8, 40(%rax)
	movq	-696(%rbp), %rcx
.L723:
	movq	-688(%rbp), %rax
	xorl	%edx, %edx
	movq	%rcx, 8(%rax)
	movq	-656(%rbp), %rcx
	imulq	24(%rbp), %rcx
	movq	%rcx, 184(%rax)
	testq	%rcx, %rcx
	jne	.L802
.L727:
	movq	-688(%rbp), %rax
	cmpq	$0, 24(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rdx, 176(%rax)
	je	.L732
	movq	24(%rbp), %rsi
	movsd	.LC1(%rip), %xmm0
	xorl	%eax, %eax
	salq	$6, %rsi
	addq	24(%rbp), %rsi
	salq	$4, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$1040, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	%xmm0, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L731
.L732:
	movq	-688(%rbp), %rax
	movq	48(%rbp), %rdx
	pxor	%xmm0, %xmm0
	leaq	-640(%rbp), %rsi
	movq	-680(%rbp), %rdi
	movq	%r11, -664(%rbp)
	leaq	48(%rax), %rcx
	leaq	200(%rax), %r9
	movb	%r10b, -656(%rbp)
	leaq	192(%rax), %r8
	movq	%rsi, -736(%rbp)
	movaps	%xmm0, -288(%rbp)
	call	InitBlockSplitterCommand.constprop.0
	movq	-688(%rbp), %rax
	movq	48(%rbp), %rdx
	leaq	-512(%rbp), %rsi
	movq	-680(%rbp), %rdi
	movq	%rsi, -744(%rbp)
	leaq	96(%rax), %rcx
	leaq	216(%rax), %r9
	leaq	208(%rax), %r8
	call	InitBlockSplitterDistance.constprop.0
	cmpq	$0, 48(%rbp)
	movzbl	-656(%rbp), %r10d
	movq	-664(%rbp), %r11
	je	.L803
	movq	48(%rbp), %rax
	movq	-648(%rbp), %r8
	movl	%r10d, %edi
	movq	%r14, %r9
	movq	%r13, %r10
	movl	%edi, %r13d
	salq	$4, %rax
	addq	%r8, %rax
	movq	%rax, -728(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, -672(%rbp)
	movq	%r15, %rax
	movq	%r8, %r15
	.p2align 4,,10
	.p2align 3
.L740:
	movzwl	12(%r15), %edi
	movzwl	14(%r15), %esi
	imulq	$2832, -568(%rbp), %rdx
	addq	-600(%rbp), %rdx
	movl	(%r15), %ecx
	movl	4(%r15), %r14d
	movw	%di, -712(%rbp)
	movw	%si, -720(%rbp)
	movzwl	%di, %esi
	movq	-576(%rbp), %rdi
	addl	$1, (%rdx,%rsi,4)
	addq	$1, 2816(%rdx)
	leaq	1(%rdi), %rdx
	movq	%rdx, -576(%rbp)
	cmpq	-584(%rbp), %rdx
	je	.L804
	testq	%rcx, %rcx
	je	.L758
.L809:
	addq	%rbx, %rcx
	movl	%r14d, -696(%rbp)
	movzbl	%r12b, %edx
	movq	%r9, %r14
	movq	%r15, -704(%rbp)
	movl	%r13d, %r12d
	movq	%r10, %r15
	movq	%rcx, %r13
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L737:
	addq	$1, %rbx
	movzbl	%r12b, %edx
	cmpq	%r13, %rbx
	je	.L805
.L759:
	movl	%r8d, %r12d
.L738:
	movq	%r15, %rsi
	movzbl	256(%rax,%rdx), %edx
	movq	-304(%rbp), %rdi
	andq	%rbx, %rsi
	movzbl	(%r14,%rsi), %r8d
	movzbl	%r12b, %esi
	orb	(%rax,%rsi), %dl
	movq	-296(%rbp), %rsi
	movzbl	%dl, %edx
	movl	(%r11,%rdx,4), %edx
	addq	%rdx, %rsi
	movq	%rsi, %rdx
	salq	$6, %rdx
	addq	%rsi, %rdx
	movzbl	%r8b, %esi
	salq	$4, %rdx
	addq	-328(%rbp), %rdx
	addl	$1, (%rdx,%rsi,4)
	addq	$1, 1024(%rdx)
	leaq	1(%rdi), %rdx
	movq	%rdx, -304(%rbp)
	cmpq	-312(%rbp), %rdx
	jne	.L737
	movq	-680(%rbp), %rsi
	movq	-672(%rbp), %rdi
	xorl	%edx, %edx
	addq	$1, %rbx
	movb	%r8b, -664(%rbp)
	movq	%r11, -656(%rbp)
	movq	%rax, -648(%rbp)
	call	ContextBlockSplitterFinishBlock
	movq	-656(%rbp), %r11
	movq	-648(%rbp), %rax
	movzbl	%r12b, %edx
	movzbl	-664(%rbp), %r8d
	cmpq	%r13, %rbx
	jne	.L759
.L805:
	movq	%r14, %r9
	movq	%r15, %r10
	movl	-696(%rbp), %r14d
	movq	%r13, %rcx
	movq	-704(%rbp), %r15
	movl	%r8d, %r13d
.L736:
	andl	$33554431, %r14d
	movl	%r14d, %ebx
	addq	%rcx, %rbx
	testl	%r14d, %r14d
	je	.L739
	leaq	-2(%rbx), %rdx
	andq	%r10, %rdx
	movzbl	(%r9,%rdx), %r12d
	leaq	-1(%rbx), %rdx
	andq	%r10, %rdx
	cmpw	$127, -712(%rbp)
	movzbl	(%r9,%rdx), %r13d
	ja	.L806
.L739:
	addq	$16, %r15
	cmpq	%r15, -728(%rbp)
	jne	.L740
.L730:
	movq	-680(%rbp), %rsi
	movq	-672(%rbp), %rdi
	movl	$1, %edx
	movq	%r11, -648(%rbp)
	call	ContextBlockSplitterFinishBlock
	movq	-736(%rbp), %rdi
	movl	$1, %esi
	call	BlockSplitterFinishBlockCommand
	movq	-744(%rbp), %rdi
	movl	$1, %esi
	call	BlockSplitterFinishBlockDistance
	cmpq	$1, 24(%rbp)
	ja	.L807
.L690:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L808
	addq	$712, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	.cfi_restore_state
	movq	-736(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r11, -704(%rbp)
	movq	%rax, -696(%rbp)
	movq	%r10, -664(%rbp)
	movq	%r9, -656(%rbp)
	movl	%ecx, -648(%rbp)
	call	BlockSplitterFinishBlockCommand
	movl	-648(%rbp), %ecx
	movq	-704(%rbp), %r11
	movq	-696(%rbp), %rax
	movq	-664(%rbp), %r10
	movq	-656(%rbp), %r9
	testq	%rcx, %rcx
	jne	.L809
.L758:
	movq	%rbx, %rcx
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L806:
	movq	-720(%rbp), %rdx
	movq	-448(%rbp), %rdi
	imulq	$2192, -440(%rbp), %rcx
	addq	-472(%rbp), %rcx
	andl	$1023, %edx
	addl	$1, (%rcx,%rdx,4)
	leaq	1(%rdi), %rdx
	addq	$1, 2176(%rcx)
	movq	%rdx, -448(%rbp)
	cmpq	-456(%rbp), %rdx
	jne	.L739
	movq	-744(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r11, -696(%rbp)
	movq	%rax, -664(%rbp)
	movq	%r10, -656(%rbp)
	movq	%r9, -648(%rbp)
	call	BlockSplitterFinishBlockDistance
	movq	-696(%rbp), %r11
	movq	-664(%rbp), %rax
	movq	-656(%rbp), %r10
	movq	-648(%rbp), %r9
	jmp	.L739
.L807:
	movq	-688(%rbp), %rbx
	movq	-648(%rbp), %r11
	movl	$0, %edx
	movq	(%rbx), %rdi
	movq	%rdi, %rax
	salq	$6, %rax
	movq	%rax, 152(%rbx)
	jne	.L810
.L741:
	movq	-688(%rbp), %rax
	movq	%rdx, 144(%rax)
	testq	%rdi, %rdi
	je	.L690
	salq	$8, %rdi
	movl	24(%rbp), %r9d
	xorl	%esi, %esi
	leaq	16(%r11), %r8
	addq	%rdx, %rdi
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L762:
	movdqu	(%r11), %xmm1
	movd	%esi, %xmm6
	pshufd	$0, %xmm6, %xmm0
	paddd	%xmm0, %xmm1
	movups	%xmm1, (%rdx)
	movdqu	16(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 16(%rdx)
	movdqu	32(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 32(%rdx)
	movdqu	48(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 48(%rdx)
	movdqu	64(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 64(%rdx)
	movdqu	80(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 80(%rdx)
	movdqu	96(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 96(%rdx)
	movdqu	112(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 112(%rdx)
	movdqu	128(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 128(%rdx)
	movdqu	144(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 144(%rdx)
	movdqu	160(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 160(%rdx)
	movdqu	176(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 176(%rdx)
	movdqu	192(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 192(%rdx)
	movdqu	208(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 208(%rdx)
	movdqu	224(%r11), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 224(%rdx)
	movdqu	240(%r11), %xmm7
	paddd	%xmm7, %xmm0
	movups	%xmm0, 240(%rdx)
.L744:
	addq	$256, %rdx
	addl	%r9d, %esi
	cmpq	%rdi, %rdx
	je	.L690
.L745:
	cmpq	%rdx, %r8
	jbe	.L762
	leaq	16(%rdx), %rax
	cmpq	%rax, %r11
	jnb	.L762
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L742:
	movl	(%r11,%rax,4), %ecx
	addl	%esi, %ecx
	movl	%ecx, (%rdx,%rax,4)
	addq	$1, %rax
	cmpq	$64, %rax
	jne	.L742
	jmp	.L744
.L802:
	movq	%rcx, %rsi
	movq	-680(%rbp), %rdi
	movq	%r11, -664(%rbp)
	salq	$6, %rsi
	movb	%r10b, -656(%rbp)
	addq	%rcx, %rsi
	salq	$4, %rsi
	call	BrotliAllocate
	movq	-664(%rbp), %r11
	movzbl	-656(%rbp), %r10d
	movq	%rax, %rdx
	jmp	.L727
.L691:
	cmpq	$0, 48(%rbp)
	je	.L749
	movq	48(%rbp), %rax
	leaq	-1(%rax), %rdx
	cmpq	$4, %rdx
	jbe	.L750
	movq	%rdx, %rcx
	movq	-648(%rbp), %rdi
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	shrq	$2, %rcx
	salq	$6, %rcx
	movq	%rdi, %rax
	addq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L696:
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm6
	addq	$64, %rax
	movdqu	-32(%rax), %xmm1
	movdqu	-16(%rax), %xmm7
	shufps	$136, %xmm6, %xmm0
	shufps	$136, %xmm7, %xmm1
	shufps	$136, %xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	punpckldq	%xmm3, %xmm0
	punpckhdq	%xmm3, %xmm1
	paddq	%xmm1, %xmm0
	paddq	%xmm0, %xmm2
	cmpq	%rcx, %rax
	jne	.L696
	movdqa	%xmm2, %xmm0
	andq	$-4, %rdx
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm2
	movq	%xmm2, %rax
.L695:
	movq	-648(%rbp), %rdi
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	leaq	1(%rdx), %rcx
	cmpq	%rcx, 48(%rbp)
	jbe	.L697
	salq	$4, %rcx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	leaq	2(%rdx), %rcx
	cmpq	%rcx, 48(%rbp)
	jbe	.L697
	salq	$4, %rcx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	leaq	3(%rdx), %rcx
	cmpq	%rcx, 48(%rbp)
	jbe	.L697
	salq	$4, %rcx
	addq	$4, %rdx
	movl	(%rdi,%rcx), %ecx
	addq	%rcx, %rax
	cmpq	%rdx, 48(%rbp)
	jbe	.L697
	salq	$4, %rdx
	movl	(%rdi,%rdx), %edx
	addq	%rdx, %rax
.L697:
	shrq	$9, %rax
	movl	$257, %r8d
	addq	$1, %rax
	movq	%r8, %rcx
	cmpq	$257, %rax
	movq	%rax, 24(%rbp)
	cmovbe	%rax, %rcx
	movq	%rcx, %r12
	salq	$6, %r12
	addq	%rcx, %r12
	salq	$4, %r12
.L694:
	movdqa	.LC10(%rip), %xmm0
	movq	-688(%rbp), %rdi
	movq	$0, -360(%rbp)
	movq	.LC11(%rip), %rax
	movq	$0, -312(%rbp)
	movaps	%xmm0, -384(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movq	32(%rdi), %r15
	movq	%rax, -368(%rbp)
	leaq	184(%rdi), %rax
	movq	%rdi, -352(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -272(%rbp)
	movups	%xmm0, -328(%rbp)
	cmpq	24(%rbp), %r15
	jnb	.L698
	testq	%r15, %r15
	je	.L751
	movq	24(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L700:
	addq	%r15, %r15
	cmpq	%rax, %r15
	jb	.L700
.L699:
	movq	-680(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rcx, -664(%rbp)
	call	BrotliAllocate
	movq	-664(%rbp), %rcx
	movq	%rax, -656(%rbp)
	movq	-688(%rbp), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L811
.L701:
	movq	-688(%rbp), %rax
	movq	-680(%rbp), %rdi
	movq	%rcx, -664(%rbp)
	movq	16(%rax), %rsi
	call	BrotliFree
	movq	-688(%rbp), %rax
	movq	-656(%rbp), %rdi
	movq	-664(%rbp), %rcx
	movq	%rdi, 16(%rax)
	movq	%r15, 32(%rax)
.L698:
	movq	-688(%rbp), %rax
	movq	40(%rax), %r15
	cmpq	24(%rbp), %r15
	jb	.L812
.L702:
	movq	-688(%rbp), %r15
	movq	24(%rbp), %rdi
	movq	%r12, %rsi
	movq	-352(%rbp), %rax
	movq	-680(%rbp), %r12
	movq	%rdi, 8(%rax)
	movq	%r12, %rdi
	movq	%rcx, 184(%r15)
	call	BrotliAllocate
	pxor	%xmm0, %xmm0
	leaq	-640(%rbp), %rsi
	leaq	200(%r15), %r9
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	movq	%rax, 176(%r15)
	andq	$-8, %rdi
	movq	%rax, -344(%rbp)
	leaq	192(%r15), %r8
	subq	%rdi, %rcx
	movq	$0, (%rax)
	addl	$1024, %ecx
	movq	$0, 1016(%rax)
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	leaq	48(%r15), %rcx
	movq	%r12, %rdi
	movq	$0, 1024(%rdx)
	movq	%rax, 1032(%rdx)
	movq	48(%rbp), %rdx
	movq	%rsi, -736(%rbp)
	movaps	%xmm0, -304(%rbp)
	call	InitBlockSplitterCommand.constprop.0
	movq	48(%rbp), %rdx
	leaq	96(%r15), %rcx
	movq	%r12, %rdi
	leaq	-512(%rbp), %rsi
	leaq	216(%r15), %r9
	leaq	208(%r15), %r8
	movq	%rsi, -744(%rbp)
	call	InitBlockSplitterDistance.constprop.0
	cmpq	$0, 48(%rbp)
	je	.L813
	movq	48(%rbp), %rax
	movq	-648(%rbp), %r15
	salq	$4, %rax
	addq	%r15, %rax
	movq	%rax, -680(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, -672(%rbp)
	movq	%r13, %rax
	movq	%rbx, %r13
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L714:
	movzwl	14(%r15), %eax
	movzwl	12(%r15), %esi
	movl	(%r15), %ecx
	movl	4(%r15), %r12d
	movw	%ax, -664(%rbp)
	imulq	$2832, -568(%rbp), %rax
	addq	-600(%rbp), %rax
	movw	%si, -656(%rbp)
	addl	$1, (%rax,%rsi,4)
	addq	$1, 2816(%rax)
	movq	-576(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -576(%rbp)
	cmpq	-584(%rbp), %rax
	je	.L814
	testq	%rcx, %rcx
	je	.L753
.L816:
	addq	%r13, %rcx
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L708:
	addq	$1, %r13
	cmpq	%rcx, %r13
	je	.L707
.L709:
	movq	-312(%rbp), %rsi
	movq	%rsi, %rax
	salq	$6, %rax
	addq	%rsi, %rax
	movq	%rbx, %rsi
	andq	%r13, %rsi
	salq	$4, %rax
	addq	-344(%rbp), %rax
	movzbl	(%r14,%rsi), %esi
	addl	$1, (%rax,%rsi,4)
	addq	$1, 1024(%rax)
	movq	-320(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -320(%rbp)
	cmpq	-328(%rbp), %rax
	jne	.L708
	movq	-672(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rcx, -648(%rbp)
	addq	$1, %r13
	call	BlockSplitterFinishBlockLiteral
	movq	-648(%rbp), %rcx
	cmpq	%rcx, %r13
	jne	.L709
	.p2align 4,,10
	.p2align 3
.L707:
	andl	$33554431, %r12d
	cmpw	$127, -656(%rbp)
	movl	%r12d, %edx
	leaq	(%rdx,%rcx), %r13
	jbe	.L712
	testl	%r12d, %r12d
	jne	.L815
.L712:
	addq	$16, %r15
	cmpq	-680(%rbp), %r15
	jne	.L714
.L747:
	movq	-672(%rbp), %rdi
	movl	$1, %esi
	call	BlockSplitterFinishBlockLiteral
	movq	-736(%rbp), %rdi
	movl	$1, %esi
	call	BlockSplitterFinishBlockCommand
	movq	-744(%rbp), %rdi
	movl	$1, %esi
	call	BlockSplitterFinishBlockDistance
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L814:
	movq	-736(%rbp), %rdi
	xorl	%esi, %esi
	movl	%ecx, -648(%rbp)
	call	BlockSplitterFinishBlockCommand
	movl	-648(%rbp), %ecx
	testq	%rcx, %rcx
	jne	.L816
.L753:
	movq	%r13, %rcx
	jmp	.L707
.L815:
	movq	-664(%rbp), %rdx
	imulq	$2192, -440(%rbp), %rax
	addq	-472(%rbp), %rax
	andl	$1023, %edx
	addl	$1, (%rax,%rdx,4)
	addq	$1, 2176(%rax)
	movq	-448(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -448(%rbp)
	cmpq	-456(%rbp), %rax
	jne	.L712
	movq	-744(%rbp), %rdi
	xorl	%esi, %esi
	call	BlockSplitterFinishBlockDistance
	jmp	.L712
.L812:
	testq	%r15, %r15
	je	.L752
	movq	24(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L704:
	addq	%r15, %r15
	cmpq	%rax, %r15
	jb	.L704
.L703:
	movq	-680(%rbp), %rdi
	leaq	0(,%r15,4), %rsi
	movq	%rcx, -664(%rbp)
	call	BrotliAllocate
	movq	-664(%rbp), %rcx
	movq	%rax, -656(%rbp)
	movq	-688(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L817
.L705:
	movq	-688(%rbp), %rax
	movq	-680(%rbp), %rdi
	movq	%rcx, -664(%rbp)
	movq	24(%rax), %rsi
	call	BrotliFree
	movq	-688(%rbp), %rax
	movq	-656(%rbp), %rdi
	movq	-664(%rbp), %rcx
	movq	%rdi, 24(%rax)
	movq	%r15, 40(%rax)
	jmp	.L702
.L755:
	movq	%rcx, %r8
	jmp	.L720
.L756:
	movq	%rcx, %r8
	jmp	.L724
.L810:
	salq	$8, %rdi
	movq	%rdi, %rsi
	movq	-680(%rbp), %rdi
	call	BrotliAllocate
	movq	-648(%rbp), %r11
	movq	%rax, %rdx
	movq	-688(%rbp), %rax
	movq	(%rax), %rdi
	jmp	.L741
.L801:
	leaq	0(,%rax,4), %rdx
	movq	-688(%rbp), %rax
	movq	-664(%rbp), %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	movq	-712(%rbp), %r11
	movzbl	-704(%rbp), %r10d
	movq	-696(%rbp), %rcx
	movq	-672(%rbp), %r8
	jmp	.L726
.L800:
	movq	16(%rax), %rsi
	movq	-664(%rbp), %rdi
	call	memcpy@PLT
	movq	-712(%rbp), %r11
	movzbl	-704(%rbp), %r10d
	movq	-696(%rbp), %rcx
	movq	-672(%rbp), %r8
	jmp	.L722
.L751:
	movq	24(%rbp), %r15
	jmp	.L699
.L752:
	movq	24(%rbp), %r15
	jmp	.L703
.L803:
	leaq	-384(%rbp), %rax
	movq	%rax, -672(%rbp)
	jmp	.L730
.L754:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L716
.L811:
	movq	16(%rax), %rsi
	movq	-656(%rbp), %rdi
	call	memcpy@PLT
	movq	-664(%rbp), %rcx
	jmp	.L701
.L817:
	leaq	0(,%rax,4), %rdx
	movq	-688(%rbp), %rax
	movq	-656(%rbp), %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	movq	-664(%rbp), %rcx
	jmp	.L705
.L749:
	movl	$1, %ecx
	movl	$1040, %r12d
	jmp	.L694
.L813:
	leaq	-384(%rbp), %rax
	movq	%rax, -672(%rbp)
	jmp	.L747
.L750:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L695
.L808:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE115:
	.size	BrotliBuildMetaBlockGreedy, .-BrotliBuildMetaBlockGreedy
	.p2align 4
	.globl	BrotliOptimizeHistograms
	.hidden	BrotliOptimizeHistograms
	.type	BrotliOptimizeHistograms, @function
BrotliOptimizeHistograms:
.LFB116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-768(%rbp), %rbx
	subq	$728, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 184(%rsi)
	je	.L822
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movl	$256, %edi
	salq	$6, %rsi
	addq	%r14, %rsi
	addq	$1, %r14
	salq	$4, %rsi
	addq	176(%r13), %rsi
	call	BrotliOptimizeHuffmanCountsForRle
	cmpq	%r14, 184(%r13)
	ja	.L819
.L822:
	xorl	%r14d, %r14d
	cmpq	$0, 200(%r13)
	leaq	-768(%rbp), %rbx
	je	.L821
	.p2align 4,,10
	.p2align 3
.L820:
	imulq	$2832, %r14, %rsi
	movq	%rbx, %rdx
	movl	$704, %edi
	addq	192(%r13), %rsi
	call	BrotliOptimizeHuffmanCountsForRle
	addq	$1, %r14
	cmpq	%r14, 200(%r13)
	ja	.L820
.L821:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	cmpq	$0, 216(%r13)
	leaq	-768(%rbp), %rbx
	je	.L818
	.p2align 4,,10
	.p2align 3
.L825:
	movq	208(%r13), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	addq	$1, %r14
	addq	%r15, %rsi
	addq	$2192, %r15
	call	BrotliOptimizeHuffmanCountsForRle
	cmpq	%r14, 216(%r13)
	ja	.L825
.L818:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L836
	addq	$728, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L836:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE116:
	.size	BrotliOptimizeHistograms, .-BrotliOptimizeHistograms
	.section	.rodata
	.align 16
	.type	bound.4732, @object
	.size	bound.4732, 16
bound.4732:
	.long	0
	.long	4
	.long	12
	.long	28
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	2146435072
	.align 8
.LC2:
	.long	0
	.long	1077149696
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	64
	.quad	512
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	1079574528
	.section	.rodata.cst16
	.align 16
.LC5:
	.quad	512
	.quad	0
	.align 16
.LC6:
	.quad	704
	.quad	1024
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1082081280
	.section	.rodata.cst16
	.align 16
.LC8:
	.quad	1024
	.quad	0
	.section	.rodata.cst8
	.align 8
.LC9:
	.long	2726797102
	.long	1417495214
	.section	.rodata.cst16
	.align 16
.LC10:
	.quad	256
	.quad	512
	.section	.rodata.cst8
	.align 8
.LC11:
	.long	0
	.long	1081671680
	.hidden	BrotliOptimizeHuffmanCountsForRle
	.hidden	BrotliClusterHistogramsDistance
	.hidden	BrotliClusterHistogramsLiteral
	.hidden	BrotliBuildHistogramsWithContext
	.hidden	BrotliSplitBlock
	.hidden	BrotliPopulationCostDistance
	.hidden	BrotliAllocate
	.hidden	BrotliFree
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
