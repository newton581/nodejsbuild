	.file	"transform.c"
	.text
	.p2align 4
	.globl	BrotliGetTransforms
	.type	BrotliGetTransforms, @function
BrotliGetTransforms:
.LFB0:
	.cfi_startproc
	endbr64
	leaq	kBrotliTransforms(%rip), %rax
	ret
	.cfi_endproc
.LFE0:
	.size	BrotliGetTransforms, .-BrotliGetTransforms
	.p2align 4
	.globl	BrotliTransformDictionaryWord
	.type	BrotliTransformDictionaryWord, @function
BrotliTransformDictionaryWord:
.LFB2:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%r8,%r8,2), %eax
	cltq
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rcx), %r9
	movq	8(%rcx), %r11
	movq	32(%rcx), %rcx
	movzbl	(%rcx,%rax), %r8d
	movzbl	1(%rcx,%rax), %r15d
	movzbl	2(%rcx,%rax), %eax
	movzwl	(%r9,%r8,2), %r14d
	movb	%r15b, -49(%rbp)
	movzwl	(%r9,%rax,2), %r13d
	leaq	(%r11,%r14), %rbx
	movzbl	(%rbx), %eax
	leaq	(%r11,%r13), %r12
	movl	%eax, %r9d
	testl	%eax, %eax
	je	.L4
	leaq	1(%r11,%r14), %rcx
	leaq	17(%r11,%r14), %r14
	cmpq	%r14, %rdi
	leaq	16(%rdi), %r14
	setnb	%r15b
	cmpq	%r14, %rcx
	setnb	%r14b
	orb	%r14b, %r15b
	je	.L5
	leal	-1(%rax), %r8d
	cmpl	$14, %r8d
	jbe	.L5
	movdqu	(%rcx), %xmm1
	movl	%eax, %r14d
	shrl	$4, %r14d
	movups	%xmm1, (%rdi)
	cmpl	$1, %r14d
	je	.L6
	movdqu	16(%rcx), %xmm3
	movups	%xmm3, 16(%rdi)
	cmpl	$2, %r14d
	je	.L6
	movdqu	32(%rcx), %xmm5
	movups	%xmm5, 32(%rdi)
	cmpl	$3, %r14d
	je	.L6
	movdqu	48(%rcx), %xmm7
	movups	%xmm7, 48(%rdi)
	cmpl	$4, %r14d
	je	.L6
	movdqu	64(%rcx), %xmm5
	movups	%xmm5, 64(%rdi)
	cmpl	$5, %r14d
	je	.L6
	movdqu	80(%rcx), %xmm3
	movups	%xmm3, 80(%rdi)
	cmpl	$6, %r14d
	je	.L6
	movdqu	96(%rcx), %xmm1
	movups	%xmm1, 96(%rdi)
	cmpl	$7, %r14d
	je	.L6
	movdqu	112(%rcx), %xmm7
	movups	%xmm7, 112(%rdi)
	cmpl	$8, %r14d
	je	.L6
	movdqu	128(%rcx), %xmm6
	movups	%xmm6, 128(%rdi)
	cmpl	$9, %r14d
	je	.L6
	movdqu	144(%rcx), %xmm4
	movups	%xmm4, 144(%rdi)
	cmpl	$10, %r14d
	je	.L6
	movdqu	160(%rcx), %xmm2
	movups	%xmm2, 160(%rdi)
	cmpl	$11, %r14d
	je	.L6
	movdqu	176(%rcx), %xmm4
	movups	%xmm4, 176(%rdi)
	cmpl	$12, %r14d
	je	.L6
	movdqu	192(%rcx), %xmm5
	movups	%xmm5, 192(%rdi)
	cmpl	$13, %r14d
	je	.L6
	movdqu	208(%rcx), %xmm2
	movups	%xmm2, 208(%rdi)
	cmpl	$15, %r14d
	jne	.L6
	movdqu	224(%rcx), %xmm5
	movups	%xmm5, 224(%rdi)
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%eax, %ecx
	andl	$-16, %ecx
	movl	%ecx, %r15d
	subl	%ecx, %r8d
	leaq	1(%rbx,%r15), %rbx
	cmpl	%ecx, %eax
	je	.L4
	movzbl	(%rbx), %r10d
	leal	1(%rcx), %r14d
	movb	%r10b, (%rdi,%r15)
	testl	%r8d, %r8d
	je	.L4
	movzbl	1(%rbx), %r10d
	movslq	%r14d, %r14
	leal	2(%rcx), %r15d
	movb	%r10b, (%rdi,%r14)
	cmpl	$1, %r8d
	je	.L4
	movzbl	2(%rbx), %r10d
	movslq	%r15d, %r15
	leal	3(%rcx), %r14d
	movb	%r10b, (%rdi,%r15)
	cmpl	$2, %r8d
	je	.L4
	movzbl	3(%rbx), %r10d
	movslq	%r14d, %r14
	leal	4(%rcx), %r15d
	movb	%r10b, (%rdi,%r14)
	cmpl	$3, %r8d
	je	.L4
	movzbl	4(%rbx), %r10d
	movslq	%r15d, %r15
	leal	5(%rcx), %r14d
	movb	%r10b, (%rdi,%r15)
	cmpl	$4, %r8d
	je	.L4
	movzbl	5(%rbx), %r10d
	movslq	%r14d, %r14
	leal	6(%rcx), %r15d
	movb	%r10b, (%rdi,%r14)
	cmpl	$5, %r8d
	je	.L4
	movzbl	6(%rbx), %r10d
	movslq	%r15d, %r15
	leal	7(%rcx), %r14d
	movb	%r10b, (%rdi,%r15)
	cmpl	$6, %r8d
	je	.L4
	movzbl	7(%rbx), %r10d
	movslq	%r14d, %r14
	leal	8(%rcx), %r15d
	movb	%r10b, (%rdi,%r14)
	cmpl	$7, %r8d
	je	.L4
	movzbl	8(%rbx), %r10d
	movslq	%r15d, %r15
	leal	9(%rcx), %r14d
	movb	%r10b, (%rdi,%r15)
	cmpl	$8, %r8d
	je	.L4
	movzbl	9(%rbx), %r10d
	movslq	%r14d, %r14
	leal	10(%rcx), %r15d
	movb	%r10b, (%rdi,%r14)
	cmpl	$9, %r8d
	je	.L4
	movzbl	10(%rbx), %r10d
	movslq	%r15d, %r15
	leal	11(%rcx), %r14d
	movb	%r10b, (%rdi,%r15)
	cmpl	$10, %r8d
	je	.L4
	movzbl	11(%rbx), %r10d
	movslq	%r14d, %r14
	leal	12(%rcx), %r15d
	movb	%r10b, (%rdi,%r14)
	cmpl	$11, %r8d
	je	.L4
	movzbl	12(%rbx), %r10d
	movslq	%r15d, %r15
	leal	13(%rcx), %r14d
	movb	%r10b, (%rdi,%r15)
	cmpl	$12, %r8d
	je	.L4
	movzbl	13(%rbx), %r15d
	movslq	%r14d, %r14
	addl	$14, %ecx
	movb	%r15b, (%rdi,%r14)
	cmpl	$13, %r8d
	je	.L4
	movzbl	14(%rbx), %r8d
	movslq	%ecx, %rcx
	movb	%r8b, (%rdi,%rcx)
	.p2align 4,,10
	.p2align 3
.L4:
	movzbl	-49(%rbp), %r10d
	cmpl	$9, %r10d
	jg	.L10
	subl	%r10d, %edx
	testl	%edx, %edx
	jle	.L12
.L11:
	movslq	%eax, %rcx
	leaq	(%rdi,%rcx), %r9
	leaq	16(%rdi,%rcx), %rcx
	cmpq	%rcx, %rsi
	leaq	16(%rsi), %rcx
	setnb	%r8b
	cmpq	%rcx, %r9
	setnb	%cl
	orb	%cl, %r8b
	je	.L33
	cmpl	$15, %edx
	jle	.L33
	testl	%edx, %edx
	movl	$1, %ebx
	cmovg	%edx, %ebx
	xorl	%ecx, %ecx
	movl	%ebx, %r8d
	shrl	$4, %r8d
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L15:
	movdqu	(%rsi,%rcx), %xmm0
	movups	%xmm0, (%r9,%rcx)
	addq	$16, %rcx
	cmpq	%r8, %rcx
	jne	.L15
	movl	%ebx, %ecx
	andl	$-16, %ecx
	leal	(%rax,%rcx), %r8d
	cmpl	%ecx, %ebx
	je	.L19
	movl	%ecx, %ebx
	leal	1(%rcx), %r9d
	leal	1(%r8), %r14d
	movzbl	(%rsi,%rbx), %r15d
	movslq	%r8d, %rbx
	movb	%r15b, (%rdi,%rbx)
	cmpl	%r9d, %edx
	jle	.L19
	movslq	%r9d, %r9
	leal	2(%rcx), %ebx
	leal	2(%r8), %r15d
	movslq	%r14d, %r14
	movzbl	(%rsi,%r9), %r9d
	movb	%r9b, (%rdi,%r14)
	cmpl	%ebx, %edx
	jle	.L19
	movslq	%ebx, %rbx
	leal	3(%rcx), %r9d
	leal	3(%r8), %r14d
	movslq	%r15d, %r15
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, (%rdi,%r15)
	cmpl	%r9d, %edx
	jle	.L19
	movslq	%r9d, %r9
	leal	4(%rcx), %ebx
	leal	4(%r8), %r15d
	movslq	%r14d, %r14
	movzbl	(%rsi,%r9), %r9d
	movb	%r9b, (%rdi,%r14)
	cmpl	%ebx, %edx
	jle	.L19
	movslq	%ebx, %rbx
	leal	5(%rcx), %r9d
	leal	5(%r8), %r14d
	movslq	%r15d, %r15
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, (%rdi,%r15)
	cmpl	%r9d, %edx
	jle	.L19
	movslq	%r9d, %r9
	leal	6(%rcx), %ebx
	leal	6(%r8), %r15d
	movslq	%r14d, %r14
	movzbl	(%rsi,%r9), %r9d
	movb	%r9b, (%rdi,%r14)
	cmpl	%ebx, %edx
	jle	.L19
	movslq	%ebx, %rbx
	leal	7(%rcx), %r9d
	leal	7(%r8), %r14d
	movslq	%r15d, %r15
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, (%rdi,%r15)
	cmpl	%r9d, %edx
	jle	.L19
	movslq	%r9d, %r9
	leal	8(%rcx), %ebx
	leal	8(%r8), %r15d
	movslq	%r14d, %r14
	movzbl	(%rsi,%r9), %r9d
	movb	%r9b, (%rdi,%r14)
	cmpl	%ebx, %edx
	jle	.L19
	movslq	%ebx, %rbx
	leal	9(%rcx), %r9d
	leal	9(%r8), %r14d
	movslq	%r15d, %r15
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, (%rdi,%r15)
	cmpl	%r9d, %edx
	jle	.L19
	movslq	%r9d, %r9
	leal	10(%rcx), %ebx
	leal	10(%r8), %r15d
	movslq	%r14d, %r14
	movzbl	(%rsi,%r9), %r9d
	movb	%r9b, (%rdi,%r14)
	cmpl	%ebx, %edx
	jle	.L19
	movslq	%ebx, %rbx
	leal	11(%rcx), %r9d
	leal	11(%r8), %r14d
	movslq	%r15d, %r15
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, (%rdi,%r15)
	cmpl	%r9d, %edx
	jle	.L19
	movslq	%r9d, %r9
	leal	12(%rcx), %ebx
	leal	12(%r8), %r15d
	movslq	%r14d, %r14
	movzbl	(%rsi,%r9), %r9d
	movb	%r9b, (%rdi,%r14)
	cmpl	%ebx, %edx
	jle	.L19
	movslq	%ebx, %rbx
	leal	13(%rcx), %r9d
	leal	13(%r8), %r14d
	movslq	%r15d, %r15
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, (%rdi,%r15)
	cmpl	%r9d, %edx
	jle	.L19
	movslq	%r9d, %r9
	movslq	%r14d, %r14
	addl	$14, %ecx
	addl	$14, %r8d
	movzbl	(%rsi,%r9), %r9d
	movb	%r9b, (%rdi,%r14)
	cmpl	%ecx, %edx
	jle	.L19
	movslq	%ecx, %rcx
	movslq	%r8d, %r8
	movzbl	(%rsi,%rcx), %ecx
	movb	%cl, (%rdi,%r8)
	.p2align 4,,10
	.p2align 3
.L19:
	leal	-1(%rdx), %ecx
	testl	%edx, %edx
	movl	$0, %esi
	cmovle	%esi, %ecx
	leal	1(%rcx,%rax), %r9d
.L17:
	cmpl	$10, %r10d
	je	.L228
	cmpl	$11, %r10d
	je	.L229
.L12:
	movzbl	(%r12), %r8d
	leal	-1(%r8), %esi
	testl	%r8d, %r8d
	je	.L3
	movslq	%r9d, %rcx
	leaq	17(%r11,%r13), %r10
	leaq	1(%r11,%r13), %rax
	leaq	(%rdi,%rcx), %rdx
	leaq	16(%rdi,%rcx), %rcx
	cmpq	%r10, %rdx
	setnb	%r10b
	cmpq	%rcx, %rax
	setnb	%cl
	orb	%cl, %r10b
	je	.L28
	cmpl	$14, %esi
	jbe	.L28
	movdqu	(%rax), %xmm2
	movl	%r8d, %ecx
	shrl	$4, %ecx
	movups	%xmm2, (%rdx)
	cmpl	$1, %ecx
	je	.L29
	movdqu	16(%rax), %xmm4
	movups	%xmm4, 16(%rdx)
	cmpl	$2, %ecx
	je	.L29
	movdqu	32(%rax), %xmm6
	movups	%xmm6, 32(%rdx)
	cmpl	$3, %ecx
	je	.L29
	movdqu	48(%rax), %xmm7
	movups	%xmm7, 48(%rdx)
	cmpl	$4, %ecx
	je	.L29
	movdqu	64(%rax), %xmm6
	movups	%xmm6, 64(%rdx)
	cmpl	$5, %ecx
	je	.L29
	movdqu	80(%rax), %xmm4
	movups	%xmm4, 80(%rdx)
	cmpl	$6, %ecx
	je	.L29
	movdqu	96(%rax), %xmm2
	movups	%xmm2, 96(%rdx)
	cmpl	$7, %ecx
	je	.L29
	movdqu	112(%rax), %xmm5
	movups	%xmm5, 112(%rdx)
	cmpl	$8, %ecx
	je	.L29
	movdqu	128(%rax), %xmm3
	movups	%xmm3, 128(%rdx)
	cmpl	$9, %ecx
	je	.L29
	movdqu	144(%rax), %xmm1
	movups	%xmm1, 144(%rdx)
	cmpl	$10, %ecx
	je	.L29
	movdqu	160(%rax), %xmm6
	movups	%xmm6, 160(%rdx)
	cmpl	$11, %ecx
	je	.L29
	movdqu	176(%rax), %xmm7
	movups	%xmm7, 176(%rdx)
	cmpl	$12, %ecx
	je	.L29
	movdqu	192(%rax), %xmm3
	movups	%xmm3, 192(%rdx)
	cmpl	$13, %ecx
	je	.L29
	movdqu	208(%rax), %xmm1
	movups	%xmm1, 208(%rdx)
	cmpl	$15, %ecx
	jne	.L29
	movdqu	224(%rax), %xmm4
	movups	%xmm4, 224(%rdx)
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%r8d, %r10d
	movl	%esi, %ecx
	andl	$-16, %r10d
	movl	%r10d, %edx
	leal	(%r9,%r10), %eax
	subl	%r10d, %ecx
	leaq	1(%r12,%rdx), %rdx
	cmpl	%r10d, %r8d
	je	.L32
	movzbl	(%rdx), %r11d
	movslq	%eax, %r10
	leal	1(%rax), %r8d
	movb	%r11b, (%rdi,%r10)
	testl	%ecx, %ecx
	je	.L32
	movzbl	1(%rdx), %r11d
	movslq	%r8d, %r8
	leal	2(%rax), %r10d
	movb	%r11b, (%rdi,%r8)
	cmpl	$1, %ecx
	je	.L32
	movzbl	2(%rdx), %r11d
	movslq	%r10d, %r10
	leal	3(%rax), %r8d
	movb	%r11b, (%rdi,%r10)
	cmpl	$2, %ecx
	je	.L32
	movzbl	3(%rdx), %r11d
	movslq	%r8d, %r8
	leal	4(%rax), %r10d
	movb	%r11b, (%rdi,%r8)
	cmpl	$3, %ecx
	je	.L32
	movzbl	4(%rdx), %r11d
	movslq	%r10d, %r10
	leal	5(%rax), %r8d
	movb	%r11b, (%rdi,%r10)
	cmpl	$4, %ecx
	je	.L32
	movzbl	5(%rdx), %r11d
	movslq	%r8d, %r8
	leal	6(%rax), %r10d
	movb	%r11b, (%rdi,%r8)
	cmpl	$5, %ecx
	je	.L32
	movzbl	6(%rdx), %r11d
	movslq	%r10d, %r10
	leal	7(%rax), %r8d
	movb	%r11b, (%rdi,%r10)
	cmpl	$6, %ecx
	je	.L32
	movzbl	7(%rdx), %r11d
	movslq	%r8d, %r8
	leal	8(%rax), %r10d
	movb	%r11b, (%rdi,%r8)
	cmpl	$7, %ecx
	je	.L32
	movzbl	8(%rdx), %r11d
	movslq	%r10d, %r10
	leal	9(%rax), %r8d
	movb	%r11b, (%rdi,%r10)
	cmpl	$8, %ecx
	je	.L32
	movzbl	9(%rdx), %r11d
	movslq	%r8d, %r8
	leal	10(%rax), %r10d
	movb	%r11b, (%rdi,%r8)
	cmpl	$9, %ecx
	je	.L32
	movzbl	10(%rdx), %r11d
	movslq	%r10d, %r10
	leal	11(%rax), %r8d
	movb	%r11b, (%rdi,%r10)
	cmpl	$10, %ecx
	je	.L32
	movzbl	11(%rdx), %r11d
	movslq	%r8d, %r8
	leal	12(%rax), %r10d
	movb	%r11b, (%rdi,%r8)
	cmpl	$11, %ecx
	je	.L32
	movzbl	12(%rdx), %r11d
	movslq	%r10d, %r10
	leal	13(%rax), %r8d
	movb	%r11b, (%rdi,%r10)
	cmpl	$12, %ecx
	je	.L32
	movzbl	13(%rdx), %r10d
	movslq	%r8d, %r8
	addl	$14, %eax
	movb	%r10b, (%rdi,%r8)
	cmpl	$13, %ecx
	je	.L32
	movzbl	14(%rdx), %edx
	cltq
	movb	%dl, (%rdi,%rax)
	.p2align 4,,10
	.p2align 3
.L32:
	leal	1(%rsi,%r9), %r9d
.L3:
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	leal	-12(%r10), %ecx
	cmpl	$8, %ecx
	ja	.L13
	leal	-11(%r10), %ecx
	movslq	%ecx, %r8
	subl	%ecx, %edx
	addq	%r8, %rsi
.L13:
	testl	%edx, %edx
	jg	.L11
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L229:
	movl	%r9d, %eax
	subl	%edx, %eax
	cltq
	addq	%rdi, %rax
	testl	%edx, %edx
	jle	.L12
	movzbl	(%rax), %ecx
	cmpb	$-65, %cl
	jbe	.L230
	.p2align 4,,10
	.p2align 3
.L23:
	cmpb	$-33, %cl
	jbe	.L231
	xorb	$5, 2(%rax)
	movl	$3, %esi
	movl	$3, %ecx
.L24:
	subl	%ecx, %edx
	addq	%rsi, %rax
	testl	%edx, %edx
	jle	.L12
	movzbl	(%rax), %ecx
	cmpb	$-65, %cl
	ja	.L23
.L230:
	leal	-97(%rcx), %esi
	cmpb	$25, %sil
	ja	.L34
	xorl	$32, %ecx
	movl	$1, %esi
	movb	%cl, (%rax)
	movl	$1, %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L231:
	xorb	$32, 1(%rax)
	movl	$2, %esi
	movl	$2, %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L228:
	movl	%r9d, %eax
	subl	%edx, %eax
	cltq
	addq	%rdi, %rax
	movzbl	(%rax), %edx
	cmpb	$-65, %dl
	jbe	.L232
	cmpb	$-33, %dl
	jbe	.L233
	xorb	$5, 2(%rax)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, %esi
	movl	$1, %ecx
	jmp	.L24
.L233:
	xorb	$32, 1(%rax)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	(%rsi,%rcx), %r8d
	movb	%r8b, (%r9,%rcx)
	addq	$1, %rcx
	cmpl	%ecx, %edx
	jg	.L14
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L28:
	leal	-1(%r8), %edi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L31:
	movzbl	1(%r12,%rax), %ecx
	movb	%cl, (%rdx,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %rdi
	jne	.L31
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L5:
	movzbl	-49(%rbp), %r10d
	leal	-1(%rax), %r14d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	1(%rbx,%rcx), %r8d
	movb	%r8b, (%rdi,%rcx)
	movq	%rcx, %r8
	addq	$1, %rcx
	cmpq	%r14, %r8
	jne	.L8
	movb	%r10b, -49(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L232:
	leal	-97(%rdx), %ecx
	cmpb	$25, %cl
	ja	.L12
	xorl	$32, %edx
	movb	%dl, (%rax)
	jmp	.L12
	.cfi_endproc
.LFE2:
	.size	BrotliTransformDictionaryWord, .-BrotliTransformDictionaryWord
	.section	.data.rel.local,"aw"
	.align 32
	.type	kBrotliTransforms, @object
	.size	kBrotliTransforms, 64
kBrotliTransforms:
	.value	217
	.zero	6
	.quad	kPrefixSuffix
	.quad	kPrefixSuffixMap
	.long	121
	.zero	4
	.quad	kTransformsData
	.value	0
	.value	12
	.value	27
	.value	23
	.value	42
	.value	63
	.value	56
	.value	48
	.value	59
	.value	64
	.zero	4
	.section	.rodata
	.align 32
	.type	kTransformsData, @object
	.size	kTransformsData, 363
kTransformsData:
	.string	"1"
	.string	"11"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"1\f11\n"
	.string	"1"
	.string	"/"
	.string	""
	.string	"1\004"
	.string	""
	.string	"1"
	.string	"\0031\n11"
	.string	"\0061\r11\0011\001"
	.string	""
	.string	"1"
	.string	"\001"
	.string	"\n"
	.string	"1"
	.string	"\0071"
	.string	"\t0"
	.string	""
	.string	"1"
	.string	"\b1"
	.string	"\0051"
	.string	"\n1"
	.string	"\0131\00311"
	.string	"\r1"
	.string	"\0161\01611\00211"
	.string	"\0171"
	.string	"\020"
	.string	"\n11"
	.string	"\f\005"
	.string	"1"
	.string	""
	.string	"\0011\01711"
	.string	"\0221"
	.string	"\0211"
	.string	"\0231"
	.string	"\0241\02011\0211/"
	.string	"11\00411"
	.string	"\0261\01311"
	.string	"\0271"
	.string	"\0301"
	.string	"\0311\00711\001\0321"
	.string	"\0331"
	.string	"\034"
	.string	""
	.string	"\f1"
	.string	"\0351\02411\02211\00611"
	.string	"\0251\n\0011\b11"
	.string	"\0371"
	.string	" /"
	.string	"\0031\00511\t1"
	.string	"\n\0011\n\b\005"
	.string	"\0251\013"
	.string	"1\n\n1"
	.string	"\036"
	.string	""
	.string	"\005#"
	.string	"1/"
	.string	"\0021\n\0211"
	.string	"$1"
	.string	"!\005"
	.string	""
	.string	"1\n\0251\n\0051"
	.string	"%"
	.string	""
	.string	"\0361"
	.string	"&"
	.string	"\013"
	.string	"1"
	.string	"'"
	.string	"\01311"
	.string	"\"1\013\b1\n\f"
	.string	""
	.string	"\0251"
	.string	"("
	.string	"\n\f1"
	.string	")1"
	.string	"*1\013\0211"
	.string	"+"
	.string	"\n\0051\013\n"
	.string	""
	.string	"\"1\n!1"
	.string	",1\013\005-"
	.string	"1"
	.string	""
	.string	"!1\n\0361\013\0361"
	.string	".1\013\0011\n\""
	.string	"\n!"
	.string	"\013\036"
	.string	"\013\0011\013!1\013\0251\013\f"
	.string	"\013\0051\013\""
	.string	"\013\f"
	.string	"\n\036"
	.string	"\013\""
	.ascii	"\n\""
	.align 32
	.type	kPrefixSuffixMap, @object
	.size	kPrefixSuffixMap, 100
kPrefixSuffixMap:
	.value	0
	.value	2
	.value	5
	.value	14
	.value	19
	.value	22
	.value	24
	.value	30
	.value	35
	.value	37
	.value	42
	.value	45
	.value	47
	.value	50
	.value	52
	.value	58
	.value	62
	.value	69
	.value	71
	.value	78
	.value	85
	.value	90
	.value	92
	.value	99
	.value	104
	.value	109
	.value	114
	.value	119
	.value	122
	.value	124
	.value	128
	.value	131
	.value	136
	.value	140
	.value	142
	.value	145
	.value	151
	.value	159
	.value	165
	.value	169
	.value	173
	.value	178
	.value	183
	.value	189
	.value	194
	.value	199
	.value	202
	.value	207
	.value	213
	.value	216
	.align 32
	.type	kPrefixSuffix, @object
	.size	kPrefixSuffix, 217
kPrefixSuffix:
	.string	"\001 \002, \b of the \004 of \002s \001.\005 and \004 in \001\"\004 to \002\">\001\n\002. \001]\005 for \003 a \006 that \001'\006 with \006 from \004 by \001(\006. The \004 on \004 as \004 is \004ing \002\n\t\001:\003ed \002=\"\004 at \003ly \001,\002='\005.com/\007. This \005 not \003er \003al \004ful \004ive \005less \004est \004ize \002\302\240\004ous \005 the \002e "
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
