	.file	"huffman.c"
	.text
	.p2align 4
	.globl	BrotliBuildCodeLengthsHuffmanTable
	.hidden	BrotliBuildCodeLengthsHuffmanTable
	.type	BrotliBuildCodeLengthsHuffmanTable, @function
BrotliBuildCodeLengthsHuffmanTable:
.LFB51:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	2(%rdx), %eax
	movzwl	4(%rdx), %edx
	movl	$17, -160(%rbp)
	subl	$1, %eax
	movl	%eax, -156(%rbp)
	addl	%eax, %edx
	movzwl	6(%rbx), %eax
	movl	%edx, -152(%rbp)
	addl	%eax, %edx
	movzwl	8(%rbx), %eax
	movl	%edx, -148(%rbp)
	addl	%edx, %eax
	movzwl	10(%rbx), %edx
	movl	%eax, -144(%rbp)
	addl	%edx, %eax
	movzbl	17(%rsi), %edx
	movl	%eax, -140(%rbp)
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$17, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	16(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$16, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	15(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, -160(%rbp,%rdx,4)
	movl	$15, -128(%rbp,%rax,4)
	movzbl	14(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$14, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	13(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$13, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	12(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$12, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	11(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$11, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	10(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$10, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	9(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$9, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	8(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$8, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	7(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$7, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	6(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$6, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	5(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$5, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	4(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$4, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	3(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$3, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	2(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$2, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	1(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$1, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movzbl	(%rsi), %edx
	movslq	-160(%rbp,%rdx,4), %rax
	leal	-1(%rax), %ecx
	movl	$0, -128(%rbp,%rax,4)
	movl	%ecx, -160(%rbp,%rdx,4)
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	je	.L19
	leaq	-128(%rbp), %rax
	movl	$2, %edi
	movl	$128, %ebx
	movq	$0, -176(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-124(%rbp), %rax
	movl	$1, %r15d
	movl	$0, -164(%rbp)
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	-184(%rbp), %rax
	movzwl	(%rax,%r15,2), %r12d
	testl	%r12d, %r12d
	je	.L4
	movslq	-164(%rbp), %rdx
	movl	%edi, %esi
	movl	%r15d, %r8d
	movslq	%edi, %r13
	movq	-192(%rbp), %rax
	movq	-200(%rbp), %rcx
	negl	%esi
	leaq	kReverseBits(%rip), %r11
	movslq	%esi, %rsi
	addq	-176(%rbp), %r11
	leaq	(%rax,%rdx,4), %r9
	leal	-1(%r12), %eax
	salq	$2, %rsi
	movq	%rax, %r12
	addq	%rdx, %rax
	leaq	(%rcx,%rax,4), %r14
	.p2align 4,,10
	.p2align 3
.L6:
	movzbl	(%r11), %eax
	movzwl	(%r9), %ecx
	movl	$32, %edx
	subq	%r13, %rax
	leaq	130(%r10,%rax,4), %rax
	.p2align 4,,10
	.p2align 3
.L5:
	subl	%edi, %edx
	movb	%r8b, -2(%rax)
	movw	%cx, (%rax)
	addq	%rsi, %rax
	testl	%edx, %edx
	jg	.L5
	addq	$4, %r9
	addq	%rbx, %r11
	cmpq	%r14, %r9
	jne	.L6
	movl	-164(%rbp), %eax
	leal	1(%rax,%r12), %eax
	movslq	%r12d, %r12
	movl	%eax, -164(%rbp)
	imulq	%rbx, %r12
	movq	-176(%rbp), %rax
	addq	%rbx, %rax
	addq	%r12, %rax
	movq	%rax, -176(%rbp)
.L4:
	addq	$1, %r15
	addl	%edi, %edi
	shrq	%rbx
	cmpq	$6, %r15
	jne	.L2
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	movzwl	-128(%rbp), %edx
	leaq	128(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movb	$0, (%r10)
	addq	$4, %r10
	movw	%dx, -2(%r10)
	cmpq	%rax, %r10
	jne	.L3
	jmp	.L1
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE51:
	.size	BrotliBuildCodeLengthsHuffmanTable, .-BrotliBuildCodeLengthsHuffmanTable
	.p2align 4
	.globl	BrotliBuildHuffmanTable
	.hidden	BrotliBuildHuffmanTable
	.type	BrotliBuildHuffmanTable, @function
BrotliBuildHuffmanTable:
.LFB52:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	cmpw	$-1, -2(%rdx)
	movl	%esi, -92(%rbp)
	movq	%rcx, -80(%rbp)
	jne	.L41
	movq	$-2, %rax
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%eax, %edx
	subq	$1, %rax
	cmpw	$-1, 2(%rbx,%rax,2)
	je	.L23
	leal	16(%rdx), %eax
	movl	%eax, -84(%rbp)
.L22:
	movl	-92(%rbp), %esi
	movl	$1, %eax
	movl	%eax, %edi
	movl	%esi, %ecx
	sall	%cl, %edi
	movl	%edi, -88(%rbp)
	movl	-84(%rbp), %edi
	cmpl	%edi, %esi
	jle	.L42
	movl	%edi, %ecx
	movl	%edi, -72(%rbp)
	sall	%cl, %eax
	movl	%eax, -68(%rbp)
.L24:
	movslq	-68(%rbp), %r14
	movl	$2, -60(%rbp)
	movl	$1, %r15d
	movl	$128, %r12d
	movq	$0, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-80(%rbp), %rax
	leal	-16(%r15), %r8d
	movzwl	(%rax,%r15,2), %r9d
	movl	%r9d, -64(%rbp)
	testl	%r9d, %r9d
	je	.L25
	movslq	-60(%rbp), %r11
	leaq	kReverseBits(%rip), %r10
	movl	%r15d, %edi
	addq	-56(%rbp), %r10
	movq	%r11, %rax
	negq	%r11
	negl	%eax
	salq	$2, %r11
	movslq	%eax, %rsi
	.p2align 4,,10
	.p2align 3
.L27:
	movzbl	(%r10), %edx
	movslq	%r8d, %r8
	movq	%r14, %rax
	movzwl	(%rbx,%r8,2), %r8d
	leaq	(%r11,%rdx,4), %rdx
	movl	%r8d, %ecx
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L26:
	movb	%dil, (%rdx,%rax,4)
	movw	%cx, 2(%rdx,%rax,4)
	addq	%rsi, %rax
	testl	%eax, %eax
	jg	.L26
	addq	%r12, %r10
	subl	$1, %r9d
	jne	.L27
	movl	-64(%rbp), %eax
	movq	-56(%rbp), %rdx
	subl	$1, %eax
	addq	%r12, %rdx
	cltq
	imulq	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
.L25:
	sall	-60(%rbp)
	shrq	%r12
	addq	$1, %r15
	cmpl	%r15d, -72(%rbp)
	jge	.L28
	movl	-68(%rbp), %eax
	movl	-88(%rbp), %r14d
	cmpl	%r14d, %eax
	je	.L29
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L30:
	movslq	%r12d, %rdx
	movq	%r13, %rsi
	addl	%r12d, %r12d
	salq	$2, %rdx
	leaq	0(%r13,%rdx), %rdi
	call	memcpy@PLT
	cmpl	%r12d, %r14d
	jne	.L30
	movl	%r12d, -68(%rbp)
.L29:
	movl	-92(%rbp), %edi
	movl	$128, %eax
	movl	-84(%rbp), %esi
	leal	-1(%rdi), %ecx
	shrq	%cl, %rax
	movq	%rax, -104(%rbp)
	leal	1(%rdi), %eax
	movl	%eax, -60(%rbp)
	cmpl	%esi, %eax
	jg	.L31
	addl	$1, %esi
	movq	-80(%rbp), %rcx
	movl	$1, -84(%rbp)
	cltq
	movl	%esi, -116(%rbp)
	movq	%r13, %r15
	movl	$128, %r12d
	movl	$256, %r9d
	movl	$2, -80(%rbp)
	leaq	(%rcx,%rax,2), %r10
	movb	%dil, -93(%rbp)
	movq	%r13, -112(%rbp)
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L40:
	movl	-60(%rbp), %eax
	leal	-16(%rax), %r8d
	movzwl	(%r10), %eax
	testw	%ax, %ax
	je	.L63
	movl	-84(%rbp), %esi
	movl	$1, %edi
	movslq	-80(%rbp), %r11
	movslq	-68(%rbp), %r14
	movl	%esi, %ecx
	sall	%cl, %edi
	movl	-60(%rbp), %ecx
	movl	%edi, -72(%rbp)
	movl	%esi, %edi
	movq	%r11, %rsi
	negq	%r11
	addl	$1, %ecx
	negl	%esi
	salq	$2, %r11
	movl	%ecx, -64(%rbp)
	movslq	%esi, %rsi
	.p2align 4,,10
	.p2align 3
.L39:
	cmpq	$256, %r9
	je	.L33
	leaq	kReverseBits(%rip), %rax
	movzbl	(%rax,%r9), %edx
	addq	%r12, %r9
.L34:
	movslq	%r8d, %r8
	leaq	(%r11,%rdx,4), %rdx
	movq	%r14, %rax
	movzwl	(%rbx,%r8,2), %r8d
	addq	%r15, %rdx
	movl	%r8d, %ecx
	.p2align 4,,10
	.p2align 3
.L38:
	movb	%dil, (%rdx,%rax,4)
	movw	%cx, 2(%rdx,%rax,4)
	addq	%rsi, %rax
	testl	%eax, %eax
	jg	.L38
	movzwl	(%r10), %eax
	subl	$1, %eax
	movw	%ax, (%r10)
	testw	%ax, %ax
	jne	.L39
.L32:
	movl	-64(%rbp), %eax
	movl	-116(%rbp), %edi
	sall	-80(%rbp)
	shrq	%r12
	addl	$1, -84(%rbp)
	addq	$2, %r10
	movl	%eax, -60(%rbp)
	cmpl	%edi, %eax
	jne	.L40
.L31:
	movl	-88(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	$15, -60(%rbp)
	leaq	(%r15,%r14,4), %r15
	je	.L44
	movl	-72(%rbp), %ecx
	movzwl	%ax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	testl	%ecx, %ecx
	jle	.L45
	movslq	-64(%rbp), %rdx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	movzwl	0(%r13,%rdx,2), %r9d
	addq	$1, %rdx
	subl	%r9d, %eax
	testl	%eax, %eax
	jle	.L35
.L36:
	addl	%eax, %eax
	movl	%edx, %ecx
	cmpl	$15, %edx
	jne	.L37
.L35:
	movq	-56(%rbp), %rdx
	movq	-104(%rbp), %r9
	leaq	kReverseBits(%rip), %rax
	movl	$1, %r14d
	subl	-92(%rbp), %ecx
	movzbl	(%rax,%rdx), %eax
	addq	%r9, %rdx
	movq	-112(%rbp), %r9
	sall	%cl, %r14d
	movq	%rdx, -56(%rbp)
	addb	-93(%rbp), %cl
	movzbl	%al, %edx
	addl	%r14d, -88(%rbp)
	leaq	(%r9,%rdx,4), %rdx
	movl	%r14d, -68(%rbp)
	movslq	%r14d, %r14
	movb	%cl, (%rdx)
	movq	%r15, %rcx
	subq	%r9, %rcx
	movq	%r12, %r9
	sarq	$2, %rcx
	subl	%eax, %ecx
	movw	%cx, 2(%rdx)
	xorl	%edx, %edx
	jmp	.L34
.L63:
	movl	-60(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -64(%rbp)
	jmp	.L32
.L44:
	movl	$15, %ecx
	jmp	.L35
.L45:
	movl	-60(%rbp), %ecx
	jmp	.L35
.L42:
	movl	-88(%rbp), %eax
	movl	%eax, -68(%rbp)
	movl	-92(%rbp), %eax
	movl	%eax, -72(%rbp)
	jmp	.L24
.L41:
	movl	$15, -84(%rbp)
	jmp	.L22
	.cfi_endproc
.LFE52:
	.size	BrotliBuildHuffmanTable, .-BrotliBuildHuffmanTable
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.globl	BrotliBuildSimpleHuffmanTable
	.hidden	BrotliBuildSimpleHuffmanTable
	.type	BrotliBuildSimpleHuffmanTable, @function
BrotliBuildSimpleHuffmanTable:
.LFB53:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	sall	%cl, %r13d
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	$4, %eax
	ja	.L88
	leaq	.L67(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L67:
	.long	.L71-.L67
	.long	.L70-.L67
	.long	.L69-.L67
	.long	.L68-.L67
	.long	.L66-.L67
	.text
	.p2align 4,,10
	.p2align 3
.L68:
	movzwl	(%rdx), %ecx
	movzwl	2(%rdx), %esi
	cmpw	%si, %cx
	jbe	.L75
	movl	%ecx, %eax
	movw	%cx, 2(%rdx)
	movl	%esi, %ecx
	movw	%si, (%rdx)
	movl	%eax, %esi
.L75:
	movzwl	4(%rdx), %eax
	cmpw	%ax, %cx
	jbe	.L81
	movl	%ecx, %edi
	movw	%cx, 4(%rdx)
	movl	%eax, %ecx
	movw	%ax, (%rdx)
	movl	%edi, %eax
.L81:
	movzwl	6(%rdx), %edi
	cmpw	%di, %cx
	jbe	.L80
	movw	%cx, 6(%rdx)
	movw	%di, (%rdx)
	movl	%ecx, %edi
.L80:
	movl	%esi, %ecx
	cmpw	%ax, %si
	jbe	.L79
	movw	%si, 4(%rdx)
	movl	%eax, %ecx
	movw	%ax, 2(%rdx)
	movl	%esi, %eax
.L79:
	cmpw	%di, %cx
	jbe	.L85
	movw	%cx, 6(%rdx)
	movw	%di, 2(%rdx)
	movl	%ecx, %edi
.L85:
	cmpw	%di, %ax
	jbe	.L84
	movw	%ax, 6(%rdx)
	movw	%di, 4(%rdx)
.L84:
	movzwl	(%rdx), %eax
	movb	$2, (%r12)
	movl	$4, %ebx
	movw	%ax, 2(%r12)
	movzwl	2(%rdx), %eax
	movb	$2, 8(%r12)
	movw	%ax, 10(%r12)
	movzwl	4(%rdx), %eax
	movb	$2, 4(%r12)
	movw	%ax, 6(%r12)
	movzwl	6(%rdx), %eax
	movb	$2, 12(%r12)
	movw	%ax, 14(%r12)
	cmpl	%r13d, %ebx
	jne	.L73
.L64:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movzwl	6(%rdx), %eax
	movzwl	4(%rdx), %ecx
	cmpw	%cx, %ax
	jnb	.L86
	movw	%cx, 6(%rdx)
	movw	%ax, 4(%rdx)
.L86:
	movzwl	(%rdx), %eax
	movb	$1, (%r12)
	movl	$8, %ebx
	movw	%ax, 2(%r12)
	movzwl	2(%rdx), %eax
	movb	$2, 4(%r12)
	movw	%ax, 6(%r12)
	movzwl	(%rdx), %eax
	movb	$1, 8(%r12)
	movw	%ax, 10(%r12)
	movzwl	4(%rdx), %eax
	movb	$3, 12(%r12)
	movw	%ax, 14(%r12)
	movzwl	(%rdx), %eax
	movb	$1, 16(%r12)
	movw	%ax, 18(%r12)
	movzwl	2(%rdx), %eax
	movb	$2, 20(%r12)
	movw	%ax, 22(%r12)
	movzwl	(%rdx), %eax
	movb	$1, 24(%r12)
	movw	%ax, 26(%r12)
	movzwl	6(%rdx), %eax
	movb	$3, 28(%r12)
	movw	%ax, 30(%r12)
.L65:
	cmpl	%r13d, %ebx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L73:
	movl	%ebx, %edx
	movq	%r12, %rsi
	addl	%ebx, %ebx
	salq	$2, %rdx
	leaq	(%r12,%rdx), %rdi
	call	memcpy@PLT
	cmpl	%ebx, %r13d
	jne	.L73
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movzwl	2(%rdx), %eax
	movzwl	(%rdx), %ecx
	movb	$1, (%rdi)
	cmpw	%cx, %ax
	jbe	.L72
	movw	%cx, 2(%rdi)
	movzwl	2(%rdx), %eax
	movl	$2, %ebx
	movb	$1, 4(%rdi)
	movw	%ax, 6(%rdi)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L69:
	movzwl	(%rdx), %eax
	movb	$1, (%rdi)
	movw	%ax, 2(%rdi)
	movzwl	(%rdx), %eax
	movb	$1, 8(%rdi)
	movw	%ax, 10(%rdi)
	movzwl	4(%rdx), %eax
	movzwl	2(%rdx), %ecx
	movb	$2, 4(%rdi)
	cmpw	%cx, %ax
	jbe	.L74
	movw	%cx, 6(%rdi)
	movzwl	4(%rdx), %eax
	movl	$4, %ebx
	movb	$2, 12(%rdi)
	movw	%ax, 14(%rdi)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L71:
	movzwl	(%rdx), %eax
	movl	$1, %ebx
	movb	$0, (%rdi)
	movw	%ax, 2(%rdi)
	jmp	.L65
.L74:
	movw	%ax, 6(%rdi)
	movzwl	2(%rdx), %eax
	movl	$4, %ebx
	movb	$2, 12(%rdi)
	movw	%ax, 14(%rdi)
	jmp	.L65
.L72:
	movw	%ax, 2(%rdi)
	movzwl	(%rdx), %eax
	movl	$2, %ebx
	movb	$1, 4(%rdi)
	movw	%ax, 6(%rdi)
	jmp	.L65
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	BrotliBuildSimpleHuffmanTable.cold, @function
BrotliBuildSimpleHuffmanTable.cold:
.LFSB53:
.L88:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, %ebx
	jmp	.L65
	.cfi_endproc
.LFE53:
	.text
	.size	BrotliBuildSimpleHuffmanTable, .-BrotliBuildSimpleHuffmanTable
	.section	.text.unlikely
	.size	BrotliBuildSimpleHuffmanTable.cold, .-BrotliBuildSimpleHuffmanTable.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.rodata
	.align 32
	.type	kReverseBits, @object
	.size	kReverseBits, 256
kReverseBits:
	.string	""
	.ascii	"\200@\300 \240`\340\020\220P\3200\260p\360\b\210H\310(\250h\350"
	.ascii	"\030\230X\3308\270x\370\004\204D\304$\244d\344\024\224T\3244"
	.ascii	"\264t\364\f\214L\314,\254l\354\034\234\\\334<\274|\374\002\202"
	.ascii	"B\302\"\242b\342\022\222R\3222\262r\362\n\212J\312*\252j\352"
	.ascii	"\032\232Z\332:\272z\372\006\206F\306&\246f\346\026\226V\3266"
	.ascii	"\266v\366\016\216N\316.\256n\356\036\236^\336>\276~\376\001\201"
	.ascii	"A\301!\241a\341\021\221Q\3211\261q\361\t\211I\311)\251i\351\031"
	.ascii	"\231Y\3319\271y\371\005\205E\305%\245e\345\025\225U\3255\265"
	.ascii	"u\365\r\215M\315-\255m\355\035\235]\335=\275}\375\003\203C\303"
	.ascii	"#\243c\343\023\223S\3233\263s\363\013\213K\313+\253k\353\033"
	.ascii	"\233[\333;\273{\373\007\207G\307'\247g\347\027\227W\3277\267"
	.ascii	"w\367\017\217O\317/\257o\357\037\237_\337?\277\177\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
