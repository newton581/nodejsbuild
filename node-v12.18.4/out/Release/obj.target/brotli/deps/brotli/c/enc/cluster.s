	.file	"cluster.c"
	.text
	.p2align 4
	.globl	BrotliCompareAndPushToQueueLiteral
	.hidden	BrotliCompareAndPushToQueueLiteral
	.type	BrotliCompareAndPushToQueueLiteral, @function
BrotliCompareAndPushToQueueLiteral:
.LFB95:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1112, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r8, -1120(%rbp)
	movq	%rax, -1112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	je	.L1
	movq	%rdi, %rbx
	movl	%edx, %r14d
	movl	%ecx, %r13d
	movq	%r9, %r15
	jbe	.L4
	movl	%ecx, %r14d
	movl	%edx, %r13d
.L4:
	movl	%r13d, %r12d
	movl	%r14d, %ecx
	pxor	%xmm1, %xmm1
	movl	(%rsi,%rcx,4), %edx
	movl	(%rsi,%r12,4), %eax
	cvtsi2sdq	%rdx, %xmm1
	leaq	(%rax,%rdx), %rsi
	cmpq	$255, %rdx
	ja	.L7
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm2, %xmm2
	cvtss2sd	(%r8,%rdx,4), %xmm2
.L8:
	mulsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpq	$255, %rax
	ja	.L11
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L12:
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rsi, %xmm2
	cmpq	$255, %rsi
	ja	.L13
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm3, %xmm3
	cvtss2sd	(%rax,%rsi,4), %xmm3
.L14:
	mulsd	%xmm3, %xmm2
	movq	%rcx, %rsi
	movq	-1112(%rbp), %rax
	salq	$6, %rsi
	addq	%rcx, %rsi
	movq	%r12, %rcx
	movq	(%rax), %rax
	salq	$4, %rsi
	salq	$6, %rcx
	addq	%rbx, %rsi
	addq	%r12, %rcx
	subsd	%xmm2, %xmm1
	mulsd	.LC2(%rip), %xmm1
	movsd	1032(%rsi), %xmm4
	salq	$4, %rcx
	leaq	(%rbx,%rcx), %r10
	movq	1024(%rsi), %rdx
	movsd	1032(%r10), %xmm0
	subsd	%xmm4, %xmm1
	subsd	%xmm0, %xmm1
	testq	%rdx, %rdx
	je	.L15
	movq	1024(%r10), %r8
	testq	%r8, %r8
	je	.L27
	movsd	.LC0(%rip), %xmm2
	testq	%rax, %rax
	je	.L16
	pxor	%xmm2, %xmm2
	maxsd	16(%r15), %xmm2
.L16:
	addq	%r8, %rdx
	leaq	-1104(%rbp), %rdi
	movl	$130, %ecx
	rep movsq
	movq	%rdx, -80(%rbp)
	movq	%r10, %rcx
	leaq	-80(%rbp), %rsi
	leaq	-1104(%rbp), %rdi
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L17:
	movdqu	(%rcx), %xmm0
	paddd	(%rax), %xmm0
	addq	$16, %rax
	addq	$16, %rcx
	movaps	%xmm0, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L17
	movsd	%xmm2, -1136(%rbp)
	movsd	%xmm1, -1128(%rbp)
	call	BrotliPopulationCostLiteral
	movsd	-1128(%rbp), %xmm1
	movsd	-1136(%rbp), %xmm2
	subsd	%xmm1, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L1
	movq	-1112(%rbp), %rax
	movq	(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L15:
	addsd	%xmm0, %xmm1
	testq	%rax, %rax
	je	.L20
	movsd	16(%r15), %xmm2
	ucomisd	%xmm2, %xmm1
	jp	.L31
	jne	.L31
	movl	%r13d, %ecx
	movl	4(%r15), %edx
	subl	(%r15), %edx
	subl	%r14d, %ecx
	cmpl	%ecx, %edx
	seta	%dl
	movzbl	%dl, %edx
.L23:
	testl	%edx, %edx
	je	.L20
	cmpq	%rax, -1120(%rbp)
	jbe	.L24
	movdqu	(%r15), %xmm5
	leaq	(%rax,%rax,2), %rdx
	movq	-1112(%rbp), %rdi
	addq	$1, %rax
	leaq	(%r15,%rdx,8), %rdx
	movups	%xmm5, (%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rdx)
	movq	%rax, (%rdi)
.L24:
	movl	%r14d, (%r15)
	unpcklpd	%xmm1, %xmm0
	movl	%r13d, 4(%r15)
	movups	%xmm0, 8(%r15)
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$1112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	movq	%rax, -1152(%rbp)
	movq	%rsi, -1144(%rbp)
	movq	%rcx, -1136(%rbp)
	movsd	%xmm1, -1128(%rbp)
	call	log2@PLT
	movq	-1152(%rbp), %rax
	movq	-1144(%rbp), %rsi
	movq	-1136(%rbp), %rcx
	movsd	-1128(%rbp), %xmm1
	movapd	%xmm0, %xmm2
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	%rax, -1120(%rbp)
	jbe	.L1
	leaq	(%rax,%rax,2), %rdx
	movq	-1112(%rbp), %rbx
	unpcklpd	%xmm1, %xmm0
	addq	$1, %rax
	leaq	(%r15,%rdx,8), %rdx
	movl	%r14d, (%rdx)
	movl	%r13d, 4(%rdx)
	movups	%xmm0, 8(%rdx)
	movq	%rax, (%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	movapd	%xmm2, %xmm0
	movq	%rcx, -1136(%rbp)
	movsd	%xmm1, -1144(%rbp)
	movsd	%xmm2, -1128(%rbp)
	call	log2@PLT
	movsd	-1144(%rbp), %xmm1
	movq	-1136(%rbp), %rcx
	movsd	-1128(%rbp), %xmm2
	movapd	%xmm0, %xmm3
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	movapd	%xmm1, %xmm0
	movq	%rsi, -1144(%rbp)
	movq	%rcx, -1136(%rbp)
	movsd	%xmm2, -1152(%rbp)
	movsd	%xmm1, -1128(%rbp)
	call	log2@PLT
	movsd	-1152(%rbp), %xmm2
	movq	-1144(%rbp), %rsi
	movq	-1136(%rbp), %rcx
	movsd	-1128(%rbp), %xmm1
	jmp	.L12
.L27:
	movapd	%xmm4, %xmm0
	jmp	.L15
.L31:
	xorl	%edx, %edx
	comisd	%xmm1, %xmm2
	seta	%dl
	jmp	.L23
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE95:
	.size	BrotliCompareAndPushToQueueLiteral, .-BrotliCompareAndPushToQueueLiteral
	.p2align 4
	.globl	BrotliHistogramCombineLiteral
	.hidden	BrotliHistogramCombineLiteral
	.type	BrotliHistogramCombineLiteral, @function
BrotliHistogramCombineLiteral:
.LFB96:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	%rdi, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r9, -72(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	$0, -64(%rbp)
	testq	%r9, %r9
	je	.L48
	leaq	-64(%rbp), %rdi
	movl	$1, %r14d
	movq	%rdi, -88(%rbp)
	cmpq	$1, %r9
	jbe	.L48
	movq	%r14, -80(%rbp)
	movq	-96(%rbp), %r15
	movq	%r8, %r14
	movq	-80(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r14, %rax
	movq	%r13, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-80(%rbp), %rax
	movl	(%r14,%rbx,4), %ecx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	32(%rbp), %r8
	movq	%r13, %r9
	movq	%r15, %rdi
	addq	$1, %rbx
	movl	-4(%r14,%rax,4), %edx
	pushq	-88(%rbp)
	call	BrotliCompareAndPushToQueueLiteral
	popq	%rcx
	popq	%rsi
	cmpq	%rbx, -72(%rbp)
	ja	.L51
	movq	%r13, %rax
	addq	$1, -80(%rbp)
	movq	%r14, %r13
	movq	%rax, %r14
	movq	-80(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L101
	movq	%rax, %rbx
	jmp	.L49
.L101:
	movq	%r14, %rbx
.L48:
	movq	-120(%rbp), %rax
	movq	16(%rbp), %rdi
	movq	$1, -88(%rbp)
	pxor	%xmm7, %xmm7
	movsd	%xmm7, -104(%rbp)
	leaq	(%rax,%rdi,4), %rax
	movq	%rax, -128(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -112(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rax, %r12
.L54:
	movq	-72(%rbp), %rdi
	cmpq	%rdi, -88(%rbp)
	jnb	.L106
.L75:
	movsd	16(%rbx), %xmm0
	comisd	-104(%rbp), %xmm0
	jb	.L107
	movq	24(%rbp), %rax
	movq	-72(%rbp), %rdi
	movq	%rax, -88(%rbp)
	movq	.LC0(%rip), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rdi, -88(%rbp)
	jb	.L75
.L106:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	leaq	-40(%rbp), %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L107:
	.cfi_restore_state
	movl	(%rbx), %esi
	movl	4(%rbx), %edi
	movq	-96(%rbp), %rax
	movq	%rdi, %rcx
	movq	%rsi, %rdx
	movq	%rsi, %r15
	movq	%rdi, %r14
	salq	$6, %rcx
	salq	$6, %rdx
	addq	%rdi, %rcx
	addq	%rsi, %rdx
	salq	$4, %rcx
	salq	$4, %rdx
	addq	%rax, %rdx
	addq	%rax, %rcx
	movq	1024(%rcx), %rax
	addq	%rax, 1024(%rdx)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L55:
	movdqu	(%rdx,%rax), %xmm0
	movdqu	(%rcx,%rax), %xmm2
	paddd	%xmm2, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$1024, %rax
	jne	.L55
	movsd	8(%rbx), %xmm0
	movl	0(%r13,%rdi,4), %eax
	movsd	%xmm0, 1032(%rdx)
	movq	-128(%rbp), %rdx
	addl	%eax, 0(%r13,%rsi,4)
	cmpq	$0, 16(%rbp)
	movq	-120(%rbp), %rax
	je	.L60
	.p2align 4,,10
	.p2align 3
.L56:
	cmpl	%r14d, (%rax)
	jne	.L59
	movl	%r15d, (%rax)
.L59:
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L56
.L60:
	subq	$1, -72(%rbp)
	xorl	%eax, %eax
	cmpq	$0, -112(%rbp)
	movq	%r12, %rdi
	movq	-64(%rbp), %r8
	movq	-112(%rbp), %rcx
	jne	.L63
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L61:
	addq	$4, %rdi
	cmpq	%rcx, %rax
	je	.L62
.L63:
	movq	%rax, %rdx
	addq	$1, %rax
	cmpl	%r14d, (%rdi)
	jne	.L61
	movq	-72(%rbp), %rsi
	movq	%r8, -136(%rbp)
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	leaq	(%r12,%rax,4), %rsi
	salq	$2, %rdx
	call	memmove@PLT
	movq	-136(%rbp), %r8
.L62:
	testq	%r8, %r8
	je	.L81
.L76:
	movq	%rbx, %rax
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L110:
	movdqu	(%rax), %xmm5
	movsd	8(%rbx), %xmm1
	movq	(%rbx), %rsi
	movups	%xmm5, (%rbx)
	movq	16(%rax), %r9
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm0, %xmm3
	movq	%r9, 16(%rbx)
	movq	%rsi, (%rcx)
	movups	%xmm3, 8(%rcx)
.L70:
	addq	$1, %rdi
.L65:
	addq	$1, %rdx
	addq	$24, %rax
	cmpq	%r8, %rdx
	jnb	.L64
.L71:
	movl	(%rax), %ecx
	cmpl	%r15d, %ecx
	je	.L65
	movl	4(%rax), %esi
	cmpl	%r15d, %esi
	sete	%r10b
	cmpl	%r14d, %ecx
	sete	%r9b
	orb	%r9b, %r10b
	jne	.L65
	cmpl	%r14d, %esi
	je	.L65
	movsd	16(%rbx), %xmm0
	movsd	16(%rax), %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L82
	jne	.L82
	subl	%ecx, %esi
	movl	4(%rbx), %r9d
	subl	(%rbx), %r9d
	cmpl	%esi, %r9d
	seta	%sil
	movzbl	%sil, %esi
.L68:
	leaq	(%rdi,%rdi,2), %rcx
	leaq	(%rbx,%rcx,8), %rcx
	testl	%esi, %esi
	jne	.L110
	movdqu	(%rax), %xmm6
	movups	%xmm6, (%rcx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%esi, %esi
	comisd	%xmm1, %xmm0
	seta	%sil
	jmp	.L68
.L81:
	xorl	%edi, %edi
.L64:
	cmpq	$0, -72(%rbp)
	movq	%rdi, -64(%rbp)
	je	.L74
.L72:
	xorl	%r14d, %r14d
	movq	%rbx, %rax
	movq	%r12, %rbx
	movq	%r14, %r12
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L73:
	subq	$8, %rsp
	movl	(%rbx,%r12,4), %ecx
	pushq	-80(%rbp)
	movl	%r15d, %edx
	movq	32(%rbp), %r8
	movq	%r14, %r9
	movq	%r13, %rsi
	addq	$1, %r12
	movq	-96(%rbp), %rdi
	call	BrotliCompareAndPushToQueueLiteral
	popq	%rax
	popq	%rdx
	cmpq	-72(%rbp), %r12
	jb	.L73
	movq	%rbx, %r12
	movq	%r14, %rbx
.L74:
	subq	$1, -112(%rbp)
	jmp	.L54
.L109:
	testq	%r8, %r8
	jne	.L76
	jmp	.L72
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE96:
	.size	BrotliHistogramCombineLiteral, .-BrotliHistogramCombineLiteral
	.p2align 4
	.globl	BrotliHistogramBitCostDistanceLiteral
	.hidden	BrotliHistogramBitCostDistanceLiteral
	.type	BrotliHistogramBitCostDistanceLiteral, @function
BrotliHistogramBitCostDistanceLiteral:
.LFB97:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$1064, %rsp
	movq	1024(%rdi), %rdx
	movq	%fs:40, %rsi
	movq	%rsi, -24(%rbp)
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.L111
	movq	%rdi, %rax
	addq	1024(%rbx), %rdx
	movl	$130, %ecx
	movq	%rax, %rsi
	leaq	-1072(%rbp), %rdi
	rep movsq
	leaq	-1072(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	leaq	-48(%rbp), %rcx
	movq	%rdi, %rax
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L113:
	movdqu	(%rdx), %xmm0
	paddd	(%rax), %xmm0
	addq	$16, %rax
	addq	$16, %rdx
	movaps	%xmm0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L113
	call	BrotliPopulationCostLiteral
	subsd	1032(%rbx), %xmm0
.L111:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	addq	$1064, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L120:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE97:
	.size	BrotliHistogramBitCostDistanceLiteral, .-BrotliHistogramBitCostDistanceLiteral
	.p2align 4
	.globl	BrotliHistogramRemapLiteral
	.hidden	BrotliHistogramRemapLiteral
	.type	BrotliHistogramRemapLiteral, @function
BrotliHistogramRemapLiteral:
.LFB98:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	testq	%rsi, %rsi
	je	.L122
	movq	$0, -64(%rbp)
	movq	%rdi, %r12
	leaq	(%rdx,%rcx,4), %r13
.L129:
	cmpq	$0, -64(%rbp)
	movq	-72(%rbp), %rax
	je	.L148
	movq	-64(%rbp), %rdi
	movl	-4(%rax,%rdi,4), %r14d
.L124:
	movl	%r14d, %eax
	movq	%r12, %rdi
	movq	%rax, %rsi
	salq	$6, %rsi
	addq	%rax, %rsi
	salq	$4, %rsi
	addq	%rbx, %rsi
	call	BrotliHistogramBitCostDistanceLiteral
	cmpq	$0, -80(%rbp)
	movsd	%xmm0, -56(%rbp)
	je	.L125
	movq	-96(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L128:
	movl	(%r15), %edx
	movq	%r12, %rdi
	movq	%rdx, %rsi
	salq	$6, %rsi
	addq	%rdx, %rsi
	salq	$4, %rsi
	addq	%rbx, %rsi
	call	BrotliHistogramBitCostDistanceLiteral
	movsd	-56(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L126
	movl	(%r15), %r14d
	movsd	%xmm0, -56(%rbp)
.L126:
	addq	$4, %r15
	cmpq	%r13, %r15
	jne	.L128
.L125:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdi
	addq	$1040, %r12
	movl	%r14d, (%rdi,%rax,4)
	addq	$1, %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, -88(%rbp)
	jne	.L129
	cmpq	$0, -80(%rbp)
	je	.L132
.L136:
	movsd	.LC3(%rip), %xmm0
	xorl	%esi, %esi
	xorl	%eax, %eax
.L131:
	movq	-96(%rbp), %rdi
	movl	(%rdi,%rsi,4), %ecx
	addq	$1, %rsi
	movq	%rcx, %rdx
	salq	$6, %rdx
	addq	%rcx, %rdx
	salq	$4, %rdx
	addq	%rbx, %rdx
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, 1016(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movsd	%xmm0, 1032(%rdx)
	movq	$0, 1024(%rdx)
	cmpq	%rsi, -80(%rbp)
	ja	.L131
	cmpq	$0, -88(%rbp)
	je	.L121
.L132:
	movq	-104(%rbp), %rcx
	xorl	%esi, %esi
.L134:
	movq	-72(%rbp), %rax
	movl	(%rax,%rsi,4), %eax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	1024(%rcx), %rax
	salq	$4, %rdx
	addq	%rbx, %rdx
	addq	%rax, 1024(%rdx)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L133:
	movdqu	(%rcx,%rax), %xmm0
	movdqu	(%rdx,%rax), %xmm1
	paddd	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$1024, %rax
	jne	.L133
	addq	$1, %rsi
	addq	$1040, %rcx
	cmpq	%rsi, -88(%rbp)
	ja	.L134
.L121:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L148:
	.cfi_restore_state
	movl	(%rax), %r14d
	jmp	.L124
.L122:
	cmpq	$0, -80(%rbp)
	jne	.L136
	jmp	.L121
	.cfi_endproc
.LFE98:
	.size	BrotliHistogramRemapLiteral, .-BrotliHistogramRemapLiteral
	.p2align 4
	.globl	BrotliHistogramReindexLiteral
	.hidden	BrotliHistogramReindexLiteral
	.type	BrotliHistogramReindexLiteral, @function
BrotliHistogramReindexLiteral:
.LFB99:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	jne	.L169
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	call	BrotliFree
	xorl	%eax, %eax
.L157:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	BrotliFree
	movq	-56(%rbp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	leaq	0(,%rcx,4), %r14
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movq	%r14, %rsi
	call	BrotliAllocate
	movq	%r14, %rdx
	movl	$255, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	(%rbx,%r14), %rdx
	movq	%rbx, %rax
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L152:
	movl	(%rax), %ecx
	leaq	(%r15,%rcx,4), %rcx
	cmpl	$-1, (%rcx)
	jne	.L151
	movl	%esi, (%rcx)
	addl	$1, %esi
.L151:
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L152
	xorl	%r14d, %r14d
	testl	%esi, %esi
	je	.L153
	movq	%rsi, %rax
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	salq	$6, %rax
	addq	%rax, %rsi
	salq	$4, %rsi
	call	BrotliAllocate
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
.L153:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L159:
	movl	(%rbx), %edi
	movq	%rcx, %rax
	movl	(%r15,%rdi,4), %esi
	cmpl	%r8d, %esi
	je	.L170
	movl	%esi, (%rbx)
	addq	$4, %rbx
	cmpq	%rdx, %rbx
	jne	.L159
.L156:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	BrotliFree
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rax
	testq	%rcx, %rcx
	je	.L157
	movq	%rcx, %r8
	xorl	%edx, %edx
	salq	$6, %r8
	addq	%rcx, %r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	(%r12,%rdx), %rdi
	leaq	(%r14,%rdx), %rsi
	addq	$1040, %rdx
	movl	$130, %ecx
	rep movsq
	cmpq	%rdx, %r8
	jne	.L158
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L170:
	salq	$6, %rax
	movq	%rdi, %rsi
	addq	$4, %rbx
	addq	%rax, %rcx
	salq	$6, %rsi
	salq	$4, %rcx
	addq	%rdi, %rsi
	leaq	(%r14,%rcx), %rax
	salq	$4, %rsi
	movl	$130, %ecx
	movq	%rax, %rdi
	addq	%r12, %rsi
	rep movsq
	leal	1(%r8), %ecx
	movq	%rcx, %r8
	movl	-4(%rbx), %eax
	movl	(%r15,%rax,4), %esi
	movq	%rcx, %rax
	movl	%esi, -4(%rbx)
	cmpq	%rdx, %rbx
	jne	.L159
	jmp	.L156
	.cfi_endproc
.LFE99:
	.size	BrotliHistogramReindexLiteral, .-BrotliHistogramReindexLiteral
	.p2align 4
	.globl	BrotliClusterHistogramsLiteral
	.hidden	BrotliClusterHistogramsLiteral
	.type	BrotliClusterHistogramsLiteral, @function
BrotliClusterHistogramsLiteral:
.LFB100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r9, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L238
	movl	$49176, %esi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	BrotliAllocate
	xorl	%r10d, %r10d
	movq	%rax, %r15
.L184:
	subq	$8, %rsp
	movq	16(%rbp), %rdx
	movq	%r10, %r9
	movq	%r15, %r8
	pushq	%rbx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	pushq	-64(%rbp)
	pushq	-96(%rbp)
	call	BrotliHistogramCombineLiteral
	movq	%r15, %rsi
	movq	-104(%rbp), %r15
	addq	$32, %rsp
	movq	%rax, %rbx
	movq	%r15, %rdi
	call	BrotliFree
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	%rbx, %rcx
	movq	-96(%rbp), %rbx
	movq	16(%rbp), %r9
	movq	-112(%rbp), %rdi
	movq	%r14, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	BrotliHistogramRemapLiteral
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	16(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rsi
	call	BrotliHistogramReindexLiteral
	movq	-120(%rbp), %rdi
	movq	%rax, (%rdi)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L238:
	.cfi_restore_state
	leaq	0(,%rdx,4), %r12
	movq	%rdi, %r15
	movq	%rdx, %rbx
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BrotliAllocate
	movq	%r15, %rdi
	movl	$49176, %esi
	movq	%rax, %r12
	call	BrotliAllocate
	movq	%rax, %r15
	leaq	-1(%rbx), %rax
	cmpq	$2, %rax
	jbe	.L186
	shrq	$2, %rbx
	movdqa	.LC4(%rip), %xmm0
	movq	%r13, %rax
	movq	%rbx, %rdx
	salq	$4, %rdx
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L174:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L174
	movq	-96(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-4, %rax
	andl	$3, %edi
	je	.L175
.L173:
	movq	-96(%rbp), %rsi
	leaq	1(%rax), %rdx
	movl	$1, 0(%r13,%rax,4)
	cmpq	%rdx, %rsi
	jbe	.L175
	movl	$1, 0(%r13,%rdx,4)
	addq	$2, %rax
	cmpq	%rax, %rsi
	jbe	.L175
	movl	$1, 0(%r13,%rax,4)
.L175:
	xorl	%ebx, %ebx
	movq	-112(%rbp), %r8
	movq	%r15, -56(%rbp)
	leaq	1032(%r14), %rax
	movq	%r12, -80(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r12
	movq	16(%rbp), %rbx
	movq	%r13, -72(%rbp)
	movq	%r8, %r13
	movq	%r14, -88(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	-1032(%r14), %rdi
	movq	%r13, %rsi
	movl	$130, %ecx
	addq	$1040, %r14
	rep movsq
	movq	%r13, %rdi
	addq	$1040, %r13
	call	BrotliPopulationCostLiteral
	movq	%r12, %rcx
	movsd	%xmm0, -1040(%r14)
	movl	%r12d, (%rbx,%r12,4)
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L176
	andq	$-64, %rcx
	movq	-88(%rbp), %r14
	movq	%r12, %rbx
	movq	-56(%rbp), %r15
	leaq	64(%rcx), %rax
	movq	-80(%rbp), %r12
	movq	16(%rbp), %rdx
	xorl	%r11d, %r11d
	movq	%rax, -80(%rbp)
	xorl	%r10d, %r10d
	movq	%r15, %r8
	movq	%r14, %r15
	movq	%rbx, -88(%rbp)
	movq	%r12, %r13
	movq	%r11, %rbx
	movq	%r10, %r12
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L178:
	movq	-88(%rbp), %rdx
	movl	$64, %r9d
	movq	%rdx, %rax
	subq	%rbx, %rax
	cmpq	$64, %rax
	cmovbe	%rax, %r9
	cmpq	%rbx, %rdx
	je	.L182
	movl	%ebx, %esi
	cmpq	$3, %rax
	jbe	.L187
	movdqa	.LC5(%rip), %xmm1
	movd	%ebx, %xmm2
	movq	%r9, %rax
	leaq	0(%r13,%r12,4), %rcx
	pshufd	$0, %xmm2, %xmm0
	shrq	$2, %rax
	paddd	%xmm0, %xmm1
	movups	%xmm1, (%rcx)
	cmpq	$1, %rax
	je	.L181
	movdqa	.LC6(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 16(%rcx)
	cmpq	$2, %rax
	je	.L181
	movdqa	.LC7(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 32(%rcx)
	cmpq	$3, %rax
	je	.L181
	movdqa	.LC8(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 48(%rcx)
	cmpq	$4, %rax
	je	.L181
	movdqa	.LC9(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 64(%rcx)
	cmpq	$5, %rax
	je	.L181
	movdqa	.LC10(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 80(%rcx)
	cmpq	$6, %rax
	je	.L181
	movdqa	.LC11(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 96(%rcx)
	cmpq	$7, %rax
	je	.L181
	movdqa	.LC12(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 112(%rcx)
	cmpq	$8, %rax
	je	.L181
	movdqa	.LC13(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 128(%rcx)
	cmpq	$9, %rax
	je	.L181
	movdqa	.LC14(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 144(%rcx)
	cmpq	$10, %rax
	je	.L181
	movdqa	.LC15(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 160(%rcx)
	cmpq	$11, %rax
	je	.L181
	movdqa	.LC16(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 176(%rcx)
	cmpq	$12, %rax
	je	.L181
	movdqa	.LC17(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 192(%rcx)
	cmpq	$13, %rax
	je	.L181
	movdqa	.LC18(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 208(%rcx)
	cmpq	$14, %rax
	je	.L181
	movdqa	.LC19(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 224(%rcx)
	cmpq	$16, %rax
	jne	.L181
	paddd	.LC20(%rip), %xmm0
	movups	%xmm0, 240(%rcx)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r9, %rax
	andq	$-4, %rax
	testb	$3, %r9b
	je	.L182
.L180:
	leaq	(%r12,%rax), %rcx
	leal	(%rsi,%rax), %edi
	movl	%edi, 0(%r13,%rcx,4)
	leaq	1(%rax), %rcx
	cmpq	%r9, %rcx
	jnb	.L182
	leaq	(%rcx,%r12), %rdi
	addq	$2, %rax
	addl	%esi, %ecx
	movl	%ecx, 0(%r13,%rdi,4)
	cmpq	%rax, %r9
	jbe	.L182
	leaq	(%r12,%rax), %rcx
	addl	%esi, %eax
	movl	%eax, 0(%r13,%rcx,4)
.L182:
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	leaq	0(%r13,%r12,4), %rcx
	movq	%r14, %rdx
	pushq	$2048
	movq	%r15, %rdi
	addq	$64, %rbx
	addq	$256, %r14
	pushq	-64(%rbp)
	pushq	%r9
	movq	%r8, -56(%rbp)
	call	BrotliHistogramCombineLiteral
	addq	$32, %rsp
	movq	-56(%rbp), %r8
	addq	%rax, %r12
	cmpq	-80(%rbp), %rbx
	jne	.L178
	movq	%r12, %r10
	movq	%r15, %r14
	movq	%r13, %r12
	movq	%r8, %r15
	movq	%r10, %rbx
	movq	%r10, %rax
	movq	-72(%rbp), %r13
	shrq	%rbx
	salq	$6, %rax
	imulq	%r10, %rbx
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	leaq	1(%rbx), %rdx
	cmpq	$2048, %rdx
	jbe	.L184
	movl	$2048, %eax
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%rax, %rcx
	addq	%rax, %rax
	cmpq	%rax, %rdx
	ja	.L185
	leaq	(%rcx,%rcx,2), %rsi
	movq	-104(%rbp), %rdi
	movq	%r10, -72(%rbp)
	salq	$4, %rsi
	call	BrotliAllocate
	movq	%r15, %rsi
	movl	$49152, %edx
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	call	BrotliFree
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r10
	movq	%rcx, %r15
	jmp	.L184
.L187:
	xorl	%eax, %eax
	jmp	.L180
.L186:
	xorl	%eax, %eax
	jmp	.L173
	.cfi_endproc
.LFE100:
	.size	BrotliClusterHistogramsLiteral, .-BrotliClusterHistogramsLiteral
	.p2align 4
	.globl	BrotliCompareAndPushToQueueCommand
	.hidden	BrotliCompareAndPushToQueueCommand
	.type	BrotliCompareAndPushToQueueCommand, @function
BrotliCompareAndPushToQueueCommand:
.LFB101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2904, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r8, -2912(%rbp)
	movq	%rax, -2904(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	je	.L239
	movl	%edx, %r12d
	movl	%ecx, %r15d
	movq	%r9, %r13
	jbe	.L242
	movl	%ecx, %r12d
	movl	%edx, %r15d
.L242:
	movl	%r15d, %ebx
	movl	%r12d, %r14d
	pxor	%xmm1, %xmm1
	movl	(%rsi,%r14,4), %ecx
	movl	(%rsi,%rbx,4), %eax
	cvtsi2sdq	%rcx, %xmm1
	leaq	(%rax,%rcx), %rsi
	cmpq	$255, %rcx
	ja	.L245
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm2, %xmm2
	cvtss2sd	(%r8,%rcx,4), %xmm2
.L246:
	mulsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpq	$255, %rax
	ja	.L249
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rax,4), %xmm0
.L250:
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rsi, %xmm2
	cmpq	$255, %rsi
	ja	.L251
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm3, %xmm3
	cvtss2sd	(%rax,%rsi,4), %xmm3
.L252:
	mulsd	%xmm3, %xmm2
	imulq	$2832, %r14, %rsi
	movq	-2904(%rbp), %rax
	imulq	$2832, %rbx, %rbx
	movq	(%rax), %rax
	addq	%rdi, %rsi
	movsd	2824(%rsi), %xmm4
	addq	%rdi, %rbx
	movq	2816(%rsi), %r8
	subsd	%xmm2, %xmm1
	mulsd	.LC2(%rip), %xmm1
	movsd	2824(%rbx), %xmm0
	subsd	%xmm4, %xmm1
	subsd	%xmm0, %xmm1
	testq	%r8, %r8
	je	.L253
	movq	2816(%rbx), %r10
	testq	%r10, %r10
	je	.L265
	movsd	.LC0(%rip), %xmm2
	testq	%rax, %rax
	je	.L254
	pxor	%xmm2, %xmm2
	maxsd	16(%r13), %xmm2
.L254:
	addq	%r10, %r8
	leaq	-2896(%rbp), %rdi
	movl	$354, %ecx
	rep movsq
	movq	%r8, -80(%rbp)
	movq	%rbx, %rcx
	leaq	-80(%rbp), %rsi
	leaq	-2896(%rbp), %rdi
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L255:
	movdqu	(%rcx), %xmm0
	paddd	(%rax), %xmm0
	addq	$16, %rax
	addq	$16, %rcx
	movaps	%xmm0, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L255
	movsd	%xmm2, -2928(%rbp)
	movsd	%xmm1, -2920(%rbp)
	call	BrotliPopulationCostCommand
	movsd	-2920(%rbp), %xmm1
	movsd	-2928(%rbp), %xmm2
	subsd	%xmm1, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L239
	movq	-2904(%rbp), %rax
	movq	(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L253:
	addsd	%xmm0, %xmm1
	testq	%rax, %rax
	je	.L258
	movsd	16(%r13), %xmm2
	ucomisd	%xmm2, %xmm1
	jp	.L269
	jne	.L269
	movl	%r15d, %esi
	movl	4(%r13), %ecx
	subl	0(%r13), %ecx
	subl	%r12d, %esi
	cmpl	%esi, %ecx
	seta	%cl
	movzbl	%cl, %ecx
.L261:
	testl	%ecx, %ecx
	je	.L258
	cmpq	%rax, -2912(%rbp)
	jbe	.L262
	movdqu	0(%r13), %xmm5
	leaq	(%rax,%rax,2), %rcx
	movq	-2904(%rbp), %rdx
	addq	$1, %rax
	leaq	0(%r13,%rcx,8), %rcx
	movups	%xmm5, (%rcx)
	movq	16(%r13), %rsi
	movq	%rsi, 16(%rcx)
	movq	%rax, (%rdx)
.L262:
	movl	%r12d, 0(%r13)
	unpcklpd	%xmm1, %xmm0
	movl	%r15d, 4(%r13)
	movups	%xmm0, 8(%r13)
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$2904, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	movq	%rax, -2944(%rbp)
	movq	%rdi, -2936(%rbp)
	movq	%rsi, -2928(%rbp)
	movsd	%xmm1, -2920(%rbp)
	call	log2@PLT
	movq	-2944(%rbp), %rax
	movq	-2936(%rbp), %rdi
	movq	-2928(%rbp), %rsi
	movsd	-2920(%rbp), %xmm1
	movapd	%xmm0, %xmm2
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L258:
	cmpq	%rax, -2912(%rbp)
	jbe	.L239
	leaq	(%rax,%rax,2), %rcx
	movq	-2904(%rbp), %rdx
	unpcklpd	%xmm1, %xmm0
	addq	$1, %rax
	leaq	0(%r13,%rcx,8), %rcx
	movl	%r12d, (%rcx)
	movl	%r15d, 4(%rcx)
	movups	%xmm0, 8(%rcx)
	movq	%rax, (%rdx)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L251:
	movapd	%xmm2, %xmm0
	movq	%rdi, -2936(%rbp)
	movsd	%xmm1, -2928(%rbp)
	movsd	%xmm2, -2920(%rbp)
	call	log2@PLT
	movq	-2936(%rbp), %rdi
	movsd	-2928(%rbp), %xmm1
	movsd	-2920(%rbp), %xmm2
	movapd	%xmm0, %xmm3
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L249:
	movapd	%xmm1, %xmm0
	movq	%rdi, -2944(%rbp)
	movq	%rsi, -2928(%rbp)
	movsd	%xmm2, -2936(%rbp)
	movsd	%xmm1, -2920(%rbp)
	call	log2@PLT
	movq	-2944(%rbp), %rdi
	movsd	-2936(%rbp), %xmm2
	movq	-2928(%rbp), %rsi
	movsd	-2920(%rbp), %xmm1
	jmp	.L250
.L265:
	movapd	%xmm4, %xmm0
	jmp	.L253
.L269:
	xorl	%ecx, %ecx
	comisd	%xmm1, %xmm2
	seta	%cl
	jmp	.L261
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE101:
	.size	BrotliCompareAndPushToQueueCommand, .-BrotliCompareAndPushToQueueCommand
	.p2align 4
	.globl	BrotliHistogramCombineCommand
	.hidden	BrotliHistogramCombineCommand
	.type	BrotliHistogramCombineCommand, @function
BrotliHistogramCombineCommand:
.LFB102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	%rdi, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r9, -72(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	$0, -64(%rbp)
	testq	%r9, %r9
	je	.L285
	leaq	-64(%rbp), %rdi
	movl	$1, %r14d
	movq	%rdi, -88(%rbp)
	cmpq	$1, %r9
	jbe	.L285
	movq	%r14, -80(%rbp)
	movq	-96(%rbp), %r15
	movq	%r8, %r14
	movq	-80(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r14, %rax
	movq	%r13, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L288:
	movq	-80(%rbp), %rax
	movl	(%r14,%rbx,4), %ecx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	32(%rbp), %r8
	movq	%r13, %r9
	movq	%r15, %rdi
	addq	$1, %rbx
	movl	-4(%r14,%rax,4), %edx
	pushq	-88(%rbp)
	call	BrotliCompareAndPushToQueueCommand
	popq	%rcx
	popq	%rsi
	cmpq	%rbx, -72(%rbp)
	ja	.L288
	movq	%r13, %rax
	addq	$1, -80(%rbp)
	movq	%r14, %r13
	movq	%rax, %r14
	movq	-80(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L338
	movq	%rax, %rbx
	jmp	.L286
.L338:
	movq	%r14, %rbx
.L285:
	movq	-120(%rbp), %rax
	movq	16(%rbp), %rdi
	movq	$1, -88(%rbp)
	pxor	%xmm7, %xmm7
	movsd	%xmm7, -104(%rbp)
	leaq	(%rax,%rdi,4), %rax
	movq	%rax, -128(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -112(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rax, %r12
.L291:
	movq	-72(%rbp), %rdi
	cmpq	%rdi, -88(%rbp)
	jnb	.L343
.L312:
	movsd	16(%rbx), %xmm0
	comisd	-104(%rbp), %xmm0
	jb	.L344
	movq	24(%rbp), %rax
	movq	-72(%rbp), %rdi
	movq	%rax, -88(%rbp)
	movq	.LC0(%rip), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rdi, -88(%rbp)
	jb	.L312
.L343:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L345
	leaq	-40(%rbp), %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L344:
	.cfi_restore_state
	movl	(%rbx), %esi
	movl	4(%rbx), %edi
	movq	-96(%rbp), %rax
	imulq	$2832, %rdi, %rcx
	movq	%rsi, %r15
	movq	%rdi, %r14
	imulq	$2832, %rsi, %rdx
	addq	%rax, %rcx
	addq	%rax, %rdx
	movq	2816(%rcx), %rax
	addq	%rax, 2816(%rdx)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L292:
	movdqu	(%rdx,%rax), %xmm0
	movdqu	(%rcx,%rax), %xmm2
	paddd	%xmm2, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$2816, %rax
	jne	.L292
	movsd	8(%rbx), %xmm0
	movl	0(%r13,%rdi,4), %eax
	movsd	%xmm0, 2824(%rdx)
	movq	-128(%rbp), %rdx
	addl	%eax, 0(%r13,%rsi,4)
	cmpq	$0, 16(%rbp)
	movq	-120(%rbp), %rax
	je	.L297
	.p2align 4,,10
	.p2align 3
.L293:
	cmpl	%r14d, (%rax)
	jne	.L296
	movl	%r15d, (%rax)
.L296:
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L293
.L297:
	subq	$1, -72(%rbp)
	xorl	%eax, %eax
	cmpq	$0, -112(%rbp)
	movq	%r12, %rdi
	movq	-64(%rbp), %r8
	movq	-112(%rbp), %rcx
	jne	.L300
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L298:
	addq	$4, %rdi
	cmpq	%rcx, %rax
	je	.L299
.L300:
	movq	%rax, %rdx
	addq	$1, %rax
	cmpl	%r14d, (%rdi)
	jne	.L298
	movq	-72(%rbp), %rsi
	movq	%r8, -136(%rbp)
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	leaq	(%r12,%rax,4), %rsi
	salq	$2, %rdx
	call	memmove@PLT
	movq	-136(%rbp), %r8
.L299:
	testq	%r8, %r8
	je	.L318
.L313:
	movq	%rbx, %rax
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L347:
	movdqu	(%rax), %xmm5
	movsd	8(%rbx), %xmm1
	movq	(%rbx), %rsi
	movups	%xmm5, (%rbx)
	movq	16(%rax), %r9
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm0, %xmm3
	movq	%r9, 16(%rbx)
	movq	%rsi, (%rcx)
	movups	%xmm3, 8(%rcx)
.L307:
	addq	$1, %rdi
.L302:
	addq	$1, %rdx
	addq	$24, %rax
	cmpq	%r8, %rdx
	jnb	.L301
.L308:
	movl	(%rax), %ecx
	cmpl	%r15d, %ecx
	je	.L302
	movl	4(%rax), %esi
	cmpl	%r15d, %esi
	sete	%r10b
	cmpl	%r14d, %ecx
	sete	%r9b
	orb	%r9b, %r10b
	jne	.L302
	cmpl	%r14d, %esi
	je	.L302
	movsd	16(%rbx), %xmm0
	movsd	16(%rax), %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L319
	jne	.L319
	subl	%ecx, %esi
	movl	4(%rbx), %r9d
	subl	(%rbx), %r9d
	cmpl	%esi, %r9d
	seta	%sil
	movzbl	%sil, %esi
.L305:
	leaq	(%rdi,%rdi,2), %rcx
	leaq	(%rbx,%rcx,8), %rcx
	testl	%esi, %esi
	jne	.L347
	movdqu	(%rax), %xmm6
	movups	%xmm6, (%rcx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L319:
	xorl	%esi, %esi
	comisd	%xmm1, %xmm0
	seta	%sil
	jmp	.L305
.L318:
	xorl	%edi, %edi
.L301:
	cmpq	$0, -72(%rbp)
	movq	%rdi, -64(%rbp)
	je	.L311
.L309:
	xorl	%r14d, %r14d
	movq	%rbx, %rax
	movq	%r12, %rbx
	movq	%r14, %r12
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L310:
	subq	$8, %rsp
	movl	(%rbx,%r12,4), %ecx
	pushq	-80(%rbp)
	movl	%r15d, %edx
	movq	32(%rbp), %r8
	movq	%r14, %r9
	movq	%r13, %rsi
	addq	$1, %r12
	movq	-96(%rbp), %rdi
	call	BrotliCompareAndPushToQueueCommand
	popq	%rax
	popq	%rdx
	cmpq	-72(%rbp), %r12
	jb	.L310
	movq	%rbx, %r12
	movq	%r14, %rbx
.L311:
	subq	$1, -112(%rbp)
	jmp	.L291
.L346:
	testq	%r8, %r8
	jne	.L313
	jmp	.L309
.L345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE102:
	.size	BrotliHistogramCombineCommand, .-BrotliHistogramCombineCommand
	.p2align 4
	.globl	BrotliHistogramBitCostDistanceCommand
	.hidden	BrotliHistogramBitCostDistanceCommand
	.type	BrotliHistogramBitCostDistanceCommand, @function
BrotliHistogramBitCostDistanceCommand:
.LFB103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$2856, %rsp
	movq	2816(%rdi), %rdx
	movq	%fs:40, %rsi
	movq	%rsi, -24(%rbp)
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.L348
	movq	%rdi, %rax
	addq	2816(%rbx), %rdx
	movl	$354, %ecx
	movq	%rax, %rsi
	leaq	-2864(%rbp), %rdi
	rep movsq
	leaq	-2864(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	leaq	-48(%rbp), %rcx
	movq	%rdi, %rax
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L350:
	movdqu	(%rdx), %xmm0
	paddd	(%rax), %xmm0
	addq	$16, %rax
	addq	$16, %rdx
	movaps	%xmm0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L350
	call	BrotliPopulationCostCommand
	subsd	2824(%rbx), %xmm0
.L348:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L357
	addq	$2856, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L357:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE103:
	.size	BrotliHistogramBitCostDistanceCommand, .-BrotliHistogramBitCostDistanceCommand
	.p2align 4
	.globl	BrotliHistogramRemapCommand
	.hidden	BrotliHistogramRemapCommand
	.type	BrotliHistogramRemapCommand, @function
BrotliHistogramRemapCommand:
.LFB104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	testq	%rsi, %rsi
	je	.L359
	movq	$0, -64(%rbp)
	movq	%rdi, %r12
	leaq	(%rdx,%rcx,4), %r13
.L366:
	cmpq	$0, -64(%rbp)
	movq	-72(%rbp), %rax
	je	.L385
	movq	-64(%rbp), %rdi
	movl	-4(%rax,%rdi,4), %r14d
.L361:
	movl	%r14d, %esi
	movq	%r12, %rdi
	imulq	$2832, %rsi, %rsi
	addq	%rbx, %rsi
	call	BrotliHistogramBitCostDistanceCommand
	cmpq	$0, -80(%rbp)
	movsd	%xmm0, -56(%rbp)
	je	.L362
	movq	-96(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L365:
	movl	(%r15), %esi
	movq	%r12, %rdi
	imulq	$2832, %rsi, %rsi
	addq	%rbx, %rsi
	call	BrotliHistogramBitCostDistanceCommand
	movsd	-56(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L363
	movl	(%r15), %r14d
	movsd	%xmm0, -56(%rbp)
.L363:
	addq	$4, %r15
	cmpq	%r13, %r15
	jne	.L365
.L362:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdi
	addq	$2832, %r12
	movl	%r14d, (%rdi,%rax,4)
	addq	$1, %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, -88(%rbp)
	jne	.L366
	cmpq	$0, -80(%rbp)
	je	.L369
.L373:
	movsd	.LC3(%rip), %xmm0
	xorl	%esi, %esi
	xorl	%eax, %eax
.L368:
	movq	-96(%rbp), %rdi
	movl	(%rdi,%rsi,4), %edx
	addq	$1, %rsi
	imulq	$2832, %rdx, %rdx
	addq	%rbx, %rdx
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, 2808(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movsd	%xmm0, 2824(%rdx)
	movq	$0, 2816(%rdx)
	cmpq	%rsi, -80(%rbp)
	ja	.L368
	cmpq	$0, -88(%rbp)
	je	.L358
.L369:
	movq	-104(%rbp), %rcx
	xorl	%esi, %esi
.L371:
	movq	-72(%rbp), %rax
	movl	(%rax,%rsi,4), %edx
	movq	2816(%rcx), %rax
	imulq	$2832, %rdx, %rdx
	addq	%rbx, %rdx
	addq	%rax, 2816(%rdx)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L370:
	movdqu	(%rcx,%rax), %xmm0
	movdqu	(%rdx,%rax), %xmm1
	paddd	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$2816, %rax
	jne	.L370
	addq	$1, %rsi
	addq	$2832, %rcx
	cmpq	%rsi, -88(%rbp)
	ja	.L371
.L358:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L385:
	.cfi_restore_state
	movl	(%rax), %r14d
	jmp	.L361
.L359:
	cmpq	$0, -80(%rbp)
	jne	.L373
	jmp	.L358
	.cfi_endproc
.LFE104:
	.size	BrotliHistogramRemapCommand, .-BrotliHistogramRemapCommand
	.p2align 4
	.globl	BrotliHistogramReindexCommand
	.hidden	BrotliHistogramReindexCommand
	.type	BrotliHistogramReindexCommand, @function
BrotliHistogramReindexCommand:
.LFB105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	jne	.L406
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	call	BrotliFree
	xorl	%r9d, %r9d
.L394:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	call	BrotliFree
	movq	-56(%rbp), %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	movq	%r9, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	leaq	0(,%rcx,4), %r15
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movq	%r15, %rsi
	call	BrotliAllocate
	movq	%r15, %rdx
	movl	$255, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	memset@PLT
	leaq	(%rbx,%r15), %r8
	movq	%rbx, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L389:
	movl	(%rax), %edx
	leaq	(%r14,%rdx,4), %rdx
	cmpl	$-1, (%rdx)
	jne	.L388
	movl	%ecx, (%rdx)
	addl	$1, %ecx
.L388:
	addq	$4, %rax
	cmpq	%rax, %r8
	jne	.L389
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	je	.L390
	imulq	$2832, %rcx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BrotliAllocate
	movq	-56(%rbp), %r8
	movq	%rax, %r15
.L390:
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L396:
	movl	(%rbx), %esi
	movq	%rdx, %r9
	movl	(%r14,%rsi,4), %ecx
	cmpl	%eax, %ecx
	je	.L407
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	cmpq	%r8, %rbx
	jne	.L396
.L393:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	BrotliFree
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %r9
	testq	%rdx, %rdx
	je	.L394
	imulq	$2832, %rdx, %rdx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L395:
	leaq	(%r12,%r8), %rdi
	leaq	(%r15,%r8), %rsi
	addq	$2832, %r8
	movl	$354, %ecx
	rep movsq
	cmpq	%r8, %rdx
	jne	.L395
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L407:
	imulq	$2832, %rdx, %rdx
	movl	$354, %ecx
	addl	$1, %eax
	addq	$4, %rbx
	imulq	$2832, %rsi, %rsi
	addq	%r15, %rdx
	movq	%rdx, %rdi
	addq	%r12, %rsi
	rep movsq
	movl	-4(%rbx), %edx
	movl	(%r14,%rdx,4), %ecx
	movl	%eax, %edx
	movq	%rdx, %r9
	movl	%ecx, -4(%rbx)
	cmpq	%r8, %rbx
	jne	.L396
	jmp	.L393
	.cfi_endproc
.LFE105:
	.size	BrotliHistogramReindexCommand, .-BrotliHistogramReindexCommand
	.p2align 4
	.globl	BrotliClusterHistogramsCommand
	.hidden	BrotliClusterHistogramsCommand
	.type	BrotliClusterHistogramsCommand, @function
BrotliClusterHistogramsCommand:
.LFB106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r9, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L475
	movl	$49176, %esi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	BrotliAllocate
	xorl	%r10d, %r10d
	movq	%rax, %r15
.L421:
	subq	$8, %rsp
	movq	16(%rbp), %rdx
	movq	%r10, %r9
	movq	%r15, %r8
	pushq	%rbx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	pushq	-64(%rbp)
	pushq	-96(%rbp)
	call	BrotliHistogramCombineCommand
	movq	%r15, %rsi
	movq	-104(%rbp), %r15
	addq	$32, %rsp
	movq	%rax, %rbx
	movq	%r15, %rdi
	call	BrotliFree
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	%rbx, %rcx
	movq	-96(%rbp), %rbx
	movq	16(%rbp), %r9
	movq	-112(%rbp), %rdi
	movq	%r14, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	BrotliHistogramRemapCommand
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	16(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rsi
	call	BrotliHistogramReindexCommand
	movq	-120(%rbp), %rdi
	movq	%rax, (%rdi)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L475:
	.cfi_restore_state
	leaq	0(,%rdx,4), %r12
	movq	%rdi, %r15
	movq	%rdx, %rbx
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BrotliAllocate
	movq	%r15, %rdi
	movl	$49176, %esi
	movq	%rax, %r12
	call	BrotliAllocate
	movq	%rax, %r15
	leaq	-1(%rbx), %rax
	cmpq	$2, %rax
	jbe	.L423
	shrq	$2, %rbx
	movdqa	.LC4(%rip), %xmm0
	movq	%r13, %rax
	movq	%rbx, %rdx
	salq	$4, %rdx
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L411:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L411
	movq	-96(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-4, %rax
	andl	$3, %edi
	je	.L412
.L410:
	movq	-96(%rbp), %rsi
	leaq	1(%rax), %rdx
	movl	$1, 0(%r13,%rax,4)
	cmpq	%rdx, %rsi
	jbe	.L412
	movl	$1, 0(%r13,%rdx,4)
	addq	$2, %rax
	cmpq	%rax, %rsi
	jbe	.L412
	movl	$1, 0(%r13,%rax,4)
.L412:
	xorl	%ebx, %ebx
	movq	-112(%rbp), %r8
	movq	%r15, -56(%rbp)
	leaq	2824(%r14), %rax
	movq	%r12, -80(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r12
	movq	16(%rbp), %rbx
	movq	%r13, -72(%rbp)
	movq	%r8, %r13
	movq	%r14, -88(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L413:
	leaq	-2824(%r14), %rdi
	movq	%r13, %rsi
	movl	$354, %ecx
	addq	$2832, %r14
	rep movsq
	movq	%r13, %rdi
	addq	$2832, %r13
	call	BrotliPopulationCostCommand
	movq	%r12, %rcx
	movsd	%xmm0, -2832(%r14)
	movl	%r12d, (%rbx,%r12,4)
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L413
	andq	$-64, %rcx
	movq	-88(%rbp), %r14
	movq	%r12, %rbx
	movq	-56(%rbp), %r15
	leaq	64(%rcx), %rax
	movq	-80(%rbp), %r12
	movq	16(%rbp), %rdx
	xorl	%r11d, %r11d
	movq	%rax, -80(%rbp)
	xorl	%r10d, %r10d
	movq	%r15, %r8
	movq	%r14, %r15
	movq	%rbx, -88(%rbp)
	movq	%r12, %r13
	movq	%r11, %rbx
	movq	%r10, %r12
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L415:
	movq	-88(%rbp), %rdx
	movl	$64, %r9d
	movq	%rdx, %rax
	subq	%rbx, %rax
	cmpq	$64, %rax
	cmovbe	%rax, %r9
	cmpq	%rbx, %rdx
	je	.L419
	movl	%ebx, %esi
	cmpq	$3, %rax
	jbe	.L424
	movdqa	.LC5(%rip), %xmm1
	movd	%ebx, %xmm2
	movq	%r9, %rax
	leaq	0(%r13,%r12,4), %rcx
	pshufd	$0, %xmm2, %xmm0
	shrq	$2, %rax
	paddd	%xmm0, %xmm1
	movups	%xmm1, (%rcx)
	cmpq	$1, %rax
	je	.L418
	movdqa	.LC6(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 16(%rcx)
	cmpq	$2, %rax
	je	.L418
	movdqa	.LC7(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 32(%rcx)
	cmpq	$3, %rax
	je	.L418
	movdqa	.LC8(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 48(%rcx)
	cmpq	$4, %rax
	je	.L418
	movdqa	.LC9(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 64(%rcx)
	cmpq	$5, %rax
	je	.L418
	movdqa	.LC10(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 80(%rcx)
	cmpq	$6, %rax
	je	.L418
	movdqa	.LC11(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 96(%rcx)
	cmpq	$7, %rax
	je	.L418
	movdqa	.LC12(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 112(%rcx)
	cmpq	$8, %rax
	je	.L418
	movdqa	.LC13(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 128(%rcx)
	cmpq	$9, %rax
	je	.L418
	movdqa	.LC14(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 144(%rcx)
	cmpq	$10, %rax
	je	.L418
	movdqa	.LC15(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 160(%rcx)
	cmpq	$11, %rax
	je	.L418
	movdqa	.LC16(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 176(%rcx)
	cmpq	$12, %rax
	je	.L418
	movdqa	.LC17(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 192(%rcx)
	cmpq	$13, %rax
	je	.L418
	movdqa	.LC18(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 208(%rcx)
	cmpq	$14, %rax
	je	.L418
	movdqa	.LC19(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 224(%rcx)
	cmpq	$16, %rax
	jne	.L418
	paddd	.LC20(%rip), %xmm0
	movups	%xmm0, 240(%rcx)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r9, %rax
	andq	$-4, %rax
	testb	$3, %r9b
	je	.L419
.L417:
	leaq	(%r12,%rax), %rcx
	leal	(%rsi,%rax), %edi
	movl	%edi, 0(%r13,%rcx,4)
	leaq	1(%rax), %rcx
	cmpq	%r9, %rcx
	jnb	.L419
	leaq	(%rcx,%r12), %rdi
	addq	$2, %rax
	addl	%esi, %ecx
	movl	%ecx, 0(%r13,%rdi,4)
	cmpq	%rax, %r9
	jbe	.L419
	leaq	(%r12,%rax), %rcx
	addl	%esi, %eax
	movl	%eax, 0(%r13,%rcx,4)
.L419:
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	leaq	0(%r13,%r12,4), %rcx
	movq	%r14, %rdx
	pushq	$2048
	movq	%r15, %rdi
	addq	$64, %rbx
	addq	$256, %r14
	pushq	-64(%rbp)
	pushq	%r9
	movq	%r8, -56(%rbp)
	call	BrotliHistogramCombineCommand
	addq	$32, %rsp
	movq	-56(%rbp), %r8
	addq	%rax, %r12
	cmpq	-80(%rbp), %rbx
	jne	.L415
	movq	%r12, %r10
	movq	%r15, %r14
	movq	%r13, %r12
	movq	%r8, %r15
	movq	%r10, %rbx
	movq	%r10, %rax
	movq	-72(%rbp), %r13
	shrq	%rbx
	salq	$6, %rax
	imulq	%r10, %rbx
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	leaq	1(%rbx), %rdx
	cmpq	$2048, %rdx
	jbe	.L421
	movl	$2048, %eax
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%rax, %rcx
	addq	%rax, %rax
	cmpq	%rax, %rdx
	ja	.L422
	leaq	(%rcx,%rcx,2), %rsi
	movq	-104(%rbp), %rdi
	movq	%r10, -72(%rbp)
	salq	$4, %rsi
	call	BrotliAllocate
	movq	%r15, %rsi
	movl	$49152, %edx
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	call	BrotliFree
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r10
	movq	%rcx, %r15
	jmp	.L421
.L424:
	xorl	%eax, %eax
	jmp	.L417
.L423:
	xorl	%eax, %eax
	jmp	.L410
	.cfi_endproc
.LFE106:
	.size	BrotliClusterHistogramsCommand, .-BrotliClusterHistogramsCommand
	.p2align 4
	.globl	BrotliCompareAndPushToQueueDistance
	.hidden	BrotliCompareAndPushToQueueDistance
	.type	BrotliCompareAndPushToQueueDistance, @function
BrotliCompareAndPushToQueueDistance:
.LFB107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r8, -2272(%rbp)
	movq	%rax, -2264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	je	.L476
	movl	%edx, %r12d
	movl	%ecx, %r15d
	movq	%r9, %r13
	jbe	.L479
	movl	%ecx, %r12d
	movl	%edx, %r15d
.L479:
	movl	%r15d, %ebx
	movl	%r12d, %r14d
	pxor	%xmm1, %xmm1
	movl	(%rsi,%r14,4), %ecx
	movl	(%rsi,%rbx,4), %eax
	cvtsi2sdq	%rcx, %xmm1
	leaq	(%rax,%rcx), %rsi
	cmpq	$255, %rcx
	ja	.L482
	leaq	kLog2Table(%rip), %r8
	pxor	%xmm2, %xmm2
	cvtss2sd	(%r8,%rcx,4), %xmm2
.L483:
	mulsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpq	$255, %rax
	ja	.L486
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rax,4), %xmm0
.L487:
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rsi, %xmm2
	cmpq	$255, %rsi
	ja	.L488
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm3, %xmm3
	cvtss2sd	(%rax,%rsi,4), %xmm3
.L489:
	mulsd	%xmm3, %xmm2
	imulq	$2192, %r14, %rsi
	movq	-2264(%rbp), %rax
	imulq	$2192, %rbx, %rbx
	movq	(%rax), %rax
	addq	%rdi, %rsi
	movsd	2184(%rsi), %xmm4
	addq	%rdi, %rbx
	movq	2176(%rsi), %r8
	subsd	%xmm2, %xmm1
	mulsd	.LC2(%rip), %xmm1
	movsd	2184(%rbx), %xmm0
	subsd	%xmm4, %xmm1
	subsd	%xmm0, %xmm1
	testq	%r8, %r8
	je	.L490
	movq	2176(%rbx), %r10
	testq	%r10, %r10
	je	.L502
	movsd	.LC0(%rip), %xmm2
	testq	%rax, %rax
	je	.L491
	pxor	%xmm2, %xmm2
	maxsd	16(%r13), %xmm2
.L491:
	addq	%r10, %r8
	leaq	-2256(%rbp), %rdi
	movl	$274, %ecx
	rep movsq
	movq	%r8, -80(%rbp)
	movq	%rbx, %rcx
	leaq	-80(%rbp), %rsi
	leaq	-2256(%rbp), %rdi
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L492:
	movdqu	(%rcx), %xmm0
	paddd	(%rax), %xmm0
	addq	$16, %rax
	addq	$16, %rcx
	movaps	%xmm0, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L492
	movsd	%xmm2, -2288(%rbp)
	movsd	%xmm1, -2280(%rbp)
	call	BrotliPopulationCostDistance
	movsd	-2280(%rbp), %xmm1
	movsd	-2288(%rbp), %xmm2
	subsd	%xmm1, %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L476
	movq	-2264(%rbp), %rax
	movq	(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L490:
	addsd	%xmm0, %xmm1
	testq	%rax, %rax
	je	.L495
	movsd	16(%r13), %xmm2
	ucomisd	%xmm2, %xmm1
	jp	.L506
	jne	.L506
	movl	%r15d, %esi
	movl	4(%r13), %ecx
	subl	0(%r13), %ecx
	subl	%r12d, %esi
	cmpl	%esi, %ecx
	seta	%cl
	movzbl	%cl, %ecx
.L498:
	testl	%ecx, %ecx
	je	.L495
	cmpq	%rax, -2272(%rbp)
	jbe	.L499
	movdqu	0(%r13), %xmm5
	leaq	(%rax,%rax,2), %rcx
	movq	-2264(%rbp), %rdx
	addq	$1, %rax
	leaq	0(%r13,%rcx,8), %rcx
	movups	%xmm5, (%rcx)
	movq	16(%r13), %rsi
	movq	%rsi, 16(%rcx)
	movq	%rax, (%rdx)
.L499:
	movl	%r12d, 0(%r13)
	unpcklpd	%xmm1, %xmm0
	movl	%r15d, 4(%r13)
	movups	%xmm0, 8(%r13)
	.p2align 4,,10
	.p2align 3
.L476:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	addq	$2264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	movapd	%xmm1, %xmm0
	movq	%rax, -2304(%rbp)
	movq	%rdi, -2296(%rbp)
	movq	%rsi, -2288(%rbp)
	movsd	%xmm1, -2280(%rbp)
	call	log2@PLT
	movq	-2304(%rbp), %rax
	movq	-2296(%rbp), %rdi
	movq	-2288(%rbp), %rsi
	movsd	-2280(%rbp), %xmm1
	movapd	%xmm0, %xmm2
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L495:
	cmpq	%rax, -2272(%rbp)
	jbe	.L476
	leaq	(%rax,%rax,2), %rcx
	movq	-2264(%rbp), %rdx
	unpcklpd	%xmm1, %xmm0
	addq	$1, %rax
	leaq	0(%r13,%rcx,8), %rcx
	movl	%r12d, (%rcx)
	movl	%r15d, 4(%rcx)
	movups	%xmm0, 8(%rcx)
	movq	%rax, (%rdx)
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L488:
	movapd	%xmm2, %xmm0
	movq	%rdi, -2296(%rbp)
	movsd	%xmm1, -2288(%rbp)
	movsd	%xmm2, -2280(%rbp)
	call	log2@PLT
	movq	-2296(%rbp), %rdi
	movsd	-2288(%rbp), %xmm1
	movsd	-2280(%rbp), %xmm2
	movapd	%xmm0, %xmm3
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L486:
	movapd	%xmm1, %xmm0
	movq	%rdi, -2304(%rbp)
	movq	%rsi, -2288(%rbp)
	movsd	%xmm2, -2296(%rbp)
	movsd	%xmm1, -2280(%rbp)
	call	log2@PLT
	movq	-2304(%rbp), %rdi
	movsd	-2296(%rbp), %xmm2
	movq	-2288(%rbp), %rsi
	movsd	-2280(%rbp), %xmm1
	jmp	.L487
.L502:
	movapd	%xmm4, %xmm0
	jmp	.L490
.L506:
	xorl	%ecx, %ecx
	comisd	%xmm1, %xmm2
	seta	%cl
	jmp	.L498
.L520:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE107:
	.size	BrotliCompareAndPushToQueueDistance, .-BrotliCompareAndPushToQueueDistance
	.p2align 4
	.globl	BrotliHistogramCombineDistance
	.hidden	BrotliHistogramCombineDistance
	.type	BrotliHistogramCombineDistance, @function
BrotliHistogramCombineDistance:
.LFB108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	%rdi, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r9, -72(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	$0, -64(%rbp)
	testq	%r9, %r9
	je	.L522
	leaq	-64(%rbp), %rdi
	movl	$1, %r14d
	movq	%rdi, -88(%rbp)
	cmpq	$1, %r9
	jbe	.L522
	movq	%r14, -80(%rbp)
	movq	-96(%rbp), %r15
	movq	%r8, %r14
	movq	-80(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%r14, %rax
	movq	%r13, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L525:
	movq	-80(%rbp), %rax
	movl	(%r14,%rbx,4), %ecx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	32(%rbp), %r8
	movq	%r13, %r9
	movq	%r15, %rdi
	addq	$1, %rbx
	movl	-4(%r14,%rax,4), %edx
	pushq	-88(%rbp)
	call	BrotliCompareAndPushToQueueDistance
	popq	%rcx
	popq	%rsi
	cmpq	%rbx, -72(%rbp)
	ja	.L525
	movq	%r13, %rax
	addq	$1, -80(%rbp)
	movq	%r14, %r13
	movq	%rax, %r14
	movq	-80(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L575
	movq	%rax, %rbx
	jmp	.L523
.L575:
	movq	%r14, %rbx
.L522:
	movq	-120(%rbp), %rax
	movq	16(%rbp), %rdi
	movq	$1, -88(%rbp)
	pxor	%xmm7, %xmm7
	movsd	%xmm7, -104(%rbp)
	leaq	(%rax,%rdi,4), %rax
	movq	%rax, -128(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -112(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rax, %r12
.L528:
	movq	-72(%rbp), %rdi
	cmpq	%rdi, -88(%rbp)
	jnb	.L580
.L549:
	movsd	16(%rbx), %xmm0
	comisd	-104(%rbp), %xmm0
	jb	.L581
	movq	24(%rbp), %rax
	movq	-72(%rbp), %rdi
	movq	%rax, -88(%rbp)
	movq	.LC0(%rip), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rdi, -88(%rbp)
	jb	.L549
.L580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L582
	leaq	-40(%rbp), %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L581:
	.cfi_restore_state
	movl	(%rbx), %esi
	movl	4(%rbx), %edi
	movq	-96(%rbp), %rax
	imulq	$2192, %rdi, %rcx
	movq	%rsi, %r15
	movq	%rdi, %r14
	imulq	$2192, %rsi, %rdx
	addq	%rax, %rcx
	addq	%rax, %rdx
	movq	2176(%rcx), %rax
	addq	%rax, 2176(%rdx)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L529:
	movdqu	(%rdx,%rax), %xmm0
	movdqu	(%rcx,%rax), %xmm2
	paddd	%xmm2, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$2176, %rax
	jne	.L529
	movsd	8(%rbx), %xmm0
	movl	0(%r13,%rdi,4), %eax
	movsd	%xmm0, 2184(%rdx)
	movq	-128(%rbp), %rdx
	addl	%eax, 0(%r13,%rsi,4)
	cmpq	$0, 16(%rbp)
	movq	-120(%rbp), %rax
	je	.L534
	.p2align 4,,10
	.p2align 3
.L530:
	cmpl	%r14d, (%rax)
	jne	.L533
	movl	%r15d, (%rax)
.L533:
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L530
.L534:
	subq	$1, -72(%rbp)
	xorl	%eax, %eax
	cmpq	$0, -112(%rbp)
	movq	%r12, %rdi
	movq	-64(%rbp), %r8
	movq	-112(%rbp), %rcx
	jne	.L537
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L535:
	addq	$4, %rdi
	cmpq	%rcx, %rax
	je	.L536
.L537:
	movq	%rax, %rdx
	addq	$1, %rax
	cmpl	%r14d, (%rdi)
	jne	.L535
	movq	-72(%rbp), %rsi
	movq	%r8, -136(%rbp)
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	leaq	(%r12,%rax,4), %rsi
	salq	$2, %rdx
	call	memmove@PLT
	movq	-136(%rbp), %r8
.L536:
	testq	%r8, %r8
	je	.L555
.L550:
	movq	%rbx, %rax
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L584:
	movdqu	(%rax), %xmm5
	movsd	8(%rbx), %xmm1
	movq	(%rbx), %rsi
	movups	%xmm5, (%rbx)
	movq	16(%rax), %r9
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm0, %xmm3
	movq	%r9, 16(%rbx)
	movq	%rsi, (%rcx)
	movups	%xmm3, 8(%rcx)
.L544:
	addq	$1, %rdi
.L539:
	addq	$1, %rdx
	addq	$24, %rax
	cmpq	%r8, %rdx
	jnb	.L538
.L545:
	movl	(%rax), %ecx
	cmpl	%r15d, %ecx
	je	.L539
	movl	4(%rax), %esi
	cmpl	%r15d, %esi
	sete	%r10b
	cmpl	%r14d, %ecx
	sete	%r9b
	orb	%r9b, %r10b
	jne	.L539
	cmpl	%r14d, %esi
	je	.L539
	movsd	16(%rbx), %xmm0
	movsd	16(%rax), %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L556
	jne	.L556
	subl	%ecx, %esi
	movl	4(%rbx), %r9d
	subl	(%rbx), %r9d
	cmpl	%esi, %r9d
	seta	%sil
	movzbl	%sil, %esi
.L542:
	leaq	(%rdi,%rdi,2), %rcx
	leaq	(%rbx,%rcx,8), %rcx
	testl	%esi, %esi
	jne	.L584
	movdqu	(%rax), %xmm6
	movups	%xmm6, (%rcx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L556:
	xorl	%esi, %esi
	comisd	%xmm1, %xmm0
	seta	%sil
	jmp	.L542
.L555:
	xorl	%edi, %edi
.L538:
	cmpq	$0, -72(%rbp)
	movq	%rdi, -64(%rbp)
	je	.L548
.L546:
	xorl	%r14d, %r14d
	movq	%rbx, %rax
	movq	%r12, %rbx
	movq	%r14, %r12
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L547:
	subq	$8, %rsp
	movl	(%rbx,%r12,4), %ecx
	pushq	-80(%rbp)
	movl	%r15d, %edx
	movq	32(%rbp), %r8
	movq	%r14, %r9
	movq	%r13, %rsi
	addq	$1, %r12
	movq	-96(%rbp), %rdi
	call	BrotliCompareAndPushToQueueDistance
	popq	%rax
	popq	%rdx
	cmpq	-72(%rbp), %r12
	jb	.L547
	movq	%rbx, %r12
	movq	%r14, %rbx
.L548:
	subq	$1, -112(%rbp)
	jmp	.L528
.L583:
	testq	%r8, %r8
	jne	.L550
	jmp	.L546
.L582:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE108:
	.size	BrotliHistogramCombineDistance, .-BrotliHistogramCombineDistance
	.p2align 4
	.globl	BrotliHistogramBitCostDistanceDistance
	.hidden	BrotliHistogramBitCostDistanceDistance
	.type	BrotliHistogramBitCostDistanceDistance, @function
BrotliHistogramBitCostDistanceDistance:
.LFB109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$2216, %rsp
	movq	2176(%rdi), %rdx
	movq	%fs:40, %rsi
	movq	%rsi, -24(%rbp)
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.L585
	movq	%rdi, %rax
	addq	2176(%rbx), %rdx
	movl	$274, %ecx
	movq	%rax, %rsi
	leaq	-2224(%rbp), %rdi
	rep movsq
	leaq	-2224(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	leaq	-48(%rbp), %rcx
	movq	%rdi, %rax
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L587:
	movdqu	(%rdx), %xmm0
	paddd	(%rax), %xmm0
	addq	$16, %rax
	addq	$16, %rdx
	movaps	%xmm0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L587
	call	BrotliPopulationCostDistance
	subsd	2184(%rbx), %xmm0
.L585:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L594
	addq	$2216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L594:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE109:
	.size	BrotliHistogramBitCostDistanceDistance, .-BrotliHistogramBitCostDistanceDistance
	.p2align 4
	.globl	BrotliHistogramRemapDistance
	.hidden	BrotliHistogramRemapDistance
	.type	BrotliHistogramRemapDistance, @function
BrotliHistogramRemapDistance:
.LFB110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	testq	%rsi, %rsi
	je	.L596
	movq	$0, -64(%rbp)
	movq	%rdi, %r12
	leaq	(%rdx,%rcx,4), %r13
.L603:
	cmpq	$0, -64(%rbp)
	movq	-72(%rbp), %rax
	je	.L622
	movq	-64(%rbp), %rdi
	movl	-4(%rax,%rdi,4), %r14d
.L598:
	movl	%r14d, %esi
	movq	%r12, %rdi
	imulq	$2192, %rsi, %rsi
	addq	%rbx, %rsi
	call	BrotliHistogramBitCostDistanceDistance
	cmpq	$0, -80(%rbp)
	movsd	%xmm0, -56(%rbp)
	je	.L599
	movq	-96(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L602:
	movl	(%r15), %esi
	movq	%r12, %rdi
	imulq	$2192, %rsi, %rsi
	addq	%rbx, %rsi
	call	BrotliHistogramBitCostDistanceDistance
	movsd	-56(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	jbe	.L600
	movl	(%r15), %r14d
	movsd	%xmm0, -56(%rbp)
.L600:
	addq	$4, %r15
	cmpq	%r13, %r15
	jne	.L602
.L599:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rdi
	addq	$2192, %r12
	movl	%r14d, (%rdi,%rax,4)
	addq	$1, %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, -88(%rbp)
	jne	.L603
	cmpq	$0, -80(%rbp)
	je	.L606
.L610:
	movsd	.LC3(%rip), %xmm0
	xorl	%esi, %esi
	xorl	%eax, %eax
.L605:
	movq	-96(%rbp), %rdi
	movl	(%rdi,%rsi,4), %edx
	addq	$1, %rsi
	imulq	$2192, %rdx, %rdx
	addq	%rbx, %rdx
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, 2168(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movsd	%xmm0, 2184(%rdx)
	movq	$0, 2176(%rdx)
	cmpq	%rsi, -80(%rbp)
	ja	.L605
	cmpq	$0, -88(%rbp)
	je	.L595
.L606:
	movq	-104(%rbp), %rcx
	xorl	%esi, %esi
.L608:
	movq	-72(%rbp), %rax
	movl	(%rax,%rsi,4), %edx
	movq	2176(%rcx), %rax
	imulq	$2192, %rdx, %rdx
	addq	%rbx, %rdx
	addq	%rax, 2176(%rdx)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L607:
	movdqu	(%rcx,%rax), %xmm0
	movdqu	(%rdx,%rax), %xmm1
	paddd	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$2176, %rax
	jne	.L607
	addq	$1, %rsi
	addq	$2192, %rcx
	cmpq	%rsi, -88(%rbp)
	ja	.L608
.L595:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L622:
	.cfi_restore_state
	movl	(%rax), %r14d
	jmp	.L598
.L596:
	cmpq	$0, -80(%rbp)
	jne	.L610
	jmp	.L595
	.cfi_endproc
.LFE110:
	.size	BrotliHistogramRemapDistance, .-BrotliHistogramRemapDistance
	.p2align 4
	.globl	BrotliHistogramReindexDistance
	.hidden	BrotliHistogramReindexDistance
	.type	BrotliHistogramReindexDistance, @function
BrotliHistogramReindexDistance:
.LFB111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	jne	.L643
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	call	BrotliFree
	xorl	%r9d, %r9d
.L631:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -56(%rbp)
	call	BrotliFree
	movq	-56(%rbp), %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	movq	%r9, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	.cfi_restore_state
	leaq	0(,%rcx,4), %r15
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movq	%r15, %rsi
	call	BrotliAllocate
	movq	%r15, %rdx
	movl	$255, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	memset@PLT
	leaq	(%rbx,%r15), %r8
	movq	%rbx, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L626:
	movl	(%rax), %edx
	leaq	(%r14,%rdx,4), %rdx
	cmpl	$-1, (%rdx)
	jne	.L625
	movl	%ecx, (%rdx)
	addl	$1, %ecx
.L625:
	addq	$4, %rax
	cmpq	%rax, %r8
	jne	.L626
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	je	.L627
	imulq	$2192, %rcx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	BrotliAllocate
	movq	-56(%rbp), %r8
	movq	%rax, %r15
.L627:
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L633:
	movl	(%rbx), %esi
	movq	%rdx, %r9
	movl	(%r14,%rsi,4), %ecx
	cmpl	%eax, %ecx
	je	.L644
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	cmpq	%r8, %rbx
	jne	.L633
.L630:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	BrotliFree
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %r9
	testq	%rdx, %rdx
	je	.L631
	imulq	$2192, %rdx, %rdx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	(%r12,%r8), %rdi
	leaq	(%r15,%r8), %rsi
	addq	$2192, %r8
	movl	$274, %ecx
	rep movsq
	cmpq	%r8, %rdx
	jne	.L632
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L644:
	imulq	$2192, %rdx, %rdx
	movl	$274, %ecx
	addl	$1, %eax
	addq	$4, %rbx
	imulq	$2192, %rsi, %rsi
	addq	%r15, %rdx
	movq	%rdx, %rdi
	addq	%r12, %rsi
	rep movsq
	movl	-4(%rbx), %edx
	movl	(%r14,%rdx,4), %ecx
	movl	%eax, %edx
	movq	%rdx, %r9
	movl	%ecx, -4(%rbx)
	cmpq	%r8, %rbx
	jne	.L633
	jmp	.L630
	.cfi_endproc
.LFE111:
	.size	BrotliHistogramReindexDistance, .-BrotliHistogramReindexDistance
	.p2align 4
	.globl	BrotliClusterHistogramsDistance
	.hidden	BrotliClusterHistogramsDistance
	.type	BrotliClusterHistogramsDistance, @function
BrotliClusterHistogramsDistance:
.LFB112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r9, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L712
	movl	$49176, %esi
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	BrotliAllocate
	xorl	%r10d, %r10d
	movq	%rax, %r15
.L658:
	subq	$8, %rsp
	movq	16(%rbp), %rdx
	movq	%r10, %r9
	movq	%r15, %r8
	pushq	%rbx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	pushq	-64(%rbp)
	pushq	-96(%rbp)
	call	BrotliHistogramCombineDistance
	movq	%r15, %rsi
	movq	-104(%rbp), %r15
	addq	$32, %rsp
	movq	%rax, %rbx
	movq	%r15, %rdi
	call	BrotliFree
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	%rbx, %rcx
	movq	-96(%rbp), %rbx
	movq	16(%rbp), %r9
	movq	-112(%rbp), %rdi
	movq	%r14, %r8
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	BrotliHistogramRemapDistance
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	16(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rsi
	call	BrotliHistogramReindexDistance
	movq	-120(%rbp), %rdi
	movq	%rax, (%rdi)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L712:
	.cfi_restore_state
	leaq	0(,%rdx,4), %r12
	movq	%rdi, %r15
	movq	%rdx, %rbx
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	BrotliAllocate
	movq	%r15, %rdi
	movl	$49176, %esi
	movq	%rax, %r12
	call	BrotliAllocate
	movq	%rax, %r15
	leaq	-1(%rbx), %rax
	cmpq	$2, %rax
	jbe	.L660
	shrq	$2, %rbx
	movdqa	.LC4(%rip), %xmm0
	movq	%r13, %rax
	movq	%rbx, %rdx
	salq	$4, %rdx
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L648:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L648
	movq	-96(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-4, %rax
	andl	$3, %edi
	je	.L649
.L647:
	movq	-96(%rbp), %rsi
	leaq	1(%rax), %rdx
	movl	$1, 0(%r13,%rax,4)
	cmpq	%rdx, %rsi
	jbe	.L649
	movl	$1, 0(%r13,%rdx,4)
	addq	$2, %rax
	cmpq	%rax, %rsi
	jbe	.L649
	movl	$1, 0(%r13,%rax,4)
.L649:
	xorl	%ebx, %ebx
	movq	-112(%rbp), %r8
	movq	%r15, -56(%rbp)
	leaq	2184(%r14), %rax
	movq	%r12, -80(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r12
	movq	16(%rbp), %rbx
	movq	%r13, -72(%rbp)
	movq	%r8, %r13
	movq	%r14, -88(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	-2184(%r14), %rdi
	movq	%r13, %rsi
	movl	$274, %ecx
	addq	$2192, %r14
	rep movsq
	movq	%r13, %rdi
	addq	$2192, %r13
	call	BrotliPopulationCostDistance
	movq	%r12, %rcx
	movsd	%xmm0, -2192(%r14)
	movl	%r12d, (%rbx,%r12,4)
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L650
	andq	$-64, %rcx
	movq	-88(%rbp), %r14
	movq	%r12, %rbx
	movq	-56(%rbp), %r15
	leaq	64(%rcx), %rax
	movq	-80(%rbp), %r12
	movq	16(%rbp), %rdx
	xorl	%r11d, %r11d
	movq	%rax, -80(%rbp)
	xorl	%r10d, %r10d
	movq	%r15, %r8
	movq	%r14, %r15
	movq	%rbx, -88(%rbp)
	movq	%r12, %r13
	movq	%r11, %rbx
	movq	%r10, %r12
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L652:
	movq	-88(%rbp), %rdx
	movl	$64, %r9d
	movq	%rdx, %rax
	subq	%rbx, %rax
	cmpq	$64, %rax
	cmovbe	%rax, %r9
	cmpq	%rbx, %rdx
	je	.L656
	movl	%ebx, %esi
	cmpq	$3, %rax
	jbe	.L661
	movdqa	.LC5(%rip), %xmm1
	movd	%ebx, %xmm2
	movq	%r9, %rax
	leaq	0(%r13,%r12,4), %rcx
	pshufd	$0, %xmm2, %xmm0
	shrq	$2, %rax
	paddd	%xmm0, %xmm1
	movups	%xmm1, (%rcx)
	cmpq	$1, %rax
	je	.L655
	movdqa	.LC6(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 16(%rcx)
	cmpq	$2, %rax
	je	.L655
	movdqa	.LC7(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 32(%rcx)
	cmpq	$3, %rax
	je	.L655
	movdqa	.LC8(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 48(%rcx)
	cmpq	$4, %rax
	je	.L655
	movdqa	.LC9(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 64(%rcx)
	cmpq	$5, %rax
	je	.L655
	movdqa	.LC10(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 80(%rcx)
	cmpq	$6, %rax
	je	.L655
	movdqa	.LC11(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 96(%rcx)
	cmpq	$7, %rax
	je	.L655
	movdqa	.LC12(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 112(%rcx)
	cmpq	$8, %rax
	je	.L655
	movdqa	.LC13(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 128(%rcx)
	cmpq	$9, %rax
	je	.L655
	movdqa	.LC14(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 144(%rcx)
	cmpq	$10, %rax
	je	.L655
	movdqa	.LC15(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 160(%rcx)
	cmpq	$11, %rax
	je	.L655
	movdqa	.LC16(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 176(%rcx)
	cmpq	$12, %rax
	je	.L655
	movdqa	.LC17(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 192(%rcx)
	cmpq	$13, %rax
	je	.L655
	movdqa	.LC18(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 208(%rcx)
	cmpq	$14, %rax
	je	.L655
	movdqa	.LC19(%rip), %xmm1
	paddd	%xmm0, %xmm1
	movups	%xmm1, 224(%rcx)
	cmpq	$16, %rax
	jne	.L655
	paddd	.LC20(%rip), %xmm0
	movups	%xmm0, 240(%rcx)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L655:
	movq	%r9, %rax
	andq	$-4, %rax
	testb	$3, %r9b
	je	.L656
.L654:
	leaq	(%r12,%rax), %rcx
	leal	(%rsi,%rax), %edi
	movl	%edi, 0(%r13,%rcx,4)
	leaq	1(%rax), %rcx
	cmpq	%r9, %rcx
	jnb	.L656
	leaq	(%rcx,%r12), %rdi
	addq	$2, %rax
	addl	%esi, %ecx
	movl	%ecx, 0(%r13,%rdi,4)
	cmpq	%rax, %r9
	jbe	.L656
	leaq	(%r12,%rax), %rcx
	addl	%esi, %eax
	movl	%eax, 0(%r13,%rcx,4)
.L656:
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	leaq	0(%r13,%r12,4), %rcx
	movq	%r14, %rdx
	pushq	$2048
	movq	%r15, %rdi
	addq	$64, %rbx
	addq	$256, %r14
	pushq	-64(%rbp)
	pushq	%r9
	movq	%r8, -56(%rbp)
	call	BrotliHistogramCombineDistance
	addq	$32, %rsp
	movq	-56(%rbp), %r8
	addq	%rax, %r12
	cmpq	-80(%rbp), %rbx
	jne	.L652
	movq	%r12, %r10
	movq	%r15, %r14
	movq	%r13, %r12
	movq	%r8, %r15
	movq	%r10, %rbx
	movq	%r10, %rax
	movq	-72(%rbp), %r13
	shrq	%rbx
	salq	$6, %rax
	imulq	%r10, %rbx
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	leaq	1(%rbx), %rdx
	cmpq	$2048, %rdx
	jbe	.L658
	movl	$2048, %eax
	.p2align 4,,10
	.p2align 3
.L659:
	movq	%rax, %rcx
	addq	%rax, %rax
	cmpq	%rax, %rdx
	ja	.L659
	leaq	(%rcx,%rcx,2), %rsi
	movq	-104(%rbp), %rdi
	movq	%r10, -72(%rbp)
	salq	$4, %rsi
	call	BrotliAllocate
	movq	%r15, %rsi
	movl	$49152, %edx
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	call	BrotliFree
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %r10
	movq	%rcx, %r15
	jmp	.L658
.L661:
	xorl	%eax, %eax
	jmp	.L654
.L660:
	xorl	%eax, %eax
	jmp	.L647
	.cfi_endproc
.LFE112:
	.size	BrotliClusterHistogramsDistance, .-BrotliClusterHistogramsDistance
	.section	.rodata
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	2726797102
	.long	1417495214
	.align 8
.LC2:
	.long	0
	.long	1071644672
	.align 8
.LC3:
	.long	0
	.long	2146435072
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
.LC5:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC6:
	.long	4
	.long	5
	.long	6
	.long	7
	.align 16
.LC7:
	.long	8
	.long	9
	.long	10
	.long	11
	.align 16
.LC8:
	.long	12
	.long	13
	.long	14
	.long	15
	.align 16
.LC9:
	.long	16
	.long	17
	.long	18
	.long	19
	.align 16
.LC10:
	.long	20
	.long	21
	.long	22
	.long	23
	.align 16
.LC11:
	.long	24
	.long	25
	.long	26
	.long	27
	.align 16
.LC12:
	.long	28
	.long	29
	.long	30
	.long	31
	.align 16
.LC13:
	.long	32
	.long	33
	.long	34
	.long	35
	.align 16
.LC14:
	.long	36
	.long	37
	.long	38
	.long	39
	.align 16
.LC15:
	.long	40
	.long	41
	.long	42
	.long	43
	.align 16
.LC16:
	.long	44
	.long	45
	.long	46
	.long	47
	.align 16
.LC17:
	.long	48
	.long	49
	.long	50
	.long	51
	.align 16
.LC18:
	.long	52
	.long	53
	.long	54
	.long	55
	.align 16
.LC19:
	.long	56
	.long	57
	.long	58
	.long	59
	.align 16
.LC20:
	.long	60
	.long	61
	.long	62
	.long	63
	.hidden	BrotliPopulationCostDistance
	.hidden	BrotliPopulationCostCommand
	.hidden	BrotliAllocate
	.hidden	BrotliFree
	.hidden	BrotliPopulationCostLiteral
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
