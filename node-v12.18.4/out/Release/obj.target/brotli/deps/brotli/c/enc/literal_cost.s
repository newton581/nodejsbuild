	.file	"literal_cost.c"
	.text
	.p2align 4
	.globl	BrotliEstimateBitCostsForLiterals
	.hidden	BrotliEstimateBitCostsForLiterals
	.type	BrotliEstimateBitCostsForLiterals, @function
BrotliEstimateBitCostsForLiterals:
.LFB52:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movsd	.LC0(%rip), %xmm0
	movq	%r8, -6256(%rbp)
	movq	%rcx, %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rdx, %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	BrotliIsMostlyUTF8
	testl	%eax, %eax
	jne	.L130
	xorl	%eax, %eax
	leaq	-6208(%rbp), %rdi
	movl	$256, %ecx
	cmpq	$2000, %r12
	rep stosq
	movl	$2000, %eax
	cmovbe	%r12, %rax
	testq	%r12, %r12
	je	.L1
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	(%rbx,%rcx), %rdx
	addq	$1, %rcx
	andq	%r13, %rdx
	movzbl	(%r15,%rdx), %edx
	addq	$1, -6208(%rbp,%rdx,8)
	cmpq	%rcx, %rax
	ja	.L59
	movsd	.LC2(%rip), %xmm7
	movq	%r12, -6248(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r15, %rax
	xorl	%r14d, %r14d
	movq	%r13, %r15
	movsd	%xmm7, -6264(%rbp)
	movq	%rax, %r13
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	kLog2Table(%rip), %rsi
	pxor	%xmm1, %xmm1
	cvtss2sd	(%rsi,%rbx,4), %xmm1
	cmpq	$255, %rax
	ja	.L67
.L132:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L68:
	subsd	%xmm0, %xmm1
	movsd	.LC7(%rip), %xmm0
	movsd	-6264(%rbp), %xmm2
	addsd	%xmm1, %xmm0
	comisd	%xmm0, %xmm2
	jbe	.L71
	mulsd	.LC3(%rip), %xmm0
	addsd	.LC3(%rip), %xmm0
.L71:
	movq	-6256(%rbp), %rax
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rax,%r14,4)
	addq	$1, %r14
	cmpq	%r14, -6248(%rbp)
	je	.L1
	cmpq	$1999, %r14
	jbe	.L61
	leaq	-1999(%r12), %rax
	subq	$1, %rbx
	andq	%r15, %rax
	movzbl	0(%r13,%rax), %eax
	subq	$1, -6208(%rbp,%rax,8)
.L61:
	addq	$1, %r12
.L60:
	leaq	2000(%r14), %rax
	cmpq	%rax, -6248(%rbp)
	jbe	.L62
	leaq	2000(%r12), %rax
	addq	$1, %rbx
	andq	%r15, %rax
	movzbl	0(%r13,%rax), %eax
	addq	$1, -6208(%rbp,%rax,8)
.L62:
	movq	%r15, %rax
	movl	$1, %edx
	andq	%r12, %rax
	movzbl	0(%r13,%rax), %eax
	movq	-6208(%rbp,%rax,8), %rax
	testq	%rax, %rax
	cmovne	%rax, %rdx
	cmpq	$255, %rbx
	jbe	.L131
	testq	%rbx, %rbx
	js	.L65
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L66:
	movq	%rax, -6280(%rbp)
	movq	%rdx, -6272(%rbp)
	call	log2@PLT
	movq	-6280(%rbp), %rax
	movq	-6272(%rbp), %rdx
	movapd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L132
.L67:
	testq	%rdx, %rdx
	js	.L69
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L70:
	movsd	%xmm1, -6272(%rbp)
	call	log2@PLT
	movsd	-6272(%rbp), %xmm1
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rbx, %rdi
	movq	%rbx, %r8
	pxor	%xmm0, %xmm0
	shrq	%rdi
	andl	$1, %r8d
	orq	%r8, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L70
.L30:
	movq	-6304(%rbp), %rbx
	movq	%r13, %r12
	movq	%r14, %r13
	movq	-6296(%rbp), %r14
	addq	%r14, %rbx
	cmpq	%r14, %r12
	jbe	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%eax, %eax
	cmpq	$495, %r14
	je	.L35
	leaq	-496(%rbx), %rdx
	andq	%r13, %rdx
	movzbl	(%r15,%rdx), %edx
	cmpq	$496, %r14
	jbe	.L133
	leaq	-497(%rbx), %rcx
	andq	%r13, %rcx
	movzbl	(%r15,%rcx), %ecx
	cmpq	$127, %rdx
	jbe	.L35
	movq	-6248(%rbp), %rdi
	movq	%rdi, %rax
	cmpq	$191, %rdx
	ja	.L35
	cmpb	$-33, %cl
	movl	$0, %eax
	cmova	%rdi, %rax
.L35:
	subq	$1, -6240(%rbp,%rax,8)
	leaq	-495(%rbx), %rdx
	andq	%r13, %rdx
	movzbl	(%r15,%rdx), %ecx
	movq	%rax, %rdx
	leaq	495(%r14), %rax
	salq	$8, %rdx
	addq	%rcx, %rdx
	subq	$1, -6208(%rbp,%rdx,8)
	cmpq	%rax, %r12
	jbe	.L42
	leaq	494(%rbx), %rax
	leaq	493(%rbx), %rdx
	andq	%r13, %rax
	andq	%r13, %rdx
	leaq	495(%rbx), %rcx
	cmpb	$0, (%r15,%rax)
	movzbl	(%r15,%rax), %edi
	movzbl	(%r15,%rdx), %edx
	jns	.L128
	cmpq	$191, %rdi
	ja	.L94
	cmpb	$-33, %dl
	ja	.L94
.L128:
	xorl	%edx, %edx
.L43:
	addq	$1, -6240(%rbp,%rdx,8)
	movq	%rcx, %rax
	andq	%r13, %rax
	movzbl	(%r15,%rax), %ecx
	movq	%rdx, %rax
	salq	$8, %rax
	addq	%rcx, %rax
	addq	$1, -6208(%rbp,%rax,8)
	testq	%r14, %r14
	je	.L39
.L42:
	leaq	-1(%rbx), %rax
	andq	%r13, %rax
	movzbl	(%r15,%rax), %eax
	cmpq	$1, %r14
	jbe	.L38
	cmpq	$127, %rax
	jbe	.L39
	movq	-6248(%rbp), %rsi
	movq	%rsi, %rdx
	cmpq	$191, %rax
	ja	.L40
	leaq	-2(%rbx), %rax
	andq	%r13, %rax
	cmpb	$-33, (%r15,%rax)
	jbe	.L39
.L40:
	movq	%r13, %rax
	andq	%rbx, %rax
	movzbl	(%r15,%rax), %ecx
	movq	%rdx, %rax
	movq	-6240(%rbp,%rdx,8), %rdx
	salq	$8, %rax
	addq	%rcx, %rax
	movq	-6208(%rbp,%rax,8), %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	cmovne	%rcx, %rax
	cmpq	$255, %rdx
	ja	.L44
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm1, %xmm1
	cvtss2sd	(%rdi,%rdx,4), %xmm1
.L45:
	cmpq	$255, %rcx
	ja	.L48
	leaq	kLog2Table(%rip), %rsi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rsi,%rax,4), %xmm0
.L49:
	subsd	%xmm0, %xmm1
	movsd	.LC1(%rip), %xmm0
	movsd	-6264(%rbp), %xmm4
	addsd	%xmm1, %xmm0
	comisd	%xmm0, %xmm4
	jbe	.L52
	mulsd	.LC3(%rip), %xmm0
	addsd	.LC3(%rip), %xmm0
.L52:
	cmpq	$1999, %r14
	jbe	.L134
	movq	-6256(%rbp), %rax
	cvtsd2ss	%xmm0, %xmm0
	addq	$1, %rbx
	movss	%xmm0, (%rax,%r14,4)
	addq	$1, %r14
	cmpq	%r14, %r12
	jne	.L57
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$6264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	$0, -6192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6208(%rbp)
	testq	%r12, %r12
	je	.L1
	leaq	(%rbx,%r12), %r10
	movq	%rbx, %rcx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r13, %rdx
	xorl	%eax, %eax
	movq	%rdi, %rsi
	andq	%rcx, %rdx
	cmpb	$0, (%r15,%rdx)
	movzbl	(%r15,%rdx), %edi
	jns	.L4
	movl	$1, %eax
	cmpq	$191, %rdi
	ja	.L4
	xorl	%eax, %eax
	cmpq	$223, %rsi
	seta	%al
	addq	%rax, %rax
.L4:
	addq	$1, -6208(%rbp,%rax,8)
	addq	$1, %rcx
	cmpq	%r10, %rcx
	jne	.L5
	movq	-6192(%rbp), %rax
	movl	$768, %ecx
	pxor	%xmm0, %xmm0
	addq	-6200(%rbp), %rax
	cmpq	$24, %rax
	leaq	-6208(%rbp), %rdi
	movl	$495, %r10d
	movq	$0, -6224(%rbp)
	seta	%al
	movaps	%xmm0, -6240(%rbp)
	movzbl	%al, %eax
	movq	%rax, -6248(%rbp)
	xorl	%eax, %eax
	cmpq	$495, %r12
	rep stosq
	cmovbe	%r12, %r10
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-6240(%rbp,%rcx,8), %rdi
	movq	%rdx, %r8
.L74:
	leaq	(%rbx,%rsi), %rax
	andq	%r13, %rax
	movzbl	(%r15,%rax), %edx
	movq	%rcx, %rax
	salq	$8, %rax
	addq	%rdx, %rax
	addq	$1, -6208(%rbp,%rax,8)
	leaq	1(%rdi), %rax
	movq	%rax, -6240(%rbp,%rcx,8)
	xorl	%ecx, %ecx
	cmpq	$127, %rdx
	jbe	.L7
	cmpq	$191, %rdx
	ja	.L95
	cmpq	$223, %r8
	ja	.L95
.L7:
	addq	$1, %rsi
	cmpq	%r10, %rsi
	jb	.L9
	leaq	-1(%r12), %rsi
	movl	$494, %eax
	movq	%rbx, %rcx
	movsd	.LC2(%rip), %xmm7
	cmpq	$494, %rsi
	movq	%rbx, -6304(%rbp)
	cmovbe	%rsi, %rax
	movsd	%xmm7, -6264(%rbp)
	leaq	1(%rax), %r14
	movq	%rax, -6288(%rbp)
	xorl	%eax, %eax
	movq	%r14, -6296(%rbp)
	movq	%rax, %rbx
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%rcx, %r12
.L32:
	leaq	495(%rbx), %rax
	cmpq	%rax, %r13
	jbe	.L13
.L12:
	leaq	494(%r12), %rax
	leaq	493(%r12), %rcx
	andq	%r14, %rcx
	andq	%r14, %rax
	leaq	495(%r12), %rsi
	movzbl	(%r15,%rcx), %r9d
	xorl	%ecx, %ecx
	cmpb	$0, (%r15,%rax)
	movzbl	(%r15,%rax), %r8d
	jns	.L16
	cmpq	$191, %r8
	ja	.L82
	cmpb	$-33, %r9b
	ja	.L82
.L16:
	addq	$1, -6240(%rbp,%rcx,8)
	movq	%rsi, %rax
	andq	%r14, %rax
	movzbl	(%r15,%rax), %esi
	movq	%rcx, %rax
	salq	$8, %rax
	addq	%rsi, %rax
	addq	$1, -6208(%rbp,%rax,8)
.L13:
	testq	%rbx, %rbx
	je	.L84
.L17:
	leaq	-1(%r12), %rax
	andq	%r14, %rax
	movzbl	(%r15,%rax), %eax
	cmpq	$1, %rbx
	je	.L136
	cmpq	$127, %rax
	jbe	.L84
	movq	-6248(%rbp), %rsi
	movq	%rsi, %rcx
	cmpq	$191, %rax
	ja	.L18
	leaq	-2(%r12), %rax
	movl	$0, %ecx
	andq	%r14, %rax
	cmpb	$-33, (%r15,%rax)
	cmova	%rsi, %rcx
.L18:
	movq	%r14, %rax
	andq	%r12, %rax
	movzbl	(%r15,%rax), %esi
	movq	%rcx, %rax
	movq	-6240(%rbp,%rcx,8), %rcx
	salq	$8, %rax
	addq	%rsi, %rax
	movq	-6208(%rbp,%rax,8), %rsi
	movl	$1, %eax
	testq	%rsi, %rsi
	cmovne	%rsi, %rax
	cmpq	$255, %rcx
	ja	.L19
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm1, %xmm1
	cvtss2sd	(%rdi,%rcx,4), %xmm1
.L20:
	cmpq	$255, %rsi
	ja	.L23
	leaq	kLog2Table(%rip), %rsi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rsi,%rax,4), %xmm0
.L24:
	subsd	%xmm0, %xmm1
	movsd	.LC1(%rip), %xmm0
	movsd	-6264(%rbp), %xmm3
	addsd	%xmm1, %xmm0
	comisd	%xmm0, %xmm3
	jbe	.L27
	mulsd	.LC3(%rip), %xmm0
	addsd	.LC3(%rip), %xmm0
.L27:
	cmpq	$1999, %rbx
	jbe	.L137
	movq	-6256(%rbp), %rax
	cvtsd2ss	%xmm0, %xmm0
	addq	$1, %r12
	movss	%xmm0, (%rax,%rbx,4)
	addq	$1, %rbx
	leaq	495(%rbx), %rax
	cmpq	%rax, %r13
	ja	.L12
	testq	%rbx, %rbx
	jne	.L17
.L84:
	xorl	%ecx, %ecx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L23:
	testq	%rax, %rax
	js	.L25
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L26:
	movsd	%xmm1, -6272(%rbp)
	call	log2@PLT
	movsd	-6272(%rbp), %xmm1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L19:
	testq	%rcx, %rcx
	js	.L21
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
.L22:
	movq	%rax, -6280(%rbp)
	movq	%rsi, -6272(%rbp)
	call	log2@PLT
	movq	-6280(%rbp), %rax
	movq	-6272(%rbp), %rsi
	movapd	%xmm0, %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$2000, %eax
	pxor	%xmm1, %xmm1
	movsd	.LC6(%rip), %xmm5
	addq	$1, %r12
	subq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm1
	divsd	.LC4(%rip), %xmm1
	mulsd	.LC5(%rip), %xmm1
	movq	-6256(%rbp), %rax
	subsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rax,%rbx,4)
	leaq	1(%rbx), %rax
	cmpq	%rbx, -6288(%rbp)
	je	.L30
	movq	%rax, %rbx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L95:
	movq	-6248(%rbp), %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rcx, %r8
	andl	$1, %ecx
	pxor	%xmm0, %xmm0
	shrq	%r8
	orq	%rcx, %r8
	cvtsi2sdq	%r8, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L136:
	cmpq	$191, %rax
	movl	$0, %ecx
	cmova	-6248(%rbp), %rcx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L82:
	movq	-6248(%rbp), %rcx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L94:
	movq	-6248(%rbp), %rdx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L48:
	testq	%rax, %rax
	js	.L50
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L51:
	movsd	%xmm1, -6272(%rbp)
	call	log2@PLT
	movsd	-6272(%rbp), %xmm1
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L44:
	testq	%rdx, %rdx
	js	.L46
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L47:
	movq	%rcx, -6280(%rbp)
	movq	%rax, -6272(%rbp)
	call	log2@PLT
	movq	-6280(%rbp), %rcx
	movq	-6272(%rbp), %rax
	movapd	%xmm0, %xmm1
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-6248(%rbp), %rdx
	cmpq	$191, %rax
	ja	.L40
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%edx, %edx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$2000, %eax
	pxor	%xmm1, %xmm1
	movsd	.LC6(%rip), %xmm6
	addq	$1, %rbx
	subq	%r14, %rax
	cvtsi2sdq	%rax, %xmm1
	divsd	.LC4(%rip), %xmm1
	mulsd	.LC5(%rip), %xmm1
	movq	-6256(%rbp), %rax
	subsd	%xmm1, %xmm6
	addsd	%xmm6, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rax,%r14,4)
	addq	$1, %r14
	cmpq	%r12, %r14
	jne	.L57
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L133:
	cmpq	$191, %rdx
	cmova	-6248(%rbp), %rax
	jmp	.L35
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE52:
	.size	BrotliEstimateBitCostsForLiterals, .-BrotliEstimateBitCostsForLiterals
	.section	.rodata
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072168960
	.align 8
.LC1:
	.long	2144047674
	.long	1067302728
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC3:
	.long	0
	.long	1071644672
	.align 8
.LC4:
	.long	0
	.long	1084178432
	.align 8
.LC5:
	.long	1717986918
	.long	1071015526
	.align 8
.LC6:
	.long	1717986918
	.long	1072064102
	.align 8
.LC7:
	.long	240518169
	.long	1067299373
	.hidden	BrotliIsMostlyUTF8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
