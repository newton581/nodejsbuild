	.file	"compress_fragment_two_pass.c"
	.text
	.p2align 4
	.type	ShouldCompress, @function
ShouldCompress:
.LFB111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	movq	%rdi, %r8
	cvtsi2sdq	%rsi, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1080, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	js	.L3
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L4:
	movsd	.LC1(%rip), %xmm2
	movapd	%xmm1, %xmm3
	movl	$1, %eax
	mulsd	%xmm2, %xmm3
	comisd	%xmm0, %xmm3
	ja	.L1
	leaq	-1088(%rbp), %rbx
	mulsd	.LC2(%rip), %xmm1
	movl	$128, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	rep stosq
	mulsd	%xmm2, %xmm1
	divsd	.LC3(%rip), %xmm1
	movsd	%xmm1, -1096(%rbp)
	.p2align 4,,10
	.p2align 3
.L5:
	movzbl	(%r8,%rax), %edx
	addq	$43, %rax
	addl	$1, -1088(%rbp,%rdx,4)
	cmpq	%rax, %rsi
	ja	.L5
	pxor	%xmm1, %xmm1
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %r14
	leaq	kLog2Table(%rip), %r15
.L15:
	movl	(%rbx), %r13d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r13, %xmm2
	cmpq	$255, %r13
	ja	.L8
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r15,%r13,4), %xmm0
.L9:
	mulsd	%xmm0, %xmm2
	movl	4(%rbx), %eax
	addq	$8, %rbx
	addq	%rax, %r13
	addq	%r13, %r12
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	ja	.L12
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r15,%rax,4), %xmm0
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cmpq	%r14, %rbx
	jne	.L15
	testq	%r12, %r12
	js	.L16
.L33:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r12, %xmm2
.L17:
	testq	%r12, %r12
	je	.L18
	cmpq	$255, %r12
	ja	.L19
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L20:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
.L18:
	maxsd	%xmm1, %xmm2
	movsd	-1096(%rbp), %xmm4
	xorl	%eax, %eax
	comisd	%xmm2, %xmm4
	seta	%al
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L32
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -1112(%rbp)
	movsd	%xmm2, -1104(%rbp)
	call	log2@PLT
	movsd	-1112(%rbp), %xmm1
	movsd	-1104(%rbp), %xmm2
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -1112(%rbp)
	movsd	%xmm2, -1104(%rbp)
	call	log2@PLT
	movsd	-1104(%rbp), %xmm2
	movsd	-1112(%rbp), %xmm1
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cmpq	%r14, %rbx
	jne	.L15
	testq	%r12, %r12
	jns	.L33
.L16:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm2, %xmm2
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -1112(%rbp)
	movsd	%xmm2, -1104(%rbp)
	call	log2@PLT
	movsd	-1112(%rbp), %xmm1
	movsd	-1104(%rbp), %xmm2
	jmp	.L20
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE111:
	.size	ShouldCompress, .-ShouldCompress
	.p2align 4
	.type	StoreCommands, @function
StoreCommands:
.LFB110:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$520, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	pxor	%xmm0, %xmm0
	movq	16(%rbp), %r13
	movq	%rcx, -4640(%rbp)
	movq	%rsi, %rbx
	movq	%rdi, %r11
	movq	%r9, %r12
	movq	%r8, -4632(%rbp)
	leaq	-3216(%rbp), %rsi
	movl	$128, %ecx
	leaq	-3728(%rbp), %r10
	movq	%rsi, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1152(%rbp)
	movaps	%xmm0, -1136(%rbp)
	movaps	%xmm0, -1120(%rbp)
	movaps	%xmm0, -1104(%rbp)
	movaps	%xmm0, -1088(%rbp)
	movaps	%xmm0, -1072(%rbp)
	rep stosq
	leaq	-4496(%rbp), %rdi
	movl	$32, %ecx
	movaps	%xmm0, -1056(%rbp)
	rep stosq
	movl	$64, %ecx
	movq	%r10, %rdi
	movaps	%xmm0, -1040(%rbp)
	rep stosq
	leaq	(%rdx,%rbx), %rdi
	movq	%rbx, %rax
	testq	%rdx, %rdx
	je	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	movzbl	(%rax), %ecx
	addq	$1, %rax
	addl	$1, -3216(%rbp,%rcx,4)
	cmpq	%rdi, %rax
	jne	.L38
.L39:
	pushq	%r13
	movl	$8, %ecx
	movq	%r11, %rdi
	leaq	-4240(%rbp), %r9
	pushq	%r12
	leaq	-1024(%rbp), %r8
	movq	%r10, -4648(%rbp)
	call	BrotliBuildAndStoreHuffmanTreeFast
	popq	%rax
	popq	%rdx
	movq	-4632(%rbp), %rdx
	movq	-4640(%rbp), %rdi
	movq	-4648(%rbp), %r10
	testq	%rdx, %rdx
	movq	%rdi, %rax
	leaq	(%rdi,%rdx,4), %rcx
	je	.L37
	.p2align 4,,10
	.p2align 3
.L42:
	movzbl	(%rax), %edx
	addq	$4, %rax
	addl	$1, -3728(%rbp,%rdx,4)
	cmpq	%rcx, %rax
	jne	.L42
.L37:
	xorl	%eax, %eax
	movl	$86, %ecx
	pxor	%xmm0, %xmm0
	movl	$15, %edx
	leaq	-2192(%rbp), %r15
	leaq	-752(%rbp), %rdi
	movl	$64, %esi
	movaps	%xmm0, -768(%rbp)
	rep stosq
	movq	%r10, %rdi
	movq	%r15, %rcx
	leaq	-1088(%rbp), %r14
	leaq	-1152(%rbp), %r8
	addl	$1, -3724(%rbp)
	addl	$1, -3720(%rbp)
	addl	$1, -3472(%rbp)
	addl	$1, -3392(%rbp)
	call	BrotliCreateHuffmanTree
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$14, %edx
	leaq	-3472(%rbp), %rdi
	movl	$64, %esi
	call	BrotliCreateHuffmanTree
	movq	-1112(%rbp), %rax
	movdqa	-1152(%rbp), %xmm1
	leaq	-768(%rbp), %r11
	movdqu	-1128(%rbp), %xmm3
	movq	%r11, %rdi
	leaq	-4624(%rbp), %rdx
	movl	$64, %esi
	movq	%rax, -752(%rbp)
	movdqa	%xmm1, %xmm2
	movq	-1136(%rbp), %rax
	punpcklqdq	-1104(%rbp), %xmm2
	punpckhqdq	-1104(%rbp), %xmm1
	movq	%r11, -4648(%rbp)
	movq	%rax, -712(%rbp)
	movups	%xmm2, -744(%rbp)
	movups	%xmm1, -728(%rbp)
	movaps	%xmm3, -768(%rbp)
	call	BrotliConvertBitDepthsToSymbols
	movdqa	-4576(%rbp), %xmm4
	movq	%r14, %rdi
	movdqa	-4544(%rbp), %xmm5
	movdqa	-4512(%rbp), %xmm6
	movdqa	-4624(%rbp), %xmm7
	leaq	-4368(%rbp), %rdx
	movl	$64, %esi
	movdqa	-4608(%rbp), %xmm3
	movaps	%xmm4, -4496(%rbp)
	movdqa	-4592(%rbp), %xmm4
	movaps	%xmm5, -4480(%rbp)
	movdqa	-4560(%rbp), %xmm5
	movaps	%xmm6, -4464(%rbp)
	movdqa	-4528(%rbp), %xmm6
	movaps	%xmm7, -4448(%rbp)
	movaps	%xmm3, -4432(%rbp)
	movaps	%xmm4, -4416(%rbp)
	movaps	%xmm5, -4400(%rbp)
	movaps	%xmm6, -4384(%rbp)
	call	BrotliConvertBitDepthsToSymbols
	pxor	%xmm0, %xmm0
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	-1128(%rbp), %rax
	movq	%r15, %rdx
	movl	$704, %esi
	movups	%xmm0, -760(%rbp)
	movups	%xmm0, -744(%rbp)
	movq	%rax, -768(%rbp)
	movq	-1120(%rbp), %rax
	movups	%xmm0, -728(%rbp)
	movq	%rax, -704(%rbp)
	movq	-1112(%rbp), %rax
	movq	$0, -712(%rbp)
	movq	%rax, -640(%rbp)
	movq	-1104(%rbp), %rax
	movq	%rax, -576(%rbp)
	movq	-1096(%rbp), %rax
	movq	%rax, -384(%rbp)
	movzbl	-1152(%rbp), %eax
	movb	%al, -640(%rbp)
	movzbl	-1144(%rbp), %eax
	movb	%al, -512(%rbp)
	movzbl	-1136(%rbp), %eax
	movb	%al, -320(%rbp)
	movzbl	-1151(%rbp), %eax
	movb	%al, -632(%rbp)
	movzbl	-1143(%rbp), %eax
	movb	%al, -504(%rbp)
	movzbl	-1135(%rbp), %eax
	movb	%al, -312(%rbp)
	movzbl	-1150(%rbp), %eax
	movb	%al, -624(%rbp)
	movzbl	-1142(%rbp), %eax
	movb	%al, -496(%rbp)
	movzbl	-1134(%rbp), %eax
	movb	%al, -304(%rbp)
	movzbl	-1149(%rbp), %eax
	movq	-4648(%rbp), %r11
	movb	%al, -616(%rbp)
	movzbl	-1141(%rbp), %eax
	movq	%r11, %rdi
	movb	%al, -488(%rbp)
	movzbl	-1133(%rbp), %eax
	movb	%al, -296(%rbp)
	movzbl	-1148(%rbp), %eax
	movb	%al, -608(%rbp)
	movzbl	-1140(%rbp), %eax
	movb	%al, -480(%rbp)
	movzbl	-1132(%rbp), %eax
	movb	%al, -288(%rbp)
	movzbl	-1147(%rbp), %eax
	movb	%al, -600(%rbp)
	movzbl	-1139(%rbp), %eax
	movb	%al, -472(%rbp)
	movzbl	-1131(%rbp), %eax
	movb	%al, -280(%rbp)
	movzbl	-1146(%rbp), %eax
	movb	%al, -592(%rbp)
	movzbl	-1138(%rbp), %eax
	movb	%al, -464(%rbp)
	movzbl	-1130(%rbp), %eax
	movb	%al, -272(%rbp)
	movzbl	-1145(%rbp), %eax
	movb	%al, -584(%rbp)
	movzbl	-1137(%rbp), %eax
	movb	%al, -456(%rbp)
	movzbl	-1129(%rbp), %eax
	movb	%al, -264(%rbp)
	call	BrotliStoreHuffmanTree
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r15, %rdx
	movl	$64, %esi
	movq	%r14, %rdi
	call	BrotliStoreHuffmanTree
	cmpq	$0, -4632(%rbp)
	je	.L34
	movq	-4632(%rbp), %rdi
	movq	-4640(%rbp), %r9
	leaq	kNumExtraBits.4849(%rip), %r11
	movq	(%r12), %rax
	leaq	(%r9,%rdi,4), %rdi
	movq	%rdi, -4632(%rbp)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$4, %r9
	cmpq	-4632(%rbp), %r9
	je	.L34
.L45:
	movl	(%r9), %edx
	movq	%rax, %rdi
	movl	%eax, %ecx
	shrq	$3, %rdi
	andl	$7, %ecx
	movzbl	%dl, %r8d
	addq	%r13, %rdi
	shrl	$8, %edx
	movzwl	-4496(%rbp,%r8,2), %esi
	movzbl	(%rdi), %r15d
	movzbl	-1152(%rbp,%r8), %r14d
	salq	%cl, %rsi
	addq	%r14, %rax
	orq	%rsi, %r15
	movl	(%r11,%r8,4), %r14d
	movl	%edx, %esi
	movq	%r15, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	%r14, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r15d
	orq	%rsi, %r15
	movq	%r15, (%rdi)
	cmpl	$23, %r8d
	ja	.L43
	leaq	kInsertOffset.4850(%rip), %rdi
	addl	(%rdi,%r8,4), %edx
	je	.L43
	subl	$1, %edx
	leaq	1(%rbx,%rdx), %r8
	.p2align 4,,10
	.p2align 3
.L44:
	movzbl	(%rbx), %ecx
	movq	%rax, %rsi
	addq	$1, %rbx
	shrq	$3, %rsi
	movzbl	-1024(%rbp,%rcx), %edi
	movzwl	-4240(%rbp,%rcx,2), %edx
	addq	%r13, %rsi
	movl	%eax, %ecx
	movzbl	(%rsi), %r10d
	andl	$7, %ecx
	salq	%cl, %rdx
	addq	%rdi, %rax
	orq	%r10, %rdx
	movq	%rax, (%r12)
	movq	%rdx, (%rsi)
	cmpq	%r8, %rbx
	jne	.L44
	addq	$4, %r9
	cmpq	-4632(%rbp), %r9
	jne	.L45
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L60:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE110:
	.size	StoreCommands, .-StoreCommands
	.p2align 4
	.type	BrotliStoreMetaBlockHeader.constprop.0, @function
BrotliStoreMetaBlockHeader.constprop.0:
.LFB136:
	.cfi_startproc
	movq	(%rsi), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	addq	%rdx, %rcx
	movzbl	(%rcx), %r8d
	movq	%r8, (%rcx)
	leaq	1(%rax), %r8
	movq	%r8, (%rsi)
	cmpq	$65536, %rdi
	jbe	.L64
	movl	%r8d, %ecx
	andl	$7, %ecx
	cmpq	$1048576, %rdi
	ja	.L63
	movl	$1, %r10d
	movl	$20, %r9d
	salq	%cl, %r10
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$16, %r9d
	xorl	%r10d, %r10d
.L62:
	shrq	$3, %r8
	addq	$3, %rax
	subq	$1, %rdi
	addq	%rdx, %r8
	movq	%rax, (%rsi)
	movzbl	(%r8), %ecx
	orq	%r10, %rcx
	movq	%rcx, (%r8)
	movq	%rax, %r8
	movl	%eax, %ecx
	addq	%r9, %rax
	shrq	$3, %r8
	andl	$7, %ecx
	movq	%rax, (%rsi)
	addq	%rdx, %r8
	salq	%cl, %rdi
	movq	%rax, %rcx
	addq	$1, %rax
	movzbl	(%r8), %r10d
	shrq	$3, %rcx
	addq	%rcx, %rdx
	orq	%r10, %rdi
	movq	%rdi, (%r8)
	movzbl	(%rdx), %ecx
	movq	%rax, (%rsi)
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$2, %r10d
	movl	$24, %r9d
	salq	%cl, %r10
	jmp	.L62
	.cfi_endproc
.LFE136:
	.size	BrotliStoreMetaBlockHeader.constprop.0, .-BrotliStoreMetaBlockHeader.constprop.0
	.p2align 4
	.type	EmitUncompressedMetaBlock, @function
EmitUncompressedMetaBlock:
.LFB113:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rdx), %rax
	movq	%rax, %rdx
	shrq	$3, %rdx
	addq	%rcx, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	cmpq	$65536, %rsi
	jbe	.L68
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	$1048576, %rsi
	ja	.L67
	movl	$1, %edi
	movl	$20, %esi
	salq	%cl, %rdi
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$16, %esi
	xorl	%edi, %edi
.L66:
	shrq	$3, %rdx
	addq	$3, %rax
	addq	%r12, %rdx
	movq	%rax, (%rbx)
	movzbl	(%rdx), %ecx
	orq	%rdi, %rcx
	movq	%rax, %rdi
	shrq	$3, %rdi
	movq	%rcx, (%rdx)
	movl	%eax, %ecx
	leaq	-1(%r13), %rdx
	addq	%r12, %rdi
	andl	$7, %ecx
	addq	%rsi, %rax
	movzbl	(%rdi), %r9d
	salq	%cl, %rdx
	movq	%rax, (%rbx)
	movl	%eax, %esi
	andl	$7, %esi
	orq	%r9, %rdx
	movq	%rdx, (%rdi)
	movq	%rax, %rdx
	addq	$8, %rax
	shrq	$3, %rdx
	andl	$4294967288, %eax
	addq	%r12, %rdx
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	%r8, %rsi
	movq	%rcx, (%rdx)
	movq	%r13, %rdx
	movq	%rax, (%rbx)
	shrq	$3, %rax
	leaq	(%r12,%rax), %rdi
	call	memcpy@PLT
	movq	(%rbx), %rax
	leaq	(%rax,%r13,8), %rax
	movq	%rax, (%rbx)
	shrq	$3, %rax
	movb	$0, (%r12,%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	$2, %edi
	movl	$24, %esi
	salq	%cl, %rdi
	jmp	.L66
	.cfi_endproc
.LFE113:
	.size	EmitUncompressedMetaBlock, .-EmitUncompressedMetaBlock
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl8.isra.0, @function
BrotliCompressFragmentTwoPassImpl8.isra.0:
.LFB126:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L70
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L123
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$56, %rdi
	cmpq	%r9, %r12
	jb	.L72
.L188:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L74:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L75
.L76:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L183
	movq	%rcx, %r9
	movl	%edx, %ecx
.L73:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$56, %rdi
	cmpl	%esi, (%rax)
	jne	.L74
	cmpq	%rax, %rdx
	jnb	.L74
	movl	%r11d, (%r8)
.L75:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L76
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L77
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L79:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L187
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L80:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L83
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L84:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L89
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L89:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L90
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L91:
	cmpq	%r11, %r12
	jbe	.L130
.L189:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$56, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$56, %rcx
	shrq	$56, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L96
.L95:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L108
.L96:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$56, %rdi
	cmpq	%r9, %r12
	jnb	.L188
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L110
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L111
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L112:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L110:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L117
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L118:
	subq	%r15, -88(%rbp)
	jne	.L71
.L70:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L117:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	$129, %eax
	ja	.L113
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L113:
	cmpl	$2113, %eax
	ja	.L114
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L123:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L72
.L83:
	cmpl	$129, %edx
	ja	.L85
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L84
.L90:
	cmpq	$71, %r9
	ja	.L92
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L189
.L130:
	movq	%r11, %rsi
	jmp	.L72
.L108:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L97
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L99:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L190
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L101:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L120
	cmpq	$133, %rax
	jbe	.L191
	cmpq	$2117, %rax
	ja	.L107
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L105
.L114:
	cmpl	$6209, %eax
	ja	.L115
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L112
.L92:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L192
	cmpq	$2119, %r9
	ja	.L94
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L91
.L85:
	cmpl	$2113, %edx
	jbe	.L193
	cmpl	$6209, %edx
	ja	.L87
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L84
.L194:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L120:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L105:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L130
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$56, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$56, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$56, %rax
	shrq	$56, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L95
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L115:
	cmpl	$22593, %eax
	ja	.L116
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L112
.L193:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L84
.L192:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L91
.L191:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L105
.L116:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L112
.L187:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L79
	movq	%r9, %r11
.L77:
	andl	$7, %edi
	je	.L124
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L124
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L80
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L80
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L80
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L80
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L80
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L80
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L80
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L80
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L80
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L80
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L80
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L80
	leaq	7(%r11), %r10
	jmp	.L80
.L87:
	cmpl	$22593, %edx
	ja	.L88
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L84
.L94:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L91
.L107:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L105
.L190:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L99
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L100
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L97:
	testq	%r8, %r8
	je	.L194
.L100:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L129
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L101
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L101
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L101
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L101
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L101
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L101
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L101
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L101
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L101
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L101
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L101
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L101
	leaq	7(%rsi), %rdx
	jmp	.L101
.L88:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L84
.L124:
	movq	%r11, %r10
	jmp	.L80
.L129:
	movq	%rsi, %rdx
	jmp	.L101
	.cfi_endproc
.LFE126:
	.size	BrotliCompressFragmentTwoPassImpl8.isra.0, .-BrotliCompressFragmentTwoPassImpl8.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl9.isra.0, @function
BrotliCompressFragmentTwoPassImpl9.isra.0:
.LFB127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L195
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L196:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L248
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$55, %rdi
	cmpq	%r9, %r12
	jb	.L197
.L313:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L199:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L200
.L201:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L308
	movq	%rcx, %r9
	movl	%edx, %ecx
.L198:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$55, %rdi
	cmpl	%esi, (%rax)
	jne	.L199
	cmpq	%rax, %rdx
	jnb	.L199
	movl	%r11d, (%r8)
.L200:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L201
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L202
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L204:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L312
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L205:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L208
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L209:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L214
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L214:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L215
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L216:
	cmpq	%r11, %r12
	jbe	.L255
.L314:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$55, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$55, %rcx
	shrq	$55, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L221
.L220:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L233
.L221:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$55, %rdi
	cmpq	%r9, %r12
	jnb	.L313
	.p2align 4,,10
	.p2align 3
.L197:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L235
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L236
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L237:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L235:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L242
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L243:
	subq	%r15, -88(%rbp)
	jne	.L196
.L195:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L242:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L236:
	cmpl	$129, %eax
	ja	.L238
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	$2113, %eax
	ja	.L239
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L248:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L197
.L208:
	cmpl	$129, %edx
	ja	.L210
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L209
.L215:
	cmpq	$71, %r9
	ja	.L217
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L314
.L255:
	movq	%r11, %rsi
	jmp	.L197
.L233:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L222
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L224:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L315
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L226:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L245
	cmpq	$133, %rax
	jbe	.L316
	cmpq	$2117, %rax
	ja	.L232
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L230
.L239:
	cmpl	$6209, %eax
	ja	.L240
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L237
.L217:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L317
	cmpq	$2119, %r9
	ja	.L219
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L216
.L210:
	cmpl	$2113, %edx
	jbe	.L318
	cmpl	$6209, %edx
	ja	.L212
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L209
.L319:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L245:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L230:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L255
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$55, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$55, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$55, %rax
	shrq	$55, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L220
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L240:
	cmpl	$22593, %eax
	ja	.L241
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L237
.L318:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L209
.L317:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L216
.L316:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L230
.L241:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L237
.L312:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L204
	movq	%r9, %r11
.L202:
	andl	$7, %edi
	je	.L249
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L249
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L205
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L205
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L205
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L205
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L205
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L205
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L205
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L205
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L205
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L205
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L205
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L205
	leaq	7(%r11), %r10
	jmp	.L205
.L212:
	cmpl	$22593, %edx
	ja	.L213
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L209
.L219:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L216
.L232:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L230
.L315:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L224
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L225
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L222:
	testq	%r8, %r8
	je	.L319
.L225:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L254
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L226
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L226
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L226
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L226
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L226
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L226
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L226
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L226
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L226
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L226
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L226
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L226
	leaq	7(%rsi), %rdx
	jmp	.L226
.L213:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L209
.L249:
	movq	%r11, %r10
	jmp	.L205
.L254:
	movq	%rsi, %rdx
	jmp	.L226
	.cfi_endproc
.LFE127:
	.size	BrotliCompressFragmentTwoPassImpl9.isra.0, .-BrotliCompressFragmentTwoPassImpl9.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl10.isra.0, @function
BrotliCompressFragmentTwoPassImpl10.isra.0:
.LFB128:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L320
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L373
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$54, %rdi
	cmpq	%r9, %r12
	jb	.L322
.L438:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L324:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L325
.L326:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L433
	movq	%rcx, %r9
	movl	%edx, %ecx
.L323:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$54, %rdi
	cmpl	%esi, (%rax)
	jne	.L324
	cmpq	%rax, %rdx
	jnb	.L324
	movl	%r11d, (%r8)
.L325:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L326
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L327
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L329:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L437
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L330:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L333
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L334:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L339
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L339:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L340
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L341:
	cmpq	%r11, %r12
	jbe	.L380
.L439:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$54, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$54, %rcx
	shrq	$54, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L346
.L345:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L358
.L346:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$54, %rdi
	cmpq	%r9, %r12
	jnb	.L438
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L360
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L361
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L362:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L360:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L367
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L368:
	subq	%r15, -88(%rbp)
	jne	.L321
.L320:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L367:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L361:
	cmpl	$129, %eax
	ja	.L363
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L363:
	cmpl	$2113, %eax
	ja	.L364
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L373:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L322
.L333:
	cmpl	$129, %edx
	ja	.L335
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L334
.L340:
	cmpq	$71, %r9
	ja	.L342
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L439
.L380:
	movq	%r11, %rsi
	jmp	.L322
.L358:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L347
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L349:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L440
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L351:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L370
	cmpq	$133, %rax
	jbe	.L441
	cmpq	$2117, %rax
	ja	.L357
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L355
.L364:
	cmpl	$6209, %eax
	ja	.L365
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L362
.L342:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L442
	cmpq	$2119, %r9
	ja	.L344
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L341
.L335:
	cmpl	$2113, %edx
	jbe	.L443
	cmpl	$6209, %edx
	ja	.L337
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L334
.L444:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L370:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L355:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L380
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$54, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$54, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$54, %rax
	shrq	$54, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L345
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L365:
	cmpl	$22593, %eax
	ja	.L366
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L362
.L443:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L334
.L442:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L341
.L441:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L355
.L366:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L362
.L437:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L329
	movq	%r9, %r11
.L327:
	andl	$7, %edi
	je	.L374
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L374
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L330
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L330
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L330
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L330
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L330
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L330
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L330
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L330
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L330
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L330
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L330
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L330
	leaq	7(%r11), %r10
	jmp	.L330
.L337:
	cmpl	$22593, %edx
	ja	.L338
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L334
.L344:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L341
.L357:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L355
.L440:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L349
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L350
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L347:
	testq	%r8, %r8
	je	.L444
.L350:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L379
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L351
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L351
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L351
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L351
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L351
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L351
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L351
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L351
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L351
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L351
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L351
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L351
	leaq	7(%rsi), %rdx
	jmp	.L351
.L338:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L334
.L374:
	movq	%r11, %r10
	jmp	.L330
.L379:
	movq	%rsi, %rdx
	jmp	.L351
	.cfi_endproc
.LFE128:
	.size	BrotliCompressFragmentTwoPassImpl10.isra.0, .-BrotliCompressFragmentTwoPassImpl10.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl11.isra.0, @function
BrotliCompressFragmentTwoPassImpl11.isra.0:
.LFB129:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L445
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L446:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L498
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$53, %rdi
	cmpq	%r9, %r12
	jb	.L447
.L563:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L449:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L450
.L451:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L558
	movq	%rcx, %r9
	movl	%edx, %ecx
.L448:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$53, %rdi
	cmpl	%esi, (%rax)
	jne	.L449
	cmpq	%rax, %rdx
	jnb	.L449
	movl	%r11d, (%r8)
.L450:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L451
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L452
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L454:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L562
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L455:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L458
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L459:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L464
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L464:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L465
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L466:
	cmpq	%r11, %r12
	jbe	.L505
.L564:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$53, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$53, %rcx
	shrq	$53, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L471
.L470:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L483
.L471:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$53, %rdi
	cmpq	%r9, %r12
	jnb	.L563
	.p2align 4,,10
	.p2align 3
.L447:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L485
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L486
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L487:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L485:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L492
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L493:
	subq	%r15, -88(%rbp)
	jne	.L446
.L445:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L492:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L486:
	cmpl	$129, %eax
	ja	.L488
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L488:
	cmpl	$2113, %eax
	ja	.L489
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L498:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L447
.L458:
	cmpl	$129, %edx
	ja	.L460
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L459
.L465:
	cmpq	$71, %r9
	ja	.L467
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L564
.L505:
	movq	%r11, %rsi
	jmp	.L447
.L483:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L472
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L474:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L565
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L476:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L495
	cmpq	$133, %rax
	jbe	.L566
	cmpq	$2117, %rax
	ja	.L482
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L480
.L489:
	cmpl	$6209, %eax
	ja	.L490
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L487
.L467:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L567
	cmpq	$2119, %r9
	ja	.L469
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L466
.L460:
	cmpl	$2113, %edx
	jbe	.L568
	cmpl	$6209, %edx
	ja	.L462
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L459
.L569:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L495:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L480:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L505
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$53, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$53, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$53, %rax
	shrq	$53, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L470
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L490:
	cmpl	$22593, %eax
	ja	.L491
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L487
.L568:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L459
.L567:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L466
.L566:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L480
.L491:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L487
.L562:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L454
	movq	%r9, %r11
.L452:
	andl	$7, %edi
	je	.L499
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L499
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L455
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L455
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L455
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L455
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L455
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L455
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L455
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L455
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L455
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L455
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L455
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L455
	leaq	7(%r11), %r10
	jmp	.L455
.L462:
	cmpl	$22593, %edx
	ja	.L463
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L459
.L469:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L466
.L482:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L480
.L565:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L474
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L475
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L472:
	testq	%r8, %r8
	je	.L569
.L475:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L504
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L476
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L476
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L476
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L476
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L476
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L476
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L476
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L476
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L476
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L476
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L476
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L476
	leaq	7(%rsi), %rdx
	jmp	.L476
.L463:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L459
.L499:
	movq	%r11, %r10
	jmp	.L455
.L504:
	movq	%rsi, %rdx
	jmp	.L476
	.cfi_endproc
.LFE129:
	.size	BrotliCompressFragmentTwoPassImpl11.isra.0, .-BrotliCompressFragmentTwoPassImpl11.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl12.isra.0, @function
BrotliCompressFragmentTwoPassImpl12.isra.0:
.LFB130:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L570
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L571:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L623
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$52, %rdi
	cmpq	%r9, %r12
	jb	.L572
.L688:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L574:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L575
.L576:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L683
	movq	%rcx, %r9
	movl	%edx, %ecx
.L573:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$52, %rdi
	cmpl	%esi, (%rax)
	jne	.L574
	cmpq	%rax, %rdx
	jnb	.L574
	movl	%r11d, (%r8)
.L575:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L576
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L577
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L579:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L687
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L580:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L583
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L584:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L589
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L589:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L590
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L591:
	cmpq	%r11, %r12
	jbe	.L630
.L689:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$52, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$52, %rcx
	shrq	$52, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L596
.L595:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L608
.L596:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$52, %rdi
	cmpq	%r9, %r12
	jnb	.L688
	.p2align 4,,10
	.p2align 3
.L572:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L610
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L611
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L612:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L610:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L617
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L618:
	subq	%r15, -88(%rbp)
	jne	.L571
.L570:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L617:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L611:
	cmpl	$129, %eax
	ja	.L613
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L613:
	cmpl	$2113, %eax
	ja	.L614
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L623:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L572
.L583:
	cmpl	$129, %edx
	ja	.L585
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L584
.L590:
	cmpq	$71, %r9
	ja	.L592
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L689
.L630:
	movq	%r11, %rsi
	jmp	.L572
.L608:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L597
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L599:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L690
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L601:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L620
	cmpq	$133, %rax
	jbe	.L691
	cmpq	$2117, %rax
	ja	.L607
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L605
.L614:
	cmpl	$6209, %eax
	ja	.L615
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L612
.L592:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L692
	cmpq	$2119, %r9
	ja	.L594
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L591
.L585:
	cmpl	$2113, %edx
	jbe	.L693
	cmpl	$6209, %edx
	ja	.L587
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L584
.L694:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L620:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L605:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L630
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$52, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$52, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$52, %rax
	shrq	$52, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L595
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L615:
	cmpl	$22593, %eax
	ja	.L616
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L612
.L693:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L584
.L692:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L591
.L691:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L605
.L616:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L612
.L687:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L579
	movq	%r9, %r11
.L577:
	andl	$7, %edi
	je	.L624
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L624
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L580
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L580
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L580
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L580
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L580
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L580
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L580
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L580
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L580
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L580
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L580
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L580
	leaq	7(%r11), %r10
	jmp	.L580
.L587:
	cmpl	$22593, %edx
	ja	.L588
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L584
.L594:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L591
.L607:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L605
.L690:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L599
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L600
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L597:
	testq	%r8, %r8
	je	.L694
.L600:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L629
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L601
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L601
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L601
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L601
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L601
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L601
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L601
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L601
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L601
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L601
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L601
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L601
	leaq	7(%rsi), %rdx
	jmp	.L601
.L588:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L584
.L624:
	movq	%r11, %r10
	jmp	.L580
.L629:
	movq	%rsi, %rdx
	jmp	.L601
	.cfi_endproc
.LFE130:
	.size	BrotliCompressFragmentTwoPassImpl12.isra.0, .-BrotliCompressFragmentTwoPassImpl12.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl13.isra.0, @function
BrotliCompressFragmentTwoPassImpl13.isra.0:
.LFB131:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L695
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L696:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L748
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$51, %rdi
	cmpq	%r9, %r12
	jb	.L697
.L813:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L699:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L700
.L701:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L808
	movq	%rcx, %r9
	movl	%edx, %ecx
.L698:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$51, %rdi
	cmpl	%esi, (%rax)
	jne	.L699
	cmpq	%rax, %rdx
	jnb	.L699
	movl	%r11d, (%r8)
.L700:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L701
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L702
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L704:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L812
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L705:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L708
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L709:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L714
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L714:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L715
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L716:
	cmpq	%r11, %r12
	jbe	.L755
.L814:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$51, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$51, %rcx
	shrq	$51, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L721
.L720:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L733
.L721:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$51, %rdi
	cmpq	%r9, %r12
	jnb	.L813
	.p2align 4,,10
	.p2align 3
.L697:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L735
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L736
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L737:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L735:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L742
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L743:
	subq	%r15, -88(%rbp)
	jne	.L696
.L695:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L808:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L742:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L736:
	cmpl	$129, %eax
	ja	.L738
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L738:
	cmpl	$2113, %eax
	ja	.L739
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L748:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L697
.L708:
	cmpl	$129, %edx
	ja	.L710
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L709
.L715:
	cmpq	$71, %r9
	ja	.L717
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L814
.L755:
	movq	%r11, %rsi
	jmp	.L697
.L733:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L722
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L724:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L815
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L726:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L745
	cmpq	$133, %rax
	jbe	.L816
	cmpq	$2117, %rax
	ja	.L732
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L730
.L739:
	cmpl	$6209, %eax
	ja	.L740
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L737
.L717:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L817
	cmpq	$2119, %r9
	ja	.L719
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L716
.L710:
	cmpl	$2113, %edx
	jbe	.L818
	cmpl	$6209, %edx
	ja	.L712
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L709
.L819:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L745:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L730:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L755
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$51, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$51, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$51, %rax
	shrq	$51, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L720
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L740:
	cmpl	$22593, %eax
	ja	.L741
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L737
.L818:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L709
.L817:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L716
.L816:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L730
.L741:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L737
.L812:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L704
	movq	%r9, %r11
.L702:
	andl	$7, %edi
	je	.L749
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L749
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L705
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L705
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L705
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L705
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L705
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L705
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L705
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L705
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L705
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L705
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L705
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L705
	leaq	7(%r11), %r10
	jmp	.L705
.L712:
	cmpl	$22593, %edx
	ja	.L713
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L709
.L719:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L716
.L732:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L730
.L815:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L724
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L725
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L722:
	testq	%r8, %r8
	je	.L819
.L725:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L754
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L726
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L726
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L726
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L726
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L726
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L726
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L726
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L726
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L726
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L726
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L726
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L726
	leaq	7(%rsi), %rdx
	jmp	.L726
.L713:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L709
.L749:
	movq	%r11, %r10
	jmp	.L705
.L754:
	movq	%rsi, %rdx
	jmp	.L726
	.cfi_endproc
.LFE131:
	.size	BrotliCompressFragmentTwoPassImpl13.isra.0, .-BrotliCompressFragmentTwoPassImpl13.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl14.isra.0, @function
BrotliCompressFragmentTwoPassImpl14.isra.0:
.LFB132:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L820
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L821:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L873
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$50, %rdi
	cmpq	%r9, %r12
	jb	.L822
.L938:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L824:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L825
.L826:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L933
	movq	%rcx, %r9
	movl	%edx, %ecx
.L823:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$50, %rdi
	cmpl	%esi, (%rax)
	jne	.L824
	cmpq	%rax, %rdx
	jnb	.L824
	movl	%r11d, (%r8)
.L825:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L826
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L827
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L829:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L937
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L830:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L833
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L834:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L839
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L839:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L840
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L841:
	cmpq	%r11, %r12
	jbe	.L880
.L939:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$50, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$50, %rcx
	shrq	$50, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L846
.L845:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L858
.L846:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$50, %rdi
	cmpq	%r9, %r12
	jnb	.L938
	.p2align 4,,10
	.p2align 3
.L822:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L860
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L861
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L862:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L860:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L867
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L868:
	subq	%r15, -88(%rbp)
	jne	.L821
.L820:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L933:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L867:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L861:
	cmpl	$129, %eax
	ja	.L863
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L863:
	cmpl	$2113, %eax
	ja	.L864
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L873:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L822
.L833:
	cmpl	$129, %edx
	ja	.L835
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L834
.L840:
	cmpq	$71, %r9
	ja	.L842
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L939
.L880:
	movq	%r11, %rsi
	jmp	.L822
.L858:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L847
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L849:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L940
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L851:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L870
	cmpq	$133, %rax
	jbe	.L941
	cmpq	$2117, %rax
	ja	.L857
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L855
.L864:
	cmpl	$6209, %eax
	ja	.L865
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L862
.L842:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L942
	cmpq	$2119, %r9
	ja	.L844
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L841
.L835:
	cmpl	$2113, %edx
	jbe	.L943
	cmpl	$6209, %edx
	ja	.L837
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L834
.L944:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L870:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L855:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L880
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$50, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$50, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$50, %rax
	shrq	$50, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L845
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L865:
	cmpl	$22593, %eax
	ja	.L866
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L862
.L943:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L834
.L942:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L841
.L941:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L855
.L866:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L862
.L937:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L829
	movq	%r9, %r11
.L827:
	andl	$7, %edi
	je	.L874
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L874
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L830
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L830
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L830
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L830
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L830
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L830
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L830
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L830
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L830
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L830
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L830
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L830
	leaq	7(%r11), %r10
	jmp	.L830
.L837:
	cmpl	$22593, %edx
	ja	.L838
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L834
.L844:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L841
.L857:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L855
.L940:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L849
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L850
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L847:
	testq	%r8, %r8
	je	.L944
.L850:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L879
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L851
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L851
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L851
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L851
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L851
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L851
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L851
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L851
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L851
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L851
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L851
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L851
	leaq	7(%rsi), %rdx
	jmp	.L851
.L838:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L834
.L874:
	movq	%r11, %r10
	jmp	.L830
.L879:
	movq	%rsi, %rdx
	jmp	.L851
	.cfi_endproc
.LFE132:
	.size	BrotliCompressFragmentTwoPassImpl14.isra.0, .-BrotliCompressFragmentTwoPassImpl14.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl15.isra.0, @function
BrotliCompressFragmentTwoPassImpl15.isra.0:
.LFB133:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L945
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L946:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rsi
	cmpq	$131072, %rax
	movq	%rsi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L998
	leaq	-16(%rax), %r12
	movq	1(%r14), %rdi
	leaq	-4(%r15), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rax, %r12
	movl	$-1, -96(%rbp)
	cmova	%rax, %r12
	salq	$32, %rdi
	movq	%rsi, -64(%rbp)
	leaq	1(%r14), %rax
	imulq	$506832829, %rdi, %rdi
	movq	-112(%rbp), %rsi
	leaq	1(%rax), %r9
	addq	%r14, %r12
	movq	%rsi, -72(%rbp)
	movq	%r14, %rsi
	shrq	$49, %rdi
	cmpq	%r9, %r12
	jb	.L947
.L1063:
	movslq	-96(%rbp), %r10
	movq	%rsi, -104(%rbp)
	movl	$33, %ecx
	negq	%r10
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L949:
	movslq	(%r8), %rdx
	movl	%r11d, (%r8)
	addq	%rbx, %rdx
	movl	(%rdx), %esi
	cmpl	%esi, (%rax)
	je	.L950
.L951:
	leal	1(%rcx), %edx
	shrl	$5, %ecx
	movq	%r9, %rax
	addq	%r9, %rcx
	cmpq	%rcx, %r12
	jb	.L1058
	movq	%rcx, %r9
	movl	%edx, %ecx
.L948:
	movl	%edi, %r8d
	movq	(%r9), %rdi
	leaq	(%rax,%r10), %rdx
	movq	%rax, %r11
	movl	(%rdx), %esi
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r11
	salq	$32, %rdi
	imulq	$506832829, %rdi, %rdi
	shrq	$49, %rdi
	cmpl	%esi, (%rax)
	jne	.L949
	cmpq	%rax, %rdx
	jnb	.L949
	movl	%r11d, (%r8)
.L950:
	movq	%rax, %r8
	subq	%rdx, %r8
	cmpq	$262128, %r8
	jg	.L951
	movq	-56(%rbp), %rdi
	leaq	4(%rdx), %r10
	movq	-104(%rbp), %rsi
	leaq	4(%rax), %rcx
	movq	%r10, -104(%rbp)
	subq	%rax, %rdi
	subq	$4, %rdi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L952
	leaq	12(%rax), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L954:
	movq	4(%rax,%r9), %rcx
	movq	4(%rdx,%r9), %r10
	cmpq	%r10, %rcx
	je	.L1062
	xorq	%r10, %rcx
	xorl	%edx, %edx
	rep bsfq	%rcx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r9), %r10
.L955:
	movq	%rax, %rdx
	leaq	4(%r10), %r9
	movl	%r8d, -104(%rbp)
	subq	%rsi, %rdx
	leaq	(%rax,%r9), %r11
	cmpl	$5, %edx
	ja	.L958
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
.L959:
	movq	-64(%rbp), %rdi
	movslq	%edx, %rdx
	movq	%r11, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	addq	%rdx, -64(%rbp)
	movl	$64, %edx
	movq	-136(%rbp), %r8
	cmpl	%r8d, -96(%rbp)
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r11
	je	.L964
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L964:
	movq	-72(%rbp), %rax
	movl	%edx, 4(%rax)
	cmpq	$11, %r9
	ja	.L965
	addl	$20, %r9d
	addq	$12, %rax
	movl	%r9d, -4(%rax)
	movq	%rax, -72(%rbp)
.L966:
	cmpq	%r11, %r12
	jbe	.L1005
.L1064:
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	movl	$1, %r9d
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$49, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	salq	$32, %rcx
	salq	$32, %rax
	imulq	$506832829, %rcx, %rcx
	imulq	$506832829, %rax, %rax
	shrq	$49, %rcx
	shrq	$49, %rax
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jg	.L971
.L970:
	movl	(%rax), %esi
	cmpl	%esi, (%r11)
	je	.L983
.L971:
	movq	1(%r11), %rdi
	movl	-104(%rbp), %esi
	leaq	1(%r11), %rax
	leaq	1(%rax), %r9
	salq	$32, %rdi
	movl	%esi, -96(%rbp)
	movq	%r11, %rsi
	imulq	$506832829, %rdi, %rdi
	shrq	$49, %rdi
	cmpq	%r9, %r12
	jnb	.L1063
	.p2align 4,,10
	.p2align 3
.L947:
	movq	-56(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L985
	subq	%rsi, %rax
	cmpl	$5, %eax
	ja	.L986
	movq	-72(%rbp), %rdx
	movl	%eax, (%rdx)
.L987:
	movl	%eax, %r12d
	movq	-64(%rbp), %rdi
	addq	$4, -72(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, -64(%rbp)
.L985:
	movq	-64(%rbp), %r12
	subq	-80(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	ShouldCompress
	testl	%eax, %eax
	je	.L992
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdx
	movq	-72(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	(%rdx), %rax
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	16(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	%rax, (%rdx)
	movq	%rdx, %r9
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L993:
	subq	%r15, -88(%rbp)
	jne	.L946
.L945:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1058:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L992:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L986:
	cmpl	$129, %eax
	ja	.L988
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %edi
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %r8d
	sall	%cl, %edi
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%r8d, %edx
	movl	%edx, (%rdi)
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L988:
	cmpl	$2113, %eax
	ja	.L989
	leal	-66(%rax), %edx
	movl	$1, %edi
	bsrl	%edx, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L998:
	movq	-80(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L947
.L958:
	cmpl	$129, %edx
	ja	.L960
	leal	-2(%rdx), %eax
	bsrl	%eax, %ecx
	movl	%eax, %edi
	movl	%eax, -128(%rbp)
	subl	$1, %ecx
	shrl	%cl, %edi
	leal	2(%rdi,%rcx,2), %eax
	sall	%cl, %edi
	movl	%eax, -136(%rbp)
	movl	-128(%rbp), %eax
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	-136(%rbp), %eax
	movl	%eax, (%rdi)
	jmp	.L959
.L965:
	cmpq	$71, %r9
	ja	.L967
	leaq	-4(%r10), %rax
	bsrl	%eax, %ecx
	movq	%rax, %rdx
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rdx
	leaq	28(%rdx,%rsi,2), %rsi
	salq	%cl, %rdx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, 8(%rsi)
	addq	$12, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r11, %r12
	ja	.L1064
.L1005:
	movq	%r11, %rsi
	jmp	.L947
.L983:
	movq	-56(%rbp), %rdx
	leaq	4(%rax), %rsi
	leaq	4(%r11), %rcx
	movq	%rsi, -96(%rbp)
	subq	%r11, %rdx
	leaq	-4(%rdx), %r8
	movq	%r8, %rsi
	shrq	$3, %rsi
	je	.L972
	leaq	12(%r11), %rdi
	xorl	%edx, %edx
	movq	%rdi, -104(%rbp)
.L974:
	movq	4(%r11,%rdx), %rcx
	movq	4(%rax,%rdx), %rdi
	cmpq	%rdi, %rcx
	je	.L1065
	xorq	%rdi, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rdx
.L976:
	leaq	4(%rdx), %rax
	movl	%r10d, -104(%rbp)
	addq	%rax, %r11
	cmpq	$9, %rax
	jbe	.L995
	cmpq	$133, %rax
	jbe	.L1066
	cmpq	$2117, %rax
	ja	.L982
	subq	$66, %rdx
	movq	%r9, %rax
	movq	-72(%rbp), %rsi
	bsrl	%edx, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rsi)
	jmp	.L980
.L989:
	cmpl	$6209, %eax
	ja	.L990
	leal	-2114(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L987
.L967:
	movq	%rax, %rsi
	cmpq	$135, %r9
	jbe	.L1067
	cmpq	$2119, %r9
	ja	.L969
	leaq	-68(%r10), %rax
	movl	$1, %edx
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L966
.L960:
	cmpl	$2113, %edx
	jbe	.L1068
	cmpl	$6209, %edx
	ja	.L962
	leal	-2114(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$21, %eax
	movl	%eax, (%rdi)
	jmp	.L959
.L1069:
	movl	%r10d, -104(%rbp)
	movq	%rcx, %r11
	movl	$4, %eax
.L995:
	movq	-72(%rbp), %rsi
	addl	$38, %eax
	movl	%eax, (%rsi)
.L980:
	addl	$3, %r10d
	bsrl	%r10d, %edx
	movl	%r10d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r10d
	movq	-72(%rbp), %rax
	sall	$8, %r10d
	orl	%edx, %r10d
	addq	$8, %rax
	movl	%r10d, -4(%rax)
	movq	%rax, -72(%rbp)
	cmpq	%r11, %r12
	jbe	.L1005
	movq	-3(%r11), %rax
	movq	%r11, %rdx
	movq	%r11, %r10
	subq	%rbx, %rdx
	movq	%rax, %rcx
	leal	-3(%rdx), %esi
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$49, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	leal	-2(%rdx), %esi
	shrq	$8, %rcx
	salq	$32, %rcx
	imulq	$506832829, %rcx, %rcx
	shrq	$49, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	movq	%rax, %rcx
	shrq	$24, %rax
	leal	-1(%rdx), %esi
	shrq	$16, %rcx
	salq	$32, %rax
	salq	$32, %rcx
	imulq	$506832829, %rax, %rax
	imulq	$506832829, %rcx, %rcx
	shrq	$49, %rax
	shrq	$49, %rcx
	movl	%esi, 0(%r13,%rcx,4)
	leaq	0(%r13,%rax,4), %rcx
	movslq	(%rcx), %rax
	movl	%edx, (%rcx)
	addq	%rbx, %rax
	subq	%rax, %r10
	cmpq	$262128, %r10
	jle	.L970
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L990:
	cmpl	$22593, %eax
	ja	.L991
	leal	-6210(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L987
.L1068:
	leal	-66(%rdx), %eax
	movl	$1, %edi
	bsrl	%eax, %ecx
	sall	%cl, %edi
	addl	$10, %ecx
	subl	%edi, %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L959
.L1067:
	leaq	-4(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	movq	%rax, %rdx
	andl	$31, %eax
	shrq	$5, %rdx
	sall	$8, %eax
	addq	$54, %rdx
	orl	%edx, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L966
.L1066:
	subq	$2, %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rax
	leal	-1(%rcx), %esi
	movq	%rsi, %rcx
	shrq	%cl, %rax
	leaq	44(%rax,%rsi,2), %rsi
	salq	%cl, %rax
	subq	%rax, %rdx
	movl	%edx, %eax
	sall	$8, %eax
	orl	%esi, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L980
.L991:
	leal	-22594(%rax), %edx
	movq	-72(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L987
.L1062:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %r11
	jne	.L954
	movq	%r9, %r11
.L952:
	andl	$7, %edi
	je	.L999
	movq	-104(%rbp), %rdx
	movzbl	(%rdx,%r11), %r10d
	cmpb	%r10b, (%rcx)
	jne	.L999
	leaq	1(%r11), %r10
	cmpq	$1, %rdi
	je	.L955
	movzbl	1(%rdx,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L955
	leaq	2(%r11), %r10
	cmpq	$2, %rdi
	je	.L955
	movzbl	2(%rdx,%r11), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L955
	leaq	3(%r11), %r10
	cmpq	$3, %rdi
	je	.L955
	movzbl	3(%rdx,%r11), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L955
	leaq	4(%r11), %r10
	cmpq	$4, %rdi
	je	.L955
	movzbl	4(%rdx,%r11), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L955
	leaq	5(%r11), %r10
	subq	$5, %rdi
	je	.L955
	movzbl	5(%rdx,%r11), %r9d
	cmpb	%r9b, 5(%rcx)
	jne	.L955
	leaq	6(%r11), %r10
	cmpq	$1, %rdi
	je	.L955
	movzbl	6(%rdx,%r11), %edx
	cmpb	%dl, 6(%rcx)
	jne	.L955
	leaq	7(%r11), %r10
	jmp	.L955
.L962:
	cmpl	$22593, %edx
	ja	.L963
	leal	-6210(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$22, %eax
	movl	%eax, (%rdi)
	jmp	.L959
.L969:
	leaq	-2116(%r10), %rax
	movl	$64, 12(%rsi)
	addq	$16, %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rsi)
	movq	%rsi, -72(%rbp)
	jmp	.L966
.L982:
	leaq	-2114(%rdx), %rax
	movq	-72(%rbp), %rsi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rsi)
	jmp	.L980
.L1065:
	movq	-104(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	addq	$8, %rdx
	subq	$1, %rsi
	jne	.L974
	movq	%rdx, %rsi
	andl	$7, %r8d
	jne	.L975
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L972:
	testq	%r8, %r8
	je	.L1069
.L975:
	movq	-96(%rbp), %rdi
	movzbl	(%rdi,%rsi), %eax
	cmpb	%al, (%rcx)
	jne	.L1004
	leaq	1(%rsi), %rdx
	cmpq	$1, %r8
	je	.L976
	movzbl	1(%rdi,%rsi), %eax
	cmpb	%al, 1(%rcx)
	jne	.L976
	leaq	2(%rsi), %rdx
	cmpq	$2, %r8
	je	.L976
	movzbl	2(%rdi,%rsi), %eax
	cmpb	%al, 2(%rcx)
	jne	.L976
	leaq	3(%rsi), %rdx
	cmpq	$3, %r8
	je	.L976
	movzbl	3(%rdi,%rsi), %eax
	cmpb	%al, 3(%rcx)
	jne	.L976
	leaq	4(%rsi), %rdx
	cmpq	$4, %r8
	je	.L976
	movzbl	4(%rdi,%rsi), %eax
	cmpb	%al, 4(%rcx)
	jne	.L976
	movq	%r8, %rax
	leaq	5(%rsi), %rdx
	subq	$5, %rax
	je	.L976
	movzbl	5(%rdi,%rsi), %r8d
	cmpb	%r8b, 5(%rcx)
	jne	.L976
	leaq	6(%rsi), %rdx
	cmpq	$1, %rax
	je	.L976
	movzbl	6(%rdi,%rsi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L976
	leaq	7(%rsi), %rdx
	jmp	.L976
.L963:
	leal	-22594(%rdx), %eax
	movq	-72(%rbp), %rdi
	sall	$8, %eax
	orl	$23, %eax
	movl	%eax, (%rdi)
	jmp	.L959
.L999:
	movq	%r11, %r10
	jmp	.L955
.L1004:
	movq	%rsi, %rdx
	jmp	.L976
	.cfi_endproc
.LFE133:
	.size	BrotliCompressFragmentTwoPassImpl15.isra.0, .-BrotliCompressFragmentTwoPassImpl15.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl16.isra.0, @function
BrotliCompressFragmentTwoPassImpl16.isra.0:
.LFB134:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1070
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rdi
	cmpq	$131072, %rax
	movq	%rdi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rdi
	movq	%rdi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L1123
	leaq	-16(%rax), %r12
	movq	1(%r14), %rsi
	leaq	-6(%r15), %rax
	movq	%r14, -72(%rbp)
	cmpq	%rax, %r12
	leaq	1(%r14), %rdx
	movl	$-1, -96(%rbp)
	movq	-80(%rbp), %r11
	cmova	%rax, %r12
	salq	$16, %rsi
	movq	-112(%rbp), %rax
	leaq	1(%rdx), %rdi
	imulq	$506832829, %rsi, %rsi
	addq	%r14, %r12
	movq	%rax, -64(%rbp)
	shrq	$48, %rsi
	cmpq	%rdi, %r12
	jb	.L1072
.L1195:
	movslq	-96(%rbp), %r9
	movq	%r11, -104(%rbp)
	movl	$33, %ecx
	negq	%r9
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1074:
	movslq	(%r8), %rax
	movl	%r10d, (%r8)
	addq	%rbx, %rax
	movl	(%rax), %r11d
	cmpl	%r11d, (%rdx)
	je	.L1192
.L1076:
	leal	1(%rcx), %eax
	shrl	$5, %ecx
	movq	%rdi, %rdx
	addq	%rdi, %rcx
	cmpq	%rcx, %r12
	jb	.L1183
	movq	%rcx, %rdi
	movl	%eax, %ecx
.L1073:
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	leaq	(%rdx,%r9), %rax
	movq	%rdx, %r10
	movl	(%rax), %r11d
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r10
	salq	$16, %rsi
	imulq	$506832829, %rsi, %rsi
	shrq	$48, %rsi
	cmpl	%r11d, (%rdx)
	jne	.L1074
	movzbl	4(%rax), %r11d
	cmpb	%r11b, 4(%rdx)
	jne	.L1074
	movzbl	5(%rax), %r11d
	cmpb	%r11b, 5(%rdx)
	jne	.L1074
	cmpq	%rdx, %rax
	jnb	.L1074
	movl	%r10d, (%r8)
.L1075:
	movq	%rdx, %r8
	subq	%rax, %r8
	cmpq	$262128, %r8
	jg	.L1076
	movq	-56(%rbp), %rsi
	leaq	6(%rax), %rdi
	movq	-104(%rbp), %r11
	leaq	6(%rdx), %rcx
	movq	%rdi, -104(%rbp)
	subq	%rdx, %rsi
	subq	$6, %rsi
	movq	%rsi, %rdi
	shrq	$3, %rdi
	je	.L1077
	leaq	14(%rdx), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L1079:
	movq	6(%rdx,%r9), %rcx
	movq	6(%rax,%r9), %r10
	cmpq	%r10, %rcx
	je	.L1193
	xorq	%rcx, %r10
	rep bsfq	%r10, %r10
	movslq	%r10d, %r10
	shrq	$3, %r10
	addq	%r10, %r9
.L1080:
	leaq	6(%r9), %rdi
	movq	%rdx, %rax
	subq	-72(%rbp), %rax
	movl	%r8d, -128(%rbp)
	movq	%rdi, -104(%rbp)
	leaq	(%rdx,%rdi), %r10
	cmpl	$5, %eax
	ja	.L1083
	movq	-64(%rbp), %rdi
	movl	%eax, (%rdi)
.L1084:
	movq	-72(%rbp), %rsi
	movslq	%eax, %rdx
	movq	%r11, %rdi
	movq	%r10, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %r8
	movq	%rax, %r11
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r10
	addq	%rdx, %r11
	cmpl	%r8d, -96(%rbp)
	movl	$64, %edx
	je	.L1089
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L1089:
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %rdi
	movl	%edx, 4(%rax)
	cmpq	$11, %rdi
	ja	.L1090
	movl	%edi, %r9d
	addq	$12, %rax
	addl	$20, %r9d
	movl	%r9d, -4(%rax)
	movq	%rax, -64(%rbp)
.L1191:
	cmpq	%r10, %r12
	jbe	.L1130
	movq	-5(%r10), %rax
	movq	%r10, %rcx
	movq	%r10, %r9
	subq	%rbx, %rcx
	movq	%rax, %rdx
	leal	-5(%rcx), %esi
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$48, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	movq	%rax, %rdx
	xorw	%ax, %ax
	leal	-4(%rcx), %esi
	shrq	$8, %rdx
	imulq	$506832829, %rax, %rax
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$48, %rax
	shrq	$48, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	leal	-3(%rcx), %edx
	leal	-2(%rcx), %esi
	movl	%edx, 0(%r13,%rax,4)
	movq	-2(%r10), %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$48, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	movq	%rax, %rdx
	xorw	%ax, %ax
	leal	-1(%rcx), %esi
	shrq	$8, %rdx
	imulq	$506832829, %rax, %rax
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$48, %rax
	leaq	0(%r13,%rax,4), %rax
	shrq	$48, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	movslq	(%rax), %rdx
	movl	%ecx, (%rax)
	addq	%rbx, %rdx
	subq	%rdx, %r9
	cmpq	$262128, %r9
	jg	.L1096
	movl	(%rdx), %eax
	cmpl	%eax, (%r10)
	je	.L1194
.L1096:
	movq	1(%r10), %rsi
	movl	-128(%rbp), %eax
	leaq	1(%r10), %rdx
	movq	%r10, -72(%rbp)
	leaq	1(%rdx), %rdi
	salq	$16, %rsi
	movl	%eax, -96(%rbp)
	imulq	$506832829, %rsi, %rsi
	shrq	$48, %rsi
	cmpq	%rdi, %r12
	jnb	.L1195
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rdi
	cmpq	%rdi, %rax
	jbe	.L1110
	subq	%rdi, %rax
	cmpl	$5, %eax
	ja	.L1111
	movq	-64(%rbp), %rdi
	movl	%eax, (%rdi)
.L1112:
	movl	%eax, %r12d
	movq	-72(%rbp), %rsi
	movq	%r11, %rdi
	addq	$4, -64(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	%rax, %r11
	addq	%r12, %r11
.L1110:
	subq	-80(%rbp), %r11
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r11, %rdx
	movq	%r11, %r12
	call	ShouldCompress
	testl	%eax, %eax
	je	.L1117
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdi
	movq	-64(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	(%rdi), %rax
	movq	%rdi, %r9
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movq	%rax, (%rdi)
	movzbl	(%rdx), %ecx
	movq	-120(%rbp), %rdi
	movq	%rcx, (%rdx)
	movq	-112(%rbp), %rcx
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L1118:
	subq	%r15, -88(%rbp)
	jne	.L1071
.L1070:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1192:
	.cfi_restore_state
	movzbl	4(%rax), %r10d
	cmpb	%r10b, 4(%rdx)
	jne	.L1076
	movzbl	5(%rax), %r10d
	cmpb	%r10b, 5(%rdx)
	jne	.L1076
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	-104(%rbp), %r11
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1111:
	cmpl	$129, %eax
	ja	.L1113
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %esi
	subl	$1, %ecx
	shrl	%cl, %esi
	leal	2(%rsi,%rcx,2), %edi
	sall	%cl, %esi
	subl	%esi, %edx
	sall	$8, %edx
	orl	%edi, %edx
	movq	-64(%rbp), %rdi
	movl	%edx, (%rdi)
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1113:
	cmpl	$2113, %eax
	ja	.L1114
	leal	-66(%rax), %edx
	movl	$1, %esi
	movq	-64(%rbp), %rdi
	bsrl	%edx, %ecx
	sall	%cl, %esi
	addl	$10, %ecx
	subl	%esi, %edx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	-112(%rbp), %rax
	movq	-80(%rbp), %r11
	movq	%r14, -72(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L1072
.L1114:
	cmpl	$6209, %eax
	ja	.L1115
	leal	-2114(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L1112
.L1115:
	cmpl	$22593, %eax
	ja	.L1116
	leal	-6210(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L1112
.L1090:
	cmpq	$71, -104(%rbp)
	movq	%rax, %rdi
	jbe	.L1196
	cmpq	$135, -104(%rbp)
	ja	.L1093
	subq	$2, %r9
	movl	$64, 12(%rdi)
	addq	$16, %rdi
	movq	%r9, %rax
	shrq	$5, %rax
	leaq	54(%rax), %rdx
	movq	%r9, %rax
	andl	$31, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -8(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1191
.L1083:
	cmpl	$129, %eax
	jbe	.L1197
	cmpl	$2113, %eax
	ja	.L1086
	leal	-66(%rax), %edx
	movl	$1, %esi
	movq	-64(%rbp), %rdi
	bsrl	%edx, %ecx
	sall	%cl, %esi
	addl	$10, %ecx
	subl	%esi, %edx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L1084
.L1116:
	leal	-22594(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L1112
.L1197:
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %esi
	subl	$1, %ecx
	shrl	%cl, %esi
	leal	2(%rsi,%rcx,2), %edi
	sall	%cl, %esi
	subl	%esi, %edx
	sall	$8, %edx
	orl	%edi, %edx
	movq	-64(%rbp), %rdi
	movl	%edx, (%rdi)
	jmp	.L1084
.L1196:
	subq	$2, %r9
	addq	$12, %rdi
	bsrl	%r9d, %ecx
	movq	%r9, %rax
	leal	-1(%rcx), %edx
	movq	%rdx, %rcx
	shrq	%cl, %rax
	leaq	28(%rax,%rdx,2), %rdx
	salq	%cl, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -4(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1191
.L1194:
	movzbl	4(%rdx), %eax
	cmpb	%al, 4(%r10)
	jne	.L1096
	movzbl	5(%rdx), %eax
	cmpb	%al, 5(%r10)
	jne	.L1096
	movq	-56(%rbp), %rsi
	leaq	6(%r10), %rdi
	leaq	6(%rdx), %r8
	subq	%r10, %rsi
	subq	$6, %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	je	.L1097
	leaq	14(%r10), %rdi
	movq	%rcx, -96(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -72(%rbp)
.L1099:
	movq	6(%r10,%rax), %rdi
	movq	6(%rdx,%rax), %rcx
	cmpq	%rcx, %rdi
	je	.L1198
	movq	%rcx, %rdx
	xorq	%rdi, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %rax
.L1101:
	leaq	6(%rax), %rdx
	movl	%r9d, -128(%rbp)
	addq	%rdx, %r10
	cmpq	$9, %rdx
	ja	.L1104
.L1120:
	movq	-64(%rbp), %rax
	addl	$38, %edx
	movl	%edx, (%rax)
	movq	%rax, %rdi
.L1105:
	addl	$3, %r9d
	addq	$8, %rdi
	bsrl	%r9d, %edx
	movl	%r9d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r9d
	movl	%r9d, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -4(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1191
.L1086:
	cmpl	$6209, %eax
	jbe	.L1199
	cmpl	$22593, %eax
	ja	.L1088
	leal	-6210(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L1084
.L1093:
	cmpq	$2119, -104(%rbp)
	ja	.L1094
	subq	$66, %r9
	movl	$1, %eax
	movl	$64, 12(%rdi)
	addq	$16, %rdi
	bsrl	%r9d, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %r9
	movq	%r9, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1191
.L1193:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %rdi
	jne	.L1079
	movq	%r9, %rdi
.L1077:
	andl	$7, %esi
	je	.L1124
	movq	-104(%rbp), %rax
	movzbl	(%rax,%rdi), %r9d
	cmpb	%r9b, (%rcx)
	jne	.L1124
	leaq	1(%rdi), %r9
	cmpq	$1, %rsi
	je	.L1080
	movzbl	1(%rax,%rdi), %r10d
	cmpb	%r10b, 1(%rcx)
	jne	.L1080
	leaq	2(%rdi), %r9
	cmpq	$2, %rsi
	je	.L1080
	movzbl	2(%rax,%rdi), %r10d
	cmpb	%r10b, 2(%rcx)
	jne	.L1080
	leaq	3(%rdi), %r9
	cmpq	$3, %rsi
	je	.L1080
	movzbl	3(%rax,%rdi), %r10d
	cmpb	%r10b, 3(%rcx)
	jne	.L1080
	leaq	4(%rdi), %r9
	cmpq	$4, %rsi
	je	.L1080
	movzbl	4(%rax,%rdi), %r10d
	cmpb	%r10b, 4(%rcx)
	jne	.L1080
	leaq	5(%rdi), %r9
	subq	$5, %rsi
	je	.L1080
	movzbl	5(%rax,%rdi), %r10d
	cmpb	%r10b, 5(%rcx)
	jne	.L1080
	leaq	6(%rdi), %r9
	cmpq	$1, %rsi
	je	.L1080
	movzbl	6(%rax,%rdi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L1080
	leaq	7(%rdi), %r9
	jmp	.L1080
.L1199:
	leal	-2114(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L1084
.L1094:
	leaq	-2114(%r9), %rax
	movl	$64, 12(%rdi)
	addq	$16, %rdi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1191
.L1130:
	movq	%r10, -72(%rbp)
	jmp	.L1072
.L1088:
	leal	-22594(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L1084
.L1124:
	movq	%rdi, %r9
	jmp	.L1080
.L1104:
	cmpq	$133, %rdx
	ja	.L1106
	bsrl	%eax, %ecx
	movq	%rax, %rsi
	movq	-64(%rbp), %rdi
	leal	-1(%rcx), %edx
	movq	%rdx, %rcx
	shrq	%cl, %rsi
	leaq	44(%rsi,%rdx,2), %rdx
	salq	%cl, %rsi
	subq	%rsi, %rax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, (%rdi)
	jmp	.L1105
.L1106:
	cmpq	$2117, %rdx
	ja	.L1107
	subq	$64, %rax
	movl	$1, %edx
	movq	-64(%rbp), %rdi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L1105
.L1198:
	movq	-72(%rbp), %rdi
	addq	%rax, %rdi
	addq	$8, %rax
	subq	$1, -96(%rbp)
	jne	.L1099
	movq	%rax, %rcx
	andl	$7, %esi
	je	.L1101
.L1100:
	movzbl	(%r8,%rcx), %eax
	cmpb	%al, (%rdi)
	jne	.L1129
	leaq	1(%rcx), %rax
	cmpq	$1, %rsi
	je	.L1101
	movzbl	1(%r8,%rcx), %edx
	cmpb	%dl, 1(%rdi)
	jne	.L1101
	leaq	2(%rcx), %rax
	cmpq	$2, %rsi
	je	.L1101
	movzbl	2(%r8,%rcx), %edx
	cmpb	%dl, 2(%rdi)
	jne	.L1101
	leaq	3(%rcx), %rax
	cmpq	$3, %rsi
	je	.L1101
	movzbl	3(%r8,%rcx), %edx
	cmpb	%dl, 3(%rdi)
	jne	.L1101
	leaq	4(%rcx), %rax
	cmpq	$4, %rsi
	je	.L1101
	movzbl	4(%r8,%rcx), %edx
	cmpb	%dl, 4(%rdi)
	jne	.L1101
	leaq	5(%rcx), %rax
	subq	$5, %rsi
	je	.L1101
	movzbl	5(%r8,%rcx), %edx
	cmpb	%dl, 5(%rdi)
	jne	.L1101
	subq	$1, %rsi
	leaq	6(%rcx), %rax
	je	.L1101
	movzbl	6(%r8,%rcx), %edx
	cmpb	%dl, 6(%rdi)
	jne	.L1101
	leaq	7(%rcx), %rax
	jmp	.L1101
.L1097:
	testq	%rsi, %rsi
	jne	.L1100
	movl	%r9d, -128(%rbp)
	movq	%rdi, %r10
	movl	$6, %edx
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1107:
	subq	$2112, %rax
	movq	-64(%rbp), %rdi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rdi)
	jmp	.L1105
.L1129:
	movq	%rcx, %rax
	jmp	.L1101
	.cfi_endproc
.LFE134:
	.size	BrotliCompressFragmentTwoPassImpl16.isra.0, .-BrotliCompressFragmentTwoPassImpl16.isra.0
	.p2align 4
	.type	BrotliCompressFragmentTwoPassImpl17.isra.0, @function
BrotliCompressFragmentTwoPassImpl17.isra.0:
.LFB135:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1200
	movq	%rsi, %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	-88(%rbp), %rax
	movl	$131072, %r15d
	movq	-56(%rbp), %rdi
	cmpq	$131072, %rax
	movq	%rdi, %r14
	cmovbe	%rax, %r15
	addq	%r15, %rdi
	movq	%rdi, -56(%rbp)
	cmpq	$15, %rax
	jbe	.L1253
	leaq	-16(%rax), %r12
	movq	1(%r14), %rsi
	leaq	-6(%r15), %rax
	movq	%r14, -72(%rbp)
	cmpq	%rax, %r12
	leaq	1(%r14), %rdx
	movl	$-1, -96(%rbp)
	movq	-80(%rbp), %r11
	cmova	%rax, %r12
	salq	$16, %rsi
	movq	-112(%rbp), %rax
	leaq	1(%rdx), %rdi
	imulq	$506832829, %rsi, %rsi
	addq	%r14, %r12
	movq	%rax, -64(%rbp)
	shrq	$47, %rsi
	cmpq	%rdi, %r12
	jb	.L1202
.L1325:
	movslq	-96(%rbp), %r9
	movq	%r11, -104(%rbp)
	movl	$33, %ecx
	negq	%r9
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1204:
	movslq	(%r8), %rax
	movl	%r10d, (%r8)
	addq	%rbx, %rax
	movl	(%rax), %r11d
	cmpl	%r11d, (%rdx)
	je	.L1322
.L1206:
	leal	1(%rcx), %eax
	shrl	$5, %ecx
	movq	%rdi, %rdx
	addq	%rdi, %rcx
	cmpq	%rcx, %r12
	jb	.L1313
	movq	%rcx, %rdi
	movl	%eax, %ecx
.L1203:
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	leaq	(%rdx,%r9), %rax
	movq	%rdx, %r10
	movl	(%rax), %r11d
	leaq	0(%r13,%r8,4), %r8
	subq	%rbx, %r10
	salq	$16, %rsi
	imulq	$506832829, %rsi, %rsi
	shrq	$47, %rsi
	cmpl	%r11d, (%rdx)
	jne	.L1204
	movzbl	4(%rax), %r11d
	cmpb	%r11b, 4(%rdx)
	jne	.L1204
	movzbl	5(%rax), %r11d
	cmpb	%r11b, 5(%rdx)
	jne	.L1204
	cmpq	%rdx, %rax
	jnb	.L1204
	movl	%r10d, (%r8)
.L1205:
	movq	%rdx, %r8
	subq	%rax, %r8
	cmpq	$262128, %r8
	jg	.L1206
	movq	-56(%rbp), %rsi
	leaq	6(%rax), %rdi
	movq	-104(%rbp), %r11
	leaq	6(%rdx), %rcx
	movq	%rdi, -104(%rbp)
	subq	%rdx, %rsi
	subq	$6, %rsi
	movq	%rsi, %rdi
	shrq	$3, %rdi
	je	.L1207
	leaq	14(%rdx), %rcx
	xorl	%r9d, %r9d
	movq	%rcx, -128(%rbp)
.L1209:
	movq	6(%rdx,%r9), %rcx
	movq	6(%rax,%r9), %r10
	cmpq	%r10, %rcx
	je	.L1323
	xorq	%rcx, %r10
	rep bsfq	%r10, %r10
	movslq	%r10d, %r10
	shrq	$3, %r10
	addq	%r10, %r9
.L1210:
	leaq	6(%r9), %rdi
	movq	%rdx, %rax
	subq	-72(%rbp), %rax
	movl	%r8d, -128(%rbp)
	movq	%rdi, -104(%rbp)
	leaq	(%rdx,%rdi), %r10
	cmpl	$5, %eax
	ja	.L1213
	movq	-64(%rbp), %rdi
	movl	%eax, (%rdi)
.L1214:
	movq	-72(%rbp), %rsi
	movslq	%eax, %rdx
	movq	%r11, %rdi
	movq	%r10, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %r8
	movq	%rax, %r11
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r10
	addq	%rdx, %r11
	cmpl	%r8d, -96(%rbp)
	movl	$64, %edx
	je	.L1219
	addl	$3, %r8d
	bsrl	%r8d, %edx
	movl	%r8d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r8d
	sall	$8, %r8d
	orl	%r8d, %edx
.L1219:
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %rdi
	movl	%edx, 4(%rax)
	cmpq	$11, %rdi
	ja	.L1220
	movl	%edi, %r9d
	addq	$12, %rax
	addl	$20, %r9d
	movl	%r9d, -4(%rax)
	movq	%rax, -64(%rbp)
.L1321:
	cmpq	%r10, %r12
	jbe	.L1260
	movq	-5(%r10), %rax
	movq	%r10, %rcx
	movq	%r10, %r9
	subq	%rbx, %rcx
	movq	%rax, %rdx
	leal	-5(%rcx), %esi
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$47, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	movq	%rax, %rdx
	xorw	%ax, %ax
	leal	-4(%rcx), %esi
	shrq	$8, %rdx
	imulq	$506832829, %rax, %rax
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$47, %rax
	shrq	$47, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	leal	-3(%rcx), %edx
	leal	-2(%rcx), %esi
	movl	%edx, 0(%r13,%rax,4)
	movq	-2(%r10), %rax
	movq	%rax, %rdx
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$47, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	movq	%rax, %rdx
	xorw	%ax, %ax
	leal	-1(%rcx), %esi
	shrq	$8, %rdx
	imulq	$506832829, %rax, %rax
	salq	$16, %rdx
	imulq	$506832829, %rdx, %rdx
	shrq	$47, %rax
	leaq	0(%r13,%rax,4), %rax
	shrq	$47, %rdx
	movl	%esi, 0(%r13,%rdx,4)
	movslq	(%rax), %rdx
	movl	%ecx, (%rax)
	addq	%rbx, %rdx
	subq	%rdx, %r9
	cmpq	$262128, %r9
	jg	.L1226
	movl	(%rdx), %eax
	cmpl	%eax, (%r10)
	je	.L1324
.L1226:
	movq	1(%r10), %rsi
	movl	-128(%rbp), %eax
	leaq	1(%r10), %rdx
	movq	%r10, -72(%rbp)
	leaq	1(%rdx), %rdi
	salq	$16, %rsi
	movl	%eax, -96(%rbp)
	imulq	$506832829, %rsi, %rsi
	shrq	$47, %rsi
	cmpq	%rdi, %r12
	jnb	.L1325
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %rdi
	cmpq	%rdi, %rax
	jbe	.L1240
	subq	%rdi, %rax
	cmpl	$5, %eax
	ja	.L1241
	movq	-64(%rbp), %rdi
	movl	%eax, (%rdi)
.L1242:
	movl	%eax, %r12d
	movq	-72(%rbp), %rsi
	movq	%r11, %rdi
	addq	$4, -64(%rbp)
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	%rax, %r11
	addq	%r12, %r11
.L1240:
	subq	-80(%rbp), %r11
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r11, %rdx
	movq	%r11, %r12
	call	ShouldCompress
	testl	%eax, %eax
	je	.L1247
	movq	24(%rbp), %rdx
	movq	16(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	16(%rbp), %rdi
	movq	-64(%rbp), %r8
	subq	$8, %rsp
	movq	-80(%rbp), %rsi
	movq	(%rdi), %rax
	movq	%rdi, %r9
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	addq	24(%rbp), %rdx
	movq	%rax, (%rdi)
	movzbl	(%rdx), %ecx
	movq	-120(%rbp), %rdi
	movq	%rcx, (%rdx)
	movq	-112(%rbp), %rcx
	movq	%r12, %rdx
	pushq	24(%rbp)
	subq	%rcx, %r8
	sarq	$2, %r8
	call	StoreCommands
	popq	%rax
	popq	%rdx
.L1248:
	subq	%r15, -88(%rbp)
	jne	.L1201
.L1200:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1322:
	.cfi_restore_state
	movzbl	4(%rax), %r10d
	cmpb	%r10b, 4(%rdx)
	jne	.L1206
	movzbl	5(%rax), %r10d
	cmpb	%r10b, 5(%rdx)
	jne	.L1206
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	-104(%rbp), %r11
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	24(%rbp), %rcx
	movq	16(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1241:
	cmpl	$129, %eax
	ja	.L1243
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %esi
	subl	$1, %ecx
	shrl	%cl, %esi
	leal	2(%rsi,%rcx,2), %edi
	sall	%cl, %esi
	subl	%esi, %edx
	sall	$8, %edx
	orl	%edi, %edx
	movq	-64(%rbp), %rdi
	movl	%edx, (%rdi)
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1243:
	cmpl	$2113, %eax
	ja	.L1244
	leal	-66(%rax), %edx
	movl	$1, %esi
	movq	-64(%rbp), %rdi
	bsrl	%edx, %ecx
	sall	%cl, %esi
	addl	$10, %ecx
	subl	%esi, %edx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	-112(%rbp), %rax
	movq	-80(%rbp), %r11
	movq	%r14, -72(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L1202
.L1244:
	cmpl	$6209, %eax
	ja	.L1245
	leal	-2114(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L1242
.L1245:
	cmpl	$22593, %eax
	ja	.L1246
	leal	-6210(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L1242
.L1220:
	cmpq	$71, -104(%rbp)
	movq	%rax, %rdi
	jbe	.L1326
	cmpq	$135, -104(%rbp)
	ja	.L1223
	subq	$2, %r9
	movl	$64, 12(%rdi)
	addq	$16, %rdi
	movq	%r9, %rax
	shrq	$5, %rax
	leaq	54(%rax), %rdx
	movq	%r9, %rax
	andl	$31, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -8(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1321
.L1213:
	cmpl	$129, %eax
	jbe	.L1327
	cmpl	$2113, %eax
	ja	.L1216
	leal	-66(%rax), %edx
	movl	$1, %esi
	movq	-64(%rbp), %rdi
	bsrl	%edx, %ecx
	sall	%cl, %esi
	addl	$10, %ecx
	subl	%esi, %edx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, (%rdi)
	jmp	.L1214
.L1246:
	leal	-22594(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L1242
.L1327:
	leal	-2(%rax), %edx
	bsrl	%edx, %ecx
	movl	%edx, %esi
	subl	$1, %ecx
	shrl	%cl, %esi
	leal	2(%rsi,%rcx,2), %edi
	sall	%cl, %esi
	subl	%esi, %edx
	sall	$8, %edx
	orl	%edi, %edx
	movq	-64(%rbp), %rdi
	movl	%edx, (%rdi)
	jmp	.L1214
.L1326:
	subq	$2, %r9
	addq	$12, %rdi
	bsrl	%r9d, %ecx
	movq	%r9, %rax
	leal	-1(%rcx), %edx
	movq	%rdx, %rcx
	shrq	%cl, %rax
	leaq	28(%rax,%rdx,2), %rdx
	salq	%cl, %rax
	subq	%rax, %r9
	movq	%r9, %rax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -4(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1321
.L1324:
	movzbl	4(%rdx), %eax
	cmpb	%al, 4(%r10)
	jne	.L1226
	movzbl	5(%rdx), %eax
	cmpb	%al, 5(%r10)
	jne	.L1226
	movq	-56(%rbp), %rsi
	leaq	6(%r10), %rdi
	leaq	6(%rdx), %r8
	subq	%r10, %rsi
	subq	$6, %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	je	.L1227
	leaq	14(%r10), %rdi
	movq	%rcx, -96(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -72(%rbp)
.L1229:
	movq	6(%r10,%rax), %rdi
	movq	6(%rdx,%rax), %rcx
	cmpq	%rcx, %rdi
	je	.L1328
	movq	%rcx, %rdx
	xorq	%rdi, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %rax
.L1231:
	leaq	6(%rax), %rdx
	movl	%r9d, -128(%rbp)
	addq	%rdx, %r10
	cmpq	$9, %rdx
	ja	.L1234
.L1250:
	movq	-64(%rbp), %rax
	addl	$38, %edx
	movl	%edx, (%rax)
	movq	%rax, %rdi
.L1235:
	addl	$3, %r9d
	addq	$8, %rdi
	bsrl	%r9d, %edx
	movl	%r9d, %eax
	leal	-1(%rdx), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	76(%rax,%rdx,2), %edx
	addl	$2, %eax
	sall	%cl, %eax
	subl	%eax, %r9d
	movl	%r9d, %eax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, -4(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1321
.L1216:
	cmpl	$6209, %eax
	jbe	.L1329
	cmpl	$22593, %eax
	ja	.L1218
	leal	-6210(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$22, %edx
	movl	%edx, (%rdi)
	jmp	.L1214
.L1223:
	cmpq	$2119, -104(%rbp)
	ja	.L1224
	subq	$66, %r9
	movl	$1, %eax
	movl	$64, 12(%rdi)
	addq	$16, %rdi
	bsrl	%r9d, %ecx
	salq	%cl, %rax
	addl	$52, %ecx
	subq	%rax, %r9
	movq	%r9, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, -8(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1321
.L1323:
	movq	-128(%rbp), %rcx
	addq	%r9, %rcx
	addq	$8, %r9
	subq	$1, %rdi
	jne	.L1209
	movq	%r9, %rdi
.L1207:
	andl	$7, %esi
	je	.L1254
	movq	-104(%rbp), %rax
	movzbl	(%rax,%rdi), %r9d
	cmpb	%r9b, (%rcx)
	jne	.L1254
	leaq	1(%rdi), %r9
	cmpq	$1, %rsi
	je	.L1210
	movzbl	1(%rax,%rdi), %r10d
	cmpb	%r10b, 1(%rcx)
	jne	.L1210
	leaq	2(%rdi), %r9
	cmpq	$2, %rsi
	je	.L1210
	movzbl	2(%rax,%rdi), %r10d
	cmpb	%r10b, 2(%rcx)
	jne	.L1210
	leaq	3(%rdi), %r9
	cmpq	$3, %rsi
	je	.L1210
	movzbl	3(%rax,%rdi), %r10d
	cmpb	%r10b, 3(%rcx)
	jne	.L1210
	leaq	4(%rdi), %r9
	cmpq	$4, %rsi
	je	.L1210
	movzbl	4(%rax,%rdi), %r10d
	cmpb	%r10b, 4(%rcx)
	jne	.L1210
	leaq	5(%rdi), %r9
	subq	$5, %rsi
	je	.L1210
	movzbl	5(%rax,%rdi), %r10d
	cmpb	%r10b, 5(%rcx)
	jne	.L1210
	leaq	6(%rdi), %r9
	cmpq	$1, %rsi
	je	.L1210
	movzbl	6(%rax,%rdi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L1210
	leaq	7(%rdi), %r9
	jmp	.L1210
.L1329:
	leal	-2114(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$21, %edx
	movl	%edx, (%rdi)
	jmp	.L1214
.L1224:
	leaq	-2114(%r9), %rax
	movl	$64, 12(%rdi)
	addq	$16, %rdi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, -8(%rdi)
	movq	%rdi, -64(%rbp)
	jmp	.L1321
.L1260:
	movq	%r10, -72(%rbp)
	jmp	.L1202
.L1218:
	leal	-22594(%rax), %edx
	movq	-64(%rbp), %rdi
	sall	$8, %edx
	orl	$23, %edx
	movl	%edx, (%rdi)
	jmp	.L1214
.L1254:
	movq	%rdi, %r9
	jmp	.L1210
.L1234:
	cmpq	$133, %rdx
	ja	.L1236
	bsrl	%eax, %ecx
	movq	%rax, %rsi
	movq	-64(%rbp), %rdi
	leal	-1(%rcx), %edx
	movq	%rdx, %rcx
	shrq	%cl, %rsi
	leaq	44(%rsi,%rdx,2), %rdx
	salq	%cl, %rsi
	subq	%rsi, %rax
	sall	$8, %eax
	orl	%edx, %eax
	movl	%eax, (%rdi)
	jmp	.L1235
.L1236:
	cmpq	$2117, %rdx
	ja	.L1237
	subq	$64, %rax
	movl	$1, %edx
	movq	-64(%rbp), %rdi
	bsrl	%eax, %ecx
	salq	%cl, %rdx
	addl	$52, %ecx
	subq	%rdx, %rax
	sall	$8, %eax
	orl	%ecx, %eax
	movl	%eax, (%rdi)
	jmp	.L1235
.L1328:
	movq	-72(%rbp), %rdi
	addq	%rax, %rdi
	addq	$8, %rax
	subq	$1, -96(%rbp)
	jne	.L1229
	movq	%rax, %rcx
	andl	$7, %esi
	je	.L1231
.L1230:
	movzbl	(%r8,%rcx), %eax
	cmpb	%al, (%rdi)
	jne	.L1259
	leaq	1(%rcx), %rax
	cmpq	$1, %rsi
	je	.L1231
	movzbl	1(%r8,%rcx), %edx
	cmpb	%dl, 1(%rdi)
	jne	.L1231
	leaq	2(%rcx), %rax
	cmpq	$2, %rsi
	je	.L1231
	movzbl	2(%r8,%rcx), %edx
	cmpb	%dl, 2(%rdi)
	jne	.L1231
	leaq	3(%rcx), %rax
	cmpq	$3, %rsi
	je	.L1231
	movzbl	3(%r8,%rcx), %edx
	cmpb	%dl, 3(%rdi)
	jne	.L1231
	leaq	4(%rcx), %rax
	cmpq	$4, %rsi
	je	.L1231
	movzbl	4(%r8,%rcx), %edx
	cmpb	%dl, 4(%rdi)
	jne	.L1231
	leaq	5(%rcx), %rax
	subq	$5, %rsi
	je	.L1231
	movzbl	5(%r8,%rcx), %edx
	cmpb	%dl, 5(%rdi)
	jne	.L1231
	subq	$1, %rsi
	leaq	6(%rcx), %rax
	je	.L1231
	movzbl	6(%r8,%rcx), %edx
	cmpb	%dl, 6(%rdi)
	jne	.L1231
	leaq	7(%rcx), %rax
	jmp	.L1231
.L1227:
	testq	%rsi, %rsi
	jne	.L1230
	movl	%r9d, -128(%rbp)
	movq	%rdi, %r10
	movl	$6, %edx
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1237:
	subq	$2112, %rax
	movq	-64(%rbp), %rdi
	sall	$8, %eax
	orl	$63, %eax
	movl	%eax, (%rdi)
	jmp	.L1235
.L1259:
	movq	%rcx, %rax
	jmp	.L1231
	.cfi_endproc
.LFE135:
	.size	BrotliCompressFragmentTwoPassImpl17.isra.0, .-BrotliCompressFragmentTwoPassImpl17.isra.0
	.section	.text.unlikely,"ax",@progbits
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4
	.globl	BrotliCompressFragmentTwoPass
	.hidden	BrotliCompressFragmentTwoPass
	.type	BrotliCompressFragmentTwoPass, @function
BrotliCompressFragmentTwoPass:
.LFB125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	bsrl	24(%rbp), %eax
	movq	32(%rbp), %rbx
	movq	40(%rbp), %r12
	movq	(%rbx), %r10
	subl	$8, %eax
	cmpl	$9, %eax
	ja	.L1345
	leaq	.L1333(%rip), %rdx
	movq	%r8, %rcx
	movq	%r10, -56(%rbp)
	movq	%r9, %r8
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1333:
	.long	.L1342-.L1333
	.long	.L1341-.L1333
	.long	.L1340-.L1333
	.long	.L1339-.L1333
	.long	.L1338-.L1333
	.long	.L1337-.L1333
	.long	.L1336-.L1333
	.long	.L1335-.L1333
	.long	.L1334-.L1333
	.long	.L1332-.L1333
	.text
	.p2align 4,,10
	.p2align 3
.L1334:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl16.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%rsi
	popq	%rdi
	subq	%r10, %rax
	.p2align 4,,10
	.p2align 3
.L1331:
	leaq	31(,%r14,8), %rdx
	cmpq	%rax, %rdx
	jb	.L1350
.L1343:
	testl	%r13d, %r13d
	je	.L1330
	movq	(%rbx), %rax
	movq	%rax, %rdx
	movl	%eax, %esi
	shrq	$3, %rdx
	andl	$7, %esi
	addq	%r12, %rdx
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	%rcx, (%rdx)
	leaq	1(%rax), %rdx
	addq	$9, %rax
	movq	%rdx, %rcx
	movq	%rdx, (%rbx)
	andl	$7, %edx
	andl	$4294967288, %eax
	shrq	$3, %rcx
	addq	%rcx, %r12
	movzbl	(%r12), %ecx
	btsq	%rdx, %rcx
	movq	%rcx, (%r12)
	movq	%rax, (%rbx)
.L1330:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1332:
	.cfi_restore_state
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl17.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%rdx
	popq	%rcx
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1342:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl8.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%r8
	popq	%r9
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1341:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl9.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%rsi
	popq	%rdi
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1340:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl10.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%rdx
	popq	%rcx
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1339:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl11.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%r9
	popq	%r11
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1338:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl12.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%rdi
	popq	%r8
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1337:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl13.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%rcx
	popq	%rsi
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1336:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl14.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%r11
	popq	%rdx
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1335:
	pushq	%r12
	movq	16(%rbp), %r9
	movq	%r14, %rdx
	pushq	%rbx
	call	BrotliCompressFragmentTwoPassImpl15.isra.0
	movq	(%rbx), %rax
	movq	-56(%rbp), %r10
	popq	%r8
	popq	%r9
	subq	%r10, %rax
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	%r10, %rcx
	movq	%r10, %rdx
	movl	$1, %eax
	movq	%r14, %rsi
	andl	$7, %ecx
	shrq	$3, %rdx
	movq	%r15, %rdi
	sall	%cl, %eax
	movq	%r12, %rcx
	subl	$1, %eax
	andb	%al, (%r12,%rdx)
	movq	%rbx, %rdx
	movq	%r10, (%rbx)
	call	EmitUncompressedMetaBlock
	jmp	.L1343
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	BrotliCompressFragmentTwoPass.cold, @function
BrotliCompressFragmentTwoPass.cold:
.LFSB125:
.L1345:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%eax, %eax
	jmp	.L1331
	.cfi_endproc
.LFE125:
	.text
	.size	BrotliCompressFragmentTwoPass, .-BrotliCompressFragmentTwoPass
	.section	.text.unlikely
	.size	BrotliCompressFragmentTwoPass.cold, .-BrotliCompressFragmentTwoPass.cold
.LCOLDE4:
	.text
.LHOTE4:
	.section	.rodata
	.align 32
	.type	kInsertOffset.4850, @object
	.size	kInsertOffset.4850, 96
kInsertOffset.4850:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	8
	.long	10
	.long	14
	.long	18
	.long	26
	.long	34
	.long	50
	.long	66
	.long	98
	.long	130
	.long	194
	.long	322
	.long	578
	.long	1090
	.long	2114
	.long	6210
	.long	22594
	.align 32
	.type	kNumExtraBits.4849, @object
	.size	kNumExtraBits.4849, 512
kNumExtraBits.4849:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	12
	.long	14
	.long	24
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	24
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	6
	.long	7
	.long	7
	.long	8
	.long	8
	.long	9
	.long	9
	.long	10
	.long	10
	.long	11
	.long	11
	.long	12
	.long	12
	.long	13
	.long	13
	.long	14
	.long	14
	.long	15
	.long	15
	.long	16
	.long	16
	.long	17
	.long	17
	.long	18
	.long	18
	.long	19
	.long	19
	.long	20
	.long	20
	.long	21
	.long	21
	.long	22
	.long	22
	.long	23
	.long	23
	.long	24
	.long	24
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4123168604
	.long	1072651304
	.align 8
.LC2:
	.long	0
	.long	1075838976
	.align 8
.LC3:
	.long	0
	.long	1078296576
	.hidden	BrotliStoreHuffmanTree
	.hidden	BrotliConvertBitDepthsToSymbols
	.hidden	BrotliCreateHuffmanTree
	.hidden	BrotliBuildAndStoreHuffmanTreeFast
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
