	.file	"bit_cost.c"
	.text
	.p2align 4
	.globl	BrotliPopulationCostLiteral
	.hidden	BrotliPopulationCostLiteral
	.type	BrotliPopulationCostLiteral, @function
BrotliPopulationCostLiteral:
.LFB93:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	1024(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rax
	cmpq	$256, %rax
	je	.L147
.L2:
	movl	(%r12,%rax,4), %r8d
	testl	%r8d, %r8d
	je	.L4
	movslq	%edx, %rcx
	addl	$1, %edx
	movq	%rax, -176(%rbp,%rcx,8)
	cmpl	$4, %edx
	jle	.L4
.L5:
	cmpl	$3, %edx
	je	.L148
	cmpl	$4, %edx
	je	.L149
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	$255, %rsi
	ja	.L19
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm5, %xmm5
	cvtss2sd	(%rax,%rsi,4), %xmm5
.L20:
	movsd	.LC5(%rip), %xmm4
	movl	$1, %r13d
	xorl	%ecx, %ecx
	pxor	%xmm3, %xmm3
	movabsq	$-9223372036854775808, %rbx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L150:
	cvttsd2siq	%xmm0, %rax
.L31:
	mulsd	%xmm2, %xmm1
	cmpq	$15, %rax
	movl	$15, %edx
	movq	%r15, %rcx
	cmova	%rdx, %rax
	cmpq	%rax, %r13
	cmovb	%rax, %r13
	addl	$1, -128(%rbp,%rax,4)
	addsd	%xmm1, %xmm3
.L32:
	cmpq	$255, %rcx
	ja	.L33
.L39:
	leaq	(%r12,%rcx,4), %r14
	leaq	1(%rcx), %r15
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.L23
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	ja	.L26
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L27:
	movapd	%xmm5, %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC4(%rip), %xmm0
	addsd	%xmm1, %xmm0
	comisd	%xmm4, %xmm0
	jb	.L150
	subsd	%xmm4, %xmm0
	cvttsd2siq	%xmm0, %rax
	xorq	%rbx, %rax
	jmp	.L31
.L147:
	cmpl	$1, %edx
	jne	.L151
.L6:
	movsd	.LC1(%rip), %xmm0
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	testq	%rsi, %rsi
	js	.L21
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L22:
	call	log2@PLT
	movapd	%xmm0, %xmm5
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L26:
	movapd	%xmm2, %xmm0
	movsd	%xmm5, -192(%rbp)
	movsd	%xmm3, -184(%rbp)
	call	log2@PLT
	movl	(%r14), %eax
	pxor	%xmm2, %xmm2
	movsd	-192(%rbp), %xmm5
	movsd	-184(%rbp), %xmm3
	cvtsi2sdq	%rax, %xmm2
	movq	.LC5(%rip), %rax
	movq	%rax, %xmm4
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	$255, %r15
	ja	.L33
	movl	$256, %esi
	leaq	(%r12,%r15,4), %rdx
	movl	$1, %eax
	subl	%ecx, %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	addl	$1, %eax
	addq	$4, %rdx
	cmpl	%esi, %eax
	je	.L35
.L34:
	movl	(%rdx), %edi
	testl	%edi, %edi
	je	.L36
.L35:
	movl	%eax, %edx
	addq	%rdx, %rcx
	cmpq	$256, %rcx
	je	.L33
	cmpl	$2, %eax
	ja	.L37
	addl	%eax, -128(%rbp)
	cmpq	$255, %rcx
	jbe	.L39
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	18(%r13,%r13), %rax
	pxor	%xmm0, %xmm0
	movl	-128(%rbp), %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm0
	cvtsi2sdq	%rbx, %xmm1
	addsd	%xmm3, %xmm0
	movsd	%xmm0, -232(%rbp)
	cmpq	$255, %rbx
	jbe	.L42
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm1
.L43:
	mulsd	%xmm1, %xmm0
	movl	-124(%rbp), %eax
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	leaq	(%rax,%rbx), %rdi
	movq	%rdi, -240(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L46
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L47:
	mulsd	%xmm2, %xmm0
	movl	-120(%rbp), %r15d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r15, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r15
	jbe	.L50
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L51:
	mulsd	%xmm2, %xmm0
	movl	-116(%rbp), %ebx
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %rbx
	jbe	.L54
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L55:
	mulsd	%xmm2, %xmm0
	movl	-112(%rbp), %r14d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r14, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r14
	jbe	.L58
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L59:
	mulsd	%xmm2, %xmm0
	movl	-108(%rbp), %r13d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r13, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r13
	jbe	.L62
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L63:
	mulsd	%xmm2, %xmm0
	movl	-104(%rbp), %r12d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r12, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r12
	jbe	.L66
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L67:
	mulsd	%xmm2, %xmm0
	movl	-100(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -200(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L70
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L71:
	mulsd	%xmm2, %xmm0
	movl	-96(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -192(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L74
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -208(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-208(%rbp), %xmm1
.L75:
	mulsd	%xmm2, %xmm0
	movl	-92(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -184(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L78
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -216(%rbp)
	movsd	%xmm2, -208(%rbp)
	call	log2@PLT
	movsd	-208(%rbp), %xmm2
	movsd	-216(%rbp), %xmm1
.L79:
	mulsd	%xmm0, %xmm2
	movl	-88(%rbp), %eax
	movq	%rax, -208(%rbp)
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	jbe	.L82
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -224(%rbp)
	movsd	%xmm2, -216(%rbp)
	call	log2@PLT
	movsd	-216(%rbp), %xmm2
	movsd	-224(%rbp), %xmm1
.L83:
	mulsd	%xmm2, %xmm0
	movl	-84(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -216(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L86
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -248(%rbp)
	movsd	%xmm2, -224(%rbp)
	call	log2@PLT
	movsd	-224(%rbp), %xmm2
	movsd	-248(%rbp), %xmm1
.L87:
	mulsd	%xmm0, %xmm2
	movl	-80(%rbp), %eax
	movq	%rax, -224(%rbp)
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	jbe	.L90
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -256(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movsd	-256(%rbp), %xmm1
.L91:
	mulsd	%xmm0, %xmm2
	movl	-76(%rbp), %edi
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdi, %xmm2
	cmpq	$255, %rdi
	jbe	.L94
	movapd	%xmm2, %xmm0
	movq	%rdi, -264(%rbp)
	movsd	%xmm1, -256(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movsd	-256(%rbp), %xmm1
	movq	-264(%rbp), %rdi
.L95:
	mulsd	%xmm0, %xmm2
	movl	-72(%rbp), %esi
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rsi, %xmm2
	cmpq	$255, %rsi
	jbe	.L98
	movapd	%xmm2, %xmm0
	movq	%rsi, -272(%rbp)
	movq	%rdi, -256(%rbp)
	movsd	%xmm1, -264(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movq	-256(%rbp), %rdi
	movsd	-264(%rbp), %xmm1
	movq	-272(%rbp), %rsi
.L99:
	mulsd	%xmm0, %xmm2
	movl	-68(%rbp), %ecx
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
	cmpq	$255, %rcx
	jbe	.L102
	movapd	%xmm2, %xmm0
	movq	%rcx, -280(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	movsd	%xmm1, -272(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movq	-256(%rbp), %rdi
	movq	-264(%rbp), %rsi
	movsd	-272(%rbp), %xmm1
	movq	-280(%rbp), %rcx
.L103:
	mulsd	%xmm0, %xmm2
	movl	-64(%rbp), %edx
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	cmpq	$255, %rdx
	ja	.L106
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L107:
	mulsd	%xmm0, %xmm2
	addq	%r15, %rbx
	addq	-240(%rbp), %rbx
	movl	-60(%rbp), %eax
	addq	%r14, %rbx
	addq	%r13, %rbx
	addq	%r12, %rbx
	addq	-200(%rbp), %rbx
	addq	-192(%rbp), %rbx
	addq	-184(%rbp), %rbx
	addq	-208(%rbp), %rbx
	addq	-216(%rbp), %rbx
	addq	-224(%rbp), %rbx
	subsd	%xmm2, %xmm1
	addq	%rdi, %rbx
	pxor	%xmm2, %xmm2
	addq	%rsi, %rbx
	cvtsi2sdq	%rax, %xmm2
	addq	%rcx, %rbx
	addq	%rdx, %rbx
	addq	%rax, %rbx
	cmpq	$255, %rax
	ja	.L110
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L111:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
	testq	%rbx, %rbx
	je	.L112
	cmpq	$255, %rbx
	ja	.L113
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
.L114:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
.L112:
	maxsd	%xmm1, %xmm2
	movsd	-232(%rbp), %xmm0
	addsd	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	movl	-60(%rbp), %edi
	subl	$2, %eax
	leal	1(%rdi), %edx
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%edx, %esi
	addl	$1, %edx
	shrl	$3, %eax
	addsd	.LC6(%rip), %xmm3
	jne	.L38
	movl	%esi, -60(%rbp)
	jmp	.L32
.L151:
	cmpl	$2, %edx
	jne	.L5
	testq	%rsi, %rsi
	js	.L7
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L8:
	addsd	.LC2(%rip), %xmm0
	jmp	.L1
.L42:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
	jmp	.L43
.L102:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rcx,4), %xmm0
	jmp	.L103
.L98:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rsi,4), %xmm0
	jmp	.L99
.L94:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L95
.L90:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L91
.L86:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L87
.L82:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L83
.L78:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L79
.L74:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L75
.L70:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L71
.L66:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
	jmp	.L67
.L62:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r13,4), %xmm0
	jmp	.L63
.L58:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r14,4), %xmm0
	jmp	.L59
.L54:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
	jmp	.L55
.L50:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r15,4), %xmm0
	jmp	.L51
.L46:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L47
.L106:
	movapd	%xmm2, %xmm0
	movq	%rcx, -280(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%rdi, -264(%rbp)
	movq	%rdx, -256(%rbp)
	movsd	%xmm1, -288(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-288(%rbp), %xmm1
	movq	-280(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdi
	movq	-256(%rbp), %rdx
	movsd	-248(%rbp), %xmm2
	jmp	.L107
.L110:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-192(%rbp), %xmm1
	movsd	-184(%rbp), %xmm2
	jmp	.L111
.L148:
	movq	-176(%rbp), %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	(%r12,%rax,4), %edx
	movq	-168(%rbp), %rax
	movl	(%r12,%rax,4), %esi
	movq	-160(%rbp), %rax
	movl	(%r12,%rax,4), %ecx
	leal	(%rdx,%rsi), %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	cmpl	%esi, %edx
	cmovb	%esi, %edx
	cvtsi2sdq	%rax, %xmm0
	addsd	.LC3(%rip), %xmm0
	cmpl	%ecx, %edx
	cmovb	%ecx, %edx
	movl	%edx, %eax
	cvtsi2sdq	%rax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L1
.L149:
	movq	-152(%rbp), %rax
	xorl	%edi, %edi
	movl	$1, %esi
	movd	(%r12,%rax,4), %xmm1
	movq	-160(%rbp), %rax
	movd	(%r12,%rax,4), %xmm6
	movq	-168(%rbp), %rax
	movd	(%r12,%rax,4), %xmm0
	movq	-176(%rbp), %rax
	punpckldq	%xmm1, %xmm6
	movd	(%r12,%rax,4), %xmm7
	movl	$1, %eax
	punpckldq	%xmm0, %xmm7
	movdqa	%xmm7, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movd	%xmm0, %ecx
	movaps	%xmm0, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L15:
	movl	-128(%rbp,%rax,4), %edx
	cmpl	%ecx, %edx
	jbe	.L16
	movl	%ecx, -128(%rbp,%rax,4)
	movl	%edx, %ecx
	movl	%edx, -128(%rbp,%rdi,4)
.L16:
	addq	$1, %rax
	cmpq	$4, %rax
	jne	.L15
	leaq	1(%rsi), %rax
	cmpq	$3, %rsi
	je	.L123
	movl	-128(%rbp,%rsi,4), %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L15
.L123:
	movl	-116(%rbp), %eax
	addl	-120(%rbp), %eax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	%eax, %ecx
	movl	-128(%rbp), %edx
	movl	-124(%rbp), %eax
	addl	%edx, %eax
	addl	%eax, %eax
	cmpl	%ecx, %edx
	cvtsi2sdq	%rax, %xmm0
	leal	(%rcx,%rcx,2), %eax
	cvtsi2sdq	%rax, %xmm1
	addsd	.LC7(%rip), %xmm1
	movl	%ecx, %eax
	cmovnb	%rdx, %rax
	addsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L1
.L113:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-192(%rbp), %xmm1
	movsd	-184(%rbp), %xmm2
	jmp	.L114
.L21:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L22
.L7:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L8
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE93:
	.size	BrotliPopulationCostLiteral, .-BrotliPopulationCostLiteral
	.p2align 4
	.globl	BrotliPopulationCostCommand
	.hidden	BrotliPopulationCostCommand
	.type	BrotliPopulationCostCommand, @function
BrotliPopulationCostCommand:
.LFB94:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	2816(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L154
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L156:
	addq	$1, %rax
	cmpq	$704, %rax
	je	.L298
.L154:
	movl	(%r12,%rax,4), %r8d
	testl	%r8d, %r8d
	je	.L156
	movslq	%edx, %rcx
	addl	$1, %edx
	movq	%rax, -176(%rbp,%rcx,8)
	cmpl	$4, %edx
	jle	.L156
.L157:
	cmpl	$3, %edx
	je	.L299
	cmpl	$4, %edx
	je	.L300
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	$255, %rsi
	ja	.L171
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm5, %xmm5
	cvtss2sd	(%rax,%rsi,4), %xmm5
.L172:
	movsd	.LC5(%rip), %xmm4
	movl	$1, %r13d
	xorl	%ecx, %ecx
	pxor	%xmm3, %xmm3
	movabsq	$-9223372036854775808, %rbx
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L301:
	cvttsd2siq	%xmm0, %rax
.L183:
	mulsd	%xmm2, %xmm1
	cmpq	$15, %rax
	movl	$15, %edx
	movq	%r15, %rcx
	cmova	%rdx, %rax
	cmpq	%rax, %r13
	cmovb	%rax, %r13
	addl	$1, -128(%rbp,%rax,4)
	addsd	%xmm1, %xmm3
.L184:
	cmpq	$703, %rcx
	ja	.L185
.L191:
	leaq	(%r12,%rcx,4), %r14
	leaq	1(%rcx), %r15
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.L175
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	ja	.L178
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L179:
	movapd	%xmm5, %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC4(%rip), %xmm0
	addsd	%xmm1, %xmm0
	comisd	%xmm4, %xmm0
	jb	.L301
	subsd	%xmm4, %xmm0
	cvttsd2siq	%xmm0, %rax
	xorq	%rbx, %rax
	jmp	.L183
.L298:
	cmpl	$1, %edx
	jne	.L302
.L158:
	movsd	.LC1(%rip), %xmm0
.L153:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L171:
	.cfi_restore_state
	testq	%rsi, %rsi
	js	.L173
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L174:
	call	log2@PLT
	movapd	%xmm0, %xmm5
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L178:
	movapd	%xmm2, %xmm0
	movsd	%xmm5, -192(%rbp)
	movsd	%xmm3, -184(%rbp)
	call	log2@PLT
	movl	(%r14), %eax
	pxor	%xmm2, %xmm2
	movsd	-192(%rbp), %xmm5
	movsd	-184(%rbp), %xmm3
	cvtsi2sdq	%rax, %xmm2
	movq	.LC5(%rip), %rax
	movq	%rax, %xmm4
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L175:
	cmpq	$703, %r15
	ja	.L185
	movl	$704, %esi
	leaq	(%r12,%r15,4), %rdx
	movl	$1, %eax
	subl	%ecx, %esi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L188:
	addl	$1, %eax
	addq	$4, %rdx
	cmpl	%esi, %eax
	je	.L187
.L186:
	movl	(%rdx), %edi
	testl	%edi, %edi
	je	.L188
.L187:
	movl	%eax, %edx
	addq	%rdx, %rcx
	cmpq	$704, %rcx
	je	.L185
	cmpl	$2, %eax
	ja	.L189
	addl	%eax, -128(%rbp)
	cmpq	$703, %rcx
	jbe	.L191
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	18(%r13,%r13), %rax
	pxor	%xmm0, %xmm0
	movl	-128(%rbp), %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm0
	cvtsi2sdq	%rbx, %xmm1
	addsd	%xmm3, %xmm0
	movsd	%xmm0, -232(%rbp)
	cmpq	$255, %rbx
	jbe	.L194
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm1
.L195:
	mulsd	%xmm1, %xmm0
	movl	-124(%rbp), %eax
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	leaq	(%rax,%rbx), %rdi
	movq	%rdi, -240(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L198
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L199:
	mulsd	%xmm2, %xmm0
	movl	-120(%rbp), %r15d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r15, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r15
	jbe	.L202
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L203:
	mulsd	%xmm2, %xmm0
	movl	-116(%rbp), %ebx
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %rbx
	jbe	.L206
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L207:
	mulsd	%xmm2, %xmm0
	movl	-112(%rbp), %r14d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r14, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r14
	jbe	.L210
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L211:
	mulsd	%xmm2, %xmm0
	movl	-108(%rbp), %r13d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r13, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r13
	jbe	.L214
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L215:
	mulsd	%xmm2, %xmm0
	movl	-104(%rbp), %r12d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r12, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r12
	jbe	.L218
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L219:
	mulsd	%xmm2, %xmm0
	movl	-100(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -200(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L222
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L223:
	mulsd	%xmm2, %xmm0
	movl	-96(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -192(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L226
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -208(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-208(%rbp), %xmm1
.L227:
	mulsd	%xmm2, %xmm0
	movl	-92(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -184(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L230
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -216(%rbp)
	movsd	%xmm2, -208(%rbp)
	call	log2@PLT
	movsd	-208(%rbp), %xmm2
	movsd	-216(%rbp), %xmm1
.L231:
	mulsd	%xmm0, %xmm2
	movl	-88(%rbp), %eax
	movq	%rax, -208(%rbp)
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	jbe	.L234
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -224(%rbp)
	movsd	%xmm2, -216(%rbp)
	call	log2@PLT
	movsd	-216(%rbp), %xmm2
	movsd	-224(%rbp), %xmm1
.L235:
	mulsd	%xmm2, %xmm0
	movl	-84(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -216(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L238
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -248(%rbp)
	movsd	%xmm2, -224(%rbp)
	call	log2@PLT
	movsd	-224(%rbp), %xmm2
	movsd	-248(%rbp), %xmm1
.L239:
	mulsd	%xmm0, %xmm2
	movl	-80(%rbp), %eax
	movq	%rax, -224(%rbp)
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	jbe	.L242
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -256(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movsd	-256(%rbp), %xmm1
.L243:
	mulsd	%xmm0, %xmm2
	movl	-76(%rbp), %edi
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdi, %xmm2
	cmpq	$255, %rdi
	jbe	.L246
	movapd	%xmm2, %xmm0
	movq	%rdi, -264(%rbp)
	movsd	%xmm1, -256(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movsd	-256(%rbp), %xmm1
	movq	-264(%rbp), %rdi
.L247:
	mulsd	%xmm0, %xmm2
	movl	-72(%rbp), %esi
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rsi, %xmm2
	cmpq	$255, %rsi
	jbe	.L250
	movapd	%xmm2, %xmm0
	movq	%rsi, -272(%rbp)
	movq	%rdi, -256(%rbp)
	movsd	%xmm1, -264(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movq	-256(%rbp), %rdi
	movsd	-264(%rbp), %xmm1
	movq	-272(%rbp), %rsi
.L251:
	mulsd	%xmm0, %xmm2
	movl	-68(%rbp), %ecx
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
	cmpq	$255, %rcx
	jbe	.L254
	movapd	%xmm2, %xmm0
	movq	%rcx, -280(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	movsd	%xmm1, -272(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movq	-256(%rbp), %rdi
	movq	-264(%rbp), %rsi
	movsd	-272(%rbp), %xmm1
	movq	-280(%rbp), %rcx
.L255:
	mulsd	%xmm0, %xmm2
	movl	-64(%rbp), %edx
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	cmpq	$255, %rdx
	ja	.L258
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L259:
	mulsd	%xmm0, %xmm2
	addq	%r15, %rbx
	addq	-240(%rbp), %rbx
	movl	-60(%rbp), %eax
	addq	%r14, %rbx
	addq	%r13, %rbx
	addq	%r12, %rbx
	addq	-200(%rbp), %rbx
	addq	-192(%rbp), %rbx
	addq	-184(%rbp), %rbx
	addq	-208(%rbp), %rbx
	addq	-216(%rbp), %rbx
	addq	-224(%rbp), %rbx
	subsd	%xmm2, %xmm1
	addq	%rdi, %rbx
	pxor	%xmm2, %xmm2
	addq	%rsi, %rbx
	cvtsi2sdq	%rax, %xmm2
	addq	%rcx, %rbx
	addq	%rdx, %rbx
	addq	%rax, %rbx
	cmpq	$255, %rax
	ja	.L262
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L263:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
	testq	%rbx, %rbx
	je	.L264
	cmpq	$255, %rbx
	ja	.L265
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
.L266:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
.L264:
	maxsd	%xmm1, %xmm2
	movsd	-232(%rbp), %xmm0
	addsd	%xmm2, %xmm0
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L189:
	movl	-60(%rbp), %edi
	subl	$2, %eax
	leal	1(%rdi), %edx
	.p2align 4,,10
	.p2align 3
.L190:
	movl	%edx, %esi
	addl	$1, %edx
	shrl	$3, %eax
	addsd	.LC6(%rip), %xmm3
	jne	.L190
	movl	%esi, -60(%rbp)
	jmp	.L184
.L302:
	cmpl	$2, %edx
	jne	.L157
	testq	%rsi, %rsi
	js	.L159
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L160:
	addsd	.LC2(%rip), %xmm0
	jmp	.L153
.L194:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
	jmp	.L195
.L254:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rcx,4), %xmm0
	jmp	.L255
.L250:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rsi,4), %xmm0
	jmp	.L251
.L246:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L247
.L242:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L243
.L238:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L239
.L234:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L235
.L230:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L231
.L226:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L227
.L222:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L223
.L218:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
	jmp	.L219
.L214:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r13,4), %xmm0
	jmp	.L215
.L210:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r14,4), %xmm0
	jmp	.L211
.L206:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
	jmp	.L207
.L202:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r15,4), %xmm0
	jmp	.L203
.L198:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L199
.L258:
	movapd	%xmm2, %xmm0
	movq	%rcx, -280(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%rdi, -264(%rbp)
	movq	%rdx, -256(%rbp)
	movsd	%xmm1, -288(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-288(%rbp), %xmm1
	movq	-280(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdi
	movq	-256(%rbp), %rdx
	movsd	-248(%rbp), %xmm2
	jmp	.L259
.L262:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-192(%rbp), %xmm1
	movsd	-184(%rbp), %xmm2
	jmp	.L263
.L299:
	movq	-176(%rbp), %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	(%r12,%rax,4), %edx
	movq	-168(%rbp), %rax
	movl	(%r12,%rax,4), %esi
	movq	-160(%rbp), %rax
	movl	(%r12,%rax,4), %ecx
	leal	(%rdx,%rsi), %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	cmpl	%esi, %edx
	cmovb	%esi, %edx
	cvtsi2sdq	%rax, %xmm0
	addsd	.LC3(%rip), %xmm0
	cmpl	%ecx, %edx
	cmovb	%ecx, %edx
	movl	%edx, %eax
	cvtsi2sdq	%rax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L153
.L300:
	movq	-152(%rbp), %rax
	xorl	%edi, %edi
	movl	$1, %esi
	movd	(%r12,%rax,4), %xmm1
	movq	-160(%rbp), %rax
	movd	(%r12,%rax,4), %xmm6
	movq	-168(%rbp), %rax
	movd	(%r12,%rax,4), %xmm0
	movq	-176(%rbp), %rax
	punpckldq	%xmm1, %xmm6
	movd	(%r12,%rax,4), %xmm7
	movl	$1, %eax
	punpckldq	%xmm0, %xmm7
	movdqa	%xmm7, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movd	%xmm0, %ecx
	movaps	%xmm0, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L167:
	movl	-128(%rbp,%rax,4), %edx
	cmpl	%ecx, %edx
	jbe	.L168
	movl	%ecx, -128(%rbp,%rax,4)
	movl	%edx, %ecx
	movl	%edx, -128(%rbp,%rdi,4)
.L168:
	addq	$1, %rax
	cmpq	$4, %rax
	jne	.L167
	leaq	1(%rsi), %rax
	cmpq	$3, %rsi
	je	.L275
	movl	-128(%rbp,%rsi,4), %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L167
.L275:
	movl	-116(%rbp), %eax
	addl	-120(%rbp), %eax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	%eax, %ecx
	movl	-128(%rbp), %edx
	movl	-124(%rbp), %eax
	addl	%edx, %eax
	addl	%eax, %eax
	cmpl	%ecx, %edx
	cvtsi2sdq	%rax, %xmm0
	leal	(%rcx,%rcx,2), %eax
	cvtsi2sdq	%rax, %xmm1
	addsd	.LC7(%rip), %xmm1
	movl	%ecx, %eax
	cmovnb	%rdx, %rax
	addsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L153
.L265:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-192(%rbp), %xmm1
	movsd	-184(%rbp), %xmm2
	jmp	.L266
.L173:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L174
.L159:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L160
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE94:
	.size	BrotliPopulationCostCommand, .-BrotliPopulationCostCommand
	.p2align 4
	.globl	BrotliPopulationCostDistance
	.hidden	BrotliPopulationCostDistance
	.type	BrotliPopulationCostDistance, @function
BrotliPopulationCostDistance:
.LFB95:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	2176(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L305
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L307:
	addq	$1, %rax
	cmpq	$544, %rax
	je	.L449
.L305:
	movl	(%r12,%rax,4), %r8d
	testl	%r8d, %r8d
	je	.L307
	movslq	%edx, %rcx
	addl	$1, %edx
	movq	%rax, -176(%rbp,%rcx,8)
	cmpl	$4, %edx
	jle	.L307
.L308:
	cmpl	$3, %edx
	je	.L450
	cmpl	$4, %edx
	je	.L451
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	$255, %rsi
	ja	.L322
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm5, %xmm5
	cvtss2sd	(%rax,%rsi,4), %xmm5
.L323:
	movsd	.LC5(%rip), %xmm4
	movl	$1, %r13d
	xorl	%ecx, %ecx
	pxor	%xmm3, %xmm3
	movabsq	$-9223372036854775808, %rbx
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L452:
	cvttsd2siq	%xmm0, %rax
.L334:
	mulsd	%xmm2, %xmm1
	cmpq	$15, %rax
	movl	$15, %edx
	movq	%r15, %rcx
	cmova	%rdx, %rax
	cmpq	%rax, %r13
	cmovb	%rax, %r13
	addl	$1, -128(%rbp,%rax,4)
	addsd	%xmm1, %xmm3
.L335:
	cmpq	$543, %rcx
	ja	.L336
.L342:
	leaq	(%r12,%rcx,4), %r14
	leaq	1(%rcx), %r15
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.L326
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	ja	.L329
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L330:
	movapd	%xmm5, %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC4(%rip), %xmm0
	addsd	%xmm1, %xmm0
	comisd	%xmm4, %xmm0
	jb	.L452
	subsd	%xmm4, %xmm0
	cvttsd2siq	%xmm0, %rax
	xorq	%rbx, %rax
	jmp	.L334
.L449:
	cmpl	$1, %edx
	jne	.L453
.L309:
	movsd	.LC1(%rip), %xmm0
.L304:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L322:
	.cfi_restore_state
	testq	%rsi, %rsi
	js	.L324
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L325:
	call	log2@PLT
	movapd	%xmm0, %xmm5
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L329:
	movapd	%xmm2, %xmm0
	movsd	%xmm5, -192(%rbp)
	movsd	%xmm3, -184(%rbp)
	call	log2@PLT
	movl	(%r14), %eax
	pxor	%xmm2, %xmm2
	movsd	-192(%rbp), %xmm5
	movsd	-184(%rbp), %xmm3
	cvtsi2sdq	%rax, %xmm2
	movq	.LC5(%rip), %rax
	movq	%rax, %xmm4
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L326:
	cmpq	$543, %r15
	ja	.L336
	movl	$544, %esi
	leaq	(%r12,%r15,4), %rdx
	movl	$1, %eax
	subl	%ecx, %esi
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L339:
	addl	$1, %eax
	addq	$4, %rdx
	cmpl	%esi, %eax
	je	.L338
.L337:
	movl	(%rdx), %edi
	testl	%edi, %edi
	je	.L339
.L338:
	movl	%eax, %edx
	addq	%rdx, %rcx
	cmpq	$544, %rcx
	je	.L336
	cmpl	$2, %eax
	ja	.L340
	addl	%eax, -128(%rbp)
	cmpq	$543, %rcx
	jbe	.L342
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	18(%r13,%r13), %rax
	pxor	%xmm0, %xmm0
	movl	-128(%rbp), %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm0
	cvtsi2sdq	%rbx, %xmm1
	addsd	%xmm3, %xmm0
	movsd	%xmm0, -232(%rbp)
	cmpq	$255, %rbx
	jbe	.L345
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm1
.L346:
	mulsd	%xmm1, %xmm0
	movl	-124(%rbp), %eax
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	leaq	(%rax,%rbx), %rdi
	movq	%rdi, -240(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L349
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L350:
	mulsd	%xmm2, %xmm0
	movl	-120(%rbp), %r15d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r15, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r15
	jbe	.L353
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L354:
	mulsd	%xmm2, %xmm0
	movl	-116(%rbp), %ebx
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %rbx
	jbe	.L357
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L358:
	mulsd	%xmm2, %xmm0
	movl	-112(%rbp), %r14d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r14, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r14
	jbe	.L361
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L362:
	mulsd	%xmm2, %xmm0
	movl	-108(%rbp), %r13d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r13, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r13
	jbe	.L365
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L366:
	mulsd	%xmm2, %xmm0
	movl	-104(%rbp), %r12d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r12, %xmm2
	subsd	%xmm0, %xmm1
	cmpq	$255, %r12
	jbe	.L369
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L370:
	mulsd	%xmm2, %xmm0
	movl	-100(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -200(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L373
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-192(%rbp), %xmm1
.L374:
	mulsd	%xmm2, %xmm0
	movl	-96(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -192(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L377
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -208(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-184(%rbp), %xmm2
	movsd	-208(%rbp), %xmm1
.L378:
	mulsd	%xmm2, %xmm0
	movl	-92(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -184(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L381
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -216(%rbp)
	movsd	%xmm2, -208(%rbp)
	call	log2@PLT
	movsd	-208(%rbp), %xmm2
	movsd	-216(%rbp), %xmm1
.L382:
	mulsd	%xmm0, %xmm2
	movl	-88(%rbp), %eax
	movq	%rax, -208(%rbp)
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	jbe	.L385
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -224(%rbp)
	movsd	%xmm2, -216(%rbp)
	call	log2@PLT
	movsd	-216(%rbp), %xmm2
	movsd	-224(%rbp), %xmm1
.L386:
	mulsd	%xmm2, %xmm0
	movl	-84(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	movq	%rax, -216(%rbp)
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L389
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -248(%rbp)
	movsd	%xmm2, -224(%rbp)
	call	log2@PLT
	movsd	-224(%rbp), %xmm2
	movsd	-248(%rbp), %xmm1
.L390:
	mulsd	%xmm0, %xmm2
	movl	-80(%rbp), %eax
	movq	%rax, -224(%rbp)
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	jbe	.L393
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -256(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movsd	-256(%rbp), %xmm1
.L394:
	mulsd	%xmm0, %xmm2
	movl	-76(%rbp), %edi
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdi, %xmm2
	cmpq	$255, %rdi
	jbe	.L397
	movapd	%xmm2, %xmm0
	movq	%rdi, -264(%rbp)
	movsd	%xmm1, -256(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movsd	-256(%rbp), %xmm1
	movq	-264(%rbp), %rdi
.L398:
	mulsd	%xmm0, %xmm2
	movl	-72(%rbp), %esi
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rsi, %xmm2
	cmpq	$255, %rsi
	jbe	.L401
	movapd	%xmm2, %xmm0
	movq	%rsi, -272(%rbp)
	movq	%rdi, -256(%rbp)
	movsd	%xmm1, -264(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movq	-256(%rbp), %rdi
	movsd	-264(%rbp), %xmm1
	movq	-272(%rbp), %rsi
.L402:
	mulsd	%xmm0, %xmm2
	movl	-68(%rbp), %ecx
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
	cmpq	$255, %rcx
	jbe	.L405
	movapd	%xmm2, %xmm0
	movq	%rcx, -280(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	movsd	%xmm1, -272(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-248(%rbp), %xmm2
	movq	-256(%rbp), %rdi
	movq	-264(%rbp), %rsi
	movsd	-272(%rbp), %xmm1
	movq	-280(%rbp), %rcx
.L406:
	mulsd	%xmm0, %xmm2
	movl	-64(%rbp), %edx
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	cmpq	$255, %rdx
	ja	.L409
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L410:
	mulsd	%xmm0, %xmm2
	addq	%r15, %rbx
	addq	-240(%rbp), %rbx
	movl	-60(%rbp), %eax
	addq	%r14, %rbx
	addq	%r13, %rbx
	addq	%r12, %rbx
	addq	-200(%rbp), %rbx
	addq	-192(%rbp), %rbx
	addq	-184(%rbp), %rbx
	addq	-208(%rbp), %rbx
	addq	-216(%rbp), %rbx
	addq	-224(%rbp), %rbx
	subsd	%xmm2, %xmm1
	addq	%rdi, %rbx
	pxor	%xmm2, %xmm2
	addq	%rsi, %rbx
	cvtsi2sdq	%rax, %xmm2
	addq	%rcx, %rbx
	addq	%rdx, %rbx
	addq	%rax, %rbx
	cmpq	$255, %rax
	ja	.L413
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L414:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
	testq	%rbx, %rbx
	je	.L415
	cmpq	$255, %rbx
	ja	.L416
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
.L417:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
.L415:
	maxsd	%xmm1, %xmm2
	movsd	-232(%rbp), %xmm0
	addsd	%xmm2, %xmm0
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L340:
	movl	-60(%rbp), %edi
	subl	$2, %eax
	leal	1(%rdi), %edx
	.p2align 4,,10
	.p2align 3
.L341:
	movl	%edx, %esi
	addl	$1, %edx
	shrl	$3, %eax
	addsd	.LC6(%rip), %xmm3
	jne	.L341
	movl	%esi, -60(%rbp)
	jmp	.L335
.L453:
	cmpl	$2, %edx
	jne	.L308
	testq	%rsi, %rsi
	js	.L310
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L311:
	addsd	.LC2(%rip), %xmm0
	jmp	.L304
.L345:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
	jmp	.L346
.L405:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rcx,4), %xmm0
	jmp	.L406
.L401:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rsi,4), %xmm0
	jmp	.L402
.L397:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L398
.L393:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L394
.L389:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L390
.L385:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L386
.L381:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L382
.L377:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L378
.L373:
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	leaq	kLog2Table(%rip), %rax
	cvtss2sd	(%rax,%rdi,4), %xmm0
	jmp	.L374
.L369:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
	jmp	.L370
.L365:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r13,4), %xmm0
	jmp	.L366
.L361:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r14,4), %xmm0
	jmp	.L362
.L357:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
	jmp	.L358
.L353:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r15,4), %xmm0
	jmp	.L354
.L349:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L350
.L409:
	movapd	%xmm2, %xmm0
	movq	%rcx, -280(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%rdi, -264(%rbp)
	movq	%rdx, -256(%rbp)
	movsd	%xmm1, -288(%rbp)
	movsd	%xmm2, -248(%rbp)
	call	log2@PLT
	movsd	-288(%rbp), %xmm1
	movq	-280(%rbp), %rcx
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdi
	movq	-256(%rbp), %rdx
	movsd	-248(%rbp), %xmm2
	jmp	.L410
.L413:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-192(%rbp), %xmm1
	movsd	-184(%rbp), %xmm2
	jmp	.L414
.L450:
	movq	-176(%rbp), %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	(%r12,%rax,4), %edx
	movq	-168(%rbp), %rax
	movl	(%r12,%rax,4), %esi
	movq	-160(%rbp), %rax
	movl	(%r12,%rax,4), %ecx
	leal	(%rdx,%rsi), %eax
	addl	%ecx, %eax
	addl	%eax, %eax
	cmpl	%esi, %edx
	cmovb	%esi, %edx
	cvtsi2sdq	%rax, %xmm0
	addsd	.LC3(%rip), %xmm0
	cmpl	%ecx, %edx
	cmovb	%ecx, %edx
	movl	%edx, %eax
	cvtsi2sdq	%rax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L304
.L451:
	movq	-152(%rbp), %rax
	xorl	%edi, %edi
	movl	$1, %esi
	movd	(%r12,%rax,4), %xmm1
	movq	-160(%rbp), %rax
	movd	(%r12,%rax,4), %xmm6
	movq	-168(%rbp), %rax
	movd	(%r12,%rax,4), %xmm0
	movq	-176(%rbp), %rax
	punpckldq	%xmm1, %xmm6
	movd	(%r12,%rax,4), %xmm7
	movl	$1, %eax
	punpckldq	%xmm0, %xmm7
	movdqa	%xmm7, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movd	%xmm0, %ecx
	movaps	%xmm0, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L318:
	movl	-128(%rbp,%rax,4), %edx
	cmpl	%ecx, %edx
	jbe	.L319
	movl	%ecx, -128(%rbp,%rax,4)
	movl	%edx, %ecx
	movl	%edx, -128(%rbp,%rdi,4)
.L319:
	addq	$1, %rax
	cmpq	$4, %rax
	jne	.L318
	leaq	1(%rsi), %rax
	cmpq	$3, %rsi
	je	.L426
	movl	-128(%rbp,%rsi,4), %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L318
.L426:
	movl	-116(%rbp), %eax
	addl	-120(%rbp), %eax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	%eax, %ecx
	movl	-128(%rbp), %edx
	movl	-124(%rbp), %eax
	addl	%edx, %eax
	addl	%eax, %eax
	cmpl	%ecx, %edx
	cvtsi2sdq	%rax, %xmm0
	leal	(%rcx,%rcx,2), %eax
	cvtsi2sdq	%rax, %xmm1
	addsd	.LC7(%rip), %xmm1
	movl	%ecx, %eax
	cmovnb	%rdx, %rax
	addsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L304
.L416:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -192(%rbp)
	movsd	%xmm2, -184(%rbp)
	call	log2@PLT
	movsd	-192(%rbp), %xmm1
	movsd	-184(%rbp), %xmm2
	jmp	.L417
.L324:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L325
.L310:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L311
.L454:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE95:
	.size	BrotliPopulationCostDistance, .-BrotliPopulationCostDistance
	.section	.rodata
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1076363264
	.align 8
.LC2:
	.long	0
	.long	1077149696
	.align 8
.LC3:
	.long	0
	.long	1077673984
	.align 8
.LC4:
	.long	0
	.long	1071644672
	.align 8
.LC5:
	.long	0
	.long	1138753536
	.align 8
.LC6:
	.long	0
	.long	1074266112
	.align 8
.LC7:
	.long	0
	.long	1078099968
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
