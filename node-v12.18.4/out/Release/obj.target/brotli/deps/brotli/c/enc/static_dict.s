	.file	"static_dict.c"
	.text
	.p2align 4
	.globl	BrotliFindAllStaticDictionaryMatches
	.hidden	BrotliFindAllStaticDictionaryMatches
	.type	BrotliFindAllStaticDictionaryMatches, @function
BrotliFindAllStaticDictionaryMatches:
.LFB52:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdi, -96(%rbp)
	movq	%rdx, -104(%rbp)
	imull	$506832829, (%rsi), %eax
	movq	32(%rdi), %rdi
	movq	%rcx, -56(%rbp)
	movq	%rdi, -112(%rbp)
	shrl	$17, %eax
	movzwl	(%rdi,%rax,2), %eax
	testq	%rax, %rax
	je	.L167
	movq	40(%rbx), %rdx
	movq	(%rbx), %rdi
	movl	$0, -80(%rbp)
	leaq	(%rdx,%rax,4), %rbx
	leaq	9(%rsi), %rax
	movq	%rdi, -64(%rbp)
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L83:
	movzbl	(%rbx), %r13d
	movq	-64(%rbp), %rax
	movzwl	2(%rbx), %r12d
	movzbl	1(%rbx), %edi
	movq	%r13, %rdx
	movl	%r13d, %r10d
	andl	$31, %edx
	movq	%r12, %rcx
	andl	$31, %r10d
	movzbl	(%rax,%rdx), %eax
	movl	%eax, -48(%rbp)
	testb	%dil, %dil
	je	.L391
	cmpq	%rdx, -56(%rbp)
	jb	.L15
	movq	-64(%rbp), %rsi
	movq	%rdx, %rax
	movzbl	%r10b, %r9d
	andl	$31, %r10d
	imulq	%r12, %rax
	movl	32(%rsi,%r10,4), %r10d
	addq	%rax, %r10
	addq	168(%rsi), %r10
	cmpb	$10, %dil
	je	.L52
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L53
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L394:
	xorl	$32, %ecx
	cmpb	%r8b, %cl
	jne	.L15
.L62:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L393
.L53:
	movzbl	(%r10,%rax), %ecx
	movzbl	(%r14,%rax), %r8d
	leal	-97(%rcx), %esi
	cmpb	$25, %sil
	jbe	.L394
	cmpb	%r8b, %cl
	je	.L62
	.p2align 4,,10
	.p2align 3
.L15:
	addq	$4, %rbx
	testb	%r13b, %r13b
	jns	.L83
.L2:
	cmpq	$4, -56(%rbp)
	jbe	.L1
	movzbl	(%r14), %eax
	movb	%al, -48(%rbp)
	cmpb	$32, %al
	je	.L230
	cmpb	$46, %al
	je	.L230
.L85:
	cmpq	$5, -56(%rbp)
	je	.L1
	movzbl	1(%r14), %edx
	movzbl	(%r14), %eax
	cmpb	$32, %dl
	je	.L395
	cmpb	$-62, %al
	jne	.L128
	cmpb	$-96, %dl
	je	.L127
.L128:
	cmpq	$8, -56(%rbp)
	jbe	.L1
	movzbl	(%r14), %eax
	cmpb	$32, %al
	je	.L396
	cmpb	$46, %al
	jne	.L1
	cmpb	$99, 1(%r14)
	jne	.L1
	cmpb	$111, 2(%r14)
	je	.L397
.L1:
	movl	-80(%rbp), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r11
	cmpq	%rdx, %rax
	movq	%rax, %rdi
	movq	%rdx, %rax
	movl	32(%r11,%rdx,4), %esi
	cmova	%rdx, %rdi
	imulq	%r12, %rax
	addq	%rax, %rsi
	addq	168(%r11), %rsi
	movq	%rdi, %r11
	shrq	$3, %r11
	je	.L168
	movq	(%r14), %rax
	movq	(%rsi), %r8
	movq	$0, -72(%rbp)
	cmpq	%rax, %r8
	je	.L398
.L5:
	xorq	%r8, %rax
	xorl	%r9d, %r9d
	rep bsfq	%rax, %r9
	movslq	%r9d, %r9
	shrq	$3, %r9
	addq	-72(%rbp), %r9
	movq	%r9, -72(%rbp)
.L7:
	cmpq	-72(%rbp), %rdx
	jne	.L10
	leaq	(%r15,%rdx,4), %rax
	sall	$5, %ecx
	movzbl	%r10b, %esi
	movl	$1, -80(%rbp)
	addl	%esi, %ecx
	movl	(%rax), %esi
	cmpl	%esi, %ecx
	cmova	%esi, %ecx
	movl	%ecx, (%rax)
.L10:
	leaq	-1(%rdx), %rax
	cmpq	-72(%rbp), %rax
	ja	.L11
	movzbl	-48(%rbp), %ecx
	leaq	(%r15,%rax,4), %rsi
	movl	$12, %eax
	movzbl	%r10b, %edi
	movl	$1, -80(%rbp)
	salq	%cl, %rax
	movl	(%rsi), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%edi, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rsi)
	leaq	2(%rdx), %rax
	cmpq	-56(%rbp), %rax
	jnb	.L11
	cmpb	$105, -1(%r14,%rdx)
	je	.L399
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-104(%rbp), %rax
	movq	%rax, %rsi
	cmpb	$9, %r10b
	jbe	.L12
	leaq	-9(%rdx), %rsi
	cmpq	%rax, %rsi
	cmovb	%rax, %rsi
.L12:
	movq	-72(%rbp), %rax
	leaq	-2(%rdx), %r11
	cmpq	%rax, %r11
	cmova	%rax, %r11
	cmpq	%r11, %rsi
	ja	.L13
	movq	-96(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rdx, -80(%rbp)
	movzbl	%r10b, %r9d
	subq	%rsi, %rdi
	movl	-48(%rbp), %edx
	movb	%r10b, -88(%rbp)
	movq	16(%rax), %rax
	leal	(%rdi,%rdi,2), %r8d
	salq	$2, %rdi
	addl	%r8d, %r8d
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r8d, %ecx
	movq	%r10, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	andl	$63, %eax
	addq	%rdi, %rax
	salq	%cl, %rax
	movl	(%r15,%rsi,4), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r9d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	subl	$6, %r8d
	subq	$4, %rdi
	movl	%eax, (%r15,%rsi,4)
	addq	$1, %rsi
	cmpq	%r11, %rsi
	jbe	.L14
	movq	-80(%rbp), %rdx
	movzbl	-88(%rbp), %r10d
	movl	$1, -80(%rbp)
.L13:
	cmpq	-72(%rbp), %rdx
	ja	.L15
	leaq	6(%rdx), %rax
	cmpq	-56(%rbp), %rax
	jnb	.L15
	leaq	(%r14,%rdx), %rax
	movzbl	(%rax), %ecx
	cmpb	$32, %cl
	je	.L400
	cmpb	$34, %cl
	je	.L401
	cmpb	$46, %cl
	je	.L402
	cmpb	$44, %cl
	je	.L403
	cmpb	$10, %cl
	je	.L404
	cmpb	$93, %cl
	je	.L405
	cmpb	$39, %cl
	je	.L406
	cmpb	$58, %cl
	je	.L407
	cmpb	$40, %cl
	je	.L408
	cmpb	$61, %cl
	je	.L409
	cmpb	$97, %cl
	je	.L410
	cmpb	$101, %cl
	je	.L411
	cmpb	$102, %cl
	je	.L412
	cmpb	$105, %cl
	je	.L413
	cmpb	$108, %cl
	je	.L414
	cmpb	$111, %cl
	jne	.L15
	cmpb	$117, 1(%rax)
	jne	.L15
	cmpb	$115, 2(%rax)
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$106, %eax
	leaq	16(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$44, %eax
.L54:
	movzbl	-48(%rbp), %ecx
	leaq	0(,%rdx,4), %rsi
	leaq	(%r15,%rsi), %r8
	salq	%cl, %rax
	movl	(%r8), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r9d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%r8)
	leaq	1(%rdx), %rax
	cmpq	-56(%rbp), %rax
	jnb	.L79
	addq	%r14, %rdx
	movzbl	(%rdx), %eax
	cmpb	$32, %al
	je	.L415
	cmpb	$34, %al
	je	.L416
	cmpb	$46, %al
	je	.L417
	cmpb	$44, %al
	je	.L418
	cmpb	$39, %al
	je	.L419
	cmpb	$40, %al
	je	.L420
	cmpb	$61, %al
	je	.L421
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$1, -80(%rbp)
	addq	$4, %rbx
	testb	%r13b, %r13b
	jns	.L83
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L52:
	movzbl	(%r10), %eax
	leal	-97(%rax), %ecx
	cmpb	$25, %cl
	ja	.L15
	xorl	$32, %eax
	cmpb	(%r14), %al
	jne	.L15
	leal	-1(%r9), %eax
	movq	%rax, %r11
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
	leaq	1(%r14), %rax
	shrq	$3, %r11
	je	.L55
	xorl	%ecx, %ecx
.L57:
	movq	1(%r14,%rcx), %rax
	movq	1(%r10,%rcx), %r8
	cmpq	%r8, %rax
	je	.L422
	xorq	%r8, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %rcx
.L58:
	cmpq	%rcx, -72(%rbp)
	jne	.L15
	movl	$9, %eax
	jmp	.L54
.L401:
	movzbl	-48(%rbp), %ecx
	leaq	4(,%rdx,4), %rdi
	movzbl	%r10b, %r10d
	movl	$19, %edx
	leaq	(%r15,%rdi), %rsi
	salq	%cl, %rdx
	movl	(%rsi), %ecx
	addq	%r12, %rdx
	sall	$5, %edx
	addl	%r10d, %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, (%rsi)
	cmpb	$62, 1(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$21, %eax
	leaq	4(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L417:
	leaq	4(%r15,%rsi), %rax
	movl	(%rax), %r8d
	cmpb	$10, %dil
	je	.L423
	movzbl	-48(%rbp), %ecx
	movl	$101, %edi
	salq	%cl, %rdi
	leaq	(%rdi,%r12), %rcx
	sall	$5, %ecx
	addl	%r9d, %ecx
	cmpl	%r8d, %ecx
	cmova	%r8d, %ecx
	movl	%ecx, (%rax)
	cmpb	$32, 1(%rdx)
	jne	.L79
	addq	$8, %rsi
	movl	$114, %eax
.L161:
	movzbl	-48(%rbp), %ecx
	leaq	(%r15,%rsi), %rdx
	movl	$1, -80(%rbp)
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r9d
	movl	(%rdx), %eax
	cmpl	%eax, %r9d
	cmova	%eax, %r9d
	movl	%r9d, (%rdx)
	jmp	.L15
.L230:
	imull	$506832829, 1(%r14), %eax
	movq	-112(%rbp), %rbx
	shrl	$17, %eax
	movzwl	(%rbx,%rax,2), %eax
	testq	%rax, %rax
	je	.L85
	movzbl	-48(%rbp), %ebx
	movl	$6, %ecx
	movq	-96(%rbp), %rdi
	movq	%r15, -64(%rbp)
	movl	$32, %edx
	cmpb	$32, %bl
	movq	(%rdi), %r12
	cmove	%rcx, %rdx
	movl	$89, %ecx
	movq	%rdx, -88(%rbp)
	movl	$67, %edx
	cmove	%rcx, %rdx
	movl	$2, %ecx
	movq	%rdx, -120(%rbp)
	movl	$77, %edx
	cmove	%rcx, %rdx
	movq	%rdx, -104(%rbp)
	movq	40(%rdi), %rdx
	leaq	(%rdx,%rax,4), %r13
	movq	-56(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -72(%rbp)
	leaq	10(%r14), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L125:
	movzbl	0(%r13), %edi
	movzbl	1(%r13), %r10d
	movzwl	2(%r13), %r11d
	movq	%rdi, %rax
	movl	%edi, %esi
	andl	$31, %eax
	andl	$31, %esi
	movzbl	(%r12,%rax), %ecx
	testb	%r10b, %r10b
	jne	.L87
	cmpq	%rax, -72(%rbp)
	jb	.L88
	movq	%rax, %r8
	movzbl	%sil, %r15d
	leaq	1(%r14), %rdx
	andl	$31, %esi
	imulq	%r11, %r8
	movl	32(%r12,%rsi,4), %esi
	addq	%r8, %rsi
	movq	%rax, %r8
	addq	168(%r12), %rsi
	shrq	$3, %r8
	je	.L89
	movq	1(%r14), %rdx
	movq	(%rsi), %r9
	xorl	%r10d, %r10d
	cmpq	%rdx, %r9
	je	.L424
.L90:
	xorq	%r9, %rdx
	xorl	%r8d, %r8d
	rep bsfq	%rdx, %r8
	movslq	%r8d, %r8
	shrq	$3, %r8
	addq	%r10, %r8
.L92:
	cmpq	%r8, %rax
	jne	.L88
	movq	-88(%rbp), %rdx
	leaq	1(%rax), %rsi
	movq	-64(%rbp), %rbx
	leaq	0(,%rsi,4), %r9
	salq	%cl, %rdx
	leaq	(%rbx,%r9), %r8
	addq	%r11, %rdx
	movl	(%r8), %r10d
	sall	$5, %edx
	addl	%r15d, %edx
	cmpl	%r10d, %edx
	cmova	%r10d, %edx
	addq	$2, %rax
	movl	%edx, (%r8)
	cmpq	-56(%rbp), %rax
	jnb	.L386
	addq	%r14, %rsi
	movzbl	(%rsi), %eax
	cmpb	$32, %al
	je	.L425
	cmpb	$40, %al
	je	.L426
	cmpb	$32, -48(%rbp)
	je	.L427
.L386:
	movl	$1, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$4, %r13
	testb	%dil, %dil
	jns	.L125
	movq	-64(%rbp), %r15
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L87:
	cmpb	$32, -48(%rbp)
	jne	.L88
	cmpq	%rax, -72(%rbp)
	jb	.L88
	movzbl	%sil, %r15d
	andl	$31, %esi
	movq	168(%r12), %rbx
	movl	32(%r12,%rsi,4), %edx
	movq	%rax, %rsi
	imulq	%r11, %rsi
	addq	%rsi, %rdx
	addq	%rdx, %rbx
	cmpb	$10, %r10b
	je	.L102
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L103
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L428:
	xorl	$32, %esi
	cmpb	%r8b, %sil
	jne	.L88
.L113:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L104
.L103:
	movzbl	(%rbx,%rdx), %esi
	movzbl	1(%r14,%rdx), %r8d
	leal	-97(%rsi), %r9d
	cmpb	$25, %r9b
	jbe	.L428
	cmpb	%r8b, %sil
	je	.L113
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L415:
	cmpb	$10, %dil
	movl	$68, %edx
	movl	$4, %eax
	movzbl	-48(%rbp), %ecx
	cmovne	%rdx, %rax
	leaq	4(%r15,%rsi), %rdx
	movl	$1, -80(%rbp)
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r9d
	movl	(%rdx), %eax
	cmpl	%eax, %r9d
	cmova	%eax, %r9d
	addq	$4, %rbx
	movl	%r9d, (%rdx)
	testb	%r13b, %r13b
	jns	.L83
	jmp	.L2
.L400:
	movzbl	-48(%rbp), %ecx
	leaq	4(,%rdx,4), %rdi
	movzbl	%r10b, %r10d
	movl	$1, %edx
	leaq	(%r15,%rdi), %rsi
	salq	%cl, %rdx
	movl	(%rsi), %ecx
	addq	%r12, %rdx
	sall	$5, %edx
	addl	%r10d, %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, (%rsi)
	movzbl	1(%rax), %edx
	cmpb	$97, %dl
	je	.L429
	cmpb	$98, %dl
	je	.L430
	cmpb	$105, %dl
	je	.L431
	cmpb	$102, %dl
	je	.L432
	cmpb	$111, %dl
	je	.L433
	cmpb	$110, %dl
	je	.L434
	cmpb	$116, %dl
	je	.L435
	cmpb	$119, %dl
	jne	.L15
	cmpb	$105, 2(%rax)
	jne	.L15
	cmpb	$116, 3(%rax)
	jne	.L15
	cmpb	$104, 4(%rax)
	jne	.L15
	cmpb	$32, 5(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$35, %eax
	leaq	20(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	4(%r15,%rsi), %rax
	movl	(%rax), %r8d
	cmpb	$10, %dil
	je	.L436
	movzbl	-48(%rbp), %ecx
	movl	$87, %edi
	salq	%cl, %rdi
	leaq	(%rdi,%r12), %rcx
	sall	$5, %ecx
	addl	%r9d, %ecx
	cmpl	%r8d, %ecx
	cmova	%r8d, %ecx
	movl	%ecx, (%rax)
	cmpb	$62, 1(%rdx)
	jne	.L79
	addq	$8, %rsi
	movl	$97, %edx
.L158:
	movzbl	-48(%rbp), %ecx
	leaq	(%r15,%rsi), %rax
	movl	$1, -80(%rbp)
	salq	%cl, %rdx
	addq	%r12, %rdx
	sall	$5, %edx
	addl	%edx, %r9d
	movl	(%rax), %edx
	cmpl	%edx, %r9d
	cmova	%edx, %r9d
	addq	$4, %rbx
	movl	%r9d, (%rax)
	testb	%r13b, %r13b
	jns	.L83
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r14, %rax
.L4:
	andl	$7, %edi
	je	.L174
	movzbl	(%rsi,%r11), %r9d
	cmpb	%r9b, (%rax)
	jne	.L174
	leaq	1(%r11), %r9
	movq	%r9, -72(%rbp)
	cmpq	$1, %rdi
	je	.L7
	movzbl	1(%rsi,%r11), %r9d
	cmpb	%r9b, 1(%rax)
	jne	.L7
	leaq	2(%r11), %r9
	movq	%r9, -72(%rbp)
	cmpq	$2, %rdi
	je	.L7
	movzbl	2(%rsi,%r11), %r9d
	cmpb	%r9b, 2(%rax)
	jne	.L7
	leaq	3(%r11), %r9
	movq	%r9, -72(%rbp)
	cmpq	$3, %rdi
	je	.L7
	movzbl	3(%rsi,%r11), %r9d
	cmpb	%r9b, 3(%rax)
	jne	.L7
	leaq	4(%r11), %r9
	movq	%r9, -72(%rbp)
	cmpq	$4, %rdi
	je	.L7
	movzbl	4(%rsi,%r11), %r9d
	cmpb	%r9b, 4(%rax)
	jne	.L7
	leaq	5(%r11), %r9
	movq	%r9, -72(%rbp)
	subq	$5, %rdi
	je	.L7
	movzbl	5(%rsi,%r11), %r9d
	cmpb	%r9b, 5(%rax)
	jne	.L7
	leaq	6(%r11), %r9
	movq	%r9, -72(%rbp)
	cmpq	$1, %rdi
	je	.L7
	movzbl	6(%rsi,%r11), %edi
	cmpb	%dil, 6(%rax)
	jne	.L7
	leaq	7(%r11), %rax
	movq	%rax, -72(%rbp)
	jmp	.L7
.L399:
	cmpb	$110, (%r14,%rdx)
	jne	.L11
	cmpb	$103, 1(%r14,%rdx)
	jne	.L11
	cmpb	$32, 2(%r14,%rdx)
	jne	.L11
	movzbl	-48(%rbp), %ecx
	movl	$49, %eax
	leaq	12(%r15,%rdx,4), %rsi
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %edi
	movl	(%rsi), %eax
	cmpl	%eax, %edi
	cmova	%eax, %edi
	movl	%edi, (%rsi)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L425:
	movq	-104(%rbp), %rdx
	leaq	4(%rbx,%r9), %rax
.L387:
	salq	%cl, %rdx
	movl	(%rax), %ecx
	addq	%r11, %rdx
	sall	$5, %edx
	addl	%r15d, %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, (%rax)
	movl	$1, -80(%rbp)
	jmp	.L88
.L403:
	movzbl	-48(%rbp), %ecx
	leaq	4(,%rdx,4), %rdi
	movzbl	%r10b, %r10d
	movl	$76, %edx
	leaq	(%r15,%rdi), %rsi
	salq	%cl, %rdx
	movl	(%rsi), %ecx
	addq	%r12, %rdx
	sall	$5, %edx
	addl	%r10d, %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, (%rsi)
	cmpb	$32, 1(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$14, %eax
	leaq	4(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L419:
	cmpb	$10, %dil
	movl	$74, %eax
	movl	$94, %edi
	movzbl	-48(%rbp), %ecx
	cmovne	%rdi, %rax
	leaq	4(%r15,%rsi), %rdx
	movl	$1, -80(%rbp)
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r9d
	movl	(%rdx), %eax
	cmpl	%eax, %r9d
	cmova	%eax, %r9d
	movl	%r9d, (%rdx)
	jmp	.L15
.L423:
	movzbl	-48(%rbp), %ecx
	movl	$79, %edi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	addq	%r12, %rcx
	sall	$5, %ecx
	addl	%r9d, %ecx
	cmpl	%r8d, %ecx
	cmova	%r8d, %ecx
	movl	%ecx, (%rax)
	cmpb	$32, 1(%rdx)
	jne	.L79
	addq	$8, %rsi
	movl	$88, %eax
	jmp	.L161
.L430:
	cmpb	$121, 2(%rax)
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$38, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L395:
	cmpb	$101, %al
	sete	%cl
	cmpb	$115, %al
	sete	%dl
	orb	%dl, %cl
	jne	.L127
	cmpb	$44, %al
	jne	.L128
.L127:
	imull	$506832829, 2(%r14), %eax
	movq	-112(%rbp), %rbx
	shrl	$17, %eax
	movzwl	(%rbx,%rax,2), %eax
	testq	%rax, %rax
	je	.L128
	movq	-96(%rbp), %rbx
	movq	40(%rbx), %rdx
	movq	(%rbx), %r12
	movq	-56(%rbp), %rbx
	leaq	(%rdx,%rax,4), %rax
	leaq	-2(%rbx), %r13
	leaq	2(%r14), %rbx
	movq	%rbx, -48(%rbp)
	leaq	10(%r14), %rbx
	movq	%rbx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L138:
	cmpb	$0, 1(%rax)
	movzbl	(%rax), %esi
	jne	.L129
	movq	%rsi, %rdi
	movl	%esi, %edx
	andl	$31, %edi
	andl	$31, %edx
	cmpq	%rdi, %r13
	jb	.L129
	movzwl	2(%rax), %r9d
	movzbl	%dl, %r10d
	andl	$31, %edx
	movq	%rdi, %r8
	movl	32(%r12,%rdx,4), %ecx
	movq	%rdi, %rdx
	movq	-48(%rbp), %r11
	imulq	%r9, %rdx
	addq	%rdx, %rcx
	addq	168(%r12), %rcx
	shrq	$3, %r8
	je	.L130
	movq	2(%r14), %r11
	movq	(%rcx), %rdx
	xorl	%ebx, %ebx
	cmpq	%r11, %rdx
	je	.L437
.L131:
	xorq	%r11, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rbx, %rdx
.L133:
	cmpq	%rdx, %rdi
	jne	.L129
	movzbl	(%r14), %r8d
	movzbl	(%r12,%rdi), %ecx
	leaq	2(%rdi), %rdx
	cmpb	$-62, %r8b
	je	.L438
	cmpq	%rdx, -56(%rbp)
	jbe	.L129
	cmpb	$32, 2(%r14,%rdi)
	je	.L439
	.p2align 4,,10
	.p2align 3
.L129:
	addq	$4, %rax
	testb	%sil, %sil
	jns	.L138
	jmp	.L128
.L398:
	leaq	8(%r14), %rax
	subq	$1, %r11
	je	.L170
	movq	8(%r14), %rax
	movq	8(%rsi), %r8
	movq	$8, -72(%rbp)
	cmpq	%rax, %r8
	jne	.L5
	leaq	16(%r14), %rax
	cmpq	$1, %r11
	je	.L172
	movq	16(%r14), %rax
	movq	16(%rsi), %r8
	movq	$16, -72(%rbp)
	cmpq	%rax, %r8
	jne	.L5
	leaq	24(%r14), %rax
	movl	$24, %r11d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L396:
	cmpb	$116, 1(%r14)
	jne	.L1
	cmpb	$104, 2(%r14)
	jne	.L1
	cmpb	$101, 3(%r14)
	jne	.L1
	cmpb	$32, 4(%r14)
	jne	.L1
.L140:
	imull	$506832829, 5(%r14), %eax
	movq	-112(%rbp), %rbx
	shrl	$17, %eax
	movzwl	(%rbx,%rax,2), %eax
	testq	%rax, %rax
	je	.L1
	movq	-96(%rbp), %rbx
	leaq	13(%r14), %r12
	movq	%r12, -72(%rbp)
	movq	40(%rbx), %rdx
	movq	(%rbx), %r13
	leaq	(%rdx,%rax,4), %r8
	movq	-56(%rbp), %rax
	subq	$5, %rax
	movq	%rax, -48(%rbp)
	leaq	5(%r14), %rax
	movq	%rax, -64(%rbp)
.L151:
	cmpb	$0, 1(%r8)
	movzbl	(%r8), %edi
	jne	.L141
	movl	%edi, %eax
	movq	%rdi, %rsi
	andl	$31, %eax
	andl	$31, %esi
	cmpq	%rsi, -48(%rbp)
	jb	.L141
	movzwl	2(%r8), %r9d
	movzbl	%al, %r10d
	andl	$31, %eax
	movq	-64(%rbp), %rcx
	movl	32(%r13,%rax,4), %edx
	movq	%rsi, %rax
	imulq	%r9, %rax
	addq	%rax, %rdx
	movq	%rsi, %rax
	addq	168(%r13), %rdx
	shrq	$3, %rax
	je	.L142
	movq	5(%r14), %r11
	movq	(%rdx), %rcx
	xorl	%ebx, %ebx
	cmpq	%r11, %rcx
	je	.L440
.L143:
	movq	%rcx, %rax
	xorq	%r11, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rbx, %rax
.L145:
	cmpq	%rax, %rsi
	jne	.L141
	cmpb	$32, (%r14)
	movl	$41, %ebx
	movl	$72, %edx
	movzbl	0(%r13,%rsi), %ecx
	cmove	%rbx, %rdx
	leaq	5(%rsi), %rax
	leaq	0(,%rax,4), %r11
	salq	%cl, %rdx
	leaq	(%r15,%r11), %rbx
	addq	%r9, %rdx
	sall	$5, %edx
	addl	%r10d, %edx
	cmpl	(%rbx), %edx
	cmova	(%rbx), %edx
	movl	%edx, (%rbx)
	cmpq	-56(%rbp), %rax
	jnb	.L389
	cmpb	$32, (%r14)
	je	.L441
.L389:
	movl	$1, -80(%rbp)
.L141:
	addq	$4, %r8
	testb	%dil, %dil
	jns	.L151
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L402:
	movzbl	-48(%rbp), %ecx
	leaq	4(,%rdx,4), %rdi
	movzbl	%r10b, %r10d
	movl	$20, %edx
	leaq	(%r15,%rdi), %rsi
	salq	%cl, %rdx
	movl	(%rsi), %ecx
	addq	%r12, %rdx
	sall	$5, %edx
	addl	%r10d, %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, (%rsi)
	cmpb	$32, 1(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$31, %edx
	leaq	4(%r15,%rdi), %rsi
	salq	%cl, %rdx
	movl	(%rsi), %ecx
	addq	%r12, %rdx
	sall	$5, %edx
	addl	%r10d, %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, (%rsi)
	cmpb	$84, 2(%rax)
	jne	.L15
	cmpb	$104, 3(%rax)
	jne	.L15
	movzbl	4(%rax), %edx
	cmpb	$101, %dl
	je	.L442
	cmpb	$105, %dl
	jne	.L15
	cmpb	$115, 5(%rax)
	jne	.L15
	cmpb	$32, 6(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$75, %eax
	leaq	24(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	4(%r15,%rsi), %rax
	movl	(%rax), %r8d
	cmpb	$10, %dil
	je	.L443
	movzbl	-48(%rbp), %ecx
	movl	$112, %edi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	addq	%r12, %rcx
	sall	$5, %ecx
	addl	%r9d, %ecx
	cmpl	%r8d, %ecx
	cmova	%r8d, %ecx
	movl	%ecx, (%rax)
	cmpb	$32, 1(%rdx)
	jne	.L79
	addq	$8, %rsi
	movl	$107, %eax
.L164:
	movzbl	-48(%rbp), %ecx
	leaq	(%r15,%rsi), %rdx
	movl	$1, -80(%rbp)
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r9d
	movl	(%rdx), %eax
	cmpl	%eax, %r9d
	cmova	%eax, %r9d
	movl	%r9d, (%rdx)
	jmp	.L15
.L392:
	xorl	%r9d, %r9d
	movl	$44, %eax
	jmp	.L54
.L167:
	movl	$0, -80(%rbp)
	jmp	.L2
.L429:
	movzbl	2(%rax), %edx
	cmpb	$32, %dl
	je	.L444
	cmpb	$115, %dl
	je	.L445
	cmpb	$116, %dl
	je	.L446
	cmpb	$110, %dl
	jne	.L15
	cmpb	$100, 3(%rax)
	jne	.L15
	cmpb	$32, 4(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$10, %eax
	leaq	16(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L436:
	movzbl	-48(%rbp), %ecx
	movl	$66, %edi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	addq	%r12, %rcx
	sall	$5, %ecx
	addl	%r9d, %ecx
	cmpl	%r8d, %ecx
	cmova	%r8d, %ecx
	movl	%ecx, (%rax)
	cmpb	$62, 1(%rdx)
	jne	.L79
	addq	$8, %rsi
	movl	$69, %edx
	jmp	.L158
.L104:
	leaq	1(%rax), %r9
	movl	$85, %edx
.L157:
	movq	-64(%rbp), %rbx
	salq	%cl, %rdx
	addq	%r11, %rdx
	leaq	(%rbx,%r9,4), %rsi
	sall	$5, %edx
	movl	(%rsi), %r8d
	addl	%r15d, %edx
	cmpl	%r8d, %edx
	cmova	%r8d, %edx
	leaq	2(%rax), %r8
	movl	%edx, (%rsi)
	cmpq	-56(%rbp), %r8
	jnb	.L386
	leaq	(%r14,%r9), %rdx
	movzbl	(%rdx), %esi
	cmpb	$32, %sil
	je	.L447
	cmpb	$44, %sil
	je	.L448
	cmpb	$46, %sil
	je	.L449
	cmpb	$61, %sil
	jne	.L386
	movzbl	1(%rdx), %edx
	cmpb	$34, %dl
	je	.L450
	cmpb	$39, %dl
	jne	.L386
	xorl	%edx, %edx
	cmpb	$10, %r10b
	sete	%dl
	addq	$119, %rdx
.L384:
	leaq	12(%rbx,%rax,4), %rax
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	9(%r14), %rdx
	subq	$1, %r8
	je	.L192
	movq	9(%r14), %rdx
	movq	8(%rsi), %r9
	movl	$8, %r10d
	cmpq	%rdx, %r9
	jne	.L90
	leaq	17(%r14), %rdx
	cmpq	$1, %r8
	je	.L194
	movq	17(%r14), %rdx
	movq	16(%rsi), %r9
	movl	$16, %r10d
	cmpq	%rdx, %r9
	jne	.L90
	leaq	25(%r14), %rdx
	movl	$24, %r8d
.L89:
	movq	%rdi, %r10
	andl	$7, %r10d
	testb	$7, %dil
	je	.L92
	movzbl	(%rsi,%r8), %ebx
	cmpb	%bl, (%rdx)
	jne	.L92
	leaq	1(%r8), %r9
	cmpq	$1, %r10
	je	.L206
	movzbl	1(%rsi,%r8), %ebx
	cmpb	%bl, 1(%rdx)
	jne	.L206
	leaq	2(%r8), %r9
	cmpq	$2, %r10
	je	.L206
	movzbl	2(%rsi,%r8), %ebx
	cmpb	%bl, 2(%rdx)
	jne	.L206
	leaq	3(%r8), %r9
	cmpq	$3, %r10
	je	.L206
	movzbl	3(%rsi,%r8), %ebx
	cmpb	%bl, 3(%rdx)
	jne	.L206
	leaq	4(%r8), %r9
	cmpq	$4, %r10
	je	.L206
	movzbl	4(%rsi,%r8), %ebx
	cmpb	%bl, 4(%rdx)
	jne	.L206
	leaq	5(%r8), %r9
	subq	$5, %r10
	je	.L206
	movzbl	5(%rsi,%r8), %ebx
	cmpb	%bl, 5(%rdx)
	jne	.L206
	leaq	6(%r8), %r9
	cmpq	$1, %r10
	je	.L206
	movzbl	6(%rsi,%r8), %ebx
	cmpb	%bl, 6(%rdx)
	jne	.L206
	addq	$7, %r8
	jmp	.L92
.L438:
	leaq	(%r15,%rdx,4), %rdi
	movl	$102, %edx
.L388:
	salq	%cl, %rdx
	addq	%r9, %rdx
	sall	$5, %edx
	addl	%edx, %r10d
	movl	(%rdi), %edx
	cmpl	%edx, %r10d
	cmova	%edx, %r10d
	movl	%r10d, (%rdi)
	movl	$1, -80(%rbp)
	jmp	.L129
.L426:
	movq	-120(%rbp), %rdx
	leaq	4(%rbx,%r9), %rax
	jmp	.L387
.L404:
	movzbl	-48(%rbp), %ecx
	leaq	4(,%rdx,4), %rdi
	movzbl	%r10b, %r10d
	movl	$22, %edx
	leaq	(%r15,%rdi), %rsi
	salq	%cl, %rdx
	movl	(%rsi), %ecx
	addq	%r12, %rdx
	sall	$5, %edx
	addl	%r10d, %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, (%rsi)
	cmpb	$9, 1(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$50, %eax
	leaq	4(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L420:
	cmpb	$10, %dil
	movl	$78, %eax
	movl	$113, %edi
	movzbl	-48(%rbp), %ecx
	cmovne	%rdi, %rax
	leaq	4(%r15,%rsi), %rdx
	movl	$1, -80(%rbp)
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r9d
	movl	(%rdx), %eax
	cmpl	%eax, %r9d
	cmova	%eax, %r9d
	movl	%r9d, (%rdx)
	jmp	.L15
.L102:
	movzbl	(%rbx), %edx
	leal	-97(%rdx), %esi
	cmpb	$25, %sil
	ja	.L88
	xorl	$32, %edx
	cmpb	1(%r14), %dl
	jne	.L88
	leal	-1(%r15), %edx
	leaq	2(%r14), %r8
	movq	%rdx, -144(%rbp)
	movq	%rdx, %r9
	shrq	$3, %rdx
	movq	%rdx, -136(%rbp)
	movl	$0, %edx
	je	.L105
.L107:
	movq	2(%r14,%rdx), %rsi
	movq	1(%rbx,%rdx), %r8
	cmpq	%r8, %rsi
	je	.L451
	xorq	%r8, %rsi
	rep bsfq	%rsi, %rsi
	movslq	%esi, %rsi
	shrq	$3, %rsi
	addq	%rsi, %rdx
.L108:
	cmpq	%rdx, -144(%rbp)
	jne	.L88
	leaq	1(%rax), %r9
	movl	$30, %edx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L427:
	cmpb	$44, %al
	je	.L452
	cmpb	$46, %al
	je	.L453
	cmpb	$61, %al
	jne	.L386
	movzbl	1(%rsi), %eax
	cmpb	$34, %al
	je	.L454
	cmpb	$39, %al
	jne	.L386
	leaq	8(%rbx,%r9), %rdx
	movl	$98, %eax
.L385:
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r11, %rax
	sall	$5, %eax
	addl	%r15d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	movl	$1, -80(%rbp)
	jmp	.L88
.L421:
	movzbl	1(%rdx), %eax
	cmpb	$34, %al
	je	.L455
	cmpb	$39, %al
	jne	.L79
	xorl	%eax, %eax
	cmpb	$10, %dil
	movzbl	-48(%rbp), %ecx
	leaq	8(%r15,%rsi), %rdx
	setne	%al
	movl	$1, -80(%rbp)
	leaq	108(,%rax,8), %rax
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r9d
	movl	(%rdx), %eax
	cmpl	%eax, %r9d
	cmova	%eax, %r9d
	movl	%r9d, (%rdx)
	jmp	.L15
.L445:
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$46, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L439:
	movl	$18, %edx
	cmpb	$101, %r8b
	je	.L137
	cmpb	$115, %r8b
	movl	$13, %edx
	movl	$7, %ebx
	cmove	%rbx, %rdx
.L137:
	leaq	12(%r15,%rdi,4), %rdi
	jmp	.L388
.L433:
	movzbl	2(%rax), %edx
	cmpb	$102, %dl
	je	.L456
	cmpb	$110, %dl
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$45, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L397:
	cmpb	$109, 3(%r14)
	jne	.L1
	cmpb	$47, 4(%r14)
	jne	.L1
	jmp	.L140
.L407:
	movzbl	-48(%rbp), %ecx
	movl	$51, %eax
	leaq	4(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L448:
	cmpb	$10, %r10b
	je	.L457
	cmpb	$32, 1(%rdx)
	jne	.L386
	addq	$3, %rax
	movl	$111, %edx
.L152:
	leaq	(%rbx,%rax,4), %rax
	jmp	.L387
.L206:
	movq	%r9, %r8
	jmp	.L92
.L431:
	movzbl	2(%rax), %edx
	cmpb	$110, %dl
	je	.L458
	cmpb	$115, %dl
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$47, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L443:
	movzbl	-48(%rbp), %ecx
	movl	$99, %edi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	addq	%r12, %rcx
	sall	$5, %ecx
	addl	%r9d, %ecx
	cmpl	%r8d, %ecx
	cmova	%r8d, %ecx
	movl	%ecx, (%rax)
	cmpb	$32, 1(%rdx)
	jne	.L79
	addq	$8, %rsi
	movl	$58, %eax
	jmp	.L164
.L437:
	movq	-72(%rbp), %r11
	subq	$1, %r8
	je	.L215
	movq	10(%r14), %r11
	movq	8(%rcx), %rdx
	movl	$8, %ebx
	cmpq	%r11, %rdx
	jne	.L131
	leaq	18(%r14), %r11
	cmpq	$1, %r8
	je	.L217
	movq	18(%r14), %r11
	movq	16(%rcx), %rdx
	movl	$16, %ebx
	cmpq	%r11, %rdx
	jne	.L131
	leaq	26(%r14), %r11
	movl	$24, %r8d
.L130:
	movq	%rsi, %rbx
	andl	$7, %ebx
	testb	$7, %sil
	je	.L219
	movzbl	(%rcx,%r8), %edx
	cmpb	%dl, (%r11)
	jne	.L219
	movq	%rbx, -64(%rbp)
	leaq	1(%r8), %rdx
	cmpq	$1, %rbx
	je	.L133
	movzbl	1(%rcx,%r8), %ebx
	cmpb	%bl, 1(%r11)
	jne	.L133
	cmpq	$2, -64(%rbp)
	leaq	2(%r8), %rdx
	je	.L133
	movzbl	2(%rcx,%r8), %ebx
	cmpb	%bl, 2(%r11)
	jne	.L133
	cmpq	$3, -64(%rbp)
	leaq	3(%r8), %rdx
	je	.L133
	movzbl	3(%rcx,%r8), %ebx
	cmpb	%bl, 3(%r11)
	jne	.L133
	cmpq	$4, -64(%rbp)
	leaq	4(%r8), %rdx
	je	.L133
	movzbl	4(%rcx,%r8), %ebx
	cmpb	%bl, 4(%r11)
	jne	.L133
	subq	$5, -64(%rbp)
	leaq	5(%r8), %rdx
	je	.L133
	movzbl	5(%rcx,%r8), %ebx
	cmpb	%bl, 5(%r11)
	jne	.L133
	cmpq	$1, -64(%rbp)
	leaq	6(%r8), %rdx
	je	.L133
	movzbl	6(%rcx,%r8), %ebx
	cmpb	%bl, 6(%r11)
	jne	.L133
	leaq	7(%r8), %rdx
	jmp	.L133
.L405:
	movzbl	-48(%rbp), %ecx
	movl	$24, %eax
	leaq	4(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L444:
	movzbl	-48(%rbp), %ecx
	movl	$28, %eax
	leaq	8(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L174:
	movq	%r11, -72(%rbp)
	jmp	.L7
.L432:
	movzbl	2(%rax), %edx
	cmpb	$111, %dl
	je	.L459
	cmpb	$114, %dl
	jne	.L15
	cmpb	$111, 3(%rax)
	jne	.L15
	cmpb	$109, 4(%rax)
	jne	.L15
	cmpb	$32, 5(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$37, %eax
	leaq	20(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$83, %edx
	cmpb	$10, %r10b
	movl	$15, %eax
	cmovne	%rdx, %rax
	leaq	(%rbx,%r8,4), %rdx
	jmp	.L385
.L406:
	movzbl	-48(%rbp), %ecx
	movl	$36, %eax
	leaq	4(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L170:
	movl	$8, %r11d
	jmp	.L4
.L452:
	movl	$103, %eax
	leaq	4(%rbx,%r9), %rdx
	salq	%cl, %rax
	movl	(%rdx), %r8d
	addq	%r11, %rax
	sall	$5, %eax
	addl	%r15d, %eax
	cmpl	%r8d, %eax
	cmova	%r8d, %eax
	movl	%eax, (%rdx)
	cmpb	$32, 1(%rsi)
	jne	.L386
	movq	-64(%rbp), %rax
	leaq	8(%rax,%r9), %rdx
	movl	$33, %eax
	jmp	.L385
.L422:
	movq	-120(%rbp), %rax
	addq	%rcx, %rax
	addq	$8, %rcx
	subq	$1, %r11
	jne	.L57
	movq	%rcx, %r11
.L55:
	andl	$7, %esi
	movl	%esi, %r8d
	je	.L183
	movzbl	1(%r10,%r11), %esi
	cmpb	%sil, (%rax)
	jne	.L183
	leaq	1(%r11), %rcx
	cmpq	$1, %r8
	je	.L58
	movzbl	1(%r10,%rcx), %esi
	cmpb	%sil, 1(%rax)
	jne	.L58
	leaq	2(%r11), %rcx
	cmpq	$2, %r8
	je	.L58
	movzbl	1(%r10,%rcx), %esi
	cmpb	%sil, 2(%rax)
	jne	.L58
	leaq	3(%r11), %rcx
	cmpq	$3, %r8
	je	.L58
	movzbl	1(%r10,%rcx), %esi
	cmpb	%sil, 3(%rax)
	jne	.L58
	leaq	4(%r11), %rcx
	cmpq	$4, %r8
	je	.L58
	movzbl	1(%r10,%rcx), %esi
	cmpb	%sil, 4(%rax)
	jne	.L58
	leaq	5(%r11), %rcx
	subq	$5, %r8
	je	.L58
	movzbl	1(%r10,%rcx), %esi
	cmpb	%sil, 5(%rax)
	jne	.L58
	leaq	6(%r11), %rcx
	cmpq	$1, %r8
	je	.L58
	movzbl	1(%r10,%rcx), %esi
	cmpb	%sil, 6(%rax)
	jne	.L58
	leaq	7(%r11), %rcx
	jmp	.L58
.L446:
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$60, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L458:
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$16, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L449:
	leaq	0(,%r8,4), %r9
	leaq	(%rbx,%r9), %rsi
	movl	(%rsi), %eax
	cmpb	$10, %r10b
	je	.L460
	movl	$115, %r8d
	salq	%cl, %r8
	addq	%r11, %r8
	sall	$5, %r8d
	addl	%r15d, %r8d
	cmpl	%eax, %r8d
	cmovbe	%r8d, %eax
	movl	%eax, (%rsi)
	cmpb	$32, 1(%rdx)
	jne	.L386
	leaq	4(%r9), %r8
	movl	$117, %eax
.L154:
	movq	-64(%rbp), %rbx
	leaq	(%rbx,%r8), %rdx
	jmp	.L385
.L434:
	cmpb	$111, 2(%rax)
	jne	.L15
	cmpb	$116, 3(%rax)
	jne	.L15
	cmpb	$32, 4(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$80, %eax
	leaq	16(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L455:
	xorl	%eax, %eax
	cmpb	$10, %dil
	movzbl	-48(%rbp), %ecx
	leaq	8(%r15,%rsi), %rdx
	setne	%al
	movl	$1, -80(%rbp)
	addq	$104, %rax
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r9d
	movl	(%rdx), %eax
	cmpl	%eax, %r9d
	cmova	%eax, %r9d
	movl	%r9d, (%rdx)
	jmp	.L15
.L459:
	cmpb	$114, 3(%rax)
	jne	.L15
	cmpb	$32, 4(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$25, %eax
	leaq	16(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L408:
	movzbl	-48(%rbp), %ecx
	movl	$57, %eax
	leaq	4(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L453:
	movl	$71, %eax
	leaq	4(%rbx,%r9), %rdx
	salq	%cl, %rax
	movl	(%rdx), %r8d
	addq	%r11, %rax
	sall	$5, %eax
	addl	%r15d, %eax
	cmpl	%r8d, %eax
	cmova	%r8d, %eax
	movl	%eax, (%rdx)
	cmpb	$32, 1(%rsi)
	jne	.L386
	movq	-64(%rbp), %rax
	leaq	8(%rax,%r9), %rdx
	movl	$52, %eax
	jmp	.L385
.L435:
	movzbl	2(%rax), %edx
	cmpb	$104, %dl
	je	.L461
	cmpb	$111, %dl
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$17, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L409:
	movzbl	1(%rax), %eax
	cmpb	$34, %al
	je	.L462
	cmpb	$39, %al
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$86, %eax
	leaq	8(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L456:
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$8, %eax
	leaq	12(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L219:
	movq	%r8, %rdx
	jmp	.L133
.L412:
	cmpb	$117, 1(%rax)
	jne	.L15
	cmpb	$108, 2(%rax)
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$90, %eax
	leaq	16(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L411:
	movzbl	1(%rax), %ecx
	cmpb	$100, %cl
	je	.L463
	cmpb	$114, %cl
	je	.L464
	cmpb	$115, %cl
	jne	.L15
	cmpb	$116, 2(%rax)
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$95, %eax
	leaq	16(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L410:
	cmpb	$108, 1(%rax)
	jne	.L15
	cmpb	$32, 2(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$84, %eax
	leaq	12(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L460:
	movl	$96, %r8d
	salq	%cl, %r8
	addq	%r11, %r8
	sall	$5, %r8d
	addl	%r15d, %r8d
	cmpl	%eax, %r8d
	cmovbe	%r8d, %eax
	movl	%eax, (%rsi)
	cmpb	$32, 1(%rdx)
	jne	.L386
	leaq	4(%r9), %r8
	movl	$91, %eax
	jmp	.L154
.L461:
	movzbl	3(%rax), %edx
	cmpb	$101, %dl
	je	.L465
	cmpb	$97, %dl
	jne	.L15
	cmpb	$116, 4(%rax)
	jne	.L15
	cmpb	$32, 5(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$29, %eax
	leaq	20(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	(%rbx,%r8,4), %rsi
	movl	$109, %r8d
	salq	%cl, %r8
	movl	(%rsi), %r9d
	addq	%r11, %r8
	sall	$5, %r8d
	addl	%r15d, %r8d
	cmpl	%r9d, %r8d
	cmova	%r9d, %r8d
	movl	%r8d, (%rsi)
	cmpb	$32, 1(%rdx)
	jne	.L386
	addq	$3, %rax
	movl	$65, %edx
	jmp	.L152
.L454:
	leaq	8(%rbx,%r9), %rdx
	movl	$81, %eax
	jmp	.L385
.L192:
	movl	$8, %r8d
	jmp	.L89
.L215:
	movl	$8, %r8d
	jmp	.L130
.L462:
	movzbl	-48(%rbp), %ecx
	movl	$70, %eax
	leaq	8(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L183:
	movq	%r11, %rcx
	jmp	.L58
.L450:
	xorl	%edx, %edx
	cmpb	$10, %r10b
	sete	%dl
	leaq	110(,%rdx,8), %rdx
	jmp	.L384
.L442:
	cmpb	$32, 5(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$43, %eax
	leaq	20(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L172:
	movl	$16, %r11d
	jmp	.L4
.L440:
	movq	-72(%rbp), %rcx
	subq	$1, %rax
	je	.L224
	movq	13(%r14), %r11
	movq	8(%rdx), %rcx
	movl	$8, %ebx
	cmpq	%r11, %rcx
	jne	.L143
	subq	$1, %rax
	leaq	21(%r14), %rcx
	je	.L226
	movq	21(%r14), %r11
	movq	16(%rdx), %rcx
	movl	$16, %ebx
	cmpq	%r11, %rcx
	jne	.L143
	leaq	29(%r14), %rcx
	movl	$24, %eax
.L142:
	movq	%rdi, %rbx
	andl	$7, %ebx
	testb	$7, %dil
	je	.L145
	movzbl	(%rcx), %r11d
	cmpb	%r11b, (%rdx,%rax)
	jne	.L228
	leaq	1(%rax), %r11
	cmpq	$1, %rbx
	je	.L147
	movzbl	1(%rdx,%rax), %r12d
	cmpb	%r12b, 1(%rcx)
	jne	.L147
	leaq	2(%rax), %r11
	cmpq	$2, %rbx
	je	.L147
	movzbl	2(%rdx,%rax), %r12d
	cmpb	%r12b, 2(%rcx)
	jne	.L147
	leaq	3(%rax), %r11
	cmpq	$3, %rbx
	je	.L147
	movzbl	3(%rdx,%rax), %r12d
	cmpb	%r12b, 3(%rcx)
	jne	.L147
	leaq	4(%rax), %r11
	cmpq	$4, %rbx
	je	.L147
	movzbl	4(%rdx,%rax), %r12d
	cmpb	%r12b, 4(%rcx)
	jne	.L147
	leaq	5(%rax), %r11
	subq	$5, %rbx
	je	.L147
	movzbl	5(%rdx,%rax), %r12d
	cmpb	%r12b, 5(%rcx)
	jne	.L147
	subq	$1, %rbx
	leaq	6(%rax), %r11
	je	.L147
	movzbl	6(%rdx,%rax), %ebx
	cmpb	%bl, 6(%rcx)
	jne	.L147
	leaq	7(%rax), %r11
.L147:
	movq	%r11, %rax
	jmp	.L145
.L228:
	movq	%rax, %r11
	jmp	.L147
.L226:
	movl	$16, %eax
	jmp	.L142
.L224:
	movl	$8, %eax
	jmp	.L142
.L194:
	movl	$16, %r8d
	jmp	.L89
.L465:
	cmpb	$32, 4(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$5, %eax
	leaq	16(%r15,%rdi), %rdx
	salq	%cl, %rax
	addq	%r12, %rax
	sall	$5, %eax
	addl	%eax, %r10d
	movl	(%rdx), %eax
	cmpl	%eax, %r10d
	cmova	%eax, %r10d
	movl	%r10d, (%rdx)
	jmp	.L15
.L217:
	movl	$16, %r8d
	jmp	.L130
.L441:
	leaq	8(%rsi), %rdx
	cmpq	-56(%rbp), %rdx
	jnb	.L389
	addq	%r14, %rax
	cmpb	$32, (%rax)
	jne	.L389
	cmpb	$111, 1(%rax)
	jne	.L389
	cmpb	$102, 2(%rax)
	jne	.L389
	cmpb	$32, 3(%rax)
	jne	.L389
	movl	$62, %ebx
	leaq	16(%r15,%r11), %rdx
	salq	%cl, %rbx
	addq	%r9, %rbx
	sall	$5, %ebx
	addl	%r10d, %ebx
	cmpl	(%rdx), %ebx
	cmova	(%rdx), %ebx
	addq	$12, %rsi
	movl	%ebx, (%rdx)
	cmpq	-56(%rbp), %rsi
	jnb	.L389
	cmpb	$116, 4(%rax)
	jne	.L389
	cmpb	$104, 5(%rax)
	jne	.L389
	cmpb	$101, 6(%rax)
	jne	.L389
	cmpb	$32, 7(%rax)
	jne	.L389
	movl	$73, %edx
	leaq	32(%r15,%r11), %rax
	salq	%cl, %rdx
	addq	%r9, %rdx
	sall	$5, %edx
	addl	%edx, %r10d
	movl	(%rax), %edx
	cmpl	%edx, %r10d
	cmova	%edx, %r10d
	movl	%r10d, (%rax)
	jmp	.L389
.L451:
	movq	-128(%rbp), %rsi
	leaq	(%rsi,%rdx), %r8
	addq	$8, %rdx
	subq	$1, -136(%rbp)
	jne	.L107
.L105:
	movl	%r9d, %esi
	andl	$7, %esi
	movl	%esi, %r9d
	je	.L108
	movzbl	1(%rbx,%rdx), %esi
	cmpb	%sil, (%r8)
	jne	.L210
	movq	%r9, -136(%rbp)
	subq	$1, %r9
	leaq	1(%rdx), %rsi
	je	.L110
	movzbl	1(%rbx,%rsi), %r9d
	cmpb	%r9b, 1(%r8)
	jne	.L110
	cmpq	$2, -136(%rbp)
	leaq	2(%rdx), %rsi
	je	.L110
	movzbl	1(%rbx,%rsi), %r9d
	cmpb	%r9b, 2(%r8)
	jne	.L110
	cmpq	$3, -136(%rbp)
	leaq	3(%rdx), %rsi
	je	.L110
	movzbl	1(%rbx,%rsi), %r9d
	cmpb	%r9b, 3(%r8)
	jne	.L110
	cmpq	$4, -136(%rbp)
	leaq	4(%rdx), %rsi
	je	.L110
	movzbl	1(%rbx,%rsi), %r9d
	cmpb	%r9b, 4(%r8)
	jne	.L110
	subq	$5, -136(%rbp)
	leaq	5(%rdx), %rsi
	je	.L110
	movzbl	1(%rbx,%rsi), %r9d
	cmpb	%r9b, 5(%r8)
	jne	.L110
	cmpq	$1, -136(%rbp)
	leaq	6(%rdx), %rsi
	je	.L110
	movzbl	1(%rbx,%rsi), %ebx
	cmpb	%bl, 6(%r8)
	jne	.L110
	leaq	7(%rdx), %rsi
.L110:
	movq	%rsi, %rdx
	jmp	.L108
.L210:
	movq	%rdx, %rsi
	jmp	.L110
.L464:
	cmpb	$32, 2(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$82, %eax
	leaq	12(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L463:
	cmpb	$32, 2(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$53, %eax
	leaq	12(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L414:
	movzbl	1(%rax), %ecx
	cmpb	$101, %cl
	je	.L466
	cmpb	$121, %cl
	jne	.L15
	cmpb	$32, 2(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$61, %eax
	leaq	12(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L413:
	movzbl	1(%rax), %ecx
	cmpb	$118, %cl
	je	.L467
	cmpb	$122, %cl
	jne	.L15
	cmpb	$101, 2(%rax)
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$100, %eax
	leaq	16(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L466:
	cmpb	$115, 2(%rax)
	jne	.L15
	cmpb	$115, 3(%rax)
	jne	.L15
	cmpb	$32, 4(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$93, %eax
	leaq	20(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
.L467:
	cmpb	$101, 2(%rax)
	jne	.L15
	cmpb	$32, 3(%rax)
	jne	.L15
	movzbl	-48(%rbp), %ecx
	movl	$92, %eax
	leaq	16(%r15,%rdx,4), %rdx
	movzbl	%r10b, %r10d
	salq	%cl, %rax
	movl	(%rdx), %ecx
	addq	%r12, %rax
	sall	$5, %eax
	addl	%r10d, %eax
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L15
	.cfi_endproc
.LFE52:
	.size	BrotliFindAllStaticDictionaryMatches, .-BrotliFindAllStaticDictionaryMatches
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
