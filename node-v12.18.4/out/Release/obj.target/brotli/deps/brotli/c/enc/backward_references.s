	.file	"backward_references.c"
	.text
	.p2align 4
	.type	CreateBackwardReferencesNH2, @function
CreateBackwardReferencesNH2:
.LFB280:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$256, %edx
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -56
	movq	%r8, -64(%rbp)
	movq	%rsi, -48(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	subq	$16, %rax
	cmpq	$7, %rdi
	movq	%rax, -112(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rcx
	leaq	-7(%rsi), %rax
	cmovbe	%r13, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -160(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -144(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movq	%rax, -168(%rbp)
	addq	%r13, %rax
	movq	%rax, -88(%rbp)
	leaq	8(%r13), %rax
	cmpq	%rax, %rsi
	jbe	.L101
	movq	32(%rbp), %rax
	movq	%r14, %rbx
	movq	%r12, %r15
	movq	%rcx, %r14
	movq	%r9, %r11
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-64(%rbp), %rax
	movq	16(%rbp), %rsi
	movq	%r13, %rdx
	movabsq	$-4819355556693147648, %r12
	movq	-48(%rbp), %r10
	movq	72(%rax), %rax
	subq	%r13, %r10
	movq	%rax, -104(%rbp)
	movq	%rbx, %rax
	andq	%r13, %rax
	addq	%r15, %rax
	movq	(%rax), %rdi
	movzbl	(%rax), %ecx
	imulq	%rdi, %r12
	movb	%cl, -72(%rbp)
	shrq	$48, %r12
	movq	%r12, -56(%rbp)
	movslq	(%rsi), %r12
	movl	%r13d, %esi
	subq	%r12, %rdx
	cmpq	%r13, %rdx
	jnb	.L5
	andl	%ebx, %edx
	addq	%r15, %rdx
	cmpb	%cl, (%rdx)
	je	.L300
.L5:
	movq	-56(%rbp), %rdx
	movzbl	-72(%rbp), %ecx
	leaq	(%r11,%rdx,4), %rdx
	movl	40(%rdx), %r8d
	movl	%esi, 40(%rdx)
	movq	%rbx, %rdx
	andq	%r8, %rdx
	addq	%r15, %rdx
	cmpb	(%rdx), %cl
	jne	.L22
	movq	-112(%rbp), %rcx
	movq	%r13, %r12
	cmpq	%r13, %rcx
	cmova	%r13, %rcx
	movq	%rcx, -72(%rbp)
	subq	%r8, %r12
	je	.L22
	cmpq	%rcx, %r12
	ja	.L22
	movq	%r10, %rcx
	shrq	$3, %rcx
	movq	%rcx, -80(%rbp)
	je	.L14
	leaq	8(%rax), %rcx
	xorl	%r8d, %r8d
	movq	%rcx, -120(%rbp)
.L16:
	movq	(%rax,%r8), %rcx
	movq	(%rdx,%r8), %r9
	cmpq	%r9, %rcx
	je	.L301
	xorq	%r9, %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%r8, %rcx
	movq	%rcx, -80(%rbp)
.L18:
	movq	-80(%rbp), %rcx
	cmpq	$3, %rcx
	jbe	.L20
	leaq	(%rcx,%rcx,8), %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	subq	%rdx, %rcx
	bsrl	%r12d, %edx
	imull	$30, %edx, %edx
	addq	$1920, %rcx
	subq	%rdx, %rcx
	movq	%rcx, -128(%rbp)
	cmpq	$2020, %rcx
	ja	.L107
.L20:
	movq	24(%r11), %r9
	movq	32(%r11), %r8
	movq	%r9, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, %r8
	jb	.L297
	imull	$506832829, (%rax), %edx
	movq	-64(%rbp), %rcx
	addq	$1, %r9
	movq	104(%rcx), %rcx
	shrl	$18, %edx
	addl	%edx, %edx
	movzwl	(%rcx,%rdx,2), %edx
	movq	%r9, 24(%r11)
	movq	%rdx, %rcx
	testq	%rdx, %rdx
	je	.L297
	movl	%edx, %r9d
	andl	$31, %r9d
	movw	%r9w, -136(%rbp)
	movq	%rdx, %r9
	andl	$31, %r9d
	cmpq	%r9, %r10
	jb	.L297
	movq	-64(%rbp), %r12
	shrq	$5, %rdx
	movq	%rdx, -128(%rbp)
	imulq	%r9, %rdx
	movq	80(%r12), %r12
	movq	%r12, -120(%rbp)
	movl	32(%r12,%r9,4), %r12d
	addq	%r12, %rdx
	movq	-120(%rbp), %r12
	addq	168(%r12), %rdx
	movq	%rdx, -152(%rbp)
	movq	%rdx, %r12
	movq	%r9, %rdx
	shrq	$3, %rdx
	movq	%rdx, -80(%rbp)
	je	.L25
	movq	(%r12), %rdx
	xorl	%r12d, %r12d
	cmpq	%rdx, %rdi
	je	.L302
.L26:
	xorq	%rdx, %rdi
	xorl	%eax, %eax
	rep bsfq	%rdi, %rax
	cltq
	shrq	$3, %rax
	addq	%r12, %rax
	movq	%rax, -80(%rbp)
.L27:
	movq	-64(%rbp), %rax
	movl	88(%rax), %eax
	addq	-80(%rbp), %rax
	cmpq	%rax, %r9
	jnb	.L297
	cmpq	$0, -80(%rbp)
	je	.L297
.L93:
	movq	-64(%rbp), %rdi
	movq	%r9, %rax
	subq	-80(%rbp), %rax
	leal	(%rax,%rax,2), %ecx
	movq	96(%rdi), %rdi
	addl	%ecx, %ecx
	movq	%rdi, %rdx
	movq	%rdi, -152(%rbp)
	movq	-120(%rbp), %rdi
	shrq	%cl, %rdx
	andl	$63, %edx
	movzbl	(%rdi,%r9), %ecx
	movq	-128(%rbp), %rdi
	leaq	(%rdx,%rax,4), %rax
	salq	%cl, %rax
	movq	-72(%rbp), %rcx
	leaq	1(%rdi,%rcx), %rdx
	leaq	(%rax,%rdx), %r12
	cmpq	%r12, -104(%rbp)
	jb	.L297
	movq	-80(%rbp), %rax
	leaq	(%rax,%rax,8), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	subq	%rax, %rdx
	bsrl	%r12d, %eax
	imull	$30, %eax, %eax
	addq	$1920, %rdx
	subq	%rax, %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$2019, %rdx
	jbe	.L297
	movzwl	-136(%rbp), %eax
	subl	-80(%rbp), %eax
	addq	$1, %r8
	movl	%eax, -204(%rbp)
	movq	-56(%rbp), %rax
	cmpq	$2020, -128(%rbp)
	movq	%r8, 32(%r11)
	movl	%esi, 40(%r11,%rax,4)
	jne	.L12
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L302:
	subq	$1, -80(%rbp)
	movq	-152(%rbp), %rdi
	leaq	8(%rdi), %rdx
	je	.L109
	movq	8(%rdi), %rdx
	movq	8(%rax), %rdi
	movl	$8, %r12d
	cmpq	%rdx, %rdi
	jne	.L26
	movq	-152(%rbp), %rdi
	cmpq	$1, -80(%rbp)
	leaq	16(%rdi), %rdx
	je	.L111
	movq	16(%rdi), %rdx
	movq	16(%rax), %rdi
	movl	$16, %r12d
	cmpq	%rdx, %rdi
	jne	.L26
	addq	$24, -152(%rbp)
	movq	$24, -80(%rbp)
.L25:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L27
	movq	-80(%rbp), %rdi
	movq	-152(%rbp), %rcx
	movzbl	(%rax,%rdi), %r12d
	cmpb	%r12b, (%rcx)
	jne	.L27
	leaq	1(%rdi), %r12
	movq	%r12, -80(%rbp)
	cmpq	$1, %rdx
	je	.L28
	movzbl	1(%rax,%rdi), %r12d
	cmpb	%r12b, 1(%rcx)
	jne	.L28
	leaq	2(%rdi), %r12
	movq	%r12, -80(%rbp)
	cmpq	$2, %rdx
	je	.L28
	movzbl	2(%rax,%rdi), %r12d
	cmpb	%r12b, 2(%rcx)
	jne	.L28
	leaq	3(%rdi), %r12
	movq	%r12, -80(%rbp)
	cmpq	$3, %rdx
	je	.L28
	movzbl	3(%rax,%rdi), %r12d
	cmpb	%r12b, 3(%rcx)
	jne	.L28
	leaq	4(%rdi), %r12
	movq	%r12, -80(%rbp)
	cmpq	$4, %rdx
	je	.L28
	movzbl	4(%rax,%rdi), %r12d
	cmpb	%r12b, 4(%rcx)
	jne	.L28
	leaq	5(%rdi), %r12
	movq	%r12, -80(%rbp)
	subq	$5, %rdx
	je	.L28
	movzbl	5(%rax,%rdi), %r12d
	cmpb	%r12b, 5(%rcx)
	jne	.L28
	leaq	6(%rdi), %r12
	movq	%r12, -80(%rbp)
	cmpq	$1, %rdx
	je	.L28
	movzbl	6(%rax,%rdi), %eax
	cmpb	%al, 6(%rcx)
	jne	.L28
	movq	-64(%rbp), %rax
	addq	$7, %rdi
	movq	%rdi, -80(%rbp)
	movl	88(%rax), %eax
	addq	%rdi, %rax
	cmpq	%rax, %r9
	jb	.L93
	.p2align 4,,10
	.p2align 3
.L297:
	movq	-56(%rbp), %rax
	movl	%esi, 40(%r11,%rax,4)
.L22:
	leaq	1(%r14), %rsi
	leaq	1(%r13), %rax
	cmpq	-88(%rbp), %rax
	jbe	.L298
	movq	-48(%rbp), %rdi
	movq	-144(%rbp), %rdx
	addq	-88(%rbp), %rdx
	leaq	-7(%rdi), %rcx
	cmpq	%rax, %rdx
	jnb	.L84
	leaq	17(%r13), %rdx
	leaq	4(%r14), %rdi
	movabsq	$-4819355556693147648, %r8
	cmpq	%rcx, %rdx
	cmovbe	%rdx, %rcx
	subq	%r13, %rdi
	cmpq	%rcx, %rax
	jnb	.L298
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rbx, %rdx
	andq	%rax, %rdx
	movq	(%r15,%rdx), %rsi
	imulq	%r8, %rsi
	movq	%rsi, %rdx
	leaq	(%rdi,%rax), %rsi
	shrq	$48, %rdx
	movl	%eax, 40(%r11,%rdx,4)
	movq	%rax, %rdx
	addq	$4, %rax
	cmpq	%rcx, %rax
	jb	.L85
	addq	$12, %rdx
	movq	%rsi, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L81:
	cmpq	%rdx, -48(%rbp)
	ja	.L87
	movq	-96(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r14, %rcx
	sarq	$4, %rdx
.L4:
	movq	-48(%rbp), %rax
	movq	24(%rbp), %rbx
	addq	%rcx, %rax
	subq	%r13, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	leaq	9(%r13), %rdx
	movq	%rsi, %r14
	movq	%rax, %r13
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	9(%r13), %rdx
	cmpq	%rcx, %rdx
	cmovbe	%rdx, %rcx
	cmpq	%rcx, %rax
	jnb	.L142
	movabsq	$-4819355556693147648, %r8
	leaq	2(%r14), %rsi
	subq	%r13, %rsi
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rbx, %rdx
	andq	%rax, %rdx
	movq	(%r15,%rdx), %rdi
	imulq	%r8, %rdi
	movq	%rdi, %rdx
	leaq	(%rsi,%rax), %rdi
	shrq	$48, %rdx
	movl	%eax, 40(%r11,%rdx,4)
	movq	%rax, %rdx
	addq	$2, %rax
	cmpq	%rcx, %rax
	jb	.L86
	addq	$10, %rdx
	movq	%rdi, %r14
	movq	%rax, %r13
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r10, %r9
	shrq	$3, %r9
	movq	%r9, -80(%rbp)
	je	.L6
	leaq	8(%rax), %r8
	xorl	%r9d, %r9d
	movq	%r8, -120(%rbp)
.L8:
	movq	(%rax,%r9), %r8
	movq	(%rdx,%r9), %rcx
	cmpq	%rcx, %r8
	je	.L303
	xorq	%rcx, %r8
	xorl	%edx, %edx
	rep bsfq	%r8, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%r9, %rdx
	movq	%rdx, -80(%rbp)
.L10:
	movq	-80(%rbp), %rdx
	cmpq	$3, %rdx
	jbe	.L5
	leaq	(%rdx,%rdx,8), %r8
	movq	%r8, %rdx
	salq	$4, %rdx
	subq	%r8, %rdx
	addq	$1935, %rdx
	movq	%rdx, -128(%rbp)
	cmpq	$2020, %rdx
	jbe	.L5
	movl	$0, -204(%rbp)
	movq	-56(%rbp), %rax
	movl	%esi, 40(%r11,%rax,4)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$0, -204(%rbp)
.L12:
	leaq	-1(%r10), %rax
	movq	%r12, -184(%rbp)
	movq	%r13, %r12
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, -88(%rbp)
	movq	16(%rbp), %rax
	movslq	(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	movq	%r13, %rax
	subq	%rdi, %rax
	leaq	1(%rax), %rdi
	leaq	4(%r13), %rax
	movq	%rax, -136(%rbp)
.L57:
	xorl	%r13d, %r13d
	cmpl	$4, -88(%rbp)
	jg	.L34
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rsi
	subq	$1, %rax
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
	movq	%rax, %r13
.L34:
	movq	-112(%rbp), %rax
	leaq	1(%r12), %rdx
	movl	%edx, %r10d
	cmpq	%rax, %rdx
	cmovbe	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	%rdx, %rax
	andq	%rbx, %rax
	addq	%r15, %rax
	movq	(%rax), %rcx
	movq	%rcx, %rsi
	movq	%rcx, -152(%rbp)
	movabsq	$-4819355556693147648, %rcx
	imulq	%rsi, %rcx
	shrq	$48, %rcx
	movq	%rcx, -56(%rbp)
	movzbl	(%rax,%r13), %ecx
	cmpq	%rdi, %rdx
	jbe	.L35
	movl	%ebx, %r8d
	andl	%edi, %r8d
	addq	%r15, %r8
	cmpb	(%r8,%r13), %cl
	je	.L304
.L35:
	movq	-56(%rbp), %rsi
	leaq	(%r11,%rsi,4), %rsi
	movl	40(%rsi), %r8d
	movl	%r10d, 40(%rsi)
	movq	%rbx, %rsi
	andq	%r8, %rsi
	addq	%r15, %rsi
	cmpb	(%rsi,%r13), %cl
	jne	.L120
	movq	%rdx, %r9
	subq	%r8, %r9
	je	.L120
	cmpq	-120(%rbp), %r9
	ja	.L120
	movq	-72(%rbp), %rcx
	shrq	$3, %rcx
	movq	%rcx, -216(%rbp)
	je	.L43
	leaq	8(%rax), %rcx
	movq	%rdx, -200(%rbp)
	xorl	%r8d, %r8d
	movq	%rcx, -224(%rbp)
.L45:
	movq	(%rsi,%r8), %rdx
	movq	(%rax,%r8), %rcx
	movq	%rdx, -176(%rbp)
	cmpq	%rdx, %rcx
	je	.L305
	xorq	-176(%rbp), %rcx
	movq	-200(%rbp), %rdx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%r8, %rcx
	movq	%rcx, -176(%rbp)
.L47:
	movq	-176(%rbp), %rsi
	cmpq	$3, %rsi
	jbe	.L49
	leaq	(%rsi,%rsi,8), %rsi
	movq	%rsi, %r8
	salq	$4, %r8
	subq	%rsi, %r8
	bsrl	%r9d, %esi
	imull	$30, %esi, %esi
	addq	$1920, %r8
	subq	%rsi, %r8
	cmpq	$2020, %r8
	ja	.L124
.L49:
	movq	24(%r11), %rcx
	movq	32(%r11), %r9
	movq	%rcx, %rsi
	movq	%r9, -176(%rbp)
	shrq	$7, %rsi
	cmpq	%rsi, %r9
	jb	.L135
	imull	$506832829, (%rax), %esi
	movq	-64(%rbp), %r9
	addq	$1, %rcx
	movq	104(%r9), %r8
	shrl	$18, %esi
	addl	%esi, %esi
	movzwl	(%r8,%rsi,2), %r9d
	movq	%rcx, 24(%r11)
	movq	%r9, %rsi
	testq	%r9, %r9
	je	.L143
	movl	%r9d, %ecx
	movq	%r9, %r8
	andl	$31, %ecx
	andl	$31, %r8d
	movw	%cx, -206(%rbp)
	cmpq	%r8, -72(%rbp)
	jb	.L135
	movq	%r9, %rcx
	movq	-64(%rbp), %r9
	shrq	$5, %rcx
	movq	80(%r9), %r9
	movq	%rcx, -200(%rbp)
	movq	%r9, -216(%rbp)
	movl	32(%r9,%r8,4), %ecx
	movq	-200(%rbp), %r9
	imulq	%r8, %r9
	addq	%r9, %rcx
	movq	-216(%rbp), %r9
	addq	168(%r9), %rcx
	movq	%r8, %r9
	shrq	$3, %r9
	movq	%rcx, -232(%rbp)
	je	.L51
	movq	$0, -224(%rbp)
	movq	(%rcx), %rcx
	cmpq	%rcx, -152(%rbp)
	je	.L306
.L52:
	xorq	-152(%rbp), %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	-224(%rbp), %rax
	movq	%rax, -152(%rbp)
.L54:
	movq	-64(%rbp), %r9
	movq	-152(%rbp), %rsi
	movl	88(%r9), %eax
	addq	%rsi, %rax
	cmpq	%rax, %r8
	jnb	.L135
	testq	%rsi, %rsi
	je	.L135
	movq	%r8, %rax
	movq	96(%r9), %r9
	subq	%rsi, %rax
	leal	(%rax,%rax,2), %ecx
	movq	%r9, -152(%rbp)
	addl	%ecx, %ecx
	shrq	%cl, %r9
	movq	%r9, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rax,4), %r9
	movq	-216(%rbp), %rax
	movzbl	(%rax,%r8), %ecx
	movq	-200(%rbp), %rax
	salq	%cl, %r9
	movq	-120(%rbp), %rcx
	leaq	1(%rax,%rcx), %rax
	addq	%rax, %r9
	cmpq	%r9, -104(%rbp)
	jb	.L135
	leaq	(%rsi,%rsi,8), %rax
	movq	%rax, %r8
	salq	$4, %r8
	subq	%rax, %r8
	bsrl	%r9d, %eax
	imull	$30, %eax, %eax
	addq	$1920, %r8
	subq	%rax, %r8
	cmpq	$2019, %r8
	jbe	.L135
	movq	-176(%rbp), %rcx
	movzwl	-206(%rbp), %eax
	movq	%rsi, %r13
	addq	$1, %rcx
	subl	%esi, %eax
	movq	%rcx, 32(%r11)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L120:
	xorl	%eax, %eax
	movl	$2020, %r8d
	xorl	%r9d, %r9d
.L42:
	movq	-128(%rbp), %rcx
	addq	$175, %rcx
	cmpq	%r8, %rcx
	ja	.L55
.L320:
	addq	$1, %r14
	cmpq	-136(%rbp), %rdx
	je	.L290
	addq	$1, %rdi
	leaq	9(%r12), %rcx
	cmpq	%rcx, -48(%rbp)
	ja	.L307
.L290:
	movq	%rdx, -72(%rbp)
	movq	%r13, %rdi
	movq	%r13, -56(%rbp)
.L56:
	addq	-168(%rbp), %rdx
	leaq	(%rdx,%rdi,2), %rdi
	movq	%rdi, -88(%rbp)
	cmpq	-120(%rbp), %r9
	jbe	.L308
	leaq	15(%r9), %rsi
.L89:
	movq	-96(%rbp), %rdi
	movl	%eax, %edx
	sall	$25, %edx
	orl	-56(%rbp), %edx
	movl	%edx, 4(%rdi)
	leaq	16(%rdi), %rcx
	movl	%r14d, (%rdi)
	movq	-64(%rbp), %rdi
	movq	%rcx, -80(%rbp)
	movl	56(%rdi), %r8d
	movl	60(%rdi), %edi
	leaq	16(%rdi), %rdx
	movq	%r8, %r13
	movq	%rdi, %r12
	cmpq	%rsi, %rdx
	ja	.L309
	leal	2(%r8), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	subq	%rdi, %rdx
	leaq	-16(%rsi,%rdx), %rsi
	bsrl	%esi, %r10d
	movq	%rsi, %rdi
	leal	-1(%r10), %edx
	movl	%edx, %ecx
	movq	%rdx, %r10
	subq	%r8, %rdx
	movl	$1, %r8d
	shrq	%cl, %rdi
	movl	%r13d, %ecx
	sall	%cl, %r8d
	andl	$1, %edi
	leal	-1(%r8), %ecx
	leaq	-2(%rdi,%rdx,2), %r8
	addq	$2, %rdi
	sall	$10, %edx
	andq	%rsi, %rcx
	leal	16(%r12,%rcx), %r12d
	movl	%r13d, %ecx
	salq	%cl, %r8
	movl	%r10d, %ecx
	salq	%cl, %rdi
	addl	%r12d, %r8d
	movl	%r13d, %ecx
	subq	%rdi, %rsi
	orl	%r8d, %edx
	shrq	%cl, %rsi
	testw	$1023, %r8w
	sete	%dil
.L67:
	movq	-96(%rbp), %rcx
	addl	-56(%rbp), %eax
	cltq
	movl	%esi, 8(%rcx)
	movw	%dx, 14(%rcx)
	cmpq	$5, %r14
	jbe	.L310
	cmpq	$129, %r14
	jbe	.L311
	cmpq	$2113, %r14
	jbe	.L312
	cmpq	$6209, %r14
	jbe	.L139
	cmpq	$22594, %r14
	sbbl	%esi, %esi
	andl	$-8, %esi
	addl	$56, %esi
	cmpq	$22594, %r14
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L72
.L318:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %esi
	cmpw	$7, %dx
	jbe	.L313
	.p2align 4,,10
	.p2align 3
.L76:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L78:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %esi
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L308:
	movq	16(%rbp), %rdi
	movq	16(%rbp), %rsi
	movslq	(%rdi), %rcx
	movslq	4(%rsi), %rsi
	movq	%rcx, %rdi
	movq	%rsi, %r8
	cmpq	%r9, %rcx
	je	.L59
	cmpq	%r9, %rsi
	je	.L314
	leaq	3(%r9), %rdx
	movq	%rdx, %r10
	subq	%rcx, %r10
	movq	%r10, %rcx
	cmpq	$6, %r10
	jbe	.L315
	subq	%rsi, %rdx
	cmpq	$6, %rdx
	jbe	.L316
	movq	16(%rbp), %rsi
	movslq	8(%rsi), %rcx
	movq	%rcx, %rdx
	cmpq	%r9, %rcx
	je	.L137
	movslq	12(%rsi), %rcx
	leaq	15(%r9), %rsi
	cmpq	%r9, %rcx
	je	.L317
.L63:
	testq	%rsi, %rsi
	je	.L59
	movq	16(%rbp), %rcx
	movl	8(%rcx), %edx
.L61:
	movq	16(%rbp), %rcx
	movl	%edx, 12(%rcx)
	movl	%r8d, 8(%rcx)
	movl	%edi, 4(%rcx)
	movl	%r9d, (%rcx)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L310:
	leal	0(,%r14,8), %esi
	movl	%r14d, %edx
	andl	$56, %esi
.L69:
	cmpq	$9, %rax
	jbe	.L318
.L72:
	cmpq	$133, %rax
	jbe	.L319
	cmpq	$2117, %rax
	ja	.L75
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L73:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %esi
	cmpw	$7, %dx
	ja	.L76
.L313:
	testb	%dil, %dil
	je	.L76
	cmpw	$15, %ax
	ja	.L76
	movl	%esi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %esi
.L79:
	movq	-96(%rbp), %rax
	movq	-72(%rbp), %r13
	movq	-56(%rbp), %rdi
	movq	-160(%rbp), %rcx
	movw	%si, 12(%rax)
	movq	48(%rbp), %rax
	addq	%r14, (%rax)
	leaq	2(%r13), %rax
	addq	%rdi, %r13
	cmpq	%rcx, %r13
	cmovbe	%r13, %rcx
	shrq	$2, %rdi
	cmpq	%r9, %rdi
	jbe	.L80
	leaq	0(,%r9,4), %rdx
	movq	%r13, %rdi
	subq	%rdx, %rdi
	cmpq	%rax, %rdi
	cmovnb	%rdi, %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
.L80:
	leaq	8(%r13), %rdx
	cmpq	%rcx, %rax
	jnb	.L141
	movabsq	$-4819355556693147648, %rdi
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rbx, %rsi
	andq	%rax, %rsi
	movq	(%r15,%rsi), %r10
	imulq	%rdi, %r10
	movq	%r10, %rsi
	shrq	$48, %rsi
	movl	%eax, 40(%r11,%rsi,4)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L82
.L141:
	movq	-80(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -96(%rbp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-96(%rbp), %rdi
	movl	%eax, %edx
	sall	$25, %edx
	orl	-56(%rbp), %edx
	leaq	16(%rdi), %rsi
	movl	%edx, 4(%rdi)
	xorl	%edx, %edx
	movq	%rsi, -80(%rbp)
	xorl	%esi, %esi
	movl	%r14d, (%rdi)
	movl	$1, %edi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L307:
	subq	$1, -72(%rbp)
	movq	%rdx, %r12
	movl	%eax, -204(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r9, -184(%rbp)
	movq	%r13, -80(%rbp)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%eax, %eax
	movl	$2020, %r8d
	xorl	%r9d, %r9d
.L50:
	movq	-128(%rbp), %rcx
	movq	-56(%rbp), %rsi
	addq	$175, %rcx
	movl	%r10d, 40(%r11,%rsi,4)
	cmpq	%r8, %rcx
	jbe	.L320
.L55:
	movq	-112(%rbp), %rax
	movq	%r12, %r13
	movq	-80(%rbp), %rdi
	movq	-184(%rbp), %r12
	movq	%r13, -72(%rbp)
	movq	%r13, %rdx
	cmpq	%rax, %r13
	movq	%rdi, -56(%rbp)
	cmovbe	%r13, %rax
	movq	%r12, %r9
	movq	%rax, -120(%rbp)
	movl	-204(%rbp), %eax
	jmp	.L56
.L51:
	movq	%rsi, %rcx
	andl	$7, %ecx
	andl	$7, %esi
	movq	%rcx, -224(%rbp)
	je	.L143
	movq	-232(%rbp), %rcx
	movzbl	(%rax), %esi
	cmpb	%sil, (%rcx)
	je	.L90
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%eax, %eax
	movl	$2020, %r8d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L315:
	sall	$2, %ecx
	movl	$158663784, %esi
	sarl	%cl, %esi
	andl	$15, %esi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L124:
	movq	-176(%rbp), %r13
	xorl	%eax, %eax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L311:
	leaq	-2(%r14), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %esi
	andl	$56, %esi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L319:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L304:
	movq	-72(%rbp), %rsi
	shrq	$3, %rsi
	movq	%rsi, -216(%rbp)
	je	.L36
	leaq	8(%rax), %rsi
	movq	%rdx, -200(%rbp)
	xorl	%r9d, %r9d
	movq	%rsi, -224(%rbp)
.L38:
	movq	(%r8,%r9), %rdx
	movq	(%rax,%r9), %rsi
	movq	%rdx, -176(%rbp)
	cmpq	%rdx, %rsi
	je	.L321
	xorq	-176(%rbp), %rsi
	movq	-200(%rbp), %rdx
	rep bsfq	%rsi, %rsi
	movslq	%esi, %rsi
	shrq	$3, %rsi
	addq	%r9, %rsi
	movq	%rsi, -176(%rbp)
.L40:
	movq	-176(%rbp), %rsi
	cmpq	$3, %rsi
	jbe	.L35
	leaq	(%rsi,%rsi,8), %r9
	movq	%r9, %r8
	salq	$4, %r8
	subq	%r9, %r8
	addq	$1935, %r8
	cmpq	$2020, %r8
	jbe	.L35
	movq	-56(%rbp), %rax
	movq	-192(%rbp), %r9
	movq	%rsi, %r13
	movl	%r10d, 40(%r11,%rax,4)
	xorl	%eax, %eax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%r10, %r10
	je	.L20
	movzbl	(%rdx), %ecx
	cmpb	%cl, (%rax)
	jne	.L20
	movq	%r10, %r9
	leaq	1(%rax), %rcx
	movl	$1, %r8d
	subq	$1, %r9
	movq	%r9, -120(%rbp)
	je	.L20
.L98:
	movzbl	(%rdx,%r8), %r9d
	cmpb	%r9b, (%rcx)
	jne	.L106
	leaq	1(%r8), %r9
	cmpq	$1, -120(%rbp)
	movq	%r9, -80(%rbp)
	je	.L18
	movzbl	1(%rdx,%r8), %r9d
	cmpb	%r9b, 1(%rcx)
	jne	.L18
	leaq	2(%r8), %r9
	cmpq	$2, -120(%rbp)
	movq	%r9, -80(%rbp)
	je	.L18
	movzbl	2(%rdx,%r8), %r9d
	cmpb	%r9b, 2(%rcx)
	jne	.L18
	leaq	3(%r8), %r9
	cmpq	$3, -120(%rbp)
	movq	%r9, -80(%rbp)
	je	.L18
	movzbl	3(%rdx,%r8), %r9d
	cmpb	%r9b, 3(%rcx)
	jne	.L18
	subq	$4, -120(%rbp)
	leaq	4(%r8), %r9
	movq	%r9, -80(%rbp)
	je	.L18
	movzbl	4(%rdx,%r8), %r9d
	cmpb	%r9b, 4(%rcx)
	jne	.L18
	leaq	5(%r8), %r9
	cmpq	$1, -120(%rbp)
	movq	%r9, -80(%rbp)
	je	.L18
	movzbl	5(%rdx,%r8), %edx
	cmpb	%dl, 5(%rcx)
	jne	.L18
	leaq	6(%r8), %rcx
	movq	%rcx, -80(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L301:
	movq	-120(%rbp), %rcx
	leaq	8(%r8), %r9
	addq	%r8, %rcx
	subq	$1, -80(%rbp)
	je	.L322
	movq	%r9, %r8
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L75:
	orl	$7, %esi
	movl	$2, %eax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L312:
	leaq	-66(%r14), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %esi
	andl	$56, %esi
	jmp	.L69
.L139:
	movl	$40, %esi
	movl	$21, %edx
	jmp	.L69
.L314:
	movq	16(%rbp), %rsi
	movl	8(%rsi), %edx
	movl	$1, %esi
	jmp	.L61
.L142:
	movq	%rsi, %r14
	movq	%rax, %r13
	jmp	.L81
.L316:
	leal	0(,%rdx,4), %ecx
	movl	$266017486, %esi
	sarl	%cl, %esi
	andl	$15, %esi
	jmp	.L63
.L6:
	testq	%r10, %r10
	je	.L5
	movq	%r10, %r9
	leaq	1(%rax), %rcx
	movl	$1, %r8d
	subq	$1, %r9
	movq	%r9, -120(%rbp)
	je	.L5
.L95:
	movq	%rcx, %r9
	movzbl	(%rdx,%r8), %ecx
	cmpb	%cl, (%r9)
	jne	.L103
	leaq	1(%r8), %rcx
	cmpq	$1, -120(%rbp)
	movq	%rcx, -80(%rbp)
	je	.L10
	movzbl	1(%rdx,%r8), %ecx
	cmpb	%cl, 1(%r9)
	jne	.L10
	leaq	2(%r8), %rcx
	cmpq	$2, -120(%rbp)
	movq	%rcx, -80(%rbp)
	je	.L10
	movzbl	2(%rdx,%r8), %ecx
	cmpb	%cl, 2(%r9)
	jne	.L10
	leaq	3(%r8), %rcx
	cmpq	$3, -120(%rbp)
	movq	%rcx, -80(%rbp)
	je	.L10
	movzbl	3(%rdx,%r8), %ecx
	cmpb	%cl, 3(%r9)
	jne	.L10
	subq	$4, -120(%rbp)
	leaq	4(%r8), %rcx
	movq	%rcx, -80(%rbp)
	je	.L10
	movzbl	4(%rdx,%r8), %ecx
	cmpb	%cl, 4(%r9)
	jne	.L10
	leaq	5(%r8), %rcx
	cmpq	$1, -120(%rbp)
	movq	%rcx, -80(%rbp)
	je	.L10
	movzbl	5(%rdx,%r8), %ecx
	cmpb	%cl, 5(%r9)
	jne	.L10
	leaq	6(%r8), %rcx
	movq	%rcx, -80(%rbp)
	jmp	.L10
.L303:
	movq	-120(%rbp), %rcx
	leaq	8(%r9), %r8
	addq	%r9, %rcx
	subq	$1, -80(%rbp)
	movq	%rcx, -128(%rbp)
	je	.L323
	movq	%r8, %r9
	jmp	.L8
.L43:
	cmpq	$0, -72(%rbp)
	je	.L49
	movzbl	(%rsi), %ecx
	cmpb	%cl, (%rax)
	jne	.L49
	movq	$1, -200(%rbp)
	leaq	1(%rax), %rcx
	movq	%rcx, -224(%rbp)
	movq	-72(%rbp), %rcx
	subq	$1, %rcx
	movq	%rcx, -216(%rbp)
	je	.L49
.L96:
	movq	-200(%rbp), %rcx
	movq	-224(%rbp), %r8
	movzbl	(%rsi,%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L123
	movq	-200(%rbp), %rcx
	addq	$1, %rcx
	cmpq	$1, -216(%rbp)
	movq	%rcx, -176(%rbp)
	je	.L47
	movq	-200(%rbp), %rcx
	movzbl	1(%rcx,%rsi), %ecx
	cmpb	%cl, 1(%r8)
	jne	.L47
	movq	-200(%rbp), %rcx
	addq	$2, %rcx
	cmpq	$2, -216(%rbp)
	movq	%rcx, -176(%rbp)
	je	.L47
	movq	-200(%rbp), %rcx
	movzbl	2(%rcx,%rsi), %ecx
	cmpb	%cl, 2(%r8)
	jne	.L47
	movq	-200(%rbp), %rcx
	addq	$3, %rcx
	cmpq	$3, -216(%rbp)
	movq	%rcx, -176(%rbp)
	je	.L47
	movq	-200(%rbp), %rcx
	movzbl	3(%rcx,%rsi), %ecx
	cmpb	%cl, 3(%r8)
	jne	.L47
	subq	$4, -216(%rbp)
	movq	-200(%rbp), %rcx
	leaq	4(%rcx), %r8
	movq	%r8, -176(%rbp)
	je	.L47
	movq	-224(%rbp), %r8
	movzbl	4(%rcx,%rsi), %ecx
	cmpb	%cl, 4(%r8)
	jne	.L47
	movq	-200(%rbp), %rcx
	cmpq	$1, -216(%rbp)
	leaq	5(%rcx), %r8
	movq	%r8, -176(%rbp)
	je	.L47
	movq	-224(%rbp), %r8
	movzbl	5(%rcx,%rsi), %ecx
	cmpb	%cl, 5(%r8)
	jne	.L47
	movq	-200(%rbp), %rcx
	addq	$6, %rcx
	movq	%rcx, -176(%rbp)
	jmp	.L47
.L305:
	movq	-224(%rbp), %rcx
	addq	%r8, %rcx
	subq	$1, -216(%rbp)
	movq	%rcx, -232(%rbp)
	leaq	8(%r8), %rcx
	je	.L324
	movq	%rcx, %r8
	jmp	.L45
.L101:
	xorl	%edx, %edx
	jmp	.L4
.L306:
	movq	-232(%rbp), %rcx
	addq	$8, %rcx
	subq	$1, %r9
	movq	%r9, -240(%rbp)
	je	.L129
	movq	-232(%rbp), %rcx
	movq	8(%rax), %r9
	movq	$8, -224(%rbp)
	movq	8(%rcx), %rcx
	movq	%r9, -152(%rbp)
	cmpq	%rcx, %r9
	jne	.L52
	movq	-232(%rbp), %r9
	cmpq	$1, -240(%rbp)
	leaq	16(%r9), %rcx
	je	.L131
	movq	$16, -224(%rbp)
	movq	16(%r9), %rcx
	movq	16(%rax), %r9
	movq	%r9, -152(%rbp)
	cmpq	%rcx, %r9
	jne	.L52
	addq	$24, -232(%rbp)
	movq	$24, -152(%rbp)
.L53:
	movq	%rsi, %rcx
	andl	$7, %ecx
	andl	$7, %esi
	movq	%rcx, -224(%rbp)
	je	.L54
	movq	-152(%rbp), %r9
	movq	-232(%rbp), %rsi
	movzbl	(%rax,%r9), %ecx
	cmpb	%cl, (%rsi)
	jne	.L54
.L90:
	leaq	1(%r9), %rcx
	cmpq	$1, -224(%rbp)
	movq	%rcx, -152(%rbp)
	je	.L54
	movq	-232(%rbp), %rsi
	movzbl	1(%rax,%r9), %ecx
	cmpb	%cl, 1(%rsi)
	jne	.L54
	leaq	2(%r9), %rcx
	cmpq	$2, -224(%rbp)
	movq	%rcx, -152(%rbp)
	je	.L54
	movzbl	2(%rax,%r9), %ecx
	cmpb	%cl, 2(%rsi)
	jne	.L54
	leaq	3(%r9), %rcx
	cmpq	$3, -224(%rbp)
	movq	%rcx, -152(%rbp)
	je	.L54
	movzbl	3(%rax,%r9), %ecx
	cmpb	%cl, 3(%rsi)
	jne	.L54
	leaq	4(%r9), %rcx
	cmpq	$4, -224(%rbp)
	movq	%rcx, -152(%rbp)
	je	.L54
	movzbl	4(%rax,%r9), %ecx
	cmpb	%cl, 4(%rsi)
	jne	.L54
	subq	$5, -224(%rbp)
	leaq	5(%r9), %rcx
	movq	%rcx, -152(%rbp)
	je	.L54
	movzbl	5(%rax,%r9), %ecx
	cmpb	%cl, 5(%rsi)
	jne	.L54
	leaq	6(%r9), %rcx
	cmpq	$1, -224(%rbp)
	movq	%rcx, -152(%rbp)
	je	.L54
	movzbl	6(%rax,%r9), %eax
	cmpb	%al, 6(%rsi)
	jne	.L54
	leaq	7(%r9), %rax
	movq	%rax, -152(%rbp)
	jmp	.L54
.L28:
	movq	-64(%rbp), %rax
	movl	88(%rax), %eax
	addq	-80(%rbp), %rax
	cmpq	%rax, %r9
	jb	.L93
	jmp	.L297
.L36:
	cmpq	$0, -72(%rbp)
	je	.L35
	movzbl	(%r8), %esi
	cmpb	%sil, (%rax)
	jne	.L35
	movq	$1, -200(%rbp)
	leaq	1(%rax), %rsi
	movq	%rsi, -216(%rbp)
	movq	-72(%rbp), %rsi
	subq	$1, %rsi
	movq	%rsi, -224(%rbp)
	je	.L35
.L97:
	movq	-216(%rbp), %r9
	movq	-200(%rbp), %rsi
	movzbl	(%r8,%rsi), %esi
	cmpb	%sil, (%r9)
	movq	-200(%rbp), %rsi
	jne	.L118
	addq	$1, %rsi
	cmpq	$1, -224(%rbp)
	movq	%rsi, -176(%rbp)
	je	.L40
	movq	-200(%rbp), %rsi
	movzbl	1(%rsi,%r8), %esi
	cmpb	%sil, 1(%r9)
	jne	.L40
	movq	-200(%rbp), %rsi
	addq	$2, %rsi
	cmpq	$2, -224(%rbp)
	movq	%rsi, -176(%rbp)
	je	.L40
	movq	-200(%rbp), %rsi
	movzbl	2(%rsi,%r8), %esi
	cmpb	%sil, 2(%r9)
	jne	.L40
	movq	-200(%rbp), %rsi
	addq	$3, %rsi
	cmpq	$3, -224(%rbp)
	movq	%rsi, -176(%rbp)
	je	.L40
	movq	-200(%rbp), %rsi
	movzbl	3(%rsi,%r8), %esi
	cmpb	%sil, 3(%r9)
	jne	.L40
	subq	$4, -224(%rbp)
	movq	-200(%rbp), %rsi
	leaq	4(%rsi), %r9
	movq	%r9, -176(%rbp)
	je	.L40
	movq	-216(%rbp), %r9
	movzbl	4(%rsi,%r8), %esi
	cmpb	%sil, 4(%r9)
	jne	.L40
	movq	-200(%rbp), %rsi
	cmpq	$1, -224(%rbp)
	leaq	5(%rsi), %r9
	movq	%r9, -176(%rbp)
	je	.L40
	movq	-216(%rbp), %r9
	movzbl	5(%rsi,%r8), %esi
	cmpb	%sil, 5(%r9)
	jne	.L40
	movq	-200(%rbp), %rsi
	addq	$6, %rsi
	movq	%rsi, -176(%rbp)
	jmp	.L40
.L321:
	movq	-224(%rbp), %rsi
	addq	%r9, %rsi
	subq	$1, -216(%rbp)
	movq	%rsi, -232(%rbp)
	leaq	8(%r9), %rsi
	je	.L325
	movq	%rsi, %r9
	jmp	.L38
.L137:
	movl	$2, %esi
	jmp	.L61
.L322:
	movq	%r9, -80(%rbp)
	movq	%r10, %r9
	andl	$7, %r9d
	movq	%r9, -120(%rbp)
	je	.L18
	movq	-80(%rbp), %r9
	movzbl	(%rdx,%r9), %r9d
	cmpb	%r9b, (%rcx)
	jne	.L18
	addq	$1, %rcx
	addq	$9, %r8
	subq	$1, -120(%rbp)
	jne	.L98
.L106:
	movq	%r8, -80(%rbp)
	jmp	.L18
.L317:
	movl	$3, %esi
	jmp	.L61
.L323:
	movq	%r10, %rcx
	movq	%r8, -80(%rbp)
	movq	-128(%rbp), %r8
	andl	$7, %ecx
	movq	%rcx, -120(%rbp)
	je	.L10
	movq	-80(%rbp), %rcx
	movzbl	(%rdx,%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L10
	subq	$1, -120(%rbp)
	leaq	1(%r8), %rcx
	leaq	9(%r9), %r8
	movq	%r8, -80(%rbp)
	jne	.L95
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r8, -80(%rbp)
	jmp	.L10
.L324:
	movq	%rcx, -176(%rbp)
	movq	-72(%rbp), %rcx
	movq	-200(%rbp), %rdx
	movq	%r8, -200(%rbp)
	andl	$7, %ecx
	movq	%rcx, -216(%rbp)
	je	.L47
	movq	-176(%rbp), %r8
	movq	-232(%rbp), %rcx
	movzbl	(%rsi,%r8), %r8d
	cmpb	%r8b, (%rcx)
	jne	.L47
	addq	$1, %rcx
	movq	%rcx, -224(%rbp)
	movq	-200(%rbp), %rcx
	addq	$9, %rcx
	subq	$1, -216(%rbp)
	movq	%rcx, -200(%rbp)
	jne	.L96
.L123:
	movq	-200(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	jmp	.L47
.L109:
	movq	%rdx, -152(%rbp)
	movq	$8, -80(%rbp)
	jmp	.L25
.L118:
	movq	%rsi, -176(%rbp)
	jmp	.L40
.L325:
	movq	%rsi, -176(%rbp)
	movq	-72(%rbp), %rsi
	movq	-200(%rbp), %rdx
	movq	%r9, -200(%rbp)
	andl	$7, %esi
	movq	%rsi, -224(%rbp)
	je	.L40
	movq	-176(%rbp), %r9
	movq	-232(%rbp), %rsi
	movzbl	(%r8,%r9), %r9d
	cmpb	%r9b, (%rsi)
	jne	.L40
	addq	$1, %rsi
	movq	%rsi, -216(%rbp)
	movq	-200(%rbp), %rsi
	addq	$9, %rsi
	subq	$1, -224(%rbp)
	movq	%rsi, -200(%rbp)
	jne	.L97
	movq	-200(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	jmp	.L40
.L129:
	movq	%rcx, -232(%rbp)
	movq	$8, -152(%rbp)
	jmp	.L53
.L131:
	movq	%rcx, -232(%rbp)
	movq	$16, -152(%rbp)
	jmp	.L53
.L111:
	movq	%rdx, -152(%rbp)
	movq	$16, -80(%rbp)
	jmp	.L25
.L309:
	testl	$1023, %esi
	movl	%esi, %edx
	sete	%dil
	xorl	%esi, %esi
	jmp	.L67
	.cfi_endproc
.LFE280:
	.size	CreateBackwardReferencesNH2, .-CreateBackwardReferencesNH2
	.p2align 4
	.type	CreateBackwardReferencesNH3, @function
CreateBackwardReferencesNH3:
.LFB281:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rdx, %r10
	movl	$2048, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	%rdi, %rsi
	subq	$136, %rsp
	movq	%rcx, -112(%rbp)
	movq	%r8, -216(%rbp)
	movl	8(%r8), %ecx
	movq	%rsi, -88(%rbp)
	salq	%cl, %rax
	movl	$256, %ecx
	subq	$16, %rax
	cmpq	$7, %rdi
	movq	%rbx, %rdi
	movq	%rax, -128(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	leaq	-7(%rsi), %rax
	cmova	%rax, %rdi
	cmpl	$9, 4(%r8)
	cmovl	%rcx, %rdx
	movl	$64, %ecx
	movq	%rdi, -256(%rbp)
	movq	%rdx, -232(%rbp)
	movl	$512, %edx
	cmovl	%rcx, %rdx
	leaq	(%rbx,%rdx), %rdi
	movq	%rdx, -264(%rbp)
	leaq	8(%rbx), %rdx
	movq	%rdi, -136(%rbp)
	cmpq	%rdx, %rsi
	jbe	.L407
	leaq	40(%r9), %rdi
	movl	%r14d, %ecx
	movq	%rbx, %r11
	movq	%r10, %r12
	movq	%rdi, -152(%rbp)
	movq	32(%rbp), %rdi
	movq	%r9, %rbx
	movabsq	$-4819355556693147648, %r14
	movq	%rcx, -72(%rbp)
	movq	%rdi, -160(%rbp)
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L394:
	movq	-128(%rbp), %rax
	movq	-88(%rbp), %r13
	movq	-112(%rbp), %rdi
	subq	%r11, %r13
	cmpq	%r11, %rax
	cmova	%r11, %rax
	movq	%rdi, %rsi
	movq	%r13, %rdx
	shrq	$3, %r13
	andq	%r11, %rsi
	andl	$7, %edx
	addq	%r12, %rsi
	movq	%rax, -120(%rbp)
	movq	16(%rbp), %rax
	movq	(%rsi), %r15
	movzbl	(%rsi), %r9d
	movq	%rdx, -104(%rbp)
	movslq	(%rax), %rax
	imulq	%r14, %r15
	movl	%r9d, %r10d
	movq	%rax, %rcx
	movq	%rax, -96(%rbp)
	movq	%r11, %rax
	subq	%rcx, %rax
	shrq	$48, %r15
	cmpq	%r11, %rax
	jnb	.L409
	andl	%edi, %eax
	addq	%r12, %rax
	cmpb	%r9b, (%rax)
	je	.L566
.L409:
	movq	%rdx, %rax
	movq	$0, -96(%rbp)
	subq	$1, %rax
	movq	$0, -56(%rbp)
	movq	$2020, -64(%rbp)
	movq	%rax, -168(%rbp)
.L330:
	leaq	1(%rsi), %rdx
	movq	-152(%rbp), %rax
	movb	%r10b, -184(%rbp)
	movq	%rdx, -192(%rbp)
	leaq	8(%rsi), %rdx
	movq	%rdx, -176(%rbp)
	leaq	(%rax,%r15,4), %rdi
	movq	%rsi, -80(%rbp)
	movl	(%rdi), %eax
	leaq	4(%rdi), %rcx
	addq	$12, %rdi
.L344:
	movq	-72(%rbp), %rdx
	movq	-56(%rbp), %rsi
	andq	%rax, %rdx
	addq	%r12, %rdx
	movzbl	(%rdx,%rsi), %esi
	cmpl	%r9d, %esi
	jne	.L337
	movq	%r11, %rsi
	subq	%rax, %rsi
	movq	%rsi, -144(%rbp)
	je	.L337
	movq	-120(%rbp), %rax
	cmpq	%rax, %rsi
	ja	.L337
	testq	%r13, %r13
	je	.L338
	movq	%r13, -200(%rbp)
	movq	-80(%rbp), %r10
	xorl	%esi, %esi
.L340:
	movq	(%r10,%rsi), %rax
	movq	(%rdx,%rsi), %r8
	cmpq	%r8, %rax
	je	.L567
	xorq	%r8, %rax
	movq	%r10, -80(%rbp)
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
.L342:
	cmpq	$3, %rax
	jbe	.L337
	leaq	(%rax,%rax,8), %rsi
	movq	-144(%rbp), %r8
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%r8d, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-64(%rbp), %rdx
	jbe	.L337
	movq	-80(%rbp), %rsi
	movq	%r8, -96(%rbp)
	movq	%rax, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movzbl	(%rsi,%rax), %r9d
	.p2align 4,,10
	.p2align 3
.L337:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	%rdi, %rcx
	jne	.L344
	movq	%r11, %rax
	shrq	$3, %rax
	andl	$1, %eax
	addq	%rax, %r15
	cmpq	$2020, -64(%rbp)
	movl	%r11d, 40(%rbx,%r15,4)
	jne	.L568
	movq	-48(%rbp), %rdi
	leaq	1(%r11), %rax
	leaq	1(%rdi), %rcx
	cmpq	%rax, -136(%rbp)
	jnb	.L565
	movq	-136(%rbp), %rdx
	addq	-232(%rbp), %rdx
	cmpq	%rax, %rdx
	jnb	.L391
	movq	-240(%rbp), %rdi
	leaq	17(%r11), %rdx
	movq	-112(%rbp), %r8
	cmpq	%rdi, %rdx
	cmova	%rdi, %rdx
	movq	-48(%rbp), %rdi
	addq	$4, %rdi
	subq	%r11, %rdi
	cmpq	%rax, %rdx
	jbe	.L565
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r8, %rcx
	andq	%rax, %rcx
	movq	(%r12,%rcx), %rsi
	imulq	%r14, %rsi
	movq	%rsi, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	shrq	$48, %rcx
	andl	$1, %esi
	addq	%rsi, %rcx
	movq	%rax, %rsi
	movl	%eax, 40(%rbx,%rcx,4)
	leaq	(%rdi,%rax), %rcx
	addq	$4, %rax
	cmpq	%rdx, %rax
	jb	.L392
	movq	%rcx, -48(%rbp)
	addq	$12, %rsi
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L388:
	cmpq	%rsi, -88(%rbp)
	ja	.L394
	movq	-160(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r11, %rbx
	sarq	$4, %rdx
.L329:
	movq	-88(%rbp), %rax
	addq	-48(%rbp), %rax
	subq	%rbx, %rax
	movq	24(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	leaq	9(%r11), %rsi
	movq	%rcx, -48(%rbp)
	movq	%rax, %r11
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L568:
	movq	-216(%rbp), %rax
	movq	%r11, %r10
	movl	4(%rax), %eax
	movl	%eax, -192(%rbp)
	movq	16(%rbp), %rax
	movslq	(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	movl	$1, %eax
	subq	%rdi, %rax
	addq	%r11, %rax
	movq	%rax, -136(%rbp)
	leaq	4(%r11), %rax
	movq	%rax, -208(%rbp)
	movq	-88(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -200(%rbp)
.L364:
	subq	%r10, %rax
	xorl	%r15d, %r15d
	cmpl	$4, -192(%rbp)
	jg	.L346
	movq	-56(%rbp), %rcx
	leaq	-1(%rcx), %r15
	cmpq	%rax, %r15
	cmova	%rax, %r15
.L346:
	movq	-128(%rbp), %rdi
	leaq	1(%r10), %r13
	movq	-112(%rbp), %rsi
	cmpq	%rdi, %r13
	cmovbe	%r13, %rdi
	andq	%r13, %rsi
	addq	%r12, %rsi
	movq	(%rsi), %rdx
	movzbl	(%rsi,%r15), %r11d
	movq	%rdi, -144(%rbp)
	movq	%rax, %rdi
	shrq	$3, %rax
	andl	$7, %edi
	imulq	%r14, %rdx
	movq	%rax, -184(%rbp)
	movq	-136(%rbp), %rax
	movq	%rdi, -168(%rbp)
	shrq	$48, %rdx
	movq	%rdx, -120(%rbp)
	cmpq	%rax, %r13
	jbe	.L421
	andq	-72(%rbp), %rax
	addq	%r12, %rax
	cmpb	(%rax,%r15), %r11b
	je	.L569
.L421:
	movq	%rdi, %rax
	movq	$0, -80(%rbp)
	movl	$2020, %r9d
	subq	$1, %rax
	movq	%rax, -248(%rbp)
.L347:
	movq	-152(%rbp), %rax
	leaq	1(%rsi), %rdx
	movq	-120(%rbp), %rdi
	movq	%r10, -104(%rbp)
	movq	%rdx, -280(%rbp)
	leaq	8(%rsi), %rdx
	leaq	(%rax,%rdi,4), %rdi
	movq	%rdx, -272(%rbp)
	movq	%rsi, -176(%rbp)
	movl	(%rdi), %eax
	leaq	4(%rdi), %rcx
	addq	$12, %rdi
	movq	%rdi, %r8
.L361:
	movq	-72(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r12, %rdx
	movzbl	(%rdx,%r15), %esi
	cmpl	%r11d, %esi
	jne	.L354
	movq	%r13, %rdi
	subq	%rax, %rdi
	movq	%rdi, -224(%rbp)
	je	.L354
	movq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	ja	.L354
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L355
	movq	%rax, -288(%rbp)
	movq	-104(%rbp), %r10
	xorl	%esi, %esi
.L357:
	movq	-176(%rbp), %rax
	movq	(%rdx,%rsi), %rdi
	movq	(%rax,%rsi), %rax
	cmpq	%rdi, %rax
	je	.L570
	xorq	%rdi, %rax
	movq	%r10, -104(%rbp)
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
.L359:
	cmpq	$3, %rax
	jbe	.L354
	leaq	(%rax,%rax,8), %rsi
	movq	-224(%rbp), %rdi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%edi, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	%r9, %rdx
	jbe	.L354
	movq	-176(%rbp), %rsi
	movq	%rdi, -80(%rbp)
	movq	%rax, %r15
	movq	%rdx, %r9
	movzbl	(%rsi,%rax), %r11d
	.p2align 4,,10
	.p2align 3
.L354:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	%r8, %rcx
	jne	.L361
	movq	%r13, %rax
	movq	-104(%rbp), %r10
	shrq	$3, %rax
	andl	$1, %eax
	addq	-120(%rbp), %rax
	movl	%r13d, 40(%rbx,%rax,4)
	movq	-64(%rbp), %rax
	addq	$175, %rax
	cmpq	%r9, %rax
	ja	.L362
	addq	$1, -48(%rbp)
	cmpq	-208(%rbp), %r13
	je	.L363
	addq	$1, -136(%rbp)
	leaq	9(%r10), %rax
	cmpq	%rax, -88(%rbp)
	ja	.L571
.L363:
	movq	-264(%rbp), %rax
	movq	-144(%rbp), %rdi
	addq	%r13, %rax
	leaq	(%rax,%r15,2), %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rdi, %rax
	jbe	.L572
	leaq	15(%rax), %rdx
.L396:
	movq	-160(%rbp), %rax
	movl	-48(%rbp), %ecx
	leaq	16(%rax), %rdi
	movl	%ecx, (%rax)
	movl	%r15d, 4(%rax)
	movq	-216(%rbp), %rax
	movq	%rdi, -56(%rbp)
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r11
	cmpq	%rcx, %rdx
	jb	.L573
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r11,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L374:
	movq	-160(%rbp), %rdi
	cmpq	$5, -48(%rbp)
	movw	%ax, 14(%rdi)
	movslq	%r15d, %rax
	movl	%edx, 8(%rdi)
	jbe	.L574
	cmpq	$129, -48(%rbp)
	jbe	.L575
	cmpq	$2113, -48(%rbp)
	jbe	.L576
	movq	-48(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L433
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L379
.L583:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L577
	.p2align 4,,10
	.p2align 3
.L383:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L385:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L391:
	movq	-240(%rbp), %rdi
	leaq	9(%r11), %rsi
	cmpq	%rdi, %rsi
	movq	%rdi, %rdx
	cmovbe	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L436
	movq	-48(%rbp), %rcx
	movq	-112(%rbp), %r8
	leaq	2(%rcx), %rdi
	subq	%r11, %rdi
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r8, %rcx
	andq	%rax, %rcx
	movq	(%r12,%rcx), %rsi
	imulq	%r14, %rsi
	movq	%rsi, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	shrq	$48, %rcx
	andl	$1, %esi
	addq	%rsi, %rcx
	movq	%rax, %rsi
	movl	%eax, 40(%rbx,%rcx,4)
	leaq	(%rdi,%rax), %rcx
	addq	$2, %rax
	cmpq	%rdx, %rax
	jb	.L393
	movq	%rcx, -48(%rbp)
	addq	$10, %rsi
	movq	%rax, %r11
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L566:
	testq	%r13, %r13
	je	.L331
	leaq	8(%rsi), %rdi
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movq	%rdi, -56(%rbp)
.L333:
	movq	(%rsi,%rcx), %rdx
	movq	(%rax,%rcx), %rdi
	cmpq	%rdi, %rdx
	je	.L578
	xorq	%rdi, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%rcx, %rax
	movq	%rax, -56(%rbp)
	movq	-104(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -168(%rbp)
.L335:
	movq	-56(%rbp), %rcx
	cmpq	$3, %rcx
	jbe	.L415
	leaq	(%rcx,%rcx,8), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	addq	$1935, %rax
	movq	%rax, -64(%rbp)
	cmpq	$2020, %rax
	jbe	.L415
	movzbl	(%rsi,%rcx), %r9d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L415:
	movq	$0, -96(%rbp)
	movq	$0, -56(%rbp)
	movq	$2020, -64(%rbp)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L572:
	movq	16(%rbp), %rcx
	movslq	(%rcx), %rdx
	movslq	4(%rcx), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rdx, %rax
	je	.L366
	cmpq	%rcx, %rax
	je	.L579
	addq	$3, %rax
	movq	%rax, %r9
	subq	%rdx, %r9
	cmpq	$6, %r9
	jbe	.L580
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L581
	movq	16(%rbp), %rax
	movq	-80(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rdx, %rcx
	je	.L431
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rdx, %rcx
	je	.L582
	leaq	15(%rcx), %rdx
.L370:
	testq	%rdx, %rdx
	je	.L366
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L368:
	movq	16(%rbp), %rcx
	movl	%edi, 8(%rcx)
	movl	-80(%rbp), %edi
	movl	%eax, 12(%rcx)
	movl	%esi, 4(%rcx)
	movl	%edi, (%rcx)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L338:
	cmpq	$0, -104(%rbp)
	je	.L337
	movzbl	-184(%rbp), %eax
	cmpb	(%rdx), %al
	jne	.L337
	movq	-192(%rbp), %rax
	cmpq	$0, -168(%rbp)
	movq	%rax, -208(%rbp)
	je	.L337
	movq	$1, -200(%rbp)
	movq	-80(%rbp), %rax
	movzbl	1(%rax), %eax
	cmpb	%al, 1(%rdx)
	jne	.L337
.L403:
	movq	-200(%rbp), %rsi
	movq	-104(%rbp), %r10
	leaq	1(%rsi), %rax
	cmpq	$2, %r10
	je	.L342
	movq	-200(%rbp), %rsi
	movq	-208(%rbp), %r8
	movzbl	1(%rsi,%rdx), %esi
	cmpb	%sil, 1(%r8)
	jne	.L342
	movq	-200(%rbp), %rsi
	leaq	2(%rsi), %rax
	cmpq	$3, %r10
	je	.L342
	movq	-200(%rbp), %rsi
	movzbl	2(%rsi,%rdx), %esi
	cmpb	%sil, 2(%r8)
	jne	.L342
	movq	-200(%rbp), %rsi
	leaq	3(%rsi), %rax
	cmpq	$4, %r10
	je	.L342
	movq	-200(%rbp), %rsi
	movzbl	3(%rsi,%rdx), %esi
	cmpb	%sil, 3(%r8)
	jne	.L342
	movq	-200(%rbp), %rsi
	leaq	4(%rsi), %rax
	cmpq	$5, %r10
	je	.L342
	movq	-200(%rbp), %rsi
	movzbl	4(%rsi,%rdx), %esi
	cmpb	%sil, 4(%r8)
	jne	.L342
	movq	-200(%rbp), %rsi
	leaq	5(%rsi), %rax
	cmpq	$6, %r10
	je	.L342
	movq	-200(%rbp), %rsi
	movzbl	5(%rsi,%rdx), %esi
	cmpb	%sil, 5(%r8)
	jne	.L342
	movq	-200(%rbp), %rsi
	leaq	6(%rsi), %rax
	cmpq	$7, %r10
	je	.L342
	movq	-200(%rbp), %r10
	movzbl	6(%r8), %esi
	cmpb	%sil, 6(%r10,%rdx)
	jne	.L342
	movq	%r10, %rax
	addq	$7, %rax
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L574:
	movq	-48(%rbp), %rdi
	movl	%edi, %ecx
	movl	%edi, %edx
	leal	0(,%rcx,8), %edi
	andl	$56, %edi
.L376:
	cmpq	$9, %rax
	jbe	.L583
.L379:
	cmpq	$133, %rax
	jbe	.L584
	cmpq	$2117, %rax
	ja	.L382
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L380:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L383
.L577:
	testb	%sil, %sil
	je	.L383
	cmpw	$15, %ax
	ja	.L383
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L386:
	movq	-160(%rbp), %rax
	leaq	0(%r13,%r15), %r11
	movq	-48(%rbp), %rcx
	movq	%r15, %rdx
	movw	%di, 12(%rax)
	movq	-256(%rbp), %rdi
	movq	48(%rbp), %rax
	addq	%rcx, (%rax)
	cmpq	%rdi, %r11
	leaq	2(%r13), %rax
	movq	%rdi, %rcx
	movq	-80(%rbp), %rdi
	cmovbe	%r11, %rcx
	shrq	$2, %rdx
	cmpq	%rdx, %rdi
	jnb	.L387
	movq	%rdi, %rdx
	movq	%r11, %rdi
	salq	$2, %rdx
	subq	%rdx, %rdi
	cmpq	%rax, %rdi
	cmovnb	%rdi, %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
.L387:
	leaq	8(%r11), %rsi
	cmpq	%rcx, %rax
	jnb	.L435
	movq	-112(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L389:
	movq	%r8, %rdx
	andq	%rax, %rdx
	movq	(%r12,%rdx), %rdi
	imulq	%r14, %rdi
	movq	%rdi, %rdx
	movq	%rax, %rdi
	shrq	$3, %rdi
	shrq	$48, %rdx
	andl	$1, %edi
	addq	%rdi, %rdx
	movl	%eax, 40(%rbx,%rdx,4)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L389
.L435:
	movq	-56(%rbp), %rax
	movq	$0, -48(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L366:
	movq	-160(%rbp), %rax
	movl	-48(%rbp), %edi
	movl	$1, %esi
	xorl	%edx, %edx
	leaq	16(%rax), %rcx
	movl	%edi, (%rax)
	movl	%r15d, 4(%rax)
	xorl	%eax, %eax
	movq	%rcx, -56(%rbp)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L567:
	subq	$1, -200(%rbp)
	movq	-176(%rbp), %rax
	leaq	(%rax,%rsi), %r8
	leaq	8(%rsi), %rax
	je	.L585
	movq	%rax, %rsi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L571:
	movq	-80(%rbp), %rax
	movq	%r9, -64(%rbp)
	movq	%r13, %r10
	movq	%r15, -56(%rbp)
	movq	%rax, -96(%rbp)
	movq	-200(%rbp), %rax
	jmp	.L364
.L580:
	leal	0(,%r9,4), %ecx
	movl	$158663784, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-48(%rbp), %rcx
	leaq	-2(%rcx), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L584:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L569:
	movq	-184(%rbp), %r8
	testq	%r8, %r8
	je	.L348
	xorl	%ecx, %ecx
	leaq	8(%rsi), %r9
.L350:
	movq	(%rsi,%rcx), %rdx
	movq	(%rax,%rcx), %rdi
	cmpq	%rdi, %rdx
	je	.L586
	movq	-168(%rbp), %rax
	xorq	%rdi, %rdx
	rep bsfq	%rdx, %rdx
	subq	$1, %rax
	movslq	%edx, %rdx
	movq	%rax, -248(%rbp)
	shrq	$3, %rdx
	addq	%rcx, %rdx
.L352:
	cmpq	$3, %rdx
	jbe	.L425
	leaq	(%rdx,%rdx,8), %rcx
	movq	%rcx, %rax
	salq	$4, %rax
	subq	%rcx, %rax
	leaq	1935(%rax), %r9
	cmpq	$2020, %r9
	ja	.L587
.L425:
	movq	$0, -80(%rbp)
.L564:
	movl	$2020, %r9d
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L382:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L576:
	movq	-48(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L355:
	cmpq	$0, -168(%rbp)
	je	.L354
	movq	-176(%rbp), %rax
	movzbl	(%rdx), %edi
	cmpb	%dil, (%rax)
	jne	.L354
	movq	-280(%rbp), %rax
	cmpq	$0, -248(%rbp)
	movq	%rax, -288(%rbp)
	je	.L354
	movq	-176(%rbp), %rax
	movl	$1, %r10d
	movzbl	1(%rax), %eax
	cmpb	%al, 1(%rdx)
	jne	.L354
.L401:
	cmpq	$2, -168(%rbp)
	leaq	1(%r10), %rax
	je	.L359
	movq	-288(%rbp), %rdi
	movzbl	1(%r10,%rdx), %esi
	cmpb	%sil, 1(%rdi)
	jne	.L359
	cmpq	$3, -168(%rbp)
	leaq	2(%r10), %rax
	je	.L359
	movzbl	2(%r10,%rdx), %esi
	cmpb	%sil, 2(%rdi)
	jne	.L359
	cmpq	$4, -168(%rbp)
	leaq	3(%r10), %rax
	je	.L359
	movzbl	3(%r10,%rdx), %esi
	cmpb	%sil, 3(%rdi)
	jne	.L359
	cmpq	$5, -168(%rbp)
	leaq	4(%r10), %rax
	je	.L359
	movzbl	4(%r10,%rdx), %esi
	cmpb	%sil, 4(%rdi)
	jne	.L359
	cmpq	$6, -168(%rbp)
	leaq	5(%r10), %rax
	je	.L359
	movzbl	5(%r10,%rdx), %esi
	cmpb	%sil, 5(%rdi)
	jne	.L359
	cmpq	$7, -168(%rbp)
	leaq	6(%r10), %rax
	je	.L359
	movzbl	6(%rdi), %edi
	cmpb	%dil, 6(%r10,%rdx)
	jne	.L359
	leaq	7(%r10), %rax
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L570:
	subq	$1, -288(%rbp)
	movq	-272(%rbp), %rax
	leaq	(%rax,%rsi), %rdi
	leaq	8(%rsi), %rax
	je	.L588
	movq	%rax, %rsi
	jmp	.L357
.L433:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L376
.L587:
	movq	-296(%rbp), %rax
	movzbl	(%rsi,%rdx), %r11d
	movq	%rdx, %r15
	movq	%rax, -80(%rbp)
	jmp	.L347
.L579:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L368
.L436:
	movq	%rcx, -48(%rbp)
	movq	%rax, %r11
	jmp	.L388
.L581:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L370
.L331:
	movq	%rdx, %rcx
	testq	%rdx, %rdx
	je	.L411
	subq	$1, %rcx
	leaq	1(%rsi), %rdi
	movq	%rcx, -168(%rbp)
	je	.L415
	movzbl	1(%rsi), %ecx
	movl	$1, %edx
	cmpb	%cl, 1(%rax)
	jne	.L415
.L404:
	leaq	1(%rdx), %rcx
	movq	%rcx, -56(%rbp)
	movq	-168(%rbp), %rcx
	cmpq	$1, %rcx
	je	.L335
	movzbl	1(%rax,%rdx), %r8d
	cmpb	%r8b, 1(%rdi)
	jne	.L335
	leaq	2(%rdx), %r8
	movq	%r8, -56(%rbp)
	cmpq	$2, %rcx
	je	.L335
	movzbl	2(%rax,%rdx), %r8d
	cmpb	%r8b, 2(%rdi)
	jne	.L335
	leaq	3(%rdx), %r8
	movq	%r8, -56(%rbp)
	cmpq	$3, %rcx
	je	.L335
	movzbl	3(%rax,%rdx), %r8d
	cmpb	%r8b, 3(%rdi)
	jne	.L335
	leaq	4(%rdx), %r8
	movq	%r8, -56(%rbp)
	subq	$4, %rcx
	je	.L335
	movzbl	4(%rax,%rdx), %r8d
	cmpb	%r8b, 4(%rdi)
	jne	.L335
	leaq	5(%rdx), %r8
	movq	%r8, -56(%rbp)
	cmpq	$1, %rcx
	je	.L335
	movzbl	5(%rax,%rdx), %eax
	cmpb	%al, 5(%rdi)
	jne	.L335
	leaq	6(%rdx), %rax
	movq	%rax, -56(%rbp)
	jmp	.L335
.L578:
	movq	-56(%rbp), %rdi
	leaq	8(%rcx), %rdx
	addq	%rcx, %rdi
	subq	$1, %r8
	je	.L589
	movq	%rdx, %rcx
	jmp	.L333
.L407:
	xorl	%edx, %edx
	jmp	.L329
.L362:
	movq	-128(%rbp), %rax
	movq	-56(%rbp), %r15
	movq	%r10, %r13
	cmpq	%rax, %r10
	cmovbe	%r10, %rax
	movq	%rax, -144(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L363
.L585:
	cmpq	$0, -104(%rbp)
	movq	%r10, -80(%rbp)
	je	.L342
	movzbl	(%rdx,%rax), %r10d
	cmpb	%r10b, (%r8)
	jne	.L342
	leaq	1(%r8), %r10
	leaq	9(%rsi), %rax
	cmpq	$0, -168(%rbp)
	movq	%r10, -208(%rbp)
	movq	%rax, -200(%rbp)
	je	.L342
	movzbl	9(%rsi,%rdx), %esi
	cmpb	%sil, 1(%r8)
	je	.L403
	movq	-200(%rbp), %rax
	jmp	.L342
.L586:
	leaq	(%r9,%rcx), %rdi
	leaq	8(%rcx), %rdx
	subq	$1, %r8
	je	.L590
	movq	%rdx, %rcx
	jmp	.L350
.L348:
	cmpq	$0, -168(%rbp)
	je	.L423
	movzbl	(%rax), %edi
	cmpb	%dil, (%rsi)
	jne	.L591
	movq	-168(%rbp), %rcx
	leaq	1(%rsi), %rdi
	subq	$1, %rcx
	movq	%rcx, -248(%rbp)
	movq	%rcx, -80(%rbp)
	je	.L564
	movzbl	1(%rax), %edx
	movl	$1, %ecx
	cmpb	%dl, 1(%rsi)
	jne	.L425
.L402:
	movq	-248(%rbp), %r9
	leaq	1(%rcx), %rdx
	cmpq	$1, %r9
	je	.L352
	movzbl	1(%rcx,%rax), %r8d
	cmpb	%r8b, 1(%rdi)
	jne	.L352
	leaq	2(%rcx), %rdx
	cmpq	$2, %r9
	je	.L352
	movzbl	2(%rcx,%rax), %r8d
	cmpb	%r8b, 2(%rdi)
	jne	.L352
	leaq	3(%rcx), %rdx
	cmpq	$3, %r9
	je	.L352
	movzbl	3(%rcx,%rax), %r8d
	cmpb	%r8b, 3(%rdi)
	jne	.L352
	subq	$4, %r9
	leaq	4(%rcx), %rdx
	movq	%r9, %r8
	je	.L352
	movzbl	4(%rcx,%rax), %r9d
	cmpb	%r9b, 4(%rdi)
	jne	.L352
	leaq	5(%rcx), %rdx
	cmpq	$1, %r8
	je	.L352
	movzbl	5(%rcx,%rax), %eax
	cmpb	%al, 5(%rdi)
	jne	.L352
	leaq	6(%rcx), %rdx
	jmp	.L352
.L431:
	movl	$2, %edx
	jmp	.L368
.L582:
	movl	$3, %edx
	jmp	.L368
.L588:
	cmpq	$0, -168(%rbp)
	movq	%rdi, -288(%rbp)
	movq	%r10, -104(%rbp)
	je	.L359
	movzbl	(%rdx,%rax), %r10d
	cmpb	%r10b, (%rdi)
	jne	.L359
	movq	%rdi, %rax
	cmpq	$0, -248(%rbp)
	leaq	1(%rdi), %rdi
	movq	%rdi, -288(%rbp)
	leaq	9(%rsi), %r10
	je	.L428
	movzbl	9(%rsi,%rdx), %edi
	cmpb	%dil, 1(%rax)
	je	.L401
.L428:
	movq	%r10, %rax
	jmp	.L359
.L589:
	cmpq	$0, -104(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rdi, -64(%rbp)
	je	.L592
	movzbl	(%rax,%rdx), %edx
	cmpb	%dl, (%rdi)
	jne	.L593
	movq	-104(%rbp), %r8
	leaq	1(%rdi), %rdi
	leaq	9(%rcx), %rdx
	subq	$1, %r8
	movq	%r8, -168(%rbp)
	je	.L413
	movq	-64(%rbp), %r8
	movzbl	9(%rax,%rcx), %ecx
	cmpb	%cl, 1(%r8)
	je	.L404
.L413:
	movq	%rdx, -56(%rbp)
	jmp	.L335
.L411:
	movq	$0, -96(%rbp)
	movq	$0, -56(%rbp)
	movq	$2020, -64(%rbp)
	movq	$-1, -168(%rbp)
	jmp	.L330
.L590:
	cmpq	$0, -168(%rbp)
	je	.L594
	movzbl	(%rax,%rdx), %r9d
	cmpb	%r9b, (%rdi)
	jne	.L595
	movq	-168(%rbp), %r8
	movq	%rdi, %r9
	leaq	9(%rcx), %rdx
	leaq	1(%rdi), %rdi
	subq	$1, %r8
	movq	%r8, -248(%rbp)
	je	.L352
	movzbl	9(%rcx,%rax), %ecx
	cmpb	%cl, 1(%r9)
	jne	.L352
	movq	%rdx, %rcx
	jmp	.L402
.L591:
	movq	-168(%rbp), %rax
	movq	$0, -80(%rbp)
	movl	$2020, %r9d
	subq	$1, %rax
	movq	%rax, -248(%rbp)
	jmp	.L347
.L423:
	movq	$0, -80(%rbp)
	movl	$2020, %r9d
	movq	$-1, -248(%rbp)
	jmp	.L347
.L593:
	movq	-104(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -168(%rbp)
	jmp	.L335
.L573:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L374
.L592:
	movq	$-1, -168(%rbp)
	jmp	.L335
.L595:
	movq	-168(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -248(%rbp)
	jmp	.L352
.L594:
	movq	$-1, -248(%rbp)
	jmp	.L352
	.cfi_endproc
.LFE281:
	.size	CreateBackwardReferencesNH3, .-CreateBackwardReferencesNH3
	.p2align 4
	.type	CreateBackwardReferencesNH4, @function
CreateBackwardReferencesNH4:
.LFB282:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movl	$1, %eax
	movq	%rdx, %r11
	movl	$256, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	addq	%rdi, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -120(%rbp)
	movq	%r8, -112(%rbp)
	movl	8(%r8), %ecx
	movq	%rsi, -88(%rbp)
	salq	%cl, %rax
	subq	$16, %rax
	cmpq	$7, %rdi
	movq	%rax, -144(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-7(%rsi), %rax
	cmovbe	%r13, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -280(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -240(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movq	%rax, -288(%rbp)
	addq	%r13, %rax
	movq	%rax, -136(%rbp)
	leaq	8(%r13), %rax
	cmpq	%rax, %rsi
	jbe	.L698
	leaq	40(%r9), %rax
	movq	%r9, %rbx
	movq	%r11, %r15
	movq	%rax, -152(%rbp)
	movl	%r10d, %eax
	movq	%rax, -48(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L682:
	movq	-144(%rbp), %rax
	movq	-88(%rbp), %rdi
	movabsq	$-4819355556693147648, %r14
	movq	-120(%rbp), %rdx
	subq	%r13, %rdi
	cmpq	%r13, %rax
	cmova	%r13, %rax
	movq	%rdx, %rsi
	movq	%rdi, -160(%rbp)
	andq	%r13, %rsi
	movq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	addq	%r15, %rsi
	movzbl	(%rsi), %r9d
	movq	72(%rax), %rax
	movb	%r9b, -192(%rbp)
	movq	%rax, -216(%rbp)
	movq	(%rsi), %rax
	imulq	%rax, %r14
	movq	%rax, -208(%rbp)
	movq	16(%rbp), %rax
	movslq	(%rax), %rax
	shrq	$47, %r14
	movq	%rax, %rcx
	movq	%rax, -104(%rbp)
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdi, %rcx
	shrq	$3, %rdi
	movq	%r14, -72(%rbp)
	andl	$7, %ecx
	movq	%rdi, %r11
	movq	%rcx, -96(%rbp)
	cmpq	%r13, %rax
	jnb	.L700
	andl	%edx, %eax
	addq	%r15, %rax
	cmpb	%r9b, (%rax)
	je	.L919
.L700:
	movq	%rcx, %rax
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
	subq	$1, %rax
	movq	$2020, -56(%rbp)
	movq	%rax, -176(%rbp)
.L600:
	leaq	1(%rsi), %rdx
	movq	-152(%rbp), %rax
	movq	-72(%rbp), %rdi
	movq	%rbx, -128(%rbp)
	movq	%rdx, -200(%rbp)
	leaq	8(%rsi), %rdx
	movq	%rdx, -184(%rbp)
	leaq	(%rax,%rdi,4), %rdi
	movl	(%rdi), %eax
	leaq	4(%rdi), %rcx
	addq	$20, %rdi
.L614:
	movq	-48(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r15, %rdx
	movzbl	(%rdx,%r14), %r8d
	cmpl	%r9d, %r8d
	jne	.L607
	movq	%r13, %r10
	subq	%rax, %r10
	je	.L607
	cmpq	-80(%rbp), %r10
	ja	.L607
	testq	%r11, %r11
	je	.L608
	movq	%r11, %r12
	xorl	%r8d, %r8d
.L610:
	movq	(%rsi,%r8), %rax
	movq	(%rdx,%r8), %rbx
	cmpq	%rbx, %rax
	je	.L920
	xorq	%rbx, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%r8, %rax
.L612:
	cmpq	$3, %rax
	jbe	.L607
	leaq	(%rax,%rax,8), %r8
	movq	%r8, %rdx
	salq	$4, %rdx
	subq	%r8, %rdx
	bsrl	%r10d, %r8d
	imull	$30, %r8d, %r8d
	addq	$1920, %rdx
	subq	%r8, %rdx
	cmpq	-56(%rbp), %rdx
	jbe	.L607
	movq	%r10, -104(%rbp)
	movzbl	(%rsi,%rax), %r9d
	movq	%rax, %r14
	movq	%rdx, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L607:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	%rdi, %rcx
	jne	.L614
	movq	%r13, %rax
	movq	-72(%rbp), %r12
	movq	-128(%rbp), %rbx
	movl	%r13d, %edx
	shrq	$3, %rax
	andl	$3, %eax
	addq	%rax, %r12
	cmpq	$2020, -56(%rbp)
	je	.L921
	movl	%r13d, 40(%rbx,%r12,4)
	movl	$0, -292(%rbp)
.L684:
	movq	-160(%rbp), %rax
	movq	%r14, -232(%rbp)
	movq	%r13, -160(%rbp)
	subq	$1, %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -176(%rbp)
	movq	-112(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, -208(%rbp)
	movq	16(%rbp), %rax
	movslq	(%rax), %rax
	movq	%rax, %rsi
	movq	%rax, -304(%rbp)
	movl	$1, %eax
	subq	%rsi, %rax
	addq	%r13, %rax
	movq	%rax, -184(%rbp)
	leaq	4(%r13), %rax
	movq	%rax, -224(%rbp)
.L652:
	xorl	%r9d, %r9d
	cmpl	$4, -208(%rbp)
	jg	.L629
	movq	-232(%rbp), %rax
	leaq	-1(%rax), %r9
	movq	-176(%rbp), %rax
	cmpq	%rax, %r9
	cmova	%rax, %r9
.L629:
	movq	-160(%rbp), %rax
	movq	-120(%rbp), %rsi
	leaq	1(%rax), %r12
	movq	-144(%rbp), %rax
	cmpq	%rax, %r12
	cmovbe	%r12, %rax
	andq	%r12, %rsi
	addq	%r15, %rsi
	movq	%rax, -96(%rbp)
	movq	(%rsi), %rax
	movzbl	(%rsi,%r9), %r10d
	movq	%rax, %rbx
	movq	%rax, -272(%rbp)
	movabsq	$-4819355556693147648, %rax
	imulq	%rbx, %rax
	movq	-176(%rbp), %rbx
	movq	%rbx, %r14
	andl	$7, %ebx
	movq	%rbx, -192(%rbp)
	shrq	$47, %rax
	shrq	$3, %r14
	movq	-184(%rbp), %rbx
	movq	%rax, -128(%rbp)
	cmpq	%rbx, %r12
	jbe	.L727
	movq	-48(%rbp), %rdx
	andq	%rbx, %rdx
	addq	%r15, %rdx
	cmpb	(%rdx,%r9), %r10b
	je	.L922
.L727:
	movq	-192(%rbp), %rax
	movq	$0, -80(%rbp)
	movl	$2020, %ebx
	subq	$1, %rax
	movq	%rax, -248(%rbp)
.L630:
	leaq	1(%rsi), %rdx
	movq	-152(%rbp), %rax
	movq	-128(%rbp), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdx, -264(%rbp)
	leaq	8(%rsi), %rdx
	movq	%rdx, -256(%rbp)
	leaq	(%rax,%rdi,4), %rdi
	movl	(%rdi), %eax
	leaq	4(%rdi), %rcx
	addq	$20, %rdi
.L644:
	movq	-48(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r15, %rdx
	movzbl	(%rdx,%r9), %r8d
	cmpl	%r10d, %r8d
	jne	.L637
	movq	%r12, %r13
	subq	%rax, %r13
	je	.L637
	cmpq	-96(%rbp), %r13
	ja	.L637
	testq	%r14, %r14
	je	.L638
	movq	%r14, -200(%rbp)
	movq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
.L640:
	movq	(%rsi,%r8), %rax
	movq	(%rdx,%r8), %r11
	cmpq	%r11, %rax
	je	.L923
	xorq	%r11, %rax
	movq	%rsi, -72(%rbp)
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%r8, %rax
.L642:
	cmpq	$3, %rax
	jbe	.L637
	leaq	(%rax,%rax,8), %r8
	movq	%r8, %rdx
	salq	$4, %rdx
	subq	%r8, %rdx
	bsrl	%r13d, %r8d
	imull	$30, %r8d, %r8d
	addq	$1920, %rdx
	subq	%r8, %rdx
	cmpq	%rbx, %rdx
	jbe	.L637
	movq	-72(%rbp), %rbx
	movq	%r13, -80(%rbp)
	movq	%rax, %r9
	movzbl	(%rbx,%rax), %r10d
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L637:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	%rdi, %rcx
	jne	.L644
	movq	-72(%rbp), %rsi
	xorl	%r11d, %r11d
	cmpq	$2020, %rbx
	je	.L924
.L645:
	movq	%r12, %rax
	movq	-136(%rbp), %rsi
	shrq	$3, %rax
	andl	$3, %eax
	addq	-128(%rbp), %rax
	movl	%r12d, 40(%rsi,%rax,4)
	movq	-56(%rbp), %rax
	addq	$175, %rax
	cmpq	%rbx, %rax
	ja	.L650
	addq	$1, -64(%rbp)
	cmpq	-224(%rbp), %r12
	je	.L909
	movq	-160(%rbp), %rax
	addq	$1, -184(%rbp)
	addq	$9, %rax
	cmpq	%rax, -88(%rbp)
	ja	.L925
.L909:
	movq	%rsi, %rbx
.L651:
	movq	-288(%rbp), %rax
	movq	-96(%rbp), %rsi
	addq	%r12, %rax
	leaq	(%rax,%r9,2), %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L926
	leaq	15(%rax), %rdx
.L685:
	movq	-168(%rbp), %rsi
	leaq	16(%rsi), %rax
	movq	%rax, -56(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, (%rsi)
	movl	%r11d, %eax
	sall	$25, %eax
	orl	%r9d, %eax
	movl	%eax, 4(%rsi)
	movq	-112(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r14
	cmpq	%rdx, %rcx
	ja	.L927
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %eax
	movq	%rdx, %rsi
	subl	$1, %eax
	movl	%eax, %ecx
	movq	%rax, %r13
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r13d, %ecx
	salq	%cl, %rsi
	leal	16(%r14,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L662:
	movq	-168(%rbp), %rdi
	cmpq	$5, -64(%rbp)
	movw	%ax, 14(%rdi)
	leal	(%r11,%r9), %eax
	movl	%edx, 8(%rdi)
	cltq
	jbe	.L928
	cmpq	$129, -64(%rbp)
	jbe	.L929
	cmpq	$2113, -64(%rbp)
	jbe	.L930
	movq	-64(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L750
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L667
.L940:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L931
	.p2align 4,,10
	.p2align 3
.L671:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L673:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L921:
	movq	24(%rbx), %r8
	movq	32(%rbx), %rax
	movq	%r8, %rcx
	shrq	$7, %rcx
	cmpq	%rcx, %rax
	jb	.L916
	imull	$506832829, (%rsi), %ecx
	movq	-112(%rbp), %rdi
	addq	$1, %r8
	movq	104(%rdi), %rdi
	shrl	$18, %ecx
	addl	%ecx, %ecx
	movzwl	(%rdi,%rcx,2), %ecx
	movq	%r8, 24(%rbx)
	movq	%rcx, %rdi
	testq	%rcx, %rcx
	je	.L916
	movl	%ecx, %r11d
	movq	%rcx, %r9
	andl	$31, %r11d
	andl	$31, %r9d
	movw	%r11w, -72(%rbp)
	cmpq	%r9, -160(%rbp)
	jb	.L916
	shrq	$5, %rcx
	movq	%r9, %r14
	movq	%rcx, %r11
	movq	%rcx, -56(%rbp)
	movq	-112(%rbp), %rcx
	imulq	%r9, %r11
	movq	80(%rcx), %r10
	movl	32(%r10,%r9,4), %ecx
	addq	%r11, %rcx
	addq	168(%r10), %rcx
	shrq	$3, %r14
	movq	%rcx, %r11
	je	.L620
	movq	(%rcx), %rcx
	xorl	%r8d, %r8d
	cmpq	%rcx, -208(%rbp)
	je	.L932
.L621:
	xorq	-208(%rbp), %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	leaq	(%rcx,%r8), %r14
.L622:
	movq	-112(%rbp), %rsi
	movl	88(%rsi), %ecx
	addq	%r14, %rcx
	cmpq	%rcx, %r9
	jnb	.L917
	testq	%r14, %r14
	je	.L917
.L689:
	movq	-112(%rbp), %rdi
	movq	%r9, %rsi
	subq	%r14, %rsi
	movq	96(%rdi), %rdi
	leal	(%rsi,%rsi,2), %ecx
	addl	%ecx, %ecx
	movq	%rdi, -96(%rbp)
	shrq	%cl, %rdi
	movq	%rdi, %rcx
	movq	-56(%rbp), %rdi
	andl	$63, %ecx
	leaq	(%rcx,%rsi,4), %rsi
	movzbl	(%r10,%r9), %ecx
	salq	%cl, %rsi
	movq	-80(%rbp), %rcx
	leaq	1(%rdi,%rcx), %rcx
	addq	%rcx, %rsi
	movq	%rsi, -104(%rbp)
	cmpq	%rsi, -216(%rbp)
	jb	.L917
	leaq	(%r14,%r14,8), %rcx
	movq	%rcx, %rsi
	salq	$4, %rsi
	subq	%rcx, %rsi
	bsrl	-104(%rbp), %ecx
	addq	$1920, %rsi
	imull	$30, %ecx, %ecx
	subq	%rcx, %rsi
	movq	%rsi, -56(%rbp)
	cmpq	$2019, %rsi
	jbe	.L917
	addq	$1, %rax
	cmpq	$2020, -56(%rbp)
	movq	%rax, 32(%rbx)
	movl	%edx, 40(%rbx,%r12,4)
	je	.L617
	movzwl	-72(%rbp), %r12d
	subl	%r14d, %r12d
	movl	%r12d, -292(%rbp)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L916:
	movl	%r13d, 40(%rbx,%r12,4)
.L617:
	movq	-64(%rbp), %rax
	leaq	1(%rax), %rdx
	leaq	1(%r13), %rax
	cmpq	-136(%rbp), %rax
	jbe	.L918
	movq	-88(%rbp), %rsi
	leaq	-7(%rsi), %rcx
	movq	-136(%rbp), %rsi
	addq	-240(%rbp), %rsi
	cmpq	%rax, %rsi
	jnb	.L679
	leaq	17(%r13), %rsi
	movq	-120(%rbp), %r9
	movabsq	$-4819355556693147648, %r8
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	-64(%rbp), %rsi
	addq	$4, %rsi
	subq	%r13, %rsi
	cmpq	%rcx, %rax
	jnb	.L918
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%r9, %rdx
	andq	%rax, %rdx
	movq	(%r15,%rdx), %rdi
	imulq	%r8, %rdi
	movq	%rdi, %rdx
	movq	%rax, %rdi
	shrq	$3, %rdi
	shrq	$47, %rdx
	andl	$3, %edi
	addq	%rdi, %rdx
	movq	%rax, %rdi
	movl	%eax, 40(%rbx,%rdx,4)
	leaq	(%rsi,%rax), %rdx
	addq	$4, %rax
	cmpq	%rcx, %rax
	jb	.L680
	movq	%rdx, -64(%rbp)
	addq	$12, %rdi
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L676:
	cmpq	%rdi, -88(%rbp)
	ja	.L682
	movq	-168(%rbp), %rdx
	subq	32(%rbp), %rdx
	sarq	$4, %rdx
.L599:
	movq	24(%rbp), %rbx
	movq	-88(%rbp), %rax
	addq	-64(%rbp), %rax
	subq	%r13, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	leaq	9(%r13), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %r13
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L608:
	cmpq	$0, -96(%rbp)
	je	.L607
	movzbl	-192(%rbp), %eax
	cmpb	(%rdx), %al
	jne	.L607
	cmpq	$0, -176(%rbp)
	movq	-200(%rbp), %r12
	je	.L607
	movzbl	1(%rsi), %eax
	movl	$1, %ebx
	cmpb	%al, 1(%rdx)
	jne	.L607
.L694:
	cmpq	$2, -96(%rbp)
	leaq	1(%rbx), %rax
	je	.L612
	movzbl	1(%rbx,%rdx), %r8d
	cmpb	%r8b, 1(%r12)
	jne	.L612
	cmpq	$3, -96(%rbp)
	leaq	2(%rbx), %rax
	je	.L612
	movzbl	2(%rbx,%rdx), %r8d
	cmpb	%r8b, 2(%r12)
	jne	.L612
	cmpq	$4, -96(%rbp)
	leaq	3(%rbx), %rax
	je	.L612
	movzbl	3(%rbx,%rdx), %r8d
	cmpb	%r8b, 3(%r12)
	jne	.L612
	cmpq	$5, -96(%rbp)
	leaq	4(%rbx), %rax
	je	.L612
	movzbl	4(%rbx,%rdx), %r8d
	cmpb	%r8b, 4(%r12)
	jne	.L612
	cmpq	$6, -96(%rbp)
	leaq	5(%rbx), %rax
	je	.L612
	movzbl	5(%rbx,%rdx), %r8d
	cmpb	%r8b, 5(%r12)
	jne	.L612
	cmpq	$7, -96(%rbp)
	leaq	6(%rbx), %rax
	je	.L612
	movzbl	6(%r12), %r8d
	cmpb	%r8b, 6(%rbx,%rdx)
	jne	.L612
	leaq	7(%rbx), %rax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	9(%r13), %rdi
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	cmpq	%rcx, %rax
	jnb	.L753
	movq	-64(%rbp), %rsi
	movq	-120(%rbp), %r10
	movabsq	$-4819355556693147648, %r8
	leaq	2(%rsi), %r9
	subq	%r13, %r9
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%r10, %rdx
	movq	%rax, %rdi
	andq	%rax, %rdx
	movq	(%r15,%rdx), %rsi
	imulq	%r8, %rsi
	movq	%rsi, %rdx
	movq	%rax, %rsi
	shrq	$3, %rsi
	shrq	$47, %rdx
	andl	$3, %esi
	addq	%rsi, %rdx
	movl	%eax, 40(%rbx,%rdx,4)
	leaq	(%r9,%rax), %rdx
	addq	$2, %rax
	cmpq	%rcx, %rax
	jb	.L681
	movq	%rdx, -64(%rbp)
	addq	$10, %rdi
	movq	%rax, %r13
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L920:
	movq	-184(%rbp), %rax
	leaq	(%rax,%r8), %rbx
	leaq	8(%r8), %rax
	subq	$1, %r12
	je	.L933
	movq	%rax, %r8
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L919:
	testq	%rdi, %rdi
	je	.L601
	xorl	%ecx, %ecx
	leaq	8(%rsi), %r8
.L603:
	movq	(%rsi,%rcx), %rdx
	movq	(%rax,%rcx), %r10
	cmpq	%r10, %rdx
	je	.L934
	xorq	%r10, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	leaq	(%rax,%rcx), %r14
	movq	-96(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -176(%rbp)
.L605:
	cmpq	$3, %r14
	jbe	.L704
	leaq	(%r14,%r14,8), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	addq	$1935, %rax
	movq	%rax, -56(%rbp)
	cmpq	$2020, %rax
	jbe	.L704
	movzbl	(%rsi,%r14), %r9d
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L704:
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
	movq	$2020, -56(%rbp)
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L926:
	movq	16(%rbp), %rsi
	movq	16(%rbp), %rdi
	movslq	(%rsi), %rdx
	movslq	4(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rax, %rdx
	je	.L654
	cmpq	%rax, %rcx
	je	.L935
	addq	$3, %rax
	movq	%rax, %r10
	subq	%rdx, %r10
	cmpq	$6, %r10
	jbe	.L936
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L937
	movq	16(%rbp), %rax
	movq	-80(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	je	.L748
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L938
	leaq	15(%rcx), %rdx
.L658:
	testq	%rdx, %rdx
	je	.L654
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L656:
	movq	16(%rbp), %rcx
	movl	%esi, 4(%rcx)
	movl	-80(%rbp), %esi
	movl	%eax, 12(%rcx)
	movl	%edi, 8(%rcx)
	movl	%esi, (%rcx)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L924:
	movq	-136(%rbp), %rdi
	movq	24(%rdi), %rdx
	movq	32(%rdi), %r8
	movq	%rdx, %rax
	shrq	$7, %rax
	cmpq	%rax, %r8
	jb	.L645
	imull	$506832829, (%rsi), %eax
	movq	-112(%rbp), %r14
	addq	$1, %rdx
	movq	104(%r14), %rcx
	shrl	$18, %eax
	addl	%eax, %eax
	movzwl	(%rcx,%rax,2), %ecx
	movq	%rdx, 24(%rdi)
	movq	%rcx, %rax
	testq	%rcx, %rcx
	je	.L645
	movl	%ecx, %edi
	movq	%rcx, %r10
	andl	$31, %edi
	andl	$31, %r10d
	movw	%di, -72(%rbp)
	cmpq	%r10, -176(%rbp)
	jb	.L645
	movq	%rcx, %rdi
	movq	80(%r14), %r13
	movq	%r10, %rcx
	shrq	$5, %rdi
	movq	%rdi, -192(%rbp)
	imulq	%r10, %rdi
	movl	32(%r13,%r10,4), %edx
	addq	%rdi, %rdx
	addq	168(%r13), %rdx
	shrq	$3, %rcx
	movq	%rdx, %r14
	je	.L646
	movq	(%rdx), %rdx
	xorl	%edi, %edi
	cmpq	%rdx, -272(%rbp)
	je	.L939
.L647:
	xorq	-272(%rbp), %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdi, %rdx
.L649:
	movq	-112(%rbp), %rsi
	movl	88(%rsi), %eax
	addq	%rdx, %rax
	cmpq	%rax, %r10
	jnb	.L744
	testq	%rdx, %rdx
	je	.L744
	movq	%r10, %rax
	movq	96(%rsi), %rsi
	movq	-96(%rbp), %rdi
	xorl	%r11d, %r11d
	subq	%rdx, %rax
	leal	(%rax,%rax,2), %ecx
	movq	%rsi, -200(%rbp)
	addl	%ecx, %ecx
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	movq	-192(%rbp), %rsi
	andl	$63, %ecx
	leaq	(%rcx,%rax,4), %rax
	movzbl	0(%r13,%r10), %ecx
	salq	%cl, %rax
	leaq	1(%rsi,%rdi), %rcx
	leaq	(%rax,%rcx), %rsi
	cmpq	%rsi, -216(%rbp)
	jb	.L645
	leaq	(%rdx,%rdx,8), %rcx
	movq	%rcx, %rax
	salq	$4, %rax
	subq	%rcx, %rax
	leaq	1920(%rax), %rcx
	bsrl	%esi, %eax
	imull	$30, %eax, %eax
	subq	%rax, %rcx
	cmpq	$2019, %rcx
	jbe	.L645
	movzwl	-72(%rbp), %r11d
	addq	$1, %r8
	movq	%rsi, -80(%rbp)
	movq	%rcx, %rbx
	movq	-136(%rbp), %rax
	movq	%rdx, %r9
	subl	%edx, %r11d
	movq	%r8, 32(%rax)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L928:
	movq	-64(%rbp), %rdi
	movl	%edi, %edx
	sall	$3, %edi
	andl	$56, %edi
.L664:
	cmpq	$9, %rax
	jbe	.L940
.L667:
	cmpq	$133, %rax
	jbe	.L941
	cmpq	$2117, %rax
	ja	.L670
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L668:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L671
.L931:
	testb	%sil, %sil
	je	.L671
	cmpw	$15, %ax
	ja	.L671
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L674:
	movq	-168(%rbp), %rax
	movq	-64(%rbp), %rsi
	leaq	(%r9,%r12), %r13
	movq	%r9, %rdx
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	movq	-80(%rbp), %rdi
	addq	%rsi, (%rax)
	movq	-280(%rbp), %rsi
	leaq	2(%r12), %rax
	cmpq	%rsi, %r13
	cmovbe	%r13, %rsi
	shrq	$2, %rdx
	cmpq	%rdi, %rdx
	jbe	.L675
	movq	%rdi, %rdx
	movq	%r13, %rdi
	salq	$2, %rdx
	subq	%rdx, %rdi
	cmpq	%rax, %rdi
	cmovnb	%rdi, %rax
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
.L675:
	leaq	8(%r13), %rdi
	cmpq	%rsi, %rax
	jnb	.L752
	movabsq	$-4819355556693147648, %r8
	movq	-120(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L677:
	movq	%r9, %rdx
	andq	%rax, %rdx
	movq	(%r15,%rdx), %rcx
	imulq	%r8, %rcx
	movq	%rcx, %rdx
	movq	%rax, %rcx
	shrq	$3, %rcx
	shrq	$47, %rdx
	andl	$3, %ecx
	addq	%rcx, %rdx
	movl	%eax, 40(%rbx,%rdx,4)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L677
.L752:
	movq	-56(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L654:
	movq	-168(%rbp), %rsi
	xorl	%edx, %edx
	leaq	16(%rsi), %rax
	movq	%rax, -56(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, (%rsi)
	movl	%r11d, %eax
	sall	$25, %eax
	orl	%r9d, %eax
	movl	%eax, 4(%rsi)
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L925:
	movq	-80(%rbp), %rax
	subq	$1, -176(%rbp)
	movl	%r11d, -292(%rbp)
	movq	%rbx, -56(%rbp)
	movq	%rax, -104(%rbp)
	movq	%r9, -232(%rbp)
	movq	%r12, -160(%rbp)
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L638:
	cmpq	$0, -192(%rbp)
	je	.L637
	movq	-72(%rbp), %rsi
	movzbl	(%rdx), %eax
	cmpb	%al, (%rsi)
	jne	.L637
	movq	-264(%rbp), %rax
	cmpq	$0, -248(%rbp)
	movq	%rax, -312(%rbp)
	je	.L637
	movq	$1, -200(%rbp)
	movzbl	1(%rdx), %eax
	cmpb	%al, 1(%rsi)
	jne	.L637
.L692:
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rsi
	leaq	1(%r8), %rax
	cmpq	$2, %rsi
	je	.L642
	movq	-200(%rbp), %r8
	movq	-312(%rbp), %r11
	movzbl	1(%r8,%rdx), %r8d
	cmpb	%r8b, 1(%r11)
	jne	.L642
	movq	-200(%rbp), %r8
	leaq	2(%r8), %rax
	cmpq	$3, %rsi
	je	.L642
	movq	-200(%rbp), %r8
	movzbl	2(%r8,%rdx), %r8d
	cmpb	%r8b, 2(%r11)
	jne	.L642
	movq	-200(%rbp), %r8
	leaq	3(%r8), %rax
	cmpq	$4, %rsi
	je	.L642
	movq	-200(%rbp), %r8
	movzbl	3(%r8,%rdx), %r8d
	cmpb	%r8b, 3(%r11)
	jne	.L642
	movq	-200(%rbp), %r8
	leaq	4(%r8), %rax
	cmpq	$5, %rsi
	je	.L642
	movq	-200(%rbp), %r8
	movzbl	4(%r8,%rdx), %r8d
	cmpb	%r8b, 4(%r11)
	jne	.L642
	movq	-200(%rbp), %r8
	leaq	5(%r8), %rax
	cmpq	$6, %rsi
	je	.L642
	movq	-200(%rbp), %r8
	movzbl	5(%r8,%rdx), %r8d
	cmpb	%r8b, 5(%r11)
	jne	.L642
	movq	-200(%rbp), %r8
	leaq	6(%r8), %rax
	cmpq	$7, %rsi
	je	.L642
	movq	-200(%rbp), %r8
	movzbl	6(%r11), %r11d
	cmpb	%r11b, 6(%r8,%rdx)
	jne	.L642
	movq	%r8, %rax
	addq	$7, %rax
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L923:
	subq	$1, -200(%rbp)
	movq	-256(%rbp), %rax
	leaq	(%rax,%r8), %r11
	leaq	8(%r8), %rax
	je	.L942
	movq	%rax, %r8
	jmp	.L640
.L936:
	leal	0(,%r10,4), %ecx
	movl	$158663784, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L658
.L932:
	leaq	8(%r11), %rcx
	subq	$1, %r14
	je	.L709
	movq	8(%rsi), %r8
	movq	8(%r11), %rcx
	movq	%r8, -208(%rbp)
	movl	$8, %r8d
	cmpq	%rcx, -208(%rbp)
	jne	.L621
	leaq	16(%r11), %rcx
	cmpq	$1, %r14
	je	.L711
	movq	16(%rsi), %r14
	movq	16(%r11), %rcx
	movl	$16, %r8d
	movq	%r14, -208(%rbp)
	cmpq	%rcx, %r14
	jne	.L621
	addq	$24, %r11
	movl	$24, %r14d
.L620:
	movq	%rdi, %r8
	andl	$7, %r8d
	andl	$7, %edi
	je	.L622
	movzbl	(%rsi,%r14), %edi
	cmpb	%dil, (%r11)
	jne	.L622
	leaq	1(%r14), %rcx
	cmpq	$1, %r8
	je	.L724
	movzbl	1(%r14,%rsi), %edi
	cmpb	%dil, 1(%r11)
	jne	.L724
	leaq	2(%r14), %rcx
	cmpq	$2, %r8
	je	.L724
	movzbl	2(%r14,%rsi), %edi
	cmpb	%dil, 2(%r11)
	jne	.L724
	leaq	3(%r14), %rcx
	cmpq	$3, %r8
	je	.L724
	movzbl	3(%r14,%rsi), %edi
	cmpb	%dil, 3(%r11)
	jne	.L724
	leaq	4(%r14), %rcx
	cmpq	$4, %r8
	je	.L724
	movzbl	4(%r14,%rsi), %edi
	cmpb	%dil, 4(%r11)
	jne	.L724
	leaq	5(%r14), %rcx
	subq	$5, %r8
	je	.L724
	movzbl	5(%r14,%rsi), %edi
	cmpb	%dil, 5(%r11)
	jne	.L724
	leaq	6(%r14), %rcx
	cmpq	$1, %r8
	je	.L724
	movzbl	6(%r14,%rsi), %esi
	cmpb	%sil, 6(%r11)
	jne	.L724
	addq	$7, %r14
.L623:
	movq	-112(%rbp), %rsi
	movl	88(%rsi), %ecx
	addq	%r14, %rcx
	cmpq	%rcx, %r9
	jb	.L689
	.p2align 4,,10
	.p2align 3
.L917:
	movl	%edx, 40(%rbx,%r12,4)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L922:
	testq	%r14, %r14
	je	.L631
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	8(%rsi), %r8
.L633:
	movq	(%rsi,%rcx), %rax
	movq	(%rdx,%rcx), %r11
	cmpq	%r11, %rax
	je	.L943
	movq	-192(%rbp), %rbx
	xorq	%r11, %rax
	rep bsfq	%rax, %rax
	subq	$1, %rbx
	cltq
	movq	%rbx, -248(%rbp)
	shrq	$3, %rax
	addq	%rcx, %rax
.L635:
	cmpq	$3, %rax
	jbe	.L731
	leaq	(%rax,%rax,8), %rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	subq	%rcx, %rdx
	leaq	1935(%rdx), %rbx
	cmpq	$2020, %rbx
	ja	.L944
.L731:
	movq	$0, -80(%rbp)
.L915:
	movl	$2020, %ebx
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L941:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L929:
	movq	-64(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L664
.L670:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L673
.L930:
	movq	-64(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L664
.L750:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L664
.L944:
	movq	-304(%rbp), %rdi
	movzbl	(%rsi,%rax), %r10d
	movq	%rax, %r9
	movq	%rdi, -80(%rbp)
	jmp	.L630
.L942:
	cmpq	$0, -192(%rbp)
	movq	%rsi, -72(%rbp)
	je	.L642
	movzbl	(%rdx,%rax), %esi
	cmpb	%sil, (%r11)
	jne	.L642
	leaq	1(%r11), %rsi
	leaq	9(%r8), %rax
	cmpq	$0, -248(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%rax, -200(%rbp)
	je	.L642
	movzbl	9(%r8,%rdx), %esi
	cmpb	%sil, 1(%r11)
	je	.L692
	movq	-200(%rbp), %rax
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L935:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L656
.L753:
	movq	%rdx, -64(%rbp)
	movq	%rax, %r13
	jmp	.L676
.L937:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L658
.L933:
	cmpq	$0, -96(%rbp)
	je	.L612
	movzbl	(%rdx,%rax), %r12d
	cmpb	%r12b, (%rbx)
	jne	.L612
	cmpq	$0, -176(%rbp)
	movq	%rbx, %rax
	leaq	1(%rbx), %r12
	leaq	9(%r8), %rbx
	je	.L707
	movzbl	9(%r8,%rdx), %r8d
	cmpb	%r8b, 1(%rax)
	je	.L694
.L707:
	movq	%rbx, %rax
	jmp	.L612
.L601:
	testq	%rcx, %rcx
	je	.L702
	subq	$1, %rcx
	leaq	1(%rsi), %rdi
	movq	%rcx, -176(%rbp)
	je	.L704
	movzbl	1(%rsi), %ecx
	movl	$1, %edx
	cmpb	%cl, 1(%rax)
	jne	.L704
.L695:
	movq	-176(%rbp), %rcx
	leaq	1(%rdx), %r14
	cmpq	$1, %rcx
	je	.L605
	movzbl	1(%rax,%rdx), %r10d
	cmpb	%r10b, 1(%rdi)
	jne	.L605
	leaq	2(%rdx), %r14
	cmpq	$2, %rcx
	je	.L605
	movzbl	2(%rax,%rdx), %r10d
	cmpb	%r10b, 2(%rdi)
	jne	.L605
	leaq	3(%rdx), %r14
	cmpq	$3, %rcx
	je	.L605
	movzbl	3(%rax,%rdx), %r10d
	cmpb	%r10b, 3(%rdi)
	jne	.L605
	leaq	4(%rdx), %r14
	subq	$4, %rcx
	je	.L605
	movzbl	4(%rax,%rdx), %r10d
	cmpb	%r10b, 4(%rdi)
	jne	.L605
	leaq	5(%rdx), %r14
	cmpq	$1, %rcx
	je	.L605
	movzbl	5(%rax,%rdx), %eax
	cmpb	%al, 5(%rdi)
	jne	.L605
	leaq	6(%rdx), %r14
	jmp	.L605
.L934:
	leaq	(%r8,%rcx), %rdx
	leaq	8(%rcx), %r14
	subq	$1, %rdi
	je	.L945
	movq	%r14, %rcx
	jmp	.L603
.L744:
	xorl	%r11d, %r11d
	jmp	.L645
.L698:
	xorl	%edx, %edx
	jmp	.L599
.L650:
	movq	-160(%rbp), %r13
	movq	-144(%rbp), %rax
	movq	%rsi, %rbx
	movq	-232(%rbp), %r14
	movl	-292(%rbp), %r11d
	cmpq	%rax, %r13
	movq	%r13, %r12
	cmovbe	%r13, %rax
	movq	%r14, %r9
	movq	%rax, -96(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L651
.L646:
	movq	%rax, %rdi
	andl	$7, %edi
	testb	$7, %al
	je	.L645
	movzbl	(%rsi), %eax
	cmpb	%al, (%rdx)
	jne	.L645
.L686:
	leaq	1(%rcx), %rdx
	cmpq	$1, %rdi
	je	.L649
	movzbl	1(%rcx,%rsi), %eax
	cmpb	%al, 1(%r14)
	jne	.L649
	leaq	2(%rcx), %rdx
	cmpq	$2, %rdi
	je	.L649
	movzbl	2(%rcx,%rsi), %eax
	cmpb	%al, 2(%r14)
	jne	.L649
	leaq	3(%rcx), %rdx
	cmpq	$3, %rdi
	je	.L649
	movzbl	3(%rcx,%rsi), %eax
	cmpb	%al, 3(%r14)
	jne	.L649
	leaq	4(%rcx), %rdx
	cmpq	$4, %rdi
	je	.L649
	movzbl	4(%rcx,%rsi), %eax
	cmpb	%al, 4(%r14)
	jne	.L649
	leaq	5(%rcx), %rdx
	subq	$5, %rdi
	je	.L649
	movzbl	5(%rcx,%rsi), %eax
	cmpb	%al, 5(%r14)
	jne	.L649
	leaq	6(%rcx), %rdx
	cmpq	$1, %rdi
	je	.L649
	movzbl	6(%rcx,%rsi), %eax
	cmpb	%al, 6(%r14)
	jne	.L649
	leaq	7(%rcx), %rdx
	jmp	.L649
.L724:
	movq	%rcx, %r14
	jmp	.L623
.L709:
	movq	%rcx, %r11
	movl	$8, %r14d
	jmp	.L620
.L631:
	cmpq	$0, -192(%rbp)
	je	.L729
	movzbl	(%rdx), %eax
	cmpb	%al, (%rsi)
	jne	.L727
	movq	-192(%rbp), %rax
	leaq	1(%rsi), %rdi
	subq	$1, %rax
	movq	%rax, -248(%rbp)
	movq	%rax, -80(%rbp)
	je	.L915
	movzbl	1(%rsi), %eax
	movl	$1, %ecx
	cmpb	%al, 1(%rdx)
	jne	.L731
.L693:
	movq	-248(%rbp), %rbx
	leaq	1(%rcx), %rax
	cmpq	$1, %rbx
	je	.L635
	movzbl	1(%rcx,%rdx), %r11d
	cmpb	%r11b, 1(%rdi)
	jne	.L635
	leaq	2(%rcx), %rax
	cmpq	$2, %rbx
	je	.L635
	movzbl	2(%rcx,%rdx), %r11d
	cmpb	%r11b, 2(%rdi)
	jne	.L635
	leaq	3(%rcx), %rax
	cmpq	$3, %rbx
	je	.L635
	movzbl	3(%rcx,%rdx), %r11d
	cmpb	%r11b, 3(%rdi)
	jne	.L635
	subq	$4, %rbx
	leaq	4(%rcx), %rax
	movq	%rbx, %r8
	je	.L635
	movzbl	4(%rcx,%rdx), %ebx
	cmpb	%bl, 4(%rdi)
	jne	.L635
	leaq	5(%rcx), %rax
	cmpq	$1, %r8
	je	.L635
	movzbl	5(%rcx,%rdx), %ebx
	cmpb	%bl, 5(%rdi)
	jne	.L635
	leaq	6(%rcx), %rax
	jmp	.L635
.L943:
	leaq	(%r8,%rcx), %r11
	leaq	8(%rcx), %rax
	subq	$1, %rdi
	je	.L946
	movq	%rax, %rcx
	jmp	.L633
.L748:
	movl	$2, %edx
	jmp	.L656
.L938:
	movl	$3, %edx
	jmp	.L656
.L939:
	leaq	8(%r14), %rdx
	subq	$1, %rcx
	je	.L740
	movq	8(%rsi), %rdi
	movq	8(%r14), %rdx
	movq	%rdi, %r11
	movq	%rdi, -272(%rbp)
	movl	$8, %edi
	cmpq	%rdx, %r11
	jne	.L647
	leaq	16(%r14), %rdx
	cmpq	$1, %rcx
	je	.L742
	movq	16(%rsi), %rdi
	movq	16(%r14), %rdx
	movq	%rdi, %rcx
	movq	%rdi, -272(%rbp)
	movl	$16, %edi
	cmpq	%rdx, %rcx
	jne	.L647
	addq	$24, %r14
	movl	$24, %edx
.L648:
	movq	%rax, %rdi
	andl	$7, %edi
	testb	$7, %al
	je	.L649
	movzbl	(%rsi,%rdx), %eax
	cmpb	%al, (%r14)
	jne	.L649
	movq	%rdx, %rcx
	jmp	.L686
.L945:
	cmpq	$0, -96(%rbp)
	je	.L947
	movzbl	(%rax,%r14), %edi
	cmpb	%dil, (%rdx)
	jne	.L948
	movq	-96(%rbp), %r10
	leaq	1(%rdx), %rdi
	leaq	9(%rcx), %r14
	subq	$1, %r10
	movq	%r10, -176(%rbp)
	je	.L605
	movzbl	9(%rax,%rcx), %ecx
	cmpb	%cl, 1(%rdx)
	jne	.L605
	movq	%r14, %rdx
	jmp	.L695
.L702:
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
	movq	$2020, -56(%rbp)
	movq	$-1, -176(%rbp)
	jmp	.L600
.L729:
	movq	$0, -80(%rbp)
	movl	$2020, %ebx
	movq	$-1, -248(%rbp)
	jmp	.L630
.L946:
	cmpq	$0, -192(%rbp)
	je	.L949
	movzbl	(%rdx,%rax), %ebx
	cmpb	%bl, (%r11)
	jne	.L950
	movq	-192(%rbp), %rbx
	leaq	1(%r11), %rdi
	leaq	9(%rcx), %rax
	subq	$1, %rbx
	movq	%rbx, -248(%rbp)
	je	.L635
	movzbl	9(%rcx,%rdx), %ebx
	cmpb	%bl, 1(%r11)
	jne	.L635
	movq	%rax, %rcx
	jmp	.L693
.L927:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L662
.L950:
	movq	-192(%rbp), %rbx
	subq	$1, %rbx
	movq	%rbx, -248(%rbp)
	jmp	.L635
.L949:
	movq	$-1, -248(%rbp)
	jmp	.L635
.L742:
	movq	%rdx, %r14
	movl	$16, %edx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L740:
	movq	%rdx, %r14
	movl	$8, %edx
	jmp	.L648
.L711:
	movq	%rcx, %r11
	movl	$16, %r14d
	jmp	.L620
.L948:
	movq	-96(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -176(%rbp)
	jmp	.L605
.L947:
	movq	$-1, -176(%rbp)
	jmp	.L605
	.cfi_endproc
.LFE282:
	.size	CreateBackwardReferencesNH4, .-CreateBackwardReferencesNH4
	.p2align 4
	.type	CreateBackwardReferencesNH5, @function
CreateBackwardReferencesNH5:
.LFB283:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$256, %edx
	subq	$192, %rsp
	movq	%r8, -144(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	subq	$16, %rax
	movq	%rax, -176(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	leaq	(%rsi,%rdi), %rax
	movq	%rax, -88(%rbp)
	subq	$3, %rax
	cmpq	$3, %rdi
	cmovbe	%rsi, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -296(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -272(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movl	16(%r9), %edx
	movq	%rax, -304(%rbp)
	addq	%rsi, %rax
	movq	%rax, -240(%rbp)
	cmpl	$4, %edx
	jle	.L955
	movq	16(%rbp), %rax
	movq	16(%rbp), %rsi
	movl	(%rax), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rsi)
	leal	1(%rax), %ecx
	movl	%ecx, 20(%rsi)
	leal	-2(%rax), %ecx
	movl	%ecx, 24(%rsi)
	leal	2(%rax), %ecx
	movl	%ecx, 28(%rsi)
	leal	-3(%rax), %ecx
	addl	$3, %eax
	movl	%ecx, 32(%rsi)
	movl	%eax, 36(%rsi)
	cmpl	$10, %edx
	jg	.L1304
.L955:
	leaq	4(%r14), %rax
	movq	%rax, -192(%rbp)
	cmpq	%rax, -88(%rbp)
	jbe	.L1070
	leaq	64(%r12), %rax
	movdqa	.LC0(%rip), %xmm1
	movq	%r15, %r13
	movq	%r12, %r15
	movq	%rax, -120(%rbp)
	movq	32(%rbp), %rax
	movq	%rbx, %r12
	movq	%rax, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	-176(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	%r13, %rdi
	movslq	16(%r15), %r9
	subq	%r14, %rbx
	movq	%rax, %rsi
	cmpq	%r14, %rax
	movq	-144(%rbp), %rax
	cmova	%r14, %rsi
	andq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movq	72(%rax), %rax
	leaq	(%r12,%rdi), %rcx
	movq	%rcx, -64(%rbp)
	movq	%rax, -208(%rbp)
	movq	40(%r15), %rax
	leaq	64(%rax,%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%r9, %r9
	je	.L1071
	movq	%rbx, %rax
	leaq	1(%rcx), %rdx
	andl	$7, %ebx
	addq	$8, %rcx
	shrq	$3, %rax
	movq	$2020, -56(%rbp)
	movq	16(%rbp), %r10
	xorl	%r11d, %r11d
	movq	$0, -184(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rbx, -152(%rbp)
	movq	%r15, -96(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-1(%rbx), %rax
	movq	%rax, -160(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L970:
	movslq	(%r10,%rax,4), %rdx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%r14, %rcx
	jnb	.L959
	cmpq	%rsi, %rdx
	ja	.L959
	leaq	(%rdi,%r11), %r8
	cmpq	%r8, %r13
	jb	.L959
	andq	%r13, %rcx
	leaq	(%rcx,%r11), %rbx
	cmpq	%rbx, %r13
	jb	.L959
	movzbl	(%r12,%rbx), %ebx
	cmpb	%bl, (%r12,%r8)
	je	.L1305
	.p2align 4,,10
	.p2align 3
.L959:
	addq	$1, %rax
	cmpq	%r9, %rax
	jne	.L970
	movq	-96(%rbp), %r15
.L958:
	movq	-64(%rbp), %r10
	movl	56(%r15), %ecx
	movq	-104(%rbp), %rbx
	imull	$506832829, (%r10), %eax
	movl	%eax, -232(%rbp)
	shrl	%cl, %eax
	movl	8(%r15), %ecx
	movl	%eax, %edx
	sall	%cl, %edx
	leaq	(%rbx,%rdx,4), %r8
	movq	-120(%rbp), %rbx
	movl	$0, %edx
	addq	%r15, %r8
	leaq	(%rbx,%rax,2), %rax
	movq	48(%r15), %rbx
	movq	%rax, -112(%rbp)
	movzwl	(%rax), %eax
	movq	%rbx, -200(%rbp)
	movq	%rax, %r9
	movw	%ax, -128(%rbp)
	subq	%rbx, %r9
	cmpq	%rbx, %rax
	movl	60(%r15), %ebx
	cmovbe	%rdx, %r9
	movl	%ebx, -136(%rbp)
	cmpq	%r9, %rax
	jbe	.L972
	movl	%ebx, %edx
	movq	-80(%rbp), %rbx
	movq	%r15, -152(%rbp)
	movq	%rdx, %r15
	movq	%rbx, %rcx
	andl	$7, %ebx
	movq	%rbx, -224(%rbp)
	subq	$1, %rbx
	shrq	$3, %rcx
	movq	%rbx, -248(%rbp)
	leaq	1(%r10), %rbx
	movq	%rbx, -264(%rbp)
	leaq	8(%r10), %rbx
	movq	%rcx, -168(%rbp)
	movq	%rbx, -256(%rbp)
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L973:
	cmpq	%r9, %rax
	jbe	.L1294
.L980:
	subq	$1, %rax
	movq	%rax, %rdx
	andq	%r15, %rdx
	movl	(%r8,%rdx,4), %ecx
	movq	%r14, %rdx
	subq	%rcx, %rdx
	cmpq	%rsi, %rdx
	ja	.L1294
	leaq	(%r11,%rdi), %r10
	cmpq	%r10, %r13
	jb	.L973
	andq	%r13, %rcx
	leaq	(%r11,%rcx), %rbx
	cmpq	%rbx, %r13
	jb	.L973
	movzbl	(%r12,%rbx), %ebx
	cmpb	%bl, (%r12,%r10)
	jne	.L973
	leaq	(%r12,%rcx), %rbx
	movq	%rbx, -160(%rbp)
	movq	-168(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L974
	movq	%rbx, -96(%rbp)
	xorl	%r10d, %r10d
.L976:
	movq	-64(%rbp), %rbx
	movq	(%rbx,%r10), %rcx
	movq	-160(%rbp), %rbx
	movq	(%rbx,%r10), %rbx
	cmpq	%rbx, %rcx
	je	.L1306
	xorq	%rbx, %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	leaq	(%rcx,%r10), %rbx
	movq	%rbx, -96(%rbp)
.L978:
	cmpq	$3, -96(%rbp)
	jbe	.L973
	movq	-96(%rbp), %rbx
	leaq	(%rbx,%rbx,8), %rbx
	movq	%rbx, %r10
	salq	$4, %r10
	subq	%rbx, %r10
	bsrl	%edx, %ebx
	imull	$30, %ebx, %ebx
	addq	$1920, %r10
	subq	%rbx, %r10
	cmpq	-56(%rbp), %r10
	jbe	.L973
	movq	%rdx, -184(%rbp)
	movq	-96(%rbp), %r11
	movq	%r10, -56(%rbp)
	cmpq	%r9, %rax
	ja	.L980
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	-152(%rbp), %r15
.L972:
	movzwl	-128(%rbp), %edi
	movzwl	-136(%rbp), %eax
	movl	$0, -136(%rbp)
	andl	%edi, %eax
	cmpq	$2020, -56(%rbp)
	movzwl	%ax, %eax
	movl	%r14d, (%r8,%rax,4)
	leal	1(%rdi), %eax
	movq	-112(%rbp), %rdi
	movw	%ax, (%rdi)
	je	.L1307
.L981:
	movq	-80(%rbp), %rax
	movq	%r11, -264(%rbp)
	movq	%r14, -160(%rbp)
	subq	$1, %rax
	movq	%rax, -112(%rbp)
	movq	%r15, %rax
	movq	%r12, %r15
	movq	%rax, %r12
.L1022:
	movq	-144(%rbp), %rax
	xorl	%esi, %esi
	cmpl	$4, 4(%rax)
	jg	.L989
	movq	-264(%rbp), %rax
	leaq	-1(%rax), %rsi
	movq	-112(%rbp), %rax
	cmpq	%rax, %rsi
	cmova	%rax, %rsi
.L989:
	movq	-160(%rbp), %rax
	movslq	16(%r12), %r8
	leaq	1(%rax), %rbx
	movq	-176(%rbp), %rax
	movq	%rbx, %r10
	cmpq	%rax, %rbx
	movq	%rax, %r14
	cmovbe	%rbx, %r14
	andq	%r13, %r10
	leaq	(%r15,%r10), %rcx
	movq	%rcx, -96(%rbp)
	testq	%r8, %r8
	je	.L1090
	movq	-112(%rbp), %rax
	movq	$2020, -64(%rbp)
	movq	$0, -80(%rbp)
	movq	16(%rbp), %r9
	movq	%rax, %rdi
	andl	$7, %eax
	movq	$0, -128(%rbp)
	shrq	$3, %rdi
	movq	%rax, %r11
	movq	%r12, -152(%rbp)
	leaq	-1(%rax), %rax
	movq	%rdi, -224(%rbp)
	leaq	1(%rcx), %rdi
	movq	%rdi, -248(%rbp)
	leaq	8(%rcx), %rdi
	movq	%rdi, -256(%rbp)
	movq	%r11, -232(%rbp)
	movq	%rax, -240(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1002:
	movslq	(%r9,%rax,4), %rdx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rbx
	jbe	.L991
	cmpq	%r14, %rdx
	ja	.L991
	leaq	(%r10,%rsi), %rdi
	cmpq	%rdi, %r13
	jb	.L991
	andq	%r13, %rcx
	leaq	(%rcx,%rsi), %r11
	cmpq	%r11, %r13
	jb	.L991
	movzbl	(%r15,%r11), %r11d
	cmpb	%r11b, (%r15,%rdi)
	je	.L1308
	.p2align 4,,10
	.p2align 3
.L991:
	addq	$1, %rax
	cmpq	%rax, %r8
	jne	.L1002
	movq	-152(%rbp), %r12
.L990:
	movq	-96(%rbp), %r9
	movl	56(%r12), %ecx
	movq	-104(%rbp), %rdi
	imull	$506832829, (%r9), %eax
	movl	%eax, -280(%rbp)
	shrl	%cl, %eax
	movl	8(%r12), %ecx
	movl	%eax, %edx
	sall	%cl, %edx
	movq	-120(%rbp), %rcx
	leaq	(%rdi,%rdx,4), %rdi
	movl	$0, %edx
	leaq	(%rcx,%rax,2), %rax
	movq	-200(%rbp), %rcx
	addq	%r12, %rdi
	movq	%rax, -168(%rbp)
	movzwl	(%rax), %eax
	movq	%rax, %r8
	movw	%ax, -224(%rbp)
	subq	%rcx, %r8
	cmpq	%rax, %rcx
	movl	60(%r12), %ecx
	cmovnb	%rdx, %r8
	movl	%ecx, -232(%rbp)
	cmpq	%r8, %rax
	jbe	.L1004
	movl	%ecx, %r11d
	movq	-112(%rbp), %rcx
	movq	%r12, -240(%rbp)
	movq	%r11, %r12
	movq	%rcx, %rdx
	andl	$7, %ecx
	movq	%rcx, -288(%rbp)
	subq	$1, %rcx
	shrq	$3, %rdx
	movq	%rcx, -312(%rbp)
	leaq	1(%r9), %rcx
	movq	%rcx, -320(%rbp)
	leaq	8(%r9), %rcx
	movq	%rdx, -256(%rbp)
	movq	%rcx, -328(%rbp)
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1005:
	cmpq	%r8, %rax
	jbe	.L1295
.L1012:
	subq	$1, %rax
	movq	%rax, %rdx
	andq	%r12, %rdx
	movl	(%rdi,%rdx,4), %ecx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	cmpq	%r14, %rdx
	ja	.L1295
	leaq	(%r10,%rsi), %r9
	cmpq	%r9, %r13
	jb	.L1005
	andq	%r13, %rcx
	leaq	(%rcx,%rsi), %r11
	cmpq	%r11, %r13
	jb	.L1005
	movzbl	(%r15,%r11), %r11d
	cmpb	%r11b, (%r15,%r9)
	jne	.L1005
	addq	%r15, %rcx
	movq	%rcx, -248(%rbp)
	movq	-256(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1006
	movq	%rcx, -152(%rbp)
	xorl	%r9d, %r9d
.L1008:
	movq	-96(%rbp), %rcx
	movq	-248(%rbp), %r11
	movq	(%rcx,%r9), %rcx
	movq	(%r11,%r9), %r11
	cmpq	%r11, %rcx
	je	.L1309
	xorq	%r11, %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%r9, %rcx
	movq	%rcx, -152(%rbp)
.L1010:
	cmpq	$3, -152(%rbp)
	jbe	.L1005
	movq	-152(%rbp), %rcx
	leaq	(%rcx,%rcx,8), %r11
	movq	%r11, %r9
	salq	$4, %r9
	subq	%r11, %r9
	bsrl	%edx, %r11d
	imull	$30, %r11d, %r11d
	addq	$1920, %r9
	subq	%r11, %r9
	cmpq	-64(%rbp), %r9
	jbe	.L1005
	movq	%rdx, -128(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r9, -64(%rbp)
	cmpq	%r8, %rax
	ja	.L1012
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	-240(%rbp), %r12
.L1004:
	movzwl	-224(%rbp), %esi
	movzwl	-232(%rbp), %eax
	movl	$0, -152(%rbp)
	andl	%esi, %eax
	cmpq	$2020, -64(%rbp)
	movzwl	%ax, %eax
	movl	%ebx, (%rdi,%rax,4)
	leal	1(%rsi), %eax
	movq	-168(%rbp), %rsi
	movw	%ax, (%rsi)
	je	.L1310
.L1013:
	movq	-56(%rbp), %rax
	addq	$175, %rax
	cmpq	-64(%rbp), %rax
	ja	.L1020
.L1326:
	addq	$1, -72(%rbp)
	cmpq	-192(%rbp), %rbx
	je	.L1297
	movq	-160(%rbp), %rax
	addq	$5, %rax
	cmpq	%rax, -88(%rbp)
	ja	.L1311
.L1297:
	movq	%r12, %rax
	movq	%r14, %rdi
	movq	%r15, %r12
	movq	%rax, %r15
.L1021:
	movq	-304(%rbp), %rax
	movq	-80(%rbp), %rsi
	addq	%rbx, %rax
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -240(%rbp)
	movq	-128(%rbp), %rax
	cmpq	%rdi, %rax
	jbe	.L1312
	leaq	15(%rax), %rdx
.L1031:
	movq	-216(%rbp), %rsi
	leaq	16(%rsi), %rax
	movq	%rax, -56(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-152(%rbp), %eax
	sall	$25, %eax
	orl	-80(%rbp), %eax
	movl	%eax, 4(%rsi)
	movq	-144(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r11
	cmpq	%rcx, %rdx
	jb	.L1313
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r11,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L1033:
	movq	-216(%rbp), %rdi
	movw	%ax, 14(%rdi)
	movl	-152(%rbp), %eax
	addl	-80(%rbp), %eax
	cmpq	$5, -72(%rbp)
	movl	%edx, 8(%rdi)
	cltq
	jbe	.L1314
	cmpq	$129, -72(%rbp)
	jbe	.L1315
	cmpq	$2113, -72(%rbp)
	jbe	.L1316
	movq	-72(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L1112
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L1038
.L1327:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L1317
	.p2align 4,,10
	.p2align 3
.L1042:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L1044:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1305:
	leaq	(%r12,%rcx), %rbx
	movq	%rbx, -136(%rbp)
	movq	-128(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L960
	movq	-96(%rbp), %r15
	movq	%rbx, -112(%rbp)
	xorl	%ebx, %ebx
.L962:
	movq	-64(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	(%rcx,%rbx), %rcx
	movq	(%r8,%rbx), %r8
	cmpq	%r8, %rcx
	je	.L1318
	xorq	%rcx, %r8
	movq	%r15, -96(%rbp)
	rep bsfq	%r8, %r8
	movslq	%r8d, %r8
	shrq	$3, %r8
	addq	%r8, %rbx
	movq	%rbx, -112(%rbp)
.L964:
	cmpq	$2, -112(%rbp)
	ja	.L1303
.L968:
	cmpq	$2, -112(%rbp)
	jne	.L959
	cmpq	$1, %rax
	ja	.L959
	movq	$2, -112(%rbp)
	movl	$2205, %r15d
.L969:
	movq	-56(%rbp), %r8
	cmpq	%r15, %r8
	jnb	.L959
	testq	%rax, %rax
	je	.L1075
	movl	%eax, %ecx
	movl	$117264, %ebx
	andl	$14, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %ecx
	movq	$-39, %rbx
	andl	$14, %ecx
	subq	%rcx, %rbx
	leaq	(%r15,%rbx), %rbx
	cmpq	%r8, %rbx
	jbe	.L959
	movq	%rdx, -184(%rbp)
	movq	-112(%rbp), %r11
	movq	%rbx, -56(%rbp)
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	24(%r15), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r15)
	jb	.L982
	movl	-232(%rbp), %edx
	addq	$1, %rsi
	movq	%r14, -128(%rbp)
	leaq	2(%rax), %r10
	movq	-144(%rbp), %rdi
	movq	%rsi, -168(%rbp)
	shrl	$18, %edx
	movq	%r12, -152(%rbp)
	movq	104(%rdi), %rcx
	addl	%edx, %edx
	movq	%r13, -160(%rbp)
	subq	%rax, %rdx
	movq	%r11, -112(%rbp)
	movq	%rdi, %r11
	leaq	(%rcx,%rdx,2), %rbx
	.p2align 4,,10
	.p2align 3
.L988:
	movzwl	(%rbx,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r15)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L983
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -96(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -80(%rbp)
	jb	.L983
	movq	80(%r11), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r13
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r13
	je	.L984
	movq	-64(%rbp), %rdx
	movq	(%r8), %r12
	xorl	%r14d, %r14d
	movq	(%rdx), %rdx
	cmpq	%r12, %rdx
	je	.L1319
.L985:
	xorq	%r12, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r14
.L987:
	movl	88(%r11), %edx
	addq	%r14, %rdx
	cmpq	%rdx, %rdi
	jnb	.L983
	testq	%r14, %r14
	je	.L983
	movq	%rdi, %rdx
	movq	96(%r11), %r8
	addq	-168(%rbp), %rsi
	subq	%r14, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %r8
	movq	%r8, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	salq	%cl, %rdx
	addq	%rsi, %rdx
	cmpq	%rdx, -208(%rbp)
	jb	.L983
	leaq	(%r14,%r14,8), %rsi
	movq	%rsi, %rcx
	salq	$4, %rcx
	subq	%rsi, %rcx
	bsrl	%edx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rcx
	subq	%rsi, %rcx
	cmpq	-56(%rbp), %rcx
	jb	.L983
	movzwl	-96(%rbp), %esi
	addq	$1, 32(%r15)
	movq	%rcx, -56(%rbp)
	subl	%r14d, %esi
	movq	%rdx, -184(%rbp)
	movl	%esi, -136(%rbp)
	movq	%r14, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L983:
	cmpq	%r10, %rax
	jne	.L988
	cmpq	$2020, -56(%rbp)
	movq	-112(%rbp), %r11
	movq	-128(%rbp), %r14
	movq	-152(%rbp), %r12
	movq	-160(%rbp), %r13
	jne	.L981
.L982:
	movq	-72(%rbp), %rsi
	movq	-240(%rbp), %rdi
	leaq	1(%r14), %rax
	leaq	1(%rsi), %rdx
	cmpq	%rdi, %rax
	jbe	.L1117
	movq	-272(%rbp), %rcx
	addq	%rdi, %rcx
	cmpq	%rax, %rcx
	jnb	.L1049
	movq	-88(%rbp), %rdi
	leaq	17(%r14), %rcx
	leaq	-4(%rdi), %r9
	cmpq	%rcx, %r9
	cmova	%rcx, %r9
	cmpq	%r9, %rax
	jnb	.L1117
	leaq	4(%rsi), %r10
	movq	-104(%rbp), %r11
	movq	-120(%rbp), %rbx
	subq	%r14, %r10
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	%r13, %rdx
	movl	56(%r15), %ecx
	movzwl	60(%r15), %esi
	andq	%rax, %rdx
	imull	$506832829, (%r12,%rdx), %edx
	shrl	%cl, %edx
	movl	%edx, %ecx
	leaq	(%rbx,%rcx,2), %r8
	movl	8(%r15), %ecx
	movzwl	(%r8), %edi
	sall	%cl, %edx
	andl	%edi, %esi
	addl	$1, %edi
	movzwl	%si, %esi
	addq	%rdx, %rsi
	leaq	(%r15,%rsi,4), %rdx
	movl	%eax, (%rdx,%r11)
	leaq	(%r10,%rax), %rdx
	addq	$4, %rax
	movw	%di, (%r8)
	cmpq	%r9, %rax
	jb	.L1050
.L1117:
	movq	%rdx, -72(%rbp)
	movq	%rax, %r14
.L1047:
	leaq	4(%r14), %rax
	movq	%rax, -192(%rbp)
	cmpq	-88(%rbp), %rax
	jb	.L1052
	movq	-216(%rbp), %rdx
	subq	32(%rbp), %rdx
	sarq	$4, %rdx
.L957:
	movq	-88(%rbp), %rax
	movq	24(%rbp), %rsi
	addq	-72(%rbp), %rax
	subq	%r14, %rax
	movq	%rax, (%rsi)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L966:
	.cfi_restore_state
	cmpq	$2, -112(%rbp)
	jbe	.L959
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	-112(%rbp), %rbx
	leaq	(%rbx,%rbx,8), %rcx
	movq	%rcx, %rbx
	salq	$4, %rbx
	subq	%rcx, %rbx
	leaq	1935(%rbx), %r15
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1308:
	leaq	(%r15,%rcx), %rdi
	movq	%rdi, -168(%rbp)
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L992
	movq	%rdi, -280(%rbp)
	movq	-152(%rbp), %r12
	xorl	%r11d, %r11d
.L994:
	movq	-96(%rbp), %rdi
	movq	(%rdi,%r11), %rcx
	movq	-168(%rbp), %rdi
	movq	(%rdi,%r11), %rdi
	cmpq	%rdi, %rcx
	je	.L1320
	xorq	%rcx, %rdi
	movq	%r12, -152(%rbp)
	rep bsfq	%rdi, %rdi
	movslq	%edi, %rdi
	shrq	$3, %rdi
	addq	%r11, %rdi
.L996:
	cmpq	$2, %rdi
	jbe	.L1000
.L1058:
	leaq	(%rdi,%rdi,8), %rcx
	movq	%rcx, %r11
	salq	$4, %r11
	subq	%rcx, %r11
	leaq	1935(%r11), %r12
.L1001:
	cmpq	%r12, -64(%rbp)
	jnb	.L991
	testq	%rax, %rax
	je	.L1094
	movl	%eax, %ecx
	movl	$117264, %r11d
	andl	$14, %ecx
	sarl	%cl, %r11d
	movl	%r11d, %ecx
	movq	$-39, %r11
	andl	$14, %ecx
	subq	%rcx, %r11
	leaq	(%r12,%r11), %r11
	cmpq	-64(%rbp), %r11
	jbe	.L991
	movq	%rdx, -128(%rbp)
	movq	%rdi, %rsi
	movq	%rdi, -80(%rbp)
	movq	%r11, -64(%rbp)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	-88(%rbp), %rsi
	leaq	9(%r14), %rcx
	leaq	-3(%rsi), %r9
	cmpq	%rcx, %r9
	cmova	%rcx, %r9
	cmpq	%r9, %rax
	jnb	.L1117
	movq	-72(%rbp), %rdi
	movq	-104(%rbp), %r11
	movq	-120(%rbp), %rbx
	leaq	2(%rdi), %r10
	subq	%r14, %r10
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	%r13, %rdx
	movl	56(%r15), %ecx
	movzwl	60(%r15), %esi
	andq	%rax, %rdx
	imull	$506832829, (%r12,%rdx), %edx
	shrl	%cl, %edx
	movl	%edx, %ecx
	leaq	(%rbx,%rcx,2), %r8
	movl	8(%r15), %ecx
	movzwl	(%r8), %edi
	sall	%cl, %edx
	andl	%edi, %esi
	addl	$1, %edi
	movzwl	%si, %esi
	addq	%rdx, %rsi
	leaq	(%r15,%rsi,4), %rdx
	movl	%eax, (%rdx,%r11)
	leaq	(%r10,%rax), %rdx
	addq	$2, %rax
	movw	%di, (%r8)
	cmpq	%r9, %rax
	jb	.L1051
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	16(%rbp), %rsi
	movq	16(%rbp), %rdi
	movslq	(%rsi), %rdx
	movslq	4(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rax, %rdx
	je	.L1024
	cmpq	%rax, %rcx
	je	.L1321
	addq	$3, %rax
	movq	%rax, %r11
	subq	%rdx, %r11
	cmpq	$6, %r11
	jbe	.L1322
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L1323
	movq	16(%rbp), %rax
	movq	-128(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	je	.L1110
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L1324
	leaq	15(%rcx), %rdx
.L1028:
	testq	%rdx, %rdx
	je	.L1024
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L1026:
	movq	16(%rbp), %rcx
	movl	%edi, 8(%rcx)
	movq	-128(%rbp), %rdi
	movl	%eax, 12(%rcx)
	movl	%esi, 4(%rcx)
	movl	%edi, (%rcx)
	movl	16(%r15), %eax
	cmpl	$4, %eax
	jle	.L1031
	movd	%edi, %xmm2
	movq	16(%rbp), %rcx
	movq	16(%rbp), %r11
	pshufd	$0, %xmm2, %xmm0
	paddd	%xmm1, %xmm0
	movups	%xmm0, 16(%rcx)
	movl	%edi, %ecx
	subl	$3, %ecx
	movl	%ecx, 32(%r11)
	leal	3(%rdi), %ecx
	movl	%ecx, 36(%r11)
	cmpl	$10, %eax
	jle	.L1031
	movd	%esi, %xmm3
	leal	-3(%rsi), %eax
	addl	$3, %esi
	pshufd	$0, %xmm3, %xmm0
	movl	%eax, 56(%r11)
	paddd	%xmm1, %xmm0
	movl	%esi, 60(%r11)
	movups	%xmm0, 40(%r11)
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	24(%r12), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r12)
	jb	.L1013
	movl	-280(%rbp), %edx
	leaq	1(%r14), %rsi
	movq	-144(%rbp), %rdi
	movq	%rbx, -232(%rbp)
	movq	%rsi, -224(%rbp)
	leaq	2(%rax), %r10
	shrl	$18, %edx
	movq	104(%rdi), %rcx
	movq	%r14, -240(%rbp)
	addl	%edx, %edx
	movq	%r15, -248(%rbp)
	subq	%rax, %rdx
	movq	%r13, -256(%rbp)
	movq	%rdi, %r13
	leaq	(%rcx,%rdx,2), %r11
	.p2align 4,,10
	.p2align 3
.L1019:
	movzwl	(%r11,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r12)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L1014
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -168(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -112(%rbp)
	jb	.L1014
	movq	80(%r13), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r14
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r14
	je	.L1015
	movq	-96(%rbp), %rdx
	movq	(%r8), %rbx
	xorl	%r15d, %r15d
	movq	(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L1325
.L1016:
	xorq	%rbx, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L1018:
	movl	88(%r13), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L1014
	testq	%r15, %r15
	je	.L1014
	movq	%rdi, %rdx
	movq	96(%r13), %rbx
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	movq	-224(%rbp), %rdi
	salq	%cl, %rdx
	leaq	(%rdi,%rsi), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, -208(%rbp)
	jb	.L1014
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%ecx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-64(%rbp), %rdx
	jb	.L1014
	movzwl	-168(%rbp), %esi
	addq	$1, 32(%r12)
	movq	%rdx, -64(%rbp)
	subl	%r15d, %esi
	movq	%rcx, -128(%rbp)
	movl	%esi, -152(%rbp)
	movq	%r15, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L1014:
	cmpq	%r10, %rax
	jne	.L1019
	movq	-56(%rbp), %rax
	movq	-232(%rbp), %rbx
	movq	-240(%rbp), %r14
	movq	-248(%rbp), %r15
	movq	-256(%rbp), %r13
	addq	$175, %rax
	cmpq	-64(%rbp), %rax
	jbe	.L1326
.L1020:
	movq	%r12, %rax
	movq	-160(%rbp), %r14
	movq	%r15, %r12
	movq	-264(%rbp), %r11
	movq	%rax, %r15
	movq	-176(%rbp), %rax
	movq	%r11, -80(%rbp)
	movq	%r14, %rbx
	cmpq	%rax, %r14
	cmovbe	%r14, %rax
	movq	%rax, %rdi
	movl	-136(%rbp), %eax
	movl	%eax, -152(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	%rdx, -184(%rbp)
	movq	-112(%rbp), %r11
	movq	%r15, -56(%rbp)
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	$0, -184(%rbp)
	xorl	%r11d, %r11d
	movq	$2020, -56(%rbp)
	jmp	.L958
.L1333:
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1000:
	cmpq	$2, %rdi
	jne	.L991
	cmpq	$1, %rax
	ja	.L991
	movl	$2205, %r12d
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	-72(%rbp), %rdi
	movl	%edi, %edx
	sall	$3, %edi
	andl	$56, %edi
.L1035:
	cmpq	$9, %rax
	jbe	.L1327
.L1038:
	cmpq	$133, %rax
	jbe	.L1328
	cmpq	$2117, %rax
	ja	.L1041
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L1039:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L1042
.L1317:
	testb	%sil, %sil
	je	.L1042
	cmpw	$15, %ax
	ja	.L1042
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L1045:
	movq	-216(%rbp), %rax
	movq	-72(%rbp), %rsi
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	movq	-296(%rbp), %rdi
	addq	%rsi, (%rax)
	movq	-80(%rbp), %rax
	leaq	2(%rbx), %rsi
	movq	%rdi, %r9
	leaq	(%rax,%rbx), %r14
	cmpq	%rdi, %r14
	movq	-128(%rbp), %rdi
	cmovbe	%r14, %r9
	shrq	$2, %rax
	cmpq	%rdi, %rax
	jbe	.L1046
	movq	%rdi, %rax
	movq	%r14, %rdi
	salq	$2, %rax
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	cmovnb	%rdi, %rsi
	cmpq	%r9, %rsi
	cmova	%r9, %rsi
.L1046:
	cmpq	%r9, %rsi
	jnb	.L1114
	movq	40(%r15), %rax
	movq	-120(%rbp), %r11
	leaq	64(%rax,%rax), %r10
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	%r13, %rax
	movl	56(%r15), %ecx
	andq	%rsi, %rax
	imull	$506832829, (%r12,%rax), %eax
	shrl	%cl, %eax
	movl	8(%r15), %ecx
	movl	%eax, %edx
	leaq	(%r11,%rdx,2), %r8
	movzwl	60(%r15), %edx
	sall	%cl, %eax
	movzwl	(%r8), %edi
	andl	%edi, %edx
	addl	$1, %edi
	movzwl	%dx, %edx
	addq	%rax, %rdx
	leaq	(%r15,%rdx,4), %rax
	movl	%esi, (%rax,%r10)
	addq	$1, %rsi
	movw	%di, (%r8)
	cmpq	%rsi, %r9
	jne	.L1048
.L1114:
	movq	-56(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -216(%rbp)
	jmp	.L1047
.L1024:
	movq	-216(%rbp), %rsi
	xorl	%edx, %edx
	leaq	16(%rsi), %rax
	movq	%rax, -56(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-152(%rbp), %eax
	sall	$25, %eax
	orl	-80(%rbp), %eax
	movl	%eax, 4(%rsi)
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	.L1033
.L1311:
	movl	-152(%rbp), %eax
	subq	$1, -112(%rbp)
	movq	%rbx, -160(%rbp)
	movl	%eax, -136(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L1022
.L974:
	cmpq	$0, -224(%rbp)
	je	.L973
	movq	-160(%rbp), %rcx
	movq	-64(%rbp), %rbx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%rbx)
	jne	.L973
	movq	-264(%rbp), %rbx
	cmpq	$0, -248(%rbp)
	movq	%rbx, -280(%rbp)
	je	.L973
	movq	-64(%rbp), %rcx
	movq	-160(%rbp), %rbx
	movq	$1, -96(%rbp)
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%rbx)
	jne	.L973
.L1066:
	movq	-96(%rbp), %rcx
	cmpq	$2, -224(%rbp)
	leaq	1(%rcx), %rbx
	movq	%rbx, -96(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movq	-280(%rbp), %rbx
	movzbl	1(%r10,%rcx), %r10d
	cmpb	%r10b, 1(%rbx)
	jne	.L978
	leaq	2(%rcx), %r10
	cmpq	$3, -224(%rbp)
	movq	%r10, -96(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movzbl	2(%r10,%rcx), %r10d
	cmpb	%r10b, 2(%rbx)
	jne	.L978
	leaq	3(%rcx), %r10
	cmpq	$4, -224(%rbp)
	movq	%r10, -96(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movzbl	3(%r10,%rcx), %r10d
	cmpb	%r10b, 3(%rbx)
	jne	.L978
	leaq	4(%rcx), %r10
	cmpq	$5, -224(%rbp)
	movq	%r10, -96(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movzbl	4(%r10,%rcx), %r10d
	cmpb	%r10b, 4(%rbx)
	jne	.L978
	leaq	5(%rcx), %r10
	cmpq	$6, -224(%rbp)
	movq	%r10, -96(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movzbl	5(%r10,%rcx), %r10d
	cmpb	%r10b, 5(%rbx)
	jne	.L978
	leaq	6(%rcx), %r10
	cmpq	$7, -224(%rbp)
	movq	%r10, -96(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movzbl	6(%rbx), %ebx
	cmpb	%bl, 6(%r10,%rcx)
	jne	.L978
	addq	$7, %rcx
	movq	%rcx, -96(%rbp)
	jmp	.L978
.L1306:
	movq	-256(%rbp), %rbx
	leaq	8(%r10), %rcx
	addq	%r10, %rbx
	subq	$1, -96(%rbp)
	je	.L1329
	movq	%rcx, %r10
	jmp	.L976
.L1094:
	movq	%rdx, -128(%rbp)
	movq	%rdi, %rsi
	movq	%rdi, -80(%rbp)
	movq	%r12, -64(%rbp)
	jmp	.L991
.L1304:
	movl	4(%rsi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 40(%rsi)
	leal	1(%rax), %edx
	movl	%edx, 44(%rsi)
	leal	-2(%rax), %edx
	movl	%edx, 48(%rsi)
	leal	2(%rax), %edx
	movl	%edx, 52(%rsi)
	leal	-3(%rax), %edx
	addl	$3, %eax
	movl	%edx, 56(%rsi)
	movl	%eax, 60(%rsi)
	jmp	.L955
.L1322:
	leal	0(,%r11,4), %ecx
	movl	$158663784, %eax
	sarl	%cl, %eax
	movl	%eax, %edx
	andl	$15, %edx
	jmp	.L1028
.L960:
	cmpq	$0, -152(%rbp)
	je	.L959
	movq	-136(%rbp), %rcx
	movq	-64(%rbp), %rbx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%rbx)
	jne	.L959
	movq	-200(%rbp), %rbx
	cmpq	$0, -160(%rbp)
	movq	%rbx, -224(%rbp)
	je	.L959
	movq	-64(%rbp), %rcx
	movq	-136(%rbp), %rbx
	movq	$1, -112(%rbp)
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%rbx)
	jne	.L968
.L1067:
	movq	-112(%rbp), %r8
	cmpq	$2, -152(%rbp)
	leaq	1(%r8), %rbx
	movq	%rbx, -112(%rbp)
	je	.L964
	movq	-136(%rbp), %r15
	movq	-224(%rbp), %rcx
	movzbl	1(%r15,%r8), %ebx
	cmpb	%bl, 1(%rcx)
	jne	.L964
	leaq	2(%r8), %rbx
	cmpq	$3, -152(%rbp)
	movq	%rbx, -112(%rbp)
	je	.L964
	movzbl	2(%r15,%r8), %ebx
	cmpb	%bl, 2(%rcx)
	jne	.L964
	leaq	3(%r8), %rbx
	cmpq	$4, -152(%rbp)
	movq	%rbx, -112(%rbp)
	je	.L964
	movzbl	3(%r15,%r8), %ebx
	cmpb	%bl, 3(%rcx)
	jne	.L964
	leaq	4(%r8), %rbx
	cmpq	$5, -152(%rbp)
	movq	%rbx, -112(%rbp)
	je	.L964
	movzbl	4(%r15,%r8), %ebx
	cmpb	%bl, 4(%rcx)
	jne	.L964
	leaq	5(%r8), %rbx
	cmpq	$6, -152(%rbp)
	movq	%rbx, -112(%rbp)
	je	.L964
	movzbl	5(%r15,%r8), %ebx
	cmpb	%bl, 5(%rcx)
	jne	.L964
	leaq	6(%r8), %rbx
	cmpq	$7, -152(%rbp)
	movq	%rbx, -112(%rbp)
	je	.L964
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r15,%r8)
	jne	.L964
	addq	$7, %r8
	movq	%r8, -112(%rbp)
	jmp	.L964
.L1315:
	movq	-72(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L1035
.L1328:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L1039
.L1318:
	movq	-168(%rbp), %rcx
	leaq	8(%rbx), %r8
	addq	%rbx, %rcx
	subq	$1, -112(%rbp)
	je	.L1330
	movq	%r8, %rbx
	jmp	.L962
.L984:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L983
	movq	-64(%rbp), %rcx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L983
.L1060:
	leaq	1(%r13), %r14
	cmpq	$1, %rdx
	je	.L987
	movq	-64(%rbp), %rcx
	movzbl	1(%rcx,%r13), %r12d
	cmpb	%r12b, 1(%r8)
	jne	.L987
	leaq	2(%r13), %r14
	cmpq	$2, %rdx
	je	.L987
	movzbl	2(%rcx,%r13), %r12d
	cmpb	%r12b, 2(%r8)
	jne	.L987
	leaq	3(%r13), %r14
	cmpq	$3, %rdx
	je	.L987
	movzbl	3(%rcx,%r13), %r12d
	cmpb	%r12b, 3(%r8)
	jne	.L987
	leaq	4(%r13), %r14
	cmpq	$4, %rdx
	je	.L987
	movzbl	4(%rcx,%r13), %r12d
	cmpb	%r12b, 4(%r8)
	jne	.L987
	leaq	5(%r13), %r14
	subq	$5, %rdx
	je	.L987
	movzbl	5(%rcx,%r13), %r12d
	cmpb	%r12b, 5(%r8)
	jne	.L987
	leaq	6(%r13), %r14
	cmpq	$1, %rdx
	je	.L987
	movzbl	6(%rcx,%r13), %ecx
	cmpb	%cl, 6(%r8)
	jne	.L987
	leaq	7(%r13), %r14
	jmp	.L987
.L1319:
	leaq	8(%r8), %rdx
	subq	$1, %r13
	je	.L1083
	movq	-64(%rbp), %rdx
	movq	8(%r8), %r12
	movl	$8, %r14d
	movq	8(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L985
	leaq	16(%r8), %rdx
	cmpq	$1, %r13
	je	.L1085
	movq	-64(%rbp), %rdx
	movq	16(%r8), %r12
	movl	$16, %r14d
	addq	$24, %r8
	movl	$24, %r13d
	movq	16(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L985
.L986:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1088
	movq	-64(%rbp), %rcx
	movzbl	(%rcx,%r13), %ecx
	cmpb	%cl, (%r8)
	je	.L1060
.L1088:
	movq	%r13, %r14
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1041:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L1044
.L1316:
	movq	-72(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L1035
.L1090:
	movq	$0, -80(%rbp)
	movq	$0, -128(%rbp)
	movq	$2020, -64(%rbp)
	jmp	.L990
.L1006:
	cmpq	$0, -288(%rbp)
	je	.L1005
	movq	-96(%rbp), %r11
	movq	-248(%rbp), %rcx
	movzbl	(%r11), %r11d
	cmpb	%r11b, (%rcx)
	jne	.L1005
	movq	-320(%rbp), %rcx
	cmpq	$0, -312(%rbp)
	movq	%rcx, -336(%rbp)
	je	.L1005
	movq	-96(%rbp), %r11
	movq	-248(%rbp), %rcx
	movq	$1, -152(%rbp)
	movzbl	1(%r11), %r11d
	cmpb	%r11b, 1(%rcx)
	jne	.L1005
.L1064:
	movq	-152(%rbp), %rcx
	cmpq	$2, -288(%rbp)
	leaq	1(%rcx), %r11
	movq	%r11, -152(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movq	-336(%rbp), %r11
	movzbl	1(%r9,%rcx), %r9d
	cmpb	%r9b, 1(%r11)
	jne	.L1010
	leaq	2(%rcx), %r9
	cmpq	$3, -288(%rbp)
	movq	%r9, -152(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movzbl	2(%r9,%rcx), %r9d
	cmpb	%r9b, 2(%r11)
	jne	.L1010
	leaq	3(%rcx), %r9
	cmpq	$4, -288(%rbp)
	movq	%r9, -152(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movzbl	3(%r9,%rcx), %r9d
	cmpb	%r9b, 3(%r11)
	jne	.L1010
	leaq	4(%rcx), %r9
	cmpq	$5, -288(%rbp)
	movq	%r9, -152(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movzbl	4(%r9,%rcx), %r9d
	cmpb	%r9b, 4(%r11)
	jne	.L1010
	leaq	5(%rcx), %r9
	cmpq	$6, -288(%rbp)
	movq	%r9, -152(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movzbl	5(%r9,%rcx), %r9d
	cmpb	%r9b, 5(%r11)
	jne	.L1010
	leaq	6(%rcx), %r9
	cmpq	$7, -288(%rbp)
	movq	%r9, -152(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movzbl	6(%r11), %r11d
	cmpb	%r11b, 6(%r9,%rcx)
	jne	.L1010
	addq	$7, %rcx
	movq	%rcx, -152(%rbp)
	jmp	.L1010
.L1309:
	subq	$1, -152(%rbp)
	movq	-328(%rbp), %rcx
	leaq	(%rcx,%r9), %r11
	leaq	8(%r9), %rcx
	je	.L1331
	movq	%rcx, %r9
	jmp	.L1008
.L1112:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L1035
.L1321:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L1026
.L1320:
	subq	$1, -280(%rbp)
	movq	-256(%rbp), %rdi
	leaq	(%rdi,%r11), %rcx
	leaq	8(%r11), %rdi
	je	.L1332
	movq	%rdi, %r11
	jmp	.L994
.L992:
	cmpq	$0, -232(%rbp)
	je	.L991
	movq	-96(%rbp), %rcx
	movq	-168(%rbp), %rdi
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%rdi)
	jne	.L991
	movq	-248(%rbp), %rdi
	cmpq	$0, -240(%rbp)
	movq	%rdi, -288(%rbp)
	je	.L991
	movq	-96(%rbp), %rcx
	movq	-168(%rbp), %rdi
	movq	$1, -280(%rbp)
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%rdi)
	jne	.L1333
.L1065:
	movq	-280(%rbp), %r12
	cmpq	$2, -232(%rbp)
	leaq	1(%r12), %rdi
	je	.L996
	movq	-168(%rbp), %r11
	movq	-288(%rbp), %rcx
	movzbl	1(%r11,%r12), %r12d
	cmpb	%r12b, 1(%rcx)
	jne	.L996
	movq	-280(%rbp), %r12
	cmpq	$3, -232(%rbp)
	leaq	2(%r12), %rdi
	je	.L996
	movzbl	2(%r11,%r12), %r12d
	cmpb	%r12b, 2(%rcx)
	jne	.L996
	movq	-280(%rbp), %r12
	cmpq	$4, -232(%rbp)
	leaq	3(%r12), %rdi
	je	.L996
	movzbl	3(%r11,%r12), %r12d
	cmpb	%r12b, 3(%rcx)
	jne	.L996
	movq	-280(%rbp), %r12
	cmpq	$5, -232(%rbp)
	leaq	4(%r12), %rdi
	je	.L996
	movzbl	4(%r11,%r12), %r12d
	cmpb	%r12b, 4(%rcx)
	jne	.L996
	movq	-280(%rbp), %r12
	cmpq	$6, -232(%rbp)
	leaq	5(%r12), %rdi
	je	.L996
	movzbl	5(%r11,%r12), %r12d
	cmpb	%r12b, 5(%rcx)
	jne	.L996
	movq	-280(%rbp), %r12
	cmpq	$7, -232(%rbp)
	leaq	6(%r12), %rdi
	je	.L996
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r11,%r12)
	jne	.L996
	addq	$1, %rdi
	jmp	.L996
.L1015:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1014
	movq	-96(%rbp), %rbx
	movzbl	(%rbx), %ebx
	cmpb	%bl, (%r8)
	jne	.L1014
.L1056:
	leaq	1(%r14), %r15
	cmpq	$1, %rdx
	je	.L1018
	movq	-96(%rbp), %rbx
	movzbl	1(%rbx,%r14), %ecx
	cmpb	%cl, 1(%r8)
	jne	.L1018
	leaq	2(%r14), %r15
	cmpq	$2, %rdx
	je	.L1018
	movzbl	2(%rbx,%r14), %ecx
	cmpb	%cl, 2(%r8)
	jne	.L1018
	leaq	3(%r14), %r15
	cmpq	$3, %rdx
	je	.L1018
	movzbl	3(%rbx,%r14), %ecx
	cmpb	%cl, 3(%r8)
	jne	.L1018
	leaq	4(%r14), %r15
	cmpq	$4, %rdx
	je	.L1018
	movzbl	4(%rbx,%r14), %ecx
	cmpb	%cl, 4(%r8)
	jne	.L1018
	leaq	5(%r14), %r15
	subq	$5, %rdx
	je	.L1018
	movzbl	5(%rbx,%r14), %ecx
	cmpb	%cl, 5(%r8)
	jne	.L1018
	leaq	6(%r14), %r15
	cmpq	$1, %rdx
	je	.L1018
	movzbl	6(%rbx,%r14), %ebx
	cmpb	%bl, 6(%r8)
	jne	.L1018
	leaq	7(%r14), %r15
	jmp	.L1018
.L1323:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L1028
.L1325:
	leaq	8(%r8), %rdx
	subq	$1, %r14
	je	.L1103
	movq	-96(%rbp), %rdx
	movq	8(%r8), %rbx
	movl	$8, %r15d
	movq	8(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1016
	leaq	16(%r8), %rdx
	cmpq	$1, %r14
	je	.L1105
	movq	-96(%rbp), %rdx
	movq	16(%r8), %rbx
	movl	$16, %r15d
	addq	$24, %r8
	movl	$24, %r14d
	movq	16(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1016
.L1017:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1108
	movq	-96(%rbp), %rbx
	movzbl	(%rbx,%r14), %ebx
	cmpb	%bl, (%r8)
	je	.L1056
.L1108:
	movq	%r14, %r15
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1070:
	xorl	%edx, %edx
	jmp	.L957
.L1329:
	cmpq	$0, -224(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r10, -288(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movzbl	(%r10,%rcx), %r10d
	cmpb	%r10b, (%rbx)
	jne	.L978
	leaq	1(%rbx), %rcx
	cmpq	$0, -248(%rbp)
	movq	%rcx, -280(%rbp)
	movq	-288(%rbp), %rcx
	leaq	9(%rcx), %r10
	movq	%r10, -96(%rbp)
	je	.L978
	movq	-160(%rbp), %r10
	movzbl	9(%r10,%rcx), %ecx
	cmpb	%cl, 1(%rbx)
	je	.L1066
	jmp	.L978
.L1331:
	cmpq	$0, -288(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r9, -344(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movq	%r11, -336(%rbp)
	movzbl	(%r9,%rcx), %r9d
	cmpb	%r9b, (%r11)
	jne	.L1010
	movq	%r11, %rcx
	leaq	1(%r11), %r11
	cmpq	$0, -312(%rbp)
	movq	%r11, -336(%rbp)
	movq	-344(%rbp), %r11
	leaq	9(%r11), %r9
	movq	%r9, -152(%rbp)
	je	.L1010
	movq	-248(%rbp), %r9
	movzbl	9(%r9,%r11), %r11d
	cmpb	%r11b, 1(%rcx)
	je	.L1064
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1330:
	cmpq	$0, -152(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r15, -96(%rbp)
	je	.L966
	movq	-136(%rbp), %r15
	movq	-112(%rbp), %r8
	movzbl	(%r15,%r8), %r15d
	cmpb	%r15b, (%rcx)
	jne	.L966
	leaq	1(%rcx), %r8
	leaq	9(%rbx), %r15
	cmpq	$0, -160(%rbp)
	movq	%r8, -224(%rbp)
	movq	%r15, -112(%rbp)
	je	.L964
	movq	-136(%rbp), %r15
	movzbl	9(%r15,%rbx), %ebx
	cmpb	%bl, 1(%rcx)
	je	.L1067
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	%rdx, %r8
	movl	$8, %r13d
	jmp	.L986
.L1110:
	movl	$2, %edx
	jmp	.L1026
.L1324:
	movl	$3, %edx
	jmp	.L1026
.L1332:
	cmpq	$0, -232(%rbp)
	movq	%r12, -152(%rbp)
	je	.L998
	movq	-168(%rbp), %r12
	movzbl	(%r12,%rdi), %r12d
	cmpb	%r12b, (%rcx)
	jne	.L999
	movq	%rcx, %rdi
	leaq	1(%rcx), %rcx
	cmpq	$0, -240(%rbp)
	movq	%rcx, -288(%rbp)
	leaq	9(%r11), %rcx
	movq	%rcx, -280(%rbp)
	je	.L1093
	movq	-168(%rbp), %r12
	movzbl	9(%r12,%r11), %ecx
	cmpb	%cl, 1(%rdi)
	je	.L1065
.L1093:
	movq	-280(%rbp), %rdi
	jmp	.L996
.L1103:
	movq	%rdx, %r8
	movl	$8, %r14d
	jmp	.L1017
.L1085:
	movq	%rdx, %r8
	movl	$16, %r13d
	jmp	.L986
.L999:
	cmpq	$2, %rdi
	ja	.L1058
	jmp	.L991
.L998:
	cmpq	$2, %rdi
	jbe	.L991
	imulq	$135, %rdi, %r11
	leaq	1935(%r11), %r12
	jmp	.L1001
.L1105:
	movq	%rdx, %r8
	movl	$16, %r14d
	jmp	.L1017
.L1313:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L1033
	.cfi_endproc
.LFE283:
	.size	CreateBackwardReferencesNH5, .-CreateBackwardReferencesNH5
	.p2align 4
	.type	CreateBackwardReferencesNH6, @function
CreateBackwardReferencesNH6:
.LFB284:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$256, %edx
	pushq	%rbx
	subq	$192, %rsp
	.cfi_offset 3, -56
	movq	%r8, -152(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	subq	$16, %rax
	movq	%rax, -192(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	leaq	(%rsi,%rdi), %rax
	movq	%rax, -96(%rbp)
	subq	$7, %rax
	cmpq	$7, %rdi
	cmovbe	%rsi, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -304(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -288(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movl	16(%r9), %edx
	movq	%rax, -312(%rbp)
	addq	%rsi, %rax
	movq	%rax, -208(%rbp)
	cmpl	$4, %edx
	jle	.L1338
	movq	16(%rbp), %rax
	movq	16(%rbp), %rsi
	movl	(%rax), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rsi)
	leal	1(%rax), %ecx
	movl	%ecx, 20(%rsi)
	leal	-2(%rax), %ecx
	movl	%ecx, 24(%rsi)
	leal	2(%rax), %ecx
	movl	%ecx, 28(%rsi)
	leal	-3(%rax), %ecx
	addl	$3, %eax
	movl	%ecx, 32(%rsi)
	movl	%eax, 36(%rsi)
	cmpl	$10, %edx
	jg	.L1690
.L1338:
	leaq	8(%r14), %rax
	cmpq	%rax, -96(%rbp)
	jbe	.L1454
.L1716:
	leaq	80(%r13), %rax
	movdqa	.LC0(%rip), %xmm1
	movq	%rax, -56(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	-192(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	%r15, %rdi
	movslq	16(%r12), %r9
	subq	%r14, %rbx
	movq	%rax, %rsi
	cmpq	%r14, %rax
	movq	-152(%rbp), %rax
	cmova	%r14, %rsi
	andq	%r14, %rdi
	movq	%rbx, -88(%rbp)
	movq	72(%rax), %rax
	leaq	0(%r13,%rdi), %rcx
	movq	%rcx, -80(%rbp)
	movq	%rax, -224(%rbp)
	movq	40(%r12), %rax
	leaq	80(%rax,%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%r9, %r9
	je	.L1455
	movq	%rbx, %rax
	leaq	1(%rcx), %rdx
	andl	$7, %ebx
	addq	$8, %rcx
	shrq	$3, %rax
	movq	$2020, -64(%rbp)
	movq	16(%rbp), %r10
	xorl	%r11d, %r11d
	movq	$0, -200(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rbx, -144(%rbp)
	movq	%r12, -104(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-1(%rbx), %rax
	movq	%rax, -160(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1353:
	movslq	(%r10,%rax,4), %rdx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%rsi, %rdx
	ja	.L1342
	cmpq	%r14, %rcx
	jnb	.L1342
	leaq	(%rdi,%r11), %r8
	cmpq	%r8, %r15
	jb	.L1342
	andq	%r15, %rcx
	leaq	(%rcx,%r11), %rbx
	cmpq	%rbx, %r15
	jb	.L1342
	movzbl	0(%r13,%rbx), %ebx
	cmpb	%bl, 0(%r13,%r8)
	je	.L1691
	.p2align 4,,10
	.p2align 3
.L1342:
	addq	$1, %rax
	cmpq	%r9, %rax
	jne	.L1353
	movq	-104(%rbp), %r12
.L1341:
	movq	-80(%rbp), %r10
	movq	64(%r12), %rax
	movl	56(%r12), %ecx
	movq	(%r10), %rbx
	movq	%rax, -176(%rbp)
	andq	%rbx, %rax
	movq	%rbx, -120(%rbp)
	movq	-112(%rbp), %rbx
	movq	%rax, %rdx
	movabsq	$2297779722762296275, %rax
	imulq	%rdx, %rax
	shrq	%cl, %rax
	movl	8(%r12), %ecx
	movl	%eax, %edx
	movl	%eax, %eax
	sall	%cl, %edx
	leaq	(%rbx,%rdx,4), %r8
	movq	-56(%rbp), %rbx
	movl	$0, %edx
	addq	%r12, %r8
	leaq	(%rbx,%rax,2), %rax
	movq	48(%r12), %rbx
	movq	%rax, -128(%rbp)
	movzwl	(%rax), %eax
	movq	%rbx, -216(%rbp)
	movq	%rax, %r9
	movw	%ax, -136(%rbp)
	subq	%rbx, %r9
	cmpq	%rbx, %rax
	movl	72(%r12), %ebx
	cmovbe	%rdx, %r9
	movl	%ebx, -144(%rbp)
	cmpq	%r9, %rax
	jbe	.L1355
	movl	%ebx, %edx
	movq	-88(%rbp), %rbx
	movq	%r12, -160(%rbp)
	movq	%rdx, %r12
	movq	%rbx, %rcx
	andl	$7, %ebx
	movq	%rbx, -240(%rbp)
	subq	$1, %rbx
	shrq	$3, %rcx
	movq	%rbx, -248(%rbp)
	leaq	1(%r10), %rbx
	movq	%rbx, -264(%rbp)
	leaq	8(%r10), %rbx
	movq	%rcx, -184(%rbp)
	movq	%rbx, -256(%rbp)
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1356:
	cmpq	%r9, %rax
	jbe	.L1678
.L1363:
	subq	$1, %rax
	movq	%rax, %rdx
	andq	%r12, %rdx
	movl	(%r8,%rdx,4), %ecx
	movq	%r14, %rdx
	subq	%rcx, %rdx
	cmpq	%rsi, %rdx
	ja	.L1678
	leaq	(%r11,%rdi), %r10
	cmpq	%r10, %r15
	jb	.L1356
	andq	%r15, %rcx
	leaq	(%r11,%rcx), %rbx
	cmpq	%rbx, %r15
	jb	.L1356
	movzbl	0(%r13,%rbx), %ebx
	cmpb	%bl, 0(%r13,%r10)
	jne	.L1356
	leaq	0(%r13,%rcx), %rbx
	movq	%rbx, -168(%rbp)
	movq	-184(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1357
	movq	%rbx, -104(%rbp)
	xorl	%r10d, %r10d
.L1359:
	movq	-80(%rbp), %rbx
	movq	(%rbx,%r10), %rcx
	movq	-168(%rbp), %rbx
	movq	(%rbx,%r10), %rbx
	cmpq	%rbx, %rcx
	je	.L1692
	xorq	%rbx, %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	leaq	(%rcx,%r10), %rbx
	movq	%rbx, -104(%rbp)
.L1361:
	cmpq	$3, -104(%rbp)
	jbe	.L1356
	movq	-104(%rbp), %rbx
	leaq	(%rbx,%rbx,8), %rbx
	movq	%rbx, %r10
	salq	$4, %r10
	subq	%rbx, %r10
	bsrl	%edx, %ebx
	imull	$30, %ebx, %ebx
	addq	$1920, %r10
	subq	%rbx, %r10
	cmpq	-64(%rbp), %r10
	jbe	.L1356
	movq	%rdx, -200(%rbp)
	movq	-104(%rbp), %r11
	movq	%r10, -64(%rbp)
	cmpq	%r9, %rax
	ja	.L1363
	.p2align 4,,10
	.p2align 3
.L1678:
	movq	-160(%rbp), %r12
.L1355:
	movzwl	-136(%rbp), %edi
	movzwl	-144(%rbp), %eax
	movl	$0, -136(%rbp)
	andl	%edi, %eax
	cmpq	$2020, -64(%rbp)
	movzwl	%ax, %eax
	movl	%r14d, (%r8,%rax,4)
	leal	1(%rdi), %eax
	movq	-128(%rbp), %rdi
	movw	%ax, (%rdi)
	je	.L1693
.L1364:
	movq	-88(%rbp), %rax
	movq	%r11, -280(%rbp)
	movq	%r14, -160(%rbp)
	subq	$1, %rax
	movq	%rax, -104(%rbp)
	leaq	4(%r14), %rax
	movq	%r13, %r14
	movq	%rax, -264(%rbp)
.L1405:
	movq	-152(%rbp), %rax
	xorl	%esi, %esi
	cmpl	$4, 4(%rax)
	jg	.L1372
	movq	-280(%rbp), %rax
	leaq	-1(%rax), %rsi
	movq	-104(%rbp), %rax
	cmpq	%rax, %rsi
	cmova	%rax, %rsi
.L1372:
	movq	-160(%rbp), %rax
	movslq	16(%r12), %r8
	leaq	1(%rax), %rbx
	movq	-192(%rbp), %rax
	movq	%rbx, %r10
	cmpq	%rax, %rbx
	movq	%rax, %r13
	cmovbe	%rbx, %r13
	andq	%r15, %r10
	leaq	(%r14,%r10), %rcx
	movq	%rcx, -128(%rbp)
	testq	%r8, %r8
	je	.L1474
	movq	-104(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	$2020, -80(%rbp)
	movq	16(%rbp), %r9
	movq	%rax, %rdi
	andl	$7, %eax
	movq	$0, -88(%rbp)
	shrq	$3, %rdi
	movq	%rax, %r11
	movq	%r12, -144(%rbp)
	leaq	-1(%rax), %rax
	movq	%rdi, -184(%rbp)
	leaq	1(%rcx), %rdi
	movq	%rdi, -248(%rbp)
	leaq	8(%rcx), %rdi
	movq	%rdi, -256(%rbp)
	movq	%r11, -208(%rbp)
	movq	%rax, -240(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1385:
	movslq	(%r9,%rax,4), %rdx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%r13, %rdx
	ja	.L1374
	cmpq	%rcx, %rbx
	jbe	.L1374
	leaq	(%r10,%rsi), %rdi
	cmpq	%rdi, %r15
	jb	.L1374
	andq	%r15, %rcx
	leaq	(%rcx,%rsi), %r11
	cmpq	%r11, %r15
	jb	.L1374
	movzbl	(%r14,%r11), %r11d
	cmpb	%r11b, (%r14,%rdi)
	je	.L1694
	.p2align 4,,10
	.p2align 3
.L1374:
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L1385
	movq	-144(%rbp), %r12
.L1373:
	movq	-128(%rbp), %r11
	movq	-176(%rbp), %rdx
	movl	56(%r12), %ecx
	movq	-112(%rbp), %rdi
	movq	(%r11), %rax
	andq	%rax, %rdx
	movq	%rax, -168(%rbp)
	movabsq	$2297779722762296275, %rax
	imulq	%rdx, %rax
	shrq	%cl, %rax
	movl	8(%r12), %ecx
	movl	%eax, %edx
	movl	%eax, %eax
	sall	%cl, %edx
	movq	-56(%rbp), %rcx
	leaq	(%rdi,%rdx,4), %rdi
	movl	$0, %edx
	leaq	(%rcx,%rax,2), %rax
	movq	-216(%rbp), %rcx
	addq	%r12, %rdi
	movq	%rax, -184(%rbp)
	movzwl	(%rax), %eax
	movq	%rax, %r8
	movw	%ax, -208(%rbp)
	subq	%rcx, %r8
	cmpq	%rax, %rcx
	movl	72(%r12), %ecx
	cmovnb	%rdx, %r8
	movl	%ecx, -240(%rbp)
	cmpq	%r8, %rax
	jbe	.L1387
	movl	%ecx, %r9d
	movq	-104(%rbp), %rcx
	movq	%r12, -248(%rbp)
	movq	%r9, %r12
	movq	%rcx, %rdx
	andl	$7, %ecx
	movq	%rcx, -296(%rbp)
	subq	$1, %rcx
	shrq	$3, %rdx
	movq	%rcx, -320(%rbp)
	leaq	1(%r11), %rcx
	movq	%rcx, -328(%rbp)
	leaq	8(%r11), %rcx
	movq	%rdx, -272(%rbp)
	movq	%rcx, -336(%rbp)
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1388:
	cmpq	%r8, %rax
	jbe	.L1679
.L1395:
	subq	$1, %rax
	movq	%rax, %rdx
	andq	%r12, %rdx
	movl	(%rdi,%rdx,4), %ecx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	cmpq	%r13, %rdx
	ja	.L1679
	leaq	(%r10,%rsi), %r9
	cmpq	%r9, %r15
	jb	.L1388
	andq	%r15, %rcx
	leaq	(%rcx,%rsi), %r11
	cmpq	%r11, %r15
	jb	.L1388
	movzbl	(%r14,%r11), %r11d
	cmpb	%r11b, (%r14,%r9)
	jne	.L1388
	addq	%r14, %rcx
	movq	%rcx, -256(%rbp)
	movq	-272(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1389
	movq	%rcx, -144(%rbp)
	xorl	%r9d, %r9d
.L1391:
	movq	-128(%rbp), %rcx
	movq	-256(%rbp), %r11
	movq	(%rcx,%r9), %rcx
	movq	(%r11,%r9), %r11
	cmpq	%r11, %rcx
	je	.L1695
	xorq	%r11, %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%r9, %rcx
	movq	%rcx, -144(%rbp)
.L1393:
	cmpq	$3, -144(%rbp)
	jbe	.L1388
	movq	-144(%rbp), %rcx
	leaq	(%rcx,%rcx,8), %r11
	movq	%r11, %r9
	salq	$4, %r9
	subq	%r11, %r9
	bsrl	%edx, %r11d
	imull	$30, %r11d, %r11d
	addq	$1920, %r9
	subq	%r11, %r9
	cmpq	-80(%rbp), %r9
	jbe	.L1388
	movq	%rdx, -120(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -88(%rbp)
	movq	%r9, -80(%rbp)
	cmpq	%r8, %rax
	ja	.L1395
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	-248(%rbp), %r12
.L1387:
	movzwl	-208(%rbp), %esi
	movzwl	-240(%rbp), %eax
	movl	$0, -144(%rbp)
	andl	%esi, %eax
	cmpq	$2020, -80(%rbp)
	movzwl	%ax, %eax
	movl	%ebx, (%rdi,%rax,4)
	leal	1(%rsi), %eax
	movq	-184(%rbp), %rsi
	movw	%ax, (%rsi)
	je	.L1696
.L1396:
	movq	-64(%rbp), %rax
	addq	$175, %rax
	cmpq	-80(%rbp), %rax
	ja	.L1403
.L1712:
	addq	$1, -72(%rbp)
	cmpq	-264(%rbp), %rbx
	je	.L1681
	movq	-160(%rbp), %rax
	addq	$9, %rax
	cmpq	%rax, -96(%rbp)
	ja	.L1697
.L1681:
	movq	%r13, %rdi
	movq	%r14, %r13
.L1404:
	movq	-312(%rbp), %rax
	movq	-88(%rbp), %rsi
	addq	%rbx, %rax
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -208(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rdi, %rax
	jbe	.L1698
	leaq	15(%rax), %rdx
.L1414:
	movq	-232(%rbp), %rsi
	leaq	16(%rsi), %rax
	movq	%rax, -64(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-144(%rbp), %eax
	sall	$25, %eax
	orl	-88(%rbp), %eax
	movl	%eax, 4(%rsi)
	movq	-152(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r11
	cmpq	%rcx, %rdx
	jb	.L1699
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r11,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L1416:
	movq	-232(%rbp), %rdi
	movw	%ax, 14(%rdi)
	movl	-144(%rbp), %eax
	addl	-88(%rbp), %eax
	cmpq	$5, -72(%rbp)
	movl	%edx, 8(%rdi)
	cltq
	jbe	.L1700
	cmpq	$129, -72(%rbp)
	jbe	.L1701
	cmpq	$2113, -72(%rbp)
	jbe	.L1702
	movq	-72(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L1496
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L1421
.L1713:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L1703
	.p2align 4,,10
	.p2align 3
.L1425:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L1427:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1691:
	leaq	0(%r13,%rcx), %rbx
	movq	%rbx, -136(%rbp)
	movq	-128(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1343
	movq	-104(%rbp), %r12
	movq	%rbx, -120(%rbp)
	xorl	%ebx, %ebx
.L1345:
	movq	-80(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	(%rcx,%rbx), %rcx
	movq	(%r8,%rbx), %r8
	cmpq	%r8, %rcx
	je	.L1704
	xorq	%rcx, %r8
	movq	%r12, -104(%rbp)
	rep bsfq	%r8, %r8
	movslq	%r8d, %r8
	shrq	$3, %r8
	addq	%r8, %rbx
	movq	%rbx, -120(%rbp)
.L1347:
	cmpq	$2, -120(%rbp)
	ja	.L1688
.L1351:
	cmpq	$2, -120(%rbp)
	jne	.L1342
	cmpq	$1, %rax
	ja	.L1342
	movq	$2, -120(%rbp)
	movl	$2205, %r12d
.L1352:
	movq	-64(%rbp), %r8
	cmpq	%r12, %r8
	jnb	.L1342
	testq	%rax, %rax
	je	.L1459
	movl	%eax, %ecx
	movl	$117264, %ebx
	andl	$14, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %ecx
	movq	$-39, %rbx
	andl	$14, %ecx
	subq	%rcx, %rbx
	leaq	(%r12,%rbx), %rbx
	cmpq	%rbx, %r8
	jnb	.L1342
	movq	%rdx, -200(%rbp)
	movq	-120(%rbp), %r11
	movq	%rbx, -64(%rbp)
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	24(%r12), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r12)
	jb	.L1365
	movq	-80(%rbp), %rdi
	addq	$1, %rsi
	movq	%r11, -128(%rbp)
	leaq	2(%rax), %r10
	movq	%rsi, -184(%rbp)
	imull	$506832829, (%rdi), %edx
	movq	-152(%rbp), %rdi
	movq	%r14, -144(%rbp)
	movq	%r13, -160(%rbp)
	movq	104(%rdi), %rcx
	movq	%r15, -168(%rbp)
	movq	%rdi, %r11
	shrl	$18, %edx
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %rbx
	.p2align 4,,10
	.p2align 3
.L1371:
	movzwl	(%rbx,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r12)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L1366
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -104(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -88(%rbp)
	jb	.L1366
	movq	80(%r11), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r14
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r14
	je	.L1367
	movq	(%r8), %r13
	movq	-120(%rbp), %rdx
	xorl	%r15d, %r15d
	cmpq	%r13, %rdx
	je	.L1705
.L1368:
	xorq	%r13, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L1370:
	movl	88(%r11), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L1366
	testq	%r15, %r15
	je	.L1366
	movq	%rdi, %rdx
	movq	96(%r11), %r14
	addq	-184(%rbp), %rsi
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %r14
	movq	%r14, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	salq	%cl, %rdx
	addq	%rsi, %rdx
	cmpq	%rdx, -224(%rbp)
	jb	.L1366
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rcx
	salq	$4, %rcx
	subq	%rsi, %rcx
	bsrl	%edx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rcx
	subq	%rsi, %rcx
	cmpq	-64(%rbp), %rcx
	jb	.L1366
	movzwl	-104(%rbp), %esi
	addq	$1, 32(%r12)
	movq	%rcx, -64(%rbp)
	subl	%r15d, %esi
	movq	%rdx, -200(%rbp)
	movl	%esi, -136(%rbp)
	movq	%r15, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L1366:
	cmpq	%r10, %rax
	jne	.L1371
	cmpq	$2020, -64(%rbp)
	movq	-128(%rbp), %r11
	movq	-144(%rbp), %r14
	movq	-160(%rbp), %r13
	movq	-168(%rbp), %r15
	jne	.L1364
.L1365:
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rdx
	leaq	1(%r14), %rax
	cmpq	-208(%rbp), %rax
	jbe	.L1689
	movq	-96(%rbp), %rsi
	movq	-208(%rbp), %rcx
	addq	-288(%rbp), %rcx
	leaq	-7(%rsi), %r10
	cmpq	%rax, %rcx
	jnb	.L1433
	leaq	17(%r14), %r8
	movq	-72(%rbp), %rsi
	movabsq	$2297779722762296275, %r11
	cmpq	%r10, %r8
	cmova	%r10, %r8
	leaq	4(%rsi), %r10
	subq	%r14, %r10
	cmpq	%r8, %rax
	jnb	.L1689
	movq	-112(%rbp), %rbx
	movq	-176(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	%r15, %rdx
	movl	56(%r12), %ecx
	andq	%rax, %rdx
	movq	0(%r13,%rdx), %rsi
	andq	%r14, %rsi
	movq	%rsi, %rdx
	movq	-56(%rbp), %rsi
	imulq	%r11, %rdx
	shrq	%cl, %rdx
	movl	%edx, %ecx
	leaq	(%rsi,%rcx,2), %r9
	movzwl	72(%r12), %esi
	movl	8(%r12), %ecx
	movzwl	(%r9), %edi
	sall	%cl, %edx
	andl	%edi, %esi
	addl	$1, %edi
	movzwl	%si, %esi
	addq	%rsi, %rdx
	leaq	(%r12,%rdx,4), %rdx
	movl	%eax, (%rdx,%rbx)
	leaq	(%r10,%rax), %rdx
	movw	%di, (%r9)
	movq	%rax, %r9
	addq	$4, %rax
	cmpq	%r8, %rax
	jb	.L1434
	movq	%rdx, -72(%rbp)
	addq	$12, %r9
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1430:
	cmpq	%r9, -96(%rbp)
	ja	.L1436
	movq	-232(%rbp), %rdx
	subq	32(%rbp), %rdx
	sarq	$4, %rdx
.L1340:
	movq	-96(%rbp), %rax
	movq	24(%rbp), %rsi
	addq	-72(%rbp), %rax
	subq	%r14, %rax
	movq	%rax, (%rsi)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1689:
	.cfi_restore_state
	leaq	9(%r14), %r9
	movq	%rdx, -72(%rbp)
	movq	%rax, %r14
	jmp	.L1430
.L1349:
	cmpq	$2, -120(%rbp)
	jbe	.L1342
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	-120(%rbp), %rbx
	leaq	(%rbx,%rbx,8), %rcx
	movq	%rcx, %rbx
	salq	$4, %rbx
	subq	%rcx, %rbx
	leaq	1935(%rbx), %r12
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1694:
	leaq	(%r14,%rcx), %rdi
	movq	%rdi, -168(%rbp)
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1375
	movq	%rdi, -272(%rbp)
	movq	-144(%rbp), %r12
	xorl	%r11d, %r11d
.L1377:
	movq	-128(%rbp), %rdi
	movq	(%rdi,%r11), %rcx
	movq	-168(%rbp), %rdi
	movq	(%rdi,%r11), %rdi
	cmpq	%rdi, %rcx
	je	.L1706
	xorq	%rcx, %rdi
	movq	%r12, -144(%rbp)
	rep bsfq	%rdi, %rdi
	movslq	%edi, %rdi
	shrq	$3, %rdi
	addq	%r11, %rdi
.L1379:
	cmpq	$2, %rdi
	jbe	.L1383
.L1442:
	leaq	(%rdi,%rdi,8), %rcx
	movq	%rcx, %r11
	salq	$4, %r11
	subq	%rcx, %r11
	leaq	1935(%r11), %r12
.L1384:
	cmpq	%r12, -80(%rbp)
	jnb	.L1374
	testq	%rax, %rax
	je	.L1478
	movl	%eax, %ecx
	movl	$117264, %r11d
	andl	$14, %ecx
	sarl	%cl, %r11d
	movl	%r11d, %ecx
	movq	$-39, %r11
	andl	$14, %ecx
	subq	%rcx, %r11
	leaq	(%r12,%r11), %r11
	cmpq	-80(%rbp), %r11
	jbe	.L1374
	movq	%rdx, -120(%rbp)
	movq	%rdi, %rsi
	movq	%rdi, -88(%rbp)
	movq	%r11, -80(%rbp)
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1433:
	leaq	9(%r14), %r9
	cmpq	%r10, %r9
	cmovbe	%r9, %r10
	cmpq	%r10, %rax
	jnb	.L1499
	movq	-72(%rbp), %rsi
	movq	-176(%rbp), %r9
	movabsq	$2297779722762296275, %r11
	leaq	2(%rsi), %rbx
	subq	%r14, %rbx
	movq	-112(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	%r15, %rdx
	movl	56(%r12), %ecx
	movq	-56(%rbp), %rsi
	andq	%rax, %rdx
	movq	0(%r13,%rdx), %rdi
	andq	%r9, %rdi
	movq	%rdi, %rdx
	imulq	%r11, %rdx
	shrq	%cl, %rdx
	movl	%edx, %ecx
	leaq	(%rsi,%rcx,2), %r8
	movzwl	72(%r12), %esi
	movl	8(%r12), %ecx
	movzwl	(%r8), %edi
	sall	%cl, %edx
	movq	%rax, %rcx
	andl	%edi, %esi
	addl	$1, %edi
	movzwl	%si, %esi
	addq	%rsi, %rdx
	leaq	(%r12,%rdx,4), %rdx
	movl	%eax, (%rdx,%r14)
	leaq	(%rbx,%rax), %rdx
	addq	$2, %rax
	movw	%di, (%r8)
	cmpq	%r10, %rax
	jb	.L1435
	movq	%rcx, %r9
	movq	%rdx, -72(%rbp)
	movq	%rax, %r14
	addq	$10, %r9
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	16(%rbp), %rsi
	movq	16(%rbp), %rdi
	movslq	(%rsi), %rdx
	movslq	4(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rax, %rdx
	je	.L1407
	cmpq	%rax, %rcx
	je	.L1707
	addq	$3, %rax
	movq	%rax, %r11
	subq	%rdx, %r11
	cmpq	$6, %r11
	jbe	.L1708
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L1709
	movq	16(%rbp), %rax
	movq	-120(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	je	.L1494
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L1710
	leaq	15(%rcx), %rdx
.L1411:
	testq	%rdx, %rdx
	je	.L1407
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L1409:
	movq	16(%rbp), %rcx
	movl	%edi, 8(%rcx)
	movq	-120(%rbp), %rdi
	movl	%eax, 12(%rcx)
	movl	%esi, 4(%rcx)
	movl	%edi, (%rcx)
	movl	16(%r12), %eax
	cmpl	$4, %eax
	jle	.L1414
	movd	%edi, %xmm2
	movq	16(%rbp), %rcx
	movq	16(%rbp), %r14
	pshufd	$0, %xmm2, %xmm0
	paddd	%xmm1, %xmm0
	movups	%xmm0, 16(%rcx)
	movl	%edi, %ecx
	subl	$3, %ecx
	movl	%ecx, 32(%r14)
	leal	3(%rdi), %ecx
	movl	%ecx, 36(%r14)
	cmpl	$10, %eax
	jle	.L1414
	movd	%esi, %xmm3
	leal	-3(%rsi), %eax
	addl	$3, %esi
	pshufd	$0, %xmm3, %xmm0
	movl	%eax, 56(%r14)
	paddd	%xmm1, %xmm0
	movl	%esi, 60(%r14)
	movups	%xmm0, 40(%r14)
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	24(%r12), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r12)
	jb	.L1396
	movq	-128(%rbp), %rsi
	leaq	1(%r13), %rdi
	movq	%rbx, -240(%rbp)
	leaq	2(%rax), %r10
	movq	%rdi, -208(%rbp)
	imull	$506832829, (%rsi), %edx
	movq	-152(%rbp), %rsi
	movq	%r14, -256(%rbp)
	movq	%r15, -272(%rbp)
	movq	104(%rsi), %rcx
	movq	%r13, -248(%rbp)
	movq	%rsi, %r13
	shrl	$18, %edx
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %r11
	.p2align 4,,10
	.p2align 3
.L1402:
	movzwl	(%r11,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r12)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L1397
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -184(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -104(%rbp)
	jb	.L1397
	movq	80(%r13), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r14
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r14
	je	.L1398
	movq	(%r8), %rbx
	movq	-168(%rbp), %rdx
	xorl	%r15d, %r15d
	cmpq	%rbx, %rdx
	je	.L1711
.L1399:
	xorq	%rbx, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L1401:
	movl	88(%r13), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L1397
	testq	%r15, %r15
	je	.L1397
	movq	%rdi, %rdx
	movq	96(%r13), %rbx
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	movq	-208(%rbp), %rdi
	salq	%cl, %rdx
	leaq	(%rdi,%rsi), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, -224(%rbp)
	jb	.L1397
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%ecx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-80(%rbp), %rdx
	jb	.L1397
	movzwl	-184(%rbp), %esi
	addq	$1, 32(%r12)
	movq	%rdx, -80(%rbp)
	subl	%r15d, %esi
	movq	%rcx, -120(%rbp)
	movl	%esi, -144(%rbp)
	movq	%r15, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1397:
	cmpq	%r10, %rax
	jne	.L1402
	movq	-64(%rbp), %rax
	movq	-240(%rbp), %rbx
	movq	-248(%rbp), %r13
	movq	-256(%rbp), %r14
	movq	-272(%rbp), %r15
	addq	$175, %rax
	cmpq	-80(%rbp), %rax
	jbe	.L1712
.L1403:
	movq	-192(%rbp), %rax
	movq	%r14, %r13
	movq	-160(%rbp), %r14
	movq	-280(%rbp), %r11
	cmpq	%rax, %r14
	movq	%r14, %rbx
	cmovbe	%r14, %rax
	movq	%r11, -88(%rbp)
	movq	%rax, %rdi
	movl	-136(%rbp), %eax
	movl	%eax, -144(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	%rdx, -200(%rbp)
	movq	-120(%rbp), %r11
	movq	%r12, -64(%rbp)
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	$0, -200(%rbp)
	xorl	%r11d, %r11d
	movq	$2020, -64(%rbp)
	jmp	.L1341
.L1720:
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1383:
	cmpq	$2, %rdi
	jne	.L1374
	cmpq	$1, %rax
	ja	.L1374
	movl	$2205, %r12d
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	-72(%rbp), %rdi
	movl	%edi, %edx
	sall	$3, %edi
	andl	$56, %edi
.L1418:
	cmpq	$9, %rax
	jbe	.L1713
.L1421:
	cmpq	$133, %rax
	jbe	.L1714
	cmpq	$2117, %rax
	ja	.L1424
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L1422:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L1425
.L1703:
	testb	%sil, %sil
	je	.L1425
	cmpw	$15, %ax
	ja	.L1425
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L1428:
	movq	-232(%rbp), %rax
	movq	-72(%rbp), %rsi
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	movq	-304(%rbp), %rdi
	addq	%rsi, (%rax)
	movq	-88(%rbp), %rax
	leaq	2(%rbx), %rsi
	movq	%rdi, %r10
	leaq	(%rax,%rbx), %r14
	cmpq	%rdi, %r14
	movq	-120(%rbp), %rdi
	cmovbe	%r14, %r10
	shrq	$2, %rax
	cmpq	%rdi, %rax
	jbe	.L1429
	movq	%rdi, %rax
	movq	%r14, %rdi
	salq	$2, %rax
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	cmovnb	%rdi, %rsi
	cmpq	%r10, %rsi
	cmova	%r10, %rsi
.L1429:
	leaq	8(%r14), %r9
	cmpq	%r10, %rsi
	jnb	.L1498
	movq	40(%r12), %rax
	movq	64(%r12), %r11
	movq	%r9, -72(%rbp)
	movq	-56(%rbp), %r9
	leaq	80(%rax,%rax), %rbx
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	%r15, %rax
	movl	56(%r12), %ecx
	andq	%rsi, %rax
	movq	0(%r13,%rax), %rdi
	andq	%r11, %rdi
	movq	%rdi, %rax
	movabsq	$2297779722762296275, %rdi
	imulq	%rdi, %rax
	shrq	%cl, %rax
	movl	8(%r12), %ecx
	movl	%eax, %edx
	leaq	(%r9,%rdx,2), %r8
	movzwl	72(%r12), %edx
	sall	%cl, %eax
	movzwl	(%r8), %edi
	andl	%edi, %edx
	addl	$1, %edi
	movzwl	%dx, %edx
	addq	%rdx, %rax
	leaq	(%r12,%rax,4), %rax
	movl	%esi, (%rax,%rbx)
	addq	$1, %rsi
	movw	%di, (%r8)
	cmpq	%rsi, %r10
	jne	.L1431
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %r9
	movq	$0, -72(%rbp)
	movq	%rax, -232(%rbp)
	jmp	.L1430
.L1407:
	movq	-232(%rbp), %rsi
	xorl	%edx, %edx
	leaq	16(%rsi), %rax
	movq	%rax, -64(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-144(%rbp), %eax
	sall	$25, %eax
	orl	-88(%rbp), %eax
	movl	%eax, 4(%rsi)
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	.L1416
.L1697:
	movl	-144(%rbp), %eax
	subq	$1, -104(%rbp)
	movq	%rbx, -160(%rbp)
	movl	%eax, -136(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L1405
.L1357:
	cmpq	$0, -240(%rbp)
	je	.L1356
	movq	-168(%rbp), %rcx
	movq	-80(%rbp), %rbx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%rbx)
	jne	.L1356
	movq	-264(%rbp), %rbx
	cmpq	$0, -248(%rbp)
	movq	%rbx, -272(%rbp)
	je	.L1356
	movq	-80(%rbp), %rcx
	movq	-168(%rbp), %rbx
	movq	$1, -104(%rbp)
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%rbx)
	jne	.L1356
.L1450:
	movq	-104(%rbp), %rcx
	cmpq	$2, -240(%rbp)
	leaq	1(%rcx), %rbx
	movq	%rbx, -104(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movq	-272(%rbp), %rbx
	movzbl	1(%r10,%rcx), %r10d
	cmpb	%r10b, 1(%rbx)
	jne	.L1361
	leaq	2(%rcx), %r10
	cmpq	$3, -240(%rbp)
	movq	%r10, -104(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movzbl	2(%r10,%rcx), %r10d
	cmpb	%r10b, 2(%rbx)
	jne	.L1361
	leaq	3(%rcx), %r10
	cmpq	$4, -240(%rbp)
	movq	%r10, -104(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movzbl	3(%r10,%rcx), %r10d
	cmpb	%r10b, 3(%rbx)
	jne	.L1361
	leaq	4(%rcx), %r10
	cmpq	$5, -240(%rbp)
	movq	%r10, -104(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movzbl	4(%r10,%rcx), %r10d
	cmpb	%r10b, 4(%rbx)
	jne	.L1361
	leaq	5(%rcx), %r10
	cmpq	$6, -240(%rbp)
	movq	%r10, -104(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movzbl	5(%r10,%rcx), %r10d
	cmpb	%r10b, 5(%rbx)
	jne	.L1361
	leaq	6(%rcx), %r10
	cmpq	$7, -240(%rbp)
	movq	%r10, -104(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movzbl	6(%rbx), %ebx
	cmpb	%bl, 6(%r10,%rcx)
	jne	.L1361
	addq	$7, %rcx
	movq	%rcx, -104(%rbp)
	jmp	.L1361
.L1692:
	movq	-256(%rbp), %rbx
	leaq	8(%r10), %rcx
	addq	%r10, %rbx
	subq	$1, -104(%rbp)
	je	.L1715
	movq	%rcx, %r10
	jmp	.L1359
.L1478:
	movq	%rdx, -120(%rbp)
	movq	%rdi, %rsi
	movq	%rdi, -88(%rbp)
	movq	%r12, -80(%rbp)
	jmp	.L1374
.L1690:
	movl	4(%rsi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 40(%rsi)
	leal	1(%rax), %edx
	movl	%edx, 44(%rsi)
	leal	-2(%rax), %edx
	movl	%edx, 48(%rsi)
	leal	2(%rax), %edx
	movl	%edx, 52(%rsi)
	leal	-3(%rax), %edx
	addl	$3, %eax
	movl	%eax, 60(%rsi)
	leaq	8(%r14), %rax
	movl	%edx, 56(%rsi)
	cmpq	%rax, -96(%rbp)
	ja	.L1716
.L1454:
	xorl	%edx, %edx
	jmp	.L1340
.L1708:
	leal	0(,%r11,4), %ecx
	movl	$158663784, %eax
	sarl	%cl, %eax
	movl	%eax, %edx
	andl	$15, %edx
	jmp	.L1411
.L1701:
	movq	-72(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L1418
.L1714:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L1422
.L1343:
	cmpq	$0, -144(%rbp)
	je	.L1342
	movq	-136(%rbp), %rcx
	movq	-80(%rbp), %rbx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%rbx)
	jne	.L1342
	movq	-176(%rbp), %rbx
	cmpq	$0, -160(%rbp)
	movq	%rbx, -184(%rbp)
	je	.L1342
	movq	-136(%rbp), %rcx
	movq	-80(%rbp), %rbx
	movq	$1, -120(%rbp)
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%rbx)
	jne	.L1351
.L1451:
	movq	-120(%rbp), %r8
	cmpq	$2, -144(%rbp)
	leaq	1(%r8), %rbx
	movq	%rbx, -120(%rbp)
	je	.L1347
	movq	-136(%rbp), %r12
	movq	-184(%rbp), %rcx
	movzbl	1(%r12,%r8), %ebx
	cmpb	%bl, 1(%rcx)
	jne	.L1347
	leaq	2(%r8), %rbx
	cmpq	$3, -144(%rbp)
	movq	%rbx, -120(%rbp)
	je	.L1347
	movzbl	2(%r12,%r8), %ebx
	cmpb	%bl, 2(%rcx)
	jne	.L1347
	leaq	3(%r8), %rbx
	cmpq	$4, -144(%rbp)
	movq	%rbx, -120(%rbp)
	je	.L1347
	movzbl	3(%r12,%r8), %ebx
	cmpb	%bl, 3(%rcx)
	jne	.L1347
	leaq	4(%r8), %rbx
	cmpq	$5, -144(%rbp)
	movq	%rbx, -120(%rbp)
	je	.L1347
	movzbl	4(%r12,%r8), %ebx
	cmpb	%bl, 4(%rcx)
	jne	.L1347
	leaq	5(%r8), %rbx
	cmpq	$6, -144(%rbp)
	movq	%rbx, -120(%rbp)
	je	.L1347
	movzbl	5(%r12,%r8), %ebx
	cmpb	%bl, 5(%rcx)
	jne	.L1347
	leaq	6(%r8), %rbx
	cmpq	$7, -144(%rbp)
	movq	%rbx, -120(%rbp)
	je	.L1347
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r12,%r8)
	jne	.L1347
	addq	$7, %r8
	movq	%r8, -120(%rbp)
	jmp	.L1347
.L1704:
	movq	-168(%rbp), %rcx
	leaq	8(%rbx), %r8
	addq	%rbx, %rcx
	subq	$1, -120(%rbp)
	je	.L1717
	movq	%r8, %rbx
	jmp	.L1345
.L1367:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1366
	movq	-80(%rbp), %rcx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L1366
.L1444:
	leaq	1(%r14), %r15
	cmpq	$1, %rdx
	je	.L1370
	movq	-80(%rbp), %rcx
	movzbl	1(%rcx,%r14), %r13d
	cmpb	%r13b, 1(%r8)
	jne	.L1370
	leaq	2(%r14), %r15
	cmpq	$2, %rdx
	je	.L1370
	movzbl	2(%rcx,%r14), %r13d
	cmpb	%r13b, 2(%r8)
	jne	.L1370
	leaq	3(%r14), %r15
	cmpq	$3, %rdx
	je	.L1370
	movzbl	3(%rcx,%r14), %r13d
	cmpb	%r13b, 3(%r8)
	jne	.L1370
	leaq	4(%r14), %r15
	cmpq	$4, %rdx
	je	.L1370
	movzbl	4(%rcx,%r14), %r13d
	cmpb	%r13b, 4(%r8)
	jne	.L1370
	leaq	5(%r14), %r15
	subq	$5, %rdx
	je	.L1370
	movzbl	5(%rcx,%r14), %r13d
	cmpb	%r13b, 5(%r8)
	jne	.L1370
	leaq	6(%r14), %r15
	cmpq	$1, %rdx
	je	.L1370
	movzbl	6(%rcx,%r14), %ecx
	cmpb	%cl, 6(%r8)
	jne	.L1370
	leaq	7(%r14), %r15
	jmp	.L1370
.L1705:
	leaq	8(%r8), %rdx
	subq	$1, %r14
	je	.L1467
	movq	-80(%rbp), %rdx
	movq	8(%r8), %r13
	movl	$8, %r15d
	movq	8(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L1368
	leaq	16(%r8), %rdx
	cmpq	$1, %r14
	je	.L1469
	movq	-80(%rbp), %rdx
	movq	16(%r8), %r13
	movl	$16, %r15d
	addq	$24, %r8
	movl	$24, %r14d
	movq	16(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L1368
.L1369:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1472
	movq	-80(%rbp), %rcx
	movzbl	(%rcx,%r14), %ecx
	cmpb	%cl, (%r8)
	je	.L1444
.L1472:
	movq	%r14, %r15
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1424:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L1427
.L1702:
	movq	-72(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L1418
.L1474:
	movq	$0, -120(%rbp)
	movq	$0, -88(%rbp)
	movq	$2020, -80(%rbp)
	jmp	.L1373
.L1498:
	movq	-64(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	%rax, -232(%rbp)
	jmp	.L1430
.L1389:
	cmpq	$0, -296(%rbp)
	je	.L1388
	movq	-128(%rbp), %r11
	movq	-256(%rbp), %rcx
	movzbl	(%r11), %r11d
	cmpb	%r11b, (%rcx)
	jne	.L1388
	movq	-328(%rbp), %rcx
	cmpq	$0, -320(%rbp)
	movq	%rcx, -344(%rbp)
	je	.L1388
	movq	-256(%rbp), %r11
	movq	-128(%rbp), %rcx
	movq	$1, -144(%rbp)
	movzbl	1(%r11), %r11d
	cmpb	%r11b, 1(%rcx)
	jne	.L1388
.L1448:
	movq	-144(%rbp), %rcx
	cmpq	$2, -296(%rbp)
	leaq	1(%rcx), %r11
	movq	%r11, -144(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movq	-344(%rbp), %r11
	movzbl	1(%r9,%rcx), %r9d
	cmpb	%r9b, 1(%r11)
	jne	.L1393
	leaq	2(%rcx), %r9
	cmpq	$3, -296(%rbp)
	movq	%r9, -144(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movzbl	2(%r9,%rcx), %r9d
	cmpb	%r9b, 2(%r11)
	jne	.L1393
	leaq	3(%rcx), %r9
	cmpq	$4, -296(%rbp)
	movq	%r9, -144(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movzbl	3(%r9,%rcx), %r9d
	cmpb	%r9b, 3(%r11)
	jne	.L1393
	leaq	4(%rcx), %r9
	cmpq	$5, -296(%rbp)
	movq	%r9, -144(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movzbl	4(%r9,%rcx), %r9d
	cmpb	%r9b, 4(%r11)
	jne	.L1393
	leaq	5(%rcx), %r9
	cmpq	$6, -296(%rbp)
	movq	%r9, -144(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movzbl	5(%r9,%rcx), %r9d
	cmpb	%r9b, 5(%r11)
	jne	.L1393
	leaq	6(%rcx), %r9
	cmpq	$7, -296(%rbp)
	movq	%r9, -144(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movzbl	6(%r11), %r11d
	cmpb	%r11b, 6(%r9,%rcx)
	jne	.L1393
	addq	$7, %rcx
	movq	%rcx, -144(%rbp)
	jmp	.L1393
.L1695:
	subq	$1, -144(%rbp)
	movq	-336(%rbp), %rcx
	leaq	(%rcx,%r9), %r11
	leaq	8(%r9), %rcx
	je	.L1718
	movq	%rcx, %r9
	jmp	.L1391
.L1496:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L1418
.L1707:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L1409
.L1706:
	subq	$1, -272(%rbp)
	movq	-256(%rbp), %rdi
	leaq	(%rdi,%r11), %rcx
	leaq	8(%r11), %rdi
	je	.L1719
	movq	%rdi, %r11
	jmp	.L1377
.L1375:
	cmpq	$0, -208(%rbp)
	je	.L1374
	movq	-128(%rbp), %rcx
	movq	-168(%rbp), %rdi
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%rdi)
	jne	.L1374
	movq	-248(%rbp), %rdi
	cmpq	$0, -240(%rbp)
	movq	%rdi, -296(%rbp)
	je	.L1374
	movq	-128(%rbp), %rcx
	movq	-168(%rbp), %rdi
	movq	$1, -272(%rbp)
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%rdi)
	jne	.L1720
.L1449:
	movq	-272(%rbp), %r12
	cmpq	$2, -208(%rbp)
	leaq	1(%r12), %rdi
	je	.L1379
	movq	-168(%rbp), %r11
	movq	-296(%rbp), %rcx
	movzbl	1(%r11,%r12), %r12d
	cmpb	%r12b, 1(%rcx)
	jne	.L1379
	movq	-272(%rbp), %r12
	cmpq	$3, -208(%rbp)
	leaq	2(%r12), %rdi
	je	.L1379
	movzbl	2(%r11,%r12), %r12d
	cmpb	%r12b, 2(%rcx)
	jne	.L1379
	movq	-272(%rbp), %r12
	cmpq	$4, -208(%rbp)
	leaq	3(%r12), %rdi
	je	.L1379
	movzbl	3(%r11,%r12), %r12d
	cmpb	%r12b, 3(%rcx)
	jne	.L1379
	movq	-272(%rbp), %r12
	cmpq	$5, -208(%rbp)
	leaq	4(%r12), %rdi
	je	.L1379
	movzbl	4(%r11,%r12), %r12d
	cmpb	%r12b, 4(%rcx)
	jne	.L1379
	movq	-272(%rbp), %r12
	cmpq	$6, -208(%rbp)
	leaq	5(%r12), %rdi
	je	.L1379
	movzbl	5(%r11,%r12), %r12d
	cmpb	%r12b, 5(%rcx)
	jne	.L1379
	movq	-272(%rbp), %r12
	cmpq	$7, -208(%rbp)
	leaq	6(%r12), %rdi
	je	.L1379
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r11,%r12)
	jne	.L1379
	addq	$1, %rdi
	jmp	.L1379
.L1398:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1397
	movq	-128(%rbp), %rbx
	movzbl	(%rbx), %ebx
	cmpb	%bl, (%r8)
	jne	.L1397
.L1440:
	leaq	1(%r14), %r15
	cmpq	$1, %rdx
	je	.L1401
	movq	-128(%rbp), %rbx
	movzbl	1(%rbx,%r14), %ecx
	cmpb	%cl, 1(%r8)
	jne	.L1401
	leaq	2(%r14), %r15
	cmpq	$2, %rdx
	je	.L1401
	movzbl	2(%rbx,%r14), %ecx
	cmpb	%cl, 2(%r8)
	jne	.L1401
	leaq	3(%r14), %r15
	cmpq	$3, %rdx
	je	.L1401
	movzbl	3(%rbx,%r14), %ecx
	cmpb	%cl, 3(%r8)
	jne	.L1401
	leaq	4(%r14), %r15
	cmpq	$4, %rdx
	je	.L1401
	movzbl	4(%rbx,%r14), %ecx
	cmpb	%cl, 4(%r8)
	jne	.L1401
	leaq	5(%r14), %r15
	subq	$5, %rdx
	je	.L1401
	movzbl	5(%rbx,%r14), %ecx
	cmpb	%cl, 5(%r8)
	jne	.L1401
	leaq	6(%r14), %r15
	cmpq	$1, %rdx
	je	.L1401
	movzbl	6(%rbx,%r14), %ebx
	cmpb	%bl, 6(%r8)
	jne	.L1401
	leaq	7(%r14), %r15
	jmp	.L1401
.L1499:
	movq	%rdx, -72(%rbp)
	movq	%rax, %r14
	jmp	.L1430
.L1711:
	leaq	8(%r8), %rdx
	subq	$1, %r14
	je	.L1487
	movq	-128(%rbp), %rdx
	movq	8(%r8), %rbx
	movl	$8, %r15d
	movq	8(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1399
	leaq	16(%r8), %rdx
	cmpq	$1, %r14
	je	.L1489
	movq	-128(%rbp), %rdx
	movq	16(%r8), %rbx
	movl	$16, %r15d
	addq	$24, %r8
	movl	$24, %r14d
	movq	16(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1399
.L1400:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1492
	movq	-128(%rbp), %rbx
	movzbl	(%rbx,%r14), %ebx
	cmpb	%bl, (%r8)
	je	.L1440
.L1492:
	movq	%r14, %r15
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1709:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L1411
.L1715:
	cmpq	$0, -240(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r10, -280(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movzbl	(%r10,%rcx), %r10d
	cmpb	%r10b, (%rbx)
	jne	.L1361
	leaq	1(%rbx), %rcx
	cmpq	$0, -248(%rbp)
	movq	%rcx, -272(%rbp)
	movq	-280(%rbp), %rcx
	leaq	9(%rcx), %r10
	movq	%r10, -104(%rbp)
	je	.L1361
	movq	-168(%rbp), %r10
	movzbl	9(%r10,%rcx), %ecx
	cmpb	%cl, 1(%rbx)
	je	.L1450
	jmp	.L1361
.L1718:
	cmpq	$0, -296(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r9, -352(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movq	%r11, -344(%rbp)
	movzbl	(%r9,%rcx), %r9d
	cmpb	%r9b, (%r11)
	jne	.L1393
	movq	%r11, %rcx
	leaq	1(%r11), %r11
	cmpq	$0, -320(%rbp)
	movq	%r11, -344(%rbp)
	movq	-352(%rbp), %r11
	leaq	9(%r11), %r9
	movq	%r9, -144(%rbp)
	je	.L1393
	movq	-256(%rbp), %r9
	movzbl	9(%r9,%r11), %r9d
	cmpb	%r9b, 1(%rcx)
	je	.L1448
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1717:
	cmpq	$0, -144(%rbp)
	movq	%r8, -120(%rbp)
	movq	%r12, -104(%rbp)
	je	.L1349
	movq	-136(%rbp), %r12
	movq	-120(%rbp), %r8
	movzbl	(%r12,%r8), %r12d
	cmpb	%r12b, (%rcx)
	jne	.L1349
	leaq	1(%rcx), %r8
	cmpq	$0, -160(%rbp)
	movq	%r8, -184(%rbp)
	leaq	9(%rbx), %r8
	movq	%r8, -120(%rbp)
	je	.L1347
	movq	-136(%rbp), %r12
	movzbl	9(%r12,%rbx), %ebx
	cmpb	%bl, 1(%rcx)
	je	.L1451
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	%rdx, %r8
	movl	$8, %r14d
	jmp	.L1369
.L1494:
	movl	$2, %edx
	jmp	.L1409
.L1710:
	movl	$3, %edx
	jmp	.L1409
.L1719:
	cmpq	$0, -208(%rbp)
	movq	%r12, -144(%rbp)
	je	.L1381
	movq	-168(%rbp), %r12
	movzbl	(%r12,%rdi), %r12d
	cmpb	%r12b, (%rcx)
	jne	.L1382
	movq	%rcx, %rdi
	leaq	1(%rcx), %rcx
	cmpq	$0, -240(%rbp)
	movq	%rcx, -296(%rbp)
	leaq	9(%r11), %rcx
	movq	%rcx, -272(%rbp)
	je	.L1477
	movq	-168(%rbp), %r12
	movzbl	9(%r12,%r11), %ecx
	cmpb	%cl, 1(%rdi)
	je	.L1449
.L1477:
	movq	-272(%rbp), %rdi
	jmp	.L1379
.L1487:
	movq	%rdx, %r8
	movl	$8, %r14d
	jmp	.L1400
.L1469:
	movq	%rdx, %r8
	movl	$16, %r14d
	jmp	.L1369
.L1382:
	cmpq	$2, %rdi
	ja	.L1442
	jmp	.L1374
.L1381:
	cmpq	$2, %rdi
	jbe	.L1374
	imulq	$135, %rdi, %r11
	leaq	1935(%r11), %r12
	jmp	.L1384
.L1489:
	movq	%rdx, %r8
	movl	$16, %r14d
	jmp	.L1400
.L1699:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L1416
	.cfi_endproc
.LFE284:
	.size	CreateBackwardReferencesNH6, .-CreateBackwardReferencesNH6
	.p2align 4
	.type	CreateBackwardReferencesNH40, @function
CreateBackwardReferencesNH40:
.LFB285:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rdx, %r10
	movl	$256, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$240, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -160(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	leaq	(%rsi,%rdi), %rcx
	subq	$16, %rax
	cmpq	$3, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rax, -176(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-3(%rcx), %rax
	cmovbe	%rsi, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -328(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -304(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movq	%rax, %rbx
	movq	%rax, -336(%rbp)
	movq	%rsi, %rax
	addq	%rbx, %rax
	movq	%rax, -248(%rbp)
	leaq	4(%rsi), %rax
	cmpq	%rcx, %rax
	jnb	.L1834
	movq	32(%rbp), %rax
	movq	%r14, %r12
	movq	%r9, %r15
	movq	%rsi, %r14
	movq	%r10, %r13
	movq	%rax, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L1815:
	movq	-176(%rbp), %rax
	movq	-88(%rbp), %rdi
	movq	%r12, %r10
	movq	$0, -168(%rbp)
	movq	$0, -72(%rbp)
	subq	%r14, %rdi
	movq	%rax, %r9
	cmpq	%r14, %rax
	movq	-160(%rbp), %rax
	cmova	%r14, %r9
	andq	%r14, %r10
	movq	%rdi, -104(%rbp)
	movq	72(%rax), %rax
	leaq	0(%r13,%r10), %rsi
	movq	$2020, -56(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rax, -192(%rbp)
	imull	$506832829, (%rsi), %eax
	movq	%r10, -128(%rbp)
	shrl	$17, %eax
	movl	%eax, %ebx
	movl	%eax, -116(%rbp)
	movq	%rbx, -112(%rbp)
	movl	%eax, %ebx
	movq	%rdi, %rax
	andl	$7, %edi
	shrq	$3, %rax
	movq	%rdi, %r11
	movq	%rax, -80(%rbp)
	leaq	-1(%rdi), %rax
	movq	%r11, %r10
	movq	%rax, -216(%rbp)
	leaq	-2(%rdi), %rax
	movq	%rax, -224(%rbp)
	leaq	-3(%rdi), %rax
	movq	%rax, -232(%rbp)
	leaq	-4(%rdi), %rax
	movq	%rax, -240(%rbp)
	leaq	-5(%rdi), %rax
	movq	%rax, -256(%rbp)
	leaq	-6(%rdi), %rax
	movq	%rax, -264(%rbp)
	leaq	-7(%rdi), %rax
	leaq	1(%rsi), %rdi
	addq	$8, %rsi
	movq	%rdi, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rax, -272(%rbp)
	xorl	%eax, %eax
.L1735:
	movq	16(%rbp), %rdi
	movq	%r14, %rcx
	leaq	1(%rax), %rsi
	movslq	(%rdi,%rax,4), %rdi
	subq	%rdi, %rcx
	testq	%rax, %rax
	je	.L1725
	movzwl	%cx, %edx
	cmpb	%bl, 196648(%r15,%rdx)
	jne	.L1726
	cmpq	%r14, %rcx
	jnb	.L1726
	cmpq	%r9, %rdi
	ja	.L1726
	movq	-80(%rbp), %rdx
	andq	%r12, %rcx
	addq	%r13, %rcx
	testq	%rdx, %rdx
	je	.L1727
.L2067:
	movq	%rdx, -96(%rbp)
	xorl	%r8d, %r8d
.L1729:
	movq	-48(%rbp), %rdx
	movq	(%rcx,%r8), %r11
	movq	(%rdx,%r8), %rdx
	cmpq	%r11, %rdx
	je	.L2064
	xorq	%r11, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	leaq	(%rdx,%r8), %rcx
	movq	%rcx, -96(%rbp)
.L1731:
	movq	-96(%rbp), %rcx
	cmpq	$1, %rcx
	jbe	.L1726
	leaq	(%rcx,%rcx,8), %rcx
	movq	-56(%rbp), %r11
	movq	%rcx, %r8
	salq	$4, %r8
	subq	%rcx, %r8
	addq	$1935, %r8
	cmpq	%r11, %r8
	jbe	.L1726
	testq	%rax, %rax
	je	.L1838
	leal	-1(%rsi), %ecx
	movl	$117264, %eax
	andl	$14, %ecx
	sarl	%cl, %eax
	movq	$-39, %rcx
	andl	$14, %eax
	subq	%rax, %rcx
	addq	%rcx, %r8
	cmpq	%r11, %r8
	ja	.L2065
	.p2align 4,,10
	.p2align 3
.L1726:
	cmpq	$4, %rsi
	je	.L2066
.L1733:
	movq	%rsi, %rax
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1725:
	cmpq	%r14, %rcx
	jnb	.L1733
	cmpq	%r9, %rdi
	ja	.L1733
	movq	-80(%rbp), %rdx
	andq	%r12, %rcx
	addq	%r13, %rcx
	testq	%rdx, %rdx
	jne	.L2067
.L1727:
	testq	%r10, %r10
	je	.L1726
	movq	-48(%rbp), %rdx
	movzbl	(%rcx), %r11d
	cmpb	%r11b, (%rdx)
	jne	.L1726
	movq	-144(%rbp), %rdx
	cmpq	$0, -216(%rbp)
	movq	%rdx, -152(%rbp)
	je	.L1726
	movq	-48(%rbp), %rdx
	movl	$1, %r11d
	movzbl	1(%rdx), %edx
	cmpb	%dl, 1(%rcx)
	jne	.L1726
.L1831:
	leaq	1(%r11), %rdx
	cmpq	$0, -224(%rbp)
	movq	%rdx, -96(%rbp)
	je	.L1731
	movq	-152(%rbp), %r8
	movzbl	1(%rcx,%r11), %edx
	cmpb	%dl, 1(%r8)
	jne	.L1731
	leaq	2(%r11), %rdx
	cmpq	$0, -232(%rbp)
	movq	%rdx, -96(%rbp)
	je	.L1731
	movzbl	2(%rcx,%r11), %edx
	cmpb	%dl, 2(%r8)
	jne	.L1731
	leaq	3(%r11), %rdx
	cmpq	$0, -240(%rbp)
	movq	%rdx, -96(%rbp)
	je	.L1731
	movzbl	3(%rcx,%r11), %edx
	cmpb	%dl, 3(%r8)
	jne	.L1731
	leaq	4(%r11), %rdx
	cmpq	$0, -256(%rbp)
	movq	%rdx, -96(%rbp)
	je	.L1731
	movzbl	4(%rcx,%r11), %edx
	cmpb	%dl, 4(%r8)
	jne	.L1731
	leaq	5(%r11), %rdx
	cmpq	$0, -264(%rbp)
	movq	%rdx, -96(%rbp)
	je	.L1731
	movzbl	5(%rcx,%r11), %edx
	cmpb	%dl, 5(%r8)
	jne	.L1731
	leaq	6(%r11), %rdx
	cmpq	$0, -272(%rbp)
	movq	%rdx, -96(%rbp)
	je	.L1731
	movzbl	6(%r8), %edx
	cmpb	%dl, 6(%rcx,%r11)
	jne	.L1731
	leaq	7(%r11), %rcx
	movq	%rcx, -96(%rbp)
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	-112(%rbp), %rsi
	movq	%r14, %rbx
	movq	524336(%r15), %rdi
	movq	%r10, %r11
	movq	-128(%rbp), %r10
	leaq	(%r15,%rsi,4), %rax
	leaq	-1(%rdi), %rcx
	movq	%rdi, -184(%rbp)
	movq	%rax, -144(%rbp)
	movl	40(%rax), %eax
	movq	%rcx, -200(%rbp)
	subq	%rax, %rbx
	movq	%rbx, -112(%rbp)
	movq	%rbx, %rax
	leaq	(%r15,%rsi,2), %rbx
	movzwl	131112(%rbx), %esi
	movq	%rbx, -136(%rbp)
	movw	%si, -128(%rbp)
	testq	%rdi, %rdi
	je	.L1736
	cmpq	%r9, %rax
	ja	.L1736
	movq	-48(%rbp), %rbx
	movq	%r11, -280(%rbp)
	leaq	1(%rbx), %rdi
	addq	$8, %rbx
	movq	%rdi, -296(%rbp)
	movq	-72(%rbp), %rdi
	movq	%rbx, -288(%rbp)
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1738:
	subq	$1, %rcx
	cmpq	$-1, %rcx
	je	.L2050
.L1745:
	addq	%rdx, %rax
	cmpq	%r9, %rax
	ja	.L2050
.L1737:
	leaq	(%r15,%rsi,4), %rdx
	leaq	(%r10,%rdi), %r11
	movzwl	262186(%rdx), %esi
	movzwl	262184(%rdx), %edx
	cmpq	%r11, %r12
	jb	.L1738
	movq	%r14, %r8
	subq	%rax, %r8
	andq	%r12, %r8
	leaq	(%r8,%rdi), %rbx
	cmpq	%rbx, %r12
	jb	.L1738
	movzbl	0(%r13,%rbx), %ebx
	cmpb	%bl, 0(%r13,%r11)
	jne	.L1738
	leaq	0(%r13,%r8), %rbx
	movq	%rbx, -72(%rbp)
	movq	-80(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1739
	movq	%rbx, -96(%rbp)
	xorl	%r11d, %r11d
.L1741:
	movq	-48(%rbp), %rbx
	movq	(%rbx,%r11), %r8
	movq	-72(%rbp), %rbx
	movq	(%rbx,%r11), %rbx
	cmpq	%rbx, %r8
	je	.L2068
	xorq	%rbx, %r8
	movq	%r11, %rbx
	movq	%r11, -152(%rbp)
	rep bsfq	%r8, %r8
	movslq	%r8d, %r8
	shrq	$3, %r8
	addq	%r8, %rbx
	movq	%rbx, -96(%rbp)
.L1743:
	movq	-96(%rbp), %r8
	cmpq	$3, %r8
	jbe	.L1738
	leaq	(%r8,%r8,8), %rbx
	movq	%rbx, %r11
	salq	$4, %r11
	subq	%rbx, %r11
	bsrl	%eax, %ebx
	imull	$30, %ebx, %ebx
	addq	$1920, %r11
	subq	%rbx, %r11
	cmpq	-56(%rbp), %r11
	jbe	.L1738
	subq	$1, %rcx
	movq	%rax, -168(%rbp)
	movq	%r8, %rdi
	movq	%r11, -56(%rbp)
	cmpq	$-1, %rcx
	jne	.L1745
	.p2align 4,,10
	.p2align 3
.L2050:
	movq	%rdi, -72(%rbp)
.L1736:
	movzwl	524328(%r15), %eax
	movzbl	-116(%rbp), %esi
	movzwl	%r14w, %edx
	movl	$65535, %ecx
	movq	-112(%rbp), %rbx
	movzwl	-128(%rbp), %edi
	movl	$0, -116(%rbp)
	leal	1(%rax), %r11d
	movw	%r11w, 524328(%r15)
	cmpq	$65535, %rbx
	movb	%sil, 196648(%r15,%rdx)
	movzwl	%ax, %edx
	cmovbe	%rbx, %rcx
	movq	-144(%rbp), %rsi
	leaq	(%r15,%rdx,4), %rdx
	cmpq	$2020, -56(%rbp)
	movw	%di, 262186(%rdx)
	movq	-136(%rbp), %rdi
	movw	%cx, 262184(%rdx)
	movl	%r14d, 40(%rsi)
	movw	%ax, 131112(%rdi)
	je	.L2069
.L1746:
	movq	-104(%rbp), %rax
	movw	%r11w, -256(%rbp)
	movq	%r14, -144(%rbp)
	leaq	-1(%rax), %rbx
	subq	$4, %rax
	movq	%rbx, -104(%rbp)
	movq	-160(%rbp), %rbx
	movq	%rax, -280(%rbp)
	movl	4(%rbx), %ebx
	movl	%ebx, -272(%rbp)
.L1785:
	xorl	%edi, %edi
	cmpl	$4, -272(%rbp)
	jg	.L1754
	movq	-72(%rbp), %rax
	leaq	-1(%rax), %rdi
	movq	-104(%rbp), %rax
	cmpq	%rax, %rdi
	cmova	%rax, %rdi
.L1754:
	movq	-144(%rbp), %rax
	movq	$0, -112(%rbp)
	movq	$2020, -80(%rbp)
	leaq	1(%rax), %rbx
	movq	-176(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	%rbx, %r14
	cmpq	%rax, %rbx
	movq	%rax, %r11
	cmovbe	%rbx, %r11
	andq	%r12, %r14
	leaq	0(%r13,%r14), %rcx
	movq	%r14, -224(%rbp)
	imull	$506832829, (%rcx), %eax
	movq	%rcx, -48(%rbp)
	shrl	$17, %eax
	movl	%eax, %esi
	movl	%eax, -216(%rbp)
	movl	%eax, %r10d
	movq	-104(%rbp), %rax
	movq	%rsi, -152(%rbp)
	movq	%rax, %rsi
	andl	$7, %eax
	shrq	$3, %rsi
	movq	%rax, -288(%rbp)
	movq	%rsi, -128(%rbp)
	leaq	-1(%rax), %rsi
	movq	%rsi, -296(%rbp)
	leaq	-2(%rax), %rsi
	movq	%rsi, -312(%rbp)
	leaq	-3(%rax), %rsi
	movq	%rsi, -320(%rbp)
	leaq	-4(%rax), %rsi
	movq	%rsi, -344(%rbp)
	leaq	-5(%rax), %rsi
	movq	%rsi, -352(%rbp)
	leaq	-6(%rax), %rsi
	subq	$7, %rax
	movq	%rsi, -368(%rbp)
	leaq	1(%rcx), %rsi
	movq	%rsi, -240(%rbp)
	leaq	8(%rcx), %rsi
	movq	%rsi, -232(%rbp)
	movq	%rax, -360(%rbp)
	xorl	%eax, %eax
.L1765:
	movq	16(%rbp), %rsi
	movq	%rbx, %rdx
	leaq	1(%rax), %r8
	movslq	(%rsi,%rax,4), %rsi
	subq	%rsi, %rdx
	testq	%rax, %rax
	je	.L1755
	movzwl	%dx, %ecx
	cmpb	196648(%r15,%rcx), %r10b
	jne	.L1756
	cmpq	%rdx, %rbx
	jbe	.L1756
	cmpq	%r11, %rsi
	ja	.L1756
.L1819:
	movq	-128(%rbp), %rcx
	andq	%r12, %rdx
	leaq	0(%r13,%rdx), %r9
	testq	%rcx, %rcx
	je	.L1757
	movq	%rcx, -136(%rbp)
	xorl	%ecx, %ecx
.L1759:
	movq	-48(%rbp), %rdx
	movq	(%r9,%rcx), %r14
	movq	(%rdx,%rcx), %rdx
	cmpq	%r14, %rdx
	je	.L2070
	xorq	%r14, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
.L1761:
	movq	-136(%rbp), %rcx
	cmpq	$1, %rcx
	jbe	.L1756
	leaq	(%rcx,%rcx,8), %rcx
	movq	-80(%rbp), %r14
	movq	%rcx, %r9
	salq	$4, %r9
	subq	%rcx, %r9
	addq	$1935, %r9
	cmpq	%r14, %r9
	jbe	.L1756
	testq	%rax, %rax
	je	.L1855
	leal	-1(%r8), %ecx
	movl	$117264, %eax
	andl	$14, %ecx
	sarl	%cl, %eax
	movq	$-39, %rcx
	andl	$14, %eax
	subq	%rax, %rcx
	addq	%rcx, %r9
	cmpq	%r14, %r9
	ja	.L2071
	.p2align 4,,10
	.p2align 3
.L1756:
	cmpq	$4, %r8
	je	.L2072
.L1763:
	movq	%r8, %rax
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1755:
	cmpq	%r11, %rsi
	ja	.L1763
	cmpq	%rdx, %rbx
	ja	.L1819
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	-152(%rbp), %rcx
	movq	%rbx, %rsi
	movq	-224(%rbp), %r14
	leaq	(%r15,%rcx,4), %rax
	movq	%rax, -248(%rbp)
	movl	40(%rax), %eax
	subq	%rax, %rsi
	cmpq	$0, -184(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rsi, %rax
	leaq	(%r15,%rcx,2), %rsi
	movq	%rsi, -240(%rbp)
	movzwl	131112(%rsi), %esi
	movw	%si, -232(%rbp)
	je	.L1766
	cmpq	%r11, %rax
	ja	.L1766
	movq	-48(%rbp), %rdx
	movq	-200(%rbp), %rcx
	leaq	1(%rdx), %r10
	addq	$8, %rdx
	movq	%r10, -376(%rbp)
	movq	%rdx, -384(%rbp)
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1768:
	subq	$1, %rcx
	cmpq	$-1, %rcx
	je	.L1766
.L1775:
	addq	%rdx, %rax
	cmpq	%r11, %rax
	ja	.L1766
.L1767:
	leaq	(%r15,%rsi,4), %rdx
	leaq	(%r14,%rdi), %r9
	movzwl	262186(%rdx), %esi
	movzwl	262184(%rdx), %edx
	cmpq	%r9, %r12
	jb	.L1768
	movq	%rbx, %r8
	subq	%rax, %r8
	andq	%r12, %r8
	leaq	(%r8,%rdi), %r10
	cmpq	%r10, %r12
	jb	.L1768
	movzbl	0(%r13,%r10), %r10d
	cmpb	%r10b, 0(%r13,%r9)
	jne	.L1768
	leaq	0(%r13,%r8), %r10
	movq	%r10, -136(%rbp)
	movq	-128(%rbp), %r10
	testq	%r10, %r10
	je	.L1769
	movq	%r10, -152(%rbp)
	xorl	%r9d, %r9d
.L1771:
	movq	-48(%rbp), %r10
	movq	(%r10,%r9), %r8
	movq	-136(%rbp), %r10
	movq	(%r10,%r9), %r10
	cmpq	%r10, %r8
	je	.L2073
	xorq	%r10, %r8
	movq	%r9, %r10
	movq	%r9, -264(%rbp)
	rep bsfq	%r8, %r8
	movslq	%r8d, %r8
	shrq	$3, %r8
	addq	%r8, %r10
	movq	%r10, -152(%rbp)
.L1773:
	movq	-152(%rbp), %r8
	cmpq	$3, %r8
	jbe	.L1768
	leaq	(%r8,%r8,8), %r10
	movq	%r10, %r9
	salq	$4, %r9
	subq	%r10, %r9
	bsrl	%eax, %r10d
	imull	$30, %r10d, %r10d
	addq	$1920, %r9
	subq	%r10, %r9
	cmpq	-80(%rbp), %r9
	jbe	.L1768
	subq	$1, %rcx
	movq	%rax, -112(%rbp)
	movq	%r8, %rdi
	movq	%r8, -96(%rbp)
	movq	%r9, -80(%rbp)
	cmpq	$-1, %rcx
	jne	.L1775
	.p2align 4,,10
	.p2align 3
.L1766:
	movzwl	-256(%rbp), %edi
	movzbl	-216(%rbp), %esi
	movzwl	%bx, %eax
	movl	$65535, %edx
	movl	$0, -128(%rbp)
	leal	1(%rdi), %r10d
	movw	%r10w, 524328(%r15)
	movb	%sil, 196648(%r15,%rax)
	movq	-224(%rbp), %rsi
	movzwl	%di, %eax
	leaq	(%r15,%rax,4), %rax
	cmpq	$65535, %rsi
	cmovbe	%rsi, %rdx
	movzwl	-232(%rbp), %esi
	cmpq	$2020, -80(%rbp)
	movw	%dx, 262184(%rax)
	movw	%si, 262186(%rax)
	movq	-248(%rbp), %rax
	movl	%ebx, 40(%rax)
	movq	-240(%rbp), %rax
	movw	%di, 131112(%rax)
	je	.L2074
.L1776:
	movq	-56(%rbp), %rax
	addq	$175, %rax
	cmpq	-80(%rbp), %rax
	ja	.L1783
.L2088:
	addq	$1, -64(%rbp)
	movq	-280(%rbp), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L2053
	movq	-144(%rbp), %rax
	addq	$5, %rax
	cmpq	%rax, -88(%rbp)
	ja	.L2075
.L2053:
	movq	%r11, %rsi
.L1784:
	movq	-336(%rbp), %rax
	movq	-96(%rbp), %rdi
	addq	%rbx, %rax
	leaq	(%rax,%rdi,2), %rax
	movq	%rax, -248(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L2076
	leaq	15(%rax), %rdx
.L1817:
	movq	-208(%rbp), %rdi
	leaq	16(%rdi), %rax
	movq	%rax, -56(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, (%rdi)
	movl	-128(%rbp), %eax
	sall	$25, %eax
	orl	-96(%rbp), %eax
	movl	%eax, 4(%rdi)
	movq	-160(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r11
	cmpq	%rcx, %rdx
	jb	.L2077
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r11,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L1795:
	movq	-208(%rbp), %rdi
	movw	%ax, 14(%rdi)
	movl	-128(%rbp), %eax
	addl	-96(%rbp), %eax
	cmpq	$5, -64(%rbp)
	movl	%edx, 8(%rdi)
	cltq
	jbe	.L2078
	cmpq	$129, -64(%rbp)
	jbe	.L2079
	cmpq	$2113, -64(%rbp)
	jbe	.L2080
	movq	-64(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L1871
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L1800
.L2091:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L2081
	.p2align 4,,10
	.p2align 3
.L1804:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L1806:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	24(%r15), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r15)
	jb	.L1747
	movq	-48(%rbp), %rbx
	leaq	1(%r9), %rsi
	movq	%r14, -112(%rbp)
	leaq	2(%rax), %r10
	movq	-160(%rbp), %rdi
	movq	%rsi, -144(%rbp)
	imull	$506832829, (%rbx), %edx
	movw	%r11w, -96(%rbp)
	movq	104(%rdi), %rcx
	movq	%r13, -128(%rbp)
	movq	%r12, -136(%rbp)
	movq	%rdi, %r12
	shrl	$18, %edx
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %rbx
	.p2align 4,,10
	.p2align 3
.L1753:
	movzwl	(%rbx,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r15)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L1748
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -80(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -104(%rbp)
	jb	.L1748
	movq	80(%r12), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r13
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r13
	je	.L1749
	movq	-48(%rbp), %rdx
	movq	(%r8), %r11
	xorl	%r14d, %r14d
	movq	(%rdx), %rdx
	cmpq	%r11, %rdx
	je	.L2082
.L1750:
	xorq	%r11, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r14
.L1752:
	movl	88(%r12), %edx
	addq	%r14, %rdx
	cmpq	%rdx, %rdi
	jnb	.L1748
	testq	%r14, %r14
	je	.L1748
	movq	%rdi, %rdx
	movq	96(%r12), %r11
	addq	-144(%rbp), %rsi
	subq	%r14, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %r11
	movq	%r11, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	salq	%cl, %rdx
	addq	%rsi, %rdx
	cmpq	%rdx, -192(%rbp)
	jb	.L1748
	leaq	(%r14,%r14,8), %rsi
	movq	%rsi, %rcx
	salq	$4, %rcx
	subq	%rsi, %rcx
	bsrl	%edx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rcx
	subq	%rsi, %rcx
	cmpq	-56(%rbp), %rcx
	jb	.L1748
	movzwl	-80(%rbp), %esi
	addq	$1, 32(%r15)
	movq	%rcx, -56(%rbp)
	subl	%r14d, %esi
	movq	%rdx, -168(%rbp)
	movl	%esi, -116(%rbp)
	movq	%r14, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L1748:
	cmpq	%r10, %rax
	jne	.L1753
	cmpq	$2020, -56(%rbp)
	movzwl	-96(%rbp), %r11d
	movq	-112(%rbp), %r14
	movq	-128(%rbp), %r13
	movq	-136(%rbp), %r12
	jne	.L1746
.L1747:
	movq	-64(%rbp), %rsi
	movq	-248(%rbp), %rbx
	leaq	1(%r14), %rax
	leaq	1(%rsi), %rdx
	cmpq	%rbx, %rax
	jbe	.L1876
	movq	-304(%rbp), %rcx
	addq	%rbx, %rcx
	movq	-88(%rbp), %rbx
	cmpq	%rax, %rcx
	jnb	.L1812
	leaq	-4(%rbx), %r10
	leaq	17(%r14), %rcx
	cmpq	%rcx, %r10
	cmova	%rcx, %r10
	cmpq	%r10, %rax
	jnb	.L1876
	leaq	4(%rsi), %rbx
	subq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	%r12, %rdx
	movq	%rax, %r14
	movl	%r11d, %esi
	movzwl	%ax, %r9d
	andq	%rax, %rdx
	addl	$1, %r11d
	imull	$506832829, 0(%r13,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	leaq	(%r15,%r8,4), %rdi
	movl	40(%rdi), %edx
	movb	%cl, 196648(%r15,%r9)
	movzwl	%si, %ecx
	leaq	(%r15,%rcx,4), %rcx
	subq	%rdx, %r14
	cmpq	$65535, %r14
	movq	%r14, %rdx
	movl	$65535, %r14d
	cmova	%r14, %rdx
	movw	%dx, 262184(%rcx)
	leaq	(%r15,%r8,2), %rdx
	movzwl	131112(%rdx), %r8d
	movw	%r8w, 262186(%rcx)
	movl	%eax, 40(%rdi)
	movw	%si, 131112(%rdx)
	leaq	(%rbx,%rax), %rdx
	addq	$4, %rax
	cmpq	%r10, %rax
	jb	.L1813
.L2062:
	movw	%r11w, 524328(%r15)
	movq	%rax, %r14
	movq	%rdx, -64(%rbp)
.L1809:
	leaq	4(%r14), %rax
	cmpq	-88(%rbp), %rax
	jb	.L1815
	movq	-208(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r14, %rsi
	sarq	$4, %rdx
.L1724:
	movq	24(%rbp), %rbx
	movq	-88(%rbp), %rax
	addq	-64(%rbp), %rax
	subq	%rsi, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1876:
	.cfi_restore_state
	movq	%rdx, -64(%rbp)
	movq	%rax, %r14
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1812:
	leaq	-3(%rbx), %r10
	leaq	9(%r14), %rcx
	cmpq	%rcx, %r10
	cmova	%rcx, %r10
	cmpq	%r10, %rax
	jnb	.L1876
	movq	%rsi, %rbx
	addq	$2, %rbx
	subq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	%r12, %rdx
	movq	%rax, %r14
	movl	%r11d, %esi
	movzwl	%ax, %r9d
	andq	%rax, %rdx
	addl	$1, %r11d
	imull	$506832829, 0(%r13,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	leaq	(%r15,%r8,4), %rdi
	movl	40(%rdi), %edx
	movb	%cl, 196648(%r15,%r9)
	movzwl	%si, %ecx
	leaq	(%r15,%rcx,4), %rcx
	subq	%rdx, %r14
	cmpq	$65535, %r14
	movq	%r14, %rdx
	movl	$65535, %r14d
	cmova	%r14, %rdx
	movw	%dx, 262184(%rcx)
	leaq	(%r15,%r8,2), %rdx
	movzwl	131112(%rdx), %r8d
	movw	%r8w, 262186(%rcx)
	movl	%eax, 40(%rdi)
	movw	%si, 131112(%rdx)
	leaq	(%rbx,%rax), %rdx
	addq	$2, %rax
	cmpq	%r10, %rax
	jb	.L1814
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	16(%rbp), %rdi
	movslq	(%rdi), %rdx
	movslq	4(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rax, %rdx
	je	.L1787
	cmpq	%rax, %rcx
	je	.L2083
	addq	$3, %rax
	movq	%rax, %r10
	subq	%rdx, %r10
	cmpq	$6, %r10
	jbe	.L2084
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L2085
	movq	16(%rbp), %rax
	movq	-112(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	je	.L1869
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2086
	leaq	15(%rcx), %rdx
.L1791:
	testq	%rdx, %rdx
	je	.L1787
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L1789:
	movq	16(%rbp), %rcx
	movl	%edi, 8(%rcx)
	movl	-112(%rbp), %edi
	movl	%eax, 12(%rcx)
	movl	%esi, 4(%rcx)
	movl	%edi, (%rcx)
	jmp	.L1817
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	-96(%rbp), %rax
	movq	%r8, -56(%rbp)
	movq	%rdi, -168(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	24(%r15), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r15)
	jb	.L1776
	movq	-48(%rbp), %rdi
	movq	%rbx, -216(%rbp)
	leaq	2(%rax), %rsi
	movw	%r10w, -224(%rbp)
	imull	$506832829, (%rdi), %edx
	movq	-160(%rbp), %rdi
	movq	%r13, -240(%rbp)
	movq	%r11, -232(%rbp)
	movq	104(%rdi), %rcx
	movq	%r12, -248(%rbp)
	movq	%rdi, %r12
	shrl	$18, %edx
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %r9
	leaq	1(%r11), %rcx
	movq	%rsi, %r11
	movq	%rcx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1782:
	movzwl	(%r9,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r15)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L1777
	movl	%esi, %ebx
	movq	%rsi, %rdi
	andl	$31, %ebx
	andl	$31, %edi
	movw	%bx, -136(%rbp)
	cmpq	%rdi, -104(%rbp)
	jb	.L1777
	movq	80(%r12), %r10
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r13
	imulq	%rsi, %rdx
	movl	32(%r10,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r10), %r8
	shrq	$3, %r13
	je	.L1778
	movq	-48(%rbp), %rdx
	movq	(%r8), %rbx
	xorl	%r14d, %r14d
	movq	(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L2087
.L1779:
	xorq	%rbx, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r14
.L1781:
	movl	88(%r12), %edx
	addq	%r14, %rdx
	cmpq	%rdx, %rdi
	jnb	.L1777
	testq	%r14, %r14
	je	.L1777
	movq	%rdi, %rdx
	movq	96(%r12), %rbx
	subq	%r14, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rcx
	movq	-152(%rbp), %rbx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r10,%rdi), %ecx
	salq	%cl, %rdx
	leaq	(%rbx,%rsi), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, -192(%rbp)
	jb	.L1777
	leaq	(%r14,%r14,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%ecx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-80(%rbp), %rdx
	jb	.L1777
	movzwl	-136(%rbp), %esi
	addq	$1, 32(%r15)
	movq	%rdx, -80(%rbp)
	subl	%r14d, %esi
	movq	%rcx, -112(%rbp)
	movl	%esi, -128(%rbp)
	movq	%r14, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L1777:
	cmpq	%r11, %rax
	jne	.L1782
	movq	-56(%rbp), %rax
	movq	-216(%rbp), %rbx
	movzwl	-224(%rbp), %r10d
	movq	-232(%rbp), %r11
	movq	-240(%rbp), %r13
	movq	-248(%rbp), %r12
	addq	$175, %rax
	cmpq	-80(%rbp), %rax
	jbe	.L2088
.L1783:
	movq	-144(%rbp), %r14
	movq	-176(%rbp), %rax
	cmpq	%rax, %r14
	movq	%r14, %rbx
	cmovbe	%r14, %rax
	movq	%rax, %rsi
	movl	-116(%rbp), %eax
	movl	%eax, -128(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L2064:
	subq	$1, -96(%rbp)
	movq	-136(%rbp), %rdx
	leaq	(%rdx,%r8), %r11
	leaq	8(%r8), %rdx
	je	.L2089
	movq	%rdx, %r8
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1739:
	cmpq	$0, -280(%rbp)
	je	.L1738
	movq	-72(%rbp), %r11
	movq	-48(%rbp), %rbx
	movzbl	(%r11), %r11d
	cmpb	%r11b, (%rbx)
	jne	.L1738
	movq	-296(%rbp), %rbx
	cmpq	$0, -216(%rbp)
	movq	%rbx, -312(%rbp)
	je	.L1738
	movq	-48(%rbp), %r8
	movq	-72(%rbp), %r11
	movq	$1, -96(%rbp)
	movzbl	1(%r8), %r8d
	cmpb	%r8b, 1(%r11)
	jne	.L1738
.L1830:
	movq	-96(%rbp), %rbx
	cmpq	$0, -224(%rbp)
	leaq	1(%rbx), %r11
	movq	%r11, -96(%rbp)
	je	.L1743
	movq	-72(%rbp), %r8
	movq	-312(%rbp), %r11
	movzbl	1(%r8,%rbx), %r8d
	cmpb	%r8b, 1(%r11)
	jne	.L1743
	leaq	2(%rbx), %r8
	cmpq	$0, -232(%rbp)
	movq	%r8, -96(%rbp)
	je	.L1743
	movq	-72(%rbp), %r8
	movzbl	2(%r8,%rbx), %r8d
	cmpb	%r8b, 2(%r11)
	jne	.L1743
	leaq	3(%rbx), %r8
	cmpq	$0, -240(%rbp)
	movq	%r8, -96(%rbp)
	je	.L1743
	movq	-72(%rbp), %r8
	movzbl	3(%r8,%rbx), %r8d
	cmpb	%r8b, 3(%r11)
	jne	.L1743
	leaq	4(%rbx), %r8
	cmpq	$0, -256(%rbp)
	movq	%r8, -96(%rbp)
	je	.L1743
	movq	-72(%rbp), %r8
	movzbl	4(%r8,%rbx), %r8d
	cmpb	%r8b, 4(%r11)
	jne	.L1743
	leaq	5(%rbx), %r8
	cmpq	$0, -264(%rbp)
	movq	%r8, -96(%rbp)
	je	.L1743
	movq	-72(%rbp), %r8
	movzbl	5(%r8,%rbx), %r8d
	cmpb	%r8b, 5(%r11)
	jne	.L1743
	leaq	6(%rbx), %r8
	cmpq	$0, -272(%rbp)
	movq	%r8, -96(%rbp)
	je	.L1743
	movq	-72(%rbp), %r8
	movzbl	6(%r11), %r11d
	cmpb	%r11b, 6(%r8,%rbx)
	jne	.L1743
	addq	$7, %rbx
	movq	%rbx, -96(%rbp)
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L2068:
	movq	-288(%rbp), %rbx
	leaq	8(%r11), %r8
	addq	%r11, %rbx
	subq	$1, -96(%rbp)
	je	.L2090
	movq	%r8, %r11
	jmp	.L1741
.L2078:
	movq	-64(%rbp), %rdi
	movl	%edi, %edx
	sall	$3, %edi
	andl	$56, %edi
.L1797:
	cmpq	$9, %rax
	jbe	.L2091
.L1800:
	cmpq	$133, %rax
	jbe	.L2092
	cmpq	$2117, %rax
	ja	.L1803
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L1801:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L1804
.L2081:
	testb	%sil, %sil
	je	.L1804
	cmpw	$15, %ax
	ja	.L1804
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L1807:
	movq	-208(%rbp), %rax
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	movq	-64(%rbp), %rdi
	addq	%rdi, (%rax)
	movq	-96(%rbp), %rax
	leaq	2(%rbx), %rdi
	movq	%rdi, -48(%rbp)
	leaq	(%rax,%rbx), %r14
	movq	-328(%rbp), %rbx
	cmpq	%rbx, %r14
	movq	%rbx, %r10
	movq	-112(%rbp), %rbx
	cmovbe	%r14, %r10
	shrq	$2, %rax
	cmpq	%rbx, %rax
	jbe	.L1808
	movq	%rbx, %rax
	movq	%r14, %rbx
	salq	$2, %rax
	subq	%rax, %rbx
	cmpq	%rdi, %rbx
	movq	%rbx, %rax
	cmovb	%rdi, %rax
	cmpq	%r10, %rax
	cmova	%r10, %rax
	movq	%rax, -48(%rbp)
.L1808:
	movq	-48(%rbp), %rdi
	movq	%rdi, %rax
	cmpq	%r10, %rdi
	jnb	.L1873
	movzwl	524328(%r15), %ebx
	movl	$65535, %r11d
	movw	%bx, -64(%rbp)
	subl	%edi, %ebx
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	%r12, %rdx
	movq	%rax, %r9
	leal	(%rbx,%rax), %esi
	andq	%rax, %rdx
	imull	$506832829, 0(%r13,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	leaq	(%r15,%r8,4), %rdi
	movl	40(%rdi), %edx
	subq	%rdx, %r9
	movq	%r9, %rdx
	movzwl	%ax, %r9d
	cmpq	$65535, %rdx
	movb	%cl, 196648(%r15,%r9)
	movzwl	%si, %ecx
	cmova	%r11, %rdx
	leaq	(%r15,%rcx,4), %rcx
	movw	%dx, 262184(%rcx)
	leaq	(%r15,%r8,2), %rdx
	movzwl	131112(%rdx), %r8d
	movw	%r8w, 262186(%rcx)
	movl	%eax, 40(%rdi)
	addq	$1, %rax
	movw	%si, 131112(%rdx)
	cmpq	%rax, %r10
	jne	.L1810
	movq	-56(%rbp), %rax
	addw	-64(%rbp), %r10w
	movq	$0, -64(%rbp)
	subw	-48(%rbp), %r10w
	movw	%r10w, 524328(%r15)
	movq	%rax, -208(%rbp)
	jmp	.L1809
.L1787:
	movq	-208(%rbp), %rdi
	movl	$1, %esi
	xorl	%edx, %edx
	leaq	16(%rdi), %rax
	movq	%rax, -56(%rbp)
	movl	-64(%rbp), %eax
	movl	%eax, (%rdi)
	movl	-128(%rbp), %eax
	sall	$25, %eax
	orl	-96(%rbp), %eax
	movl	%eax, 4(%rdi)
	xorl	%eax, %eax
	jmp	.L1795
.L2075:
	movl	-128(%rbp), %eax
	subq	$1, -104(%rbp)
	movw	%r10w, -256(%rbp)
	movl	%eax, -116(%rbp)
	movq	-80(%rbp), %rax
	movq	%rbx, -144(%rbp)
	movq	%rax, -56(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1785
.L2084:
	leal	0(,%r10,4), %ecx
	movl	$158663784, %eax
	sarl	%cl, %eax
	movl	%eax, %edx
	andl	$15, %edx
	jmp	.L1791
.L2079:
	movq	-64(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L1797
.L2092:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L1801
.L1749:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1748
	movq	-48(%rbp), %rcx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L1748
.L1825:
	leaq	1(%r13), %r14
	cmpq	$1, %rdx
	je	.L1752
	movq	-48(%rbp), %rcx
	movzbl	1(%rcx,%r13), %r11d
	cmpb	%r11b, 1(%r8)
	jne	.L1752
	leaq	2(%r13), %r14
	cmpq	$2, %rdx
	je	.L1752
	movzbl	2(%rcx,%r13), %r11d
	cmpb	%r11b, 2(%r8)
	jne	.L1752
	leaq	3(%r13), %r14
	cmpq	$3, %rdx
	je	.L1752
	movzbl	3(%rcx,%r13), %r11d
	cmpb	%r11b, 3(%r8)
	jne	.L1752
	leaq	4(%r13), %r14
	cmpq	$4, %rdx
	je	.L1752
	movzbl	4(%rcx,%r13), %r11d
	cmpb	%r11b, 4(%r8)
	jne	.L1752
	leaq	5(%r13), %r14
	subq	$5, %rdx
	je	.L1752
	movzbl	5(%rcx,%r13), %r11d
	cmpb	%r11b, 5(%r8)
	jne	.L1752
	leaq	6(%r13), %r14
	cmpq	$1, %rdx
	je	.L1752
	movzbl	6(%rcx,%r13), %ecx
	cmpb	%cl, 6(%r8)
	jne	.L1752
	leaq	7(%r13), %r14
	jmp	.L1752
.L2082:
	leaq	8(%r8), %r11
	subq	$1, %r13
	je	.L1845
	movq	-48(%rbp), %rdx
	movq	8(%r8), %r11
	movl	$8, %r14d
	movq	8(%rdx), %rdx
	cmpq	%r11, %rdx
	jne	.L1750
	leaq	16(%r8), %r11
	cmpq	$1, %r13
	je	.L1847
	movq	-48(%rbp), %rdx
	movq	16(%r8), %r11
	movl	$16, %r14d
	movq	16(%rdx), %rdx
	cmpq	%r11, %rdx
	jne	.L1750
	leaq	24(%r8), %r11
	movl	$24, %r13d
.L1751:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1850
	movq	-48(%rbp), %rcx
	movzbl	(%rcx,%r13), %ecx
	cmpb	%cl, (%r11)
	jne	.L1850
	movq	%r11, %r8
	jmp	.L1825
.L2065:
	movq	-96(%rbp), %rax
	movq	%rdi, -168(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L1726
.L1855:
	movq	-136(%rbp), %rdi
	movq	%rsi, -112(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rdi, -96(%rbp)
	jmp	.L1763
.L1757:
	cmpq	$0, -288(%rbp)
	je	.L1756
	movq	-48(%rbp), %rcx
	movzbl	(%r9), %edx
	cmpb	%dl, (%rcx)
	jne	.L1756
	movq	-240(%rbp), %rcx
	cmpq	$0, -296(%rbp)
	movq	%rcx, -248(%rbp)
	je	.L1756
	movq	-48(%rbp), %rcx
	movl	$1, %r14d
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%r9)
	jne	.L1756
.L1829:
	leaq	1(%r14), %rcx
	cmpq	$0, -312(%rbp)
	movq	%rcx, -136(%rbp)
	je	.L1761
	movq	-248(%rbp), %rcx
	movzbl	1(%r9,%r14), %edx
	cmpb	%dl, 1(%rcx)
	jne	.L1761
	leaq	2(%r14), %rdx
	cmpq	$0, -320(%rbp)
	movq	%rdx, -136(%rbp)
	je	.L1761
	movzbl	2(%r9,%r14), %edx
	cmpb	%dl, 2(%rcx)
	jne	.L1761
	leaq	3(%r14), %rdx
	cmpq	$0, -344(%rbp)
	movq	%rdx, -136(%rbp)
	je	.L1761
	movzbl	3(%r9,%r14), %edx
	cmpb	%dl, 3(%rcx)
	jne	.L1761
	leaq	4(%r14), %rdx
	cmpq	$0, -352(%rbp)
	movq	%rdx, -136(%rbp)
	je	.L1761
	movzbl	4(%r9,%r14), %edx
	cmpb	%dl, 4(%rcx)
	jne	.L1761
	leaq	5(%r14), %rdx
	cmpq	$0, -368(%rbp)
	movq	%rdx, -136(%rbp)
	je	.L1761
	movzbl	5(%r9,%r14), %edx
	cmpb	%dl, 5(%rcx)
	jne	.L1761
	leaq	6(%r14), %rdx
	cmpq	$0, -360(%rbp)
	movq	%rdx, -136(%rbp)
	je	.L1761
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r9,%r14)
	jne	.L1761
	leaq	7(%r14), %rcx
	movq	%rcx, -136(%rbp)
	jmp	.L1761
.L2070:
	subq	$1, -136(%rbp)
	movq	-232(%rbp), %rdx
	leaq	(%rdx,%rcx), %r14
	leaq	8(%rcx), %rdx
	je	.L2093
	movq	%rdx, %rcx
	jmp	.L1759
.L1803:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L1806
.L2080:
	movq	-64(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L1797
.L1769:
	cmpq	$0, -288(%rbp)
	je	.L1768
	movq	-136(%rbp), %r9
	movq	-48(%rbp), %r10
	movzbl	(%r9), %r9d
	cmpb	%r9b, (%r10)
	jne	.L1768
	movq	-376(%rbp), %r10
	cmpq	$0, -296(%rbp)
	movq	%r10, -400(%rbp)
	je	.L1768
	movq	-48(%rbp), %r8
	movq	-136(%rbp), %r9
	movq	$1, -392(%rbp)
	movzbl	1(%r8), %r8d
	cmpb	%r8b, 1(%r9)
	jne	.L1768
.L1828:
	movq	-392(%rbp), %r10
	cmpq	$0, -312(%rbp)
	leaq	1(%r10), %r9
	movq	%r9, -152(%rbp)
	je	.L1773
	movq	-136(%rbp), %r8
	movq	-400(%rbp), %r9
	movzbl	1(%r8,%r10), %r8d
	cmpb	%r8b, 1(%r9)
	jne	.L1773
	leaq	2(%r10), %r8
	cmpq	$0, -320(%rbp)
	movq	%r8, -152(%rbp)
	je	.L1773
	movq	-136(%rbp), %r8
	movq	%r8, -264(%rbp)
	movzbl	2(%r8,%r10), %r8d
	cmpb	%r8b, 2(%r9)
	jne	.L1773
	leaq	3(%r10), %r8
	cmpq	$0, -344(%rbp)
	movq	%r8, -152(%rbp)
	je	.L1773
	movq	-264(%rbp), %r8
	movzbl	3(%r8,%r10), %r8d
	cmpb	%r8b, 3(%r9)
	jne	.L1773
	leaq	4(%r10), %r8
	cmpq	$0, -352(%rbp)
	movq	%r8, -152(%rbp)
	je	.L1773
	movq	-136(%rbp), %r8
	movzbl	4(%r8,%r10), %r10d
	cmpb	%r10b, 4(%r9)
	jne	.L1773
	movq	-392(%rbp), %r10
	cmpq	$0, -368(%rbp)
	leaq	5(%r10), %r8
	movq	%r8, -152(%rbp)
	je	.L1773
	movq	-136(%rbp), %r8
	movzbl	5(%r8,%r10), %r10d
	cmpb	%r10b, 5(%r9)
	jne	.L1773
	movq	-392(%rbp), %r10
	addq	$6, %r10
	cmpq	$0, -360(%rbp)
	movq	%r10, -152(%rbp)
	je	.L1773
	movzbl	6(%r9), %r10d
	movq	-392(%rbp), %r9
	cmpb	%r10b, 6(%r8,%r9)
	jne	.L1773
	movq	%r9, %r10
	addq	$7, %r10
	movq	%r10, -152(%rbp)
	jmp	.L1773
.L2073:
	movq	-384(%rbp), %r10
	leaq	8(%r9), %r8
	addq	%r9, %r10
	subq	$1, -152(%rbp)
	je	.L2094
	movq	%r8, %r9
	jmp	.L1771
.L1873:
	movq	-56(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L1809
.L2071:
	movq	-136(%rbp), %rdi
	movq	%rsi, -112(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rdi, -96(%rbp)
	jmp	.L1756
.L1871:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L1797
.L2083:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L1789
.L1778:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1777
	movq	-48(%rbp), %rbx
	movzbl	(%rbx), %ebx
	cmpb	%bl, (%r8)
	jne	.L1777
.L1822:
	leaq	1(%r13), %r14
	cmpq	$1, %rdx
	je	.L1781
	movq	-48(%rbp), %rbx
	movzbl	1(%rbx,%r13), %ecx
	cmpb	%cl, 1(%r8)
	jne	.L1781
	leaq	2(%r13), %r14
	cmpq	$2, %rdx
	je	.L1781
	movzbl	2(%rbx,%r13), %ecx
	cmpb	%cl, 2(%r8)
	jne	.L1781
	leaq	3(%r13), %r14
	cmpq	$3, %rdx
	je	.L1781
	movzbl	3(%rbx,%r13), %ecx
	cmpb	%cl, 3(%r8)
	jne	.L1781
	leaq	4(%r13), %r14
	cmpq	$4, %rdx
	je	.L1781
	movzbl	4(%rbx,%r13), %ecx
	cmpb	%cl, 4(%r8)
	jne	.L1781
	leaq	5(%r13), %r14
	subq	$5, %rdx
	je	.L1781
	movzbl	5(%rbx,%r13), %ecx
	cmpb	%cl, 5(%r8)
	jne	.L1781
	leaq	6(%r13), %r14
	cmpq	$1, %rdx
	je	.L1781
	movzbl	6(%rbx,%r13), %ebx
	cmpb	%bl, 6(%r8)
	jne	.L1781
	leaq	7(%r13), %r14
	jmp	.L1781
.L2085:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L1791
.L2087:
	leaq	8(%r8), %rdx
	subq	$1, %r13
	je	.L1863
	movq	-48(%rbp), %rdx
	movq	8(%r8), %rbx
	movl	$8, %r14d
	movq	8(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1779
	leaq	16(%r8), %rdx
	cmpq	$1, %r13
	je	.L1865
	movq	-48(%rbp), %rdx
	movq	16(%r8), %rbx
	movl	$16, %r14d
	addq	$24, %r8
	movl	$24, %r13d
	movq	16(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1779
.L1780:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L1868
	movq	-48(%rbp), %rbx
	movzbl	(%rbx,%r13), %ebx
	cmpb	%bl, (%r8)
	je	.L1822
.L1868:
	movq	%r13, %r14
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1834:
	xorl	%edx, %edx
	jmp	.L1724
.L2089:
	movq	%rdx, -96(%rbp)
	movq	%r11, %rdx
	testq	%r10, %r10
	je	.L1731
	movq	-96(%rbp), %r11
	movzbl	(%rcx,%r11), %r11d
	cmpb	%r11b, (%rdx)
	jne	.L1731
	leaq	1(%rdx), %r11
	cmpq	$0, -216(%rbp)
	movq	%r11, -152(%rbp)
	leaq	9(%r8), %r11
	je	.L1837
	movzbl	9(%rcx,%r8), %r8d
	cmpb	%r8b, 1(%rdx)
	je	.L1831
.L1837:
	movq	%r11, -96(%rbp)
	jmp	.L1731
.L2090:
	cmpq	$0, -280(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rbx, %r8
	movq	%r11, -152(%rbp)
	je	.L1743
	movq	-72(%rbp), %rbx
	movq	-96(%rbp), %r11
	movzbl	(%rbx,%r11), %ebx
	cmpb	%bl, (%r8)
	jne	.L1743
	leaq	1(%r8), %rbx
	movq	%rbx, -312(%rbp)
	movq	-152(%rbp), %rbx
	addq	$9, %rbx
	cmpq	$0, -216(%rbp)
	movq	%rbx, -96(%rbp)
	je	.L1743
	movq	-72(%rbp), %rbx
	movq	-152(%rbp), %r11
	movzbl	9(%rbx,%r11), %ebx
	cmpb	%bl, 1(%r8)
	je	.L1830
	jmp	.L1743
.L1845:
	movl	$8, %r13d
	jmp	.L1751
.L1869:
	movl	$2, %edx
	jmp	.L1789
.L2093:
	cmpq	$0, -288(%rbp)
	movq	%rdx, -136(%rbp)
	je	.L1761
	movq	%r14, %rdx
	movq	-136(%rbp), %r14
	movzbl	(%r9,%r14), %r14d
	cmpb	%r14b, (%rdx)
	jne	.L1761
	leaq	1(%rdx), %r14
	cmpq	$0, -296(%rbp)
	movq	%r14, -248(%rbp)
	leaq	9(%rcx), %r14
	je	.L1854
	movzbl	9(%r9,%rcx), %ecx
	cmpb	%cl, 1(%rdx)
	je	.L1829
.L1854:
	movq	%r14, -136(%rbp)
	jmp	.L1761
.L2094:
	cmpq	$0, -288(%rbp)
	movq	%r8, -152(%rbp)
	movq	%r10, %r8
	movq	%r9, -264(%rbp)
	je	.L1773
	movq	-136(%rbp), %r10
	movq	-152(%rbp), %r9
	movzbl	(%r10,%r9), %r10d
	cmpb	%r10b, (%r8)
	jne	.L1773
	movq	-264(%rbp), %r9
	leaq	1(%r8), %r10
	cmpq	$0, -296(%rbp)
	movq	%r10, -400(%rbp)
	leaq	9(%r9), %r10
	movq	%r10, -392(%rbp)
	je	.L2061
	movq	-136(%rbp), %r10
	movzbl	9(%r10,%r9), %r10d
	cmpb	%r10b, 1(%r8)
	je	.L1828
	movq	-392(%rbp), %r10
.L2061:
	movq	%r10, -152(%rbp)
	jmp	.L1773
.L2086:
	movl	$3, %edx
	jmp	.L1789
.L1863:
	movq	%rdx, %r8
	movl	$8, %r13d
	jmp	.L1780
.L1850:
	movq	%r13, %r14
	jmp	.L1752
.L1847:
	movl	$16, %r13d
	jmp	.L1751
.L1865:
	movq	%rdx, %r8
	movl	$16, %r13d
	jmp	.L1780
.L2077:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L1795
	.cfi_endproc
.LFE285:
	.size	CreateBackwardReferencesNH40, .-CreateBackwardReferencesNH40
	.p2align 4
	.type	CreateBackwardReferencesNH41, @function
CreateBackwardReferencesNH41:
.LFB286:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rdx, %r11
	movl	$256, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$240, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -168(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	leaq	(%rsi,%rdi), %rcx
	subq	$16, %rax
	cmpq	$3, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rax, -176(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	leaq	-3(%rcx), %rax
	cmovbe	%rsi, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -360(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -304(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
	movq	%rsi, %rax
	addq	%rdi, %rax
	movq	16(%rbp), %rdi
	movq	%rax, -280(%rbp)
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	leal	-1(%rax), %edx
	movl	%edx, 16(%rdi)
	leal	1(%rax), %edx
	movl	%edx, 20(%rdi)
	leal	-2(%rax), %edx
	movl	%edx, 24(%rdi)
	leal	2(%rax), %edx
	movl	%edx, 28(%rdi)
	leal	-3(%rax), %edx
	addl	$3, %eax
	movl	%eax, 36(%rdi)
	leaq	4(%rsi), %rax
	movl	%edx, 32(%rdi)
	cmpq	%rcx, %rax
	jnb	.L2208
	movq	32(%rbp), %rax
	movq	%r15, %rbx
	movq	%rsi, %r13
	movq	%r9, %r12
	movq	%r11, %r15
	movq	%rax, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L2189:
	movq	-176(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rbx, %r9
	movq	%rbx, -72(%rbp)
	movq	$0, -160(%rbp)
	movq	16(%rbp), %rbx
	subq	%r13, %rdi
	movq	%rax, %r8
	cmpq	%r13, %rax
	movq	-168(%rbp), %rax
	cmova	%r13, %r8
	andq	%r13, %r9
	movq	%rdi, -104(%rbp)
	movq	72(%rax), %rax
	leaq	(%r15,%r9), %r14
	movq	%r9, -128(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -208(%rbp)
	imull	$506832829, (%r14), %eax
	movq	$2020, -48(%rbp)
	movq	%r15, -64(%rbp)
	shrl	$17, %eax
	movl	%eax, %esi
	movl	%eax, -120(%rbp)
	movl	%eax, %r11d
	movq	%rdi, %rax
	shrq	$3, %rax
	andl	$7, %edi
	movq	%rsi, -112(%rbp)
	movq	%rax, -56(%rbp)
	movq	%rdi, %rax
	leaq	-2(%rax), %rsi
	movq	%rdi, -184(%rbp)
	leaq	-1(%rdi), %rdi
	movq	%rdi, -192(%rbp)
	leaq	-3(%rax), %rdi
	movq	%rsi, -224(%rbp)
	leaq	-4(%rax), %rsi
	movq	%rdi, -240(%rbp)
	leaq	-5(%rax), %rdi
	movq	%rsi, -248(%rbp)
	leaq	-6(%rax), %rsi
	subq	$7, %rax
	movq	%rdi, -256(%rbp)
	leaq	1(%r14), %rdi
	movq	%rsi, -264(%rbp)
	leaq	8(%r14), %rsi
	movq	%rax, -272(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -144(%rbp)
	movq	%rsi, -136(%rbp)
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2440:
	movzwl	%cx, %edx
	cmpb	196648(%r12,%rdx), %r11b
	jne	.L2100
	cmpq	%r13, %rcx
	jnb	.L2100
	cmpq	%r8, %rdi
	ja	.L2100
.L2195:
	movq	-56(%rbp), %r10
	andq	-72(%rbp), %rcx
	addq	-64(%rbp), %rcx
	testq	%r10, %r10
	je	.L2101
	xorl	%r9d, %r9d
.L2103:
	movq	(%r14,%r9), %rdx
	movq	(%rcx,%r9), %r15
	cmpq	%r15, %rdx
	je	.L2437
	xorq	%r15, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%r9, %rdx
.L2105:
	cmpq	$1, %rdx
	jbe	.L2100
	leaq	(%rdx,%rdx,8), %rcx
	movq	-48(%rbp), %r15
	movq	%rcx, %r9
	salq	$4, %r9
	subq	%rcx, %r9
	addq	$1935, %r9
	cmpq	%r15, %r9
	jbe	.L2100
	testq	%rax, %rax
	je	.L2212
	leal	-1(%rsi), %ecx
	movl	$117264, %eax
	andl	$14, %ecx
	sarl	%cl, %eax
	movq	$-39, %rcx
	andl	$14, %eax
	subq	%rax, %rcx
	addq	%rcx, %r9
	cmpq	%r15, %r9
	ja	.L2438
	.p2align 4,,10
	.p2align 3
.L2100:
	cmpq	$10, %rsi
	je	.L2439
.L2107:
	movq	%rsi, %rax
.L2109:
	movslq	(%rbx,%rax,4), %rdi
	movq	%r13, %rcx
	leaq	1(%rax), %rsi
	subq	%rdi, %rcx
	testq	%rax, %rax
	jne	.L2440
	cmpq	%r13, %rcx
	jnb	.L2107
	cmpq	%r8, %rdi
	jbe	.L2195
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movq	524336(%r12), %rdi
	movq	-128(%rbp), %r9
	movq	-64(%rbp), %r15
	leaq	(%r12,%rcx,4), %rax
	movq	%rdi, -200(%rbp)
	movq	-72(%rbp), %rbx
	movq	%rax, -144(%rbp)
	movl	40(%rax), %eax
	subq	%rax, %rsi
	movq	%rsi, -112(%rbp)
	movq	%rsi, %rax
	leaq	(%r12,%rcx,2), %rsi
	leaq	-1(%rdi), %rcx
	movq	%rsi, -136(%rbp)
	movzwl	131112(%rsi), %esi
	movq	%rcx, -216(%rbp)
	movw	%si, -128(%rbp)
	testq	%rdi, %rdi
	je	.L2110
	cmpq	%r8, %rax
	ja	.L2110
	leaq	1(%r14), %rdi
	movq	%r14, -64(%rbp)
	movq	%rdi, -296(%rbp)
	leaq	8(%r14), %rdi
	movq	%r13, %r14
	movq	%rdi, -288(%rbp)
	movq	-88(%rbp), %rdi
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2112:
	subq	$1, %rcx
	cmpq	$-1, %rcx
	je	.L2424
.L2119:
	addq	%rdx, %rax
	cmpq	%r8, %rax
	ja	.L2424
.L2111:
	leaq	(%r12,%rsi,4), %rdx
	leaq	(%r9,%rdi), %r11
	movzwl	262186(%rdx), %esi
	movzwl	262184(%rdx), %edx
	cmpq	%r11, %rbx
	jb	.L2112
	movq	%r14, %r10
	subq	%rax, %r10
	andq	%rbx, %r10
	leaq	(%r10,%rdi), %r13
	cmpq	%r13, %rbx
	jb	.L2112
	movzbl	(%r15,%r13), %r13d
	cmpb	%r13b, (%r15,%r11)
	jne	.L2112
	leaq	(%r15,%r10), %r11
	movq	%r11, -72(%rbp)
	movq	-56(%rbp), %r11
	testq	%r11, %r11
	je	.L2113
	movq	%r11, -88(%rbp)
	xorl	%r11d, %r11d
.L2115:
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r13
	movq	(%r10,%r11), %r10
	movq	0(%r13,%r11), %r13
	cmpq	%r13, %r10
	je	.L2441
	xorq	%r13, %r10
	movq	%r11, -152(%rbp)
	rep bsfq	%r10, %r10
	movslq	%r10d, %r10
	shrq	$3, %r10
	addq	%r10, %r11
	movq	%r11, -88(%rbp)
.L2117:
	movq	-88(%rbp), %r10
	cmpq	$3, %r10
	jbe	.L2112
	leaq	(%r10,%r10,8), %r13
	movq	%r13, %r11
	salq	$4, %r11
	subq	%r13, %r11
	bsrl	%eax, %r13d
	imull	$30, %r13d, %r13d
	addq	$1920, %r11
	subq	%r13, %r11
	cmpq	-48(%rbp), %r11
	jbe	.L2112
	subq	$1, %rcx
	movq	%rax, -160(%rbp)
	movq	%r10, %rdi
	movq	%r11, -48(%rbp)
	cmpq	$-1, %rcx
	jne	.L2119
	.p2align 4,,10
	.p2align 3
.L2424:
	movq	%rdi, -88(%rbp)
	movq	%r14, %r13
	movq	-64(%rbp), %r14
.L2110:
	movq	-112(%rbp), %rsi
	movzbl	-120(%rbp), %edi
	movzwl	%r13w, %edx
	movl	$65535, %ecx
	movzwl	524328(%r12), %eax
	cmpq	$65535, %rsi
	leal	1(%rax), %r10d
	cmovbe	%rsi, %rcx
	movzwl	-128(%rbp), %esi
	cmpq	$2020, -48(%rbp)
	movw	%r10w, 524328(%r12)
	movb	%dil, 196648(%r12,%rdx)
	movzwl	%ax, %edx
	movq	-144(%rbp), %rdi
	leaq	(%r12,%rdx,4), %rdx
	movw	%si, 262186(%rdx)
	movq	-136(%rbp), %rsi
	movw	%cx, 262184(%rdx)
	movl	$0, -136(%rbp)
	movl	%r13d, 40(%rdi)
	movw	%ax, 131112(%rsi)
	je	.L2442
.L2120:
	movq	-104(%rbp), %rax
	movw	%r10w, -264(%rbp)
	movq	%r13, -144(%rbp)
	leaq	-1(%rax), %rsi
	subq	$4, %rax
	movq	%rsi, -120(%rbp)
	movq	-168(%rbp), %rsi
	movq	%rax, -280(%rbp)
	movl	4(%rsi), %esi
	movl	%esi, -272(%rbp)
.L2159:
	xorl	%r8d, %r8d
	cmpl	$4, -272(%rbp)
	jg	.L2128
	movq	-88(%rbp), %rax
	leaq	-1(%rax), %r8
	movq	-120(%rbp), %rax
	cmpq	%rax, %r8
	cmova	%rax, %r8
.L2128:
	movq	-144(%rbp), %rax
	movq	%rbx, -112(%rbp)
	movq	$0, -128(%rbp)
	leaq	1(%rax), %r11
	movq	-176(%rbp), %rax
	movq	$2020, -64(%rbp)
	movq	$0, -104(%rbp)
	cmpq	%rax, %r11
	movq	%rax, %r13
	movq	%r11, %rax
	movq	%r8, -152(%rbp)
	cmovbe	%r11, %r13
	andq	%rbx, %rax
	movq	16(%rbp), %rbx
	leaq	(%r15,%rax), %rdi
	movq	%rax, -256(%rbp)
	imull	$506832829, (%rdi), %eax
	movq	%rdi, -56(%rbp)
	shrl	$17, %eax
	movl	%eax, %esi
	movl	%eax, -184(%rbp)
	movl	%eax, %r10d
	movq	-120(%rbp), %rax
	movq	%rsi, -192(%rbp)
	movq	%rax, %rsi
	andl	$7, %eax
	shrq	$3, %rsi
	movq	%rax, -288(%rbp)
	movq	%rsi, -72(%rbp)
	leaq	-1(%rax), %rsi
	movq	%rsi, -296(%rbp)
	leaq	-2(%rax), %rsi
	movq	%rsi, -312(%rbp)
	leaq	-3(%rax), %rsi
	movq	%rsi, -320(%rbp)
	leaq	-4(%rax), %rsi
	movq	%rsi, -328(%rbp)
	leaq	-5(%rax), %rsi
	movq	%rsi, -344(%rbp)
	leaq	-6(%rax), %rsi
	subq	$7, %rax
	movq	%rsi, -336(%rbp)
	leaq	1(%rdi), %rsi
	movq	%rsi, -240(%rbp)
	leaq	8(%rdi), %rsi
	movq	%rax, -352(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -224(%rbp)
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2446:
	movzwl	%cx, %edx
	cmpb	196648(%r12,%rdx), %r10b
	jne	.L2130
	cmpq	%rcx, %r11
	jbe	.L2130
	cmpq	%r13, %rdi
	ja	.L2130
	movq	-72(%rbp), %r9
	andq	-112(%rbp), %rcx
	addq	%r15, %rcx
	testq	%r9, %r9
	je	.L2131
.L2447:
	xorl	%r8d, %r8d
.L2133:
	movq	-56(%rbp), %rdx
	movq	(%rcx,%r8), %r14
	movq	(%rdx,%r8), %rdx
	cmpq	%r14, %rdx
	je	.L2443
	xorq	%r14, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%r8, %rdx
.L2135:
	cmpq	$1, %rdx
	jbe	.L2130
	leaq	(%rdx,%rdx,8), %rcx
	movq	-64(%rbp), %r14
	movq	%rcx, %r8
	salq	$4, %r8
	subq	%rcx, %r8
	addq	$1935, %r8
	cmpq	%r14, %r8
	jbe	.L2130
	testq	%rax, %rax
	je	.L2229
	leal	-1(%rsi), %ecx
	movl	$117264, %eax
	andl	$14, %ecx
	sarl	%cl, %eax
	movq	$-39, %rcx
	andl	$14, %eax
	subq	%rax, %rcx
	addq	%rcx, %r8
	cmpq	%r14, %r8
	ja	.L2444
	.p2align 4,,10
	.p2align 3
.L2130:
	cmpq	$10, %rsi
	je	.L2445
.L2137:
	movq	%rsi, %rax
.L2139:
	movslq	(%rbx,%rax,4), %rdi
	movq	%r11, %rcx
	leaq	1(%rax), %rsi
	subq	%rdi, %rcx
	testq	%rax, %rax
	jne	.L2446
	cmpq	%r13, %rdi
	ja	.L2137
	cmpq	%rcx, %r11
	jbe	.L2137
	movq	-72(%rbp), %r9
	andq	-112(%rbp), %rcx
	addq	%r15, %rcx
	testq	%r9, %r9
	jne	.L2447
.L2131:
	cmpq	$0, -288(%rbp)
	je	.L2130
	movq	-56(%rbp), %rdx
	movzbl	(%rcx), %r14d
	cmpb	%r14b, (%rdx)
	jne	.L2130
	cmpq	$0, -296(%rbp)
	movq	-240(%rbp), %r14
	je	.L2130
	movzbl	1(%rdx), %edx
	movl	$1, %r9d
	cmpb	%dl, 1(%rcx)
	jne	.L2130
.L2203:
	cmpq	$0, -312(%rbp)
	leaq	1(%r9), %rdx
	je	.L2135
	movzbl	1(%rcx,%r9), %r8d
	cmpb	%r8b, 1(%r14)
	jne	.L2135
	cmpq	$0, -320(%rbp)
	leaq	2(%r9), %rdx
	je	.L2135
	movzbl	2(%rcx,%r9), %r8d
	cmpb	%r8b, 2(%r14)
	jne	.L2135
	cmpq	$0, -328(%rbp)
	leaq	3(%r9), %rdx
	je	.L2135
	movzbl	3(%rcx,%r9), %r8d
	cmpb	%r8b, 3(%r14)
	jne	.L2135
	cmpq	$0, -344(%rbp)
	leaq	4(%r9), %rdx
	je	.L2135
	movzbl	4(%rcx,%r9), %r8d
	cmpb	%r8b, 4(%r14)
	jne	.L2135
	cmpq	$0, -336(%rbp)
	leaq	5(%r9), %rdx
	je	.L2135
	movzbl	5(%rcx,%r9), %r8d
	cmpb	%r8b, 5(%r14)
	jne	.L2135
	cmpq	$0, -352(%rbp)
	leaq	6(%r9), %rdx
	je	.L2135
	movzbl	6(%r14), %r14d
	cmpb	%r14b, 6(%rcx,%r9)
	jne	.L2135
	leaq	7(%r9), %rdx
	jmp	.L2135
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	-192(%rbp), %rdi
	movq	%r11, %rsi
	movq	-152(%rbp), %r8
	movq	-112(%rbp), %rbx
	leaq	(%r12,%rdi,4), %rax
	movq	%rax, -248(%rbp)
	movl	40(%rax), %eax
	subq	%rax, %rsi
	cmpq	$0, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rsi, %rax
	leaq	(%r12,%rdi,2), %rsi
	movq	%rsi, -240(%rbp)
	movzwl	131112(%rsi), %esi
	movw	%si, -224(%rbp)
	je	.L2140
	cmpq	%r13, %rax
	ja	.L2140
	movq	-56(%rbp), %rdi
	movq	-216(%rbp), %rdx
	movq	-256(%rbp), %r14
	leaq	1(%rdi), %rcx
	addq	$8, %rdi
	movq	%rcx, -384(%rbp)
	movq	%rdi, -376(%rbp)
	jmp	.L2141
	.p2align 4,,10
	.p2align 3
.L2142:
	subq	$1, %rdx
	cmpq	$-1, %rdx
	je	.L2140
.L2149:
	addq	%rcx, %rax
	cmpq	%r13, %rax
	ja	.L2140
.L2141:
	leaq	(%r12,%rsi,4), %rcx
	leaq	(%r14,%r8), %r9
	movzwl	262186(%rcx), %esi
	movzwl	262184(%rcx), %ecx
	cmpq	%r9, %rbx
	jb	.L2142
	movq	%r11, %rdi
	subq	%rax, %rdi
	andq	%rbx, %rdi
	leaq	(%rdi,%r8), %r10
	cmpq	%r10, %rbx
	jb	.L2142
	movzbl	(%r15,%r10), %r10d
	cmpb	%r10b, (%r15,%r9)
	jne	.L2142
	addq	%r15, %rdi
	movq	%rdi, -112(%rbp)
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2143
	movq	%rdi, -152(%rbp)
	xorl	%r9d, %r9d
.L2145:
	movq	-56(%rbp), %rdi
	movq	-112(%rbp), %r10
	movq	(%rdi,%r9), %rdi
	movq	(%r10,%r9), %r10
	cmpq	%r10, %rdi
	je	.L2448
	xorq	%r10, %rdi
	movq	%r9, -256(%rbp)
	rep bsfq	%rdi, %rdi
	movslq	%edi, %rdi
	shrq	$3, %rdi
	addq	%r9, %rdi
	movq	%rdi, -152(%rbp)
.L2147:
	movq	-152(%rbp), %rdi
	cmpq	$3, %rdi
	jbe	.L2142
	leaq	(%rdi,%rdi,8), %r10
	movq	%r10, %r9
	salq	$4, %r9
	subq	%r10, %r9
	bsrl	%eax, %r10d
	imull	$30, %r10d, %r10d
	addq	$1920, %r9
	subq	%r10, %r9
	cmpq	-64(%rbp), %r9
	jbe	.L2142
	subq	$1, %rdx
	movq	%rax, -128(%rbp)
	movq	%rdi, %r8
	movq	%rdi, -104(%rbp)
	movq	%r9, -64(%rbp)
	cmpq	$-1, %rdx
	jne	.L2149
	.p2align 4,,10
	.p2align 3
.L2140:
	movzwl	-264(%rbp), %esi
	movzbl	-184(%rbp), %edi
	movzwl	%r11w, %eax
	movl	$65535, %edx
	movl	$0, -72(%rbp)
	leal	1(%rsi), %r14d
	movw	%r14w, 524328(%r12)
	movb	%dil, 196648(%r12,%rax)
	movq	-192(%rbp), %rdi
	movzwl	%si, %eax
	leaq	(%r12,%rax,4), %rax
	cmpq	$65535, %rdi
	cmovbe	%rdi, %rdx
	movzwl	-224(%rbp), %edi
	cmpq	$2020, -64(%rbp)
	movw	%dx, 262184(%rax)
	movw	%di, 262186(%rax)
	movq	-248(%rbp), %rax
	movl	%r11d, 40(%rax)
	movq	-240(%rbp), %rax
	movw	%si, 131112(%rax)
	je	.L2449
.L2150:
	movq	-48(%rbp), %rax
	addq	$175, %rax
	cmpq	-64(%rbp), %rax
	ja	.L2157
.L2464:
	addq	$1, -80(%rbp)
	movq	-280(%rbp), %rsi
	cmpq	%rsi, -120(%rbp)
	je	.L2427
	movq	-144(%rbp), %rax
	addq	$5, %rax
	cmpq	%rax, -96(%rbp)
	ja	.L2450
.L2427:
	movq	%r13, %rdi
.L2158:
	movq	-368(%rbp), %rax
	movq	-104(%rbp), %rsi
	addq	%r11, %rax
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -280(%rbp)
	movq	-128(%rbp), %rax
	cmpq	%rdi, %rax
	jbe	.L2451
	leaq	15(%rax), %rdx
.L2191:
	movq	-232(%rbp), %rsi
	leaq	16(%rsi), %rax
	movq	%rax, -56(%rbp)
	movl	-80(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-72(%rbp), %eax
	sall	$25, %eax
	orl	-104(%rbp), %eax
	movl	%eax, 4(%rsi)
	movq	-168(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r13
	cmpq	%rcx, %rdx
	jb	.L2452
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r13,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L2169:
	movq	-232(%rbp), %rdi
	movw	%ax, 14(%rdi)
	movl	-72(%rbp), %eax
	addl	-104(%rbp), %eax
	cmpq	$5, -80(%rbp)
	movl	%edx, 8(%rdi)
	cltq
	jbe	.L2453
	cmpq	$129, -80(%rbp)
	jbe	.L2454
	cmpq	$2113, -80(%rbp)
	jbe	.L2455
	movq	-80(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L2245
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L2174
.L2467:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L2456
	.p2align 4,,10
	.p2align 3
.L2178:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L2180:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	24(%r12), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r12)
	jb	.L2121
	imull	$506832829, (%r14), %edx
	movq	-168(%rbp), %rsi
	movq	%r15, -112(%rbp)
	leaq	2(%rax), %rdi
	movq	%rbx, -120(%rbp)
	movq	104(%rsi), %rcx
	movw	%r10w, -64(%rbp)
	movq	%rdi, %r10
	shrl	$18, %edx
	movq	%r13, -72(%rbp)
	movq	%rsi, %r13
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %r11
	leaq	1(%r8), %rcx
	movq	%rcx, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L2127:
	movzwl	(%r11,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r12)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L2122
	movl	%esi, %ebx
	movq	%rsi, %rdi
	andl	$31, %ebx
	andl	$31, %edi
	movw	%bx, -56(%rbp)
	cmpq	%rdi, -104(%rbp)
	jb	.L2122
	movq	80(%r13), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %rbx
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %rbx
	movq	%rbx, -128(%rbp)
	je	.L2123
	movq	(%r8), %rbx
	movq	(%r14), %rdx
	xorl	%r15d, %r15d
	cmpq	%rbx, %rdx
	je	.L2457
.L2124:
	xorq	%rbx, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L2126:
	movl	88(%r13), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L2122
	testq	%r15, %r15
	je	.L2122
	movq	%rdi, %rdx
	movq	96(%r13), %rbx
	addq	-144(%rbp), %rsi
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	salq	%cl, %rdx
	addq	%rsi, %rdx
	cmpq	%rdx, -208(%rbp)
	jb	.L2122
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rcx
	salq	$4, %rcx
	subq	%rsi, %rcx
	bsrl	%edx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rcx
	subq	%rsi, %rcx
	cmpq	-48(%rbp), %rcx
	jb	.L2122
	movzwl	-56(%rbp), %esi
	addq	$1, 32(%r12)
	movq	%rcx, -48(%rbp)
	subl	%r15d, %esi
	movq	%rdx, -160(%rbp)
	movl	%esi, -136(%rbp)
	movq	%r15, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L2122:
	cmpq	%r10, %rax
	jne	.L2127
	cmpq	$2020, -48(%rbp)
	movzwl	-64(%rbp), %r10d
	movq	-72(%rbp), %r13
	movq	-112(%rbp), %r15
	movq	-120(%rbp), %rbx
	jne	.L2120
.L2121:
	movq	-80(%rbp), %rdi
	movq	-280(%rbp), %rsi
	leaq	1(%r13), %rax
	leaq	1(%rdi), %rdx
	cmpq	%rsi, %rax
	jbe	.L2250
	movq	-304(%rbp), %rcx
	addq	%rsi, %rcx
	movq	-96(%rbp), %rsi
	cmpq	%rax, %rcx
	jnb	.L2186
	subq	$4, %rsi
	leaq	17(%r13), %rcx
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	cmpq	%rcx, %rax
	jnb	.L2250
	leaq	4(%rdi), %rdx
	movl	$65535, %r11d
	subq	%r13, %rdx
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	%rbx, %rdx
	movq	%rax, %r9
	movl	%r10d, %esi
	addl	$1, %r10d
	andq	%rax, %rdx
	imull	$506832829, (%r15,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	leaq	(%r12,%r8,4), %rdi
	movl	40(%rdi), %edx
	subq	%rdx, %r9
	movq	%r9, %rdx
	movzwl	%ax, %r9d
	cmpq	$65535, %rdx
	movb	%cl, 196648(%r12,%r9)
	movzwl	%si, %ecx
	cmova	%r11, %rdx
	leaq	(%r12,%rcx,4), %rcx
	movw	%dx, 262184(%rcx)
	leaq	(%r12,%r8,2), %rdx
	movzwl	131112(%rdx), %r8d
	movw	%r8w, 262186(%rcx)
	movl	%eax, 40(%rdi)
	movw	%si, 131112(%rdx)
	leaq	0(%r13,%rax), %rdx
	addq	$4, %rax
	cmpq	%r14, %rax
	jb	.L2187
.L2435:
	movw	%r10w, 524328(%r12)
	movq	%rax, %r13
	movq	%rdx, -80(%rbp)
.L2183:
	leaq	4(%r13), %rax
	cmpq	-96(%rbp), %rax
	jb	.L2189
	movq	-232(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r13, %rsi
	sarq	$4, %rdx
.L2098:
	movq	24(%rbp), %rbx
	movq	-96(%rbp), %rax
	addq	-80(%rbp), %rax
	subq	%rsi, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2250:
	.cfi_restore_state
	movq	%rdx, -80(%rbp)
	movq	%rax, %r13
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	%rdx, -88(%rbp)
	movq	%r9, -48(%rbp)
	movq	%rdi, -160(%rbp)
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2101:
	cmpq	$0, -184(%rbp)
	je	.L2100
	movzbl	(%rcx), %edx
	cmpb	%dl, (%r14)
	jne	.L2100
	cmpq	$0, -192(%rbp)
	movq	-144(%rbp), %r15
	je	.L2100
	movzbl	1(%r14), %edx
	movl	$1, %r10d
	cmpb	%dl, 1(%rcx)
	jne	.L2100
.L2205:
	cmpq	$0, -224(%rbp)
	leaq	1(%r10), %rdx
	je	.L2105
	movzbl	1(%rcx,%r10), %r9d
	cmpb	%r9b, 1(%r15)
	jne	.L2105
	cmpq	$0, -240(%rbp)
	leaq	2(%r10), %rdx
	je	.L2105
	movzbl	2(%rcx,%r10), %r9d
	cmpb	%r9b, 2(%r15)
	jne	.L2105
	cmpq	$0, -248(%rbp)
	leaq	3(%r10), %rdx
	je	.L2105
	movzbl	3(%rcx,%r10), %r9d
	cmpb	%r9b, 3(%r15)
	jne	.L2105
	cmpq	$0, -256(%rbp)
	leaq	4(%r10), %rdx
	je	.L2105
	movzbl	4(%rcx,%r10), %r9d
	cmpb	%r9b, 4(%r15)
	jne	.L2105
	cmpq	$0, -264(%rbp)
	leaq	5(%r10), %rdx
	je	.L2105
	movzbl	5(%rcx,%r10), %r9d
	cmpb	%r9b, 5(%r15)
	jne	.L2105
	cmpq	$0, -272(%rbp)
	leaq	6(%r10), %rdx
	je	.L2105
	movzbl	6(%r15), %r15d
	cmpb	%r15b, 6(%rcx,%r10)
	jne	.L2105
	leaq	7(%r10), %rdx
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2437:
	movq	-136(%rbp), %rdx
	leaq	(%rdx,%r9), %r15
	leaq	8(%r9), %rdx
	subq	$1, %r10
	je	.L2458
	movq	%rdx, %r9
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2186:
	subq	$3, %rsi
	leaq	9(%r13), %rcx
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	cmpq	%rcx, %rax
	jnb	.L2250
	leaq	2(%rdi), %rdx
	movl	$65535, %r11d
	subq	%r13, %rdx
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L2188:
	movq	%rbx, %rdx
	movq	%rax, %r9
	movl	%r10d, %esi
	addl	$1, %r10d
	andq	%rax, %rdx
	imull	$506832829, (%r15,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	leaq	(%r12,%r8,4), %rdi
	movl	40(%rdi), %edx
	subq	%rdx, %r9
	movq	%r9, %rdx
	movzwl	%ax, %r9d
	cmpq	$65535, %rdx
	movb	%cl, 196648(%r12,%r9)
	movzwl	%si, %ecx
	cmova	%r11, %rdx
	leaq	(%r12,%rcx,4), %rcx
	movw	%dx, 262184(%rcx)
	leaq	(%r12,%r8,2), %rdx
	movzwl	131112(%rdx), %r8d
	movw	%r8w, 262186(%rcx)
	movl	%eax, 40(%rdi)
	movw	%si, 131112(%rdx)
	leaq	0(%r13,%rax), %rdx
	addq	$2, %rax
	cmpq	%r14, %rax
	jb	.L2188
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	16(%rbp), %rsi
	movq	16(%rbp), %rdi
	movslq	(%rsi), %rdx
	movslq	4(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rax, %rdx
	je	.L2161
	cmpq	%rax, %rcx
	je	.L2459
	addq	$3, %rax
	movq	%rax, %r14
	subq	%rdx, %r14
	cmpq	$6, %r14
	jbe	.L2460
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L2461
	movq	16(%rbp), %rax
	movq	-128(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	je	.L2243
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2462
	leaq	15(%rcx), %rdx
.L2165:
	testq	%rdx, %rdx
	je	.L2161
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L2163:
	movq	16(%rbp), %rcx
	movl	%esi, 4(%rcx)
	movq	-128(%rbp), %rsi
	movl	%eax, 12(%rcx)
	movl	%esi, %eax
	movl	%edi, 8(%rcx)
	subl	$1, %eax
	movl	%esi, (%rcx)
	movl	%eax, 16(%rcx)
	movl	%esi, %eax
	addl	$1, %eax
	movl	%eax, 20(%rcx)
	movl	%esi, %eax
	subl	$2, %eax
	movl	%eax, 24(%rcx)
	movl	%esi, %eax
	addl	$2, %eax
	movl	%eax, 28(%rcx)
	movl	%esi, %eax
	subl	$3, %eax
	movl	%eax, 32(%rcx)
	movl	%esi, %eax
	addl	$3, %eax
	movl	%eax, 36(%rcx)
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2438:
	movq	%rdi, -160(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r9, -48(%rbp)
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	24(%r12), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r12)
	jb	.L2150
	movq	-56(%rbp), %rsi
	movw	%r14w, -192(%rbp)
	leaq	2(%rax), %rdi
	movq	%r13, -224(%rbp)
	imull	$506832829, (%rsi), %edx
	movq	-168(%rbp), %rsi
	movq	%r15, -240(%rbp)
	movq	%r11, -184(%rbp)
	movq	%rdi, %r11
	movq	104(%rsi), %rcx
	movq	%rbx, -248(%rbp)
	movq	%rsi, %rbx
	shrl	$18, %edx
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %r10
	leaq	1(%r13), %rcx
	movq	%rcx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L2156:
	movzwl	(%r10,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r12)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L2151
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -112(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -120(%rbp)
	jb	.L2151
	movq	80(%rbx), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r14
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r14
	je	.L2152
	movq	-56(%rbp), %rdx
	movq	(%r8), %r13
	xorl	%r15d, %r15d
	movq	(%rdx), %rdx
	cmpq	%r13, %rdx
	je	.L2463
.L2153:
	xorq	%r13, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L2155:
	movl	88(%rbx), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L2151
	testq	%r15, %r15
	je	.L2151
	movq	%rdi, %rdx
	movq	96(%rbx), %r14
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %r14
	movq	%r14, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	movq	-152(%rbp), %rdi
	salq	%cl, %rdx
	leaq	(%rdi,%rsi), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, -208(%rbp)
	jb	.L2151
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%ecx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-64(%rbp), %rdx
	jb	.L2151
	movzwl	-112(%rbp), %esi
	addq	$1, 32(%r12)
	movq	%rdx, -64(%rbp)
	subl	%r15d, %esi
	movq	%rcx, -128(%rbp)
	movl	%esi, -72(%rbp)
	movq	%r15, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2151:
	cmpq	%r11, %rax
	jne	.L2156
	movq	-48(%rbp), %rax
	movq	-184(%rbp), %r11
	movzwl	-192(%rbp), %r14d
	movq	-224(%rbp), %r13
	movq	-240(%rbp), %r15
	movq	-248(%rbp), %rbx
	addq	$175, %rax
	cmpq	-64(%rbp), %rax
	jbe	.L2464
.L2157:
	movq	-144(%rbp), %r13
	movq	-176(%rbp), %rax
	cmpq	%rax, %r13
	movq	%r13, %r11
	cmovbe	%r13, %rax
	movq	%rax, %rdi
	movl	-136(%rbp), %eax
	movl	%eax, -72(%rbp)
	movq	-160(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2229:
	movq	%rdx, -104(%rbp)
	movq	%rdi, -128(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%r8, -64(%rbp)
	jmp	.L2137
.L2113:
	cmpq	$0, -184(%rbp)
	je	.L2112
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r11
	movzbl	(%r10), %r10d
	cmpb	%r10b, (%r11)
	jne	.L2112
	movq	-296(%rbp), %r11
	cmpq	$0, -192(%rbp)
	movq	%r11, -312(%rbp)
	je	.L2112
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r11
	movq	$1, -88(%rbp)
	movzbl	1(%r10), %r10d
	cmpb	%r10b, 1(%r11)
	jne	.L2112
.L2204:
	movq	-88(%rbp), %r13
	cmpq	$0, -224(%rbp)
	leaq	1(%r13), %r11
	movq	%r11, -88(%rbp)
	je	.L2117
	movq	-72(%rbp), %r10
	movq	-312(%rbp), %r11
	movzbl	1(%r10,%r13), %r10d
	cmpb	%r10b, 1(%r11)
	jne	.L2117
	leaq	2(%r13), %r10
	cmpq	$0, -240(%rbp)
	movq	%r10, -88(%rbp)
	je	.L2117
	movq	-72(%rbp), %r10
	movzbl	2(%r10,%r13), %r10d
	cmpb	%r10b, 2(%r11)
	jne	.L2117
	leaq	3(%r13), %r10
	cmpq	$0, -248(%rbp)
	movq	%r10, -88(%rbp)
	je	.L2117
	movq	-72(%rbp), %r10
	movzbl	3(%r10,%r13), %r10d
	cmpb	%r10b, 3(%r11)
	jne	.L2117
	leaq	4(%r13), %r10
	cmpq	$0, -256(%rbp)
	movq	%r10, -88(%rbp)
	je	.L2117
	movq	-72(%rbp), %r10
	movzbl	4(%r10,%r13), %r10d
	cmpb	%r10b, 4(%r11)
	jne	.L2117
	leaq	5(%r13), %r10
	cmpq	$0, -264(%rbp)
	movq	%r10, -88(%rbp)
	je	.L2117
	movq	-72(%rbp), %r10
	movzbl	5(%r10,%r13), %r10d
	cmpb	%r10b, 5(%r11)
	jne	.L2117
	leaq	6(%r13), %r10
	cmpq	$0, -272(%rbp)
	movq	%r10, -88(%rbp)
	je	.L2117
	movq	-72(%rbp), %r10
	movzbl	6(%r11), %r11d
	cmpb	%r11b, 6(%r10,%r13)
	jne	.L2117
	addq	$7, %r13
	movq	%r13, -88(%rbp)
	jmp	.L2117
.L2441:
	subq	$1, -88(%rbp)
	movq	-288(%rbp), %r10
	leaq	(%r10,%r11), %r13
	leaq	8(%r11), %r10
	je	.L2465
	movq	%r10, %r11
	jmp	.L2115
.L2443:
	movq	-224(%rbp), %rdx
	leaq	(%rdx,%r8), %r14
	leaq	8(%r8), %rdx
	subq	$1, %r9
	je	.L2466
	movq	%rdx, %r8
	jmp	.L2133
.L2161:
	movq	-232(%rbp), %rsi
	xorl	%edx, %edx
	leaq	16(%rsi), %rax
	movq	%rax, -56(%rbp)
	movl	-80(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-72(%rbp), %eax
	sall	$25, %eax
	orl	-104(%rbp), %eax
	movl	%eax, 4(%rsi)
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	.L2169
.L2453:
	movq	-80(%rbp), %rdi
	movl	%edi, %edx
	sall	$3, %edi
	andl	$56, %edi
.L2171:
	cmpq	$9, %rax
	jbe	.L2467
.L2174:
	cmpq	$133, %rax
	jbe	.L2468
	cmpq	$2117, %rax
	ja	.L2177
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L2175:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L2178
.L2456:
	testb	%sil, %sil
	je	.L2178
	cmpw	$15, %ax
	ja	.L2178
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L2181:
	movq	-232(%rbp), %rax
	movq	-80(%rbp), %rsi
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	leaq	2(%r11), %rdi
	movq	%rdi, -48(%rbp)
	addq	%rsi, (%rax)
	movq	-104(%rbp), %rax
	movq	-360(%rbp), %rsi
	leaq	(%rax,%r11), %r13
	cmpq	%rsi, %r13
	movq	%rsi, %r10
	movq	-128(%rbp), %rsi
	cmovbe	%r13, %r10
	shrq	$2, %rax
	cmpq	%rsi, %rax
	jbe	.L2182
	movq	%rsi, %rax
	movq	%r13, %rsi
	salq	$2, %rax
	subq	%rax, %rsi
	cmpq	%rdi, %rsi
	movq	%rsi, %rax
	cmovb	%rdi, %rax
	cmpq	%r10, %rax
	cmova	%r10, %rax
	movq	%rax, -48(%rbp)
.L2182:
	movq	-48(%rbp), %rsi
	movq	%rsi, %rax
	cmpq	%r10, %rsi
	jnb	.L2247
	movzwl	524328(%r12), %edi
	movl	$65535, %r11d
	movw	%di, -64(%rbp)
	subl	%esi, %edi
	movl	%edi, %r14d
	.p2align 4,,10
	.p2align 3
.L2184:
	movq	%rbx, %rdx
	movq	%rax, %r9
	leal	(%r14,%rax), %esi
	andq	%rax, %rdx
	imull	$506832829, (%r15,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	leaq	(%r12,%r8,4), %rdi
	movl	40(%rdi), %edx
	subq	%rdx, %r9
	movq	%r9, %rdx
	movzwl	%ax, %r9d
	cmpq	$65535, %rdx
	movb	%cl, 196648(%r12,%r9)
	movzwl	%si, %ecx
	cmova	%r11, %rdx
	leaq	(%r12,%rcx,4), %rcx
	movw	%dx, 262184(%rcx)
	leaq	(%r12,%r8,2), %rdx
	movzwl	131112(%rdx), %r8d
	movw	%r8w, 262186(%rcx)
	movl	%eax, 40(%rdi)
	addq	$1, %rax
	movw	%si, 131112(%rdx)
	cmpq	%rax, %r10
	jne	.L2184
	movq	-56(%rbp), %rax
	addw	-64(%rbp), %r10w
	movq	$0, -80(%rbp)
	subw	-48(%rbp), %r10w
	movw	%r10w, 524328(%r12)
	movq	%rax, -232(%rbp)
	jmp	.L2183
.L2450:
	movl	-72(%rbp), %eax
	subq	$1, -120(%rbp)
	movw	%r14w, -264(%rbp)
	movl	%eax, -136(%rbp)
	movq	-64(%rbp), %rax
	movq	%r11, -144(%rbp)
	movq	%rax, -48(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L2159
.L2460:
	leal	0(,%r14,4), %ecx
	movl	$158663784, %eax
	sarl	%cl, %eax
	movl	%eax, %edx
	andl	$15, %edx
	jmp	.L2165
.L2454:
	movq	-80(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L2171
.L2468:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L2175
.L2123:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2122
	movzbl	(%r14), %ebx
	cmpb	%bl, (%r8)
	jne	.L2122
.L2199:
	movq	-128(%rbp), %rbx
	leaq	1(%rbx), %r15
	cmpq	$1, %rdx
	je	.L2126
	movzbl	1(%r14,%rbx), %ecx
	cmpb	%cl, 1(%r8)
	jne	.L2126
	leaq	2(%rbx), %r15
	cmpq	$2, %rdx
	je	.L2126
	movzbl	2(%r14,%rbx), %ecx
	cmpb	%cl, 2(%r8)
	jne	.L2126
	leaq	3(%rbx), %r15
	cmpq	$3, %rdx
	je	.L2126
	movzbl	3(%r14,%rbx), %ecx
	cmpb	%cl, 3(%r8)
	jne	.L2126
	leaq	4(%rbx), %r15
	cmpq	$4, %rdx
	je	.L2126
	movzbl	4(%r14,%rbx), %ecx
	cmpb	%cl, 4(%r8)
	jne	.L2126
	leaq	5(%rbx), %r15
	subq	$5, %rdx
	je	.L2126
	movzbl	5(%r14,%rbx), %ecx
	cmpb	%cl, 5(%r8)
	jne	.L2126
	leaq	6(%rbx), %r15
	cmpq	$1, %rdx
	je	.L2126
	movzbl	6(%r14,%rbx), %ecx
	cmpb	%cl, 6(%r8)
	jne	.L2126
	addq	$7, %rbx
	movq	%rbx, %r15
	jmp	.L2126
.L2457:
	subq	$1, -128(%rbp)
	leaq	8(%r8), %rdx
	je	.L2219
	movq	8(%r8), %rbx
	movq	8(%r14), %rdx
	movl	$8, %r15d
	cmpq	%rbx, %rdx
	jne	.L2124
	cmpq	$1, -128(%rbp)
	leaq	16(%r8), %rdx
	je	.L2221
	movq	16(%r8), %rbx
	movq	16(%r14), %rdx
	movl	$16, %r15d
	cmpq	%rbx, %rdx
	jne	.L2124
	movq	$24, -128(%rbp)
	addq	$24, %r8
.L2125:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2224
	movq	-128(%rbp), %rbx
	movzbl	(%r14,%rbx), %ebx
	cmpb	%bl, (%r8)
	je	.L2199
.L2224:
	movq	-128(%rbp), %r15
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	%rdi, -128(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%r8, -64(%rbp)
	jmp	.L2130
.L2177:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L2180
.L2455:
	movq	-80(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L2171
.L2143:
	cmpq	$0, -288(%rbp)
	je	.L2142
	movq	-112(%rbp), %r9
	movq	-56(%rbp), %rdi
	movzbl	(%r9), %r9d
	cmpb	%r9b, (%rdi)
	jne	.L2142
	movq	-384(%rbp), %rdi
	cmpq	$0, -296(%rbp)
	movq	%rdi, -400(%rbp)
	je	.L2142
	movq	-56(%rbp), %r9
	movq	-112(%rbp), %rdi
	movq	$1, -392(%rbp)
	movzbl	1(%r9), %r9d
	cmpb	%r9b, 1(%rdi)
	jne	.L2142
.L2202:
	movq	-392(%rbp), %r10
	cmpq	$0, -312(%rbp)
	leaq	1(%r10), %rdi
	movq	%rdi, -152(%rbp)
	je	.L2147
	movq	-112(%rbp), %rdi
	movq	-400(%rbp), %r9
	movq	%rdi, -256(%rbp)
	movzbl	1(%rdi,%r10), %edi
	cmpb	%dil, 1(%r9)
	jne	.L2147
	leaq	2(%r10), %rdi
	cmpq	$0, -320(%rbp)
	movq	%rdi, -152(%rbp)
	je	.L2147
	movq	-256(%rbp), %rdi
	movzbl	2(%rdi,%r10), %edi
	cmpb	%dil, 2(%r9)
	jne	.L2147
	leaq	3(%r10), %rdi
	cmpq	$0, -328(%rbp)
	movq	%rdi, -152(%rbp)
	je	.L2147
	movq	-112(%rbp), %rdi
	movzbl	3(%rdi,%r10), %r10d
	cmpb	%r10b, 3(%r9)
	jne	.L2147
	movq	-392(%rbp), %r10
	cmpq	$0, -344(%rbp)
	leaq	4(%r10), %rdi
	movq	%rdi, -152(%rbp)
	je	.L2147
	movq	-112(%rbp), %rdi
	movzbl	4(%rdi,%r10), %r10d
	cmpb	%r10b, 4(%r9)
	jne	.L2147
	movq	-392(%rbp), %r10
	cmpq	$0, -336(%rbp)
	leaq	5(%r10), %rdi
	movq	%rdi, -152(%rbp)
	je	.L2147
	movq	-112(%rbp), %rdi
	movzbl	5(%rdi,%r10), %r10d
	cmpb	%r10b, 5(%r9)
	jne	.L2147
	movq	-392(%rbp), %r10
	cmpq	$0, -352(%rbp)
	leaq	6(%r10), %rdi
	movq	%rdi, -152(%rbp)
	je	.L2147
	movq	-112(%rbp), %rdi
	movzbl	6(%r9), %r9d
	cmpb	%r9b, 6(%rdi,%r10)
	jne	.L2147
	addq	$7, %r10
	movq	%r10, -152(%rbp)
	jmp	.L2147
.L2448:
	subq	$1, -152(%rbp)
	movq	-376(%rbp), %rdi
	leaq	(%rdi,%r9), %r10
	leaq	8(%r9), %rdi
	je	.L2469
	movq	%rdi, %r9
	jmp	.L2145
.L2247:
	movq	-56(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -232(%rbp)
	jmp	.L2183
.L2458:
	cmpq	$0, -184(%rbp)
	je	.L2105
	movzbl	(%rcx,%rdx), %r10d
	cmpb	%r10b, (%r15)
	jne	.L2105
	cmpq	$0, -192(%rbp)
	movq	%r15, %rdx
	leaq	9(%r9), %r10
	leaq	1(%r15), %r15
	je	.L2211
	movzbl	9(%rcx,%r9), %r9d
	cmpb	%r9b, 1(%rdx)
	je	.L2205
.L2211:
	movq	%r10, %rdx
	jmp	.L2105
.L2245:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L2171
.L2459:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L2163
.L2152:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2151
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L2151
.L2196:
	leaq	1(%r14), %r15
	cmpq	$1, %rdx
	je	.L2155
	movq	-56(%rbp), %rcx
	movzbl	1(%rcx,%r14), %r13d
	cmpb	%r13b, 1(%r8)
	jne	.L2155
	leaq	2(%r14), %r15
	cmpq	$2, %rdx
	je	.L2155
	movzbl	2(%rcx,%r14), %r13d
	cmpb	%r13b, 2(%r8)
	jne	.L2155
	leaq	3(%r14), %r15
	cmpq	$3, %rdx
	je	.L2155
	movzbl	3(%rcx,%r14), %r13d
	cmpb	%r13b, 3(%r8)
	jne	.L2155
	leaq	4(%r14), %r15
	cmpq	$4, %rdx
	je	.L2155
	movzbl	4(%rcx,%r14), %r13d
	cmpb	%r13b, 4(%r8)
	jne	.L2155
	leaq	5(%r14), %r15
	subq	$5, %rdx
	je	.L2155
	movzbl	5(%rcx,%r14), %r13d
	cmpb	%r13b, 5(%r8)
	jne	.L2155
	leaq	6(%r14), %r15
	cmpq	$1, %rdx
	je	.L2155
	movzbl	6(%rcx,%r14), %ecx
	cmpb	%cl, 6(%r8)
	jne	.L2155
	leaq	7(%r14), %r15
	jmp	.L2155
.L2463:
	leaq	8(%r8), %rdx
	subq	$1, %r14
	je	.L2237
	movq	-56(%rbp), %rdx
	movq	8(%r8), %r13
	movl	$8, %r15d
	movq	8(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L2153
	leaq	16(%r8), %rdx
	cmpq	$1, %r14
	je	.L2239
	movq	-56(%rbp), %rdx
	movq	16(%r8), %r13
	movl	$16, %r15d
	addq	$24, %r8
	movl	$24, %r14d
	movq	16(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L2153
.L2154:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2242
	movq	-56(%rbp), %rcx
	movzbl	(%rcx,%r14), %ecx
	cmpb	%cl, (%r8)
	je	.L2196
.L2242:
	movq	%r14, %r15
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2461:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %eax
	sarl	%cl, %eax
	movl	%eax, %edx
	andl	$15, %edx
	jmp	.L2165
.L2208:
	xorl	%edx, %edx
	jmp	.L2098
.L2465:
	cmpq	$0, -184(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r11, -152(%rbp)
	je	.L2117
	movq	%r10, %r11
	movq	%r13, %r10
	movq	-72(%rbp), %r13
	movzbl	0(%r13,%r11), %r13d
	cmpb	%r13b, (%r10)
	jne	.L2117
	movq	-152(%rbp), %r11
	leaq	1(%r10), %r13
	movq	%r13, -312(%rbp)
	addq	$9, %r11
	cmpq	$0, -192(%rbp)
	movq	%r11, -88(%rbp)
	je	.L2117
	movq	-72(%rbp), %r13
	movq	-152(%rbp), %r11
	movzbl	9(%r13,%r11), %r11d
	cmpb	%r11b, 1(%r10)
	je	.L2204
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2466:
	cmpq	$0, -288(%rbp)
	je	.L2135
	movzbl	(%rcx,%rdx), %r9d
	cmpb	%r9b, (%r14)
	jne	.L2135
	cmpq	$0, -296(%rbp)
	movq	%r14, %rdx
	leaq	9(%r8), %r9
	leaq	1(%r14), %r14
	je	.L2228
	movzbl	9(%rcx,%r8), %r8d
	cmpb	%r8b, 1(%rdx)
	je	.L2203
.L2228:
	movq	%r9, %rdx
	jmp	.L2135
.L2219:
	movq	$8, -128(%rbp)
	movq	%rdx, %r8
	jmp	.L2125
.L2462:
	movl	$3, %edx
	jmp	.L2163
.L2243:
	movl	$2, %edx
	jmp	.L2163
.L2469:
	cmpq	$0, -288(%rbp)
	movq	%rdi, -152(%rbp)
	movq	%r9, -256(%rbp)
	je	.L2147
	movq	%rdi, %r9
	movq	%r10, %rdi
	movq	-112(%rbp), %r10
	movzbl	(%r10,%r9), %r10d
	cmpb	%r10b, (%rdi)
	jne	.L2147
	movq	-256(%rbp), %r9
	leaq	1(%rdi), %r10
	movq	%r10, -400(%rbp)
	addq	$9, %r9
	cmpq	$0, -296(%rbp)
	movq	%r9, -392(%rbp)
	je	.L2232
	movq	-112(%rbp), %r10
	movq	-256(%rbp), %r9
	movzbl	9(%r10,%r9), %r9d
	cmpb	%r9b, 1(%rdi)
	je	.L2202
	movq	-392(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	jmp	.L2147
.L2237:
	movq	%rdx, %r8
	movl	$8, %r14d
	jmp	.L2154
.L2221:
	movq	$16, -128(%rbp)
	movq	%rdx, %r8
	jmp	.L2125
.L2452:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2239:
	movq	%rdx, %r8
	movl	$16, %r14d
	jmp	.L2154
.L2232:
	movq	%r9, -152(%rbp)
	jmp	.L2147
	.cfi_endproc
.LFE286:
	.size	CreateBackwardReferencesNH41, .-CreateBackwardReferencesNH41
	.p2align 4
	.type	CreateBackwardReferencesNH42, @function
CreateBackwardReferencesNH42:
.LFB287:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$240, %rsp
	movq	%rdx, -48(%rbp)
	movl	$256, %edx
	movq	%r8, -176(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	leaq	(%rsi,%rdi), %rcx
	subq	$16, %rax
	cmpq	$3, %rdi
	movq	16(%rbp), %rdi
	movq	%rcx, -112(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	leaq	-3(%rcx), %rax
	cmovbe	%rsi, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -368(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -328(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movq	%rax, -376(%rbp)
	addq	%rsi, %rax
	movq	%rax, -296(%rbp)
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	leal	-1(%rax), %edx
	movl	%edx, 16(%rdi)
	leal	1(%rax), %edx
	movl	%edx, 20(%rdi)
	leal	-2(%rax), %edx
	movl	%edx, 24(%rdi)
	leal	2(%rax), %edx
	movl	%edx, 28(%rdi)
	leal	-3(%rax), %edx
	addl	$3, %eax
	movl	%eax, 36(%rdi)
	movl	4(%rdi), %eax
	movl	%edx, 32(%rdi)
	leal	-1(%rax), %edx
	movl	%edx, 40(%rdi)
	leal	1(%rax), %edx
	movl	%edx, 44(%rdi)
	leal	-2(%rax), %edx
	movl	%edx, 48(%rdi)
	leal	2(%rax), %edx
	movl	%edx, 52(%rdi)
	leal	-3(%rax), %edx
	addl	$3, %eax
	movl	%eax, 60(%rdi)
	leaq	4(%rsi), %rax
	movl	%edx, 56(%rdi)
	movq	%rax, -208(%rbp)
	cmpq	%rcx, %rax
	jnb	.L2582
	movq	32(%rbp), %rax
	movq	%rsi, %r13
	movq	%rbx, %r15
	movq	%r9, %r12
	movq	%rax, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L2563:
	movq	-200(%rbp), %rax
	movq	-112(%rbp), %rbx
	movq	$0, -128(%rbp)
	movq	-48(%rbp), %r14
	movq	16(%rbp), %r11
	movq	$0, -80(%rbp)
	subq	%r13, %rbx
	movq	%rax, %rdi
	cmpq	%r13, %rax
	movq	%r15, -56(%rbp)
	movq	-176(%rbp), %rax
	cmova	%r13, %rdi
	movq	%rbx, -120(%rbp)
	movq	$2020, -64(%rbp)
	movq	72(%rax), %rax
	movq	%rax, -232(%rbp)
	movq	%r15, %rax
	andq	%r13, %rax
	addq	%rax, %r14
	movq	%rax, -168(%rbp)
	imull	$506832829, (%r14), %eax
	shrl	$17, %eax
	movl	%eax, %esi
	movl	%eax, -96(%rbp)
	movl	%eax, %r10d
	movq	%rbx, %rax
	shrq	$3, %rax
	andl	$7, %ebx
	movq	%rsi, -104(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rbx, %rax
	movq	%rbx, -184(%rbp)
	leaq	-1(%rbx), %rbx
	movq	%rbx, -192(%rbp)
	leaq	-2(%rax), %rbx
	movq	%rbx, -216(%rbp)
	leaq	-3(%rax), %rbx
	movq	%rbx, -224(%rbp)
	leaq	-4(%rax), %rbx
	movq	%rbx, -256(%rbp)
	leaq	-5(%rax), %rbx
	movq	%rbx, -264(%rbp)
	leaq	-6(%rax), %rbx
	subq	$7, %rax
	movq	%rbx, -280(%rbp)
	leaq	1(%r14), %rbx
	movq	%rbx, -144(%rbp)
	leaq	8(%r14), %rbx
	movq	%rax, -288(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -136(%rbp)
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2813:
	movzwl	%cx, %edx
	cmpb	196648(%r12,%rdx), %r10b
	jne	.L2475
	cmpq	%r13, %rcx
	jnb	.L2475
	cmpq	%rdi, %r8
	ja	.L2475
.L2569:
	movq	-72(%rbp), %rbx
	andq	-56(%rbp), %rcx
	addq	-48(%rbp), %rcx
	testq	%rbx, %rbx
	je	.L2476
	xorl	%r9d, %r9d
.L2478:
	movq	(%r14,%r9), %rdx
	movq	(%rcx,%r9), %r15
	cmpq	%r15, %rdx
	je	.L2810
	xorq	%r15, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%r9, %rdx
.L2480:
	cmpq	$1, %rdx
	jbe	.L2475
	leaq	(%rdx,%rdx,8), %rcx
	movq	-64(%rbp), %rbx
	movq	%rcx, %r9
	salq	$4, %r9
	subq	%rcx, %r9
	addq	$1935, %r9
	cmpq	%rbx, %r9
	jbe	.L2475
	testq	%rax, %rax
	je	.L2586
	leal	-1(%rsi), %ecx
	movl	$117264, %eax
	andl	$14, %ecx
	sarl	%cl, %eax
	movq	$-39, %rcx
	andl	$14, %eax
	subq	%rax, %rcx
	addq	%rcx, %r9
	cmpq	%rbx, %r9
	ja	.L2811
	.p2align 4,,10
	.p2align 3
.L2475:
	cmpq	$16, %rsi
	je	.L2812
.L2482:
	movq	%rsi, %rax
.L2484:
	movslq	(%r11,%rax,4), %r8
	movq	%r13, %rcx
	leaq	1(%rax), %rsi
	subq	%r8, %rcx
	testq	%rax, %rax
	jne	.L2813
	cmpq	%r13, %rcx
	jnb	.L2482
	cmpq	%rdi, %r8
	jbe	.L2569
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2812:
	movq	-104(%rbp), %rdx
	movq	%r13, %rbx
	movq	1311784(%r12), %rcx
	movl	-96(%rbp), %esi
	movq	-56(%rbp), %r15
	leaq	(%r12,%rdx,4), %rax
	movq	%rcx, -240(%rbp)
	movq	%rax, -160(%rbp)
	movl	40(%rax), %eax
	andl	$511, %esi
	movq	%rsi, -136(%rbp)
	subq	%rax, %rbx
	leaq	(%r12,%rdx,2), %rax
	leaq	-1(%rcx), %rdx
	movq	%rax, -152(%rbp)
	movzwl	131112(%rax), %eax
	movq	%rdx, -248(%rbp)
	movw	%ax, -144(%rbp)
	testq	%rcx, %rcx
	je	.L2485
	cmpq	%rdi, %rbx
	ja	.L2485
	salq	$9, %rsi
	movq	%r14, -104(%rbp)
	movq	%rdx, %rcx
	movq	-80(%rbp), %r8
	movq	%rsi, %r10
	leaq	1(%r14), %rsi
	movq	%r13, -56(%rbp)
	movq	%rbx, %rdx
	movq	%rsi, -320(%rbp)
	leaq	8(%r14), %rsi
	movq	-168(%rbp), %r14
	movq	%rsi, -312(%rbp)
	movq	%rbx, -168(%rbp)
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2487:
	subq	$1, %rcx
	cmpq	$-1, %rcx
	je	.L2800
.L2494:
	addq	%rsi, %rdx
	cmpq	%rdi, %rdx
	ja	.L2800
.L2486:
	addq	%r10, %rax
	leaq	(%r14,%r8), %r11
	leaq	(%r12,%rax,4), %rsi
	movzwl	262186(%rsi), %eax
	movzwl	262184(%rsi), %esi
	cmpq	%r11, %r15
	jb	.L2487
	movq	-56(%rbp), %r9
	subq	%rdx, %r9
	andq	%r15, %r9
	leaq	(%r9,%r8), %rbx
	cmpq	%rbx, %r15
	jb	.L2487
	movq	-48(%rbp), %r13
	movzbl	0(%r13,%rbx), %ebx
	cmpb	%bl, 0(%r13,%r11)
	jne	.L2487
	movq	-72(%rbp), %rbx
	addq	%r13, %r9
	movq	%r9, -80(%rbp)
	testq	%rbx, %rbx
	je	.L2488
	movq	%rbx, -304(%rbp)
	movq	-56(%rbp), %r13
	xorl	%r11d, %r11d
.L2490:
	movq	-104(%rbp), %rbx
	movq	(%rbx,%r11), %r9
	movq	-80(%rbp), %rbx
	movq	(%rbx,%r11), %rbx
	cmpq	%rbx, %r9
	je	.L2814
	xorq	%rbx, %r9
	movq	%r13, -56(%rbp)
	rep bsfq	%r9, %r9
	movslq	%r9d, %r9
	shrq	$3, %r9
	addq	%r11, %r9
.L2492:
	cmpq	$3, %r9
	jbe	.L2487
	leaq	(%r9,%r9,8), %rbx
	movq	%rbx, %r11
	salq	$4, %r11
	subq	%rbx, %r11
	bsrl	%edx, %ebx
	imull	$30, %ebx, %ebx
	addq	$1920, %r11
	subq	%rbx, %r11
	cmpq	-64(%rbp), %r11
	jbe	.L2487
	subq	$1, %rcx
	movq	%rdx, -128(%rbp)
	movq	%r9, %r8
	movq	%r11, -64(%rbp)
	cmpq	$-1, %rcx
	jne	.L2494
	.p2align 4,,10
	.p2align 3
.L2800:
	movq	-104(%rbp), %r14
	movq	-168(%rbp), %rbx
	movq	%r8, -80(%rbp)
	movq	-56(%rbp), %r13
.L2485:
	movq	-136(%rbp), %rsi
	movzbl	-96(%rbp), %r10d
	leaq	(%r12,%rsi,2), %rdx
	salq	$9, %rsi
	movzwl	1310760(%rdx), %eax
	leal	1(%rax), %ecx
	movw	%cx, 1310760(%rdx)
	movl	%eax, %edx
	movzwl	%r13w, %ecx
	andl	$511, %eax
	movb	%r10b, 196648(%r12,%rcx)
	andw	$511, %dx
	addq	%rsi, %rax
	movl	$65535, %ecx
	cmpq	$65535, %rbx
	leaq	(%r12,%rax,4), %rax
	cmovbe	%rbx, %rcx
	movzwl	-144(%rbp), %ebx
	cmpq	$2020, -64(%rbp)
	movl	$0, -144(%rbp)
	movw	%cx, 262184(%rax)
	movw	%bx, 262186(%rax)
	movq	-160(%rbp), %rax
	movl	%r13d, 40(%rax)
	movq	-152(%rbp), %rax
	movw	%dx, 131112(%rax)
	je	.L2815
.L2495:
	movq	-120(%rbp), %rax
	movq	%r13, -168(%rbp)
	subq	$1, %rax
	movq	%rax, -152(%rbp)
	movq	-176(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, -288(%rbp)
.L2534:
	xorl	%r8d, %r8d
	cmpl	$4, -288(%rbp)
	jg	.L2503
	movq	-80(%rbp), %rax
	leaq	-1(%rax), %r8
	movq	-152(%rbp), %rax
	cmpq	%rax, %r8
	cmova	%rax, %r8
.L2503:
	movq	-168(%rbp), %rax
	movq	-48(%rbp), %rsi
	movq	%r8, -72(%rbp)
	movq	$0, -136(%rbp)
	movq	16(%rbp), %r9
	leaq	1(%rax), %rbx
	movq	-200(%rbp), %rax
	movq	$2020, -96(%rbp)
	movq	$0, -120(%rbp)
	cmpq	%rax, %rbx
	movq	%rax, %r14
	movq	%rbx, %rax
	cmovbe	%rbx, %r14
	andq	%r15, %rax
	addq	%rax, %rsi
	movq	%rax, -264(%rbp)
	imull	$506832829, (%rsi), %eax
	movq	%rsi, -56(%rbp)
	shrl	$17, %eax
	movl	%eax, %edi
	movl	%eax, -160(%rbp)
	movl	%eax, %r10d
	movq	-152(%rbp), %rax
	movq	%rdi, -184(%rbp)
	movq	%rax, %rdi
	andl	$7, %eax
	shrq	$3, %rdi
	movq	%rax, -296(%rbp)
	movq	%rdi, -104(%rbp)
	leaq	-1(%rax), %rdi
	movq	%rdi, -304(%rbp)
	leaq	-2(%rax), %rdi
	movq	%rdi, -312(%rbp)
	leaq	-3(%rax), %rdi
	movq	%rdi, -320(%rbp)
	leaq	-4(%rax), %rdi
	movq	%rdi, -336(%rbp)
	leaq	-5(%rax), %rdi
	movq	%rdi, -352(%rbp)
	leaq	-6(%rax), %rdi
	subq	$7, %rax
	movq	%rdi, -344(%rbp)
	leaq	1(%rsi), %rdi
	movq	%rdi, -216(%rbp)
	leaq	8(%rsi), %rdi
	movq	%rax, -360(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -192(%rbp)
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2819:
	movzwl	%cx, %edx
	cmpb	196648(%r12,%rdx), %r10b
	jne	.L2505
	cmpq	%rcx, %rbx
	jbe	.L2505
	cmpq	%r14, %rdi
	ja	.L2505
	movq	-104(%rbp), %r11
	andq	%r15, %rcx
	addq	-48(%rbp), %rcx
	testq	%r11, %r11
	je	.L2506
.L2820:
	xorl	%r8d, %r8d
.L2508:
	movq	-56(%rbp), %rdx
	movq	(%rcx,%r8), %r13
	movq	(%rdx,%r8), %rdx
	cmpq	%r13, %rdx
	je	.L2816
	xorq	%r13, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%r8, %rdx
.L2510:
	cmpq	$1, %rdx
	jbe	.L2505
	leaq	(%rdx,%rdx,8), %rcx
	movq	-96(%rbp), %r11
	movq	%rcx, %r8
	salq	$4, %r8
	subq	%rcx, %r8
	addq	$1935, %r8
	cmpq	%r11, %r8
	jbe	.L2505
	testq	%rax, %rax
	je	.L2603
	leal	-1(%rsi), %ecx
	movl	$117264, %eax
	andl	$14, %ecx
	sarl	%cl, %eax
	movq	$-39, %rcx
	andl	$14, %eax
	subq	%rax, %rcx
	addq	%rcx, %r8
	cmpq	%r11, %r8
	ja	.L2817
	.p2align 4,,10
	.p2align 3
.L2505:
	cmpq	$16, %rsi
	je	.L2818
.L2512:
	movq	%rsi, %rax
.L2514:
	movslq	(%r9,%rax,4), %rdi
	movq	%rbx, %rcx
	leaq	1(%rax), %rsi
	subq	%rdi, %rcx
	testq	%rax, %rax
	jne	.L2819
	cmpq	%r14, %rdi
	ja	.L2512
	cmpq	%rcx, %rbx
	jbe	.L2512
	movq	-104(%rbp), %r11
	andq	%r15, %rcx
	addq	-48(%rbp), %rcx
	testq	%r11, %r11
	jne	.L2820
.L2506:
	cmpq	$0, -296(%rbp)
	je	.L2505
	movq	-56(%rbp), %rdx
	movzbl	(%rcx), %r11d
	cmpb	%r11b, (%rdx)
	jne	.L2505
	cmpq	$0, -304(%rbp)
	movq	-216(%rbp), %r13
	je	.L2505
	movzbl	1(%rdx), %edx
	movl	$1, %r11d
	cmpb	%dl, 1(%rcx)
	jne	.L2505
.L2577:
	cmpq	$0, -312(%rbp)
	leaq	1(%r11), %rdx
	je	.L2510
	movzbl	1(%rcx,%r11), %r8d
	cmpb	%r8b, 1(%r13)
	jne	.L2510
	cmpq	$0, -320(%rbp)
	leaq	2(%r11), %rdx
	je	.L2510
	movzbl	2(%rcx,%r11), %r8d
	cmpb	%r8b, 2(%r13)
	jne	.L2510
	cmpq	$0, -336(%rbp)
	leaq	3(%r11), %rdx
	je	.L2510
	movzbl	3(%rcx,%r11), %r8d
	cmpb	%r8b, 3(%r13)
	jne	.L2510
	cmpq	$0, -352(%rbp)
	leaq	4(%r11), %rdx
	je	.L2510
	movzbl	4(%rcx,%r11), %r8d
	cmpb	%r8b, 4(%r13)
	jne	.L2510
	cmpq	$0, -344(%rbp)
	leaq	5(%r11), %rdx
	je	.L2510
	movzbl	5(%rcx,%r11), %r8d
	cmpb	%r8b, 5(%r13)
	jne	.L2510
	cmpq	$0, -360(%rbp)
	leaq	6(%r11), %rdx
	je	.L2510
	movzbl	6(%r13), %r8d
	cmpb	%r8b, 6(%rcx,%r11)
	jne	.L2510
	leaq	7(%r11), %rdx
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2586:
	movq	%rdx, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -128(%rbp)
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2476:
	cmpq	$0, -184(%rbp)
	je	.L2475
	movzbl	(%rcx), %ebx
	cmpb	%bl, (%r14)
	jne	.L2475
	cmpq	$0, -192(%rbp)
	movq	-144(%rbp), %r15
	je	.L2475
	movzbl	1(%r14), %edx
	movl	$1, %ebx
	cmpb	%dl, 1(%rcx)
	jne	.L2475
.L2579:
	cmpq	$0, -216(%rbp)
	leaq	1(%rbx), %rdx
	je	.L2480
	movzbl	1(%rcx,%rbx), %r9d
	cmpb	%r9b, 1(%r15)
	jne	.L2480
	cmpq	$0, -224(%rbp)
	leaq	2(%rbx), %rdx
	je	.L2480
	movzbl	2(%rcx,%rbx), %r9d
	cmpb	%r9b, 2(%r15)
	jne	.L2480
	cmpq	$0, -256(%rbp)
	leaq	3(%rbx), %rdx
	je	.L2480
	movzbl	3(%rcx,%rbx), %r9d
	cmpb	%r9b, 3(%r15)
	jne	.L2480
	cmpq	$0, -264(%rbp)
	leaq	4(%rbx), %rdx
	je	.L2480
	movzbl	4(%rcx,%rbx), %r9d
	cmpb	%r9b, 4(%r15)
	jne	.L2480
	cmpq	$0, -280(%rbp)
	leaq	5(%rbx), %rdx
	je	.L2480
	movzbl	5(%rcx,%rbx), %r9d
	cmpb	%r9b, 5(%r15)
	jne	.L2480
	cmpq	$0, -288(%rbp)
	leaq	6(%rbx), %rdx
	je	.L2480
	movzbl	6(%r15), %r15d
	cmpb	%r15b, 6(%rcx,%rbx)
	jne	.L2480
	leaq	7(%rbx), %rdx
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2810:
	movq	-136(%rbp), %rdx
	leaq	(%rdx,%r9), %r15
	leaq	8(%r9), %rdx
	subq	$1, %rbx
	je	.L2821
	movq	%rdx, %r9
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L2818:
	movq	-184(%rbp), %rsi
	movq	%rbx, %r13
	movl	-160(%rbp), %edi
	movq	-72(%rbp), %r8
	leaq	(%r12,%rsi,4), %rax
	andl	$511, %edi
	movq	%rax, -256(%rbp)
	movl	40(%rax), %eax
	movq	%rdi, -192(%rbp)
	subq	%rax, %r13
	leaq	(%r12,%rsi,2), %rax
	cmpq	$0, -240(%rbp)
	movq	%rax, -224(%rbp)
	movzwl	131112(%rax), %eax
	movw	%ax, -216(%rbp)
	je	.L2515
	cmpq	%r14, %r13
	ja	.L2515
	salq	$9, %rdi
	movq	%r13, -280(%rbp)
	movq	%r13, %rdx
	movq	-248(%rbp), %rcx
	movq	%rdi, %r10
	movq	-56(%rbp), %rdi
	movq	%rbx, -72(%rbp)
	movq	-264(%rbp), %r13
	leaq	1(%rdi), %rsi
	addq	$8, %rdi
	movq	%rsi, -392(%rbp)
	movq	%rdi, -384(%rbp)
	jmp	.L2516
	.p2align 4,,10
	.p2align 3
.L2517:
	subq	$1, %rcx
	cmpq	$-1, %rcx
	je	.L2801
.L2524:
	addq	%rsi, %rdx
	cmpq	%r14, %rdx
	ja	.L2801
.L2516:
	addq	%r10, %rax
	leaq	0(%r13,%r8), %r9
	leaq	(%r12,%rax,4), %rsi
	movzwl	262186(%rsi), %eax
	movzwl	262184(%rsi), %esi
	cmpq	%r9, %r15
	jb	.L2517
	movq	-72(%rbp), %rdi
	subq	%rdx, %rdi
	andq	%r15, %rdi
	leaq	(%rdi,%r8), %r11
	cmpq	%r11, %r15
	jb	.L2517
	movq	-48(%rbp), %rbx
	movzbl	(%rbx,%r11), %r11d
	cmpb	%r11b, (%rbx,%r9)
	jne	.L2517
	addq	%rbx, %rdi
	movq	%rdi, -184(%rbp)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2518
	movq	%rdi, -264(%rbp)
	movq	-72(%rbp), %rbx
	xorl	%r9d, %r9d
.L2520:
	movq	-56(%rbp), %rdi
	movq	-184(%rbp), %r11
	movq	(%rdi,%r9), %rdi
	movq	(%r11,%r9), %r11
	cmpq	%r11, %rdi
	je	.L2822
	xorq	%r11, %rdi
	movq	%rbx, -72(%rbp)
	rep bsfq	%rdi, %rdi
	movslq	%edi, %rdi
	shrq	$3, %rdi
	addq	%r9, %rdi
.L2522:
	cmpq	$3, %rdi
	jbe	.L2517
	leaq	(%rdi,%rdi,8), %r11
	movq	%r11, %r9
	salq	$4, %r9
	subq	%r11, %r9
	bsrl	%edx, %r11d
	imull	$30, %r11d, %r11d
	addq	$1920, %r9
	subq	%r11, %r9
	cmpq	-96(%rbp), %r9
	jbe	.L2517
	subq	$1, %rcx
	movq	%rdx, -136(%rbp)
	movq	%rdi, %r8
	movq	%rdi, -120(%rbp)
	movq	%r9, -96(%rbp)
	cmpq	$-1, %rcx
	jne	.L2524
	.p2align 4,,10
	.p2align 3
.L2801:
	movq	-72(%rbp), %rbx
	movq	-280(%rbp), %r13
.L2515:
	movq	-192(%rbp), %rdi
	movzbl	-160(%rbp), %esi
	leaq	(%r12,%rdi,2), %rdx
	salq	$9, %rdi
	movzwl	1310760(%rdx), %eax
	leal	1(%rax), %ecx
	movw	%cx, 1310760(%rdx)
	movl	%eax, %edx
	movzwl	%bx, %ecx
	andl	$511, %eax
	movb	%sil, 196648(%r12,%rcx)
	addq	%rdi, %rax
	andw	$511, %dx
	movl	$65535, %ecx
	cmpq	$65535, %r13
	movzwl	-216(%rbp), %edi
	leaq	(%r12,%rax,4), %rax
	cmovbe	%r13, %rcx
	xorl	%r13d, %r13d
	cmpq	$2020, -96(%rbp)
	movw	%di, 262186(%rax)
	movw	%cx, 262184(%rax)
	movq	-256(%rbp), %rax
	movl	%ebx, 40(%rax)
	movq	-224(%rbp), %rax
	movw	%dx, 131112(%rax)
	je	.L2823
.L2525:
	movq	-64(%rbp), %rax
	addq	$175, %rax
	cmpq	-96(%rbp), %rax
	ja	.L2532
.L2838:
	addq	$1, -88(%rbp)
	cmpq	-208(%rbp), %rbx
	je	.L2803
	movq	-168(%rbp), %rax
	addq	$5, %rax
	cmpq	%rax, -112(%rbp)
	ja	.L2824
.L2803:
	movq	%r14, %r10
	movl	%r13d, %r14d
.L2533:
	movq	-376(%rbp), %rax
	movq	-120(%rbp), %rdi
	movq	-136(%rbp), %rdx
	addq	%rbx, %rax
	leaq	(%rax,%rdi,2), %rax
	movq	%rax, -296(%rbp)
	cmpq	%r10, %rdx
	jbe	.L2825
	leaq	15(%rdx), %rdx
.L2565:
	movq	-272(%rbp), %rdi
	leaq	16(%rdi), %rax
	movq	%rax, -56(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, (%rdi)
	movl	%r14d, %eax
	sall	$25, %eax
	orl	-120(%rbp), %eax
	movl	%eax, 4(%rdi)
	movq	-176(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r11
	cmpq	%rcx, %rdx
	jb	.L2826
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	salq	%cl, %rdi
	andq	%rdx, %r10
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r11,%r10), %r10d
	movl	%r8d, %ecx
	addl	%edi, %r10d
	subq	%rsi, %rdx
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%dil
.L2544:
	movq	-272(%rbp), %rsi
	movw	%ax, 14(%rsi)
	movl	-120(%rbp), %eax
	movl	%edx, 8(%rsi)
	addl	%r14d, %eax
	cmpq	$5, -88(%rbp)
	cltq
	jbe	.L2827
	cmpq	$129, -88(%rbp)
	jbe	.L2828
	cmpq	$2113, -88(%rbp)
	jbe	.L2829
	movq	-88(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L2616
	cmpq	$22594, %rcx
	sbbl	%esi, %esi
	andl	$-8, %esi
	addl	$56, %esi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L2549
.L2840:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %esi
	cmpw	$7, %dx
	jbe	.L2830
.L2553:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L2555:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %esi
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2815:
	movq	24(%r12), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r12)
	jb	.L2496
	imull	$506832829, (%r14), %edx
	addq	$1, %rdi
	movq	%r15, -96(%rbp)
	leaq	2(%rax), %r11
	movq	-176(%rbp), %rsi
	movq	%rdi, -136(%rbp)
	movq	%r13, -72(%rbp)
	shrl	$18, %edx
	movq	104(%rsi), %rcx
	movq	%rsi, %r13
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %rbx
	.p2align 4,,10
	.p2align 3
.L2502:
	movzwl	(%rbx,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r12)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L2497
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -56(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -120(%rbp)
	jb	.L2497
	movq	80(%r13), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	movq	%rdi, %rdx
	addq	168(%r9), %r8
	shrq	$3, %rdx
	movq	%rdx, -104(%rbp)
	je	.L2498
	movq	(%r8), %r10
	movq	(%r14), %rdx
	xorl	%r15d, %r15d
	cmpq	%r10, %rdx
	je	.L2831
.L2499:
	xorq	%r10, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L2501:
	movl	88(%r13), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L2497
	testq	%r15, %r15
	je	.L2497
	movq	%rdi, %rdx
	movq	96(%r13), %r10
	addq	-136(%rbp), %rsi
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %r10
	movq	%r10, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	salq	%cl, %rdx
	addq	%rsi, %rdx
	cmpq	%rdx, -232(%rbp)
	jb	.L2497
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rcx
	salq	$4, %rcx
	subq	%rsi, %rcx
	bsrl	%edx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rcx
	subq	%rsi, %rcx
	cmpq	-64(%rbp), %rcx
	jb	.L2497
	movzwl	-56(%rbp), %esi
	addq	$1, 32(%r12)
	movq	%rcx, -64(%rbp)
	subl	%r15d, %esi
	movq	%rdx, -128(%rbp)
	movl	%esi, -144(%rbp)
	movq	%r15, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2497:
	cmpq	%r11, %rax
	jne	.L2502
	cmpq	$2020, -64(%rbp)
	movq	-72(%rbp), %r13
	movq	-96(%rbp), %r15
	jne	.L2495
.L2496:
	movq	-88(%rbp), %rsi
	movq	-296(%rbp), %rdi
	leaq	1(%r13), %rax
	leaq	1(%rsi), %rdx
	cmpq	%rdi, %rax
	jbe	.L2621
	addq	-328(%rbp), %rdi
	movq	%rdi, %rcx
	movq	-112(%rbp), %rdi
	cmpq	%rax, %rcx
	jnb	.L2560
	leaq	-4(%rdi), %rbx
	leaq	17(%r13), %rcx
	cmpq	%rcx, %rbx
	cmova	%rcx, %rbx
	cmpq	%rbx, %rax
	jnb	.L2621
	leaq	4(%rsi), %rdx
	movl	$65535, %r14d
	subq	%r13, %rdx
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L2561:
	movq	-48(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rax, %r11
	andq	%rax, %rdx
	imull	$506832829, (%rdi,%rdx), %esi
	shrl	$17, %esi
	movl	%esi, %edx
	movl	%esi, %r9d
	andl	$511, %edx
	leaq	(%r12,%rdx,2), %rdi
	salq	$9, %rdx
	movzwl	1310760(%rdi), %ecx
	leal	1(%rcx), %r8d
	movl	%ecx, %r10d
	andl	$511, %ecx
	movw	%r8w, 1310760(%rdi)
	leaq	(%r12,%r9,4), %r8
	addq	%rcx, %rdx
	andw	$511, %r10w
	movl	40(%r8), %edi
	leaq	(%r12,%rdx,4), %rcx
	leaq	(%r12,%r9,2), %rdx
	subq	%rdi, %r11
	movq	%r11, %rdi
	movzwl	%ax, %r11d
	cmpq	$65535, %rdi
	movb	%sil, 196648(%r12,%r11)
	cmova	%r14, %rdi
	movw	%di, 262184(%rcx)
	movzwl	131112(%rdx), %esi
	movw	%si, 262186(%rcx)
	movl	%eax, 40(%r8)
	movw	%r10w, 131112(%rdx)
	leaq	0(%r13,%rax), %rdx
	addq	$4, %rax
	cmpq	%rbx, %rax
	jb	.L2561
.L2621:
	movq	%rdx, -88(%rbp)
	movq	%rax, %r13
.L2558:
	leaq	4(%r13), %rax
	movq	%rax, -208(%rbp)
	cmpq	-112(%rbp), %rax
	jb	.L2563
	movq	-272(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r13, %r12
	sarq	$4, %rdx
.L2473:
	movq	24(%rbp), %rbx
	movq	-112(%rbp), %rax
	addq	-88(%rbp), %rax
	subq	%r12, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2811:
	.cfi_restore_state
	movq	%r8, -128(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r9, -64(%rbp)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2560:
	leaq	-3(%rdi), %rbx
	leaq	9(%r13), %rcx
	cmpq	%rcx, %rbx
	cmova	%rcx, %rbx
	cmpq	%rbx, %rax
	jnb	.L2621
	leaq	2(%rsi), %rdx
	movl	$65535, %r14d
	subq	%r13, %rdx
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L2562:
	movq	-48(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rax, %r11
	andq	%rax, %rdx
	imull	$506832829, (%rdi,%rdx), %esi
	shrl	$17, %esi
	movl	%esi, %edx
	movl	%esi, %r9d
	andl	$511, %edx
	leaq	(%r12,%rdx,2), %rdi
	salq	$9, %rdx
	movzwl	1310760(%rdi), %ecx
	leal	1(%rcx), %r8d
	movl	%ecx, %r10d
	andl	$511, %ecx
	movw	%r8w, 1310760(%rdi)
	leaq	(%r12,%r9,4), %r8
	addq	%rcx, %rdx
	andw	$511, %r10w
	movl	40(%r8), %edi
	leaq	(%r12,%rdx,4), %rcx
	leaq	(%r12,%r9,2), %rdx
	subq	%rdi, %r11
	movq	%r11, %rdi
	movzwl	%ax, %r11d
	cmpq	$65535, %rdi
	movb	%sil, 196648(%r12,%r11)
	cmova	%r14, %rdi
	movw	%di, 262184(%rcx)
	movzwl	131112(%rdx), %esi
	movw	%si, 262186(%rcx)
	movl	%eax, 40(%r8)
	movw	%r10w, 131112(%rdx)
	leaq	0(%r13,%rax), %rdx
	addq	$2, %rax
	cmpq	%rbx, %rax
	jb	.L2562
	jmp	.L2621
	.p2align 4,,10
	.p2align 3
.L2603:
	movq	%rdx, -120(%rbp)
	movq	%rdi, -136(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r8, -96(%rbp)
	jmp	.L2512
.L2816:
	movq	-192(%rbp), %rdx
	leaq	(%rdx,%r8), %r13
	leaq	8(%r8), %rdx
	subq	$1, %r11
	je	.L2832
	movq	%rdx, %r8
	jmp	.L2508
.L2825:
	movq	16(%rbp), %rax
	movq	16(%rbp), %rdi
	movslq	(%rax), %rcx
	movslq	4(%rdi), %rsi
	movq	%rcx, %rax
	movq	%rsi, %rdi
	cmpq	%rdx, %rcx
	je	.L2536
	cmpq	%rdx, %rsi
	je	.L2833
	addq	$3, %rdx
	movq	%rdx, %r10
	subq	%rcx, %r10
	movq	%r10, %rcx
	cmpq	$6, %r10
	jbe	.L2834
	subq	%rsi, %rdx
	cmpq	$6, %rdx
	jbe	.L2835
	movq	16(%rbp), %rsi
	movslq	8(%rsi), %rdx
	movq	-136(%rbp), %rsi
	movq	%rdx, %rcx
	cmpq	%rsi, %rdx
	je	.L2614
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L2836
	leaq	15(%rsi), %rdx
.L2540:
	testq	%rdx, %rdx
	je	.L2536
	movq	16(%rbp), %rsi
	movl	8(%rsi), %ecx
.L2538:
	movl	%edi, 8(%rsi)
	movq	-136(%rbp), %rdi
	movl	%ecx, 12(%rsi)
	movl	%eax, 4(%rsi)
	leal	-1(%rdi), %ecx
	movl	%edi, (%rsi)
	movq	16(%rbp), %rsi
	movl	%ecx, 16(%rsi)
	movq	16(%rbp), %rsi
	leal	1(%rdi), %ecx
	movl	%ecx, 20(%rsi)
	movq	16(%rbp), %rsi
	leal	-2(%rdi), %ecx
	movl	%ecx, 24(%rsi)
	movq	16(%rbp), %rsi
	leal	2(%rdi), %ecx
	movl	%ecx, 28(%rsi)
	movq	16(%rbp), %rsi
	leal	-3(%rdi), %ecx
	movl	%ecx, 32(%rsi)
	leal	3(%rdi), %ecx
	movl	%ecx, 36(%rsi)
	leal	-1(%rax), %ecx
	movl	%ecx, 40(%rsi)
	leal	1(%rax), %ecx
	movl	%ecx, 44(%rsi)
	leal	-2(%rax), %ecx
	movl	%ecx, 48(%rsi)
	leal	2(%rax), %ecx
	movl	%ecx, 52(%rsi)
	leal	-3(%rax), %ecx
	addl	$3, %eax
	movl	%ecx, 56(%rsi)
	movl	%eax, 60(%rsi)
	jmp	.L2565
.L2823:
	movq	24(%r12), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r12)
	jb	.L2525
	movq	-56(%rbp), %rdi
	leaq	1(%r14), %rsi
	movl	%r13d, -104(%rbp)
	leaq	2(%rax), %r10
	movq	%rsi, -160(%rbp)
	imull	$506832829, (%rdi), %edx
	movq	-176(%rbp), %rdi
	movq	%rbx, -184(%rbp)
	movq	%r15, -216(%rbp)
	movq	104(%rdi), %rcx
	movq	%r14, -192(%rbp)
	movq	%rdi, %r14
	shrl	$18, %edx
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %r11
	.p2align 4,,10
	.p2align 3
.L2531:
	movzwl	(%r11,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r12)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L2526
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -72(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -152(%rbp)
	jb	.L2526
	movq	80(%r14), %r9
	shrq	$5, %rsi
	movq	%rdi, %r8
	movq	%rdi, %r13
	imulq	%rsi, %r8
	movl	32(%r9,%rdi,4), %edx
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r13
	je	.L2527
	movq	-56(%rbp), %rdx
	movq	(%r8), %rbx
	xorl	%r15d, %r15d
	movq	(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L2837
.L2528:
	xorq	%rbx, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L2530:
	movl	88(%r14), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L2526
	testq	%r15, %r15
	je	.L2526
	movq	%rdi, %rdx
	movq	96(%r14), %rbx
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rcx
	movq	-160(%rbp), %rbx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	salq	%cl, %rdx
	leaq	(%rbx,%rsi), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, -232(%rbp)
	jb	.L2526
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%ecx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-96(%rbp), %rdx
	jb	.L2526
	movzwl	-72(%rbp), %esi
	addq	$1, 32(%r12)
	movq	%rdx, -96(%rbp)
	subl	%r15d, %esi
	movq	%rcx, -136(%rbp)
	movl	%esi, -104(%rbp)
	movq	%r15, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L2526:
	cmpq	%r10, %rax
	jne	.L2531
	movq	-64(%rbp), %rax
	movq	-184(%rbp), %rbx
	movl	-104(%rbp), %r13d
	movq	-192(%rbp), %r14
	movq	-216(%rbp), %r15
	addq	$175, %rax
	cmpq	-96(%rbp), %rax
	jbe	.L2838
.L2532:
	movq	-168(%rbp), %r13
	movq	-200(%rbp), %rax
	movl	-144(%rbp), %r14d
	cmpq	%rax, %r13
	movq	%r13, %rbx
	cmovbe	%r13, %rax
	movq	%rax, %r10
	movq	-128(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L2533
.L2814:
	movq	-312(%rbp), %rbx
	leaq	8(%r11), %r9
	addq	%r11, %rbx
	subq	$1, -304(%rbp)
	je	.L2839
	movq	%r9, %r11
	jmp	.L2490
.L2488:
	cmpq	$0, -184(%rbp)
	je	.L2487
	movq	-104(%rbp), %rbx
	movzbl	(%r9), %r11d
	cmpb	%r11b, (%rbx)
	jne	.L2487
	movq	-320(%rbp), %rbx
	cmpq	$0, -192(%rbp)
	movq	%rbx, -304(%rbp)
	je	.L2487
	movq	-104(%rbp), %r9
	movq	-80(%rbp), %r11
	movl	$1, %ebx
	movzbl	1(%r9), %r9d
	cmpb	%r9b, 1(%r11)
	jne	.L2487
.L2578:
	cmpq	$0, -216(%rbp)
	leaq	1(%rbx), %r9
	je	.L2492
	movq	-80(%rbp), %r13
	movq	-304(%rbp), %r11
	movzbl	1(%r13,%rbx), %r13d
	cmpb	%r13b, 1(%r11)
	jne	.L2492
	cmpq	$0, -224(%rbp)
	leaq	2(%rbx), %r9
	je	.L2492
	movq	-80(%rbp), %r13
	movzbl	2(%r13,%rbx), %r13d
	cmpb	%r13b, 2(%r11)
	jne	.L2492
	cmpq	$0, -256(%rbp)
	leaq	3(%rbx), %r9
	je	.L2492
	movq	-80(%rbp), %r13
	movzbl	3(%r13,%rbx), %r13d
	cmpb	%r13b, 3(%r11)
	jne	.L2492
	cmpq	$0, -264(%rbp)
	leaq	4(%rbx), %r9
	je	.L2492
	movq	-80(%rbp), %r13
	movzbl	4(%r13,%rbx), %r13d
	cmpb	%r13b, 4(%r11)
	jne	.L2492
	cmpq	$0, -280(%rbp)
	leaq	5(%rbx), %r9
	je	.L2492
	movq	-80(%rbp), %r13
	movzbl	5(%r13,%rbx), %r13d
	cmpb	%r13b, 5(%r11)
	jne	.L2492
	cmpq	$0, -288(%rbp)
	leaq	6(%rbx), %r9
	je	.L2492
	movq	-80(%rbp), %r13
	movzbl	6(%r11), %r11d
	cmpb	%r11b, 6(%r13,%rbx)
	jne	.L2492
	leaq	7(%rbx), %r9
	jmp	.L2492
.L2536:
	movq	-272(%rbp), %rdi
	xorl	%edx, %edx
	leaq	16(%rdi), %rax
	movq	%rax, -56(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, (%rdi)
	movl	%r14d, %eax
	sall	$25, %eax
	orl	-120(%rbp), %eax
	movl	%eax, 4(%rdi)
	movl	$1, %edi
	xorl	%eax, %eax
	jmp	.L2544
.L2827:
	movq	-88(%rbp), %rsi
	movl	%esi, %edx
	sall	$3, %esi
	andl	$56, %esi
.L2546:
	cmpq	$9, %rax
	jbe	.L2840
.L2549:
	cmpq	$133, %rax
	jbe	.L2841
	cmpq	$2117, %rax
	ja	.L2552
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L2550:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %esi
	cmpw	$7, %dx
	ja	.L2553
.L2830:
	testb	%dil, %dil
	je	.L2553
	cmpw	$15, %ax
	ja	.L2553
	movl	%esi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %esi
.L2556:
	movq	-272(%rbp), %rax
	movq	-88(%rbp), %rdi
	movw	%si, 12(%rax)
	movq	48(%rbp), %rax
	addq	%rdi, (%rax)
	movq	-120(%rbp), %rdi
	leaq	2(%rbx), %rax
	leaq	(%rdi,%rbx), %r13
	movq	-368(%rbp), %rbx
	cmpq	%rbx, %r13
	cmovbe	%r13, %rbx
	shrq	$2, %rdi
	movq	%rdi, %rdx
	movq	-136(%rbp), %rdi
	cmpq	%rdi, %rdx
	jbe	.L2557
	movq	%rdi, %rdx
	movq	%r13, %rdi
	salq	$2, %rdx
	subq	%rdx, %rdi
	cmpq	%rax, %rdi
	cmovnb	%rdi, %rax
	cmpq	%rbx, %rax
	cmova	%rbx, %rax
.L2557:
	cmpq	%rbx, %rax
	jnb	.L2618
	movq	%r13, -64(%rbp)
	movq	-48(%rbp), %r13
	movl	$65535, %r14d
	.p2align 4,,10
	.p2align 3
.L2559:
	movq	%rax, %rdx
	movq	%rax, %r11
	andq	%r15, %rdx
	imull	$506832829, 0(%r13,%rdx), %esi
	shrl	$17, %esi
	movl	%esi, %edx
	movl	%esi, %r9d
	andl	$511, %edx
	leaq	(%r12,%rdx,2), %rdi
	salq	$9, %rdx
	movzwl	1310760(%rdi), %ecx
	leal	1(%rcx), %r8d
	movl	%ecx, %r10d
	andl	$511, %ecx
	movw	%r8w, 1310760(%rdi)
	leaq	(%r12,%r9,4), %r8
	addq	%rcx, %rdx
	andw	$511, %r10w
	movl	40(%r8), %edi
	leaq	(%r12,%rdx,4), %rcx
	leaq	(%r12,%r9,2), %rdx
	subq	%rdi, %r11
	movq	%r11, %rdi
	movzwl	%ax, %r11d
	cmpq	$65535, %rdi
	movb	%sil, 196648(%r12,%r11)
	cmova	%r14, %rdi
	movw	%di, 262184(%rcx)
	movzwl	131112(%rdx), %esi
	movw	%si, 262186(%rcx)
	movl	%eax, 40(%r8)
	addq	$1, %rax
	movw	%r10w, 131112(%rdx)
	cmpq	%rax, %rbx
	jne	.L2559
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r13
	movq	$0, -88(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L2558
.L2824:
	movq	-96(%rbp), %rax
	subq	$1, -152(%rbp)
	movl	%r13d, -144(%rbp)
	movq	%rax, -64(%rbp)
	movq	-136(%rbp), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2534
.L2817:
	movq	%rdi, -136(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r8, -96(%rbp)
	jmp	.L2505
.L2834:
	sall	$2, %ecx
	movl	$158663784, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L2540
.L2841:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L2550
.L2828:
	movq	-88(%rbp), %rsi
	subq	$2, %rsi
	bsrl	%esi, %edx
	leal	-1(%rdx), %ecx
	shrq	%cl, %rsi
	leal	2(%rsi,%rcx,2), %edx
	leal	0(,%rdx,8), %esi
	andl	$56, %esi
	jmp	.L2546
.L2498:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2497
	movzbl	(%r14), %ecx
	cmpb	%cl, (%r8)
	jne	.L2497
.L2573:
	movq	-104(%rbp), %rcx
	leaq	1(%rcx), %r15
	cmpq	$1, %rdx
	je	.L2501
	movzbl	1(%r14,%rcx), %r10d
	cmpb	%r10b, 1(%r8)
	jne	.L2501
	leaq	2(%rcx), %r15
	cmpq	$2, %rdx
	je	.L2501
	movzbl	2(%r14,%rcx), %r10d
	cmpb	%r10b, 2(%r8)
	jne	.L2501
	leaq	3(%rcx), %r15
	cmpq	$3, %rdx
	je	.L2501
	movzbl	3(%r14,%rcx), %r10d
	cmpb	%r10b, 3(%r8)
	jne	.L2501
	leaq	4(%rcx), %r15
	cmpq	$4, %rdx
	je	.L2501
	movzbl	4(%r14,%rcx), %r10d
	cmpb	%r10b, 4(%r8)
	jne	.L2501
	leaq	5(%rcx), %r15
	subq	$5, %rdx
	je	.L2501
	movzbl	5(%r14,%rcx), %r10d
	cmpb	%r10b, 5(%r8)
	jne	.L2501
	leaq	6(%rcx), %r15
	cmpq	$1, %rdx
	je	.L2501
	movzbl	6(%r14,%rcx), %edx
	cmpb	%dl, 6(%r8)
	jne	.L2501
	addq	$7, %rcx
	movq	%rcx, %r15
	jmp	.L2501
.L2831:
	subq	$1, -104(%rbp)
	leaq	8(%r8), %rdx
	je	.L2593
	movq	8(%r8), %r10
	movq	8(%r14), %rdx
	movl	$8, %r15d
	cmpq	%r10, %rdx
	jne	.L2499
	cmpq	$1, -104(%rbp)
	leaq	16(%r8), %rdx
	je	.L2595
	movq	16(%r8), %r10
	movq	16(%r14), %rdx
	movl	$16, %r15d
	cmpq	%r10, %rdx
	jne	.L2499
	movq	$24, -104(%rbp)
	addq	$24, %r8
.L2500:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2598
	movq	-104(%rbp), %rcx
	movzbl	(%r14,%rcx), %ecx
	cmpb	%cl, (%r8)
	je	.L2573
.L2598:
	movq	-104(%rbp), %r15
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2821:
	cmpq	$0, -184(%rbp)
	je	.L2480
	movq	%r15, %rbx
	movzbl	(%rcx,%rdx), %r15d
	cmpb	%r15b, (%rbx)
	jne	.L2480
	cmpq	$0, -192(%rbp)
	movq	%rbx, %rdx
	leaq	1(%rbx), %r15
	leaq	9(%r9), %rbx
	je	.L2585
	movzbl	9(%rcx,%r9), %r9d
	cmpb	%r9b, 1(%rdx)
	je	.L2579
.L2585:
	movq	%rbx, %rdx
	jmp	.L2480
.L2552:
	orl	$7, %esi
	movl	$2, %eax
	jmp	.L2555
.L2829:
	movq	-88(%rbp), %rsi
	leaq	-66(%rsi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %esi
	andl	$56, %esi
	jmp	.L2546
.L2518:
	cmpq	$0, -296(%rbp)
	je	.L2517
	movq	-184(%rbp), %rdi
	movq	-56(%rbp), %rbx
	movzbl	(%rdi), %edi
	cmpb	%dil, (%rbx)
	jne	.L2517
	movq	-392(%rbp), %rdi
	cmpq	$0, -304(%rbp)
	movq	%rdi, -400(%rbp)
	je	.L2517
	movq	$1, -264(%rbp)
	movq	%rbx, %rdi
	movq	-184(%rbp), %rbx
	movzbl	1(%rdi), %edi
	cmpb	%dil, 1(%rbx)
	jne	.L2517
.L2576:
	movq	-264(%rbp), %rbx
	cmpq	$0, -312(%rbp)
	leaq	1(%rbx), %rdi
	je	.L2522
	movq	%rbx, %r11
	movq	-400(%rbp), %r9
	movq	-184(%rbp), %rbx
	movzbl	1(%rbx,%r11), %r11d
	cmpb	%r11b, 1(%r9)
	jne	.L2522
	movq	-264(%rbp), %rdi
	addq	$2, %rdi
	cmpq	$0, -320(%rbp)
	je	.L2522
	movq	-264(%rbp), %r11
	movzbl	2(%rbx,%r11), %r11d
	cmpb	%r11b, 2(%r9)
	jne	.L2522
	movq	-264(%rbp), %rdi
	addq	$3, %rdi
	cmpq	$0, -336(%rbp)
	je	.L2522
	movq	-264(%rbp), %r11
	movzbl	3(%rbx,%r11), %r11d
	cmpb	%r11b, 3(%r9)
	jne	.L2522
	movq	-264(%rbp), %rdi
	addq	$4, %rdi
	cmpq	$0, -352(%rbp)
	je	.L2522
	movq	-264(%rbp), %r11
	movzbl	4(%rbx,%r11), %r11d
	cmpb	%r11b, 4(%r9)
	jne	.L2522
	movq	-264(%rbp), %rdi
	addq	$5, %rdi
	cmpq	$0, -344(%rbp)
	je	.L2522
	movq	-264(%rbp), %r11
	movzbl	5(%rbx,%r11), %r11d
	cmpb	%r11b, 5(%r9)
	jne	.L2522
	movq	-264(%rbp), %rdi
	addq	$6, %rdi
	cmpq	$0, -360(%rbp)
	je	.L2522
	movzbl	6(%r9), %r11d
	movq	-264(%rbp), %r9
	cmpb	%r11b, 6(%rbx,%r9)
	jne	.L2522
	movq	%r9, %rdi
	addq	$7, %rdi
	jmp	.L2522
.L2822:
	subq	$1, -264(%rbp)
	movq	-384(%rbp), %rdi
	leaq	(%rdi,%r9), %r11
	leaq	8(%r9), %rdi
	je	.L2842
	movq	%rdi, %r9
	jmp	.L2520
.L2618:
	movq	-56(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L2558
.L2616:
	movl	$40, %esi
	movl	$21, %edx
	jmp	.L2546
.L2833:
	movq	16(%rbp), %rsi
	movl	$1, %edx
	movl	8(%rsi), %ecx
	jmp	.L2538
.L2527:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2526
	movq	-56(%rbp), %rbx
	movzbl	(%rbx), %ebx
	cmpb	%bl, (%r8)
	jne	.L2526
.L2570:
	leaq	1(%r13), %r15
	cmpq	$1, %rdx
	je	.L2530
	movq	-56(%rbp), %rbx
	movzbl	1(%rbx,%r13), %ecx
	cmpb	%cl, 1(%r8)
	jne	.L2530
	leaq	2(%r13), %r15
	cmpq	$2, %rdx
	je	.L2530
	movzbl	2(%rbx,%r13), %ecx
	cmpb	%cl, 2(%r8)
	jne	.L2530
	leaq	3(%r13), %r15
	cmpq	$3, %rdx
	je	.L2530
	movzbl	3(%rbx,%r13), %ecx
	cmpb	%cl, 3(%r8)
	jne	.L2530
	leaq	4(%r13), %r15
	cmpq	$4, %rdx
	je	.L2530
	movzbl	4(%rbx,%r13), %ecx
	cmpb	%cl, 4(%r8)
	jne	.L2530
	leaq	5(%r13), %r15
	subq	$5, %rdx
	je	.L2530
	movzbl	5(%rbx,%r13), %ecx
	cmpb	%cl, 5(%r8)
	jne	.L2530
	leaq	6(%r13), %r15
	cmpq	$1, %rdx
	je	.L2530
	movzbl	6(%rbx,%r13), %ebx
	cmpb	%bl, 6(%r8)
	jne	.L2530
	leaq	7(%r13), %r15
	jmp	.L2530
.L2837:
	leaq	8(%r8), %rdx
	subq	$1, %r13
	je	.L2609
	movq	-56(%rbp), %rdx
	movq	8(%r8), %rbx
	movl	$8, %r15d
	movq	8(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2528
	leaq	16(%r8), %rdx
	cmpq	$1, %r13
	je	.L2611
	movq	-56(%rbp), %rdx
	movq	16(%r8), %rbx
	movl	$16, %r15d
	movq	16(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2528
	addq	$24, %r8
	movl	$24, %r15d
.L2529:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L2530
	movq	-56(%rbp), %rbx
	movzbl	(%rbx,%r15), %ebx
	cmpb	%bl, (%r8)
	jne	.L2530
	movq	%r15, %r13
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2835:
	leal	0(,%rdx,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L2540
.L2832:
	cmpq	$0, -296(%rbp)
	je	.L2510
	movq	%r13, %r11
	movzbl	(%rcx,%rdx), %r13d
	cmpb	%r13b, (%r11)
	jne	.L2510
	cmpq	$0, -304(%rbp)
	movq	%r11, %rdx
	leaq	1(%r11), %r13
	leaq	9(%r8), %r11
	je	.L2602
	movzbl	9(%rcx,%r8), %r8d
	cmpb	%r8b, 1(%rdx)
	je	.L2577
.L2602:
	movq	%r11, %rdx
	jmp	.L2510
.L2582:
	xorl	%edx, %edx
	jmp	.L2473
.L2839:
	cmpq	$0, -184(%rbp)
	movq	%r13, -56(%rbp)
	je	.L2492
	movq	-80(%rbp), %r13
	movzbl	0(%r13,%r9), %r13d
	cmpb	%r13b, (%rbx)
	jne	.L2492
	movq	%rbx, %r9
	cmpq	$0, -192(%rbp)
	leaq	1(%rbx), %rbx
	movq	%rbx, -304(%rbp)
	leaq	9(%r11), %rbx
	je	.L2590
	movq	-80(%rbp), %r13
	movzbl	9(%r13,%r11), %r11d
	cmpb	%r11b, 1(%r9)
	je	.L2578
.L2590:
	movq	%rbx, %r9
	jmp	.L2492
.L2614:
	movq	16(%rbp), %rsi
	movl	$2, %edx
	jmp	.L2538
.L2593:
	movq	$8, -104(%rbp)
	movq	%rdx, %r8
	jmp	.L2500
.L2836:
	movq	16(%rbp), %rsi
	movl	$3, %edx
	jmp	.L2538
.L2842:
	cmpq	$0, -296(%rbp)
	movq	%rbx, -72(%rbp)
	je	.L2522
	movq	%r11, %rbx
	movq	-184(%rbp), %r11
	movzbl	(%r11,%rdi), %r11d
	cmpb	%r11b, (%rbx)
	jne	.L2522
	leaq	1(%rbx), %rdi
	cmpq	$0, -304(%rbp)
	movq	%rdi, -400(%rbp)
	leaq	9(%r9), %rdi
	je	.L2522
	movq	-184(%rbp), %r11
	movzbl	9(%r11,%r9), %r11d
	cmpb	%r11b, 1(%rbx)
	jne	.L2522
	movq	%rdi, -264(%rbp)
	jmp	.L2576
.L2609:
	movq	%rdx, %r8
	movl	$8, %r15d
	jmp	.L2529
.L2595:
	movq	$16, -104(%rbp)
	movq	%rdx, %r8
	jmp	.L2500
.L2826:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%dil
	xorl	%edx, %edx
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2611:
	movl	$16, %r15d
	movq	%rdx, %r8
	jmp	.L2529
	.cfi_endproc
.LFE287:
	.size	CreateBackwardReferencesNH42, .-CreateBackwardReferencesNH42
	.p2align 4
	.type	CreateBackwardReferencesNH54, @function
CreateBackwardReferencesNH54:
.LFB288:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	addq	%rdi, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$2048, %edx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -112(%rbp)
	movq	%r8, -216(%rbp)
	movl	8(%r8), %ecx
	movq	%r9, -120(%rbp)
	movq	%rsi, -96(%rbp)
	salq	%cl, %rax
	movl	$256, %ecx
	subq	$16, %rax
	cmpq	$7, %rdi
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	leaq	-7(%rsi), %rax
	cmova	%rax, %rdi
	cmpl	$9, 4(%r8)
	cmovl	%rcx, %rdx
	movl	$64, %ecx
	movq	%rdi, -272(%rbp)
	movq	%rdx, -232(%rbp)
	movl	$512, %edx
	cmovl	%rcx, %rdx
	leaq	(%r15,%rdx), %rbx
	movq	%rdx, -280(%rbp)
	leaq	8(%r15), %rdx
	movq	%rbx, -128(%rbp)
	cmpq	%rdx, %rsi
	jbe	.L2924
	leaq	40(%r9), %rbx
	movq	%rax, -240(%rbp)
	movq	%r15, %r13
	movq	%r14, %r15
	movq	%rbx, -144(%rbp)
	movl	%r10d, %ebx
	movq	%rbx, -48(%rbp)
	movq	32(%rbp), %rbx
	movq	%rbx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L2911:
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %r8
	movabsq	$3866266742567714048, %r14
	movq	-112(%rbp), %rbx
	subq	%r13, %r8
	cmpq	%r13, %rax
	cmova	%r13, %rax
	movq	%rbx, %rsi
	andq	%r13, %rsi
	movq	%rax, -80(%rbp)
	movq	16(%rbp), %rax
	addq	%r15, %rsi
	imulq	(%rsi), %r14
	movzbl	(%rsi), %r10d
	movslq	(%rax), %rax
	movb	%r10b, -176(%rbp)
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	movq	%r13, %rax
	shrq	$44, %r14
	subq	%rdi, %rax
	movq	%r8, %rdi
	movq	%r14, -72(%rbp)
	shrq	$3, %r8
	andl	$7, %edi
	movq	%rdi, -88(%rbp)
	cmpq	%r13, %rax
	jnb	.L2926
	andl	%ebx, %eax
	addq	%r15, %rax
	cmpb	%r10b, (%rax)
	je	.L3082
.L2926:
	movq	%rdi, %rax
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
	subq	$1, %rax
	movq	$2020, -64(%rbp)
	movq	%rax, -160(%rbp)
.L2847:
	movq	-144(%rbp), %rax
	movq	-72(%rbp), %rbx
	leaq	(%rax,%rbx,4), %rdi
	leaq	1(%rsi), %rbx
	movq	%rbx, -184(%rbp)
	leaq	8(%rsi), %rbx
	movl	(%rdi), %eax
	leaq	4(%rdi), %rcx
	movq	%rbx, -168(%rbp)
	addq	$20, %rdi
.L2861:
	movq	-48(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r15, %rdx
	movzbl	(%rdx,%r14), %r9d
	cmpl	%r10d, %r9d
	jne	.L2854
	movq	%r13, %r11
	subq	%rax, %r11
	je	.L2854
	cmpq	-80(%rbp), %r11
	ja	.L2854
	testq	%r8, %r8
	je	.L2855
	movq	%r8, %r12
	xorl	%r9d, %r9d
.L2857:
	movq	(%rsi,%r9), %rax
	movq	(%rdx,%r9), %rbx
	cmpq	%rbx, %rax
	je	.L3083
	xorq	%rbx, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%r9, %rax
.L2859:
	cmpq	$3, %rax
	jbe	.L2854
	leaq	(%rax,%rax,8), %r9
	movq	%r9, %rdx
	salq	$4, %rdx
	subq	%r9, %rdx
	bsrl	%r11d, %r9d
	imull	$30, %r9d, %r9d
	addq	$1920, %rdx
	subq	%r9, %rdx
	cmpq	-64(%rbp), %rdx
	jbe	.L2854
	movq	%r11, -104(%rbp)
	movzbl	(%rsi,%rax), %r10d
	movq	%rax, %r14
	movq	%rdx, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L2854:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	%rdi, %rcx
	jne	.L2861
	movq	%r13, %rax
	movq	-120(%rbp), %rbx
	shrq	$3, %rax
	andl	$3, %eax
	addq	-72(%rbp), %rax
	cmpq	$2020, -64(%rbp)
	movl	%r13d, 40(%rbx,%rax,4)
	jne	.L3084
	movq	-56(%rbp), %rbx
	leaq	1(%r13), %rax
	leaq	1(%rbx), %rsi
	cmpq	%rax, -128(%rbp)
	jnb	.L3081
	movq	-128(%rbp), %rdx
	addq	-232(%rbp), %rdx
	cmpq	%rax, %rdx
	jnb	.L2908
	movq	-240(%rbp), %rbx
	leaq	17(%r13), %rcx
	cmpq	%rbx, %rcx
	cmova	%rbx, %rcx
	movq	-56(%rbp), %rbx
	leaq	4(%rbx), %rdi
	subq	%r13, %rdi
	cmpq	%rax, %rcx
	jbe	.L3081
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2909:
	movabsq	$3866266742567714048, %rsi
	movq	%r8, %rdx
	andq	%rax, %rdx
	imulq	(%r15,%rdx), %rsi
	movq	%rsi, %rdx
	movq	%rax, %rsi
	shrq	$3, %rsi
	shrq	$44, %rdx
	andl	$3, %esi
	addq	%rsi, %rdx
	leaq	(%rdi,%rax), %rsi
	movl	%eax, 40(%rbx,%rdx,4)
	movq	%rax, %rdx
	addq	$4, %rax
	cmpq	%rcx, %rax
	jb	.L2909
	movq	%rsi, -56(%rbp)
	addq	$12, %rdx
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L2905:
	cmpq	%rdx, -96(%rbp)
	ja	.L2911
	movq	-152(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r13, %r15
	sarq	$4, %rdx
.L2846:
	movq	24(%rbp), %rbx
	movq	-96(%rbp), %rax
	addq	-56(%rbp), %rax
	subq	%r15, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3081:
	.cfi_restore_state
	leaq	9(%r13), %rdx
	movq	%rsi, -56(%rbp)
	movq	%rax, %r13
	jmp	.L2905
	.p2align 4,,10
	.p2align 3
.L3084:
	movq	-216(%rbp), %rax
	movq	%r14, -224(%rbp)
	movq	%r13, -128(%rbp)
	movl	4(%rax), %eax
	movl	%eax, -188(%rbp)
	movq	16(%rbp), %rax
	movslq	(%rax), %rax
	movq	%rax, %rbx
	movq	%rax, -288(%rbp)
	movl	$1, %eax
	subq	%rbx, %rax
	addq	%r13, %rax
	movq	%rax, -176(%rbp)
	leaq	4(%r13), %rax
	movq	%rax, -208(%rbp)
	movq	-96(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -200(%rbp)
	movq	%rax, %rdx
.L2881:
	subq	-128(%rbp), %rdx
	xorl	%r12d, %r12d
	cmpl	$4, -188(%rbp)
	jg	.L2863
	movq	-224(%rbp), %rax
	leaq	-1(%rax), %r12
	cmpq	%rdx, %r12
	cmova	%rdx, %r12
.L2863:
	movq	-128(%rbp), %rax
	movq	-112(%rbp), %rsi
	movq	%rdx, %rcx
	leaq	1(%rax), %r11
	movq	-136(%rbp), %rax
	cmpq	%rax, %r11
	cmovbe	%r11, %rax
	andq	%r11, %rsi
	andl	$7, %ecx
	shrq	$3, %rdx
	addq	%r15, %rsi
	movq	%rcx, -168(%rbp)
	movq	%rdx, %r14
	movq	-176(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movzbl	(%rsi,%r12), %ebx
	movabsq	$3866266742567714048, %rax
	imulq	(%rsi), %rax
	shrq	$44, %rax
	movq	%rax, -160(%rbp)
	cmpq	%rcx, %r11
	jbe	.L2936
	movq	-48(%rbp), %rdx
	andq	%rcx, %rdx
	addq	%r15, %rdx
	cmpb	(%rdx,%r12), %bl
	je	.L3085
.L2936:
	movq	-168(%rbp), %rax
	movq	$0, -80(%rbp)
	movl	$2020, %r10d
	subq	$1, %rax
	movq	%rax, -248(%rbp)
.L2864:
	leaq	1(%rsi), %rdx
	movq	-144(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	%rsi, -72(%rbp)
	movq	%rdx, -264(%rbp)
	leaq	8(%rsi), %rdx
	movq	%rdx, -256(%rbp)
	leaq	(%rax,%rcx,4), %rdi
	movl	(%rdi), %eax
	leaq	4(%rdi), %rcx
	addq	$20, %rdi
.L2878:
	movq	-48(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r15, %rdx
	movzbl	(%rdx,%r12), %esi
	cmpl	%ebx, %esi
	jne	.L2871
	movq	%r11, %r8
	subq	%rax, %r8
	je	.L2871
	cmpq	-88(%rbp), %r8
	ja	.L2871
	testq	%r14, %r14
	je	.L2872
	movq	%r14, -184(%rbp)
	movq	-72(%rbp), %r13
	xorl	%esi, %esi
.L2874:
	movq	0(%r13,%rsi), %rax
	movq	(%rdx,%rsi), %r9
	cmpq	%r9, %rax
	je	.L3086
	xorq	%r9, %rax
	movq	%r13, -72(%rbp)
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
.L2876:
	cmpq	$3, %rax
	jbe	.L2871
	leaq	(%rax,%rax,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%r8d, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	%r10, %rdx
	jbe	.L2871
	movq	-72(%rbp), %rbx
	movq	%r8, -80(%rbp)
	movq	%rax, %r12
	movq	%rdx, %r10
	movzbl	(%rbx,%rax), %ebx
	.p2align 4,,10
	.p2align 3
.L2871:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	%rdi, %rcx
	jne	.L2878
	movq	%r11, %rax
	movq	-120(%rbp), %rbx
	shrq	$3, %rax
	andl	$3, %eax
	addq	-160(%rbp), %rax
	movl	%r11d, 40(%rbx,%rax,4)
	movq	-64(%rbp), %rax
	addq	$175, %rax
	cmpq	%r10, %rax
	ja	.L2879
	addq	$1, -56(%rbp)
	cmpq	-208(%rbp), %r11
	je	.L2880
	movq	-128(%rbp), %rax
	addq	$1, -176(%rbp)
	addq	$9, %rax
	cmpq	%rax, -96(%rbp)
	ja	.L3087
.L2880:
	movq	-280(%rbp), %rax
	movq	-88(%rbp), %rbx
	addq	%r11, %rax
	leaq	(%rax,%r12,2), %rax
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rbx, %rax
	jbe	.L3088
	leaq	15(%rax), %rdx
.L2913:
	movq	-152(%rbp), %rax
	movl	-56(%rbp), %ebx
	movl	%r12d, 4(%rax)
	leaq	16(%rax), %r14
	movl	%ebx, (%rax)
	movq	-216(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r13
	cmpq	%rcx, %rdx
	jb	.L3089
	leal	2(%rdi), %ecx
	movl	$1, %ebx
	salq	%cl, %rbx
	movq	%rbx, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r13,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L2891:
	movq	-152(%rbp), %rbx
	cmpq	$5, -56(%rbp)
	movw	%ax, 14(%rbx)
	movslq	%r12d, %rax
	movl	%edx, 8(%rbx)
	movq	-56(%rbp), %rbx
	jbe	.L3090
	cmpq	$129, -56(%rbp)
	jbe	.L3091
	cmpq	$2113, -56(%rbp)
	jbe	.L3092
	cmpq	$6209, %rbx
	jbe	.L2948
	cmpq	$22594, %rbx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rbx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L2896
.L3100:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L3093
	.p2align 4,,10
	.p2align 3
.L2900:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L2902:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L2903
	.p2align 4,,10
	.p2align 3
.L2855:
	cmpq	$0, -88(%rbp)
	je	.L2854
	movzbl	-176(%rbp), %eax
	cmpb	(%rdx), %al
	jne	.L2854
	cmpq	$0, -160(%rbp)
	movq	-184(%rbp), %r12
	je	.L2854
	movzbl	1(%rsi), %eax
	movl	$1, %ebx
	cmpb	%al, 1(%rdx)
	jne	.L2854
.L2920:
	cmpq	$2, -88(%rbp)
	leaq	1(%rbx), %rax
	je	.L2859
	movzbl	1(%rbx,%rdx), %r9d
	cmpb	%r9b, 1(%r12)
	jne	.L2859
	cmpq	$3, -88(%rbp)
	leaq	2(%rbx), %rax
	je	.L2859
	movzbl	2(%rbx,%rdx), %r9d
	cmpb	%r9b, 2(%r12)
	jne	.L2859
	cmpq	$4, -88(%rbp)
	leaq	3(%rbx), %rax
	je	.L2859
	movzbl	3(%rbx,%rdx), %r9d
	cmpb	%r9b, 3(%r12)
	jne	.L2859
	cmpq	$5, -88(%rbp)
	leaq	4(%rbx), %rax
	je	.L2859
	movzbl	4(%rbx,%rdx), %r9d
	cmpb	%r9b, 4(%r12)
	jne	.L2859
	cmpq	$6, -88(%rbp)
	leaq	5(%rbx), %rax
	je	.L2859
	movzbl	5(%rbx,%rdx), %r9d
	cmpb	%r9b, 5(%r12)
	jne	.L2859
	cmpq	$7, -88(%rbp)
	leaq	6(%rbx), %rax
	je	.L2859
	movzbl	6(%r12), %r9d
	cmpb	%r9b, 6(%rbx,%rdx)
	jne	.L2859
	leaq	7(%rbx), %rax
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L2908:
	movq	-240(%rbp), %rbx
	leaq	9(%r13), %rdx
	cmpq	%rbx, %rdx
	movq	%rbx, %rcx
	cmovbe	%rdx, %rcx
	cmpq	%rax, %rcx
	jbe	.L2951
	movq	-56(%rbp), %rbx
	movq	-112(%rbp), %r8
	leaq	2(%rbx), %rdi
	movq	-120(%rbp), %rbx
	subq	%r13, %rdi
	.p2align 4,,10
	.p2align 3
.L2910:
	movabsq	$3866266742567714048, %rsi
	movq	%r8, %rdx
	andq	%rax, %rdx
	imulq	(%r15,%rdx), %rsi
	movq	%rsi, %rdx
	movq	%rax, %rsi
	shrq	$3, %rsi
	shrq	$44, %rdx
	andl	$3, %esi
	addq	%rsi, %rdx
	leaq	(%rdi,%rax), %rsi
	movl	%eax, 40(%rbx,%rdx,4)
	movq	%rax, %rdx
	addq	$2, %rax
	cmpq	%rcx, %rax
	jb	.L2910
	movq	%rsi, -56(%rbp)
	addq	$10, %rdx
	movq	%rax, %r13
	jmp	.L2905
	.p2align 4,,10
	.p2align 3
.L3083:
	movq	-168(%rbp), %rax
	leaq	(%rax,%r9), %rbx
	leaq	8(%r9), %rax
	subq	$1, %r12
	je	.L3094
	movq	%rax, %r9
	jmp	.L2857
	.p2align 4,,10
	.p2align 3
.L3082:
	testq	%r8, %r8
	je	.L2848
	movq	%r8, %rdi
	xorl	%ecx, %ecx
	leaq	8(%rsi), %r9
.L2850:
	movq	(%rsi,%rcx), %rdx
	movq	(%rax,%rcx), %r11
	cmpq	%r11, %rdx
	je	.L3095
	xorq	%r11, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	leaq	(%rax,%rcx), %r14
	movq	-88(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -160(%rbp)
.L2852:
	cmpq	$3, %r14
	jbe	.L2930
	leaq	(%r14,%r14,8), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	addq	$1935, %rax
	movq	%rax, -64(%rbp)
	cmpq	$2020, %rax
	jbe	.L2930
	movzbl	(%rsi,%r14), %r10d
	jmp	.L2847
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
	movq	$2020, -64(%rbp)
	jmp	.L2847
	.p2align 4,,10
	.p2align 3
.L3088:
	movq	16(%rbp), %rbx
	movslq	(%rbx), %rdx
	movslq	4(%rbx), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rdx, %rax
	je	.L2883
	cmpq	%rcx, %rax
	je	.L3096
	addq	$3, %rax
	movq	%rax, %rbx
	subq	%rdx, %rbx
	cmpq	$6, %rbx
	jbe	.L3097
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L3098
	movq	16(%rbp), %rax
	movq	-80(%rbp), %rbx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rdx, %rbx
	je	.L2946
	movq	16(%rbp), %rcx
	movslq	12(%rcx), %rdx
	cmpq	%rdx, %rbx
	je	.L3099
	leaq	15(%rbx), %rdx
.L2887:
	testq	%rdx, %rdx
	je	.L2883
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L2885:
	movq	16(%rbp), %rbx
	movl	%eax, 12(%rbx)
	movq	%rbx, %rax
	movl	%edi, 8(%rbx)
	movl	%esi, 4(%rbx)
	movl	-80(%rbp), %ebx
	movl	%ebx, (%rax)
	jmp	.L2913
	.p2align 4,,10
	.p2align 3
.L3090:
	leal	0(,%rbx,8), %edi
	movl	%ebx, %edx
	andl	$56, %edi
.L2893:
	cmpq	$9, %rax
	jbe	.L3100
.L2896:
	cmpq	$133, %rax
	jbe	.L3101
	cmpq	$2117, %rax
	ja	.L2899
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L2897:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L2900
.L3093:
	testb	%sil, %sil
	je	.L2900
	cmpw	$15, %ax
	ja	.L2900
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L2903:
	movq	-152(%rbp), %rax
	movq	-56(%rbp), %rbx
	leaq	(%r11,%r12), %r13
	movq	%r12, %rdx
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	addq	%rbx, (%rax)
	movq	-272(%rbp), %rbx
	leaq	2(%r11), %rax
	cmpq	%rbx, %r13
	movq	%rbx, %rcx
	movq	-80(%rbp), %rbx
	cmovbe	%r13, %rcx
	shrq	$2, %rdx
	cmpq	%rdx, %rbx
	jnb	.L2904
	movq	%rbx, %rdx
	movq	%r13, %rbx
	salq	$2, %rdx
	subq	%rdx, %rbx
	cmpq	%rax, %rbx
	cmovnb	%rbx, %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
.L2904:
	leaq	8(%r13), %rdx
	cmpq	%rcx, %rax
	jnb	.L2950
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2906:
	movabsq	$3866266742567714048, %rdi
	movq	%r8, %rsi
	andq	%rax, %rsi
	imulq	(%r15,%rsi), %rdi
	movq	%rdi, %rsi
	movq	%rax, %rdi
	shrq	$3, %rdi
	shrq	$44, %rsi
	andl	$3, %edi
	addq	%rdi, %rsi
	movl	%eax, 40(%rbx,%rsi,4)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L2906
.L2950:
	movq	%r14, -152(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L2905
	.p2align 4,,10
	.p2align 3
.L2883:
	movq	-152(%rbp), %rax
	movl	-56(%rbp), %ebx
	movl	$1, %esi
	xorl	%edx, %edx
	movl	%ebx, (%rax)
	leaq	16(%rax), %r14
	movl	%r12d, 4(%rax)
	xorl	%eax, %eax
	jmp	.L2891
	.p2align 4,,10
	.p2align 3
.L3087:
	movq	-80(%rbp), %rax
	movq	-200(%rbp), %rdx
	movq	%r10, -64(%rbp)
	movq	%r12, -224(%rbp)
	movq	%rax, -104(%rbp)
	movq	%r11, -128(%rbp)
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2872:
	cmpq	$0, -168(%rbp)
	je	.L2871
	movq	-72(%rbp), %rsi
	movzbl	(%rdx), %eax
	cmpb	%al, (%rsi)
	jne	.L2871
	movq	-264(%rbp), %rax
	cmpq	$0, -248(%rbp)
	movq	%rax, -184(%rbp)
	je	.L2871
	movzbl	1(%rsi), %eax
	movl	$1, %r13d
	cmpb	%al, 1(%rdx)
	jne	.L2871
.L2918:
	cmpq	$2, -168(%rbp)
	leaq	1(%r13), %rax
	je	.L2876
	movq	-184(%rbp), %r9
	movzbl	1(%r13,%rdx), %esi
	cmpb	%sil, 1(%r9)
	jne	.L2876
	cmpq	$3, -168(%rbp)
	leaq	2(%r13), %rax
	je	.L2876
	movzbl	2(%r13,%rdx), %esi
	cmpb	%sil, 2(%r9)
	jne	.L2876
	cmpq	$4, -168(%rbp)
	leaq	3(%r13), %rax
	je	.L2876
	movzbl	3(%r13,%rdx), %esi
	cmpb	%sil, 3(%r9)
	jne	.L2876
	cmpq	$5, -168(%rbp)
	leaq	4(%r13), %rax
	je	.L2876
	movzbl	4(%r13,%rdx), %esi
	cmpb	%sil, 4(%r9)
	jne	.L2876
	cmpq	$6, -168(%rbp)
	leaq	5(%r13), %rax
	je	.L2876
	movzbl	5(%r13,%rdx), %esi
	cmpb	%sil, 5(%r9)
	jne	.L2876
	cmpq	$7, -168(%rbp)
	leaq	6(%r13), %rax
	je	.L2876
	movzbl	6(%r9), %esi
	cmpb	%sil, 6(%r13,%rdx)
	jne	.L2876
	leaq	7(%r13), %rax
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L3086:
	subq	$1, -184(%rbp)
	movq	-256(%rbp), %rax
	leaq	(%rax,%rsi), %r9
	leaq	8(%rsi), %rax
	je	.L3102
	movq	%rax, %rsi
	jmp	.L2874
.L3097:
	leal	0(,%rbx,4), %ecx
	movl	$158663784, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L2887
	.p2align 4,,10
	.p2align 3
.L3101:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L2897
	.p2align 4,,10
	.p2align 3
.L3085:
	testq	%r14, %r14
	je	.L2865
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	8(%rsi), %r8
.L2867:
	movq	(%rsi,%rcx), %rax
	movq	(%rdx,%rcx), %r9
	cmpq	%r9, %rax
	je	.L3103
	xorq	%r9, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rcx, %rax
	movq	-168(%rbp), %rcx
	subq	$1, %rcx
	movq	%rcx, -248(%rbp)
.L2869:
	cmpq	$3, %rax
	jbe	.L2940
	leaq	(%rax,%rax,8), %rdx
	movq	%rdx, %r10
	salq	$4, %r10
	subq	%rdx, %r10
	addq	$1935, %r10
	cmpq	$2020, %r10
	ja	.L3104
.L2940:
	movq	$0, -80(%rbp)
.L3080:
	movl	$2020, %r10d
	jmp	.L2864
	.p2align 4,,10
	.p2align 3
.L3091:
	leaq	-2(%rbx), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L2893
.L2899:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L2902
.L3092:
	leaq	-66(%rbx), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L2893
.L2948:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L2893
.L3104:
	movq	-288(%rbp), %rcx
	movzbl	(%rsi,%rax), %ebx
	movq	%rax, %r12
	movq	%rcx, -80(%rbp)
	jmp	.L2864
.L3102:
	cmpq	$0, -168(%rbp)
	movq	%r9, -184(%rbp)
	movq	%r13, -72(%rbp)
	je	.L2876
	movzbl	(%rdx,%rax), %r13d
	cmpb	%r13b, (%r9)
	jne	.L2876
	movq	%r9, %rax
	cmpq	$0, -248(%rbp)
	leaq	1(%r9), %r9
	movq	%r9, -184(%rbp)
	leaq	9(%rsi), %r13
	je	.L2943
	movzbl	9(%rsi,%rdx), %esi
	cmpb	%sil, 1(%rax)
	je	.L2918
.L2943:
	movq	%r13, %rax
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L3096:
	movl	8(%rbx), %eax
	movl	$1, %edx
	jmp	.L2885
.L2951:
	movq	%rsi, -56(%rbp)
	movq	%rax, %r13
	jmp	.L2905
.L3098:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L2887
.L3094:
	cmpq	$0, -88(%rbp)
	je	.L2859
	movzbl	(%rdx,%rax), %r12d
	cmpb	%r12b, (%rbx)
	jne	.L2859
	cmpq	$0, -160(%rbp)
	movq	%rbx, %rax
	leaq	1(%rbx), %r12
	leaq	9(%r9), %rbx
	je	.L2933
	movzbl	9(%r9,%rdx), %r9d
	cmpb	%r9b, 1(%rax)
	je	.L2920
.L2933:
	movq	%rbx, %rax
	jmp	.L2859
.L2848:
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L2928
	subq	$1, %rbx
	leaq	1(%rsi), %rdi
	movq	%rbx, -160(%rbp)
	je	.L2930
	movzbl	1(%rsi), %ebx
	movl	$1, %edx
	cmpb	%bl, 1(%rax)
	jne	.L2930
.L2921:
	movq	-160(%rbp), %rbx
	leaq	1(%rdx), %r14
	cmpq	$1, %rbx
	je	.L2852
	movzbl	1(%rax,%rdx), %ecx
	cmpb	%cl, 1(%rdi)
	jne	.L2852
	leaq	2(%rdx), %r14
	cmpq	$2, %rbx
	je	.L2852
	movzbl	2(%rax,%rdx), %ecx
	cmpb	%cl, 2(%rdi)
	jne	.L2852
	leaq	3(%rdx), %r14
	cmpq	$3, %rbx
	je	.L2852
	movzbl	3(%rax,%rdx), %ecx
	cmpb	%cl, 3(%rdi)
	jne	.L2852
	subq	$4, %rbx
	leaq	4(%rdx), %r14
	movq	%rbx, %rcx
	je	.L2852
	movzbl	4(%rax,%rdx), %ebx
	cmpb	%bl, 4(%rdi)
	jne	.L2852
	leaq	5(%rdx), %r14
	cmpq	$1, %rcx
	je	.L2852
	movzbl	5(%rax,%rdx), %eax
	cmpb	%al, 5(%rdi)
	jne	.L2852
	leaq	6(%rdx), %r14
	jmp	.L2852
.L3095:
	leaq	(%r9,%rcx), %rdx
	leaq	8(%rcx), %r14
	subq	$1, %rdi
	je	.L3105
	movq	%r14, %rcx
	jmp	.L2850
.L2924:
	xorl	%edx, %edx
	jmp	.L2846
.L2879:
	movq	-128(%rbp), %r13
	movq	-136(%rbp), %rax
	movq	-224(%rbp), %r14
	cmpq	%rax, %r13
	movq	%r13, %r11
	cmovbe	%r13, %rax
	movq	%r14, %r12
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2880
.L3103:
	leaq	(%r8,%rcx), %r9
	leaq	8(%rcx), %rax
	subq	$1, %rdi
	je	.L3106
	movq	%rax, %rcx
	jmp	.L2867
.L2865:
	cmpq	$0, -168(%rbp)
	je	.L2938
	movzbl	(%rdx), %eax
	cmpb	%al, (%rsi)
	jne	.L2936
	movq	-168(%rbp), %rax
	leaq	1(%rsi), %rdi
	subq	$1, %rax
	movq	%rax, -248(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3080
	movzbl	1(%rdx), %eax
	movl	$1, %ecx
	cmpb	%al, 1(%rsi)
	jne	.L2940
.L2919:
	movq	-248(%rbp), %r10
	leaq	1(%rcx), %rax
	cmpq	$1, %r10
	je	.L2869
	movzbl	1(%rcx,%rdx), %r8d
	cmpb	%r8b, 1(%rdi)
	jne	.L2869
	leaq	2(%rcx), %rax
	cmpq	$2, %r10
	je	.L2869
	movzbl	2(%rcx,%rdx), %r8d
	cmpb	%r8b, 2(%rdi)
	jne	.L2869
	leaq	3(%rcx), %rax
	cmpq	$3, %r10
	je	.L2869
	movzbl	3(%rcx,%rdx), %r8d
	cmpb	%r8b, 3(%rdi)
	jne	.L2869
	subq	$4, %r10
	leaq	4(%rcx), %rax
	movq	%r10, %r8
	je	.L2869
	movzbl	4(%rcx,%rdx), %r10d
	cmpb	%r10b, 4(%rdi)
	jne	.L2869
	leaq	5(%rcx), %rax
	cmpq	$1, %r8
	je	.L2869
	movzbl	5(%rcx,%rdx), %edx
	cmpb	%dl, 5(%rdi)
	jne	.L2869
	leaq	6(%rcx), %rax
	jmp	.L2869
.L2946:
	movl	$2, %edx
	jmp	.L2885
.L3099:
	movl	$3, %edx
	jmp	.L2885
.L3105:
	cmpq	$0, -88(%rbp)
	je	.L3107
	movzbl	(%rax,%r14), %ebx
	cmpb	%bl, (%rdx)
	jne	.L3108
	movq	-88(%rbp), %rbx
	leaq	1(%rdx), %rdi
	leaq	9(%rcx), %r14
	subq	$1, %rbx
	movq	%rbx, -160(%rbp)
	je	.L2852
	movzbl	9(%rax,%rcx), %ebx
	cmpb	%bl, 1(%rdx)
	jne	.L2852
	movq	%r14, %rdx
	jmp	.L2921
.L2928:
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
	movq	$2020, -64(%rbp)
	movq	$-1, -160(%rbp)
	jmp	.L2847
.L2938:
	movq	$0, -80(%rbp)
	movl	$2020, %r10d
	movq	$-1, -248(%rbp)
	jmp	.L2864
.L3106:
	cmpq	$0, -168(%rbp)
	je	.L3109
	movzbl	(%rdx,%rax), %edi
	cmpb	%dil, (%r9)
	jne	.L3110
	movq	-168(%rbp), %r10
	leaq	1(%r9), %rdi
	leaq	9(%rcx), %rax
	subq	$1, %r10
	movq	%r10, -248(%rbp)
	je	.L2869
	movzbl	9(%rcx,%rdx), %ecx
	cmpb	%cl, 1(%r9)
	jne	.L2869
	movq	%rax, %rcx
	jmp	.L2919
.L3089:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L2891
.L3110:
	movq	-168(%rbp), %rcx
	subq	$1, %rcx
	movq	%rcx, -248(%rbp)
	jmp	.L2869
.L3109:
	movq	$-1, -248(%rbp)
	jmp	.L2869
.L3108:
	movq	-88(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -160(%rbp)
	jmp	.L2852
.L3107:
	movq	$-1, -160(%rbp)
	jmp	.L2852
	.cfi_endproc
.LFE288:
	.size	CreateBackwardReferencesNH54, .-CreateBackwardReferencesNH54
	.p2align 4
	.type	CreateBackwardReferencesNH35, @function
CreateBackwardReferencesNH35:
.LFB289:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$256, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$160, %rsp
	movq	%r8, -256(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	leaq	(%rsi,%rdi), %rcx
	subq	$16, %rax
	cmpq	$7, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, -144(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	leaq	-7(%rcx), %rax
	cmovbe	%rsi, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -272(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -264(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movq	%rax, -280(%rbp)
	addq	%rsi, %rax
	movq	%rax, -136(%rbp)
	leaq	8(%rsi), %rax
	cmpq	%rax, %rcx
	jbe	.L3208
	movq	40(%r9), %r15
	movq	48(%r9), %r11
	movq	%r12, %r13
	movq	%r10, %rbx
	movq	%rsi, %r12
	leaq	40(%r15), %rax
	movq	%rax, -160(%rbp)
	movl	%r10d, %eax
	movq	%rax, -72(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L3195:
	movq	-144(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rbx, %rcx
	movabsq	$-4819355556693147648, %r14
	subq	%r12, %rdi
	cmpq	%r12, %rax
	cmova	%r12, %rax
	andq	%r12, %rcx
	movq	%rdi, -128(%rbp)
	addq	%r13, %rcx
	imulq	(%rcx), %r14
	movzbl	(%rcx), %r10d
	movq	%rax, -88(%rbp)
	movq	16(%rbp), %rax
	movb	%r10b, -192(%rbp)
	movslq	(%rax), %rax
	shrq	$48, %r14
	movq	%rax, %rsi
	movq	%rax, -104(%rbp)
	movq	%r12, %rax
	subq	%rsi, %rax
	movq	%rdi, %rsi
	andl	$7, %edi
	shrq	$3, %rsi
	movq	%rsi, -96(%rbp)
	cmpq	%r12, %rax
	jnb	.L3210
	andl	%ebx, %eax
	addq	%r13, %rax
	cmpb	%r10b, (%rax)
	je	.L3432
.L3210:
	leaq	-1(%rdi), %rax
	movq	$0, -104(%rbp)
	movq	$0, -48(%rbp)
	movq	$2020, -64(%rbp)
	movq	%rax, -176(%rbp)
.L3115:
	leaq	-2(%rdi), %rdx
	movq	-160(%rbp), %rax
	movq	%r11, -120(%rbp)
	movq	%rdx, -200(%rbp)
	leaq	-3(%rdi), %rdx
	movq	%rdx, -208(%rbp)
	leaq	-4(%rdi), %rdx
	leaq	(%rax,%r14,4), %r8
	movq	%rdx, -224(%rbp)
	leaq	-5(%rdi), %rdx
	movl	(%r8), %eax
	leaq	4(%r8), %rsi
	movq	%rdx, -232(%rbp)
	leaq	-6(%rdi), %rdx
	addq	$12, %r8
	movq	%rdx, -240(%rbp)
	leaq	-7(%rdi), %rdx
	movq	%r8, %r11
	movq	%rdx, -248(%rbp)
	leaq	1(%rcx), %rdx
	movq	%rdx, -216(%rbp)
	leaq	8(%rcx), %rdx
	movq	%rdx, -184(%rbp)
.L3129:
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %r9
	andq	%rax, %rdx
	addq	%r13, %rdx
	movzbl	(%rdx,%r9), %r8d
	cmpl	%r10d, %r8d
	jne	.L3122
	movq	%r12, %r8
	subq	%rax, %r8
	movq	%r8, -152(%rbp)
	je	.L3122
	movq	-88(%rbp), %rax
	cmpq	%rax, %r8
	ja	.L3122
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L3123
	movq	%rax, -112(%rbp)
	xorl	%r8d, %r8d
.L3125:
	movq	(%rcx,%r8), %rax
	movq	(%rdx,%r8), %r9
	cmpq	%r9, %rax
	je	.L3433
	xorq	%r9, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%r8, %rax
	movq	%rax, -112(%rbp)
.L3127:
	movq	-112(%rbp), %rax
	cmpq	$3, %rax
	jbe	.L3122
	leaq	(%rax,%rax,8), %r8
	movq	-152(%rbp), %r9
	movq	%r8, %rdx
	salq	$4, %rdx
	subq	%r8, %rdx
	bsrl	%r9d, %r8d
	imull	$30, %r8d, %r8d
	addq	$1920, %rdx
	subq	%r8, %rdx
	cmpq	-64(%rbp), %rdx
	jbe	.L3122
	movq	%r9, -104(%rbp)
	movzbl	(%rcx,%rax), %r10d
	movq	%rax, -48(%rbp)
	movq	%rdx, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L3122:
	addq	$4, %rsi
	movl	-4(%rsi), %eax
	cmpq	%r11, %rsi
	jne	.L3129
	movq	%r12, %rax
	movq	-120(%rbp), %r11
	movl	%r12d, %r8d
	shrq	$3, %rax
	andl	$1, %eax
	movq	56(%r11), %r9
	addq	%rax, %r14
	movl	%r12d, 40(%r15,%r14,4)
	testb	$3, %r12b
	jne	.L3130
	cmpq	$31, -128(%rbp)
	jbe	.L3130
	cmpq	%r12, %r9
	ja	.L3131
	leaq	8(%rcx), %rax
	movq	%rdi, %r14
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L3137:
	leaq	32(%r9), %rax
	movl	40(%r11), %edx
	andq	%rbx, %rax
	movzbl	0(%r13,%rax), %esi
	movl	68(%r11), %eax
	imull	%edx, %eax
	leal	1(%rsi,%rax), %esi
	movq	%rbx, %rax
	andq	%r9, %rax
	movzbl	0(%r13,%rax), %eax
	addl	$1, %eax
	imull	72(%r11), %eax
	subl	%eax, %esi
	movl	%esi, 40(%r11)
	testl	$1056964608, %edx
	jne	.L3132
	movq	48(%r11), %rax
	andl	$1073741823, %edx
	leaq	(%rax,%rdx,4), %rax
	movl	(%rax), %edi
	movl	%r9d, (%rax)
	cmpq	%r9, %r12
	jne	.L3132
	movl	$4294967295, %eax
	cmpq	%rax, %rdi
	je	.L3132
	movl	%r8d, %eax
	subl	%edi, %eax
	movl	%eax, %esi
	movq	%rsi, %rdx
	cmpq	-88(%rbp), %rsi
	jbe	.L3434
	.p2align 4,,10
	.p2align 3
.L3132:
	addq	$4, %r9
	cmpq	%r12, %r9
	jbe	.L3137
.L3131:
	leaq	4(%r12), %r9
	movq	%r9, 56(%r11)
.L3130:
	cmpq	$2020, -64(%rbp)
	jne	.L3435
	movq	-56(%rbp), %rcx
	leaq	1(%r12), %rax
	leaq	1(%rcx), %rdx
	cmpq	%rax, -136(%rbp)
	jnb	.L3431
	movq	-80(%rbp), %rdi
	movq	-136(%rbp), %rsi
	addq	-264(%rbp), %rsi
	leaq	-7(%rdi), %rcx
	cmpq	%rax, %rsi
	jnb	.L3192
	movabsq	$-4819355556693147648, %r8
	leaq	17(%r12), %rsi
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	-56(%rbp), %rsi
	addq	$4, %rsi
	subq	%r12, %rsi
	cmpq	%rax, %rcx
	jbe	.L3431
	.p2align 4,,10
	.p2align 3
.L3193:
	movq	%rbx, %rdx
	andq	%rax, %rdx
	movq	0(%r13,%rdx), %rdi
	imulq	%r8, %rdi
	movq	%rdi, %rdx
	movq	%rax, %rdi
	shrq	$3, %rdi
	shrq	$48, %rdx
	andl	$1, %edi
	addq	%rdi, %rdx
	movq	%rax, %rdi
	movl	%eax, 40(%r15,%rdx,4)
	leaq	(%rsi,%rax), %rdx
	addq	$4, %rax
	cmpq	%rcx, %rax
	jb	.L3193
	movq	%rdx, -56(%rbp)
	addq	$12, %rdi
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L3189:
	cmpq	%rdi, -80(%rbp)
	ja	.L3195
	movq	-168(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r12, %rbx
	sarq	$4, %rdx
.L3114:
	movq	-80(%rbp), %rax
	addq	-56(%rbp), %rax
	subq	%rbx, %rax
	movq	24(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3431:
	.cfi_restore_state
	leaq	9(%r12), %rdi
	movq	%rdx, -56(%rbp)
	movq	%rax, %r12
	jmp	.L3189
	.p2align 4,,10
	.p2align 3
.L3435:
	movq	-128(%rbp), %rax
	movq	%r15, -176(%rbp)
	movq	%r12, -120(%rbp)
	subq	$1, %rax
	movq	%rax, -112(%rbp)
	leaq	4(%r12), %rax
	movq	%rax, -200(%rbp)
.L3165:
	movq	-256(%rbp), %rax
	xorl	%r10d, %r10d
	cmpl	$4, 4(%rax)
	jg	.L3139
	movq	-48(%rbp), %rax
	movq	-112(%rbp), %rcx
	subq	$1, %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	movq	%rax, %r10
.L3139:
	movq	-120(%rbp), %rax
	leaq	1(%rax), %r14
	movq	-144(%rbp), %rax
	cmpq	%rax, %r14
	cmovbe	%r14, %rax
	movq	%rax, -96(%rbp)
	movq	%r14, %rax
	andq	%rbx, %rax
	leaq	0(%r13,%rax), %r15
	movabsq	$-4819355556693147648, %rax
	imulq	(%r15), %rax
	movzbl	(%r15,%r10), %r8d
	shrq	$48, %rax
	movq	%rax, -152(%rbp)
	movq	16(%rbp), %rax
	movslq	(%rax), %rax
	movq	%rax, %rcx
	movq	%rax, -88(%rbp)
	movq	%r14, %rax
	subq	%rcx, %rax
	movq	-112(%rbp), %rcx
	movq	%rcx, %rdi
	andl	$7, %ecx
	shrq	$3, %rdi
	movq	%rcx, %rsi
	movq	%rdi, -184(%rbp)
	cmpq	%rax, %r14
	jbe	.L3223
	andq	-72(%rbp), %rax
	addq	%r13, %rax
	cmpb	(%rax,%r10), %r8b
	je	.L3436
.L3223:
	leaq	-1(%rsi), %rax
	movq	$0, -88(%rbp)
	movq	$2020, -128(%rbp)
	movq	%rax, -216(%rbp)
.L3140:
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %rcx
	movl	%r8d, %r12d
	leaq	(%rax,%rcx,4), %rdx
	leaq	12(%rdx), %rdi
	movl	(%rdx), %eax
	leaq	4(%rdx), %rcx
	movq	%rdi, -136(%rbp)
	leaq	-2(%rsi), %rdi
	movq	%rdi, -232(%rbp)
	leaq	-3(%rsi), %rdi
	movq	%rdi, -248(%rbp)
	leaq	-4(%rsi), %rdi
	movq	%rdi, -296(%rbp)
	leaq	-5(%rsi), %rdi
	movq	%rdi, -288(%rbp)
	leaq	-6(%rsi), %rdi
	movq	%rdi, -304(%rbp)
	leaq	-7(%rsi), %rdi
	movq	%rdi, -312(%rbp)
	leaq	1(%r15), %rdi
	movq	%rdi, -240(%rbp)
	leaq	8(%r15), %rdi
	movq	%rdi, -224(%rbp)
.L3154:
	movq	-72(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r13, %rdx
	movzbl	(%rdx,%r10), %edi
	cmpl	%r12d, %edi
	jne	.L3147
	movq	%r14, %rdi
	subq	%rax, %rdi
	movq	%rdi, -208(%rbp)
	je	.L3147
	movq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	ja	.L3147
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L3148
	movq	%rax, -192(%rbp)
	xorl	%edi, %edi
.L3150:
	movq	(%r15,%rdi), %rax
	movq	(%rdx,%rdi), %r8
	cmpq	%r8, %rax
	je	.L3437
	xorq	%r8, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rdi, %rax
	movq	%rax, -192(%rbp)
.L3152:
	movq	-192(%rbp), %rax
	cmpq	$3, %rax
	jbe	.L3147
	leaq	(%rax,%rax,8), %rdi
	movq	-208(%rbp), %r8
	movq	%rdi, %rdx
	salq	$4, %rdx
	subq	%rdi, %rdx
	bsrl	%r8d, %edi
	imull	$30, %edi, %edi
	addq	$1920, %rdx
	subq	%rdi, %rdx
	cmpq	-128(%rbp), %rdx
	jbe	.L3147
	movq	%r8, -88(%rbp)
	movzbl	(%r15,%rax), %r12d
	movq	%rax, %r10
	movq	%rdx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L3147:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	-136(%rbp), %rcx
	jne	.L3154
	movq	%r14, %rax
	movq	-176(%rbp), %rcx
	movl	%r14d, %r12d
	shrq	$3, %rax
	andl	$1, %eax
	addq	-152(%rbp), %rax
	movl	%r14d, 40(%rcx,%rax,4)
	testb	$3, %r14b
	jne	.L3155
	cmpq	$31, -112(%rbp)
	jbe	.L3155
	cmpq	%r9, %r14
	jb	.L3156
	leaq	8(%r15), %rax
	movq	%rsi, %r8
	movq	%rax, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L3162:
	leaq	32(%r9), %rax
	movl	40(%r11), %edx
	andq	%rbx, %rax
	movzbl	0(%r13,%rax), %ecx
	movl	68(%r11), %eax
	imull	%edx, %eax
	leal	1(%rcx,%rax), %ecx
	movq	%rbx, %rax
	andq	%r9, %rax
	movzbl	0(%r13,%rax), %eax
	addl	$1, %eax
	imull	72(%r11), %eax
	subl	%eax, %ecx
	movl	%ecx, 40(%r11)
	testl	$1056964608, %edx
	jne	.L3157
	movq	48(%r11), %rax
	andl	$1073741823, %edx
	leaq	(%rax,%rdx,4), %rax
	movl	(%rax), %esi
	movl	%r9d, (%rax)
	cmpq	%r9, %r14
	jne	.L3157
	movl	$4294967295, %eax
	cmpq	%rax, %rsi
	je	.L3157
	movl	%r12d, %eax
	subl	%esi, %eax
	movl	%eax, %ecx
	movq	%rcx, %rdx
	cmpq	-96(%rbp), %rcx
	jbe	.L3438
	.p2align 4,,10
	.p2align 3
.L3157:
	addq	$4, %r9
	cmpq	%r9, %r14
	jnb	.L3162
.L3156:
	movq	-120(%rbp), %rax
	addq	$5, %rax
	movq	%rax, 56(%r11)
.L3155:
	movq	-64(%rbp), %rax
	addq	$175, %rax
	cmpq	-128(%rbp), %rax
	ja	.L3163
	addq	$1, -56(%rbp)
	cmpq	-200(%rbp), %r14
	je	.L3423
	movq	-120(%rbp), %rax
	addq	$9, %rax
	cmpq	%rax, -80(%rbp)
	ja	.L3439
.L3423:
	movq	%r10, -48(%rbp)
	movq	-176(%rbp), %r15
	movq	%r10, %rsi
.L3164:
	movq	-280(%rbp), %rax
	addq	%r14, %rax
	leaq	(%rax,%rsi,2), %rax
	movq	-96(%rbp), %rsi
	movq	%rax, -136(%rbp)
	movq	-88(%rbp), %rax
	cmpq	%rsi, %rax
	jbe	.L3440
	leaq	15(%rax), %rdx
.L3197:
	movq	-168(%rbp), %rax
	movl	-56(%rbp), %ecx
	leaq	16(%rax), %rsi
	movl	%ecx, (%rax)
	movq	%rsi, -64(%rbp)
	movl	-48(%rbp), %esi
	movl	%esi, 4(%rax)
	movq	-256(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r8
	movq	%rax, %r12
	cmpq	%rcx, %rdx
	jb	.L3441
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r12,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L3175:
	movq	-168(%rbp), %rcx
	cmpq	$5, -56(%rbp)
	movw	%ax, 14(%rcx)
	movslq	-48(%rbp), %rax
	movl	%edx, 8(%rcx)
	movq	-56(%rbp), %rcx
	jbe	.L3442
	cmpq	$129, -56(%rbp)
	jbe	.L3443
	cmpq	$2113, -56(%rbp)
	jbe	.L3444
	cmpq	$6209, %rcx
	jbe	.L3235
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L3180
.L3452:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L3445
	.p2align 4,,10
	.p2align 3
.L3184:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L3186:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L3187
	.p2align 4,,10
	.p2align 3
.L3434:
	andq	%rbx, %rdi
	leaq	0(%r13,%rdi), %rax
	xorl	%edi, %edi
	movq	%rax, -120(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
.L3134:
	movq	-120(%rbp), %r10
	movq	(%rcx,%rdi), %rax
	movq	(%r10,%rdi), %r10
	cmpq	%r10, %rax
	je	.L3446
	xorq	%r10, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rdi, %rax
	movq	%rax, -112(%rbp)
.L3136:
	movq	-48(%rbp), %r10
	movl	$3, %edi
	cmpq	$3, %r10
	cmovnb	%r10, %rdi
	cmpq	-112(%rbp), %rdi
	jnb	.L3132
	movq	-112(%rbp), %rax
	bsrl	%edx, %edx
	imull	$30, %edx, %edx
	leaq	(%rax,%rax,8), %r10
	movq	%r10, %rdi
	salq	$4, %rdi
	subq	%r10, %rdi
	addq	$1920, %rdi
	subq	%rdx, %rdi
	cmpq	-64(%rbp), %rdi
	jbe	.L3132
	movq	%rdi, -64(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rax, -48(%rbp)
	jmp	.L3132
	.p2align 4,,10
	.p2align 3
.L3192:
	leaq	9(%r12), %rdi
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	cmpq	%rax, %rcx
	jbe	.L3238
	movabsq	$-4819355556693147648, %r8
	movq	-56(%rbp), %rsi
	leaq	2(%rsi), %r9
	subq	%r12, %r9
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	%rbx, %rdx
	movq	%rax, %rsi
	andq	%rax, %rdx
	shrq	$3, %rsi
	movq	0(%r13,%rdx), %rdi
	andl	$1, %esi
	imulq	%r8, %rdi
	movq	%rdi, %rdx
	movq	%rax, %rdi
	shrq	$48, %rdx
	addq	%rsi, %rdx
	movl	%eax, 40(%r15,%rdx,4)
	leaq	(%r9,%rax), %rdx
	addq	$2, %rax
	cmpq	%rcx, %rax
	jb	.L3194
	movq	%rdx, -56(%rbp)
	addq	$10, %rdi
	movq	%rax, %r12
	jmp	.L3189
	.p2align 4,,10
	.p2align 3
.L3432:
	movq	%rsi, %r9
	testq	%rsi, %rsi
	je	.L3116
	leaq	8(%rcx), %rdx
	xorl	%esi, %esi
	movq	%rdx, -48(%rbp)
.L3118:
	movq	(%rcx,%rsi), %rdx
	movq	(%rax,%rsi), %r8
	cmpq	%r8, %rdx
	je	.L3447
	xorq	%r8, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
	movq	%rax, -48(%rbp)
	leaq	-1(%rdi), %rax
	movq	%rax, -176(%rbp)
.L3120:
	movq	-48(%rbp), %rsi
	cmpq	$3, %rsi
	jbe	.L3216
	leaq	(%rsi,%rsi,8), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	addq	$1935, %rax
	movq	%rax, -64(%rbp)
	cmpq	$2020, %rax
	jbe	.L3216
	movzbl	(%rcx,%rsi), %r10d
	jmp	.L3115
	.p2align 4,,10
	.p2align 3
.L3216:
	movq	$0, -104(%rbp)
	movq	$0, -48(%rbp)
	movq	$2020, -64(%rbp)
	jmp	.L3115
	.p2align 4,,10
	.p2align 3
.L3440:
	movq	16(%rbp), %rsi
	movq	16(%rbp), %rcx
	movslq	(%rsi), %rdx
	movslq	4(%rcx), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rax, %rdx
	je	.L3167
	cmpq	%rax, %rcx
	je	.L3448
	addq	$3, %rax
	movq	%rax, %r10
	subq	%rdx, %r10
	cmpq	$6, %r10
	jbe	.L3449
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L3450
	movq	16(%rbp), %rax
	movq	-88(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	je	.L3233
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3451
	leaq	15(%rcx), %rdx
.L3171:
	testq	%rdx, %rdx
	je	.L3167
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L3169:
	movq	16(%rbp), %rcx
	movl	%esi, 4(%rcx)
	movl	-88(%rbp), %esi
	movl	%eax, 12(%rcx)
	movl	%edi, 8(%rcx)
	movl	%esi, (%rcx)
	jmp	.L3197
	.p2align 4,,10
	.p2align 3
.L3442:
	movl	%ecx, %edi
	movl	%ecx, %edx
	sall	$3, %edi
	andl	$56, %edi
.L3177:
	cmpq	$9, %rax
	jbe	.L3452
.L3180:
	cmpq	$133, %rax
	jbe	.L3453
	cmpq	$2117, %rax
	ja	.L3183
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L3181:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L3184
.L3445:
	testb	%sil, %sil
	je	.L3184
	cmpw	$15, %ax
	ja	.L3184
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L3187:
	movq	-168(%rbp), %rax
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	leaq	(%r14,%rcx), %r12
	movq	-272(%rbp), %rdi
	addq	%rsi, (%rax)
	leaq	2(%r14), %rax
	cmpq	%rdi, %r12
	movq	%rdi, %rsi
	cmovbe	%r12, %rsi
	shrq	$2, %rcx
	movq	%rcx, %rdx
	movq	-88(%rbp), %rcx
	cmpq	%rcx, %rdx
	jbe	.L3188
	movq	%rcx, %rdx
	movq	%r12, %rcx
	salq	$2, %rdx
	subq	%rdx, %rcx
	cmpq	%rax, %rcx
	cmovnb	%rcx, %rax
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
.L3188:
	leaq	8(%r12), %rdi
	cmpq	%rsi, %rax
	jnb	.L3237
	movabsq	$-4819355556693147648, %r8
	.p2align 4,,10
	.p2align 3
.L3190:
	movq	%rbx, %rdx
	andq	%rax, %rdx
	movq	0(%r13,%rdx), %rcx
	imulq	%r8, %rcx
	movq	%rcx, %rdx
	movq	%rax, %rcx
	shrq	$3, %rcx
	shrq	$48, %rdx
	andl	$1, %ecx
	addq	%rcx, %rdx
	movl	%eax, 40(%r15,%rdx,4)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L3190
.L3237:
	movq	-64(%rbp), %rax
	movq	$0, -56(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L3189
	.p2align 4,,10
	.p2align 3
.L3167:
	movq	-168(%rbp), %rax
	xorl	%edx, %edx
	leaq	16(%rax), %rsi
	movq	%rsi, -64(%rbp)
	movl	-56(%rbp), %esi
	movl	%esi, (%rax)
	movl	-48(%rbp), %esi
	movl	%esi, 4(%rax)
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3123:
	testq	%rdi, %rdi
	je	.L3122
	movzbl	-192(%rbp), %eax
	cmpb	(%rdx), %al
	jne	.L3122
	movq	-216(%rbp), %rax
	cmpq	$0, -176(%rbp)
	movq	%rax, -288(%rbp)
	je	.L3122
	movzbl	1(%rcx), %eax
	movl	$1, %r9d
	cmpb	%al, 1(%rdx)
	jne	.L3122
.L3204:
	leaq	1(%r9), %rax
	cmpq	$0, -200(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3127
	movq	-288(%rbp), %r8
	movzbl	1(%r9,%rdx), %eax
	cmpb	%al, 1(%r8)
	jne	.L3127
	leaq	2(%r9), %rax
	cmpq	$0, -208(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3127
	movzbl	2(%r9,%rdx), %eax
	cmpb	%al, 2(%r8)
	jne	.L3127
	leaq	3(%r9), %rax
	cmpq	$0, -224(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3127
	movzbl	3(%r9,%rdx), %eax
	cmpb	%al, 3(%r8)
	jne	.L3127
	leaq	4(%r9), %rax
	cmpq	$0, -232(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3127
	movzbl	4(%r9,%rdx), %eax
	cmpb	%al, 4(%r8)
	jne	.L3127
	leaq	5(%r9), %rax
	cmpq	$0, -240(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3127
	movzbl	5(%r9,%rdx), %eax
	cmpb	%al, 5(%r8)
	jne	.L3127
	leaq	6(%r9), %rax
	cmpq	$0, -248(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3127
	movzbl	6(%r8), %eax
	cmpb	%al, 6(%r9,%rdx)
	jne	.L3127
	leaq	7(%r9), %rax
	movq	%rax, -112(%rbp)
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3433:
	subq	$1, -112(%rbp)
	movq	-184(%rbp), %rax
	leaq	(%rax,%r8), %r9
	leaq	8(%r8), %rax
	je	.L3454
	movq	%rax, %r8
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3438:
	andq	%rbx, %rsi
	leaq	0(%r13,%rsi), %rax
	xorl	%esi, %esi
	movq	%rax, -152(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -136(%rbp)
.L3159:
	movq	-152(%rbp), %rdi
	movq	(%r15,%rsi), %rax
	movq	(%rdi,%rsi), %rdi
	cmpq	%rdi, %rax
	je	.L3455
	xorq	%rdi, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
	movq	%rax, -136(%rbp)
.L3161:
	cmpq	$3, %r10
	movl	$3, %esi
	cmovnb	%r10, %rsi
	cmpq	%rsi, -136(%rbp)
	jbe	.L3157
	movq	-136(%rbp), %rax
	bsrl	%edx, %edx
	imull	$30, %edx, %edx
	leaq	(%rax,%rax,8), %rdi
	movq	%rdi, %rsi
	salq	$4, %rsi
	subq	%rdi, %rsi
	addq	$1920, %rsi
	subq	%rdx, %rsi
	cmpq	-128(%rbp), %rsi
	jbe	.L3157
	movq	%rsi, -128(%rbp)
	movq	%rax, %r10
	movq	%rcx, -88(%rbp)
	jmp	.L3157
	.p2align 4,,10
	.p2align 3
.L3439:
	movq	-128(%rbp), %rax
	movq	56(%r11), %r9
	movq	%r10, -48(%rbp)
	subq	$1, -112(%rbp)
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -104(%rbp)
	jmp	.L3165
.L3449:
	leal	0(,%r10,4), %ecx
	movl	$158663784, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L3171
	.p2align 4,,10
	.p2align 3
.L3453:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L3443:
	leaq	-2(%rcx), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L3177
	.p2align 4,,10
	.p2align 3
.L3436:
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L3141
	leaq	8(%r15), %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -128(%rbp)
.L3143:
	movq	(%r15,%rcx), %rdx
	movq	(%rax,%rcx), %rdi
	cmpq	%rdi, %rdx
	je	.L3456
	xorq	%rdi, %rdx
	xorl	%edi, %edi
	leaq	-1(%rsi), %rax
	rep bsfq	%rdx, %rdi
	movq	%rax, -216(%rbp)
	movslq	%edi, %rdi
	shrq	$3, %rdi
	addq	%rcx, %rdi
.L3145:
	cmpq	$3, %rdi
	jbe	.L3227
	leaq	(%rdi,%rdi,8), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	addq	$1935, %rax
	movq	%rax, -128(%rbp)
	cmpq	$2020, %rax
	jbe	.L3227
	movzbl	(%r15,%rdi), %r8d
	movq	%rdi, %r10
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L3227:
	movq	$0, -88(%rbp)
	movq	$2020, -128(%rbp)
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L3183:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3444:
	leaq	-66(%rcx), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L3177
.L3148:
	testq	%rsi, %rsi
	je	.L3147
	movzbl	(%r15), %eax
	cmpb	%al, (%rdx)
	jne	.L3147
	movq	-240(%rbp), %rax
	cmpq	$0, -216(%rbp)
	movq	%rax, -320(%rbp)
	je	.L3147
	movzbl	1(%r15), %eax
	movl	$1, %r8d
	cmpb	%al, 1(%rdx)
	jne	.L3147
.L3202:
	leaq	1(%r8), %rax
	cmpq	$0, -232(%rbp)
	movq	%rax, -192(%rbp)
	je	.L3152
	movq	-320(%rbp), %rdi
	movzbl	1(%r8,%rdx), %eax
	cmpb	%al, 1(%rdi)
	jne	.L3152
	leaq	2(%r8), %rax
	cmpq	$0, -248(%rbp)
	movq	%rax, -192(%rbp)
	je	.L3152
	movzbl	2(%r8,%rdx), %eax
	cmpb	%al, 2(%rdi)
	jne	.L3152
	leaq	3(%r8), %rax
	cmpq	$0, -296(%rbp)
	movq	%rax, -192(%rbp)
	je	.L3152
	movzbl	3(%r8,%rdx), %eax
	cmpb	%al, 3(%rdi)
	jne	.L3152
	leaq	4(%r8), %rax
	cmpq	$0, -288(%rbp)
	movq	%rax, -192(%rbp)
	je	.L3152
	movzbl	4(%r8,%rdx), %eax
	cmpb	%al, 4(%rdi)
	jne	.L3152
	leaq	5(%r8), %rax
	cmpq	$0, -304(%rbp)
	movq	%rax, -192(%rbp)
	je	.L3152
	movzbl	5(%r8,%rdx), %eax
	cmpb	%al, 5(%rdi)
	jne	.L3152
	leaq	6(%r8), %rax
	cmpq	$0, -312(%rbp)
	movq	%rax, -192(%rbp)
	je	.L3152
	movzbl	6(%rdi), %eax
	cmpb	%al, 6(%r8,%rdx)
	jne	.L3152
	leaq	7(%r8), %rax
	movq	%rax, -192(%rbp)
	jmp	.L3152
.L3437:
	subq	$1, -192(%rbp)
	movq	-224(%rbp), %rax
	leaq	(%rax,%rdi), %r8
	leaq	8(%rdi), %rax
	je	.L3457
	movq	%rax, %rdi
	jmp	.L3150
.L3235:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L3177
.L3446:
	subq	$1, -112(%rbp)
	movq	-152(%rbp), %rax
	leaq	(%rax,%rdi), %r10
	leaq	8(%rdi), %rax
	je	.L3458
	movq	%rax, %rdi
	jmp	.L3134
.L3448:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L3169
.L3238:
	movq	%rdx, -56(%rbp)
	movq	%rax, %r12
	jmp	.L3189
.L3450:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L3171
.L3116:
	testq	%rdi, %rdi
	je	.L3212
	movq	%rdi, %rsi
	leaq	1(%rcx), %r8
	subq	$1, %rsi
	movq	%rsi, -176(%rbp)
	je	.L3216
	movzbl	1(%rcx), %esi
	movl	$1, %edx
	cmpb	%sil, 1(%rax)
	jne	.L3216
.L3205:
	leaq	1(%rdx), %rsi
	movq	%rsi, -48(%rbp)
	movq	-176(%rbp), %rsi
	cmpq	$1, %rsi
	je	.L3120
	movzbl	1(%rax,%rdx), %r9d
	cmpb	%r9b, 1(%r8)
	jne	.L3120
	leaq	2(%rdx), %r9
	movq	%r9, -48(%rbp)
	cmpq	$2, %rsi
	je	.L3120
	movzbl	2(%rax,%rdx), %r9d
	cmpb	%r9b, 2(%r8)
	jne	.L3120
	leaq	3(%rdx), %r9
	movq	%r9, -48(%rbp)
	cmpq	$3, %rsi
	je	.L3120
	movzbl	3(%rax,%rdx), %r9d
	cmpb	%r9b, 3(%r8)
	jne	.L3120
	leaq	4(%rdx), %r9
	movq	%r9, -48(%rbp)
	subq	$4, %rsi
	je	.L3120
	movzbl	4(%rax,%rdx), %r9d
	cmpb	%r9b, 4(%r8)
	jne	.L3120
	leaq	5(%rdx), %r9
	movq	%r9, -48(%rbp)
	cmpq	$1, %rsi
	je	.L3120
	movzbl	5(%rax,%rdx), %eax
	cmpb	%al, 5(%r8)
	jne	.L3120
	leaq	6(%rdx), %rax
	movq	%rax, -48(%rbp)
	jmp	.L3120
.L3447:
	movq	-48(%rbp), %rdx
	leaq	(%rdx,%rsi), %r8
	leaq	8(%rsi), %rdx
	subq	$1, %r9
	je	.L3459
	movq	%rdx, %rsi
	jmp	.L3118
.L3208:
	xorl	%edx, %edx
	jmp	.L3114
.L3163:
	movq	-120(%rbp), %r12
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r15
	cmpq	%rax, %r12
	movq	%r12, %r14
	cmovbe	%r12, %rax
	movq	%rax, -96(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rsi
	jmp	.L3164
.L3454:
	movq	%rax, -112(%rbp)
	movq	%r9, %rax
	testq	%rdi, %rdi
	je	.L3127
	movq	-112(%rbp), %r9
	movzbl	(%rdx,%r9), %r9d
	cmpb	%r9b, (%rax)
	jne	.L3127
	leaq	1(%rax), %r9
	cmpq	$0, -176(%rbp)
	movq	%r9, -288(%rbp)
	leaq	9(%r8), %r9
	je	.L3219
	movzbl	9(%r8,%rdx), %r8d
	cmpb	%r8b, 1(%rax)
	je	.L3204
.L3219:
	movq	%r9, -112(%rbp)
	jmp	.L3127
.L3455:
	subq	$1, -136(%rbp)
	movq	-192(%rbp), %rax
	leaq	(%rax,%rsi), %rdi
	leaq	8(%rsi), %rax
	je	.L3460
	movq	%rax, %rsi
	jmp	.L3159
.L3456:
	movq	-128(%rbp), %rdi
	leaq	(%rdi,%rcx), %rdx
	leaq	8(%rcx), %rdi
	subq	$1, %r12
	je	.L3461
	movq	%rdi, %rcx
	jmp	.L3143
.L3141:
	testq	%rcx, %rcx
	je	.L3225
	movzbl	(%rax), %ecx
	cmpb	%cl, (%r15)
	jne	.L3223
	movq	%rsi, %rcx
	leaq	1(%r15), %rdi
	subq	$1, %rcx
	movq	%rdi, -136(%rbp)
	movq	%rcx, -216(%rbp)
	je	.L3227
	movq	$1, -128(%rbp)
	movzbl	1(%r15), %ecx
	cmpb	%cl, 1(%rax)
	jne	.L3227
.L3203:
	movq	-128(%rbp), %rdi
	movq	-216(%rbp), %rcx
	addq	$1, %rdi
	cmpq	$1, %rcx
	je	.L3145
	movq	-128(%rbp), %rdx
	movzbl	1(%rdx,%rax), %r12d
	movq	-136(%rbp), %rdx
	cmpb	%r12b, 1(%rdx)
	jne	.L3145
	movq	-128(%rbp), %rdi
	addq	$2, %rdi
	cmpq	$2, %rcx
	je	.L3145
	movq	-128(%rbp), %r12
	movzbl	2(%r12,%rax), %r12d
	cmpb	%r12b, 2(%rdx)
	jne	.L3145
	movq	-128(%rbp), %rdi
	addq	$3, %rdi
	cmpq	$3, %rcx
	je	.L3145
	movq	-128(%rbp), %r12
	movzbl	3(%r12,%rax), %r12d
	cmpb	%r12b, 3(%rdx)
	jne	.L3145
	movq	-128(%rbp), %rdi
	addq	$4, %rdi
	subq	$4, %rcx
	je	.L3145
	movq	-128(%rbp), %r12
	movzbl	4(%r12,%rax), %r12d
	cmpb	%r12b, 4(%rdx)
	jne	.L3145
	movq	-128(%rbp), %rdi
	addq	$5, %rdi
	cmpq	$1, %rcx
	je	.L3145
	movq	-128(%rbp), %rcx
	movzbl	5(%rcx,%rax), %eax
	cmpb	%al, 5(%rdx)
	jne	.L3145
	movq	-128(%rbp), %rdi
	addq	$6, %rdi
	jmp	.L3145
.L3233:
	movl	$2, %edx
	jmp	.L3169
.L3451:
	movl	$3, %edx
	jmp	.L3169
.L3457:
	movq	%rax, -192(%rbp)
	movq	%r8, %rax
	testq	%rsi, %rsi
	je	.L3152
	movq	-192(%rbp), %r8
	movzbl	(%rdx,%r8), %r8d
	cmpb	%r8b, (%rax)
	jne	.L3152
	leaq	1(%rax), %r8
	cmpq	$0, -216(%rbp)
	movq	%r8, -320(%rbp)
	leaq	9(%rdi), %r8
	je	.L3230
	movzbl	9(%rdi,%rdx), %edi
	cmpb	%dil, 1(%rax)
	je	.L3202
.L3230:
	movq	%r8, -192(%rbp)
	jmp	.L3152
.L3458:
	movq	%rax, -112(%rbp)
	movq	%r10, -184(%rbp)
	testq	%r14, %r14
	je	.L3136
	movq	-120(%rbp), %r10
	movzbl	(%r10,%rax), %r10d
	movq	-184(%rbp), %rax
	cmpb	%r10b, (%rax)
	jne	.L3136
	leaq	9(%rdi), %rax
	cmpq	$0, -176(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3136
	movq	-120(%rbp), %r10
	movq	-184(%rbp), %rax
	movzbl	9(%r10,%rdi), %r10d
	cmpb	%r10b, 1(%rax)
	jne	.L3136
	leaq	10(%rdi), %rax
	cmpq	$0, -200(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3136
	movq	-120(%rbp), %r10
	movq	-184(%rbp), %rax
	movzbl	10(%r10,%rdi), %r10d
	cmpb	%r10b, 2(%rax)
	jne	.L3136
	leaq	11(%rdi), %rax
	cmpq	$0, -208(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3136
	movq	-120(%rbp), %r10
	movq	-184(%rbp), %rax
	movzbl	11(%r10,%rdi), %r10d
	cmpb	%r10b, 3(%rax)
	jne	.L3136
	leaq	12(%rdi), %rax
	cmpq	$0, -224(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3136
	movq	-120(%rbp), %r10
	movq	-184(%rbp), %rax
	movzbl	12(%r10,%rdi), %r10d
	cmpb	%r10b, 4(%rax)
	jne	.L3136
	leaq	13(%rdi), %rax
	cmpq	$0, -232(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3136
	movq	-120(%rbp), %r10
	movq	-184(%rbp), %rax
	movzbl	13(%r10,%rdi), %r10d
	cmpb	%r10b, 5(%rax)
	jne	.L3136
	leaq	14(%rdi), %rax
	cmpq	$0, -240(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3136
	movq	-120(%rbp), %r10
	movq	-184(%rbp), %rax
	movzbl	14(%r10,%rdi), %r10d
	cmpb	%r10b, 6(%rax)
	jne	.L3136
	leaq	15(%rdi), %rax
	cmpq	$0, -248(%rbp)
	movq	%rax, -112(%rbp)
	je	.L3136
	movq	-184(%rbp), %rax
	movq	-120(%rbp), %r10
	movzbl	7(%rax), %eax
	cmpb	%al, 15(%r10,%rdi)
	jne	.L3136
	leaq	16(%rdi), %rax
	movq	%rax, -112(%rbp)
	jmp	.L3136
.L3459:
	movq	%rdx, -48(%rbp)
	movq	%r8, -64(%rbp)
	testq	%rdi, %rdi
	je	.L3462
	movq	%r8, %rdx
	movq	-48(%rbp), %r8
	movzbl	(%rax,%r8), %r8d
	cmpb	%r8b, (%rdx)
	jne	.L3463
	movq	%rdi, %r9
	leaq	1(%rdx), %r8
	leaq	9(%rsi), %rdx
	subq	$1, %r9
	movq	%r9, -176(%rbp)
	je	.L3214
	movq	-64(%rbp), %r9
	movzbl	9(%rax,%rsi), %esi
	cmpb	%sil, 1(%r9)
	je	.L3205
.L3214:
	movq	%rdx, -48(%rbp)
	jmp	.L3120
.L3212:
	movq	$0, -104(%rbp)
	movq	$0, -48(%rbp)
	movq	$2020, -64(%rbp)
	movq	$-1, -176(%rbp)
	jmp	.L3115
.L3461:
	testq	%rsi, %rsi
	je	.L3464
	movzbl	(%rax,%rdi), %r12d
	cmpb	%r12b, (%rdx)
	jne	.L3465
	movq	%rsi, %r12
	leaq	1(%rdx), %rdi
	subq	$1, %r12
	movq	%rdi, -136(%rbp)
	leaq	9(%rcx), %rdi
	movq	%r12, -216(%rbp)
	je	.L3145
	movzbl	9(%rcx,%rax), %ecx
	cmpb	%cl, 1(%rdx)
	jne	.L3145
	movq	%rdi, -128(%rbp)
	jmp	.L3203
.L3460:
	movq	%rax, -136(%rbp)
	movq	%rdi, -208(%rbp)
	testq	%r8, %r8
	je	.L3161
	movq	-152(%rbp), %rdi
	movzbl	(%rdi,%rax), %edi
	movq	-208(%rbp), %rax
	cmpb	%dil, (%rax)
	jne	.L3161
	leaq	9(%rsi), %rax
	cmpq	$0, -216(%rbp)
	movq	%rax, -136(%rbp)
	je	.L3161
	movq	-152(%rbp), %rdi
	movq	-208(%rbp), %rax
	movzbl	9(%rdi,%rsi), %edi
	cmpb	%dil, 1(%rax)
	jne	.L3161
	leaq	10(%rsi), %rax
	cmpq	$0, -232(%rbp)
	movq	%rax, -136(%rbp)
	je	.L3161
	movq	-152(%rbp), %rdi
	movq	-208(%rbp), %rax
	movzbl	10(%rdi,%rsi), %edi
	cmpb	%dil, 2(%rax)
	jne	.L3161
	leaq	11(%rsi), %rax
	cmpq	$0, -248(%rbp)
	movq	%rax, -136(%rbp)
	je	.L3161
	movq	-152(%rbp), %rdi
	movq	-208(%rbp), %rax
	movzbl	11(%rdi,%rsi), %edi
	cmpb	%dil, 3(%rax)
	jne	.L3161
	leaq	12(%rsi), %rax
	cmpq	$0, -296(%rbp)
	movq	%rax, -136(%rbp)
	je	.L3161
	movq	-152(%rbp), %rdi
	movq	-208(%rbp), %rax
	movzbl	12(%rdi,%rsi), %edi
	cmpb	%dil, 4(%rax)
	jne	.L3161
	leaq	13(%rsi), %rax
	cmpq	$0, -288(%rbp)
	movq	%rax, -136(%rbp)
	je	.L3161
	movq	-152(%rbp), %rdi
	movq	-208(%rbp), %rax
	movzbl	13(%rdi,%rsi), %edi
	cmpb	%dil, 5(%rax)
	jne	.L3161
	leaq	14(%rsi), %rax
	cmpq	$0, -304(%rbp)
	movq	%rax, -136(%rbp)
	je	.L3161
	movq	-152(%rbp), %rdi
	movq	-208(%rbp), %rax
	movzbl	14(%rdi,%rsi), %edi
	cmpb	%dil, 6(%rax)
	jne	.L3161
	leaq	15(%rsi), %rax
	cmpq	$0, -312(%rbp)
	movq	%rax, -136(%rbp)
	je	.L3161
	movq	-208(%rbp), %rax
	movq	-152(%rbp), %rdi
	movzbl	7(%rax), %eax
	cmpb	%al, 15(%rdi,%rsi)
	jne	.L3161
	leaq	16(%rsi), %rax
	movq	%rax, -136(%rbp)
	jmp	.L3161
.L3225:
	movq	$0, -88(%rbp)
	movq	$2020, -128(%rbp)
	movq	$-1, -216(%rbp)
	jmp	.L3140
.L3441:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3463:
	leaq	-1(%rdi), %rax
	movq	%rax, -176(%rbp)
	jmp	.L3120
.L3462:
	movq	$-1, -176(%rbp)
	jmp	.L3120
.L3465:
	leaq	-1(%rsi), %rax
	movq	%rax, -216(%rbp)
	jmp	.L3145
.L3464:
	movq	$-1, -216(%rbp)
	jmp	.L3145
	.cfi_endproc
.LFE289:
	.size	CreateBackwardReferencesNH35, .-CreateBackwardReferencesNH35
	.p2align 4
	.type	CreateBackwardReferencesNH55, @function
CreateBackwardReferencesNH55:
.LFB290:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	addq	%rdi, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$256, %edx
	pushq	%rbx
	subq	$160, %rsp
	.cfi_offset 3, -56
	movq	%r8, -256(%rbp)
	movq	%rsi, -104(%rbp)
	movl	8(%r8), %ecx
	salq	%cl, %rax
	subq	$16, %rax
	cmpq	$7, %rdi
	movq	%rax, -152(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	leaq	-7(%rsi), %rax
	cmovbe	%r15, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -280(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -264(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movq	%rax, -288(%rbp)
	addq	%r15, %rax
	movq	%rax, -144(%rbp)
	leaq	8(%r15), %rax
	cmpq	%rax, %rsi
	jbe	.L3563
	movq	40(%r9), %rax
	movq	48(%r9), %r13
	movq	%rax, -120(%rbp)
	addq	$40, %rax
	movq	%r13, %r14
	movq	%r11, %r13
	movq	%rax, -160(%rbp)
	movl	%r11d, %eax
	movq	%r15, %r11
	movq	%rax, -48(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L3550:
	movq	-152(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	%r13, %rsi
	movabsq	$3866266742567714048, %r15
	subq	%r11, %rbx
	cmpq	%r11, %rax
	cmova	%r11, %rax
	andq	%r11, %rsi
	movq	%rbx, %r8
	movq	%rbx, -136(%rbp)
	addq	%r12, %rsi
	shrq	$3, %rbx
	andl	$7, %r8d
	imulq	(%rsi), %r15
	movzbl	(%rsi), %r10d
	movq	%rax, -80(%rbp)
	movq	16(%rbp), %rax
	movq	%rbx, -88(%rbp)
	movb	%r10b, -192(%rbp)
	movslq	(%rax), %rax
	shrq	$44, %r15
	movq	%r15, -96(%rbp)
	movq	%rax, %rcx
	movq	%rax, -112(%rbp)
	movq	%r11, %rax
	subq	%rcx, %rax
	cmpq	%r11, %rax
	jnb	.L3565
	andl	%r13d, %eax
	addq	%r12, %rax
	cmpb	%r10b, (%rax)
	je	.L3786
.L3565:
	leaq	-1(%r8), %rax
	movq	$0, -112(%rbp)
	xorl	%r15d, %r15d
	movq	$2020, -72(%rbp)
	movq	%rax, -176(%rbp)
.L3470:
	movq	-160(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	%r14, -128(%rbp)
	leaq	(%rax,%rbx,4), %rdi
	leaq	20(%rdi), %rbx
	movl	(%rdi), %eax
	leaq	4(%rdi), %rcx
	movq	%rbx, -56(%rbp)
	leaq	-2(%r8), %rbx
	movq	%rbx, -200(%rbp)
	leaq	-3(%r8), %rbx
	movq	%rbx, -216(%rbp)
	leaq	-4(%r8), %rbx
	movq	%rbx, -224(%rbp)
	leaq	-5(%r8), %rbx
	movq	%rbx, -232(%rbp)
	leaq	-6(%r8), %rbx
	movq	%rbx, -240(%rbp)
	leaq	-7(%r8), %rbx
	movq	%rbx, -248(%rbp)
	leaq	1(%rsi), %rbx
	movq	%rbx, -208(%rbp)
	leaq	8(%rsi), %rbx
	movq	%rbx, -184(%rbp)
.L3484:
	movq	-48(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r12, %rdx
	movzbl	(%rdx,%r15), %edi
	cmpl	%r10d, %edi
	jne	.L3477
	movq	%r11, %r9
	subq	%rax, %r9
	je	.L3477
	cmpq	-80(%rbp), %r9
	ja	.L3477
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	je	.L3478
	xorl	%edi, %edi
.L3480:
	movq	(%rsi,%rdi), %rax
	movq	(%rdx,%rdi), %rbx
	cmpq	%rbx, %rax
	je	.L3787
	xorq	%rbx, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rdi, %rax
.L3482:
	cmpq	$3, %rax
	jbe	.L3477
	leaq	(%rax,%rax,8), %rdi
	movq	%rdi, %rdx
	salq	$4, %rdx
	subq	%rdi, %rdx
	bsrl	%r9d, %edi
	imull	$30, %edi, %edi
	addq	$1920, %rdx
	subq	%rdi, %rdx
	cmpq	-72(%rbp), %rdx
	jbe	.L3477
	movq	%r9, -112(%rbp)
	movzbl	(%rsi,%rax), %r10d
	movq	%rax, %r15
	movq	%rdx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L3477:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	-56(%rbp), %rcx
	jne	.L3484
	movq	%r11, %rax
	movq	-128(%rbp), %r14
	movq	-120(%rbp), %rbx
	movl	%r11d, %edi
	shrq	$3, %rax
	andl	$3, %eax
	addq	-96(%rbp), %rax
	movq	56(%r14), %r10
	movl	%r11d, 40(%rbx,%rax,4)
	testb	$3, %r11b
	jne	.L3485
	cmpq	$31, -136(%rbp)
	jbe	.L3485
	cmpq	%r11, %r10
	ja	.L3486
	leaq	8(%rsi), %rax
	movq	%r8, -96(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rsi, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L3492:
	leaq	32(%r10), %rax
	movl	40(%r14), %edx
	andq	%r13, %rax
	movzbl	(%r12,%rax), %ecx
	movl	68(%r14), %eax
	imull	%edx, %eax
	leal	1(%rcx,%rax), %ecx
	movq	%r13, %rax
	andq	%r10, %rax
	movzbl	(%r12,%rax), %eax
	addl	$1, %eax
	imull	72(%r14), %eax
	subl	%eax, %ecx
	movl	%ecx, 40(%r14)
	testl	$1056964608, %edx
	jne	.L3487
	movq	48(%r14), %rax
	andl	$1073741823, %edx
	leaq	(%rax,%rdx,4), %rdx
	movl	(%rdx), %eax
	movl	%r10d, (%rdx)
	cmpq	%r10, %r11
	jne	.L3487
	movl	$4294967295, %esi
	cmpq	%rsi, %rax
	je	.L3487
	movl	%edi, %esi
	subl	%eax, %esi
	movl	%esi, %ecx
	cmpq	-80(%rbp), %rsi
	jbe	.L3788
	.p2align 4,,10
	.p2align 3
.L3487:
	addq	$4, %r10
	cmpq	%r11, %r10
	jbe	.L3492
.L3486:
	leaq	4(%r11), %r10
	movq	%r10, 56(%r14)
.L3485:
	cmpq	$2020, -72(%rbp)
	jne	.L3789
	movq	-64(%rbp), %rsi
	leaq	1(%r11), %rax
	leaq	1(%rsi), %rdx
	cmpq	%rax, -144(%rbp)
	jnb	.L3785
	movq	-104(%rbp), %rsi
	leaq	-7(%rsi), %rcx
	movq	-144(%rbp), %rsi
	addq	-264(%rbp), %rsi
	cmpq	%rax, %rsi
	jnb	.L3547
	leaq	17(%r11), %rsi
	movq	-64(%rbp), %rbx
	movq	-120(%rbp), %r9
	movabsq	$3866266742567714048, %r8
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	leaq	4(%rbx), %rsi
	subq	%r11, %rsi
	cmpq	%rax, %rcx
	jbe	.L3785
	.p2align 4,,10
	.p2align 3
.L3548:
	movq	%r13, %rdx
	movq	%rax, %rdi
	andq	%rax, %rdx
	shrq	$3, %rdi
	movq	(%r12,%rdx), %rbx
	andl	$3, %edi
	imulq	%r8, %rbx
	movq	%rbx, %rdx
	shrq	$44, %rdx
	addq	%rdi, %rdx
	movq	%rax, %rdi
	movl	%eax, 40(%r9,%rdx,4)
	leaq	(%rsi,%rax), %rdx
	addq	$4, %rax
	cmpq	%rcx, %rax
	jb	.L3548
	movq	%rdx, -64(%rbp)
	addq	$12, %rdi
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L3544:
	cmpq	%rdi, -104(%rbp)
	ja	.L3550
	movq	-168(%rbp), %rdx
	subq	32(%rbp), %rdx
	movq	%r11, %r15
	sarq	$4, %rdx
.L3469:
	movq	24(%rbp), %rbx
	movq	-104(%rbp), %rax
	addq	-64(%rbp), %rax
	subq	%r15, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3785:
	.cfi_restore_state
	leaq	9(%r11), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %r11
	jmp	.L3544
	.p2align 4,,10
	.p2align 3
.L3789:
	movq	-136(%rbp), %rax
	movq	%r15, -216(%rbp)
	movq	%r11, -144(%rbp)
	subq	$1, %rax
	movq	%rax, -136(%rbp)
	leaq	4(%r11), %rax
	movq	%rax, -208(%rbp)
.L3520:
	movq	-256(%rbp), %rax
	xorl	%r15d, %r15d
	cmpl	$4, 4(%rax)
	jg	.L3494
	movq	-216(%rbp), %rax
	movq	-136(%rbp), %rbx
	subq	$1, %rax
	cmpq	%rbx, %rax
	cmova	%rbx, %rax
	movq	%rax, %r15
.L3494:
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %rsi
	leaq	1(%rax), %rbx
	movq	-152(%rbp), %rax
	cmpq	%rax, %rbx
	cmovbe	%rbx, %rax
	movq	%rax, -88(%rbp)
	movq	%rbx, %rax
	andq	%r13, %rax
	leaq	(%r12,%rax), %r9
	movabsq	$3866266742567714048, %rax
	imulq	(%r9), %rax
	shrq	$44, %rax
	movq	%rax, -176(%rbp)
	movzbl	(%r9,%r15), %eax
	movl	%eax, -184(%rbp)
	movl	%eax, %edx
	movq	16(%rbp), %rax
	movslq	(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	movq	%rbx, %rax
	subq	%rdi, %rax
	movq	%rsi, %rdi
	andl	$7, %esi
	shrq	$3, %rdi
	movq	%rsi, %r8
	movq	%rdi, -128(%rbp)
	cmpq	%rax, %rbx
	jbe	.L3576
	andq	-48(%rbp), %rax
	addq	%r12, %rax
	cmpb	(%rax,%r15), %dl
	je	.L3790
.L3576:
	leaq	-1(%r8), %rax
	movq	$0, -56(%rbp)
	movq	$2020, -96(%rbp)
	movq	%rax, -224(%rbp)
.L3495:
	movq	-160(%rbp), %rax
	movq	-176(%rbp), %rsi
	leaq	-4(%r8), %rdi
	movq	%r10, -192(%rbp)
	movq	%rdi, -296(%rbp)
	leaq	-6(%r8), %rdi
	leaq	(%rax,%rsi,4), %rsi
	movq	%rdi, -312(%rbp)
	leaq	1(%r9), %rdi
	movl	(%rsi), %eax
	leaq	4(%rsi), %rcx
	addq	$20, %rsi
	movq	%rdi, -248(%rbp)
	movq	%rsi, -80(%rbp)
	leaq	-2(%r8), %rsi
	movq	%rsi, -240(%rbp)
	leaq	-3(%r8), %rsi
	movq	%rsi, -272(%rbp)
	leaq	-5(%r8), %rsi
	movq	%rsi, -304(%rbp)
	leaq	-7(%r8), %rsi
	movq	%rsi, -320(%rbp)
	leaq	8(%r9), %rsi
	movq	%rsi, -232(%rbp)
	movq	%r13, -200(%rbp)
	movl	-184(%rbp), %r13d
.L3509:
	movq	-48(%rbp), %rdx
	andq	%rax, %rdx
	addq	%r12, %rdx
	movzbl	(%rdx,%r15), %esi
	cmpl	%r13d, %esi
	jne	.L3502
	movq	%rbx, %rdi
	subq	%rax, %rdi
	je	.L3502
	cmpq	-88(%rbp), %rdi
	ja	.L3502
	movq	-128(%rbp), %r11
	testq	%r11, %r11
	je	.L3503
	xorl	%esi, %esi
.L3505:
	movq	(%r9,%rsi), %rax
	movq	(%rdx,%rsi), %r10
	cmpq	%r10, %rax
	je	.L3791
	xorq	%r10, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
.L3507:
	cmpq	$3, %rax
	jbe	.L3502
	leaq	(%rax,%rax,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%edi, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-96(%rbp), %rdx
	jbe	.L3502
	movq	%rdi, -56(%rbp)
	movzbl	(%r9,%rax), %r13d
	movq	%rax, %r15
	movq	%rdx, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L3502:
	addq	$4, %rcx
	movl	-4(%rcx), %eax
	cmpq	-80(%rbp), %rcx
	jne	.L3509
	movq	%rbx, %rax
	movq	-120(%rbp), %rsi
	movl	%ebx, %r11d
	movq	-192(%rbp), %r10
	shrq	$3, %rax
	movq	-200(%rbp), %r13
	andl	$3, %eax
	addq	-176(%rbp), %rax
	movl	%ebx, 40(%rsi,%rax,4)
	testb	$3, %bl
	jne	.L3510
	cmpq	$31, -136(%rbp)
	jbe	.L3510
	cmpq	%r10, %rbx
	jb	.L3511
	leaq	8(%r9), %rax
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L3517:
	leaq	32(%r10), %rax
	movl	40(%r14), %edx
	andq	%r13, %rax
	movzbl	(%r12,%rax), %ecx
	movl	68(%r14), %eax
	imull	%edx, %eax
	leal	1(%rcx,%rax), %ecx
	movq	%r13, %rax
	andq	%r10, %rax
	movzbl	(%r12,%rax), %eax
	addl	$1, %eax
	imull	72(%r14), %eax
	subl	%eax, %ecx
	movl	%ecx, 40(%r14)
	testl	$1056964608, %edx
	jne	.L3512
	movq	48(%r14), %rax
	andl	$1073741823, %edx
	leaq	(%rax,%rdx,4), %rax
	movl	(%rax), %esi
	movl	%r10d, (%rax)
	cmpq	%r10, %rbx
	jne	.L3512
	movl	$4294967295, %eax
	cmpq	%rax, %rsi
	je	.L3512
	movl	%r11d, %eax
	subl	%esi, %eax
	movl	%eax, %ecx
	movq	%rcx, %rdx
	cmpq	-88(%rbp), %rcx
	jbe	.L3792
	.p2align 4,,10
	.p2align 3
.L3512:
	addq	$4, %r10
	cmpq	%r10, %rbx
	jnb	.L3517
.L3511:
	movq	-144(%rbp), %rax
	addq	$5, %rax
	movq	%rax, 56(%r14)
.L3510:
	movq	-72(%rbp), %rax
	addq	$175, %rax
	cmpq	-96(%rbp), %rax
	ja	.L3518
	addq	$1, -64(%rbp)
	cmpq	-208(%rbp), %rbx
	je	.L3779
	movq	-144(%rbp), %rax
	addq	$9, %rax
	cmpq	%rax, -104(%rbp)
	ja	.L3793
.L3779:
	movq	%r15, %r8
.L3519:
	movq	-288(%rbp), %rax
	movq	-88(%rbp), %rdi
	addq	%rbx, %rax
	leaq	(%rax,%r8,2), %rax
	movq	%rax, -144(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rdi, %rax
	jbe	.L3794
	leaq	15(%rax), %rdx
.L3552:
	movq	-168(%rbp), %rax
	leaq	16(%rax), %rdi
	movl	%r8d, 4(%rax)
	movq	%rdi, -72(%rbp)
	movl	-64(%rbp), %edi
	movl	%edi, (%rax)
	movq	-256(%rbp), %rax
	movl	56(%rax), %edi
	movl	60(%rax), %eax
	leaq	16(%rax), %rcx
	movq	%rdi, %r15
	movq	%rax, %r11
	cmpq	%rcx, %rdx
	jb	.L3795
	leal	2(%rdi), %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	leaq	-16(%rdx,%rcx), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r15d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r11,%r10), %r10d
	movl	%r15d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L3530:
	movq	-168(%rbp), %rdi
	cmpq	$5, -64(%rbp)
	movw	%ax, 14(%rdi)
	movslq	%r8d, %rax
	movl	%edx, 8(%rdi)
	jbe	.L3796
	cmpq	$129, -64(%rbp)
	jbe	.L3797
	cmpq	$2113, -64(%rbp)
	jbe	.L3798
	movq	-64(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L3588
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L3535
.L3807:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L3799
	.p2align 4,,10
	.p2align 3
.L3539:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L3541:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L3542
	.p2align 4,,10
	.p2align 3
.L3788:
	movq	-88(%rbp), %rdx
	andq	%r13, %rax
	movq	-56(%rbp), %rbx
	xorl	%r8d, %r8d
	addq	%r12, %rax
	movq	%rdx, -184(%rbp)
.L3489:
	movq	(%rbx,%r8), %rdx
	movq	(%rax,%r8), %r9
	cmpq	%r9, %rdx
	je	.L3800
	xorq	%r9, %rdx
	movq	%rbx, -56(%rbp)
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%r8, %rdx
.L3491:
	cmpq	$3, %r15
	movl	$3, %eax
	cmovnb	%r15, %rax
	cmpq	%rdx, %rax
	jnb	.L3487
	leaq	(%rdx,%rdx,8), %r8
	bsrl	%ecx, %ecx
	movq	%r8, %rax
	imull	$30, %ecx, %ecx
	salq	$4, %rax
	subq	%r8, %rax
	addq	$1920, %rax
	subq	%rcx, %rax
	cmpq	-72(%rbp), %rax
	jbe	.L3487
	movq	%rax, -72(%rbp)
	movq	%rdx, %r15
	movq	%rsi, -112(%rbp)
	jmp	.L3487
	.p2align 4,,10
	.p2align 3
.L3478:
	testq	%r8, %r8
	je	.L3477
	movzbl	-192(%rbp), %eax
	cmpb	(%rdx), %al
	jne	.L3477
	cmpq	$0, -176(%rbp)
	movq	-208(%rbp), %r14
	je	.L3477
	movzbl	1(%rsi), %eax
	movl	$1, %ebx
	cmpb	%al, 1(%rdx)
	jne	.L3477
.L3559:
	cmpq	$0, -200(%rbp)
	leaq	1(%rbx), %rax
	je	.L3482
	movzbl	1(%rbx,%rdx), %edi
	cmpb	%dil, 1(%r14)
	jne	.L3482
	cmpq	$0, -216(%rbp)
	leaq	2(%rbx), %rax
	je	.L3482
	movzbl	2(%rbx,%rdx), %edi
	cmpb	%dil, 2(%r14)
	jne	.L3482
	cmpq	$0, -224(%rbp)
	leaq	3(%rbx), %rax
	je	.L3482
	movzbl	3(%rbx,%rdx), %edi
	cmpb	%dil, 3(%r14)
	jne	.L3482
	cmpq	$0, -232(%rbp)
	leaq	4(%rbx), %rax
	je	.L3482
	movzbl	4(%rbx,%rdx), %edi
	cmpb	%dil, 4(%r14)
	jne	.L3482
	cmpq	$0, -240(%rbp)
	leaq	5(%rbx), %rax
	je	.L3482
	movzbl	5(%rbx,%rdx), %edi
	cmpb	%dil, 5(%r14)
	jne	.L3482
	cmpq	$0, -248(%rbp)
	leaq	6(%rbx), %rax
	je	.L3482
	movzbl	6(%r14), %edi
	cmpb	%dil, 6(%rbx,%rdx)
	jne	.L3482
	leaq	7(%rbx), %rax
	jmp	.L3482
	.p2align 4,,10
	.p2align 3
.L3547:
	leaq	9(%r11), %rdi
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	cmpq	%rax, %rcx
	jbe	.L3591
	movq	-64(%rbp), %rbx
	movq	-120(%rbp), %r10
	movabsq	$3866266742567714048, %r8
	leaq	2(%rbx), %r9
	subq	%r11, %r9
	.p2align 4,,10
	.p2align 3
.L3549:
	movq	%r13, %rdx
	movq	%rax, %rdi
	andq	%rax, %rdx
	movq	(%r12,%rdx), %rsi
	imulq	%r8, %rsi
	movq	%rsi, %rdx
	movq	%rax, %rsi
	shrq	$3, %rsi
	shrq	$44, %rdx
	andl	$3, %esi
	addq	%rsi, %rdx
	movl	%eax, 40(%r10,%rdx,4)
	leaq	(%r9,%rax), %rdx
	addq	$2, %rax
	cmpq	%rcx, %rax
	jb	.L3549
	movq	%rdx, -64(%rbp)
	addq	$10, %rdi
	movq	%rax, %r11
	jmp	.L3544
	.p2align 4,,10
	.p2align 3
.L3787:
	movq	-184(%rbp), %rax
	leaq	(%rax,%rdi), %rbx
	leaq	8(%rdi), %rax
	subq	$1, %r14
	je	.L3801
	movq	%rax, %rdi
	jmp	.L3480
	.p2align 4,,10
	.p2align 3
.L3786:
	movq	%rbx, %r9
	testq	%rbx, %rbx
	je	.L3471
	xorl	%ecx, %ecx
	leaq	8(%rsi), %rbx
.L3473:
	movq	(%rsi,%rcx), %rdx
	movq	(%rax,%rcx), %rdi
	cmpq	%rdi, %rdx
	je	.L3802
	xorq	%rdi, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	leaq	(%rax,%rcx), %r15
	leaq	-1(%r8), %rax
	movq	%rax, -176(%rbp)
.L3475:
	cmpq	$3, %r15
	jbe	.L3569
	leaq	(%r15,%r15,8), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	subq	%rdx, %rax
	addq	$1935, %rax
	movq	%rax, -72(%rbp)
	cmpq	$2020, %rax
	jbe	.L3569
	movzbl	(%rsi,%r15), %r10d
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L3569:
	movq	$0, -112(%rbp)
	xorl	%r15d, %r15d
	movq	$2020, -72(%rbp)
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L3794:
	movq	16(%rbp), %rsi
	movq	16(%rbp), %rdi
	movslq	(%rsi), %rdx
	movslq	4(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rax, %rdx
	je	.L3522
	cmpq	%rax, %rcx
	je	.L3803
	addq	$3, %rax
	movq	%rax, %r9
	subq	%rdx, %r9
	cmpq	$6, %r9
	jbe	.L3804
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L3805
	movq	16(%rbp), %rax
	movq	-56(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	je	.L3586
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3806
	leaq	15(%rcx), %rdx
.L3526:
	testq	%rdx, %rdx
	je	.L3522
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L3524:
	movq	16(%rbp), %rcx
	movl	%esi, 4(%rcx)
	movl	-56(%rbp), %esi
	movl	%eax, 12(%rcx)
	movl	%edi, 8(%rcx)
	movl	%esi, (%rcx)
	jmp	.L3552
	.p2align 4,,10
	.p2align 3
.L3796:
	movq	-64(%rbp), %rdi
	movl	%edi, %edx
	sall	$3, %edi
	andl	$56, %edi
.L3532:
	cmpq	$9, %rax
	jbe	.L3807
.L3535:
	cmpq	$133, %rax
	jbe	.L3808
	cmpq	$2117, %rax
	ja	.L3538
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L3536:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L3539
.L3799:
	testb	%sil, %sil
	je	.L3539
	cmpw	$15, %ax
	ja	.L3539
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L3542:
	movq	-168(%rbp), %rax
	leaq	(%rbx,%r8), %r11
	movq	-64(%rbp), %rsi
	movq	%r8, %rdx
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	addq	%rsi, (%rax)
	leaq	2(%rbx), %rax
	movq	-280(%rbp), %rbx
	cmpq	%rbx, %r11
	movq	%rbx, %rsi
	movq	-56(%rbp), %rbx
	cmovbe	%r11, %rsi
	shrq	$2, %rdx
	cmpq	%rbx, %rdx
	jbe	.L3543
	movq	%rbx, %rdx
	movq	%r11, %rbx
	salq	$2, %rdx
	subq	%rdx, %rbx
	cmpq	%rax, %rbx
	cmovnb	%rbx, %rax
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
.L3543:
	leaq	8(%r11), %rdi
	cmpq	%rsi, %rax
	jnb	.L3590
	movabsq	$3866266742567714048, %r8
	movq	-120(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L3545:
	movq	%r13, %rdx
	movq	%rax, %rcx
	andq	%rax, %rdx
	shrq	$3, %rcx
	movq	(%r12,%rdx), %rbx
	andl	$3, %ecx
	imulq	%r8, %rbx
	movq	%rbx, %rdx
	shrq	$44, %rdx
	addq	%rcx, %rdx
	movl	%eax, 40(%r9,%rdx,4)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L3545
.L3590:
	movq	-72(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L3544
	.p2align 4,,10
	.p2align 3
.L3522:
	movq	-168(%rbp), %rax
	xorl	%edx, %edx
	leaq	16(%rax), %rsi
	movl	%r8d, 4(%rax)
	movq	%rsi, -72(%rbp)
	movl	-64(%rbp), %esi
	movl	%esi, (%rax)
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	.L3530
	.p2align 4,,10
	.p2align 3
.L3792:
	andq	%r13, %rsi
	leaq	(%r12,%rsi), %rax
	xorl	%esi, %esi
	movq	%rax, -176(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -80(%rbp)
.L3514:
	movq	-176(%rbp), %rdi
	movq	(%r9,%rsi), %rax
	movq	(%rdi,%rsi), %rdi
	cmpq	%rdi, %rax
	je	.L3809
	xorq	%rdi, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
	movq	%rax, -80(%rbp)
.L3516:
	cmpq	$3, %r15
	movl	$3, %esi
	cmovnb	%r15, %rsi
	cmpq	%rsi, -80(%rbp)
	jbe	.L3512
	movq	-80(%rbp), %rax
	bsrl	%edx, %edx
	imull	$30, %edx, %edx
	leaq	(%rax,%rax,8), %rdi
	movq	%rdi, %rsi
	salq	$4, %rsi
	subq	%rdi, %rsi
	addq	$1920, %rsi
	subq	%rdx, %rsi
	cmpq	-96(%rbp), %rsi
	jbe	.L3512
	movq	%rsi, -96(%rbp)
	movq	%rax, %r15
	movq	%rcx, -56(%rbp)
	jmp	.L3512
	.p2align 4,,10
	.p2align 3
.L3793:
	movq	-96(%rbp), %rax
	subq	$1, -136(%rbp)
	movq	%r15, -216(%rbp)
	movq	56(%r14), %r10
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	%rbx, -144(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L3520
	.p2align 4,,10
	.p2align 3
.L3503:
	testq	%r8, %r8
	je	.L3502
	movzbl	(%r9), %eax
	cmpb	%al, (%rdx)
	jne	.L3502
	cmpq	$0, -224(%rbp)
	movq	-248(%rbp), %r11
	je	.L3502
	movzbl	1(%r9), %eax
	movl	$1, %r10d
	cmpb	%al, 1(%rdx)
	jne	.L3502
.L3557:
	cmpq	$0, -240(%rbp)
	leaq	1(%r10), %rax
	je	.L3507
	movzbl	1(%r10,%rdx), %esi
	cmpb	%sil, 1(%r11)
	jne	.L3507
	cmpq	$0, -272(%rbp)
	leaq	2(%r10), %rax
	je	.L3507
	movzbl	2(%r10,%rdx), %esi
	cmpb	%sil, 2(%r11)
	jne	.L3507
	cmpq	$0, -296(%rbp)
	leaq	3(%r10), %rax
	je	.L3507
	movzbl	3(%r10,%rdx), %esi
	cmpb	%sil, 3(%r11)
	jne	.L3507
	cmpq	$0, -304(%rbp)
	leaq	4(%r10), %rax
	je	.L3507
	movzbl	4(%r10,%rdx), %esi
	cmpb	%sil, 4(%r11)
	jne	.L3507
	cmpq	$0, -312(%rbp)
	leaq	5(%r10), %rax
	je	.L3507
	movzbl	5(%r10,%rdx), %esi
	cmpb	%sil, 5(%r11)
	jne	.L3507
	cmpq	$0, -320(%rbp)
	leaq	6(%r10), %rax
	je	.L3507
	movzbl	6(%r11), %esi
	cmpb	%sil, 6(%r10,%rdx)
	jne	.L3507
	leaq	7(%r10), %rax
	jmp	.L3507
	.p2align 4,,10
	.p2align 3
.L3791:
	movq	-232(%rbp), %rax
	leaq	(%rax,%rsi), %r10
	leaq	8(%rsi), %rax
	subq	$1, %r11
	je	.L3810
	movq	%rax, %rsi
	jmp	.L3505
.L3804:
	leal	0(,%r9,4), %ecx
	movl	$158663784, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L3526
	.p2align 4,,10
	.p2align 3
.L3808:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L3536
	.p2align 4,,10
	.p2align 3
.L3790:
	testq	%rdi, %rdi
	je	.L3496
	xorl	%ecx, %ecx
	leaq	8(%r9), %r11
.L3498:
	movq	(%r9,%rcx), %rdx
	movq	(%rax,%rcx), %rsi
	cmpq	%rsi, %rdx
	je	.L3811
	xorq	%rsi, %rdx
	leaq	-1(%r8), %rax
	rep bsfq	%rdx, %rdx
	movq	%rax, -224(%rbp)
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rcx, %rdx
.L3500:
	cmpq	$3, %rdx
	jbe	.L3580
	leaq	(%rdx,%rdx,8), %rcx
	movq	%rcx, %rax
	salq	$4, %rax
	subq	%rcx, %rax
	addq	$1935, %rax
	movq	%rax, -96(%rbp)
	cmpq	$2020, %rax
	jbe	.L3580
	movzbl	(%r9,%rdx), %eax
	movq	%rdx, %r15
	movl	%eax, -184(%rbp)
	jmp	.L3495
.L3580:
	movq	$0, -56(%rbp)
	movq	$2020, -96(%rbp)
	jmp	.L3495
	.p2align 4,,10
	.p2align 3
.L3797:
	movq	-64(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L3532
.L3538:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L3541
.L3798:
	movq	-64(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L3532
.L3588:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L3532
.L3810:
	movq	%r10, %r11
	testq	%r8, %r8
	je	.L3507
	movzbl	(%rdx,%rax), %r10d
	cmpb	%r10b, (%r11)
	jne	.L3507
	cmpq	$0, -224(%rbp)
	movq	%r11, %rax
	leaq	9(%rsi), %r10
	leaq	1(%r11), %r11
	je	.L3583
	movzbl	9(%rsi,%rdx), %esi
	cmpb	%sil, 1(%rax)
	je	.L3557
.L3583:
	movq	%r10, %rax
	jmp	.L3507
	.p2align 4,,10
	.p2align 3
.L3800:
	subq	$1, -184(%rbp)
	movq	-128(%rbp), %rdx
	leaq	(%rdx,%r8), %r9
	leaq	8(%r8), %rdx
	je	.L3812
	movq	%rdx, %r8
	jmp	.L3489
.L3803:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L3524
.L3591:
	movq	%rdx, -64(%rbp)
	movq	%rax, %r11
	jmp	.L3544
.L3805:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L3526
.L3801:
	testq	%r8, %r8
	je	.L3482
	movzbl	(%rdx,%rax), %r14d
	cmpb	%r14b, (%rbx)
	jne	.L3482
	cmpq	$0, -176(%rbp)
	movq	%rbx, %rax
	leaq	1(%rbx), %r14
	leaq	9(%rdi), %rbx
	je	.L3572
	movzbl	9(%rdi,%rdx), %edi
	cmpb	%dil, 1(%rax)
	je	.L3559
.L3572:
	movq	%rbx, %rax
	jmp	.L3482
.L3802:
	leaq	(%rbx,%rcx), %rdx
	leaq	8(%rcx), %r15
	subq	$1, %r9
	je	.L3813
	movq	%r15, %rcx
	jmp	.L3473
.L3471:
	testq	%r8, %r8
	je	.L3567
	movq	%r8, %rbx
	leaq	1(%rsi), %rdi
	subq	$1, %rbx
	movq	%rbx, -176(%rbp)
	je	.L3569
	movzbl	1(%rsi), %ebx
	movl	$1, %edx
	cmpb	%bl, 1(%rax)
	jne	.L3569
.L3560:
	movq	-176(%rbp), %rbx
	leaq	1(%rdx), %r15
	cmpq	$1, %rbx
	je	.L3475
	movzbl	1(%rax,%rdx), %ecx
	cmpb	%cl, 1(%rdi)
	jne	.L3475
	leaq	2(%rdx), %r15
	cmpq	$2, %rbx
	je	.L3475
	movzbl	2(%rax,%rdx), %ecx
	cmpb	%cl, 2(%rdi)
	jne	.L3475
	leaq	3(%rdx), %r15
	cmpq	$3, %rbx
	je	.L3475
	movzbl	3(%rax,%rdx), %ecx
	cmpb	%cl, 3(%rdi)
	jne	.L3475
	subq	$4, %rbx
	leaq	4(%rdx), %r15
	movq	%rbx, %rcx
	je	.L3475
	movzbl	4(%rax,%rdx), %ebx
	cmpb	%bl, 4(%rdi)
	jne	.L3475
	leaq	5(%rdx), %r15
	cmpq	$1, %rcx
	je	.L3475
	movzbl	5(%rax,%rdx), %eax
	cmpb	%al, 5(%rdi)
	jne	.L3475
	leaq	6(%rdx), %r15
	jmp	.L3475
.L3563:
	xorl	%edx, %edx
	jmp	.L3469
.L3518:
	movq	-144(%rbp), %r11
	movq	-152(%rbp), %rax
	movq	-216(%rbp), %r15
	cmpq	%rax, %r11
	movq	%r11, %rbx
	cmovbe	%r11, %rax
	movq	%r15, %r8
	movq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -56(%rbp)
	jmp	.L3519
.L3809:
	subq	$1, -80(%rbp)
	movq	-184(%rbp), %rax
	leaq	(%rax,%rsi), %rdi
	leaq	8(%rsi), %rax
	je	.L3814
	movq	%rax, %rsi
	jmp	.L3514
.L3496:
	testq	%rsi, %rsi
	je	.L3578
	movzbl	(%rax), %esi
	cmpb	%sil, (%r9)
	jne	.L3576
	movq	%r8, %rdi
	leaq	1(%r9), %rsi
	subq	$1, %rdi
	movq	%rdi, -224(%rbp)
	je	.L3580
	movzbl	1(%r9), %edi
	movl	$1, %ecx
	cmpb	%dil, 1(%rax)
	jne	.L3580
.L3558:
	movq	-224(%rbp), %rdi
	leaq	1(%rcx), %rdx
	cmpq	$1, %rdi
	je	.L3500
	movzbl	1(%rcx,%rax), %r11d
	cmpb	%r11b, 1(%rsi)
	jne	.L3500
	leaq	2(%rcx), %rdx
	cmpq	$2, %rdi
	je	.L3500
	movzbl	2(%rcx,%rax), %r11d
	cmpb	%r11b, 2(%rsi)
	jne	.L3500
	leaq	3(%rcx), %rdx
	cmpq	$3, %rdi
	je	.L3500
	movzbl	3(%rcx,%rax), %r11d
	cmpb	%r11b, 3(%rsi)
	jne	.L3500
	leaq	4(%rcx), %rdx
	subq	$4, %rdi
	je	.L3500
	movzbl	4(%rcx,%rax), %r11d
	cmpb	%r11b, 4(%rsi)
	jne	.L3500
	leaq	5(%rcx), %rdx
	cmpq	$1, %rdi
	je	.L3500
	movzbl	5(%rcx,%rax), %eax
	cmpb	%al, 5(%rsi)
	jne	.L3500
	leaq	6(%rcx), %rdx
	jmp	.L3500
.L3811:
	leaq	(%r11,%rcx), %rsi
	leaq	8(%rcx), %rdx
	subq	$1, %rdi
	je	.L3815
	movq	%rdx, %rcx
	jmp	.L3498
.L3586:
	movl	$2, %edx
	jmp	.L3524
.L3806:
	movl	$3, %edx
	jmp	.L3524
.L3812:
	cmpq	$0, -96(%rbp)
	movq	%rbx, -56(%rbp)
	je	.L3491
	movzbl	(%rax,%rdx), %ebx
	cmpb	%bl, (%r9)
	jne	.L3491
	cmpq	$0, -176(%rbp)
	leaq	9(%r8), %rdx
	je	.L3491
	movzbl	9(%rax,%r8), %ebx
	cmpb	%bl, 1(%r9)
	jne	.L3491
	cmpq	$0, -200(%rbp)
	leaq	10(%r8), %rdx
	je	.L3491
	movzbl	10(%rax,%r8), %ebx
	cmpb	%bl, 2(%r9)
	jne	.L3491
	cmpq	$0, -216(%rbp)
	leaq	11(%r8), %rdx
	je	.L3491
	movzbl	11(%rax,%r8), %ebx
	cmpb	%bl, 3(%r9)
	jne	.L3491
	cmpq	$0, -224(%rbp)
	leaq	12(%r8), %rdx
	je	.L3491
	movzbl	12(%rax,%r8), %ebx
	cmpb	%bl, 4(%r9)
	jne	.L3491
	cmpq	$0, -232(%rbp)
	leaq	13(%r8), %rdx
	je	.L3491
	movzbl	13(%rax,%r8), %ebx
	cmpb	%bl, 5(%r9)
	jne	.L3491
	cmpq	$0, -240(%rbp)
	leaq	14(%r8), %rdx
	je	.L3491
	movzbl	14(%rax,%r8), %ebx
	cmpb	%bl, 6(%r9)
	jne	.L3491
	cmpq	$0, -248(%rbp)
	leaq	15(%r8), %rdx
	je	.L3491
	movzbl	7(%r9), %ebx
	cmpb	%bl, 15(%rax,%r8)
	jne	.L3491
	leaq	16(%r8), %rdx
	jmp	.L3491
.L3813:
	testq	%r8, %r8
	je	.L3816
	movzbl	(%rax,%r15), %ebx
	cmpb	%bl, (%rdx)
	jne	.L3817
	movq	%r8, %rbx
	leaq	1(%rdx), %rdi
	leaq	9(%rcx), %r15
	subq	$1, %rbx
	movq	%rbx, -176(%rbp)
	je	.L3475
	movzbl	9(%rax,%rcx), %ebx
	cmpb	%bl, 1(%rdx)
	jne	.L3475
	movq	%r15, %rdx
	jmp	.L3560
.L3567:
	movq	$0, -112(%rbp)
	xorl	%r15d, %r15d
	movq	$2020, -72(%rbp)
	movq	$-1, -176(%rbp)
	jmp	.L3470
.L3814:
	movq	%rax, -80(%rbp)
	movq	%rdi, -192(%rbp)
	testq	%r8, %r8
	je	.L3516
	movq	-176(%rbp), %rdi
	movzbl	(%rdi,%rax), %edi
	movq	-192(%rbp), %rax
	cmpb	%dil, (%rax)
	jne	.L3516
	leaq	9(%rsi), %rax
	cmpq	$0, -224(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3516
	movq	-176(%rbp), %rdi
	movq	-192(%rbp), %rax
	movzbl	9(%rdi,%rsi), %edi
	cmpb	%dil, 1(%rax)
	jne	.L3516
	leaq	10(%rsi), %rax
	cmpq	$0, -240(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3516
	movq	-176(%rbp), %rdi
	movq	-192(%rbp), %rax
	movzbl	10(%rdi,%rsi), %edi
	cmpb	%dil, 2(%rax)
	jne	.L3516
	leaq	11(%rsi), %rax
	cmpq	$0, -272(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3516
	movq	-176(%rbp), %rdi
	movq	-192(%rbp), %rax
	movzbl	11(%rdi,%rsi), %edi
	cmpb	%dil, 3(%rax)
	jne	.L3516
	leaq	12(%rsi), %rax
	cmpq	$0, -296(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3516
	movq	-176(%rbp), %rdi
	movq	-192(%rbp), %rax
	movzbl	12(%rdi,%rsi), %edi
	cmpb	%dil, 4(%rax)
	jne	.L3516
	leaq	13(%rsi), %rax
	cmpq	$0, -304(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3516
	movq	-176(%rbp), %rdi
	movq	-192(%rbp), %rax
	movzbl	13(%rdi,%rsi), %edi
	cmpb	%dil, 5(%rax)
	jne	.L3516
	leaq	14(%rsi), %rax
	cmpq	$0, -312(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3516
	movq	-176(%rbp), %rdi
	movq	-192(%rbp), %rax
	movzbl	14(%rdi,%rsi), %edi
	cmpb	%dil, 6(%rax)
	jne	.L3516
	leaq	15(%rsi), %rax
	cmpq	$0, -320(%rbp)
	movq	%rax, -80(%rbp)
	je	.L3516
	movq	-192(%rbp), %rax
	movq	-176(%rbp), %rdi
	movzbl	7(%rax), %eax
	cmpb	%al, 15(%rdi,%rsi)
	jne	.L3516
	leaq	16(%rsi), %rax
	movq	%rax, -80(%rbp)
	jmp	.L3516
.L3578:
	movq	$0, -56(%rbp)
	movq	$2020, -96(%rbp)
	movq	$-1, -224(%rbp)
	jmp	.L3495
.L3815:
	testq	%r8, %r8
	je	.L3818
	movzbl	(%rax,%rdx), %edi
	cmpb	%dil, (%rsi)
	jne	.L3819
	movq	%r8, %r11
	movq	%rsi, %rdi
	leaq	9(%rcx), %rdx
	subq	$1, %r11
	leaq	1(%rsi), %rsi
	movq	%r11, -224(%rbp)
	je	.L3500
	movzbl	9(%rcx,%rax), %ecx
	cmpb	%cl, 1(%rdi)
	jne	.L3500
	movq	%rdx, %rcx
	jmp	.L3558
.L3795:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L3530
.L3819:
	leaq	-1(%r8), %rax
	movq	%rax, -224(%rbp)
	jmp	.L3500
.L3818:
	movq	$-1, -224(%rbp)
	jmp	.L3500
.L3817:
	leaq	-1(%r8), %rax
	movq	%rax, -176(%rbp)
	jmp	.L3475
.L3816:
	movq	$-1, -176(%rbp)
	jmp	.L3475
	.cfi_endproc
.LFE290:
	.size	CreateBackwardReferencesNH55, .-CreateBackwardReferencesNH55
	.p2align 4
	.type	CreateBackwardReferencesNH65, @function
CreateBackwardReferencesNH65:
.LFB291:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	movl	$256, %edx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$240, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -168(%rbp)
	movl	8(%r8), %ecx
	movq	40(%r9), %r12
	salq	%cl, %rax
	subq	$16, %rax
	movq	%rax, -208(%rbp)
	movq	24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	leaq	(%r14,%rdi), %rax
	movq	%rax, -104(%rbp)
	subq	$7, %rax
	cmpq	$7, %rdi
	cmovbe	%r14, %rax
	cmpl	$9, 4(%r8)
	movq	%rax, -336(%rbp)
	movl	$2048, %eax
	cmovl	%rdx, %rax
	movl	$64, %edx
	movq	%rax, -320(%rbp)
	movl	$512, %eax
	cmovl	%rdx, %rax
	movl	16(%r12), %edx
	movq	%rax, -344(%rbp)
	addq	%r14, %rax
	movq	%rax, -216(%rbp)
	cmpl	$4, %edx
	jle	.L3824
	movq	16(%rbp), %rax
	movq	16(%rbp), %rbx
	movl	(%rax), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rbx)
	leal	1(%rax), %ecx
	movl	%ecx, 20(%rbx)
	leal	-2(%rax), %ecx
	movl	%ecx, 24(%rbx)
	leal	2(%rax), %ecx
	movl	%ecx, 28(%rbx)
	leal	-3(%rax), %ecx
	addl	$3, %eax
	movl	%ecx, 32(%rbx)
	movl	%eax, 36(%rbx)
	cmpl	$10, %edx
	jg	.L4265
.L3824:
	leaq	8(%r14), %rax
	cmpq	-104(%rbp), %rax
	jnb	.L3959
.L4294:
	leaq	80(%r12), %rax
	movq	48(%r9), %r13
	movdqa	.LC0(%rip), %xmm1
	movq	%r12, %rbx
	movq	%rax, -80(%rbp)
	movq	32(%rbp), %rax
	movq	%rsi, %r12
	movq	%r13, -120(%rbp)
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L3939:
	movq	-208(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movslq	16(%rbx), %r8
	subq	%r14, %rdi
	movq	%rax, %r10
	cmpq	%r14, %rax
	movq	-168(%rbp), %rax
	cmova	%r14, %r10
	andq	%r14, %rsi
	movq	%rdi, -64(%rbp)
	movq	72(%rax), %rax
	leaq	(%r12,%rsi), %rcx
	movq	%rcx, -56(%rbp)
	movq	%rax, -232(%rbp)
	movq	40(%rbx), %rax
	leaq	80(%rax,%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%r8, %r8
	je	.L3960
	movq	%rdi, %rax
	andl	$7, %edi
	movq	%rbx, -96(%rbp)
	xorl	%r9d, %r9d
	shrq	$3, %rax
	movq	%rdi, %r11
	movq	$2020, -72(%rbp)
	movq	%rax, -136(%rbp)
	leaq	-1(%rdi), %rax
	leaq	1(%rcx), %rdi
	movq	%rdi, -184(%rbp)
	leaq	8(%rcx), %rdi
	movq	$0, -200(%rbp)
	movq	%rdi, -176(%rbp)
	movq	%r11, -152(%rbp)
	movq	16(%rbp), %r11
	movq	%rax, -160(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3839:
	movslq	(%r11,%rax,4), %rdx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%r14, %rcx
	jnb	.L3828
	cmpq	%r10, %rdx
	ja	.L3828
	leaq	(%rsi,%r9), %rdi
	cmpq	%rdi, %r15
	jb	.L3828
	andq	%r15, %rcx
	leaq	(%rcx,%r9), %rbx
	cmpq	%rbx, %r15
	jb	.L3828
	movzbl	(%r12,%rbx), %ebx
	cmpb	%bl, (%r12,%rdi)
	je	.L4266
	.p2align 4,,10
	.p2align 3
.L3828:
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L3839
	movq	-96(%rbp), %rbx
	movq	%r9, -96(%rbp)
.L3827:
	movq	-56(%rbp), %r11
	movq	64(%rbx), %rax
	movl	56(%rbx), %ecx
	movq	(%r11), %rdi
	movq	%rax, -184(%rbp)
	andq	%rdi, %rax
	movq	%rdi, -136(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rax, %rdx
	movabsq	$2297779722762296275, %rax
	imulq	%rdx, %rax
	shrq	%cl, %rax
	movl	8(%rbx), %ecx
	movl	%eax, %edx
	movl	%eax, %eax
	sall	%cl, %edx
	movq	-80(%rbp), %rcx
	leaq	(%rdi,%rdx,4), %rdi
	movl	$0, %edx
	leaq	(%rcx,%rax,2), %rax
	movq	48(%rbx), %rcx
	addq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	movzwl	(%rax), %eax
	movq	%rcx, -224(%rbp)
	movq	%rax, %r8
	movw	%ax, -152(%rbp)
	subq	%rcx, %r8
	cmpq	%rcx, %rax
	movl	72(%rbx), %ecx
	cmovbe	%rdx, %r8
	movl	%ecx, -160(%rbp)
	cmpq	%r8, %rax
	jbe	.L3841
	movl	%ecx, %r13d
	movq	-64(%rbp), %rcx
	movq	%rbx, -128(%rbp)
	movq	%rcx, %rdx
	andl	$7, %ecx
	movq	%rcx, -248(%rbp)
	subq	$1, %rcx
	shrq	$3, %rdx
	movq	%rcx, -256(%rbp)
	leaq	1(%r11), %rcx
	movq	%rcx, -272(%rbp)
	leaq	8(%r11), %rcx
	movq	%rcx, -264(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rdx, -192(%rbp)
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L3842:
	cmpq	%r8, %rax
	jbe	.L4250
.L3849:
	subq	$1, %rax
	movq	%rax, %rdx
	andq	%r13, %rdx
	movl	(%rdi,%rdx,4), %r9d
	movq	%r14, %rdx
	subq	%r9, %rdx
	cmpq	%r10, %rdx
	ja	.L4250
	leaq	(%rsi,%rcx), %r11
	cmpq	%r11, %r15
	jb	.L3842
	andq	%r15, %r9
	leaq	(%r9,%rcx), %rbx
	cmpq	%rbx, %r15
	jb	.L3842
	movzbl	(%r12,%rbx), %ebx
	cmpb	%bl, (%r12,%r11)
	jne	.L3842
	leaq	(%r12,%r9), %rbx
	movq	%rbx, -176(%rbp)
	movq	-192(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L3843
	movq	%rbx, -96(%rbp)
	xorl	%r11d, %r11d
.L3845:
	movq	-56(%rbp), %rbx
	movq	(%rbx,%r11), %r9
	movq	-176(%rbp), %rbx
	movq	(%rbx,%r11), %rbx
	cmpq	%rbx, %r9
	je	.L4267
	xorq	%rbx, %r9
	rep bsfq	%r9, %r9
	movslq	%r9d, %r9
	shrq	$3, %r9
	leaq	(%r9,%r11), %rbx
	movq	%rbx, -96(%rbp)
.L3847:
	cmpq	$3, -96(%rbp)
	jbe	.L3842
	movq	-96(%rbp), %rbx
	leaq	(%rbx,%rbx,8), %rbx
	movq	%rbx, %r11
	salq	$4, %r11
	subq	%rbx, %r11
	bsrl	%edx, %ebx
	imull	$30, %ebx, %ebx
	addq	$1920, %r11
	subq	%rbx, %r11
	cmpq	-72(%rbp), %r11
	jbe	.L3842
	movq	%rdx, -200(%rbp)
	movq	-96(%rbp), %rcx
	movq	%r11, -72(%rbp)
	cmpq	%r8, %rax
	ja	.L3849
	.p2align 4,,10
	.p2align 3
.L4250:
	movq	%rcx, -96(%rbp)
	movq	-128(%rbp), %rbx
.L3841:
	movzwl	-152(%rbp), %esi
	movzwl	-160(%rbp), %eax
	leaq	1(%r14), %r9
	movl	%r14d, -128(%rbp)
	andl	%esi, %eax
	cmpq	$2020, -72(%rbp)
	movzwl	%ax, %eax
	movl	%r14d, (%rdi,%rax,4)
	movq	-144(%rbp), %rdi
	leal	1(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-120(%rbp), %rax
	movq	56(%rax), %r11
	movq	%r9, %rax
	je	.L4268
	cmpq	$31, -64(%rbp)
	jbe	.L4004
.L4262:
	movl	$0, -160(%rbp)
.L3943:
	cmpq	%r11, %r14
	jb	.L3859
	movq	-64(%rbp), %rdi
	movq	-120(%rbp), %r13
	movq	%rdi, %rsi
	andl	$7, %edi
	shrq	$3, %rsi
	movq	%rdi, -176(%rbp)
	movq	%rsi, -136(%rbp)
	movq	-56(%rbp), %rsi
	addq	$8, %rsi
	movq	%rsi, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L3865:
	leaq	32(%r11), %rdx
	movl	40(%r13), %ecx
	andq	%r15, %rdx
	movzbl	(%r12,%rdx), %esi
	movl	68(%r13), %edx
	imull	%ecx, %edx
	leal	1(%rsi,%rdx), %esi
	movq	%r15, %rdx
	andq	%r11, %rdx
	movzbl	(%r12,%rdx), %edx
	addl	$1, %edx
	imull	72(%r13), %edx
	subl	%edx, %esi
	movl	%esi, 40(%r13)
	testl	$1056964608, %ecx
	jne	.L3860
	movq	48(%r13), %rdx
	andl	$1073741823, %ecx
	leaq	(%rdx,%rcx,4), %rdx
	movl	(%rdx), %edi
	movl	%r11d, (%rdx)
	cmpq	%r11, %r14
	jne	.L3860
	movl	$4294967295, %esi
	cmpq	%rsi, %rdi
	je	.L3860
	movl	-128(%rbp), %esi
	subl	%edi, %esi
	movl	%esi, %ecx
	cmpq	%r10, %rsi
	jbe	.L4269
	.p2align 4,,10
	.p2align 3
.L3860:
	addq	$1, %r11
	cmpq	%r14, %r11
	jbe	.L3865
.L3859:
	movq	-120(%rbp), %rdi
	cmpq	$2020, -72(%rbp)
	movq	%r9, %r11
	movq	%r9, 56(%rdi)
	jne	.L3942
.L3866:
	movq	-88(%rbp), %rsi
	leaq	1(%rsi), %rdx
	cmpq	%r9, -216(%rbp)
	jnb	.L4263
	movq	-104(%rbp), %rsi
	movq	-216(%rbp), %rcx
	addq	-320(%rbp), %rcx
	leaq	-7(%rsi), %r8
	cmpq	%r9, %rcx
	jnb	.L3936
	leaq	17(%r14), %rdi
	movq	-88(%rbp), %rsi
	movabsq	$2297779722762296275, %r10
	cmpq	%r8, %rdi
	cmova	%r8, %rdi
	leaq	4(%rsi), %r8
	subq	%r14, %r8
	cmpq	%r9, %rdi
	jbe	.L4263
	movq	-112(%rbp), %r13
	movq	-184(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L3937:
	movq	%r15, %rdx
	movl	56(%rbx), %ecx
	andq	%rax, %rdx
	movq	(%r12,%rdx), %rsi
	andq	%r14, %rsi
	movq	%rsi, %rdx
	movq	-80(%rbp), %rsi
	imulq	%r10, %rdx
	shrq	%cl, %rdx
	movl	%edx, %ecx
	leaq	(%rsi,%rcx,2), %r11
	movzwl	72(%rbx), %esi
	movl	8(%rbx), %ecx
	movzwl	(%r11), %r9d
	sall	%cl, %edx
	andl	%r9d, %esi
	addl	$1, %r9d
	movzwl	%si, %esi
	addq	%rsi, %rdx
	leaq	(%rbx,%rdx,4), %rdx
	movl	%eax, (%rdx,%r13)
	leaq	(%r8,%rax), %rdx
	movw	%r9w, (%r11)
	movq	%rax, %r9
	addq	$4, %rax
	cmpq	%rdi, %rax
	jb	.L3937
	leaq	12(%r9), %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, %r14
	movq	%rdi, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L3933:
	movq	-56(%rbp), %rdi
	cmpq	%rdi, -104(%rbp)
	ja	.L3939
	movq	-240(%rbp), %rdx
	subq	32(%rbp), %rdx
	sarq	$4, %rdx
.L3826:
	movq	24(%rbp), %rbx
	movq	-104(%rbp), %rax
	addq	-88(%rbp), %rax
	subq	%r14, %rax
	movq	%rax, (%rbx)
	movq	40(%rbp), %rax
	addq	%rdx, (%rax)
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4266:
	.cfi_restore_state
	leaq	(%r12,%rcx), %rbx
	movq	%rbx, -144(%rbp)
	movq	-136(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L3829
	movq	-96(%rbp), %r13
	movq	%rbx, -128(%rbp)
	xorl	%ebx, %ebx
.L3831:
	movq	-56(%rbp), %rdi
	movq	(%rdi,%rbx), %rcx
	movq	-144(%rbp), %rdi
	movq	(%rdi,%rbx), %rdi
	cmpq	%rdi, %rcx
	je	.L4270
	xorq	%rcx, %rdi
	movq	%r13, -96(%rbp)
	rep bsfq	%rdi, %rdi
	movslq	%edi, %rdi
	shrq	$3, %rdi
	addq	%rdi, %rbx
	movq	%rbx, -128(%rbp)
.L3833:
	cmpq	$2, -128(%rbp)
	jbe	.L3837
.L3951:
	movq	-128(%rbp), %rbx
.L4261:
	leaq	(%rbx,%rbx,8), %rcx
	movq	%rcx, %rbx
	salq	$4, %rbx
	subq	%rcx, %rbx
	leaq	1935(%rbx), %r13
.L3838:
	movq	-72(%rbp), %rdi
	cmpq	%r13, %rdi
	jnb	.L3828
	testq	%rax, %rax
	je	.L3964
	movl	%eax, %ecx
	movl	$117264, %ebx
	andl	$14, %ecx
	sarl	%cl, %ebx
	movl	%ebx, %ecx
	movq	$-39, %rbx
	andl	$14, %ecx
	subq	%rcx, %rbx
	leaq	0(%r13,%rbx), %rbx
	cmpq	%rdi, %rbx
	jbe	.L3828
	movq	%rdx, -200(%rbp)
	movq	-128(%rbp), %r9
	movq	%rbx, -72(%rbp)
	jmp	.L3828
	.p2align 4,,10
	.p2align 3
.L4263:
	leaq	9(%r14), %rax
	movq	%rdx, -88(%rbp)
	movq	%r9, %r14
	movq	%rax, -56(%rbp)
	jmp	.L3933
	.p2align 4,,10
	.p2align 3
.L4269:
	andq	%r15, %rdi
	addq	%r12, %rdi
	movq	%rdi, -152(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	xorl	%edi, %edi
.L3862:
	movq	-56(%rbp), %rdx
	movq	-152(%rbp), %r8
	movq	(%rdx,%rdi), %rdx
	movq	(%r8,%rdi), %r8
	cmpq	%r8, %rdx
	je	.L4271
	xorq	%r8, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %rdi
	movq	%rdi, -144(%rbp)
.L3864:
	movq	-96(%rbp), %r8
	movl	$3, %edi
	cmpq	$3, %r8
	cmovnb	%r8, %rdi
	cmpq	-144(%rbp), %rdi
	jnb	.L3860
	movq	-144(%rbp), %rdx
	bsrl	%ecx, %ecx
	imull	$30, %ecx, %ecx
	leaq	(%rdx,%rdx,8), %r8
	movq	%r8, %rdi
	salq	$4, %rdi
	subq	%r8, %rdi
	addq	$1920, %rdi
	subq	%rcx, %rdi
	cmpq	-72(%rbp), %rdi
	jbe	.L3860
	movq	%rdi, -72(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rdx, -96(%rbp)
	movl	$0, -160(%rbp)
	jmp	.L3860
	.p2align 4,,10
	.p2align 3
.L4268:
	movq	24(%rbx), %rdx
	movq	%rdx, %rcx
	shrq	$7, %rcx
	cmpq	%rcx, 32(%rbx)
	jb	.L3851
	movq	-56(%rbp), %rdi
	movq	%r9, -176(%rbp)
	leaq	2(%rdx), %r8
	movl	$0, -160(%rbp)
	imull	$506832829, (%rdi), %ecx
	movq	-168(%rbp), %rdi
	movq	%r12, -264(%rbp)
	movq	%r15, -272(%rbp)
	movq	104(%rdi), %rsi
	movq	%r9, -280(%rbp)
	shrl	$18, %ecx
	movq	%r11, -192(%rbp)
	addl	%ecx, %ecx
	movq	%r10, -248(%rbp)
	subq	%rdx, %rcx
	movq	%r14, -256(%rbp)
	movq	%rdi, %r14
	leaq	(%rsi,%rcx,2), %rsi
	leaq	1(%r10), %rcx
	movq	%r8, %r10
	movq	%rcx, -152(%rbp)
	movq	%rsi, %r11
	.p2align 4,,10
	.p2align 3
.L3857:
	movzwl	(%r11,%rdx,2), %esi
	addq	$1, %rdx
	movq	%rdx, 24(%rbx)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L3852
	movl	%esi, %eax
	movq	%rsi, %rdi
	andl	$31, %eax
	andl	$31, %edi
	movw	%ax, -144(%rbp)
	cmpq	%rdi, -64(%rbp)
	jb	.L3852
	movq	80(%r14), %r9
	shrq	$5, %rsi
	movq	%rdi, %rax
	movq	%rdi, %r13
	imulq	%rsi, %rax
	movl	32(%r9,%rdi,4), %r8d
	addq	%rax, %r8
	addq	168(%r9), %r8
	shrq	$3, %r13
	je	.L3853
	movq	(%r8), %r12
	movq	-136(%rbp), %rax
	xorl	%r15d, %r15d
	cmpq	%r12, %rax
	je	.L4272
.L3854:
	xorq	%r12, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rax, %r15
.L3856:
	movl	88(%r14), %eax
	addq	%r15, %rax
	cmpq	%rax, %rdi
	jnb	.L3852
	testq	%r15, %r15
	je	.L3852
	movq	%rdi, %rax
	movq	96(%r14), %r8
	addq	-152(%rbp), %rsi
	subq	%r15, %rax
	leal	(%rax,%rax,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %r8
	movq	%r8, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rax,4), %rax
	movzbl	(%r9,%rdi), %ecx
	salq	%cl, %rax
	addq	%rsi, %rax
	cmpq	%rax, -232(%rbp)
	jb	.L3852
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rcx
	salq	$4, %rcx
	subq	%rsi, %rcx
	bsrl	%eax, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rcx
	subq	%rsi, %rcx
	cmpq	-72(%rbp), %rcx
	jb	.L3852
	movzwl	-144(%rbp), %esi
	addq	$1, 32(%rbx)
	movq	%rcx, -72(%rbp)
	subl	%r15d, %esi
	movq	%rax, -200(%rbp)
	movl	%esi, -160(%rbp)
	movq	%r15, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L3852:
	cmpq	%r10, %rdx
	jne	.L3857
	cmpq	$31, -64(%rbp)
	movq	-176(%rbp), %rax
	movq	-192(%rbp), %r11
	movq	-248(%rbp), %r10
	movq	-256(%rbp), %r14
	movq	-264(%rbp), %r12
	movq	-272(%rbp), %r15
	movq	-280(%rbp), %r9
	ja	.L3943
	cmpq	$2020, -72(%rbp)
	je	.L3866
	.p2align 4,,10
	.p2align 3
.L3942:
	movq	-64(%rbp), %rax
	movq	-120(%rbp), %r13
	movq	%r11, -304(%rbp)
	movq	%r14, -216(%rbp)
	subq	$1, %rax
	movq	%rbx, -152(%rbp)
	movq	%r12, %rbx
	movq	%rax, -56(%rbp)
	leaq	4(%r14), %rax
	movq	%rax, -280(%rbp)
.L3908:
	movq	-168(%rbp), %rax
	xorl	%esi, %esi
	cmpl	$4, 4(%rax)
	jg	.L3867
	movq	-96(%rbp), %rax
	leaq	-1(%rax), %rsi
	movq	-56(%rbp), %rax
	cmpq	%rax, %rsi
	cmova	%rax, %rsi
.L3867:
	movq	-208(%rbp), %rax
	movq	%r9, %r12
	movq	%r9, -192(%rbp)
	cmpq	%rax, %r9
	movq	%rax, %r14
	movq	-152(%rbp), %rax
	cmovbe	%r9, %r14
	andq	%r15, %r12
	movslq	16(%rax), %r8
	leaq	(%rbx,%r12), %rcx
	movq	%r12, -248(%rbp)
	movq	%rcx, -128(%rbp)
	testq	%r8, %r8
	je	.L3979
	movq	-56(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	$0, -144(%rbp)
	movq	16(%rbp), %r10
	movq	%rax, %rdi
	andl	$7, %eax
	movq	$2020, -136(%rbp)
	shrq	$3, %rdi
	movq	%rax, %r11
	movq	%r13, -176(%rbp)
	leaq	-1(%rax), %rax
	movq	%rdi, -264(%rbp)
	leaq	1(%rcx), %rdi
	movq	%rdi, -296(%rbp)
	leaq	8(%rcx), %rdi
	movq	%rdi, -312(%rbp)
	movq	%r11, -272(%rbp)
	movq	%rax, -288(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3880:
	movslq	(%r10,%rax,4), %rdx
	movq	%r9, %rcx
	subq	%rdx, %rcx
	cmpq	%r14, %rdx
	ja	.L3869
	cmpq	%rcx, %r9
	jbe	.L3869
	leaq	(%r12,%rsi), %rdi
	cmpq	%rdi, %r15
	jb	.L3869
	andq	%r15, %rcx
	leaq	(%rcx,%rsi), %r11
	cmpq	%r11, %r15
	jb	.L3869
	movzbl	(%rbx,%r11), %r11d
	cmpb	%r11b, (%rbx,%rdi)
	je	.L4273
	.p2align 4,,10
	.p2align 3
.L3869:
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L3880
	movq	-176(%rbp), %r13
.L3868:
	movq	-128(%rbp), %r11
	movq	-184(%rbp), %rdx
	movq	-152(%rbp), %r10
	movq	-112(%rbp), %rdi
	movq	(%r11), %rax
	movl	56(%r10), %ecx
	andq	%rax, %rdx
	movq	%rax, -256(%rbp)
	movabsq	$2297779722762296275, %rax
	imulq	%rdx, %rax
	shrq	%cl, %rax
	movl	8(%r10), %ecx
	movl	%eax, %edx
	movl	%eax, %eax
	sall	%cl, %edx
	movl	72(%r10), %ecx
	leaq	(%rdi,%rdx,4), %rdi
	movq	-80(%rbp), %rdx
	addq	%r10, %rdi
	movl	%ecx, -272(%rbp)
	leaq	(%rdx,%rax,2), %rax
	movq	-224(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movzwl	(%rax), %eax
	movq	%rax, %r8
	movw	%ax, -264(%rbp)
	subq	%rdx, %r8
	cmpq	%rax, %rdx
	movl	$0, %edx
	cmovnb	%rdx, %r8
	cmpq	%r8, %rax
	jbe	.L3882
	movl	%ecx, %r12d
	movq	-56(%rbp), %rcx
	movq	-248(%rbp), %r10
	movq	%r13, -248(%rbp)
	movq	%r12, %r13
	movq	%rcx, %rdx
	andl	$7, %ecx
	movq	%rcx, -328(%rbp)
	subq	$1, %rcx
	shrq	$3, %rdx
	movq	%rcx, -352(%rbp)
	leaq	1(%r11), %rcx
	movq	%rcx, -360(%rbp)
	leaq	8(%r11), %rcx
	movq	%rdx, -288(%rbp)
	movq	%rcx, -368(%rbp)
	jmp	.L3890
	.p2align 4,,10
	.p2align 3
.L3883:
	cmpq	%r8, %rax
	jbe	.L4251
.L3890:
	subq	$1, %rax
	movq	%rax, %rdx
	andq	%r13, %rdx
	movl	(%rdi,%rdx,4), %ecx
	movq	%r9, %rdx
	subq	%rcx, %rdx
	cmpq	%r14, %rdx
	ja	.L4251
	leaq	(%r10,%rsi), %r11
	cmpq	%r11, %r15
	jb	.L3883
	andq	%r15, %rcx
	leaq	(%rcx,%rsi), %r12
	cmpq	%r12, %r15
	jb	.L3883
	movzbl	(%rbx,%r12), %r12d
	cmpb	%r12b, (%rbx,%r11)
	jne	.L3883
	addq	%rbx, %rcx
	movq	%rcx, -312(%rbp)
	movq	-288(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L3884
	movq	%rcx, -296(%rbp)
	xorl	%r11d, %r11d
.L3886:
	movq	-128(%rbp), %rcx
	movq	-312(%rbp), %r12
	movq	(%rcx,%r11), %rcx
	movq	(%r12,%r11), %r12
	cmpq	%r12, %rcx
	je	.L4274
	xorq	%r12, %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%r11, %rcx
	movq	%rcx, -296(%rbp)
.L3888:
	movq	-296(%rbp), %rcx
	cmpq	$3, %rcx
	jbe	.L3883
	leaq	(%rcx,%rcx,8), %r12
	movq	%r12, %r11
	salq	$4, %r11
	subq	%r12, %r11
	bsrl	%edx, %r12d
	imull	$30, %r12d, %r12d
	addq	$1920, %r11
	subq	%r12, %r11
	cmpq	-136(%rbp), %r11
	jbe	.L3883
	movq	%rdx, -144(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -64(%rbp)
	movq	%r11, -136(%rbp)
	cmpq	%r8, %rax
	ja	.L3890
	.p2align 4,,10
	.p2align 3
.L4251:
	movq	-248(%rbp), %r13
.L3882:
	movzwl	-264(%rbp), %esi
	movzwl	-272(%rbp), %eax
	movl	%r9d, -248(%rbp)
	andl	%esi, %eax
	cmpq	$2020, -136(%rbp)
	movzwl	%ax, %eax
	movl	%r9d, (%rdi,%rax,4)
	leal	1(%rsi), %eax
	movq	-176(%rbp), %rsi
	movl	$0, -176(%rbp)
	movw	%ax, (%rsi)
	je	.L4275
.L3891:
	movq	-56(%rbp), %rax
	cmpq	$31, %rax
	jbe	.L3898
	movq	-304(%rbp), %r12
	cmpq	%r12, %r9
	jb	.L3899
	movq	%rax, %r8
	andl	$7, %eax
	movq	%rax, -256(%rbp)
	movq	-128(%rbp), %rax
	shrq	$3, %r8
	addq	$8, %rax
	movq	%rax, -264(%rbp)
	.p2align 4,,10
	.p2align 3
.L3905:
	leaq	32(%r12), %rax
	movl	40(%r13), %edx
	andq	%r15, %rax
	movzbl	(%rbx,%rax), %ecx
	movl	68(%r13), %eax
	imull	%edx, %eax
	leal	1(%rcx,%rax), %ecx
	movq	%r15, %rax
	andq	%r12, %rax
	movzbl	(%rbx,%rax), %eax
	addl	$1, %eax
	imull	72(%r13), %eax
	subl	%eax, %ecx
	movl	%ecx, 40(%r13)
	testl	$1056964608, %edx
	jne	.L3900
	movq	48(%r13), %rax
	andl	$1073741823, %edx
	leaq	(%rax,%rdx,4), %rax
	movl	(%rax), %esi
	movl	%r12d, (%rax)
	cmpq	%r12, %r9
	jne	.L3900
	movl	$4294967295, %eax
	cmpq	%rax, %rsi
	je	.L3900
	movl	-248(%rbp), %eax
	subl	%esi, %eax
	movl	%eax, %ecx
	movq	%rcx, %rdx
	cmpq	%r14, %rcx
	jbe	.L4276
	.p2align 4,,10
	.p2align 3
.L3900:
	addq	$1, %r12
	cmpq	%r12, %r9
	jnb	.L3905
.L3899:
	movq	-216(%rbp), %rax
	addq	$2, %rax
	movq	%rax, 56(%r13)
.L3898:
	movq	-72(%rbp), %rax
	addq	$175, %rax
	cmpq	-136(%rbp), %rax
	ja	.L3906
	addq	$1, -88(%rbp)
	cmpq	-280(%rbp), %r9
	je	.L4253
	movq	-216(%rbp), %rax
	addq	$9, %rax
	cmpq	%rax, -104(%rbp)
	ja	.L4277
.L4253:
	movq	%rbx, %r12
	movq	-152(%rbp), %rbx
	movq	%r14, %r8
.L3907:
	movq	-64(%rbp), %rsi
	movq	-192(%rbp), %rax
	addq	-344(%rbp), %rax
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, -216(%rbp)
	movq	-144(%rbp), %rax
	cmpq	%r8, %rax
	jbe	.L4278
	leaq	15(%rax), %rdx
.L3917:
	movq	-240(%rbp), %rsi
	leaq	16(%rsi), %rax
	movq	%rax, -72(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-176(%rbp), %eax
	sall	$25, %eax
	orl	-64(%rbp), %eax
	movl	%eax, 4(%rsi)
	movq	-168(%rbp), %rax
	movl	60(%rax), %esi
	movl	56(%rax), %edi
	leaq	16(%rsi), %rax
	movq	%rdi, %r8
	movq	%rsi, %r11
	cmpq	%rax, %rdx
	jb	.L4279
	leal	2(%rdi), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	subq	%rsi, %rax
	leaq	-16(%rdx,%rax), %rdx
	bsrl	%edx, %r9d
	movq	%rdx, %rsi
	leal	-1(%r9), %eax
	movl	%eax, %ecx
	movq	%rax, %r9
	subq	%rdi, %rax
	movl	$1, %edi
	shrq	%cl, %rsi
	movl	%r8d, %ecx
	sall	%cl, %edi
	andl	$1, %esi
	leal	-1(%rdi), %r10d
	leaq	-2(%rsi,%rax,2), %rdi
	addq	$2, %rsi
	sall	$10, %eax
	andq	%rdx, %r10
	salq	%cl, %rdi
	movl	%r9d, %ecx
	salq	%cl, %rsi
	leal	16(%r11,%r10), %r10d
	movl	%r8d, %ecx
	subq	%rsi, %rdx
	addl	%edi, %r10d
	orl	%r10d, %eax
	shrq	%cl, %rdx
	testw	$1023, %r10w
	sete	%sil
.L3919:
	movq	-240(%rbp), %rdi
	movw	%ax, 14(%rdi)
	movl	-176(%rbp), %eax
	addl	-64(%rbp), %eax
	cmpq	$5, -88(%rbp)
	movl	%edx, 8(%rdi)
	cltq
	jbe	.L4280
	cmpq	$129, -88(%rbp)
	jbe	.L4281
	cmpq	$2113, -88(%rbp)
	jbe	.L4282
	movq	-88(%rbp), %rcx
	cmpq	$6209, %rcx
	jbe	.L4000
	cmpq	$22594, %rcx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rcx
	sbbl	%edx, %edx
	addl	$23, %edx
	cmpq	$9, %rax
	ja	.L3924
.L4291:
	subl	$2, %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	jbe	.L4283
	.p2align 4,,10
	.p2align 3
.L3928:
	shrw	$3, %ax
	movzwl	%ax, %eax
.L3930:
	shrw	$3, %dx
	movzwl	%dx, %edx
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %eax
	movl	$5377344, %edx
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	shrl	%cl, %edx
	andl	$192, %edx
	leal	64(%rdx,%rax), %eax
	orl	%eax, %edi
	jmp	.L3931
	.p2align 4,,10
	.p2align 3
.L3837:
	cmpq	$2, -128(%rbp)
	jne	.L3828
	cmpq	$1, %rax
	ja	.L3828
	movq	$2, -128(%rbp)
	movl	$2205, %r13d
	jmp	.L3838
	.p2align 4,,10
	.p2align 3
.L4004:
	movl	$0, -160(%rbp)
	jmp	.L3942
	.p2align 4,,10
	.p2align 3
.L4273:
	leaq	(%rbx,%rcx), %rdi
	movq	%rdi, -256(%rbp)
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3870
	movq	%rdi, -328(%rbp)
	movq	-176(%rbp), %r13
	xorl	%r11d, %r11d
.L3872:
	movq	-128(%rbp), %rdi
	movq	(%rdi,%r11), %rcx
	movq	-256(%rbp), %rdi
	movq	(%rdi,%r11), %rdi
	cmpq	%rdi, %rcx
	je	.L4284
	xorq	%rcx, %rdi
	movq	%r13, -176(%rbp)
	rep bsfq	%rdi, %rdi
	movslq	%edi, %rdi
	shrq	$3, %rdi
	addq	%r11, %rdi
.L3874:
	cmpq	$2, %rdi
	jbe	.L3878
.L3947:
	leaq	(%rdi,%rdi,8), %rcx
	movq	%rcx, %r11
	salq	$4, %r11
	subq	%rcx, %r11
	leaq	1935(%r11), %r13
.L3879:
	cmpq	%r13, -136(%rbp)
	jnb	.L3869
	testq	%rax, %rax
	je	.L3983
	movl	%eax, %ecx
	movl	$117264, %r11d
	andl	$14, %ecx
	sarl	%cl, %r11d
	movl	%r11d, %ecx
	movq	$-39, %r11
	andl	$14, %ecx
	subq	%rcx, %r11
	leaq	0(%r13,%r11), %r11
	cmpq	-136(%rbp), %r11
	jbe	.L3869
	movq	%rdx, -144(%rbp)
	movq	%rdi, %rsi
	movq	%rdi, -64(%rbp)
	movq	%r11, -136(%rbp)
	jmp	.L3869
	.p2align 4,,10
	.p2align 3
.L3936:
	leaq	9(%r14), %rsi
	cmpq	%r8, %rsi
	movq	%rsi, -56(%rbp)
	cmovbe	%rsi, %r8
	cmpq	%r9, %r8
	jbe	.L4003
	movq	-88(%rbp), %rsi
	movq	-112(%rbp), %r13
	movabsq	$2297779722762296275, %r10
	leaq	2(%rsi), %r11
	subq	%r14, %r11
	movq	-184(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L3938:
	movq	%r15, %rdx
	movl	56(%rbx), %ecx
	movzwl	72(%rbx), %esi
	andq	%rax, %rdx
	movq	(%r12,%rdx), %rdi
	andq	%r14, %rdi
	movq	%rdi, %rdx
	movq	-80(%rbp), %rdi
	imulq	%r10, %rdx
	shrq	%cl, %rdx
	movl	%edx, %ecx
	leaq	(%rdi,%rcx,2), %r9
	movl	8(%rbx), %ecx
	movzwl	(%r9), %edi
	sall	%cl, %edx
	andl	%edi, %esi
	addl	$1, %edi
	movzwl	%si, %esi
	addq	%rsi, %rdx
	leaq	(%rbx,%rdx,4), %rdx
	movl	%eax, (%rdx,%r13)
	leaq	(%r11,%rax), %rdx
	movw	%di, (%r9)
	movq	%rax, %r9
	addq	$2, %rax
	cmpq	%r8, %rax
	jb	.L3938
	leaq	10(%r9), %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, %r14
	movq	%rdi, -56(%rbp)
	jmp	.L3933
	.p2align 4,,10
	.p2align 3
.L4278:
	movq	16(%rbp), %rsi
	movq	16(%rbp), %rdi
	movslq	(%rsi), %rdx
	movslq	4(%rdi), %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	cmpq	%rdx, %rax
	je	.L3910
	cmpq	%rcx, %rax
	je	.L4285
	addq	$3, %rax
	movq	%rax, %r10
	subq	%rdx, %r10
	cmpq	$6, %r10
	jbe	.L4286
	subq	%rcx, %rax
	cmpq	$6, %rax
	jbe	.L4287
	movq	16(%rbp), %rax
	movq	-144(%rbp), %rcx
	movslq	8(%rax), %rdx
	movq	%rdx, %rax
	cmpq	%rdx, %rcx
	je	.L3998
	movq	16(%rbp), %rdx
	movslq	12(%rdx), %rdx
	cmpq	%rdx, %rcx
	je	.L4288
	leaq	15(%rcx), %rdx
.L3914:
	testq	%rdx, %rdx
	je	.L3910
	movq	16(%rbp), %rax
	movl	8(%rax), %eax
.L3912:
	movq	16(%rbp), %rcx
	movl	%edi, 8(%rcx)
	movq	-144(%rbp), %rdi
	movl	%eax, 12(%rcx)
	movl	%esi, 4(%rcx)
	movl	%edi, (%rcx)
	movl	16(%rbx), %eax
	cmpl	$4, %eax
	jle	.L3917
	movd	%edi, %xmm2
	movq	16(%rbp), %rcx
	movq	16(%rbp), %r10
	pshufd	$0, %xmm2, %xmm0
	paddd	%xmm1, %xmm0
	movups	%xmm0, 16(%rcx)
	movl	%edi, %ecx
	subl	$3, %ecx
	movl	%ecx, 32(%r10)
	leal	3(%rdi), %ecx
	movl	%ecx, 36(%r10)
	cmpl	$10, %eax
	jle	.L3917
	movd	%esi, %xmm3
	leal	-3(%rsi), %eax
	addl	$3, %esi
	pshufd	$0, %xmm3, %xmm0
	movl	%eax, 56(%r10)
	paddd	%xmm1, %xmm0
	movl	%esi, 60(%r10)
	movups	%xmm0, 40(%r10)
	jmp	.L3917
	.p2align 4,,10
	.p2align 3
.L4276:
	andq	%r15, %rsi
	movq	%r8, %r11
	leaq	(%rbx,%rsi), %r10
	xorl	%esi, %esi
.L3902:
	movq	-128(%rbp), %rax
	movq	(%r10,%rsi), %rdi
	movq	(%rax,%rsi), %rax
	cmpq	%rdi, %rax
	je	.L4289
	xorq	%rdi, %rax
	rep bsfq	%rax, %rax
	cltq
	shrq	$3, %rax
	addq	%rsi, %rax
.L3904:
	movq	-64(%rbp), %rsi
	movl	$3, %edi
	cmpq	$3, %rsi
	cmovb	%rdi, %rsi
	cmpq	%rax, %rsi
	jnb	.L3900
	leaq	(%rax,%rax,8), %rdi
	bsrl	%edx, %edx
	movq	%rdi, %rsi
	imull	$30, %edx, %edx
	salq	$4, %rsi
	subq	%rdi, %rsi
	addq	$1920, %rsi
	subq	%rdx, %rsi
	cmpq	-136(%rbp), %rsi
	jbe	.L3900
	movq	%rsi, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rax, -64(%rbp)
	movl	$0, -176(%rbp)
	jmp	.L3900
	.p2align 4,,10
	.p2align 3
.L4275:
	movq	-152(%rbp), %r10
	movq	24(%r10), %rax
	movq	%rax, %rdx
	shrq	$7, %rdx
	cmpq	%rdx, 32(%r10)
	jb	.L3891
	movq	-128(%rbp), %rsi
	movq	%r14, -288(%rbp)
	leaq	2(%rax), %rdi
	movq	%r15, -328(%rbp)
	imull	$506832829, (%rsi), %edx
	movq	-168(%rbp), %rsi
	movq	%r9, -352(%rbp)
	movq	%r13, -296(%rbp)
	movq	104(%rsi), %rcx
	movq	%rbx, -312(%rbp)
	movq	%rsi, %r13
	movq	%rdi, %rbx
	shrl	$18, %edx
	addl	%edx, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,2), %r11
	leaq	1(%r14), %rcx
	movq	%rcx, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L3897:
	movzwl	(%r11,%rax,2), %esi
	addq	$1, %rax
	movq	%rax, 24(%r10)
	movq	%rsi, %rcx
	testq	%rsi, %rsi
	je	.L3892
	movl	%esi, %edi
	andl	$31, %edi
	movw	%di, -264(%rbp)
	movq	%rsi, %rdi
	andl	$31, %edi
	cmpq	%rdi, -56(%rbp)
	jb	.L3892
	movq	80(%r13), %r9
	shrq	$5, %rsi
	movq	%rdi, %rdx
	movq	%rdi, %r14
	imulq	%rsi, %rdx
	movl	32(%r9,%rdi,4), %r8d
	addq	%rdx, %r8
	addq	168(%r9), %r8
	shrq	$3, %r14
	je	.L3893
	movq	(%r8), %r12
	movq	-256(%rbp), %rdx
	xorl	%r15d, %r15d
	cmpq	%r12, %rdx
	je	.L4290
.L3894:
	xorq	%r12, %rdx
	rep bsfq	%rdx, %rdx
	movslq	%edx, %rdx
	shrq	$3, %rdx
	addq	%rdx, %r15
.L3896:
	movl	88(%r13), %edx
	addq	%r15, %rdx
	cmpq	%rdx, %rdi
	jnb	.L3892
	testq	%r15, %r15
	je	.L3892
	movq	%rdi, %rdx
	movq	96(%r13), %r8
	subq	%r15, %rdx
	leal	(%rdx,%rdx,2), %ecx
	addl	%ecx, %ecx
	shrq	%cl, %r8
	movq	%r8, %rcx
	andl	$63, %ecx
	leaq	(%rcx,%rdx,4), %rdx
	movzbl	(%r9,%rdi), %ecx
	movq	-272(%rbp), %rdi
	salq	%cl, %rdx
	leaq	(%rdi,%rsi), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, -232(%rbp)
	jb	.L3892
	leaq	(%r15,%r15,8), %rsi
	movq	%rsi, %rdx
	salq	$4, %rdx
	subq	%rsi, %rdx
	bsrl	%ecx, %esi
	imull	$30, %esi, %esi
	addq	$1920, %rdx
	subq	%rsi, %rdx
	cmpq	-136(%rbp), %rdx
	jb	.L3892
	movzwl	-264(%rbp), %esi
	addq	$1, 32(%r10)
	movq	%rdx, -136(%rbp)
	subl	%r15d, %esi
	movq	%rcx, -144(%rbp)
	movl	%esi, -176(%rbp)
	movq	%r15, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L3892:
	cmpq	%rbx, %rax
	jne	.L3897
	movq	-288(%rbp), %r14
	movq	-296(%rbp), %r13
	movq	-312(%rbp), %rbx
	movq	-328(%rbp), %r15
	movq	-352(%rbp), %r9
	jmp	.L3891
	.p2align 4,,10
	.p2align 3
.L3851:
	cmpq	$31, -64(%rbp)
	ja	.L4262
	jmp	.L3866
	.p2align 4,,10
	.p2align 3
.L3964:
	movq	%rdx, -200(%rbp)
	movq	-128(%rbp), %r9
	movq	%r13, -72(%rbp)
	jmp	.L3828
	.p2align 4,,10
	.p2align 3
.L3960:
	movq	$0, -96(%rbp)
	movq	$0, -200(%rbp)
	movq	$2020, -72(%rbp)
	jmp	.L3827
.L4299:
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L3878:
	cmpq	$2, %rdi
	jne	.L3869
	cmpq	$1, %rax
	ja	.L3869
	movl	$2205, %r13d
	jmp	.L3879
.L3910:
	movq	-240(%rbp), %rsi
	xorl	%edx, %edx
	leaq	16(%rsi), %rax
	movq	%rax, -72(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, (%rsi)
	movl	-176(%rbp), %eax
	sall	$25, %eax
	orl	-64(%rbp), %eax
	movl	%eax, 4(%rsi)
	movl	$1, %esi
	xorl	%eax, %eax
	jmp	.L3919
	.p2align 4,,10
	.p2align 3
.L4280:
	movq	-88(%rbp), %rdi
	movl	%edi, %edx
	sall	$3, %edi
	andl	$56, %edi
.L3921:
	cmpq	$9, %rax
	jbe	.L4291
.L3924:
	cmpq	$133, %rax
	jbe	.L4292
	cmpq	$2117, %rax
	ja	.L3927
	subq	$70, %rax
	bsrl	%eax, %eax
	addl	$12, %eax
.L3925:
	movl	%eax, %ecx
	andl	$7, %ecx
	orl	%ecx, %edi
	cmpw	$7, %dx
	ja	.L3928
.L4283:
	testb	%sil, %sil
	je	.L3928
	cmpw	$15, %ax
	ja	.L3928
	movl	%edi, %edx
	orl	$64, %edx
	cmpw	$7, %ax
	cmova	%edx, %edi
.L3931:
	movq	-240(%rbp), %rax
	movq	-88(%rbp), %rsi
	movq	-192(%rbp), %r14
	movw	%di, 12(%rax)
	movq	48(%rbp), %rax
	movq	-336(%rbp), %rdi
	addq	%rsi, (%rax)
	movq	-64(%rbp), %rax
	leaq	2(%r14), %rsi
	movq	%rdi, %r10
	addq	%rax, %r14
	cmpq	%rdi, %r14
	movq	-144(%rbp), %rdi
	cmovbe	%r14, %r10
	shrq	$2, %rax
	cmpq	%rdi, %rax
	jbe	.L3932
	movq	%rdi, %rax
	movq	%r14, %rdi
	salq	$2, %rax
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	cmovnb	%rdi, %rsi
	cmpq	%r10, %rsi
	cmova	%r10, %rsi
.L3932:
	leaq	8(%r14), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r10, %rsi
	jnb	.L4002
	movq	40(%rbx), %rax
	movq	64(%rbx), %r11
	movq	-80(%rbp), %r13
	leaq	80(%rax,%rax), %r9
	.p2align 4,,10
	.p2align 3
.L3934:
	movq	%r15, %rax
	movl	56(%rbx), %ecx
	andq	%rsi, %rax
	movq	(%r12,%rax), %rdi
	andq	%r11, %rdi
	movq	%rdi, %rax
	movabsq	$2297779722762296275, %rdi
	imulq	%rdi, %rax
	shrq	%cl, %rax
	movl	8(%rbx), %ecx
	movl	%eax, %edx
	leaq	0(%r13,%rdx,2), %r8
	movzwl	72(%rbx), %edx
	sall	%cl, %eax
	movzwl	(%r8), %edi
	andl	%edi, %edx
	addl	$1, %edi
	movzwl	%dx, %edx
	addq	%rdx, %rax
	leaq	(%rbx,%rax,4), %rax
	movl	%esi, (%rax,%r9)
	addq	$1, %rsi
	movw	%di, (%r8)
	cmpq	%rsi, %r10
	jne	.L3934
.L4002:
	movq	-72(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L3933
.L4277:
	movq	56(%r13), %rax
	movq	%r9, -216(%rbp)
	addq	$1, %r9
	subq	$1, -56(%rbp)
	movq	%rax, -304(%rbp)
	movl	-176(%rbp), %eax
	movl	%eax, -160(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L3908
.L3843:
	cmpq	$0, -248(%rbp)
	je	.L3842
	movq	-176(%rbp), %r9
	movq	-56(%rbp), %rbx
	movzbl	(%r9), %r9d
	cmpb	%r9b, (%rbx)
	jne	.L3842
	movq	-272(%rbp), %rbx
	cmpq	$0, -256(%rbp)
	movq	%rbx, -280(%rbp)
	je	.L3842
	movq	-176(%rbp), %r9
	movq	-56(%rbp), %rbx
	movq	$1, -96(%rbp)
	movzbl	1(%r9), %r9d
	cmpb	%r9b, 1(%rbx)
	jne	.L3842
.L3955:
	movq	-96(%rbp), %r9
	cmpq	$2, -248(%rbp)
	leaq	1(%r9), %rbx
	movq	%rbx, -96(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movq	-280(%rbp), %rbx
	movzbl	1(%r11,%r9), %r11d
	cmpb	%r11b, 1(%rbx)
	jne	.L3847
	leaq	2(%r9), %r11
	cmpq	$3, -248(%rbp)
	movq	%r11, -96(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movzbl	2(%r11,%r9), %r11d
	cmpb	%r11b, 2(%rbx)
	jne	.L3847
	leaq	3(%r9), %r11
	cmpq	$4, -248(%rbp)
	movq	%r11, -96(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movzbl	3(%r11,%r9), %r11d
	cmpb	%r11b, 3(%rbx)
	jne	.L3847
	leaq	4(%r9), %r11
	cmpq	$5, -248(%rbp)
	movq	%r11, -96(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movzbl	4(%r11,%r9), %r11d
	cmpb	%r11b, 4(%rbx)
	jne	.L3847
	leaq	5(%r9), %r11
	cmpq	$6, -248(%rbp)
	movq	%r11, -96(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movzbl	5(%r11,%r9), %r11d
	cmpb	%r11b, 5(%rbx)
	jne	.L3847
	leaq	6(%r9), %r11
	cmpq	$7, -248(%rbp)
	movq	%r11, -96(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movzbl	6(%rbx), %ebx
	cmpb	%bl, 6(%r11,%r9)
	jne	.L3847
	addq	$7, %r9
	movq	%r9, -96(%rbp)
	jmp	.L3847
.L4267:
	movq	-264(%rbp), %rbx
	leaq	8(%r11), %r9
	addq	%r11, %rbx
	subq	$1, -96(%rbp)
	je	.L4293
	movq	%r9, %r11
	jmp	.L3845
.L3983:
	movq	%rdx, -144(%rbp)
	movq	%rdi, %rsi
	movq	%rdi, -64(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L3869
.L4265:
	movl	4(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 40(%rbx)
	leal	1(%rax), %edx
	movl	%edx, 44(%rbx)
	leal	-2(%rax), %edx
	movl	%edx, 48(%rbx)
	leal	2(%rax), %edx
	movl	%edx, 52(%rbx)
	leal	-3(%rax), %edx
	addl	$3, %eax
	movl	%eax, 60(%rbx)
	leaq	8(%r14), %rax
	movl	%edx, 56(%rbx)
	cmpq	-104(%rbp), %rax
	jb	.L4294
.L3959:
	xorl	%edx, %edx
	jmp	.L3826
.L4286:
	leal	0(,%r10,4), %ecx
	movl	$158663784, %eax
	sarl	%cl, %eax
	movl	%eax, %edx
	andl	$15, %edx
	jmp	.L3914
.L3829:
	cmpq	$0, -152(%rbp)
	je	.L3828
	movq	-144(%rbp), %rdi
	movq	-56(%rbp), %rbx
	movzbl	(%rdi), %edi
	cmpb	%dil, (%rbx)
	jne	.L3828
	movq	-184(%rbp), %rbx
	cmpq	$0, -160(%rbp)
	movq	%rbx, -192(%rbp)
	je	.L3828
	movq	-56(%rbp), %rdi
	movq	-144(%rbp), %rbx
	movq	$1, -128(%rbp)
	movzbl	1(%rdi), %edi
	cmpb	%dil, 1(%rbx)
	jne	.L3837
.L3956:
	movq	-128(%rbp), %rdi
	cmpq	$2, -152(%rbp)
	leaq	1(%rdi), %rbx
	movq	%rbx, -128(%rbp)
	je	.L3833
	movq	-144(%rbp), %r13
	movq	-192(%rbp), %rcx
	movzbl	1(%r13,%rdi), %ebx
	cmpb	%bl, 1(%rcx)
	jne	.L3833
	leaq	2(%rdi), %rbx
	cmpq	$3, -152(%rbp)
	movq	%rbx, -128(%rbp)
	je	.L3833
	movzbl	2(%r13,%rdi), %ebx
	cmpb	%bl, 2(%rcx)
	jne	.L3833
	leaq	3(%rdi), %rbx
	cmpq	$4, -152(%rbp)
	movq	%rbx, -128(%rbp)
	je	.L3833
	movzbl	3(%r13,%rdi), %ebx
	cmpb	%bl, 3(%rcx)
	jne	.L3833
	leaq	4(%rdi), %rbx
	cmpq	$5, -152(%rbp)
	movq	%rbx, -128(%rbp)
	je	.L3833
	movzbl	4(%r13,%rdi), %ebx
	cmpb	%bl, 4(%rcx)
	jne	.L3833
	leaq	5(%rdi), %rbx
	cmpq	$6, -152(%rbp)
	movq	%rbx, -128(%rbp)
	je	.L3833
	movzbl	5(%r13,%rdi), %ebx
	cmpb	%bl, 5(%rcx)
	jne	.L3833
	leaq	6(%rdi), %rbx
	cmpq	$7, -152(%rbp)
	movq	%rbx, -128(%rbp)
	je	.L3833
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r13,%rdi)
	jne	.L3833
	addq	$7, %rdi
	movq	%rdi, -128(%rbp)
	jmp	.L3833
.L4292:
	subq	$6, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %eax
	jmp	.L3925
.L4281:
	movq	-88(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	bsrl	%edx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L3921
.L4270:
	subq	$1, -128(%rbp)
	movq	-176(%rbp), %rdi
	leaq	(%rdi,%rbx), %rcx
	leaq	8(%rbx), %rdi
	je	.L4295
	movq	%rdi, %rbx
	jmp	.L3831
.L3853:
	movq	%rcx, %rax
	andl	$7, %eax
	andl	$7, %ecx
	je	.L3852
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L3852
.L3949:
	leaq	1(%r13), %r15
	cmpq	$1, %rax
	je	.L3856
	movq	-56(%rbp), %rcx
	movzbl	1(%rcx,%r13), %r12d
	cmpb	%r12b, 1(%r8)
	jne	.L3856
	leaq	2(%r13), %r15
	cmpq	$2, %rax
	je	.L3856
	movzbl	2(%rcx,%r13), %r12d
	cmpb	%r12b, 2(%r8)
	jne	.L3856
	leaq	3(%r13), %r15
	cmpq	$3, %rax
	je	.L3856
	movzbl	3(%rcx,%r13), %r12d
	cmpb	%r12b, 3(%r8)
	jne	.L3856
	leaq	4(%r13), %r15
	cmpq	$4, %rax
	je	.L3856
	movzbl	4(%rcx,%r13), %r12d
	cmpb	%r12b, 4(%r8)
	jne	.L3856
	leaq	5(%r13), %r15
	subq	$5, %rax
	je	.L3856
	movzbl	5(%rcx,%r13), %r12d
	cmpb	%r12b, 5(%r8)
	jne	.L3856
	leaq	6(%r13), %r15
	cmpq	$1, %rax
	je	.L3856
	movzbl	6(%rcx,%r13), %eax
	cmpb	%al, 6(%r8)
	jne	.L3856
	leaq	7(%r13), %r15
	jmp	.L3856
.L4272:
	leaq	8(%r8), %rax
	subq	$1, %r13
	je	.L3971
	movq	-56(%rbp), %rax
	movq	8(%r8), %r12
	movl	$8, %r15d
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L3854
	leaq	16(%r8), %rax
	cmpq	$1, %r13
	je	.L3973
	movq	-56(%rbp), %rax
	movq	16(%r8), %r12
	movl	$16, %r15d
	addq	$24, %r8
	movl	$24, %r13d
	movq	16(%rax), %rax
	cmpq	%r12, %rax
	jne	.L3854
.L3855:
	movq	%rcx, %rax
	andl	$7, %eax
	andl	$7, %ecx
	je	.L3976
	movq	-56(%rbp), %rcx
	movzbl	(%rcx,%r13), %ecx
	cmpb	%cl, (%r8)
	je	.L3949
.L3976:
	movq	%r13, %r15
	jmp	.L3856
	.p2align 4,,10
	.p2align 3
.L3927:
	orl	$7, %edi
	movl	$2, %eax
	jmp	.L3930
.L4282:
	movq	-88(%rbp), %rdi
	leaq	-66(%rdi), %rdx
	bsrl	%edx, %edx
	addl	$10, %edx
	leal	0(,%rdx,8), %edi
	andl	$56, %edi
	jmp	.L3921
.L3979:
	movq	$0, -144(%rbp)
	movq	$0, -64(%rbp)
	movq	$2020, -136(%rbp)
	jmp	.L3868
.L4271:
	subq	$1, -144(%rbp)
	movq	-192(%rbp), %rdx
	leaq	(%rdx,%rdi), %r8
	leaq	8(%rdi), %rdx
	je	.L4296
	movq	%rdx, %rdi
	jmp	.L3862
.L3884:
	cmpq	$0, -328(%rbp)
	je	.L3883
	movq	-128(%rbp), %r11
	movq	-312(%rbp), %rcx
	movzbl	(%r11), %r11d
	cmpb	%r11b, (%rcx)
	jne	.L3883
	movq	-360(%rbp), %rcx
	cmpq	$0, -352(%rbp)
	movq	%rcx, -384(%rbp)
	je	.L3883
	movq	-128(%rbp), %r12
	movq	-312(%rbp), %rcx
	movq	$1, -376(%rbp)
	movzbl	1(%r12), %r12d
	cmpb	%r12b, 1(%rcx)
	jne	.L3883
.L3953:
	movq	-376(%rbp), %rcx
	addq	$1, %rcx
	cmpq	$2, -328(%rbp)
	movq	%rcx, -296(%rbp)
	je	.L3888
	movq	-376(%rbp), %r11
	movq	-312(%rbp), %rcx
	movzbl	1(%rcx,%r11), %r12d
	movq	-384(%rbp), %r11
	cmpb	%r12b, 1(%r11)
	jne	.L3888
	movq	-376(%rbp), %r11
	addq	$2, %r11
	cmpq	$3, -328(%rbp)
	movq	%r11, -296(%rbp)
	je	.L3888
	movq	-376(%rbp), %r11
	movzbl	2(%rcx,%r11), %r12d
	movq	-384(%rbp), %r11
	cmpb	%r12b, 2(%r11)
	jne	.L3888
	movq	-376(%rbp), %r11
	addq	$3, %r11
	cmpq	$4, -328(%rbp)
	movq	%r11, -296(%rbp)
	je	.L3888
	movq	-376(%rbp), %r11
	movzbl	3(%rcx,%r11), %r12d
	movq	-384(%rbp), %r11
	cmpb	%r12b, 3(%r11)
	jne	.L3888
	movq	-376(%rbp), %r11
	addq	$4, %r11
	cmpq	$5, -328(%rbp)
	movq	%r11, -296(%rbp)
	je	.L3888
	movq	-376(%rbp), %r11
	movzbl	4(%rcx,%r11), %r12d
	movq	-384(%rbp), %r11
	cmpb	%r12b, 4(%r11)
	jne	.L3888
	movq	-376(%rbp), %r11
	addq	$5, %r11
	cmpq	$6, -328(%rbp)
	movq	%r11, -296(%rbp)
	je	.L3888
	movq	-376(%rbp), %r11
	movzbl	5(%rcx,%r11), %r12d
	movq	-384(%rbp), %r11
	cmpb	%r12b, 5(%r11)
	jne	.L3888
	movq	-376(%rbp), %r11
	addq	$6, %r11
	cmpq	$7, -328(%rbp)
	movq	%r11, -296(%rbp)
	je	.L3888
	movq	%rcx, %r12
	movq	-376(%rbp), %r11
	movq	-384(%rbp), %rcx
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r12,%r11)
	jne	.L3888
	movq	%r11, %rcx
	addq	$7, %rcx
	movq	%rcx, -296(%rbp)
	jmp	.L3888
.L4274:
	subq	$1, -296(%rbp)
	movq	-368(%rbp), %rcx
	leaq	(%rcx,%r11), %r12
	leaq	8(%r11), %rcx
	je	.L4297
	movq	%rcx, %r11
	jmp	.L3886
.L4000:
	movl	$40, %edi
	movl	$21, %edx
	jmp	.L3921
.L4285:
	movq	16(%rbp), %rax
	movl	$1, %edx
	movl	8(%rax), %eax
	jmp	.L3912
.L4284:
	subq	$1, -328(%rbp)
	movq	-312(%rbp), %rdi
	leaq	(%rdi,%r11), %rcx
	leaq	8(%r11), %rdi
	je	.L4298
	movq	%rdi, %r11
	jmp	.L3872
.L3870:
	cmpq	$0, -272(%rbp)
	je	.L3869
	movq	-128(%rbp), %rcx
	movq	-256(%rbp), %rdi
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%rdi)
	jne	.L3869
	movq	-296(%rbp), %rdi
	cmpq	$0, -288(%rbp)
	movq	%rdi, -352(%rbp)
	je	.L3869
	movq	-256(%rbp), %rcx
	movq	-128(%rbp), %rdi
	movq	$1, -328(%rbp)
	movzbl	1(%rcx), %ecx
	cmpb	%cl, 1(%rdi)
	jne	.L4299
.L3954:
	movq	-328(%rbp), %r13
	cmpq	$2, -272(%rbp)
	leaq	1(%r13), %rdi
	je	.L3874
	movq	-256(%rbp), %r11
	movq	-352(%rbp), %rcx
	movzbl	1(%r11,%r13), %r13d
	cmpb	%r13b, 1(%rcx)
	jne	.L3874
	movq	-328(%rbp), %r13
	cmpq	$3, -272(%rbp)
	leaq	2(%r13), %rdi
	je	.L3874
	movzbl	2(%r11,%r13), %r13d
	cmpb	%r13b, 2(%rcx)
	jne	.L3874
	movq	-328(%rbp), %r13
	cmpq	$4, -272(%rbp)
	leaq	3(%r13), %rdi
	je	.L3874
	movzbl	3(%r11,%r13), %r13d
	cmpb	%r13b, 3(%rcx)
	jne	.L3874
	movq	-328(%rbp), %r13
	cmpq	$5, -272(%rbp)
	leaq	4(%r13), %rdi
	je	.L3874
	movzbl	4(%r11,%r13), %r13d
	cmpb	%r13b, 4(%rcx)
	jne	.L3874
	movq	-328(%rbp), %r13
	cmpq	$6, -272(%rbp)
	leaq	5(%r13), %rdi
	je	.L3874
	movzbl	5(%r11,%r13), %r13d
	cmpb	%r13b, 5(%rcx)
	jne	.L3874
	movq	-328(%rbp), %r13
	cmpq	$7, -272(%rbp)
	leaq	6(%r13), %rdi
	je	.L3874
	movzbl	6(%rcx), %ecx
	cmpb	%cl, 6(%r11,%r13)
	jne	.L3874
	addq	$1, %rdi
	jmp	.L3874
.L3893:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L3892
	movq	-128(%rbp), %rcx
	movzbl	(%rcx), %ecx
	cmpb	%cl, (%r8)
	jne	.L3892
.L3945:
	leaq	1(%r14), %r15
	cmpq	$1, %rdx
	je	.L3896
	movq	-128(%rbp), %rcx
	movzbl	1(%rcx,%r14), %r12d
	cmpb	%r12b, 1(%r8)
	jne	.L3896
	leaq	2(%r14), %r15
	cmpq	$2, %rdx
	je	.L3896
	movzbl	2(%rcx,%r14), %r12d
	cmpb	%r12b, 2(%r8)
	jne	.L3896
	leaq	3(%r14), %r15
	cmpq	$3, %rdx
	je	.L3896
	movzbl	3(%rcx,%r14), %r12d
	cmpb	%r12b, 3(%r8)
	jne	.L3896
	leaq	4(%r14), %r15
	cmpq	$4, %rdx
	je	.L3896
	movzbl	4(%rcx,%r14), %r12d
	cmpb	%r12b, 4(%r8)
	jne	.L3896
	leaq	5(%r14), %r15
	subq	$5, %rdx
	je	.L3896
	movzbl	5(%rcx,%r14), %r12d
	cmpb	%r12b, 5(%r8)
	jne	.L3896
	leaq	6(%r14), %r15
	cmpq	$1, %rdx
	je	.L3896
	movzbl	6(%rcx,%r14), %ecx
	cmpb	%cl, 6(%r8)
	jne	.L3896
	leaq	7(%r14), %r15
	jmp	.L3896
.L4003:
	movq	%rdx, -88(%rbp)
	movq	%r9, %r14
	jmp	.L3933
.L4287:
	leal	0(,%rax,4), %ecx
	movl	$266017486, %edx
	sarl	%cl, %edx
	andl	$15, %edx
	jmp	.L3914
.L4290:
	leaq	8(%r8), %rdx
	subq	$1, %r14
	je	.L3990
	movq	-128(%rbp), %rdx
	movq	8(%r8), %r12
	movl	$8, %r15d
	movq	8(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L3894
	leaq	16(%r8), %rdx
	cmpq	$1, %r14
	je	.L3992
	movq	-128(%rbp), %rdx
	movq	16(%r8), %r12
	movl	$16, %r15d
	addq	$24, %r8
	movl	$24, %r14d
	movq	16(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L3894
.L3895:
	movq	%rcx, %rdx
	andl	$7, %edx
	andl	$7, %ecx
	je	.L3995
	movq	-128(%rbp), %rcx
	movzbl	(%rcx,%r14), %ecx
	cmpb	%cl, (%r8)
	je	.L3945
.L3995:
	movq	%r14, %r15
	jmp	.L3896
	.p2align 4,,10
	.p2align 3
.L3906:
	movq	-216(%rbp), %r14
	movq	-208(%rbp), %rax
	movq	%rbx, %r12
	movq	-152(%rbp), %rbx
	cmpq	%rax, %r14
	movq	%r14, -192(%rbp)
	cmovbe	%r14, %rax
	movq	%rax, %r8
	movl	-160(%rbp), %eax
	movl	%eax, -176(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	jmp	.L3907
.L4289:
	movq	-264(%rbp), %rax
	leaq	(%rax,%rsi), %rdi
	leaq	8(%rsi), %rax
	subq	$1, %r11
	je	.L4300
	movq	%rax, %rsi
	jmp	.L3902
.L4300:
	cmpq	$0, -256(%rbp)
	je	.L3904
	movzbl	(%r10,%rax), %r11d
	cmpb	%r11b, (%rdi)
	jne	.L3904
	cmpq	$1, -256(%rbp)
	leaq	9(%rsi), %rax
	je	.L3904
	movzbl	9(%r10,%rsi), %r11d
	cmpb	%r11b, 1(%rdi)
	jne	.L3904
	cmpq	$2, -256(%rbp)
	leaq	10(%rsi), %rax
	je	.L3904
	movzbl	10(%r10,%rsi), %r11d
	cmpb	%r11b, 2(%rdi)
	jne	.L3904
	cmpq	$3, -256(%rbp)
	leaq	11(%rsi), %rax
	je	.L3904
	movzbl	11(%r10,%rsi), %r11d
	cmpb	%r11b, 3(%rdi)
	jne	.L3904
	cmpq	$4, -256(%rbp)
	leaq	12(%rsi), %rax
	je	.L3904
	movzbl	12(%r10,%rsi), %r11d
	cmpb	%r11b, 4(%rdi)
	jne	.L3904
	cmpq	$5, -256(%rbp)
	leaq	13(%rsi), %rax
	je	.L3904
	movzbl	13(%r10,%rsi), %r11d
	cmpb	%r11b, 5(%rdi)
	jne	.L3904
	cmpq	$6, -256(%rbp)
	leaq	14(%rsi), %rax
	je	.L3904
	movzbl	14(%r10,%rsi), %r11d
	cmpb	%r11b, 6(%rdi)
	jne	.L3904
	cmpq	$7, -256(%rbp)
	leaq	15(%rsi), %rax
	je	.L3904
	movzbl	7(%rdi), %edi
	cmpb	%dil, 15(%r10,%rsi)
	jne	.L3904
	leaq	16(%rsi), %rax
	jmp	.L3904
.L4293:
	cmpq	$0, -248(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r11, -288(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movzbl	(%r11,%r9), %r11d
	cmpb	%r11b, (%rbx)
	jne	.L3847
	leaq	1(%rbx), %r9
	cmpq	$0, -256(%rbp)
	movq	%r9, -280(%rbp)
	movq	-288(%rbp), %r9
	leaq	9(%r9), %r11
	movq	%r11, -96(%rbp)
	je	.L3847
	movq	-176(%rbp), %r11
	movzbl	9(%r11,%r9), %r9d
	cmpb	%r9b, 1(%rbx)
	je	.L3955
	jmp	.L3847
.L4297:
	cmpq	$0, -328(%rbp)
	movq	%rcx, -296(%rbp)
	movq	%r11, -376(%rbp)
	je	.L3888
	movq	-312(%rbp), %rcx
	movq	%rcx, %r11
	movq	%rcx, -392(%rbp)
	movq	-296(%rbp), %rcx
	movzbl	(%r11,%rcx), %r11d
	cmpb	%r11b, (%r12)
	jne	.L3888
	movq	-376(%rbp), %r11
	leaq	1(%r12), %rcx
	movq	%rcx, -384(%rbp)
	addq	$9, %r11
	cmpq	$0, -352(%rbp)
	movq	%r11, -296(%rbp)
	je	.L3888
	movq	-392(%rbp), %r11
	movq	-376(%rbp), %rcx
	movzbl	9(%r11,%rcx), %r11d
	cmpb	%r11b, 1(%r12)
	jne	.L3888
	movq	-296(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	jmp	.L3953
.L4295:
	cmpq	$0, -152(%rbp)
	movq	%r13, -96(%rbp)
	movq	%rdi, -128(%rbp)
	je	.L3835
	movq	-144(%rbp), %r13
	movq	%rcx, -192(%rbp)
	movzbl	0(%r13,%rdi), %r13d
	cmpb	%r13b, (%rcx)
	jne	.L3836
	movq	%rcx, %rdi
	leaq	1(%rcx), %rcx
	cmpq	$0, -160(%rbp)
	movq	%rcx, -192(%rbp)
	leaq	9(%rbx), %rcx
	movq	%rcx, -128(%rbp)
	je	.L3833
	movq	-144(%rbp), %r13
	movzbl	9(%r13,%rbx), %ebx
	cmpb	%bl, 1(%rdi)
	je	.L3956
	jmp	.L3833
	.p2align 4,,10
	.p2align 3
.L3971:
	movq	%rax, %r8
	movl	$8, %r13d
	jmp	.L3855
.L3998:
	movl	$2, %edx
	jmp	.L3912
.L4288:
	movl	$3, %edx
	jmp	.L3912
.L4296:
	cmpq	$0, -176(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%r8, -248(%rbp)
	je	.L3864
	movq	-152(%rbp), %r8
	movzbl	(%r8,%rdx), %r8d
	movq	-248(%rbp), %rdx
	cmpb	%r8b, (%rdx)
	jne	.L3864
	leaq	9(%rdi), %rdx
	cmpq	$1, -176(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L3864
	movq	-152(%rbp), %r8
	movq	-248(%rbp), %rdx
	movzbl	9(%r8,%rdi), %r8d
	cmpb	%r8b, 1(%rdx)
	jne	.L3864
	leaq	10(%rdi), %rdx
	cmpq	$2, -176(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L3864
	movq	-152(%rbp), %r8
	movq	-248(%rbp), %rdx
	movzbl	10(%r8,%rdi), %r8d
	cmpb	%r8b, 2(%rdx)
	jne	.L3864
	leaq	11(%rdi), %rdx
	cmpq	$3, -176(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L3864
	movq	-152(%rbp), %r8
	movq	-248(%rbp), %rdx
	movzbl	11(%r8,%rdi), %r8d
	cmpb	%r8b, 3(%rdx)
	jne	.L3864
	leaq	12(%rdi), %rdx
	cmpq	$4, -176(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L3864
	movq	-152(%rbp), %r8
	movq	-248(%rbp), %rdx
	movzbl	12(%r8,%rdi), %r8d
	cmpb	%r8b, 4(%rdx)
	jne	.L3864
	leaq	13(%rdi), %rdx
	cmpq	$5, -176(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L3864
	movq	-152(%rbp), %r8
	movq	-248(%rbp), %rdx
	movzbl	13(%r8,%rdi), %r8d
	cmpb	%r8b, 5(%rdx)
	jne	.L3864
	leaq	14(%rdi), %rdx
	cmpq	$6, -176(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L3864
	movq	-152(%rbp), %r8
	movq	-248(%rbp), %rdx
	movzbl	14(%r8,%rdi), %r8d
	cmpb	%r8b, 6(%rdx)
	jne	.L3864
	leaq	15(%rdi), %rdx
	cmpq	$7, -176(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L3864
	movq	-248(%rbp), %rdx
	movq	-152(%rbp), %r8
	movzbl	7(%rdx), %edx
	cmpb	%dl, 15(%r8,%rdi)
	jne	.L3864
	addq	$16, %rdi
	movq	%rdi, -144(%rbp)
	jmp	.L3864
.L3990:
	movq	%rdx, %r8
	movl	$8, %r14d
	jmp	.L3895
.L4298:
	cmpq	$0, -272(%rbp)
	movq	%r13, -176(%rbp)
	je	.L3876
	movq	-256(%rbp), %r13
	movzbl	0(%r13,%rdi), %r13d
	cmpb	%r13b, (%rcx)
	jne	.L3877
	movq	%rcx, %rdi
	leaq	1(%rcx), %rcx
	cmpq	$0, -288(%rbp)
	movq	%rcx, -352(%rbp)
	leaq	9(%r11), %rcx
	movq	%rcx, -328(%rbp)
	je	.L3982
	movq	-256(%rbp), %r13
	movzbl	9(%r13,%r11), %ecx
	cmpb	%cl, 1(%rdi)
	je	.L3954
.L3982:
	movq	-328(%rbp), %rdi
	jmp	.L3874
.L3973:
	movq	%rax, %r8
	movl	$16, %r13d
	jmp	.L3855
.L3836:
	cmpq	$2, -128(%rbp)
	ja	.L3951
	jmp	.L3828
.L3835:
	cmpq	$2, -128(%rbp)
	jbe	.L3828
	movq	%rdi, %rbx
	jmp	.L4261
.L3877:
	cmpq	$2, %rdi
	ja	.L3947
	jmp	.L3869
.L3876:
	cmpq	$2, %rdi
	jbe	.L3869
	imulq	$135, %rdi, %r11
	leaq	1935(%r11), %r13
	jmp	.L3879
.L3992:
	movq	%rdx, %r8
	movl	$16, %r14d
	jmp	.L3895
.L4279:
	testl	$1023, %edx
	movl	%edx, %eax
	sete	%sil
	xorl	%edx, %edx
	jmp	.L3919
	.cfi_endproc
.LFE291:
	.size	CreateBackwardReferencesNH65, .-CreateBackwardReferencesNH65
	.p2align 4
	.globl	BrotliCreateBackwardReferences
	.hidden	BrotliCreateBackwardReferences
	.type	BrotliCreateBackwardReferences, @function
BrotliCreateBackwardReferences:
.LFB292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$65, 32(%r8)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	movq	16(%rbp), %r10
	movq	24(%rbp), %r11
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	48(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	40(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	32(%rbp), %rbx
	ja	.L4301
	movl	32(%r8), %eax
	leaq	.L4304(%rip), %r14
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4304:
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4315-.L4304
	.long	.L4314-.L4304
	.long	.L4313-.L4304
	.long	.L4312-.L4304
	.long	.L4311-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4310-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4309-.L4304
	.long	.L4308-.L4304
	.long	.L4307-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4306-.L4304
	.long	.L4305-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4301-.L4304
	.long	.L4303-.L4304
	.text
	.p2align 4,,10
	.p2align 3
.L4301:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4315:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH2
	.p2align 4,,10
	.p2align 3
.L4314:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH3
	.p2align 4,,10
	.p2align 3
.L4313:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH4
	.p2align 4,,10
	.p2align 3
.L4312:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH5
	.p2align 4,,10
	.p2align 3
.L4311:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH6
	.p2align 4,,10
	.p2align 3
.L4310:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH35
	.p2align 4,,10
	.p2align 3
.L4309:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH40
	.p2align 4,,10
	.p2align 3
.L4308:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH41
	.p2align 4,,10
	.p2align 3
.L4307:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH42
	.p2align 4,,10
	.p2align 3
.L4306:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH54
	.p2align 4,,10
	.p2align 3
.L4305:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH55
	.p2align 4,,10
	.p2align 3
.L4303:
	.cfi_restore_state
	movq	%r13, 48(%rbp)
	movq	%r12, 40(%rbp)
	movq	%rbx, 32(%rbp)
	popq	%rbx
	movq	%r11, 24(%rbp)
	popq	%r12
	movq	%r10, 16(%rbp)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	CreateBackwardReferencesNH65
	.cfi_endproc
.LFE292:
	.size	BrotliCreateBackwardReferences, .-BrotliCreateBackwardReferences
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1
	.long	1
	.long	-2
	.long	2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
