	.file	"memory.c"
	.text
	.p2align 4
	.type	BrotliDefaultFreeFunc, @function
BrotliDefaultFreeFunc:
.LFB45:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	free@PLT
	.cfi_endproc
.LFE45:
	.size	BrotliDefaultFreeFunc, .-BrotliDefaultFreeFunc
	.p2align 4
	.type	BrotliDefaultAllocFunc, @function
BrotliDefaultAllocFunc:
.LFB44:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	malloc@PLT
	.cfi_endproc
.LFE44:
	.size	BrotliDefaultAllocFunc, .-BrotliDefaultAllocFunc
	.p2align 4
	.globl	BrotliInitMemoryManager
	.hidden	BrotliInitMemoryManager
	.type	BrotliInitMemoryManager, @function
BrotliInitMemoryManager:
.LFB47:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdx, -8(%rbp)
	testq	%rsi, %rsi
	je	.L8
	movq	%rcx, 16(%rdi)
	movq	%rsi, %xmm0
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, (%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	BrotliDefaultFreeFunc(%rip), %rax
	leaq	BrotliDefaultAllocFunc(%rip), %rdx
	movq	$0, 16(%rdi)
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE47:
	.size	BrotliInitMemoryManager, .-BrotliInitMemoryManager
	.p2align 4
	.globl	BrotliAllocate
	.hidden	BrotliAllocate
	.type	BrotliAllocate, @function
BrotliAllocate:
.LFB48:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*(%rax)
	testq	%rax, %rax
	je	.L12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	movl	$1, %edi
	call	exit@PLT
	.cfi_endproc
.LFE48:
	.size	BrotliAllocate, .-BrotliAllocate
	.p2align 4
	.globl	BrotliFree
	.hidden	BrotliFree
	.type	BrotliFree, @function
BrotliFree:
.LFB49:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movq	8(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE49:
	.size	BrotliFree, .-BrotliFree
	.p2align 4
	.globl	BrotliWipeOutMemoryManager
	.hidden	BrotliWipeOutMemoryManager
	.type	BrotliWipeOutMemoryManager, @function
BrotliWipeOutMemoryManager:
.LFB50:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE50:
	.size	BrotliWipeOutMemoryManager, .-BrotliWipeOutMemoryManager
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
