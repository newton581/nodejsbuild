	.file	"decode.c"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	DecodeMetaBlockLength, @function
DecodeMetaBlockLength:
.LFB75:
	.cfi_startproc
	cmpl	$7, 5044(%rdi)
	ja	.L50
	movl	5044(%rdi), %eax
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
.L23:
	movq	(%rsi), %rax
.L25:
	shrq	%cl, %rax
	addl	$2, %ecx
	andl	$3, %eax
	movl	%ecx, 8(%rsi)
	leal	4(%rax), %edx
	movl	$0, 4(%rdi)
	movl	%edx, %ecx
	movzwl	5072(%rdi), %edx
	sall	$6, %ecx
	andw	$-16321, %dx
	orl	%ecx, %edx
	movw	%dx, 5072(%rdi)
	cmpl	$3, %eax
	jne	.L26
	orb	$4, 5072(%rdi)
	movl	$5, 5044(%rdi)
	.p2align 4,,10
	.p2align 3
.L6:
	movl	8(%rsi), %edx
	cmpl	$64, %edx
	jne	.L86
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	(%rsi), %r8
	movq	16(%rsi), %rcx
	subq	$1, %rdx
	shrq	$8, %r8
	addq	$1, %rcx
	movq	%r8, (%rsi)
	movzbl	-1(%rcx), %eax
	movq	%rdx, 24(%rsi)
	movl	$56, %edx
	salq	$56, %rax
	movq	%rcx, 16(%rsi)
	movl	$57, %ecx
	orq	%r8, %rax
	movq	%rax, (%rsi)
.L16:
	btq	%rdx, %rax
	movl	%ecx, 8(%rsi)
	jc	.L52
	movl	$6, 5044(%rdi)
	.p2align 4,,10
	.p2align 3
.L5:
	movl	8(%rsi), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	ja	.L38
	movq	24(%rsi), %rdx
	movl	$64, %r10d
.L39:
	testq	%rdx, %rdx
	je	.L82
	movq	(%rsi), %r9
	movq	16(%rsi), %r8
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r9
	addq	$1, %r8
	movq	%r9, (%rsi)
	movzbl	-1(%r8), %eax
	movq	%r8, 16(%rsi)
	movl	%r10d, %r8d
	salq	$56, %rax
	subl	%ecx, %r8d
	movl	%ecx, 8(%rsi)
	orq	%r9, %rax
	movq	%rdx, 24(%rsi)
	movq	%rax, (%rsi)
	cmpl	$1, %r8d
	jbe	.L39
.L40:
	shrq	%cl, %rax
	addl	$2, %ecx
	movl	%ecx, 8(%rsi)
	andl	$3, %eax
	je	.L83
	movzwl	5072(%rdi), %ecx
	movl	%eax, %edx
	sall	$6, %eax
	movl	$7, 5044(%rdi)
	andw	$-16321, %cx
	orl	%ecx, %eax
	movw	%ax, 5072(%rdi)
	jmp	.L12
.L27:
	movl	$4, 5044(%rdi)
	.p2align 4,,10
	.p2align 3
.L7:
	testb	$1, 5072(%rdi)
	jne	.L35
	movl	8(%rsi), %ecx
	cmpl	$64, %ecx
	jne	.L87
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	(%rsi), %r8
	movq	16(%rsi), %rcx
	subq	$1, %rdx
	shrq	$8, %r8
	addq	$1, %rcx
	movq	%r8, (%rsi)
	movzbl	-1(%rcx), %eax
	movq	%rdx, 24(%rsi)
	movl	$57, %edx
	salq	$56, %rax
	movq	%rcx, 16(%rsi)
	movl	$56, %ecx
	orq	%r8, %rax
	movq	%rax, (%rsi)
.L37:
	movl	%edx, 8(%rsi)
	shrq	%cl, %rax
	movzbl	5072(%rdi), %edx
	andl	$1, %eax
	addl	%eax, %eax
	andl	$-3, %edx
	orl	%edx, %eax
	movb	%al, 5072(%rdi)
	.p2align 4,,10
	.p2align 3
.L35:
	addl	$1, 272(%rdi)
.L83:
	movl	$0, 5044(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	8(%rsi), %ecx
	cmpl	$64, %ecx
	jne	.L88
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	(%rsi), %r8
	movq	16(%rsi), %rcx
	subq	$1, %rdx
	shrq	$8, %r8
	addq	$1, %rcx
	movq	%r8, (%rsi)
	movzbl	-1(%rcx), %eax
	movq	%rdx, 24(%rsi)
	movl	$57, %edx
	salq	$56, %rax
	movq	%rcx, 16(%rsi)
	movl	$56, %ecx
	orq	%r8, %rax
	movq	%rax, (%rsi)
.L14:
	movl	%edx, 8(%rsi)
	movzbl	5072(%rdi), %edx
	shrq	%cl, %rax
	movl	$0, 272(%rdi)
	andl	$1, %eax
	andl	$-8, %edx
	orl	%edx, %eax
	movb	%al, 5072(%rdi)
	testb	$1, %al
	je	.L22
	movl	$1, 5044(%rdi)
	.p2align 4,,10
	.p2align 3
.L10:
	movl	8(%rsi), %edx
	cmpl	$64, %edx
	jne	.L89
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	(%rsi), %r8
	movq	16(%rsi), %rcx
	subq	$1, %rdx
	shrq	$8, %r8
	addq	$1, %rcx
	movq	%r8, (%rsi)
	movzbl	-1(%rcx), %eax
	movq	%rdx, 24(%rsi)
	movl	$56, %edx
	salq	$56, %rax
	movq	%rcx, 16(%rsi)
	movl	$57, %ecx
	orq	%r8, %rax
	movq	%rax, (%rsi)
.L20:
	btq	%rdx, %rax
	movl	%ecx, 8(%rsi)
	jc	.L83
.L22:
	movl	$2, 5044(%rdi)
.L9:
	movl	8(%rsi), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	ja	.L23
	movq	24(%rsi), %rdx
	movl	$64, %r10d
	testq	%rdx, %rdx
	je	.L82
.L90:
	movq	(%rsi), %r9
	movq	16(%rsi), %r8
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r9
	addq	$1, %r8
	movq	%r9, (%rsi)
	movzbl	-1(%r8), %eax
	movq	%r8, 16(%rsi)
	movl	%r10d, %r8d
	salq	$56, %rax
	subl	%ecx, %r8d
	movl	%ecx, 8(%rsi)
	orq	%r9, %rax
	movq	%rdx, 24(%rsi)
	movq	%rax, (%rsi)
	cmpl	$1, %r8d
	ja	.L25
	testq	%rdx, %rdx
	jne	.L90
.L82:
	movl	$2, %eax
	ret
.L26:
	movl	$3, 5044(%rdi)
	.p2align 4,,10
	.p2align 3
.L8:
	movzwl	5072(%rdi), %eax
	movl	4(%rdi), %r9d
	shrw	$6, %ax
	movzbl	%al, %eax
	cmpl	%eax, %r9d
	jge	.L27
	movl	$64, %r10d
.L28:
	movl	8(%rsi), %ecx
	movl	%r10d, %eax
	subl	%ecx, %eax
	cmpl	$3, %eax
	ja	.L91
	movq	24(%rsi), %rdx
.L30:
	testq	%rdx, %rdx
	je	.L29
	movq	(%rsi), %r11
	movq	16(%rsi), %r8
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r11
	addq	$1, %r8
	movq	%r11, (%rsi)
	movzbl	-1(%r8), %eax
	movq	%r8, 16(%rsi)
	movl	%r10d, %r8d
	salq	$56, %rax
	subl	%ecx, %r8d
	movl	%ecx, 8(%rsi)
	orq	%r11, %rax
	movq	%rdx, 24(%rsi)
	movq	%rax, (%rsi)
	cmpl	$3, %r8d
	jbe	.L30
.L34:
	shrq	%cl, %rax
	addl	$4, %ecx
	leal	1(%r9), %r8d
	movl	%ecx, 8(%rsi)
	movzwl	5072(%rdi), %edx
	andl	$15, %eax
	shrw	$6, %dx
	movzbl	%dl, %r11d
	cmpl	%r8d, %r11d
	sete	%cl
	cmpb	$4, %dl
	seta	%dl
	testb	%dl, %cl
	je	.L32
	testl	%eax, %eax
	je	.L92
.L32:
	leal	0(,%r9,4), %ecx
	sall	%cl, %eax
	orl	%eax, 272(%rdi)
	cmpl	%r8d, %r11d
	jle	.L27
	movl	%r8d, %r9d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L3:
	movzwl	5072(%rdi), %eax
	shrw	$6, %ax
	movl	%eax, %edx
.L12:
	movl	4(%rdi), %r11d
	movzbl	%dl, %eax
	cmpl	%eax, %r11d
	jge	.L35
	movl	$64, %r10d
.L43:
	movl	8(%rsi), %r9d
	movl	%r10d, %eax
	subl	%r9d, %eax
	cmpl	$7, %eax
	ja	.L93
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L44
.L94:
	movq	(%rsi), %rcx
	movq	16(%rsi), %r8
	subq	$1, %rdx
	shrq	$8, %rcx
	addq	$1, %r8
	movq	%rcx, (%rsi)
	movzbl	-1(%r8), %eax
	movq	%r8, 16(%rsi)
	movl	%r10d, %r8d
	salq	$56, %rax
	movq	%rdx, 24(%rsi)
	orq	%rcx, %rax
	leal	-8(%r9), %ecx
	subl	%ecx, %r8d
	movq	%rax, (%rsi)
	movl	%ecx, 8(%rsi)
	cmpl	$7, %r8d
	ja	.L49
	movl	%ecx, %r9d
	testq	%rdx, %rdx
	jne	.L94
.L44:
	movl	%r11d, 4(%rdi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%r9d, 4(%rdi)
	movl	$2, %eax
	ret
.L52:
	movl	$-2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%rsi), %rax
	movl	%r9d, %ecx
	addl	$8, %r9d
.L49:
	movl	%r9d, 8(%rsi)
	movzwl	5072(%rdi), %edx
	shrq	%cl, %rax
	leal	1(%r11), %r8d
	movzbl	%al, %eax
	shrw	$6, %dx
	cmpb	$1, %dl
	movzbl	%dl, %r9d
	seta	%cl
	cmpl	%r8d, %r9d
	sete	%dl
	testb	%dl, %cl
	je	.L47
	testl	%eax, %eax
	je	.L95
.L47:
	leal	0(,%r11,8), %ecx
	sall	%cl, %eax
	orl	%eax, 272(%rdi)
	cmpl	%r8d, %r9d
	jle	.L35
	movl	%r8d, %r11d
	jmp	.L43
.L38:
	movq	(%rsi), %rax
	jmp	.L40
.L91:
	movq	(%rsi), %rax
	jmp	.L34
.L89:
	movq	(%rsi), %rax
	leal	1(%rdx), %ecx
	jmp	.L20
.L88:
	movq	(%rsi), %rax
	leal	1(%rcx), %edx
	jmp	.L14
.L86:
	movq	(%rsi), %rax
	leal	1(%rdx), %ecx
	jmp	.L16
.L87:
	movq	(%rsi), %rax
	leal	1(%rcx), %edx
	jmp	.L37
.L95:
	movl	$-3, %eax
	ret
.L92:
	orl	$-1, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	DecodeMetaBlockLength.cold, @function
DecodeMetaBlockLength.cold:
.LFSB75:
.L50:
	movl	$-31, %eax
	ret
	.cfi_endproc
.LFE75:
	.text
	.size	DecodeMetaBlockLength, .-DecodeMetaBlockLength
	.section	.text.unlikely
	.size	DecodeMetaBlockLength.cold, .-DecodeMetaBlockLength.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.type	InverseMoveToFrontTransform, @function
InverseMoveToFrontTransform:
.LFB92:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	4788(%rdx), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	4780(%rdx), %ecx
	movl	$50462976, 4788(%rdx)
	leal	-8(%rcx), %eax
	cmpl	$-10, %eax
	ja	.L103
	movl	%ecx, %esi
	movdqa	.LC1(%rip), %xmm1
	xorl	%eax, %eax
	movdqa	.LC2(%rip), %xmm3
	movdqa	.LC3(%rip), %xmm2
	shrl	$2, %esi
	.p2align 4,,10
	.p2align 3
.L98:
	movdqa	%xmm1, %xmm0
	movq	%rax, %rdx
	paddd	%xmm3, %xmm1
	addq	$1, %rax
	paddd	%xmm2, %xmm0
	salq	$4, %rdx
	movups	%xmm0, 4792(%r15,%rdx)
	cmpl	%eax, %esi
	ja	.L98
	movl	%ecx, %eax
	andl	$-4, %eax
	imull	$67372036, %eax, %edx
	leal	1(%rax), %esi
	cmpl	%eax, %ecx
	je	.L100
	leal	117835012(%rdx), %edi
	movl	%edi, (%r8,%rsi,4)
	leal	2(%rax), %esi
	cmpl	%esi, %ecx
	jb	.L100
	leal	185207048(%rdx), %edi
	addl	$3, %eax
	movl	%edi, (%r8,%rsi,4)
	cmpl	%eax, %ecx
	jb	.L100
	addl	$252579084, %edx
	movl	%edx, (%r8,%rax,4)
.L100:
	testl	%r12d, %r12d
	je	.L101
	leal	-1(%r12), %eax
	leaq	-1(%r8), %r13
	xorl	%r12d, %r12d
	leaq	1(%rbx,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L102:
	movzbl	(%rbx), %edx
	movq	%r8, %rdi
	movq	%r13, %rsi
	addq	$1, %rbx
	movzbl	(%r8,%rdx), %ecx
	orl	%edx, %r12d
	addq	$1, %rdx
	movb	%cl, -1(%rbx)
	movb	%cl, 4787(%r15)
	call	memmove@PLT
	movq	%rax, %r8
	cmpq	%r14, %rbx
	jne	.L102
	shrl	$2, %r12d
.L101:
	movl	%r12d, 4780(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L103:
	.cfi_restore_state
	movl	$50462976, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%eax, %esi
	addl	$67372036, %edx
	addl	$1, %eax
	movl	%edx, (%r8,%rsi,4)
	cmpl	%eax, %ecx
	jnb	.L97
	jmp	.L100
	.cfi_endproc
.LFE92:
	.size	InverseMoveToFrontTransform, .-InverseMoveToFrontTransform
	.p2align 4
	.type	BrotliCalculateRingBufferSize, @function
BrotliCalculateRingBufferSize:
.LFB112:
	.cfi_startproc
	movl	5076(%rdi), %ecx
	movl	88(%rdi), %eax
	movl	$1, %edx
	sall	%cl, %edx
	movl	$1024, %ecx
	testl	%eax, %eax
	movl	%ecx, %esi
	cmovne	%eax, %esi
	cmpl	%edx, %eax
	je	.L111
	movzbl	5072(%rdi), %eax
	testb	$4, %al
	jne	.L111
	xorl	%r8d, %r8d
	cmpq	$0, 128(%rdi)
	je	.L114
	movl	76(%rdi), %r8d
.L114:
	movl	272(%rdi), %ecx
	testb	$16, %al
	je	.L115
	addl	%r8d, %ecx
	movl	%edx, %eax
	cmpl	%esi, %ecx
	cmovl	%esi, %ecx
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%eax, %edx
	sarl	%eax
	cmpl	%ecx, %eax
	jge	.L116
.L115:
	movl	%edx, 5080(%rdi)
.L111:
	ret
	.cfi_endproc
.LFE112:
	.size	BrotliCalculateRingBufferSize, .-BrotliCalculateRingBufferSize
	.p2align 4
	.type	WriteRingBuffer, @function
WriteRingBuffer:
.LFB108:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	88(%rdi), %r12
	movslq	76(%rdi), %rax
	movq	392(%rdi), %rcx
	movl	272(%rbx), %r9d
	cmpl	%r12d, %eax
	movq	%r12, %rdi
	cmovg	%r12, %rax
	imulq	384(%rbx), %r12
	subq	%rcx, %r12
	addq	%rax, %r12
	movq	(%rsi), %rax
	movq	%r12, %rdx
	cmpq	%r12, %rax
	cmovbe	%rax, %rdx
	testl	%r9d, %r9d
	js	.L132
	movq	%rsi, %r14
	testq	%r13, %r13
	je	.L126
	movslq	92(%rbx), %rsi
	movq	0(%r13), %r9
	andq	%rcx, %rsi
	addq	128(%rbx), %rsi
	testq	%r9, %r9
	je	.L145
	movq	%r9, %rdi
	movl	%r8d, -60(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
	addq	%rdx, 0(%r13)
	movq	(%r14), %rax
	movl	88(%rbx), %edi
	movl	-60(%rbp), %r8d
.L126:
	subq	%rdx, %rax
	movq	%rax, (%r14)
	movq	392(%rbx), %rax
	addq	%rdx, %rax
	movq	%rax, 392(%rbx)
	testq	%r15, %r15
	je	.L128
	movq	%rax, (%r15)
.L128:
	movl	5076(%rbx), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cmpq	%r12, %rdx
	jnb	.L129
	cmpl	%eax, %edi
	je	.L136
	andl	$1, %r8d
	movl	$1, %r9d
	je	.L123
.L136:
	movl	$3, %r9d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$1, %r9d
	cmpl	%eax, %edi
	je	.L146
.L123:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	%rsi, 0(%r13)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L146:
	movl	76(%rbx), %eax
	cmpl	%edi, %eax
	jl	.L123
	subl	%edi, %eax
	addq	$1, 384(%rbx)
	testl	%eax, %eax
	movl	%eax, 76(%rbx)
	setne	%al
	leal	0(,%rax,8), %edx
	movzbl	5072(%rbx), %eax
	andl	$-9, %eax
	orl	%edx, %eax
	movb	%al, 5072(%rbx)
	jmp	.L123
.L132:
	movl	$-9, %r9d
	jmp	.L123
	.cfi_endproc
.LFE108:
	.size	WriteRingBuffer, .-WriteRingBuffer
	.p2align 4
	.type	WrapRingBuffer, @function
WrapRingBuffer:
.LFB109:
	.cfi_startproc
	testb	$8, 5072(%rdi)
	jne	.L156
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	76(%rbx), %rdx
	movq	136(%rbx), %rsi
	movq	128(%rdi), %rdi
	call	memcpy@PLT
	andb	$-9, 5072(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE109:
	.size	WrapRingBuffer, .-WrapRingBuffer
	.p2align 4
	.type	BrotliEnsureRingBuffer, @function
BrotliEnsureRingBuffer:
.LFB110:
	.cfi_startproc
	movslq	5080(%rdi), %rsi
	movl	$1, %eax
	cmpl	%esi, 88(%rdi)
	je	.L166
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$42, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	128(%rdi), %r12
	movq	56(%rdi), %rdi
	call	*40(%rbx)
	movq	%rax, 128(%rbx)
	testq	%rax, %rax
	je	.L169
	movslq	5080(%rbx), %rdx
	movb	$0, -2(%rax,%rdx)
	movslq	5080(%rbx), %rax
	movq	128(%rbx), %rdx
	movb	$0, -1(%rdx,%rax)
	testq	%r12, %r12
	je	.L160
	movq	128(%rbx), %rdi
	movslq	76(%rbx), %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	56(%rbx), %rdi
	movq	%r12, %rsi
	call	*48(%rbx)
.L160:
	movslq	5080(%rbx), %rax
	movl	%eax, 88(%rbx)
	leal	-1(%rax), %edx
	addq	128(%rbx), %rax
	movq	%rax, 136(%rbx)
	movl	$1, %eax
	movl	%edx, 92(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%r12, 128(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE110:
	.size	BrotliEnsureRingBuffer, .-BrotliEnsureRingBuffer
	.p2align 4
	.type	CopyUncompressedBlockToOutput, @function
CopyUncompressedBlockToOutput:
.LFB111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	BrotliEnsureRingBuffer
	testl	%eax, %eax
	je	.L181
	movl	5056(%rbx), %eax
	movl	$64, %r15d
	.p2align 4,,10
	.p2align 3
.L174:
	testl	%eax, %eax
	je	.L172
	cmpl	$1, %eax
	jne	.L174
.L173:
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	WriteRingBuffer
	cmpl	$1, %eax
	jne	.L170
	movl	5076(%rbx), %ecx
	sall	%cl, %eax
	cmpl	%eax, 88(%rbx)
	jne	.L179
	movl	80(%rbx), %eax
	movl	%eax, 84(%rbx)
.L179:
	movl	$0, 5056(%rbx)
.L172:
	movl	16(%rbx), %ecx
	movl	%r15d, %eax
	movslq	76(%rbx), %rdi
	movl	88(%rbx), %edx
	subl	%ecx, %eax
	movl	%eax, %r8d
	movl	%edx, %esi
	shrl	$3, %r8d
	addq	32(%rbx), %r8
	cmpl	%r8d, 272(%rbx)
	cmovle	272(%rbx), %r8d
	subl	%edi, %esi
	leal	(%rdi,%r8), %r9d
	cmpl	%edx, %r9d
	cmovg	%esi, %r8d
	addq	128(%rbx), %rdi
	movslq	%r8d, %rdx
	cmpl	$7, %eax
	ja	.L191
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L192:
	movq	8(%rbx), %rax
	addq	$1, %rdi
	subq	$1, %rdx
	shrq	%cl, %rax
	movb	%al, -1(%rdi)
	movl	16(%rbx), %eax
	leal	8(%rax), %ecx
	movl	%r15d, %eax
	subl	%ecx, %eax
	movl	%ecx, 16(%rbx)
	cmpl	$7, %eax
	jbe	.L176
.L191:
	testq	%rdx, %rdx
	jne	.L192
.L176:
	movq	24(%rbx), %rsi
	movl	%r8d, -60(%rbp)
	movq	%rdx, -56(%rbp)
	call	memcpy@PLT
	movl	-60(%rbp), %r8d
	movl	76(%rbx), %esi
	movl	272(%rbx), %eax
	movl	5076(%rbx), %ecx
	movq	-56(%rbp), %rdx
	subq	%rdx, 32(%rbx)
	addl	%r8d, %esi
	addq	%rdx, 24(%rbx)
	movl	$1, %edx
	subl	%r8d, %eax
	sall	%cl, %edx
	movl	%esi, 76(%rbx)
	movl	%eax, 272(%rbx)
	cmpl	%edx, %esi
	jl	.L193
	movl	$1, 5056(%rbx)
	jmp	.L173
.L181:
	movl	$-26, %eax
.L170:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L193:
	.cfi_restore_state
	testl	%eax, %eax
	setne	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%r12
	addl	$1, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE111:
	.size	CopyUncompressedBlockToOutput, .-CopyUncompressedBlockToOutput
	.p2align 4
	.type	DecodeLiteralBlockSwitch, @function
DecodeLiteralBlockSwitch:
.LFB99:
	.cfi_startproc
	movl	292(%rdi), %esi
	cmpl	$1, %esi
	jbe	.L207
	movl	16(%rdi), %ecx
	movq	248(%rdi), %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	256(%rdi), %r10
	movq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$47, %ecx
	jbe	.L196
	movq	24(%rdi), %rdx
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rdi)
	movq	(%rdx), %r9
	addq	$6, %rdx
	subq	$6, 32(%rdi)
	movq	%rdx, 24(%rdi)
	salq	$16, %r9
	orq	%r9, %rax
	movq	%rax, 8(%rdi)
.L196:
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movzbl	%dl, %r9d
	leaq	(%r8,%r9,4), %r11
	leaq	kBitMask(%rip), %r9
	movzbl	(%r11), %ebx
	movzwl	2(%r11), %r8d
	cmpb	$8, %bl
	jbe	.L197
	subl	$8, %ebx
	shrl	$8, %edx
	addl	$8, %ecx
	andl	(%r9,%rbx,4), %edx
	addl	%edx, %r8d
	leaq	(%r11,%r8,4), %rdx
	movzbl	(%rdx), %ebx
	movzwl	2(%rdx), %r8d
.L197:
	addl	%ebx, %ecx
	movl	%ecx, 16(%rdi)
	cmpl	$47, %ecx
	jbe	.L198
	movq	24(%rdi), %rdx
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rdi)
	movq	(%rdx), %r11
	addq	$6, %rdx
	subq	$6, 32(%rdi)
	movq	%rdx, 24(%rdi)
	salq	$16, %r11
	orq	%r11, %rax
	movq	%rax, 8(%rdi)
.L198:
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movzbl	%dl, %r11d
	leaq	(%r10,%r11,4), %r11
	movzbl	(%r11), %r10d
	movzwl	2(%r11), %ebx
	cmpb	$8, %r10b
	jbe	.L199
	subl	$8, %r10d
	shrl	$8, %edx
	addl	$8, %ecx
	andl	(%r9,%r10,4), %edx
	addl	%ebx, %edx
	leaq	(%r11,%rdx,4), %rdx
	movzbl	(%rdx), %r10d
	movzwl	2(%rdx), %ebx
.L199:
	leaq	kBlockLengthPrefixCode(%rip), %r11
	movl	%ebx, %edx
	addl	%r10d, %ecx
	movzbl	2(%r11,%rdx,4), %r10d
	movzwl	(%r11,%rdx,4), %edx
	cmpl	$31, %ecx
	jbe	.L200
	movq	24(%rdi), %r11
	xorl	$32, %ecx
	shrq	$32, %rax
	movl	%ecx, 16(%rdi)
	movl	(%r11), %ebx
	addq	$4, %r11
	subq	$4, 32(%rdi)
	movq	%r11, 24(%rdi)
	salq	$32, %rbx
	orq	%rbx, %rax
	movq	%rax, 8(%rdi)
.L200:
	movl	%r10d, %r11d
	shrq	%cl, %rax
	addl	%r10d, %ecx
	andl	(%r9,%r11,4), %eax
	movl	%ecx, 16(%rdi)
	addl	%edx, %eax
	movl	308(%rdi), %edx
	movl	%eax, 280(%rdi)
	leal	1(%rdx), %eax
	cmpl	$1, %r8d
	je	.L202
	leal	-2(%r8), %eax
	testl	%r8d, %r8d
	jne	.L202
	movl	304(%rdi), %eax
.L202:
	movl	%eax, %ecx
	movl	%edx, 304(%rdi)
	popq	%rbx
	subl	%esi, %ecx
	cmpl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmovbe	%rcx, %rax
	movl	%eax, %ecx
	movl	%eax, 308(%rdi)
	movl	%eax, %edx
	shrl	$5, %ecx
	sall	$6, %edx
	addq	5088(%rdi), %rdx
	movl	5120(%rdi,%rcx,4), %esi
	movl	%eax, %ecx
	movq	%rdx, 160(%rdi)
	andl	$31, %ecx
	shrq	%cl, %rsi
	andl	$1, %esi
	movl	%esi, 264(%rdi)
	movzbl	(%rdx), %ecx
	movq	176(%rdi), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 352(%rdi)
	movq	5096(%rdi), %rdx
	movzbl	(%rdx,%rax), %eax
	leaq	kContextLookup(%rip), %rdx
	salq	$9, %rax
	andl	$1536, %eax
	addq	%rdx, %rax
	movq	%rax, 152(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE99:
	.size	DecodeLiteralBlockSwitch, .-DecodeLiteralBlockSwitch
	.p2align 4
	.type	DecodeCommandBlockSwitch, @function
DecodeCommandBlockSwitch:
.LFB102:
	.cfi_startproc
	movl	296(%rdi), %esi
	cmpl	$1, %esi
	jbe	.L223
	movl	16(%rdi), %ecx
	movq	248(%rdi), %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	256(%rdi), %r10
	movq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$47, %ecx
	jbe	.L212
	movq	24(%rdi), %rdx
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rdi)
	movq	(%rdx), %r9
	addq	$6, %rdx
	subq	$6, 32(%rdi)
	movq	%rdx, 24(%rdi)
	salq	$16, %r9
	orq	%r9, %rax
	movq	%rax, 8(%rdi)
.L212:
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movzbl	%dl, %r9d
	leaq	2528(%r8,%r9,4), %r11
	leaq	kBitMask(%rip), %r9
	movzbl	(%r11), %ebx
	movzwl	2(%r11), %r8d
	cmpb	$8, %bl
	jbe	.L213
	subl	$8, %ebx
	shrl	$8, %edx
	addl	$8, %ecx
	andl	(%r9,%rbx,4), %edx
	addl	%edx, %r8d
	leaq	(%r11,%r8,4), %rdx
	movzbl	(%rdx), %ebx
	movzwl	2(%rdx), %r8d
.L213:
	addl	%ebx, %ecx
	movl	%ecx, 16(%rdi)
	cmpl	$47, %ecx
	jbe	.L214
	movq	24(%rdi), %rdx
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rdi)
	movq	(%rdx), %r11
	addq	$6, %rdx
	subq	$6, 32(%rdi)
	movq	%rdx, 24(%rdi)
	salq	$16, %r11
	orq	%r11, %rax
	movq	%rax, 8(%rdi)
.L214:
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movzbl	%dl, %r11d
	leaq	1584(%r10,%r11,4), %r11
	movzbl	(%r11), %r10d
	movzwl	2(%r11), %ebx
	cmpb	$8, %r10b
	jbe	.L215
	subl	$8, %r10d
	shrl	$8, %edx
	addl	$8, %ecx
	andl	(%r9,%r10,4), %edx
	addl	%ebx, %edx
	leaq	(%r11,%rdx,4), %rdx
	movzbl	(%rdx), %r10d
	movzwl	2(%rdx), %ebx
.L215:
	leaq	kBlockLengthPrefixCode(%rip), %r11
	movl	%ebx, %edx
	addl	%r10d, %ecx
	movzbl	2(%r11,%rdx,4), %r10d
	movzwl	(%r11,%rdx,4), %edx
	cmpl	$31, %ecx
	jbe	.L216
	movq	24(%rdi), %r11
	xorl	$32, %ecx
	shrq	$32, %rax
	movl	%ecx, 16(%rdi)
	movl	(%r11), %ebx
	addq	$4, %r11
	subq	$4, 32(%rdi)
	movq	%r11, 24(%rdi)
	salq	$32, %rbx
	orq	%rbx, %rax
	movq	%rax, 8(%rdi)
.L216:
	movl	%r10d, %r11d
	shrq	%cl, %rax
	addl	%r10d, %ecx
	andl	(%r9,%r11,4), %eax
	movl	%ecx, 16(%rdi)
	addl	%edx, %eax
	movl	316(%rdi), %edx
	movl	%eax, 284(%rdi)
	leal	1(%rdx), %eax
	cmpl	$1, %r8d
	je	.L218
	leal	-2(%r8), %eax
	testl	%r8d, %r8d
	jne	.L218
	movl	312(%rdi), %eax
.L218:
	movl	%eax, %ecx
	movl	%edx, 312(%rdi)
	movq	200(%rdi), %rdx
	subl	%esi, %ecx
	cmpl	%eax, %esi
	popq	%rbx
	popq	%r12
	cmovbe	%rcx, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, 316(%rdi)
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 144(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE102:
	.size	DecodeCommandBlockSwitch, .-DecodeCommandBlockSwitch
	.p2align 4
	.type	DecodeDistanceBlockSwitch, @function
DecodeDistanceBlockSwitch:
.LFB105:
	.cfi_startproc
	movl	300(%rdi), %esi
	cmpl	$1, %esi
	jbe	.L239
	movl	16(%rdi), %ecx
	movq	248(%rdi), %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	256(%rdi), %r10
	movq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$47, %ecx
	jbe	.L228
	movq	24(%rdi), %rdx
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rdi)
	movq	(%rdx), %r9
	addq	$6, %rdx
	subq	$6, 32(%rdi)
	movq	%rdx, 24(%rdi)
	salq	$16, %r9
	orq	%r9, %rax
	movq	%rax, 8(%rdi)
.L228:
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movzbl	%dl, %r9d
	leaq	5056(%r8,%r9,4), %r11
	leaq	kBitMask(%rip), %r9
	movzbl	(%r11), %ebx
	movzwl	2(%r11), %r8d
	cmpb	$8, %bl
	jbe	.L229
	subl	$8, %ebx
	shrl	$8, %edx
	addl	$8, %ecx
	andl	(%r9,%rbx,4), %edx
	addl	%edx, %r8d
	leaq	(%r11,%r8,4), %rdx
	movzbl	(%rdx), %ebx
	movzwl	2(%rdx), %r8d
.L229:
	addl	%ebx, %ecx
	movl	%ecx, 16(%rdi)
	cmpl	$47, %ecx
	jbe	.L230
	movq	24(%rdi), %rdx
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rdi)
	movq	(%rdx), %r11
	addq	$6, %rdx
	subq	$6, 32(%rdi)
	movq	%rdx, 24(%rdi)
	salq	$16, %r11
	orq	%r11, %rax
	movq	%rax, 8(%rdi)
.L230:
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movzbl	%dl, %r11d
	leaq	3168(%r10,%r11,4), %r11
	movzbl	(%r11), %r10d
	movzwl	2(%r11), %ebx
	cmpb	$8, %r10b
	jbe	.L231
	subl	$8, %r10d
	shrl	$8, %edx
	addl	$8, %ecx
	andl	(%r9,%r10,4), %edx
	addl	%ebx, %edx
	leaq	(%r11,%rdx,4), %rdx
	movzbl	(%rdx), %r10d
	movzwl	2(%rdx), %ebx
.L231:
	leaq	kBlockLengthPrefixCode(%rip), %r11
	movl	%ebx, %edx
	addl	%r10d, %ecx
	movzbl	2(%r11,%rdx,4), %r10d
	movzwl	(%r11,%rdx,4), %edx
	cmpl	$31, %ecx
	jbe	.L232
	movq	24(%rdi), %r11
	xorl	$32, %ecx
	shrq	$32, %rax
	movl	%ecx, 16(%rdi)
	movl	(%r11), %ebx
	addq	$4, %r11
	subq	$4, 32(%rdi)
	movq	%r11, 24(%rdi)
	salq	$32, %rbx
	orq	%rbx, %rax
	movq	%rax, 8(%rdi)
.L232:
	movl	%r10d, %r11d
	shrq	%cl, %rax
	addl	%r10d, %ecx
	andl	(%r9,%r11,4), %eax
	movl	%ecx, 16(%rdi)
	addl	%edx, %eax
	movl	324(%rdi), %edx
	movl	%eax, 288(%rdi)
	leal	1(%rdx), %eax
	cmpl	$1, %r8d
	je	.L234
	leal	-2(%r8), %eax
	testl	%r8d, %r8d
	jne	.L234
	movl	320(%rdi), %eax
.L234:
	movl	%eax, %ecx
	movl	%edx, 320(%rdi)
	movslq	268(%rdi), %rdx
	subl	%esi, %ecx
	cmpl	%eax, %esi
	popq	%rbx
	popq	%r12
	cmovbe	%ecx, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, 324(%rdi)
	sall	$2, %eax
	addq	344(%rdi), %rax
	movq	%rax, 168(%rdi)
	movzbl	(%rax,%rdx), %eax
	movb	%al, 360(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE105:
	.size	DecodeDistanceBlockSwitch, .-DecodeDistanceBlockSwitch
	.section	.text.unlikely
.LCOLDB8:
	.text
.LHOTB8:
	.p2align 4
	.type	ReadHuffmanCode, @function
ReadHuffmanCode:
.LFB89:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$5, 5060(%r8)
	movl	%esi, -52(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	ja	.L337
	movl	5060(%r8), %eax
	leaq	.L245(%rip), %rdx
	movq	%r8, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L245:
	.long	.L250-.L245
	.long	.L249-.L245
	.long	.L248-.L245
	.long	.L247-.L245
	.long	.L246-.L245
	.long	.L244-.L245
	.text
	.p2align 4,,10
	.p2align 3
.L247:
	movl	400(%r8), %r8d
.L278:
	cmpl	$3, %r8d
	jne	.L275
	movl	16(%rbx), %ecx
	cmpl	$64, %ecx
	jne	.L424
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L282
	movq	8(%rbx), %rcx
	movq	24(%rbx), %rdx
	subq	$1, %rax
	shrq	$8, %rcx
	addq	$1, %rdx
	movq	%rcx, 8(%rbx)
	movzbl	-1(%rdx), %r8d
	movq	%rdx, 24(%rbx)
	salq	$56, %r8
	movq	%rax, 32(%rbx)
	movl	$57, %eax
	orq	%rcx, %r8
	movl	$56, %ecx
	movq	%r8, 8(%rbx)
.L281:
	shrq	%cl, %r8
	movl	%eax, 16(%rbx)
	andl	$1, %r8d
	addl	$3, %r8d
	movl	%r8d, 400(%rbx)
	.p2align 4,,10
	.p2align 3
.L275:
	movq	-64(%rbp), %rdi
	leaq	552(%rbx), %rdx
	movl	%r8d, %ecx
	movl	$8, %esi
	call	BrotliBuildSimpleHuffmanTable
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L284
.L417:
	movl	%eax, (%rdi)
.L284:
	movl	$0, 5060(%rbx)
	movl	$1, %eax
.L242:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movl	368(%rbx), %eax
	movl	400(%rbx), %r12d
	leaq	2138(%r8), %r8
	movl	404(%rbx), %r13d
	movl	408(%rbx), %r14d
	movl	364(%rbx), %edx
	movq	544(%rbx), %r15
	movl	%eax, -56(%rbp)
	leaq	1992(%rbx), %rax
	movq	%rax, -80(%rbp)
.L251:
	leaq	8(%rbx), %rdi
	movq	%r8, -88(%rbp)
	movl	%edx, -92(%rbp)
	call	BrotliWarmupBitReader
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	je	.L298
	cmpl	%r12d, -52(%rbp)
	jbe	.L299
	testl	%r14d, %r14d
	je	.L299
	movl	-92(%rbp), %edx
	leaq	412(%rbx), %rdi
	movl	$32768, %r11d
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L426:
	testl	%r9d, %r9d
	je	.L305
	movq	-80(%rbp), %rax
	movl	%r9d, -56(%rbp)
	leaq	(%rax,%r9,4), %rax
	movslq	(%rax), %rcx
	movw	%r12w, (%r15,%rcx,2)
	movl	%r9d, %ecx
	movl	%r12d, (%rax)
	movl	%r11d, %eax
	addw	$1, (%r8,%r9,2)
	shrl	%cl, %eax
	subl	%eax, %r14d
.L305:
	addl	$1, %r12d
	xorl	%r13d, %r13d
.L306:
	cmpl	%r12d, -52(%rbp)
	jbe	.L299
	testl	%r14d, %r14d
	je	.L299
.L311:
	movq	32(%rbx), %rsi
	cmpq	$3, %rsi
	jbe	.L425
	movl	16(%rbx), %ecx
	movq	8(%rbx), %rax
	cmpl	$31, %ecx
	jbe	.L303
	movq	24(%rbx), %r9
	xorl	$32, %ecx
	shrq	$32, %rax
	subq	$4, %rsi
	movl	%ecx, 16(%rbx)
	movl	(%r9), %r10d
	addq	$4, %r9
	movq	%rsi, 32(%rbx)
	movq	%r9, 24(%rbx)
	salq	$32, %r10
	orq	%r10, %rax
	movq	%rax, 8(%rbx)
.L303:
	movq	%rax, %rsi
	shrq	%cl, %rsi
	andl	$31, %esi
	leaq	(%rdi,%rsi,4), %rsi
	movzbl	(%rsi), %r9d
	addl	%r9d, %ecx
	movzwl	2(%rsi), %r9d
	movl	%ecx, 16(%rbx)
	cmpw	$15, %r9w
	jbe	.L426
	cmpl	$16, %r9d
	je	.L427
	shrq	%cl, %rax
	addl	$3, %ecx
	xorl	%r9d, %r9d
	movl	%ecx, 16(%rbx)
	andl	$7, %eax
	movl	$3, %ecx
.L335:
	cmpl	%edx, %r9d
	movl	$0, %edx
	je	.L428
	xorl	%r13d, %r13d
.L309:
	leal	3(%r13,%rax), %r13d
	movl	%r13d, %eax
	subl	%edx, %eax
	movl	%eax, %edx
	leal	(%rax,%r12), %esi
	cmpl	%esi, -52(%rbp)
	jb	.L345
	testl	%r9d, %r9d
	je	.L346
	movq	-80(%rbp), %rax
	movl	%r9d, %r10d
	leaq	(%rax,%r10,4), %rcx
	movslq	(%rcx), %rax
	.p2align 4,,10
	.p2align 3
.L310:
	movw	%r12w, (%r15,%rax,2)
	movslq	%r12d, %rax
	addl	$1, %r12d
	cmpl	%r12d, %esi
	jne	.L310
	leal	-1(%rsi), %eax
	movl	%esi, %r12d
	movl	%eax, (%rcx)
	movl	$15, %ecx
	movl	%edx, %eax
	subl	%r9d, %ecx
	addw	%dx, (%r8,%r10,2)
	movl	%r9d, %edx
	sall	%cl, %eax
	subl	%eax, %r14d
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L250:
	movl	16(%r8), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	ja	.L253
	movq	32(%r8), %rdx
	movl	$64, %r9d
.L255:
	testq	%rdx, %rdx
	je	.L422
	movq	8(%rbx), %r8
	movq	24(%rbx), %rsi
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r8
	addq	$1, %rsi
	movq	%r8, 8(%rbx)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%rbx)
	movl	%r9d, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%rbx)
	orq	%r8, %rax
	movq	%rdx, 32(%rbx)
	movq	%rax, 8(%rbx)
	cmpl	$1, %esi
	jbe	.L255
.L256:
	shrq	%cl, %rax
	addl	$2, %ecx
	andl	$3, %eax
	movl	%ecx, 16(%rbx)
	movl	%eax, 120(%rbx)
	cmpl	$1, %eax
	je	.L252
	movl	120(%rbx), %edi
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	movabsq	$137438953472, %rax
	movw	%dx, 2136(%rbx)
	movl	$32, %r9d
	movq	%rax, 404(%rbx)
	movq	$0, 2138(%rbx)
	movl	$0, 2146(%rbx)
	movl	$4, 5060(%rbx)
	movups	%xmm0, 2120(%rbx)
	cmpl	$17, %edi
	jbe	.L260
.L342:
	movl	$-6, %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L249:
	movl	16(%r8), %ecx
.L252:
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	ja	.L429
	movq	32(%rbx), %rax
	movl	$64, %r9d
.L262:
	testq	%rax, %rax
	je	.L261
	movq	8(%rbx), %rsi
	movq	24(%rbx), %rdx
	subl	$8, %ecx
	subq	$1, %rax
	shrq	$8, %rsi
	addq	$1, %rdx
	movq	%rsi, 8(%rbx)
	movzbl	-1(%rdx), %r8d
	movq	%rdx, 24(%rbx)
	movl	%r9d, %edx
	salq	$56, %r8
	subl	%ecx, %edx
	movl	%ecx, 16(%rbx)
	orq	%rsi, %r8
	movq	%rax, 32(%rbx)
	movq	%r8, 8(%rbx)
	cmpl	$1, %edx
	jbe	.L262
.L263:
	shrq	%cl, %r8
	andl	$2047, %edi
	addl	$2, %ecx
	xorl	%r9d, %r9d
	andl	$3, %r8d
	movl	%edi, %eax
	movl	%ecx, 16(%rbx)
	movl	%r8d, 400(%rbx)
	movl	$0, 120(%rbx)
	subl	$1, %eax
	jne	.L264
	xorl	%edx, %edx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L248:
	andl	$2047, %edi
	movl	120(%r8), %r9d
	movl	400(%r8), %r8d
	movl	%edi, %eax
	subl	$1, %eax
	je	.L339
.L264:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L267:
	addl	$1, %edx
	shrl	%eax
	jne	.L267
.L266:
	cmpl	%r9d, %r8d
	jb	.L268
.L265:
	movl	16(%rbx), %ecx
	movl	-52(%rbp), %r13d
	movl	$64, %edi
	movl	%edx, %r10d
	leaq	kBitMask(%rip), %r11
	.p2align 4,,10
	.p2align 3
.L269:
	movl	%edi, %eax
	subl	%ecx, %eax
	cmpl	%edx, %eax
	jnb	.L430
	movq	32(%rbx), %r14
.L271:
	testq	%r14, %r14
	je	.L270
	movq	8(%rbx), %r12
	movq	24(%rbx), %rsi
	subl	$8, %ecx
	subq	$1, %r14
	shrq	$8, %r12
	addq	$1, %rsi
	movq	%r12, 8(%rbx)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%rbx)
	movl	%edi, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%rbx)
	orq	%r12, %rax
	movq	%r14, 32(%rbx)
	movq	%rax, 8(%rbx)
	cmpl	%edx, %esi
	jb	.L271
.L274:
	shrq	%cl, %rax
	addl	%edx, %ecx
	andl	(%r11,%r10,4), %eax
	movl	%ecx, 16(%rbx)
	cmpl	%eax, %r13d
	jbe	.L431
	movl	%r9d, %esi
	addl	$1, %r9d
	movw	%ax, 552(%rbx,%rsi,2)
	cmpl	%r8d, %r9d
	jbe	.L269
.L268:
	testl	%r8d, %r8d
	je	.L275
	leaq	552(%rbx), %rdi
	xorl	%esi, %esi
.L279:
	addl	$1, %esi
	cmpl	%r8d, %esi
	ja	.L276
	movzwl	(%rdi), %ecx
	movl	%esi, %eax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L433:
	addl	$1, %eax
	cmpl	%r8d, %eax
	ja	.L432
.L277:
	movl	%eax, %edx
	cmpw	552(%rbx,%rdx,2), %cx
	jne	.L433
	movl	$-5, %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L246:
	movl	120(%r8), %edi
	movl	404(%r8), %r11d
	movl	408(%r8), %r9d
	cmpl	$17, %edi
	ja	.L285
.L260:
	movl	%edi, %r8d
	leaq	kCodeLengthCodeOrder(%rip), %rax
	movl	16(%rbx), %edx
	movl	$64, %r10d
	addq	%rax, %r8
	leaq	kCodeLengthPrefixValue(%rip), %r13
	movl	$32, %r12d
	.p2align 4,,10
	.p2align 3
.L293:
	movl	%r10d, %eax
	movzbl	(%r8), %esi
	subl	%edx, %eax
	cmpl	$3, %eax
	ja	.L286
	movq	32(%rbx), %rcx
.L288:
	movl	%r10d, %r14d
	subl	%edx, %r14d
	testq	%rcx, %rcx
	je	.L287
	movq	8(%rbx), %r15
	movq	24(%rbx), %r14
	subl	$8, %edx
	subq	$1, %rcx
	shrq	$8, %r15
	addq	$1, %r14
	movq	%r15, 8(%rbx)
	movzbl	-1(%r14), %eax
	movq	%r14, 24(%rbx)
	movl	%r10d, %r14d
	salq	$56, %rax
	subl	%edx, %r14d
	movl	%edx, 16(%rbx)
	orq	%r15, %rax
	movq	%rcx, 32(%rbx)
	movq	%rax, 8(%rbx)
	cmpl	$3, %r14d
	jbe	.L288
.L289:
	movl	%edx, %ecx
	shrq	%cl, %rax
	leaq	kCodeLengthPrefixLength(%rip), %rcx
	andl	$15, %eax
	movzbl	(%rcx,%rax), %r15d
.L290:
	movzbl	0(%r13,%rax), %ecx
	addl	%r15d, %edx
	movl	%edx, 16(%rbx)
	movb	%cl, 2120(%rbx,%rsi)
	testb	%cl, %cl
	je	.L292
	addw	$1, 2138(%rbx,%rcx,2)
	movl	%r12d, %eax
	addl	$1, %r11d
	shrl	%cl, %eax
	subl	%eax, %r9d
	leal	-1(%r9), %eax
	cmpl	$31, %eax
	ja	.L285
.L292:
	addl	$1, %edi
	addq	$1, %r8
	cmpl	$18, %edi
	jne	.L293
.L285:
	cmpl	$1, %r11d
	je	.L351
	testl	%r9d, %r9d
	jne	.L342
.L351:
	leaq	2138(%rbx), %r8
	leaq	2120(%rbx), %rsi
	movq	%r8, %rdx
	leaq	412(%rbx), %rdi
	movq	%r8, -80(%rbp)
	call	BrotliBuildCodeLengthsHuffmanTable
	movq	-80(%rbp), %r8
	pxor	%xmm0, %xmm0
	leaq	2056(%rbx), %rax
	movups	%xmm0, 2138(%rbx)
	movups	%xmm0, 16(%r8)
	movq	544(%rbx), %r15
	leaq	-32(%r15), %rdx
	cmpq	%rax, %rdx
	leaq	1992(%rbx), %rax
	movq	%rax, -80(%rbp)
	jnb	.L352
	cmpq	%rax, %r15
	ja	.L295
.L352:
	movdqa	.LC4(%rip), %xmm0
	movdqa	.LC6(%rip), %xmm1
	movups	%xmm0, 1992(%rbx)
	movdqa	.LC5(%rip), %xmm0
	movups	%xmm0, 2008(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movups	%xmm0, -32(%r15)
	movups	%xmm1, 2024(%rbx)
	movdqa	.LC7(%rip), %xmm1
	movups	%xmm1, 2040(%rbx)
	movups	%xmm0, -16(%r15)
.L297:
	movabsq	$34359738368, %rax
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	movq	$0, 400(%rbx)
	movq	%rax, 364(%rbx)
	movl	$32768, %r14d
	xorl	%r12d, %r12d
	movl	$32768, 408(%rbx)
	movl	$5, 5060(%rbx)
	movl	$8, -56(%rbp)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L287:
	testl	%r14d, %r14d
	jne	.L434
.L291:
	movl	%edi, 120(%rbx)
	movl	$2, %eax
	movl	%r11d, 404(%rbx)
	movl	%r9d, 408(%rbx)
	movl	$4, 5060(%rbx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L270:
	movl	%r9d, 120(%rbx)
	movl	$2, %eax
	movl	$2, 5060(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movl	$1, 5060(%rbx)
.L422:
	addq	$56, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movl	%edx, %ecx
	shrq	%cl, %rax
	leaq	kCodeLengthPrefixLength(%rip), %rcx
	andl	$15, %eax
	movzbl	(%rcx,%rax), %r15d
	cmpl	%r14d, %r15d
	jbe	.L290
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L345:
	movl	$1048575, %r14d
.L299:
	movl	%r14d, 408(%rbx)
.L302:
	testl	%r14d, %r14d
	jne	.L350
.L421:
	movq	544(%rbx), %r9
.L334:
	movq	-64(%rbp), %rdi
	movq	%r8, %rcx
	movq	%r9, %rdx
	movl	$8, %esi
	call	BrotliBuildHuffmanTable
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L417
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L428:
	testl	%r13d, %r13d
	je	.L309
	leal	-2(%r13), %esi
	movl	%r13d, %edx
	sall	%cl, %esi
	movl	%esi, %r13d
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L427:
	shrq	%cl, %rax
	addl	$2, %ecx
	movl	-56(%rbp), %r9d
	movl	%ecx, 16(%rbx)
	andl	$3, %eax
	movl	$2, %ecx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L346:
	xorl	%edx, %edx
	movl	%esi, %r12d
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L286:
	movq	8(%rbx), %rax
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$3, 5060(%rbx)
	movl	$2, %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L425:
	movl	-56(%rbp), %eax
	movl	%r12d, 400(%rbx)
	movl	%r13d, 404(%rbx)
	movl	%eax, 368(%rbx)
	movl	%edx, 364(%rbx)
	movl	%r14d, 408(%rbx)
	cmpl	%r12d, -52(%rbp)
	jbe	.L302
.L301:
	movl	16(%rbx), %eax
	movl	$64, %r15d
.L314:
	movl	%r15d, %r9d
	subl	%eax, %r9d
	je	.L347
	movq	8(%rbx), %rdx
	movl	%eax, %ecx
	shrq	%cl, %rdx
	movl	%edx, %esi
	andl	$31, %edx
	leaq	(%rdi,%rdx,4), %rcx
.L316:
	movzbl	(%rcx), %edx
	movl	%edx, %r10d
	cmpl	%edx, %r9d
	jb	.L322
	movzwl	2(%rcx), %r11d
	cmpw	$15, %r11w
	jbe	.L435
	leal	-14(%r11), %ecx
	addl	%ecx, %edx
	cmpl	%edx, %r9d
	jb	.L322
	addl	%edx, %eax
	leaq	kBitMask(%rip), %r14
	movq	544(%rbx), %r9
	xorl	%edx, %edx
	movl	(%r14,%rcx,4), %r14d
	movl	%eax, 16(%rbx)
	movl	$3, %ecx
	cmpl	$16, %r11d
	jne	.L323
	movl	368(%rbx), %edx
	movl	$2, %ecx
.L323:
	cmpl	%edx, 364(%rbx)
	je	.L324
	movl	%edx, 364(%rbx)
	movl	$3, %r11d
	xorl	%r13d, %r13d
.L325:
	movl	%r10d, %ecx
	shrl	%cl, %esi
	andl	%esi, %r14d
	addl	%r11d, %r14d
	movl	%r14d, 404(%rbx)
	subl	%r13d, %r14d
	addl	%r14d, %r12d
	cmpl	%r12d, -52(%rbp)
	jb	.L436
	testl	%edx, %edx
	je	.L327
	movq	-80(%rbp), %rax
	leaq	(%rax,%rdx,4), %rcx
	movl	400(%rbx), %eax
	movslq	(%rcx), %rdx
	.p2align 4,,10
	.p2align 3
.L328:
	movw	%ax, (%r9,%rdx,2)
	movslq	%eax, %rdx
	addl	$1, %eax
	cmpl	%eax, %r12d
	jne	.L328
	movl	%r12d, 400(%rbx)
	subl	$1, %r12d
	movl	%r14d, %edx
	movl	%r12d, (%rcx)
	movl	364(%rbx), %eax
	movl	$15, %ecx
	movl	400(%rbx), %r12d
	subl	%eax, %ecx
	sall	%cl, %edx
	movl	408(%rbx), %ecx
	subl	%edx, %ecx
	movl	%ecx, 408(%rbx)
	movl	%ecx, %edx
	addw	%r14w, (%r8,%rax,2)
	cmpl	%r12d, -52(%rbp)
	ja	.L321
.L414:
	movl	%edx, %r14d
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%rdi, %rcx
	xorl	%esi, %esi
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L322:
	movl	400(%rbx), %r12d
	movl	408(%rbx), %edx
	cmpl	%r12d, -52(%rbp)
	jbe	.L414
	testl	%edx, %edx
	je	.L421
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L422
	movq	8(%rbx), %r9
	movq	24(%rbx), %rsi
	subl	$8, %eax
	subq	$1, %rdx
	shrq	$8, %r9
	addq	$1, %rsi
	movq	%r9, 8(%rbx)
	movzbl	-1(%rsi), %ecx
	movl	%eax, 16(%rbx)
	salq	$56, %rcx
	movq	%rdx, 32(%rbx)
	orq	%r9, %rcx
	movq	%rsi, 24(%rbx)
	movq	%rcx, 8(%rbx)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L430:
	movq	8(%rbx), %rax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L435:
	addl	%edx, %eax
	movq	544(%rbx), %r9
	movl	$0, 404(%rbx)
	movl	%eax, 16(%rbx)
	testl	%r11d, %r11d
	jne	.L319
	movl	408(%rbx), %edx
.L320:
	movl	400(%rbx), %eax
	leal	1(%rax), %r12d
	movl	%r12d, 400(%rbx)
	cmpl	%r12d, -52(%rbp)
	jbe	.L414
.L321:
	testl	%edx, %edx
	je	.L334
	movl	16(%rbx), %eax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L339:
	xorl	%edx, %edx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L298:
	movl	400(%rbx), %r12d
	movl	408(%rbx), %r14d
	cmpl	%r12d, -52(%rbp)
	jbe	.L302
	testl	%r14d, %r14d
	je	.L421
	leaq	412(%rbx), %rdi
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$-4, %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L432:
	addq	$2, %rdi
	cmpl	%r8d, %esi
	jne	.L279
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$-1, %eax
	movl	$-1, %edx
	movl	$-1, %ecx
	movl	$-16, 1992(%rbx)
	movl	$-1, %esi
	movw	%ax, -32(%r15)
	movl	$-1, %edi
	movl	$-1, %eax
	movl	$-1, %r9d
	movl	$-1, %r10d
	movl	$-1, %r11d
	movl	$-1, %r12d
	movl	$-1, %r13d
	movl	$-1, %r14d
	movl	$-15, 1996(%rbx)
	movw	%dx, -30(%r15)
	movl	$-14, 2000(%rbx)
	movw	%cx, -28(%r15)
	movl	$-13, 2004(%rbx)
	movw	%si, -26(%r15)
	movl	$-12, 2008(%rbx)
	movw	%di, -24(%r15)
	movl	$-11, 2012(%rbx)
	movw	%r9w, -22(%r15)
	movl	$-10, 2016(%rbx)
	movw	%r10w, -20(%r15)
	movl	$-9, 2020(%rbx)
	movw	%r11w, -18(%r15)
	movl	$-8, 2024(%rbx)
	movw	%r12w, -16(%r15)
	movl	$-7, 2028(%rbx)
	movw	%r13w, -14(%r15)
	movl	$-6, 2032(%rbx)
	movw	%r14w, -12(%r15)
	movl	$-5, 2036(%rbx)
	movw	%ax, -10(%r15)
	movl	$-1, %eax
	movl	$-4, 2040(%rbx)
	movw	%ax, -8(%r15)
	movl	$-1, %eax
	movl	$-3, 2044(%rbx)
	movw	%ax, -6(%r15)
	movl	$-1, %eax
	movl	$-2, 2048(%rbx)
	movw	%ax, -4(%r15)
	movl	$-1, %eax
	movl	$-1, 2052(%rbx)
	movw	%ax, -2(%r15)
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-80(%rbp), %rdx
	movzwl	%r11w, %eax
	leaq	(%rdx,%rax,4), %rdx
	movslq	(%rdx), %rcx
	movw	%r12w, (%r9,%rcx,2)
	movl	%r11d, %ecx
	movl	%r12d, (%rdx)
	movl	$32768, %edx
	shrl	%cl, %edx
	movl	408(%rbx), %ecx
	movl	%r11d, 368(%rbx)
	subl	%edx, %ecx
	movl	%ecx, 408(%rbx)
	movl	%ecx, %edx
	addw	$1, (%r8,%rax,2)
	jmp	.L320
.L324:
	movl	404(%rbx), %r13d
	testl	%r13d, %r13d
	je	.L349
	leal	-2(%r13), %r11d
	sall	%cl, %r11d
	addl	$3, %r11d
	jmp	.L325
.L436:
	movl	$1048575, 408(%rbx)
	movl	-52(%rbp), %eax
	movl	%eax, 400(%rbx)
	movl	$-7, %eax
	jmp	.L242
.L327:
	movl	%r12d, 400(%rbx)
	movl	408(%rbx), %edx
	cmpl	%r12d, -52(%rbp)
	jbe	.L414
	testl	%edx, %edx
	jne	.L314
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L429:
	movq	8(%rbx), %r8
	jmp	.L263
.L253:
	movq	8(%r8), %rax
	jmp	.L256
.L424:
	movq	8(%rbx), %r8
	leal	1(%rcx), %eax
	jmp	.L281
.L276:
	addq	$2, %rdi
	jmp	.L279
.L350:
	movl	$-7, %eax
	jmp	.L242
.L349:
	movl	$3, %r11d
	jmp	.L325
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	ReadHuffmanCode.cold, @function
ReadHuffmanCode.cold:
.LFSB89:
.L337:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$-31, %eax
	jmp	.L242
	.cfi_endproc
.LFE89:
	.text
	.size	ReadHuffmanCode, .-ReadHuffmanCode
	.section	.text.unlikely
	.size	ReadHuffmanCode.cold, .-ReadHuffmanCode.cold
.LCOLDE8:
	.text
.LHOTE8:
	.p2align 4
	.type	ProcessCommands, @function
ProcessCommands:
.LFB124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$27, 32(%rdi)
	movl	76(%rdi), %r12d
	movl	4(%rdi), %r13d
	jbe	.L510
	leaq	8(%rdi), %rdi
	call	BrotliWarmupBitReader
	movl	(%rbx), %eax
	cmpl	$7, %eax
	je	.L547
	cmpl	$8, %eax
	je	.L442
	cmpl	$9, %eax
	je	.L443
	cmpl	$10, %eax
	jne	.L554
	movl	88(%rbx), %eax
	movslq	%r12d, %rdx
.L504:
	subl	%r12d, %eax
	subl	$1, %eax
	leaq	1(%rdx,%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L508:
	subl	$1, %r13d
	js	.L555
	movq	128(%rbx), %rcx
	movl	%r12d, %eax
	subl	376(%rbx), %eax
	addl	$1, %r12d
	andl	92(%rbx), %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	movb	%al, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	jne	.L508
	movl	$16, (%rbx)
	movl	$1, %eax
.L438:
	movl	%r12d, 76(%rbx)
	movl	%r13d, 4(%rbx)
.L437:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L557:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	DecodeCommandBlockSwitch
	.p2align 4,,10
	.p2align 3
.L547:
	movq	32(%rbx), %rdx
	cmpq	$27, %rdx
	jbe	.L556
	movl	284(%rbx), %esi
	testl	%esi, %esi
	je	.L557
	movl	16(%rbx), %ecx
	movq	144(%rbx), %r9
	movq	8(%rbx), %rax
	cmpl	$47, %ecx
	jbe	.L447
	movq	24(%rbx), %rdi
	shrq	$48, %rax
	subq	$6, %rdx
	xorl	$48, %ecx
	movq	%rax, 8(%rbx)
	movq	(%rdi), %r8
	addq	$6, %rdi
	movq	%rdx, 32(%rbx)
	movq	%rdi, 24(%rbx)
	salq	$16, %r8
	orq	%r8, %rax
	movq	%rax, 8(%rbx)
.L447:
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movzbl	%dl, %edi
	leaq	(%r9,%rdi,4), %r9
	leaq	kBitMask(%rip), %rdi
	movzbl	(%r9), %r10d
	movzwl	2(%r9), %r8d
	cmpb	$8, %r10b
	jbe	.L448
	subl	$8, %r10d
	shrl	$8, %edx
	addl	$8, %ecx
	andl	(%rdi,%r10,4), %edx
	addl	%r8d, %edx
	leaq	(%r9,%rdx,4), %rdx
	movzbl	(%rdx), %r10d
	movzwl	2(%rdx), %r8d
.L448:
	movl	%r8d, %edx
	leaq	kCmdLut(%rip), %r8
	addl	%r10d, %ecx
	leaq	(%r8,%rdx,8), %rdx
	movl	%ecx, 16(%rbx)
	movzbl	3(%rdx), %r10d
	movzbl	(%rdx), %r14d
	movzbl	1(%rdx), %r11d
	movzwl	4(%rdx), %r13d
	movzwl	6(%rdx), %r9d
	movsbl	2(%rdx), %r8d
	movzbl	%r10b, %edx
	movl	%edx, 268(%rbx)
	movq	168(%rbx), %rdx
	movl	%r8d, 376(%rbx)
	movzbl	(%rdx,%r10), %edx
	movb	%dl, 360(%rbx)
	testb	%r14b, %r14b
	jne	.L558
.L449:
	movzbl	%r11b, %edx
	cmpl	$31, %ecx
	jbe	.L451
	movq	24(%rbx), %r10
	xorl	$32, %ecx
	shrq	$32, %rax
	movl	%ecx, 16(%rbx)
	movl	(%r10), %r11d
	addq	$4, %r10
	subq	$4, 32(%rbx)
	movq	%r10, 24(%rbx)
	salq	$32, %r11
	orq	%r11, %rax
	movq	%rax, 8(%rbx)
.L451:
	movl	%edx, %r10d
	shrq	%cl, %rax
	addl	%edx, %ecx
	movzwl	%r9w, %edx
	andl	(%rdi,%r10,4), %eax
	subl	$1, %esi
	movl	%ecx, 16(%rbx)
	addl	%edx, %eax
	movl	%esi, 284(%rbx)
	movl	%eax, 372(%rbx)
	testl	%r13d, %r13d
	je	.L452
	subl	%r13d, 272(%rbx)
.L442:
	movl	264(%rbx), %edi
	leaq	kBitMask(%rip), %r14
.L453:
	movq	32(%rbx), %rax
	testl	%edi, %edi
	je	.L454
	movl	16(%rbx), %ecx
	movq	352(%rbx), %rdi
	movq	8(%rbx), %rdx
	cmpl	$55, %ecx
	ja	.L559
.L455:
	shrq	%cl, %rdx
	movslq	%r12d, %r15
	movzbl	%dl, %edx
	leaq	(%rdi,%rdx,4), %rdx
	movzbl	(%rdx), %r8d
	movzwl	2(%rdx), %edx
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L561:
	movq	352(%rbx), %rsi
	movq	8(%rbx), %rax
	movl	16(%rbx), %ecx
.L458:
	cmpl	$8, %r8d
	ja	.L560
	addl	%r8d, %ecx
	movl	%ecx, 16(%rbx)
.L464:
	cmpl	$55, %ecx
	jbe	.L465
	movq	24(%rbx), %rdi
	shrq	$56, %rax
	xorl	$56, %ecx
	movq	%rax, 8(%rbx)
	movq	(%rdi), %r8
	addq	$7, %rdi
	subq	$7, 32(%rbx)
	movl	%ecx, 16(%rbx)
	salq	$8, %r8
	movq	%rdi, 24(%rbx)
	orq	%r8, %rax
	movq	%rax, 8(%rbx)
.L465:
	shrq	%cl, %rax
	addl	$1, %r12d
	subl	$1, %r13d
	movzbl	%al, %eax
	leaq	(%rsi,%rax,4), %rax
	movzbl	(%rax), %r8d
	movzwl	2(%rax), %ecx
	movq	128(%rbx), %rax
	movb	%dl, (%rax,%r15)
	subl	$1, 280(%rbx)
	cmpl	88(%rbx), %r12d
	je	.L552
	addq	$1, %r15
	testl	%r13d, %r13d
	je	.L467
	movq	32(%rbx), %rax
	movl	%ecx, %edx
.L468:
	cmpq	$27, %rax
	jbe	.L551
	movl	280(%rbx), %edi
	testl	%edi, %edi
	jne	.L561
	movq	%rbx, %rdi
	call	DecodeLiteralBlockSwitch
	movl	16(%rbx), %ecx
	movq	352(%rbx), %rsi
	cmpl	$55, %ecx
	ja	.L459
	movq	8(%rbx), %rax
.L460:
	movq	%rax, %rdx
	movl	264(%rbx), %edi
	shrq	%cl, %rdx
	movzbl	%dl, %edx
	leaq	(%rsi,%rdx,4), %rdx
	movzbl	(%rdx), %r8d
	movzwl	2(%rdx), %edx
	testl	%edi, %edi
	jne	.L458
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L510:
	movl	$2, %eax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L443:
	movl	376(%rbx), %r8d
.L452:
	testl	%r8d, %r8d
	js	.L478
	sete	%al
	movzbl	%al, %eax
	movl	%eax, 268(%rbx)
	movl	96(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 96(%rbx)
	andl	$3, %eax
	movl	100(%rbx,%rax,4), %eax
	movl	%eax, 376(%rbx)
.L479:
	movl	80(%rbx), %edx
	cmpl	%edx, 84(%rbx)
	je	.L492
	cmpl	%r12d, %edx
	cmovg	%r12d, %edx
	movl	%edx, 84(%rbx)
.L492:
	movl	372(%rbx), %r13d
	cmpl	%eax, %edx
	jge	.L493
	cmpl	$2147483644, %eax
	jg	.L511
	leal	-4(%r13), %ecx
	cmpl	$20, %ecx
	ja	.L512
	subl	%edx, %eax
	movslq	%r13d, %rsi
	leaq	kBitMask(%rip), %rdi
	movq	5112(%rbx), %r9
	leal	-1(%rax), %r8d
	movq	5104(%rbx), %rax
	movzbl	(%rax,%rsi), %edx
	movl	32(%rax,%rsi,4), %r10d
	movq	168(%rax), %rax
	movl	(%rdi,%rdx,4), %r11d
	movq	%rdx, %rcx
	andl	%r8d, %r11d
	sarl	%cl, %r8d
	movl	268(%rbx), %ecx
	addl	%ecx, 96(%rbx)
	movl	%r11d, %edx
	imull	%r13d, %edx
	addl	%r10d, %edx
	testq	%rax, %rax
	je	.L513
	cmpl	24(%r9), %r8d
	jge	.L514
	movslq	%edx, %rdx
	movslq	%r12d, %rdi
	addq	128(%rbx), %rdi
	leaq	(%rax,%rdx), %r10
	movswl	40(%r9), %eax
	cmpl	%eax, %r8d
	je	.L562
	movq	%r9, %rcx
	movl	%r13d, %edx
	movq	%r10, %rsi
	call	BrotliTransformDictionaryWord@PLT
.L501:
	movl	272(%rbx), %esi
	addl	%eax, %r12d
	subl	%eax, %esi
	movl	%esi, 272(%rbx)
	movl	%esi, %eax
	cmpl	88(%rbx), %r12d
	jge	.L563
.L502:
	testl	%eax, %eax
	jg	.L547
.L550:
	movl	$14, (%rbx)
	movl	$1, %eax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L552:
	movl	$13, (%rbx)
	movl	$1, %eax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L555:
	movl	272(%rbx), %eax
	testl	%eax, %eax
	jg	.L547
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L454:
	movl	92(%rbx), %edi
	leal	-1(%r12), %edx
	leal	-2(%r12), %ecx
	movslq	%r12d, %r15
	movq	128(%rbx), %rsi
	andl	%edi, %edx
	andl	%edi, %ecx
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rdx), %edx
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L473:
	movq	152(%rbx), %rdi
	movzbl	%dl, %esi
	movzbl	256(%rdi,%rcx), %eax
	movq	160(%rbx), %rcx
	orb	(%rdi,%rsi), %al
	movzbl	%al, %eax
	movzbl	(%rcx,%rax), %ecx
	movq	176(%rbx), %rax
	movq	(%rax,%rcx,8), %rdi
	movl	16(%rbx), %ecx
	movq	8(%rbx), %rax
	cmpl	$47, %ecx
	jbe	.L472
	movq	24(%rbx), %rsi
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rbx)
	movq	(%rsi), %r8
	addq	$6, %rsi
	subq	$6, 32(%rbx)
	movq	%rsi, 24(%rbx)
	salq	$16, %r8
	orq	%r8, %rax
	movq	%rax, 8(%rbx)
.L472:
	shrq	%cl, %rax
	movzbl	%al, %esi
	leaq	(%rdi,%rsi,4), %rdi
	movzbl	(%rdi), %esi
	movzwl	2(%rdi), %r8d
	cmpb	$8, %sil
	jbe	.L474
	subl	$8, %esi
	shrl	$8, %eax
	addl	$8, %ecx
	andl	(%r14,%rsi,4), %eax
	addl	%r8d, %eax
	leaq	(%rdi,%rax,4), %rax
	movzbl	(%rax), %esi
	movzwl	2(%rax), %r8d
.L474:
	movq	128(%rbx), %rax
	addl	%ecx, %esi
	addl	$1, %r12d
	subl	$1, %r13d
	movl	%esi, 16(%rbx)
	movb	%r8b, (%rax,%r15)
	subl	$1, 280(%rbx)
	cmpl	88(%rbx), %r12d
	je	.L552
	addq	$1, %r15
	testl	%r13d, %r13d
	je	.L467
	movq	32(%rbx), %rax
	movzbl	%dl, %ecx
	movl	%r8d, %edx
.L476:
	cmpq	$27, %rax
	jbe	.L551
	movl	280(%rbx), %esi
	testl	%esi, %esi
	jne	.L473
	movq	%rbx, %rdi
	movb	%cl, -50(%rbp)
	movb	%dl, -49(%rbp)
	call	DecodeLiteralBlockSwitch
	movl	264(%rbx), %edi
	movzbl	-49(%rbp), %edx
	movzbl	-50(%rbp), %ecx
	testl	%edi, %edi
	je	.L473
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L560:
	cmpl	$47, %ecx
	jbe	.L463
	movq	24(%rbx), %rdi
	shrq	$48, %rax
	xorl	$48, %ecx
	movq	%rax, 8(%rbx)
	movq	(%rdi), %r9
	addq	$6, %rdi
	subq	$6, 32(%rbx)
	movq	%rdi, 24(%rbx)
	salq	$16, %r9
	orq	%r9, %rax
	movq	%rax, 8(%rbx)
.L463:
	movq	%rax, %r9
	subl	$8, %r8d
	shrq	%cl, %r9
	movl	%r9d, %edi
	shrl	$8, %edi
	andl	(%r14,%r8,4), %edi
	movzbl	%r9b, %r8d
	addq	%r8, %rdx
	addq	%rdi, %rdx
	leaq	(%rsi,%rdx,4), %rdx
	movzbl	(%rdx), %edi
	movzwl	2(%rdx), %edx
	leal	8(%rcx,%rdi), %ecx
	movl	%ecx, 16(%rbx)
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L478:
	movl	288(%rbx), %edx
	testl	%edx, %edx
	je	.L564
.L480:
	movzbl	360(%rbx), %ecx
	movq	224(%rbx), %rax
	movq	8(%rbx), %r8
	movq	(%rax,%rcx,8), %rdi
	movl	16(%rbx), %ecx
	cmpl	$47, %ecx
	jbe	.L481
	movq	24(%rbx), %rsi
	movq	%r8, %rax
	xorl	$48, %ecx
	shrq	$48, %rax
	movq	%rax, 8(%rbx)
	movq	(%rsi), %r9
	addq	$6, %rsi
	subq	$6, 32(%rbx)
	salq	$16, %r9
	movq	%rsi, 24(%rbx)
	orq	%rax, %r9
	movq	%r9, 8(%rbx)
	movq	%r9, %r8
.L481:
	movq	%r8, %rsi
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %r9
	movzbl	(%r9), %r10d
	movzwl	2(%r9), %eax
	cmpb	$8, %r10b
	jbe	.L482
	subl	$8, %r10d
	leaq	kBitMask(%rip), %rdi
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rdi,%r10,4), %esi
	addl	%esi, %eax
	leaq	(%r9,%rax,4), %rax
	movzbl	(%rax), %r10d
	movzwl	2(%rax), %eax
.L482:
	addl	%ecx, %r10d
	movl	%eax, 376(%rbx)
	movl	%r10d, 16(%rbx)
	movl	$0, 268(%rbx)
	testl	$65520, %eax
	je	.L565
	movl	332(%rbx), %esi
	movl	%eax, %r11d
	subl	%esi, %r11d
	js	.L488
	movl	328(%rbx), %r13d
	movl	%r11d, %r9d
	testl	%r13d, %r13d
	jne	.L489
	shrl	%r9d
	andl	$1, %r11d
	addl	$1, %r9d
	leal	2(%r11), %eax
	movl	%r9d, %ecx
	sall	%cl, %eax
	leal	-4(%rsi,%rax), %esi
	cmpl	$31, %r10d
	jbe	.L490
	movl	%r10d, %ecx
	shrq	$32, %r8
	xorl	$32, %ecx
	movl	%ecx, 16(%rbx)
	movq	24(%rbx), %rcx
	movl	(%rcx), %eax
	addq	$4, %rcx
	subq	$4, 32(%rbx)
	movq	%rcx, 24(%rbx)
	salq	$32, %rax
	orq	%r8, %rax
	movq	%rax, 8(%rbx)
.L490:
	movl	16(%rbx), %ecx
	movl	%r9d, %r8d
	movq	8(%rbx), %rax
	leaq	kBitMask(%rip), %rdi
	addl	%ecx, %r9d
	shrq	%cl, %rax
	andl	(%rdi,%r8,4), %eax
	movl	%r9d, 16(%rbx)
	addl	%esi, %eax
.L488:
	subl	$15, %eax
.L553:
	movl	%eax, 376(%rbx)
.L548:
	subl	$1, %edx
	movl	%edx, 288(%rbx)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L493:
	movl	%r12d, %edi
	movq	128(%rbx), %rcx
	movslq	%r12d, %rdx
	leal	(%r12,%r13), %r14d
	subl	%eax, %edi
	andl	92(%rbx), %edi
	movslq	%edi, %r9
	leaq	(%rcx,%rdx), %r8
	leal	0(%r13,%rdi), %esi
	addq	%rcx, %r9
	movl	96(%rbx), %ecx
	movl	%ecx, %r10d
	addl	$1, %ecx
	andl	$3, %r10d
	movl	%eax, 100(%rbx,%r10,4)
	subl	%r13d, 272(%rbx)
	movl	%ecx, 96(%rbx)
	movdqu	(%r9), %xmm0
	movups	%xmm0, (%r8)
	movl	88(%rbx), %eax
	cmpl	%esi, %r12d
	jge	.L503
	cmpl	%r14d, %edi
	jl	.L504
.L503:
	cmpl	%esi, %r14d
	cmovge	%r14d, %esi
	cmpl	%esi, %eax
	jle	.L504
	cmpl	$16, %r13d
	jle	.L549
	cmpl	$32, %r13d
	jle	.L506
	leal	-16(%r13), %edx
	leaq	16(%r8), %rdi
	movl	%r14d, %r12d
	movslq	%edx, %rdx
	leaq	16(%r9), %rsi
	call	memcpy@PLT
	movl	272(%rbx), %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L559:
	movq	24(%rbx), %rsi
	shrq	$56, %rdx
	xorl	$56, %ecx
	subq	$7, %rax
	movq	%rdx, 8(%rbx)
	movq	(%rsi), %r8
	addq	$7, %rsi
	movl	%ecx, 16(%rbx)
	movq	%rax, 32(%rbx)
	salq	$8, %r8
	movq	%rsi, 24(%rbx)
	orq	%r8, %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L459:
	movzbl	15(%rbx), %edi
	movq	24(%rbx), %rdx
	xorl	$56, %ecx
	movl	%ecx, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	(%rdx), %rax
	addq	$7, %rdx
	subq	$7, 32(%rbx)
	salq	$8, %rax
	movq	%rdx, 24(%rbx)
	orq	%rdi, %rax
	movq	%rax, 8(%rbx)
	jmp	.L460
.L565:
	movl	96(%rbx), %esi
	testl	%eax, %eax
	jne	.L484
	subl	$1, %esi
	movl	%esi, 96(%rbx)
	andl	$3, %esi
	movl	100(%rbx,%rsi,4), %eax
	movl	$1, 268(%rbx)
	movl	%eax, 376(%rbx)
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L562:
	cmpl	$8, %esi
	jnb	.L495
	testb	$4, %sil
	jne	.L566
	testl	%esi, %esi
	je	.L496
	movzbl	(%r10), %eax
	movb	%al, (%rdi)
	testb	$2, %sil
	jne	.L567
.L496:
	movl	%r13d, %eax
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L563:
	movl	$15, (%rbx)
	movl	$1, %eax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L551:
	movl	$8, (%rbx)
	movl	$2, %eax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L467:
	movl	272(%rbx), %edx
	testl	%edx, %edx
	jg	.L443
	movl	$14, (%rbx)
	xorl	%r13d, %r13d
	movl	$1, %eax
	jmp	.L438
.L495:
	movq	(%r10), %rax
	leaq	8(%rdi), %r8
	andq	$-8, %r8
	movq	%rax, (%rdi)
	movl	%esi, %eax
	movq	-8(%r10,%rax), %rdx
	movq	%rdx, -8(%rdi,%rax)
	subq	%r8, %rdi
	movq	%r10, %rdx
	leal	(%rsi,%rdi), %eax
	subq	%rdi, %rdx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L496
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L499:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%rdx,%rsi), %rdi
	movq	%rdi, (%r8,%rsi)
	cmpl	%eax, %ecx
	jb	.L499
	jmp	.L496
.L558:
	cmpl	$31, %ecx
	jbe	.L450
	movq	24(%rbx), %rdx
	xorl	$32, %ecx
	shrq	$32, %rax
	movl	%ecx, 16(%rbx)
	movl	(%rdx), %r10d
	addq	$4, %rdx
	subq	$4, 32(%rbx)
	movq	%rdx, 24(%rbx)
	salq	$32, %r10
	orq	%r10, %rax
	movq	%rax, 8(%rbx)
.L450:
	movq	%rax, %rdx
	movl	%r14d, %r10d
	shrq	%cl, %rdx
	andl	(%rdi,%r10,4), %edx
	addl	%r14d, %ecx
	addl	%edx, %r13d
	jmp	.L449
.L489:
	movl	%r13d, %ecx
	andl	336(%rbx), %r9d
	sarl	%cl, %r11d
	movl	%r11d, %r14d
	shrl	%r14d
	addl	$1, %r14d
	cmpl	$31, %r10d
	jbe	.L491
	movq	24(%rbx), %rcx
	xorl	$32, %r10d
	movq	%r8, %rax
	movl	%r10d, 16(%rbx)
	shrq	$32, %rax
	movl	(%rcx), %edi
	addq	$4, %rcx
	subq	$4, 32(%rbx)
	movq	%rcx, 24(%rbx)
	salq	$32, %rdi
	orq	%rax, %rdi
	movq	%rdi, 8(%rbx)
	movq	%rdi, %r8
.L491:
	movl	%r10d, %ecx
	movq	%r8, %rax
	leaq	kBitMask(%rip), %rdi
	andl	$1, %r11d
	shrq	%cl, %rax
	movl	%r14d, %ecx
	andl	(%rdi,%rcx,4), %eax
	leal	(%r14,%r10), %ecx
	leal	2(%r11), %edi
	movl	%ecx, 16(%rbx)
	movl	%r14d, %ecx
	sall	%cl, %edi
	movl	%r13d, %ecx
	leal	-4(%rax,%rdi), %eax
	sall	%cl, %eax
	addl	%esi, %eax
	addl	%r9d, %eax
	jmp	.L488
.L554:
	movl	$-31, %eax
	jmp	.L437
.L506:
	movdqu	16(%r9), %xmm1
	movups	%xmm1, 16(%r8)
.L549:
	movl	272(%rbx), %eax
	movl	%r14d, %r12d
	jmp	.L502
.L484:
	leal	(%rax,%rax), %ecx
	movl	$-1431306469, %eax
	shrl	%cl, %eax
	addl	%eax, %esi
	andl	$3, %esi
	movl	100(%rbx,%rsi,4), %eax
	movl	$-94395136, %esi
	shrl	%cl, %esi
	andl	$3, %esi
	andl	$2, %ecx
	je	.L486
	addl	%esi, %eax
	movl	%eax, 376(%rbx)
	jmp	.L548
.L513:
	movl	$-19, %eax
	jmp	.L437
.L556:
	movl	$7, (%rbx)
	movl	$2, %eax
	jmp	.L438
.L564:
	movq	%rbx, %rdi
	call	DecodeDistanceBlockSwitch
	movl	288(%rbx), %edx
	jmp	.L480
.L486:
	subl	%esi, %eax
	testl	%eax, %eax
	jg	.L553
	movl	$2147483647, 376(%rbx)
	movl	$2147483647, %eax
	jmp	.L548
.L566:
	movl	(%r10), %eax
	movl	%eax, (%rdi)
	movl	%esi, %eax
	movl	-4(%r10,%rax), %edx
	movl	%edx, -4(%rdi,%rax)
	jmp	.L496
.L511:
	movl	$-16, %eax
	jmp	.L437
.L512:
	movl	$-12, %eax
	jmp	.L437
.L514:
	movl	$-11, %eax
	jmp	.L437
.L567:
	movl	%esi, %eax
	movzwl	-2(%r10,%rax), %edx
	movw	%dx, -2(%rdi,%rax)
	jmp	.L496
	.cfi_endproc
.LFE124:
	.size	ProcessCommands, .-ProcessCommands
	.p2align 4
	.type	SaveErrorCode.isra.0, @function
SaveErrorCode.isra.0:
.LFB136:
	.cfi_startproc
	leal	-1(%rsi), %eax
	movl	%esi, (%rdi)
	cmpl	$3, %eax
	movl	$0, %eax
	cmovb	%esi, %eax
	ret
	.cfi_endproc
.LFE136:
	.size	SaveErrorCode.isra.0, .-SaveErrorCode.isra.0
	.p2align 4
	.type	SafeDecodeSymbol.isra.0, @function
SafeDecodeSymbol.isra.0:
.LFB138:
	.cfi_startproc
	movq	%rcx, %r8
	movl	(%rdx), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	jne	.L572
	xorl	%r9d, %r9d
	cmpb	$0, (%rdi)
	jne	.L571
	movzwl	2(%rdi), %eax
	movl	$1, %r9d
	movl	%eax, (%r8)
.L571:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	shrq	%cl, %rsi
	movzbl	%sil, %r9d
	leaq	(%rdi,%r9,4), %r10
	xorl	%r9d, %r9d
	movzbl	(%r10), %edi
	cmpb	$8, %dil
	ja	.L574
	cmpl	%edi, %eax
	jb	.L571
	movzwl	2(%r10), %eax
	addl	%ecx, %edi
	movl	$1, %r9d
	movl	%edi, (%rdx)
	movl	%eax, (%r8)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L574:
	cmpl	$8, %eax
	jbe	.L571
	leaq	kBitMask(%rip), %rax
	andl	(%rax,%rdi,4), %esi
	movzwl	2(%r10), %eax
	shrl	$8, %esi
	addl	%esi, %eax
	leaq	(%r10,%rax,4), %rsi
	movl	$56, %eax
	movzbl	(%rsi), %edi
	subl	%ecx, %eax
	cmpl	%edi, %eax
	jb	.L571
	leal	8(%rcx,%rdi), %eax
	movl	$1, %r9d
	movl	%eax, (%rdx)
	movzwl	2(%rsi), %eax
	movl	%eax, (%r8)
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE138:
	.size	SafeDecodeSymbol.isra.0, .-SafeDecodeSymbol.isra.0
	.p2align 4
	.type	SafeDecodeLiteralBlockSwitch, @function
SafeDecodeLiteralBlockSwitch:
.LFB100:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	292(%rdi), %eax
	movl	%eax, -76(%rbp)
	cmpl	$1, %eax
	jbe	.L587
	movq	%rdi, %r11
	movl	$64, %eax
	movq	248(%rdi), %rdi
	movl	16(%r11), %r12d
	movq	8(%r11), %rsi
	movq	256(%r11), %r14
	movq	24(%r11), %rbx
	subl	%r12d, %eax
	movq	%rsi, -72(%rbp)
	movq	32(%r11), %r13
	cmpl	$14, %eax
	ja	.L608
	leaq	(%rbx,%r13), %r8
	movq	%rbx, %rax
	movl	%r12d, %ecx
	movl	$64, %r9d
	leaq	-1(%rbx,%r13), %r10
.L584:
	cmpq	%r8, %rax
	je	.L583
	shrq	$8, %rsi
	subl	$8, %ecx
	movq	%rsi, 8(%r11)
	movzbl	(%rax), %edx
	movl	%ecx, 16(%r11)
	salq	$56, %rdx
	orq	%rdx, %rsi
	movq	%r10, %rdx
	subq	%rax, %rdx
	addq	$1, %rax
	movq	%rsi, 8(%r11)
	movq	%rdx, 32(%r11)
	movl	%r9d, %edx
	subl	%ecx, %edx
	movq	%rax, 24(%r11)
	cmpl	$14, %edx
	jbe	.L584
.L582:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edi
	cmpb	$8, %al
	ja	.L585
.L586:
	addl	%eax, %ecx
	movl	%edi, -64(%rbp)
	movl	%ecx, 16(%r11)
.L588:
	movl	5068(%r11), %edx
	movl	$64, %eax
	subl	%ecx, %eax
	testl	%edx, %edx
	je	.L621
	movl	276(%r11), %edi
	movl	%edi, -60(%rbp)
.L596:
	leaq	kBlockLengthPrefixCode(%rip), %r9
	movl	%edi, %r10d
	movzbl	2(%r9,%r10,4), %r8d
	cmpl	%eax, %r8d
	jbe	.L597
	movq	32(%r11), %rdx
	movl	$64, %r14d
.L599:
	testq	%rdx, %rdx
	je	.L598
	movq	8(%r11), %r15
	movq	24(%r11), %rsi
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r15
	addq	$1, %rsi
	movq	%r15, 8(%r11)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%r11)
	movl	%r14d, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%r11)
	orq	%r15, %rax
	movq	%rdx, 32(%r11)
	movq	%rax, 8(%r11)
	cmpl	%esi, %r8d
	ja	.L599
.L600:
	leaq	kBitMask(%rip), %rdx
	movl	%r8d, %esi
	shrq	%cl, %rax
	addl	%r8d, %ecx
	andl	(%rdx,%rsi,4), %eax
	movzwl	(%r9,%r10,4), %edx
	movl	%ecx, 16(%r11)
	movl	308(%r11), %ecx
	movl	$0, 5068(%r11)
	addl	%edx, %eax
	movl	-64(%rbp), %edx
	movl	%eax, 280(%r11)
	leal	1(%rcx), %eax
	cmpl	$1, %edx
	je	.L604
	leal	-2(%rdx), %eax
	testl	%edx, %edx
	jne	.L604
	movl	304(%r11), %eax
.L604:
	movl	-76(%rbp), %ebx
	movl	%eax, %edx
	movl	%ecx, 304(%r11)
	subl	%ebx, %edx
	cmpl	%eax, %ebx
	cmovbe	%rdx, %rax
	movl	%eax, %ecx
	movl	%eax, 308(%r11)
	movl	%eax, %edx
	shrl	$5, %ecx
	sall	$6, %edx
	addq	5088(%r11), %rdx
	movl	5120(%r11,%rcx,4), %esi
	movl	%eax, %ecx
	movq	%rdx, 160(%r11)
	andl	$31, %ecx
	shrq	%cl, %rsi
	andl	$1, %esi
	movl	%esi, 264(%r11)
	movzbl	(%rdx), %ecx
	movq	176(%r11), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 352(%r11)
	movq	5096(%r11), %rdx
	movzbl	(%rdx,%rax), %eax
	leaq	kContextLookup(%rip), %rdx
	salq	$9, %rax
	andl	$1536, %eax
	addq	%rdx, %rax
	movq	%rax, 152(%r11)
	movl	$1, %eax
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L598:
	movl	%edi, 276(%r11)
.L603:
	movq	-72(%rbp), %rax
	movl	%r12d, 16(%r11)
	movl	$0, 5068(%r11)
	movq	%rax, 8(%r11)
	movq	%rbx, 24(%r11)
	movq	%r13, 32(%r11)
.L587:
	xorl	%eax, %eax
.L579:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L622
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	leaq	-64(%rbp), %rcx
	leaq	16(%r11), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L587
	movl	16(%r11), %ecx
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L621:
	movq	8(%r11), %rsi
	cmpl	$14, %eax
	ja	.L590
	movq	32(%r11), %rax
	movl	$64, %r8d
.L592:
	testq	%rax, %rax
	je	.L591
	movq	24(%r11), %rdx
	shrq	$8, %rsi
	subl	$8, %ecx
	subq	$1, %rax
	movq	%rsi, 8(%r11)
	movzbl	(%rdx), %edi
	addq	$1, %rdx
	movl	%ecx, 16(%r11)
	movq	%rdx, 24(%r11)
	movl	%r8d, %edx
	salq	$56, %rdi
	subl	%ecx, %edx
	movq	%rax, 32(%r11)
	orq	%rdi, %rsi
	movq	%rsi, 8(%r11)
	cmpl	$14, %edx
	jbe	.L592
.L590:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%r14,%rax,4), %rax
	movzbl	(%rax), %r8d
	movzwl	2(%rax), %edi
	cmpb	$8, %r8b
	jbe	.L594
	subl	$8, %r8d
	leaq	kBitMask(%rip), %rdx
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rdx,%r8,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdi), %edx
	leaq	(%rax,%rdx,4), %rax
	movzbl	(%rax), %r8d
	movzwl	2(%rax), %edi
.L594:
	addl	%r8d, %ecx
	movl	$64, %eax
	movl	%edi, -60(%rbp)
	movl	%ecx, 16(%r11)
	subl	%ecx, %eax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L585:
	leal	-8(%rax), %r8d
	leaq	kBitMask(%rip), %rax
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rax,%r8,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdi), %eax
	leaq	(%rdx,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edi
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L591:
	leaq	-60(%rbp), %rcx
	leaq	16(%r11), %rdx
	movq	%r14, %rdi
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L603
	movl	16(%r11), %ecx
	movl	$64, %eax
	movl	-60(%rbp), %edi
	subl	%ecx, %eax
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L608:
	movl	%r12d, %ecx
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L597:
	movq	8(%r11), %rax
	jmp	.L600
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE100:
	.size	SafeDecodeLiteralBlockSwitch, .-SafeDecodeLiteralBlockSwitch
	.p2align 4
	.type	SafeDecodeCommandBlockSwitch, @function
SafeDecodeCommandBlockSwitch:
.LFB103:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	296(%rdi), %eax
	movl	%eax, -76(%rbp)
	cmpl	$1, %eax
	jbe	.L631
	movq	%rdi, %r11
	movq	248(%rdi), %rax
	movl	16(%r11), %r12d
	movq	8(%r11), %rsi
	leaq	2528(%rax), %rdi
	movl	$64, %eax
	movq	24(%r11), %rbx
	movq	32(%r11), %r13
	subl	%r12d, %eax
	movq	%rsi, -72(%rbp)
	movq	256(%r11), %r14
	cmpl	$14, %eax
	ja	.L652
	leaq	(%rbx,%r13), %r8
	movq	%rbx, %rax
	movl	%r12d, %ecx
	movl	$64, %r9d
	leaq	-1(%rbx,%r13), %r10
.L628:
	cmpq	%r8, %rax
	je	.L627
	shrq	$8, %rsi
	subl	$8, %ecx
	movq	%rsi, 8(%r11)
	movzbl	(%rax), %edx
	movl	%ecx, 16(%r11)
	salq	$56, %rdx
	orq	%rdx, %rsi
	movq	%r10, %rdx
	subq	%rax, %rdx
	addq	$1, %rax
	movq	%rsi, 8(%r11)
	movq	%rdx, 32(%r11)
	movl	%r9d, %edx
	subl	%ecx, %edx
	movq	%rax, 24(%r11)
	cmpl	$14, %edx
	jbe	.L628
.L626:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edi
	cmpb	$8, %al
	ja	.L629
.L630:
	addl	%eax, %ecx
	movl	%edi, -64(%rbp)
	movl	%ecx, 16(%r11)
.L632:
	movl	5068(%r11), %edx
	movl	$64, %eax
	subl	%ecx, %eax
	testl	%edx, %edx
	je	.L665
	movl	276(%r11), %edi
	movl	%edi, -60(%rbp)
.L640:
	leaq	kBlockLengthPrefixCode(%rip), %r9
	movl	%edi, %r10d
	movzbl	2(%r9,%r10,4), %r8d
	cmpl	%eax, %r8d
	jbe	.L641
	movq	32(%r11), %rdx
	movl	$64, %r14d
.L643:
	testq	%rdx, %rdx
	je	.L642
	movq	8(%r11), %r15
	movq	24(%r11), %rsi
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r15
	addq	$1, %rsi
	movq	%r15, 8(%r11)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%r11)
	movl	%r14d, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%r11)
	orq	%r15, %rax
	movq	%rdx, 32(%r11)
	movq	%rax, 8(%r11)
	cmpl	%r8d, %esi
	jb	.L643
.L644:
	leaq	kBitMask(%rip), %rdx
	movl	%r8d, %esi
	shrq	%cl, %rax
	addl	%r8d, %ecx
	andl	(%rdx,%rsi,4), %eax
	movzwl	(%r9,%r10,4), %edx
	movl	%ecx, 16(%r11)
	movl	316(%r11), %ecx
	movl	$0, 5068(%r11)
	addl	%edx, %eax
	movl	-64(%rbp), %edx
	movl	%eax, 284(%r11)
	leal	1(%rcx), %eax
	cmpl	$1, %edx
	je	.L648
	leal	-2(%rdx), %eax
	testl	%edx, %edx
	jne	.L648
	movl	312(%r11), %eax
.L648:
	movl	-76(%rbp), %ebx
	movl	%eax, %edx
	movl	%ecx, 312(%r11)
	subl	%ebx, %edx
	cmpl	%eax, %ebx
	cmovbe	%rdx, %rax
	movq	200(%r11), %rdx
	movl	%eax, 316(%r11)
	movq	(%rdx,%rax,8), %rax
	movq	%rax, 144(%r11)
	movl	$1, %eax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L642:
	movl	%edi, 276(%r11)
.L647:
	movq	-72(%rbp), %rax
	movl	%r12d, 16(%r11)
	movl	$0, 5068(%r11)
	movq	%rax, 8(%r11)
	movq	%rbx, 24(%r11)
	movq	%r13, 32(%r11)
.L631:
	xorl	%eax, %eax
.L623:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L666
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	leaq	-64(%rbp), %rcx
	leaq	16(%r11), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L631
	movl	16(%r11), %ecx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L665:
	movq	8(%r11), %rsi
	leaq	1584(%r14), %rdi
	cmpl	$14, %eax
	ja	.L634
	movq	32(%r11), %rax
	movl	$64, %r9d
.L636:
	testq	%rax, %rax
	je	.L635
	movq	24(%r11), %rdx
	shrq	$8, %rsi
	subl	$8, %ecx
	subq	$1, %rax
	movq	%rsi, 8(%r11)
	movzbl	(%rdx), %r8d
	addq	$1, %rdx
	movl	%ecx, 16(%r11)
	movq	%rdx, 24(%r11)
	movl	%r9d, %edx
	salq	$56, %r8
	subl	%ecx, %edx
	movq	%rax, 32(%r11)
	orq	%r8, %rsi
	movq	%rsi, 8(%r11)
	cmpl	$14, %edx
	jbe	.L636
.L634:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rax
	movzbl	(%rax), %r8d
	movzwl	2(%rax), %edi
	cmpb	$8, %r8b
	jbe	.L638
	subl	$8, %r8d
	leaq	kBitMask(%rip), %rdx
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rdx,%r8,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdi), %edx
	leaq	(%rax,%rdx,4), %rax
	movzbl	(%rax), %r8d
	movzwl	2(%rax), %edi
.L638:
	addl	%r8d, %ecx
	movl	$64, %eax
	movl	%edi, -60(%rbp)
	movl	%ecx, 16(%r11)
	subl	%ecx, %eax
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L629:
	leal	-8(%rax), %r8d
	leaq	kBitMask(%rip), %rax
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rax,%r8,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdi), %eax
	leaq	(%rdx,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edi
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	-60(%rbp), %rcx
	leaq	16(%r11), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L647
	movl	16(%r11), %ecx
	movl	$64, %eax
	movl	-60(%rbp), %edi
	subl	%ecx, %eax
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L652:
	movl	%r12d, %ecx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L641:
	movq	8(%r11), %rax
	jmp	.L644
.L666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE103:
	.size	SafeDecodeCommandBlockSwitch, .-SafeDecodeCommandBlockSwitch
	.p2align 4
	.type	SafeDecodeDistanceBlockSwitch, @function
SafeDecodeDistanceBlockSwitch:
.LFB106:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	300(%rdi), %eax
	movl	%eax, -76(%rbp)
	cmpl	$1, %eax
	jbe	.L675
	movq	%rdi, %r11
	movq	248(%rdi), %rax
	movl	16(%r11), %r12d
	movq	8(%r11), %rsi
	leaq	5056(%rax), %rdi
	movl	$64, %eax
	movq	24(%r11), %rbx
	movq	32(%r11), %r13
	subl	%r12d, %eax
	movq	%rsi, -72(%rbp)
	movq	256(%r11), %r14
	cmpl	$14, %eax
	ja	.L696
	leaq	(%rbx,%r13), %r8
	movq	%rbx, %rax
	movl	%r12d, %ecx
	movl	$64, %r9d
	leaq	-1(%rbx,%r13), %r10
.L672:
	cmpq	%r8, %rax
	je	.L671
	shrq	$8, %rsi
	subl	$8, %ecx
	movq	%rsi, 8(%r11)
	movzbl	(%rax), %edx
	movl	%ecx, 16(%r11)
	salq	$56, %rdx
	orq	%rdx, %rsi
	movq	%r10, %rdx
	subq	%rax, %rdx
	addq	$1, %rax
	movq	%rsi, 8(%r11)
	movq	%rdx, 32(%r11)
	movl	%r9d, %edx
	subl	%ecx, %edx
	movq	%rax, 24(%r11)
	cmpl	$14, %edx
	jbe	.L672
.L670:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edi
	cmpb	$8, %al
	ja	.L673
.L674:
	addl	%eax, %ecx
	movl	%edi, -64(%rbp)
	movl	%ecx, 16(%r11)
.L676:
	movl	5068(%r11), %edx
	movl	$64, %eax
	subl	%ecx, %eax
	testl	%edx, %edx
	je	.L709
	movl	276(%r11), %edi
	movl	%edi, -60(%rbp)
.L684:
	leaq	kBlockLengthPrefixCode(%rip), %r9
	movl	%edi, %r10d
	movzbl	2(%r9,%r10,4), %r8d
	cmpl	%eax, %r8d
	jbe	.L685
	movq	32(%r11), %rdx
	movl	$64, %r14d
.L687:
	testq	%rdx, %rdx
	je	.L686
	movq	8(%r11), %r15
	movq	24(%r11), %rsi
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r15
	addq	$1, %rsi
	movq	%r15, 8(%r11)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%r11)
	movl	%r14d, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%r11)
	orq	%r15, %rax
	movq	%rdx, 32(%r11)
	movq	%rax, 8(%r11)
	cmpl	%r8d, %esi
	jb	.L687
.L688:
	leaq	kBitMask(%rip), %rdx
	movl	%r8d, %esi
	shrq	%cl, %rax
	addl	%r8d, %ecx
	andl	(%rdx,%rsi,4), %eax
	movzwl	(%r9,%r10,4), %edx
	movl	%ecx, 16(%r11)
	movl	324(%r11), %ecx
	movl	$0, 5068(%r11)
	addl	%edx, %eax
	movl	-64(%rbp), %edx
	movl	%eax, 288(%r11)
	leal	1(%rcx), %eax
	cmpl	$1, %edx
	je	.L692
	leal	-2(%rdx), %eax
	testl	%edx, %edx
	jne	.L692
	movl	320(%r11), %eax
.L692:
	movl	-76(%rbp), %ebx
	movl	%eax, %edx
	movl	%ecx, 320(%r11)
	subl	%ebx, %edx
	cmpl	%eax, %ebx
	cmovbe	%edx, %eax
	movslq	268(%r11), %rdx
	movl	%eax, 324(%r11)
	sall	$2, %eax
	addq	344(%r11), %rax
	movq	%rax, 168(%r11)
	movzbl	(%rax,%rdx), %eax
	movb	%al, 360(%r11)
	movl	$1, %eax
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L686:
	movl	%edi, 276(%r11)
.L691:
	movq	-72(%rbp), %rax
	movl	%r12d, 16(%r11)
	movl	$0, 5068(%r11)
	movq	%rax, 8(%r11)
	movq	%rbx, 24(%r11)
	movq	%r13, 32(%r11)
.L675:
	xorl	%eax, %eax
.L667:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L710
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	leaq	-64(%rbp), %rcx
	leaq	16(%r11), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L675
	movl	16(%r11), %ecx
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L709:
	movq	8(%r11), %rsi
	leaq	3168(%r14), %rdi
	cmpl	$14, %eax
	ja	.L678
	movq	32(%r11), %rax
	movl	$64, %r9d
.L680:
	testq	%rax, %rax
	je	.L679
	movq	24(%r11), %rdx
	shrq	$8, %rsi
	subl	$8, %ecx
	subq	$1, %rax
	movq	%rsi, 8(%r11)
	movzbl	(%rdx), %r8d
	addq	$1, %rdx
	movl	%ecx, 16(%r11)
	movq	%rdx, 24(%r11)
	movl	%r9d, %edx
	salq	$56, %r8
	subl	%ecx, %edx
	movq	%rax, 32(%r11)
	orq	%r8, %rsi
	movq	%rsi, 8(%r11)
	cmpl	$14, %edx
	jbe	.L680
.L678:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rax
	movzbl	(%rax), %r8d
	movzwl	2(%rax), %edi
	cmpb	$8, %r8b
	jbe	.L682
	subl	$8, %r8d
	leaq	kBitMask(%rip), %rdx
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rdx,%r8,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdi), %edx
	leaq	(%rax,%rdx,4), %rax
	movzbl	(%rax), %r8d
	movzwl	2(%rax), %edi
.L682:
	addl	%r8d, %ecx
	movl	$64, %eax
	movl	%edi, -60(%rbp)
	movl	%ecx, 16(%r11)
	subl	%ecx, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L673:
	leal	-8(%rax), %r8d
	leaq	kBitMask(%rip), %rax
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rax,%r8,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdi), %eax
	leaq	(%rdx,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edi
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	-60(%rbp), %rcx
	leaq	16(%r11), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L691
	movl	16(%r11), %ecx
	movl	$64, %eax
	movl	-60(%rbp), %edi
	subl	%ecx, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L696:
	movl	%r12d, %ecx
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L685:
	movq	8(%r11), %rax
	jmp	.L688
.L710:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE106:
	.size	SafeDecodeDistanceBlockSwitch, .-SafeDecodeDistanceBlockSwitch
	.p2align 4
	.type	SafeProcessCommands, @function
SafeProcessCommands:
.LFB125:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	76(%rdi), %r12d
	movl	4(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	cmpl	$7, %eax
	je	.L719
	cmpl	$8, %eax
	je	.L713
	cmpl	$9, %eax
	je	.L714
	cmpl	$10, %eax
	jne	.L877
	movl	88(%rdi), %eax
	movslq	%r12d, %rdx
.L804:
	subl	%r12d, %eax
	subl	$1, %eax
	leaq	1(%rdx,%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L807:
	subl	$1, %r13d
	js	.L878
	movq	128(%rbx), %rcx
	movl	%r12d, %eax
	subl	376(%rbx), %eax
	addl	$1, %r12d
	andl	92(%rbx), %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	movb	%al, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	jne	.L807
	movl	$16, (%rbx)
	movl	$1, %eax
.L718:
	movl	%r12d, 76(%rbx)
	movl	%r13d, 4(%rbx)
.L711:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L879
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L880:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	SafeDecodeCommandBlockSwitch
	testl	%eax, %eax
	je	.L816
	.p2align 4,,10
	.p2align 3
.L719:
	movl	284(%rbx), %esi
	movl	$7, (%rbx)
	testl	%esi, %esi
	je	.L880
	movl	16(%rbx), %r14d
	movq	8(%rbx), %rsi
	movl	$64, %eax
	movq	24(%rbx), %r11
	movq	32(%rbx), %r15
	subl	%r14d, %eax
	movq	%rsi, -72(%rbp)
	movq	144(%rbx), %rdi
	cmpl	$14, %eax
	ja	.L811
	leaq	(%r11,%r15), %r8
	movq	%r11, %rax
	movl	%r14d, %ecx
	movl	$64, %r9d
	leaq	-1(%r11,%r15), %r10
.L722:
	cmpq	%r8, %rax
	je	.L721
	shrq	$8, %rsi
	subl	$8, %ecx
	movq	%rsi, %rdx
	movq	%rsi, 8(%rbx)
	movzbl	(%rax), %esi
	movl	%ecx, 16(%rbx)
	salq	$56, %rsi
	orq	%rdx, %rsi
	movq	%r10, %rdx
	subq	%rax, %rdx
	addq	$1, %rax
	movq	%rsi, 8(%rbx)
	movq	%rdx, 32(%rbx)
	movl	%r9d, %edx
	subl	%ecx, %edx
	movq	%rax, 24(%rbx)
	cmpl	$14, %edx
	jbe	.L722
.L720:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %r9
	movzbl	(%r9), %edi
	movzwl	2(%r9), %eax
	cmpb	$8, %dil
	jbe	.L724
	subl	$8, %edi
	movl	%esi, %edx
	leaq	kBitMask(%rip), %rsi
	addl	$8, %ecx
	shrl	$8, %edx
	andl	(%rsi,%rdi,4), %edx
	andl	$127, %edx
	addl	%edx, %eax
	leaq	(%r9,%rax,4), %rax
	movzbl	(%rax), %edi
	movzwl	2(%rax), %eax
.L724:
	addl	%edi, %ecx
	movl	%eax, -60(%rbp)
	movl	%ecx, 16(%rbx)
.L726:
	leaq	kCmdLut(%rip), %rdx
	leaq	(%rdx,%rax,8), %rdx
	movzwl	6(%rdx), %edi
	movzbl	3(%rdx), %ecx
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %r8d
	movzwl	4(%rdx), %r13d
	movw	%di, -80(%rbp)
	movsbl	2(%rdx), %edi
	movzbl	%cl, %edx
	movl	%edx, 268(%rbx)
	movq	168(%rbx), %rdx
	movl	%edi, 376(%rbx)
	movzbl	(%rdx,%rcx), %edx
	movb	%dl, 360(%rbx)
	testl	%eax, %eax
	je	.L727
	movl	16(%rbx), %ecx
	movl	$64, %edx
	subl	%ecx, %edx
	cmpl	%edx, %eax
	jbe	.L728
	movq	32(%rbx), %rsi
.L730:
	testq	%rsi, %rsi
	je	.L874
	movq	8(%rbx), %r10
	movq	24(%rbx), %r9
	subl	$8, %ecx
	subq	$1, %rsi
	shrq	$8, %r10
	addq	$1, %r9
	movq	%r10, 8(%rbx)
	movzbl	-1(%r9), %edx
	movq	%r9, 24(%rbx)
	movl	$64, %r9d
	salq	$56, %rdx
	subl	%ecx, %r9d
	movl	%ecx, 16(%rbx)
	orq	%r10, %rdx
	movq	%rsi, 32(%rbx)
	movq	%rdx, 8(%rbx)
	cmpl	%r9d, %eax
	ja	.L730
.L731:
	movl	%eax, %esi
	addl	%ecx, %eax
	shrq	%cl, %rdx
	movl	%eax, 16(%rbx)
	leaq	kBitMask(%rip), %rax
	movl	(%rax,%rsi,4), %ecx
	andl	%edx, %ecx
	movl	%ecx, %eax
	.p2align 4,,10
	.p2align 3
.L727:
	testl	%r8d, %r8d
	je	.L812
	movl	16(%rbx), %ecx
	movl	$64, %edx
	subl	%ecx, %edx
	cmpl	%edx, %r8d
	jbe	.L733
	movq	32(%rbx), %rsi
.L734:
	testq	%rsi, %rsi
	je	.L874
	movq	8(%rbx), %r10
	movq	24(%rbx), %r9
	subl	$8, %ecx
	subq	$1, %rsi
	shrq	$8, %r10
	addq	$1, %r9
	movq	%r10, 8(%rbx)
	movzbl	-1(%r9), %edx
	movq	%r9, 24(%rbx)
	movl	$64, %r9d
	salq	$56, %rdx
	subl	%ecx, %r9d
	movl	%ecx, 16(%rbx)
	orq	%r10, %rdx
	movq	%rsi, 32(%rbx)
	movq	%rdx, 8(%rbx)
	cmpl	%r9d, %r8d
	ja	.L734
.L735:
	shrq	%cl, %rdx
	movl	%r8d, %esi
	addl	%ecx, %r8d
	leaq	kBitMask(%rip), %rcx
	movl	%r8d, 16(%rbx)
	andl	(%rcx,%rsi,4), %edx
	movl	%edx, %ecx
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L812:
	xorl	%ecx, %ecx
.L732:
	movzwl	-80(%rbp), %edx
	subl	$1, 284(%rbx)
	addl	%ecx, %edx
	movl	%edx, 372(%rbx)
	addl	%eax, %r13d
	je	.L736
	subl	%r13d, 272(%rbx)
.L713:
	movl	264(%rbx), %edx
	movl	$64, %r14d
.L738:
	movl	$8, (%rbx)
	movl	280(%rbx), %eax
	testl	%edx, %edx
	je	.L739
	leaq	-60(%rbp), %rdi
	movslq	%r12d, %r15
	movq	%rdi, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L753:
	testl	%eax, %eax
	je	.L740
.L744:
	movl	16(%rbx), %ecx
	movl	%r14d, %eax
	movq	352(%rbx), %rdi
	movq	8(%rbx), %rsi
	subl	%ecx, %eax
	cmpl	$14, %eax
	ja	.L742
	movq	32(%rbx), %rax
.L746:
	testq	%rax, %rax
	je	.L745
	movq	24(%rbx), %rdx
	shrq	$8, %rsi
	subl	$8, %ecx
	subq	$1, %rax
	movq	%rsi, 8(%rbx)
	movzbl	(%rdx), %r9d
	addq	$1, %rdx
	movl	%ecx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%r14d, %edx
	salq	$56, %r9
	subl	%ecx, %edx
	movq	%rax, 32(%rbx)
	orq	%r9, %rsi
	movq	%rsi, 8(%rbx)
	cmpl	$14, %edx
	jbe	.L746
.L742:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rdi
	movzbl	(%rdi), %eax
	movzwl	2(%rdi), %edx
	cmpb	$8, %al
	jbe	.L748
	subl	$8, %eax
	leaq	kBitMask(%rip), %r11
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%r11,%rax,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdx), %eax
	leaq	(%rdi,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edx
.L748:
	addl	%eax, %ecx
	movl	%ecx, 16(%rbx)
.L751:
	movq	128(%rbx), %rax
	addl	$1, %r12d
	subl	$1, %r13d
	movb	%dl, (%rax,%r15)
	movl	280(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 280(%rbx)
	cmpl	88(%rbx), %r12d
	je	.L875
	addq	$1, %r15
	testl	%r13d, %r13d
	jne	.L753
.L754:
	movl	272(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L768
	movl	376(%rbx), %edi
	xorl	%r13d, %r13d
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L714:
	movl	376(%rdi), %edi
.L736:
	movl	$9, (%rbx)
	testl	%edi, %edi
	js	.L769
	sete	%al
	movzbl	%al, %eax
	movl	%eax, 268(%rbx)
	movl	96(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 96(%rbx)
	andl	$3, %eax
	movl	100(%rbx,%rax,4), %eax
	movl	%eax, 376(%rbx)
.L770:
	movl	80(%rbx), %edx
	cmpl	%edx, 84(%rbx)
	je	.L792
	cmpl	%r12d, %edx
	cmovg	%r12d, %edx
	movl	%edx, 84(%rbx)
.L792:
	movl	372(%rbx), %r13d
	cmpl	%edx, %eax
	jle	.L793
	cmpl	$2147483644, %eax
	jg	.L817
	leal	-4(%r13), %ecx
	cmpl	$20, %ecx
	ja	.L818
	movq	5104(%rbx), %rsi
	subl	%edx, %eax
	leaq	kBitMask(%rip), %rdx
	movq	5112(%rbx), %r9
	leal	-1(%rax), %r8d
	movslq	%r13d, %rax
	movzbl	(%rsi,%rax), %r10d
	movl	32(%rsi,%rax,4), %edi
	movq	168(%rsi), %rsi
	movl	(%rdx,%r10,4), %r11d
	movq	%r10, %rcx
	andl	%r8d, %r11d
	sarl	%cl, %r8d
	movl	268(%rbx), %ecx
	addl	%ecx, 96(%rbx)
	movl	%r11d, %edx
	imull	%r13d, %edx
	addl	%edi, %edx
	testq	%rsi, %rsi
	je	.L819
	cmpl	24(%r9), %r8d
	jge	.L820
	movslq	%edx, %rdx
	movslq	%r12d, %rdi
	addq	128(%rbx), %rdi
	addq	%rdx, %rsi
	movswl	40(%r9), %edx
	cmpl	%edx, %r8d
	je	.L881
	movq	%r9, %rcx
	movl	%r13d, %edx
	call	BrotliTransformDictionaryWord@PLT
.L801:
	movl	272(%rbx), %edi
	addl	%eax, %r12d
	subl	%eax, %edi
	movl	%edi, 272(%rbx)
	movl	%edi, %eax
	cmpl	88(%rbx), %r12d
	jge	.L882
.L802:
	testl	%eax, %eax
	jg	.L719
.L873:
	movl	$14, (%rbx)
	movl	$1, %eax
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L739:
	movl	92(%rbx), %edi
	leal	-1(%r12), %ecx
	leal	-2(%r12), %edx
	movslq	%r12d, %r15
	movq	128(%rbx), %rsi
	andl	%edi, %ecx
	andl	%edi, %edx
	leaq	-60(%rbp), %rdi
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	movq	%rdi, -80(%rbp)
	movzbl	(%rsi,%rcx), %r9d
	movzbl	(%rsi,%rdx), %ecx
	.p2align 4,,10
	.p2align 3
.L767:
	testl	%eax, %eax
	je	.L755
.L758:
	movq	152(%rbx), %rsi
	movzbl	%r9b, %edx
	movzbl	256(%rsi,%rcx), %eax
	orb	(%rsi,%rdx), %al
	movq	160(%rbx), %rdx
	movzbl	%al, %eax
	movl	16(%rbx), %ecx
	movq	8(%rbx), %rsi
	movzbl	(%rdx,%rax), %edx
	movq	176(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	movl	%r14d, %eax
	subl	%ecx, %eax
	cmpl	$14, %eax
	ja	.L757
	movq	32(%rbx), %rax
.L760:
	testq	%rax, %rax
	je	.L759
	movq	24(%rbx), %rdx
	shrq	$8, %rsi
	subl	$8, %ecx
	subq	$1, %rax
	movq	%rsi, 8(%rbx)
	movzbl	(%rdx), %r10d
	addq	$1, %rdx
	movl	%ecx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%r14d, %edx
	salq	$56, %r10
	subl	%ecx, %edx
	movq	%rax, 32(%rbx)
	orq	%r10, %rsi
	movq	%rsi, 8(%rbx)
	cmpl	$14, %edx
	jbe	.L760
.L757:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rax
	movzbl	(%rax), %edi
	movzwl	2(%rax), %edx
	cmpb	$8, %dil
	jbe	.L762
	subl	$8, %edi
	leaq	kBitMask(%rip), %r11
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%r11,%rdi,4), %esi
	andl	$127, %esi
	addl	%esi, %edx
	leaq	(%rax,%rdx,4), %rax
	movzbl	(%rax), %edi
	movzwl	2(%rax), %edx
.L762:
	addl	%edi, %ecx
	movl	%ecx, 16(%rbx)
.L765:
	movq	128(%rbx), %rax
	addl	$1, %r12d
	subl	$1, %r13d
	movb	%dl, (%rax,%r15)
	movl	280(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 280(%rbx)
	cmpl	88(%rbx), %r12d
	je	.L875
	addq	$1, %r15
	movzbl	%r9b, %ecx
	testl	%r13d, %r13d
	je	.L754
	movl	%edx, %r9d
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L759:
	movq	-80(%rbp), %rcx
	leaq	16(%rbx), %rdx
	movb	%r9b, -72(%rbp)
	call	SafeDecodeSymbol.isra.0
	movzbl	-72(%rbp), %r9d
	testl	%eax, %eax
	je	.L816
	movl	-60(%rbp), %edx
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L745:
	movq	-72(%rbp), %rcx
	leaq	16(%rbx), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L816
	movl	-60(%rbp), %edx
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L878:
	movl	272(%rbx), %eax
	testl	%eax, %eax
	jg	.L719
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	-60(%rbp), %rcx
	leaq	16(%rbx), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L816
	movl	-60(%rbp), %eax
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L875:
	movl	$13, (%rbx)
	movl	$1, %eax
	jmp	.L718
.L789:
	movl	$-1, 376(%rbx)
	.p2align 4,,10
	.p2align 3
.L874:
	movq	-72(%rbp), %rax
	movl	%r14d, 16(%rbx)
	movq	%r11, 24(%rbx)
	movq	%rax, 8(%rbx)
	movl	$2, %eax
	movq	%r15, 32(%rbx)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L755:
	movq	%rbx, %rdi
	movb	%cl, -84(%rbp)
	movb	%r9b, -72(%rbp)
	call	SafeDecodeLiteralBlockSwitch
	testl	%eax, %eax
	je	.L816
	movl	264(%rbx), %edx
	movzbl	-72(%rbp), %r9d
	movzbl	-84(%rbp), %ecx
	testl	%edx, %edx
	je	.L758
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L740:
	movq	%rbx, %rdi
	call	SafeDecodeLiteralBlockSwitch
	testl	%eax, %eax
	je	.L816
	movl	264(%rbx), %edx
	testl	%edx, %edx
	jne	.L744
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L769:
	movl	288(%rbx), %edx
	testl	%edx, %edx
	je	.L771
.L774:
	movq	224(%rbx), %rax
	movzbl	360(%rbx), %edx
	movl	16(%rbx), %r14d
	movq	24(%rbx), %r11
	movq	(%rax,%rdx,8), %rdi
	movq	8(%rbx), %rax
	movq	32(%rbx), %r15
	movl	%r14d, %ecx
	movq	%rax, -72(%rbp)
	movl	$64, %eax
	movq	-72(%rbp), %rsi
	subl	%r14d, %eax
	cmpl	$14, %eax
	ja	.L773
	movq	-72(%rbp), %rsi
	leaq	(%r11,%r15), %r8
	movq	%r11, %rax
	leaq	-1(%r11,%r15), %r10
	movl	$64, %r9d
.L776:
	cmpq	%r8, %rax
	je	.L775
	shrq	$8, %rsi
	subl	$8, %ecx
	movq	%rsi, %rdx
	movq	%rsi, 8(%rbx)
	movzbl	(%rax), %esi
	movl	%ecx, 16(%rbx)
	salq	$56, %rsi
	orq	%rdx, %rsi
	movq	%r10, %rdx
	subq	%rax, %rdx
	addq	$1, %rax
	movq	%rsi, 8(%rbx)
	movq	%rdx, 32(%rbx)
	movl	%r9d, %edx
	subl	%ecx, %edx
	movq	%rax, 24(%rbx)
	cmpl	$14, %edx
	jbe	.L776
.L773:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %r8
	movzbl	(%r8), %edi
	movzwl	2(%r8), %edx
	cmpb	$8, %dil
	ja	.L777
.L778:
	addl	%edi, %ecx
	movl	%ecx, 16(%rbx)
.L781:
	movl	%edx, 376(%rbx)
	movl	%edx, %eax
	movl	$0, 268(%rbx)
	testl	$-16, %edx
	je	.L883
	movl	332(%rbx), %edi
	movl	%edx, %ecx
	subl	%edi, %ecx
	movl	%ecx, -80(%rbp)
	js	.L787
	movl	328(%rbx), %esi
	movl	336(%rbx), %eax
	movl	%eax, -84(%rbp)
	movl	%ecx, %eax
	movl	%esi, %ecx
	sarl	%cl, %eax
	movl	%esi, -88(%rbp)
	movl	16(%rbx), %ecx
	movl	%eax, %esi
	movl	%eax, -92(%rbp)
	movl	$64, %eax
	shrl	%esi
	subl	%ecx, %eax
	addl	$1, %esi
	cmpl	%esi, %eax
	jnb	.L788
	movq	32(%rbx), %rdx
	movl	$64, %r10d
.L790:
	testq	%rdx, %rdx
	je	.L789
	movq	8(%rbx), %r9
	movq	24(%rbx), %r8
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r9
	addq	$1, %r8
	movq	%r9, 8(%rbx)
	movzbl	-1(%r8), %eax
	movq	%r8, 24(%rbx)
	movl	%r10d, %r8d
	salq	$56, %rax
	subl	%ecx, %r8d
	movl	%ecx, 16(%rbx)
	orq	%r9, %rax
	movq	%rdx, 32(%rbx)
	movq	%rax, 8(%rbx)
	cmpl	%r8d, %esi
	ja	.L790
.L791:
	leaq	kBitMask(%rip), %rdx
	movl	%esi, %r8d
	shrq	%cl, %rax
	addl	%esi, %ecx
	andl	(%rdx,%r8,4), %eax
	movl	-92(%rbp), %edx
	movl	%ecx, 16(%rbx)
	movl	%esi, %ecx
	andl	$1, %edx
	addl	$2, %edx
	sall	%cl, %edx
	movzbl	-88(%rbp), %ecx
	leal	-4(%rax,%rdx), %eax
	movl	-80(%rbp), %edx
	andl	-84(%rbp), %edx
	sall	%cl, %eax
	addl	%edi, %eax
	addl	%edx, %eax
.L787:
	subl	$15, %eax
.L876:
	movl	%eax, 376(%rbx)
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L775:
	leaq	16(%rbx), %rdx
	leaq	-60(%rbp), %rcx
	call	SafeDecodeSymbol.isra.0
	movl	-60(%rbp), %edx
	testl	%eax, %eax
	jne	.L781
	.p2align 4,,10
	.p2align 3
.L816:
	movl	$2, %eax
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L793:
	movl	%r12d, %edi
	movq	128(%rbx), %rcx
	movslq	%r12d, %rdx
	leal	(%r12,%r13), %r14d
	subl	%eax, %edi
	andl	92(%rbx), %edi
	movslq	%edi, %r9
	leaq	(%rcx,%rdx), %r8
	leal	0(%r13,%rdi), %esi
	addq	%rcx, %r9
	movl	96(%rbx), %ecx
	movl	%ecx, %r10d
	addl	$1, %ecx
	andl	$3, %r10d
	movl	%eax, 100(%rbx,%r10,4)
	subl	%r13d, 272(%rbx)
	movl	%ecx, 96(%rbx)
	movdqu	(%r9), %xmm0
	movups	%xmm0, (%r8)
	movl	88(%rbx), %eax
	cmpl	%esi, %r12d
	jge	.L803
	cmpl	%r14d, %edi
	jl	.L804
.L803:
	cmpl	%esi, %r14d
	cmovge	%r14d, %esi
	cmpl	%eax, %esi
	jge	.L804
	cmpl	$16, %r13d
	jle	.L872
	cmpl	$32, %r13d
	jle	.L806
	leal	-16(%r13), %edx
	leaq	16(%r8), %rdi
	movl	%r14d, %r12d
	movslq	%edx, %rdx
	leaq	16(%r9), %rsi
	call	memcpy@PLT
	movl	272(%rbx), %eax
	jmp	.L802
.L777:
	subl	$8, %edi
	movl	%esi, %eax
	leaq	kBitMask(%rip), %rsi
	addl	$8, %ecx
	shrl	$8, %eax
	andl	(%rsi,%rdi,4), %eax
	andl	$127, %eax
	addl	%edx, %eax
	leaq	(%r8,%rax,4), %rax
	movzbl	(%rax), %edi
	movzwl	2(%rax), %edx
	jmp	.L778
.L883:
	movl	96(%rbx), %eax
	testl	%edx, %edx
	jne	.L783
	subl	$1, %eax
	movl	%eax, 96(%rbx)
	andl	$3, %eax
	movl	100(%rbx,%rax,4), %eax
	movl	$1, 268(%rbx)
	movl	%eax, 376(%rbx)
.L871:
	subl	$1, 288(%rbx)
	jmp	.L770
.L811:
	movl	%r14d, %ecx
	jmp	.L720
.L882:
	movl	$15, (%rbx)
	movl	$1, %eax
	jmp	.L718
.L881:
	cmpl	$8, %eax
	jnb	.L795
	testb	$4, %al
	jne	.L884
	testl	%eax, %eax
	je	.L796
	movzbl	(%rsi), %edx
	movb	%dl, (%rdi)
	testb	$2, %al
	jne	.L885
.L796:
	movl	%r13d, %eax
	jmp	.L801
.L877:
	movl	$-31, %eax
	jmp	.L711
.L795:
	movq	(%rsi), %rdx
	leaq	8(%rdi), %r8
	andq	$-8, %r8
	movq	%rdx, (%rdi)
	movl	%eax, %edx
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%rdi,%rdx)
	subq	%r8, %rdi
	addl	%edi, %eax
	subq	%rdi, %rsi
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L796
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L799:
	movl	%ecx, %edx
	addl	$8, %ecx
	movq	(%rsi,%rdx), %rdi
	movq	%rdi, (%r8,%rdx)
	cmpl	%eax, %ecx
	jb	.L799
	jmp	.L796
.L783:
	leal	(%rdx,%rdx), %ecx
	movl	$-1431306469, %esi
	shrl	%cl, %esi
	addl	%esi, %eax
	movl	$-94395136, %esi
	shrl	%cl, %esi
	andl	$3, %eax
	andl	$3, %esi
	andl	$2, %ecx
	movl	100(%rbx,%rax,4), %eax
	je	.L785
	addl	%esi, %eax
	movl	%eax, 376(%rbx)
	jmp	.L871
.L806:
	movdqu	16(%r9), %xmm1
	movups	%xmm1, 16(%r8)
.L872:
	movl	272(%rbx), %eax
	movl	%r14d, %r12d
	jmp	.L802
.L819:
	movl	$-19, %eax
	jmp	.L711
.L771:
	movq	%rbx, %rdi
	call	SafeDecodeDistanceBlockSwitch
	testl	%eax, %eax
	jne	.L774
	jmp	.L816
.L785:
	subl	%esi, %eax
	testl	%eax, %eax
	jg	.L876
	movl	$2147483647, 376(%rbx)
	movl	$2147483647, %eax
	jmp	.L871
.L728:
	movq	8(%rbx), %rdx
	jmp	.L731
.L768:
	movl	$14, (%rbx)
	xorl	%r13d, %r13d
	movl	$1, %eax
	jmp	.L718
.L733:
	movq	8(%rbx), %rdx
	jmp	.L735
.L884:
	movl	(%rsi), %edx
	movl	%eax, %eax
	movl	%edx, (%rdi)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%rdi,%rax)
	jmp	.L796
.L788:
	movq	8(%rbx), %rax
	jmp	.L791
.L820:
	movl	$-11, %eax
	jmp	.L711
.L818:
	movl	$-12, %eax
	jmp	.L711
.L817:
	movl	$-16, %eax
	jmp	.L711
.L885:
	movl	%eax, %eax
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%rdi,%rax)
	jmp	.L796
.L879:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE125:
	.size	SafeProcessCommands, .-SafeProcessCommands
	.p2align 4
	.type	DecodeVarLenUint8.isra.0, @function
DecodeVarLenUint8.isra.0:
.LFB139:
	.cfi_startproc
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L887
	cmpl	$2, %eax
	je	.L888
	testl	%eax, %eax
	je	.L889
	movl	$-31, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	movl	8(%rsi), %ecx
.L892:
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$2, %eax
	ja	.L920
	movq	24(%rsi), %r8
	movl	$64, %r11d
.L898:
	testq	%r8, %r8
	je	.L897
	movq	(%rsi), %r10
	movq	16(%rsi), %r9
	subl	$8, %ecx
	subq	$1, %r8
	shrq	$8, %r10
	addq	$1, %r9
	movq	%r10, (%rsi)
	movzbl	-1(%r9), %eax
	movq	%r9, 16(%rsi)
	movl	%r11d, %r9d
	salq	$56, %rax
	subl	%ecx, %r9d
	movl	%ecx, 8(%rsi)
	orq	%r10, %rax
	movq	%r8, 24(%rsi)
	movq	%rax, (%rsi)
	cmpl	$2, %r9d
	jbe	.L898
.L899:
	shrq	%cl, %rax
	addl	$3, %ecx
	movl	%ecx, 8(%rsi)
	andl	$7, %eax
	jne	.L921
	movl	$1, (%rdx)
	movl	$1, %eax
	movl	$0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L897:
	movl	$1, (%rdi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L889:
	movl	8(%rsi), %eax
	cmpl	$64, %eax
	jne	.L922
	movq	24(%rsi), %rcx
	movl	$2, %eax
	testq	%rcx, %rcx
	je	.L918
	movq	(%rsi), %r9
	movq	16(%rsi), %r8
	subq	$1, %rcx
	shrq	$8, %r9
	addq	$1, %r8
	movq	%r9, (%rsi)
	movzbl	-1(%r8), %eax
	movq	%rcx, 24(%rsi)
	movl	$57, %ecx
	salq	$56, %rax
	movq	%r8, 16(%rsi)
	orq	%r9, %rax
	movl	$57, 8(%rsi)
	btq	$56, %rax
	movq	%rax, (%rsi)
	jc	.L899
.L894:
	movl	$0, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	movl	(%rdx), %eax
.L891:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rsi), %ecx
	movl	$64, %r8d
	subl	%ecx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	cmpl	%eax, %r8d
	jnb	.L902
	movq	24(%rsi), %r9
	movl	$64, %ebx
.L904:
	testq	%r9, %r9
	je	.L903
	movq	(%rsi), %r11
	movq	16(%rsi), %r10
	subl	$8, %ecx
	subq	$1, %r9
	shrq	$8, %r11
	addq	$1, %r10
	movq	%r11, (%rsi)
	movzbl	-1(%r10), %r8d
	movq	%r10, 16(%rsi)
	movl	%ebx, %r10d
	salq	$56, %r8
	subl	%ecx, %r10d
	movl	%ecx, 8(%rsi)
	orq	%r11, %r8
	movq	%r9, 24(%rsi)
	movq	%r8, (%rsi)
	cmpl	%r10d, %eax
	ja	.L904
.L905:
	shrq	%cl, %r8
	addl	%eax, %ecx
	movl	%eax, %r10d
	movl	$1, %eax
	movl	%ecx, 8(%rsi)
	movl	(%rdx), %ecx
	leaq	kBitMask(%rip), %r9
	andl	(%r9,%r10,4), %r8d
	popq	%rbx
	sall	%cl, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addl	%eax, %r8d
	movl	$1, %eax
	movl	%r8d, (%rdx)
	movl	$0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$2, (%rdi)
	ret
.L921:
	.cfi_restore 3
	.cfi_restore 6
	movl	%eax, (%rdx)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rsi), %r8
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movq	(%rsi), %r8
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rsi)
	btq	%rax, %r8
	jc	.L892
	jmp	.L894
.L918:
	ret
.L920:
	movq	(%rsi), %rax
	jmp	.L899
	.cfi_endproc
.LFE139:
	.size	DecodeVarLenUint8.isra.0, .-DecodeVarLenUint8.isra.0
	.section	.text.unlikely
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4
	.type	DecodeContextMap, @function
DecodeContextMap:
.LFB94:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 5052(%rcx)
	ja	.L965
	movl	5052(%rcx), %eax
	movq	%rdx, %r13
	movl	%edi, %r12d
	movq	%rsi, %r14
	leaq	.L926(%rip), %rdx
	movq	%rcx, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L926:
	.long	.L930-.L926
	.long	.L929-.L926
	.long	.L928-.L926
	.long	.L927-.L926
	.long	.L925-.L926
	.text
	.p2align 4,,10
	.p2align 3
.L930:
	leaq	8(%rcx), %rsi
	leaq	5064(%rcx), %rdi
	movq	%r14, %rdx
	call	DecodeVarLenUint8.isra.0
	movl	%eax, %r8d
	cmpl	$1, %eax
	jne	.L923
	movl	%r12d, %r15d
	addl	$1, (%r14)
	movq	56(%rbx), %rdi
	movl	%eax, -68(%rbp)
	movq	%r15, %rsi
	movl	$0, 2184(%rbx)
	call	*40(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L966
	cmpl	$1, (%r14)
	jbe	.L980
	movl	$1, 5052(%rbx)
.L929:
	movl	16(%rbx), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$4, %eax
	ja	.L935
	movq	32(%rbx), %rdx
	movl	$64, %r8d
.L936:
	testq	%rdx, %rdx
	je	.L967
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rsi
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %rdi
	addq	$1, %rsi
	movq	%rdi, 8(%rbx)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%rbx)
	movl	%r8d, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%rbx)
	orq	%rdi, %rax
	movq	%rdx, 32(%rbx)
	movq	%rax, 8(%rbx)
	cmpl	$4, %esi
	jbe	.L936
.L937:
	shrq	%cl, %rax
	leal	1(%rcx), %edx
	movl	%eax, %edi
	andl	$1, %edi
	je	.L939
	shrl	%eax
	leal	5(%rcx), %edx
	andl	$15, %eax
	leal	1(%rax), %edi
.L939:
	movl	%edx, 16(%rbx)
	movl	%edi, 2188(%rbx)
	movl	$2, 5052(%rbx)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L967:
	movl	$2, %r8d
.L923:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L981
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	movb	$0, (%r14,%r11)
	cmpl	%r15d, -72(%rbp)
	ja	.L953
.L979:
	movl	-72(%rbp), %r12d
	movq	-80(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L925:
	movl	16(%rbx), %edx
	cmpl	$64, %edx
	jne	.L982
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L961
	movq	8(%rbx), %rsi
	movq	24(%rbx), %rcx
	subq	$1, %rdx
	shrq	$8, %rsi
	addq	$1, %rcx
	movq	%rsi, 8(%rbx)
	movzbl	-1(%rcx), %eax
	movq	%rdx, 32(%rbx)
	movl	$56, %edx
	salq	$56, %rax
	movq	%rcx, 24(%rbx)
	movl	$57, %ecx
	orq	%rsi, %rax
	movq	%rax, 8(%rbx)
.L960:
	btq	%rdx, %rax
	movl	%ecx, 16(%rbx)
	jc	.L983
.L963:
	movl	$0, 5052(%rbx)
	movl	$1, %r8d
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L927:
	movl	2192(%rcx), %edx
	movl	2188(%rcx), %eax
	movl	2184(%rcx), %r11d
	movq	0(%r13), %r8
	movl	%eax, -68(%rbp)
	movl	%edx, -60(%rbp)
	cmpl	%r11d, %edi
	ja	.L931
	cmpl	$65535, %edx
	je	.L925
.L941:
	movl	16(%rbx), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	%edx, %eax
	jnb	.L954
	movq	32(%rbx), %r9
	movl	$64, %r10d
.L956:
	testq	%r9, %r9
	je	.L955
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rsi
	subl	$8, %ecx
	subq	$1, %r9
	shrq	$8, %rdi
	addq	$1, %rsi
	movq	%rdi, 8(%rbx)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%rbx)
	movl	%r10d, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%rbx)
	orq	%rdi, %rax
	movq	%r9, 32(%rbx)
	movq	%rax, 8(%rbx)
	cmpl	%esi, %edx
	ja	.L956
.L957:
	shrq	%cl, %rax
	addl	%edx, %ecx
	movl	%edx, %edi
	movl	$1, %r14d
	movl	%ecx, 16(%rbx)
	leaq	kBitMask(%rip), %rsi
	movl	%edx, %ecx
	andl	(%rsi,%rdi,4), %eax
	sall	%cl, %r14d
	addl	%eax, %r14d
	addl	%r11d, %r14d
	cmpl	%r12d, %r14d
	ja	.L984
	.p2align 4,,10
	.p2align 3
.L958:
	movl	%r11d, %eax
	addl	$1, %r11d
	movb	$0, (%r8,%rax)
	cmpl	%r11d, %r14d
	jne	.L958
	leaq	2196(%rbx), %r15
	cmpl	%r12d, %r14d
	jnb	.L925
.L940:
	addl	$1, %r14d
	leaq	-60(%rbp), %rax
	movl	%r12d, -72(%rbp)
	movl	$64, %r9d
	movq	%r13, -80(%rbp)
	movq	%r15, %r12
	movq	%rax, %r13
	movl	%r14d, %r15d
	movq	%r8, %r14
.L942:
	movl	16(%rbx), %ecx
	movl	%r9d, %eax
	movq	8(%rbx), %rsi
	leal	-1(%r15), %r11d
	subl	%ecx, %eax
	cmpl	$14, %eax
	ja	.L943
	movq	32(%rbx), %rax
.L945:
	testq	%rax, %rax
	je	.L944
	movq	24(%rbx), %rdx
	shrq	$8, %rsi
	subl	$8, %ecx
	subq	$1, %rax
	movq	%rsi, 8(%rbx)
	movzbl	(%rdx), %edi
	addq	$1, %rdx
	movl	%ecx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movl	%r9d, %edx
	salq	$56, %rdi
	subl	%ecx, %edx
	movq	%rax, 32(%rbx)
	orq	%rdi, %rsi
	movq	%rsi, 8(%rbx)
	cmpl	$14, %edx
	jbe	.L945
.L943:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%r12,%rax,4), %r8
	movzbl	(%r8), %eax
	movzwl	2(%r8), %edx
	cmpb	$8, %al
	jbe	.L947
	subl	$8, %eax
	leaq	kBitMask(%rip), %rdi
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rdi,%rax,4), %esi
	andl	$127, %esi
	leal	(%rsi,%rdx), %eax
	leaq	(%r8,%rax,4), %rdx
	movzbl	(%rdx), %eax
	movzwl	2(%rdx), %edx
.L947:
	addl	%eax, %ecx
	movl	%edx, -60(%rbp)
	movl	%ecx, 16(%rbx)
.L950:
	testl	%edx, %edx
	je	.L985
	movl	-68(%rbp), %eax
	cmpl	%eax, %edx
	jbe	.L976
	subl	%eax, %edx
	movb	%dl, (%r14,%r11)
	cmpl	%r15d, -72(%rbp)
	jbe	.L979
.L953:
	addl	$1, %r15d
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L928:
	movl	2188(%rcx), %edi
.L933:
	addl	(%r14), %edi
	leaq	2196(%rbx), %r15
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	%edi, %esi
	call	ReadHuffmanCode
	movl	%eax, %r8d
	cmpl	$1, %eax
	jne	.L923
	movl	2188(%rbx), %eax
	movq	0(%r13), %r8
	movl	$65535, 2192(%rbx)
	movl	2184(%rbx), %r14d
	movl	$65535, -60(%rbp)
	movl	$3, 5052(%rbx)
	movl	%eax, -68(%rbp)
	cmpl	%r14d, %r12d
	jbe	.L925
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L955:
	movl	%edx, 2192(%rbx)
	movl	$2, %r8d
	movl	%r11d, 2184(%rbx)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L931:
	cmpl	$65535, %edx
	jne	.L941
	movl	%r11d, %r14d
	leaq	2196(%rcx), %r15
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L961:
	movl	$4, 5052(%rbx)
	movl	$2, %r8d
	jmp	.L923
.L983:
	movq	0(%r13), %rdi
	movq	%rbx, %rdx
	movl	%r12d, %esi
	call	InverseMoveToFrontTransform
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L944:
	leaq	16(%rbx), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	SafeDecodeSymbol.isra.0
	movl	$64, %r9d
	testl	%eax, %eax
	je	.L986
	movl	-60(%rbp), %edx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L976:
	movl	-72(%rbp), %r12d
	movq	-80(%rbp), %r13
	movq	%r14, %r8
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L954:
	movq	8(%rbx), %rax
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L935:
	movq	8(%rbx), %rax
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L980:
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movl	-68(%rbp), %r8d
	jmp	.L923
.L982:
	movq	8(%rbx), %rax
	leal	1(%rdx), %ecx
	jmp	.L960
.L984:
	movl	$-8, %r8d
	jmp	.L923
.L986:
	movl	$65535, 2192(%rbx)
	movl	$2, %r8d
	movl	%r11d, 2184(%rbx)
	jmp	.L923
.L981:
	call	__stack_chk_fail@PLT
.L966:
	movl	$-25, %r8d
	jmp	.L923
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	DecodeContextMap.cold, @function
DecodeContextMap.cold:
.LFSB94:
.L965:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$-31, %r8d
	jmp	.L923
	.cfi_endproc
.LFE94:
	.text
	.size	DecodeContextMap, .-DecodeContextMap
	.section	.text.unlikely
	.size	DecodeContextMap.cold, .-DecodeContextMap.cold
.LCOLDE9:
	.text
.LHOTE9:
	.p2align 4
	.globl	BrotliDecoderSetParameter
	.type	BrotliDecoderSetParameter, @function
BrotliDecoderSetParameter:
.LFB68:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L987
	testl	%esi, %esi
	je	.L989
	cmpl	$1, %esi
	je	.L990
	ret
	.p2align 4,,10
	.p2align 3
.L990:
	testl	%edx, %edx
	setne	%al
	sall	$5, %eax
	movl	%eax, %edx
	movzbl	5072(%rdi), %eax
	andl	$-33, %eax
	orl	%edx, %eax
	movb	%al, 5072(%rdi)
	movl	$1, %eax
.L987:
	ret
	.p2align 4,,10
	.p2align 3
.L989:
	testl	%edx, %edx
	sete	%al
	sall	$4, %eax
	movl	%eax, %edx
	movzbl	5072(%rdi), %eax
	andl	$-17, %eax
	orl	%edx, %eax
	movb	%al, 5072(%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE68:
	.size	BrotliDecoderSetParameter, .-BrotliDecoderSetParameter
	.p2align 4
	.globl	BrotliDecoderCreateInstance
	.type	BrotliDecoderCreateInstance, @function
BrotliDecoderCreateInstance:
.LFB69:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	orq	%rsi, %r15
	je	.L1014
	testq	%rdi, %rdi
	je	.L1013
	testq	%rsi, %rsi
	je	.L1013
	movl	$5152, %esi
	movq	%rdx, %rdi
	call	*%rbx
	movq	%rax, %r13
.L994:
	testq	%r13, %r13
	je	.L1013
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	BrotliDecoderStateInit
	testl	%eax, %eax
	je	.L1015
.L992:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L1016
	testq	%rbx, %rbx
	je	.L1013
	testq	%r12, %r12
	je	.L1013
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*%r12
	.p2align 4,,10
	.p2align 3
.L1013:
	xorl	%r13d, %r13d
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1014:
	movl	$5152, %edi
	call	malloc@PLT
	movq	%rax, %r13
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	free@PLT
	jmp	.L992
	.cfi_endproc
.LFE69:
	.size	BrotliDecoderCreateInstance, .-BrotliDecoderCreateInstance
	.p2align 4
	.globl	BrotliDecoderDestroyInstance
	.type	BrotliDecoderDestroyInstance, @function
BrotliDecoderDestroyInstance:
.LFB70:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1017
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	48(%rdi), %rbx
	movq	56(%rdi), %r13
	call	BrotliDecoderStateCleanup
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, %rax
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1017:
	ret
	.cfi_endproc
.LFE70:
	.size	BrotliDecoderDestroyInstance, .-BrotliDecoderDestroyInstance
	.p2align 4
	.globl	BrotliDecoderDecompress
	.type	BrotliDecoderDecompress, @function
BrotliDecoderDecompress:
.LFB127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1160, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-5216(%rbp), %r12
	movq	(%rdx), %rax
	movq	%rdi, %r14
	movq	%rcx, -5224(%rbp)
	movq	%rsi, %r13
	movq	%rdx, %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -5232(%rbp)
	movq	$0, -5240(%rbp)
	call	BrotliDecoderStateInit
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L1269
.L1022:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1270
	addq	$5256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	.cfi_restore_state
	movq	-4824(%rbp), %rdx
	movl	-5100(%rbp), %r15d
	movq	%rdx, -5240(%rbp)
	testl	%r15d, %r15d
	js	.L1271
	cmpq	$0, -5232(%rbp)
	jne	.L1272
	movq	$0, -5256(%rbp)
.L1025:
	movl	-5144(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L1154
	movq	%r14, -5184(%rbp)
	movq	%r13, %rax
	movl	$1, %esi
.L1027:
	movq	%r13, -5264(%rbp)
	movq	%rbx, -5272(%rbp)
	movq	%rax, -5192(%rbp)
	movl	%esi, %eax
.L1028:
	movq	%r12, %rbx
	cmpl	$1, %eax
	jne	.L1233
.L1030:
	movl	-5216(%rbp), %eax
	leaq	.L1045(%rip), %r12
.L1040:
	cmpl	$24, %eax
	ja	.L1030
.L1239:
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1045:
	.long	.L1064-.L1045
	.long	.L1063-.L1045
	.long	.L1062-.L1045
	.long	.L1061-.L1045
	.long	.L1060-.L1045
	.long	.L1059-.L1045
	.long	.L1058-.L1045
	.long	.L1246-.L1045
	.long	.L1246-.L1045
	.long	.L1246-.L1045
	.long	.L1246-.L1045
	.long	.L1056-.L1045
	.long	.L1055-.L1045
	.long	.L1053-.L1045
	.long	.L1054-.L1045
	.long	.L1053-.L1045
	.long	.L1053-.L1045
	.long	.L1052-.L1045
	.long	.L1051-.L1045
	.long	.L1050-.L1045
	.long	.L1049-.L1045
	.long	.L1048-.L1045
	.long	.L1047-.L1045
	.long	.L1046-.L1045
	.long	.L1245-.L1045
	.text
.L1082:
	movl	$2, -5216(%rbp)
	.p2align 4,,10
	.p2align 3
.L1062:
	movl	-140(%rbp), %ecx
.L1068:
	movl	$1, %eax
	movq	-5160(%rbp), %rdi
	movl	$12336, %esi
	sall	%cl, %eax
	subl	$16, %eax
	movl	%eax, -5136(%rbp)
	call	*-5176(%rbp)
	movq	%rax, -4968(%rbp)
	testq	%rax, %rax
	je	.L1159
	movl	$3, -5216(%rbp)
	addq	$7584, %rax
	movq	%rax, -4960(%rbp)
.L1061:
	movq	%rbx, %rdi
	call	BrotliDecoderStateMetablockBegin
	movl	$4, -5216(%rbp)
.L1060:
	leaq	-5208(%rbp), %r13
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	DecodeMetaBlockLength
	cmpl	$1, %eax
	je	.L1273
.L1265:
	movq	%rbx, %r12
.L1233:
	cmpl	$2, %eax
	jne	.L1243
.L1075:
	cmpq	$0, -5088(%rbp)
	je	.L1036
	movq	-5256(%rbp), %rdx
	leaq	-5240(%rbp), %rcx
	leaq	-5232(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	call	WriteRingBuffer
	testl	%eax, %eax
	js	.L1274
.L1036:
	movl	-5144(%rbp), %eax
	movq	-5184(%rbp), %rdx
	testl	%eax, %eax
	je	.L1275
	testq	%rdx, %rdx
	je	.L1276
	testq	%r14, %r14
	jne	.L1039
	movq	-5272(%rbp), %rbx
.L1041:
	movl	$2, %esi
.L1035:
	leaq	-5100(%rbp), %rdi
	call	SaveErrorCode.isra.0
	movl	%eax, %r13d
.L1026:
	movq	-5240(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	call	BrotliDecoderStateCleanup
	xorl	%eax, %eax
	cmpl	$1, %r13d
	sete	%al
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1272:
	leaq	-5224(%rbp), %rax
	cmpq	$0, -5224(%rbp)
	movq	%rax, -5256(%rbp)
	jne	.L1025
	leaq	-5100(%rbp), %rdi
	movl	$-20, %esi
	call	SaveErrorCode.isra.0
	movl	%eax, %r13d
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	%rbx, %r12
.L1057:
	movq	%r12, %rdi
	call	ProcessCommands
	cmpl	$2, %eax
	jne	.L1028
	movq	%r12, %rdi
	call	SafeProcessCommands
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1154:
	leaq	-5152(%rbp), %rax
	movl	$2, %esi
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	-5264(%rbp), %rax
	movq	%r14, -5184(%rbp)
	movq	%r12, %rbx
	movl	$0, -5144(%rbp)
	movq	%rax, -5192(%rbp)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	%rdx, (%rbx)
	movq	%r12, %rdi
	movl	%eax, -5256(%rbp)
	call	BrotliDecoderStateCleanup
	movl	-5256(%rbp), %eax
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	-5264(%rbp), %rbx
	movl	%eax, %edx
	addl	$1, %eax
	subq	$1, %r14
	movzbl	(%rbx), %ecx
	addq	$1, %rbx
	movl	%eax, -5144(%rbp)
	movq	%rbx, -5264(%rbp)
	movq	%r12, %rbx
	movb	%cl, -5152(%rbp,%rdx)
	movq	%rax, -5184(%rbp)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	-5256(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	leaq	-5240(%rbp), %rcx
	leaq	-5232(%rbp), %rsi
	call	WriteRingBuffer
	cmpl	$1, %eax
	jne	.L1265
	movq	%rbx, %rdi
	movl	%eax, -5276(%rbp)
	call	WrapRingBuffer
	movl	-140(%rbp), %ecx
	movl	-5276(%rbp), %eax
	sall	%cl, %eax
	cmpl	%eax, -5128(%rbp)
	jne	.L1137
	movl	-5136(%rbp), %eax
	movl	%eax, -5132(%rbp)
.L1137:
	movl	-5216(%rbp), %eax
	cmpl	$15, %eax
	je	.L1277
	cmpl	$16, %eax
	je	.L1278
	movl	-5212(%rbp), %esi
	testl	%esi, %esi
	jne	.L1141
	movl	-4944(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1092
	movl	$9, -5216(%rbp)
	movq	%rbx, %r12
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1273:
	movzbl	-144(%rbp), %eax
	testb	$6, %al
	je	.L1088
	movl	-5200(%rbp), %ecx
	movl	%ecx, %edx
	negl	%edx
	andl	$7, %edx
	je	.L1088
	movq	-5208(%rbp), %rsi
	movl	%edx, %edi
	addl	%ecx, %edx
	movl	%edx, -5200(%rbp)
	leaq	kBitMask(%rip), %rdx
	shrq	%cl, %rsi
	testl	%esi, (%rdx,%rdi,4)
	je	.L1088
	movq	%rbx, %r12
	movq	-5272(%rbp), %rbx
	movl	$-14, %esi
	.p2align 4,,10
	.p2align 3
.L1031:
	movl	-5144(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L1042
	movl	$0, -5144(%rbp)
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1051:
	movslq	-5212(%rbp), %rdx
	movl	-4924(%rbp,%rdx,4), %edi
	movq	%rdx, %rax
.L1066:
	imull	$632, %eax, %eax
	addl	$2, %edi
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movq	-4968(%rbp), %rdx
	movl	%edi, %esi
	cltq
	leaq	(%rdx,%rax,4), %rdx
	call	ReadHuffmanCode
	cmpl	$1, %eax
	jne	.L1265
	movl	$19, -5216(%rbp)
.L1050:
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movl	$26, %esi
	movl	$26, %edi
	imull	$396, -5212(%rbp), %eax
	movq	-4960(%rbp), %rdx
	cltq
	leaq	(%rdx,%rax,4), %rdx
	call	ReadHuffmanCode
	cmpl	$1, %eax
	jne	.L1265
	movl	$20, -5216(%rbp)
.L1049:
	movl	-5200(%rbp), %ecx
	movl	-148(%rbp), %r8d
	movl	$64, %eax
	movslq	-5212(%rbp), %r11
	subl	%ecx, %eax
	testl	%r8d, %r8d
	jne	.L1099
	imull	$396, %r11d, %edx
	movq	-4960(%rbp), %rsi
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,4), %rdi
	movq	-5208(%rbp), %rsi
	cmpl	$14, %eax
	ja	.L1100
	movq	-5192(%rbp), %rax
	movq	-5184(%rbp), %r8
	leaq	1(%rax), %rdx
	leaq	1(%rax,%r8), %r9
	addq	%rax, %r8
.L1102:
	cmpq	%rdx, %r9
	je	.L1101
	shrq	$8, %rsi
	subl	$8, %ecx
	movq	%rsi, -5208(%rbp)
	movzbl	-1(%rdx), %eax
	movq	%rdx, -5192(%rbp)
	salq	$56, %rax
	movl	%ecx, -5200(%rbp)
	orq	%rax, %rsi
	movq	%r8, %rax
	subq	%rdx, %rax
	movq	%rsi, -5208(%rbp)
	addq	$1, %rdx
	movq	%rax, -5184(%rbp)
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$14, %eax
	jbe	.L1102
.L1100:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rdx
	movzbl	(%rdx), %edi
	movzwl	2(%rdx), %r9d
	cmpb	$8, %dil
	jbe	.L1104
	subl	$8, %edi
	leaq	kBitMask(%rip), %rax
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rax,%rdi,4), %esi
	movl	%esi, %eax
	andl	$127, %eax
	addl	%r9d, %eax
	leaq	(%rdx,%rax,4), %rax
	movzbl	(%rax), %edi
	movzwl	2(%rax), %r9d
.L1104:
	addl	%edi, %ecx
	movl	%r9d, -5244(%rbp)
	movl	$64, %eax
	movl	%ecx, -5200(%rbp)
	subl	%ecx, %eax
.L1106:
	leaq	kBlockLengthPrefixCode(%rip), %rdi
	movl	%r9d, %r8d
	movzbl	2(%rdi,%r8,4), %esi
	cmpl	%eax, %esi
	jbe	.L1107
	movq	-5192(%rbp), %r10
	movq	-5184(%rbp), %r15
	movq	-5208(%rbp), %rax
	leaq	1(%r10), %rdx
	leaq	1(%r10,%r15), %r13
	addq	%r15, %r10
.L1109:
	cmpq	%r13, %rdx
	je	.L1108
	shrq	$8, %rax
	subl	$8, %ecx
	movq	%rax, %r15
	movq	%rax, -5208(%rbp)
	movzbl	-1(%rdx), %eax
	movq	%rdx, -5192(%rbp)
	salq	$56, %rax
	movl	%ecx, -5200(%rbp)
	orq	%r15, %rax
	movq	%r10, %r15
	subq	%rdx, %r15
	movq	%rax, -5208(%rbp)
	addq	$1, %rdx
	movq	%r15, -5184(%rbp)
	movl	$64, %r15d
	subl	%ecx, %r15d
	cmpl	%r15d, %esi
	ja	.L1109
.L1110:
	leaq	kBitMask(%rip), %rdx
	movl	%esi, %r9d
	shrq	%cl, %rax
	addl	%esi, %ecx
	andl	(%rdx,%r9,4), %eax
	movzwl	(%rdi,%r8,4), %edx
	movl	%ecx, -5200(%rbp)
	addl	$1, -5212(%rbp)
	movl	$0, -148(%rbp)
	addl	%edx, %eax
	movl	%eax, -4936(%rbp,%r11,4)
	movl	$17, %eax
	movl	$17, -5216(%rbp)
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1048:
	movl	-4924(%rbp), %edi
.L1065:
	sall	$6, %edi
	leaq	-128(%rbp), %rdx
	leaq	-132(%rbp), %rsi
	movq	%rbx, %rcx
	call	DecodeContextMap
	cmpl	$1, %eax
	jne	.L1265
	movl	-4924(%rbp), %r9d
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r9, %r9
	je	.L1123
	movq	-128(%rbp), %rax
	xorl	%ecx, %ecx
	movl	$1, %r10d
	leaq	64(%rax), %r8
	.p2align 4,,10
	.p2align 3
.L1122:
	movzbl	-64(%r8), %esi
	leaq	-64(%r8), %rdx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L1120:
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %r11d
	addq	$4, %rdx
	xorl	%esi, %r11d
	xorl	%esi, %eax
	orl	%r11d, %eax
	movzbl	-2(%rdx), %r11d
	xorl	%esi, %r11d
	orl	%r11d, %eax
	movzbl	-1(%rdx), %r11d
	xorl	%esi, %r11d
	orl	%r11d, %eax
	movzbl	%al, %eax
	orq	%rax, %rdi
	cmpq	%rdx, %r8
	jne	.L1120
	testq	%rdi, %rdi
	jne	.L1121
	movq	%rcx, %rax
	movl	%r10d, %edx
	shrq	$5, %rax
	sall	%cl, %edx
	orl	%edx, -96(%rbp,%rax,4)
.L1121:
	addq	$1, %rcx
	addq	$64, %r8
	cmpq	%r9, %rcx
	jne	.L1122
.L1123:
	movl	$22, -5216(%rbp)
.L1047:
	movl	-4888(%rbp), %eax
	movl	-4884(%rbp), %edx
	leal	1(%rax), %ecx
	testb	$32, -144(%rbp)
	je	.L1279
	movl	$62, %r15d
	leal	-16(%rdx), %esi
	leaq	diff.3983(%rip), %rdi
	sall	%cl, %r15d
	movl	%eax, %ecx
	movl	(%rdi,%rax,4), %r13d
	addl	%edx, %r15d
	movl	$1, %edx
	sall	%cl, %edx
	leaq	bound.3982(%rip), %rcx
	movl	(%rcx,%rax,4), %ecx
	cmpl	%ecx, %esi
	jb	.L1280
	addl	%ecx, %edx
	leal	(%rsi,%r13), %eax
	addl	%edx, %r13d
	cmpl	%edx, %esi
	cmova	%eax, %r13d
.L1126:
	movl	-4916(%rbp), %eax
	leaq	-4872(%rbp), %rdx
	leaq	-4876(%rbp), %rsi
	movq	%rbx, %rcx
	leal	0(,%rax,4), %edi
	call	DecodeContextMap
	cmpl	$1, %eax
	jne	.L1265
	leaq	-5040(%rbp), %r9
	movl	-132(%rbp), %r8d
	movl	$256, %ecx
	movq	%rbx, %rdi
	movq	%r9, %rsi
	movl	$256, %edx
	movq	%r9, -5288(%rbp)
	call	BrotliDecoderHuffmanTreeGroupInit
	movl	-4920(%rbp), %r8d
	movl	$704, %ecx
	movq	%rbx, %rdi
	leaq	-5016(%rbp), %rsi
	movl	$704, %edx
	movl	%eax, -5276(%rbp)
	call	BrotliDecoderHuffmanTreeGroupInit
	movl	%r13d, %ecx
	movl	%r15d, %edx
	movq	%rbx, %rdi
	movl	-4876(%rbp), %r8d
	leaq	-4992(%rbp), %rsi
	movl	%eax, -5280(%rbp)
	call	BrotliDecoderHuffmanTreeGroupInit
	andl	-5276(%rbp), %eax
	andl	-5280(%rbp), %eax
	testb	$1, %al
	movq	-5288(%rbp), %r9
	je	.L1281
	movq	$23, -5216(%rbp)
	xorl	%eax, %eax
	movq	%r9, %r15
.L1129:
	cmpl	$1, -168(%rbp)
	je	.L1130
	movq	8(%r15), %rdx
	xorl	%esi, %esi
	movl	$0, -3044(%rbp)
	movl	$1, -168(%rbp)
	movq	%rdx, -3040(%rbp)
.L1131:
	movzwl	20(%r15), %ecx
	movq	-3040(%rbp), %rdx
	leaq	-5244(%rbp), %r13
	cmpl	%esi, %ecx
	jg	.L1133
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	(%r15), %rcx
	movslq	-3044(%rbp), %rsi
	movq	-3040(%rbp), %rdx
	movq	%rsi, %rax
	movq	%rdx, (%rcx,%rsi,8)
	movl	-5244(%rbp), %ecx
	addl	$1, %eax
	movl	%eax, -3044(%rbp)
	leaq	(%rdx,%rcx,4), %rdx
	movzwl	20(%r15), %ecx
	movq	%rdx, -3040(%rbp)
	cmpl	%ecx, %eax
	jge	.L1282
.L1133:
	movzwl	18(%r15), %esi
	movzwl	16(%r15), %edi
	movq	%rbx, %r8
	movq	%r13, %rcx
	call	ReadHuffmanCode
	cmpl	$1, %eax
	je	.L1283
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1046:
	movl	-5212(%rbp), %eax
	cmpl	$1, %eax
	je	.L1165
	cmpl	$2, %eax
	je	.L1166
	testl	%eax, %eax
	je	.L1167
	leaq	-5100(%rbp), %rdi
	movl	$-31, %esi
	movq	%rbx, %r12
	movq	-5272(%rbp), %rbx
	call	SaveErrorCode.isra.0
	movl	%eax, %r13d
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1058:
	movl	-5212(%rbp), %r8d
.L1067:
	movl	-4924(%rbp), %edi
	xorl	%r9d, %r9d
	movl	%r8d, %r15d
	movslq	%r8d, %r10
	cmpl	%r8d, %edi
	jle	.L1114
.L1113:
	movl	-5200(%rbp), %ecx
	movl	$64, %eax
	movq	-5208(%rbp), %rdx
	subl	%ecx, %eax
	cmpl	$1, %eax
	ja	.L1118
	movq	-5192(%rbp), %rdi
	movq	-5184(%rbp), %rsi
	movq	-5208(%rbp), %rdx
	leaq	1(%rdi), %rax
	leaq	1(%rdi,%rsi), %r11
	addq	%rsi, %rdi
.L1116:
	cmpq	%rax, %r11
	je	.L1115
	shrq	$8, %rdx
	subl	$8, %ecx
	movq	%rdx, -5208(%rbp)
	movzbl	-1(%rax), %esi
	movq	%rax, -5192(%rbp)
	salq	$56, %rsi
	movl	%ecx, -5200(%rbp)
	orq	%rsi, %rdx
	movq	%rdi, %rsi
	subq	%rax, %rsi
	movq	%rdx, -5208(%rbp)
	addq	$1, %rax
	movq	%rsi, -5184(%rbp)
	movl	$64, %esi
	subl	%ecx, %esi
	cmpl	$1, %esi
	jbe	.L1116
.L1118:
	leal	2(%rcx), %eax
	shrq	%cl, %rdx
	movl	%eax, -5200(%rbp)
	movq	-120(%rbp), %rax
	andl	$3, %edx
	addq	%r9, %rax
	addq	$1, %r9
	movb	%dl, (%rax,%r10)
	movl	-4924(%rbp), %edi
	leal	(%r8,%r9), %r15d
	cmpl	%r15d, %edi
	jg	.L1113
.L1114:
	movl	$21, -5216(%rbp)
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1059:
	movl	-5200(%rbp), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$5, %eax
	ja	.L1284
	movq	-5192(%rbp), %rsi
	movq	-5184(%rbp), %rdi
	movq	-5208(%rbp), %rax
	leaq	1(%rsi,%rdi), %rdx
	leaq	1(%rsi), %r8
	addq	%rsi, %rdi
.L1111:
	cmpq	%r8, %rdx
	je	.L1259
	shrq	$8, %rax
	subl	$8, %ecx
	movq	%rax, -5208(%rbp)
	movzbl	-1(%r8), %esi
	movq	%r8, -5192(%rbp)
	salq	$56, %rsi
	movl	%ecx, -5200(%rbp)
	orq	%rsi, %rax
	movq	%rdi, %rsi
	subq	%r8, %rsi
	movq	%rax, -5208(%rbp)
	addq	$1, %r8
	movq	%rsi, -5184(%rbp)
	movl	$64, %esi
	subl	%ecx, %esi
	cmpl	$5, %esi
	jbe	.L1111
.L1112:
	shrq	%cl, %rax
	leal	6(%rcx), %edx
	movl	-4924(%rbp), %esi
	movq	-5160(%rbp), %rdi
	movl	%eax, %ecx
	shrl	$2, %eax
	movl	%edx, -5200(%rbp)
	andl	$3, %ecx
	andl	$15, %eax
	sall	%cl, %eax
	movl	%ecx, -4888(%rbp)
	addl	$16, %eax
	movl	%eax, -4884(%rbp)
	leaq	kBitMask(%rip), %rax
	movl	(%rax,%rcx,4), %eax
	movl	%eax, -4880(%rbp)
	call	*-5176(%rbp)
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1163
	movq	$6, -5216(%rbp)
	xorl	%r8d, %r8d
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1064:
	leaq	-5208(%rbp), %r13
	movq	%r13, %rdi
	call	BrotliWarmupBitReader
	testl	%eax, %eax
	jne	.L1285
	.p2align 4,,10
	.p2align 3
.L1259:
	movq	%rbx, %r12
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	%rbx, %r12
.L1044:
	cmpq	$0, -5088(%rbp)
	je	.L1268
	movq	-5256(%rbp), %rdx
	leaq	-5240(%rbp), %rcx
	leaq	-5232(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	call	WriteRingBuffer
	cmpl	$1, %eax
	jne	.L1233
.L1268:
	leaq	-5100(%rbp), %rdi
	movl	$1, %esi
	movq	-5272(%rbp), %rbx
	call	SaveErrorCode.isra.0
	movl	%eax, %r13d
	jmp	.L1026
.L1285:
	movzbl	-144(%rbp), %eax
	movq	-5208(%rbp), %r8
	movl	%eax, %esi
	andl	$-33, %eax
	movb	%al, -144(%rbp)
	movl	%eax, %edi
	movl	-5200(%rbp), %eax
	shrb	$5, %sil
	andl	$1, %esi
	leal	1(%rax), %ecx
	btq	%rax, %r8
	movl	%ecx, -5200(%rbp)
	jnc	.L1286
	movq	%r8, %rdx
	shrq	%cl, %rdx
	leal	4(%rax), %ecx
	movl	%ecx, -5200(%rbp)
	andl	$7, %edx
	jne	.L1287
	movq	%r8, %rdx
	shrq	%cl, %rdx
	leal	7(%rax), %ecx
	andl	$7, %edx
	movl	%ecx, -5200(%rbp)
	cmpl	$1, %edx
	je	.L1288
	leal	8(%rdx), %eax
	testl	%edx, %edx
	movl	$17, %edx
	cmove	%edx, %eax
	movl	%eax, -140(%rbp)
.L1077:
	testb	$32, -144(%rbp)
	je	.L1082
	movl	$1, -5216(%rbp)
	.p2align 4,,10
	.p2align 3
.L1063:
	movl	-5200(%rbp), %r8d
	movl	$64, %eax
	subl	%r8d, %eax
	cmpl	$5, %eax
	ja	.L1289
	movq	-5192(%rbp), %rdx
	movq	-5184(%rbp), %rsi
	movq	-5208(%rbp), %rax
	leaq	1(%rdx,%rsi), %rcx
	leaq	1(%rdx), %rdi
	addq	%rdx, %rsi
.L1083:
	cmpq	%rdi, %rcx
	je	.L1259
	shrq	$8, %rax
	subl	$8, %r8d
	movq	%rax, -5208(%rbp)
	movzbl	-1(%rdi), %edx
	movq	%rdi, -5192(%rbp)
	salq	$56, %rdx
	movl	%r8d, -5200(%rbp)
	orq	%rdx, %rax
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	movq	%rax, -5208(%rbp)
	addq	$1, %rdi
	movq	%rdx, -5184(%rbp)
	movl	$64, %edx
	subl	%r8d, %edx
	cmpl	$5, %edx
	jbe	.L1083
.L1084:
	movl	%r8d, %ecx
	shrq	%cl, %rax
	movl	%eax, %ecx
	leal	6(%r8), %eax
	andl	$63, %ecx
	movl	%eax, -5200(%rbp)
	leal	-10(%rcx), %eax
	movl	%ecx, -140(%rbp)
	cmpl	$20, %eax
	ja	.L1158
	movl	$2, -5216(%rbp)
	jmp	.L1068
.L1291:
	movl	-4944(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1092
	movq	%rbx, %rdi
	call	BrotliCalculateRingBufferSize
	testb	$2, -144(%rbp)
	je	.L1090
	movl	$11, -5216(%rbp)
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	-5256(%rbp), %rsi
	leaq	-5240(%rbp), %rdx
	leaq	-5232(%rbp), %rdi
	movq	%rbx, %rcx
	call	CopyUncompressedBlockToOutput
	cmpl	$1, %eax
	jne	.L1265
.L1092:
	movl	$14, -5216(%rbp)
.L1054:
	movl	-4944(%rbp), %edx
	testl	%edx, %edx
	jns	.L1290
	movq	%rbx, %r12
	movl	$-10, %esi
	movq	-5272(%rbp), %rbx
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1052:
	movslq	-5212(%rbp), %rax
	leaq	-5208(%rbp), %r13
	cmpl	$2, %eax
	jle	.L1091
	movl	$5, -5216(%rbp)
	movl	$5, %eax
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1088:
	testb	$4, %al
	je	.L1291
	movl	$12, -5216(%rbp)
.L1055:
	movl	-4944(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L1092
	movl	-5200(%rbp), %edi
.L1070:
	movl	$64, %eax
	subl	%edi, %eax
	cmpl	$7, %eax
	ja	.L1292
	movq	-5192(%rbp), %rcx
	movq	-5184(%rbp), %rsi
	movq	-5208(%rbp), %rdx
	leaq	1(%rcx,%rsi), %r8
	leaq	1(%rcx), %rax
	addq	%rcx, %rsi
	cmpq	%rax, %r8
	je	.L1259
.L1293:
	shrq	$8, %rdx
	movq	%rsi, %r10
	movq	%rdx, -5208(%rbp)
	movzbl	-1(%rax), %ecx
	subq	%rax, %r10
	movq	%r10, -5184(%rbp)
	movl	$64, %r10d
	salq	$56, %rcx
	movq	%rax, -5192(%rbp)
	addq	$1, %rax
	orq	%rcx, %rdx
	leal	-8(%rdi), %ecx
	subl	%ecx, %r10d
	movq	%rdx, -5208(%rbp)
	movl	%ecx, -5200(%rbp)
	cmpl	$7, %r10d
	ja	.L1095
	movl	%ecx, %edi
	cmpq	%rax, %r8
	jne	.L1293
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	%r9d, -4940(%rbp)
	movq	%rbx, %r12
	movl	$1, -148(%rbp)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1115:
	movl	%r15d, -5212(%rbp)
	movq	%rbx, %r12
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1099:
	movl	-4940(%rbp), %r9d
	movl	%r9d, -5244(%rbp)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1101:
	leaq	-5244(%rbp), %rcx
	leaq	-5200(%rbp), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L1259
	movl	-5200(%rbp), %ecx
	movl	$64, %eax
	movl	-5244(%rbp), %r9d
	subl	%ecx, %eax
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1279:
	movl	$24, %r13d
	sall	%cl, %r13d
	addl	%edx, %r13d
	movl	%r13d, %r15d
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1282:
	movl	-5212(%rbp), %eax
.L1134:
	movl	$0, -168(%rbp)
	addl	$1, %eax
	movl	%eax, -5212(%rbp)
	cmpl	$2, %eax
	jg	.L1294
	movl	-5216(%rbp), %eax
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1166:
	leaq	-4992(%rbp), %r15
	jmp	.L1129
.L1290:
	movq	%rbx, %rdi
	call	BrotliDecoderStateCleanupAfterMetablock
	testb	$1, -144(%rbp)
	jne	.L1142
	movl	$3, -5216(%rbp)
	movl	$3, %eax
	jmp	.L1239
.L1275:
	movq	-5272(%rbp), %rbx
	movq	-5192(%rbp), %rax
	testq	%rdx, %rdx
	je	.L1041
	movzbl	(%rax), %ecx
	movl	$1, -5144(%rbp)
	movb	%cl, -5152(%rbp)
	cmpq	$1, %rdx
	je	.L1041
	movzbl	1(%rax), %ecx
	movl	$2, -5144(%rbp)
	movb	%cl, -5151(%rbp)
	cmpq	$2, %rdx
	je	.L1041
	movzbl	2(%rax), %ecx
	movl	$3, -5144(%rbp)
	movb	%cl, -5150(%rbp)
	cmpq	$3, %rdx
	je	.L1041
	movzbl	3(%rax), %ecx
	movl	$4, -5144(%rbp)
	movb	%cl, -5149(%rbp)
	cmpq	$4, %rdx
	je	.L1041
	movzbl	4(%rax), %ecx
	movl	$5, -5144(%rbp)
	movb	%cl, -5148(%rbp)
	cmpq	$5, %rdx
	je	.L1041
	movzbl	5(%rax), %ecx
	movl	$6, -5144(%rbp)
	movb	%cl, -5147(%rbp)
	cmpq	$6, %rdx
	je	.L1041
	movzbl	6(%rax), %ecx
	movl	$7, -5144(%rbp)
	movb	%cl, -5146(%rbp)
	cmpq	$7, %rdx
	je	.L1041
	movzbl	7(%rax), %eax
	movl	$8, -5144(%rbp)
	movb	%al, -5145(%rbp)
	jmp	.L1041
.L1090:
	movq	$17, -5216(%rbp)
	xorl	%eax, %eax
.L1091:
	leaq	292(%rbx,%rax,4), %rdx
	leaq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	DecodeVarLenUint8.isra.0
	cmpl	$1, %eax
	jne	.L1265
	movslq	-5212(%rbp), %rdx
	movq	%rdx, %rax
	addq	$72, %rdx
	movl	-5212(%rbp,%rdx,4), %ecx
	leal	1(%rcx), %edi
	movl	%edi, -5212(%rbp,%rdx,4)
	cmpl	$1, %edi
	jbe	.L1295
	movl	$18, -5216(%rbp)
	jmp	.L1066
.L1159:
	movq	%rbx, %r12
	movl	$-30, %esi
	movq	-5272(%rbp), %rbx
	jmp	.L1031
.L1130:
	movl	-3044(%rbp), %esi
	jmp	.L1131
.L1165:
	leaq	-5016(%rbp), %r15
	jmp	.L1129
.L1274:
	movq	-5272(%rbp), %rbx
	movl	%eax, %esi
	jmp	.L1035
.L1280:
	addl	%esi, %edx
	addl	%edx, %r13d
	jmp	.L1126
.L1142:
	movl	-5200(%rbp), %edx
	movq	%rbx, %r12
	movl	%edx, %eax
	negl	%eax
	andl	$7, %eax
	je	.L1143
	movq	-5208(%rbp), %rbx
	movl	%edx, %ecx
	movl	%eax, %esi
	addl	%eax, %edx
	leaq	kBitMask(%rip), %rax
	movl	%edx, -5200(%rbp)
	shrq	%cl, %rbx
	testl	%ebx, (%rax,%rsi,4)
	je	.L1143
	movq	-5272(%rbp), %rbx
	movl	$-15, %esi
	jmp	.L1031
.L1042:
	movl	-5200(%rbp), %edx
	movl	$64, %eax
	subl	%edx, %eax
	movl	%eax, %ecx
	shrl	$3, %eax
	addq	%rax, -5184(%rbp)
	andl	$-8, %ecx
	subq	%rax, -5192(%rbp)
	xorl	%eax, %eax
	cmpl	$64, %ecx
	je	.L1043
	movq	-5208(%rbp), %rax
	salq	%cl, %rax
.L1043:
	addl	%ecx, %edx
	movq	%rax, -5208(%rbp)
	movl	%edx, -5200(%rbp)
	jmp	.L1035
.L1292:
	addl	$8, %edi
.L1095:
	subl	$1, %r9d
	movl	%edi, -5200(%rbp)
	movl	%r9d, -4944(%rbp)
	jne	.L1070
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1143:
	movl	-5144(%rbp), %eax
	testl	%eax, %eax
	jne	.L1144
	movl	$64, %eax
	movq	-5184(%rbp), %r14
	movq	-5192(%rbp), %rbx
	subl	%edx, %eax
	movl	%eax, %ecx
	shrl	$3, %eax
	subq	%rax, %rbx
	addq	%rax, %r14
	andl	$-8, %ecx
	xorl	%eax, %eax
	movq	%r14, -5184(%rbp)
	movq	%rbx, -5264(%rbp)
	movq	%rbx, -5192(%rbp)
	cmpl	$64, %ecx
	je	.L1145
	movq	-5208(%rbp), %rax
	salq	%cl, %rax
.L1145:
	addl	%ecx, %edx
	movq	%rax, -5208(%rbp)
	movl	%edx, -5200(%rbp)
.L1144:
	movl	$24, -5216(%rbp)
	jmp	.L1044
.L1286:
	movl	$16, -140(%rbp)
	jmp	.L1077
.L1294:
	movl	-4908(%rbp), %eax
	movq	%rbx, %rdi
	movq	%rbx, %r12
	movl	%eax, %ecx
	movl	%eax, %edx
	shrl	$5, %ecx
	sall	$6, %edx
	addq	-128(%rbp), %rdx
	movl	-96(%rbp,%rcx,4), %esi
	movl	%eax, %ecx
	movq	%rdx, -5056(%rbp)
	andl	$31, %ecx
	shrq	%cl, %rsi
	andl	$1, %esi
	movl	%esi, -4952(%rbp)
	movzbl	(%rdx), %ecx
	movq	-5040(%rbp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, -4864(%rbp)
	movq	-120(%rbp), %rdx
	movzbl	(%rdx,%rax), %eax
	leaq	kContextLookup(%rip), %rdx
	salq	$9, %rax
	andl	$1536, %eax
	addq	%rdx, %rax
	movq	%rax, -5064(%rbp)
	movq	-4872(%rbp), %rax
	movq	%rax, -5048(%rbp)
	movq	-5016(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -5072(%rbp)
	call	BrotliEnsureRingBuffer
	testl	%eax, %eax
	jne	.L1139
	movq	-5272(%rbp), %rbx
	movl	$-27, %esi
	jmp	.L1031
.L1167:
	leaq	-5040(%rbp), %r15
	jmp	.L1129
.L1277:
	movl	-4944(%rbp), %edi
	testl	%edi, %edi
	je	.L1092
	movq	%rbx, %r12
.L1139:
	movl	$7, -5216(%rbp)
	jmp	.L1057
.L1107:
	movq	-5208(%rbp), %rax
	jmp	.L1110
.L1158:
	movq	%rbx, %r12
	movl	$-13, %esi
	movq	-5272(%rbp), %rbx
	jmp	.L1031
.L1287:
	addl	$17, %edx
	movl	%edx, -140(%rbp)
	jmp	.L1077
.L1284:
	movq	-5208(%rbp), %rax
	jmp	.L1112
.L1289:
	movq	-5208(%rbp), %rax
	jmp	.L1084
.L1288:
	testb	%sil, %sil
	je	.L1158
	addl	$8, %eax
	btq	%rcx, %r8
	movl	%eax, -5200(%rbp)
	movl	$-13, %eax
	jc	.L1249
	movl	%edi, %eax
	orl	$32, %eax
	movb	%al, -144(%rbp)
	jmp	.L1077
.L1278:
	movl	$10, -5216(%rbp)
	movq	%rbx, %r12
	jmp	.L1057
.L1141:
	movl	$8, -5216(%rbp)
	movq	%rbx, %r12
	jmp	.L1057
.L1163:
	movq	%rbx, %r12
	movl	$-21, %esi
	movq	-5272(%rbp), %rbx
	jmp	.L1031
.L1295:
	addl	$1, %eax
	movl	%eax, -5212(%rbp)
	movl	-5216(%rbp), %eax
	jmp	.L1040
.L1270:
	call	__stack_chk_fail@PLT
.L1281:
	leaq	-5100(%rbp), %rdi
	movl	$-22, %esi
	movq	%rbx, %r12
	movq	-5272(%rbp), %rbx
	call	SaveErrorCode.isra.0
	movl	%eax, %r13d
	jmp	.L1026
.L1249:
	movq	%rbx, %r12
	movl	%eax, %esi
	movq	-5272(%rbp), %rbx
	jmp	.L1031
.L1243:
	movq	-5272(%rbp), %rbx
	movl	%eax, %esi
	jmp	.L1031
	.cfi_endproc
.LFE127:
	.size	BrotliDecoderDecompress, .-BrotliDecoderDecompress
	.p2align 4
	.globl	BrotliDecoderDecompressStream
	.type	BrotliDecoderDecompressStream, @function
BrotliDecoderDecompressStream:
.LFB128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L1297
	movq	392(%rdi), %rax
	movq	%rax, (%r9)
.L1297:
	movl	116(%r14), %r13d
	xorl	%eax, %eax
	testl	%r13d, %r13d
	js	.L1296
	movq	-72(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L1552
	xorl	%r15d, %r15d
.L1299:
	movl	72(%r14), %r11d
	testl	%r11d, %r11d
	jne	.L1301
	movq	(%rbx), %rax
	movl	$1, %esi
	movq	%rax, 32(%r14)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
.L1303:
	leaq	8(%r14), %rdi
	movq	%rax, 24(%r14)
	movl	%esi, %eax
	movq	%rdi, -96(%rbp)
	movq	%r15, -88(%rbp)
	movq	%r14, %r15
	movq	%rbx, %r14
.L1302:
	cmpl	$1, %eax
	je	.L1305
.L1516:
	cmpl	$2, %eax
	jne	.L1524
.L1388:
	cmpq	$0, 128(%r15)
	je	.L1310
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	$1, %r8d
	call	WriteRingBuffer
	testl	%eax, %eax
	js	.L1553
.L1310:
	movl	72(%r15), %eax
	movq	32(%r15), %rdx
	testl	%eax, %eax
	je	.L1554
	movq	(%r14), %rcx
	testq	%rdx, %rdx
	je	.L1555
	testq	%rcx, %rcx
	jne	.L1314
	movq	%r15, %r14
.L1316:
	movl	$2, %esi
.L1309:
	leaq	116(%r14), %rdi
	call	SaveErrorCode.isra.0
.L1296:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1556
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1552:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L1300
	cmpq	$0, (%r15)
	jne	.L1299
.L1300:
	leaq	116(%r14), %rdi
	movl	$-20, %esi
	call	SaveErrorCode.isra.0
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	-80(%rbp), %rbx
	movl	%eax, %ecx
	addl	$1, %eax
	movq	(%rbx), %rdx
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	movb	%sil, 64(%r15,%rcx)
	movl	%eax, 72(%r15)
	movq	%rax, 32(%r15)
	movq	%rdx, (%rbx)
	subq	$1, (%r14)
.L1305:
	movq	%r14, %r13
	leaq	.L1320(%rip), %rbx
	movq	%r15, %r14
	movq	%r12, %r15
.L1522:
	movl	(%r14), %eax
.L1313:
	cmpl	$24, %eax
	ja	.L1313
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1320:
	.long	.L1339-.L1320
	.long	.L1338-.L1320
	.long	.L1337-.L1320
	.long	.L1336-.L1320
	.long	.L1335-.L1320
	.long	.L1334-.L1320
	.long	.L1333-.L1320
	.long	.L1526-.L1320
	.long	.L1526-.L1320
	.long	.L1526-.L1320
	.long	.L1526-.L1320
	.long	.L1331-.L1320
	.long	.L1330-.L1320
	.long	.L1328-.L1320
	.long	.L1329-.L1320
	.long	.L1328-.L1320
	.long	.L1328-.L1320
	.long	.L1327-.L1320
	.long	.L1326-.L1320
	.long	.L1325-.L1320
	.long	.L1324-.L1320
	.long	.L1323-.L1320
	.long	.L1322-.L1320
	.long	.L1321-.L1320
	.long	.L1525-.L1320
	.text
	.p2align 4,,10
	.p2align 3
.L1301:
	leaq	64(%r14), %rax
	movl	$2, %esi
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	-80(%rbp), %rax
	movl	$0, 72(%r15)
	movq	%rcx, 32(%r15)
	movq	(%rax), %rax
	movq	%rax, 24(%r15)
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
.L1332:
	movq	%r15, %rdi
	call	ProcessCommands
	cmpl	$2, %eax
	jne	.L1302
	movq	%r15, %rdi
	call	SafeProcessCommands
	jmp	.L1302
.L1357:
	movl	$2, (%r14)
	.p2align 4,,10
	.p2align 3
.L1337:
	movl	5076(%r14), %ecx
.L1343:
	movl	$1, %eax
	movq	56(%r14), %rdi
	movl	$12336, %esi
	sall	%cl, %eax
	subl	$16, %eax
	movl	%eax, 80(%r14)
	call	*40(%r14)
	movq	%rax, 248(%r14)
	testq	%rax, %rax
	je	.L1439
	addq	$7584, %rax
	movl	$3, (%r14)
	movq	%rax, 256(%r14)
.L1336:
	movq	%r14, %rdi
	call	BrotliDecoderStateMetablockBegin
	movl	$4, (%r14)
.L1335:
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	DecodeMetaBlockLength
	cmpl	$1, %eax
	je	.L1557
.L1547:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1326:
	movslq	4(%r14), %rdx
	movl	292(%r14,%rdx,4), %edi
	movq	%rdx, %rax
.L1341:
	imull	$632, %eax, %eax
	addl	$2, %edi
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movq	248(%r14), %rdx
	movl	%edi, %esi
	cltq
	leaq	(%rdx,%rax,4), %rdx
	call	ReadHuffmanCode
	cmpl	$1, %eax
	jne	.L1547
	movl	$19, (%r14)
.L1325:
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movl	$26, %esi
	movl	$26, %edi
	imull	$396, 4(%r14), %eax
	movq	256(%r14), %rdx
	cltq
	leaq	(%rdx,%rax,4), %rdx
	call	ReadHuffmanCode
	cmpl	$1, %eax
	jne	.L1547
	movl	$20, (%r14)
.L1324:
	movl	16(%r14), %ecx
	movl	5068(%r14), %r8d
	movl	$64, %eax
	movslq	4(%r14), %r11
	subl	%ecx, %eax
	testl	%r8d, %r8d
	je	.L1558
	movl	276(%r14), %r9d
	movl	%r9d, -60(%rbp)
.L1383:
	leaq	kBlockLengthPrefixCode(%rip), %r8
	movl	%r9d, %r10d
	movzbl	2(%r8,%r10,4), %edi
	cmpl	%eax, %edi
	jbe	.L1384
	movq	32(%r14), %rdx
.L1386:
	testq	%rdx, %rdx
	je	.L1385
	movq	8(%r14), %r12
	movq	24(%r14), %rsi
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %r12
	addq	$1, %rsi
	movq	%r12, 8(%r14)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%r14)
	movl	$64, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%r14)
	orq	%r12, %rax
	movq	%rdx, 32(%r14)
	movq	%rax, 8(%r14)
	cmpl	%esi, %edi
	ja	.L1386
.L1387:
	leaq	kBitMask(%rip), %rdx
	movl	%edi, %esi
	shrq	%cl, %rax
	addl	%edi, %ecx
	andl	(%rdx,%rsi,4), %eax
	movzwl	(%r8,%r10,4), %edx
	movl	%ecx, 16(%r14)
	addl	%edx, %eax
	movl	%eax, 280(%r14,%r11,4)
	movl	$0, 5068(%r14)
	addl	$1, 4(%r14)
	movl	$17, (%r14)
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1323:
	movl	292(%r14), %edi
.L1340:
	sall	$6, %edi
	leaq	5088(%r14), %rdx
	leaq	5084(%r14), %rsi
	movq	%r14, %rcx
	call	DecodeContextMap
	cmpl	$1, %eax
	jne	.L1547
	movl	292(%r14), %r9d
	pxor	%xmm0, %xmm0
	movups	%xmm0, 5120(%r14)
	movups	%xmm0, 5136(%r14)
	testq	%r9, %r9
	je	.L1402
	movq	5088(%r14), %rax
	xorl	%ecx, %ecx
	movl	$1, %r11d
	leaq	64(%rax), %r8
	.p2align 4,,10
	.p2align 3
.L1401:
	movzbl	-64(%r8), %esi
	leaq	-64(%r8), %rdx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L1399:
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %r10d
	addq	$4, %rdx
	xorl	%esi, %r10d
	xorl	%esi, %eax
	orl	%r10d, %eax
	movzbl	-2(%rdx), %r10d
	xorl	%esi, %r10d
	orl	%r10d, %eax
	movzbl	-1(%rdx), %r10d
	xorl	%esi, %r10d
	orl	%r10d, %eax
	movzbl	%al, %eax
	orq	%rax, %rdi
	cmpq	%rdx, %r8
	jne	.L1399
	testq	%rdi, %rdi
	jne	.L1400
	movq	%rcx, %rax
	movl	%r11d, %edx
	shrq	$5, %rax
	sall	%cl, %edx
	orl	%edx, 5120(%r14,%rax,4)
.L1400:
	addq	$1, %rcx
	addq	$64, %r8
	cmpq	%r9, %rcx
	jne	.L1401
.L1402:
	movl	$22, (%r14)
.L1322:
	movl	328(%r14), %eax
	movl	332(%r14), %edx
	leal	1(%rax), %ecx
	testb	$32, 5072(%r14)
	jne	.L1403
	movl	$24, %eax
	sall	%cl, %eax
	addl	%edx, %eax
	movl	%eax, -104(%rbp)
	movl	%eax, -108(%rbp)
.L1405:
	movl	300(%r14), %eax
	leaq	344(%r14), %rdx
	leaq	340(%r14), %rsi
	movq	%r14, %rcx
	leal	0(,%rax,4), %edi
	call	DecodeContextMap
	cmpl	$1, %eax
	jne	.L1547
	movl	5084(%r14), %r8d
	leaq	176(%r14), %r12
	movl	$256, %ecx
	movq	%r14, %rdi
	movl	$256, %edx
	movq	%r12, %rsi
	call	BrotliDecoderHuffmanTreeGroupInit
	movl	296(%r14), %r8d
	movl	$704, %ecx
	movq	%r14, %rdi
	leaq	200(%r14), %rsi
	movl	$704, %edx
	movl	%eax, -112(%rbp)
	call	BrotliDecoderHuffmanTreeGroupInit
	movl	-104(%rbp), %ecx
	movl	-108(%rbp), %edx
	movq	%r14, %rdi
	movl	340(%r14), %r8d
	leaq	224(%r14), %rsi
	movl	%eax, -116(%rbp)
	call	BrotliDecoderHuffmanTreeGroupInit
	andl	-112(%rbp), %eax
	andl	-116(%rbp), %eax
	testb	$1, %al
	je	.L1559
	movq	$23, (%r14)
.L1412:
	cmpl	$1, 5048(%r14)
	je	.L1560
	movq	8(%r12), %rax
	movl	$0, 2172(%r14)
	movl	$1, 5048(%r14)
	movq	%rax, 2176(%r14)
	xorl	%eax, %eax
.L1414:
	movzwl	20(%r12), %edx
	cmpl	%eax, %edx
	jle	.L1420
	leaq	-60(%rbp), %rax
	movq	%r13, -104(%rbp)
	movq	2176(%r14), %rdx
	movq	%r12, %r13
	movq	%rax, %r12
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	0(%r13), %rcx
	movslq	2172(%r14), %rsi
	movq	2176(%r14), %rdx
	movq	%rsi, %rax
	movq	%rdx, (%rcx,%rsi,8)
	movl	-60(%rbp), %ecx
	addl	$1, %eax
	movl	%eax, 2172(%r14)
	leaq	(%rdx,%rcx,4), %rdx
	movzwl	20(%r13), %ecx
	movq	%rdx, 2176(%r14)
	cmpl	%ecx, %eax
	jge	.L1561
.L1419:
	movzwl	18(%r13), %esi
	movzwl	16(%r13), %edi
	movq	%r14, %r8
	movq	%r12, %rcx
	call	ReadHuffmanCode
	cmpl	$1, %eax
	je	.L1562
	movq	%r15, %r12
	movq	%r14, %r15
	movq	-104(%rbp), %r14
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	WriteRingBuffer
	cmpl	$1, %eax
	jne	.L1547
	movq	%r14, %rdi
	movl	%eax, -104(%rbp)
	call	WrapRingBuffer
	movl	5076(%r14), %ecx
	movl	-104(%rbp), %eax
	sall	%cl, %eax
	cmpl	%eax, 88(%r14)
	jne	.L1421
	movl	80(%r14), %eax
	movl	%eax, 84(%r14)
.L1421:
	movl	(%r14), %eax
	cmpl	$15, %eax
	je	.L1563
	cmpl	$16, %eax
	je	.L1564
	movl	4(%r14), %esi
	testl	%esi, %esi
	jne	.L1425
	movl	272(%r14), %ecx
	testl	%ecx, %ecx
	je	.L1369
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	movl	$9, (%r15)
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1333:
	movl	4(%r14), %r8d
.L1342:
	movl	292(%r14), %edi
	xorl	%r11d, %r11d
	movl	%r8d, %r9d
	movslq	%r8d, %r12
	cmpl	%r8d, %edi
	jle	.L1393
.L1392:
	movl	16(%r14), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	ja	.L1565
	movq	32(%r14), %rdi
.L1395:
	testq	%rdi, %rdi
	je	.L1394
	movq	8(%r14), %rsi
	movq	24(%r14), %rdx
	subl	$8, %ecx
	subq	$1, %rdi
	shrq	$8, %rsi
	addq	$1, %rdx
	movq	%rsi, 8(%r14)
	movzbl	-1(%rdx), %eax
	movq	%rdx, 24(%r14)
	movl	$64, %edx
	salq	$56, %rax
	subl	%ecx, %edx
	movl	%ecx, 16(%r14)
	orq	%rsi, %rax
	movq	%rdi, 32(%r14)
	movq	%rax, 8(%r14)
	cmpl	$1, %edx
	jbe	.L1395
.L1397:
	leal	2(%rcx), %edx
	shrq	%cl, %rax
	movl	%edx, 16(%r14)
	movq	5096(%r14), %rdx
	andl	$3, %eax
	addq	%r11, %rdx
	addq	$1, %r11
	movb	%al, (%rdx,%r12)
	movl	292(%r14), %edi
	leal	(%r8,%r11), %r9d
	cmpl	%r9d, %edi
	jg	.L1392
.L1393:
	movl	$21, (%r14)
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1334:
	movl	16(%r14), %ecx
	movl	$64, %eax
	subl	%ecx, %eax
	cmpl	$5, %eax
	ja	.L1566
	movq	32(%r14), %rdx
.L1390:
	testq	%rdx, %rdx
	je	.L1542
	movq	8(%r14), %rdi
	movq	24(%r14), %rsi
	subl	$8, %ecx
	subq	$1, %rdx
	shrq	$8, %rdi
	addq	$1, %rsi
	movq	%rdi, 8(%r14)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%r14)
	movl	$64, %esi
	salq	$56, %rax
	subl	%ecx, %esi
	movl	%ecx, 16(%r14)
	orq	%rdi, %rax
	movq	%rdx, 32(%r14)
	movq	%rax, 8(%r14)
	cmpl	$5, %esi
	jbe	.L1390
.L1391:
	shrq	%cl, %rax
	addl	$6, %ecx
	movl	292(%r14), %esi
	movq	56(%r14), %rdi
	movl	%ecx, 16(%r14)
	movl	%eax, %ecx
	shrl	$2, %eax
	andl	$3, %ecx
	andl	$15, %eax
	sall	%cl, %eax
	movl	%ecx, 328(%r14)
	addl	$16, %eax
	movl	%eax, 332(%r14)
	leaq	kBitMask(%rip), %rax
	movl	(%rax,%rcx,4), %eax
	movl	%eax, 336(%r14)
	call	*40(%r14)
	movq	%rax, 5096(%r14)
	testq	%rax, %rax
	je	.L1443
	movq	$6, (%r14)
	xorl	%r8d, %r8d
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	-96(%rbp), %rdi
	call	BrotliWarmupBitReader
	testl	%eax, %eax
	jne	.L1567
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1321:
	movl	4(%r14), %eax
	cmpl	$1, %eax
	je	.L1409
	cmpl	$2, %eax
	je	.L1410
	testl	%eax, %eax
	je	.L1568
	leaq	116(%r14), %rdi
	movl	$-31, %esi
	call	SaveErrorCode.isra.0
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
.L1319:
	cmpq	$0, 128(%r15)
	je	.L1551
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	$1, %r8d
	call	WriteRingBuffer
	cmpl	$1, %eax
	jne	.L1516
.L1551:
	leaq	116(%r15), %rdi
	movl	$1, %esi
	call	SaveErrorCode.isra.0
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1567:
	movzbl	5072(%r14), %eax
	movq	8(%r14), %rdi
	movl	%eax, %r8d
	andl	$-33, %eax
	movb	%al, 5072(%r14)
	movl	%eax, %esi
	movl	16(%r14), %eax
	shrb	$5, %r8b
	andl	$1, %r8d
	leal	1(%rax), %ecx
	btq	%rax, %rdi
	movl	%ecx, 16(%r14)
	jnc	.L1569
	movq	%rdi, %rdx
	shrq	%cl, %rdx
	leal	4(%rax), %ecx
	movl	%ecx, 16(%r14)
	andl	$7, %edx
	jne	.L1570
	movq	%rdi, %rdx
	shrq	%cl, %rdx
	leal	7(%rax), %ecx
	andl	$7, %edx
	movl	%ecx, 16(%r14)
	cmpl	$1, %edx
	je	.L1571
	leal	8(%rdx), %eax
	testl	%edx, %edx
	movl	$17, %edx
	cmove	%edx, %eax
	movl	%eax, 5076(%r14)
.L1352:
	testb	$32, 5072(%r14)
	je	.L1357
	movl	$1, (%r14)
	.p2align 4,,10
	.p2align 3
.L1338:
	movl	16(%r14), %edx
	movl	$64, %eax
	subl	%edx, %eax
	cmpl	$5, %eax
	ja	.L1572
	movq	32(%r14), %rcx
.L1359:
	testq	%rcx, %rcx
	je	.L1542
	movq	8(%r14), %rdi
	movq	24(%r14), %rsi
	subl	$8, %edx
	subq	$1, %rcx
	shrq	$8, %rdi
	addq	$1, %rsi
	movq	%rdi, 8(%r14)
	movzbl	-1(%rsi), %eax
	movq	%rsi, 24(%r14)
	movl	$64, %esi
	salq	$56, %rax
	subl	%edx, %esi
	movl	%edx, 16(%r14)
	orq	%rdi, %rax
	movq	%rcx, 32(%r14)
	movq	%rax, 8(%r14)
	cmpl	$5, %esi
	jbe	.L1359
.L1360:
	movl	%edx, %ecx
	addl	$6, %edx
	shrq	%cl, %rax
	movl	%edx, 16(%r14)
	movl	%eax, %ecx
	andl	$63, %ecx
	leal	-10(%rcx), %eax
	movl	%ecx, 5076(%r14)
	cmpl	$20, %eax
	ja	.L1438
	movl	$2, (%r14)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1575:
	movl	272(%r14), %r9d
	testl	%r9d, %r9d
	je	.L1369
	movq	%r14, %rdi
	call	BrotliCalculateRingBufferSize
	testb	$2, 5072(%r14)
	je	.L1367
	movl	$11, (%r14)
	.p2align 4,,10
	.p2align 3
.L1331:
	movq	-88(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	CopyUncompressedBlockToOutput
	cmpl	$1, %eax
	jne	.L1547
.L1369:
	movl	$14, (%r14)
.L1329:
	movl	272(%r14), %edx
	testl	%edx, %edx
	jns	.L1573
	movq	%r13, %rbx
	movl	$-10, %esi
.L1304:
	movl	72(%r14), %r10d
	testl	%r10d, %r10d
	je	.L1317
	movl	$0, 72(%r14)
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1327:
	movslq	4(%r14), %rax
	cmpl	$2, %eax
	jle	.L1368
	movl	$5, (%r14)
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1558:
	imull	$396, %r11d, %edx
	movq	256(%r14), %rsi
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,4), %rdi
	movq	8(%r14), %rsi
	cmpl	$14, %eax
	ja	.L1377
	movq	32(%r14), %r8
.L1379:
	testq	%r8, %r8
	je	.L1378
	movq	24(%r14), %rax
	shrq	$8, %rsi
	subl	$8, %ecx
	subq	$1, %r8
	movq	%rsi, 8(%r14)
	movzbl	(%rax), %edx
	addq	$1, %rax
	movl	%ecx, 16(%r14)
	movq	%rax, 24(%r14)
	movl	$64, %eax
	salq	$56, %rdx
	subl	%ecx, %eax
	movq	%r8, 32(%r14)
	orq	%rdx, %rsi
	movq	%rsi, 8(%r14)
	cmpl	$14, %eax
	jbe	.L1379
.L1377:
	shrq	%cl, %rsi
	movzbl	%sil, %eax
	leaq	(%rdi,%rax,4), %rax
	movzbl	(%rax), %edi
	movzwl	2(%rax), %r9d
	cmpb	$8, %dil
	jbe	.L1381
	subl	$8, %edi
	leaq	kBitMask(%rip), %rdx
	shrl	$8, %esi
	addl	$8, %ecx
	andl	(%rdx,%rdi,4), %esi
	andl	$127, %esi
	leal	(%rsi,%r9), %edx
	leaq	(%rax,%rdx,4), %rax
	movzbl	(%rax), %edi
	movzwl	2(%rax), %r9d
.L1381:
	addl	%edi, %ecx
	movl	$64, %eax
	movl	%r9d, -60(%rbp)
	movl	%ecx, 16(%r14)
	subl	%ecx, %eax
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1403:
	movl	$62, %edi
	leal	-16(%rdx), %esi
	sall	%cl, %edi
	movl	%edi, %ecx
	addl	%edx, %ecx
	movl	$1, %edx
	movl	%ecx, -108(%rbp)
	movl	%eax, %ecx
	sall	%cl, %edx
	leaq	bound.3982(%rip), %rcx
	movl	(%rcx,%rax,4), %edi
	leaq	diff.3983(%rip), %rcx
	movl	(%rcx,%rax,4), %ecx
	cmpl	%edi, %esi
	jb	.L1574
	leal	(%rdx,%rdi), %eax
	leal	(%rsi,%rcx), %edx
	addl	%eax, %ecx
	cmpl	%eax, %esi
	cmovbe	%ecx, %edx
	movl	%edx, -104(%rbp)
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1365:
	testb	$4, %al
	je	.L1575
	movl	$12, (%r14)
.L1330:
	movl	272(%r14), %r8d
	testl	%r8d, %r8d
	jle	.L1369
	movl	16(%r14), %edi
.L1345:
	movl	$64, %eax
	subl	%edi, %eax
	cmpl	$7, %eax
	ja	.L1576
	movq	32(%r14), %rdx
	testq	%rdx, %rdx
	je	.L1542
.L1577:
	movq	8(%r14), %rsi
	movq	24(%r14), %rcx
	subq	$1, %rdx
	shrq	$8, %rsi
	addq	$1, %rcx
	movq	%rsi, 8(%r14)
	movzbl	-1(%rcx), %eax
	movq	%rcx, 24(%r14)
	movl	$64, %ecx
	salq	$56, %rax
	movq	%rdx, 32(%r14)
	orq	%rsi, %rax
	movq	%rax, 8(%r14)
	leal	-8(%rdi), %eax
	subl	%eax, %ecx
	movl	%eax, 16(%r14)
	cmpl	$7, %ecx
	ja	.L1372
	movl	%eax, %edi
	testq	%rdx, %rdx
	jne	.L1577
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	movl	%r9d, 276(%r15)
	movl	$1, 5068(%r15)
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	movl	%r9d, 4(%r15)
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1557:
	movzbl	5072(%r14), %eax
	testb	$6, %al
	je	.L1365
	movl	16(%r14), %ecx
	movl	%ecx, %edx
	negl	%edx
	andl	$7, %edx
	je	.L1365
	movq	8(%r14), %rsi
	movl	%edx, %edi
	addl	%ecx, %edx
	movl	%edx, 16(%r14)
	leaq	kBitMask(%rip), %rdx
	shrq	%cl, %rsi
	testl	%esi, (%rdx,%rdi,4)
	je	.L1365
	movq	%r13, %rbx
	movl	$-14, %esi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1378:
	leaq	-60(%rbp), %rcx
	leaq	16(%r14), %rdx
	call	SafeDecodeSymbol.isra.0
	testl	%eax, %eax
	je	.L1542
	movl	16(%r14), %ecx
	movl	$64, %eax
	movl	-60(%rbp), %r9d
	subl	%ecx, %eax
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	-104(%rbp), %r13
.L1420:
	movl	$0, 5048(%r14)
	movl	4(%r14), %eax
	addl	$1, %eax
	movl	%eax, 4(%r14)
	cmpl	$2, %eax
	jle	.L1522
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	movl	308(%r15), %eax
	movq	%r15, %rdi
	movl	%eax, %ecx
	movl	%eax, %edx
	shrl	$5, %ecx
	sall	$6, %edx
	addq	5088(%r15), %rdx
	movl	5120(%r15,%rcx,4), %esi
	movl	%eax, %ecx
	movq	%rdx, 160(%r15)
	andl	$31, %ecx
	shrq	%cl, %rsi
	andl	$1, %esi
	movl	%esi, 264(%r15)
	movzbl	(%rdx), %ecx
	movq	176(%r15), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 352(%r15)
	movq	5096(%r15), %rdx
	movzbl	(%rdx,%rax), %eax
	leaq	kContextLookup(%rip), %rdx
	salq	$9, %rax
	andl	$1536, %eax
	addq	%rdx, %rax
	movq	%rax, 152(%r15)
	movq	344(%r15), %rax
	movq	%rax, 168(%r15)
	movq	200(%r15), %rax
	movq	(%rax), %rax
	movq	%rax, 144(%r15)
	call	BrotliEnsureRingBuffer
	testl	%eax, %eax
	jne	.L1423
	movq	%r14, %rbx
	movl	$-27, %esi
	movq	%r15, %r14
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	%r14, %rdi
	call	BrotliDecoderStateCleanupAfterMetablock
	testb	$1, 5072(%r14)
	jne	.L1426
	movl	$3, (%r14)
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	24(%r15), %rax
	movq	-80(%rbp), %rcx
	movq	%r14, %rbx
	movq	%r15, %r14
	movq	%rax, (%rcx)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L1316
	movzbl	(%rax), %edx
	movl	$1, 72(%r15)
	movb	%dl, 64(%r15)
	leaq	1(%rax), %rdx
	movq	%rdx, (%rcx)
	subq	$1, (%rbx)
	je	.L1316
	movzbl	1(%rax), %edx
	movl	$2, 72(%r15)
	movb	%dl, 65(%r15)
	leaq	2(%rax), %rdx
	movq	%rdx, (%rcx)
	subq	$1, (%rbx)
	je	.L1316
	movzbl	2(%rax), %edx
	movl	$3, 72(%r15)
	movb	%dl, 66(%r15)
	leaq	3(%rax), %rdx
	movq	%rdx, (%rcx)
	subq	$1, (%rbx)
	je	.L1316
	movzbl	3(%rax), %edx
	movl	$4, 72(%r15)
	movb	%dl, 67(%r15)
	leaq	4(%rax), %rdx
	movq	%rdx, (%rcx)
	subq	$1, (%rbx)
	je	.L1316
	movzbl	4(%rax), %edx
	movl	$5, 72(%r15)
	movb	%dl, 68(%r15)
	leaq	5(%rax), %rdx
	movq	%rdx, (%rcx)
	subq	$1, (%rbx)
	je	.L1316
	movzbl	5(%rax), %edx
	movl	$6, 72(%r15)
	movb	%dl, 69(%r15)
	leaq	6(%rax), %rdx
	movq	%rdx, (%rcx)
	subq	$1, (%rbx)
	je	.L1316
	movzbl	6(%rax), %edx
	movl	$7, 72(%r15)
	movb	%dl, 70(%r15)
	leaq	7(%rax), %rdx
	movq	%rdx, (%rcx)
	subq	$1, (%rbx)
	je	.L1316
	movzbl	7(%rax), %edx
	addq	$8, %rax
	movl	$8, 72(%r15)
	movb	%dl, 71(%r15)
	movq	%rax, (%rcx)
	subq	$1, (%rbx)
	jmp	.L1316
.L1367:
	movq	$17, (%r14)
	xorl	%eax, %eax
.L1368:
	movq	-96(%rbp), %rsi
	leaq	292(%r14,%rax,4), %rdx
	leaq	5064(%r14), %rdi
	call	DecodeVarLenUint8.isra.0
	cmpl	$1, %eax
	jne	.L1547
	movslq	4(%r14), %rdx
	movq	%rdx, %rax
	leaq	(%r14,%rdx,4), %rdx
	movl	292(%rdx), %ecx
	leal	1(%rcx), %edi
	movl	%edi, 292(%rdx)
	cmpl	$1, %edi
	jbe	.L1578
	movl	$18, (%r14)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	%r13, %rbx
	movl	$-30, %esi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1560:
	movl	2172(%r14), %eax
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1410:
	leaq	224(%r14), %r12
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1409:
	leaq	200(%r14), %r12
	jmp	.L1412
.L1553:
	movl	%eax, %esi
	movq	%r15, %r14
	jmp	.L1309
.L1426:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	movl	16(%r15), %edx
	movl	%edx, %eax
	negl	%eax
	andl	$7, %eax
	je	.L1427
	movq	8(%r15), %rbx
	movl	%edx, %ecx
	movl	%eax, %esi
	addl	%eax, %edx
	leaq	kBitMask(%rip), %rax
	movl	%edx, 16(%r15)
	shrq	%cl, %rbx
	testl	%ebx, (%rax,%rsi,4)
	je	.L1427
	movq	%r14, %rbx
	movl	$-15, %esi
	movq	%r15, %r14
	jmp	.L1304
.L1574:
	addl	%esi, %edx
	leal	(%rdx,%rcx), %eax
	movl	%eax, -104(%rbp)
	jmp	.L1405
.L1317:
	movl	16(%r14), %edx
	movl	$64, %eax
	movq	32(%r14), %r8
	movq	24(%r14), %rdi
	subl	%edx, %eax
	movl	%eax, %ecx
	shrl	$3, %eax
	subq	%rax, %rdi
	addq	%rax, %r8
	andl	$-8, %ecx
	movq	%rdi, 24(%r14)
	movq	%rdi, %rax
	xorl	%edi, %edi
	movq	%r8, 32(%r14)
	cmpl	$64, %ecx
	je	.L1318
	movq	8(%r14), %rdi
	salq	%cl, %rdi
.L1318:
	addl	%ecx, %edx
	movq	%rdi, 8(%r14)
	movl	%edx, 16(%r14)
	movq	%r8, (%rbx)
	movq	-80(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L1309
.L1576:
	addl	$8, %edi
.L1372:
	subl	$1, %r8d
	movl	%edi, 16(%r14)
	movl	%r8d, 272(%r14)
	jne	.L1345
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1427:
	movl	72(%r15), %eax
	testl	%eax, %eax
	jne	.L1428
	movl	$64, %eax
	movq	32(%r15), %rdi
	movq	24(%r15), %rbx
	xorl	%esi, %esi
	subl	%edx, %eax
	movl	%eax, %ecx
	shrl	$3, %eax
	addq	%rax, %rdi
	subq	%rax, %rbx
	andl	$-8, %ecx
	movq	%rdi, 32(%r15)
	movq	%rbx, %rax
	movq	%rbx, 24(%r15)
	cmpl	$64, %ecx
	je	.L1429
	movq	8(%r15), %rsi
	salq	%cl, %rsi
.L1429:
	movq	-80(%rbp), %rbx
	addl	%ecx, %edx
	movq	%rsi, 8(%r15)
	movl	%edx, 16(%r15)
	movq	%rdi, (%r14)
	movq	%rax, (%rbx)
.L1428:
	movl	$24, (%r15)
	jmp	.L1319
.L1569:
	movl	$16, 5076(%r14)
	jmp	.L1352
.L1563:
	movl	272(%r14), %edi
	testl	%edi, %edi
	je	.L1369
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
.L1423:
	movl	$7, (%r15)
	jmp	.L1332
.L1384:
	movq	8(%r14), %rax
	jmp	.L1387
.L1438:
	movq	%r13, %rbx
	movl	$-13, %esi
	jmp	.L1304
.L1570:
	addl	$17, %edx
	movl	%edx, 5076(%r14)
	jmp	.L1352
.L1565:
	movq	8(%r14), %rax
	jmp	.L1397
.L1568:
	leaq	176(%r14), %r12
	jmp	.L1412
.L1566:
	movq	8(%r14), %rax
	jmp	.L1391
.L1572:
	movq	8(%r14), %rax
	jmp	.L1360
.L1571:
	testb	%r8b, %r8b
	je	.L1438
	addl	$8, %eax
	btq	%rcx, %rdi
	movl	%eax, 16(%r14)
	jc	.L1438
	movl	%esi, %eax
	orl	$32, %eax
	movb	%al, 5072(%r14)
	jmp	.L1352
.L1564:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	movl	$10, (%r15)
	jmp	.L1332
.L1578:
	addl	$1, %eax
	movl	%eax, 4(%r14)
	movl	(%r14), %eax
	jmp	.L1313
.L1425:
	movq	%r15, %r12
	movq	%r14, %r15
	movq	%r13, %r14
	movl	$8, (%r15)
	jmp	.L1332
.L1443:
	movq	%r13, %rbx
	movl	$-21, %esi
	jmp	.L1304
.L1556:
	call	__stack_chk_fail@PLT
.L1559:
	leaq	116(%r14), %rdi
	movl	$-22, %esi
	call	SaveErrorCode.isra.0
	jmp	.L1296
.L1524:
	movq	%r14, %rbx
	movl	%eax, %esi
	movq	%r15, %r14
	jmp	.L1304
	.cfi_endproc
.LFE128:
	.size	BrotliDecoderDecompressStream, .-BrotliDecoderDecompressStream
	.p2align 4
	.globl	BrotliDecoderHasMoreOutput
	.type	BrotliDecoderHasMoreOutput, @function
BrotliDecoderHasMoreOutput:
.LFB129:
	.cfi_startproc
	endbr64
	movl	116(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L1579
	cmpq	$0, 128(%rdi)
	je	.L1579
	movslq	88(%rdi), %rax
	imulq	384(%rdi), %rax
	movslq	76(%rdi), %rdx
	addq	%rdx, %rax
	cmpq	392(%rdi), %rax
	setne	%al
	movzbl	%al, %eax
.L1579:
	ret
	.cfi_endproc
.LFE129:
	.size	BrotliDecoderHasMoreOutput, .-BrotliDecoderHasMoreOutput
	.p2align 4
	.globl	BrotliDecoderTakeOutput
	.type	BrotliDecoderTakeOutput, @function
BrotliDecoderTakeOutput:
.LFB130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$16777216, %eax
	movq	$0, -56(%rbp)
	testq	%r12, %r12
	cmove	%rax, %r12
	cmpq	$0, 128(%rdi)
	movq	%r12, -48(%rbp)
	je	.L1590
	movl	116(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	js	.L1590
	call	WrapRingBuffer
	leaq	-48(%rbp), %rsi
	leaq	-56(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	WriteRingBuffer
	movl	%eax, %esi
	andl	$-3, %eax
	cmpl	$1, %eax
	jne	.L1588
	movq	%r12, %rax
	subq	-48(%rbp), %rax
	movq	%rax, 0(%r13)
	movq	-56(%rbp), %rax
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1588:
	testl	%esi, %esi
	jns	.L1590
	leaq	116(%rbx), %rdi
	call	SaveErrorCode.isra.0
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	$0, 0(%r13)
	xorl	%eax, %eax
.L1583:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1594
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1594:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE130:
	.size	BrotliDecoderTakeOutput, .-BrotliDecoderTakeOutput
	.p2align 4
	.globl	BrotliDecoderIsUsed
	.type	BrotliDecoderIsUsed, @function
BrotliDecoderIsUsed:
.LFB131:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L1595
	xorl	%eax, %eax
	cmpl	$64, 16(%rdi)
	setne	%al
.L1595:
	ret
	.cfi_endproc
.LFE131:
	.size	BrotliDecoderIsUsed, .-BrotliDecoderIsUsed
	.p2align 4
	.globl	BrotliDecoderIsFinished
	.type	BrotliDecoderIsFinished, @function
BrotliDecoderIsFinished:
.LFB132:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$24, (%rdi)
	je	.L1603
.L1598:
	ret
	.p2align 4,,10
	.p2align 3
.L1603:
	movl	116(%rdi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	js	.L1598
	cmpq	$0, 128(%rdi)
	je	.L1598
	movslq	88(%rdi), %rax
	imulq	384(%rdi), %rax
	movslq	76(%rdi), %rdx
	addq	%rdx, %rax
	cmpq	392(%rdi), %rax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE132:
	.size	BrotliDecoderIsFinished, .-BrotliDecoderIsFinished
	.p2align 4
	.globl	BrotliDecoderGetErrorCode
	.type	BrotliDecoderGetErrorCode, @function
BrotliDecoderGetErrorCode:
.LFB133:
	.cfi_startproc
	endbr64
	movl	116(%rdi), %eax
	ret
	.cfi_endproc
.LFE133:
	.size	BrotliDecoderGetErrorCode, .-BrotliDecoderGetErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC10:
	.string	"INVALID"
.LC11:
	.string	"NO_ERROR"
.LC12:
	.string	"NEEDS_MORE_INPUT"
.LC13:
	.string	"NEEDS_MORE_OUTPUT"
.LC14:
	.string	"EXUBERANT_NIBBLE"
.LC15:
	.string	"RESERVED"
.LC16:
	.string	"EXUBERANT_META_NIBBLE"
.LC17:
	.string	"SIMPLE_HUFFMAN_ALPHABET"
.LC18:
	.string	"SIMPLE_HUFFMAN_SAME"
.LC19:
	.string	"CL_SPACE"
.LC20:
	.string	"HUFFMAN_SPACE"
.LC21:
	.string	"CONTEXT_MAP_REPEAT"
.LC22:
	.string	"BLOCK_LENGTH_1"
.LC23:
	.string	"BLOCK_LENGTH_2"
.LC24:
	.string	"TRANSFORM"
.LC25:
	.string	"DICTIONARY"
.LC26:
	.string	"WINDOW_BITS"
.LC27:
	.string	"PADDING_1"
.LC28:
	.string	"PADDING_2"
.LC29:
	.string	"DISTANCE"
.LC30:
	.string	"DICTIONARY_NOT_SET"
.LC31:
	.string	"INVALID_ARGUMENTS"
.LC32:
	.string	"CONTEXT_MODES"
.LC33:
	.string	"TREE_GROUPS"
.LC34:
	.string	"CONTEXT_MAP"
.LC35:
	.string	"RING_BUFFER_1"
.LC36:
	.string	"RING_BUFFER_2"
.LC37:
	.string	"BLOCK_TYPE_TREES"
.LC38:
	.string	"UNREACHABLE"
.LC39:
	.string	"SUCCESS"
	.text
	.p2align 4
	.globl	BrotliDecoderErrorString
	.type	BrotliDecoderErrorString, @function
BrotliDecoderErrorString:
.LFB134:
	.cfi_startproc
	endbr64
	addl	$31, %edi
	cmpl	$34, %edi
	ja	.L1606
	leaq	.L1608(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1608:
	.long	.L1636-.L1608
	.long	.L1635-.L1608
	.long	.L1606-.L1608
	.long	.L1606-.L1608
	.long	.L1634-.L1608
	.long	.L1633-.L1608
	.long	.L1632-.L1608
	.long	.L1606-.L1608
	.long	.L1606-.L1608
	.long	.L1631-.L1608
	.long	.L1630-.L1608
	.long	.L1629-.L1608
	.long	.L1628-.L1608
	.long	.L1606-.L1608
	.long	.L1606-.L1608
	.long	.L1627-.L1608
	.long	.L1626-.L1608
	.long	.L1625-.L1608
	.long	.L1624-.L1608
	.long	.L1623-.L1608
	.long	.L1622-.L1608
	.long	.L1621-.L1608
	.long	.L1620-.L1608
	.long	.L1619-.L1608
	.long	.L1618-.L1608
	.long	.L1617-.L1608
	.long	.L1616-.L1608
	.long	.L1615-.L1608
	.long	.L1614-.L1608
	.long	.L1613-.L1608
	.long	.L1612-.L1608
	.long	.L1637-.L1608
	.long	.L1610-.L1608
	.long	.L1609-.L1608
	.long	.L1607-.L1608
	.text
	.p2align 4,,10
	.p2align 3
.L1606:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1607:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1609:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1612:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1613:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1614:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1615:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1617:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1618:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1619:
	leaq	.LC21(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1620:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1621:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1622:
	leaq	.LC24(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1623:
	leaq	.LC25(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1624:
	leaq	.LC26(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1625:
	leaq	.LC27(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1626:
	leaq	.LC28(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1627:
	leaq	.LC29(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1628:
	leaq	.LC30(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1629:
	leaq	.LC31(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1630:
	leaq	.LC32(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1631:
	leaq	.LC33(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1632:
	leaq	.LC34(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1633:
	leaq	.LC35(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1634:
	leaq	.LC36(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1635:
	leaq	.LC37(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1636:
	leaq	.LC38(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1637:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1610:
	leaq	.LC39(%rip), %rax
	ret
	.cfi_endproc
.LFE134:
	.size	BrotliDecoderErrorString, .-BrotliDecoderErrorString
	.p2align 4
	.globl	BrotliDecoderVersion
	.type	BrotliDecoderVersion, @function
BrotliDecoderVersion:
.LFB135:
	.cfi_startproc
	endbr64
	movl	$16777223, %eax
	ret
	.cfi_endproc
.LFE135:
	.size	BrotliDecoderVersion, .-BrotliDecoderVersion
	.section	.rodata
	.align 16
	.type	diff.3983, @object
	.size	diff.3983, 16
diff.3983:
	.long	73
	.long	126
	.long	228
	.long	424
	.align 16
	.type	bound.3982, @object
	.size	bound.3982, 16
bound.3982:
	.long	0
	.long	4
	.long	12
	.long	28
	.align 16
	.type	kCodeLengthPrefixValue, @object
	.size	kCodeLengthPrefixValue, 16
kCodeLengthPrefixValue:
	.string	""
	.string	"\004\003\002"
	.string	"\004\003\001"
	.string	"\004\003\002"
	.ascii	"\004\003\005"
	.align 16
	.type	kCodeLengthPrefixLength, @object
	.size	kCodeLengthPrefixLength, 16
kCodeLengthPrefixLength:
	.ascii	"\002\002\002\003\002\002\002\004\002\002\002\003\002\002\002"
	.ascii	"\004"
	.align 16
	.type	kCodeLengthCodeOrder, @object
	.size	kCodeLengthCodeOrder, 18
kCodeLengthCodeOrder:
	.string	"\001\002\003\004"
	.ascii	"\005\021\006\020\007\b\t\n\013\f\r\016\017"
	.align 32
	.type	kCmdLut, @object
	.size	kCmdLut, 5632
kCmdLut:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.value	0
	.value	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.value	0
	.value	3
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.value	0
	.value	4
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	0
	.value	5
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	0
	.value	6
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	0
	.value	7
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	0
	.value	8
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	0
	.value	9
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.value	1
	.value	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.value	1
	.value	3
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.value	1
	.value	4
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	1
	.value	5
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	1
	.value	6
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	1
	.value	7
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	1
	.value	8
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	1
	.value	9
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.value	2
	.value	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.value	2
	.value	3
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.value	2
	.value	4
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	2
	.value	5
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	2
	.value	6
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	2
	.value	7
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	2
	.value	8
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	2
	.value	9
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.value	3
	.value	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.value	3
	.value	3
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.value	3
	.value	4
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	3
	.value	5
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	3
	.value	6
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	3
	.value	7
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	3
	.value	8
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	3
	.value	9
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.value	4
	.value	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.value	4
	.value	3
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.value	4
	.value	4
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	4
	.value	5
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	4
	.value	6
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	4
	.value	7
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	4
	.value	8
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	4
	.value	9
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.value	5
	.value	2
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.value	5
	.value	3
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.value	5
	.value	4
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	5
	.value	5
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	5
	.value	6
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	5
	.value	7
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	5
	.value	8
	.byte	0
	.byte	0
	.byte	0
	.byte	3
	.value	5
	.value	9
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.value	6
	.value	2
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.value	6
	.value	3
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.value	6
	.value	4
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	6
	.value	5
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	6
	.value	6
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	6
	.value	7
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	6
	.value	8
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	6
	.value	9
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.value	8
	.value	2
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.value	8
	.value	3
	.byte	1
	.byte	0
	.byte	0
	.byte	2
	.value	8
	.value	4
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	8
	.value	5
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	8
	.value	6
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	8
	.value	7
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	8
	.value	8
	.byte	1
	.byte	0
	.byte	0
	.byte	3
	.value	8
	.value	9
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	0
	.value	10
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	0
	.value	12
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	0
	.value	14
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	0
	.value	18
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	0
	.value	22
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	0
	.value	30
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	0
	.value	38
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	0
	.value	54
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	1
	.value	10
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	1
	.value	12
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	1
	.value	14
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	1
	.value	18
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	1
	.value	22
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	1
	.value	30
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	1
	.value	38
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	1
	.value	54
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	2
	.value	10
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	2
	.value	12
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	2
	.value	14
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	2
	.value	18
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	2
	.value	22
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	2
	.value	30
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	2
	.value	38
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	2
	.value	54
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	3
	.value	10
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	3
	.value	12
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	3
	.value	14
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	3
	.value	18
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	3
	.value	22
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	3
	.value	30
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	3
	.value	38
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	3
	.value	54
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	4
	.value	10
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	4
	.value	12
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	4
	.value	14
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	4
	.value	18
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	4
	.value	22
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	4
	.value	30
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	4
	.value	38
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	4
	.value	54
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	5
	.value	10
	.byte	0
	.byte	1
	.byte	0
	.byte	3
	.value	5
	.value	12
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	5
	.value	14
	.byte	0
	.byte	2
	.byte	0
	.byte	3
	.value	5
	.value	18
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	5
	.value	22
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.value	5
	.value	30
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	5
	.value	38
	.byte	0
	.byte	4
	.byte	0
	.byte	3
	.value	5
	.value	54
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.value	6
	.value	10
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.value	6
	.value	12
	.byte	1
	.byte	2
	.byte	0
	.byte	3
	.value	6
	.value	14
	.byte	1
	.byte	2
	.byte	0
	.byte	3
	.value	6
	.value	18
	.byte	1
	.byte	3
	.byte	0
	.byte	3
	.value	6
	.value	22
	.byte	1
	.byte	3
	.byte	0
	.byte	3
	.value	6
	.value	30
	.byte	1
	.byte	4
	.byte	0
	.byte	3
	.value	6
	.value	38
	.byte	1
	.byte	4
	.byte	0
	.byte	3
	.value	6
	.value	54
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.value	8
	.value	10
	.byte	1
	.byte	1
	.byte	0
	.byte	3
	.value	8
	.value	12
	.byte	1
	.byte	2
	.byte	0
	.byte	3
	.value	8
	.value	14
	.byte	1
	.byte	2
	.byte	0
	.byte	3
	.value	8
	.value	18
	.byte	1
	.byte	3
	.byte	0
	.byte	3
	.value	8
	.value	22
	.byte	1
	.byte	3
	.byte	0
	.byte	3
	.value	8
	.value	30
	.byte	1
	.byte	4
	.byte	0
	.byte	3
	.value	8
	.value	38
	.byte	1
	.byte	4
	.byte	0
	.byte	3
	.value	8
	.value	54
	.byte	0
	.byte	0
	.byte	-1
	.byte	0
	.value	0
	.value	2
	.byte	0
	.byte	0
	.byte	-1
	.byte	1
	.value	0
	.value	3
	.byte	0
	.byte	0
	.byte	-1
	.byte	2
	.value	0
	.value	4
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	0
	.value	5
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	0
	.value	6
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	0
	.value	7
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	0
	.value	8
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	0
	.value	9
	.byte	0
	.byte	0
	.byte	-1
	.byte	0
	.value	1
	.value	2
	.byte	0
	.byte	0
	.byte	-1
	.byte	1
	.value	1
	.value	3
	.byte	0
	.byte	0
	.byte	-1
	.byte	2
	.value	1
	.value	4
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	1
	.value	5
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	1
	.value	6
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	1
	.value	7
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	1
	.value	8
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	1
	.value	9
	.byte	0
	.byte	0
	.byte	-1
	.byte	0
	.value	2
	.value	2
	.byte	0
	.byte	0
	.byte	-1
	.byte	1
	.value	2
	.value	3
	.byte	0
	.byte	0
	.byte	-1
	.byte	2
	.value	2
	.value	4
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	2
	.value	5
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	2
	.value	6
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	2
	.value	7
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	2
	.value	8
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	2
	.value	9
	.byte	0
	.byte	0
	.byte	-1
	.byte	0
	.value	3
	.value	2
	.byte	0
	.byte	0
	.byte	-1
	.byte	1
	.value	3
	.value	3
	.byte	0
	.byte	0
	.byte	-1
	.byte	2
	.value	3
	.value	4
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	3
	.value	5
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	3
	.value	6
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	3
	.value	7
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	3
	.value	8
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	3
	.value	9
	.byte	0
	.byte	0
	.byte	-1
	.byte	0
	.value	4
	.value	2
	.byte	0
	.byte	0
	.byte	-1
	.byte	1
	.value	4
	.value	3
	.byte	0
	.byte	0
	.byte	-1
	.byte	2
	.value	4
	.value	4
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	4
	.value	5
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	4
	.value	6
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	4
	.value	7
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	4
	.value	8
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	4
	.value	9
	.byte	0
	.byte	0
	.byte	-1
	.byte	0
	.value	5
	.value	2
	.byte	0
	.byte	0
	.byte	-1
	.byte	1
	.value	5
	.value	3
	.byte	0
	.byte	0
	.byte	-1
	.byte	2
	.value	5
	.value	4
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	5
	.value	5
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	5
	.value	6
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	5
	.value	7
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	5
	.value	8
	.byte	0
	.byte	0
	.byte	-1
	.byte	3
	.value	5
	.value	9
	.byte	1
	.byte	0
	.byte	-1
	.byte	0
	.value	6
	.value	2
	.byte	1
	.byte	0
	.byte	-1
	.byte	1
	.value	6
	.value	3
	.byte	1
	.byte	0
	.byte	-1
	.byte	2
	.value	6
	.value	4
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	6
	.value	5
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	6
	.value	6
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	6
	.value	7
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	6
	.value	8
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	6
	.value	9
	.byte	1
	.byte	0
	.byte	-1
	.byte	0
	.value	8
	.value	2
	.byte	1
	.byte	0
	.byte	-1
	.byte	1
	.value	8
	.value	3
	.byte	1
	.byte	0
	.byte	-1
	.byte	2
	.value	8
	.value	4
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	8
	.value	5
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	8
	.value	6
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	8
	.value	7
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	8
	.value	8
	.byte	1
	.byte	0
	.byte	-1
	.byte	3
	.value	8
	.value	9
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	0
	.value	10
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	0
	.value	12
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	0
	.value	14
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	0
	.value	18
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	0
	.value	22
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	0
	.value	30
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	0
	.value	38
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	0
	.value	54
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	1
	.value	10
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	1
	.value	12
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	1
	.value	14
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	1
	.value	18
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	1
	.value	22
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	1
	.value	30
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	1
	.value	38
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	1
	.value	54
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	2
	.value	10
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	2
	.value	12
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	2
	.value	14
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	2
	.value	18
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	2
	.value	22
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	2
	.value	30
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	2
	.value	38
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	2
	.value	54
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	3
	.value	10
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	3
	.value	12
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	3
	.value	14
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	3
	.value	18
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	3
	.value	22
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	3
	.value	30
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	3
	.value	38
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	3
	.value	54
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	4
	.value	10
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	4
	.value	12
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	4
	.value	14
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	4
	.value	18
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	4
	.value	22
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	4
	.value	30
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	4
	.value	38
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	4
	.value	54
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	5
	.value	10
	.byte	0
	.byte	1
	.byte	-1
	.byte	3
	.value	5
	.value	12
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	5
	.value	14
	.byte	0
	.byte	2
	.byte	-1
	.byte	3
	.value	5
	.value	18
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	5
	.value	22
	.byte	0
	.byte	3
	.byte	-1
	.byte	3
	.value	5
	.value	30
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	5
	.value	38
	.byte	0
	.byte	4
	.byte	-1
	.byte	3
	.value	5
	.value	54
	.byte	1
	.byte	1
	.byte	-1
	.byte	3
	.value	6
	.value	10
	.byte	1
	.byte	1
	.byte	-1
	.byte	3
	.value	6
	.value	12
	.byte	1
	.byte	2
	.byte	-1
	.byte	3
	.value	6
	.value	14
	.byte	1
	.byte	2
	.byte	-1
	.byte	3
	.value	6
	.value	18
	.byte	1
	.byte	3
	.byte	-1
	.byte	3
	.value	6
	.value	22
	.byte	1
	.byte	3
	.byte	-1
	.byte	3
	.value	6
	.value	30
	.byte	1
	.byte	4
	.byte	-1
	.byte	3
	.value	6
	.value	38
	.byte	1
	.byte	4
	.byte	-1
	.byte	3
	.value	6
	.value	54
	.byte	1
	.byte	1
	.byte	-1
	.byte	3
	.value	8
	.value	10
	.byte	1
	.byte	1
	.byte	-1
	.byte	3
	.value	8
	.value	12
	.byte	1
	.byte	2
	.byte	-1
	.byte	3
	.value	8
	.value	14
	.byte	1
	.byte	2
	.byte	-1
	.byte	3
	.value	8
	.value	18
	.byte	1
	.byte	3
	.byte	-1
	.byte	3
	.value	8
	.value	22
	.byte	1
	.byte	3
	.byte	-1
	.byte	3
	.value	8
	.value	30
	.byte	1
	.byte	4
	.byte	-1
	.byte	3
	.value	8
	.value	38
	.byte	1
	.byte	4
	.byte	-1
	.byte	3
	.value	8
	.value	54
	.byte	2
	.byte	0
	.byte	-1
	.byte	0
	.value	10
	.value	2
	.byte	2
	.byte	0
	.byte	-1
	.byte	1
	.value	10
	.value	3
	.byte	2
	.byte	0
	.byte	-1
	.byte	2
	.value	10
	.value	4
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	10
	.value	5
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	10
	.value	6
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	10
	.value	7
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	10
	.value	8
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	10
	.value	9
	.byte	2
	.byte	0
	.byte	-1
	.byte	0
	.value	14
	.value	2
	.byte	2
	.byte	0
	.byte	-1
	.byte	1
	.value	14
	.value	3
	.byte	2
	.byte	0
	.byte	-1
	.byte	2
	.value	14
	.value	4
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	14
	.value	5
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	14
	.value	6
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	14
	.value	7
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	14
	.value	8
	.byte	2
	.byte	0
	.byte	-1
	.byte	3
	.value	14
	.value	9
	.byte	3
	.byte	0
	.byte	-1
	.byte	0
	.value	18
	.value	2
	.byte	3
	.byte	0
	.byte	-1
	.byte	1
	.value	18
	.value	3
	.byte	3
	.byte	0
	.byte	-1
	.byte	2
	.value	18
	.value	4
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	18
	.value	5
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	18
	.value	6
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	18
	.value	7
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	18
	.value	8
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	18
	.value	9
	.byte	3
	.byte	0
	.byte	-1
	.byte	0
	.value	26
	.value	2
	.byte	3
	.byte	0
	.byte	-1
	.byte	1
	.value	26
	.value	3
	.byte	3
	.byte	0
	.byte	-1
	.byte	2
	.value	26
	.value	4
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	26
	.value	5
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	26
	.value	6
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	26
	.value	7
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	26
	.value	8
	.byte	3
	.byte	0
	.byte	-1
	.byte	3
	.value	26
	.value	9
	.byte	4
	.byte	0
	.byte	-1
	.byte	0
	.value	34
	.value	2
	.byte	4
	.byte	0
	.byte	-1
	.byte	1
	.value	34
	.value	3
	.byte	4
	.byte	0
	.byte	-1
	.byte	2
	.value	34
	.value	4
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	34
	.value	5
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	34
	.value	6
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	34
	.value	7
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	34
	.value	8
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	34
	.value	9
	.byte	4
	.byte	0
	.byte	-1
	.byte	0
	.value	50
	.value	2
	.byte	4
	.byte	0
	.byte	-1
	.byte	1
	.value	50
	.value	3
	.byte	4
	.byte	0
	.byte	-1
	.byte	2
	.value	50
	.value	4
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	50
	.value	5
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	50
	.value	6
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	50
	.value	7
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	50
	.value	8
	.byte	4
	.byte	0
	.byte	-1
	.byte	3
	.value	50
	.value	9
	.byte	5
	.byte	0
	.byte	-1
	.byte	0
	.value	66
	.value	2
	.byte	5
	.byte	0
	.byte	-1
	.byte	1
	.value	66
	.value	3
	.byte	5
	.byte	0
	.byte	-1
	.byte	2
	.value	66
	.value	4
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	66
	.value	5
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	66
	.value	6
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	66
	.value	7
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	66
	.value	8
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	66
	.value	9
	.byte	5
	.byte	0
	.byte	-1
	.byte	0
	.value	98
	.value	2
	.byte	5
	.byte	0
	.byte	-1
	.byte	1
	.value	98
	.value	3
	.byte	5
	.byte	0
	.byte	-1
	.byte	2
	.value	98
	.value	4
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	98
	.value	5
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	98
	.value	6
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	98
	.value	7
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	98
	.value	8
	.byte	5
	.byte	0
	.byte	-1
	.byte	3
	.value	98
	.value	9
	.byte	2
	.byte	1
	.byte	-1
	.byte	3
	.value	10
	.value	10
	.byte	2
	.byte	1
	.byte	-1
	.byte	3
	.value	10
	.value	12
	.byte	2
	.byte	2
	.byte	-1
	.byte	3
	.value	10
	.value	14
	.byte	2
	.byte	2
	.byte	-1
	.byte	3
	.value	10
	.value	18
	.byte	2
	.byte	3
	.byte	-1
	.byte	3
	.value	10
	.value	22
	.byte	2
	.byte	3
	.byte	-1
	.byte	3
	.value	10
	.value	30
	.byte	2
	.byte	4
	.byte	-1
	.byte	3
	.value	10
	.value	38
	.byte	2
	.byte	4
	.byte	-1
	.byte	3
	.value	10
	.value	54
	.byte	2
	.byte	1
	.byte	-1
	.byte	3
	.value	14
	.value	10
	.byte	2
	.byte	1
	.byte	-1
	.byte	3
	.value	14
	.value	12
	.byte	2
	.byte	2
	.byte	-1
	.byte	3
	.value	14
	.value	14
	.byte	2
	.byte	2
	.byte	-1
	.byte	3
	.value	14
	.value	18
	.byte	2
	.byte	3
	.byte	-1
	.byte	3
	.value	14
	.value	22
	.byte	2
	.byte	3
	.byte	-1
	.byte	3
	.value	14
	.value	30
	.byte	2
	.byte	4
	.byte	-1
	.byte	3
	.value	14
	.value	38
	.byte	2
	.byte	4
	.byte	-1
	.byte	3
	.value	14
	.value	54
	.byte	3
	.byte	1
	.byte	-1
	.byte	3
	.value	18
	.value	10
	.byte	3
	.byte	1
	.byte	-1
	.byte	3
	.value	18
	.value	12
	.byte	3
	.byte	2
	.byte	-1
	.byte	3
	.value	18
	.value	14
	.byte	3
	.byte	2
	.byte	-1
	.byte	3
	.value	18
	.value	18
	.byte	3
	.byte	3
	.byte	-1
	.byte	3
	.value	18
	.value	22
	.byte	3
	.byte	3
	.byte	-1
	.byte	3
	.value	18
	.value	30
	.byte	3
	.byte	4
	.byte	-1
	.byte	3
	.value	18
	.value	38
	.byte	3
	.byte	4
	.byte	-1
	.byte	3
	.value	18
	.value	54
	.byte	3
	.byte	1
	.byte	-1
	.byte	3
	.value	26
	.value	10
	.byte	3
	.byte	1
	.byte	-1
	.byte	3
	.value	26
	.value	12
	.byte	3
	.byte	2
	.byte	-1
	.byte	3
	.value	26
	.value	14
	.byte	3
	.byte	2
	.byte	-1
	.byte	3
	.value	26
	.value	18
	.byte	3
	.byte	3
	.byte	-1
	.byte	3
	.value	26
	.value	22
	.byte	3
	.byte	3
	.byte	-1
	.byte	3
	.value	26
	.value	30
	.byte	3
	.byte	4
	.byte	-1
	.byte	3
	.value	26
	.value	38
	.byte	3
	.byte	4
	.byte	-1
	.byte	3
	.value	26
	.value	54
	.byte	4
	.byte	1
	.byte	-1
	.byte	3
	.value	34
	.value	10
	.byte	4
	.byte	1
	.byte	-1
	.byte	3
	.value	34
	.value	12
	.byte	4
	.byte	2
	.byte	-1
	.byte	3
	.value	34
	.value	14
	.byte	4
	.byte	2
	.byte	-1
	.byte	3
	.value	34
	.value	18
	.byte	4
	.byte	3
	.byte	-1
	.byte	3
	.value	34
	.value	22
	.byte	4
	.byte	3
	.byte	-1
	.byte	3
	.value	34
	.value	30
	.byte	4
	.byte	4
	.byte	-1
	.byte	3
	.value	34
	.value	38
	.byte	4
	.byte	4
	.byte	-1
	.byte	3
	.value	34
	.value	54
	.byte	4
	.byte	1
	.byte	-1
	.byte	3
	.value	50
	.value	10
	.byte	4
	.byte	1
	.byte	-1
	.byte	3
	.value	50
	.value	12
	.byte	4
	.byte	2
	.byte	-1
	.byte	3
	.value	50
	.value	14
	.byte	4
	.byte	2
	.byte	-1
	.byte	3
	.value	50
	.value	18
	.byte	4
	.byte	3
	.byte	-1
	.byte	3
	.value	50
	.value	22
	.byte	4
	.byte	3
	.byte	-1
	.byte	3
	.value	50
	.value	30
	.byte	4
	.byte	4
	.byte	-1
	.byte	3
	.value	50
	.value	38
	.byte	4
	.byte	4
	.byte	-1
	.byte	3
	.value	50
	.value	54
	.byte	5
	.byte	1
	.byte	-1
	.byte	3
	.value	66
	.value	10
	.byte	5
	.byte	1
	.byte	-1
	.byte	3
	.value	66
	.value	12
	.byte	5
	.byte	2
	.byte	-1
	.byte	3
	.value	66
	.value	14
	.byte	5
	.byte	2
	.byte	-1
	.byte	3
	.value	66
	.value	18
	.byte	5
	.byte	3
	.byte	-1
	.byte	3
	.value	66
	.value	22
	.byte	5
	.byte	3
	.byte	-1
	.byte	3
	.value	66
	.value	30
	.byte	5
	.byte	4
	.byte	-1
	.byte	3
	.value	66
	.value	38
	.byte	5
	.byte	4
	.byte	-1
	.byte	3
	.value	66
	.value	54
	.byte	5
	.byte	1
	.byte	-1
	.byte	3
	.value	98
	.value	10
	.byte	5
	.byte	1
	.byte	-1
	.byte	3
	.value	98
	.value	12
	.byte	5
	.byte	2
	.byte	-1
	.byte	3
	.value	98
	.value	14
	.byte	5
	.byte	2
	.byte	-1
	.byte	3
	.value	98
	.value	18
	.byte	5
	.byte	3
	.byte	-1
	.byte	3
	.value	98
	.value	22
	.byte	5
	.byte	3
	.byte	-1
	.byte	3
	.value	98
	.value	30
	.byte	5
	.byte	4
	.byte	-1
	.byte	3
	.value	98
	.value	38
	.byte	5
	.byte	4
	.byte	-1
	.byte	3
	.value	98
	.value	54
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	0
	.value	70
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	0
	.value	102
	.byte	0
	.byte	6
	.byte	-1
	.byte	3
	.value	0
	.value	134
	.byte	0
	.byte	7
	.byte	-1
	.byte	3
	.value	0
	.value	198
	.byte	0
	.byte	8
	.byte	-1
	.byte	3
	.value	0
	.value	326
	.byte	0
	.byte	9
	.byte	-1
	.byte	3
	.value	0
	.value	582
	.byte	0
	.byte	10
	.byte	-1
	.byte	3
	.value	0
	.value	1094
	.byte	0
	.byte	24
	.byte	-1
	.byte	3
	.value	0
	.value	2118
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	1
	.value	70
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	1
	.value	102
	.byte	0
	.byte	6
	.byte	-1
	.byte	3
	.value	1
	.value	134
	.byte	0
	.byte	7
	.byte	-1
	.byte	3
	.value	1
	.value	198
	.byte	0
	.byte	8
	.byte	-1
	.byte	3
	.value	1
	.value	326
	.byte	0
	.byte	9
	.byte	-1
	.byte	3
	.value	1
	.value	582
	.byte	0
	.byte	10
	.byte	-1
	.byte	3
	.value	1
	.value	1094
	.byte	0
	.byte	24
	.byte	-1
	.byte	3
	.value	1
	.value	2118
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	2
	.value	70
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	2
	.value	102
	.byte	0
	.byte	6
	.byte	-1
	.byte	3
	.value	2
	.value	134
	.byte	0
	.byte	7
	.byte	-1
	.byte	3
	.value	2
	.value	198
	.byte	0
	.byte	8
	.byte	-1
	.byte	3
	.value	2
	.value	326
	.byte	0
	.byte	9
	.byte	-1
	.byte	3
	.value	2
	.value	582
	.byte	0
	.byte	10
	.byte	-1
	.byte	3
	.value	2
	.value	1094
	.byte	0
	.byte	24
	.byte	-1
	.byte	3
	.value	2
	.value	2118
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	3
	.value	70
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	3
	.value	102
	.byte	0
	.byte	6
	.byte	-1
	.byte	3
	.value	3
	.value	134
	.byte	0
	.byte	7
	.byte	-1
	.byte	3
	.value	3
	.value	198
	.byte	0
	.byte	8
	.byte	-1
	.byte	3
	.value	3
	.value	326
	.byte	0
	.byte	9
	.byte	-1
	.byte	3
	.value	3
	.value	582
	.byte	0
	.byte	10
	.byte	-1
	.byte	3
	.value	3
	.value	1094
	.byte	0
	.byte	24
	.byte	-1
	.byte	3
	.value	3
	.value	2118
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	4
	.value	70
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	4
	.value	102
	.byte	0
	.byte	6
	.byte	-1
	.byte	3
	.value	4
	.value	134
	.byte	0
	.byte	7
	.byte	-1
	.byte	3
	.value	4
	.value	198
	.byte	0
	.byte	8
	.byte	-1
	.byte	3
	.value	4
	.value	326
	.byte	0
	.byte	9
	.byte	-1
	.byte	3
	.value	4
	.value	582
	.byte	0
	.byte	10
	.byte	-1
	.byte	3
	.value	4
	.value	1094
	.byte	0
	.byte	24
	.byte	-1
	.byte	3
	.value	4
	.value	2118
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	5
	.value	70
	.byte	0
	.byte	5
	.byte	-1
	.byte	3
	.value	5
	.value	102
	.byte	0
	.byte	6
	.byte	-1
	.byte	3
	.value	5
	.value	134
	.byte	0
	.byte	7
	.byte	-1
	.byte	3
	.value	5
	.value	198
	.byte	0
	.byte	8
	.byte	-1
	.byte	3
	.value	5
	.value	326
	.byte	0
	.byte	9
	.byte	-1
	.byte	3
	.value	5
	.value	582
	.byte	0
	.byte	10
	.byte	-1
	.byte	3
	.value	5
	.value	1094
	.byte	0
	.byte	24
	.byte	-1
	.byte	3
	.value	5
	.value	2118
	.byte	1
	.byte	5
	.byte	-1
	.byte	3
	.value	6
	.value	70
	.byte	1
	.byte	5
	.byte	-1
	.byte	3
	.value	6
	.value	102
	.byte	1
	.byte	6
	.byte	-1
	.byte	3
	.value	6
	.value	134
	.byte	1
	.byte	7
	.byte	-1
	.byte	3
	.value	6
	.value	198
	.byte	1
	.byte	8
	.byte	-1
	.byte	3
	.value	6
	.value	326
	.byte	1
	.byte	9
	.byte	-1
	.byte	3
	.value	6
	.value	582
	.byte	1
	.byte	10
	.byte	-1
	.byte	3
	.value	6
	.value	1094
	.byte	1
	.byte	24
	.byte	-1
	.byte	3
	.value	6
	.value	2118
	.byte	1
	.byte	5
	.byte	-1
	.byte	3
	.value	8
	.value	70
	.byte	1
	.byte	5
	.byte	-1
	.byte	3
	.value	8
	.value	102
	.byte	1
	.byte	6
	.byte	-1
	.byte	3
	.value	8
	.value	134
	.byte	1
	.byte	7
	.byte	-1
	.byte	3
	.value	8
	.value	198
	.byte	1
	.byte	8
	.byte	-1
	.byte	3
	.value	8
	.value	326
	.byte	1
	.byte	9
	.byte	-1
	.byte	3
	.value	8
	.value	582
	.byte	1
	.byte	10
	.byte	-1
	.byte	3
	.value	8
	.value	1094
	.byte	1
	.byte	24
	.byte	-1
	.byte	3
	.value	8
	.value	2118
	.byte	6
	.byte	0
	.byte	-1
	.byte	0
	.value	130
	.value	2
	.byte	6
	.byte	0
	.byte	-1
	.byte	1
	.value	130
	.value	3
	.byte	6
	.byte	0
	.byte	-1
	.byte	2
	.value	130
	.value	4
	.byte	6
	.byte	0
	.byte	-1
	.byte	3
	.value	130
	.value	5
	.byte	6
	.byte	0
	.byte	-1
	.byte	3
	.value	130
	.value	6
	.byte	6
	.byte	0
	.byte	-1
	.byte	3
	.value	130
	.value	7
	.byte	6
	.byte	0
	.byte	-1
	.byte	3
	.value	130
	.value	8
	.byte	6
	.byte	0
	.byte	-1
	.byte	3
	.value	130
	.value	9
	.byte	7
	.byte	0
	.byte	-1
	.byte	0
	.value	194
	.value	2
	.byte	7
	.byte	0
	.byte	-1
	.byte	1
	.value	194
	.value	3
	.byte	7
	.byte	0
	.byte	-1
	.byte	2
	.value	194
	.value	4
	.byte	7
	.byte	0
	.byte	-1
	.byte	3
	.value	194
	.value	5
	.byte	7
	.byte	0
	.byte	-1
	.byte	3
	.value	194
	.value	6
	.byte	7
	.byte	0
	.byte	-1
	.byte	3
	.value	194
	.value	7
	.byte	7
	.byte	0
	.byte	-1
	.byte	3
	.value	194
	.value	8
	.byte	7
	.byte	0
	.byte	-1
	.byte	3
	.value	194
	.value	9
	.byte	8
	.byte	0
	.byte	-1
	.byte	0
	.value	322
	.value	2
	.byte	8
	.byte	0
	.byte	-1
	.byte	1
	.value	322
	.value	3
	.byte	8
	.byte	0
	.byte	-1
	.byte	2
	.value	322
	.value	4
	.byte	8
	.byte	0
	.byte	-1
	.byte	3
	.value	322
	.value	5
	.byte	8
	.byte	0
	.byte	-1
	.byte	3
	.value	322
	.value	6
	.byte	8
	.byte	0
	.byte	-1
	.byte	3
	.value	322
	.value	7
	.byte	8
	.byte	0
	.byte	-1
	.byte	3
	.value	322
	.value	8
	.byte	8
	.byte	0
	.byte	-1
	.byte	3
	.value	322
	.value	9
	.byte	9
	.byte	0
	.byte	-1
	.byte	0
	.value	578
	.value	2
	.byte	9
	.byte	0
	.byte	-1
	.byte	1
	.value	578
	.value	3
	.byte	9
	.byte	0
	.byte	-1
	.byte	2
	.value	578
	.value	4
	.byte	9
	.byte	0
	.byte	-1
	.byte	3
	.value	578
	.value	5
	.byte	9
	.byte	0
	.byte	-1
	.byte	3
	.value	578
	.value	6
	.byte	9
	.byte	0
	.byte	-1
	.byte	3
	.value	578
	.value	7
	.byte	9
	.byte	0
	.byte	-1
	.byte	3
	.value	578
	.value	8
	.byte	9
	.byte	0
	.byte	-1
	.byte	3
	.value	578
	.value	9
	.byte	10
	.byte	0
	.byte	-1
	.byte	0
	.value	1090
	.value	2
	.byte	10
	.byte	0
	.byte	-1
	.byte	1
	.value	1090
	.value	3
	.byte	10
	.byte	0
	.byte	-1
	.byte	2
	.value	1090
	.value	4
	.byte	10
	.byte	0
	.byte	-1
	.byte	3
	.value	1090
	.value	5
	.byte	10
	.byte	0
	.byte	-1
	.byte	3
	.value	1090
	.value	6
	.byte	10
	.byte	0
	.byte	-1
	.byte	3
	.value	1090
	.value	7
	.byte	10
	.byte	0
	.byte	-1
	.byte	3
	.value	1090
	.value	8
	.byte	10
	.byte	0
	.byte	-1
	.byte	3
	.value	1090
	.value	9
	.byte	12
	.byte	0
	.byte	-1
	.byte	0
	.value	2114
	.value	2
	.byte	12
	.byte	0
	.byte	-1
	.byte	1
	.value	2114
	.value	3
	.byte	12
	.byte	0
	.byte	-1
	.byte	2
	.value	2114
	.value	4
	.byte	12
	.byte	0
	.byte	-1
	.byte	3
	.value	2114
	.value	5
	.byte	12
	.byte	0
	.byte	-1
	.byte	3
	.value	2114
	.value	6
	.byte	12
	.byte	0
	.byte	-1
	.byte	3
	.value	2114
	.value	7
	.byte	12
	.byte	0
	.byte	-1
	.byte	3
	.value	2114
	.value	8
	.byte	12
	.byte	0
	.byte	-1
	.byte	3
	.value	2114
	.value	9
	.byte	14
	.byte	0
	.byte	-1
	.byte	0
	.value	6210
	.value	2
	.byte	14
	.byte	0
	.byte	-1
	.byte	1
	.value	6210
	.value	3
	.byte	14
	.byte	0
	.byte	-1
	.byte	2
	.value	6210
	.value	4
	.byte	14
	.byte	0
	.byte	-1
	.byte	3
	.value	6210
	.value	5
	.byte	14
	.byte	0
	.byte	-1
	.byte	3
	.value	6210
	.value	6
	.byte	14
	.byte	0
	.byte	-1
	.byte	3
	.value	6210
	.value	7
	.byte	14
	.byte	0
	.byte	-1
	.byte	3
	.value	6210
	.value	8
	.byte	14
	.byte	0
	.byte	-1
	.byte	3
	.value	6210
	.value	9
	.byte	24
	.byte	0
	.byte	-1
	.byte	0
	.value	22594
	.value	2
	.byte	24
	.byte	0
	.byte	-1
	.byte	1
	.value	22594
	.value	3
	.byte	24
	.byte	0
	.byte	-1
	.byte	2
	.value	22594
	.value	4
	.byte	24
	.byte	0
	.byte	-1
	.byte	3
	.value	22594
	.value	5
	.byte	24
	.byte	0
	.byte	-1
	.byte	3
	.value	22594
	.value	6
	.byte	24
	.byte	0
	.byte	-1
	.byte	3
	.value	22594
	.value	7
	.byte	24
	.byte	0
	.byte	-1
	.byte	3
	.value	22594
	.value	8
	.byte	24
	.byte	0
	.byte	-1
	.byte	3
	.value	22594
	.value	9
	.byte	2
	.byte	5
	.byte	-1
	.byte	3
	.value	10
	.value	70
	.byte	2
	.byte	5
	.byte	-1
	.byte	3
	.value	10
	.value	102
	.byte	2
	.byte	6
	.byte	-1
	.byte	3
	.value	10
	.value	134
	.byte	2
	.byte	7
	.byte	-1
	.byte	3
	.value	10
	.value	198
	.byte	2
	.byte	8
	.byte	-1
	.byte	3
	.value	10
	.value	326
	.byte	2
	.byte	9
	.byte	-1
	.byte	3
	.value	10
	.value	582
	.byte	2
	.byte	10
	.byte	-1
	.byte	3
	.value	10
	.value	1094
	.byte	2
	.byte	24
	.byte	-1
	.byte	3
	.value	10
	.value	2118
	.byte	2
	.byte	5
	.byte	-1
	.byte	3
	.value	14
	.value	70
	.byte	2
	.byte	5
	.byte	-1
	.byte	3
	.value	14
	.value	102
	.byte	2
	.byte	6
	.byte	-1
	.byte	3
	.value	14
	.value	134
	.byte	2
	.byte	7
	.byte	-1
	.byte	3
	.value	14
	.value	198
	.byte	2
	.byte	8
	.byte	-1
	.byte	3
	.value	14
	.value	326
	.byte	2
	.byte	9
	.byte	-1
	.byte	3
	.value	14
	.value	582
	.byte	2
	.byte	10
	.byte	-1
	.byte	3
	.value	14
	.value	1094
	.byte	2
	.byte	24
	.byte	-1
	.byte	3
	.value	14
	.value	2118
	.byte	3
	.byte	5
	.byte	-1
	.byte	3
	.value	18
	.value	70
	.byte	3
	.byte	5
	.byte	-1
	.byte	3
	.value	18
	.value	102
	.byte	3
	.byte	6
	.byte	-1
	.byte	3
	.value	18
	.value	134
	.byte	3
	.byte	7
	.byte	-1
	.byte	3
	.value	18
	.value	198
	.byte	3
	.byte	8
	.byte	-1
	.byte	3
	.value	18
	.value	326
	.byte	3
	.byte	9
	.byte	-1
	.byte	3
	.value	18
	.value	582
	.byte	3
	.byte	10
	.byte	-1
	.byte	3
	.value	18
	.value	1094
	.byte	3
	.byte	24
	.byte	-1
	.byte	3
	.value	18
	.value	2118
	.byte	3
	.byte	5
	.byte	-1
	.byte	3
	.value	26
	.value	70
	.byte	3
	.byte	5
	.byte	-1
	.byte	3
	.value	26
	.value	102
	.byte	3
	.byte	6
	.byte	-1
	.byte	3
	.value	26
	.value	134
	.byte	3
	.byte	7
	.byte	-1
	.byte	3
	.value	26
	.value	198
	.byte	3
	.byte	8
	.byte	-1
	.byte	3
	.value	26
	.value	326
	.byte	3
	.byte	9
	.byte	-1
	.byte	3
	.value	26
	.value	582
	.byte	3
	.byte	10
	.byte	-1
	.byte	3
	.value	26
	.value	1094
	.byte	3
	.byte	24
	.byte	-1
	.byte	3
	.value	26
	.value	2118
	.byte	4
	.byte	5
	.byte	-1
	.byte	3
	.value	34
	.value	70
	.byte	4
	.byte	5
	.byte	-1
	.byte	3
	.value	34
	.value	102
	.byte	4
	.byte	6
	.byte	-1
	.byte	3
	.value	34
	.value	134
	.byte	4
	.byte	7
	.byte	-1
	.byte	3
	.value	34
	.value	198
	.byte	4
	.byte	8
	.byte	-1
	.byte	3
	.value	34
	.value	326
	.byte	4
	.byte	9
	.byte	-1
	.byte	3
	.value	34
	.value	582
	.byte	4
	.byte	10
	.byte	-1
	.byte	3
	.value	34
	.value	1094
	.byte	4
	.byte	24
	.byte	-1
	.byte	3
	.value	34
	.value	2118
	.byte	4
	.byte	5
	.byte	-1
	.byte	3
	.value	50
	.value	70
	.byte	4
	.byte	5
	.byte	-1
	.byte	3
	.value	50
	.value	102
	.byte	4
	.byte	6
	.byte	-1
	.byte	3
	.value	50
	.value	134
	.byte	4
	.byte	7
	.byte	-1
	.byte	3
	.value	50
	.value	198
	.byte	4
	.byte	8
	.byte	-1
	.byte	3
	.value	50
	.value	326
	.byte	4
	.byte	9
	.byte	-1
	.byte	3
	.value	50
	.value	582
	.byte	4
	.byte	10
	.byte	-1
	.byte	3
	.value	50
	.value	1094
	.byte	4
	.byte	24
	.byte	-1
	.byte	3
	.value	50
	.value	2118
	.byte	5
	.byte	5
	.byte	-1
	.byte	3
	.value	66
	.value	70
	.byte	5
	.byte	5
	.byte	-1
	.byte	3
	.value	66
	.value	102
	.byte	5
	.byte	6
	.byte	-1
	.byte	3
	.value	66
	.value	134
	.byte	5
	.byte	7
	.byte	-1
	.byte	3
	.value	66
	.value	198
	.byte	5
	.byte	8
	.byte	-1
	.byte	3
	.value	66
	.value	326
	.byte	5
	.byte	9
	.byte	-1
	.byte	3
	.value	66
	.value	582
	.byte	5
	.byte	10
	.byte	-1
	.byte	3
	.value	66
	.value	1094
	.byte	5
	.byte	24
	.byte	-1
	.byte	3
	.value	66
	.value	2118
	.byte	5
	.byte	5
	.byte	-1
	.byte	3
	.value	98
	.value	70
	.byte	5
	.byte	5
	.byte	-1
	.byte	3
	.value	98
	.value	102
	.byte	5
	.byte	6
	.byte	-1
	.byte	3
	.value	98
	.value	134
	.byte	5
	.byte	7
	.byte	-1
	.byte	3
	.value	98
	.value	198
	.byte	5
	.byte	8
	.byte	-1
	.byte	3
	.value	98
	.value	326
	.byte	5
	.byte	9
	.byte	-1
	.byte	3
	.value	98
	.value	582
	.byte	5
	.byte	10
	.byte	-1
	.byte	3
	.value	98
	.value	1094
	.byte	5
	.byte	24
	.byte	-1
	.byte	3
	.value	98
	.value	2118
	.byte	6
	.byte	1
	.byte	-1
	.byte	3
	.value	130
	.value	10
	.byte	6
	.byte	1
	.byte	-1
	.byte	3
	.value	130
	.value	12
	.byte	6
	.byte	2
	.byte	-1
	.byte	3
	.value	130
	.value	14
	.byte	6
	.byte	2
	.byte	-1
	.byte	3
	.value	130
	.value	18
	.byte	6
	.byte	3
	.byte	-1
	.byte	3
	.value	130
	.value	22
	.byte	6
	.byte	3
	.byte	-1
	.byte	3
	.value	130
	.value	30
	.byte	6
	.byte	4
	.byte	-1
	.byte	3
	.value	130
	.value	38
	.byte	6
	.byte	4
	.byte	-1
	.byte	3
	.value	130
	.value	54
	.byte	7
	.byte	1
	.byte	-1
	.byte	3
	.value	194
	.value	10
	.byte	7
	.byte	1
	.byte	-1
	.byte	3
	.value	194
	.value	12
	.byte	7
	.byte	2
	.byte	-1
	.byte	3
	.value	194
	.value	14
	.byte	7
	.byte	2
	.byte	-1
	.byte	3
	.value	194
	.value	18
	.byte	7
	.byte	3
	.byte	-1
	.byte	3
	.value	194
	.value	22
	.byte	7
	.byte	3
	.byte	-1
	.byte	3
	.value	194
	.value	30
	.byte	7
	.byte	4
	.byte	-1
	.byte	3
	.value	194
	.value	38
	.byte	7
	.byte	4
	.byte	-1
	.byte	3
	.value	194
	.value	54
	.byte	8
	.byte	1
	.byte	-1
	.byte	3
	.value	322
	.value	10
	.byte	8
	.byte	1
	.byte	-1
	.byte	3
	.value	322
	.value	12
	.byte	8
	.byte	2
	.byte	-1
	.byte	3
	.value	322
	.value	14
	.byte	8
	.byte	2
	.byte	-1
	.byte	3
	.value	322
	.value	18
	.byte	8
	.byte	3
	.byte	-1
	.byte	3
	.value	322
	.value	22
	.byte	8
	.byte	3
	.byte	-1
	.byte	3
	.value	322
	.value	30
	.byte	8
	.byte	4
	.byte	-1
	.byte	3
	.value	322
	.value	38
	.byte	8
	.byte	4
	.byte	-1
	.byte	3
	.value	322
	.value	54
	.byte	9
	.byte	1
	.byte	-1
	.byte	3
	.value	578
	.value	10
	.byte	9
	.byte	1
	.byte	-1
	.byte	3
	.value	578
	.value	12
	.byte	9
	.byte	2
	.byte	-1
	.byte	3
	.value	578
	.value	14
	.byte	9
	.byte	2
	.byte	-1
	.byte	3
	.value	578
	.value	18
	.byte	9
	.byte	3
	.byte	-1
	.byte	3
	.value	578
	.value	22
	.byte	9
	.byte	3
	.byte	-1
	.byte	3
	.value	578
	.value	30
	.byte	9
	.byte	4
	.byte	-1
	.byte	3
	.value	578
	.value	38
	.byte	9
	.byte	4
	.byte	-1
	.byte	3
	.value	578
	.value	54
	.byte	10
	.byte	1
	.byte	-1
	.byte	3
	.value	1090
	.value	10
	.byte	10
	.byte	1
	.byte	-1
	.byte	3
	.value	1090
	.value	12
	.byte	10
	.byte	2
	.byte	-1
	.byte	3
	.value	1090
	.value	14
	.byte	10
	.byte	2
	.byte	-1
	.byte	3
	.value	1090
	.value	18
	.byte	10
	.byte	3
	.byte	-1
	.byte	3
	.value	1090
	.value	22
	.byte	10
	.byte	3
	.byte	-1
	.byte	3
	.value	1090
	.value	30
	.byte	10
	.byte	4
	.byte	-1
	.byte	3
	.value	1090
	.value	38
	.byte	10
	.byte	4
	.byte	-1
	.byte	3
	.value	1090
	.value	54
	.byte	12
	.byte	1
	.byte	-1
	.byte	3
	.value	2114
	.value	10
	.byte	12
	.byte	1
	.byte	-1
	.byte	3
	.value	2114
	.value	12
	.byte	12
	.byte	2
	.byte	-1
	.byte	3
	.value	2114
	.value	14
	.byte	12
	.byte	2
	.byte	-1
	.byte	3
	.value	2114
	.value	18
	.byte	12
	.byte	3
	.byte	-1
	.byte	3
	.value	2114
	.value	22
	.byte	12
	.byte	3
	.byte	-1
	.byte	3
	.value	2114
	.value	30
	.byte	12
	.byte	4
	.byte	-1
	.byte	3
	.value	2114
	.value	38
	.byte	12
	.byte	4
	.byte	-1
	.byte	3
	.value	2114
	.value	54
	.byte	14
	.byte	1
	.byte	-1
	.byte	3
	.value	6210
	.value	10
	.byte	14
	.byte	1
	.byte	-1
	.byte	3
	.value	6210
	.value	12
	.byte	14
	.byte	2
	.byte	-1
	.byte	3
	.value	6210
	.value	14
	.byte	14
	.byte	2
	.byte	-1
	.byte	3
	.value	6210
	.value	18
	.byte	14
	.byte	3
	.byte	-1
	.byte	3
	.value	6210
	.value	22
	.byte	14
	.byte	3
	.byte	-1
	.byte	3
	.value	6210
	.value	30
	.byte	14
	.byte	4
	.byte	-1
	.byte	3
	.value	6210
	.value	38
	.byte	14
	.byte	4
	.byte	-1
	.byte	3
	.value	6210
	.value	54
	.byte	24
	.byte	1
	.byte	-1
	.byte	3
	.value	22594
	.value	10
	.byte	24
	.byte	1
	.byte	-1
	.byte	3
	.value	22594
	.value	12
	.byte	24
	.byte	2
	.byte	-1
	.byte	3
	.value	22594
	.value	14
	.byte	24
	.byte	2
	.byte	-1
	.byte	3
	.value	22594
	.value	18
	.byte	24
	.byte	3
	.byte	-1
	.byte	3
	.value	22594
	.value	22
	.byte	24
	.byte	3
	.byte	-1
	.byte	3
	.value	22594
	.value	30
	.byte	24
	.byte	4
	.byte	-1
	.byte	3
	.value	22594
	.value	38
	.byte	24
	.byte	4
	.byte	-1
	.byte	3
	.value	22594
	.value	54
	.byte	6
	.byte	5
	.byte	-1
	.byte	3
	.value	130
	.value	70
	.byte	6
	.byte	5
	.byte	-1
	.byte	3
	.value	130
	.value	102
	.byte	6
	.byte	6
	.byte	-1
	.byte	3
	.value	130
	.value	134
	.byte	6
	.byte	7
	.byte	-1
	.byte	3
	.value	130
	.value	198
	.byte	6
	.byte	8
	.byte	-1
	.byte	3
	.value	130
	.value	326
	.byte	6
	.byte	9
	.byte	-1
	.byte	3
	.value	130
	.value	582
	.byte	6
	.byte	10
	.byte	-1
	.byte	3
	.value	130
	.value	1094
	.byte	6
	.byte	24
	.byte	-1
	.byte	3
	.value	130
	.value	2118
	.byte	7
	.byte	5
	.byte	-1
	.byte	3
	.value	194
	.value	70
	.byte	7
	.byte	5
	.byte	-1
	.byte	3
	.value	194
	.value	102
	.byte	7
	.byte	6
	.byte	-1
	.byte	3
	.value	194
	.value	134
	.byte	7
	.byte	7
	.byte	-1
	.byte	3
	.value	194
	.value	198
	.byte	7
	.byte	8
	.byte	-1
	.byte	3
	.value	194
	.value	326
	.byte	7
	.byte	9
	.byte	-1
	.byte	3
	.value	194
	.value	582
	.byte	7
	.byte	10
	.byte	-1
	.byte	3
	.value	194
	.value	1094
	.byte	7
	.byte	24
	.byte	-1
	.byte	3
	.value	194
	.value	2118
	.byte	8
	.byte	5
	.byte	-1
	.byte	3
	.value	322
	.value	70
	.byte	8
	.byte	5
	.byte	-1
	.byte	3
	.value	322
	.value	102
	.byte	8
	.byte	6
	.byte	-1
	.byte	3
	.value	322
	.value	134
	.byte	8
	.byte	7
	.byte	-1
	.byte	3
	.value	322
	.value	198
	.byte	8
	.byte	8
	.byte	-1
	.byte	3
	.value	322
	.value	326
	.byte	8
	.byte	9
	.byte	-1
	.byte	3
	.value	322
	.value	582
	.byte	8
	.byte	10
	.byte	-1
	.byte	3
	.value	322
	.value	1094
	.byte	8
	.byte	24
	.byte	-1
	.byte	3
	.value	322
	.value	2118
	.byte	9
	.byte	5
	.byte	-1
	.byte	3
	.value	578
	.value	70
	.byte	9
	.byte	5
	.byte	-1
	.byte	3
	.value	578
	.value	102
	.byte	9
	.byte	6
	.byte	-1
	.byte	3
	.value	578
	.value	134
	.byte	9
	.byte	7
	.byte	-1
	.byte	3
	.value	578
	.value	198
	.byte	9
	.byte	8
	.byte	-1
	.byte	3
	.value	578
	.value	326
	.byte	9
	.byte	9
	.byte	-1
	.byte	3
	.value	578
	.value	582
	.byte	9
	.byte	10
	.byte	-1
	.byte	3
	.value	578
	.value	1094
	.byte	9
	.byte	24
	.byte	-1
	.byte	3
	.value	578
	.value	2118
	.byte	10
	.byte	5
	.byte	-1
	.byte	3
	.value	1090
	.value	70
	.byte	10
	.byte	5
	.byte	-1
	.byte	3
	.value	1090
	.value	102
	.byte	10
	.byte	6
	.byte	-1
	.byte	3
	.value	1090
	.value	134
	.byte	10
	.byte	7
	.byte	-1
	.byte	3
	.value	1090
	.value	198
	.byte	10
	.byte	8
	.byte	-1
	.byte	3
	.value	1090
	.value	326
	.byte	10
	.byte	9
	.byte	-1
	.byte	3
	.value	1090
	.value	582
	.byte	10
	.byte	10
	.byte	-1
	.byte	3
	.value	1090
	.value	1094
	.byte	10
	.byte	24
	.byte	-1
	.byte	3
	.value	1090
	.value	2118
	.byte	12
	.byte	5
	.byte	-1
	.byte	3
	.value	2114
	.value	70
	.byte	12
	.byte	5
	.byte	-1
	.byte	3
	.value	2114
	.value	102
	.byte	12
	.byte	6
	.byte	-1
	.byte	3
	.value	2114
	.value	134
	.byte	12
	.byte	7
	.byte	-1
	.byte	3
	.value	2114
	.value	198
	.byte	12
	.byte	8
	.byte	-1
	.byte	3
	.value	2114
	.value	326
	.byte	12
	.byte	9
	.byte	-1
	.byte	3
	.value	2114
	.value	582
	.byte	12
	.byte	10
	.byte	-1
	.byte	3
	.value	2114
	.value	1094
	.byte	12
	.byte	24
	.byte	-1
	.byte	3
	.value	2114
	.value	2118
	.byte	14
	.byte	5
	.byte	-1
	.byte	3
	.value	6210
	.value	70
	.byte	14
	.byte	5
	.byte	-1
	.byte	3
	.value	6210
	.value	102
	.byte	14
	.byte	6
	.byte	-1
	.byte	3
	.value	6210
	.value	134
	.byte	14
	.byte	7
	.byte	-1
	.byte	3
	.value	6210
	.value	198
	.byte	14
	.byte	8
	.byte	-1
	.byte	3
	.value	6210
	.value	326
	.byte	14
	.byte	9
	.byte	-1
	.byte	3
	.value	6210
	.value	582
	.byte	14
	.byte	10
	.byte	-1
	.byte	3
	.value	6210
	.value	1094
	.byte	14
	.byte	24
	.byte	-1
	.byte	3
	.value	6210
	.value	2118
	.byte	24
	.byte	5
	.byte	-1
	.byte	3
	.value	22594
	.value	70
	.byte	24
	.byte	5
	.byte	-1
	.byte	3
	.value	22594
	.value	102
	.byte	24
	.byte	6
	.byte	-1
	.byte	3
	.value	22594
	.value	134
	.byte	24
	.byte	7
	.byte	-1
	.byte	3
	.value	22594
	.value	198
	.byte	24
	.byte	8
	.byte	-1
	.byte	3
	.value	22594
	.value	326
	.byte	24
	.byte	9
	.byte	-1
	.byte	3
	.value	22594
	.value	582
	.byte	24
	.byte	10
	.byte	-1
	.byte	3
	.value	22594
	.value	1094
	.byte	24
	.byte	24
	.byte	-1
	.byte	3
	.value	22594
	.value	2118
	.align 32
	.type	kBlockLengthPrefixCode, @object
	.size	kBlockLengthPrefixCode, 104
kBlockLengthPrefixCode:
	.value	1
	.byte	2
	.zero	1
	.value	5
	.byte	2
	.zero	1
	.value	9
	.byte	2
	.zero	1
	.value	13
	.byte	2
	.zero	1
	.value	17
	.byte	3
	.zero	1
	.value	25
	.byte	3
	.zero	1
	.value	33
	.byte	3
	.zero	1
	.value	41
	.byte	3
	.zero	1
	.value	49
	.byte	4
	.zero	1
	.value	65
	.byte	4
	.zero	1
	.value	81
	.byte	4
	.zero	1
	.value	97
	.byte	4
	.zero	1
	.value	113
	.byte	5
	.zero	1
	.value	145
	.byte	5
	.zero	1
	.value	177
	.byte	5
	.zero	1
	.value	209
	.byte	5
	.zero	1
	.value	241
	.byte	6
	.zero	1
	.value	305
	.byte	6
	.zero	1
	.value	369
	.byte	7
	.zero	1
	.value	497
	.byte	8
	.zero	1
	.value	753
	.byte	9
	.zero	1
	.value	1265
	.byte	10
	.zero	1
	.value	2289
	.byte	11
	.zero	1
	.value	4337
	.byte	12
	.zero	1
	.value	8433
	.byte	13
	.zero	1
	.value	16625
	.byte	24
	.zero	1
	.align 32
	.type	kBitMask, @object
	.size	kBitMask, 132
kBitMask:
	.long	0
	.long	1
	.long	3
	.long	7
	.long	15
	.long	31
	.long	63
	.long	127
	.long	255
	.long	511
	.long	1023
	.long	2047
	.long	4095
	.long	8191
	.long	16383
	.long	32767
	.long	65535
	.long	131071
	.long	262143
	.long	524287
	.long	1048575
	.long	2097151
	.long	4194303
	.long	8388607
	.long	16777215
	.long	33554431
	.long	67108863
	.long	134217727
	.long	268435455
	.long	536870911
	.long	1073741823
	.long	2147483647
	.long	-1
	.align 32
	.type	kContextLookup, @object
	.size	kContextLookup, 2048
kContextLookup:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	30
	.byte	30
	.byte	30
	.byte	30
	.byte	31
	.byte	31
	.byte	31
	.byte	31
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	33
	.byte	33
	.byte	33
	.byte	33
	.byte	34
	.byte	34
	.byte	34
	.byte	34
	.byte	35
	.byte	35
	.byte	35
	.byte	35
	.byte	36
	.byte	36
	.byte	36
	.byte	36
	.byte	37
	.byte	37
	.byte	37
	.byte	37
	.byte	38
	.byte	38
	.byte	38
	.byte	38
	.byte	39
	.byte	39
	.byte	39
	.byte	39
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	41
	.byte	41
	.byte	41
	.byte	41
	.byte	42
	.byte	42
	.byte	42
	.byte	42
	.byte	43
	.byte	43
	.byte	43
	.byte	43
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	46
	.byte	46
	.byte	46
	.byte	46
	.byte	47
	.byte	47
	.byte	47
	.byte	47
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	57
	.byte	57
	.byte	58
	.byte	58
	.byte	58
	.byte	58
	.byte	59
	.byte	59
	.byte	59
	.byte	59
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	61
	.byte	61
	.byte	61
	.byte	61
	.byte	62
	.byte	62
	.byte	62
	.byte	62
	.byte	63
	.byte	63
	.byte	63
	.byte	63
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	8
	.byte	12
	.byte	16
	.byte	12
	.byte	12
	.byte	20
	.byte	12
	.byte	16
	.byte	24
	.byte	28
	.byte	12
	.byte	12
	.byte	32
	.byte	12
	.byte	36
	.byte	12
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	32
	.byte	32
	.byte	24
	.byte	40
	.byte	28
	.byte	12
	.byte	12
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	24
	.byte	12
	.byte	28
	.byte	12
	.byte	12
	.byte	12
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	24
	.byte	12
	.byte	28
	.byte	12
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	56
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	50462976
	.long	117835012
	.long	185207048
	.long	252579084
	.align 16
.LC2:
	.long	269488144
	.long	269488144
	.long	269488144
	.long	269488144
	.align 16
.LC3:
	.long	67372036
	.long	67372036
	.long	67372036
	.long	67372036
	.align 16
.LC4:
	.long	-16
	.long	-15
	.long	-14
	.long	-13
	.align 16
.LC5:
	.long	-12
	.long	-11
	.long	-10
	.long	-9
	.align 16
.LC6:
	.long	-8
	.long	-7
	.long	-6
	.long	-5
	.align 16
.LC7:
	.long	-4
	.long	-3
	.long	-2
	.long	-1
	.hidden	BrotliDecoderStateCleanupAfterMetablock
	.hidden	BrotliDecoderHuffmanTreeGroupInit
	.hidden	BrotliDecoderStateMetablockBegin
	.hidden	BrotliDecoderStateCleanup
	.hidden	BrotliDecoderStateInit
	.hidden	BrotliBuildHuffmanTable
	.hidden	BrotliBuildCodeLengthsHuffmanTable
	.hidden	BrotliWarmupBitReader
	.hidden	BrotliBuildSimpleHuffmanTable
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
