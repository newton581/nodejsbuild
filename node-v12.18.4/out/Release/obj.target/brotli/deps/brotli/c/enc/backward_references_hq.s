	.file	"backward_references_hq.c"
	.text
	.p2align 4
	.type	ZopfliCostModelSetFromLiteralCosts, @function
ZopfliCostModelSetFromLiteralCosts:
.LFB289:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rcx, %rdx
	movq	%r9, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	2848(%r12), %r14
	movq	2832(%r12), %rbx
	movq	2816(%r12), %r13
	movq	%r14, %rsi
	leaq	4(%rbx), %r8
	call	BrotliEstimateBitCostsForLiterals
	movl	$0x00000000, (%rbx)
	testq	%r14, %r14
	je	.L7
	cmpq	$2, %r14
	jbe	.L17
	leaq	-3(%r14), %rax
	pxor	%xmm1, %xmm1
	shrq	%rax
	movaps	%xmm1, %xmm0
	leaq	2(%rax,%rax), %rdx
	xorl	%eax, %eax
.L5:
	addss	4(%rbx,%rax,4), %xmm0
	movaps	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm2, %xmm3
	movss	%xmm2, 4(%rbx,%rax,4)
	addq	$2, %rax
	subss	%xmm1, %xmm3
	movaps	%xmm2, %xmm1
	subss	%xmm3, %xmm0
	addss	(%rbx,%rax,4), %xmm0
	addss	%xmm0, %xmm1
	movaps	%xmm1, %xmm4
	movss	%xmm1, (%rbx,%rax,4)
	subss	%xmm2, %xmm4
	subss	%xmm4, %xmm0
	cmpq	%rdx, %rax
	jne	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$1, %rax
	addss	(%rbx,%rax,4), %xmm0
	movss	-4(%rbx,%rax,4), %xmm2
	movaps	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbx,%rax,4)
	subss	%xmm2, %xmm1
	subss	%xmm1, %xmm0
	cmpq	%rax, %r14
	ja	.L6
.L7:
	xorl	%ebx, %ebx
	leaq	44+kLog2Table(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L3:
	leal	11(%rbx), %eax
	cmpl	$255, %eax
	ja	.L8
	movss	(%r14,%rbx,4), %xmm0
	movss	%xmm0, (%r12,%rbx,4)
	addq	$1, %rbx
	cmpq	$704, %rbx
	jne	.L3
.L10:
	movl	2824(%r12), %edx
	xorl	%ebx, %ebx
	leaq	kLog2Table(%rip), %r14
	testl	%edx, %edx
	je	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	leal	20(%rbx), %eax
	cmpq	$255, %rax
	ja	.L12
	movss	(%r14,%rax,4), %xmm0
	movl	%edx, %eax
	movss	%xmm0, 0(%r13,%rbx,4)
	addq	$1, %rbx
	cmpq	%rax, %rbx
	jb	.L11
.L14:
	movl	$0x405d6754, 2840(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	call	log2@PLT
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%r12,%rbx,4)
	addq	$1, %rbx
	cmpq	$704, %rbx
	jne	.L3
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	call	log2@PLT
	movl	2824(%r12), %eax
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 0(%r13,%rbx,4)
	addq	$1, %rbx
	movq	%rax, %rdx
	cmpq	%rbx, %rax
	ja	.L11
	jmp	.L14
.L17:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	jmp	.L6
	.cfi_endproc
.LFE289:
	.size	ZopfliCostModelSetFromLiteralCosts, .-ZopfliCostModelSetFromLiteralCosts
	.p2align 4
	.type	EvaluateNode.isra.0.constprop.0, @function
EvaluateNode.isra.0.constprop.0:
.LFB317:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	(%r8,%rsi,4), %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	salq	$4, %rax
	addq	%r10, %rax
	movss	12(%rax), %xmm0
	testq	%rsi, %rsi
	je	.L26
	movq	%rdx, %r11
	movl	(%rax), %edx
	movl	4(%rax), %r14d
	addq	%rsi, %rdi
	movl	%edx, %ebx
	movq	%r14, %r13
	movl	8(%rax), %edx
	andl	$33554431, %ebx
	leaq	(%rbx,%r14), %r15
	cmpq	%rdi, %r15
	ja	.L27
	cmpq	%r14, %r11
	jb	.L27
	movl	%edx, %r11d
	addl	$15, %r13d
	shrl	$27, %r11d
	leal	-1(%r11), %edi
	testl	%r11d, %r11d
	cmovne	%edi, %r13d
	testl	%r13d, %r13d
	jne	.L104
.L27:
	andl	$134217727, %edx
	addq	%rbx, %rdx
	movq	%rsi, %rbx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
.L30:
	movl	%edx, 12(%rax)
	movss	(%r12), %xmm1
	subss	(%r8), %xmm1
	comiss	%xmm0, %xmm1
	jnb	.L105
.L25:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movss	%xmm0, -68(%rbp)
	subss	%xmm1, %xmm0
	movq	%rsi, -96(%rbp)
	movss	%xmm0, -72(%rbp)
	testq	%rdx, %rdx
	je	.L33
	movq	%rdx, %rdi
	salq	$4, %rdi
	addq	%r10, %rdi
	movl	8(%rdi), %eax
	movl	%eax, %esi
	movl	(%rdi), %eax
	movl	4(%rdi), %edi
	andl	$134217727, %esi
	andl	$33554431, %eax
	movl	%edi, -88(%rbp)
	addq	%rsi, %rax
	subq	%rax, %rdx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %eax
	testq	%rax, %rax
	je	.L62
	movq	%rax, %rdi
	salq	$4, %rdi
	addq	%r10, %rdi
	movl	8(%rdi), %edx
	movl	%edx, %esi
	movl	(%rdi), %edx
	movl	4(%rdi), %edi
	andl	$134217727, %esi
	andl	$33554431, %edx
	movl	%edi, -84(%rbp)
	addq	%rsi, %rdx
	subq	%rdx, %rax
	salq	$4, %rax
	movl	12(%r10,%rax), %eax
	testq	%rax, %rax
	je	.L63
	movq	%rax, %rdi
	salq	$4, %rdi
	addq	%r10, %rdi
	movl	8(%rdi), %edx
	movl	%edx, %esi
	movl	(%rdi), %edx
	movl	4(%rdi), %edi
	andl	$134217727, %esi
	andl	$33554431, %edx
	movl	%edi, -80(%rbp)
	addq	%rsi, %rdx
	subq	%rdx, %rax
	salq	$4, %rax
	movl	12(%r10,%rax), %eax
	testq	%rax, %rax
	je	.L64
	salq	$4, %rax
	movl	4(%r10,%rax), %eax
	movl	%eax, -76(%rbp)
.L35:
	movq	256(%r9), %rax
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	1(%rax), %rsi
	notq	%rax
	andl	$7, %eax
	movq	%rsi, 256(%r9)
	movq	%rax, %rcx
	salq	$5, %rcx
	addq	%r9, %rcx
	movaps	%xmm4, (%rcx)
	movaps	%xmm5, 16(%rcx)
	cmpq	$1, %rsi
	jbe	.L25
	leaq	1(%rax), %rdx
	movss	24(%rcx), %xmm1
	andl	$7, %edx
	salq	$5, %rdx
	addq	%r9, %rdx
	movss	24(%rdx), %xmm0
	comiss	%xmm0, %xmm1
	jbe	.L45
	movdqa	(%rdx), %xmm6
	movd	8(%rcx), %xmm0
	movl	12(%rcx), %r8d
	movl	20(%rcx), %r10d
	movq	(%rcx), %r11
	movaps	%xmm6, (%rcx)
	movdqa	16(%rdx), %xmm7
	movd	16(%rcx), %xmm2
	movq	24(%rcx), %rdi
	movd	%r10d, %xmm6
	movaps	%xmm7, 16(%rcx)
	movd	%r8d, %xmm7
	punpckldq	%xmm6, %xmm2
	punpckldq	%xmm7, %xmm0
	movq	%r11, (%rdx)
	movq	%rdi, 24(%rdx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rdx)
	movaps	%xmm1, %xmm0
.L45:
	cmpq	$2, %rsi
	jbe	.L25
	leaq	2(%rax), %rcx
	andl	$7, %ecx
	salq	$5, %rcx
	addq	%r9, %rcx
	movss	24(%rcx), %xmm1
	comiss	%xmm1, %xmm0
	jbe	.L47
	movdqa	(%rcx), %xmm6
	movl	12(%rdx), %edi
	movl	20(%rdx), %r8d
	movd	8(%rdx), %xmm1
	movd	16(%rdx), %xmm2
	movq	(%rdx), %r10
	movaps	%xmm6, (%rdx)
	movd	%edi, %xmm5
	movdqa	16(%rcx), %xmm7
	movd	%r8d, %xmm4
	movss	28(%rdx), %xmm3
	punpckldq	%xmm5, %xmm1
	punpckldq	%xmm4, %xmm2
	movaps	%xmm7, 16(%rdx)
	punpcklqdq	%xmm2, %xmm1
	movq	%r10, (%rcx)
	movups	%xmm1, 8(%rcx)
	movaps	%xmm0, %xmm1
	movss	%xmm0, 24(%rcx)
	movss	%xmm3, 28(%rcx)
.L47:
	cmpq	$3, %rsi
	je	.L25
	leaq	3(%rax), %rdx
	andl	$7, %edx
	salq	$5, %rdx
	addq	%r9, %rdx
	movss	24(%rdx), %xmm0
	comiss	%xmm0, %xmm1
	jbe	.L49
	movdqa	(%rdx), %xmm6
	movl	12(%rcx), %edi
	movl	20(%rcx), %r8d
	movd	8(%rcx), %xmm0
	movd	16(%rcx), %xmm2
	movq	(%rcx), %r10
	movaps	%xmm6, (%rcx)
	movd	%edi, %xmm5
	movdqa	16(%rdx), %xmm7
	movd	%r8d, %xmm4
	movss	28(%rcx), %xmm3
	punpckldq	%xmm5, %xmm0
	punpckldq	%xmm4, %xmm2
	movaps	%xmm7, 16(%rcx)
	punpcklqdq	%xmm2, %xmm0
	movq	%r10, (%rdx)
	movups	%xmm0, 8(%rdx)
	movaps	%xmm1, %xmm0
	movss	%xmm1, 24(%rdx)
	movss	%xmm3, 28(%rdx)
.L49:
	cmpq	$4, %rsi
	je	.L25
	leaq	4(%rax), %rcx
	andl	$7, %ecx
	salq	$5, %rcx
	addq	%r9, %rcx
	movss	24(%rcx), %xmm1
	comiss	%xmm1, %xmm0
	jbe	.L51
	movdqa	(%rcx), %xmm6
	movl	12(%rdx), %edi
	movl	20(%rdx), %r8d
	movd	8(%rdx), %xmm1
	movd	16(%rdx), %xmm2
	movq	(%rdx), %r10
	movaps	%xmm6, (%rdx)
	movd	%edi, %xmm5
	movdqa	16(%rcx), %xmm7
	movd	%r8d, %xmm4
	movss	28(%rdx), %xmm3
	punpckldq	%xmm5, %xmm1
	punpckldq	%xmm4, %xmm2
	movaps	%xmm7, 16(%rdx)
	punpcklqdq	%xmm2, %xmm1
	movq	%r10, (%rcx)
	movups	%xmm1, 8(%rcx)
	movaps	%xmm0, %xmm1
	movss	%xmm0, 24(%rcx)
	movss	%xmm3, 28(%rcx)
.L51:
	cmpq	$5, %rsi
	je	.L25
	leaq	5(%rax), %rdx
	andl	$7, %edx
	salq	$5, %rdx
	addq	%r9, %rdx
	movss	24(%rdx), %xmm0
	comiss	%xmm0, %xmm1
	jbe	.L53
	movdqa	(%rdx), %xmm6
	movl	12(%rcx), %edi
	movl	20(%rcx), %r8d
	movd	8(%rcx), %xmm0
	movd	16(%rcx), %xmm2
	movq	(%rcx), %r10
	movaps	%xmm6, (%rcx)
	movd	%edi, %xmm5
	movdqa	16(%rdx), %xmm7
	movd	%r8d, %xmm4
	movss	28(%rcx), %xmm3
	punpckldq	%xmm5, %xmm0
	punpckldq	%xmm4, %xmm2
	movaps	%xmm7, 16(%rcx)
	punpcklqdq	%xmm2, %xmm0
	movq	%r10, (%rdx)
	movups	%xmm0, 8(%rdx)
	movaps	%xmm1, %xmm0
	movss	%xmm1, 24(%rdx)
	movss	%xmm3, 28(%rdx)
.L53:
	cmpq	$6, %rsi
	je	.L25
	leaq	6(%rax), %rcx
	andl	$7, %ecx
	salq	$5, %rcx
	addq	%r9, %rcx
	movss	24(%rcx), %xmm1
	comiss	%xmm1, %xmm0
	jbe	.L55
	movdqa	(%rcx), %xmm6
	movl	12(%rdx), %edi
	movl	20(%rdx), %r8d
	movd	8(%rdx), %xmm1
	movd	16(%rdx), %xmm2
	movq	(%rdx), %r10
	movaps	%xmm6, (%rdx)
	movd	%edi, %xmm5
	movdqa	16(%rcx), %xmm7
	movd	%r8d, %xmm4
	movss	28(%rdx), %xmm3
	punpckldq	%xmm5, %xmm1
	punpckldq	%xmm4, %xmm2
	movaps	%xmm7, 16(%rdx)
	punpcklqdq	%xmm2, %xmm1
	movq	%r10, (%rcx)
	movups	%xmm1, 8(%rcx)
	movaps	%xmm0, %xmm1
	movss	%xmm0, 24(%rcx)
	movss	%xmm3, 28(%rcx)
.L55:
	cmpq	$7, %rsi
	jbe	.L25
	addq	$7, %rax
	andl	$7, %eax
	salq	$5, %rax
	addq	%rax, %r9
	comiss	24(%r9), %xmm1
	jbe	.L25
	movl	12(%rcx), %eax
	movl	20(%rcx), %edx
	movd	8(%rcx), %xmm0
	movd	16(%rcx), %xmm2
	movdqa	(%r9), %xmm6
	movd	%edx, %xmm4
	movd	%eax, %xmm5
	movq	(%rcx), %rsi
	movss	28(%rcx), %xmm3
	punpckldq	%xmm4, %xmm2
	punpckldq	%xmm5, %xmm0
	movaps	%xmm6, (%rcx)
	movdqa	16(%r9), %xmm7
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm7, 16(%rcx)
	movq	%rsi, (%r9)
	movups	%xmm0, 8(%r9)
	movss	%xmm1, 24(%r9)
	movss	%xmm3, 28(%r9)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$0, 12(%rax)
	movss	(%r12), %xmm1
	subss	(%r8), %xmm1
	comiss	%xmm0, %xmm1
	jb	.L25
	movss	%xmm0, -68(%rbp)
	subss	%xmm1, %xmm0
	movq	$0, -96(%rbp)
	movss	%xmm0, -72(%rbp)
.L33:
	xorl	%edx, %edx
.L36:
	movslq	%edx, %rax
	leaq	-88(%rbp,%rax,4), %rsi
	movl	$3, %eax
	subl	%edx, %eax
	leaq	4(,%rax,4), %rdx
	movl	%edx, %eax
	cmpl	$8, %edx
	jnb	.L39
	andl	$4, %edx
	jne	.L107
	testl	%eax, %eax
	je	.L35
	movzbl	(%rcx), %edx
	movb	%dl, (%rsi)
	testb	$2, %al
	je	.L35
	movl	%eax, %edx
	movzwl	-2(%rcx,%rdx), %eax
	movw	%ax, -2(%rsi,%rdx)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L104:
	movl	%esi, %edx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%rcx), %rax
	movq	%rax, (%rsi)
	movl	%edx, %eax
	movq	-8(%rcx,%rax), %rdi
	movq	%rdi, -8(%rsi,%rax)
	leaq	8(%rsi), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rsi
	leal	(%rdx,%rsi), %eax
	subq	%rsi, %rcx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L35
	andl	$-8, %eax
	xorl	%edx, %edx
.L43:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rcx,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %edx
	jb	.L43
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %edx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$2, %edx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$3, %edx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L107:
	movl	(%rcx), %edx
	movl	%edx, (%rsi)
	movl	%eax, %edx
	movl	-4(%rcx,%rdx), %eax
	movl	%eax, -4(%rsi,%rdx)
	jmp	.L35
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE317:
	.size	EvaluateNode.isra.0.constprop.0, .-EvaluateNode.isra.0.constprop.0
	.p2align 4
	.type	UpdateNodes, @function
UpdateNodes:
.LFB303:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	leaq	(%rsi,%r14), %rax
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	andq	%r8, %rsi
	subq	$304, %rsp
	movq	%rdx, -136(%rbp)
	movq	16(%rbp), %rdx
	movq	%rax, -280(%rbp)
	cmpq	%rdx, %rax
	movq	%rcx, -248(%rbp)
	movl	$150, %ecx
	cmova	%rdx, %rax
	movq	%r8, -296(%rbp)
	movq	%r9, -328(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	cmpl	$11, 4(%r9)
	movq	56(%rbp), %r9
	pushq	64(%rbp)
	movq	%rax, -104(%rbp)
	movl	$325, %eax
	setge	%r12b
	cmovl	%rcx, %rax
	movq	24(%rbp), %rcx
	movq	%rsi, -120(%rbp)
	movq	%r14, %rsi
	movzbl	%r12b, %r12d
	movq	%rax, -240(%rbp)
	movq	48(%rbp), %rax
	leaq	1(,%r12,4), %r12
	movq	2832(%rax), %r8
	call	EvaluateNode.isra.0.constprop.0
	movq	56(%rbp), %rax
	movq	48(%rbp), %rsi
	popq	%r8
	popq	%r9
	movq	256(%rax), %rax
	movq	2832(%rsi), %rdi
	movq	%rax, -224(%rbp)
	negq	%rax
	leaq	(%rdi,%r14,4), %rcx
	movq	%rax, -128(%rbp)
	andl	$7, %eax
	movss	(%rcx), %xmm0
	salq	$5, %rax
	addq	56(%rbp), %rax
	movq	%rdi, -256(%rbp)
	movq	(%rax), %rdx
	movss	28(%rax), %xmm1
	movq	%rcx, -264(%rbp)
	movq	48(%rbp), %rax
	subss	(%rdi,%rdx,4), %xmm0
	addss	2840(%rax), %xmm1
	leaq	2(%r14), %rax
	addss	%xmm1, %xmm0
	cmpq	%rax, %rbx
	jb	.L237
	movl	$4, %ecx
	movl	$10, %edx
	movl	$2, %esi
	movq	-136(%rbp), %rax
	movss	.LC5(%rip), %xmm1
	salq	$4, %rax
	addq	64(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L115:
	comiss	44(%rax), %xmm0
	jb	.L238
.L116:
	addq	$1, %rsi
	cmpq	%rdx, %rsi
	je	.L239
	movq	-136(%rbp), %rdi
	addq	$16, %rax
	addq	%rsi, %rdi
	cmpq	%rdi, %rbx
	jnb	.L115
.L232:
	movq	%rsi, -312(%rbp)
	movq	%rsi, %rax
.L235:
	subq	$1, %rax
	movq	%rax, -216(%rbp)
.L111:
	movq	-120(%rbp), %rsi
	movq	-216(%rbp), %rax
	movss	.LC4(%rip), %xmm4
	addq	%rsi, %rax
	movq	%rax, -288(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, %rbx
	andl	$7, %eax
	shrq	$3, %rbx
	movq	%rax, -320(%rbp)
	movl	$8, %eax
	movq	%rbx, -304(%rbp)
	movq	-224(%rbp), %rbx
	cmpq	$8, %rbx
	cmovbe	%rbx, %rax
	subq	%rbx, %r12
	xorl	%r14d, %r14d
	movq	%r12, -272(%rbp)
	subq	%rbx, %rax
	movq	32(%rbp), %rbx
	movq	%rax, -232(%rbp)
	movq	40(%rbp), %rax
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, -160(%rbp)
	movq	-248(%rbp), %rax
	addq	%rsi, %rax
	movq	%rax, -208(%rbp)
	addq	$8, %rax
	movq	%rax, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L171:
	movq	-128(%rbp), %rbx
	cmpq	%rbx, -232(%rbp)
	je	.L108
	movq	%rbx, %r10
	andl	$7, %r10d
	movq	%r10, %rdx
	salq	$5, %rdx
	addq	56(%rbp), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rbx
	movq	%rax, -72(%rbp)
	movq	-136(%rbp), %rax
	subq	%rbx, %rax
	cmpq	$5, %rax
	jbe	.L240
	cmpq	$129, %rax
	jbe	.L241
	cmpq	$2113, %rax
	jbe	.L242
	cmpq	$6209, %rax
	jbe	.L179
	cmpq	$22593, %rax
	jbe	.L180
	movl	$23, %edi
	movss	.LC4(%rip), %xmm0
	movw	%di, -60(%rbp)
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-264(%rbp), %rax
	movq	56(%rbp), %rbx
	movq	-216(%rbp), %rdi
	movss	(%rax), %xmm3
	movq	-256(%rbp), %rax
	subss	(%rax), %xmm3
	movq	%r10, %rax
	salq	$5, %rax
	addss	24(%rbx,%rax), %xmm0
	addss	%xmm0, %xmm3
	cmpq	%rdi, -104(%rbp)
	jbe	.L127
	movq	-280(%rbp), %r11
	movslq	8(%rdx), %rax
	movq	-248(%rbp), %r15
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r9
	movq	%r11, %rcx
	movzbl	(%r15,%rbx), %r8d
	subq	%rax, %rcx
	cmpq	%rbx, %r9
	jb	.L127
	movzwl	-60(%rbp), %esi
	leal	0(,%rsi,8), %edx
	shrw	$3, %si
	movl	%edx, %ebx
	movzwl	%si, %edx
	xorl	%esi, %esi
	andl	$56, %ebx
	movw	%bx, -80(%rbp)
	orl	$7, %ebx
	movw	%bx, -64(%rbp)
	leal	(%rdx,%rdx,2), %ebx
	movq	%rdi, %rdx
	movl	%ebx, -88(%rbp)
	leaq	kDistanceCacheIndex(%rip), %rbx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	cmpq	$15, %rsi
	ja	.L127
	cmpq	-104(%rbp), %rdx
	jnb	.L127
	movl	(%rbx,%rsi,4), %eax
	movq	56(%rbp), %rdi
	leaq	(%rax,%r10,8), %rcx
	leaq	kDistanceCacheOffset(%rip), %rax
	movl	(%rax,%rsi,4), %eax
	addl	8(%rdi,%rcx,4), %eax
	movq	%r11, %rcx
	movq	-120(%rbp), %rdi
	cltq
	subq	%rax, %rcx
	addq	%rdx, %rdi
	movzbl	(%r15,%rdi), %r8d
	cmpq	%r9, %rdi
	ja	.L127
.L128:
	movq	%rsi, %rdi
	addq	$1, %rsi
	cmpq	%rax, -112(%rbp)
	jb	.L129
	cmpq	%rcx, %r11
	jbe	.L129
	andq	%r9, %rcx
	leaq	(%rcx,%rdx), %r12
	cmpq	%r9, %r12
	ja	.L129
	cmpb	%r8b, (%r15,%r12)
	jne	.L129
	leaq	(%r15,%rcx), %r13
	movq	-304(%rbp), %rcx
	movq	-208(%rbp), %r12
	testq	%rcx, %rcx
	je	.L130
	movq	%rcx, -56(%rbp)
	xorl	%r8d, %r8d
.L132:
	movq	-208(%rbp), %rcx
	movq	0(%r13,%r8), %r12
	movq	(%rcx,%r8), %rcx
	cmpq	%r12, %rcx
	je	.L243
	xorq	%r12, %rcx
	rep bsfq	%rcx, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%r8, %rcx
	movq	%rcx, -56(%rbp)
.L134:
	movq	48(%rbp), %rcx
	movq	-56(%rbp), %r13
	movq	2816(%rcx), %rcx
	movss	-4(%rcx,%rsi,4), %xmm2
	leaq	1(%rdx), %rcx
	addss	%xmm3, %xmm2
	cmpq	%r13, %rcx
	ja	.L129
	testq	%rdi, %rdi
	movl	-136(%rbp), %r8d
	movq	%r11, -96(%rbp)
	sete	%r12b
	cmpw	$7, -60(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r13, %r10
	setbe	%dil
	subl	-72(%rbp), %r8d
	movq	%rsi, -152(%rbp)
	andl	%edi, %r12d
	movl	%esi, %edi
	movq	%rax, -56(%rbp)
	movq	48(%rbp), %r11
	sall	$27, %edi
	movq	%r15, -168(%rbp)
	movl	-88(%rbp), %r15d
	orl	%edi, %r8d
	movq	%r9, -176(%rbp)
	movzwl	-80(%rbp), %r9d
	movl	%r8d, %edi
	movq	-136(%rbp), %r8
	movl	%edi, %esi
	leaq	1(%rdx,%r8), %r8
	movq	%rcx, %rdx
	salq	$4, %r8
	addq	64(%rbp), %r8
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L246:
	cmpw	$15, %cx
	ja	.L141
	movaps	%xmm3, %xmm0
	cmpw	$7, %cx
	jbe	.L143
	orl	$64, %edi
	.p2align 4,,10
	.p2align 3
.L143:
	movzwl	%di, %edi
	addss	%xmm1, %xmm0
	movss	12(%r8), %xmm1
	addss	(%r11,%rdi,4), %xmm0
	comiss	%xmm0, %xmm1
	jbe	.L144
	movl	-56(%rbp), %eax
	movl	%edx, %ecx
	movl	%esi, 8(%r8)
	orl	$301989888, %ecx
	cmpq	%rdx, %r14
	movss	%xmm0, 12(%r8)
	movl	%ecx, (%r8)
	cmovb	%rdx, %r14
	movl	%eax, 4(%r8)
.L144:
	leaq	1(%rdx), %rcx
	addq	$16, %r8
	cmpq	%r10, %rcx
	ja	.L244
	movq	%rcx, %rdx
.L146:
	leal	-2(%rdx), %ecx
	cmpq	$9, %rdx
	jbe	.L136
	cmpq	$133, %rdx
	jbe	.L245
	cmpq	$2117, %rdx
	ja	.L183
	leal	-70(%rdx), %ecx
	bsrl	%ecx, %ecx
	addl	$12, %ecx
.L136:
	movzwl	%cx, %r13d
	leaq	kCopyExtra(%rip), %rax
	pxor	%xmm1, %xmm1
	movl	%ecx, %edi
	movl	(%rax,%r13,4), %r13d
	andl	$7, %edi
	orl	%r9d, %edi
	cvtsi2ssq	%r13, %xmm1
	testb	%r12b, %r12b
	jne	.L246
.L141:
	shrw	$3, %cx
	movzwl	%cx, %r13d
.L138:
	addl	%r15d, %r13d
	movl	$5377344, %eax
	movaps	%xmm2, %xmm0
	leal	(%r13,%r13), %ecx
	sall	$6, %r13d
	shrl	%cl, %eax
	movl	%eax, %ecx
	andl	$192, %ecx
	leal	64(%rcx,%r13), %ecx
	orl	%ecx, %edi
	cmpw	$127, %di
	ja	.L143
	movaps	%xmm3, %xmm0
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-136(%rbp), %rdi
	leaq	(%rcx,%rsi), %rdx
	addss	%xmm1, %xmm0
	addq	%rcx, %rcx
	addq	$16, %rax
	addq	%rsi, %rdi
	cmpq	%rbx, %rdi
	ja	.L232
	comiss	44(%rax), %xmm0
	jnb	.L116
.L238:
	movq	%rsi, -312(%rbp)
	movq	%rsi, %rax
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L245:
	leal	-6(%rdx), %ecx
	leaq	-6(%rdx), %rdi
	bsrl	%ecx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdi
	leal	4(%rdi,%rcx,2), %ecx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L127:
	movq	-224(%rbp), %rax
	addq	-128(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L247
.L149:
	addq	$1, -128(%rbp)
	movq	-128(%rbp), %rax
	cmpq	%rax, -272(%rbp)
	jne	.L171
.L108:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movzwl	-64(%rbp), %edi
	movaps	%xmm4, %xmm1
	movl	$2, %r13d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-96(%rbp), %r11
	movq	-144(%rbp), %r10
	movq	-152(%rbp), %rsi
	movq	-168(%rbp), %r15
	movq	-176(%rbp), %r9
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L247:
	cmpq	$0, 32(%rbp)
	je	.L149
	movq	-328(%rbp), %rax
	movss	.LC4(%rip), %xmm5
	movq	48(%rbp), %r15
	movl	56(%rax), %esi
	movq	%rsi, %rbx
	movl	%esi, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movl	60(%rax), %esi
	leal	2(%rbx), %ecx
	leaq	16(%rsi), %rax
	movq	%rax, -152(%rbp)
	movl	$1, %eax
	salq	%cl, %rax
	movl	%ebx, %ecx
	subq	%rsi, %rax
	subq	$16, %rax
	movq	%rax, -184(%rbp)
	movl	$1, %eax
	sall	%cl, %eax
	subl	$1, %eax
	movq	%rax, -192(%rbp)
	leal	16(%rsi), %eax
	movw	%ax, -194(%rbp)
	movq	48(%rbp), %rax
	movq	2816(%rax), %rax
	movq	%rax, -144(%rbp)
	movzwl	-60(%rbp), %eax
	leal	0(,%rax,8), %r12d
	shrw	$3, %ax
	movzwl	%ax, %eax
	andl	$56, %r12d
	leal	(%rax,%rax,2), %r13d
	movq	40(%rbp), %rax
	movw	%r12w, -56(%rbp)
	movq	-112(%rbp), %r12
	movq	%rax, -80(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -88(%rbp)
	movl	-136(%rbp), %eax
	subl	-72(%rbp), %eax
	movl	%eax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L170:
	movq	-80(%rbp), %rax
	movl	(%rax), %r10d
	movl	4(%rax), %ebx
	movl	%r10d, -60(%rbp)
	leaq	15(%r10), %rsi
	cmpq	-152(%rbp), %rsi
	jb	.L151
	movq	-184(%rbp), %rax
	leaq	(%rax,%rsi), %rdx
	bsrl	%edx, %ecx
	movq	%rdx, %rsi
	andq	-192(%rbp), %rdx
	addw	-194(%rbp), %dx
	leal	-1(%rcx), %eax
	movq	%rax, %rcx
	subq	-176(%rbp), %rax
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	andl	$1, %ecx
	leaq	-2(%rcx,%rax,2), %rsi
	movzbl	-168(%rbp), %ecx
	sall	$10, %eax
	salq	%cl, %rsi
	addl	%edx, %esi
	orl	%eax, %esi
.L151:
	movq	%rsi, %rcx
	shrw	$10, %si
	pxor	%xmm2, %xmm2
	movl	%ebx, %r9d
	movzwl	%si, %esi
	movq	-144(%rbp), %rax
	andl	$1023, %ecx
	shrl	$5, %r9d
	cvtsi2ssl	%esi, %xmm2
	addss	%xmm3, %xmm2
	addss	(%rax,%rcx,4), %xmm2
	cmpq	-88(%rbp), %r9
	jbe	.L152
	cmpq	%r10, %r12
	jb	.L192
	cmpq	-240(%rbp), %r9
	ja	.L192
.L153:
	movq	-88(%rbp), %rsi
	movq	-136(%rbp), %rdi
	andl	$31, %ebx
	movl	$5377344, %r11d
	movl	%ebx, %eax
	movl	%ebx, -72(%rbp)
	addq	%rsi, %rdi
	movq	%rax, -96(%rbp)
	salq	$4, %rdi
	addq	64(%rbp), %rdi
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L249:
	movl	-72(%rbp), %eax
	movq	%r9, %r8
	testl	%eax, %eax
	je	.L156
	movq	-96(%rbp), %rax
	movq	%rax, %r8
	cmpq	$9, %rax
	jbe	.L174
.L175:
	leaq	-6(%r8), %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %ecx
.L234:
	movl	%ecx, %edx
	leaq	kCopyExtra(%rip), %rbx
	pxor	%xmm0, %xmm0
	movl	%ecx, %eax
	movzwl	%cx, %ecx
	shrw	$3, %ax
	andl	$7, %edx
	movl	(%rbx,%rcx,4), %ecx
	movzwl	%ax, %eax
	cvtsi2ssq	%rcx, %xmm0
.L161:
	addl	%r13d, %eax
	movl	%r11d, %ebx
	orw	-56(%rbp), %dx
	movss	12(%rdi), %xmm1
	leal	(%rax,%rax), %ecx
	sall	$6, %eax
	addss	%xmm2, %xmm0
	shrl	%cl, %ebx
	movl	%ebx, %ecx
	andl	$192, %ecx
	leal	64(%rcx,%rax), %eax
	orl	%eax, %edx
	movzwl	%dx, %edx
	addss	(%r15,%rdx,4), %xmm0
	comiss	%xmm0, %xmm1
	jbe	.L167
	leal	9(%rsi), %eax
	movss	%xmm0, 12(%rdi)
	subl	%r8d, %eax
	sall	$25, %eax
	orl	%esi, %eax
	cmpq	%rsi, %r14
	movl	%eax, (%rdi)
	movl	-60(%rbp), %eax
	cmovb	%rsi, %r14
	movl	%eax, 4(%rdi)
	movl	-64(%rbp), %eax
	movl	%eax, 8(%rdi)
.L167:
	addq	$1, %rsi
	addq	$16, %rdi
	cmpq	%r9, %rsi
	ja	.L248
.L169:
	cmpq	%r10, %r12
	jb	.L249
	movq	%rsi, %r8
.L156:
	cmpq	$9, %r8
	jbe	.L174
	cmpq	$133, %r8
	jbe	.L175
	cmpq	$2117, %r8
	ja	.L188
	leaq	-70(%r8), %rcx
	bsrl	%ecx, %ecx
	addl	$12, %ecx
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L174:
	leal	-2(%r8), %ecx
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L188:
	movaps	%xmm5, %xmm0
	movl	$2, %eax
	movl	$7, %edx
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L248:
	movq	-88(%rbp), %rbx
	movq	%r9, %rdx
	addq	$1, %r9
	movl	$0, %ecx
	leaq	1(%rbx), %rax
	subq	%rbx, %rdx
	cmpq	%r9, %rax
	cmova	%rcx, %rdx
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
.L155:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	-160(%rbp), %rax
	jne	.L170
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L152:
	jnb	.L153
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r9, -88(%rbp)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L240:
	movw	%ax, -60(%rbp)
	leaq	kInsExtra(%rip), %rcx
	cltq
	pxor	%xmm0, %xmm0
	movl	(%rcx,%rax,4), %eax
	cvtsi2ssq	%rax, %xmm0
	jmp	.L120
.L241:
	subq	$2, %rax
	bsrl	%eax, %ecx
	subl	$1, %ecx
	shrq	%cl, %rax
	leal	2(%rax,%rcx,2), %eax
.L233:
	movzwl	%ax, %ecx
	movw	%ax, -60(%rbp)
	leaq	kInsExtra(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	(%rax,%rcx,4), %eax
	cvtsi2ssq	%rax, %xmm0
	jmp	.L120
.L242:
	subq	$66, %rax
	bsrl	%eax, %eax
	addl	$10, %eax
	jmp	.L233
.L180:
	movl	$22, %ecx
	movss	.LC3(%rip), %xmm0
	movw	%cx, -60(%rbp)
	jmp	.L120
.L243:
	movq	-336(%rbp), %rcx
	leaq	(%rcx,%r8), %r12
	addq	$8, %r8
	subq	$1, -56(%rbp)
	jne	.L132
	cmpq	$0, -320(%rbp)
	jne	.L176
.L182:
	movq	%r8, -56(%rbp)
	jmp	.L134
.L130:
	xorl	%r8d, %r8d
	cmpq	$0, -320(%rbp)
	je	.L129
.L176:
	movzbl	(%r12), %ecx
	cmpb	%cl, 0(%r13,%r8)
	jne	.L182
	leaq	1(%r8), %rcx
	cmpq	$1, -320(%rbp)
	movq	%rcx, -56(%rbp)
	je	.L134
	movzbl	1(%r12), %ecx
	cmpb	%cl, 1(%r13,%r8)
	jne	.L134
	leaq	2(%r8), %rcx
	cmpq	$2, -320(%rbp)
	movq	%rcx, -56(%rbp)
	je	.L134
	movzbl	2(%r12), %ecx
	cmpb	%cl, 2(%r13,%r8)
	jne	.L134
	leaq	3(%r8), %rcx
	cmpq	$3, -320(%rbp)
	movq	%rcx, -56(%rbp)
	je	.L134
	movzbl	3(%r12), %ecx
	cmpb	%cl, 3(%r13,%r8)
	jne	.L134
	leaq	4(%r8), %rcx
	cmpq	$4, -320(%rbp)
	movq	%rcx, -56(%rbp)
	je	.L134
	movzbl	4(%r12), %ecx
	cmpb	%cl, 4(%r13,%r8)
	jne	.L134
	leaq	5(%r8), %rcx
	cmpq	$5, -320(%rbp)
	movq	%rcx, -56(%rbp)
	je	.L134
	movzbl	5(%r13,%r8), %ecx
	cmpb	%cl, 5(%r12)
	jne	.L134
	leaq	6(%r8), %rcx
	cmpq	$6, -320(%rbp)
	movq	%rcx, -56(%rbp)
	je	.L134
	movzbl	6(%r12), %ecx
	cmpb	%cl, 6(%r13,%r8)
	jne	.L134
	leaq	7(%r8), %rcx
	cmpq	$7, -320(%rbp)
	movq	%rcx, -56(%rbp)
	je	.L134
	movzbl	7(%r12), %ecx
	cmpb	%cl, 7(%r13,%r8)
	jne	.L134
	leaq	8(%r8), %rcx
	movq	%rcx, -56(%rbp)
	jmp	.L134
.L179:
	movl	$21, %esi
	movss	.LC2(%rip), %xmm0
	movw	%si, -60(%rbp)
	jmp	.L120
.L237:
	movq	$1, -216(%rbp)
	movq	$2, -312(%rbp)
	jmp	.L111
	.cfi_endproc
.LFE303:
	.size	UpdateNodes, .-UpdateNodes
	.p2align 4
	.globl	BrotliInitZopfliNodes
	.hidden	BrotliInitZopfliNodes
	.type	BrotliInitZopfliNodes, @function
BrotliInitZopfliNodes:
.LFB279:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L250
	salq	$4, %rsi
	movss	.LC6(%rip), %xmm0
	leaq	(%rdi,%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L252:
	movl	$1, (%rdi)
	addq	$16, %rdi
	movl	$0, -12(%rdi)
	movl	$0, -8(%rdi)
	movss	%xmm0, -4(%rdi)
	cmpq	%rax, %rdi
	jne	.L252
.L250:
	ret
	.cfi_endproc
.LFE279:
	.size	BrotliInitZopfliNodes, .-BrotliInitZopfliNodes
	.p2align 4
	.globl	BrotliZopfliCreateCommands
	.hidden	BrotliZopfliCreateCommands
	.type	BrotliZopfliCreateCommands, @function
BrotliZopfliCreateCommands:
.LFB305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%rsi, -104(%rbp)
	movl	8(%r9), %ecx
	movq	%rdx, -112(%rbp)
	movq	%r8, -128(%rbp)
	salq	%cl, %rax
	movq	%r9, -120(%rbp)
	subq	$16, %rax
	movq	%rax, -96(%rbp)
	movl	12(%rdx), %eax
	movl	%eax, -44(%rbp)
	cmpl	$-1, %eax
	je	.L279
	movq	$0, -64(%rbp)
	movq	16(%rbp), %r15
	xorl	%edi, %edi
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L262:
	leal	2(%r10), %ecx
	movl	$1, %edi
	movl	$1, %r11d
	salq	%cl, %rdi
	movq	%rdi, %rcx
	subq	%rsi, %rcx
	leaq	-16(%r13,%rcx), %rsi
	bsrl	%esi, %ebx
	movq	%rsi, %rdi
	leal	-1(%rbx), %r9d
	movl	%r9d, %ecx
	movq	%r9, %rbx
	shrq	%cl, %rdi
	movl	%r10d, %ecx
	subq	%rcx, %r9
	movl	%r10d, %ecx
	andl	$1, %edi
	sall	%cl, %r11d
	leal	-1(%r11), %r11d
	andq	%rsi, %r11
	leal	16(%rax,%r11), %r11d
	leaq	-2(%rdi,%r9,2), %rax
	addq	$2, %rdi
	sall	$10, %r9d
	salq	%cl, %rax
	movl	%ebx, %ecx
	salq	%cl, %rdi
	addl	%eax, %r11d
	movl	%r10d, %ecx
	subq	%rdi, %rsi
	orl	%r11d, %r9d
	shrq	%cl, %rsi
.L263:
	testw	$1023, %r9w
	movw	%r9w, 14(%r15)
	movslq	%r8d, %rax
	movl	%esi, 8(%r15)
	sete	%r9b
	cmpq	$5, %rdx
	jbe	.L299
	cmpq	$129, %rdx
	jbe	.L300
	cmpq	$2113, %rdx
	jbe	.L301
	cmpq	$6209, %rdx
	jbe	.L280
	cmpq	$22594, %rdx
	sbbl	%edi, %edi
	andl	$-8, %edi
	addl	$56, %edi
	cmpq	$22594, %rdx
	sbbl	%esi, %esi
	addl	$23, %esi
.L265:
	subl	$2, %r8d
	cmpq	$9, %rax
	jbe	.L269
	cmpq	$133, %rax
	jbe	.L302
	cmpq	$2117, %rax
	ja	.L271
	leaq	-70(%rax), %r8
	bsrl	%r8d, %r8d
	addl	$12, %r8d
.L269:
	movl	%r8d, %eax
	andl	$7, %eax
	orl	%eax, %edi
	cmpw	$7, %si
	ja	.L272
	testb	%r9b, %r9b
	je	.L272
	cmpw	$15, %r8w
	ja	.L272
	movl	%edi, %eax
	orl	$64, %eax
	cmpw	$7, %r8w
	cmova	%eax, %edi
.L275:
	movw	%di, 12(%r15)
	movq	-88(%rbp), %rbx
	cmpq	%rbx, -80(%rbp)
	ja	.L276
	testq	%r13, %r13
	je	.L276
	movl	8(%r14), %eax
	movl	%eax, 12(%r14)
	movl	4(%r14), %eax
	movl	%eax, 8(%r14)
	movl	(%r14), %eax
	movl	%r12d, (%r14)
	movl	%eax, 4(%r14)
.L276:
	movq	24(%rbp), %rax
	addq	$1, -64(%rbp)
	addq	$16, %r15
	movq	-72(%rbp), %rdi
	addq	-56(%rbp), %rdi
	addq	%rdx, (%rax)
	cmpl	$-1, -44(%rbp)
	je	.L258
.L278:
	movl	-44(%rbp), %r8d
	addq	%rdi, %r8
	salq	$4, %r8
	addq	-112(%rbp), %r8
	movl	(%r8), %eax
	movl	8(%r8), %ecx
	movl	%eax, %esi
	movl	%ecx, %r10d
	andl	$33554431, %esi
	andl	$134217727, %r10d
	cmpq	$0, -64(%rbp)
	movl	%esi, %edx
	movq	%rdx, -72(%rbp)
	movl	%r10d, %edx
	leaq	(%rdx,%rdi), %rbx
	movq	%rbx, -56(%rbp)
	movl	12(%r8), %ebx
	movl	%ebx, -44(%rbp)
	jne	.L259
	movq	-128(%rbp), %rbx
	addq	(%rbx), %rdx
	movq	$0, (%rbx)
	movl	%edx, %r10d
.L259:
	movl	4(%r8), %ebx
	shrl	$25, %eax
	leal	9(%rsi), %r8d
	movl	%r10d, (%r15)
	subl	%eax, %r8d
	movq	-56(%rbp), %rax
	addq	-104(%rbp), %rax
	movq	%rbx, %r12
	movq	%rbx, -80(%rbp)
	movq	-96(%rbp), %rbx
	cmpq	%rbx, %rax
	cmova	%rbx, %rax
	shrl	$27, %ecx
	leal	-1(%rcx), %r9d
	movq	%rax, -88(%rbp)
	leal	15(%r12), %eax
	cmove	%eax, %r9d
	movl	%r8d, %eax
	subl	%esi, %eax
	sall	$25, %eax
	movl	%r9d, %r13d
	orl	%eax, %esi
	movq	-120(%rbp), %rax
	movl	%esi, 4(%r15)
	movl	60(%rax), %esi
	movl	56(%rax), %r10d
	leaq	16(%rsi), %rcx
	movq	%rsi, %rax
	cmpq	%rcx, %r13
	jnb	.L262
	xorl	%esi, %esi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L272:
	shrw	$3, %r8w
	movzwl	%r8w, %r8d
.L274:
	shrw	$3, %si
	movzwl	%si, %esi
	leal	(%rsi,%rsi,2), %eax
	addl	%eax, %r8d
	movl	$5377344, %eax
	leal	(%r8,%r8), %ecx
	sall	$6, %r8d
	shrl	%cl, %eax
	andl	$192, %eax
	leal	64(%rax,%r8), %eax
	orl	%eax, %edi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L299:
	leal	0(,%rdx,8), %edi
	movl	%edx, %esi
	andl	$56, %edi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	-2(%rdx), %rsi
	bsrl	%esi, %ecx
	subl	$1, %ecx
	shrq	%cl, %rsi
	leal	2(%rsi,%rcx,2), %esi
	leal	0(,%rsi,8), %edi
	andl	$56, %edi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L302:
	subq	$6, %rax
	bsrl	%eax, %r8d
	leal	-1(%r8), %ecx
	shrq	%cl, %rax
	leal	4(%rax,%rcx,2), %r8d
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L271:
	orl	$7, %edi
	movl	$2, %r8d
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	-66(%rdx), %rsi
	bsrl	%esi, %esi
	addl	$10, %esi
	leal	0(,%rsi,8), %edi
	andl	$56, %edi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L279:
	xorl	%edi, %edi
.L258:
	movq	-136(%rbp), %rax
	subq	%rdi, %rax
	movq	%rax, %rdi
	movq	-128(%rbp), %rax
	addq	%rdi, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movl	$40, %edi
	movl	$21, %esi
	jmp	.L265
	.cfi_endproc
.LFE305:
	.size	BrotliZopfliCreateCommands, .-BrotliZopfliCreateCommands
	.p2align 4
	.globl	BrotliZopfliComputeShortestPath
	.hidden	BrotliZopfliComputeShortestPath
	.type	BrotliZopfliComputeShortestPath, @function
BrotliZopfliComputeShortestPath:
.LFB307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2552, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdi, -6680(%rbp)
	movq	32(%rbp), %rdi
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rax, -6624(%rbp)
	movq	24(%rbp), %rax
	movq	%rdx, -6608(%rbp)
	movl	8(%r9), %ecx
	movl	$150, %edx
	movq	%rsi, -6536(%rbp)
	movq	%r8, -6440(%rbp)
	movq	%r9, -6600(%rbp)
	movq	%rax, -6528(%rbp)
	movq	%rdi, -6584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	salq	%cl, %rax
	subq	$16, %rax
	cmpl	$11, 4(%r9)
	movq	%rax, -6616(%rbp)
	movl	$325, %eax
	cmovl	%rdx, %rax
	movq	%rax, -6664(%rbp)
	cmpq	$127, %rsi
	jbe	.L305
	leaq	-127(%rsi,%r14), %rax
	movl	$0, (%rdi)
	movl	$544, %ebx
	movq	%rax, -6672(%rbp)
	movl	$0x00000000, 12(%rdi)
	movl	64(%r9), %eax
	movq	%rsi, -3152(%rbp)
	cmpl	$544, %eax
	cmovbe	%eax, %ebx
	cmpq	$-2, %rsi
	jne	.L384
	movq	$0, -3168(%rbp)
	testl	%eax, %eax
	jne	.L385
	leaq	-6000(%rbp), %rax
	movq	%r8, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -6648(%rbp)
	movq	$0, -3184(%rbp)
	movl	$0, -3176(%rbp)
	call	ZopfliCostModelSetFromLiteralCosts
	movq	$0, -6016(%rbp)
.L308:
	movq	-6600(%rbp), %rax
	movq	$0, -6544(%rbp)
	addq	$80, %rax
	movq	%rax, -6640(%rbp)
	leaq	-3136(%rbp), %rax
	movq	%rax, -6592(%rbp)
	leaq	-6432(%rbp), %rax
	movq	%rax, -6656(%rbp)
	leaq	-6272(%rbp), %rax
	movq	%rax, -6632(%rbp)
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-6544(%rbp), %rdx
	movq	-6608(%rbp), %rax
	movl	$16, %edi
	movq	-6616(%rbp), %rbx
	movq	-6440(%rbp), %rcx
	addq	%rdx, %rax
	movq	-6536(%rbp), %r13
	cmpq	%rax, %rbx
	movq	%rax, %rsi
	movq	%rax, -6496(%rbp)
	cmovbe	%rbx, %rsi
	movq	%rcx, %rbx
	subq	%rdx, %r13
	movq	%rax, %rdx
	andq	%rax, %rbx
	movq	-6600(%rbp), %rax
	leaq	(%r15,%rbx), %r11
	movq	%rsi, -6504(%rbp)
	cmpl	$11, 4(%rax)
	movl	$64, %eax
	movq	%rbx, -6456(%rbp)
	cmovne	%rdi, %rax
	movq	%rdx, %rdi
	movq	%r11, -6480(%rbp)
	subq	%rax, %rdi
	cmpq	%rax, %rdx
	movl	$0, %eax
	cmovb	%rax, %rdi
	leaq	-1(%rdx), %rax
	cmpq	%rdi, %rax
	jbe	.L395
	testq	%rsi, %rsi
	je	.L395
	movq	%r13, %r12
	movq	%r13, %rbx
	movzbl	(%r11), %r8d
	leaq	1(%r11), %r9
	andl	$7, %r12d
	shrq	$3, %rbx
	addq	$8, %r11
	movq	%r13, -6464(%rbp)
	leaq	-1(%r12), %rdx
	movq	%r12, -6472(%rbp)
	movq	-6592(%rbp), %r14
	movl	$1, %r10d
	movq	%rdx, -6488(%rbp)
	movq	%rsi, %r12
	movq	%rcx, %r13
	movl	$1, %edx
	movq	%r9, -6520(%rbp)
	movq	%r11, -6512(%rbp)
	movq	%rbx, -6448(%rbp)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L315:
	subq	$1, %rax
	cmpq	%rdi, %rax
	jbe	.L516
.L533:
	cmpq	$2, %r10
	ja	.L516
	addq	$1, %rdx
	cmpq	%r12, %rdx
	ja	.L516
.L314:
	movq	%r13, %rsi
	andq	%rax, %rsi
	leaq	(%r15,%rsi), %rcx
	cmpb	(%rcx), %r8b
	jne	.L315
	movq	-6456(%rbp), %r11
	movzbl	1(%r15,%rsi), %esi
	cmpb	%sil, 1(%r15,%r11)
	jne	.L315
	movq	-6448(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L316
	xorl	%r9d, %r9d
.L318:
	movq	-6480(%rbp), %rsi
	movq	(%rcx,%r9), %r11
	movq	(%rsi,%r9), %rsi
	cmpq	%r11, %rsi
	je	.L532
	xorq	%r11, %rsi
	movq	%rbx, -6448(%rbp)
	rep bsfq	%rsi, %rsi
	movslq	%esi, %rsi
	shrq	$3, %rsi
	addq	%r9, %rsi
.L320:
	cmpq	%r10, %rsi
	jbe	.L315
	movl	%esi, %ecx
	subq	$1, %rax
	movl	%edx, (%r14)
	movq	%rsi, %r10
	sall	$5, %ecx
	addq	$8, %r14
	movl	%ecx, -4(%r14)
	cmpq	%rdi, %rax
	ja	.L533
	.p2align 4,,10
	.p2align 3
.L516:
	movq	-6464(%rbp), %r13
.L313:
	cmpq	%r10, %r13
	jbe	.L323
	cmpq	$128, %r13
	movl	$128, %eax
	movq	-6528(%rbp), %rsi
	cmovbe	%r13, %rax
	movq	%rax, -6520(%rbp)
	leaq	524344(%rsi), %rax
	movq	%rax, -6472(%rbp)
	movq	-6480(%rbp), %rax
	imull	$506832829, (%rax), %eax
	shrl	$15, %eax
	leaq	(%rsi,%rax,4), %rax
	movq	40(%rsi), %rsi
	movl	48(%rax), %ecx
	movq	%rsi, %rbx
	movq	%rsi, -6552(%rbp)
	movq	-6496(%rbp), %rsi
	movq	%rsi, %rdx
	andq	%rbx, %rdx
	addq	%rdx, %rdx
	movq	%rdx, -6568(%rbp)
	addq	$1, %rdx
	movq	%rdx, -6576(%rbp)
	cmpq	$127, %r13
	jbe	.L324
	movq	-6440(%rbp), %r11
	movl	%esi, 48(%rax)
	andq	%rcx, %r11
	subq	%rcx, %rsi
	movq	%rsi, -6488(%rbp)
	je	.L325
	cmpq	%rsi, -6504(%rbp)
	jb	.L325
.L326:
	xorl	%eax, %eax
	movq	%r13, -6448(%rbp)
	movl	$64, %ebx
	movq	$0, -6464(%rbp)
	movq	%rax, %r13
	movq	%r10, -6512(%rbp)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L536:
	cmpq	$127, -6448(%rbp)
	jbe	.L334
	movq	-6472(%rbp), %rsi
	movq	-6568(%rbp), %rdx
	movl	%ecx, (%rsi,%rdx,4)
.L334:
	andq	-6552(%rbp), %rcx
	movq	-6472(%rbp), %rsi
	movq	%rax, %r13
	leaq	1(%rcx,%rcx), %rdx
	movq	%rdx, -6568(%rbp)
	movl	4(%rsi,%rcx,8), %ecx
.L335:
	movq	-6440(%rbp), %r11
	movq	-6496(%rbp), %rax
	subq	$1, %rbx
	andq	%rcx, %r11
	subq	%rcx, %rax
	sete	%dl
	cmpq	-6504(%rbp), %rax
	movq	%rax, -6488(%rbp)
	seta	%al
	orb	%al, %dl
	jne	.L517
	testq	%rbx, %rbx
	je	.L517
.L338:
	movq	-6464(%rbp), %rax
	movq	-6448(%rbp), %r10
	cmpq	%r13, %rax
	movq	%rax, %rdi
	movq	-6456(%rbp), %rax
	cmova	%r13, %rdi
	subq	%rdi, %r10
	leaq	(%rax,%rdi), %r8
	leaq	(%rdi,%r11), %rsi
	movq	%r10, %rax
	addq	%r15, %rsi
	addq	%r15, %r8
	shrq	$3, %rax
	je	.L327
	leaq	8(%rsi), %rdx
	xorl	%r9d, %r9d
	movq	%rdx, -6560(%rbp)
.L329:
	movq	(%rsi,%r9), %rdx
	movq	(%r8,%r9), %r12
	cmpq	%r12, %rdx
	je	.L534
	xorq	%r12, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r9, %rax
.L330:
	addq	%rdi, %rax
	testq	%r14, %r14
	je	.L331
	cmpq	-6512(%rbp), %rax
	jbe	.L331
	movl	-6488(%rbp), %edx
	movq	%rax, -6512(%rbp)
	addq	$8, %r14
	movl	%edx, -8(%r14)
	movl	%eax, %edx
	sall	$5, %edx
	movl	%edx, -4(%r14)
.L331:
	cmpq	%rax, -6520(%rbp)
	jbe	.L535
	movq	-6480(%rbp), %rsi
	leaq	(%r15,%rax), %rdx
	movzbl	(%rdx,%r11), %edx
	cmpb	%dl, (%rsi,%rax)
	ja	.L536
	cmpq	$127, -6448(%rbp)
	jbe	.L336
	movq	-6472(%rbp), %rsi
	movq	-6576(%rbp), %rdx
	movl	%ecx, (%rsi,%rdx,4)
.L336:
	andq	-6552(%rbp), %rcx
	movq	-6472(%rbp), %rsi
	movq	%rax, -6464(%rbp)
	leaq	(%rcx,%rcx), %rdx
	movq	%rdx, -6576(%rbp)
	movl	(%rsi,%rcx,8), %ecx
	jmp	.L335
.L305:
	movq	-6600(%rbp), %rax
	movl	$0, (%rdi)
	movl	$0x00000000, 12(%rdi)
	movl	64(%rax), %ebx
	movl	$544, %eax
	cmpl	$544, %ebx
	cmova	%eax, %ebx
	movq	-6536(%rbp), %rax
	movq	%rax, -3152(%rbp)
	movq	-6608(%rbp), %rax
	movq	%rax, -6672(%rbp)
.L384:
	movq	-6536(%rbp), %rax
	movq	-6680(%rbp), %rdi
	leaq	8(,%rax,4), %rsi
	call	BrotliAllocate
	xorl	%edx, %edx
	movq	%rax, -3168(%rbp)
	movq	-6600(%rbp), %rax
	movl	64(%rax), %eax
	testl	%eax, %eax
	je	.L307
.L385:
	movq	-6680(%rbp), %rdi
	leaq	0(,%rax,4), %rsi
	call	BrotliAllocate
	movq	%rax, %rdx
.L307:
	leaq	-6000(%rbp), %rax
	movq	-6440(%rbp), %rcx
	movq	-6608(%rbp), %rsi
	movq	%rdx, -3184(%rbp)
	movq	%rax, %rdi
	movq	%r15, %rdx
	movl	%ebx, -3176(%rbp)
	movq	%rax, -6648(%rbp)
	call	ZopfliCostModelSetFromLiteralCosts
	cmpq	$3, -6536(%rbp)
	movq	$0, -6016(%rbp)
	ja	.L308
.L379:
	movq	-6680(%rbp), %rbx
	movq	-3168(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-3184(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -3168(%rbp)
	call	BrotliFree
	movq	-6536(%rbp), %rax
	movq	$0, -3184(%rbp)
	movq	-6536(%rbp), %rcx
	salq	$4, %rax
	addq	-6584(%rbp), %rax
	testl	$134217727, 8(%rax)
	jne	.L309
	movq	-6536(%rbp), %rdx
	movq	-6584(%rbp), %rsi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L380:
	subq	$1, %rdx
	subq	$16, %rax
	movq	%rdx, %rcx
	salq	$4, %rcx
	testl	$134217727, 8(%rsi,%rcx)
	jne	.L529
.L310:
	cmpl	$1, (%rax)
	je	.L380
.L529:
	movq	%rdx, -6536(%rbp)
	movq	%rdx, %rcx
.L309:
	movl	$-1, 12(%rax)
	xorl	%r8d, %r8d
	testq	%rcx, %rcx
	je	.L303
	movq	-6584(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%rcx, %rax
	addq	$1, %r8
	salq	$4, %rax
	addq	%rsi, %rax
	movl	(%rax), %edx
	movl	8(%rax), %eax
	andl	$33554431, %edx
	andl	$134217727, %eax
	addl	%eax, %edx
	subq	%rdx, %rcx
	movq	%rdx, %rax
	movq	%rcx, %rdx
	salq	$4, %rdx
	movl	%eax, 12(%rsi,%rdx)
	testq	%rcx, %rcx
	jne	.L382
.L303:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L537
	leaq	-40(%rbp), %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L517:
	.cfi_restore_state
	movq	-6448(%rbp), %r13
	movq	-6512(%rbp), %r10
	cmpq	$127, %r13
	jbe	.L323
.L325:
	movq	-6528(%rbp), %rax
	movq	-6568(%rbp), %rdx
	movq	-6472(%rbp), %rsi
	movl	524336(%rax), %eax
	movl	%eax, (%rsi,%rdx,4)
	movq	-6576(%rbp), %rdx
	movl	%eax, (%rsi,%rdx,4)
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	1(%r10), %rbx
	movq	-6656(%rbp), %r12
	movabsq	$1152921500580315135, %rax
	movdqa	.LC7(%rip), %xmm0
	cmpq	$4, %rbx
	movq	%rax, -6288(%rbp)
	movl	$4, %eax
	movq	%r13, %rcx
	cmovb	%rax, %rbx
	movq	-6480(%rbp), %rsi
	movq	%r12, %r8
	movq	-6640(%rbp), %rdi
	movaps	%xmm0, -6432(%rbp)
	movq	%rbx, %rdx
	movaps	%xmm0, -6416(%rbp)
	movaps	%xmm0, -6400(%rbp)
	movaps	%xmm0, -6384(%rbp)
	movaps	%xmm0, -6368(%rbp)
	movaps	%xmm0, -6352(%rbp)
	movaps	%xmm0, -6336(%rbp)
	movaps	%xmm0, -6320(%rbp)
	movaps	%xmm0, -6304(%rbp)
	call	BrotliFindAllStaticDictionaryMatches
	testl	%eax, %eax
	je	.L339
	cmpq	$37, %r13
	movl	$37, %ecx
	cmovbe	%r13, %rcx
	cmpq	%rcx, %rbx
	ja	.L339
	movq	-6504(%rbp), %rsi
	movq	-6600(%rbp), %r8
	movq	%r12, %rdi
	addq	$1, %rsi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%rax, %rbx
.L342:
	movl	(%rdi,%rbx,4), %eax
	cmpl	$268435454, %eax
	ja	.L340
	movl	%eax, %edx
	shrl	$5, %edx
	addq	%rsi, %rdx
	cmpq	72(%r8), %rdx
	ja	.L340
	andl	$31, %eax
	movl	%edx, (%r14)
	movl	%ebx, %edx
	sall	$5, %edx
	movl	%eax, %r9d
	orl	%edx, %eax
	cmpq	%rbx, %r9
	cmovne	%eax, %edx
	addq	$8, %r14
	movl	%edx, -4(%r14)
.L340:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %rcx
	jne	.L410
.L339:
	movq	-6592(%rbp), %rsi
	movq	%r14, %rbx
	subq	%rsi, %rbx
	sarq	$3, %rbx
	je	.L343
	leaq	-1(%rbx), %rdx
	movl	-3132(%rbp,%rdx,8), %eax
	shrl	$5, %eax
	cmpq	%rax, -6664(%rbp)
	jnb	.L344
	subq	$8, %rsp
	pushq	-6584(%rbp)
	movq	-3136(%rbp,%rdx,8), %rax
	movq	%r15, %rcx
	movq	-6600(%rbp), %r9
	pushq	-6632(%rbp)
	movq	-6440(%rbp), %r8
	pushq	-6648(%rbp)
	movq	-6544(%rbp), %rdx
	pushq	%rsi
	movq	-6536(%rbp), %rdi
	movq	-6608(%rbp), %rsi
	pushq	$1
	pushq	-6624(%rbp)
	pushq	-6616(%rbp)
	movq	%rax, -3136(%rbp)
	call	UpdateNodes
	addq	$64, %rsp
	movq	%rax, -6552(%rbp)
	cmpq	$16383, %rax
	ja	.L346
.L345:
	movl	-3132(%rbp), %eax
	shrl	$5, %eax
	cmpq	%rax, -6664(%rbp)
	jb	.L439
	movq	-6544(%rbp), %rbx
.L348:
	leaq	1(%rbx), %rax
	addq	$4, %rbx
	movq	%rax, -6544(%rbp)
	cmpq	-6536(%rbp), %rbx
	jb	.L378
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L534:
	movq	-6560(%rbp), %rdx
	addq	%r9, %rdx
	addq	$8, %r9
	subq	$1, %rax
	jne	.L329
	movq	%r9, %rax
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L327:
	andl	$7, %r10d
	je	.L330
	movzbl	(%r8,%rax), %edx
	cmpb	%dl, (%rsi)
	jne	.L330
	leaq	1(%rax), %rdx
	cmpq	$1, %r10
	je	.L409
	movzbl	1(%r8,%rax), %r9d
	cmpb	%r9b, 1(%rsi)
	jne	.L409
	leaq	2(%rax), %rdx
	cmpq	$2, %r10
	je	.L409
	movzbl	2(%r8,%rax), %r9d
	cmpb	%r9b, 2(%rsi)
	jne	.L409
	leaq	3(%rax), %rdx
	cmpq	$3, %r10
	je	.L409
	movzbl	3(%r8,%rax), %r9d
	cmpb	%r9b, 3(%rsi)
	jne	.L409
	leaq	4(%rax), %rdx
	cmpq	$4, %r10
	je	.L409
	movzbl	4(%r8,%rax), %r9d
	cmpb	%r9b, 4(%rsi)
	jne	.L409
	leaq	5(%rax), %rdx
	subq	$5, %r10
	je	.L409
	movzbl	5(%r8,%rax), %r9d
	cmpb	%r9b, 5(%rsi)
	jne	.L409
	leaq	6(%rax), %rdx
	cmpq	$1, %r10
	je	.L409
	movzbl	6(%r8,%rax), %r10d
	cmpb	%r10b, 6(%rsi)
	jne	.L409
	addq	$7, %rax
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L343:
	subq	$8, %rsp
	pushq	-6584(%rbp)
	movq	-6544(%rbp), %rbx
	movq	%r15, %rcx
	movq	-6600(%rbp), %r9
	pushq	-6632(%rbp)
	movq	-6440(%rbp), %r8
	pushq	-6648(%rbp)
	movq	%rbx, %rdx
	movq	-6608(%rbp), %rsi
	pushq	-6592(%rbp)
	movq	-6536(%rbp), %rdi
	pushq	$0
	pushq	-6624(%rbp)
	pushq	-6616(%rbp)
	call	UpdateNodes
	addq	$64, %rsp
	movq	%rax, -6552(%rbp)
	cmpq	$16383, %rax
	jbe	.L348
.L349:
	movq	-6496(%rbp), %rsi
	movq	-6552(%rbp), %rax
	movq	-6672(%rbp), %rdx
	addq	%rsi, %rax
	leaq	1(%rsi), %rcx
	cmpq	%rdx, %rax
	movq	%rcx, -6480(%rbp)
	cmova	%rdx, %rax
	movq	%rcx, -6472(%rbp)
	movq	%rax, -6560(%rbp)
	movq	%rax, %rdx
	leaq	64(%rsi), %rax
	cmpq	%rdx, %rax
	ja	.L350
	subq	$63, %rdx
	leaq	513(%rsi), %rax
	movq	%rdx, -6472(%rbp)
	cmpq	%rax, %rdx
	jb	.L350
	cmpq	%rdx, %rcx
	jnb	.L350
	movq	-6528(%rbp), %rax
	movq	40(%rax), %rsi
	leaq	524344(%rax), %r13
	movq	%rsi, -6488(%rbp)
	subq	$15, %rsi
	movq	%rsi, -6504(%rbp)
	.p2align 4,,10
	.p2align 3
.L362:
	movq	-6440(%rbp), %rbx
	movq	-6480(%rbp), %rdi
	movq	-6528(%rbp), %rsi
	movq	-6488(%rbp), %rdx
	movq	%rbx, %rcx
	andq	%rdi, %rcx
	andq	%rdi, %rdx
	imull	$506832829, (%r15,%rcx), %eax
	leaq	(%rdx,%rdx), %r14
	movq	%rcx, -6496(%rbp)
	movq	%r14, -6448(%rbp)
	addq	$1, %r14
	movq	%r14, -6456(%rbp)
	shrl	$15, %eax
	leaq	(%rsi,%rax,4), %rax
	movl	48(%rax), %esi
	movl	%edi, 48(%rax)
	movq	%rdi, %rax
	andq	%rsi, %rbx
	subq	%rsi, %rax
	movq	%rbx, %r11
	cmpq	-6504(%rbp), %rax
	ja	.L442
	movl	$64, %ebx
	addq	%r15, %rcx
	xorl	%r14d, %r14d
	movq	$0, -6464(%rbp)
	movq	%rcx, -6512(%rbp)
	movq	%rbx, %r12
	testq	%rax, %rax
	je	.L442
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-6464(%rbp), %rax
	movl	$128, %r10d
	cmpq	%r14, %rax
	movq	%rax, %rcx
	movq	-6496(%rbp), %rax
	cmova	%r14, %rcx
	subq	%rcx, %r10
	leaq	(%rax,%rcx), %r8
	leaq	(%rcx,%r11), %rdi
	movq	%r10, %rax
	addq	%r15, %rdi
	addq	%r15, %r8
	shrq	$3, %rax
	je	.L354
	leaq	8(%rdi), %rdx
	xorl	%r9d, %r9d
	movq	%rdx, -6520(%rbp)
.L356:
	movq	(%rdi,%r9), %rdx
	movq	(%r8,%r9), %rbx
	cmpq	%rbx, %rdx
	je	.L538
	xorq	%rbx, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r9, %rax
.L357:
	movq	-6448(%rbp), %rbx
	movq	-6488(%rbp), %rdi
	addq	%rcx, %rax
	leaq	0(%r13,%rbx,4), %rcx
	movq	-6456(%rbp), %rbx
	andq	%rsi, %rdi
	leaq	(%rdi,%rdi), %r8
	leaq	0(%r13,%rbx,4), %rdx
	cmpq	$127, %rax
	ja	.L539
	movq	-6512(%rbp), %rbx
	leaq	(%r15,%rax), %r9
	movzbl	(%r9,%r11), %r11d
	cmpb	%r11b, (%rbx,%rax)
	jbe	.L359
	movl	%esi, (%rcx)
	leaq	1(%r8), %rsi
	movq	-6480(%rbp), %rdi
	subq	$1, %r12
	leaq	0(%r13,%rsi,4), %rcx
	movq	%rsi, -6448(%rbp)
	movq	-6440(%rbp), %r11
	movl	(%rcx), %esi
	subq	%rsi, %rdi
	andq	%rsi, %r11
	cmpq	%rdi, -6504(%rbp)
	setb	%r8b
	testq	%rdi, %rdi
	sete	%dil
	orb	%dil, %r8b
	jne	.L360
	testq	%r12, %r12
	je	.L360
	movq	%rax, %r14
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L359:
	movl	%esi, (%rdx)
	leaq	0(%r13,%rdi,8), %rdx
	movq	-6440(%rbp), %r11
	subq	$1, %r12
	movl	(%rdx), %esi
	movq	-6480(%rbp), %rdi
	andq	%rsi, %r11
	subq	%rsi, %rdi
	sete	%r9b
	cmpq	%rdi, -6504(%rbp)
	setb	%dil
	orb	%dil, %r9b
	jne	.L360
	testq	%r12, %r12
	je	.L360
	movq	%rax, -6464(%rbp)
	movq	%r8, -6456(%rbp)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L538:
	movq	-6520(%rbp), %rdx
	addq	%r9, %rdx
	addq	$8, %r9
	subq	$1, %rax
	jne	.L356
	movq	%r9, %rax
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L354:
	andl	$7, %r10d
	je	.L357
	movzbl	(%r8,%rax), %edx
	cmpb	%dl, (%rdi)
	jne	.L357
	leaq	1(%rax), %rdx
	cmpq	$1, %r10
	je	.L424
	movzbl	1(%r8,%rax), %ebx
	cmpb	%bl, 1(%rdi)
	jne	.L424
	leaq	2(%rax), %rdx
	cmpq	$2, %r10
	je	.L424
	movzbl	2(%r8,%rax), %ebx
	cmpb	%bl, 2(%rdi)
	jne	.L424
	leaq	3(%rax), %rdx
	cmpq	$3, %r10
	je	.L424
	movzbl	3(%r8,%rax), %ebx
	cmpb	%bl, 3(%rdi)
	jne	.L424
	leaq	4(%rax), %rdx
	cmpq	$4, %r10
	je	.L424
	movzbl	4(%r8,%rax), %ebx
	cmpb	%bl, 4(%rdi)
	jne	.L424
	leaq	5(%rax), %rdx
	subq	$5, %r10
	je	.L424
	movzbl	5(%r8,%rax), %ebx
	cmpb	%bl, 5(%rdi)
	jne	.L424
	leaq	6(%rax), %rdx
	cmpq	$1, %r10
	je	.L424
	movzbl	6(%r8,%rax), %ebx
	cmpb	%bl, 6(%rdi)
	jne	.L424
	addq	$7, %rax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rdx, %rax
	jmp	.L357
.L442:
	movq	-6456(%rbp), %rax
	leaq	0(%r13,%rdx,8), %rcx
	leaq	0(%r13,%rax,4), %rdx
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-6528(%rbp), %rax
	movl	524336(%rax), %eax
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
.L353:
	addq	$8, -6480(%rbp)
	movq	-6480(%rbp), %rax
	cmpq	%rax, -6472(%rbp)
	ja	.L362
.L350:
	movq	-6560(%rbp), %rsi
	cmpq	%rsi, -6472(%rbp)
	jnb	.L377
	movq	-6528(%rbp), %rax
	movq	40(%rax), %rsi
	leaq	524344(%rax), %r13
	movq	%rsi, -6480(%rbp)
	subq	$15, %rsi
	movq	%rsi, -6496(%rbp)
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-6440(%rbp), %rdi
	movq	-6472(%rbp), %rbx
	movq	-6528(%rbp), %rsi
	movq	%rdi, %rcx
	andq	%rbx, %rcx
	imull	$506832829, (%r15,%rcx), %eax
	movq	%rcx, -6488(%rbp)
	shrl	$15, %eax
	leaq	(%rsi,%rax,4), %rdx
	movq	-6480(%rbp), %rax
	movl	48(%rdx), %esi
	movl	%ebx, 48(%rdx)
	andq	%rbx, %rax
	leaq	(%rax,%rax), %r14
	andq	%rsi, %rdi
	movq	%r14, -6448(%rbp)
	addq	$1, %r14
	subq	%rsi, %rbx
	movq	%rdi, %r11
	movq	%r14, -6456(%rbp)
	movq	%rbx, %rdx
	je	.L443
	leaq	(%r15,%rcx), %rbx
	xorl	%r14d, %r14d
	movl	$64, %r12d
	movq	$0, -6464(%rbp)
	movq	%rbx, -6504(%rbp)
	cmpq	%rdx, -6496(%rbp)
	jb	.L443
	.p2align 4,,10
	.p2align 3
.L365:
	movq	-6464(%rbp), %rax
	movl	$128, %r10d
	cmpq	%rax, %r14
	movq	%rax, %rcx
	movq	-6488(%rbp), %rax
	cmovbe	%r14, %rcx
	subq	%rcx, %r10
	leaq	(%rax,%rcx), %r8
	leaq	(%rcx,%r11), %rdi
	movq	%r10, %rax
	addq	%r15, %rdi
	addq	%r15, %r8
	shrq	$3, %rax
	je	.L368
	leaq	8(%rdi), %rbx
	xorl	%r9d, %r9d
	movq	%rbx, -6512(%rbp)
.L370:
	movq	(%rdi,%r9), %rdx
	movq	(%r8,%r9), %rbx
	cmpq	%rbx, %rdx
	je	.L540
	xorq	%rbx, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r9, %rax
.L371:
	movq	-6448(%rbp), %rbx
	movq	-6480(%rbp), %rdi
	addq	%rcx, %rax
	leaq	0(%r13,%rbx,4), %rcx
	movq	-6456(%rbp), %rbx
	andq	%rsi, %rdi
	leaq	(%rdi,%rdi), %r8
	leaq	0(%r13,%rbx,4), %rdx
	cmpq	$127, %rax
	ja	.L541
	movq	-6504(%rbp), %rbx
	leaq	(%r15,%rax), %r9
	movzbl	(%r9,%r11), %r10d
	cmpb	%r10b, (%rbx,%rax)
	jbe	.L373
	movl	%esi, (%rcx)
	leaq	1(%r8), %rsi
	movq	-6472(%rbp), %rdi
	subq	$1, %r12
	leaq	0(%r13,%rsi,4), %rcx
	movq	%rsi, -6448(%rbp)
	movq	-6440(%rbp), %r11
	movl	(%rcx), %esi
	subq	%rsi, %rdi
	andq	%rsi, %r11
	cmpq	%rdi, -6496(%rbp)
	setb	%r8b
	testq	%rdi, %rdi
	sete	%dil
	orb	%dil, %r8b
	jne	.L374
	testq	%r12, %r12
	je	.L374
	movq	%rax, -6464(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L373:
	movl	%esi, (%rdx)
	leaq	0(%r13,%rdi,8), %rdx
	movq	-6440(%rbp), %r11
	subq	$1, %r12
	movl	(%rdx), %esi
	movq	-6472(%rbp), %rdi
	andq	%rsi, %r11
	subq	%rsi, %rdi
	sete	%r9b
	cmpq	%rdi, -6496(%rbp)
	setb	%dil
	orb	%dil, %r9b
	jne	.L374
	testq	%r12, %r12
	je	.L374
	movq	%r8, -6456(%rbp)
	movq	%rax, %r14
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L540:
	movq	-6512(%rbp), %rdx
	addq	%r9, %rdx
	addq	$8, %r9
	subq	$1, %rax
	jne	.L370
	movq	%r9, %rax
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L368:
	andl	$7, %r10d
	je	.L371
	movzbl	(%r8,%rax), %edx
	cmpb	%dl, (%rdi)
	jne	.L371
	leaq	1(%rax), %rdx
	cmpq	$1, %r10
	je	.L437
	movzbl	1(%r8,%rax), %ebx
	cmpb	%bl, 1(%rdi)
	jne	.L437
	leaq	2(%rax), %rdx
	cmpq	$2, %r10
	je	.L437
	movzbl	2(%r8,%rax), %ebx
	cmpb	%bl, 2(%rdi)
	jne	.L437
	leaq	3(%rax), %rdx
	cmpq	$3, %r10
	je	.L437
	movzbl	3(%r8,%rax), %ebx
	cmpb	%bl, 3(%rdi)
	jne	.L437
	leaq	4(%rax), %rdx
	cmpq	$4, %r10
	je	.L437
	movzbl	4(%r8,%rax), %ebx
	cmpb	%bl, 4(%rdi)
	jne	.L437
	leaq	5(%rax), %rdx
	subq	$5, %r10
	je	.L437
	movzbl	5(%r8,%rax), %ebx
	cmpb	%bl, 5(%rdi)
	jne	.L437
	leaq	6(%rax), %rdx
	cmpq	$1, %r10
	je	.L437
	movzbl	6(%r8,%rax), %ebx
	cmpb	%bl, 6(%rdi)
	jne	.L437
	addq	$7, %rax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%rdx, %rax
	jmp	.L371
.L443:
	leaq	0(%r13,%rax,8), %rcx
	movq	-6456(%rbp), %rax
	leaq	0(%r13,%rax,4), %rdx
	.p2align 4,,10
	.p2align 3
.L374:
	movq	-6528(%rbp), %rax
	movl	524336(%rax), %eax
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
.L367:
	addq	$1, -6472(%rbp)
	movq	-6472(%rbp), %rax
	cmpq	-6560(%rbp), %rax
	jne	.L376
.L377:
	movq	-6544(%rbp), %rbx
	movq	%r15, -6448(%rbp)
	movq	-6616(%rbp), %r12
	movq	-6608(%rbp), %r14
	movq	%rbx, %r13
	movq	-6624(%rbp), %r15
	movq	-6632(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r13, %rdx
	addq	$1, %r13
	addq	$4, %rdx
	cmpq	%rdx, -6536(%rbp)
	jbe	.L528
	subq	$8, %rsp
	pushq	-6584(%rbp)
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	-3168(%rbp), %r8
	movq	%rbx, %r9
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	EvaluateNode.isra.0.constprop.0
	movq	%r13, %rdx
	popq	%rax
	popq	%rcx
	notq	%rdx
	addq	-6544(%rbp), %rdx
	addq	-6552(%rbp), %rdx
	jne	.L364
.L528:
	movq	-6448(%rbp), %r15
	movq	%r13, %rbx
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L541:
	movl	0(%r13,%rdi,8), %eax
	movl	%eax, (%rcx)
	movl	4(%r13,%r8,4), %eax
	movl	%eax, (%rdx)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L539:
	movl	0(%r13,%rdi,8), %eax
	movl	%eax, (%rcx)
	movl	4(%r13,%r8,4), %eax
	movl	%eax, (%rdx)
	jmp	.L353
.L409:
	movq	%rdx, %rax
	jmp	.L330
.L324:
	movq	-6440(%rbp), %r11
	movq	%rsi, %rax
	andq	%rcx, %r11
	subq	%rcx, %rax
	movq	%rax, -6488(%rbp)
	je	.L323
	cmpq	%rax, -6504(%rbp)
	jnb	.L326
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L344:
	subq	$8, %rsp
	pushq	-6584(%rbp)
	movq	-6544(%rbp), %r14
	movq	%r15, %rcx
	movq	-6600(%rbp), %r9
	pushq	-6632(%rbp)
	movq	-6440(%rbp), %r8
	pushq	-6648(%rbp)
	movq	%r14, %rdx
	movq	-6608(%rbp), %rsi
	pushq	-6592(%rbp)
	movq	-6536(%rbp), %rdi
	pushq	%rbx
	pushq	-6624(%rbp)
	pushq	-6616(%rbp)
	call	UpdateNodes
	addq	$64, %rsp
	movq	%rax, -6552(%rbp)
	cmpq	$16383, %rax
	ja	.L347
	cmpq	$1, %rbx
	je	.L345
	movq	%r14, %rbx
	jmp	.L348
.L535:
	movq	-6448(%rbp), %r13
	movq	-6512(%rbp), %r10
	cmpq	$127, %r13
	jbe	.L323
	movq	-6472(%rbp), %rsi
	andq	-6552(%rbp), %rcx
	movq	-6568(%rbp), %rdx
	movl	(%rsi,%rcx,8), %eax
	movl	%eax, (%rsi,%rdx,4)
	movq	-6576(%rbp), %rdx
	movl	4(%rsi,%rcx,8), %eax
	movl	%eax, (%rsi,%rdx,4)
	jmp	.L323
.L347:
	cmpq	$1, %rbx
	jne	.L349
.L346:
	movl	-3132(%rbp), %eax
	shrl	$5, %eax
	cmpq	%rax, -6664(%rbp)
	jnb	.L349
.L383:
	movq	-6552(%rbp), %rsi
	cmpq	%rax, %rsi
	cmovnb	%rsi, %rax
	movq	%rax, -6552(%rbp)
	jmp	.L349
.L395:
	movq	-6592(%rbp), %r14
	movl	$1, %r10d
	jmp	.L313
.L532:
	subq	$1, -6448(%rbp)
	movq	-6512(%rbp), %rsi
	leaq	(%rsi,%r9), %r11
	leaq	8(%r9), %rsi
	je	.L542
	movq	%rsi, %r9
	jmp	.L318
.L316:
	cmpq	$0, -6472(%rbp)
	je	.L315
	cmpq	$0, -6488(%rbp)
	movq	-6520(%rbp), %r11
	movl	$1, %r9d
	je	.L315
.L387:
	movzbl	(%rcx,%r9), %esi
	cmpb	%sil, (%r11)
	jne	.L397
	cmpq	$2, -6472(%rbp)
	leaq	1(%r9), %rsi
	je	.L320
	movzbl	1(%rcx,%r9), %ebx
	cmpb	%bl, 1(%r11)
	jne	.L320
	cmpq	$3, -6472(%rbp)
	leaq	2(%r9), %rsi
	je	.L320
	movzbl	2(%rcx,%r9), %ebx
	cmpb	%bl, 2(%r11)
	jne	.L320
	cmpq	$4, -6472(%rbp)
	leaq	3(%r9), %rsi
	je	.L320
	movzbl	3(%rcx,%r9), %ebx
	cmpb	%bl, 3(%r11)
	jne	.L320
	cmpq	$5, -6472(%rbp)
	leaq	4(%r9), %rsi
	je	.L320
	movzbl	4(%rcx,%r9), %ebx
	cmpb	%bl, 4(%r11)
	jne	.L320
	cmpq	$6, -6472(%rbp)
	leaq	5(%r9), %rsi
	je	.L320
	movzbl	5(%rcx,%r9), %ebx
	cmpb	%bl, 5(%r11)
	jne	.L320
	cmpq	$7, -6472(%rbp)
	leaq	6(%r9), %rsi
	je	.L320
	movzbl	6(%r11), %ebx
	cmpb	%bl, 6(%rcx,%r9)
	jne	.L320
	leaq	7(%r9), %rsi
	jmp	.L320
.L397:
	movq	%r9, %rsi
	jmp	.L320
.L542:
	cmpq	$0, -6472(%rbp)
	movq	%rbx, -6448(%rbp)
	je	.L320
	movzbl	(%rcx,%rsi), %ebx
	cmpb	%bl, (%r11)
	jne	.L320
	addq	$1, %r11
	cmpq	$0, -6488(%rbp)
	leaq	9(%r9), %rsi
	je	.L320
	movq	%rsi, %r9
	jmp	.L387
.L537:
	call	__stack_chk_fail@PLT
.L439:
	movq	$0, -6552(%rbp)
	jmp	.L383
	.cfi_endproc
.LFE307:
	.size	BrotliZopfliComputeShortestPath, .-BrotliZopfliComputeShortestPath
	.p2align 4
	.globl	BrotliCreateZopfliBackwardReferences
	.hidden	BrotliCreateZopfliBackwardReferences
	.type	BrotliCreateZopfliBackwardReferences, @function
BrotliCreateZopfliBackwardReferences:
.LFB308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	32(%rbp), %rax
	movq	16(%rbp), %rdx
	movq	24(%rbp), %r14
	movq	%rax, -56(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -80(%rbp)
	cmpq	$-1, %rsi
	jne	.L551
.L544:
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r9, -88(%rbp)
	pushq	%r12
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	%r14
	call	BrotliZopfliComputeShortestPath
	movq	-72(%rbp), %rdi
	addq	$32, %rsp
	movq	%r12, %rdx
	movq	-88(%rbp), %r9
	movq	-56(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r14, %rcx
	addq	%rax, (%rdi)
	movq	%r13, %rdi
	pushq	-80(%rbp)
	pushq	-64(%rbp)
	call	BrotliZopfliCreateCommands
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BrotliFree
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	leaq	1(%rsi), %rsi
	movq	%rdx, -120(%rbp)
	salq	$4, %rsi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	BrotliAllocate
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rcx
	movss	.LC6(%rip), %xmm0
	movq	-104(%rbp), %r8
	movq	%rax, %r12
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %rdx
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L545:
	movl	$1, (%rax)
	addq	$16, %rax
	movl	$0, -12(%rax)
	movl	$0, -8(%rax)
	movss	%xmm0, -4(%rax)
	cmpq	%rax, %rsi
	jne	.L545
	jmp	.L544
	.cfi_endproc
.LFE308:
	.size	BrotliCreateZopfliBackwardReferences, .-BrotliCreateZopfliBackwardReferences
	.p2align 4
	.globl	BrotliCreateHqZopfliBackwardReferences
	.hidden	BrotliCreateHqZopfliBackwardReferences
	.type	BrotliCreateHqZopfliBackwardReferences, @function
BrotliCreateHqZopfliBackwardReferences:
.LFB309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2008, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rax, -10136(%rbp)
	movq	24(%rbp), %rax
	movq	%rcx, %r15
	movq	%rdi, -10208(%rbp)
	movl	8(%r9), %ecx
	movq	%rax, -10056(%rbp)
	movq	32(%rbp), %rax
	movq	%rsi, -10000(%rbp)
	movq	%rax, -10216(%rbp)
	movq	40(%rbp), %rax
	movq	%rdx, -10168(%rbp)
	movq	%rax, -10232(%rbp)
	movq	48(%rbp), %rax
	movq	%r8, -9960(%rbp)
	movq	%rax, -10200(%rbp)
	movq	56(%rbp), %rax
	movq	%r9, -10120(%rbp)
	movq	%rax, -10224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	salq	%cl, %rax
	subq	$16, %rax
	movq	%rax, -10184(%rbp)
	testq	%rsi, %rsi
	jne	.L913
	movq	-10224(%rbp), %rax
	movq	$1, -10064(%rbp)
	movq	$0, -10048(%rbp)
	movq	$0, -10096(%rbp)
	movq	(%rax), %rax
	movq	%rax, -10088(%rbp)
	movq	-10216(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -10040(%rbp)
	movq	-10056(%rbp), %rax
	movdqu	(%rax), %xmm7
	movq	-10200(%rbp), %rax
	movq	(%rax), %rax
	movaps	%xmm7, -10080(%rbp)
	movq	%rax, -10032(%rbp)
	leaq	-2912(%rbp), %rax
	movq	%rax, -10128(%rbp)
.L733:
	movq	-10064(%rbp), %rsi
	movq	-10208(%rbp), %rdi
	salq	$4, %rsi
	call	BrotliAllocate
	movl	$544, %edx
	movq	-10000(%rbp), %rsi
	movq	%rax, %rbx
	movq	-10120(%rbp), %rax
	movq	%rsi, -64(%rbp)
	movl	64(%rax), %eax
	cmpl	$544, %eax
	cmovbe	%eax, %edx
	movl	%edx, %r12d
	cmpq	$-2, %rsi
	jne	.L726
	xorl	%edx, %edx
	movq	%rdx, -80(%rbp)
	xorl	%edx, %edx
	testl	%eax, %eax
	jne	.L914
.L626:
	movq	-10000(%rbp), %rax
	movq	%rdx, -96(%rbp)
	movq	$0, -10016(%rbp)
	salq	$4, %rax
	movq	%rbx, -9968(%rbp)
	leaq	(%rbx,%rax), %rsi
	leaq	16(%rbx,%rax), %rax
	movq	%r15, -9984(%rbp)
	movq	%rsi, -10024(%rbp)
	movq	%rax, -10104(%rbp)
	movl	%r12d, -88(%rbp)
	movq	-9960(%rbp), %r12
.L721:
	cmpq	$0, -10064(%rbp)
	movq	-9968(%rbp), %rax
	movq	-10104(%rbp), %rdx
	je	.L630
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$1, (%rax)
	addq	$16, %rax
	movl	$0, -12(%rax)
	movl	$0, -8(%rax)
	movl	$0x7effc99e, -4(%rax)
	cmpq	%rax, %rdx
	jne	.L627
.L630:
	cmpq	$0, -10016(%rbp)
	je	.L915
	movq	-10200(%rbp), %rax
	leaq	-9952(%rbp), %r15
	movl	$128, %ecx
	leaq	-5728(%rbp), %r14
	movq	%r15, %rdi
	leaq	-7904(%rbp), %r13
	movq	-10168(%rbp), %r8
	subq	-10040(%rbp), %r8
	movq	(%rax), %rax
	movq	%rax, %rdx
	movq	%rax, -9960(%rbp)
	xorl	%eax, %eax
	subq	-10032(%rbp), %rdx
	rep stosq
	movl	$352, %ecx
	movq	%r14, %rdi
	movq	%rdx, %r10
	rep stosq
	movl	$272, %ecx
	movq	%r13, %rdi
	salq	$4, %r10
	rep stosq
	movq	-10232(%rbp), %rax
	movq	-9984(%rbp), %rdi
	movq	%rax, %rsi
	addq	%rax, %r10
	testq	%rdx, %rdx
	je	.L637
	.p2align 4,,10
	.p2align 3
.L636:
	movl	4(%rsi), %r11d
	movzwl	12(%rsi), %eax
	movl	(%rsi), %r9d
	movzwl	14(%rsi), %edx
	andl	$33554431, %r11d
	addl	$1, -5728(%rbp,%rax,4)
	cmpq	$127, %rax
	jbe	.L634
	movq	%rdx, %rax
	andl	$1023, %eax
	addl	$1, -7904(%rbp,%rax,4)
.L634:
	leaq	(%r9,%r8), %rcx
	movq	%r8, %rax
	testq	%r9, %r9
	je	.L639
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r12, %rdx
	andq	%rax, %rdx
	addq	$1, %rax
	movzbl	(%rdi,%rdx), %edx
	addl	$1, -9952(%rbp,%rdx,4)
	cmpq	%rax, %rcx
	jne	.L638
.L639:
	addq	%r11, %r9
	addq	$16, %rsi
	addq	%r9, %r8
	cmpq	%r10, %rsi
	jne	.L636
.L637:
	movq	%r15, %rax
	leaq	-8928(%rbp), %rcx
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	.p2align 4,,10
	.p2align 3
.L633:
	movdqa	(%rax), %xmm0
	addq	$16, %rax
	movdqa	%xmm0, %xmm3
	punpckldq	%xmm2, %xmm0
	punpckhdq	%xmm2, %xmm3
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rax, %rcx
	jne	.L633
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rax
	cmpq	$255, %rax
	ja	.L640
	leaq	kLog2Table(%rip), %rdx
	movss	(%rdx,%rax,4), %xmm1
	movaps	%xmm1, %xmm0
.L641:
	addss	.LC8(%rip), %xmm0
	movq	%r12, -9992(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, %r12
	movss	%xmm0, -9976(%rbp)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L644:
	movl	%eax, %edx
	cmpl	$255, %eax
	ja	.L646
	leaq	kLog2Table(%rip), %rax
	movss	(%rax,%rdx,4), %xmm0
.L647:
	movaps	%xmm1, %xmm5
	movss	.LC5(%rip), %xmm6
	subss	%xmm0, %xmm5
	maxss	%xmm5, %xmm6
	movss	%xmm6, (%r12,%rbx,4)
	addq	$1, %rbx
	cmpq	$256, %rbx
	je	.L916
.L651:
	movl	(%r15,%rbx,4), %eax
	testl	%eax, %eax
	jne	.L644
	movss	-9976(%rbp), %xmm4
	movss	%xmm4, (%r12,%rbx,4)
	addq	$1, %rbx
	cmpq	$256, %rbx
	jne	.L651
.L916:
	movq	%r14, %rbx
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movq	%r14, %rax
	movq	-9992(%rbp), %r12
	movq	-10128(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L652:
	movdqa	(%rax), %xmm0
	addq	$16, %rax
	movdqa	%xmm0, %xmm3
	punpckldq	%xmm2, %xmm0
	punpckhdq	%xmm2, %xmm3
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rax, %rdx
	jne	.L652
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm1
	movq	%xmm1, %r15
	cmpq	$255, %r15
	ja	.L653
	leaq	kLog2Table(%rip), %rax
	movss	(%rax,%r15,4), %xmm4
.L654:
	movq	-10128(%rbp), %rax
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	.p2align 4,,10
	.p2align 3
.L657:
	movdqa	%xmm2, %xmm0
	addq	$16, %rbx
	pcmpeqd	-16(%rbx), %xmm0
	pand	.LC9(%rip), %xmm0
	movdqa	%xmm0, %xmm3
	punpckldq	%xmm2, %xmm0
	punpckhdq	%xmm2, %xmm3
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rbx, %rax
	jne	.L657
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rax
	addq	%r15, %rax
	cmpq	$255, %rax
	ja	.L658
	leaq	kLog2Table(%rip), %rdx
	movss	(%rdx,%rax,4), %xmm0
.L659:
	addss	.LC8(%rip), %xmm0
	movq	%r12, -9992(%rbp)
	xorl	%r15d, %r15d
	leaq	kLog2Table(%rip), %rbx
	movq	-10128(%rbp), %r12
	movss	%xmm0, -9976(%rbp)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L662:
	movl	%eax, %edx
	cmpl	$255, %eax
	ja	.L664
	movss	(%rbx,%rdx,4), %xmm0
.L665:
	movaps	%xmm4, %xmm7
	movss	.LC5(%rip), %xmm5
	subss	%xmm0, %xmm7
	maxss	%xmm7, %xmm5
	movss	%xmm5, (%r12,%r15,4)
	addq	$1, %r15
	cmpq	$704, %r15
	je	.L917
.L669:
	movl	(%r14,%r15,4), %eax
	testl	%eax, %eax
	jne	.L662
	movss	-9976(%rbp), %xmm5
	movss	%xmm5, (%r12,%r15,4)
	addq	$1, %r15
	cmpq	$704, %r15
	jne	.L669
.L917:
	movl	-88(%rbp), %r15d
	movq	-9992(%rbp), %r12
	movq	-96(%rbp), %rcx
	movq	%r15, %r14
	testq	%r15, %r15
	je	.L788
	leaq	-1(%r15), %rax
	cmpq	$2, %rax
	jbe	.L789
	movq	%r15, %rdx
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movq	%r13, %rax
	shrq	$2, %rdx
	salq	$4, %rdx
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L672:
	movdqa	(%rax), %xmm0
	addq	$16, %rax
	movdqa	%xmm0, %xmm3
	punpckldq	%xmm2, %xmm0
	punpckhdq	%xmm2, %xmm3
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rdx, %rax
	jne	.L672
	movdqa	%xmm1, %xmm0
	movq	%r15, %rax
	psrldq	$8, %xmm0
	andq	$-4, %rax
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rbx
	cmpq	%rax, %r15
	je	.L673
.L671:
	movl	-7904(%rbp,%rax,4), %esi
	leaq	0(,%rax,4), %rdx
	addq	%rsi, %rbx
	leaq	1(%rax), %rsi
	cmpq	%rsi, %r15
	jbe	.L673
	movl	-7900(%rbp,%rdx), %esi
	addq	$2, %rax
	addq	%rsi, %rbx
	cmpq	%rax, %r15
	jbe	.L673
	movl	-7896(%rbp,%rdx), %eax
	addq	%rax, %rbx
.L673:
	cmpq	$255, %rbx
	jbe	.L918
	testq	%rbx, %rbx
	js	.L676
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L677:
	movq	%rcx, -9960(%rbp)
	call	log2@PLT
	pxor	%xmm4, %xmm4
	movq	-9960(%rbp), %rcx
	cvtsd2ss	%xmm0, %xmm4
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L646:
	pxor	%xmm0, %xmm0
	movss	%xmm1, -9960(%rbp)
	cvtsi2sdq	%rdx, %xmm0
	call	log2@PLT
	movss	-9960(%rbp), %xmm1
	cvtsd2ss	%xmm0, %xmm0
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L664:
	pxor	%xmm0, %xmm0
	movss	%xmm4, -9960(%rbp)
	cvtsi2sdq	%rdx, %xmm0
	call	log2@PLT
	movss	-9960(%rbp), %xmm4
	cvtsd2ss	%xmm0, %xmm0
	jmp	.L665
.L915:
	movq	-9984(%rbp), %rdx
	movq	-10168(%rbp), %rsi
	movq	%r12, %rcx
	movq	-10128(%rbp), %rdi
	call	ZopfliCostModelSetFromLiteralCosts
.L631:
	movq	-10200(%rbp), %rax
	movq	-10032(%rbp), %rsi
	movl	$150, %edx
	movl	$1, %r15d
	movdqa	-10080(%rbp), %xmm4
	movq	%rsi, (%rax)
	movq	-10224(%rbp), %rax
	movq	-10088(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	-10216(%rbp), %rax
	movq	-10040(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	-10056(%rbp), %rax
	movups	%xmm4, (%rax)
	movq	-10120(%rbp), %rax
	movl	8(%rax), %ecx
	salq	%cl, %r15
	subq	$16, %r15
	cmpl	$11, 4(%rax)
	movl	$325, %eax
	cmovl	%rdx, %rax
	cmpq	$3, -10000(%rbp)
	movq	%rax, -10008(%rbp)
	movq	-9968(%rbp), %rax
	movl	$0, (%rax)
	movl	$0x00000000, 12(%rax)
	movq	$0, -5472(%rbp)
	jbe	.L707
	xorl	%ebx, %ebx
	movq	%r12, -9992(%rbp)
	xorl	%r13d, %r13d
	leaq	-5728(%rbp), %r14
	movq	%rbx, %r12
	movq	-10168(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-10048(%rbp), %rax
	subq	$8, %rsp
	pushq	-9968(%rbp)
	movq	%r13, %rdx
	movq	-10120(%rbp), %r9
	pushq	%r14
	movq	%rbx, %rsi
	leaq	(%rax,%r13,4), %r11
	movq	-10096(%rbp), %rax
	pushq	-10128(%rbp)
	movq	-9992(%rbp), %r8
	movq	%r11, -9976(%rbp)
	leaq	(%rax,%r12,8), %rax
	movq	-9984(%rbp), %rcx
	movq	-10000(%rbp), %rdi
	pushq	%rax
	movl	(%r11), %eax
	pushq	%rax
	pushq	-10056(%rbp)
	pushq	%r15
	call	UpdateNodes
	movq	-9976(%rbp), %r11
	addq	$64, %rsp
	cmpq	$16383, %rax
	movq	%rax, -9960(%rbp)
	movl	(%r11), %edx
	ja	.L708
	addq	%rdx, %r12
	cmpl	$1, %edx
	je	.L709
.L908:
	movq	%r13, %rax
	leaq	1(%r13), %r13
	addq	$4, %rax
	cmpq	%rax, -10000(%rbp)
	ja	.L714
	movq	-10024(%rbp), %rax
	movq	-9992(%rbp), %r12
	movl	8(%rax), %edx
	testl	$134217727, %edx
	jne	.L919
.L724:
	movq	-10024(%rbp), %rax
	movq	-10000(%rbp), %rcx
	movq	-9968(%rbp), %rsi
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L718:
	subq	$1, %rcx
	subq	$16, %rax
	movq	%rcx, %rdx
	salq	$4, %rdx
	testl	$134217727, 8(%rsi,%rdx)
	jne	.L716
.L717:
	cmpl	$1, (%rax)
	je	.L718
.L716:
	movl	$-1, 12(%rax)
	testq	%rcx, %rcx
	je	.L719
	movq	-9968(%rbp), %rsi
	movq	%rcx, %rax
	salq	$4, %rax
	movl	8(%rsi,%rax), %edx
	movq	%rsi, %rbx
.L723:
	movq	%rcx, %rax
	andl	$134217727, %edx
	movq	%rcx, %rdi
	movl	$1, %ecx
	salq	$4, %rax
	movl	(%rbx,%rax), %eax
	andl	$33554431, %eax
	addl	%eax, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rsi
	salq	$4, %rsi
	addq	%rbx, %rsi
	movl	%edx, 12(%rsi)
	testq	%rdi, %rdi
	je	.L719
	movq	%rbx, %r8
	.p2align 4,,10
	.p2align 3
.L720:
	movq	%rdi, %rax
	addq	$1, %rcx
	salq	$4, %rax
	movl	(%r8,%rax), %eax
	andl	$33554431, %eax
	movl	%eax, %edx
	movl	8(%rsi), %eax
	andl	$134217727, %eax
	addl	%eax, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rsi
	salq	$4, %rsi
	addq	%r8, %rsi
	movl	%edx, 12(%rsi)
	testq	%rdi, %rdi
	jne	.L720
.L719:
	movq	-10200(%rbp), %rax
	movq	-9968(%rbp), %rbx
	movq	-10120(%rbp), %r9
	movq	-10216(%rbp), %r8
	addq	%rcx, (%rax)
	movq	-10168(%rbp), %rsi
	movq	%rbx, %rdx
	pushq	-10224(%rbp)
	movq	-10056(%rbp), %rcx
	movq	-10000(%rbp), %rdi
	pushq	-10232(%rbp)
	call	BrotliZopfliCreateCommands
	cmpq	$1, -10016(%rbp)
	popq	%rax
	popq	%rdx
	jne	.L795
	movq	-10208(%rbp), %r15
	movq	-80(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -80(%rbp)
	call	BrotliFree
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	$0, -96(%rbp)
	call	BrotliFree
	movq	-10096(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	-10048(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L920
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore_state
	addq	%rdx, %r12
	cmpl	$1, %edx
	je	.L921
.L712:
	movq	%r13, -9976(%rbp)
	.p2align 4,,10
	.p2align 3
.L713:
	movq	%r13, %rdx
	addq	$1, %r13
	addq	$4, %rdx
	cmpq	%rdx, -10000(%rbp)
	jbe	.L908
	subq	$8, %rsp
	pushq	-9968(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	-10056(%rbp), %rcx
	movq	%r14, %r9
	movq	%rbx, %rdi
	movq	-80(%rbp), %r8
	call	EvaluateNode.isra.0.constprop.0
	movq	-10048(%rbp), %rax
	popq	%rcx
	popq	%rsi
	movl	(%rax,%r13,4), %edx
	addq	%rdx, %r12
	movq	%r13, %rdx
	notq	%rdx
	addq	-9976(%rbp), %rdx
	addq	-9960(%rbp), %rdx
	jne	.L713
	jmp	.L908
.L709:
	movq	-10096(%rbp), %rax
	movl	-4(%rax,%r12,8), %eax
	movl	%eax, -9960(%rbp)
	shrl	$5, %eax
	cmpq	%rax, -10008(%rbp)
	jnb	.L908
	movq	$0, -9960(%rbp)
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L921:
	movq	-10096(%rbp), %rax
	movl	-4(%rax,%r12,8), %eax
	shrl	$5, %eax
	cmpq	%rax, -10008(%rbp)
	jnb	.L712
.L722:
	movq	-9960(%rbp), %rsi
	cmpq	%rax, %rsi
	cmovnb	%rsi, %rax
	movq	%rax, -9960(%rbp)
	jmp	.L712
.L795:
	movq	$1, -10016(%rbp)
	jmp	.L721
.L658:
	testq	%rax, %rax
	js	.L660
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L661:
	movss	%xmm4, -9960(%rbp)
	call	log2@PLT
	movss	-9960(%rbp), %xmm4
	cvtsd2ss	%xmm0, %xmm0
	jmp	.L659
.L653:
	testq	%r15, %r15
	js	.L655
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r15, %xmm0
.L656:
	call	log2@PLT
	pxor	%xmm4, %xmm4
	cvtsd2ss	%xmm0, %xmm4
	jmp	.L654
.L640:
	testq	%rax, %rax
	js	.L642
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L643:
	movapd	%xmm2, %xmm0
	movq	%rcx, -9992(%rbp)
	movsd	%xmm2, -9976(%rbp)
	call	log2@PLT
	movsd	-9976(%rbp), %xmm2
	pxor	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	movss	%xmm1, -9960(%rbp)
	movapd	%xmm2, %xmm0
	call	log2@PLT
	movq	-9992(%rbp), %rcx
	movss	-9960(%rbp), %xmm1
	cvtsd2ss	%xmm0, %xmm0
	jmp	.L641
.L913:
	leaq	0(,%rsi,4), %rbx
	movq	%rsi, %r14
	movq	%rbx, %rsi
	movq	%rbx, -10176(%rbp)
	call	BrotliAllocate
	movq	%rax, -10048(%rbp)
	movq	-10168(%rbp), %rax
	cmpq	$127, %r14
	jbe	.L554
	leaq	-127(%r14,%rax), %rax
	movq	%rax, -10240(%rbp)
	testq	%rbx, %rbx
	je	.L922
.L555:
	movq	-10000(%rbp), %rsi
	movq	-10208(%rbp), %rdi
	salq	$5, %rsi
	call	BrotliAllocate
	movq	%rax, -10096(%rbp)
.L557:
	cmpq	$3, -10000(%rbp)
	jbe	.L923
.L556:
	movq	-10120(%rbp), %rax
	movdqa	.LC7(%rip), %xmm0
	movq	%r15, %r14
	movq	$0, -10144(%rbp)
	movq	$0, -10152(%rbp)
	addq	$80, %rax
	movq	%rax, -10192(%rbp)
	leaq	-2912(%rbp), %rax
	movq	%rax, -10128(%rbp)
	.p2align 4,,10
	.p2align 3
.L623:
	movq	-10144(%rbp), %rsi
	movq	-10168(%rbp), %rax
	movq	-10184(%rbp), %rbx
	movq	-10000(%rbp), %r15
	addq	%rsi, %rax
	cmpq	%rax, %rbx
	movq	%rax, -10024(%rbp)
	cmovbe	%rbx, %rax
	subq	%rsi, %r15
	movq	%rax, -10016(%rbp)
	movq	-10152(%rbp), %rax
	leaq	128(%rax), %r13
	cmpq	-10176(%rbp), %r13
	ja	.L924
.L559:
	movq	-10096(%rbp), %rax
	movq	-10152(%rbp), %rsi
	movl	$64, %edx
	movq	-9960(%rbp), %rcx
	leaq	(%rax,%rsi,8), %r13
	movq	-10024(%rbp), %rsi
	movq	-10120(%rbp), %rax
	movq	%rcx, %rbx
	movq	%r13, -10160(%rbp)
	andq	%rsi, %rbx
	cmpl	$11, 4(%rax)
	movl	$16, %eax
	movq	%rsi, %rdi
	cmove	%rdx, %rax
	movq	%rbx, -9976(%rbp)
	leaq	(%r14,%rbx), %r11
	subq	%rax, %rdi
	cmpq	%rsi, %rax
	movl	$0, %eax
	cmova	%rax, %rdi
	leaq	-1(%rsi), %rax
	cmpq	%rdi, %rax
	jbe	.L739
	movq	-10016(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L739
	movq	%r15, %rbx
	movq	%r15, %r9
	leaq	1(%r11), %r10
	movl	$1, %r8d
	andl	$7, %ebx
	shrq	$3, %r9
	movq	%r10, -10040(%rbp)
	leaq	8(%r11), %r10
	leaq	-1(%rbx), %r12
	movq	%r15, -9984(%rbp)
	movl	$1, %edx
	movq	%rcx, %r15
	movq	%r12, -10032(%rbp)
	movq	%rsi, %r12
	movq	%r10, -10008(%rbp)
	movq	%r9, -9968(%rbp)
	movq	%rbx, -9992(%rbp)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L567:
	subq	$1, %rax
	cmpq	%rdi, %rax
	jbe	.L893
.L926:
	cmpq	$2, %r8
	ja	.L893
	addq	$1, %rdx
	cmpq	%rdx, %r12
	jb	.L893
.L566:
	movq	%r15, %rsi
	andq	%rax, %rsi
	leaq	(%r14,%rsi), %rcx
	movzbl	(%rcx), %ebx
	cmpb	%bl, (%r11)
	jne	.L567
	movq	-9976(%rbp), %rbx
	movzbl	1(%r14,%rsi), %esi
	cmpb	%sil, 1(%r14,%rbx)
	jne	.L567
	movq	-9968(%rbp), %r9
	testq	%r9, %r9
	je	.L568
	xorl	%r10d, %r10d
.L570:
	movq	(%r11,%r10), %rsi
	movq	(%rcx,%r10), %rbx
	cmpq	%rbx, %rsi
	je	.L925
	xorq	%rbx, %rsi
	movq	%r9, -9968(%rbp)
	rep bsfq	%rsi, %rsi
	movslq	%esi, %rsi
	shrq	$3, %rsi
	addq	%r10, %rsi
.L572:
	cmpq	%rsi, %r8
	jnb	.L567
	movl	%esi, %ecx
	subq	$1, %rax
	movl	%edx, 0(%r13)
	movq	%rsi, %r8
	sall	$5, %ecx
	addq	$8, %r13
	movl	%ecx, -4(%r13)
	cmpq	%rdi, %rax
	ja	.L926
	.p2align 4,,10
	.p2align 3
.L893:
	movq	-9984(%rbp), %r15
	movq	%r8, -9984(%rbp)
.L565:
	cmpq	-9984(%rbp), %r15
	jbe	.L575
	cmpq	$128, %r15
	movl	$128, %eax
	movq	-10136(%rbp), %rbx
	cmovbe	%r15, %rax
	movq	%rax, -10064(%rbp)
	leaq	524344(%rbx), %rax
	movq	%rax, -10008(%rbp)
	imull	$506832829, (%r11), %eax
	shrl	$15, %eax
	leaq	(%rbx,%rax,4), %rax
	movq	40(%rbx), %rbx
	movl	48(%rax), %esi
	movq	%rbx, %rcx
	movq	%rbx, -10080(%rbp)
	movq	-10024(%rbp), %rbx
	movq	%rbx, %rdx
	andq	%rcx, %rdx
	leaq	(%rdx,%rdx), %rdi
	leaq	1(%rdi), %rcx
	movq	%rdi, -10104(%rbp)
	movq	%rcx, -10112(%rbp)
	cmpq	$127, %r15
	jbe	.L576
	movq	-9960(%rbp), %r8
	movl	%ebx, 48(%rax)
	andq	%rsi, %r8
	subq	%rsi, %rbx
	movq	%rbx, -10032(%rbp)
	je	.L577
	cmpq	%rbx, -10016(%rbp)
	jb	.L577
.L578:
	xorl	%eax, %eax
	movq	%r15, -9968(%rbp)
	movl	$64, %ebx
	movq	$0, -9992(%rbp)
	movq	%rax, %r15
	movq	%r11, -10040(%rbp)
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L929:
	cmpq	$127, -9968(%rbp)
	jbe	.L586
	movq	-10008(%rbp), %rdi
	movq	-10104(%rbp), %rcx
	movl	%esi, (%rdi,%rcx,4)
.L586:
	andq	-10080(%rbp), %rsi
	movq	%rax, %r15
	leaq	1(%rsi,%rsi), %rcx
	movq	%rcx, -10104(%rbp)
	movq	-10008(%rbp), %rcx
	movl	4(%rcx,%rsi,8), %esi
.L587:
	movq	-9960(%rbp), %r8
	movq	-10024(%rbp), %rax
	subq	$1, %rbx
	andq	%rsi, %r8
	subq	%rsi, %rax
	sete	%dl
	cmpq	%rax, -10016(%rbp)
	movq	%rax, -10032(%rbp)
	setb	%al
	orb	%al, %dl
	jne	.L894
	testq	%rbx, %rbx
	je	.L894
.L590:
	movq	-9992(%rbp), %rax
	movq	-9968(%rbp), %r11
	cmpq	%r15, %rax
	movq	%rax, %rcx
	movq	-9976(%rbp), %rax
	cmova	%r15, %rcx
	subq	%rcx, %r11
	leaq	(%rax,%rcx), %r9
	leaq	(%rcx,%r8), %rdi
	movq	%r11, %rax
	addq	%r14, %rdi
	addq	%r14, %r9
	shrq	$3, %rax
	je	.L579
	leaq	8(%rdi), %rdx
	xorl	%r10d, %r10d
	movq	%rdx, -10088(%rbp)
.L581:
	movq	(%rdi,%r10), %rdx
	movq	(%r9,%r10), %r12
	cmpq	%r12, %rdx
	je	.L927
	xorq	%r12, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r10, %rax
.L582:
	addq	%rcx, %rax
	testq	%r13, %r13
	je	.L583
	cmpq	-9984(%rbp), %rax
	jbe	.L583
	movl	-10032(%rbp), %edi
	movl	%eax, %edx
	movq	%rax, -9984(%rbp)
	addq	$8, %r13
	sall	$5, %edx
	movl	%edi, -8(%r13)
	movl	%edx, -4(%r13)
.L583:
	cmpq	%rax, -10064(%rbp)
	jbe	.L928
	movq	-10040(%rbp), %rcx
	addq	%r14, %r8
	movzbl	(%r8,%rax), %edi
	cmpb	%dil, (%rcx,%rax)
	ja	.L929
	cmpq	$127, -9968(%rbp)
	jbe	.L588
	movq	-10008(%rbp), %rdi
	movq	-10112(%rbp), %rcx
	movl	%esi, (%rdi,%rcx,4)
.L588:
	andq	-10080(%rbp), %rsi
	movq	-10008(%rbp), %rcx
	movq	%rax, -9992(%rbp)
	leaq	(%rsi,%rsi), %rdi
	movq	%rdi, -10112(%rbp)
	movl	(%rcx,%rsi,8), %esi
	jmp	.L587
.L918:
	leaq	kLog2Table(%rip), %rax
	movss	(%rax,%rbx,4), %xmm4
.L675:
	testl	%r14d, %r14d
	movl	$1, %esi
	cmovne	%r15, %rsi
	cmpl	$4, %r14d
	jbe	.L790
	movq	%rsi, %rdx
	pxor	%xmm2, %xmm2
	movq	%r13, %rax
	shrq	$2, %rdx
	pxor	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L679:
	movdqa	%xmm1, %xmm0
	addq	$16, %rax
	pcmpeqd	-16(%rax), %xmm0
	pand	.LC9(%rip), %xmm0
	movdqa	%xmm0, %xmm3
	punpckldq	%xmm1, %xmm0
	punpckhdq	%xmm1, %xmm3
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm2
	cmpq	%rdx, %rax
	jne	.L679
	movdqa	%xmm2, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm2
	movq	%xmm2, %rax
	addq	%rax, %rbx
	movq	%rsi, %rax
	andq	$-4, %rax
	cmpq	%rsi, %rax
	je	.L680
.L678:
	leaq	0(,%rax,4), %rdx
	leaq	1(%rax), %rsi
	cmpl	$1, -7904(%rbp,%rdx)
	adcq	$0, %rbx
	cmpq	%rsi, %r15
	jbe	.L680
	cmpl	$1, -7900(%rbp,%rdx)
	leaq	2(%rax), %rsi
	adcq	$0, %rbx
	cmpq	%rsi, %r15
	jbe	.L680
	cmpl	$1, -7896(%rbp,%rdx)
	adcq	$0, %rbx
	addq	$3, %rax
	cmpq	%rax, %r15
	jbe	.L680
	cmpl	$1, -7892(%rbp,%rdx)
	adcq	$0, %rbx
.L680:
	cmpq	$255, %rbx
	ja	.L686
	pxor	%xmm7, %xmm7
	movss	%xmm7, -9992(%rbp)
.L670:
	leaq	kLog2Table(%rip), %rax
	movss	.LC8(%rip), %xmm7
	addss	(%rax,%rbx,4), %xmm7
	movss	%xmm7, -9976(%rbp)
	testq	%r15, %r15
	je	.L688
.L687:
	movq	%r12, -10008(%rbp)
	xorl	%r14d, %r14d
	movq	%r13, %r12
	movq	%rcx, %r13
	leaq	kLog2Table(%rip), %rbx
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L692:
	movl	%eax, %edx
	cmpl	$255, %eax
	ja	.L694
	movss	(%rbx,%rdx,4), %xmm0
.L695:
	movaps	%xmm4, %xmm6
	movss	.LC5(%rip), %xmm7
	subss	%xmm0, %xmm6
	maxss	%xmm6, %xmm7
	movss	%xmm7, 0(%r13,%r14,4)
	addq	$1, %r14
	cmpq	%r14, %r15
	je	.L930
.L699:
	movl	(%r12,%r14,4), %eax
	testl	%eax, %eax
	jne	.L692
	movss	-9976(%rbp), %xmm6
	movss	%xmm6, 0(%r13,%r14,4)
	addq	$1, %r14
	cmpq	%r14, %r15
	jne	.L699
.L930:
	movq	-10008(%rbp), %r12
.L688:
	movq	-10128(%rbp), %rax
	movss	.LC6(%rip), %xmm0
	leaq	-96(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L691:
	minss	(%rax), %xmm0
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L691
	movq	-80(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movss	%xmm0, -72(%rbp)
	movl	$0x00000000, (%rcx)
	testq	%rsi, %rsi
	je	.L631
	cmpq	$2, %rsi
	jbe	.L791
	movq	-10168(%rbp), %rbx
	leaq	-3(%rsi), %rax
	pxor	%xmm0, %xmm0
	movq	-9984(%rbp), %r9
	shrq	%rax
	movaps	%xmm0, %xmm1
	leaq	2(%rax,%rax), %r8
	leaq	1(%rbx), %rdi
	xorl	%eax, %eax
.L703:
	movq	-10168(%rbp), %rbx
	leaq	(%rbx,%rax), %rdx
	andq	%r12, %rdx
	movzbl	(%r9,%rdx), %edx
	addss	-8928(%rbp,%rdx,4), %xmm1
	leaq	(%rdi,%rax), %rdx
	andq	%r12, %rdx
	movaps	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm2, %xmm4
	movss	%xmm2, 4(%rcx,%rax,4)
	movzbl	(%r9,%rdx), %edx
	addq	$2, %rax
	subss	%xmm0, %xmm4
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm1
	addss	-8928(%rbp,%rdx,4), %xmm1
	addss	%xmm1, %xmm0
	movaps	%xmm0, %xmm7
	movss	%xmm0, (%rcx,%rax,4)
	subss	%xmm2, %xmm7
	subss	%xmm7, %xmm1
	cmpq	%r8, %rax
	jne	.L703
	movq	-9984(%rbp), %r8
	movq	%rbx, %rdi
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	(%rdi,%rax), %rdx
	movss	(%rcx,%rax,4), %xmm0
	addq	$1, %rax
	andq	%r12, %rdx
	movzbl	(%r8,%rdx), %edx
	addss	-8928(%rbp,%rdx,4), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rcx,%rax,4)
	subss	-4(%rcx,%rax,4), %xmm0
	subss	%xmm0, %xmm1
	cmpq	%rax, %rsi
	ja	.L704
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L694:
	pxor	%xmm0, %xmm0
	movss	%xmm4, -9960(%rbp)
	cvtsi2sdq	%rdx, %xmm0
	call	log2@PLT
	movss	-9960(%rbp), %xmm4
	cvtsd2ss	%xmm0, %xmm0
	jmp	.L695
.L686:
	testq	%rbx, %rbx
	js	.L689
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L690:
	movq	%rcx, -10008(%rbp)
	movss	%xmm4, -9960(%rbp)
	call	log2@PLT
	pxor	%xmm4, %xmm4
	movq	-10008(%rbp), %rcx
	cvtsd2ss	%xmm0, %xmm0
	addss	.LC8(%rip), %xmm0
	movss	%xmm4, -9992(%rbp)
	movss	-9960(%rbp), %xmm4
	movss	%xmm0, -9976(%rbp)
	jmp	.L687
.L676:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L677
.L655:
	movq	%xmm1, %rax
	movq	%xmm1, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L656
.L660:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L661
.L689:
	movq	%rbx, %rax
	andl	$1, %ebx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rbx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L690
.L642:
	movq	%xmm1, %rdx
	andl	$1, %eax
	pxor	%xmm2, %xmm2
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L643
.L894:
	movq	-9968(%rbp), %r15
	movq	-10040(%rbp), %r11
	cmpq	$127, %r15
	jbe	.L575
.L577:
	movq	-10136(%rbp), %rax
	movq	-10008(%rbp), %rsi
	movq	-10104(%rbp), %rbx
	movl	524336(%rax), %eax
	movl	%eax, (%rsi,%rbx,4)
	movq	-10112(%rbp), %rbx
	movl	%eax, (%rsi,%rbx,4)
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-9984(%rbp), %rbx
	movq	%r15, %rcx
	movq	%r11, %rsi
	movabsq	$1152921500580315135, %rax
	movq	%rax, -2768(%rbp)
	movl	$4, %eax
	movq	-10128(%rbp), %r12
	addq	$1, %rbx
	movq	-10192(%rbp), %rdi
	movaps	%xmm0, -2912(%rbp)
	cmpq	$4, %rbx
	movq	%r12, %r8
	movaps	%xmm0, -2896(%rbp)
	cmovb	%rax, %rbx
	movaps	%xmm0, -2880(%rbp)
	movaps	%xmm0, -2864(%rbp)
	movq	%rbx, %rdx
	movaps	%xmm0, -2848(%rbp)
	movaps	%xmm0, -2832(%rbp)
	movaps	%xmm0, -2816(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movaps	%xmm0, -2784(%rbp)
	call	BrotliFindAllStaticDictionaryMatches
	movdqa	.LC7(%rip), %xmm0
	testl	%eax, %eax
	je	.L591
	cmpq	$37, %r15
	movl	$37, %edx
	cmovbe	%r15, %rdx
	cmpq	%rdx, %rbx
	ja	.L591
	movq	-10016(%rbp), %rsi
	movq	-10120(%rbp), %r8
	movq	%r12, %rdi
	addq	$1, %rsi
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L755:
	movq	%rax, %rbx
.L594:
	movl	(%rdi,%rbx,4), %eax
	cmpl	$268435454, %eax
	ja	.L592
	movl	%eax, %ecx
	shrl	$5, %ecx
	addq	%rsi, %rcx
	cmpq	72(%r8), %rcx
	ja	.L592
	andl	$31, %eax
	movl	%ecx, 0(%r13)
	movl	%ebx, %ecx
	sall	$5, %ecx
	movl	%eax, %r9d
	orl	%ecx, %eax
	cmpq	%rbx, %r9
	cmovne	%eax, %ecx
	addq	$8, %r13
	movl	%ecx, -4(%r13)
.L592:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %rdx
	jne	.L755
.L591:
	movq	-10144(%rbp), %rdx
	movq	-10048(%rbp), %rcx
	movq	%r13, %rax
	subq	-10160(%rbp), %rax
	leaq	0(,%rdx,4), %rsi
	sarq	$3, %rax
	addq	%rsi, %rcx
	movq	%rsi, -10088(%rbp)
	movl	%eax, (%rcx)
	testq	%rax, %rax
	je	.L595
	movq	-10096(%rbp), %rsi
	addq	-10152(%rbp), %rax
	leaq	-8(%rsi,%rax,8), %r8
	movl	4(%r8), %esi
	movl	%esi, %edi
	shrl	$5, %edi
	cmpl	$10431, %esi
	ja	.L931
	movq	%rax, -10152(%rbp)
.L595:
	leaq	1(%rdx), %rax
	addq	$4, %rdx
	movq	%rax, -10144(%rbp)
	cmpq	-10000(%rbp), %rdx
	jb	.L623
	movq	%r14, %r15
.L558:
	movq	-10224(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -10088(%rbp)
	movq	-10216(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -10040(%rbp)
	movq	-10056(%rbp), %rax
	movdqu	(%rax), %xmm4
	movq	-10200(%rbp), %rax
	movq	(%rax), %rax
	movaps	%xmm4, -10080(%rbp)
	movq	%rax, -10032(%rbp)
	movq	-10000(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -10064(%rbp)
	cmpq	$-1, %rax
	jne	.L733
	movq	-10120(%rbp), %rax
	movq	$-1, -64(%rbp)
	movl	64(%rax), %ebx
	movl	$544, %eax
	cmpl	$544, %ebx
	cmovbe	%ebx, %eax
	xorl	%ebx, %ebx
	movl	%eax, %r12d
.L726:
	movq	-10000(%rbp), %rax
	movq	-10208(%rbp), %rdi
	leaq	8(,%rax,4), %rsi
	call	BrotliAllocate
	movq	%rax, %rdx
	movq	-10120(%rbp), %rax
	movq	%rdx, -80(%rbp)
	xorl	%edx, %edx
	movl	64(%rax), %eax
	testl	%eax, %eax
	je	.L626
.L914:
	movq	-10208(%rbp), %rdi
	leaq	0(,%rax,4), %rsi
	call	BrotliAllocate
	movq	%rax, %rdx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L927:
	movq	-10088(%rbp), %rdx
	addq	%r10, %rdx
	addq	$8, %r10
	subq	$1, %rax
	jne	.L581
	movq	%r10, %rax
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L579:
	andl	$7, %r11d
	je	.L582
	movzbl	(%r9,%rax), %edx
	cmpb	%dl, (%rdi)
	jne	.L582
	leaq	1(%rax), %rdx
	cmpq	$1, %r11
	je	.L754
	movzbl	1(%r9,%rax), %r10d
	cmpb	%r10b, 1(%rdi)
	jne	.L754
	leaq	2(%rax), %rdx
	cmpq	$2, %r11
	je	.L754
	movzbl	2(%r9,%rax), %r10d
	cmpb	%r10b, 2(%rdi)
	jne	.L754
	leaq	3(%rax), %rdx
	cmpq	$3, %r11
	je	.L754
	movzbl	3(%r9,%rax), %r10d
	cmpb	%r10b, 3(%rdi)
	jne	.L754
	leaq	4(%rax), %rdx
	cmpq	$4, %r11
	je	.L754
	movzbl	4(%r9,%rax), %r10d
	cmpb	%r10b, 4(%rdi)
	jne	.L754
	leaq	5(%rax), %rdx
	subq	$5, %r11
	je	.L754
	movzbl	5(%r9,%rax), %r10d
	cmpb	%r10b, 5(%rdi)
	jne	.L754
	leaq	6(%rax), %rdx
	cmpq	$1, %r11
	je	.L754
	movzbl	6(%r9,%rax), %r10d
	cmpb	%r10b, 6(%rdi)
	jne	.L754
	addq	$7, %rax
	jmp	.L582
.L754:
	movq	%rdx, %rax
	jmp	.L582
.L924:
	movq	-10176(%rbp), %rax
	testq	%rax, %rax
	je	.L560
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L561:
	movq	%r12, %rax
	addq	%r12, %r12
	cmpq	%r12, %r13
	ja	.L561
	xorl	%ebx, %ebx
	testq	%r12, %r12
	jne	.L932
.L728:
	movq	-10176(%rbp), %rdx
	movq	-10096(%rbp), %rsi
	movq	%rbx, %rdi
	salq	$3, %rdx
	call	memcpy@PLT
	movq	%r12, -10176(%rbp)
.L730:
	movq	-10096(%rbp), %rsi
	movq	-10208(%rbp), %rdi
	call	BrotliFree
	movq	%rbx, -10096(%rbp)
	movdqa	.LC7(%rip), %xmm0
	jmp	.L559
.L576:
	movq	-9960(%rbp), %r8
	movq	%rbx, %rax
	subq	%rsi, %rax
	movq	%rax, -10032(%rbp)
	andq	%rsi, %r8
	cmpq	%rax, -10016(%rbp)
	jb	.L575
	testq	%rax, %rax
	jne	.L578
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L928:
	movq	-9968(%rbp), %r15
	movq	-10040(%rbp), %r11
	cmpq	$127, %r15
	jbe	.L575
	movq	-10008(%rbp), %rbx
	andq	-10080(%rbp), %rsi
	movq	-10104(%rbp), %rdi
	movl	(%rbx,%rsi,8), %eax
	movl	%eax, (%rbx,%rdi,4)
	movl	4(%rbx,%rsi,8), %eax
	movq	-10112(%rbp), %rsi
	movl	%eax, (%rbx,%rsi,4)
	jmp	.L575
.L739:
	movq	$1, -9984(%rbp)
	jmp	.L565
.L560:
	xorl	%ebx, %ebx
	testq	%r13, %r13
	je	.L730
	movq	-10208(%rbp), %rdi
	leaq	0(,%r13,8), %rsi
	call	BrotliAllocate
	movq	%r13, -10176(%rbp)
	movq	%rax, %rbx
	jmp	.L730
.L931:
	movl	%edi, %eax
	movq	(%r8), %rdx
	movq	-10240(%rbp), %rbx
	leaq	-1(%rax), %rsi
	addq	$1, -10152(%rbp)
	movq	%rsi, -10104(%rbp)
	movq	-10160(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	-10024(%rbp), %rsi
	movl	$1, (%rcx)
	addq	%rsi, %rax
	leaq	1(%rsi), %rcx
	cmpq	%rbx, %rax
	movq	%rcx, -10008(%rbp)
	cmova	%rbx, %rax
	movq	%rcx, -9992(%rbp)
	movq	%rax, -10080(%rbp)
	movq	%rax, %rbx
	leaq	64(%rsi), %rax
	cmpq	%rbx, %rax
	ja	.L596
	subq	$63, %rbx
	leaq	513(%rsi), %rax
	movq	%rbx, -9992(%rbp)
	cmpq	%rax, %rbx
	jb	.L596
	cmpq	%rbx, %rcx
	jnb	.L596
	movq	-10136(%rbp), %rax
	movq	40(%rax), %rsi
	leaq	524344(%rax), %r15
	movq	%rsi, -10016(%rbp)
	subq	$15, %rsi
	movq	%rsi, -10032(%rbp)
	.p2align 4,,10
	.p2align 3
.L608:
	movq	-9960(%rbp), %rbx
	movq	-10008(%rbp), %rdi
	movq	-10136(%rbp), %rsi
	movq	%rbx, %rcx
	andq	%rdi, %rcx
	imull	$506832829, (%r14,%rcx), %eax
	movq	%rcx, -10024(%rbp)
	shrl	$15, %eax
	leaq	(%rsi,%rax,4), %rdx
	movq	-10016(%rbp), %rax
	movl	48(%rdx), %esi
	movl	%edi, 48(%rdx)
	andq	%rdi, %rax
	leaq	(%rax,%rax), %r10
	andq	%rsi, %rbx
	movq	%r10, -9968(%rbp)
	addq	$1, %r10
	movq	%rbx, %r11
	movq	%r10, -9976(%rbp)
	subq	%rsi, %rdi
	je	.L798
	movl	$64, %ebx
	addq	%r14, %rcx
	xorl	%r13d, %r13d
	movq	$0, -9984(%rbp)
	movq	%rcx, -10040(%rbp)
	movq	%rbx, %r12
	cmpq	-10032(%rbp), %rdi
	ja	.L798
	.p2align 4,,10
	.p2align 3
.L597:
	movq	-9984(%rbp), %rax
	movl	$128, %r10d
	cmpq	%rax, %r13
	movq	%rax, %rcx
	movq	-10024(%rbp), %rax
	cmovbe	%r13, %rcx
	subq	%rcx, %r10
	leaq	(%rax,%rcx), %r8
	leaq	(%rcx,%r11), %rdi
	movq	%r10, %rax
	addq	%r14, %rdi
	addq	%r14, %r8
	shrq	$3, %rax
	je	.L600
	leaq	8(%rdi), %rbx
	xorl	%r9d, %r9d
	movq	%rbx, -10064(%rbp)
.L602:
	movq	(%rdi,%r9), %rdx
	movq	(%r8,%r9), %rbx
	cmpq	%rbx, %rdx
	je	.L933
	xorq	%rbx, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r9, %rax
.L603:
	movq	-9968(%rbp), %rbx
	movq	-10016(%rbp), %rdi
	addq	%rcx, %rax
	leaq	(%r15,%rbx,4), %rcx
	movq	-9976(%rbp), %rbx
	andq	%rsi, %rdi
	leaq	(%rdi,%rdi), %r8
	leaq	(%r15,%rbx,4), %rdx
	cmpq	$127, %rax
	ja	.L934
	movq	-10040(%rbp), %rbx
	leaq	(%r14,%rax), %r9
	movzbl	(%r9,%r11), %r10d
	cmpb	%r10b, (%rbx,%rax)
	jbe	.L605
	movl	%esi, (%rcx)
	leaq	1(%r8), %rsi
	movq	-9960(%rbp), %r11
	subq	$1, %r12
	leaq	(%r15,%rsi,4), %rcx
	movq	%rsi, -9968(%rbp)
	movq	-10008(%rbp), %rdi
	movl	(%rcx), %esi
	andq	%rsi, %r11
	subq	%rsi, %rdi
	sete	%r8b
	cmpq	%rdi, -10032(%rbp)
	setb	%dil
	orb	%dil, %r8b
	jne	.L606
	testq	%r12, %r12
	je	.L606
	movq	%rax, %r13
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L605:
	movl	%esi, (%rdx)
	leaq	(%r15,%rdi,8), %rdx
	movq	-9960(%rbp), %r11
	subq	$1, %r12
	movl	(%rdx), %esi
	movq	-10008(%rbp), %rdi
	andq	%rsi, %r11
	subq	%rsi, %rdi
	sete	%r9b
	cmpq	%rdi, -10032(%rbp)
	setb	%dil
	orb	%dil, %r9b
	jne	.L606
	testq	%r12, %r12
	je	.L606
	movq	%rax, -9984(%rbp)
	movq	%r8, -9976(%rbp)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L933:
	movq	-10064(%rbp), %rbx
	leaq	(%rbx,%r9), %rdx
	addq	$8, %r9
	subq	$1, %rax
	jne	.L602
	movq	%r9, %rax
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L600:
	andl	$7, %r10d
	je	.L603
	movzbl	(%r8,%rax), %ebx
	cmpb	%bl, (%rdi)
	jne	.L603
	leaq	1(%rax), %rdx
	cmpq	$1, %r10
	je	.L771
	movzbl	1(%r8,%rax), %ebx
	cmpb	%bl, 1(%rdi)
	jne	.L771
	leaq	2(%rax), %rdx
	cmpq	$2, %r10
	je	.L771
	movzbl	2(%r8,%rax), %ebx
	cmpb	%bl, 2(%rdi)
	jne	.L771
	leaq	3(%rax), %rdx
	cmpq	$3, %r10
	je	.L771
	movzbl	3(%r8,%rax), %ebx
	cmpb	%bl, 3(%rdi)
	jne	.L771
	leaq	4(%rax), %rdx
	cmpq	$4, %r10
	je	.L771
	movzbl	4(%r8,%rax), %ebx
	cmpb	%bl, 4(%rdi)
	jne	.L771
	leaq	5(%rax), %rdx
	subq	$5, %r10
	je	.L771
	movzbl	5(%r8,%rax), %ebx
	cmpb	%bl, 5(%rdi)
	jne	.L771
	leaq	6(%rax), %rdx
	cmpq	$1, %r10
	je	.L771
	movzbl	6(%r8,%rax), %ebx
	cmpb	%bl, 6(%rdi)
	jne	.L771
	addq	$7, %rax
	jmp	.L603
.L771:
	movq	%rdx, %rax
	jmp	.L603
.L798:
	leaq	(%r15,%rax,8), %rcx
	movq	-9976(%rbp), %rax
	leaq	(%r15,%rax,4), %rdx
.L606:
	movq	-10136(%rbp), %rax
	movl	524336(%rax), %eax
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
.L599:
	addq	$8, -10008(%rbp)
	movq	-10008(%rbp), %rax
	cmpq	%rax, -9992(%rbp)
	ja	.L608
.L596:
	movq	-10080(%rbp), %rsi
	cmpq	%rsi, -9992(%rbp)
	jnb	.L622
	movq	-10136(%rbp), %rax
	movq	40(%rax), %rsi
	leaq	524344(%rax), %r15
	movq	%rsi, -10008(%rbp)
	subq	$15, %rsi
	movq	%rsi, -10024(%rbp)
	.p2align 4,,10
	.p2align 3
.L621:
	movq	-9960(%rbp), %rbx
	movq	-9992(%rbp), %rdi
	movq	-10136(%rbp), %rsi
	movq	%rbx, %rcx
	andq	%rdi, %rcx
	imull	$506832829, (%r14,%rcx), %eax
	movq	%rcx, -10016(%rbp)
	shrl	$15, %eax
	leaq	(%rsi,%rax,4), %rdx
	movq	-10008(%rbp), %rax
	movl	48(%rdx), %esi
	movl	%edi, 48(%rdx)
	andq	%rdi, %rax
	leaq	(%rax,%rax), %r10
	andq	%rsi, %rbx
	movq	%r10, -9968(%rbp)
	addq	$1, %r10
	subq	%rsi, %rdi
	movq	%rbx, %r11
	movq	%r10, -9976(%rbp)
	movq	%rdi, %rdx
	je	.L799
	movl	$64, %ebx
	leaq	(%r14,%rcx), %rdi
	xorl	%r13d, %r13d
	movq	$0, -9984(%rbp)
	movq	%rdi, -10032(%rbp)
	movq	%rbx, %r12
	cmpq	-10024(%rbp), %rdx
	ja	.L799
	.p2align 4,,10
	.p2align 3
.L610:
	movq	-9984(%rbp), %rax
	movl	$128, %r10d
	cmpq	%rax, %r13
	movq	%rax, %rcx
	movq	-10016(%rbp), %rax
	cmovbe	%r13, %rcx
	subq	%rcx, %r10
	leaq	(%rax,%rcx), %r8
	leaq	(%rcx,%r11), %rdi
	movq	%r10, %rax
	addq	%r14, %rdi
	addq	%r14, %r8
	shrq	$3, %rax
	je	.L613
	leaq	8(%rdi), %rbx
	xorl	%r9d, %r9d
	movq	%rbx, -10040(%rbp)
.L615:
	movq	(%rdi,%r9), %rdx
	movq	(%r8,%r9), %rbx
	cmpq	%rbx, %rdx
	je	.L935
	xorq	%rbx, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r9, %rax
.L616:
	movq	-9968(%rbp), %rbx
	movq	-10008(%rbp), %rdi
	addq	%rcx, %rax
	leaq	(%r15,%rbx,4), %rcx
	movq	-9976(%rbp), %rbx
	andq	%rsi, %rdi
	leaq	(%rdi,%rdi), %r8
	leaq	(%r15,%rbx,4), %rdx
	cmpq	$127, %rax
	ja	.L936
	movq	-10032(%rbp), %rbx
	leaq	(%r14,%rax), %r9
	movzbl	(%r9,%r11), %r10d
	cmpb	%r10b, (%rbx,%rax)
	jbe	.L618
	leaq	1(%r8), %rbx
	movl	%esi, (%rcx)
	subq	$1, %r12
	movq	-9960(%rbp), %r11
	leaq	(%r15,%rbx,4), %rcx
	movq	-9992(%rbp), %rdi
	movq	%rbx, -9968(%rbp)
	movl	(%rcx), %esi
	andq	%rsi, %r11
	subq	%rsi, %rdi
	sete	%r8b
	cmpq	%rdi, -10024(%rbp)
	setb	%dil
	orb	%dil, %r8b
	jne	.L619
	testq	%r12, %r12
	je	.L619
	movq	%rax, %r13
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L618:
	movl	%esi, (%rdx)
	leaq	(%r15,%rdi,8), %rdx
	movq	-9960(%rbp), %r11
	subq	$1, %r12
	movl	(%rdx), %esi
	movq	-9992(%rbp), %rdi
	andq	%rsi, %r11
	subq	%rsi, %rdi
	sete	%r9b
	cmpq	%rdi, -10024(%rbp)
	setb	%dil
	orb	%dil, %r9b
	jne	.L619
	testq	%r12, %r12
	je	.L619
	movq	%rax, -9984(%rbp)
	movq	%r8, -9976(%rbp)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L935:
	movq	-10040(%rbp), %rbx
	leaq	(%rbx,%r9), %rdx
	addq	$8, %r9
	subq	$1, %rax
	jne	.L615
	movq	%r9, %rax
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L613:
	andl	$7, %r10d
	je	.L616
	movzbl	(%r8,%rax), %ebx
	cmpb	%bl, (%rdi)
	jne	.L616
	leaq	1(%rax), %rdx
	cmpq	$1, %r10
	je	.L784
	movzbl	1(%r8,%rax), %ebx
	cmpb	%bl, 1(%rdi)
	jne	.L784
	leaq	2(%rax), %rdx
	cmpq	$2, %r10
	je	.L784
	movzbl	2(%r8,%rax), %ebx
	cmpb	%bl, 2(%rdi)
	jne	.L784
	leaq	3(%rax), %rdx
	cmpq	$3, %r10
	je	.L784
	movzbl	3(%r8,%rax), %ebx
	cmpb	%bl, 3(%rdi)
	jne	.L784
	leaq	4(%rax), %rdx
	cmpq	$4, %r10
	je	.L784
	movzbl	4(%r8,%rax), %ebx
	cmpb	%bl, 4(%rdi)
	jne	.L784
	leaq	5(%rax), %rdx
	subq	$5, %r10
	je	.L784
	movzbl	5(%r8,%rax), %ebx
	cmpb	%bl, 5(%rdi)
	jne	.L784
	leaq	6(%rax), %rdx
	cmpq	$1, %r10
	je	.L784
	movzbl	6(%r8,%rax), %ebx
	cmpb	%bl, 6(%rdi)
	jne	.L784
	addq	$7, %rax
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L784:
	movq	%rdx, %rax
	jmp	.L616
.L799:
	leaq	(%r15,%rax,8), %rcx
	movq	-9976(%rbp), %rax
	leaq	(%r15,%rax,4), %rdx
	.p2align 4,,10
	.p2align 3
.L619:
	movq	-10136(%rbp), %rax
	movl	524336(%rax), %eax
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
.L612:
	addq	$1, -9992(%rbp)
	movq	-9992(%rbp), %rax
	cmpq	-10080(%rbp), %rax
	jne	.L621
.L622:
	movq	-10104(%rbp), %rbx
	movq	-10088(%rbp), %rsi
	movq	-10048(%rbp), %rax
	leaq	0(,%rbx,4), %rdx
	leaq	4(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	addq	-10144(%rbp), %rbx
	movdqa	.LC7(%rip), %xmm0
	movq	%rbx, %rdx
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L936:
	movl	(%r15,%rdi,8), %eax
	movl	%eax, (%rcx)
	movl	4(%r15,%r8,4), %eax
	movl	%eax, (%rdx)
	jmp	.L612
.L934:
	movl	(%r15,%rdi,8), %eax
	movl	%eax, (%rcx)
	movl	4(%r15,%r8,4), %eax
	movl	%eax, (%rdx)
	jmp	.L599
.L925:
	subq	$1, -9968(%rbp)
	movq	-10008(%rbp), %rsi
	leaq	(%rsi,%r10), %rbx
	leaq	8(%r10), %rsi
	je	.L937
	movq	%rsi, %r10
	jmp	.L570
.L568:
	cmpq	$0, -9992(%rbp)
	je	.L567
	cmpq	$0, -10032(%rbp)
	movq	-10040(%rbp), %rbx
	movl	$1, %r10d
	je	.L567
.L731:
	movzbl	(%rcx,%r10), %esi
	cmpb	%sil, (%rbx)
	jne	.L742
	cmpq	$2, -9992(%rbp)
	leaq	1(%r10), %rsi
	je	.L572
	movzbl	1(%rcx,%r10), %r9d
	cmpb	%r9b, 1(%rbx)
	jne	.L572
	cmpq	$3, -9992(%rbp)
	leaq	2(%r10), %rsi
	je	.L572
	movzbl	2(%rcx,%r10), %r9d
	cmpb	%r9b, 2(%rbx)
	jne	.L572
	cmpq	$4, -9992(%rbp)
	leaq	3(%r10), %rsi
	je	.L572
	movzbl	3(%rcx,%r10), %r9d
	cmpb	%r9b, 3(%rbx)
	jne	.L572
	cmpq	$5, -9992(%rbp)
	leaq	4(%r10), %rsi
	je	.L572
	movzbl	4(%rcx,%r10), %r9d
	cmpb	%r9b, 4(%rbx)
	jne	.L572
	cmpq	$6, -9992(%rbp)
	leaq	5(%r10), %rsi
	je	.L572
	movzbl	5(%rcx,%r10), %r9d
	cmpb	%r9b, 5(%rbx)
	jne	.L572
	cmpq	$7, -9992(%rbp)
	leaq	6(%r10), %rsi
	je	.L572
	movzbl	6(%rbx), %ebx
	cmpb	%bl, 6(%rcx,%r10)
	jne	.L572
	leaq	7(%r10), %rsi
	jmp	.L572
.L707:
	movq	-10024(%rbp), %rax
	testl	$134217727, 8(%rax)
	je	.L724
	movq	-10000(%rbp), %rcx
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L922:
	movq	$0, -10096(%rbp)
	jmp	.L556
.L788:
	pxor	%xmm4, %xmm4
	xorl	%ebx, %ebx
	movss	%xmm4, -9992(%rbp)
	jmp	.L670
.L789:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	jmp	.L671
.L790:
	xorl	%eax, %eax
	jmp	.L678
.L791:
	movq	-10168(%rbp), %rdi
	movq	-9984(%rbp), %r8
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	jmp	.L704
.L919:
	movl	$-1, 12(%rax)
	movq	-10000(%rbp), %rcx
	movq	-9968(%rbp), %rbx
	jmp	.L723
.L937:
	cmpq	$0, -9992(%rbp)
	movq	%r9, -9968(%rbp)
	je	.L572
	movzbl	(%rcx,%rsi), %r9d
	cmpb	%r9b, (%rbx)
	jne	.L572
	addq	$1, %rbx
	addq	$9, %r10
	cmpq	$0, -10032(%rbp)
	jne	.L731
.L742:
	movq	%r10, %rsi
	jmp	.L572
.L932:
	salq	$4, %rax
	movq	-10208(%rbp), %rdi
	movq	%rax, %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	jmp	.L728
.L923:
	leaq	-2912(%rbp), %rax
	movq	%rax, -10128(%rbp)
	jmp	.L558
.L920:
	call	__stack_chk_fail@PLT
.L554:
	cmpq	$0, -10176(%rbp)
	movq	%rax, -10240(%rbp)
	jne	.L555
	movq	$0, -10096(%rbp)
	jmp	.L557
	.cfi_endproc
.LFE309:
	.size	BrotliCreateHqZopfliBackwardReferences, .-BrotliCreateHqZopfliBackwardReferences
	.section	.rodata
	.align 32
	.type	kDistanceCacheOffset, @object
	.size	kDistanceCacheOffset, 64
kDistanceCacheOffset:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	-1
	.long	1
	.long	-2
	.long	2
	.long	-3
	.long	3
	.long	-1
	.long	1
	.long	-2
	.long	2
	.long	-3
	.long	3
	.align 32
	.type	kDistanceCacheIndex, @object
	.size	kDistanceCacheIndex, 64
kDistanceCacheIndex:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.align 32
	.type	kCopyExtra, @object
	.size	kCopyExtra, 96
kCopyExtra:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	24
	.align 32
	.type	kInsExtra, @object
	.size	kInsExtra, 96
kInsExtra:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	12
	.long	14
	.long	24
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	1094713344
	.align 4
.LC3:
	.long	1096810496
	.align 4
.LC4:
	.long	1103101952
	.align 4
.LC5:
	.long	1065353216
	.align 4
.LC6:
	.long	2130692510
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	268435455
	.long	268435455
	.long	268435455
	.long	268435455
	.section	.rodata.cst4
	.align 4
.LC8:
	.long	1073741824
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	1
	.long	1
	.long	1
	.long	1
	.hidden	BrotliFindAllStaticDictionaryMatches
	.hidden	BrotliFree
	.hidden	BrotliAllocate
	.hidden	BrotliEstimateBitCostsForLiterals
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
