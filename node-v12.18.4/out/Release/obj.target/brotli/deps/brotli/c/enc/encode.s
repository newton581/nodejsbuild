	.file	"encode.c"
	.text
	.p2align 4
	.type	InjectFlushOrPushOutput, @function
InjectFlushOrPushOutput:
.LFB344:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpl	$1, 5444(%rdi)
	je	.L20
.L2:
	movq	5408(%rbx), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1
	movq	(%r14), %r12
	testq	%r12, %r12
	jne	.L21
.L1:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movzbl	346(%rdi), %eax
	testb	%al, %al
	je	.L2
	xorl	%edx, %edx
	movzwl	344(%rdi), %edi
	movl	%eax, %ecx
	movzbl	%al, %esi
	movw	%dx, 344(%rbx)
	movl	$6, %edx
	sall	%cl, %edx
	movb	$0, 346(%rbx)
	movl	%edx, %eax
	movq	5400(%rbx), %rdx
	orl	%edi, %eax
	leaq	6(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L3
	addq	5408(%rbx), %rdx
.L4:
	movb	%al, (%rdx)
	cmpq	$8, %rdi
	jbe	.L6
	movb	%ah, 1(%rdx)
	cmpq	$16, %rdi
	jbe	.L6
	shrl	$16, %eax
	movb	%al, 2(%rdx)
.L6:
	addq	$13, %rsi
	movl	$1, %r8d
	shrq	$3, %rsi
	addq	%rsi, 5408(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	cmpq	%r12, %rax
	movq	0(%r13), %rdi
	movq	5400(%rbx), %rsi
	movq	%rcx, -40(%rbp)
	cmovbe	%rax, %r12
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, 0(%r13)
	movq	-40(%rbp), %rcx
	movl	$1, %r8d
	subq	%r12, (%r14)
	movq	5416(%rbx), %rax
	addq	%r12, 5400(%rbx)
	addq	%r12, %rax
	subq	%r12, 5408(%rbx)
	movq	%rax, 5416(%rbx)
	testq	%rcx, %rcx
	je	.L1
	movq	%rax, (%rcx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	5424(%rbx), %rdx
	movq	%rdx, 5400(%rbx)
	jmp	.L4
	.cfi_endproc
.LFE344:
	.size	InjectFlushOrPushOutput, .-InjectFlushOrPushOutput
	.p2align 4
	.type	ShouldCompress.part.0, @function
ShouldCompress.part.0:
.LFB377:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rdi, %r10
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1080, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	js	.L23
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
.L24:
	testq	%r8, %r8
	js	.L26
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r8, %xmm0
.L27:
	movsd	.LC1(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	comisd	%xmm2, %xmm0
	jbe	.L58
	leaq	-1088(%rbp), %r13
	xorl	%eax, %eax
	movl	$128, %ecx
	mulsd	.LC2(%rip), %xmm1
	movq	%r13, %rdi
	rep stosq
	leaq	12(%rdx), %rdi
	divsd	.LC3(%rip), %xmm1
	movabsq	$5675921253449092805, %rdx
	movq	%rdi, %rax
	movsd	%xmm1, -1096(%rbp)
	mulq	%rdx
	movl	%r9d, %eax
	shrq	$2, %rdx
	cmpq	$12, %rdi
	jbe	.L31
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%eax, %edi
	addq	$1, %rcx
	addl	$13, %eax
	andl	%esi, %edi
	movzbl	(%r10,%rdi), %edi
	addl	$1, -1088(%rbp,%rdi,4)
	cmpq	%rcx, %rdx
	ja	.L29
.L31:
	pxor	%xmm1, %xmm1
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
	leaq	kLog2Table(%rip), %r15
.L30:
	movl	0(%r13), %r12d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r12, %xmm2
	cmpq	$255, %r12
	ja	.L34
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r15,%r12,4), %xmm0
.L35:
	mulsd	%xmm0, %xmm2
	movl	4(%r13), %eax
	addq	$8, %r13
	addq	%rax, %r12
	addq	%r12, %rbx
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	ja	.L38
	pxor	%xmm0, %xmm0
	cvtss2sd	(%r15,%rax,4), %xmm0
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cmpq	%r14, %r13
	jne	.L30
	testq	%rbx, %rbx
	js	.L41
.L61:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rbx, %xmm2
.L42:
	testq	%rbx, %rbx
	je	.L43
	cmpq	$255, %rbx
	ja	.L44
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rbx,4), %xmm0
.L45:
	mulsd	%xmm2, %xmm0
	xorl	%eax, %eax
	addsd	%xmm0, %xmm1
	maxsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	comisd	-1096(%rbp), %xmm1
	ja	.L22
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$1, %eax
.L22:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L60
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -1112(%rbp)
	movsd	%xmm2, -1104(%rbp)
	call	log2@PLT
	movsd	-1112(%rbp), %xmm1
	movsd	-1104(%rbp), %xmm2
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r8, %rax
	andl	$1, %r8d
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%r8, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rcx, %rax
	andl	$1, %ecx
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L43:
	comisd	%xmm1, %xmm2
	ja	.L58
	xorl	%eax, %eax
	comisd	-1096(%rbp), %xmm1
	jbe	.L58
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L38:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -1112(%rbp)
	movsd	%xmm2, -1104(%rbp)
	call	log2@PLT
	movsd	-1104(%rbp), %xmm2
	movsd	-1112(%rbp), %xmm1
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cmpq	%r14, %r13
	jne	.L30
	testq	%rbx, %rbx
	jns	.L61
.L41:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	pxor	%xmm2, %xmm2
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L44:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -1112(%rbp)
	movsd	%xmm2, -1104(%rbp)
	call	log2@PLT
	movsd	-1112(%rbp), %xmm1
	movsd	-1104(%rbp), %xmm2
	jmp	.L45
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE377:
	.size	ShouldCompress.part.0, .-ShouldCompress.part.0
	.p2align 4
	.type	WriteMetaBlockInternal, @function
WriteMetaBlockInternal:
.LFB326:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movq	%rcx, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$2472, %rsp
	.cfi_offset 3, -56
	movq	64(%rbp), %rax
	movq	24(%rbp), %rbx
	movl	%r9d, -2236(%rbp)
	movl	40(%rbp), %r10d
	movl	32(%rbp), %r9d
	movq	%rdi, -2256(%rbp)
	movq	%rax, -2264(%rbp)
	movq	72(%rbp), %rax
	movq	88(%rbp), %r11
	movq	%rax, -2272(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -2280(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -2232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	shrq	$30, %rax
	cmpq	$2, %rax
	jbe	.L63
	leal	-1(%rax), %r15d
	andl	$1073741823, %ecx
	andl	$1, %r15d
	addl	$1, %r15d
	sall	$30, %r15d
	orl	%ecx, %r15d
.L63:
	movdqu	(%rbx), %xmm4
	movdqu	16(%rbx), %xmm5
	movdqu	32(%rbx), %xmm6
	movdqu	48(%rbx), %xmm7
	movdqu	64(%rbx), %xmm3
	movaps	%xmm4, -2224(%rbp)
	movdqu	80(%rbx), %xmm4
	movaps	%xmm5, -2208(%rbp)
	movdqu	96(%rbx), %xmm5
	movaps	%xmm6, -2192(%rbp)
	movdqu	112(%rbx), %xmm6
	movaps	%xmm7, -2176(%rbp)
	movaps	%xmm3, -2160(%rbp)
	movaps	%xmm4, -2144(%rbp)
	movaps	%xmm5, -2128(%rbp)
	movaps	%xmm6, -2112(%rbp)
	testq	%r12, %r12
	je	.L271
	cmpq	$2, %r12
	jbe	.L68
	movq	%r12, %rax
	shrq	$8, %rax
	addq	$2, %rax
	cmpq	%rax, 56(%rbp)
	jnb	.L67
	movq	48(%rbp), %r8
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r11, -2304(%rbp)
	movl	%r10d, -2296(%rbp)
	movl	%r9d, -2288(%rbp)
	call	ShouldCompress.part.0
	movl	-2288(%rbp), %r9d
	movl	-2296(%rbp), %r10d
	testl	%eax, %eax
	movq	-2304(%rbp), %r11
	je	.L68
.L67:
	movq	-2232(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -2304(%rbp)
	movq	(%r11), %rax
	movq	%rax, -2296(%rbp)
	movl	4(%rbx), %eax
	cmpl	$2, %eax
	jle	.L272
	cmpl	$3, %eax
	je	.L273
	leaq	-2096(%rbp), %rax
	movq	%r11, -2320(%rbp)
	movq	%rax, %rdi
	movl	%r10d, -2240(%rbp)
	movl	%r9d, -2308(%rbp)
	movq	%rax, -2288(%rbp)
	call	BrotliInitBlockSplit
	leaq	-2048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2328(%rbp)
	call	BrotliInitBlockSplit
	leaq	-2000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2336(%rbp)
	call	BrotliInitBlockSplit
	movl	-2240(%rbp), %r10d
	movl	4(%rbx), %eax
	movq	$0, -1952(%rbp)
	movl	-2308(%rbp), %r9d
	movq	$0, -1944(%rbp)
	movzbl	%r10b, %esi
	cmpl	$9, %eax
	movq	$0, -1936(%rbp)
	movq	-2320(%rbp), %r11
	movl	%esi, -2240(%rbp)
	movzbl	%r9b, %esi
	movq	$0, -1928(%rbp)
	movq	$0, -1920(%rbp)
	movq	$0, -1912(%rbp)
	movq	$0, -1904(%rbp)
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	$0, -1880(%rbp)
	movl	%eax, -2312(%rbp)
	movl	%esi, -2308(%rbp)
	jg	.L72
	movl	24(%rbx), %ecx
	xorl	%eax, %eax
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.L274
.L73:
	pushq	-2288(%rbp)
	movq	%r14, %rcx
	movq	%r13, %rsi
	movl	-2308(%rbp), %r8d
	movl	-2240(%rbp), %r9d
	pushq	56(%rbp)
	movq	-2256(%rbp), %rdi
	pushq	-2264(%rbp)
	pushq	%rax
	movl	16(%rbp), %eax
	pushq	%rdx
	leaq	kContextLookup(%rip), %rdx
	movq	%r11, -2320(%rbp)
	sall	$9, %eax
	addq	%rdx, %rax
	movq	%r15, %rdx
	pushq	%rax
	call	BrotliBuildMetaBlockGreedy
	movq	-2320(%rbp), %r11
	addq	$48, %rsp
	leaq	-2224(%rbp), %r8
.L218:
	cmpl	$3, 4(%rbx)
	jg	.L275
.L219:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	pushq	-2232(%rbp)
	movq	%r12, %rcx
	movq	-2256(%rbp), %rbx
	pushq	%r11
	movq	%r15, %rdx
	movq	%r13, %rsi
	pushq	-2288(%rbp)
	movl	-2308(%rbp), %r9d
	pushq	56(%rbp)
	movq	%rbx, %rdi
	pushq	-2264(%rbp)
	pushq	%rax
	movl	-2236(%rbp), %eax
	pushq	%r8
	movq	%r14, %r8
	movq	%r11, -2320(%rbp)
	pushq	%rax
	movl	-2240(%rbp), %eax
	pushq	%rax
	call	BrotliStoreMetaBlock
	movq	-2288(%rbp), %rsi
	addq	$80, %rsp
	movq	%rbx, %rdi
	call	BrotliDestroyBlockSplit
	movq	-2328(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliDestroyBlockSplit
	movq	-2336(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliDestroyBlockSplit
	movq	-1952(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-1936(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -1952(%rbp)
	call	BrotliFree
	movq	-1920(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -1936(%rbp)
	call	BrotliFree
	movq	-1904(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -1920(%rbp)
	call	BrotliFree
	movq	-1888(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -1904(%rbp)
	call	BrotliFree
	movq	-2320(%rbp), %r11
.L70:
	movq	(%r11), %rax
	leaq	4(%r12), %rdx
	shrq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L62
	movq	-2272(%rbp), %rax
	movq	%r15, %rdx
	movq	%r11, %r9
	movq	%r12, %r8
	movq	-2232(%rbp), %rsi
	subq	$8, %rsp
	movl	-2236(%rbp), %edi
	movq	%r14, %rcx
	movdqu	(%rax), %xmm7
	movq	-2280(%rbp), %rax
	movups	%xmm7, (%rax)
	movzwl	-2304(%rbp), %eax
	movaps	%xmm7, -2256(%rbp)
	movw	%ax, (%rsi)
	movzbl	-2296(%rbp), %eax
	movq	%rax, (%r11)
	pushq	%rsi
	movq	%r13, %rsi
	call	BrotliStoreUncompressedMetaBlock
	popq	%rax
	popq	%rdx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-2272(%rbp), %rax
	movq	%r13, %rsi
	movl	%r15d, %edx
	movq	%r11, %r9
	subq	$8, %rsp
	movl	-2236(%rbp), %edi
	movq	%r12, %r8
	movq	%r14, %rcx
	movdqu	(%rax), %xmm7
	movq	-2280(%rbp), %rax
	movups	%xmm7, (%rax)
	pushq	-2232(%rbp)
	movaps	%xmm7, -2256(%rbp)
	call	BrotliStoreUncompressedMetaBlock
	popq	%rsi
	popq	%rdi
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	(%r11), %rax
	movl	$3, %edx
	movq	%rax, %rsi
	movl	%eax, %ecx
	addq	$9, %rax
	shrq	$3, %rsi
	addq	-2232(%rbp), %rsi
	andl	$7, %ecx
	andl	$4294967288, %eax
	movzbl	(%rsi), %edi
	salq	%cl, %rdx
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	movq	%rax, (%r11)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L273:
	subq	$8, %rsp
	pushq	-2232(%rbp)
	movq	%r14, %r8
	movq	%r12, %rcx
	movl	-2236(%rbp), %r9d
	pushq	%r11
	movq	%r15, %rdx
	movq	%r13, %rsi
	pushq	56(%rbp)
	movq	-2256(%rbp), %rdi
	pushq	-2264(%rbp)
	pushq	%rbx
	movq	%r11, -2288(%rbp)
	call	BrotliStoreMetaBlockTrivial
	movq	-2288(%rbp), %r11
	addq	$48, %rsp
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L272:
	subq	$8, %rsp
	pushq	-2232(%rbp)
	movq	%r14, %r8
	movq	%r12, %rcx
	movl	-2236(%rbp), %r9d
	pushq	%r11
	movq	%r15, %rdx
	movq	%r13, %rsi
	pushq	56(%rbp)
	movq	-2256(%rbp), %rdi
	pushq	-2264(%rbp)
	pushq	%rbx
	movq	%r11, -2288(%rbp)
	call	BrotliStoreMetaBlockFast
	movq	-2288(%rbp), %r11
	addq	$48, %rsp
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L275:
	movl	-2160(%rbp), %edi
	movl	$544, %eax
	movq	-2288(%rbp), %rsi
	movq	%r8, -2344(%rbp)
	movq	%r11, -2320(%rbp)
	cmpl	$544, %edi
	cmova	%eax, %edi
	call	BrotliOptimizeHistograms
	movq	-2344(%rbp), %r8
	movq	-2320(%rbp), %r11
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L72:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	pushq	-2288(%rbp)
	movl	%esi, %r9d
	movq	-2256(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	pushq	%rax
	movl	-2240(%rbp), %eax
	leaq	-2224(%rbp), %r8
	pushq	56(%rbp)
	pushq	-2264(%rbp)
	pushq	%rax
	movq	%r11, -2344(%rbp)
	movq	%r8, -2320(%rbp)
	call	BrotliBuildMetaBlock
	movq	-2344(%rbp), %r11
	movq	-2320(%rbp), %r8
	addq	$48, %rsp
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L274:
	cmpl	$4, -2312(%rbp)
	jle	.L222
	cmpq	$63, %r12
	jbe	.L222
	leaq	(%r12,%r15), %rsi
	cmpq	$1048575, 16(%rbx)
	movq	%rsi, -2384(%rbp)
	leaq	64(%r15), %rsi
	movq	%rsi, -2376(%rbp)
	jbe	.L277
	leaq	-1856(%rbp), %rdi
	movl	$16, %ecx
	leaq	64(%r15), %r8
	movq	%rdi, -2352(%rbp)
	rep stosq
	leaq	-1728(%rbp), %rdi
	movl	$208, %ecx
	movq	%rdi, -2400(%rbp)
	rep stosq
	leaq	(%r12,%r15), %rax
	cmpq	%r8, %rax
	jb	.L223
	leaq	2(%r15), %rsi
	leaq	1280+kContextLookup(%rip), %r10
	movq	%r15, -2344(%rbp)
	movl	%ecx, %r15d
	movq	%r12, -2360(%rbp)
	leaq	-256(%r10), %r9
	movq	%rsi, %r12
	movq	%r11, -2368(%rbp)
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	-64(%r8), %rax
	leaq	-63(%r8), %rdx
	andq	%r14, %rax
	andq	%r14, %rdx
	movzbl	0(%r13,%rax), %eax
	movzbl	0(%r13,%rdx), %esi
	cmpq	%r12, %r8
	jbe	.L76
	movq	%r11, -2320(%rbp)
	movq	%r12, %rcx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L224:
	movl	%edx, %esi
.L77:
	movq	%r14, %rdx
	movzbl	%sil, %edi
	movzbl	(%r10,%rax), %eax
	leaq	kStaticContextMapComplexUTF8.6859(%rip), %r11
	andq	%rcx, %rdx
	orb	(%r9,%rdi), %al
	addq	$1, %rcx
	movzbl	0(%r13,%rdx), %edx
	movzbl	%al, %eax
	movzbl	(%r11,%rax,4), %eax
	movl	%edx, %edi
	shrb	$3, %dil
	salq	$5, %rax
	andl	$31, %edi
	addq	%rdi, %rax
	addl	$1, -1856(%rbp,%rdi,4)
	addl	$1, -1728(%rbp,%rax,4)
	movzbl	%sil, %eax
	cmpq	%r8, %rcx
	jne	.L224
	movq	-2320(%rbp), %r11
	addl	$62, %r15d
.L76:
	addq	$4096, %r8
	addq	$4096, %r12
	cmpq	%r11, %r8
	jbe	.L78
	movl	%r15d, -2392(%rbp)
	movq	-2360(%rbp), %r12
	movq	-2344(%rbp), %r15
	movq	-2368(%rbp), %r11
.L75:
	movq	-2352(%rbp), %rax
	movq	%rbx, -2360(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r14, -2320(%rbp)
	leaq	kLog2Table(%rip), %rsi
	movq	%r13, %r14
	movq	%rdx, %r13
	movq	%r12, -2352(%rbp)
	movq	%rax, %r12
	movsd	%xmm0, -2344(%rbp)
	movsd	%xmm0, -2368(%rbp)
.L88:
	movl	(%r12), %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rbx, %xmm1
	cmpq	$255, %rbx
	ja	.L81
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rsi,%rbx,4), %xmm0
.L82:
	mulsd	%xmm0, %xmm1
	movl	4(%r12), %eax
	movsd	-2368(%rbp), %xmm2
	addq	$8, %r12
	leaq	(%rbx,%rax), %rcx
	addq	%rcx, %r13
	subsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpq	$255, %rax
	ja	.L85
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rsi,%rax,4), %xmm0
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, -2368(%rbp)
	cmpq	-2400(%rbp), %r12
	jne	.L88
.L87:
	movq	%r13, %rdx
	movq	-2352(%rbp), %r12
	movq	%r14, %r13
	movq	-2360(%rbp), %rbx
	movq	-2320(%rbp), %r14
	testq	%rdx, %rdx
	je	.L89
	js	.L90
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
.L91:
	cmpq	$255, %rdx
	ja	.L92
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L93:
	mulsd	%xmm0, %xmm1
	addsd	-2368(%rbp), %xmm1
	movsd	%xmm1, -2368(%rbp)
.L89:
	movsd	-2344(%rbp), %xmm0
	leaq	-1600(%rbp), %rcx
	leaq	64(%rbp), %rax
	movq	%r15, -2400(%rbp)
	movq	%rbx, -2432(%rbp)
	movq	%rax, %r15
	movq	%rcx, %rbx
	movq	%r13, -2408(%rbp)
	movq	%r14, -2416(%rbp)
	movq	%r12, -2424(%rbp)
	movq	%r11, -2440(%rbp)
	movsd	%xmm0, -2320(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r14,4), %xmm0
.L97:
	mulsd	%xmm0, %xmm2
	movl	4(%r13), %eax
	addq	$8, %r13
	addq	%rax, %r14
	addq	%r14, %r12
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	ja	.L100
	leaq	kLog2Table(%rip), %rsi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rsi,%rax,4), %xmm0
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cmpq	%rbx, %r13
	jne	.L103
.L102:
	testq	%r12, %r12
	je	.L104
	js	.L105
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r12, %xmm2
.L106:
	cmpq	$255, %r12
	ja	.L107
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%r12,4), %xmm0
.L108:
	mulsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
.L104:
	addsd	-2320(%rbp), %xmm1
	subq	$-128, %rbx
	movsd	%xmm1, -2320(%rbp)
	cmpq	%rbx, %r15
	je	.L278
.L109:
	leaq	-128(%rbx), %r13
	pxor	%xmm1, %xmm1
	xorl	%r12d, %r12d
.L103:
	movl	0(%r13), %r14d
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%r14, %xmm2
	cmpq	$255, %r14
	jbe	.L279
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -2360(%rbp)
	movsd	%xmm2, -2352(%rbp)
	call	log2@PLT
	movsd	-2360(%rbp), %xmm1
	movsd	-2352(%rbp), %xmm2
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L100:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -2360(%rbp)
	movsd	%xmm2, -2352(%rbp)
	call	log2@PLT
	movsd	-2352(%rbp), %xmm2
	movsd	-2360(%rbp), %xmm1
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cmpq	%rbx, %r13
	jne	.L103
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L107:
	movapd	%xmm2, %xmm0
	movsd	%xmm1, -2360(%rbp)
	movsd	%xmm2, -2352(%rbp)
	call	log2@PLT
	movsd	-2360(%rbp), %xmm1
	movsd	-2352(%rbp), %xmm2
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%r12, %rax
	movq	%r12, %rdx
	pxor	%xmm2, %xmm2
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L222:
	xorl	%eax, %eax
	movl	$1, %edx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L278:
	movl	-2392(%rbp), %eax
	pxor	%xmm0, %xmm0
	movsd	.LC4(%rip), %xmm1
	movq	-2400(%rbp), %r15
	movq	-2408(%rbp), %r13
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm1, -2392(%rbp)
	movq	-2416(%rbp), %r14
	movq	-2424(%rbp), %r12
	movq	-2432(%rbp), %rbx
	movq	-2440(%rbp), %r11
	divsd	%xmm0, %xmm1
	movsd	-2320(%rbp), %xmm0
	mulsd	%xmm1, %xmm0
	comisd	.LC5(%rip), %xmm0
	ja	.L280
	mulsd	-2368(%rbp), %xmm1
	leaq	kStaticContextMapComplexUTF8.6859(%rip), %rax
	movl	$13, %edx
	subsd	%xmm0, %xmm1
	movsd	.LC6(%rip), %xmm0
	movsd	%xmm0, -2400(%rbp)
	comisd	%xmm1, %xmm0
	jbe	.L73
.L74:
	movq	-2376(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movl	$0, -1696(%rbp)
	leaq	1(%r15), %rdi
	movq	-2384(%rbp), %r9
	movaps	%xmm0, -1728(%rbp)
	leaq	lut.6895(%rip), %rsi
	movaps	%xmm0, -1712(%rbp)
	movq	%rcx, %r8
	cmpq	%rcx, -2384(%rbp)
	jb	.L120
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	-64(%r8), %rax
	andq	%r14, %rax
	movzbl	0(%r13,%rax), %eax
	shrb	$6, %al
	andl	$3, %eax
	movl	(%rsi,%rax,4), %eax
	leal	(%rax,%rax,2), %eax
	cmpq	%rdi, %r8
	jbe	.L121
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r14, %rdx
	andq	%rcx, %rdx
	addq	$1, %rcx
	movzbl	0(%r13,%rdx), %edx
	shrb	$6, %dl
	andl	$3, %edx
	movl	(%rsi,%rdx,4), %edx
	addl	%edx, %eax
	cltq
	addl	$1, -1728(%rbp,%rax,4)
	leal	(%rdx,%rdx,2), %eax
	cmpq	%r8, %rcx
	jne	.L118
.L121:
	addq	$4096, %r8
	addq	$4096, %rdi
	cmpq	%r9, %r8
	jbe	.L119
.L120:
	pxor	%xmm0, %xmm0
	movl	-1716(%rbp), %eax
	movl	-1728(%rbp), %edx
	pxor	%xmm1, %xmm1
	movaps	%xmm0, -1856(%rbp)
	movl	-1844(%rbp), %edi
	movl	-1724(%rbp), %esi
	movl	%edx, %r10d
	movl	%eax, -2476(%rbp)
	movl	-1712(%rbp), %r9d
	addl	%eax, %edi
	addl	%edx, %eax
	movl	%edx, -2488(%rbp)
	movl	-1720(%rbp), %ecx
	movl	%edi, -2472(%rbp)
	movl	-1704(%rbp), %edi
	movl	%esi, -2484(%rbp)
	movl	-1708(%rbp), %r8d
	addl	%edi, %eax
	movl	%edi, -2432(%rbp)
	addl	%r10d, %edi
	movl	%eax, %edx
	movl	%eax, -2424(%rbp)
	movl	%esi, %eax
	addl	-1852(%rbp), %esi
	movl	%edi, -2416(%rbp)
	addl	%r9d, %eax
	movl	-1700(%rbp), %edi
	movl	%ecx, -2480(%rbp)
	addl	%edi, %eax
	movl	%r9d, -2464(%rbp)
	movl	%eax, -2368(%rbp)
	movl	%esi, %eax
	movl	-1696(%rbp), %esi
	addl	%edi, %eax
	movl	%r9d, -2456(%rbp)
	movl	%eax, -2384(%rbp)
	movl	%ecx, %eax
	addl	-1848(%rbp), %ecx
	addl	%r8d, %eax
	movl	%r8d, -2448(%rbp)
	addl	%esi, %eax
	movl	%r8d, -2440(%rbp)
	movl	%eax, -2320(%rbp)
	movl	%ecx, %eax
	addl	%esi, %eax
	movl	%edi, -2408(%rbp)
	movl	%eax, -2360(%rbp)
	movl	%edx, %eax
	cvtsi2sdq	%rax, %xmm1
	movl	%esi, -2376(%rbp)
	movq	$0, -1840(%rbp)
	cmpl	$255, %edx
	ja	.L281
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L122:
	mulsd	%xmm0, %xmm1
	movsd	-2344(%rbp), %xmm2
	movl	-2368(%rbp), %edx
	addq	%rdx, %rax
	subsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	cmpl	$255, %edx
	jbe	.L125
	movapd	%xmm1, %xmm0
	movq	%r11, -2512(%rbp)
	movq	%rax, -2504(%rbp)
	movsd	%xmm2, -2496(%rbp)
	movsd	%xmm1, -2352(%rbp)
	call	log2@PLT
	movsd	-2352(%rbp), %xmm1
	movsd	-2496(%rbp), %xmm2
	movq	-2504(%rbp), %rax
	movq	-2512(%rbp), %r11
.L126:
	mulsd	%xmm0, %xmm1
	movl	-2320(%rbp), %edx
	addq	%rdx, %rax
	subsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	cmpl	$255, %edx
	ja	.L129
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
.L130:
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, -2352(%rbp)
	testq	%rax, %rax
	je	.L131
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpq	$255, %rax
	ja	.L132
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L133:
	mulsd	%xmm0, %xmm1
	addsd	-2352(%rbp), %xmm1
	movsd	%xmm1, -2352(%rbp)
.L131:
	movl	-2416(%rbp), %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpl	$255, %eax
	jbe	.L136
	movapd	%xmm1, %xmm0
	movq	%r11, -2504(%rbp)
	movq	%rax, -2496(%rbp)
	movsd	%xmm1, -2416(%rbp)
	call	log2@PLT
	movsd	-2416(%rbp), %xmm1
	movq	-2496(%rbp), %rax
	movq	-2504(%rbp), %r11
.L137:
	mulsd	%xmm0, %xmm1
	movsd	-2344(%rbp), %xmm2
	movl	-2384(%rbp), %edx
	addq	%rdx, %rax
	subsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	cmpl	$255, %edx
	jbe	.L140
	movapd	%xmm1, %xmm0
	movq	%r11, -2504(%rbp)
	movq	%rax, -2496(%rbp)
	movsd	%xmm2, -2416(%rbp)
	movsd	%xmm1, -2384(%rbp)
	call	log2@PLT
	movsd	-2384(%rbp), %xmm1
	movsd	-2416(%rbp), %xmm2
	movq	-2496(%rbp), %rax
	movq	-2504(%rbp), %r11
.L141:
	mulsd	%xmm0, %xmm1
	movl	-2360(%rbp), %edx
	addq	%rdx, %rax
	subsd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	cmpl	$255, %edx
	ja	.L144
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
.L145:
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, -2360(%rbp)
	testq	%rax, %rax
	je	.L146
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpq	$255, %rax
	ja	.L147
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L148:
	mulsd	%xmm0, %xmm1
	addsd	-2360(%rbp), %xmm1
	movsd	%xmm1, -2360(%rbp)
.L146:
	movl	-2472(%rbp), %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	cmpl	$255, %eax
	jbe	.L151
	movapd	%xmm1, %xmm0
	movq	%r11, -2472(%rbp)
	movq	%rax, -2416(%rbp)
	movsd	%xmm1, -2384(%rbp)
	call	log2@PLT
	movsd	-2384(%rbp), %xmm1
	movq	-2416(%rbp), %rax
	movq	-2472(%rbp), %r11
.L152:
	mulsd	%xmm0, %xmm1
	movsd	-2344(%rbp), %xmm0
	movl	-2456(%rbp), %edx
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	addq	%rdx, %rax
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	cmpl	$255, %edx
	jbe	.L155
	movsd	%xmm0, -2416(%rbp)
	movapd	%xmm2, %xmm0
	movq	%r11, -2472(%rbp)
	movq	%rax, -2456(%rbp)
	movsd	%xmm2, -2384(%rbp)
	call	log2@PLT
	movsd	-2384(%rbp), %xmm2
	movsd	-2416(%rbp), %xmm1
	movq	-2456(%rbp), %rax
	movq	-2472(%rbp), %r11
.L156:
	mulsd	%xmm0, %xmm2
	movl	-2440(%rbp), %edx
	addq	%rdx, %rax
	subsd	%xmm2, %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
	cmpl	$255, %edx
	ja	.L159
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
.L160:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	testq	%rax, %rax
	je	.L161
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	cmpq	$255, %rax
	ja	.L162
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L163:
	mulsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
.L161:
	addsd	-2360(%rbp), %xmm1
	movl	-2488(%rbp), %edx
	movsd	%xmm1, -2360(%rbp)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	cmpq	$255, %rdx
	ja	.L166
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L167:
	mulsd	%xmm1, %xmm0
	movl	-2484(%rbp), %eax
	movsd	-2344(%rbp), %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	leaq	(%rax,%rdx), %rcx
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	ja	.L170
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L171:
	mulsd	%xmm2, %xmm0
	movl	-2480(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	addq	%rax, %rcx
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L174
	movapd	%xmm2, %xmm0
	movq	%r11, -2456(%rbp)
	movq	%rcx, -2416(%rbp)
	movsd	%xmm1, -2440(%rbp)
	movsd	%xmm2, -2384(%rbp)
	call	log2@PLT
	movsd	-2384(%rbp), %xmm2
	movq	-2416(%rbp), %rcx
	movsd	-2440(%rbp), %xmm1
	movq	-2456(%rbp), %r11
.L175:
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	testq	%rcx, %rcx
	je	.L176
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
	cmpq	$255, %rcx
	ja	.L282
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rcx,4), %xmm0
.L179:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
.L176:
	addsd	-2344(%rbp), %xmm1
	movl	-2476(%rbp), %edx
	movsd	%xmm1, -2384(%rbp)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	cmpq	$255, %rdx
	ja	.L182
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L183:
	mulsd	%xmm1, %xmm0
	movl	-2464(%rbp), %eax
	movsd	-2344(%rbp), %xmm1
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	leaq	(%rax,%rdx), %rcx
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	ja	.L186
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L187:
	mulsd	%xmm2, %xmm0
	movl	-2448(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	addq	%rax, %rcx
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L190
	movapd	%xmm2, %xmm0
	movq	%r11, -2456(%rbp)
	movq	%rcx, -2440(%rbp)
	movsd	%xmm1, -2448(%rbp)
	movsd	%xmm2, -2416(%rbp)
	call	log2@PLT
	movsd	-2416(%rbp), %xmm2
	movq	-2440(%rbp), %rcx
	movsd	-2448(%rbp), %xmm1
	movq	-2456(%rbp), %r11
.L191:
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	testq	%rcx, %rcx
	je	.L192
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
	cmpq	$255, %rcx
	ja	.L283
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rcx,4), %xmm0
.L195:
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
.L192:
	addsd	-2384(%rbp), %xmm1
	movl	-2432(%rbp), %edx
	movsd	%xmm1, -2384(%rbp)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	cmpq	$255, %rdx
	jbe	.L198
	movapd	%xmm1, %xmm0
	movq	%r11, -2440(%rbp)
	movq	%rdx, -2432(%rbp)
	movsd	%xmm1, -2416(%rbp)
	call	log2@PLT
	movsd	-2416(%rbp), %xmm1
	movq	-2432(%rbp), %rdx
	movq	-2440(%rbp), %r11
.L199:
	mulsd	%xmm0, %xmm1
	movsd	-2344(%rbp), %xmm0
	movl	-2408(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	leaq	(%rdx,%rax), %rcx
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	cmpq	$255, %rax
	jbe	.L202
	movsd	%xmm0, -2408(%rbp)
	movapd	%xmm2, %xmm0
	movq	%r11, -2432(%rbp)
	movq	%rcx, -2416(%rbp)
	movsd	%xmm2, -2344(%rbp)
	call	log2@PLT
	movsd	-2344(%rbp), %xmm2
	movsd	-2408(%rbp), %xmm1
	movq	-2416(%rbp), %rcx
	movq	-2432(%rbp), %r11
.L203:
	mulsd	%xmm2, %xmm0
	movl	-2376(%rbp), %eax
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	addq	%rax, %rcx
	subsd	%xmm0, %xmm1
	cmpq	$255, %rax
	ja	.L206
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
.L207:
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	testq	%rcx, %rcx
	je	.L208
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rcx, %xmm2
	cmpq	$255, %rcx
	ja	.L209
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rcx,4), %xmm0
.L210:
	mulsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
.L208:
	movl	-2320(%rbp), %esi
	movl	-2368(%rbp), %eax
	pxor	%xmm0, %xmm0
	addl	-2424(%rbp), %eax
	movsd	-2392(%rbp), %xmm7
	addl	%esi, %eax
	movsd	-2352(%rbp), %xmm3
	movsd	-2360(%rbp), %xmm2
	cvtsi2sdq	%rax, %xmm0
	cmpl	$6, -2312(%rbp)
	addsd	-2384(%rbp), %xmm1
	divsd	%xmm0, %xmm7
	mulsd	%xmm7, %xmm3
	mulsd	%xmm7, %xmm2
	jle	.L213
	mulsd	%xmm7, %xmm1
.L214:
	movapd	%xmm3, %xmm0
	movsd	-2400(%rbp), %xmm7
	subsd	%xmm2, %xmm0
	comisd	%xmm0, %xmm7
	jbe	.L215
	subsd	%xmm1, %xmm3
	comisd	%xmm3, %xmm7
	ja	.L222
.L215:
	subsd	%xmm1, %xmm2
	movsd	.LC8(%rip), %xmm0
	leaq	kStaticContextMapSimpleUTF8.6836(%rip), %rdx
	leaq	kStaticContextMapContinuation.6835(%rip), %rax
	comisd	%xmm2, %xmm0
	cmova	%rdx, %rax
	xorl	%edx, %edx
	ucomisd	%xmm2, %xmm0
	setbe	%dl
	addq	$2, %rdx
	jmp	.L73
.L277:
	pxor	%xmm0, %xmm0
	movsd	.LC4(%rip), %xmm3
	movsd	%xmm0, -2344(%rbp)
	movsd	.LC6(%rip), %xmm0
	movsd	%xmm3, -2392(%rbp)
	movsd	%xmm0, -2400(%rbp)
	jmp	.L74
.L85:
	movapd	%xmm1, %xmm0
	movq	%r11, -2416(%rbp)
	movsd	%xmm2, -2408(%rbp)
	movsd	%xmm1, -2368(%rbp)
	call	log2@PLT
	movsd	-2368(%rbp), %xmm1
	movsd	-2408(%rbp), %xmm2
	leaq	kLog2Table(%rip), %rsi
	cmpq	-2400(%rbp), %r12
	movq	-2416(%rbp), %r11
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	movsd	%xmm2, -2368(%rbp)
	je	.L87
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L81:
	movapd	%xmm1, %xmm0
	movq	%r11, -2416(%rbp)
	movsd	%xmm1, -2408(%rbp)
	call	log2@PLT
	movq	-2416(%rbp), %r11
	movsd	-2408(%rbp), %xmm1
	leaq	kLog2Table(%rip), %rsi
	jmp	.L82
.L140:
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
	jmp	.L141
.L136:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L137
.L125:
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
	jmp	.L126
.L202:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L203
.L198:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
	jmp	.L199
.L190:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L191
.L174:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L175
.L155:
	leaq	kLog2Table(%rip), %rcx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rcx,%rdx,4), %xmm0
	jmp	.L156
.L151:
	leaq	kLog2Table(%rip), %rdx
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdx,%rax,4), %xmm0
	jmp	.L152
.L213:
	movsd	.LC7(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	jmp	.L214
.L166:
	movapd	%xmm1, %xmm0
	movq	%r11, -2440(%rbp)
	movq	%rdx, -2416(%rbp)
	movsd	%xmm1, -2384(%rbp)
	call	log2@PLT
	movq	-2440(%rbp), %r11
	movq	-2416(%rbp), %rdx
	movsd	-2384(%rbp), %xmm1
	jmp	.L167
.L170:
	movapd	%xmm2, %xmm0
	movq	%r11, -2456(%rbp)
	movq	%rcx, -2416(%rbp)
	movsd	%xmm1, -2440(%rbp)
	movsd	%xmm2, -2384(%rbp)
	call	log2@PLT
	movq	-2456(%rbp), %r11
	movsd	-2440(%rbp), %xmm1
	movq	-2416(%rbp), %rcx
	movsd	-2384(%rbp), %xmm2
	jmp	.L171
.L159:
	movapd	%xmm2, %xmm0
	movq	%r11, -2456(%rbp)
	movq	%rax, -2416(%rbp)
	movsd	%xmm1, -2440(%rbp)
	movsd	%xmm2, -2384(%rbp)
	call	log2@PLT
	movq	-2456(%rbp), %r11
	movsd	-2440(%rbp), %xmm1
	movq	-2416(%rbp), %rax
	movsd	-2384(%rbp), %xmm2
	jmp	.L160
.L144:
	movapd	%xmm1, %xmm0
	movq	%r11, -2496(%rbp)
	movq	%rax, -2384(%rbp)
	movsd	%xmm2, -2416(%rbp)
	movsd	%xmm1, -2360(%rbp)
	call	log2@PLT
	movq	-2496(%rbp), %r11
	movsd	-2416(%rbp), %xmm2
	movq	-2384(%rbp), %rax
	movsd	-2360(%rbp), %xmm1
	jmp	.L145
.L186:
	movapd	%xmm2, %xmm0
	movq	%r11, -2464(%rbp)
	movq	%rcx, -2440(%rbp)
	movsd	%xmm1, -2456(%rbp)
	movsd	%xmm2, -2416(%rbp)
	call	log2@PLT
	movq	-2464(%rbp), %r11
	movsd	-2456(%rbp), %xmm1
	movq	-2440(%rbp), %rcx
	movsd	-2416(%rbp), %xmm2
	jmp	.L187
.L182:
	movapd	%xmm1, %xmm0
	movq	%r11, -2456(%rbp)
	movq	%rdx, -2440(%rbp)
	movsd	%xmm1, -2416(%rbp)
	call	log2@PLT
	movq	-2456(%rbp), %r11
	movq	-2440(%rbp), %rdx
	movsd	-2416(%rbp), %xmm1
	jmp	.L183
.L281:
	movapd	%xmm1, %xmm0
	movq	%r11, -2504(%rbp)
	movq	%rax, -2496(%rbp)
	movsd	%xmm1, -2352(%rbp)
	call	log2@PLT
	movsd	-2352(%rbp), %xmm1
	movq	-2496(%rbp), %rax
	movq	-2504(%rbp), %r11
	jmp	.L122
.L129:
	movapd	%xmm1, %xmm0
	movq	%r11, -2512(%rbp)
	movq	%rax, -2496(%rbp)
	movsd	%xmm2, -2504(%rbp)
	movsd	%xmm1, -2352(%rbp)
	call	log2@PLT
	movq	-2512(%rbp), %r11
	movsd	-2504(%rbp), %xmm2
	movq	-2496(%rbp), %rax
	movsd	-2352(%rbp), %xmm1
	jmp	.L130
.L206:
	movapd	%xmm2, %xmm0
	movq	%r11, -2416(%rbp)
	movq	%rcx, -2376(%rbp)
	movsd	%xmm1, -2408(%rbp)
	movsd	%xmm2, -2344(%rbp)
	call	log2@PLT
	movq	-2416(%rbp), %r11
	movsd	-2408(%rbp), %xmm1
	movq	-2376(%rbp), %rcx
	movsd	-2344(%rbp), %xmm2
	jmp	.L207
.L280:
	movsd	.LC6(%rip), %xmm7
	movsd	%xmm7, -2400(%rbp)
	jmp	.L74
.L92:
	movapd	%xmm1, %xmm0
	movq	%r11, -2352(%rbp)
	movsd	%xmm1, -2320(%rbp)
	call	log2@PLT
	movq	-2352(%rbp), %r11
	movsd	-2320(%rbp), %xmm1
	jmp	.L93
.L90:
	movq	%rdx, %rax
	movq	%rdx, %rcx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %ecx
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L91
.L209:
	movapd	%xmm2, %xmm0
	movq	%r11, -2408(%rbp)
	movsd	%xmm1, -2376(%rbp)
	movsd	%xmm2, -2344(%rbp)
	call	log2@PLT
	movq	-2408(%rbp), %r11
	movsd	-2376(%rbp), %xmm1
	movsd	-2344(%rbp), %xmm2
	jmp	.L210
.L283:
	movapd	%xmm2, %xmm0
	movq	%r11, -2448(%rbp)
	movsd	%xmm1, -2440(%rbp)
	movsd	%xmm2, -2416(%rbp)
	call	log2@PLT
	movsd	-2416(%rbp), %xmm2
	movsd	-2440(%rbp), %xmm1
	movq	-2448(%rbp), %r11
	jmp	.L195
.L282:
	movapd	%xmm2, %xmm0
	movq	%r11, -2440(%rbp)
	movsd	%xmm1, -2416(%rbp)
	movsd	%xmm2, -2384(%rbp)
	call	log2@PLT
	movsd	-2384(%rbp), %xmm2
	movsd	-2416(%rbp), %xmm1
	movq	-2440(%rbp), %r11
	jmp	.L179
.L147:
	movapd	%xmm1, %xmm0
	movq	%r11, -2416(%rbp)
	movsd	%xmm1, -2384(%rbp)
	call	log2@PLT
	movq	-2416(%rbp), %r11
	movsd	-2384(%rbp), %xmm1
	jmp	.L148
.L132:
	movapd	%xmm1, %xmm0
	movq	%r11, -2504(%rbp)
	movsd	%xmm1, -2496(%rbp)
	call	log2@PLT
	movq	-2504(%rbp), %r11
	movsd	-2496(%rbp), %xmm1
	jmp	.L133
.L162:
	movapd	%xmm2, %xmm0
	movq	%r11, -2440(%rbp)
	movsd	%xmm1, -2416(%rbp)
	movsd	%xmm2, -2384(%rbp)
	call	log2@PLT
	movq	-2440(%rbp), %r11
	movsd	-2416(%rbp), %xmm1
	movsd	-2384(%rbp), %xmm2
	jmp	.L163
.L223:
	movl	$0, -2392(%rbp)
	jmp	.L75
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE326:
	.size	WriteMetaBlockInternal, .-WriteMetaBlockInternal
	.p2align 4
	.type	PrepareH55, @function
PrepareH55:
.LFB256:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L300
.L285:
	cmpq	$32768, %r14
	ja	.L286
	andl	$1, %r15d
	je	.L286
	leaq	40(%rbx), %rsi
	movq	%r13, %rax
	leaq	(%r14,%r13), %rdi
	movabsq	$3866266742567714048, %rcx
	testq	%r14, %r14
	je	.L284
	.p2align 4,,10
	.p2align 3
.L288:
	movq	(%rax), %rdx
	pxor	%xmm0, %xmm0
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$44, %rdx
	movups	%xmm0, (%rsi,%rdx,4)
	cmpq	%rdi, %rax
	jne	.L288
.L289:
	cmpq	$31, %r14
	jbe	.L284
	movq	48(%r12), %rax
	movl	$0, 40(%rax)
	movzbl	0(%r13), %ecx
	movl	68(%rax), %edx
	addl	$1, %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	4(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	8(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	12(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	16(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	20(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	24(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	imull	%ecx, %edx
	movl	%ecx, 40(%rax)
	movzbl	28(%r13), %esi
	leal	1(%rsi,%rdx), %edx
	movl	%edx, 40(%rax)
.L284:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	leaq	40(%rbx), %rdi
	movl	$4194320, %edx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L289
.L300:
	movq	56(%rdi), %rax
	leaq	64(%rdi), %rbx
	pxor	%xmm0, %xmm0
	movl	$255, %esi
	movq	%rbx, 40(%rdi)
	movdqu	32(%rax), %xmm1
	movl	48(%rax), %edx
	movups	%xmm0, 88(%rdi)
	movdqu	32(%rax), %xmm2
	movl	48(%rax), %eax
	movl	$0, 84(%rdi)
	movl	%edx, 80(%rdi)
	leaq	4194424(%rdi), %rdx
	leaq	4194504(%rdi), %rdi
	movl	%eax, -64(%rdi)
	movabsq	$1640495679631592909, %rax
	movq	%rdx, -4194456(%rdi)
	movl	$67108864, %edx
	movups	%xmm1, -4194440(%rdi)
	movups	%xmm2, -80(%rdi)
	movl	$0, -60(%rdi)
	movups	%xmm0, -56(%rdi)
	movl	$0, -40(%rdi)
	movq	$0, -24(%rdi)
	movq	%rax, -12(%rdi)
	movq	%rdi, 4194472(%r12)
	call	memset@PLT
	jmp	.L285
	.cfi_endproc
.LFE256:
	.size	PrepareH55, .-PrepareH55
	.p2align 4
	.type	PrepareH35, @function
PrepareH35:
.LFB245:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L317
.L302:
	cmpq	$2048, %r14
	ja	.L303
	andl	$1, %r15d
	je	.L303
	leaq	40(%rbx), %rsi
	movq	%r13, %rax
	leaq	(%r14,%r13), %rdi
	movabsq	$-4819355556693147648, %rcx
	testq	%r14, %r14
	je	.L301
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%rax), %rdx
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$48, %rdx
	movq	$0, (%rsi,%rdx,4)
	cmpq	%rdi, %rax
	jne	.L305
.L306:
	cmpq	$31, %r14
	jbe	.L301
	movq	48(%r12), %rax
	movl	$0, 40(%rax)
	movzbl	0(%r13), %ecx
	movl	68(%rax), %edx
	addl	$1, %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	4(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	8(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	12(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	16(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	20(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	24(%r13), %esi
	leal	1(%rsi,%rcx), %ecx
	imull	%ecx, %edx
	movl	%ecx, 40(%rax)
	movzbl	28(%r13), %esi
	leal	1(%rsi,%rdx), %edx
	movl	%edx, 40(%rax)
.L301:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	leaq	40(%rbx), %rdi
	movl	$262152, %edx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L306
.L317:
	movq	56(%rdi), %rax
	leaq	64(%rdi), %rbx
	pxor	%xmm0, %xmm0
	movl	$255, %esi
	movq	%rbx, 40(%rdi)
	movdqu	32(%rax), %xmm1
	movl	48(%rax), %edx
	movups	%xmm0, 88(%rdi)
	movdqu	32(%rax), %xmm2
	movl	48(%rax), %eax
	movl	$0, 84(%rdi)
	movl	%edx, 80(%rdi)
	leaq	262256(%rdi), %rdx
	leaq	262336(%rdi), %rdi
	movl	%eax, -64(%rdi)
	movabsq	$1640495679631592909, %rax
	movq	%rdx, -262288(%rdi)
	movl	$67108864, %edx
	movups	%xmm1, -262272(%rdi)
	movups	%xmm2, -80(%rdi)
	movl	$0, -60(%rdi)
	movups	%xmm0, -56(%rdi)
	movl	$0, -40(%rdi)
	movq	$0, -24(%rdi)
	movq	%rax, -12(%rdi)
	movq	%rdi, 262304(%r12)
	call	memset@PLT
	jmp	.L302
	.cfi_endproc
.LFE245:
	.size	PrepareH35, .-PrepareH35
	.p2align 4
	.type	PrepareH65, @function
PrepareH65:
.LFB267:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %r14
	testq	%r14, %r14
	je	.L338
	movq	40(%r14), %r8
.L321:
	movq	%r8, %rax
	leaq	80(%r14), %rdi
	shrq	$6, %rax
	cmpq	%rax, %r13
	ja	.L322
	andl	$1, %r15d
	je	.L322
	testq	%r13, %r13
	je	.L318
	movl	56(%r14), %ecx
	movq	64(%r14), %r9
	movq	%rbx, %rdx
	leaq	0(%r13,%rbx), %r8
	movabsq	$2297779722762296275, %rsi
	.p2align 4,,10
	.p2align 3
.L324:
	movq	(%rdx), %rax
	xorl	%r10d, %r10d
	addq	$1, %rdx
	andq	%r9, %rax
	imulq	%rsi, %rax
	shrq	%cl, %rax
	movl	%eax, %eax
	movw	%r10w, (%rdi,%rax,2)
	cmpq	%rdx, %r8
	jne	.L324
.L325:
	cmpq	$31, %r13
	jbe	.L318
	movq	48(%r12), %rsi
	movq	%rbx, %rcx
	leaq	32(%rbx), %rdi
	xorl	%eax, %eax
	movl	$0, 40(%rsi)
	movl	68(%rsi), %r8d
	.p2align 4,,10
	.p2align 3
.L327:
	imull	%r8d, %eax
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	leal	1(%rax,%rdx), %eax
	movl	%eax, 40(%rsi)
	cmpq	%rcx, %rdi
	jne	.L327
.L318:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	leaq	(%r8,%r8), %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L325
.L338:
	movq	56(%rdi), %rdx
	movl	$8, %ecx
	leaq	64(%rdi), %r14
	pxor	%xmm0, %xmm0
	movq	%r14, 40(%rdi)
	movdqu	32(%rdx), %xmm1
	movl	48(%rdx), %eax
	movups	%xmm0, 88(%rdi)
	movdqu	32(%rdx), %xmm2
	movl	$0, 84(%rdi)
	movups	%xmm1, 64(%rdi)
	movl	68(%rdi), %esi
	subl	76(%rdi), %ecx
	movl	%eax, 80(%rdi)
	movl	$64, %eax
	sall	$3, %ecx
	subl	%esi, %eax
	movl	%eax, 120(%rdi)
	movq	$-1, %rax
	shrq	%cl, %rax
	movl	%esi, %ecx
	movq	%rax, 128(%rdi)
	movl	$1, %eax
	movq	%rax, %r8
	salq	%cl, %r8
	movl	72(%rdi), %ecx
	movq	%r8, 104(%rdi)
	salq	%cl, %rax
	movl	40(%rdx), %ecx
	movq	%rax, 112(%rdi)
	subl	$1, %eax
	movl	%eax, 136(%rdi)
	movl	$4, %eax
	salq	%cl, %rax
	movl	36(%rdx), %ecx
	addq	$2, %rax
	salq	%cl, %rax
	leaq	80(%r14,%rax), %rcx
	movl	48(%rdx), %eax
	movl	$1, %edx
	movq	%rcx, 48(%rdi)
	movl	$0, 20(%rcx)
	movl	$0, 40(%rcx)
	movq	$0, 56(%rcx)
	movl	$69069, 68(%rcx)
	movl	%eax, 16(%rcx)
	movl	$32, %eax
	movups	%xmm2, (%rcx)
	movups	%xmm0, 24(%rcx)
	.p2align 4,,10
	.p2align 3
.L320:
	imull	$69069, %edx, %edx
	subq	$1, %rax
	jne	.L320
	leaq	80(%rcx), %rdi
	movl	%edx, 72(%rcx)
	movl	$255, %esi
	movl	$67108864, %edx
	movq	%rdi, 48(%rcx)
	movq	%r8, -56(%rbp)
	call	memset@PLT
	movq	-56(%rbp), %r8
	jmp	.L321
	.cfi_endproc
.LFE267:
	.size	PrepareH65, .-PrepareH65
	.section	.text.unlikely,"ax",@progbits
.LCOLDB11:
	.text
.LHOTB11:
	.p2align 4
	.type	BrotliCompressBufferQuality10, @function
BrotliCompressBufferQuality10:
.LFB339:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$68719476751, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$856, %rsp
	movq	%rcx, -896(%rbp)
	movdqa	.LC9(%rip), %xmm0
	movq	%rsi, -832(%rbp)
	movq	%r8, -888(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -872(%rbp)
	movl	$24, %edx
	movabsq	$47244640260, %rax
	movq	%rax, -880(%rbp)
	movq	(%rcx), %rax
	movl	%edi, %ecx
	movq	$22, -552(%rbp)
	movq	%rax, -848(%rbp)
	movl	$1, %eax
	movq	%rax, %r12
	movaps	%xmm0, -80(%rbp)
	movq	$0, -544(%rbp)
	salq	%cl, %r12
	leal	1(%rdi), %ecx
	leaq	-480(%rbp), %rdi
	movq	$0, -536(%rbp)
	cmpq	%rsi, %r12
	cmova	%rsi, %r12
	cmpl	$24, %ecx
	cmovg	%edx, %ecx
	salq	%cl, %rax
	movq	%rax, -856(%rbp)
	shrq	$3, %rax
	movq	%rax, -800(%rbp)
	movabsq	$47244640256, %rax
	movq	%rax, -560(%rbp)
	call	BrotliInitEncoderDictionary
	movl	%ebx, -552(%rbp)
	movq	$0, -504(%rbp)
	movl	$64, -496(%rbp)
	movq	$67108860, -488(%rbp)
	movl	$10, -556(%rbp)
	cmpl	$24, %ebx
	jle	.L340
	movl	$1, -532(%rbp)
.L340:
	movl	-548(%rbp), %eax
	testl	%eax, %eax
	jne	.L591
	cmpl	$18, %ebx
	movl	$18, %eax
	cmovle	%ebx, %eax
.L341:
	cmpl	$2, -560(%rbp)
	movl	%eax, -548(%rbp)
	movl	$12, %edx
	movl	$0, %eax
	cmovne	%eax, %edx
	sete	%sil
	leaq	-560(%rbp), %rax
	movq	%rax, %rdi
	movzbl	%sil, %esi
	movq	%rax, -760(%rbp)
	call	BrotliInitDistanceParams
	movl	-548(%rbp), %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %eax
	salq	%cl, %rax
	xorl	%ecx, %ecx
	movq	%rax, -808(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -728(%rbp)
	call	BrotliInitMemoryManager
	movl	-532(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L577
	sall	$8, %ebx
	movb	$14, -833(%rbp)
	movl	%ebx, %eax
	orl	$17, %eax
	movw	%ax, -836(%rbp)
.L343:
	movl	-556(%rbp), %eax
	movl	-552(%rbp), %ecx
	cmpl	$9, %eax
	jle	.L344
	movl	$10, -528(%rbp)
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%rax, %r12
	cmovbe	%r12, %rax
	leaq	524344(,%rax,8), %rax
.L345:
	movq	-728(%rbp), %rdi
	movq	%rax, %rsi
	call	BrotliAllocate
	movdqa	-528(%rbp), %xmm2
	movl	-528(%rbp), %ebx
	movq	%rax, %rcx
	movq	%rax, -704(%rbp)
	movups	%xmm2, (%rax)
	movl	-512(%rbp), %eax
	movl	%eax, 16(%rcx)
	leal	-5(%rbx), %eax
	cmpl	$60, %eax
	ja	.L369
	leaq	.L371(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L371:
	.long	.L379-.L371
	.long	.L378-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L377-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L376-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L375-.L371
	.long	.L374-.L371
	.long	.L373-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L372-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L369-.L371
	.long	.L370-.L371
	.text
.L591:
	cmpl	$16, %eax
	movl	$16, %edx
	cmovl	%edx, %eax
	movl	$24, %edx
	cmpl	$24, %eax
	cmovg	%edx, %eax
	jmp	.L341
.L344:
	cmpl	$4, %eax
	je	.L592
	jle	.L593
	cmpl	$16, %ecx
	jg	.L352
	cmpl	$6, %eax
	jg	.L353
	movl	$40, -528(%rbp)
.L354:
	movl	$524344, %eax
	jmp	.L345
.L577:
	cmpl	$16, %ebx
	je	.L492
	cmpl	$17, %ebx
	je	.L493
	leal	-34(%rbx,%rbx), %eax
	movb	$4, -833(%rbp)
	orl	$1, %eax
	movw	%ax, -836(%rbp)
	jmp	.L343
.L369:
	movq	-704(%rbp), %rax
	movl	$0, 20(%rax)
	cmpl	$55, %ebx
	ja	.L392
	leaq	.L393(%rip), %rdx
	movl	%ebx, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L393:
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L400-.L393
	.long	.L399-.L393
	.long	.L398-.L393
	.long	.L397-.L393
	.long	.L396-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L395-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L388-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L383-.L393
	.long	.L385-.L393
	.long	.L387-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L392-.L393
	.long	.L394-.L393
	.long	.L389-.L393
	.text
.L392:
	movq	-704(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$1, 20(%rax)
	movups	%xmm0, 24(%rax)
.L488:
	cmpl	$65, %ebx
	je	.L432
	cmpl	$35, %ebx
	je	.L594
.L433:
	cmpq	$0, -832(%rbp)
	je	.L500
	movq	-704(%rbp), %rax
	movb	$0, -858(%rbp)
	movb	$0, -857(%rbp)
	movq	$0, -816(%rbp)
	leaq	524344(%rax), %r14
	movq	$0, -824(%rbp)
.L485:
	movq	-816(%rbp), %rbx
	movq	-856(%rbp), %rax
	movabsq	$-6148914691236517205, %rdx
	movq	$0, -616(%rbp)
	movq	-832(%rbp), %rcx
	movq	$0, -608(%rbp)
	addq	%rbx, %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	movq	%rax, %rcx
	movq	%rax, -752(%rbp)
	subq	%rbx, %rcx
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$3, %rdx
	cmpl	$9, -556(%rbp)
	leaq	16(%rdx), %rax
	movq	%rax, -792(%rbp)
	jg	.L595
.L442:
	movl	$2, -840(%rbp)
.L443:
	movq	-816(%rbp), %rax
	movq	$0, -776(%rbp)
	movq	$0, -784(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -768(%rbp)
	movq	$0, -736(%rbp)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L597:
	leaq	1(%rax), %r12
	movq	-728(%rbp), %rdi
	movq	%r12, %rsi
	salq	$4, %rsi
	call	BrotliAllocate
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -720(%rbp)
	call	BrotliInitZopfliNodes
	cmpq	$2, %rbx
	ja	.L478
.L446:
	subq	$8, %rsp
	leaq	-80(%rbp), %r12
	pushq	-720(%rbp)
	movq	-696(%rbp), %rdx
	movq	-760(%rbp), %r9
	pushq	-704(%rbp)
	movq	%r15, %rcx
	movabsq	$9223372036854775807, %r8
	movq	-712(%rbp), %rsi
	pushq	%r12
	movq	-728(%rbp), %rdi
	call	BrotliZopfliComputeShortestPath
	movq	-768(%rbp), %rcx
	addq	$32, %rsp
	leaq	(%rcx,%rax), %r13
	movq	-792(%rbp), %rax
	leaq	1(%r13), %rbx
	cmpq	%rax, %rbx
	cmovb	%rax, %rbx
	salq	$4, %rcx
	movq	%rcx, %rdx
	cmpq	%rbx, -776(%rbp)
	je	.L459
	movq	-728(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rcx, -632(%rbp)
	salq	$4, %rsi
	call	BrotliAllocate
	movq	-632(%rbp), %rdx
	movq	%rax, %rcx
	movq	-736(%rbp), %rax
	testq	%rax, %rax
	je	.L515
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	%rdx, -640(%rbp)
	movq	%rcx, -632(%rbp)
	call	memcpy@PLT
	movq	-736(%rbp), %rsi
	movq	-728(%rbp), %rdi
	call	BrotliFree
	movq	-632(%rbp), %rcx
	movq	-640(%rbp), %rdx
	movq	%rcx, -736(%rbp)
.L459:
	leaq	-608(%rbp), %rax
	movq	%r12, %rcx
	movq	-696(%rbp), %r12
	movq	-760(%rbp), %r9
	pushq	%rax
	movq	-736(%rbp), %rax
	leaq	-616(%rbp), %r8
	movq	-712(%rbp), %rdi
	movq	%r12, %rsi
	addq	%rdx, %rax
	movq	-720(%rbp), %rdx
	pushq	%rax
	call	BrotliZopfliCreateCommands
	movq	-712(%rbp), %rcx
	movq	-720(%rbp), %rsi
	movq	%r12, %rax
	movq	-728(%rbp), %rdi
	addq	%rcx, -784(%rbp)
	addq	%rcx, %rax
	movq	%rax, -696(%rbp)
	call	BrotliFree
	popq	%r9
	movq	%r13, %rax
	popq	%r10
	cmpq	%r13, -608(%rbp)
	cmovnb	-608(%rbp), %rax
	cmpq	%rax, -800(%rbp)
	jb	.L460
	movq	%rbx, -776(%rbp)
	movq	%r13, -768(%rbp)
.L444:
	movq	-752(%rbp), %rbx
	cmpq	%rbx, -696(%rbp)
	jnb	.L596
	movq	%rbx, %rax
	movq	-808(%rbp), %rbx
	subq	-696(%rbp), %rax
	cmpq	%rbx, %rax
	cmova	%rbx, %rax
	movq	%rax, -712(%rbp)
	movq	%rax, %rbx
	cmpq	$-1, %rax
	jne	.L597
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	BrotliInitZopfliNodes
	movq	$0, -720(%rbp)
.L478:
	movq	-696(%rbp), %rbx
	cmpq	$127, %rbx
	jbe	.L446
	movq	-712(%rbp), %rax
	leaq	-127(%rbx), %rcx
	movq	%rcx, -648(%rbp)
	addq	%rcx, %rax
	cmpq	%rbx, %rax
	cmova	%rbx, %rax
	movq	%rax, -744(%rbp)
	cmpq	%rax, %rcx
	jnb	.L446
	movq	-704(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -656(%rbp)
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-648(%rbp), %rbx
	movq	-696(%rbp), %rax
	movl	$15, %ecx
	movabsq	$9223372036854775807, %rdi
	movq	-656(%rbp), %rdx
	movq	-704(%rbp), %rsi
	subq	%rbx, %rax
	cmpq	$15, %rax
	cmovb	%rcx, %rax
	movq	%rdx, %rcx
	andq	%rbx, %rdi
	andq	%rbx, %rdx
	leaq	(%rdx,%rdx), %r11
	movq	%rdi, -664(%rbp)
	subq	%rax, %rcx
	imull	$506832829, (%r15,%rdi), %eax
	movq	%r11, -632(%rbp)
	addq	$1, %r11
	movq	%rcx, -672(%rbp)
	movq	%r11, -640(%rbp)
	shrl	$15, %eax
	leaq	(%rsi,%rax,4), %rax
	movl	48(%rax), %esi
	movl	%ebx, 48(%rax)
	movq	%rbx, %rax
	subq	%rsi, %rax
	cmpq	%rax, %rcx
	jb	.L519
	leaq	(%r15,%rdi), %rbx
	movl	$64, %r11d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	%rbx, -680(%rbp)
	movq	%r11, %rbx
	testq	%rax, %rax
	je	.L519
	.p2align 4,,10
	.p2align 3
.L447:
	cmpq	%r12, %r13
	movq	%r12, %rdi
	movq	-664(%rbp), %rax
	movl	$128, %r10d
	cmovbe	%r13, %rdi
	subq	%rdi, %r10
	leaq	(%rax,%rdi), %r8
	leaq	(%rdi,%rsi), %rcx
	movq	%r10, %rax
	addq	%r15, %rcx
	addq	%r15, %r8
	shrq	$3, %rax
	je	.L450
	leaq	8(%rcx), %rdx
	xorl	%r9d, %r9d
	movq	%rdx, -688(%rbp)
.L452:
	movq	(%rcx,%r9), %rdx
	movq	(%r8,%r9), %r11
	cmpq	%r11, %rdx
	je	.L598
	xorq	%r11, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r9, %rax
.L453:
	addq	%rdi, %rax
	movq	-656(%rbp), %rdi
	movq	-632(%rbp), %rcx
	movq	-640(%rbp), %rdx
	andq	%rsi, %rdi
	leaq	(%r14,%rcx,4), %rcx
	leaq	(%r14,%rdx,4), %rdx
	leaq	(%rdi,%rdi), %r8
	cmpq	$127, %rax
	ja	.L599
	movq	-680(%rbp), %r11
	leaq	(%r15,%rax), %r9
	movzbl	(%r9,%rsi), %r10d
	cmpb	%r10b, (%r11,%rax)
	jbe	.L455
	movl	%esi, (%rcx)
	leaq	1(%r8), %rsi
	movq	-648(%rbp), %rdi
	subq	$1, %rbx
	leaq	(%r14,%rsi,4), %rcx
	movq	%rsi, -632(%rbp)
	movl	(%rcx), %esi
	subq	%rsi, %rdi
	cmpq	%rdi, -672(%rbp)
	setb	%r8b
	testq	%rdi, %rdi
	sete	%dil
	orb	%dil, %r8b
	jne	.L456
	testq	%rbx, %rbx
	je	.L456
	movq	%rax, %r12
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L455:
	movl	%esi, (%rdx)
	leaq	(%r14,%rdi,8), %rdx
	movq	-648(%rbp), %rdi
	subq	$1, %rbx
	movl	(%rdx), %esi
	subq	%rsi, %rdi
	sete	%r9b
	cmpq	%rdi, -672(%rbp)
	setb	%dil
	orb	%dil, %r9b
	jne	.L456
	testq	%rbx, %rbx
	je	.L456
	movq	%r8, -640(%rbp)
	movq	%rax, %r13
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L598:
	movq	-688(%rbp), %rdx
	addq	%r9, %rdx
	addq	$8, %r9
	subq	$1, %rax
	jne	.L452
	movq	%r9, %rax
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L450:
	andl	$7, %r10d
	je	.L453
	movzbl	(%r8,%rax), %edx
	cmpb	%dl, (%rcx)
	jne	.L453
	leaq	1(%rax), %rdx
	cmpq	$1, %r10
	je	.L514
	movzbl	1(%r8,%rax), %r11d
	cmpb	%r11b, 1(%rcx)
	jne	.L514
	leaq	2(%rax), %rdx
	cmpq	$2, %r10
	je	.L514
	movzbl	2(%r8,%rax), %r11d
	cmpb	%r11b, 2(%rcx)
	jne	.L514
	leaq	3(%rax), %rdx
	cmpq	$3, %r10
	je	.L514
	movzbl	3(%r8,%rax), %r11d
	cmpb	%r11b, 3(%rcx)
	jne	.L514
	leaq	4(%rax), %rdx
	cmpq	$4, %r10
	je	.L514
	movzbl	4(%r8,%rax), %r11d
	cmpb	%r11b, 4(%rcx)
	jne	.L514
	leaq	5(%rax), %rdx
	subq	$5, %r10
	je	.L514
	movzbl	5(%r8,%rax), %r11d
	cmpb	%r11b, 5(%rcx)
	jne	.L514
	leaq	6(%rax), %rdx
	cmpq	$1, %r10
	je	.L514
	movzbl	6(%r8,%rax), %r10d
	cmpb	%r10b, 6(%rcx)
	jne	.L514
	addq	$7, %rax
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%rdx, %rax
	jmp	.L453
.L519:
	movq	-640(%rbp), %rax
	leaq	(%r14,%rdx,8), %rcx
	leaq	(%r14,%rax,4), %rdx
	.p2align 4,,10
	.p2align 3
.L456:
	movq	-704(%rbp), %rax
	movl	524336(%rax), %eax
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
.L449:
	addq	$1, -648(%rbp)
	movq	-648(%rbp), %rax
	cmpq	%rax, -744(%rbp)
	jne	.L458
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L599:
	movl	(%r14,%rdi,8), %eax
	movl	%eax, (%rcx)
	movl	4(%r14,%r8,4), %eax
	movl	%eax, (%rdx)
	jmp	.L449
.L515:
	movq	%rcx, -736(%rbp)
	jmp	.L459
.L596:
	movq	-768(%rbp), %r13
.L460:
	movq	-616(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L462
	movq	-736(%rbp), %rax
	leaq	1(%r13), %rdi
	salq	$4, %r13
	movl	$16, %r8d
	leaq	(%rax,%r13), %rsi
	movl	%edx, (%rsi)
	movq	$134217728, 4(%rsi)
	movw	%r8w, 14(%rsi)
	cmpq	$5, %rdx
	jbe	.L600
	cmpq	$129, %rdx
	jbe	.L601
	cmpq	$2113, %rdx
	jbe	.L602
	movl	$490, %eax
	cmpq	$6209, %rdx
	jbe	.L464
	cmpq	$22594, %rdx
	sbbl	%eax, %eax
	andl	$-8, %eax
	addw	$506, %ax
.L464:
	addq	%rdx, -608(%rbp)
	movq	%rdi, %r13
	movw	%ax, 12(%rsi)
.L462:
	movq	-784(%rbp), %rax
	movq	-816(%rbp), %rbx
	movzbl	-833(%rbp), %r12d
	addq	%rax, %rbx
	movq	%r12, -600(%rbp)
	testq	%rax, %rax
	je	.L603
	xorl	%eax, %eax
	cmpq	-832(%rbp), %rbx
	sete	%al
	cmpq	$2, -784(%rbp)
	movl	%eax, -632(%rbp)
	jbe	.L473
	movq	-784(%rbp), %rcx
	movq	%rcx, %rax
	shrq	$8, %rax
	addq	$2, %rax
	cmpq	%rax, %r13
	jnb	.L472
	movq	-608(%rbp), %r8
	movq	-816(%rbp), %rdx
	movq	%r15, %rdi
	movabsq	$9223372036854775807, %rsi
	call	ShouldCompress.part.0
	testl	%eax, %eax
	jne	.L472
.L473:
	movdqa	-880(%rbp), %xmm4
	cmpq	$-16, -784(%rbp)
	movaps	%xmm4, -80(%rbp)
	je	.L604
	movq	-784(%rbp), %r13
	movq	-728(%rbp), %rdi
	leaq	16(%r13), %rsi
	call	BrotliAllocate
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r13, %r8
	movq	%rax, %r11
	movl	-632(%rbp), %edi
	movzwl	-836(%rbp), %eax
	movabsq	$9223372036854775807, %rcx
	movq	-816(%rbp), %rdx
	leaq	-600(%rbp), %r9
	movq	%r11, -640(%rbp)
	movw	%ax, (%r11)
	pushq	%r11
	call	BrotliStoreUncompressedMetaBlock
	movq	-600(%rbp), %r12
	movq	-640(%rbp), %r11
	popq	%rcx
	popq	%rsi
.L468:
	movq	%r12, %r13
	shrq	$3, %r13
	movzbl	(%r11,%r13), %eax
	movw	%ax, -836(%rbp)
	movl	%r12d, %eax
	andl	$7, %eax
	movb	%al, -833(%rbp)
	cmpq	-832(%rbp), %rbx
	jnb	.L475
	movzbl	-1(%r15,%rbx), %eax
	movdqa	-80(%rbp), %xmm1
	addq	%r13, -824(%rbp)
	movb	%al, -857(%rbp)
	movzbl	-2(%r15,%rbx), %eax
	movaps	%xmm1, -880(%rbp)
	movb	%al, -858(%rbp)
	movq	-824(%rbp), %rax
	cmpq	%rax, -848(%rbp)
	jb	.L605
	movq	-888(%rbp), %r12
	movq	%r13, %rdx
	movq	%r11, %rsi
	movq	%r11, -632(%rbp)
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	-632(%rbp), %r11
	addq	%r13, %rax
	movq	-728(%rbp), %r13
	movq	%r11, %rsi
	movq	%rax, -888(%rbp)
	movq	%r13, %rdi
	call	BrotliFree
	movq	-736(%rbp), %rsi
	movq	%r13, %rdi
	call	BrotliFree
	movq	%rbx, -816(%rbp)
	jmp	.L485
.L475:
	addq	%r13, -824(%rbp)
	movq	%r13, %r12
	movq	-824(%rbp), %rax
	movq	%r11, %rsi
	cmpq	%rax, -848(%rbp)
	jnb	.L606
.L477:
	movq	-728(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-736(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
.L441:
	movq	-896(%rbp), %rax
	movq	-824(%rbp), %rbx
	movq	-704(%rbp), %rsi
	movq	-728(%rbp), %rdi
	movq	%rbx, (%rax)
	call	BrotliFree
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L607
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L595:
	.cfi_restore_state
	movsd	.LC10(%rip), %xmm0
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movabsq	$9223372036854775807, %rdx
	call	BrotliIsMostlyUTF8
	movl	$3, -840(%rbp)
	testl	%eax, %eax
	jne	.L442
	jmp	.L443
.L603:
	movq	-728(%rbp), %rdi
	movl	$16, %esi
	call	BrotliAllocate
	movl	$3, %edx
	movq	%rax, %r11
	movzwl	-836(%rbp), %eax
	movw	%ax, (%r11)
	movq	-600(%rbp), %rax
	movq	%rax, %rsi
	movl	%eax, %ecx
	addq	$9, %rax
	shrq	$3, %rsi
	andl	$7, %ecx
	andl	$4294967288, %eax
	addq	%r11, %rsi
	salq	%cl, %rdx
	movq	%rax, %r12
	movzbl	(%rsi), %edi
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	movq	%rax, -600(%rbp)
	jmp	.L468
.L472:
	movdqa	-560(%rbp), %xmm5
	movdqa	-544(%rbp), %xmm6
	leaq	-304(%rbp), %rax
	movdqa	-528(%rbp), %xmm7
	movdqa	-512(%rbp), %xmm2
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	movdqa	-496(%rbp), %xmm3
	movdqa	-480(%rbp), %xmm4
	movaps	%xmm5, -432(%rbp)
	movaps	%xmm6, -416(%rbp)
	movdqa	-464(%rbp), %xmm5
	movdqa	-448(%rbp), %xmm6
	movaps	%xmm7, -400(%rbp)
	movaps	%xmm2, -384(%rbp)
	movaps	%xmm3, -368(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm5, -336(%rbp)
	movaps	%xmm6, -320(%rbp)
	call	BrotliInitBlockSplit
	leaq	-256(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -648(%rbp)
	call	BrotliInitBlockSplit
	leaq	-208(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -656(%rbp)
	call	BrotliInitBlockSplit
	movzbl	-858(%rbp), %r10d
	movq	%r15, %rsi
	movl	-840(%rbp), %ecx
	subq	$8, %rsp
	pushq	-640(%rbp)
	movzbl	-857(%rbp), %r9d
	leaq	-432(%rbp), %r8
	movq	-816(%rbp), %rdx
	pushq	%rcx
	movabsq	$9223372036854775807, %rcx
	pushq	%r13
	movq	-728(%rbp), %rdi
	pushq	-736(%rbp)
	pushq	%r10
	movl	%r10d, -696(%rbp)
	movl	%r9d, -688(%rbp)
	movq	%r8, -680(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	call	BrotliBuildMetaBlock
	movl	-368(%rbp), %eax
	addq	$48, %rsp
	movl	$544, %edi
	movq	-640(%rbp), %rsi
	cmpl	$544, %eax
	cmovbe	%eax, %edi
	call	BrotliOptimizeHistograms
	movq	-784(%rbp), %rcx
	movq	-728(%rbp), %rdi
	leaq	503(%rcx,%rcx), %rsi
	call	BrotliAllocate
	movzwl	-836(%rbp), %r10d
	subq	$8, %rsp
	movq	-680(%rbp), %r8
	movq	%rax, -672(%rbp)
	movq	-816(%rbp), %rdx
	movq	%r15, %rsi
	movw	%r10w, (%rax)
	movl	-696(%rbp), %r10d
	movl	-688(%rbp), %r9d
	movq	-728(%rbp), %rdi
	pushq	%rax
	leaq	-600(%rbp), %rax
	pushq	%rax
	movq	%rax, -664(%rbp)
	movl	-840(%rbp), %eax
	pushq	-640(%rbp)
	pushq	%r13
	pushq	-736(%rbp)
	movq	-784(%rbp), %r13
	pushq	%rax
	movl	-632(%rbp), %eax
	pushq	%r8
	movq	%r13, %rcx
	movabsq	$9223372036854775807, %r8
	pushq	%rax
	pushq	%r10
	call	BrotliStoreMetaBlock
	movq	-600(%rbp), %rdx
	movq	%r13, %rcx
	addq	$80, %rsp
	addq	$4, %rcx
	movq	-664(%rbp), %rax
	movq	-672(%rbp), %r11
	shrq	$3, %rdx
	cmpq	%rdx, %rcx
	jb	.L608
.L474:
	movq	-728(%rbp), %r13
	movq	-640(%rbp), %rsi
	movq	%r11, -632(%rbp)
	movq	%r13, %rdi
	call	BrotliDestroyBlockSplit
	movq	-648(%rbp), %rsi
	movq	%r13, %rdi
	call	BrotliDestroyBlockSplit
	movq	-656(%rbp), %rsi
	movq	%r13, %rdi
	call	BrotliDestroyBlockSplit
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	call	BrotliFree
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -160(%rbp)
	call	BrotliFree
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -144(%rbp)
	call	BrotliFree
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -128(%rbp)
	call	BrotliFree
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -112(%rbp)
	call	BrotliFree
	movq	-600(%rbp), %r12
	movq	-632(%rbp), %r11
	jmp	.L468
.L605:
	movq	%r11, %rsi
	jmp	.L477
.L600:
	leal	0(,%rdx,8), %eax
	andl	$56, %eax
	orb	$-126, %al
	jmp	.L464
.L601:
	leaq	-2(%rdx), %r8
	bsrl	%r8d, %eax
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %r8
	leal	2(%r8,%rax,2), %eax
.L589:
	movl	%eax, %ecx
	movl	$5377344, %r8d
	sall	$3, %eax
	shrw	$3, %cx
	andl	$56, %eax
	movzwl	%cx, %ecx
	leal	(%rcx,%rcx,2), %r9d
	leal	(%r9,%r9), %ecx
	sall	$6, %r9d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	$192, %ecx
	leal	64(%r9,%rcx), %ecx
	orl	%ecx, %eax
	orl	$2, %eax
	jmp	.L464
.L608:
	movzwl	-836(%rbp), %ecx
	subq	$8, %rsp
	movdqa	-880(%rbp), %xmm7
	movq	%rax, %r9
	movq	-816(%rbp), %rdx
	movq	-784(%rbp), %r8
	movq	%r15, %rsi
	movq	%r11, -664(%rbp)
	movaps	%xmm7, -80(%rbp)
	movl	-632(%rbp), %edi
	movw	%cx, (%r11)
	movabsq	$9223372036854775807, %rcx
	pushq	%r11
	movq	%r12, -600(%rbp)
	call	BrotliStoreUncompressedMetaBlock
	popq	%rax
	movq	-664(%rbp), %r11
	popq	%rdx
	jmp	.L474
.L602:
	leaq	-66(%rdx), %rax
	bsrl	%eax, %eax
	addl	$10, %eax
	jmp	.L589
.L352:
	cmpq	$1048575, -544(%rbp)
	leal	-1(%rax), %edx
	jbe	.L356
	cmpl	$18, %ecx
	jg	.L609
.L356:
	movl	$5, -528(%rbp)
	movl	%edx, -520(%rbp)
	cmpl	$6, %eax
	jle	.L360
	cmpl	$9, %eax
	movl	$16, %edx
	movl	$10, %eax
	movl	$15, -524(%rbp)
	cmove	%edx, %eax
.L479:
	movl	%eax, -512(%rbp)
.L362:
	movl	-520(%rbp), %ecx
	movl	$4, %eax
	salq	%cl, %rax
	movl	-524(%rbp), %ecx
	addq	$2, %rax
	salq	%cl, %rax
	addq	$64, %rax
	jmp	.L364
.L492:
	xorl	%r13d, %r13d
	movb	$1, -833(%rbp)
	movw	%r13w, -836(%rbp)
	jmp	.L343
.L592:
	cmpq	$1048575, -544(%rbp)
	jbe	.L347
	movl	$54, -528(%rbp)
	movl	$4194360, %eax
	cmpl	$24, %ecx
	jle	.L345
	movl	$55, -528(%rbp)
	movl	$71303368, %eax
	jmp	.L345
.L500:
	movq	$0, -824(%rbp)
	movl	$1, %r12d
	jmp	.L441
.L372:
	movq	-704(%rbp), %rax
	movq	-760(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	%rbx, 56(%rax)
	movl	$0, 20(%rax)
	movups	%xmm0, 40(%rax)
.L389:
	movq	-704(%rbp), %rbx
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	PrepareH55
	movl	(%rbx), %ebx
.L390:
	movq	-704(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$1, 20(%rax)
	movups	%xmm0, 24(%rax)
	cmpl	$55, %ebx
	jne	.L488
	movq	48(%rax), %rax
	cmpq	$31, %r12
	jbe	.L437
.L436:
	movl	$0, 40(%rax)
	movzbl	(%r15), %ecx
	movl	68(%rax), %edx
	addl	$1, %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	4(%r15), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	8(%r15), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	12(%r15), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	16(%r15), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	20(%r15), %esi
	leal	1(%rsi,%rcx), %ecx
	movl	%ecx, 40(%rax)
	imull	%edx, %ecx
	movzbl	24(%r15), %esi
	leal	1(%rsi,%rcx), %ecx
	imull	%ecx, %edx
	movl	%ecx, 40(%rax)
	movzbl	28(%r15), %esi
	leal	1(%rsi,%rdx), %edx
	movl	%edx, 40(%rax)
.L437:
	movq	$0, 56(%rax)
	jmp	.L433
.L373:
	movl	-556(%rbp), %edx
	xorl	%eax, %eax
	cmpl	$7, %edx
	setl	%al
	subl	$4, %edx
	movl	%edx, %ecx
	addl	$7, %eax
	sall	%cl, %eax
	movq	-704(%rbp), %rcx
	movq	%rax, 1311784(%rcx)
	movl	$0, 20(%rcx)
.L387:
	cmpq	$512, %r12
	ja	.L423
	movq	%r15, %rdx
	leaq	(%r12,%r15), %rcx
	testq	%r12, %r12
	je	.L425
	movq	-704(%rbp), %rsi
.L426:
	imull	$506832829, (%rdx), %eax
	movl	$-13108, %r11d
	addq	$1, %rdx
	shrl	$17, %eax
	movl	$-858993460, 40(%rsi,%rax,4)
	movw	%r11w, 131112(%rsi,%rax,2)
	cmpq	%rcx, %rdx
	jne	.L426
	jmp	.L425
.L376:
	movq	-704(%rbp), %rax
	movq	-760(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	%rbx, 56(%rax)
	movl	$0, 20(%rax)
	movups	%xmm0, 40(%rax)
.L388:
	movq	-704(%rbp), %rbx
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	PrepareH35
	movl	(%rbx), %ebx
	jmp	.L390
.L370:
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	-704(%rbp), %rax
	movq	-760(%rbp), %rbx
	movl	$0, 20(%rax)
	movq	%rax, %rdi
	movq	%rbx, 56(%rax)
	movq	%rax, %rbx
	movups	%xmm0, 40(%rax)
	call	PrepareH65
	movl	(%rbx), %ebx
	jmp	.L390
.L493:
	movl	$1, %ebx
	movb	$7, -833(%rbp)
	movw	%bx, -836(%rbp)
	jmp	.L343
.L593:
	movl	%eax, -528(%rbp)
	cmpl	$24, %ecx
	jle	.L351
	cmpl	$3, %eax
	jne	.L363
	movl	$35, -528(%rbp)
	movl	$67371200, %eax
	jmp	.L345
.L363:
	cmpl	$6, %eax
	je	.L358
.L351:
	cmpl	$6, %eax
	ja	.L496
.L532:
	leaq	.L365(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L365:
	.long	.L496-.L365
	.long	.L496-.L365
	.long	.L367-.L365
	.long	.L366-.L365
	.long	.L354-.L365
	.long	.L362-.L365
	.long	.L359-.L365
	.text
.L496:
	movl	$40, %eax
	jmp	.L345
.L377:
	movl	-552(%rbp), %ecx
	movl	$1, %eax
	movq	-704(%rbp), %rsi
	movl	%eax, %edx
	sall	%cl, %edx
	subl	%edx, %eax
	leal	-1(%rdx), %ecx
	movl	%eax, 524336(%rsi)
	movq	%rcx, 40(%rsi)
	movq	%rsi, %rcx
.L391:
	movd	%eax, %xmm3
	leaq	524336(%rcx), %rdx
	leaq	48(%rcx), %rax
	pshufd	$0, %xmm3, %xmm0
.L430:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	je	.L390
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L430
	jmp	.L390
.L378:
	movq	-704(%rbp), %rsi
	movl	$64, %eax
	movl	$8, %ecx
	movl	4(%rsi), %edx
	subl	12(%rsi), %ecx
	movl	$0, 20(%rsi)
	sall	$3, %ecx
	subl	%edx, %eax
	movl	%eax, 56(%rsi)
	movq	$-1, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	movq	%rax, 64(%rsi)
	movl	$1, %eax
	movq	%rax, %rdi
	salq	%cl, %rdi
	movl	8(%rsi), %ecx
	movq	%rdi, 40(%rsi)
	movq	%rdi, %rdx
	salq	%cl, %rax
	movq	%rsi, %rcx
	movq	%rax, 48(%rsi)
	subl	$1, %eax
	movl	%eax, 72(%rsi)
.L381:
	movq	%rdx, %rax
	leaq	80(%rcx), %rdi
	shrq	$6, %rax
	cmpq	%rax, %r12
	ja	.L411
	testq	%r12, %r12
	je	.L409
	movq	%rcx, %rax
	movl	56(%rcx), %ecx
	movq	%r15, %rdx
	leaq	(%r12,%r15), %r8
	movabsq	$2297779722762296275, %rsi
	movq	64(%rax), %r9
.L412:
	movq	(%rdx), %rax
	xorl	%r10d, %r10d
	addq	$1, %rdx
	andq	%r9, %rax
	imulq	%rsi, %rax
	shrq	%cl, %rax
	movl	%eax, %eax
	movw	%r10w, (%rdi,%rax,2)
	cmpq	%r8, %rdx
	jne	.L412
	jmp	.L390
.L379:
	movq	-704(%rbp), %rsi
	movl	$32, %eax
	movl	4(%rsi), %ecx
	movl	$0, 20(%rsi)
	subl	%ecx, %eax
	movl	%eax, 56(%rsi)
	movl	$1, %eax
	movq	%rax, %rdx
	salq	%cl, %rdx
	movl	8(%rsi), %ecx
	movq	%rdx, 40(%rsi)
	salq	%cl, %rax
	movq	%rsi, %rcx
	movq	%rax, 48(%rsi)
	subl	$1, %eax
	movl	%eax, 60(%rsi)
.L380:
	movq	%rdx, %rax
	leaq	64(%rcx), %rdi
	shrq	$6, %rax
	cmpq	%rax, %r12
	ja	.L411
	testq	%r12, %r12
	je	.L409
	movl	56(%rcx), %ecx
	movq	%r15, %rax
	leaq	(%r12,%r15), %rsi
.L410:
	imull	$506832829, (%rax), %edx
	xorl	%r11d, %r11d
	addq	$1, %rax
	shrl	%cl, %edx
	movw	%r11w, (%rdi,%rdx,2)
	cmpq	%rsi, %rax
	jne	.L410
	jmp	.L390
.L374:
	movl	-556(%rbp), %edx
	xorl	%eax, %eax
	cmpl	$7, %edx
	setl	%al
	subl	$4, %edx
	movl	%edx, %ecx
	addl	$7, %eax
	sall	%cl, %eax
	movq	-704(%rbp), %rcx
	movq	%rax, 524336(%rcx)
	movl	$0, 20(%rcx)
.L385:
	cmpq	$512, %r12
	ja	.L418
	movq	%r15, %rdx
	leaq	(%r12,%r15), %rcx
	testq	%r12, %r12
	je	.L420
	movq	-704(%rbp), %rsi
.L421:
	imull	$506832829, (%rdx), %eax
	movl	$-13108, %r14d
	addq	$1, %rdx
	shrl	$17, %eax
	movl	$-858993460, 40(%rsi,%rax,4)
	movw	%r14w, 131112(%rsi,%rax,2)
	cmpq	%rdx, %rcx
	jne	.L421
	jmp	.L420
.L375:
	movl	-556(%rbp), %edx
	xorl	%eax, %eax
	cmpl	$7, %edx
	setl	%al
	subl	$4, %edx
	movl	%edx, %ecx
	addl	$7, %eax
	sall	%cl, %eax
	movq	-704(%rbp), %rcx
	movq	%rax, 524336(%rcx)
	movl	$0, 20(%rcx)
.L383:
	cmpq	$512, %r12
	ja	.L418
	movq	%r15, %rdx
	leaq	(%r12,%r15), %rcx
	testq	%r12, %r12
	je	.L420
	movq	-704(%rbp), %rsi
.L416:
	imull	$506832829, (%rdx), %eax
	movl	$-13108, %edi
	addq	$1, %rdx
	shrl	$17, %eax
	movl	$-858993460, 40(%rsi,%rax,4)
	movw	%di, 131112(%rsi,%rax,2)
	cmpq	%rcx, %rdx
	jne	.L416
	jmp	.L420
.L347:
	movl	$4, -528(%rbp)
	cmpl	$24, %ecx
	jle	.L354
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L359:
	movl	-520(%rbp), %ecx
	movl	$4, %eax
	salq	%cl, %rax
	movl	-524(%rbp), %ecx
	addq	$2, %rax
	salq	%cl, %rax
	addq	$80, %rax
.L364:
	testq	%rax, %rax
	jne	.L345
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$262192, %eax
	jmp	.L345
.L353:
	cmpl	$9, %eax
	je	.L610
	movl	$41, -528(%rbp)
	jmp	.L354
.L594:
	movq	-704(%rbp), %rax
	movq	48(%rax), %rax
	cmpq	$31, %r12
	ja	.L436
	jmp	.L437
.L432:
	movq	-704(%rbp), %rax
	movq	48(%rax), %rdx
	cmpq	$31, %r12
	jbe	.L440
	movl	$0, 40(%rdx)
	movl	68(%rdx), %edi
	movq	%r15, %rcx
	xorl	%eax, %eax
	leaq	32(%r15), %rsi
.L439:
	imull	%edi, %eax
	movzbl	(%rcx), %r8d
	addq	$1, %rcx
	leal	1(%rax,%r8), %eax
	movl	%eax, 40(%rdx)
	cmpq	%rcx, %rsi
	jne	.L439
.L440:
	movq	$0, 56(%rdx)
	jmp	.L433
.L610:
	movl	$42, -528(%rbp)
	movl	$1311792, %eax
	jmp	.L345
.L609:
	movabsq	$64424509446, %rbx
	movl	%edx, -520(%rbp)
	movl	$4, %edx
	movq	%rbx, -528(%rbp)
	movl	$5, -516(%rbp)
	cmpl	$6, %eax
	jle	.L357
	cmpl	$9, %eax
	movl	$10, %edx
	movl	$16, %eax
	cmove	%eax, %edx
.L357:
	movl	%edx, -512(%rbp)
	cmpl	$24, %ecx
	jle	.L359
.L358:
	movl	-520(%rbp), %ecx
	movl	$4, %eax
	movl	$65, -528(%rbp)
	salq	%cl, %rax
	movl	-524(%rbp), %ecx
	addq	$2, %rax
	salq	%cl, %rax
	addq	$67109088, %rax
	jmp	.L364
.L397:
	movq	-704(%rbp), %rax
	movq	40(%rax), %rdx
	movq	%rax, %rcx
	jmp	.L380
.L396:
	movq	-704(%rbp), %rax
	movq	40(%rax), %rdx
	movq	%rax, %rcx
	jmp	.L381
.L606:
	movq	-888(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rsi, %rbx
	movl	$1, %r12d
	call	memcpy@PLT
	movq	%rbx, %rsi
	movq	-728(%rbp), %rbx
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-736(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	jmp	.L441
.L607:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$14, -524(%rbp)
	movl	$4, %eax
	jmp	.L479
.L399:
	cmpq	$2048, %r12
	ja	.L404
	testq	%r12, %r12
	je	.L402
	movq	-704(%rbp), %rax
	leaq	(%r15,%r12), %rsi
	movabsq	$-4819355556693147648, %rcx
	leaq	40(%rax), %rdi
	movq	%r15, %rax
.L405:
	movq	(%rax), %rdx
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$48, %rdx
	movq	$0, (%rdi,%rdx,4)
	cmpq	%rax, %rsi
	jne	.L405
.L588:
	movq	-704(%rbp), %rax
	movl	(%rax), %ebx
	jmp	.L390
.L398:
	cmpq	$4096, %r12
	ja	.L406
	testq	%r12, %r12
	je	.L402
	movq	-704(%rbp), %rax
	leaq	(%r15,%r12), %rsi
	movabsq	$-4819355556693147648, %rcx
	leaq	40(%rax), %rdi
	movq	%r15, %rax
.L407:
	movq	(%rax), %rdx
	pxor	%xmm0, %xmm0
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$47, %rdx
	movups	%xmm0, (%rdi,%rdx,4)
	cmpq	%rsi, %rax
	jne	.L407
	jmp	.L588
.L395:
	movq	-704(%rbp), %rax
	movq	-704(%rbp), %rcx
	movl	524336(%rax), %eax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L367:
	movl	$262188, %eax
	jmp	.L345
.L423:
	movq	-704(%rbp), %r14
	movl	$131072, %edx
	movl	$204, %esi
	leaq	40(%r14), %rdi
	call	memset@PLT
	leaq	131112(%r14), %rdi
	movl	$65536, %edx
	xorl	%esi, %esi
	call	memset@PLT
.L425:
	movq	-704(%rbp), %r14
	movl	$65536, %edx
	xorl	%esi, %esi
	leaq	196648(%r14), %rdi
	call	memset@PLT
	leaq	1310768(%r14), %rdi
	movq	%r14, %rax
	movq	$0, 1310760(%r14)
	movq	$0, 1311776(%r14)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	1311784(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	jmp	.L390
.L411:
	addq	%rdx, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L390
.L406:
	movq	-704(%rbp), %rax
	movl	$524304, %edx
	xorl	%esi, %esi
	leaq	40(%rax), %rdi
	call	memset@PLT
.L402:
	movq	-704(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$1, 20(%rax)
	movups	%xmm0, 24(%rax)
	jmp	.L433
.L400:
	cmpq	$2048, %r12
	ja	.L401
	testq	%r12, %r12
	je	.L402
	movq	-704(%rbp), %rax
	leaq	(%r12,%r15), %rsi
	movabsq	$-4819355556693147648, %rcx
	leaq	40(%rax), %rdi
	movq	%r15, %rax
.L403:
	movq	(%rax), %rdx
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$48, %rdx
	movl	$0, (%rdi,%rdx,4)
	cmpq	%rax, %rsi
	jne	.L403
	jmp	.L588
.L401:
	movq	-704(%rbp), %rax
	movl	$262148, %edx
	xorl	%esi, %esi
	leaq	40(%rax), %rdi
	call	memset@PLT
	jmp	.L402
.L409:
	movq	-704(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$1, 20(%rax)
	movups	%xmm0, 24(%rax)
	cmpl	$55, %ebx
	jne	.L488
	movq	-704(%rbp), %rax
	movq	48(%rax), %rax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L404:
	movq	-704(%rbp), %rax
	movl	$262152, %edx
	xorl	%esi, %esi
	leaq	40(%rax), %rdi
	call	memset@PLT
	jmp	.L402
.L418:
	movq	-704(%rbp), %r14
	movl	$131072, %edx
	movl	$204, %esi
	leaq	40(%r14), %rdi
	call	memset@PLT
	leaq	131112(%r14), %rdi
	movl	$65536, %edx
	xorl	%esi, %esi
	call	memset@PLT
.L420:
	movq	-704(%rbp), %r14
	movl	$65536, %edx
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	leaq	196648(%r14), %rdi
	call	memset@PLT
	movw	%r13w, 524328(%r14)
	jmp	.L390
.L394:
	cmpq	$32768, %r12
	ja	.L428
	testq	%r12, %r12
	je	.L402
	movq	-704(%rbp), %rax
	leaq	(%r12,%r15), %rsi
	movabsq	$3866266742567714048, %rcx
	leaq	40(%rax), %rdi
	movq	%r15, %rax
.L429:
	movq	(%rax), %rdx
	pxor	%xmm0, %xmm0
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$44, %rdx
	movups	%xmm0, (%rdi,%rdx,4)
	cmpq	%rax, %rsi
	jne	.L429
	jmp	.L588
.L428:
	movq	-704(%rbp), %rax
	movl	$4194320, %edx
	xorl	%esi, %esi
	leaq	40(%rax), %rdi
	call	memset@PLT
	jmp	.L402
.L604:
	jmp	.L576
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	BrotliCompressBufferQuality10.cold, @function
BrotliCompressBufferQuality10.cold:
.LFSB339:
.L576:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$0, 0
	ud2
.L368:
	movdqa	-528(%rbp), %xmm1
	movaps	%xmm1, 0
	movl	-512(%rbp), %eax
	movl	%eax, 16
	ud2
	.cfi_endproc
.LFE339:
	.text
	.size	BrotliCompressBufferQuality10, .-BrotliCompressBufferQuality10
	.section	.text.unlikely
	.size	BrotliCompressBufferQuality10.cold, .-BrotliCompressBufferQuality10.cold
.LCOLDE11:
	.text
.LHOTE11:
	.section	.text.unlikely
.LCOLDB12:
	.text
.LHOTB12:
	.p2align 4
	.type	EncodeData, @function
EncodeData:
.LFB337:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movl	%edx, -104(%rbp)
	movq	256(%rdi), %rdx
	movq	%rcx, -88(%rbp)
	movq	%r8, -96(%rbp)
	movl	%edx, %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	shrq	$30, %rax
	cmpq	$2, %rax
	jbe	.L612
	leal	-1(%rax), %esi
	movl	%edx, %ecx
	andl	$1, %esi
	andl	$1073741823, %ecx
	addl	$1, %esi
	sall	$30, %esi
	orl	%ecx, %esi
	movl	%esi, %r14d
.L612:
	movl	5448(%r15), %ebx
	testl	%ebx, %ebx
	jne	.L616
	movl	172(%r15), %ebx
	movq	160(%r15), %rax
	movq	200(%r15), %r13
	movl	%ebx, -72(%rbp)
	testl	%r12d, %r12d
	je	.L615
	movl	$1, 5448(%r15)
.L615:
	subq	%rdx, %rax
	movl	12(%r15), %ecx
	movq	%rax, -80(%rbp)
	movq	%rax, %rbx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%rax, %rbx
	ja	.L616
	leaq	128(%r15), %rax
	movl	%ebx, -188(%rbp)
	movq	%rax, -120(%rbp)
	movl	4(%r15), %eax
	cmpl	$1, %eax
	je	.L1101
.L617:
	cmpl	$1, %eax
	jbe	.L622
	movq	-80(%rbp), %rbx
	movq	224(%r15), %rdx
	movl	%ebx, %eax
	shrl	%eax
	leaq	1(%rdx,%rax), %rax
	cmpq	%rax, 208(%r15)
	jnb	.L637
	movl	%ebx, %esi
	movl	$0, %ebx
	shrl	$2, %esi
	addl	$16, %esi
	addq	%rax, %rsi
	movq	%rsi, 208(%r15)
	jne	.L1102
.L638:
	movq	216(%r15), %rsi
	testq	%rsi, %rsi
	je	.L639
	movq	224(%r15), %rdx
	movq	%rbx, %rdi
	salq	$4, %rdx
	call	memcpy@PLT
	movq	216(%r15), %rsi
	leaq	128(%r15), %rdi
	call	BrotliFree
.L639:
	movq	%rbx, 216(%r15)
.L637:
	movl	-80(%rbp), %eax
	movl	-72(%rbp), %ebx
	movq	152(%r15), %r11
	movq	%rax, -128(%rbp)
	movl	%r14d, %eax
	testq	%rax, %rax
	movq	%rbx, -112(%rbp)
	sete	%bl
	movq	%rax, -176(%rbp)
	movq	%rax, -168(%rbp)
	andl	%r12d, %ebx
	testq	%r11, %r11
	je	.L1103
	movl	20(%r11), %r10d
	movl	(%r11), %eax
	testl	%r10d, %r10d
	je	.L684
.L685:
	cmpl	$65, %eax
	ja	.L732
	leaq	.L734(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L734:
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L746-.L734
	.long	.L745-.L734
	.long	.L744-.L734
	.long	.L743-.L734
	.long	.L742-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L741-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L740-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L739-.L734
	.long	.L738-.L734
	.long	.L737-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L736-.L734
	.long	.L735-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L732-.L734
	.long	.L733-.L734
	.text
	.p2align 4,,10
	.p2align 3
.L616:
	xorl	%eax, %eax
.L611:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1104
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L735:
	.cfi_restore_state
	movl	%r14d, %edx
	movq	-128(%rbp), %rbx
	movq	48(%r11), %rax
	andl	$3, %edx
	movl	%edx, %r9d
	cmpq	$6, %rbx
	jbe	.L755
	cmpl	$2, %r14d
	ja	.L1105
.L755:
	testl	%edx, %edx
	jne	.L758
	movq	-176(%rbp), %r8
	movq	-128(%rbp), %rbx
	movl	-72(%rbp), %ecx
	movq	%r8, %rdx
	notq	%rdx
	andq	-112(%rbp), %rdx
	andl	%r14d, %ecx
	cmpq	%rbx, %rdx
	cmova	%rbx, %rdx
.L757:
	cmpq	$31, %rdx
	jbe	.L759
	movl	$0, 40(%rax)
	movzbl	0(%r13,%rcx), %esi
	movl	68(%rax), %edx
	addl	$1, %esi
	movl	%esi, 40(%rax)
	imull	%edx, %esi
	movzbl	4(%r13,%rcx), %edi
	leal	1(%rdi,%rsi), %esi
	movl	%esi, 40(%rax)
	imull	%edx, %esi
	movzbl	8(%r13,%rcx), %edi
	leal	1(%rdi,%rsi), %esi
	movl	%esi, 40(%rax)
	imull	%edx, %esi
	movzbl	12(%r13,%rcx), %edi
	leal	1(%rdi,%rsi), %esi
	movl	%esi, 40(%rax)
	imull	%edx, %esi
	movzbl	16(%r13,%rcx), %edi
	leal	1(%rdi,%rsi), %esi
	movl	%esi, 40(%rax)
	imull	%edx, %esi
	movzbl	20(%r13,%rcx), %edi
	leal	1(%rdi,%rsi), %esi
	movl	%esi, 40(%rax)
	imull	%edx, %esi
	movzbl	24(%r13,%rcx), %edi
	leal	1(%rdi,%rsi), %esi
	imull	%esi, %edx
	movl	%esi, 40(%rax)
	movzbl	28(%r13,%rcx), %ecx
	leal	1(%rcx,%rdx), %edx
	movl	%edx, 40(%rax)
.L759:
	movq	%r8, 56(%rax)
	.p2align 4,,10
	.p2align 3
.L732:
	movq	248(%r15), %rcx
	movq	%rcx, %rax
	movl	%ecx, %esi
	shrq	$30, %rax
	cmpq	$2, %rax
	jbe	.L777
	subl	$1, %eax
	movl	%ecx, %edx
	andl	$1, %eax
	andl	$1073741823, %edx
	leal	1(%rax), %esi
	sall	$30, %esi
	orl	%edx, %esi
.L777:
	movl	$2, -136(%rbp)
	movl	4(%r15), %edx
	cmpl	$9, %edx
	jg	.L1106
.L778:
	movq	224(%r15), %rcx
	movq	216(%r15), %rax
	movq	%rcx, %rsi
	salq	$4, %rsi
	testq	%rcx, %rcx
	je	.L780
	cmpq	$0, 240(%r15)
	je	.L1107
.L780:
	leaq	232(%r15), %r8
	leaq	224(%r15), %rdi
	addq	%rsi, %rax
	leaq	240(%r15), %rcx
	leaq	264(%r15), %rbx
	cmpl	$10, %edx
	je	.L1108
	cmpl	$11, %edx
	je	.L1109
	subq	$8, %rsp
	movq	-168(%rbp), %rsi
	movq	%r11, %r9
	movq	%r13, %rdx
	pushq	%r8
	movq	%r15, %r8
	pushq	%rdi
	movq	-128(%rbp), %rdi
	pushq	%rax
	pushq	%rcx
	movq	-112(%rbp), %rcx
	pushq	%rbx
	call	BrotliCreateBackwardReferences
	addq	$48, %rsp
.L799:
	movl	12(%r15), %edx
	cmpl	%edx, 8(%r15)
	movl	$24, %eax
	movl	$1, %esi
	movq	%rsi, %rdi
	movq	248(%r15), %r8
	movl	%edx, %ecx
	cmovge	8(%r15), %ecx
	addl	$1, %ecx
	cmpl	$24, %ecx
	cmovg	%eax, %ecx
	movq	160(%r15), %rax
	salq	%cl, %rdi
	movq	%rax, %r11
	movl	%edx, %ecx
	subq	%r8, %r11
	salq	%cl, %rsi
	addq	%r11, %rsi
	cmpl	$3, 4(%r15)
	jg	.L801
	movq	224(%r15), %rdx
	addq	232(%r15), %rdx
	cmpq	$12286, %rdx
	ja	.L802
.L801:
	movl	-104(%rbp), %edx
	orl	%r12d, %edx
	andl	$1, %edx
	jne	.L802
	cmpq	%rdi, %rsi
	ja	.L802
	shrq	$3, %rdi
	cmpq	%rdi, 232(%r15)
	jnb	.L802
	cmpq	%rdi, 224(%r15)
	jnb	.L802
	movq	256(%r15), %rcx
	movq	%rcx, %rsi
	movl	%ecx, %edx
	shrq	$30, %rsi
	cmpq	$2, %rsi
	jbe	.L803
	leal	-1(%rsi), %edx
	andl	$1073741823, %ecx
	andl	$1, %edx
	addl	$1, %edx
	sall	$30, %edx
	orl	%ecx, %edx
.L803:
	movq	%rax, %rsi
	movl	%eax, %ecx
	shrq	$30, %rsi
	cmpq	$2, %rsi
	jbe	.L804
	leal	-1(%rsi), %ecx
	movl	%eax, %edi
	andl	$1, %ecx
	andl	$1073741823, %edi
	addl	$1, %ecx
	sall	$30, %ecx
	orl	%edi, %ecx
.L804:
	movq	%rax, 256(%r15)
	cmpl	%edx, %ecx
	jnb	.L805
	movq	152(%r15), %rax
	testq	%rax, %rax
	je	.L805
	movl	$0, 20(%rax)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L1103:
	movl	4(%r15), %eax
	movl	8(%r15), %ecx
	cmpl	$9, %eax
	jg	.L1110
	cmpl	$4, %eax
	je	.L1111
	jle	.L1112
	cmpl	$16, %ecx
	jg	.L651
	cmpl	$6, %eax
	jle	.L652
	cmpl	$9, %eax
	je	.L653
	movl	$41, 32(%r15)
.L654:
	movl	$524344, %esi
	.p2align 4,,10
	.p2align 3
.L647:
	leaq	128(%r15), %rdi
	call	BrotliAllocate
	movl	48(%r15), %edx
	movdqu	32(%r15), %xmm2
	movq	%rax, 152(%r15)
	movq	%rax, %r11
	movl	32(%r15), %eax
	movl	%edx, 16(%r11)
	leal	-5(%rax), %edx
	movups	%xmm2, (%r11)
	cmpl	$60, %edx
	ja	.L668
	leaq	.L829(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L829:
	.long	.L678-.L829
	.long	.L677-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L676-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L669-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L674-.L829
	.long	.L673-.L829
	.long	.L672-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L669-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L668-.L829
	.long	.L669-.L829
	.text
	.p2align 4,,10
	.p2align 3
.L802:
	movq	240(%r15), %rdi
	testq	%rdi, %rdi
	je	.L806
	movq	224(%r15), %rdx
	leaq	1(%rdx), %rcx
	salq	$4, %rdx
	addq	216(%r15), %rdx
	movq	%rcx, 224(%r15)
	movl	$16, %ecx
	movl	%edi, (%rdx)
	movq	$134217728, 4(%rdx)
	movw	%cx, 14(%rdx)
	cmpq	$5, %rdi
	jbe	.L1113
	cmpq	$129, %rdi
	jbe	.L1114
	cmpq	$2113, %rdi
	jbe	.L1115
	movl	$490, %ecx
	cmpq	$6209, %rdi
	jbe	.L808
	cmpq	$22594, %rdi
	sbbl	%ecx, %ecx
	andl	$-8, %ecx
	addw	$506, %cx
.L808:
	movw	%cx, 12(%rdx)
	addq	%rdi, 232(%r15)
	movq	$0, 240(%r15)
.L806:
	cmpq	%r8, %rax
	jne	.L869
	testb	$1, %r12b
	je	.L805
.L869:
	subl	%r8d, %eax
	movq	360(%r15), %r14
	leal	503(%rax,%rax), %edx
	cmpq	352(%r15), %rdx
	ja	.L1116
.L812:
	subq	$8, %rsp
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movq	%r13, %rsi
	movzbl	346(%r15), %eax
	movl	%r12d, %r9d
	movl	%r11d, %r8d
	movq	%rax, -64(%rbp)
	movzwl	344(%r15), %eax
	movb	%al, (%r14)
	movzbl	345(%r15), %eax
	movb	%al, 1(%r14)
	leaq	-64(%rbp), %rax
	movq	248(%r15), %rcx
	pushq	%r14
	pushq	%rax
	leaq	328(%r15), %rax
	pushq	%rbx
	pushq	%rax
	pushq	216(%r15)
	pushq	224(%r15)
	pushq	232(%r15)
	movzbl	348(%r15), %eax
	pushq	%rax
	movzbl	347(%r15), %eax
	pushq	%rax
	movl	-136(%rbp), %eax
	pushq	%r15
	pushq	%rax
	call	WriteMetaBlockInternal
	movq	-64(%rbp), %rax
	movq	256(%r15), %rdi
	addq	$96, %rsp
	movq	%rax, %rcx
	andl	$7, %eax
	movl	%edi, %esi
	shrq	$3, %rcx
	movzbl	(%r14,%rcx), %edx
	movb	%al, 346(%r15)
	movq	160(%r15), %rax
	movw	%dx, 344(%r15)
	movq	%rdi, %rdx
	shrq	$30, %rdx
	movq	%rax, 248(%r15)
	cmpq	$2, %rdx
	jbe	.L813
	leal	-1(%rdx), %esi
	andl	$1073741823, %edi
	andl	$1, %esi
	leal	1(%rsi), %esi
	sall	$30, %esi
	orl	%edi, %esi
.L813:
	movq	%rax, %rdx
	shrq	$30, %rdx
	cmpq	$2, %rdx
	jbe	.L814
	subl	$1, %edx
	movl	%eax, %edi
	movq	%rax, 256(%r15)
	andl	$1, %edx
	andl	$1073741823, %edi
	addl	$1, %edx
	sall	$30, %edx
	orl	%edi, %edx
	cmpl	%esi, %edx
	jb	.L815
	leal	-1(%rax), %edx
	andl	-72(%rbp), %edx
	movzbl	0(%r13,%rdx), %edx
	movb	%dl, 347(%r15)
.L817:
	subl	$2, %eax
	andl	-72(%rbp), %eax
	movzbl	0(%r13,%rax), %eax
	movb	%al, 348(%r15)
.L823:
	movdqu	264(%r15), %xmm1
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 224(%r15)
	movups	%xmm1, 328(%r15)
	movq	%r14, (%rax)
	movq	-88(%rbp), %rax
	movq	%rcx, (%rax)
	movl	$1, %eax
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L1101:
	cmpq	$0, 5384(%r15)
	je	.L618
.L622:
	movzbl	346(%r15), %eax
	cmpq	$0, -80(%rbp)
	movq	%rax, -64(%rbp)
	jne	.L621
	testb	$1, %r12b
	jne	.L621
.L805:
	movq	-88(%rbp), %rax
	movq	$0, (%rax)
.L1092:
	movl	$1, %eax
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L669:
	pxor	%xmm0, %xmm0
	movq	%r15, 56(%r11)
	movups	%xmm0, 40(%r11)
	movq	152(%r15), %r11
	movl	(%r11), %eax
.L668:
	movl	$0, 20(%r11)
.L684:
	movzbl	%bl, %esi
	cmpl	$65, %eax
	ja	.L840
	leaq	.L688(%rip), %rcx
	movl	%eax, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L688:
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L700-.L688
	.long	.L699-.L688
	.long	.L698-.L688
	.long	.L697-.L688
	.long	.L696-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L695-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L694-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L693-.L688
	.long	.L692-.L688
	.long	.L691-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L690-.L688
	.long	.L689-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L840-.L688
	.long	.L687-.L688
	.text
	.p2align 4,,10
	.p2align 3
.L840:
	movq	%r11, %rdx
.L686:
	testl	%r14d, %r14d
	je	.L702
.L730:
	movl	$1, 20(%r11)
	movq	%rdx, %r11
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L814:
	movq	%rax, 256(%r15)
	cmpl	%esi, %eax
	jnb	.L821
.L815:
	movq	152(%r15), %rdx
	testq	%rdx, %rdx
	je	.L821
	movl	$0, 20(%rdx)
.L821:
	testq	%rax, %rax
	je	.L823
	leal	-1(%rax), %edx
	andl	-72(%rbp), %edx
	movzbl	0(%r13,%rdx), %edx
	movb	%dl, 347(%r15)
	cmpq	$1, %rax
	ja	.L817
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	160(%r15), %rax
	movq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movsd	.LC10(%rip), %xmm0
	subq	%rcx, %rax
	movq	%rax, %rcx
	call	BrotliIsMostlyUTF8
	movl	4(%r15), %edx
	movq	152(%r15), %r11
	testl	%eax, %eax
	jne	.L778
	movl	$3, -136(%rbp)
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	%r14, %rsi
	movq	-120(%rbp), %r14
	movq	%r11, -104(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r14, %rdi
	call	BrotliFree
	movq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movq	$0, 360(%r15)
	movq	%rdx, %rsi
	call	BrotliAllocate
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %r11
	movq	%rax, 360(%r15)
	movq	%rax, %r14
	movq	%rdx, 352(%r15)
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	-168(%rbp), %rdx
	movq	-128(%rbp), %rsi
	pushq	%r8
	movq	%r15, %r9
	pushq	%rdi
	movq	-112(%rbp), %r8
	pushq	%rax
	movq	-120(%rbp), %rdi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rbx
	pushq	%r11
	call	BrotliCreateHqZopfliBackwardReferences
	addq	$48, %rsp
	jmp	.L799
.L690:
	movq	-128(%rbp), %rdi
	cmpq	$32768, %rdi
	ja	.L727
	testb	%bl, %bl
	je	.L727
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L846
	leaq	40(%r11), %rdi
	movq	%r13, %rax
	leaq	(%rbx,%r13), %rsi
	movabsq	$3866266742567714048, %rcx
	.p2align 4,,10
	.p2align 3
.L728:
	movq	(%rax), %rdx
	pxor	%xmm0, %xmm0
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$44, %rdx
	movups	%xmm0, (%rdi,%rdx,4)
	cmpq	%rax, %rsi
	jne	.L728
.L1087:
	movq	152(%r15), %rdx
	movl	(%rdx), %eax
.L702:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%r11)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L621:
	movq	-80(%rbp), %rax
	movq	360(%r15), %r11
	leal	503(%rax,%rax), %ebx
	cmpq	352(%r15), %rbx
	ja	.L1117
.L625:
	movzwl	344(%r15), %eax
	movl	-80(%rbp), %r10d
	movb	%al, (%r11)
	movzbl	345(%r15), %eax
	movb	%al, 1(%r11)
	movl	4(%r15), %ecx
	testl	%ecx, %ecx
	jne	.L626
	cmpq	$32768, %r10
	movl	$32768, %eax
	cmovbe	%r10, %rax
	cmpq	$256, %r10
	ja	.L824
	movl	$2048, %edx
	movl	$512, %ebx
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	368(%r15), %r8
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	-168(%rbp), %rdx
	movq	-128(%rbp), %rsi
	pushq	%r8
	movq	%r15, %r9
	pushq	%rdi
	movq	-112(%rbp), %r8
	pushq	%rax
	movq	-120(%rbp), %rdi
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rbx
	pushq	%r11
	call	BrotliCreateZopfliBackwardReferences
	addq	$48, %rsp
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L1110:
	movl	$1, %eax
	movq	-128(%rbp), %rdi
	movl	$10, 32(%r15)
	salq	%cl, %rax
	cmpq	%rdi, %rax
	jbe	.L642
	testb	%bl, %bl
	cmovne	%rdi, %rax
.L642:
	leaq	524344(,%rax,8), %rsi
	.p2align 4,,10
	.p2align 3
.L663:
	testq	%rsi, %rsi
	jne	.L647
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L626:
	movl	$131072, %eax
	movl	$1024, %edx
	movl	$256, %ebx
	cmpq	$131072, %r10
	cmovbe	%r10, %rax
	cmpq	$256, %r10
	jbe	.L627
.L824:
	movl	$256, %ebx
	.p2align 4,,10
	.p2align 3
.L628:
	movq	%rbx, %rdx
	addq	%rbx, %rbx
	cmpq	%rax, %rbx
	jb	.L628
	testl	%ecx, %ecx
	jne	.L1085
	testl	$699050, %ebx
	je	.L631
.L1085:
	leaq	0(,%rbx,4), %rdx
.L630:
	cmpq	$1024, %rbx
	jbe	.L627
	movq	4464(%r15), %r8
	cmpq	4472(%r15), %rbx
	ja	.L1118
.L633:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r11, -104(%rbp)
	movq	%r10, -80(%rbp)
	call	memset@PLT
	movl	-72(%rbp), %esi
	movl	4(%r15), %r11d
	movq	-80(%rbp), %r10
	movq	%rax, %r8
	andl	%r14d, %esi
	addq	%r13, %rsi
	testl	%r11d, %r11d
	movq	-104(%rbp), %r11
	jne	.L635
	leaq	-64(%rbp), %rax
	pushq	%r11
	movq	-120(%rbp), %rdi
	movq	%rbx, %r9
	pushq	%rax
	leaq	4864(%r15), %rax
	movl	%r12d, %ecx
	movq	%r10, %rdx
	pushq	%rax
	leaq	5376(%r15), %rax
	pushq	%rax
	leaq	4608(%r15), %rax
	pushq	%rax
	leaq	4480(%r15), %rax
	pushq	%rax
	movq	%r11, -72(%rbp)
	call	BrotliCompressFragmentFast
	movq	-72(%rbp), %r11
	addq	$48, %rsp
.L636:
	movq	-64(%rbp), %rax
	movq	%rax, %rdx
	andl	$7, %eax
	shrq	$3, %rdx
	movzbl	(%r11,%rdx), %ecx
	movb	%al, 346(%r15)
	movq	160(%r15), %rax
	movw	%cx, 344(%r15)
	movq	%rax, 256(%r15)
	movq	-96(%rbp), %rax
	movq	%r11, (%rax)
	movq	-88(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	128(%r15), %rbx
	movl	$524288, %esi
	movq	%rbx, %rdi
	call	BrotliAllocate
	movl	$131072, %esi
	movq	%rbx, %rdi
	movq	%rax, 5384(%r15)
	call	BrotliAllocate
	movq	%rax, 5392(%r15)
	movl	4(%r15), %eax
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	%r11, %rsi
	leaq	128(%r15), %rdi
	call	BrotliFree
	movq	%rbx, %rsi
	leaq	128(%r15), %rdi
	movq	$0, 360(%r15)
	call	BrotliAllocate
	movq	%rbx, 352(%r15)
	movq	%rax, 360(%r15)
	movq	%rax, %r11
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L1115:
	leaq	-66(%rdi), %rsi
	bsrl	%esi, %esi
	addl	$10, %esi
.L1091:
	movl	%esi, %ecx
	movl	$5377344, %r9d
	shrw	$3, %cx
	movzwl	%cx, %ecx
	leal	(%rcx,%rcx,2), %r10d
	leal	(%r10,%r10), %ecx
	sall	$6, %r10d
	shrl	%cl, %r9d
	leal	0(,%rsi,8), %ecx
	andl	$192, %r9d
	andl	$56, %ecx
	leal	64(%r9,%r10), %r9d
	orl	%r9d, %ecx
	orl	$2, %ecx
	jmp	.L808
.L673:
	movl	4(%r15), %edx
	xorl	%eax, %eax
	cmpl	$7, %edx
	leal	-4(%rdx), %ecx
	setl	%al
	addl	$7, %eax
	sall	%cl, %eax
	movq	%rax, 524336(%r11)
	movl	$41, %eax
	jmp	.L668
.L674:
	movl	4(%r15), %edx
	xorl	%eax, %eax
	cmpl	$7, %edx
	leal	-4(%rdx), %ecx
	setl	%al
	addl	$7, %eax
	sall	%cl, %eax
	movq	%rax, 524336(%r11)
	movl	$40, %eax
	jmp	.L668
.L677:
	movl	4(%r11), %edx
	movl	$64, %eax
	movl	$8, %ecx
	subl	12(%r11), %ecx
	sall	$3, %ecx
	subl	%edx, %eax
	movl	%eax, 56(%r11)
	movq	$-1, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	movq	%rax, 64(%r11)
	movl	$1, %eax
	movq	%rax, %rdi
	salq	%cl, %rdi
	movl	8(%r11), %ecx
	movq	%rdi, 40(%r11)
	salq	%cl, %rax
	movq	%rax, 48(%r11)
	subl	$1, %eax
	movl	%eax, 72(%r11)
	movl	$6, %eax
	jmp	.L668
.L678:
	movl	4(%r11), %ecx
	movl	$32, %eax
	subl	%ecx, %eax
	movl	%eax, 56(%r11)
	movl	$1, %eax
	movq	%rax, %rdx
	salq	%cl, %rdx
	movl	8(%r11), %ecx
	movq	%rdx, 40(%r11)
	salq	%cl, %rax
	movq	%rax, 48(%r11)
	subl	$1, %eax
	movl	%eax, 60(%r11)
	movl	$5, %eax
	jmp	.L668
.L676:
	movl	8(%r15), %ecx
	movl	$1, %eax
	movl	%eax, %edx
	sall	%cl, %edx
	subl	%edx, %eax
	leal	-1(%rdx), %edi
	movl	%eax, 524336(%r11)
	movl	$10, %eax
	movq	%rdi, 40(%r11)
	jmp	.L668
.L672:
	movl	4(%r15), %edx
	xorl	%eax, %eax
	cmpl	$7, %edx
	leal	-4(%rdx), %ecx
	setl	%al
	addl	$7, %eax
	sall	%cl, %eax
	movq	%rax, 1311784(%r11)
	movl	$42, %eax
	jmp	.L668
.L733:
	cmpq	$6, -128(%rbp)
	jbe	.L761
	cmpl	$2, %r14d
	jbe	.L761
	movq	-176(%rbp), %rdi
	movq	40(%r11), %rax
	leaq	-3(%rdi), %rdx
	andq	-112(%rbp), %rdx
	movq	64(%rax), %r9
	leaq	80(%rax), %rbx
	movq	0(%r13,%rdx), %rcx
	movzwl	72(%rax), %esi
	andq	%r9, %rcx
	movq	%rcx, %rdx
	movabsq	$2297779722762296275, %rcx
	imulq	%rcx, %rdx
	movl	56(%rax), %ecx
	shrq	%cl, %rdx
	movl	%edx, %ecx
	leaq	(%rbx,%rcx,2), %rdi
	movq	40(%rax), %rcx
	movzwl	(%rdi), %r8d
	leaq	80(%rcx,%rcx), %r10
	movl	8(%rax), %ecx
	andl	%r8d, %esi
	addl	$1, %r8d
	movzwl	%si, %esi
	sall	%cl, %edx
	leal	-3(%r14), %ecx
	addq	%rsi, %rdx
	leaq	(%rax,%rdx,4), %rdx
	movl	%ecx, (%rdx,%r10)
	movzwl	72(%rax), %esi
	movabsq	$2297779722762296275, %rcx
	movw	%r8w, (%rdi)
	movq	-176(%rbp), %rdi
	leaq	-2(%rdi), %rdx
	andq	-112(%rbp), %rdx
	movq	0(%r13,%rdx), %r8
	andq	%r9, %r8
	movq	%r8, %rdx
	imulq	%rcx, %rdx
	movl	56(%rax), %ecx
	shrq	%cl, %rdx
	movl	%edx, %ecx
	leaq	(%rbx,%rcx,2), %r8
	movl	8(%rax), %ecx
	movzwl	(%r8), %edi
	sall	%cl, %edx
	leal	-2(%r14), %ecx
	andl	%edi, %esi
	addl	$1, %edi
	movzwl	%si, %esi
	addq	%rsi, %rdx
	leaq	(%rax,%rdx,4), %rdx
	movl	%ecx, (%rdx,%r10)
	movzwl	72(%rax), %esi
	movabsq	$2297779722762296275, %rcx
	movw	%di, (%r8)
	movq	-176(%rbp), %rdi
	leaq	-1(%rdi), %rdx
	andq	-112(%rbp), %rdx
	andq	0(%r13,%rdx), %r9
	movq	%r9, %rdx
	imulq	%rcx, %rdx
	movl	56(%rax), %ecx
	shrq	%cl, %rdx
	movl	%edx, %ecx
	leaq	(%rbx,%rcx,2), %r8
	movl	8(%rax), %ecx
	movzwl	(%r8), %edi
	sall	%cl, %edx
	andl	%edi, %esi
	addl	$1, %edi
	movzwl	%si, %esi
	addq	%rsi, %rdx
	leaq	(%rax,%rdx,4), %rax
	leal	-1(%r14), %edx
	movl	%edx, (%rax,%r10)
	movw	%di, (%r8)
.L761:
	movl	-72(%rbp), %ecx
	movq	-112(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	48(%r11), %rsi
	andl	%r14d, %ecx
	subq	%rcx, %rax
	cmpq	%rbx, %rax
	cmova	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L764
	movl	68(%rsi), %r8d
	leaq	0(%r13,%rcx), %rdx
	leaq	32(%r13,%rcx), %rdi
	xorl	%eax, %eax
	movl	$0, 40(%rsi)
	.p2align 4,,10
	.p2align 3
.L763:
	imull	%r8d, %eax
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	leal	1(%rax,%rcx), %eax
	movl	%eax, 40(%rsi)
	cmpq	%rdx, %rdi
	jne	.L763
.L764:
	movq	-176(%rbp), %rax
	movq	%rax, 56(%rsi)
	jmp	.L732
.L736:
	cmpq	$6, -128(%rbp)
	jbe	.L732
	cmpl	$2, %r14d
	jbe	.L732
	movq	-176(%rbp), %rdi
	movq	-112(%rbp), %rbx
	movabsq	$3866266742567714048, %rax
	leaq	-3(%rdi), %rdx
	movq	%rbx, %rcx
	andq	%rdx, %rcx
	shrq	$3, %rdx
	movq	0(%r13,%rcx), %rsi
	andl	$3, %edx
	imulq	%rax, %rsi
	movq	%rsi, %rcx
	shrq	$44, %rcx
	addq	%rcx, %rdx
	leal	-3(%r14), %ecx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-2(%rdi), %rdx
	movq	%rbx, %rcx
	andq	%rdx, %rcx
	shrq	$3, %rdx
	movq	0(%r13,%rcx), %rsi
	andl	$3, %edx
	imulq	%rax, %rsi
	movq	%rsi, %rcx
	shrq	$44, %rcx
	addq	%rcx, %rdx
	leal	-2(%r14), %ecx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-1(%rdi), %rdx
	andq	%rdx, %rbx
	imulq	0(%r13,%rbx), %rax
	shrq	$44, %rax
.L1090:
	shrq	$3, %rdx
	andl	$3, %edx
	addq	%rdx, %rax
	leal	-1(%r14), %edx
	movl	%edx, 40(%r11,%rax,4)
	jmp	.L732
.L737:
	cmpl	$2, %r14d
	jbe	.L732
	cmpq	$2, -128(%rbp)
	jbe	.L732
	movq	-176(%rbp), %rax
	leaq	-3(%rax), %rdx
	movq	-112(%rbp), %rax
	movq	%rdx, %rbx
	andq	%rdx, %rax
	movzwl	%dx, %edx
	imull	$506832829, 0(%r13,%rax), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	movl	%ecx, %r9d
	andl	$511, %r8d
	leaq	(%r11,%r8,2), %rsi
	salq	$9, %r8
	movzwl	1310760(%rsi), %eax
	leal	1(%rax), %edi
	movl	%eax, %r10d
	andl	$511, %eax
	movw	%di, 1310760(%rsi)
	leaq	(%r11,%r9,4), %rdi
	andw	$511, %r10w
	addq	%r8, %rax
	movl	40(%rdi), %esi
	leaq	(%r11,%rax,4), %rax
	movb	%cl, 196648(%r11,%rdx)
	leaq	(%r11,%r9,2), %rdx
	subq	%rsi, %rbx
	cmpq	$65535, %rbx
	movq	%rbx, %rsi
	movl	$65535, %ebx
	cmova	%rbx, %rsi
	movw	%si, 262184(%rax)
	movzwl	131112(%rdx), %ecx
	movw	%cx, 262186(%rax)
	leal	-3(%r14), %eax
	movl	%eax, 40(%rdi)
	movq	-176(%rbp), %rdi
	movq	-112(%rbp), %rax
	movw	%r10w, 131112(%rdx)
	leaq	-2(%rdi), %rdx
	andq	%rdx, %rax
	movq	%rdx, %rbx
	movzwl	%dx, %edx
	imull	$506832829, 0(%r13,%rax), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	movl	%ecx, %r9d
	andl	$511, %r8d
	leaq	(%r11,%r8,2), %rsi
	salq	$9, %r8
	movzwl	1310760(%rsi), %eax
	leal	1(%rax), %edi
	movl	%eax, %r10d
	andl	$511, %eax
	movw	%di, 1310760(%rsi)
	leaq	(%r11,%r9,4), %rdi
	andw	$511, %r10w
	addq	%r8, %rax
	movl	40(%rdi), %esi
	leaq	(%r11,%rax,4), %rax
	movb	%cl, 196648(%r11,%rdx)
	leaq	(%r11,%r9,2), %rdx
	subq	%rsi, %rbx
	cmpq	$65535, %rbx
	movq	%rbx, %rsi
	movl	$65535, %ebx
	cmova	%rbx, %rsi
	movw	%si, 262184(%rax)
	movzwl	131112(%rdx), %ecx
	movw	%cx, 262186(%rax)
	leal	-2(%r14), %eax
	movl	%eax, 40(%rdi)
	movq	-176(%rbp), %rdi
	movq	-112(%rbp), %rax
	movw	%r10w, 131112(%rdx)
	leaq	-1(%rdi), %rdx
	andq	%rdx, %rax
	movq	%rdx, %rbx
	movzwl	%dx, %edx
	imull	$506832829, 0(%r13,%rax), %ecx
	shrl	$17, %ecx
	movl	%ecx, %r8d
	movl	%ecx, %r9d
	andl	$511, %r8d
	leaq	(%r11,%r8,2), %rsi
	salq	$9, %r8
	movzwl	1310760(%rsi), %eax
	leal	1(%rax), %edi
	movl	%eax, %r10d
	andl	$511, %eax
	movw	%di, 1310760(%rsi)
	leaq	(%r11,%r9,4), %rdi
	andw	$511, %r10w
	addq	%r8, %rax
	movl	40(%rdi), %esi
	leaq	(%r11,%rax,4), %rax
	movb	%cl, 196648(%r11,%rdx)
	leaq	(%r11,%r9,2), %rdx
	subq	%rsi, %rbx
	movq	%rbx, %rsi
	cmpq	$65535, %rbx
	movl	$65535, %ebx
	cmova	%rbx, %rsi
	movw	%si, 262184(%rax)
	movzwl	131112(%rdx), %ecx
	movw	%cx, 262186(%rax)
	leal	-1(%r14), %eax
	movl	%eax, 40(%rdi)
	movw	%r10w, 131112(%rdx)
	jmp	.L732
.L746:
	cmpl	$2, %r14d
	jbe	.L732
	cmpq	$6, -128(%rbp)
	jbe	.L732
	movq	-176(%rbp), %rbx
	movq	-112(%rbp), %rdi
	leal	-3(%r14), %ecx
	movabsq	$-4819355556693147648, %rax
	leaq	-3(%rbx), %rdx
	andq	%rdi, %rdx
	movq	0(%r13,%rdx), %rsi
	imulq	%rax, %rsi
	movq	%rsi, %rdx
	shrq	$48, %rdx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-2(%rbx), %rdx
	leal	-2(%r14), %ecx
	andq	%rdi, %rdx
	movq	0(%r13,%rdx), %rsi
	imulq	%rax, %rsi
	movq	%rsi, %rdx
	shrq	$48, %rdx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-1(%rbx), %rdx
	andq	%rdi, %rdx
	imulq	0(%r13,%rdx), %rax
	leal	-1(%r14), %edx
	shrq	$48, %rax
	movl	%edx, 40(%r11,%rax,4)
	jmp	.L732
.L738:
	cmpq	$2, -128(%rbp)
	jbe	.L732
	cmpl	$2, %r14d
	jbe	.L732
.L1088:
	movq	-176(%rbp), %r10
	movq	-112(%rbp), %rbx
	movzwl	524328(%r11), %r8d
	movq	%r10, %rax
	movq	%rbx, %rdx
	subq	$3, %rax
	andq	%rax, %rdx
	movq	%rax, %r9
	movzwl	%ax, %eax
	imull	$506832829, 0(%r13,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %edi
	leaq	(%r11,%rdi,4), %rsi
	movl	40(%rsi), %edx
	movb	%cl, 196648(%r11,%rax)
	movzwl	%r8w, %eax
	leaq	(%r11,%rax,4), %rax
	subq	%rdx, %r9
	movq	%r9, %rdx
	movl	$65535, %r9d
	cmpq	$65535, %rdx
	cmova	%r9, %rdx
	movw	%dx, 262184(%rax)
	leaq	(%r11,%rdi,2), %rdx
	movzwl	131112(%rdx), %ecx
	movw	%cx, 262186(%rax)
	leal	-3(%r14), %eax
	movl	%eax, 40(%rsi)
	movq	%r10, %rax
	movw	%r8w, 131112(%rdx)
	subq	$2, %rax
	movq	%rbx, %rdx
	leal	2(%r8), %ebx
	andq	%rax, %rdx
	movq	%rax, %r10
	movzwl	%ax, %eax
	imull	$506832829, 0(%r13,%rdx), %ecx
	shrl	$17, %ecx
	movl	%ecx, %edi
	leaq	(%r11,%rdi,4), %rsi
	movl	40(%rsi), %edx
	movb	%cl, 196648(%r11,%rax)
	subq	%rdx, %r10
	movq	%r10, %rdx
	leal	1(%r8), %r10d
	cmpq	$65535, %rdx
	movzwl	%r10w, %eax
	cmova	%r9, %rdx
	leaq	(%r11,%rax,4), %rax
	addl	$3, %r8d
	movw	%dx, 262184(%rax)
	leaq	(%r11,%rdi,2), %rdx
	movzwl	131112(%rdx), %ecx
	movw	%cx, 262186(%rax)
	leal	-2(%r14), %eax
	movl	%eax, 40(%rsi)
	movw	%r10w, 131112(%rdx)
	movq	-176(%rbp), %rax
	movq	-112(%rbp), %rdx
	subq	$1, %rax
	andq	%rax, %rdx
	movq	%rax, %r10
	movzwl	%ax, %eax
	imull	$506832829, 0(%r13,%rdx), %ecx
	movw	%r8w, 524328(%r11)
	shrl	$17, %ecx
	movl	%ecx, %esi
	leaq	(%r11,%rsi,4), %rdi
	movl	40(%rdi), %edx
	movb	%cl, 196648(%r11,%rax)
	movzwl	%bx, %eax
	leaq	(%r11,%rax,4), %rax
	subq	%rdx, %r10
	movq	%r10, %rdx
	cmpq	$65535, %r10
	cmova	%r9, %rdx
	movw	%dx, 262184(%rax)
	leaq	(%r11,%rsi,2), %rdx
	movzwl	131112(%rdx), %ecx
	movw	%cx, 262186(%rax)
	leal	-1(%r14), %eax
	movl	%eax, 40(%rdi)
	movw	%bx, 131112(%rdx)
	jmp	.L732
.L739:
	cmpl	$2, %r14d
	jbe	.L732
	cmpq	$2, -128(%rbp)
	jbe	.L732
	jmp	.L1088
.L740:
	movl	%r14d, %edx
	movq	-128(%rbp), %rbx
	movq	48(%r11), %rax
	andl	$3, %edx
	movl	%edx, %r9d
	cmpq	$6, %rbx
	jbe	.L755
	cmpl	$2, %r14d
	jbe	.L755
	movq	-112(%rbp), %r10
	movq	40(%r11), %r8
	movabsq	$-4819355556693147648, %rcx
	movq	-176(%rbp), %rdi
	movq	%r8, -136(%rbp)
	leaq	-3(%rdi), %rsi
	movq	%r10, %rdi
	andq	%rsi, %rdi
	shrq	$3, %rsi
	movq	0(%r13,%rdi), %r8
	andl	$1, %esi
	imulq	%rcx, %r8
	movq	%r8, %rdi
	movq	-136(%rbp), %r8
	shrq	$48, %rdi
	addq	%rdi, %rsi
	leal	-3(%r14), %edi
	movl	%edi, 40(%r8,%rsi,4)
	movq	-176(%rbp), %rdi
	leaq	-2(%rdi), %rsi
	movq	%r10, %rdi
	andq	%rsi, %rdi
	shrq	$3, %rsi
	movq	0(%r13,%rdi), %r8
	andl	$1, %esi
	imulq	%rcx, %r8
	movq	%r8, %rdi
	movq	-136(%rbp), %r8
	shrq	$48, %rdi
	addq	%rdi, %rsi
	leal	-2(%r14), %edi
	movl	%edi, 40(%r8,%rsi,4)
	movq	-176(%rbp), %r8
	movq	%r10, %rdi
	leaq	-1(%r8), %rsi
	andq	%rsi, %rdi
	shrq	$3, %rsi
	imulq	0(%r13,%rdi), %rcx
	andl	$1, %esi
	shrq	$48, %rcx
.L1098:
	movq	-136(%rbp), %rdi
	addq	%rsi, %rcx
	leal	-1(%r14), %esi
	movl	%esi, 40(%rdi,%rcx,4)
	testl	%edx, %edx
	jne	.L756
	movq	%r8, %rdx
	movl	-72(%rbp), %ecx
	notq	%rdx
	andq	%r10, %rdx
	andl	%r14d, %ecx
	cmpq	%rbx, %rdx
	cmova	%rbx, %rdx
	jmp	.L757
.L742:
	cmpq	$6, -128(%rbp)
	jbe	.L732
	cmpl	$2, %r14d
	jbe	.L732
	movq	-176(%rbp), %rax
	movq	64(%r11), %r8
	leaq	80(%r11), %rbx
	movabsq	$2297779722762296275, %r10
	movl	56(%r11), %ecx
	subq	$3, %rax
	andq	-112(%rbp), %rax
	movq	0(%r13,%rax), %rsi
	andq	%r8, %rsi
	movq	%rsi, %rax
	imulq	%r10, %rax
	shrq	%cl, %rax
	movl	8(%r11), %ecx
	movl	%eax, %edx
	leaq	(%rbx,%rdx,2), %rdi
	movq	40(%r11), %rdx
	sall	%cl, %eax
	movzwl	(%rdi), %esi
	leaq	80(%rdx,%rdx), %r9
	movzwl	72(%r11), %edx
	andl	%esi, %edx
	addl	$1, %esi
	movzwl	%dx, %edx
	addq	%rdx, %rax
	leal	-3(%r14), %edx
	leaq	(%r11,%rax,4), %rax
	movl	%edx, (%rax,%r9)
	movq	-176(%rbp), %rax
	movw	%si, (%rdi)
	movl	56(%r11), %ecx
	subq	$2, %rax
	andq	-112(%rbp), %rax
	movq	0(%r13,%rax), %rsi
	andq	%r8, %rsi
	movq	%rsi, %rax
	imulq	%r10, %rax
	shrq	%cl, %rax
	movl	8(%r11), %ecx
	movl	%eax, %edx
	leaq	(%rbx,%rdx,2), %rdi
	movzwl	72(%r11), %edx
	sall	%cl, %eax
	movzwl	(%rdi), %esi
	andl	%esi, %edx
	addl	$1, %esi
	movzwl	%dx, %edx
	addq	%rdx, %rax
	leal	-2(%r14), %edx
	leaq	(%r11,%rax,4), %rax
	movl	%edx, (%rax,%r9)
	movq	-176(%rbp), %rax
	movl	56(%r11), %ecx
	movw	%si, (%rdi)
	subq	$1, %rax
	andq	-112(%rbp), %rax
	andq	0(%r13,%rax), %r8
	movq	%r8, %rax
	imulq	%r10, %rax
	shrq	%cl, %rax
	movl	%eax, %edx
	leaq	(%rbx,%rdx,2), %rdi
	movzwl	72(%r11), %edx
	movzwl	(%rdi), %esi
	andl	%esi, %edx
.L1089:
	movl	8(%r11), %ecx
	movzwl	%dx, %edx
	addl	$1, %esi
	sall	%cl, %eax
	addq	%rdx, %rax
	leal	-1(%r14), %edx
	leaq	(%r11,%rax,4), %rax
	movl	%edx, (%rax,%r9)
	movw	%si, (%rdi)
	jmp	.L732
.L744:
	cmpl	$2, %r14d
	jbe	.L732
	cmpq	$6, -128(%rbp)
	jbe	.L732
	movq	-176(%rbp), %rdi
	movq	-112(%rbp), %rbx
	movabsq	$-4819355556693147648, %rax
	leaq	-3(%rdi), %rdx
	movq	%rbx, %rcx
	andq	%rdx, %rcx
	shrq	$3, %rdx
	movq	0(%r13,%rcx), %rsi
	andl	$3, %edx
	imulq	%rax, %rsi
	movq	%rsi, %rcx
	shrq	$47, %rcx
	addq	%rcx, %rdx
	leal	-3(%r14), %ecx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-2(%rdi), %rdx
	movq	%rbx, %rcx
	andq	%rdx, %rcx
	shrq	$3, %rdx
	movq	0(%r13,%rcx), %rsi
	andl	$3, %edx
	imulq	%rax, %rsi
	movq	%rsi, %rcx
	shrq	$47, %rcx
	addq	%rcx, %rdx
	leal	-2(%r14), %ecx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-1(%rdi), %rdx
	andq	%rdx, %rbx
	imulq	0(%r13,%rbx), %rax
	shrq	$47, %rax
	jmp	.L1090
.L745:
	cmpq	$6, -128(%rbp)
	jbe	.L732
	cmpl	$2, %r14d
	jbe	.L732
	movq	-176(%rbp), %rdi
	movq	-112(%rbp), %rbx
	movabsq	$-4819355556693147648, %rax
	leaq	-3(%rdi), %rdx
	movq	%rbx, %rcx
	andq	%rdx, %rcx
	shrq	$3, %rdx
	movq	0(%r13,%rcx), %rsi
	andl	$1, %edx
	imulq	%rax, %rsi
	movq	%rsi, %rcx
	shrq	$48, %rcx
	addq	%rcx, %rdx
	leal	-3(%r14), %ecx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-2(%rdi), %rdx
	movq	%rbx, %rcx
	andq	%rdx, %rcx
	shrq	$3, %rdx
	movq	0(%r13,%rcx), %rsi
	andl	$1, %edx
	imulq	%rax, %rsi
	movq	%rsi, %rcx
	shrq	$48, %rcx
	addq	%rcx, %rdx
	leal	-2(%r14), %ecx
	movl	%ecx, 40(%r11,%rdx,4)
	leaq	-1(%rdi), %rdx
	andq	%rdx, %rbx
	shrq	$3, %rdx
	imulq	0(%r13,%rbx), %rax
	andl	$1, %edx
	shrq	$48, %rax
	addq	%rdx, %rax
	leal	-1(%r14), %edx
	movl	%edx, 40(%r11,%rax,4)
	jmp	.L732
.L743:
	cmpl	$2, %r14d
	jbe	.L732
	cmpq	$2, -128(%rbp)
	jbe	.L732
	movq	-176(%rbp), %rbx
	movq	-112(%rbp), %r10
	leaq	64(%r11), %r8
	movl	56(%r11), %ecx
	leaq	-3(%rbx), %rax
	andq	%r10, %rax
	imull	$506832829, 0(%r13,%rax), %eax
	shrl	%cl, %eax
	movl	8(%r11), %ecx
	movl	%eax, %edx
	leaq	(%r8,%rdx,2), %rdi
	movq	40(%r11), %rdx
	sall	%cl, %eax
	movzwl	(%rdi), %esi
	leaq	64(%rdx,%rdx), %r9
	movzwl	60(%r11), %edx
	andl	%esi, %edx
	addl	$1, %esi
	movzwl	%dx, %edx
	addq	%rdx, %rax
	leal	-3(%r14), %edx
	leaq	(%r11,%rax,4), %rax
	movl	%edx, (%rax,%r9)
	leaq	-2(%rbx), %rax
	movl	56(%r11), %ecx
	andq	%r10, %rax
	movw	%si, (%rdi)
	imull	$506832829, 0(%r13,%rax), %eax
	shrl	%cl, %eax
	movl	8(%r11), %ecx
	movl	%eax, %edx
	leaq	(%r8,%rdx,2), %rdi
	movzwl	60(%r11), %edx
	sall	%cl, %eax
	movzwl	(%rdi), %esi
	andl	%esi, %edx
	addl	$1, %esi
	movzwl	%dx, %edx
	addq	%rdx, %rax
	leal	-2(%r14), %edx
	leaq	(%r11,%rax,4), %rax
	movl	%edx, (%rax,%r9)
	leaq	-1(%rbx), %rax
	movl	56(%r11), %ecx
	andq	%r10, %rax
	movw	%si, (%rdi)
	imull	$506832829, 0(%r13,%rax), %eax
	shrl	%cl, %eax
	movl	%eax, %edx
	leaq	(%r8,%rdx,2), %rdi
	movzwl	60(%r11), %edx
	movzwl	(%rdi), %esi
	andl	%esi, %edx
	jmp	.L1089
.L687:
	movq	-128(%rbp), %rdx
	movq	%r11, %rdi
	movq	%r13, %rcx
	movq	%r11, -136(%rbp)
	call	PrepareH65
	movq	152(%r15), %rdx
	movq	-136(%rbp), %r11
	movl	(%rdx), %eax
	jmp	.L686
.L689:
	movq	-128(%rbp), %rdx
	movq	%r11, %rdi
	movq	%r13, %rcx
	movq	%r11, -136(%rbp)
	call	PrepareH55
	movq	152(%r15), %rdx
	movq	-136(%rbp), %r11
	movl	(%rdx), %eax
	jmp	.L686
.L698:
	movq	-128(%rbp), %rdi
	cmpq	$4096, %rdi
	ja	.L706
	testb	%bl, %bl
	je	.L706
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L846
	leaq	40(%r11), %rdi
	movq	%r13, %rax
	leaq	(%rbx,%r13), %rsi
	movabsq	$-4819355556693147648, %rcx
	.p2align 4,,10
	.p2align 3
.L707:
	movq	(%rax), %rdx
	pxor	%xmm0, %xmm0
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$47, %rdx
	movups	%xmm0, (%rdi,%rdx,4)
	cmpq	%rsi, %rax
	jne	.L707
	jmp	.L1087
.L699:
	movq	-128(%rbp), %rdi
	cmpq	$2048, %rdi
	ja	.L704
	testb	%bl, %bl
	je	.L704
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L846
	leaq	40(%r11), %rdi
	movq	%r13, %rax
	leaq	(%rbx,%r13), %rsi
	movabsq	$-4819355556693147648, %rcx
	.p2align 4,,10
	.p2align 3
.L705:
	movq	(%rax), %rdx
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$48, %rdx
	movq	$0, (%rdi,%rdx,4)
	cmpq	%rax, %rsi
	jne	.L705
	jmp	.L1087
.L700:
	movq	-128(%rbp), %rdi
	cmpq	$2048, %rdi
	ja	.L701
	testb	%bl, %bl
	je	.L701
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L846
	leaq	40(%r11), %rdi
	movq	%r13, %rax
	leaq	0(%r13,%rbx), %rsi
	movabsq	$-4819355556693147648, %rcx
	.p2align 4,,10
	.p2align 3
.L703:
	movq	(%rax), %rdx
	addq	$1, %rax
	imulq	%rcx, %rdx
	shrq	$48, %rdx
	movl	$0, (%rdi,%rdx,4)
	cmpq	%rsi, %rax
	jne	.L703
	jmp	.L1087
.L694:
	movq	-128(%rbp), %rdx
	movq	%r11, %rdi
	movq	%r13, %rcx
	movq	%r11, -136(%rbp)
	call	PrepareH35
	movq	152(%r15), %rdx
	movq	-136(%rbp), %r11
	movl	(%rdx), %eax
	jmp	.L686
.L692:
	movq	-128(%rbp), %rax
	cmpq	$512, %rax
	ja	.L717
	testb	%bl, %bl
	je	.L717
	movq	%r13, %rdx
	leaq	(%rax,%r13), %rcx
	testq	%rax, %rax
	je	.L719
	.p2align 4,,10
	.p2align 3
.L720:
	imull	$506832829, (%rdx), %eax
	movl	$-13108, %esi
	addq	$1, %rdx
	shrl	$17, %eax
	movl	$-858993460, 40(%r11,%rax,4)
	movw	%si, 131112(%r11,%rax,2)
	cmpq	%rcx, %rdx
	jne	.L720
.L719:
	leaq	196648(%r11), %rdi
	movl	$65536, %edx
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	movq	%r11, -136(%rbp)
	call	memset@PLT
	movq	-136(%rbp), %r11
	movw	%bx, 524328(%r11)
	movq	152(%r15), %rdx
	movl	(%rdx), %eax
	jmp	.L686
.L693:
	movq	-128(%rbp), %rax
	cmpq	$512, %rax
	ja	.L717
	testb	%bl, %bl
	je	.L717
	movq	%r13, %rdx
	leaq	(%rax,%r13), %rcx
	testq	%rax, %rax
	je	.L719
	.p2align 4,,10
	.p2align 3
.L715:
	imull	$506832829, (%rdx), %eax
	movl	$-13108, %edi
	addq	$1, %rdx
	shrl	$17, %eax
	movl	$-858993460, 40(%r11,%rax,4)
	movw	%di, 131112(%r11,%rax,2)
	cmpq	%rdx, %rcx
	jne	.L715
	jmp	.L719
.L691:
	movq	-128(%rbp), %rax
	cmpq	$512, %rax
	ja	.L722
	testb	%bl, %bl
	je	.L722
	movq	%r13, %rdx
	leaq	(%rax,%r13), %rcx
	testq	%rax, %rax
	je	.L724
	.p2align 4,,10
	.p2align 3
.L725:
	imull	$506832829, (%rdx), %eax
	movl	$-13108, %r10d
	addq	$1, %rdx
	shrl	$17, %eax
	movl	$-858993460, 40(%r11,%rax,4)
	movw	%r10w, 131112(%r11,%rax,2)
	cmpq	%rdx, %rcx
	jne	.L725
.L724:
	leaq	196648(%r11), %rdi
	movl	$65536, %edx
	xorl	%esi, %esi
	movq	%r11, -136(%rbp)
	call	memset@PLT
	movq	-136(%rbp), %r11
	leaq	1310768(%r11), %rdi
	movl	%r11d, %eax
	movq	$0, 1310760(%r11)
	movq	$0, 1311776(%r11)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	1311784(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	152(%r15), %rdx
	movl	(%rdx), %eax
	jmp	.L686
.L696:
	movq	40(%r11), %rdx
	movq	-128(%rbp), %rsi
	leaq	80(%r11), %rdi
	movq	%rdx, %rcx
	shrq	$6, %rcx
	cmpq	%rsi, %rcx
	jb	.L710
	testb	%bl, %bl
	je	.L710
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L846
	movl	56(%r11), %ecx
	movq	64(%r11), %r10
	movq	%r13, %rsi
	leaq	(%rbx,%r13), %r9
	movabsq	$2297779722762296275, %r8
	.p2align 4,,10
	.p2align 3
.L711:
	movq	(%rsi), %rdx
	xorl	%ebx, %ebx
	addq	$1, %rsi
	andq	%r10, %rdx
	imulq	%r8, %rdx
	shrq	%cl, %rdx
	movl	%edx, %edx
	movw	%bx, (%rdi,%rdx,2)
	cmpq	%r9, %rsi
	jne	.L711
.L846:
	movq	%r11, %rdx
	jmp	.L702
.L695:
	movd	524336(%r11), %xmm3
	leaq	48(%r11), %rdx
	leaq	524336(%r11), %rcx
	pshufd	$0, %xmm3, %xmm0
	.p2align 4,,10
	.p2align 3
.L729:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L729
	movq	%r11, %rdx
	testl	%r14d, %r14d
	jne	.L730
	pxor	%xmm0, %xmm0
	movl	$1, 20(%r11)
	movups	%xmm0, 24(%r11)
	jmp	.L732
.L697:
	movq	40(%r11), %rdx
	movq	-128(%rbp), %rsi
	leaq	64(%r11), %rdi
	movq	%rdx, %rcx
	shrq	$6, %rcx
	cmpq	%rsi, %rcx
	jb	.L710
	testb	%bl, %bl
	je	.L710
	testq	%rsi, %rsi
	je	.L846
	movl	56(%r11), %ecx
	movq	%r13, %rdx
	leaq	(%rsi,%r13), %r8
	.p2align 4,,10
	.p2align 3
.L709:
	imull	$506832829, (%rdx), %esi
	xorl	%r9d, %r9d
	addq	$1, %rdx
	shrl	%cl, %esi
	movw	%r9w, (%rdi,%rsi,2)
	cmpq	%r8, %rdx
	jne	.L709
	jmp	.L846
.L741:
	movq	-128(%rbp), %rbx
	cmpq	$2, %rbx
	jbe	.L732
	cmpl	$127, %r14d
	jbe	.L732
	movq	-176(%rbp), %rsi
	leaq	-127(%rsi), %rdi
	movq	%rdi, %rax
	movq	%rdi, -160(%rbp)
	addq	%rbx, %rax
	cmpq	%rsi, %rax
	cmova	%rsi, %rax
	movq	%rax, -240(%rbp)
	cmpq	%rax, %rdi
	jnb	.L732
	movq	40(%r11), %rax
	movl	%r14d, -192(%rbp)
	leaq	524344(%r11), %rbx
	movq	%r11, -232(%rbp)
	movq	%rax, -184(%rbp)
	movl	%r12d, -252(%rbp)
	movq	%r15, -248(%rbp)
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L776:
	movq	-160(%rbp), %rdi
	movl	$15, %esi
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rdx
	movq	-112(%rbp), %r10
	subq	%rdi, %rax
	cmpq	$15, %rax
	movq	%rdx, %rcx
	cmovb	%rsi, %rax
	andq	%rdi, %r10
	movq	-232(%rbp), %rsi
	andq	%rdi, %rdx
	leaq	(%rdx,%rdx), %r9
	movq	%r10, -200(%rbp)
	subq	%rax, %rcx
	imull	$506832829, (%r15,%r10), %eax
	movq	%r9, -136(%rbp)
	addq	$1, %r9
	movq	%rcx, -208(%rbp)
	movq	%r9, -144(%rbp)
	shrl	$15, %eax
	leaq	(%rsi,%rax,4), %rax
	movl	48(%rax), %r8d
	movl	%edi, 48(%rax)
	movq	%rdi, %rax
	subq	%r8, %rax
	movq	%r8, %rsi
	andl	-72(%rbp), %esi
	cmpq	%rax, %rcx
	jb	.L868
	movl	$64, %r12d
	leaq	(%r15,%r10), %rdi
	xorl	%r14d, %r14d
	movq	$0, -152(%rbp)
	movq	%rdi, -216(%rbp)
	movq	%r12, %r13
	testq	%rax, %rax
	je	.L868
	.p2align 4,,10
	.p2align 3
.L765:
	movq	-152(%rbp), %rax
	movl	$128, %r11d
	cmpq	%r14, %rax
	movq	%rax, %rcx
	movq	-200(%rbp), %rax
	cmova	%r14, %rcx
	subq	%rcx, %r11
	leaq	(%rax,%rcx), %r9
	leaq	(%rcx,%rsi), %rdi
	movq	%r11, %rax
	addq	%r15, %rdi
	addq	%r15, %r9
	shrq	$3, %rax
	je	.L768
	leaq	8(%rdi), %rdx
	xorl	%r10d, %r10d
	movq	%rdx, -224(%rbp)
.L770:
	movq	(%rdi,%r10), %rdx
	movq	(%r9,%r10), %r12
	cmpq	%r12, %rdx
	je	.L1119
	xorq	%r12, %rdx
	xorl	%eax, %eax
	rep bsfq	%rdx, %rax
	cltq
	shrq	$3, %rax
	addq	%r10, %rax
.L771:
	movq	-136(%rbp), %rdi
	addq	%rcx, %rax
	leaq	(%rbx,%rdi,4), %rcx
	movq	-144(%rbp), %rdi
	leaq	(%rbx,%rdi,4), %rdx
	movq	-184(%rbp), %rdi
	andq	%r8, %rdi
	leaq	(%rdi,%rdi), %r9
	cmpq	$127, %rax
	ja	.L1120
	movq	-216(%rbp), %r11
	leaq	(%r15,%rax), %r10
	movzbl	(%r10,%rsi), %esi
	cmpb	%sil, (%r11,%rax)
	jbe	.L773
	leaq	1(%r9), %rdi
	movl	%r8d, (%rcx)
	subq	$1, %r13
	leaq	(%rbx,%rdi,4), %rcx
	movq	%rdi, -136(%rbp)
	movq	-160(%rbp), %rdi
	movl	(%rcx), %r8d
	subq	%r8, %rdi
	movq	%r8, %rsi
	andl	-72(%rbp), %esi
	cmpq	%rdi, -208(%rbp)
	setb	%r9b
	testq	%rdi, %rdi
	sete	%dil
	orb	%dil, %r9b
	jne	.L774
	testq	%r13, %r13
	je	.L774
	movq	%rax, %r14
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L773:
	movl	%r8d, (%rdx)
	leaq	(%rbx,%rdi,8), %rdx
	movq	-160(%rbp), %rdi
	subq	$1, %r13
	movl	(%rdx), %r8d
	movq	%r8, %rsi
	andl	-72(%rbp), %esi
	subq	%r8, %rdi
	sete	%r10b
	cmpq	%rdi, -208(%rbp)
	setb	%dil
	orb	%dil, %r10b
	jne	.L774
	testq	%r13, %r13
	je	.L774
	movq	%rax, -152(%rbp)
	movq	%r9, -144(%rbp)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	-224(%rbp), %rdx
	addq	%r10, %rdx
	addq	$8, %r10
	subq	$1, %rax
	jne	.L770
	movq	%r10, %rax
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L768:
	andl	$7, %r11d
	je	.L771
	movzbl	(%rdi), %edx
	cmpb	%dl, (%r9,%rax)
	jne	.L771
	leaq	1(%rax), %rdx
	cmpq	$1, %r11
	je	.L860
	movzbl	1(%r9,%rax), %r10d
	cmpb	%r10b, 1(%rdi)
	jne	.L860
	leaq	2(%rax), %rdx
	cmpq	$2, %r11
	je	.L860
	movzbl	2(%r9,%rax), %r10d
	cmpb	%r10b, 2(%rdi)
	jne	.L860
	leaq	3(%rax), %rdx
	cmpq	$3, %r11
	je	.L860
	movzbl	3(%r9,%rax), %r10d
	cmpb	%r10b, 3(%rdi)
	jne	.L860
	leaq	4(%rax), %rdx
	cmpq	$4, %r11
	je	.L860
	movzbl	4(%r9,%rax), %r10d
	cmpb	%r10b, 4(%rdi)
	jne	.L860
	leaq	5(%rax), %rdx
	subq	$5, %r11
	je	.L860
	movzbl	5(%r9,%rax), %r10d
	cmpb	%r10b, 5(%rdi)
	jne	.L860
	leaq	6(%rax), %rdx
	cmpq	$1, %r11
	je	.L860
	movzbl	6(%r9,%rax), %r9d
	cmpb	%r9b, 6(%rdi)
	jne	.L860
	addq	$7, %rax
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L1113:
	leal	0(,%rdi,8), %ecx
	andl	$56, %ecx
	orb	$-126, %cl
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L1107:
	leaq	-16(%rax,%rsi), %r8
	movslq	264(%r15), %rbx
	movl	60(%r15), %ecx
	movzwl	14(%r8), %edi
	movq	%rbx, -144(%rbp)
	leal	16(%rcx), %r9d
	movl	%edi, %ebx
	movl	%edi, %r10d
	andw	$1023, %bx
	andl	$1023, %r10d
	movw	%bx, -152(%rbp)
	cmpl	%r9d, %r10d
	jb	.L781
	subl	%ecx, %r10d
	movzbl	56(%r15), %ecx
	shrw	$10, %di
	leal	-16(%r10), %ebx
	movl	%ebx, %r10d
	shrl	%cl, %r10d
	movl	%r10d, %ecx
	andl	$1, %ecx
	leal	2(%rcx), %r10d
	movl	%edi, %ecx
	movl	8(%r8), %edi
	sall	%cl, %r10d
	movzbl	56(%r15), %ecx
	leal	-4(%r10,%rdi), %edi
	sall	%cl, %edi
	movl	%edi, %r10d
	movl	$-1, %edi
	sall	%cl, %edi
	notl	%edi
	andl	%edi, %ebx
	addl	%ebx, %r9d
	addl	%r9d, %r10d
.L781:
	cmpl	$15, %r10d
	jbe	.L782
	leal	-15(%r10), %ecx
	cmpq	%rcx, -144(%rbp)
	jne	.L780
.L782:
	movl	8(%r15), %ecx
	movl	4(%r8), %edi
	movl	$1, %r9d
	movq	256(%r15), %rbx
	movq	-144(%rbp), %r10
	salq	%cl, %r9
	leaq	-16(%r9), %rcx
	movl	%edi, %r9d
	andl	$33554431, %r9d
	subq	%r9, %rbx
	cmpq	%rbx, %rcx
	cmova	%rbx, %rcx
	cmpq	%r10, %rcx
	jb	.L783
	movl	-80(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L862
	movl	172(%r15), %ecx
	movq	%r13, -128(%rbp)
	movl	%edx, -144(%rbp)
	movq	200(%r15), %r9
	movq	%rcx, -80(%rbp)
	movq	%rcx, %rbx
	movq	-80(%rbp), %r13
	movq	%r11, -160(%rbp)
	movl	-188(%rbp), %ecx
	movq	%r10, %r11
	movq	%rax, -80(%rbp)
	movq	-176(%rbp), %rdx
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L785:
	addl	$1, %edi
	addl	$1, %r14d
	movl	%edi, 4(%r8)
	subl	$1, %ecx
	je	.L1075
	movl	%r14d, %edx
.L784:
	movq	%rdx, %rax
	movl	%r14d, %r10d
	subq	%r11, %rax
	andl	%ebx, %r10d
	andq	%r13, %rax
	movzbl	(%r9,%rax), %eax
	cmpb	%al, (%r9,%r10)
	je	.L785
	movl	%ecx, %ebx
	movq	-128(%rbp), %r13
	movq	-80(%rbp), %rax
	movq	%rdx, -168(%rbp)
	movq	%rbx, -128(%rbp)
	movl	-144(%rbp), %edx
	movq	-160(%rbp), %r11
.L783:
	cmpw	$0, -152(%rbp)
	movl	%edi, %ecx
	sete	%bl
	andl	$33554431, %ecx
	shrl	$25, %edi
	leal	(%rcx,%rdi), %r14d
	movl	(%r8), %edi
	movq	%rdi, %rcx
	cmpl	$5, %edi
	jbe	.L1121
	cmpq	$129, %rdi
	jbe	.L1122
	cmpq	$2113, %rdi
	jbe	.L1123
	cmpq	$6209, %rdi
	jbe	.L863
	cmpq	$22594, %rdi
	sbbl	%r9d, %r9d
	andl	$-8, %r9d
	addl	$56, %r9d
	cmpq	$22594, %rdi
	sbbl	%edi, %edi
	addl	$23, %edi
.L787:
	leal	-2(%r14), %ecx
	cmpl	$9, %r14d
	jle	.L791
	movslq	%r14d, %r10
	cmpl	$133, %r14d
	jle	.L1124
	cmpq	$2117, %r10
	ja	.L793
	leaq	-70(%r10), %rcx
	bsrl	%ecx, %ecx
	addl	$12, %ecx
.L791:
	movl	%ecx, %r10d
	andl	$7, %r10d
	orl	%r10d, %r9d
	cmpw	$7, %di
	ja	.L794
	testb	%bl, %bl
	je	.L794
	cmpw	$15, %cx
	ja	.L794
	movl	%r9d, %edi
	orl	$64, %edi
	cmpw	$7, %cx
	cmova	%edi, %r9d
.L797:
	movw	%r9w, 12(%r8)
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L1102:
	salq	$4, %rsi
	leaq	128(%r15), %rdi
	call	BrotliAllocate
	movq	%rax, %rbx
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	-64(%rbp), %rax
	pushq	%r11
	movq	-120(%rbp), %rdi
	movl	%r12d, %ecx
	pushq	%rax
	movq	5392(%r15), %r9
	movq	%r10, %rdx
	pushq	%rbx
	pushq	%r8
	movq	5384(%r15), %r8
	movq	%r11, -72(%rbp)
	call	BrotliCompressFragmentTwoPass
	movq	-72(%rbp), %r11
	addq	$32, %rsp
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L1114:
	leaq	-2(%rdi), %rsi
	bsrl	%esi, %ecx
	subl	$1, %ecx
	shrq	%cl, %rsi
	leal	2(%rsi,%rcx,2), %esi
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L860:
	movq	%rdx, %rax
	jmp	.L771
.L868:
	movq	-144(%rbp), %rax
	leaq	(%rbx,%rdx,8), %rcx
	leaq	(%rbx,%rax,4), %rdx
	.p2align 4,,10
	.p2align 3
.L774:
	movq	-232(%rbp), %rax
	movl	524336(%rax), %eax
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
.L767:
	addq	$1, -160(%rbp)
	movq	-160(%rbp), %rax
	cmpq	%rax, -240(%rbp)
	jne	.L776
	movq	%r15, %r13
	movl	-192(%rbp), %r14d
	movq	-232(%rbp), %r11
	movq	-248(%rbp), %r15
	movl	-252(%rbp), %r12d
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L1120:
	movl	(%rbx,%rdi,8), %eax
	movl	%eax, (%rcx)
	movl	4(%rbx,%r9,4), %eax
	movl	%eax, (%rdx)
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L794:
	shrw	$3, %cx
	movzwl	%cx, %ecx
.L796:
	shrw	$3, %di
	movl	$5377344, %r10d
	movzwl	%di, %edi
	leal	(%rdi,%rdi,2), %edi
	addl	%ecx, %edi
	leal	(%rdi,%rdi), %ecx
	sall	$6, %edi
	shrl	%cl, %r10d
	andl	$192, %r10d
	leal	64(%r10,%rdi), %ecx
	orl	%ecx, %r9d
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L758:
	movq	-176(%rbp), %rbx
	movl	$4, %edx
	movq	-112(%rbp), %rcx
	subq	%r9, %rdx
	leaq	(%rbx,%rdx), %r8
	andq	%r8, %rcx
	cmpq	-128(%rbp), %rdx
	ja	.L759
	movq	-112(%rbp), %rdx
	subq	%rcx, %rdx
.L826:
	movq	-128(%rbp), %rbx
	leaq	-4(%rbx,%r9), %rsi
	cmpq	%rdx, %rsi
	cmovbe	%rsi, %rdx
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L1111:
	cmpq	$1048575, 16(%r15)
	jbe	.L645
	movl	$54, 32(%r15)
	movl	$4194360, %esi
	cmpl	$24, %ecx
	jle	.L647
	movl	$55, 32(%r15)
	movl	$71303368, %esi
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L631:
	leaq	0(,%rdx,4), %rbx
	salq	$4, %rdx
	jmp	.L630
.L1121:
	leal	0(,%rdi,8), %r9d
	andl	$56, %r9d
	jmp	.L787
.L1118:
	movq	%rbx, 4472(%r15)
	movq	-120(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%rdx, -80(%rbp)
	call	BrotliFree
	movq	-80(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movq	$0, 4464(%r15)
	movq	%rdx, %rsi
	call	BrotliAllocate
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movq	%rax, 4464(%r15)
	movq	-80(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L633
.L1112:
	movl	%eax, 32(%r15)
	cmpl	$24, %ecx
	jle	.L650
	cmpl	$3, %eax
	jne	.L662
	movl	$35, 32(%r15)
	movl	$67371200, %esi
	jmp	.L647
.L662:
	cmpl	$6, %eax
	je	.L657
.L650:
	cmpl	$6, %eax
	ja	.L664
.L930:
	leaq	.L665(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L665:
	.long	.L664-.L665
	.long	.L664-.L665
	.long	.L666-.L665
	.long	.L836-.L665
	.long	.L654-.L665
	.long	.L661-.L665
	.long	.L658-.L665
	.text
	.p2align 4,,10
	.p2align 3
.L664:
	movl	$40, %esi
	jmp	.L647
.L836:
	movl	$262192, %esi
	jmp	.L647
.L666:
	movl	$262188, %esi
	leaq	128(%r15), %rdi
	call	BrotliAllocate
	movl	48(%r15), %edx
	movdqu	32(%r15), %xmm4
	movq	%rax, 152(%r15)
	movq	%rax, %r11
	movl	32(%r15), %eax
	movl	%edx, 16(%r11)
	leal	-5(%rax), %edx
	movups	%xmm4, (%r11)
	cmpl	$60, %edx
	ja	.L668
	leaq	.L670(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L670:
	.long	.L678-.L670
	.long	.L677-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L676-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L669-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L674-.L670
	.long	.L673-.L670
	.long	.L672-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L669-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L668-.L670
	.long	.L669-.L670
	.text
	.p2align 4,,10
	.p2align 3
.L651:
	cmpq	$1048575, 16(%r15)
	leal	-1(%rax), %edx
	jbe	.L655
	cmpl	$18, %ecx
	jg	.L1125
.L655:
	movl	$5, 32(%r15)
	movl	%edx, 40(%r15)
	cmpl	$6, %eax
	jg	.L1126
	movl	$14, 36(%r15)
	movl	$4, %eax
.L827:
	movl	%eax, 48(%r15)
.L661:
	movl	40(%r15), %ecx
	movl	$4, %esi
	salq	%cl, %rsi
	movl	36(%r15), %ecx
	addq	$2, %rsi
	salq	%cl, %rsi
	addq	$64, %rsi
	jmp	.L663
.L645:
	movl	$4, 32(%r15)
	cmpl	$24, %ecx
	jle	.L654
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L1075:
	movl	%r14d, %ebx
	movq	-128(%rbp), %r13
	movq	-80(%rbp), %rax
	movq	$0, -128(%rbp)
	movq	%rbx, -168(%rbp)
	movl	-144(%rbp), %edx
	movq	-160(%rbp), %r11
	jmp	.L783
.L1122:
	subl	$2, %ecx
	subq	$2, %rdi
	bsrl	%ecx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdi
	leal	2(%rdi,%rcx,2), %edi
	leal	0(,%rdi,8), %r9d
	andl	$56, %r9d
	jmp	.L787
.L1124:
	subq	$6, %r10
	bsrl	%r10d, %ecx
	subl	$1, %ecx
	shrq	%cl, %r10
	leal	4(%r10,%rcx,2), %ecx
	jmp	.L791
.L717:
	leaq	40(%r11), %rdi
	movl	$131072, %edx
	movl	$204, %esi
	movq	%r11, -136(%rbp)
	call	memset@PLT
	movq	-136(%rbp), %r11
	movl	$65536, %edx
	xorl	%esi, %esi
	leaq	131112(%r11), %rdi
	call	memset@PLT
	movq	-136(%rbp), %r11
	jmp	.L719
.L710:
	movq	%r11, -136(%rbp)
	addq	%rdx, %rdx
.L1086:
	xorl	%esi, %esi
	call	memset@PLT
	movq	152(%r15), %rdx
	movq	-136(%rbp), %r11
	movl	(%rdx), %eax
	jmp	.L686
.L653:
	movl	$42, 32(%r15)
	movl	$1311792, %esi
	jmp	.L647
.L658:
	movl	40(%r15), %ecx
	movl	$4, %esi
	salq	%cl, %rsi
	movl	36(%r15), %ecx
	addq	$2, %rsi
	salq	%cl, %rsi
	addq	$80, %rsi
	jmp	.L663
.L756:
	movq	-112(%rbp), %rbx
	leaq	4(%r8), %r8
	subq	%r9, %r8
	movq	%rbx, %rcx
	andq	%r8, %rcx
	subq	%rcx, %rbx
	movq	%rbx, %rdx
	jmp	.L826
.L1105:
	movq	-112(%rbp), %r10
	movq	40(%r11), %r8
	movabsq	$3866266742567714048, %rcx
	movq	-176(%rbp), %rdi
	movq	%r8, -136(%rbp)
	leaq	-3(%rdi), %rsi
	movq	%r10, %rdi
	andq	%rsi, %rdi
	shrq	$3, %rsi
	movq	0(%r13,%rdi), %r8
	andl	$3, %esi
	imulq	%rcx, %r8
	movq	%r8, %rdi
	movq	-136(%rbp), %r8
	shrq	$44, %rdi
	addq	%rdi, %rsi
	leal	-3(%r14), %edi
	movl	%edi, 40(%r8,%rsi,4)
	movq	-176(%rbp), %rdi
	leaq	-2(%rdi), %rsi
	movq	%r10, %rdi
	andq	%rsi, %rdi
	shrq	$3, %rsi
	movq	0(%r13,%rdi), %r8
	andl	$3, %esi
	imulq	%rcx, %r8
	movq	%r8, %rdi
	movq	-136(%rbp), %r8
	shrq	$44, %rdi
	addq	%rdi, %rsi
	leal	-2(%r14), %edi
	movl	%edi, 40(%r8,%rsi,4)
	movq	-176(%rbp), %r8
	movq	%r10, %rdi
	leaq	-1(%r8), %rsi
	andq	%rsi, %rdi
	shrq	$3, %rsi
	imulq	0(%r13,%rdi), %rcx
	andl	$3, %esi
	shrq	$44, %rcx
	jmp	.L1098
.L793:
	orl	$7, %r9d
	movl	$2, %ecx
	jmp	.L796
.L1123:
	leal	-66(%rdi), %edi
	bsrl	%edi, %edi
	addl	$10, %edi
	leal	0(,%rdi,8), %r9d
	andl	$56, %r9d
	jmp	.L787
.L652:
	movl	$40, 32(%r15)
	jmp	.L654
.L706:
	movq	%r11, -136(%rbp)
	leaq	40(%r11), %rdi
	movl	$524304, %edx
	jmp	.L1086
.L722:
	leaq	40(%r11), %rdi
	movl	$131072, %edx
	movl	$204, %esi
	movq	%r11, -136(%rbp)
	call	memset@PLT
	movq	-136(%rbp), %r11
	movl	$65536, %edx
	xorl	%esi, %esi
	leaq	131112(%r11), %rdi
	call	memset@PLT
	movq	-136(%rbp), %r11
	jmp	.L724
.L701:
	movq	%r11, -136(%rbp)
	leaq	40(%r11), %rdi
	movl	$262148, %edx
	jmp	.L1086
.L727:
	movq	%r11, -136(%rbp)
	leaq	40(%r11), %rdi
	movl	$4194320, %edx
	jmp	.L1086
.L1126:
	cmpl	$9, %eax
	movl	$16, %edx
	movl	$10, %eax
	movl	$15, 36(%r15)
	cmove	%edx, %eax
	jmp	.L827
.L704:
	movq	%r11, -136(%rbp)
	leaq	40(%r11), %rdi
	movl	$262152, %edx
	jmp	.L1086
.L1125:
	movabsq	$64424509446, %rdi
	movl	%edx, 40(%r15)
	movl	$4, %edx
	movq	%rdi, 32(%r15)
	movl	$5, 44(%r15)
	cmpl	$6, %eax
	jle	.L656
	cmpl	$9, %eax
	movl	$10, %edx
	movl	$16, %eax
	cmove	%eax, %edx
.L656:
	movl	%edx, 48(%r15)
	cmpl	$24, %ecx
	jle	.L658
.L657:
	movl	40(%r15), %ecx
	movl	$4, %esi
	movl	$65, 32(%r15)
	salq	%cl, %rsi
	movl	36(%r15), %ecx
	addq	$2, %rsi
	salq	%cl, %rsi
	addq	$67109088, %rsi
	jmp	.L663
.L863:
	movl	$40, %r9d
	movl	$21, %edi
	jmp	.L787
.L862:
	movq	$0, -128(%rbp)
	jmp	.L783
.L1104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	EncodeData.cold, @function
EncodeData.cold:
.LFSB337:
.L1072:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movdqu	32(%r15), %xmm5
	movq	$0, 152(%r15)
	movaps	%xmm5, 0
	movl	48(%r15), %eax
	movl	%eax, 16
	ud2
	.cfi_endproc
.LFE337:
	.text
	.size	EncodeData, .-EncodeData
	.section	.text.unlikely
	.size	EncodeData.cold, .-EncodeData.cold
.LCOLDE12:
	.text
.LHOTE12:
	.p2align 4
	.globl	BrotliEncoderSetParameter
	.type	BrotliEncoderSetParameter, @function
BrotliEncoderSetParameter:
.LFB314:
	.cfi_startproc
	endbr64
	movl	5452(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L1139
	cmpl	$8, %esi
	ja	.L1127
	leaq	.L1130(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1130:
	.long	.L1138-.L1130
	.long	.L1137-.L1130
	.long	.L1136-.L1130
	.long	.L1135-.L1130
	.long	.L1134-.L1130
	.long	.L1133-.L1130
	.long	.L1132-.L1130
	.long	.L1131-.L1130
	.long	.L1129-.L1130
	.text
	.p2align 4,,10
	.p2align 3
.L1139:
	xorl	%r8d, %r8d
.L1127:
	movl	%r8d, %eax
	ret
.L1131:
	movl	%edx, 56(%rdi)
	movl	$1, %r8d
	jmp	.L1127
.L1129:
	movl	%edx, 60(%rdi)
	movl	$1, %r8d
	jmp	.L1127
.L1138:
	movl	%edx, (%rdi)
	movl	$1, %r8d
	jmp	.L1127
.L1137:
	movl	%edx, 4(%rdi)
	movl	$1, %r8d
	jmp	.L1127
.L1136:
	movl	%edx, 8(%rdi)
	movl	$1, %r8d
	jmp	.L1127
.L1135:
	movl	%edx, 12(%rdi)
	movl	$1, %r8d
	jmp	.L1127
.L1134:
	cmpl	$1, %edx
	ja	.L1127
	movl	%edx, 24(%rdi)
	movl	$1, %r8d
	jmp	.L1127
.L1133:
	movl	%edx, %eax
	movl	$1, %r8d
	movq	%rax, 16(%rdi)
	jmp	.L1127
.L1132:
	xorl	%eax, %eax
	testl	%edx, %edx
	movl	$1, %r8d
	setne	%al
	movl	%eax, 28(%rdi)
	jmp	.L1127
	.cfi_endproc
.LFE314:
	.size	BrotliEncoderSetParameter, .-BrotliEncoderSetParameter
	.p2align 4
	.globl	BrotliEncoderCreateInstance
	.type	BrotliEncoderCreateInstance, @function
BrotliEncoderCreateInstance:
.LFB331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	orq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	je	.L1155
	testq	%rdi, %rdi
	je	.L1144
	testq	%rsi, %rsi
	je	.L1144
	movl	$5456, %esi
	movq	%rdx, %rdi
	call	*%r13
	movq	%rax, %r12
.L1142:
	testq	%r12, %r12
	je	.L1140
	leaq	128(%r12), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	BrotliInitMemoryManager
	leaq	80(%r12), %rdi
	movabsq	$47244640256, %rax
	movq	$22, 8(%r12)
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	call	BrotliInitEncoderDictionary
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	$0, 56(%r12)
	movups	%xmm0, 5384(%r12)
	movups	%xmm0, 5408(%r12)
	movups	%xmm0, 192(%r12)
	movups	%xmm0, 224(%r12)
	movups	%xmm0, 240(%r12)
	movdqa	.LC9(%rip), %xmm0
	movl	$64, 64(%r12)
	movq	$67108860, 72(%r12)
	movq	$0, 160(%r12)
	movw	%ax, 347(%r12)
	movq	$0, 352(%r12)
	movq	$0, 360(%r12)
	movq	$0, 152(%r12)
	movq	$0, 4464(%r12)
	movq	$0, 4472(%r12)
	movq	$0, 5376(%r12)
	movq	$0, 5400(%r12)
	movl	$0, 5444(%r12)
	movq	$0, 5448(%r12)
	movq	$0, 184(%r12)
	movq	$0, 208(%r12)
	movq	$0, 216(%r12)
	movq	$0, 256(%r12)
	movups	%xmm0, 264(%r12)
	movups	%xmm0, 328(%r12)
.L1140:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1155:
	.cfi_restore_state
	movl	$5456, %edi
	call	malloc@PLT
	movq	%rax, %r12
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1144:
	xorl	%r12d, %r12d
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE331:
	.size	BrotliEncoderCreateInstance, .-BrotliEncoderCreateInstance
	.p2align 4
	.globl	BrotliEncoderDestroyInstance
	.type	BrotliEncoderDestroyInstance, @function
BrotliEncoderDestroyInstance:
.LFB333:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1156
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	128(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	360(%rdi), %rsi
	movq	136(%rdi), %rbx
	movq	144(%rdi), %r14
	movq	%r13, %rdi
	call	BrotliFree
	movq	216(%r12), %rsi
	movq	%r13, %rdi
	movq	$0, 360(%r12)
	call	BrotliFree
	movq	192(%r12), %rsi
	movq	%r13, %rdi
	movq	$0, 216(%r12)
	call	BrotliFree
	movq	152(%r12), %rsi
	movq	$0, 192(%r12)
	testq	%rsi, %rsi
	je	.L1158
	movq	%r13, %rdi
	call	BrotliFree
	movq	$0, 152(%r12)
.L1158:
	movq	4464(%r12), %rsi
	movq	%r13, %rdi
	call	BrotliFree
	movq	5384(%r12), %rsi
	movq	%r13, %rdi
	movq	$0, 4464(%r12)
	call	BrotliFree
	movq	5392(%r12), %rsi
	movq	%r13, %rdi
	movq	$0, 5384(%r12)
	call	BrotliFree
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rbx, %rax
	movq	$0, 5392(%r12)
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1156:
	ret
	.cfi_endproc
.LFE333:
	.size	BrotliEncoderDestroyInstance, .-BrotliEncoderDestroyInstance
	.p2align 4
	.globl	BrotliEncoderMaxCompressedSize
	.type	BrotliEncoderMaxCompressedSize, @function
BrotliEncoderMaxCompressedSize:
.LFB340:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	shrq	$14, %rax
	leaq	6(%rdi,%rax,4), %rax
	testq	%rdi, %rdi
	je	.L1167
	cmpq	%rax, %rdi
	movl	$0, %edx
	cmova	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1167:
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE340:
	.size	BrotliEncoderMaxCompressedSize, .-BrotliEncoderMaxCompressedSize
	.p2align 4
	.globl	BrotliEncoderCompressStream
	.type	BrotliEncoderCompressStream, @function
BrotliEncoderCompressStream:
.LFB349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movl	%esi, -72(%rbp)
	movq	%rcx, -88(%rbp)
	movl	5452(%rdi), %r9d
	movq	%rax, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jne	.L1169
	movl	4(%rdi), %edx
	xorl	%r8d, %r8d
	movl	$11, %ecx
	movb	$0, 346(%rdi)
	movw	%r8w, 344(%rdi)
	movl	$-1, 5440(%rdi)
	testl	%edx, %edx
	cmovns	%edx, %eax
	cmpl	$11, %eax
	cmovg	%ecx, %eax
	movl	%eax, 4(%rdi)
	movl	8(%rdi), %eax
	cmpl	$2, %edx
	jg	.L1170
	movl	$0, 28(%rdi)
	cmpl	$9, %eax
	jg	.L1175
.L1171:
	movl	$10, 8(%rbx)
	movl	$10, %eax
.L1173:
	cmpl	$1, %edx
	jle	.L1377
.L1176:
	movl	12(%rbx), %ecx
	cmpl	$3, %edx
	jle	.L1178
	testl	%ecx, %ecx
	jne	.L1179
	movl	$16, %ecx
	cmpl	$8, %edx
	jle	.L1180
	cmpl	$18, %eax
	movl	$18, %ecx
	cmovle	%eax, %ecx
	movl	$16, %eax
	cmpl	$16, %ecx
	cmovl	%eax, %ecx
.L1180:
	cmpl	$2, (%rbx)
	movl	%ecx, 12(%rbx)
	je	.L1378
.L1367:
	movl	56(%rbx), %esi
	movl	60(%rbx), %eax
	cmpl	$3, %esi
	ja	.L1372
	cmpl	$120, %eax
	jbe	.L1379
.L1372:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1377:
	movl	%eax, 12(%rbx)
	xorl	%edx, %edx
	xorl	%esi, %esi
.L1177:
	movq	%rbx, %rdi
	call	BrotliInitDistanceParams
	movl	12(%rbx), %edi
	movl	8(%rbx), %eax
	movl	$1, %esi
	movl	%esi, %edx
	cmpl	%eax, %edi
	movl	%eax, %ecx
	cmovge	%edi, %ecx
	addl	$1, %ecx
	sall	%cl, %edx
	leal	-1(%rdx), %ecx
	movl	%edx, 168(%rbx)
	movl	%ecx, 172(%rbx)
	movl	%edi, %ecx
	sall	%cl, %esi
	addl	%esi, %edx
	movl	%esi, 176(%rbx)
	movl	%edx, 180(%rbx)
	movl	4(%rbx), %edx
	cmpl	$1, %edx
	jbe	.L1380
	movl	28(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L1184
.L1182:
	sall	$8, %eax
	movb	$14, 346(%rbx)
	andw	$16128, %ax
	orl	$17, %eax
	movw	%ax, 344(%rbx)
.L1185:
	testl	%edx, %edx
	jne	.L1187
	movdqa	.LC13(%rip), %xmm0
	movdqa	kDefaultCommandBits.6826(%rip), %xmm1
	movq	$5, 4544(%rbx)
	movabsq	$357950746977689926, %rax
	movdqa	32+kDefaultCommandBits.6826(%rip), %xmm3
	movdqa	48+kDefaultCommandBits.6826(%rip), %xmm4
	movq	$0, 4552(%rbx)
	movups	%xmm0, 4480(%rbx)
	movdqa	.LC14(%rip), %xmm0
	movdqa	64+kDefaultCommandBits.6826(%rip), %xmm5
	movdqa	80+kDefaultCommandBits.6826(%rip), %xmm6
	movdqa	96+kDefaultCommandBits.6826(%rip), %xmm7
	movups	%xmm1, 4608(%rbx)
	movups	%xmm0, 4496(%rbx)
	movdqa	.LC15(%rip), %xmm0
	movdqa	112+kDefaultCommandBits.6826(%rip), %xmm1
	movdqa	16+kDefaultCommandBits.6826(%rip), %xmm2
	movups	%xmm3, 4640(%rbx)
	movups	%xmm0, 4512(%rbx)
	movdqa	.LC16(%rip), %xmm0
	movups	%xmm4, 4656(%rbx)
	movups	%xmm0, 4528(%rbx)
	movdqa	.LC17(%rip), %xmm0
	movups	%xmm5, 4672(%rbx)
	movups	%xmm0, 4560(%rbx)
	movdqa	.LC18(%rip), %xmm0
	movups	%xmm6, 4688(%rbx)
	movups	%xmm0, 4576(%rbx)
	movdqa	.LC19(%rip), %xmm0
	movups	%xmm7, 4704(%rbx)
	movups	%xmm0, 4592(%rbx)
	movups	%xmm1, 4720(%rbx)
	movups	%xmm2, 4624(%rbx)
	movdqa	128+kDefaultCommandBits.6826(%rip), %xmm2
	movdqa	.LC20(%rip), %xmm0
	movdqa	144+kDefaultCommandBits.6826(%rip), %xmm3
	movdqa	160+kDefaultCommandBits.6826(%rip), %xmm4
	movq	%rax, 4912(%rbx)
	movups	%xmm0, 4864(%rbx)
	movdqa	.LC21(%rip), %xmm0
	movdqa	176+kDefaultCommandBits.6826(%rip), %xmm5
	movdqa	192+kDefaultCommandBits.6826(%rip), %xmm6
	movdqa	208+kDefaultCommandBits.6826(%rip), %xmm7
	movups	%xmm2, 4736(%rbx)
	movdqa	224+kDefaultCommandBits.6826(%rip), %xmm1
	movdqa	240+kDefaultCommandBits.6826(%rip), %xmm2
	movups	%xmm0, 4880(%rbx)
	movb	$0, 4920(%rbx)
	movdqa	.LC22(%rip), %xmm0
	movq	$448, 5376(%rbx)
	movups	%xmm3, 4752(%rbx)
	movups	%xmm4, 4768(%rbx)
	movups	%xmm5, 4784(%rbx)
	movups	%xmm6, 4800(%rbx)
	movups	%xmm7, 4816(%rbx)
	movups	%xmm1, 4832(%rbx)
	movups	%xmm2, 4848(%rbx)
	movups	%xmm0, 4896(%rbx)
.L1187:
	movl	$1, 5452(%rbx)
.L1169:
	movl	5440(%rbx), %eax
	cmpl	$-1, %eax
	je	.L1191
	cmpq	%rax, (%r12)
	jne	.L1197
	cmpl	$3, -72(%rbp)
	je	.L1192
.L1197:
	xorl	%eax, %eax
.L1168:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1381
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	.cfi_restore_state
	cmpl	$9, %eax
	jle	.L1171
	movl	28(%rdi), %edi
	testl	%edi, %edi
	je	.L1175
	cmpl	$30, %eax
	jle	.L1176
	movl	$30, %eax
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1380:
	movl	28(%rbx), %esi
	cmpl	$18, %eax
	movl	$18, %ecx
	cmovl	%ecx, %eax
	testl	%esi, %esi
	jne	.L1182
.L1183:
	leal	-34(%rax,%rax), %eax
	movb	$4, 346(%rbx)
	orl	$1, %eax
	movw	%ax, 344(%rbx)
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1191:
	cmpl	$3, -72(%rbp)
	je	.L1192
	movl	5444(%rbx), %eax
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1197
	testl	%eax, %eax
	je	.L1217
	cmpq	$0, (%r12)
	jne	.L1197
.L1217:
	movl	4(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L1382
.L1218:
	movq	160(%rbx), %rdx
	movl	12(%rbx), %ecx
	movl	$1, %edi
	xorl	%r14d, %r14d
	movq	256(%rbx), %rax
	movq	%rdx, %rsi
	salq	%cl, %rdi
	subq	%rax, %rsi
	movq	%rdi, %rcx
	cmpq	%rdi, %rsi
	jnb	.L1252
	subq	%rdx, %rcx
	addq	%rax, %rcx
	movq	%rcx, %r14
	je	.L1252
	movq	(%r12), %rcx
	testq	%rcx, %rcx
	jne	.L1383
.L1252:
	movq	-80(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	InjectFlushOrPushOutput
	movl	%eax, %edx
	testl	%eax, %eax
	jne	.L1218
	cmpq	$0, 5408(%rbx)
	jne	.L1376
	movl	5444(%rbx), %eax
	testl	%eax, %eax
	jne	.L1265
	testq	%r14, %r14
	je	.L1295
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	je	.L1376
.L1295:
	cmpl	$2, -72(%rbp)
	movq	(%r12), %rax
	jne	.L1296
	testq	%rax, %rax
	je	.L1287
.L1296:
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L1267
.L1269:
	cmpq	$0, 16(%rbx)
	jne	.L1270
	movq	160(%rbx), %rcx
	subq	256(%rbx), %rcx
	cmpq	$1073741823, %rax
	ja	.L1289
	cmpq	$1073741823, %rcx
	jbe	.L1384
.L1289:
	movl	$1073741824, %eax
.L1271:
	movq	%rax, 16(%rbx)
.L1270:
	leaq	5408(%rbx), %rcx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%edx, -96(%rbp)
	leaq	5400(%rbx), %r8
	call	EncodeData
	testl	%eax, %eax
	je	.L1197
	movl	-96(%rbp), %edx
	testl	%edx, %edx
	je	.L1272
	movl	$1, 5444(%rbx)
.L1272:
	testl	%r14d, %r14d
	je	.L1218
	movl	$2, 5444(%rbx)
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1184:
	cmpl	$16, %eax
	jne	.L1186
	xorl	%r14d, %r14d
	movb	$1, 346(%rbx)
	movw	%r14w, 344(%rbx)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1175:
	cmpl	$24, %eax
	jle	.L1173
	movl	$24, %eax
.L1172:
	movl	%eax, 8(%rbx)
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1179:
	cmpl	$16, %ecx
	movl	$16, %eax
	cmovl	%eax, %ecx
	movl	$24, %eax
	cmpl	$24, %ecx
	cmovg	%eax, %ecx
	cmpl	$2, (%rbx)
	movl	%ecx, 12(%rbx)
	jne	.L1367
.L1378:
	movl	$12, %edx
	movl	$1, %esi
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1192:
	cmpq	$0, 16(%rbx)
	jne	.L1196
	movq	160(%rbx), %rax
	subq	256(%rbx), %rax
	movl	$1073741824, %edx
	cmpq	$1073741824, %rax
	cmova	%rdx, %rax
	movq	%rax, 16(%rbx)
.L1196:
	movq	(%r12), %rdx
	cmpq	$16777216, %rdx
	ja	.L1197
	movl	5444(%rbx), %eax
	testl	%eax, %eax
	jne	.L1198
	movl	%edx, 5440(%rbx)
	movl	$3, 5444(%rbx)
.L1199:
	movq	-80(%rbp), %r14
.L1201:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	InjectFlushOrPushOutput
	testl	%eax, %eax
	jne	.L1201
	cmpq	$0, 5408(%rbx)
	jne	.L1376
	movq	248(%rbx), %rax
	cmpq	%rax, 160(%rbx)
	je	.L1203
	xorl	%esi, %esi
	leaq	5408(%rbx), %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	leaq	5400(%rbx), %r8
	call	EncodeData
	testl	%eax, %eax
	je	.L1197
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1186:
	cmpl	$17, %eax
	jne	.L1188
	movl	$1, %r11d
	movb	$7, 346(%rbx)
	movw	%r11w, 344(%rbx)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1287:
	movl	$1, %edx
.L1267:
	cmpl	$1, -72(%rbp)
	movl	%edx, %r14d
	sete	%dl
	movzbl	%dl, %edx
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1379:
	movl	%esi, %ecx
	movl	%eax, %edx
	shrl	%cl, %edx
	andl	$15, %edx
	sall	%cl, %edx
	cmpl	%edx, %eax
	jne	.L1372
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1188:
	jg	.L1183
	subl	$8, %eax
	movb	$7, 346(%rbx)
	sall	$4, %eax
	orl	$1, %eax
	movw	%ax, 344(%rbx)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1178:
	movl	$14, 12(%rbx)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1384:
	leaq	(%rax,%rcx), %rdi
	addl	%ecx, %eax
	movl	$1073741824, %ecx
	cmpq	$1073741824, %rdi
	cmovnb	%rcx, %rax
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1203:
	cmpl	$3, 5444(%rbx)
	movl	5440(%rbx), %eax
	je	.L1385
	testl	%eax, %eax
	jne	.L1209
	movl	$4294967295, %eax
	movq	%rax, 5440(%rbx)
.L1376:
	movl	$1, %eax
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	-88(%rbp), %rax
	cmpq	%rcx, %r14
	leaq	128(%rbx), %r11
	cmova	%rcx, %r14
	movq	(%rax), %r9
	movl	188(%rbx), %eax
	testl	%eax, %eax
	jne	.L1253
	movl	176(%rbx), %edx
	cmpq	%rdx, %r14
	jb	.L1386
.L1253:
	movl	180(%rbx), %r8d
	cmpl	%r8d, 184(%rbx)
	jb	.L1387
.L1256:
	movl	176(%rbx), %edx
	andl	172(%rbx), %eax
	movl	168(%rbx), %esi
	movq	200(%rbx), %rdi
	movl	%eax, %r8d
	cmpl	%edx, %eax
	jb	.L1388
.L1258:
	leaq	(%r14,%r8), %rax
	addq	%r8, %rdi
	cmpq	%rsi, %rax
	ja	.L1259
	movq	%r14, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
.L1260:
	movl	168(%rbx), %esi
	movq	200(%rbx), %rax
	movl	188(%rbx), %edx
	subl	$2, %esi
	movzbl	(%rax,%rsi), %esi
	movb	%sil, -2(%rax)
	movl	168(%rbx), %esi
	movq	200(%rbx), %rax
	subl	$1, %esi
	movzbl	(%rax,%rsi), %esi
	movb	%sil, -1(%rax)
	movl	188(%rbx), %eax
	movl	%r14d, %esi
	andl	$2147483647, %esi
	andl	$2147483647, %eax
	addl	%esi, %eax
	testl	%edx, %edx
	jns	.L1375
	orl	$-2147483648, %eax
.L1375:
	movl	%eax, 188(%rbx)
.L1255:
	addq	%r14, 160(%rbx)
	cmpl	%eax, 172(%rbx)
	jnb	.L1389
.L1262:
	movq	-88(%rbp), %rax
	addq	%r14, (%rax)
	subq	%r14, (%r12)
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1382:
	movl	8(%rbx), %ecx
	movl	$1, %edx
	leaq	128(%rbx), %rdi
	movq	%rdi, -112(%rbp)
	salq	%cl, %rdx
	movq	%rdx, -144(%rbp)
	cmpl	$1, %eax
	je	.L1390
	movq	$0, -160(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	-80(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	InjectFlushOrPushOutput
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1224
	cmpq	$0, 5408(%rbx)
	jne	.L1225
	movl	5444(%rbx), %r9d
	testl	%r9d, %r9d
	jne	.L1225
	movq	(%r12), %rax
	testq	%rax, %rax
	jne	.L1226
	movl	-72(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1225
	movl	-72(%rbp), %eax
	movzbl	346(%rbx), %edx
	cmpl	$2, %eax
	je	.L1291
	cmpl	$1, %eax
	je	.L1275
	movl	$0, -96(%rbp)
	xorl	%r10d, %r10d
	movl	$503, %ecx
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	%rdx, -64(%rbp)
	movl	$0, -104(%rbp)
.L1231:
	cmpq	%rcx, (%r15)
	jb	.L1233
	movq	0(%r13), %r11
	movl	$1, %r14d
.L1234:
	movzwl	344(%rbx), %eax
	movb	%al, (%r11)
	movzbl	345(%rbx), %eax
	movb	%al, 1(%r11)
	movl	4(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L1236
	cmpq	$32768, %r10
	movl	$32768, %eax
	cmovbe	%r10, %rax
	cmpq	$256, %r10
	jbe	.L1285
.L1274:
	movl	$256, %r9d
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	%r9, %rdx
	addq	%r9, %r9
	cmpq	%r9, %rax
	ja	.L1238
	testl	%ecx, %ecx
	jne	.L1374
	testl	$699050, %r9d
	je	.L1241
.L1374:
	leaq	0(,%r9,4), %rdx
.L1240:
	cmpq	$1024, %r9
	jbe	.L1237
	movq	4464(%rbx), %r8
	cmpq	4472(%rbx), %r9
	ja	.L1391
.L1243:
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	memset@PLT
	movl	4(%rbx), %edi
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
	movq	-136(%rbp), %r11
	movq	%rax, %r8
	testl	%edi, %edi
	jne	.L1245
	leaq	-64(%rbp), %rax
	pushq	%r11
	movl	-96(%rbp), %ecx
	movq	%r10, %rdx
	pushq	%rax
	leaq	4864(%rbx), %rax
	movq	-112(%rbp), %rdi
	pushq	%rax
	leaq	5376(%rbx), %rax
	pushq	%rax
	leaq	4608(%rbx), %rax
	pushq	%rax
	leaq	4480(%rbx), %rax
	pushq	%rax
	movq	-88(%rbp), %rax
	movq	%r11, -128(%rbp)
	movq	(%rax), %rsi
	movq	%r10, -120(%rbp)
	call	BrotliCompressFragmentFast
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r11
	addq	$48, %rsp
.L1246:
	movq	-88(%rbp), %rax
	addq	%r10, (%rax)
	movq	-64(%rbp), %rax
	subq	%r10, (%r12)
	movq	%rax, %rdx
	shrq	$3, %rdx
	testl	%r14d, %r14d
	je	.L1247
	addq	%rdx, 0(%r13)
	movq	-80(%rbp), %rdi
	subq	%rdx, (%r15)
	movq	5416(%rbx), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 5416(%rbx)
	testq	%rdi, %rdi
	je	.L1248
	movq	%rcx, (%rdi)
.L1248:
	movzbl	(%r11,%rdx), %edx
	movl	-104(%rbp), %esi
	andl	$7, %eax
	movb	%al, 346(%rbx)
	movw	%dx, 344(%rbx)
	testl	%esi, %esi
	je	.L1249
	movl	$1, 5444(%rbx)
.L1249:
	movl	-96(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1224
	movl	$2, 5444(%rbx)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	-144(%rbp), %rdi
	movzbl	346(%rbx), %edx
	cmpq	%rax, %rdi
	movq	%rdi, %r10
	cmova	%rax, %r10
	leaq	503(%r10,%r10), %rcx
	jnb	.L1228
.L1373:
	movl	$0, -96(%rbp)
	jmp	.L1229
.L1228:
	cmpl	$2, -72(%rbp)
	je	.L1392
	cmpl	$1, -72(%rbp)
	jne	.L1373
	movq	%rdx, -64(%rbp)
	testq	%r10, %r10
	jne	.L1284
.L1275:
	movl	$1, 5444(%rbx)
	jmp	.L1224
.L1285:
	movl	$2048, %edx
	movl	$512, %r9d
	.p2align 4,,10
	.p2align 3
.L1237:
	leaq	368(%rbx), %r8
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	%r11, 5400(%rbx)
	movq	%rdx, 5408(%rbx)
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1245:
	leaq	-64(%rbp), %rax
	pushq	%r11
	movl	-96(%rbp), %ecx
	movq	%r10, %rdx
	pushq	%rax
	movq	-88(%rbp), %rax
	pushq	%r9
	movq	-112(%rbp), %rdi
	movq	-160(%rbp), %r9
	movq	(%rax), %rsi
	pushq	%r8
	movq	-152(%rbp), %r8
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	BrotliCompressFragmentTwoPass
	movq	-128(%rbp), %r11
	movq	-120(%rbp), %r10
	addq	$32, %rsp
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1236:
	movl	$131072, %eax
	cmpq	$131072, %r10
	movl	$1024, %edx
	movl	$256, %r9d
	cmovbe	%r10, %rax
	cmpq	$256, %r10
	ja	.L1274
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	360(%rbx), %r11
	cmpq	%rcx, 352(%rbx)
	jnb	.L1234
	movq	-112(%rbp), %rdi
	movq	%r11, %rsi
	movq	%r10, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	BrotliFree
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdi
	movq	$0, 360(%rbx)
	movq	%rcx, %rsi
	call	BrotliAllocate
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r10
	movq	%rax, 360(%rbx)
	movq	%rax, %r11
	movq	%rcx, 352(%rbx)
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1198:
	subl	$3, %eax
	cmpl	$1, %eax
	ja	.L1197
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1291:
	movl	$1, -96(%rbp)
	xorl	%r10d, %r10d
	movl	$503, %ecx
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	-112(%rbp), %r15
	movq	-168(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliFree
	movq	-176(%rbp), %rsi
	movq	%r15, %rdi
	call	BrotliFree
	cmpl	$1, 5444(%rbx)
	jne	.L1376
	cmpq	$0, 5408(%rbx)
	jne	.L1376
.L1273:
	movl	$0, 5444(%rbx)
	movl	$1, %eax
	movq	$0, 5400(%rbx)
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1241:
	leaq	0(,%rdx,4), %r9
	salq	$4, %rdx
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	%r9, 4472(%rbx)
	movq	-112(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r11, -184(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	BrotliFree
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %rdi
	movq	$0, 4464(%rbx)
	movq	%rdx, %rsi
	call	BrotliAllocate
	movq	-184(%rbp), %r11
	movq	-136(%rbp), %r10
	movq	%rax, 4464(%rbx)
	movq	-128(%rbp), %r9
	movq	%rax, %r8
	movq	-120(%rbp), %rdx
	jmp	.L1243
.L1389:
	addq	200(%rbx), %rax
	xorl	%edx, %edx
	movl	$0, (%rax)
	movw	%dx, 4(%rax)
	movb	$0, 6(%rax)
	jmp	.L1262
.L1387:
	leal	2(%r8), %esi
	movq	%r11, %rdi
	movq	%r9, -104(%rbp)
	addq	$7, %rsi
	movl	%r8d, -96(%rbp)
	movq	%r11, -112(%rbp)
	call	BrotliAllocate
	movq	192(%rbx), %rsi
	movl	-96(%rbp), %r8d
	movq	-104(%rbp), %r9
	movq	%rax, %r10
	testq	%rsi, %rsi
	je	.L1257
	movl	184(%rbx), %eax
	movq	%r10, %rdi
	movl	%r8d, -120(%rbp)
	movq	%r10, -96(%rbp)
	leal	2(%rax), %edx
	addq	$7, %rdx
	call	memcpy@PLT
	movq	-112(%rbp), %r11
	movq	192(%rbx), %rsi
	movq	%r11, %rdi
	call	BrotliFree
	movl	-120(%rbp), %r8d
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r10
.L1257:
	leaq	2(%r10), %rax
	movl	%r8d, 184(%rbx)
	movq	%rax, 200(%rbx)
	movq	%r10, 192(%rbx)
	movb	$0, 1(%r10)
	movq	200(%rbx), %rax
	movb	$0, -2(%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, (%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 1(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 2(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 3(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 4(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 5(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 6(%rdx,%rax)
	movl	168(%rbx), %eax
	movq	200(%rbx), %rdx
	subl	$2, %eax
	movb	$0, (%rdx,%rax)
	movl	168(%rbx), %eax
	movq	200(%rbx), %rdx
	subl	$1, %eax
	movb	$0, (%rdx,%rax)
	movl	188(%rbx), %eax
	jmp	.L1256
.L1209:
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	je	.L1210
	cmpq	%rdx, %rax
	movq	0(%r13), %rdi
	cmovbe	%rax, %rdx
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	movq	%rdx, -72(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rax
	movq	-72(%rbp), %rdx
	addq	%rdx, (%rax)
	subq	%rdx, (%r12)
	subl	%edx, 5440(%rbx)
	addq	%rdx, 0(%r13)
	subq	%rdx, (%r15)
	jmp	.L1201
.L1265:
	cmpl	$1, %eax
	je	.L1273
	jmp	.L1376
.L1390:
	movq	5384(%rbx), %rax
	cmpq	%rdx, (%r12)
	movq	%rdx, %r14
	cmovbe	(%r12), %r14
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	jne	.L1221
	cmpq	$131071, %r14
	ja	.L1393
	cmpq	$0, -152(%rbp)
	jne	.L1221
	testq	%r14, %r14
	jne	.L1223
	movq	$0, -160(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	jmp	.L1224
.L1210:
	movl	$16, %edi
	cmpl	$16, %eax
	leaq	5424(%rbx), %rdx
	cmova	%edi, %eax
	movq	-88(%rbp), %rdi
	movq	%rdx, 5400(%rbx)
	movl	%eax, %esi
	movq	(%rdi), %rcx
	cmpl	$8, %eax
	jnb	.L1211
	testb	$4, %al
	jne	.L1394
	testl	%eax, %eax
	je	.L1212
	movzbl	(%rcx), %edi
	movb	%dil, 5424(%rbx)
	testb	$2, %al
	jne	.L1395
.L1212:
	movq	-88(%rbp), %rdi
	addq	%rsi, (%rdi)
	subq	%rsi, (%r12)
	subl	%eax, 5440(%rbx)
	movq	%rsi, 5408(%rbx)
	jmp	.L1201
.L1221:
	movq	5392(%rbx), %rax
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1224
.L1386:
	movl	%r14d, 188(%rbx)
	leal	2(%r14), %esi
	movq	%r11, %rdi
	addq	$7, %rsi
	movq	%r9, -96(%rbp)
	movq	%r11, -104(%rbp)
	call	BrotliAllocate
	movq	192(%rbx), %rsi
	movq	-96(%rbp), %r9
	movq	%rax, %r8
	testq	%rsi, %rsi
	je	.L1254
	movl	184(%rbx), %eax
	movq	%r8, %rdi
	movq	%r9, -112(%rbp)
	movq	%r8, -96(%rbp)
	leal	2(%rax), %edx
	addq	$7, %rdx
	call	memcpy@PLT
	movq	-104(%rbp), %r11
	movq	192(%rbx), %rsi
	movq	%r11, %rdi
	call	BrotliFree
	movq	-112(%rbp), %r9
	movq	-96(%rbp), %r8
.L1254:
	leaq	2(%r8), %rax
	movl	%r14d, 184(%rbx)
	movq	%r9, %rsi
	movq	%rax, 200(%rbx)
	movq	%r8, 192(%rbx)
	movb	$0, 1(%r8)
	movq	200(%rbx), %rax
	movb	$0, -2(%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, (%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 1(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 2(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 3(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 4(%rdx,%rax)
	movl	184(%rbx), %eax
	movq	200(%rbx), %rdx
	movb	$0, 5(%rdx,%rax)
	movq	200(%rbx), %rdx
	movl	184(%rbx), %eax
	movb	$0, 6(%rdx,%rax)
	movq	200(%rbx), %rdi
	movq	%r14, %rdx
	call	memcpy@PLT
	movl	188(%rbx), %eax
	jmp	.L1255
.L1385:
	movzbl	346(%rbx), %edx
	movzwl	344(%rbx), %ecx
	xorl	%r10d, %r10d
	movl	%eax, %edi
	leaq	5424(%rbx), %rsi
	movw	%r10w, 344(%rbx)
	movw	%cx, 5424(%rbx)
	movq	%rdx, %rcx
	shrq	$3, %rcx
	movq	%rsi, 5400(%rbx)
	addq	%rsi, %rcx
	movb	$0, 346(%rbx)
	movzbl	(%rcx), %r8d
	movq	%r8, (%rcx)
	leaq	1(%rdx), %rcx
	movl	$3, %r8d
	movq	%rcx, %r9
	andl	$7, %ecx
	shrq	$3, %r9
	salq	%cl, %r8
	leaq	3(%rdx), %rcx
	addq	%rsi, %r9
	shrq	$3, %rcx
	movzbl	(%r9), %r10d
	addq	%rsi, %rcx
	orq	%r10, %r8
	movq	%r8, (%r9)
	movzbl	(%rcx), %r8d
	movq	%r8, (%rcx)
	leaq	4(%rdx), %rcx
	addq	$6, %rdx
	movq	%rcx, %r9
	shrq	$3, %r9
	addq	%rsi, %r9
	testq	%rdi, %rdi
	jne	.L1206
	movzbl	(%r9), %eax
	movq	%rax, (%r9)
.L1207:
	movl	$4, 5444(%rbx)
	addq	$7, %rdx
	shrq	$3, %rdx
	movq	%rdx, 5408(%rbx)
	jmp	.L1201
.L1259:
	movl	180(%rbx), %eax
	movq	%r9, %rsi
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	subq	%r8, %rax
	cmpq	%r14, %rax
	cmova	%r14, %rax
	movq	%rax, %rdx
	call	memcpy@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r9
	movq	%r14, %rdx
	movl	168(%rbx), %esi
	movq	200(%rbx), %rdi
	subq	%rsi, %rdx
	subq	%r8, %rsi
	addq	%r8, %rdx
	addq	%r9, %rsi
	call	memcpy@PLT
	jmp	.L1260
.L1388:
	addq	%r8, %rsi
	subq	%r8, %rdx
	movq	%r8, -104(%rbp)
	addq	%rsi, %rdi
	cmpq	%r14, %rdx
	movq	%r9, %rsi
	movq	%r9, -96(%rbp)
	cmova	%r14, %rdx
	call	memcpy@PLT
	movl	168(%rbx), %esi
	movq	200(%rbx), %rdi
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r9
	jmp	.L1258
.L1211:
	movq	(%rcx), %rdi
	leaq	5432(%rbx), %r8
	andq	$-8, %r8
	movq	%rdi, 5424(%rbx)
	movq	-8(%rcx,%rsi), %rdi
	movq	%rdi, -8(%rdx,%rsi)
	subq	%r8, %rdx
	subq	%rdx, %rcx
	addl	%eax, %edx
	andl	$-8, %edx
	movq	%rcx, %r9
	cmpl	$8, %edx
	jb	.L1212
	andl	$-8, %edx
	xorl	%ecx, %ecx
.L1215:
	movl	%ecx, %edi
	addl	$8, %ecx
	movq	(%r9,%rdi), %r10
	movq	%r10, (%r8,%rdi)
	cmpl	%edx, %ecx
	jb	.L1215
	jmp	.L1212
.L1206:
	xorl	%r8d, %r8d
	cmpq	$1, %rdi
	je	.L1208
	leal	-1(%rax), %r8d
	bsrl	%r8d, %r8d
	addl	$1, %r8d
.L1208:
	addl	$7, %r8d
	movzbl	(%r9), %r10d
	andl	$7, %ecx
	subq	$1, %rdi
	movl	%r8d, %eax
	andl	$-8, %r8d
	shrl	$3, %eax
	salq	%cl, %rax
	movl	%edx, %ecx
	orq	%rax, %r10
	movq	%rdx, %rax
	andl	$7, %ecx
	addq	%r8, %rdx
	shrq	$3, %rax
	movq	%r10, (%r9)
	salq	%cl, %rdi
	addq	%rax, %rsi
	movzbl	(%rsi), %eax
	orq	%rdi, %rax
	movq	%rax, (%rsi)
	jmp	.L1207
.L1393:
	movl	$524288, %esi
	call	BrotliAllocate
	movl	$131072, %esi
	leaq	128(%rbx), %rdi
	movq	%rax, 5384(%rbx)
	call	BrotliAllocate
	movq	%rax, 5392(%rbx)
	movq	5384(%rbx), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	jne	.L1221
.L1223:
	cmpq	$131072, %r14
	movl	$131072, %eax
	leaq	128(%rbx), %rdi
	cmova	%rax, %r14
	leaq	0(,%r14,4), %rsi
	call	BrotliAllocate
	movq	%r14, %rsi
	leaq	128(%rbx), %rdi
	movq	%rax, -168(%rbp)
	call	BrotliAllocate
	movq	%rax, -176(%rbp)
	movq	%rax, -160(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -152(%rbp)
	jmp	.L1224
.L1381:
	call	__stack_chk_fail@PLT
.L1394:
	movl	(%rcx), %edi
	movl	%edi, 5424(%rbx)
	movl	-4(%rcx,%rsi), %ecx
	movl	%ecx, -4(%rdx,%rsi)
	jmp	.L1212
.L1395:
	movzwl	-2(%rcx,%rsi), %ecx
	movw	%cx, -2(%rdx,%rsi)
	jmp	.L1212
.L1392:
	movl	$1, -96(%rbp)
	jmp	.L1229
.L1284:
	movl	$0, -96(%rbp)
	movl	$1, -104(%rbp)
	jmp	.L1231
	.cfi_endproc
.LFE349:
	.size	BrotliEncoderCompressStream, .-BrotliEncoderCompressStream
	.p2align 4
	.globl	BrotliEncoderCompress
	.type	BrotliEncoderCompress, @function
BrotliEncoderCompress:
.LFB342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	(%r9), %rcx
	movq	%r8, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rax
	movq	%rcx, -120(%rbp)
	shrq	$14, %rax
	leaq	6(%rbx,%rax,4), %r15
	testq	%rbx, %rbx
	je	.L1397
	cmpq	%r15, %rbx
	movl	$0, %eax
	cmova	%rax, %r15
	testq	%rcx, %rcx
	je	.L1412
	movl	%esi, %r14d
	cmpl	$10, %edi
	je	.L1454
	movl	%edx, -136(%rbp)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%edi, -124(%rbp)
	xorl	%edi, %edi
	call	BrotliEncoderCreateInstance
	movl	-124(%rbp), %r11d
	movl	-136(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1412
	movq	-112(%rbp), %rax
	movl	5452(%r12), %r8d
	movq	%rbx, -96(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -88(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -80(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -72(%rbp)
	testl	%r8d, %r8d
	jne	.L1406
	movl	%ebx, %eax
	movl	%r11d, 4(%r12)
	movl	%r14d, 8(%r12)
	movl	%r10d, (%r12)
	movq	%rax, 16(%r12)
	cmpl	$24, %r14d
	jle	.L1406
	movl	$1, 28(%r12)
.L1406:
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	$2, %esi
	movq	%r12, %rdi
	pushq	%rax
	leaq	-88(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	leaq	-72(%rbp), %r9
	leaq	-80(%rbp), %r8
	call	BrotliEncoderCompressStream
	popq	%rsi
	popq	%rdi
	cmpl	$2, 5444(%r12)
	movl	%eax, -124(%rbp)
	je	.L1455
	movl	$0, -124(%rbp)
.L1408:
	movq	-64(%rbp), %rax
	leaq	128(%r12), %r14
	movq	360(%r12), %rsi
	movq	%r14, %rdi
	movq	%rax, 0(%r13)
	movq	136(%r12), %rax
	movq	%rax, -136(%rbp)
	movq	144(%r12), %rax
	movq	%rax, -144(%rbp)
	call	BrotliFree
	movq	216(%r12), %rsi
	movq	%r14, %rdi
	movq	$0, 360(%r12)
	call	BrotliFree
	movq	192(%r12), %rsi
	movq	%r14, %rdi
	movq	$0, 216(%r12)
	call	BrotliFree
	movq	152(%r12), %rsi
	movq	$0, 192(%r12)
	testq	%rsi, %rsi
	je	.L1409
	movq	%r14, %rdi
	call	BrotliFree
	movq	$0, 152(%r12)
.L1409:
	movq	4464(%r12), %rsi
	movq	%r14, %rdi
	call	BrotliFree
	movq	5384(%r12), %rsi
	movq	%r14, %rdi
	movq	$0, 4464(%r12)
	call	BrotliFree
	movq	5392(%r12), %rsi
	movq	%r14, %rdi
	movq	$0, 5384(%r12)
	call	BrotliFree
	movq	-144(%rbp), %rdi
	movq	-136(%rbp), %rax
	movq	%r12, %rsi
	movq	$0, 5392(%r12)
	call	*%rax
	movl	-124(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1402
	testq	%r15, %r15
	je	.L1453
	cmpq	%r15, 0(%r13)
	ja	.L1402
.L1453:
	movl	$1, %eax
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	$0, 0(%r13)
	testq	%r15, %r15
	je	.L1412
.L1418:
	cmpq	%r15, -120(%rbp)
	jnb	.L1456
	.p2align 4,,10
	.p2align 3
.L1412:
	xorl	%eax, %eax
.L1396:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1457
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_restore_state
	cmpq	$0, -120(%rbp)
	je	.L1412
	movq	-104(%rbp), %rax
	movq	$1, 0(%r13)
	movb	$6, (%rax)
	movl	$1, %eax
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1455:
	cmpq	$0, 5408(%r12)
	movl	$0, %eax
	cmove	-124(%rbp), %eax
	movl	%eax, -124(%rbp)
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1454:
	cmpl	$16, %esi
	movl	$16, %edi
	movl	$30, %eax
	movq	%r8, %rdx
	cmovge	%esi, %edi
	movq	-104(%rbp), %r8
	movq	%r9, %rcx
	movq	%rbx, %rsi
	cmpl	$30, %edi
	cmovg	%eax, %edi
	call	BrotliCompressBufferQuality10
	testl	%eax, %eax
	je	.L1402
	movl	$1, %eax
	testq	%r15, %r15
	je	.L1396
	cmpq	%r15, 0(%r13)
	jbe	.L1396
	movq	$0, 0(%r13)
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	-104(%rbp), %rax
	movl	$801, %edx
	xorl	%r12d, %r12d
	movw	%dx, (%rax)
	movl	$2, %edx
.L1417:
	cmpq	$16777216, %rbx
	ja	.L1427
	leal	-8(,%rbx,8), %eax
	cmpq	$65536, %rbx
	ja	.L1414
	orl	$524288, %eax
	movl	%eax, %esi
	movzbl	%ah, %ecx
	shrl	$16, %eax
.L1415:
	movq	-104(%rbp), %rdi
	leaq	3(%rdx), %r15
	movb	%sil, (%rdi,%rdx)
	movq	-112(%rbp), %rsi
	movb	%cl, 1(%rdi,%rdx)
	movb	%al, 2(%rdi,%rdx)
	addq	%r12, %rsi
	movq	%rbx, %rdx
	addq	%r15, %rdi
	call	memcpy@PLT
	leaq	(%r15,%rbx), %rdx
.L1422:
	movq	-104(%rbp), %rax
	movb	$3, (%rax,%rdx)
	addq	$1, %rdx
	movq	%rdx, 0(%r13)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1427:
	movl	$16777216, %r15d
	movl	$-1, %ecx
	movl	$-1, %esi
	movl	$-4, %edi
	movl	$268435452, %eax
.L1413:
	movq	-104(%rbp), %r10
	shrl	$24, %eax
	leaq	4(%rdx), %r14
	movb	%dil, (%r10,%rdx)
	movq	%r10, %rdi
	movb	%sil, 1(%r10,%rdx)
	addq	%r14, %rdi
	movb	%al, 3(%r10,%rdx)
	movq	-112(%rbp), %rax
	movb	%cl, 2(%r10,%rdx)
	movq	%r15, %rdx
	leaq	(%rax,%r12), %rsi
	addq	%r15, %r12
	call	memcpy@PLT
	leaq	(%r15,%r14), %rdx
	subq	%r15, %rbx
	jne	.L1417
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1414:
	cmpq	$1048576, %rbx
	ja	.L1416
	orl	$8388610, %eax
	movl	%eax, %esi
	movzbl	%ah, %ecx
	shrl	$16, %eax
	jmp	.L1415
.L1416:
	orl	$134217732, %eax
	movq	%rbx, %r15
	movl	%eax, %ecx
	movl	%eax, %edi
	movzbl	%ah, %esi
	shrl	$16, %ecx
	jmp	.L1413
.L1457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE342:
	.size	BrotliEncoderCompress, .-BrotliEncoderCompress
	.p2align 4
	.globl	BrotliEncoderIsFinished
	.type	BrotliEncoderIsFinished, @function
BrotliEncoderIsFinished:
.LFB350:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, 5444(%rdi)
	je	.L1461
	ret
	.p2align 4,,10
	.p2align 3
.L1461:
	xorl	%eax, %eax
	cmpq	$0, 5408(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE350:
	.size	BrotliEncoderIsFinished, .-BrotliEncoderIsFinished
	.p2align 4
	.globl	BrotliEncoderHasMoreOutput
	.type	BrotliEncoderHasMoreOutput, @function
BrotliEncoderHasMoreOutput:
.LFB351:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 5408(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE351:
	.size	BrotliEncoderHasMoreOutput, .-BrotliEncoderHasMoreOutput
	.p2align 4
	.globl	BrotliEncoderTakeOutput
	.type	BrotliEncoderTakeOutput, @function
BrotliEncoderTakeOutput:
.LFB352:
	.cfi_startproc
	endbr64
	movq	5408(%rdi), %rax
	movq	(%rsi), %rcx
	movq	%rax, %rdx
	testq	%rcx, %rcx
	je	.L1464
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	movq	%rcx, %rdx
.L1464:
	testq	%rdx, %rdx
	je	.L1465
	movq	5400(%rdi), %r8
	subq	%rdx, %rax
	addq	%rdx, 5416(%rdi)
	cmpl	$1, 5444(%rdi)
	movq	%rax, 5408(%rdi)
	leaq	(%r8,%rdx), %rcx
	movq	%rcx, 5400(%rdi)
	jne	.L1466
	testq	%rax, %rax
	je	.L1476
.L1466:
	movq	%rdx, (%rsi)
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	xorl	%r8d, %r8d
	movq	$0, (%rsi)
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1476:
	movl	$0, 5444(%rdi)
	movq	$0, 5400(%rdi)
	jmp	.L1466
	.cfi_endproc
.LFE352:
	.size	BrotliEncoderTakeOutput, .-BrotliEncoderTakeOutput
	.p2align 4
	.globl	BrotliEncoderVersion
	.type	BrotliEncoderVersion, @function
BrotliEncoderVersion:
.LFB353:
	.cfi_startproc
	endbr64
	movl	$16777223, %eax
	ret
	.cfi_endproc
.LFE353:
	.size	BrotliEncoderVersion, .-BrotliEncoderVersion
	.section	.rodata
	.align 32
	.type	kStaticContextMapContinuation.6835, @object
	.size	kStaticContextMapContinuation.6835, 256
kStaticContextMapContinuation.6835:
	.long	1
	.long	1
	.long	2
	.long	2
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	kStaticContextMapSimpleUTF8.6836, @object
	.size	kStaticContextMapSimpleUTF8.6836, 256
kStaticContextMapSimpleUTF8.6836:
	.long	0
	.long	0
	.long	1
	.long	1
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	kStaticContextMapComplexUTF8.6859, @object
	.size	kStaticContextMapComplexUTF8.6859, 256
kStaticContextMapComplexUTF8.6859:
	.long	11
	.long	11
	.long	12
	.long	12
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	9
	.long	9
	.long	2
	.long	2
	.long	2
	.long	2
	.long	1
	.long	1
	.long	1
	.long	1
	.long	8
	.long	3
	.long	3
	.long	3
	.long	1
	.long	1
	.long	1
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	8
	.long	4
	.long	4
	.long	4
	.long	8
	.long	7
	.long	4
	.long	4
	.long	8
	.long	0
	.long	0
	.long	0
	.long	3
	.long	3
	.long	3
	.long	3
	.long	5
	.long	5
	.long	10
	.long	5
	.long	5
	.long	5
	.long	10
	.long	5
	.long	6
	.long	6
	.long	6
	.long	6
	.long	6
	.long	6
	.long	6
	.long	6
	.align 16
	.type	lut.6895, @object
	.size	lut.6895, 16
lut.6895:
	.long	0
	.long	0
	.long	1
	.long	2
	.align 32
	.type	kDefaultCommandBits.6826, @object
	.size	kDefaultCommandBits.6826, 256
kDefaultCommandBits.6826:
	.value	0
	.value	0
	.value	8
	.value	9
	.value	3
	.value	35
	.value	7
	.value	71
	.value	39
	.value	103
	.value	23
	.value	47
	.value	175
	.value	111
	.value	239
	.value	31
	.value	0
	.value	0
	.value	0
	.value	4
	.value	12
	.value	2
	.value	10
	.value	6
	.value	13
	.value	29
	.value	11
	.value	43
	.value	27
	.value	59
	.value	87
	.value	55
	.value	15
	.value	79
	.value	319
	.value	831
	.value	191
	.value	703
	.value	447
	.value	959
	.value	0
	.value	14
	.value	1
	.value	25
	.value	5
	.value	21
	.value	19
	.value	51
	.value	119
	.value	159
	.value	95
	.value	223
	.value	479
	.value	991
	.value	63
	.value	575
	.value	127
	.value	639
	.value	383
	.value	895
	.value	255
	.value	767
	.value	511
	.value	1023
	.value	14
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	27
	.value	59
	.value	7
	.value	39
	.value	23
	.value	55
	.value	30
	.value	1
	.value	17
	.value	9
	.value	25
	.value	5
	.value	0
	.value	8
	.value	4
	.value	12
	.value	2
	.value	10
	.value	6
	.value	21
	.value	13
	.value	29
	.value	3
	.value	19
	.value	11
	.value	15
	.value	47
	.value	31
	.value	95
	.value	63
	.value	127
	.value	255
	.value	767
	.value	2815
	.value	1791
	.value	3839
	.value	511
	.value	2559
	.value	1535
	.value	3583
	.value	1023
	.value	3071
	.value	2047
	.value	4095
	.zero	8
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.align 32
	.type	kContextLookup, @object
	.size	kContextLookup, 2048
kContextLookup:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	30
	.byte	30
	.byte	30
	.byte	30
	.byte	31
	.byte	31
	.byte	31
	.byte	31
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	33
	.byte	33
	.byte	33
	.byte	33
	.byte	34
	.byte	34
	.byte	34
	.byte	34
	.byte	35
	.byte	35
	.byte	35
	.byte	35
	.byte	36
	.byte	36
	.byte	36
	.byte	36
	.byte	37
	.byte	37
	.byte	37
	.byte	37
	.byte	38
	.byte	38
	.byte	38
	.byte	38
	.byte	39
	.byte	39
	.byte	39
	.byte	39
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	41
	.byte	41
	.byte	41
	.byte	41
	.byte	42
	.byte	42
	.byte	42
	.byte	42
	.byte	43
	.byte	43
	.byte	43
	.byte	43
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	46
	.byte	46
	.byte	46
	.byte	46
	.byte	47
	.byte	47
	.byte	47
	.byte	47
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	57
	.byte	57
	.byte	58
	.byte	58
	.byte	58
	.byte	58
	.byte	59
	.byte	59
	.byte	59
	.byte	59
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	61
	.byte	61
	.byte	61
	.byte	61
	.byte	62
	.byte	62
	.byte	62
	.byte	62
	.byte	63
	.byte	63
	.byte	63
	.byte	63
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	8
	.byte	12
	.byte	16
	.byte	12
	.byte	12
	.byte	20
	.byte	12
	.byte	16
	.byte	24
	.byte	28
	.byte	12
	.byte	12
	.byte	32
	.byte	12
	.byte	36
	.byte	12
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	32
	.byte	32
	.byte	24
	.byte	40
	.byte	28
	.byte	12
	.byte	12
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	24
	.byte	12
	.byte	28
	.byte	12
	.byte	12
	.byte	12
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	24
	.byte	12
	.byte	28
	.byte	12
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	56
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	2061584302
	.long	1072672276
	.align 8
.LC2:
	.long	2061584302
	.long	1075818004
	.align 8
.LC3:
	.long	0
	.long	1076494336
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC5:
	.long	0
	.long	1074266112
	.align 8
.LC6:
	.long	2576980378
	.long	1070176665
	.align 8
.LC7:
	.long	0
	.long	1076101120
	.align 8
.LC8:
	.long	1202590843
	.long	1066695393
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.long	4
	.long	11
	.long	15
	.long	16
	.section	.rodata.cst8
	.align 8
.LC10:
	.long	0
	.long	1072168960
	.section	.rodata.cst16
	.align 16
.LC13:
	.quad	506380106026189824
	.quad	578721382704547591
	.align 16
.LC14:
	.quad	289360691352043520
	.quad	506380106043098373
	.align 16
.LC15:
	.quad	723401728380765959
	.quad	434039933204956160
	.align 16
.LC16:
	.quad	723401728363857927
	.quad	723401728380766730
	.align 16
.LC17:
	.quad	361701968013821446
	.quad	289360691369149701
	.align 16
.LC18:
	.quad	361700864190317572
	.quad	722835466956965381
	.align 16
.LC19:
	.quad	868082074056920076
	.quad	202116108
	.align 16
.LC20:
	.quad	-6995533984229394433
	.quad	6394081712843677009
	.align 16
.LC21:
	.quad	-8301306910555482024
	.quad	6971997869591224554
	.align 16
.LC22:
	.quad	6971997762803099420
	.quad	-7758425836993374160
	.hidden	BrotliCompressFragmentTwoPass
	.hidden	BrotliCompressFragmentFast
	.hidden	BrotliCreateZopfliBackwardReferences
	.hidden	BrotliCreateHqZopfliBackwardReferences
	.hidden	BrotliCreateBackwardReferences
	.hidden	BrotliIsMostlyUTF8
	.hidden	BrotliZopfliCreateCommands
	.hidden	BrotliZopfliComputeShortestPath
	.hidden	BrotliInitZopfliNodes
	.hidden	BrotliAllocate
	.hidden	BrotliInitMemoryManager
	.hidden	BrotliInitDistanceParams
	.hidden	BrotliInitEncoderDictionary
	.hidden	BrotliBuildMetaBlock
	.hidden	BrotliOptimizeHistograms
	.hidden	BrotliStoreMetaBlockFast
	.hidden	BrotliStoreMetaBlockTrivial
	.hidden	BrotliStoreUncompressedMetaBlock
	.hidden	BrotliFree
	.hidden	BrotliDestroyBlockSplit
	.hidden	BrotliStoreMetaBlock
	.hidden	BrotliBuildMetaBlockGreedy
	.hidden	BrotliInitBlockSplit
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
