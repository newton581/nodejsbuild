	.file	"entropy_encode.c"
	.text
	.p2align 4
	.globl	BrotliSetDepth
	.hidden	BrotliSetDepth
	.type	BrotliSetDepth, @function
BrotliSetDepth:
.LFB49:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-1, -80(%rbp)
.L2:
	movslq	%edi, %rdi
	leaq	(%rsi,%rdi,8), %r8
	movswl	4(%r8), %edi
	testw	%di, %di
	js	.L3
	addl	$1, %eax
	cmpl	%ecx, %eax
	jg	.L8
	movswl	6(%r8), %r8d
	movslq	%eax, %r9
	movl	%r8d, -80(%rbp,%r9,4)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
.L1:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L41
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movswq	6(%r8), %rdi
	movb	%al, (%rdx,%rdi)
	testl	%eax, %eax
	js	.L38
	movslq	%eax, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L6
	leal	-1(%rax), %r9d
	testl	%eax, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-2(%rax), %r9d
	cmpl	$1, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-3(%rax), %r9d
	cmpl	$2, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-4(%rax), %r9d
	cmpl	$3, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-5(%rax), %r9d
	cmpl	$4, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-6(%rax), %r9d
	cmpl	$5, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-7(%rax), %r9d
	cmpl	$6, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-8(%rax), %r9d
	cmpl	$7, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-9(%rax), %r9d
	cmpl	$8, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-10(%rax), %r9d
	cmpl	$9, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-11(%rax), %r9d
	cmpl	$10, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-12(%rax), %r9d
	cmpl	$11, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-13(%rax), %r9d
	cmpl	$12, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	leal	-14(%rax), %r9d
	cmpl	$13, %eax
	je	.L38
	movslq	%r9d, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L37
	subl	$15, %eax
	cmpl	$-1, %eax
	je	.L38
	movslq	%eax, %r8
	movl	-80(%rbp,%r8,4), %edi
	cmpl	$-1, %edi
	jne	.L6
.L38:
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%r9d, %eax
.L6:
	movl	$-1, -80(%rbp,%r8,4)
	jmp	.L2
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE49:
	.size	BrotliSetDepth, .-BrotliSetDepth
	.p2align 4
	.globl	BrotliCreateHuffmanTree
	.hidden	BrotliCreateHuffmanTree
	.type	BrotliCreateHuffmanTree, @function
BrotliCreateHuffmanTree:
.LFB51:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -208(%rbp)
	movq	%rdi, -200(%rbp)
	movl	%edx, -188(%rbp)
	movq	%r8, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-208(%rbp), %rax
	movl	$1, -148(%rbp)
	testq	%rax, %rax
	je	.L99
.L211:
	movl	-148(%rbp), %esi
	movq	-200(%rbp), %rdi
	movq	%rax, %rdx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L45:
	subq	$1, %rdx
	movl	(%rdi,%rdx,4), %eax
	testl	%eax, %eax
	je	.L44
	cmpl	%esi, %eax
	leaq	(%r15,%r14,8), %rcx
	movl	$-1, %r8d
	cmovb	%esi, %eax
	movw	%r8w, 4(%rcx)
	addq	$1, %r14
	movw	%dx, 6(%rcx)
	movl	%eax, (%rcx)
.L44:
	testq	%rdx, %rdx
	jne	.L45
	cmpq	$1, %r14
	je	.L203
	leaq	0(,%r14,8), %rax
	leaq	(%r15,%rax), %rbx
	addq	$8, %rax
	movq	%rax, -176(%rbp)
	leaq	(%r14,%r14), %rax
	movq	%rax, -184(%rbp)
	leal	-1(%r14,%r14), %eax
	movq	%rbx, -168(%rbp)
	leaq	1(%r14), %rbx
	movl	%eax, -152(%rbp)
	leaq	-1(%r14), %rax
	movq	%rbx, -160(%rbp)
	movq	%rax, -144(%rbp)
	cmpq	$12, %r14
	jbe	.L204
	xorl	%eax, %eax
	cmpq	$57, %r14
	movq	%r15, -136(%rbp)
	setb	%al
	addl	%eax, %eax
	cltq
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	gaps.3006(%rip), %rax
	movq	(%rax,%r15,8), %rdi
	cmpq	%r14, %rdi
	jnb	.L84
	movq	-136(%rbp), %rax
	movq	%rdi, %rsi
	movq	%rdi, %r12
	negq	%rsi
	leaq	(%rax,%rdi,8), %r11
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L83:
	movl	(%r11), %r10d
	movzwl	4(%r11), %r13d
	leaq	(%rsi,%r11), %rax
	movq	%r11, %rcx
	movzwl	6(%r11), %ebx
	movq	%r12, %rdx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L206:
	setb	%r8b
	movzbl	%r8b, %r8d
	testl	%r8d, %r8d
	je	.L205
.L82:
	movq	(%rax), %r8
	addq	%rsi, %rax
	movq	%r8, (%rcx)
	addq	%rsi, %rcx
	cmpq	%rdx, %rdi
	ja	.L79
.L78:
	subq	%rdi, %rdx
	movq	%rax, %r9
	cmpl	(%rax), %r10d
	jne	.L206
	xorl	%r8d, %r8d
	cmpw	6(%rax), %bx
	setg	%r8b
	testl	%r8d, %r8d
	jne	.L82
.L205:
	movq	%rcx, %r9
.L79:
	addq	$1, %r12
	movl	%r10d, (%r9)
	addq	$8, %r11
	movw	%r13w, 4(%r9)
	movw	%bx, 6(%r9)
	cmpq	%r14, %r12
	jne	.L83
.L84:
	addq	$1, %r15
	cmpl	$5, %r15d
	jle	.L77
	movq	-136(%rbp), %r15
.L43:
	movq	-168(%rbp), %rax
	movq	-184(%rbp), %rbx
	movl	$-1, %r8d
	xorl	%ecx, %ecx
	movq	-160(%rbp), %r9
	movq	$-1, (%rax)
	movq	-176(%rbp), %rax
	leaq	(%r15,%rbx,8), %r11
	movq	$-1, (%r15,%rax)
	movq	%rbx, %rax
	subq	-144(%rbp), %rax
	leaq	(%r15,%rax,8), %rax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L207:
	movl	8(%r15,%rsi), %esi
	movl	%r8d, %ebx
	leaq	1(%rcx), %rdi
	movq	%rcx, %r8
	movq	%r9, %r10
	cmpl	%ebx, %esi
	ja	.L87
.L208:
	leaq	1(%rdi), %rcx
	movq	%r10, %r9
.L88:
	addl	%esi, %edx
	movl	$-1, %esi
	movw	%r8w, 4(%rax)
	addq	$8, %rax
	movl	%edx, -8(%rax)
	movl	$-1, %edx
	movw	%di, -2(%rax)
	movl	$-1, (%rax)
	movw	%dx, 4(%rax)
	movw	%si, 6(%rax)
	cmpq	%rax, %r11
	je	.L89
	movl	(%r15,%r9,8), %r8d
.L90:
	movl	(%r15,%rcx,8), %edx
	leaq	0(,%rcx,8), %rsi
	cmpl	%r8d, %edx
	jbe	.L207
	leaq	1(%r9), %r10
	movl	%edx, %esi
	movq	%rcx, %rdi
	movl	%r8d, %edx
	movl	(%r15,%r10,8), %ebx
	movq	%r9, %r8
	cmpl	%ebx, %esi
	jbe	.L208
.L87:
	movq	%rdi, %rcx
	leaq	1(%r10), %r9
	movl	%ebx, %esi
	movq	%r10, %rdi
	jmp	.L88
.L89:
	movl	$-1, -128(%rbp)
	movslq	-152(%rbp), %rdx
	xorl	%eax, %eax
.L91:
	leaq	(%r15,%rdx,8), %rcx
	movswq	4(%rcx), %rdx
	testw	%dx, %dx
	js	.L92
.L209:
	addl	$1, %eax
	cmpl	%eax, -188(%rbp)
	jl	.L93
	movswl	6(%rcx), %ecx
	movslq	%eax, %rsi
	movl	%ecx, -128(%rbp,%rsi,4)
	leaq	(%r15,%rdx,8), %rcx
	movswq	4(%rcx), %rdx
	testw	%dx, %dx
	jns	.L209
.L92:
	movswq	6(%rcx), %rdx
	movq	-216(%rbp), %rbx
	movb	%al, (%rbx,%rdx)
	testl	%eax, %eax
	js	.L42
	movslq	%eax, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L96
	leal	-1(%rax), %esi
	testl	%eax, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-2(%rax), %esi
	cmpl	$1, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-3(%rax), %esi
	cmpl	$2, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-4(%rax), %esi
	cmpl	$3, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-5(%rax), %esi
	cmpl	$4, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-6(%rax), %esi
	cmpl	$5, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-7(%rax), %esi
	cmpl	$6, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-8(%rax), %esi
	cmpl	$7, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-9(%rax), %esi
	cmpl	$8, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-10(%rax), %esi
	cmpl	$9, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-11(%rax), %esi
	cmpl	$10, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-12(%rax), %esi
	cmpl	$11, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-13(%rax), %esi
	cmpl	$12, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	leal	-14(%rax), %esi
	cmpl	$13, %eax
	je	.L42
	movslq	%esi, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L129
	subl	$15, %eax
	cmpl	$-1, %eax
	je	.L42
	movslq	%eax, %rcx
	movslq	-128(%rbp,%rcx,4), %rdx
	cmpl	$-1, %edx
	jne	.L96
.L42:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	movq	-208(%rbp), %rax
	sall	-148(%rbp)
	testq	%rax, %rax
	jne	.L211
.L99:
	movq	$0, -184(%rbp)
	movq	%r15, -168(%rbp)
	movq	$-1, -144(%rbp)
	movl	$-1, -152(%rbp)
	movq	$8, -176(%rbp)
	movq	$1, -160(%rbp)
	jmp	.L43
.L129:
	movl	%esi, %eax
.L96:
	movl	$-1, -128(%rbp,%rcx,4)
	jmp	.L91
.L204:
	cmpq	$1, %r14
	jbe	.L43
	movq	%r15, %r8
	movq	%rax, %r11
	jmp	.L74
.L212:
	xorl	%r10d, %r10d
	cmpw	6(%rax), %di
	setg	%r10b
.L50:
	testl	%r10d, %r10d
	je	.L51
	movq	(%rax), %rcx
	movq	%rcx, 8(%rax)
	testq	%rdx, %rdx
	je	.L114
	leaq	-8(%rax), %rcx
	cmpl	-8(%rax), %esi
	jne	.L52
	xorl	%r10d, %r10d
	cmpw	-2(%rax), %di
	setg	%r10b
	testl	%r10d, %r10d
	je	.L101
.L213:
	movq	-8(%rax), %r10
	movq	%r10, (%rax)
	cmpq	$1, %rdx
	je	.L114
	leaq	-16(%rax), %rbx
	cmpl	-16(%rax), %esi
	jne	.L54
	xorl	%r10d, %r10d
	cmpw	-10(%rax), %di
	setg	%r10b
.L55:
	testl	%r10d, %r10d
	je	.L51
	movq	-16(%rax), %rcx
	movq	%rcx, -8(%rax)
	cmpq	$2, %rdx
	je	.L114
	leaq	-24(%rax), %rcx
	cmpl	-24(%rax), %esi
	jne	.L56
	xorl	%r10d, %r10d
	cmpw	-18(%rax), %di
	setg	%r10b
	testl	%r10d, %r10d
	je	.L113
.L214:
	movq	-24(%rax), %r10
	movq	%r10, -16(%rax)
	cmpq	$3, %rdx
	je	.L114
	leaq	-32(%rax), %rbx
	cmpl	-32(%rax), %esi
	jne	.L58
	xorl	%r10d, %r10d
	cmpw	-26(%rax), %di
	setg	%r10b
.L59:
	testl	%r10d, %r10d
	je	.L51
	movq	-32(%rax), %rcx
	movq	%rcx, -24(%rax)
	cmpq	$4, %rdx
	je	.L114
	leaq	-40(%rax), %rcx
	cmpl	%esi, -40(%rax)
	jne	.L60
	xorl	%r10d, %r10d
	cmpw	%di, -34(%rax)
	setl	%r10b
.L61:
	testl	%r10d, %r10d
	je	.L113
	movq	-40(%rax), %r10
	movq	%r10, -32(%rax)
	cmpq	$5, %rdx
	je	.L114
	leaq	-48(%rax), %rbx
	cmpl	-48(%rax), %esi
	jne	.L62
	xorl	%r10d, %r10d
	cmpw	-42(%rax), %di
	setg	%r10b
.L63:
	testl	%r10d, %r10d
	je	.L51
	movq	-48(%rax), %rcx
	movq	%rcx, -40(%rax)
	cmpq	$6, %rdx
	je	.L114
	leaq	-56(%rax), %rcx
	cmpl	-56(%rax), %esi
	jne	.L64
	xorl	%r10d, %r10d
	cmpw	-50(%rax), %di
	setg	%r10b
.L65:
	testl	%r10d, %r10d
	je	.L113
	movq	-56(%rax), %r10
	movq	%r10, -48(%rax)
	cmpq	$7, %rdx
	je	.L114
	leaq	-64(%rax), %rbx
	cmpl	-64(%rax), %esi
	jne	.L66
	xorl	%r10d, %r10d
	cmpw	-58(%rax), %di
	setg	%r10b
.L67:
	testl	%r10d, %r10d
	je	.L51
	movq	-64(%rax), %rcx
	movq	%rcx, -56(%rax)
	cmpq	$8, %rdx
	je	.L114
	leaq	-72(%rax), %rcx
	cmpl	-72(%rax), %esi
	jne	.L68
	xorl	%r10d, %r10d
	cmpw	-66(%rax), %di
	setg	%r10b
.L69:
	testl	%r10d, %r10d
	je	.L113
	movq	-72(%rax), %r10
	movq	%r10, -64(%rax)
	cmpq	$9, %rdx
	je	.L114
	cmpl	%esi, -80(%rax)
	je	.L71
	seta	%r10b
	movzbl	%r10b, %r10d
.L72:
	testl	%r10d, %r10d
	je	.L51
	movq	-80(%rax), %rcx
	movq	%rcx, -72(%rax)
	movq	%r15, %rcx
.L51:
	addq	$1, %rdx
	movl	%esi, (%rcx)
	movw	%r9w, 4(%rcx)
	movw	%di, 6(%rcx)
	cmpq	%r11, %rdx
	je	.L43
.L74:
	movq	%r8, %rax
	addq	$8, %r8
	movl	8(%rax), %esi
	movzwl	12(%rax), %r9d
	movq	%r8, %rcx
	movzwl	14(%rax), %edi
	cmpl	(%rax), %esi
	je	.L212
	setb	%r10b
	movzbl	%r10b, %r10d
	jmp	.L50
.L52:
	setb	%r10b
	movzbl	%r10b, %r10d
	testl	%r10d, %r10d
	jne	.L213
.L101:
	movq	%rax, %rcx
	jmp	.L51
.L54:
	setb	%r10b
	movzbl	%r10b, %r10d
	jmp	.L55
.L56:
	setb	%r10b
	movzbl	%r10b, %r10d
	testl	%r10d, %r10d
	jne	.L214
.L113:
	movq	%rbx, %rcx
	jmp	.L51
.L114:
	movq	%r15, %rcx
	jmp	.L51
.L58:
	setb	%r10b
	movzbl	%r10b, %r10d
	jmp	.L59
.L60:
	seta	%r10b
	movzbl	%r10b, %r10d
	jmp	.L61
.L62:
	setb	%r10b
	movzbl	%r10b, %r10d
	jmp	.L63
.L64:
	setb	%r10b
	movzbl	%r10b, %r10d
	jmp	.L65
.L66:
	setb	%r10b
	movzbl	%r10b, %r10d
	jmp	.L67
.L68:
	setb	%r10b
	movzbl	%r10b, %r10d
	jmp	.L69
.L71:
	xorl	%r10d, %r10d
	cmpw	%di, -74(%rax)
	setl	%r10b
	jmp	.L72
.L203:
	movswq	6(%r15), %rax
	movq	-216(%rbp), %rbx
	movb	$1, (%rbx,%rax)
	jmp	.L42
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE51:
	.size	BrotliCreateHuffmanTree, .-BrotliCreateHuffmanTree
	.p2align 4
	.globl	BrotliOptimizeHuffmanCountsForRle
	.hidden	BrotliOptimizeHuffmanCountsForRle
	.type	BrotliOptimizeHuffmanCountsForRle, @function
BrotliOptimizeHuffmanCountsForRle:
.LFB55:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L335
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdi), %rax
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$3, %rax
	jbe	.L283
	movq	%rdi, %rdx
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movq	%rsi, %rax
	shrq	$2, %rdx
	movdqa	.LC0(%rip), %xmm4
	salq	$4, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L218:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	pcmpeqd	%xmm2, %xmm0
	pandn	%xmm4, %xmm0
	movdqa	%xmm0, %xmm3
	punpckhdq	%xmm2, %xmm0
	punpckldq	%xmm2, %xmm3
	paddq	%xmm3, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rdx, %rax
	jne	.L218
	movdqa	%xmm1, %xmm0
	movq	%rbx, %rdx
	psrldq	$8, %xmm0
	andq	$-4, %rdx
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rax
	testb	$3, %bl
	je	.L219
.L217:
	cmpl	$1, (%r12,%rdx,4)
	leaq	1(%rdx), %rcx
	sbbq	$-1, %rax
	cmpq	%rcx, %rbx
	jbe	.L219
	cmpl	$1, (%r12,%rcx,4)
	leaq	2(%rdx), %rcx
	sbbq	$-1, %rax
	cmpq	%rbx, %rcx
	jnb	.L219
	cmpl	$1, (%r12,%rcx,4)
	sbbq	$-1, %rax
	addq	$3, %rdx
	cmpq	%rdx, %rbx
	jbe	.L219
	cmpl	$1, (%r12,%rdx,4)
	sbbq	$-1, %rax
.L219:
	cmpq	$15, %rax
	ja	.L226
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L227:
	subq	$1, %rbx
	je	.L215
.L226:
	movl	-4(%r12,%rbx,4), %r9d
	testl	%r9d, %r9d
	je	.L227
	movl	$1073741824, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L229:
	movl	(%r12,%rax,4), %edx
	testl	%edx, %edx
	je	.L228
	addq	$1, %rsi
	cmpl	%edx, %ecx
	cmova	%edx, %ecx
.L228:
	addq	$1, %rax
	cmpq	%rax, %rbx
	ja	.L229
	cmpq	$4, %rsi
	jbe	.L215
	cmpl	$3, %ecx
	ja	.L232
	movq	%rbx, %rax
	subq	%rsi, %rax
	cmpq	$5, %rax
	ja	.L232
	leaq	-1(%rbx), %rax
	cmpq	$1, %rax
	jbe	.L232
	movq	%r12, %rax
	leaq	-8(%r12,%rbx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L234:
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	je	.L233
	movl	4(%rax), %edi
	testl	%edi, %edi
	jne	.L233
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.L233
	movl	$1, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L233:
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L234
.L232:
	cmpq	$27, %rsi
	ja	.L339
.L215:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r11, %rdi
	xorl	%r14d, %r14d
	call	memset@PLT
	movl	(%r12), %r15d
	leaq	1(%rbx), %r13
	xorl	%edx, %edx
	movq	%r13, -56(%rbp)
	movq	%r12, %r13
	movl	%r15d, %r12d
	movq	%rax, %r15
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L341:
	cmpq	$4, %rdx
	ja	.L237
	testl	%r12d, %r12d
	jne	.L328
.L242:
	cmpq	%r14, %rbx
	je	.L338
	movl	0(%r13,%r14,4), %r12d
.L338:
	movl	$1, %edx
	leaq	1(%r14), %rax
	cmpq	%r14, %rbx
	je	.L340
.L284:
	movq	%rax, %r14
.L243:
	cmpq	%r14, %rbx
	je	.L235
	cmpl	%r12d, 0(%r13,%r14,4)
	je	.L236
.L235:
	testl	%r12d, %r12d
	je	.L341
.L328:
	cmpq	$6, %rdx
	jbe	.L242
.L237:
	movq	%r14, %rdi
	movl	$1, %esi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memset@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L236:
	addq	$1, %rdx
	leaq	1(%r14), %rax
	cmpq	%r14, %rbx
	jne	.L284
.L340:
	movq	%r13, %r12
	movq	%r15, %r11
	movl	$2863311531, %r15d
	movq	-56(%rbp), %r13
	movl	4(%r12), %eax
	addl	(%r12), %eax
	leaq	-16(%r12), %r9
	leaq	-2(%rbx), %r14
	addl	8(%r12), %eax
	movq	%r13, %r10
	sall	$8, %eax
	imulq	%r15, %rax
	shrq	$33, %rax
	addl	$420, %eax
	cmpq	%r13, %rbx
	cmovbe	%rbx, %r10
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L343:
	testq	%r8, %r8
	je	.L245
	cmpb	$0, -1(%r11,%r8)
	jne	.L244
.L245:
	movl	16(%r9), %edx
	movl	%edx, %r13d
	sall	$8, %r13d
	movq	%r13, -56(%rbp)
	movl	$1240, %r13d
	subq	%rax, %r13
	addq	-56(%rbp), %r13
	cmpq	$2479, %r13
	ja	.L244
	addq	$1, %rcx
	addq	%rdx, %rdi
	cmpq	$3, %rcx
	jbe	.L259
	movq	%rdi, %rax
	movq	%rcx, %rdx
	shrq	%rdx
	salq	$8, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	$4, %rcx
	leaq	120(%rax), %rdx
	cmove	%rdx, %rax
.L259:
	addq	$4, %r9
	cmpq	%rsi, %r10
	jbe	.L342
.L260:
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%r8, %rbx
	je	.L244
	cmpb	$0, (%r11,%r8)
	je	.L343
.L244:
	cmpq	$3, %rcx
	ja	.L247
	jne	.L253
	testq	%rdi, %rdi
	je	.L248
.L253:
	cmpq	%r8, %r14
	ja	.L250
	movl	16(%r9), %edi
	movl	%edi, %eax
	sall	$8, %eax
.L258:
	movl	$1, %ecx
	addq	$4, %r9
	cmpq	%rsi, %r10
	ja	.L260
.L342:
	cmpq	%rsi, %rbx
	jb	.L215
	leaq	-16(%r12,%rsi,4), %r8
	xorl	%r13d, %r13d
	leaq	-2(%rbx), %r9
	movl	$2863311531, %r10d
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L344:
	cmpb	$0, -1(%r11,%rsi)
	jne	.L262
	movl	16(%r8), %edx
	movl	$1240, %r14d
	subq	%rax, %r14
	movl	%edx, %r15d
	sall	$8, %r15d
	addq	%r15, %r14
	cmpq	$2479, %r14
	ja	.L262
	addq	$1, %rcx
	addq	%rdx, %rdi
	cmpq	$3, %rcx
	jbe	.L263
	movq	%rcx, %rax
	movq	%rdi, %rdx
	salq	$8, %rdx
	shrq	%rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	$4, %rcx
	leaq	120(%rax), %rdx
	cmove	%rdx, %rax
.L263:
	addq	$1, %rsi
	addq	$4, %r8
	cmpq	%rbx, %rsi
	ja	.L215
.L276:
	cmpq	%rbx, %rsi
	je	.L262
	cmpb	$0, (%r11,%rsi)
	je	.L344
.L262:
	cmpq	$3, %rcx
	ja	.L264
	testq	%rdi, %rdi
	jne	.L271
	cmpq	$3, %rcx
	je	.L265
.L271:
	cmpq	%rsi, %r9
	ja	.L267
	xorl	%eax, %eax
.L268:
	movl	$1, %ecx
	xorl	%edi, %edi
	cmpq	%rbx, %rsi
	je	.L263
	movl	16(%r8), %edi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L250:
	movl	16(%r9), %edi
	movl	20(%r9), %eax
	addl	%edi, %eax
	addl	24(%r9), %eax
	sall	$8, %eax
	imulq	%r15, %rax
	shrq	$33, %rax
	addl	$420, %eax
	cmpq	%r8, %rbx
	jne	.L258
	movl	$1, %ecx
	xorl	%edi, %edi
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%rcx, %r13
	xorl	%edx, %edx
	shrq	%r13
	addq	%rdi, %r13
	movq	%r13, %rax
	divq	%rcx
	cmpq	%rcx, %r13
	movl	$1, %edx
	cmovb	%rdx, %rax
	testq	%rdi, %rdi
	movl	$0, %edi
	cmove	%rdi, %rax
.L280:
	leaq	-1(%rcx), %rdx
	cmpq	$2, %rdx
	jbe	.L285
	movq	%rcx, %rdx
	movd	%eax, %xmm5
	movq	%r9, %rdi
	shrq	$2, %rdx
	pshufd	$0, %xmm5, %xmm0
	salq	$4, %rdx
	subq	%rdx, %rdi
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L255:
	movups	%xmm0, (%rdx)
	subq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L255
	movq	%rcx, %rdx
	andq	$-4, %rdx
	testb	$3, %cl
	je	.L253
.L254:
	movq	%r8, %rdi
	subq	%rdx, %rdi
	movl	%eax, -4(%r12,%rdi,4)
	leaq	1(%rdx), %rdi
	cmpq	%rcx, %rdi
	jnb	.L253
	movq	%r8, %r13
	addq	$2, %rdx
	subq	%rdi, %r13
	movl	%eax, -4(%r12,%r13,4)
	cmpq	%rcx, %rdx
	jnb	.L253
	movq	%r8, %rcx
	subq	%rdx, %rcx
	movl	%eax, -4(%r12,%rcx,4)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L248:
	testq	%rcx, %rcx
	je	.L253
	xorl	%eax, %eax
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L267:
	movl	16(%r8), %eax
	addl	20(%r8), %eax
	addl	24(%r8), %eax
	sall	$8, %eax
	imulq	%r10, %rax
	shrq	$33, %rax
	addl	$420, %eax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%rcx, %r14
	xorl	%edx, %edx
	shrq	%r14
	addq	%rdi, %r14
	movq	%r14, %rax
	divq	%rcx
	cmpq	%rcx, %r14
	jb	.L345
	testq	%rdi, %rdi
	cmove	%r13, %rax
.L279:
	leaq	-1(%rcx), %rdx
	cmpq	$2, %rdx
	jbe	.L287
	movq	%rcx, %rdx
	movd	%eax, %xmm6
	movq	%r8, %rdi
	shrq	$2, %rdx
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rdx
	subq	%rdx, %rdi
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L273:
	movups	%xmm0, (%rdx)
	subq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L273
	movq	%rcx, %rdx
	andq	$-4, %rdx
	testb	$3, %cl
	je	.L271
.L272:
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	movl	%eax, -4(%r12,%rdi,4)
	leaq	1(%rdx), %rdi
	cmpq	%rcx, %rdi
	jnb	.L271
	movq	%rsi, %r15
	addq	$2, %rdx
	subq	%rdi, %r15
	movl	%eax, -4(%r12,%r15,4)
	cmpq	%rcx, %rdx
	jnb	.L271
	movq	%rsi, %rcx
	subq	%rdx, %rcx
	movl	%eax, -4(%r12,%rcx,4)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L265:
	testq	%rcx, %rcx
	je	.L271
	xorl	%eax, %eax
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L283:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L217
.L285:
	xorl	%edx, %edx
	jmp	.L254
.L287:
	xorl	%edx, %edx
	jmp	.L272
.L345:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	setne	%al
	jmp	.L279
	.cfi_endproc
.LFE55:
	.size	BrotliOptimizeHuffmanCountsForRle, .-BrotliOptimizeHuffmanCountsForRle
	.p2align 4
	.globl	BrotliWriteHuffmanTree
	.hidden	BrotliWriteHuffmanTree
	.type	BrotliWriteHuffmanTree, @function
BrotliWriteHuffmanTree:
.LFB57:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L441
	movq	%rsi, %r10
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L448:
	subq	$1, %r10
	je	.L447
.L349:
	cmpb	$0, -1(%rdi,%r10)
	je	.L448
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	cmpq	$50, %rsi
	jbe	.L360
	testq	%r10, %r10
	je	.L346
	movl	$1, %r12d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L359:
	leaq	(%rdi,%r9), %rbx
	leaq	1(%r9), %rax
	movzbl	(%rbx), %r11d
	cmpq	%r10, %rax
	jnb	.L351
	movl	$1, %eax
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L353:
	addq	$1, %rax
	leaq	(%rax,%r9), %rsi
	cmpq	%rsi, %r10
	jbe	.L352
.L350:
	cmpb	(%rbx,%rax), %r11b
	je	.L353
	leaq	(%rax,%r9), %rsi
.L352:
	movq	%rsi, %r9
	cmpq	$2, %rax
	jbe	.L401
	testb	%r11b, %r11b
	jne	.L401
	addq	%rax, %r14
	addq	$1, %r13
.L358:
	cmpq	%rsi, %r10
	ja	.L359
.L351:
	addq	%r12, %r12
	cmpq	%r15, %r12
	setb	%r12b
	addq	%r13, %r13
	cmpq	%r14, %r13
	movzbl	%r12b, %r12d
	setb	%r13b
	movzbl	%r13b, %r13d
.L360:
	xorl	%r11d, %r11d
	movl	$8, %r9d
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L361:
	testl	%r13d, %r13d
	jne	.L364
	movq	(%rdx), %rax
	movq	%r14, %r11
	movb	$0, (%rcx,%rax)
	movq	(%rdx), %rax
	movb	$0, (%r8,%rax)
	addq	$1, (%rdx)
.L365:
	cmpq	%r11, %r10
	jbe	.L346
.L389:
	leaq	(%rdi,%r11), %rsi
	leaq	1(%r11), %r14
	movzbl	(%rsi), %ebx
	testb	%bl, %bl
	je	.L361
	testl	%r12d, %r12d
	jne	.L449
	movq	(%rdx), %rsi
	movq	%r14, %r11
	xorl	%eax, %eax
	addq	%rcx, %rsi
	cmpb	%r9b, %bl
	je	.L450
.L379:
	movb	%bl, (%rsi)
	movq	(%rdx), %rsi
	movb	$0, (%r8,%rsi)
	addq	$1, (%rdx)
.L381:
	cmpq	$7, %rax
	jne	.L382
	movq	(%rdx), %rax
	movb	%bl, (%rcx,%rax)
	movq	(%rdx), %rax
	movb	$0, (%r8,%rax)
	movq	(%rdx), %rax
	leaq	1(%rax), %r9
	movq	%r9, (%rdx)
	movb	$16, 1(%rcx,%rax)
	movq	(%rdx), %rax
	movb	$3, (%r8,%rax)
	movq	(%rdx), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, (%rdx)
.L383:
	movq	%r9, %rax
	cmpq	%r9, %rsi
	jbe	.L399
	.p2align 4,,10
	.p2align 3
.L387:
	movzbl	(%rcx,%rax), %r14d
	movzbl	(%rcx,%rsi), %r15d
	movb	%r15b, (%rcx,%rax)
	addq	$1, %rax
	movb	%r14b, (%rcx,%rsi)
	subq	$1, %rsi
	cmpq	%rsi, %rax
	jb	.L387
	movq	(%rdx), %rax
	subq	$1, %rax
	cmpq	%rax, %r9
	jnb	.L399
	.p2align 4,,10
	.p2align 3
.L388:
	movzbl	(%r8,%r9), %esi
	movzbl	(%r8,%rax), %r14d
	movb	%r14b, (%r8,%r9)
	addq	$1, %r9
	movb	%sil, (%r8,%rax)
	subq	$1, %rax
	cmpq	%rax, %r9
	jb	.L388
.L399:
	movl	%ebx, %r9d
.L451:
	cmpq	%r11, %r10
	ja	.L389
.L346:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	$2, %rax
	ja	.L384
	movl	%ebx, %r9d
	testq	%rax, %rax
	je	.L365
.L380:
	movq	(%rdx), %rsi
	xorl	%r9d, %r9d
.L385:
	movb	%bl, (%rcx,%rsi)
	movq	(%rdx), %rsi
	addq	$1, %r9
	movb	$0, (%r8,%rsi)
	movq	(%rdx), %rsi
	addq	$1, %rsi
	movq	%rsi, (%rdx)
	cmpq	%r9, %rax
	jne	.L385
	movl	%ebx, %r9d
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L384:
	movq	(%rdx), %r9
	subq	$3, %rax
	movb	$16, (%rcx,%r9)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L386:
	movb	$16, (%rcx,%r14)
	subq	$1, %rax
.L446:
	movq	(%rdx), %rsi
	movl	%eax, %r14d
	andl	$3, %r14d
	shrq	$2, %rax
	movb	%r14b, (%r8,%rsi)
	movq	(%rdx), %rsi
	leaq	1(%rsi), %r14
	movq	%r14, (%rdx)
	jne	.L386
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L401:
	cmpq	$3, %rax
	jbe	.L358
	testb	%r11b, %r11b
	je	.L358
	addq	%rax, %r15
	addq	$1, %r12
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$1, %eax
	cmpq	%r14, %r10
	ja	.L368
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L370:
	addq	$1, %rax
	leaq	(%rax,%r11), %r14
	cmpq	%r14, %r10
	jbe	.L369
.L368:
	cmpb	%bl, (%rsi,%rax)
	je	.L370
.L369:
	testb	%bl, %bl
	jne	.L371
	cmpq	$11, %rax
	jne	.L372
	movq	(%rdx), %rax
	addq	$11, %r11
	movb	$0, (%rcx,%rax)
	movq	(%rdx), %rax
	movb	$0, (%r8,%rax)
	movq	(%rdx), %rax
	leaq	1(%rax), %rbx
	movq	%rbx, (%rdx)
	movb	$17, 1(%rcx,%rax)
	movq	(%rdx), %rax
	movb	$7, (%r8,%rax)
	movq	(%rdx), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, (%rdx)
.L373:
	movq	%rbx, %rax
	cmpq	%rbx, %rsi
	jbe	.L365
	.p2align 4,,10
	.p2align 3
.L377:
	movzbl	(%rcx,%rax), %r14d
	movzbl	(%rcx,%rsi), %r15d
	movb	%r15b, (%rcx,%rax)
	addq	$1, %rax
	movb	%r14b, (%rcx,%rsi)
	subq	$1, %rsi
	cmpq	%rsi, %rax
	jb	.L377
	movq	(%rdx), %rax
	subq	$1, %rax
	cmpq	%rax, %rbx
	jnb	.L365
	.p2align 4,,10
	.p2align 3
.L378:
	movzbl	(%r8,%rbx), %esi
	movzbl	(%r8,%rax), %r14d
	movb	%r14b, (%r8,%rbx)
	addq	$1, %rbx
	movb	%sil, (%r8,%rax)
	subq	$1, %rax
	cmpq	%rax, %rbx
	jb	.L378
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L371:
	addq	%rax, %r11
	cmpb	%r9b, %bl
	je	.L381
	movq	(%rdx), %rsi
	subq	$1, %rax
	addq	%rcx, %rsi
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L364:
	movl	$1, %eax
	cmpq	%r14, %r10
	ja	.L368
.L367:
	movq	(%rdx), %rsi
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L375:
	movb	$0, (%rcx,%rsi)
	movq	(%rdx), %rsi
	addq	$1, %r11
	movb	$0, (%r8,%rsi)
	movq	(%rdx), %rbx
	leaq	1(%rbx), %rsi
	movq	%rsi, (%rdx)
	cmpq	%rax, %r11
	jne	.L375
	movq	%r14, %r11
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L450:
	movb	%r9b, (%rsi)
	movq	(%rdx), %rax
	movb	$0, (%r8,%rax)
	addq	$1, (%rdx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L372:
	addq	%rax, %r11
	cmpq	$2, %rax
	jbe	.L453
	movq	(%rdx), %rbx
	subq	$3, %rax
	movb	$17, (%rcx,%rbx)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L376:
	movb	$17, (%rcx,%r14)
	subq	$1, %rax
.L444:
	movq	(%rdx), %rsi
	movl	%eax, %r14d
	andl	$7, %r14d
	shrq	$3, %rax
	movb	%r14b, (%r8,%rsi)
	movq	(%rdx), %rsi
	leaq	1(%rsi), %r14
	movq	%r14, (%rdx)
	jne	.L376
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L452:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	%r9b, %bl
	je	.L396
	movq	(%rdx), %rsi
	movq	%r14, %r11
	xorl	%eax, %eax
	addq	%rcx, %rsi
	jmp	.L379
.L396:
	movq	%r14, %r11
	jmp	.L380
.L453:
	testq	%rax, %rax
	je	.L365
	movq	%r11, %r14
	jmp	.L367
	.cfi_endproc
.LFE57:
	.size	BrotliWriteHuffmanTree, .-BrotliWriteHuffmanTree
	.p2align 4
	.globl	BrotliConvertBitDepthsToSymbols
	.hidden	BrotliConvertBitDepthsToSymbols
	.type	BrotliConvertBitDepthsToSymbols, @function
BrotliConvertBitDepthsToSymbols:
.LFB59:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rsi, %rsi
	je	.L455
	movq	%rdi, %rax
	leaq	(%rsi,%rdi), %r8
	.p2align 4,,10
	.p2align 3
.L456:
	movzbl	(%rax), %ecx
	addq	$1, %rax
	addw	$1, -96(%rbp,%rcx,2)
	cmpq	%r8, %rax
	jne	.L456
.L455:
	xorl	%eax, %eax
	movzwl	-92(%rbp), %ecx
	movw	%ax, -62(%rbp)
	movzwl	-94(%rbp), %eax
	addl	%eax, %eax
	addl	%eax, %ecx
	movw	%ax, -60(%rbp)
	leal	(%rcx,%rcx), %eax
	movzwl	-90(%rbp), %ecx
	movw	%ax, -58(%rbp)
	addl	%eax, %ecx
	movzwl	-88(%rbp), %eax
	addl	%ecx, %ecx
	addl	%ecx, %eax
	movw	%cx, -56(%rbp)
	leal	(%rax,%rax), %ecx
	movzwl	-86(%rbp), %eax
	movw	%cx, -54(%rbp)
	addl	%ecx, %eax
	movzwl	-84(%rbp), %ecx
	addl	%eax, %eax
	addl	%eax, %ecx
	movw	%ax, -52(%rbp)
	leal	(%rcx,%rcx), %eax
	movzwl	-82(%rbp), %ecx
	movw	%ax, -50(%rbp)
	addl	%eax, %ecx
	movzwl	-80(%rbp), %eax
	addl	%ecx, %ecx
	addl	%ecx, %eax
	movw	%cx, -48(%rbp)
	leal	(%rax,%rax), %ecx
	movzwl	-78(%rbp), %eax
	movw	%cx, -46(%rbp)
	addl	%ecx, %eax
	movzwl	-76(%rbp), %ecx
	addl	%eax, %eax
	addl	%eax, %ecx
	movw	%ax, -44(%rbp)
	leal	(%rcx,%rcx), %eax
	movzwl	-74(%rbp), %ecx
	movw	%ax, -42(%rbp)
	addl	%eax, %ecx
	movzwl	-72(%rbp), %eax
	addl	%ecx, %ecx
	addl	%ecx, %eax
	movw	%cx, -40(%rbp)
	leal	(%rax,%rax), %ecx
	movzwl	-70(%rbp), %eax
	movw	%cx, -38(%rbp)
	addl	%ecx, %eax
	movzwl	-68(%rbp), %ecx
	addl	%eax, %eax
	movw	%ax, -36(%rbp)
	addl	%ecx, %eax
	addl	%eax, %eax
	movw	%ax, -34(%rbp)
	testq	%rsi, %rsi
	je	.L454
	xorl	%r10d, %r10d
	leaq	kLut.3202(%rip), %rbx
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L458:
	addq	$1, %r10
	cmpq	%r10, %rsi
	je	.L454
.L461:
	movzbl	(%rdi,%r10), %r9d
	testb	%r9b, %r9b
	je	.L458
	movzbl	%r9b, %eax
	movzbl	%r9b, %ecx
	movzwl	-64(%rbp,%rax,2), %r8d
	movq	%rax, %r12
	leal	1(%r8), %r11d
	movw	%r11w, -64(%rbp,%rax,2)
	movq	%r8, %rax
	andl	$15, %eax
	movq	(%rbx,%rax,8), %rax
	cmpb	$4, %r9b
	jbe	.L459
	movl	$4, %r9d
	.p2align 4,,10
	.p2align 3
.L460:
	shrw	$4, %r8w
	addq	$4, %r9
	salq	$4, %rax
	movq	%r8, %r11
	andl	$15, %r11d
	orq	(%rbx,%r11,8), %rax
	cmpq	%r9, %r12
	ja	.L460
.L459:
	negl	%ecx
	andl	$3, %ecx
	shrq	%cl, %rax
	movw	%ax, (%rdx,%r10,2)
	addq	$1, %r10
	cmpq	%r10, %rsi
	jne	.L461
.L454:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L476
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L476:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE59:
	.size	BrotliConvertBitDepthsToSymbols, .-BrotliConvertBitDepthsToSymbols
	.section	.rodata
	.align 32
	.type	kLut.3202, @object
	.size	kLut.3202, 128
kLut.3202:
	.quad	0
	.quad	8
	.quad	4
	.quad	12
	.quad	2
	.quad	10
	.quad	6
	.quad	14
	.quad	1
	.quad	9
	.quad	5
	.quad	13
	.quad	3
	.quad	11
	.quad	7
	.quad	15
	.align 32
	.type	gaps.3006, @object
	.size	gaps.3006, 48
gaps.3006:
	.quad	132
	.quad	57
	.quad	23
	.quad	10
	.quad	4
	.quad	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1
	.long	1
	.long	1
	.long	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
