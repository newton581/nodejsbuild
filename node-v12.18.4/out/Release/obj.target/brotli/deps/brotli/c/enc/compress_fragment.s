	.file	"compress_fragment.c"
	.text
	.p2align 4
	.type	ShouldMergeBlock, @function
ShouldMergeBlock:
.LFB112:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movl	$256, %ecx
	pxor	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-2096(%rbp), %r14
	movq	%rdx, %r13
	movabsq	$-4718934530483838785, %rdx
	pushq	%r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$2096, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	rep stosq
	leaq	42(%rsi), %rdi
	movq	%rdi, %rax
	mulq	%rdx
	xorl	%eax, %eax
	shrq	$5, %rdx
	cvtsi2sdq	%rdx, %xmm1
	testq	%rsi, %rsi
	je	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	movzbl	(%r8,%rax), %ecx
	addq	$43, %rax
	addq	$1, -2096(%rbp,%rcx,8)
	cmpq	%rax, %rsi
	ja	.L2
	cmpq	$11007, %rdi
	jbe	.L4
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -2104(%rbp)
	call	log2@PLT
	movsd	-2104(%rbp), %xmm1
	movapd	%xmm0, %xmm3
	jmp	.L3
.L4:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm3, %xmm3
	cvtss2sd	(%rax,%rdx,4), %xmm3
.L3:
	addsd	.LC0(%rip), %xmm3
	xorl	%ebx, %ebx
	leaq	kLog2Table(%rip), %r12
	mulsd	%xmm1, %xmm3
	addsd	.LC1(%rip), %xmm3
	.p2align 4,,10
	.p2align 3
.L10:
	movq	(%r14,%rbx,8), %rdx
	testq	%rdx, %rdx
	js	.L5
.L23:
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm2
.L6:
	movzbl	0(%r13,%rbx), %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	cmpq	$255, %rdx
	ja	.L7
	pxor	%xmm0, %xmm0
	addq	$1, %rbx
	cvtss2sd	(%r12,%rdx,4), %xmm0
	addsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm3
	cmpq	$256, %rbx
	jne	.L10
.L9:
	xorl	%eax, %eax
	comisd	.LC2(%rip), %xmm3
	setnb	%al
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L22
	addq	$2096, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movapd	%xmm2, %xmm0
	movsd	%xmm3, -2120(%rbp)
	addq	$1, %rbx
	movsd	%xmm1, -2112(%rbp)
	movsd	%xmm2, -2104(%rbp)
	call	log2@PLT
	movsd	-2112(%rbp), %xmm1
	movsd	-2104(%rbp), %xmm2
	movsd	-2120(%rbp), %xmm3
	addsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm3
	cmpq	$256, %rbx
	je	.L9
	movq	(%r14,%rbx,8), %rdx
	testq	%rdx, %rdx
	jns	.L23
.L5:
	movq	%rdx, %rax
	movq	%rdx, %rcx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %ecx
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm2
	addsd	%xmm1, %xmm2
	jmp	.L6
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE112:
	.size	ShouldMergeBlock, .-ShouldMergeBlock
	.p2align 4
	.type	BuildAndStoreLiteralPrefixCode, @function
BuildAndStoreLiteralPrefixCode:
.LFB101:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-1072(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	movl	$128, %ecx
	subq	$1048, %rsp
	movq	16(%rbp), %r11
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	rep stosq
	cmpq	$32767, %rdx
	ja	.L35
	testq	%rdx, %rdx
	je	.L26
	movq	%rsi, %rax
	leaq	(%rsi,%rdx), %rsi
	.p2align 4,,10
	.p2align 3
.L27:
	movzbl	(%rax), %ecx
	addq	$1, %rax
	addl	$1, -1072(%rbp,%rcx,4)
	cmpq	%rsi, %rax
	jne	.L27
.L26:
	leaq	-48(%rbp), %rcx
	pxor	%xmm5, %xmm5
	pxor	%xmm6, %xmm6
	movq	%r12, %rax
	movdqa	.LC3(%rip), %xmm3
	movdqa	.LC4(%rip), %xmm2
	movdqa	%xmm3, %xmm8
	psubd	%xmm2, %xmm8
	.p2align 4,,10
	.p2align 3
.L28:
	movdqa	(%rax), %xmm4
	movdqa	%xmm3, %xmm7
	addq	$16, %rax
	movdqa	%xmm4, %xmm1
	psubd	%xmm2, %xmm1
	pcmpgtd	%xmm8, %xmm1
	pand	%xmm1, %xmm7
	pandn	%xmm4, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm7, %xmm0
	pslld	$1, %xmm0
	movdqa	%xmm0, %xmm1
	paddd	%xmm0, %xmm4
	punpckldq	%xmm6, %xmm0
	punpckhdq	%xmm6, %xmm1
	movaps	%xmm4, -16(%rax)
	paddq	%xmm1, %xmm0
	paddq	%xmm0, %xmm5
	cmpq	%rax, %rcx
	jne	.L28
	movdqa	%xmm5, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm5
	movq	%xmm5, %rax
	leaq	(%rax,%rdx), %r13
	jmp	.L31
.L35:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L25:
	movzbl	(%rsi,%rax), %ecx
	addq	$29, %rax
	addl	$1, -1072(%rbp,%rcx,4)
	cmpq	%rax, %rdx
	ja	.L25
	leaq	28(%rdx), %rcx
	pxor	%xmm5, %xmm5
	movabsq	$5088756985850910791, %rdx
	movdqa	.LC3(%rip), %xmm3
	movq	%rcx, %rax
	movdqa	.LC4(%rip), %xmm2
	sarq	$63, %rcx
	pxor	%xmm6, %xmm6
	movdqa	.LC5(%rip), %xmm8
	imulq	%rdx
	movdqa	%xmm3, %xmm7
	movq	%r12, %rax
	psubd	%xmm2, %xmm7
	sarq	$3, %rdx
	movq	%rdx, %r13
	leaq	-48(%rbp), %rdx
	subq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L30:
	movdqa	(%rax), %xmm4
	movdqa	%xmm3, %xmm9
	addq	$16, %rax
	movdqa	%xmm4, %xmm1
	psubd	%xmm2, %xmm1
	pcmpgtd	%xmm7, %xmm1
	pand	%xmm1, %xmm9
	pandn	%xmm4, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm9, %xmm0
	pslld	$1, %xmm0
	paddd	%xmm8, %xmm0
	movdqa	%xmm0, %xmm1
	paddd	%xmm0, %xmm4
	punpckldq	%xmm6, %xmm0
	punpckhdq	%xmm6, %xmm1
	movaps	%xmm4, -16(%rax)
	paddq	%xmm1, %xmm0
	paddq	%xmm0, %xmm5
	cmpq	%rdx, %rax
	jne	.L30
	movdqa	%xmm5, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm5
	movq	%xmm5, %rax
	addq	%rax, %r13
.L31:
	pushq	%r11
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$8, %ecx
	pushq	%r9
	movq	%r10, %rdi
	movq	%r8, %r9
	movq	%rbx, %r8
	call	BrotliBuildAndStoreHuffmanTreeFast
	popq	%rax
	xorl	%esi, %esi
	popq	%rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%r12,%rax,4), %edx
	testl	%edx, %edx
	je	.L32
	movzbl	(%rbx,%rax), %ecx
	imull	%ecx, %edx
	addq	%rdx, %rsi
.L32:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L33
	movq	%rsi, %rax
	xorl	%edx, %edx
	salq	$5, %rax
	subq	%rsi, %rax
	leaq	(%rsi,%rax,4), %rax
	divq	%r13
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L49
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L49:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE101:
	.size	BuildAndStoreLiteralPrefixCode, .-BuildAndStoreLiteralPrefixCode
	.p2align 4
	.type	BuildAndStoreCommandPrefixCode, @function
BuildAndStoreCommandPrefixCode:
.LFB102:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movl	$86, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1808(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-752(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$15, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1928, %rsp
	movq	%r8, -1960(%rbp)
	movq	%rsi, %r8
	movl	$64, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -768(%rbp)
	rep stosq
	movq	%r13, %rdi
	movq	%r14, %rcx
	call	BrotliCreateHuffmanTree
	leaq	64(%rbx), %r10
	movq	%r14, %rcx
	leaq	256(%r13), %rdi
	movq	%r10, %r8
	movl	$14, %edx
	movl	$64, %esi
	movq	%r10, -1944(%rbp)
	call	BrotliCreateHuffmanTree
	movq	16(%rbx), %rax
	movdqu	(%rbx), %xmm3
	leaq	-768(%rbp), %r13
	movdqu	40(%rbx), %xmm1
	movdqu	24(%rbx), %xmm4
	movl	$64, %esi
	movq	%r13, %rdi
	movq	%rax, -752(%rbp)
	movq	56(%rbx), %rax
	leaq	-1936(%rbp), %rdx
	movdqa	%xmm1, %xmm2
	punpckhqdq	%xmm4, %xmm1
	movaps	%xmm3, -768(%rbp)
	punpcklqdq	%xmm4, %xmm2
	movups	%xmm1, -728(%rbp)
	movups	%xmm2, -744(%rbp)
	movq	%rax, -712(%rbp)
	call	BrotliConvertBitDepthsToSymbols
	movdqa	-1936(%rbp), %xmm6
	movdqa	-1920(%rbp), %xmm7
	leaq	128(%r12), %rdx
	movdqa	-1904(%rbp), %xmm3
	movq	-1944(%rbp), %r10
	movl	$64, %esi
	movdqa	-1872(%rbp), %xmm4
	movdqa	-1840(%rbp), %xmm5
	movups	%xmm6, (%r12)
	movups	%xmm7, 16(%r12)
	movdqa	-1888(%rbp), %xmm6
	movdqa	-1856(%rbp), %xmm7
	movq	%r10, %rdi
	movups	%xmm3, 32(%r12)
	movdqa	-1824(%rbp), %xmm3
	movups	%xmm4, 48(%r12)
	movups	%xmm5, 64(%r12)
	movups	%xmm6, 80(%r12)
	movups	%xmm7, 96(%r12)
	movups	%xmm3, 112(%r12)
	movq	%r10, -1952(%rbp)
	call	BrotliConvertBitDepthsToSymbols
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movups	%xmm0, -760(%rbp)
	movq	%r14, %rdx
	movl	$704, %esi
	movq	%r13, %rdi
	movq	%rax, -768(%rbp)
	movq	8(%rbx), %rax
	movups	%xmm0, -744(%rbp)
	movq	%rax, -704(%rbp)
	movq	16(%rbx), %rax
	movups	%xmm0, -728(%rbp)
	movq	%rax, -640(%rbp)
	movq	24(%rbx), %rax
	movq	$0, -712(%rbp)
	movq	%rax, -576(%rbp)
	movq	32(%rbx), %rax
	movq	%rax, -384(%rbp)
	movzbl	40(%rbx), %eax
	movb	%al, -640(%rbp)
	movzbl	48(%rbx), %eax
	movb	%al, -512(%rbp)
	movzbl	56(%rbx), %eax
	movb	%al, -320(%rbp)
	movzbl	41(%rbx), %eax
	movb	%al, -632(%rbp)
	movzbl	49(%rbx), %eax
	movb	%al, -504(%rbp)
	movzbl	57(%rbx), %eax
	movb	%al, -312(%rbp)
	movzbl	42(%rbx), %eax
	movb	%al, -624(%rbp)
	movzbl	50(%rbx), %eax
	movb	%al, -496(%rbp)
	movzbl	58(%rbx), %eax
	movb	%al, -304(%rbp)
	movzbl	43(%rbx), %eax
	movq	-1960(%rbp), %r9
	movb	%al, -616(%rbp)
	movzbl	51(%rbx), %eax
	movq	%r9, %r8
	movq	%r9, -1944(%rbp)
	movb	%al, -488(%rbp)
	movzbl	59(%rbx), %eax
	movb	%al, -296(%rbp)
	movzbl	44(%rbx), %eax
	movb	%al, -608(%rbp)
	movzbl	52(%rbx), %eax
	movb	%al, -480(%rbp)
	movzbl	60(%rbx), %eax
	movb	%al, -288(%rbp)
	movzbl	45(%rbx), %eax
	movb	%al, -600(%rbp)
	movzbl	53(%rbx), %eax
	movb	%al, -472(%rbp)
	movzbl	61(%rbx), %eax
	movb	%al, -280(%rbp)
	movzbl	46(%rbx), %eax
	movb	%al, -592(%rbp)
	movzbl	54(%rbx), %eax
	movb	%al, -464(%rbp)
	movzbl	62(%rbx), %eax
	movb	%al, -272(%rbp)
	movzbl	47(%rbx), %eax
	movb	%al, -584(%rbp)
	movzbl	55(%rbx), %eax
	movb	%al, -456(%rbp)
	movzbl	63(%rbx), %eax
	movb	%al, -264(%rbp)
	call	BrotliStoreHuffmanTree
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$64, %esi
	movq	-1944(%rbp), %r9
	movq	-1952(%rbp), %r10
	movq	%r9, %r8
	movq	%r10, %rdi
	call	BrotliStoreHuffmanTree
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$1928, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE102:
	.size	BuildAndStoreCommandPrefixCode, .-BuildAndStoreCommandPrefixCode
	.p2align 4
	.type	BrotliStoreMetaBlockHeader.constprop.0, @function
BrotliStoreMetaBlockHeader.constprop.0:
.LFB121:
	.cfi_startproc
	movq	(%rsi), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	addq	%rdx, %rcx
	movzbl	(%rcx), %r8d
	movq	%r8, (%rcx)
	leaq	1(%rax), %r8
	movq	%r8, (%rsi)
	cmpq	$65536, %rdi
	jbe	.L57
	movl	%r8d, %ecx
	andl	$7, %ecx
	cmpq	$1048576, %rdi
	ja	.L56
	movl	$1, %r10d
	movl	$20, %r9d
	salq	%cl, %r10
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$16, %r9d
	xorl	%r10d, %r10d
.L55:
	shrq	$3, %r8
	addq	$3, %rax
	subq	$1, %rdi
	addq	%rdx, %r8
	movq	%rax, (%rsi)
	movzbl	(%r8), %ecx
	orq	%r10, %rcx
	movq	%rcx, (%r8)
	movq	%rax, %r8
	movl	%eax, %ecx
	addq	%r9, %rax
	shrq	$3, %r8
	andl	$7, %ecx
	movq	%rax, (%rsi)
	addq	%rdx, %r8
	salq	%cl, %rdi
	movq	%rax, %rcx
	addq	$1, %rax
	movzbl	(%r8), %r10d
	shrq	$3, %rcx
	addq	%rcx, %rdx
	orq	%r10, %rdi
	movq	%rdi, (%r8)
	movzbl	(%rdx), %ecx
	movq	%rax, (%rsi)
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$2, %r10d
	movl	$24, %r9d
	salq	%cl, %r10
	jmp	.L55
	.cfi_endproc
.LFE121:
	.size	BrotliStoreMetaBlockHeader.constprop.0, .-BrotliStoreMetaBlockHeader.constprop.0
	.p2align 4
	.type	EmitUncompressedMetaBlock, @function
EmitUncompressedMetaBlock:
.LFB114:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	%rdi, %rsi
	movq	%rdx, %rax
	movq	%rdi, %r9
	shrq	$3, %rax
	addq	%r8, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	movq	%rdx, %rcx
	andl	$7, %ecx
	sall	%cl, %esi
	subq	$8, %rsp
	subl	$1, %esi
	andb	%sil, (%rax)
	movq	%rdx, (%rbx)
	movzbl	(%rax), %ecx
	movq	%rcx, (%rax)
	leaq	1(%rdx), %rax
	movq	%rax, (%rbx)
	cmpq	$65536, %r13
	jbe	.L61
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpq	$1048576, %r13
	ja	.L60
	movl	$1, %edi
	movl	$20, %esi
	salq	%cl, %rdi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$16, %esi
	xorl	%edi, %edi
.L59:
	shrq	$3, %rax
	addq	$3, %rdx
	addq	%r12, %rax
	movq	%rdx, (%rbx)
	movzbl	(%rax), %ecx
	orq	%rdi, %rcx
	movq	%rdx, %rdi
	shrq	$3, %rdi
	movq	%rcx, (%rax)
	movl	%edx, %ecx
	leaq	-1(%r13), %rax
	addq	%r12, %rdi
	andl	$7, %ecx
	addq	%rsi, %rdx
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rdx, (%rbx)
	movl	%edx, %esi
	andl	$7, %esi
	orq	%r8, %rax
	movq	%rax, (%rdi)
	movq	%rdx, %rax
	addq	$8, %rdx
	shrq	$3, %rax
	andl	$4294967288, %edx
	addq	%r12, %rax
	movzbl	(%rax), %ecx
	btsq	%rsi, %rcx
	movq	%r9, %rsi
	movq	%rcx, (%rax)
	movq	%rdx, (%rbx)
	shrq	$3, %rdx
	leaq	(%r12,%rdx), %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rax
	leaq	(%rax,%r13,8), %rax
	movq	%rax, (%rbx)
	shrq	$3, %rax
	movb	$0, (%r12,%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$2, %edi
	movl	$24, %esi
	salq	%cl, %rdi
	jmp	.L59
	.cfi_endproc
.LFE114:
	.size	EmitUncompressedMetaBlock, .-EmitUncompressedMetaBlock
	.p2align 4
	.type	BrotliCompressFragmentFastImpl11, @function
BrotliCompressFragmentFastImpl11:
.LFB117:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r12
	movq	%rdx, -1352(%rbp)
	movl	%ecx, -1524(%rbp)
	movq	40(%rbp), %rbx
	movq	%rax, -1424(%rbp)
	movq	32(%rbp), %rax
	movq	%r8, -1456(%rbp)
	movq	48(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r9, -1416(%rbp)
	movq	%rax, -1496(%rbp)
	movl	$98304, %eax
	movq	%rdi, -1472(%rbp)
	movq	%r12, -1544(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpq	$98304, %rdx
	movq	(%rbx), %rdi
	cmovbe	%rdx, %rax
	movq	%r15, %rdx
	addq	$3, %rdi
	movq	%rdi, -1448(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	(%rbx), %rax
	subq	$8, %rsp
	movq	%r14, %rsi
	leaq	-832(%rbp), %r8
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%rax, %rdx
	addq	$13, %rax
	movq	%r8, -1480(%rbp)
	shrq	$3, %rdx
	movq	%rax, (%rbx)
	leaq	-320(%rbp), %rax
	addq	%r15, %rdx
	movq	%rax, -1432(%rbp)
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	%r13, %rdx
	movq	%rax, %rcx
	pushq	%r15
	call	BuildAndStoreLiteralPrefixCode
	movq	(%r12), %rsi
	popq	%rdi
	movq	(%rbx), %rdx
	popq	%r8
	movq	%rax, -1464(%rbp)
	cmpq	$7, %rsi
	jbe	.L66
	movq	-1496(%rbp), %r9
	xorl	%edi, %edi
	movq	%r12, %r8
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rdx, %rsi
	movq	%rdi, %rax
	movl	%edx, %ecx
	addq	$8, %rdx
	shrq	$3, %rsi
	shrq	$3, %rax
	andl	$7, %ecx
	movzbl	(%r9,%rax), %eax
	addq	%r15, %rsi
	movq	%rdx, (%rbx)
	movzbl	(%rsi), %r10d
	salq	%cl, %rax
	orq	%r10, %rax
	movq	%rax, (%rsi)
	movq	%rdi, %rax
	movq	(%r8), %rsi
	addq	$8, %rdi
	addq	$15, %rax
	cmpq	%rax, %rsi
	ja	.L65
.L66:
	movq	-1496(%rbp), %rcx
	movq	%rdx, %rdi
	movq	%rsi, %rax
	andl	$7, %esi
	shrq	$3, %rdi
	shrq	$3, %rax
	movq	%r14, -1392(%rbp)
	movq	%r15, %r12
	movzbl	(%rcx,%rax), %eax
	addq	%r15, %rdi
	movl	%edx, %ecx
	addq	%rsi, %rdx
	movzbl	(%rdi), %r8d
	andl	$7, %ecx
	movq	%rdx, (%rbx)
	movq	%r14, %r15
	salq	%cl, %rax
	movq	%r13, -1384(%rbp)
	orq	%r8, %rax
	movq	%r14, -1440(%rbp)
	movq	%rax, (%rdi)
	leaq	-1344(%rbp), %rax
	movq	%rax, -1376(%rbp)
	movq	%r14, -1368(%rbp)
	movq	%r13, %r14
	movq	%rbx, %r13
	movq	-1392(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	(%rbx,%r14), %rax
	movq	-1376(%rbp), %rdi
	movl	$64, %ecx
	leaq	kCmdHistoSeed(%rip), %rsi
	rep movsq
	movq	%rax, -1360(%rbp)
	cmpq	$15, %r14
	jbe	.L68
	movq	-1352(%rbp), %rax
	leaq	-5(%r14), %rdx
	movabsq	$8503243848024064, %rsi
	movq	%r14, -1408(%rbp)
	movl	$-1, -1400(%rbp)
	movq	-1456(%rbp), %r14
	subq	$16, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	leaq	(%rbx,%rax), %r11
	movq	%rbx, %rax
	leaq	1(%rbx), %rbx
	imulq	1(%rax), %rsi
	leaq	1(%rbx), %rdi
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	shrq	$53, %rsi
	cmpq	%rdi, %r11
	jb	.L212
.L222:
	movslq	-1400(%rbp), %r8
	movl	$33, %ecx
	negq	%r8
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movslq	(%rdx), %rax
	movl	%r9d, (%rdx)
	addq	%r15, %rax
	movl	(%rax), %edx
	cmpl	%edx, (%rbx)
	je	.L219
.L72:
	leal	1(%rcx), %eax
	shrl	$5, %ecx
	movq	%rdi, %rbx
	addq	%rdi, %rcx
	cmpq	%rcx, %r11
	jb	.L212
	movq	%rcx, %rdi
	movl	%eax, %ecx
.L69:
	movl	%esi, %edx
	leaq	(%rbx,%r8), %rax
	movq	%rbx, %r9
	movabsq	$8503243848024064, %rsi
	imulq	(%rdi), %rsi
	movl	(%rax), %r10d
	leaq	(%r14,%rdx,4), %rdx
	subq	%r15, %r9
	shrq	$53, %rsi
	cmpl	%r10d, (%rbx)
	jne	.L70
	movzbl	4(%rax), %r10d
	cmpb	%r10b, 4(%rbx)
	jne	.L70
	cmpq	%rbx, %rax
	jnb	.L70
	movl	%r9d, (%rdx)
.L71:
	movq	%rbx, %rdx
	subq	%rax, %rdx
	cmpq	$262128, %rdx
	jg	.L72
	movq	-1360(%rbp), %rsi
	leaq	5(%rbx), %rcx
	leaq	5(%rax), %r9
	subq	%rbx, %rsi
	subq	$5, %rsi
	movq	%rsi, %r10
	shrq	$3, %r10
	je	.L73
	leaq	13(%rbx), %rcx
	xorl	%edi, %edi
	movq	%rcx, -1488(%rbp)
.L75:
	movq	5(%rbx,%rdi), %rcx
	movq	5(%rax,%rdi), %r8
	cmpq	%r8, %rcx
	je	.L220
	xorq	%r8, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rdi, %rax
	movq	%rax, -1488(%rbp)
.L76:
	movq	-1488(%rbp), %rax
	movq	%rbx, %rdi
	subq	-1368(%rbp), %rdi
	movq	%rdi, -1512(%rbp)
	addq	$5, %rax
	movq	%rax, -1520(%rbp)
	addq	%rbx, %rax
	movq	%rax, -1504(%rbp)
	cmpq	$6209, %rdi
	ja	.L79
	movq	(%r12), %rax
	movq	%rdi, %rbx
	movq	%rax, %rsi
	movl	%eax, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r13, %rsi
	cmpq	$5, %rdi
	ja	.L80
	movq	-1416(%rbp), %rcx
	movzbl	(%rsi), %r9d
	movzbl	40(%rcx,%rdi), %r8d
	movq	-1424(%rbp), %rdi
	movl	%r10d, %ecx
	movzwl	80(%rdi,%rbx,2), %edi
	addq	%r8, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdi
	orq	%rdi, %r9
	movq	-1376(%rbp), %rdi
	movq	%r9, (%rsi)
	addl	$1, 160(%rdi,%rbx,4)
	testq	%rbx, %rbx
	je	.L82
.L81:
	movq	%r11, -1536(%rbp)
	movq	-1512(%rbp), %rbx
	xorl	%r8d, %r8d
	movq	-1368(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L89:
	movzbl	(%r11,%r8), %ecx
	movq	%rax, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %esi
	addq	%r13, %rdi
	movl	%eax, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rsi
	addq	%r9, %rax
	orq	%rsi, %r10
	movq	%rax, (%r12)
	movq	%r10, (%rdi)
	cmpq	%r8, %rbx
	ja	.L89
	movq	-1536(%rbp), %r11
.L82:
	movq	%rax, %r10
	shrq	$3, %r10
	addq	%r13, %r10
	movzbl	(%r10), %ebx
	movq	%rbx, -1368(%rbp)
	movl	%eax, %ebx
	andl	$7, %ebx
	movl	%ebx, -1512(%rbp)
	cmpl	%edx, -1400(%rbp)
	je	.L221
	movslq	%edx, %rdi
	movq	-1416(%rbp), %rbx
	addq	$3, %rdi
	bsrl	%edi, %r8d
	movq	%rdi, %r9
	leal	-1(%r8), %esi
	leal	-4(%r8,%r8), %r8d
	movl	%esi, %ecx
	shrq	%cl, %r9
	movq	-1424(%rbp), %rcx
	andl	$1, %r9d
	addq	%r9, %r8
	addq	$2, %r9
	movzbl	80(%rbx,%r8), %ebx
	movq	%rbx, -1400(%rbp)
	addq	-1400(%rbp), %rax
	movzwl	160(%rcx,%r8,2), %ebx
	movzbl	-1512(%rbp), %ecx
	movq	%rax, (%r12)
	movl	%edx, -1400(%rbp)
	salq	%cl, %rbx
	movq	-1368(%rbp), %rcx
	orq	%rbx, %rcx
	movq	%rcx, (%r10)
	movq	%rax, %r10
	movl	%esi, %ecx
	shrq	$3, %r10
	salq	%cl, %r9
	movl	%eax, %ecx
	addq	%rsi, %rax
	addq	%r13, %r10
	subq	%r9, %rdi
	andl	$7, %ecx
	movq	%rax, (%r12)
	movzbl	(%r10), %ebx
	salq	%cl, %rdi
	orq	%rdi, %rbx
	movq	%rbx, (%r10)
	movq	-1376(%rbp), %rbx
	addl	$1, 320(%rbx,%r8,4)
.L91:
	movq	%rax, %rdx
	movq	-1520(%rbp), %rbx
	movl	%eax, %edi
	shrq	$3, %rdx
	andl	$7, %edi
	addq	%r13, %rdx
	movzbl	(%rdx), %esi
	cmpq	$11, %rbx
	ja	.L92
	movq	-1416(%rbp), %rcx
	movq	-1488(%rbp), %r10
	movzbl	1(%rcx,%r10), %r9d
	movq	-1424(%rbp), %rcx
	addq	%r9, %rax
	movzwl	-8(%rcx,%rbx,2), %r8d
	movl	%edi, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	salq	%cl, %r8
	orq	%r8, %rsi
	addl	$1, -16(%rax,%rbx,4)
	movq	%rsi, (%rdx)
.L93:
	movq	-1504(%rbp), %rbx
	cmpq	%rbx, %r11
	jbe	.L132
	movq	-3(%rbx), %rax
	movq	%rbx, %rcx
	movabsq	$8503243848024064, %rdx
	movabsq	$8503243848024064, %rdi
	subq	%r15, %rcx
	imulq	%rax, %rdx
	leal	-3(%rcx), %esi
	shrq	$53, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	leal	-2(%rcx), %esi
	shrq	$8, %rdx
	imulq	%rdi, %rdx
	shrq	$53, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	shrq	$24, %rax
	leal	-1(%rcx), %esi
	imulq	%rdi, %rax
	shrq	$16, %rdx
	imulq	%rdi, %rdx
	shrq	$53, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$53, %rdx
	movl	%esi, (%r14,%rdx,4)
	movslq	(%rax), %r10
	movq	%rbx, %rdx
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rbx)
	je	.L97
.L98:
	movq	-1504(%rbp), %rax
	movabsq	$8503243848024064, %rsi
	imulq	1(%rax), %rsi
	leaq	1(%rax), %rbx
	movq	%rax, -1368(%rbp)
	leaq	1(%rbx), %rdi
	shrq	$53, %rsi
	cmpq	%rdi, %r11
	jnb	.L222
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r13, %rax
	movq	-1408(%rbp), %r14
	movq	%r12, %r13
	movq	%rax, %r12
.L68:
	subq	%r14, -1352(%rbp)
	movq	-1352(%rbp), %rdi
	je	.L110
	cmpq	$65536, %rdi
	movl	$65536, %eax
	cmovbe	%rdi, %rax
	addq	%rax, -1384(%rbp)
	movq	%rax, %r14
	movq	-1384(%rbp), %rax
	cmpq	$1048576, %rax
	jbe	.L223
.L111:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	ja	.L127
.L128:
	movq	-1352(%rbp), %rax
	movl	$98304, %r14d
	movq	%r12, %rdx
	movq	%r13, %rsi
	cmpq	$98304, %rax
	cmovbe	%rax, %r14
	movq	0(%r13), %rax
	movq	%r14, %rdi
	addq	$3, %rax
	movq	%rax, -1448(%rbp)
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	0(%r13), %rax
	subq	$8, %rsp
	movq	%r13, %r9
	movq	-1360(%rbp), %rbx
	movq	-1480(%rbp), %r8
	movq	%rax, %rdx
	addq	$13, %rax
	movq	-1472(%rbp), %rdi
	shrq	$3, %rdx
	movq	%rax, 0(%r13)
	movq	%rbx, %rsi
	addq	%r12, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	-1432(%rbp), %rcx
	movq	%r14, %rdx
	pushq	%r12
	call	BuildAndStoreLiteralPrefixCode
	movq	-1424(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	-1416(%rbp), %rsi
	movq	-1376(%rbp), %rdi
	movq	%rax, -1464(%rbp)
	call	BuildAndStoreCommandPrefixCode
	popq	%rdx
	popq	%rcx
	movq	%r14, -1384(%rbp)
	movq	%rbx, -1440(%rbp)
	movq	%rbx, -1368(%rbp)
	movq	%rbx, -1392(%rbp)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L219:
	movzbl	4(%rax), %edx
	cmpb	%dl, 4(%rbx)
	jne	.L72
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	jbe	.L125
.L127:
	movq	-1360(%rbp), %rdx
	subq	-1368(%rbp), %rdx
	cmpq	$6209, %rdx
	ja	.L114
	movq	0(%r13), %rdi
	movq	%rdi, %rsi
	movl	%edi, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	cmpq	$5, %rdx
	ja	.L115
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	movzbl	40(%rax,%rdx), %r11d
	movq	-1424(%rbp), %rax
	movzwl	80(%rax,%rdx,2), %eax
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rax
	orq	%rax, %r9
	movq	-1376(%rbp), %rax
	movq	%r9, (%rsi)
	addl	$1, 160(%rax,%rdx,4)
	testq	%rdx, %rdx
	je	.L87
.L116:
	movq	-1368(%rbp), %r10
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L120:
	movzbl	(%r10,%r8), %ecx
	movq	%rdi, %rsi
	addq	$1, %r8
	shrq	$3, %rsi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rsi
	movl	%edi, %ecx
	movzbl	(%rsi), %r11d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rdi
	orq	%r11, %rax
	movq	%rdi, 0(%r13)
	movq	%rax, (%rsi)
	cmpq	%r8, %rdx
	ja	.L120
.L87:
	cmpq	$0, -1352(%rbp)
	jne	.L128
	.p2align 4,,10
	.p2align 3
.L125:
	movl	-1524(%rbp), %eax
	testl	%eax, %eax
	je	.L224
.L63:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	cmpq	$129, %rdx
	ja	.L117
	leaq	-2(%rdx), %r8
	movq	-1416(%rbp), %rbx
	bsrl	%r8d, %eax
	movq	%r8, %r11
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %r11
	leal	(%rax,%rax), %ecx
	leaq	42(%r11,%rcx), %r14
	movq	-1424(%rbp), %rcx
	movzbl	(%rbx,%r14), %ebx
	movq	%rbx, -1384(%rbp)
	movzwl	(%rcx,%r14,2), %ebx
	movl	%r10d, %ecx
	addq	-1384(%rbp), %rdi
	salq	%cl, %rbx
	movl	%eax, %ecx
	movq	%rdi, 0(%r13)
	orq	%rbx, %r9
	salq	%cl, %r11
	movl	%edi, %ecx
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	subq	%r11, %r8
	andl	$7, %ecx
	shrq	$3, %rsi
	addq	%rax, %rdi
	salq	%cl, %r8
	movq	-1376(%rbp), %rax
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	orq	%r8, %r9
	movq	%r9, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L223:
	movq	-1432(%rbp), %rdx
	movq	-1360(%rbp), %rdi
	movq	%r14, %rsi
	call	ShouldMergeBlock
	testl	%eax, %eax
	je	.L111
	movl	-1384(%rbp), %eax
	movl	$20, %edi
	movl	$1, %ebx
	movq	-1448(%rbp), %r10
	leal	-1(%rax), %r11d
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r10, %r9
	movl	$8, %eax
	movl	%ebx, %edx
	movl	%ebx, %esi
	andl	$7, %r9d
	movq	%r10, %r8
	subq	%r9, %rax
	cmpq	%rdi, %rax
	cmova	%rdi, %rax
	shrq	$3, %r8
	addq	%r12, %r8
	leaq	(%r9,%rax), %rcx
	addq	%rax, %r10
	sall	%cl, %edx
	movl	%r9d, %ecx
	sall	%cl, %esi
	negl	%edx
	movl	%esi, %ecx
	movl	$-1, %esi
	subl	$1, %ecx
	orl	%ecx, %edx
	movl	%eax, %ecx
	andb	(%r8), %dl
	sall	%cl, %esi
	movl	%r9d, %ecx
	notl	%esi
	andl	%r11d, %esi
	sall	%cl, %esi
	movl	%eax, %ecx
	orl	%edx, %esi
	shrl	%cl, %r11d
	movb	%sil, (%r8)
	subq	%rax, %rdi
	jne	.L112
	movq	-1360(%rbp), %rax
	movq	%rax, -1392(%rbp)
	movq	%rax, %rbx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L117:
	cmpq	$2113, %rdx
	ja	.L119
	leaq	-66(%rdx), %r8
	movq	-1416(%rbp), %rbx
	movl	%r10d, %ecx
	bsrl	%r8d, %eax
	leal	50(%rax), %r14d
	movzbl	(%rbx,%r14), %r11d
	movq	-1424(%rbp), %rbx
	movzwl	(%rbx,%r14,2), %ebx
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rbx
	movl	%eax, %ecx
	movl	%eax, %eax
	orq	%rbx, %r9
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	movl	$1, %r9d
	shrq	$3, %rsi
	salq	%cl, %r9
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r12, %rsi
	subq	%r9, %r8
	andl	$7, %ecx
	movq	-1376(%rbp), %rax
	movzbl	(%rsi), %r10d
	salq	%cl, %r8
	orq	%r8, %r10
	movq	%r10, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L116
.L119:
	movq	-1424(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzwl	122(%rbx), %r8d
	movzbl	61(%rax), %eax
	salq	%cl, %r8
	addq	%rax, %rdi
	leaq	-2114(%rdx), %rax
	orq	%r8, %r9
	movl	%edi, %ecx
	movq	%rdi, 0(%r13)
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	andl	$7, %ecx
	addq	$12, %rdi
	shrq	$3, %rsi
	salq	%cl, %rax
	movq	%rdi, 0(%r13)
	addq	%r12, %rsi
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L116
.L80:
	cmpq	$129, %rdi
	jbe	.L226
	cmpq	$2113, %rdi
	ja	.L85
	subq	$66, %rbx
	movq	-1416(%rbp), %rdi
	movq	-1424(%rbp), %rcx
	movq	%rbx, -1536(%rbp)
	movzbl	(%rsi), %r8d
	bsrl	-1536(%rbp), %ebx
	leal	50(%rbx), %r9d
	movzbl	(%rdi,%r9), %edi
	movq	%rdi, -1552(%rbp)
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	-1552(%rbp), %rdi
	movq	%r8, (%rsi)
	leaq	(%rdi,%rax), %rsi
	movl	$1, %eax
	movq	%rsi, %rdi
	movq	%rsi, (%r12)
	shrq	$3, %rdi
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
.L218:
	movl	%ebx, %ecx
	salq	%cl, %rax
	movq	-1536(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movl	%esi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movl	%ebx, %eax
	movq	-1376(%rbp), %rbx
	addq	%rsi, %rax
	movq	%r8, (%rdi)
	movq	%rax, (%r12)
	addl	$1, (%rbx,%r9,4)
	jmp	.L81
.L229:
	movq	-1360(%rbp), %rcx
	xorl	%eax, %eax
	leaq	5(%rdx), %rsi
	leaq	5(%r10), %rdi
	leaq	13(%rdx), %rbx
	subq	%rdx, %rcx
	subq	$5, %rcx
	movq	%rcx, %r8
	shrq	$3, %r8
	je	.L101
.L100:
	movq	5(%rdx,%rax), %rsi
	movq	5(%r10,%rax), %r9
	cmpq	%r9, %rsi
	je	.L227
	xorq	%r9, %rsi
	xorl	%ecx, %ecx
	rep bsfq	%rsi, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%rcx, %rax
.L102:
	movq	%rdx, %rbx
	leaq	5(%rax), %rcx
	subq	%r10, %rbx
	movq	%rbx, -1504(%rbp)
	cmpq	$262128, %rbx
	jg	.L215
	movq	(%r12), %rdi
	movl	%ebx, -1400(%rbp)
	addq	%rcx, %rdx
	movq	%rdi, %rsi
	movl	%edi, %ebx
	shrq	$3, %rsi
	andl	$7, %ebx
	addq	%r13, %rsi
	movl	%ebx, -1368(%rbp)
	movzbl	(%rsi), %r10d
	cmpq	$9, %rcx
	jbe	.L228
	cmpq	$133, %rcx
	ja	.L107
	leaq	-1(%rax), %r9
	bsrl	%r9d, %eax
	movq	%r9, %rbx
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %rbx
	leal	(%rax,%rax), %ecx
	movq	%rbx, -1488(%rbp)
	leaq	20(%rbx,%rcx), %rbx
	movq	-1416(%rbp), %rcx
	movzbl	(%rcx,%rbx), %ecx
	movq	%rcx, -1512(%rbp)
	movq	-1424(%rbp), %rcx
	addq	-1512(%rbp), %rdi
	movzwl	(%rcx,%rbx,2), %r8d
	movzbl	-1368(%rbp), %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movq	-1488(%rbp), %r10
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%eax, %ecx
	shrq	$3, %rsi
	salq	%cl, %r10
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r13, %rsi
	movq	-1376(%rbp), %rax
	subq	%r10, %r9
	andl	$7, %ecx
	movzbl	(%rsi), %r8d
	salq	%cl, %r9
	movq	%rdi, (%r12)
	addl	$1, (%rax,%rbx,4)
	orq	%r9, %r8
	movq	%r8, (%rsi)
.L106:
	movslq	-1504(%rbp), %rax
	movq	%rdi, %r10
	movq	-1424(%rbp), %r8
	shrq	$3, %r10
	addq	$3, %rax
	addq	%r13, %r10
	bsrl	%eax, %esi
	movq	%rax, %rbx
	leal	-1(%rsi), %r9d
	leal	-4(%rsi,%rsi), %esi
	movl	%r9d, %ecx
	shrq	%cl, %rbx
	movq	-1416(%rbp), %rcx
	andl	$1, %ebx
	addq	%rbx, %rsi
	movq	%rbx, -1368(%rbp)
	movzbl	(%r10), %ebx
	movzwl	160(%r8,%rsi,2), %r8d
	movzbl	80(%rcx,%rsi), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	addq	-1488(%rbp), %rdi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movl	%r9d, %ecx
	orq	%r8, %rbx
	movq	%rdi, %r8
	movq	%rbx, (%r10)
	movq	-1368(%rbp), %rbx
	shrq	$3, %r8
	addq	%r13, %r8
	addq	$2, %rbx
	movzbl	(%r8), %r10d
	salq	%cl, %rbx
	movl	%edi, %ecx
	addq	%r9, %rdi
	subq	%rbx, %rax
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %rax
	orq	%rax, %r10
	movq	-1376(%rbp), %rax
	movq	%r10, (%r8)
	addl	$1, 320(%rax,%rsi,4)
	cmpq	%rdx, %r11
	jbe	.L134
	movq	-3(%rdx), %rax
	movq	%rdx, %rcx
	movabsq	$8503243848024064, %rsi
	movabsq	$8503243848024064, %rbx
	subq	%r15, %rcx
	imulq	%rax, %rsi
	leal	-3(%rcx), %edi
	shrq	$53, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	leal	-2(%rcx), %edi
	shrq	$8, %rsi
	imulq	%rbx, %rsi
	shrq	$53, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	shrq	$24, %rax
	leal	-1(%rcx), %edi
	imulq	%rbx, %rax
	shrq	$16, %rsi
	imulq	%rbx, %rsi
	shrq	$53, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$53, %rsi
	movl	%edi, (%r14,%rsi,4)
	movslq	(%rax), %r10
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rdx)
	jne	.L215
.L97:
	movzbl	4(%r10), %eax
	cmpb	%al, 4(%rdx)
	je	.L229
.L215:
	movq	%rdx, -1504(%rbp)
	jmp	.L98
.L114:
	movq	-1368(%rbp), %rax
	movq	-1440(%rbp), %rdi
	subq	%rdi, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, %rdx
	jb	.L121
	cmpq	$980, -1464(%rbp)
	ja	.L230
.L121:
	movq	0(%r13), %rsi
	movq	-1416(%rbp), %rbx
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$3, %rax
	andl	$7, %ecx
	addq	%r12, %rax
	movzbl	(%rax), %r8d
	cmpq	$22593, %rdx
	jbe	.L231
	movzbl	63(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1092(%rbp)
	movzwl	126(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$24, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-22594(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
.L123:
	movq	-1368(%rbp), %r8
	addq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%r8), %ecx
	movq	%rsi, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rdi
	movl	%esi, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rsi
	orq	%r10, %rax
	movq	%rsi, 0(%r13)
	movq	%rax, (%rdi)
	cmpq	%rdx, %r8
	jne	.L124
	cmpq	$0, -1352(%rbp)
	jne	.L128
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L92:
	cmpq	$71, -1520(%rbp)
	ja	.L94
	movq	-1488(%rbp), %r8
	movq	-1424(%rbp), %r9
	subq	$3, %r8
	bsrl	%r8d, %ebx
	movq	%r8, %r10
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %r10
	leal	(%rbx,%rbx), %ecx
	movq	%r10, -1368(%rbp)
	leaq	4(%r10,%rcx), %r10
	movq	-1416(%rbp), %rcx
	movzwl	(%r9,%r10,2), %r9d
	movzbl	(%rcx,%r10), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	movq	-1368(%rbp), %rdi
	salq	%cl, %r9
	addq	-1488(%rbp), %rax
	movl	%ebx, %ecx
	orq	%r9, %rsi
	salq	%cl, %rdi
	movq	%rax, (%r12)
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movq	%rdi, %rcx
	movq	%r8, %rdi
	shrq	$3, %rdx
	subq	%rcx, %rdi
	movl	%eax, %ecx
	addq	%rbx, %rax
	addq	%r13, %rdx
	andl	$7, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	movzbl	(%rdx), %esi
	salq	%cl, %rdi
	addl	$1, (%rax,%r10,4)
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	jmp	.L93
.L224:
	movq	-1544(%rbp), %rax
	movq	-1496(%rbp), %r8
	movq	-1424(%rbp), %rdx
	movq	-1416(%rbp), %rsi
	movb	$0, (%r8)
	movq	-1376(%rbp), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	call	BuildAndStoreCommandPrefixCode
	jmp	.L63
.L221:
	movq	-1416(%rbp), %rbx
	movzbl	-1512(%rbp), %ecx
	addl	$1, -1088(%rbp)
	movzbl	64(%rbx), %esi
	movq	-1424(%rbp), %rbx
	movzwl	128(%rbx), %edx
	addq	%rsi, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdx
	orq	-1368(%rbp), %rdx
	movq	%rdx, (%r10)
	jmp	.L91
.L94:
	movq	-1424(%rbp), %rcx
	cmpq	$135, -1520(%rbp)
	movzwl	128(%rcx), %ebx
	jbe	.L232
	cmpq	$2119, -1520(%rbp)
	ja	.L96
	movq	%rcx, %r9
	movq	-1488(%rbp), %rcx
	movq	-1416(%rbp), %r8
	subq	$67, %rcx
	movq	%rcx, -1368(%rbp)
	movl	%edi, %ecx
	bsrl	-1368(%rbp), %r10d
	movl	%r10d, -1488(%rbp)
	addl	$28, %r10d
	movl	-1488(%rbp), %edi
	movzbl	(%r8,%r10), %r8d
	movzwl	(%r9,%r10,2), %r9d
	addq	%rax, %r8
	salq	%cl, %r9
	movl	$1, %eax
	movl	%edi, %ecx
	salq	%cl, %rax
	orq	%r9, %rsi
	movq	-1368(%rbp), %rcx
	movq	%r8, (%r12)
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	addq	%r13, %rdx
	movq	%rcx, %rax
	movl	%r8d, %ecx
	movzbl	(%rdx), %esi
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %rsi
	movl	%edi, %eax
	addq	%rax, %r8
	movq	-1416(%rbp), %rax
	movq	%rsi, (%rdx)
	movq	%r8, (%r12)
	movl	%r8d, %ecx
	movzbl	64(%rax), %esi
	movq	%r8, %rax
	andl	$7, %ecx
	shrq	$3, %rax
	salq	%cl, %rbx
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edx
	movq	%r8, (%r12)
	orq	%rbx, %rdx
	movq	%rdx, (%rax)
	movq	-1376(%rbp), %rax
	addl	$1, (%rax,%r10,4)
	addl	$1, -1088(%rbp)
	jmp	.L93
.L226:
	leaq	-2(%rdi), %rdi
	movzbl	(%rsi), %r8d
	bsrl	%edi, %ebx
	movq	%rdi, -1536(%rbp)
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %rdi
	leal	(%rbx,%rbx), %ecx
	leaq	42(%rdi,%rcx), %r9
	movq	-1416(%rbp), %rcx
	movq	%rdi, -1552(%rbp)
	movzbl	(%rcx,%r9), %ecx
	movq	%rcx, -1560(%rbp)
	movq	-1424(%rbp), %rcx
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	%r8, (%rsi)
	movq	-1560(%rbp), %rsi
	addq	%rax, %rsi
	movq	%rsi, %rax
	movq	%rsi, (%r12)
	shrq	$3, %rax
	movq	%rax, %rdi
	movq	-1552(%rbp), %rax
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
	jmp	.L218
.L231:
	movzbl	62(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1096(%rbp)
	movzwl	124(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$14, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-6210(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
	jmp	.L123
.L230:
	movq	-1448(%rbp), %rdx
	movq	-1360(%rbp), %rsi
	movq	%r12, %r8
	movq	%r13, %rcx
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	cmpq	$0, -1352(%rbp)
	jne	.L128
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L232:
	movq	-1488(%rbp), %r8
	movq	-1416(%rbp), %r9
	subq	$3, %r8
	movq	%r8, %r10
	shrq	$5, %r10
	movzbl	30(%r9,%r10), %r9d
	movq	%r9, -1368(%rbp)
	movzwl	60(%rcx,%r10,2), %r9d
	movl	%edi, %ecx
	movq	%r8, %rdi
	addq	-1368(%rbp), %rax
	andl	$31, %edi
	salq	%cl, %r9
	movl	%eax, %ecx
	movq	%rax, (%r12)
	orq	%r9, %rsi
	andl	$7, %ecx
	movq	-1416(%rbp), %r9
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	salq	%cl, %rdi
	addq	$5, %rax
	shrq	$3, %rdx
	movq	%rax, (%r12)
	movl	%eax, %ecx
	addq	%r13, %rdx
	andl	$7, %ecx
	movzbl	(%rdx), %esi
	salq	%cl, %rbx
	movq	%rbx, %rcx
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movzbl	64(%r9), %esi
	shrq	$3, %rdx
	addq	%r13, %rdx
	addq	%rsi, %rax
	movzbl	(%rdx), %edi
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	orq	%rdi, %rcx
	addl	$1, 120(%rax,%r10,4)
	movq	%rcx, (%rdx)
	addl	$1, -1088(%rbp)
	jmp	.L93
.L220:
	movq	-1488(%rbp), %rcx
	addq	%rdi, %rcx
	addq	$8, %rdi
	subq	$1, %r10
	jne	.L75
	movq	%rdi, %r10
.L73:
	andl	$7, %esi
	je	.L130
	movzbl	(%r9,%r10), %eax
	cmpb	%al, (%rcx)
	jne	.L130
	leaq	1(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L76
	movzbl	1(%r9,%r10), %eax
	cmpb	%al, 1(%rcx)
	jne	.L76
	leaq	2(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$2, %rsi
	je	.L76
	movzbl	2(%r9,%r10), %eax
	cmpb	%al, 2(%rcx)
	jne	.L76
	leaq	3(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$3, %rsi
	je	.L76
	movzbl	3(%r9,%r10), %eax
	cmpb	%al, 3(%rcx)
	jne	.L76
	leaq	4(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$4, %rsi
	je	.L76
	movzbl	4(%r9,%r10), %eax
	cmpb	%al, 4(%rcx)
	jne	.L76
	leaq	5(%r10), %rax
	movq	%rax, -1488(%rbp)
	subq	$5, %rsi
	je	.L76
	movzbl	5(%r9,%r10), %eax
	cmpb	%al, 5(%rcx)
	jne	.L76
	leaq	6(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L76
	movzbl	6(%r9,%r10), %eax
	cmpb	%al, 6(%rcx)
	jne	.L76
	leaq	7(%r10), %rax
	movq	%rax, -1488(%rbp)
	jmp	.L76
.L85:
	movq	-1416(%rbp), %rbx
	movzbl	(%rsi), %r8d
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzbl	61(%rbx), %r9d
	movq	-1424(%rbp), %rbx
	movzwl	122(%rbx), %edi
	addq	%r9, %rax
	movq	-1512(%rbp), %rbx
	movq	%rax, (%r12)
	salq	%cl, %rdi
	movl	%eax, %ecx
	orq	%rdi, %r8
	movq	%rax, %rdi
	andl	$7, %ecx
	addq	$12, %rax
	shrq	$3, %rdi
	movq	%r8, (%rsi)
	leaq	-2114(%rbx), %rsi
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L81
.L132:
	movq	%r13, %rax
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	movq	-1504(%rbp), %rax
	movq	%rax, -1368(%rbp)
	jmp	.L68
.L79:
	movq	-1368(%rbp), %rax
	subq	-1440(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, -1512(%rbp)
	jb	.L86
	cmpq	$980, -1464(%rbp)
	jbe	.L86
	movq	-1448(%rbp), %rdx
	movq	%r13, %rax
	movq	-1440(%rbp), %rdi
	movq	%r12, %r13
	movq	%rax, %r8
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%rax, %r12
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	movq	%rbx, %rax
	movq	%rbx, -1360(%rbp)
	subq	-1392(%rbp), %rax
	subq	%rax, -1352(%rbp)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L86:
	movq	(%r12), %r8
	movq	-1512(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movq	%r8, %rdi
	movl	%r8d, %ecx
	shrq	$3, %rdi
	andl	$7, %ecx
	addq	%r13, %rdi
	cmpq	$22593, %rbx
	ja	.L88
	movzbl	62(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1096(%rbp)
	movzwl	124(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-6210(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$14, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L81
.L96:
	movq	-1416(%rbp), %r10
	movzwl	78(%rcx), %r9d
	movl	%edi, %ecx
	movq	-1488(%rbp), %rdi
	addl	$1, -1188(%rbp)
	movzbl	39(%r10), %r8d
	salq	%cl, %r9
	addl	$1, -1088(%rbp)
	orq	%r9, %rsi
	subq	$2115, %rdi
	addq	%rax, %r8
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	movl	%r8d, %ecx
	movq	%r8, (%r12)
	addq	$24, %r8
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%r8, (%r12)
	addq	%r13, %rdx
	salq	%cl, %rdi
	movl	%r8d, %ecx
	movzbl	(%rdx), %eax
	andl	$7, %ecx
	salq	%cl, %rbx
	orq	%rdi, %rax
	movq	%rax, (%rdx)
	movq	%r8, %rax
	movzbl	64(%r10), %esi
	movq	%rbx, %rdx
	shrq	$3, %rax
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edi
	movq	%r8, (%r12)
	orq	%rdi, %rdx
	movq	%rdx, (%rax)
	jmp	.L93
.L228:
	movq	-1416(%rbp), %rcx
	movzbl	19(%rcx,%rax), %r8d
	movq	-1424(%rbp), %rcx
	movzwl	38(%rcx,%rax,2), %r9d
	movl	%ebx, %ecx
	movq	-1376(%rbp), %rbx
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	addl	$1, 76(%rbx,%rax,4)
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	jmp	.L106
.L88:
	movzbl	63(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1092(%rbp)
	movzwl	126(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-22594(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$24, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L81
.L107:
	cmpq	$2117, %rcx
	ja	.L108
	subq	$65, %rax
	movq	-1416(%rbp), %r8
	movzbl	-1368(%rbp), %ecx
	movq	%rax, -1488(%rbp)
	bsrl	-1488(%rbp), %ebx
	leal	28(%rbx), %r9d
	movzbl	(%r8,%r9), %eax
	movq	-1424(%rbp), %r8
	movzwl	(%r8,%r9,2), %r8d
	addq	%rax, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movl	$1, %r10d
	movq	%rcx, (%rsi)
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	salq	%cl, %r10
	movq	-1488(%rbp), %rcx
	shrq	$3, %rsi
	addq	%r13, %rsi
	subq	%r10, %rcx
	movzbl	(%rsi), %r8d
	movq	%rcx, %rax
	movl	%edi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movq	-1376(%rbp), %rax
	movq	%r8, (%rsi)
	movl	%ebx, %r8d
	addq	%r8, %rdi
	addl	$1, (%rax,%r9,4)
	movq	%rdi, (%r12)
	jmp	.L106
.L130:
	movq	%r10, -1488(%rbp)
	jmp	.L76
.L134:
	movq	%r13, %rax
	movq	%rdx, -1368(%rbp)
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	jmp	.L68
.L227:
	leaq	(%rbx,%rax), %rsi
	addq	$8, %rax
	subq	$1, %r8
	jne	.L100
.L101:
	andl	$7, %ecx
	je	.L102
	movzbl	(%rdi,%rax), %ebx
	cmpb	%bl, (%rsi)
	jne	.L133
	leaq	1(%rax), %r8
	cmpq	$1, %rcx
	je	.L104
	movzbl	1(%rdi,%rax), %ebx
	cmpb	%bl, 1(%rsi)
	jne	.L104
	leaq	2(%rax), %r8
	cmpq	$2, %rcx
	je	.L104
	movzbl	2(%rdi,%rax), %ebx
	cmpb	%bl, 2(%rsi)
	jne	.L104
	leaq	3(%rax), %r8
	cmpq	$3, %rcx
	je	.L104
	movzbl	3(%rdi,%rax), %ebx
	cmpb	%bl, 3(%rsi)
	jne	.L104
	leaq	4(%rax), %r8
	cmpq	$4, %rcx
	je	.L104
	movzbl	4(%rdi,%rax), %ebx
	cmpb	%bl, 4(%rsi)
	jne	.L104
	leaq	5(%rax), %r8
	subq	$5, %rcx
	je	.L104
	movzbl	5(%rdi,%rax), %ebx
	cmpb	%bl, 5(%rsi)
	jne	.L104
	leaq	6(%rax), %r8
	cmpq	$1, %rcx
	je	.L104
	movzbl	6(%rdi,%rax), %ebx
	cmpb	%bl, 6(%rsi)
	jne	.L104
	leaq	7(%rax), %r8
.L104:
	movq	%r8, %rax
	jmp	.L102
.L108:
	movq	-1416(%rbp), %rbx
	movzbl	-1368(%rbp), %ecx
	subq	$2113, %rax
	addl	$1, -1188(%rbp)
	movzbl	39(%rbx), %r8d
	movq	-1424(%rbp), %rbx
	movzwl	78(%rbx), %r9d
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%edi, %ecx
	addq	$24, %rdi
	shrq	$3, %rsi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	addq	%r13, %rsi
	salq	%cl, %rax
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L106
.L133:
	movq	%rax, %r8
	jmp	.L104
.L225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE117:
	.size	BrotliCompressFragmentFastImpl11, .-BrotliCompressFragmentFastImpl11
	.p2align 4
	.type	BrotliCompressFragmentFastImpl13, @function
BrotliCompressFragmentFastImpl13:
.LFB118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r12
	movq	%rdx, -1352(%rbp)
	movl	%ecx, -1524(%rbp)
	movq	40(%rbp), %rbx
	movq	%rax, -1424(%rbp)
	movq	32(%rbp), %rax
	movq	%r8, -1456(%rbp)
	movq	48(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r9, -1416(%rbp)
	movq	%rax, -1496(%rbp)
	movl	$98304, %eax
	movq	%rdi, -1472(%rbp)
	movq	%r12, -1544(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpq	$98304, %rdx
	movq	(%rbx), %rdi
	cmovbe	%rdx, %rax
	movq	%r15, %rdx
	addq	$3, %rdi
	movq	%rdi, -1448(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	(%rbx), %rax
	subq	$8, %rsp
	movq	%r14, %rsi
	leaq	-832(%rbp), %r8
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%rax, %rdx
	addq	$13, %rax
	movq	%r8, -1480(%rbp)
	shrq	$3, %rdx
	movq	%rax, (%rbx)
	leaq	-320(%rbp), %rax
	addq	%r15, %rdx
	movq	%rax, -1432(%rbp)
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	%r13, %rdx
	movq	%rax, %rcx
	pushq	%r15
	call	BuildAndStoreLiteralPrefixCode
	movq	(%r12), %rsi
	popq	%rdi
	movq	(%rbx), %rdx
	popq	%r8
	movq	%rax, -1464(%rbp)
	cmpq	$7, %rsi
	jbe	.L236
	movq	-1496(%rbp), %r9
	xorl	%edi, %edi
	movq	%r12, %r8
	.p2align 4,,10
	.p2align 3
.L235:
	movq	%rdx, %rsi
	movq	%rdi, %rax
	movl	%edx, %ecx
	addq	$8, %rdx
	shrq	$3, %rsi
	shrq	$3, %rax
	andl	$7, %ecx
	movzbl	(%r9,%rax), %eax
	addq	%r15, %rsi
	movq	%rdx, (%rbx)
	movzbl	(%rsi), %r10d
	salq	%cl, %rax
	orq	%r10, %rax
	movq	%rax, (%rsi)
	movq	%rdi, %rax
	movq	(%r8), %rsi
	addq	$8, %rdi
	addq	$15, %rax
	cmpq	%rax, %rsi
	ja	.L235
.L236:
	movq	-1496(%rbp), %rcx
	movq	%rdx, %rdi
	movq	%rsi, %rax
	andl	$7, %esi
	shrq	$3, %rdi
	shrq	$3, %rax
	movq	%r14, -1392(%rbp)
	movq	%r15, %r12
	movzbl	(%rcx,%rax), %eax
	addq	%r15, %rdi
	movl	%edx, %ecx
	addq	%rsi, %rdx
	movzbl	(%rdi), %r8d
	andl	$7, %ecx
	movq	%rdx, (%rbx)
	movq	%r14, %r15
	salq	%cl, %rax
	movq	%r13, -1384(%rbp)
	orq	%r8, %rax
	movq	%r14, -1440(%rbp)
	movq	%rax, (%rdi)
	leaq	-1344(%rbp), %rax
	movq	%rax, -1376(%rbp)
	movq	%r14, -1368(%rbp)
	movq	%r13, %r14
	movq	%rbx, %r13
	movq	-1392(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	(%rbx,%r14), %rax
	movq	-1376(%rbp), %rdi
	movl	$64, %ecx
	leaq	kCmdHistoSeed(%rip), %rsi
	rep movsq
	movq	%rax, -1360(%rbp)
	cmpq	$15, %r14
	jbe	.L238
	movq	-1352(%rbp), %rax
	leaq	-5(%r14), %rdx
	movabsq	$8503243848024064, %rsi
	movq	%r14, -1408(%rbp)
	movl	$-1, -1400(%rbp)
	movq	-1456(%rbp), %r14
	subq	$16, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	leaq	(%rbx,%rax), %r11
	movq	%rbx, %rax
	leaq	1(%rbx), %rbx
	imulq	1(%rax), %rsi
	leaq	1(%rbx), %rdi
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	shrq	$51, %rsi
	cmpq	%rdi, %r11
	jb	.L382
.L392:
	movslq	-1400(%rbp), %r8
	movl	$33, %ecx
	negq	%r8
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L240:
	movslq	(%rdx), %rax
	movl	%r9d, (%rdx)
	addq	%r15, %rax
	movl	(%rax), %edx
	cmpl	%edx, (%rbx)
	je	.L389
.L242:
	leal	1(%rcx), %eax
	shrl	$5, %ecx
	movq	%rdi, %rbx
	addq	%rdi, %rcx
	cmpq	%rcx, %r11
	jb	.L382
	movq	%rcx, %rdi
	movl	%eax, %ecx
.L239:
	movl	%esi, %edx
	leaq	(%rbx,%r8), %rax
	movq	%rbx, %r9
	movabsq	$8503243848024064, %rsi
	imulq	(%rdi), %rsi
	movl	(%rax), %r10d
	leaq	(%r14,%rdx,4), %rdx
	subq	%r15, %r9
	shrq	$51, %rsi
	cmpl	%r10d, (%rbx)
	jne	.L240
	movzbl	4(%rax), %r10d
	cmpb	%r10b, 4(%rbx)
	jne	.L240
	cmpq	%rbx, %rax
	jnb	.L240
	movl	%r9d, (%rdx)
.L241:
	movq	%rbx, %rdx
	subq	%rax, %rdx
	cmpq	$262128, %rdx
	jg	.L242
	movq	-1360(%rbp), %rsi
	leaq	5(%rbx), %rcx
	leaq	5(%rax), %r9
	subq	%rbx, %rsi
	subq	$5, %rsi
	movq	%rsi, %r10
	shrq	$3, %r10
	je	.L243
	leaq	13(%rbx), %rcx
	xorl	%edi, %edi
	movq	%rcx, -1488(%rbp)
.L245:
	movq	5(%rbx,%rdi), %rcx
	movq	5(%rax,%rdi), %r8
	cmpq	%r8, %rcx
	je	.L390
	xorq	%r8, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rdi, %rax
	movq	%rax, -1488(%rbp)
.L246:
	movq	-1488(%rbp), %rax
	movq	%rbx, %rdi
	subq	-1368(%rbp), %rdi
	movq	%rdi, -1512(%rbp)
	addq	$5, %rax
	movq	%rax, -1520(%rbp)
	addq	%rbx, %rax
	movq	%rax, -1504(%rbp)
	cmpq	$6209, %rdi
	ja	.L249
	movq	(%r12), %rax
	movq	%rdi, %rbx
	movq	%rax, %rsi
	movl	%eax, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r13, %rsi
	cmpq	$5, %rdi
	ja	.L250
	movq	-1416(%rbp), %rcx
	movzbl	(%rsi), %r9d
	movzbl	40(%rcx,%rdi), %r8d
	movq	-1424(%rbp), %rdi
	movl	%r10d, %ecx
	movzwl	80(%rdi,%rbx,2), %edi
	addq	%r8, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdi
	orq	%rdi, %r9
	movq	-1376(%rbp), %rdi
	movq	%r9, (%rsi)
	addl	$1, 160(%rdi,%rbx,4)
	testq	%rbx, %rbx
	je	.L252
.L251:
	movq	%r11, -1536(%rbp)
	movq	-1512(%rbp), %rbx
	xorl	%r8d, %r8d
	movq	-1368(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L259:
	movzbl	(%r11,%r8), %ecx
	movq	%rax, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %esi
	addq	%r13, %rdi
	movl	%eax, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rsi
	addq	%r9, %rax
	orq	%rsi, %r10
	movq	%rax, (%r12)
	movq	%r10, (%rdi)
	cmpq	%r8, %rbx
	ja	.L259
	movq	-1536(%rbp), %r11
.L252:
	movq	%rax, %r10
	shrq	$3, %r10
	addq	%r13, %r10
	movzbl	(%r10), %ebx
	movq	%rbx, -1368(%rbp)
	movl	%eax, %ebx
	andl	$7, %ebx
	movl	%ebx, -1512(%rbp)
	cmpl	%edx, -1400(%rbp)
	je	.L391
	movslq	%edx, %rdi
	movq	-1416(%rbp), %rbx
	addq	$3, %rdi
	bsrl	%edi, %r8d
	movq	%rdi, %r9
	leal	-1(%r8), %esi
	leal	-4(%r8,%r8), %r8d
	movl	%esi, %ecx
	shrq	%cl, %r9
	movq	-1424(%rbp), %rcx
	andl	$1, %r9d
	addq	%r9, %r8
	addq	$2, %r9
	movzbl	80(%rbx,%r8), %ebx
	movq	%rbx, -1400(%rbp)
	addq	-1400(%rbp), %rax
	movzwl	160(%rcx,%r8,2), %ebx
	movzbl	-1512(%rbp), %ecx
	movq	%rax, (%r12)
	movl	%edx, -1400(%rbp)
	salq	%cl, %rbx
	movq	-1368(%rbp), %rcx
	orq	%rbx, %rcx
	movq	%rcx, (%r10)
	movq	%rax, %r10
	movl	%esi, %ecx
	shrq	$3, %r10
	salq	%cl, %r9
	movl	%eax, %ecx
	addq	%rsi, %rax
	addq	%r13, %r10
	subq	%r9, %rdi
	andl	$7, %ecx
	movq	%rax, (%r12)
	movzbl	(%r10), %ebx
	salq	%cl, %rdi
	orq	%rdi, %rbx
	movq	%rbx, (%r10)
	movq	-1376(%rbp), %rbx
	addl	$1, 320(%rbx,%r8,4)
.L261:
	movq	%rax, %rdx
	movq	-1520(%rbp), %rbx
	movl	%eax, %edi
	shrq	$3, %rdx
	andl	$7, %edi
	addq	%r13, %rdx
	movzbl	(%rdx), %esi
	cmpq	$11, %rbx
	ja	.L262
	movq	-1416(%rbp), %rcx
	movq	-1488(%rbp), %r10
	movzbl	1(%rcx,%r10), %r9d
	movq	-1424(%rbp), %rcx
	addq	%r9, %rax
	movzwl	-8(%rcx,%rbx,2), %r8d
	movl	%edi, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	salq	%cl, %r8
	orq	%r8, %rsi
	addl	$1, -16(%rax,%rbx,4)
	movq	%rsi, (%rdx)
.L263:
	movq	-1504(%rbp), %rbx
	cmpq	%rbx, %r11
	jbe	.L302
	movq	-3(%rbx), %rax
	movq	%rbx, %rcx
	movabsq	$8503243848024064, %rdx
	movabsq	$8503243848024064, %rdi
	subq	%r15, %rcx
	imulq	%rax, %rdx
	leal	-3(%rcx), %esi
	shrq	$51, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	leal	-2(%rcx), %esi
	shrq	$8, %rdx
	imulq	%rdi, %rdx
	shrq	$51, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	shrq	$24, %rax
	leal	-1(%rcx), %esi
	imulq	%rdi, %rax
	shrq	$16, %rdx
	imulq	%rdi, %rdx
	shrq	$51, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$51, %rdx
	movl	%esi, (%r14,%rdx,4)
	movslq	(%rax), %r10
	movq	%rbx, %rdx
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rbx)
	je	.L267
.L268:
	movq	-1504(%rbp), %rax
	movabsq	$8503243848024064, %rsi
	imulq	1(%rax), %rsi
	leaq	1(%rax), %rbx
	movq	%rax, -1368(%rbp)
	leaq	1(%rbx), %rdi
	shrq	$51, %rsi
	cmpq	%rdi, %r11
	jnb	.L392
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r13, %rax
	movq	-1408(%rbp), %r14
	movq	%r12, %r13
	movq	%rax, %r12
.L238:
	subq	%r14, -1352(%rbp)
	movq	-1352(%rbp), %rdi
	je	.L280
	cmpq	$65536, %rdi
	movl	$65536, %eax
	cmovbe	%rdi, %rax
	addq	%rax, -1384(%rbp)
	movq	%rax, %r14
	movq	-1384(%rbp), %rax
	cmpq	$1048576, %rax
	jbe	.L393
.L281:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	ja	.L297
.L298:
	movq	-1352(%rbp), %rax
	movl	$98304, %r14d
	movq	%r12, %rdx
	movq	%r13, %rsi
	cmpq	$98304, %rax
	cmovbe	%rax, %r14
	movq	0(%r13), %rax
	movq	%r14, %rdi
	addq	$3, %rax
	movq	%rax, -1448(%rbp)
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	0(%r13), %rax
	subq	$8, %rsp
	movq	%r13, %r9
	movq	-1360(%rbp), %rbx
	movq	-1480(%rbp), %r8
	movq	%rax, %rdx
	addq	$13, %rax
	movq	-1472(%rbp), %rdi
	shrq	$3, %rdx
	movq	%rax, 0(%r13)
	movq	%rbx, %rsi
	addq	%r12, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	-1432(%rbp), %rcx
	movq	%r14, %rdx
	pushq	%r12
	call	BuildAndStoreLiteralPrefixCode
	movq	-1424(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	-1416(%rbp), %rsi
	movq	-1376(%rbp), %rdi
	movq	%rax, -1464(%rbp)
	call	BuildAndStoreCommandPrefixCode
	popq	%rdx
	popq	%rcx
	movq	%r14, -1384(%rbp)
	movq	%rbx, -1440(%rbp)
	movq	%rbx, -1368(%rbp)
	movq	%rbx, -1392(%rbp)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L389:
	movzbl	4(%rax), %edx
	cmpb	%dl, 4(%rbx)
	jne	.L242
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L280:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	jbe	.L295
.L297:
	movq	-1360(%rbp), %rdx
	subq	-1368(%rbp), %rdx
	cmpq	$6209, %rdx
	ja	.L284
	movq	0(%r13), %rdi
	movq	%rdi, %rsi
	movl	%edi, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	cmpq	$5, %rdx
	ja	.L285
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	movzbl	40(%rax,%rdx), %r11d
	movq	-1424(%rbp), %rax
	movzwl	80(%rax,%rdx,2), %eax
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rax
	orq	%rax, %r9
	movq	-1376(%rbp), %rax
	movq	%r9, (%rsi)
	addl	$1, 160(%rax,%rdx,4)
	testq	%rdx, %rdx
	je	.L257
.L286:
	movq	-1368(%rbp), %r10
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L290:
	movzbl	(%r10,%r8), %ecx
	movq	%rdi, %rsi
	addq	$1, %r8
	shrq	$3, %rsi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rsi
	movl	%edi, %ecx
	movzbl	(%rsi), %r11d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rdi
	orq	%r11, %rax
	movq	%rdi, 0(%r13)
	movq	%rax, (%rsi)
	cmpq	%r8, %rdx
	ja	.L290
.L257:
	cmpq	$0, -1352(%rbp)
	jne	.L298
	.p2align 4,,10
	.p2align 3
.L295:
	movl	-1524(%rbp), %eax
	testl	%eax, %eax
	je	.L394
.L233:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	cmpq	$129, %rdx
	ja	.L287
	leaq	-2(%rdx), %r8
	movq	-1416(%rbp), %rbx
	bsrl	%r8d, %eax
	movq	%r8, %r11
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %r11
	leal	(%rax,%rax), %ecx
	leaq	42(%r11,%rcx), %r14
	movq	-1424(%rbp), %rcx
	movzbl	(%rbx,%r14), %ebx
	movq	%rbx, -1384(%rbp)
	movzwl	(%rcx,%r14,2), %ebx
	movl	%r10d, %ecx
	addq	-1384(%rbp), %rdi
	salq	%cl, %rbx
	movl	%eax, %ecx
	movq	%rdi, 0(%r13)
	orq	%rbx, %r9
	salq	%cl, %r11
	movl	%edi, %ecx
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	subq	%r11, %r8
	andl	$7, %ecx
	shrq	$3, %rsi
	addq	%rax, %rdi
	salq	%cl, %r8
	movq	-1376(%rbp), %rax
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	orq	%r8, %r9
	movq	%r9, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L393:
	movq	-1432(%rbp), %rdx
	movq	-1360(%rbp), %rdi
	movq	%r14, %rsi
	call	ShouldMergeBlock
	testl	%eax, %eax
	je	.L281
	movl	-1384(%rbp), %eax
	movl	$20, %edi
	movl	$1, %ebx
	movq	-1448(%rbp), %r10
	leal	-1(%rax), %r11d
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r10, %r9
	movl	$8, %eax
	movl	%ebx, %edx
	movl	%ebx, %esi
	andl	$7, %r9d
	movq	%r10, %r8
	subq	%r9, %rax
	cmpq	%rdi, %rax
	cmova	%rdi, %rax
	shrq	$3, %r8
	addq	%r12, %r8
	leaq	(%r9,%rax), %rcx
	addq	%rax, %r10
	sall	%cl, %edx
	movl	%r9d, %ecx
	sall	%cl, %esi
	negl	%edx
	movl	%esi, %ecx
	movl	$-1, %esi
	subl	$1, %ecx
	orl	%ecx, %edx
	movl	%eax, %ecx
	andb	(%r8), %dl
	sall	%cl, %esi
	movl	%r9d, %ecx
	notl	%esi
	andl	%r11d, %esi
	sall	%cl, %esi
	movl	%eax, %ecx
	orl	%edx, %esi
	shrl	%cl, %r11d
	movb	%sil, (%r8)
	subq	%rax, %rdi
	jne	.L282
	movq	-1360(%rbp), %rax
	movq	%rax, -1392(%rbp)
	movq	%rax, %rbx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L287:
	cmpq	$2113, %rdx
	ja	.L289
	leaq	-66(%rdx), %r8
	movq	-1416(%rbp), %rbx
	movl	%r10d, %ecx
	bsrl	%r8d, %eax
	leal	50(%rax), %r14d
	movzbl	(%rbx,%r14), %r11d
	movq	-1424(%rbp), %rbx
	movzwl	(%rbx,%r14,2), %ebx
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rbx
	movl	%eax, %ecx
	movl	%eax, %eax
	orq	%rbx, %r9
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	movl	$1, %r9d
	shrq	$3, %rsi
	salq	%cl, %r9
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r12, %rsi
	subq	%r9, %r8
	andl	$7, %ecx
	movq	-1376(%rbp), %rax
	movzbl	(%rsi), %r10d
	salq	%cl, %r8
	orq	%r8, %r10
	movq	%r10, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L286
.L289:
	movq	-1424(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzwl	122(%rbx), %r8d
	movzbl	61(%rax), %eax
	salq	%cl, %r8
	addq	%rax, %rdi
	leaq	-2114(%rdx), %rax
	orq	%r8, %r9
	movl	%edi, %ecx
	movq	%rdi, 0(%r13)
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	andl	$7, %ecx
	addq	$12, %rdi
	shrq	$3, %rsi
	salq	%cl, %rax
	movq	%rdi, 0(%r13)
	addq	%r12, %rsi
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L286
.L250:
	cmpq	$129, %rdi
	jbe	.L396
	cmpq	$2113, %rdi
	ja	.L255
	subq	$66, %rbx
	movq	-1416(%rbp), %rdi
	movq	-1424(%rbp), %rcx
	movq	%rbx, -1536(%rbp)
	movzbl	(%rsi), %r8d
	bsrl	-1536(%rbp), %ebx
	leal	50(%rbx), %r9d
	movzbl	(%rdi,%r9), %edi
	movq	%rdi, -1552(%rbp)
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	-1552(%rbp), %rdi
	movq	%r8, (%rsi)
	leaq	(%rdi,%rax), %rsi
	movl	$1, %eax
	movq	%rsi, %rdi
	movq	%rsi, (%r12)
	shrq	$3, %rdi
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
.L388:
	movl	%ebx, %ecx
	salq	%cl, %rax
	movq	-1536(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movl	%esi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movl	%ebx, %eax
	movq	-1376(%rbp), %rbx
	addq	%rsi, %rax
	movq	%r8, (%rdi)
	movq	%rax, (%r12)
	addl	$1, (%rbx,%r9,4)
	jmp	.L251
.L399:
	movq	-1360(%rbp), %rcx
	xorl	%eax, %eax
	leaq	5(%rdx), %rsi
	leaq	5(%r10), %rdi
	leaq	13(%rdx), %rbx
	subq	%rdx, %rcx
	subq	$5, %rcx
	movq	%rcx, %r8
	shrq	$3, %r8
	je	.L271
.L270:
	movq	5(%rdx,%rax), %rsi
	movq	5(%r10,%rax), %r9
	cmpq	%r9, %rsi
	je	.L397
	xorq	%r9, %rsi
	xorl	%ecx, %ecx
	rep bsfq	%rsi, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%rcx, %rax
.L272:
	movq	%rdx, %rbx
	leaq	5(%rax), %rcx
	subq	%r10, %rbx
	movq	%rbx, -1504(%rbp)
	cmpq	$262128, %rbx
	jg	.L385
	movq	(%r12), %rdi
	movl	%ebx, -1400(%rbp)
	addq	%rcx, %rdx
	movq	%rdi, %rsi
	movl	%edi, %ebx
	shrq	$3, %rsi
	andl	$7, %ebx
	addq	%r13, %rsi
	movl	%ebx, -1368(%rbp)
	movzbl	(%rsi), %r10d
	cmpq	$9, %rcx
	jbe	.L398
	cmpq	$133, %rcx
	ja	.L277
	leaq	-1(%rax), %r9
	bsrl	%r9d, %eax
	movq	%r9, %rbx
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %rbx
	leal	(%rax,%rax), %ecx
	movq	%rbx, -1488(%rbp)
	leaq	20(%rbx,%rcx), %rbx
	movq	-1416(%rbp), %rcx
	movzbl	(%rcx,%rbx), %ecx
	movq	%rcx, -1512(%rbp)
	movq	-1424(%rbp), %rcx
	addq	-1512(%rbp), %rdi
	movzwl	(%rcx,%rbx,2), %r8d
	movzbl	-1368(%rbp), %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movq	-1488(%rbp), %r10
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%eax, %ecx
	shrq	$3, %rsi
	salq	%cl, %r10
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r13, %rsi
	movq	-1376(%rbp), %rax
	subq	%r10, %r9
	andl	$7, %ecx
	movzbl	(%rsi), %r8d
	salq	%cl, %r9
	movq	%rdi, (%r12)
	addl	$1, (%rax,%rbx,4)
	orq	%r9, %r8
	movq	%r8, (%rsi)
.L276:
	movslq	-1504(%rbp), %rax
	movq	%rdi, %r10
	movq	-1424(%rbp), %r8
	shrq	$3, %r10
	addq	$3, %rax
	addq	%r13, %r10
	bsrl	%eax, %esi
	movq	%rax, %rbx
	leal	-1(%rsi), %r9d
	leal	-4(%rsi,%rsi), %esi
	movl	%r9d, %ecx
	shrq	%cl, %rbx
	movq	-1416(%rbp), %rcx
	andl	$1, %ebx
	addq	%rbx, %rsi
	movq	%rbx, -1368(%rbp)
	movzbl	(%r10), %ebx
	movzwl	160(%r8,%rsi,2), %r8d
	movzbl	80(%rcx,%rsi), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	addq	-1488(%rbp), %rdi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movl	%r9d, %ecx
	orq	%r8, %rbx
	movq	%rdi, %r8
	movq	%rbx, (%r10)
	movq	-1368(%rbp), %rbx
	shrq	$3, %r8
	addq	%r13, %r8
	addq	$2, %rbx
	movzbl	(%r8), %r10d
	salq	%cl, %rbx
	movl	%edi, %ecx
	addq	%r9, %rdi
	subq	%rbx, %rax
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %rax
	orq	%rax, %r10
	movq	-1376(%rbp), %rax
	movq	%r10, (%r8)
	addl	$1, 320(%rax,%rsi,4)
	cmpq	%rdx, %r11
	jbe	.L304
	movq	-3(%rdx), %rax
	movq	%rdx, %rcx
	movabsq	$8503243848024064, %rsi
	movabsq	$8503243848024064, %rbx
	subq	%r15, %rcx
	imulq	%rax, %rsi
	leal	-3(%rcx), %edi
	shrq	$51, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	leal	-2(%rcx), %edi
	shrq	$8, %rsi
	imulq	%rbx, %rsi
	shrq	$51, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	shrq	$24, %rax
	leal	-1(%rcx), %edi
	imulq	%rbx, %rax
	shrq	$16, %rsi
	imulq	%rbx, %rsi
	shrq	$51, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$51, %rsi
	movl	%edi, (%r14,%rsi,4)
	movslq	(%rax), %r10
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rdx)
	jne	.L385
.L267:
	movzbl	4(%r10), %eax
	cmpb	%al, 4(%rdx)
	je	.L399
.L385:
	movq	%rdx, -1504(%rbp)
	jmp	.L268
.L284:
	movq	-1368(%rbp), %rax
	movq	-1440(%rbp), %rdi
	subq	%rdi, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, %rdx
	jb	.L291
	cmpq	$980, -1464(%rbp)
	ja	.L400
.L291:
	movq	0(%r13), %rsi
	movq	-1416(%rbp), %rbx
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$3, %rax
	andl	$7, %ecx
	addq	%r12, %rax
	movzbl	(%rax), %r8d
	cmpq	$22593, %rdx
	jbe	.L401
	movzbl	63(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1092(%rbp)
	movzwl	126(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$24, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-22594(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
.L293:
	movq	-1368(%rbp), %r8
	addq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L294:
	movzbl	(%r8), %ecx
	movq	%rsi, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rdi
	movl	%esi, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rsi
	orq	%r10, %rax
	movq	%rsi, 0(%r13)
	movq	%rax, (%rdi)
	cmpq	%rdx, %r8
	jne	.L294
	cmpq	$0, -1352(%rbp)
	jne	.L298
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L262:
	cmpq	$71, -1520(%rbp)
	ja	.L264
	movq	-1488(%rbp), %r8
	movq	-1424(%rbp), %r9
	subq	$3, %r8
	bsrl	%r8d, %ebx
	movq	%r8, %r10
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %r10
	leal	(%rbx,%rbx), %ecx
	movq	%r10, -1368(%rbp)
	leaq	4(%r10,%rcx), %r10
	movq	-1416(%rbp), %rcx
	movzwl	(%r9,%r10,2), %r9d
	movzbl	(%rcx,%r10), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	movq	-1368(%rbp), %rdi
	salq	%cl, %r9
	addq	-1488(%rbp), %rax
	movl	%ebx, %ecx
	orq	%r9, %rsi
	salq	%cl, %rdi
	movq	%rax, (%r12)
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movq	%rdi, %rcx
	movq	%r8, %rdi
	shrq	$3, %rdx
	subq	%rcx, %rdi
	movl	%eax, %ecx
	addq	%rbx, %rax
	addq	%r13, %rdx
	andl	$7, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	movzbl	(%rdx), %esi
	salq	%cl, %rdi
	addl	$1, (%rax,%r10,4)
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	jmp	.L263
.L394:
	movq	-1544(%rbp), %rax
	movq	-1496(%rbp), %r8
	movq	-1424(%rbp), %rdx
	movq	-1416(%rbp), %rsi
	movb	$0, (%r8)
	movq	-1376(%rbp), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	call	BuildAndStoreCommandPrefixCode
	jmp	.L233
.L391:
	movq	-1416(%rbp), %rbx
	movzbl	-1512(%rbp), %ecx
	addl	$1, -1088(%rbp)
	movzbl	64(%rbx), %esi
	movq	-1424(%rbp), %rbx
	movzwl	128(%rbx), %edx
	addq	%rsi, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdx
	orq	-1368(%rbp), %rdx
	movq	%rdx, (%r10)
	jmp	.L261
.L264:
	movq	-1424(%rbp), %rcx
	cmpq	$135, -1520(%rbp)
	movzwl	128(%rcx), %ebx
	jbe	.L402
	cmpq	$2119, -1520(%rbp)
	ja	.L266
	movq	%rcx, %r9
	movq	-1488(%rbp), %rcx
	movq	-1416(%rbp), %r8
	subq	$67, %rcx
	movq	%rcx, -1368(%rbp)
	movl	%edi, %ecx
	bsrl	-1368(%rbp), %r10d
	movl	%r10d, -1488(%rbp)
	addl	$28, %r10d
	movl	-1488(%rbp), %edi
	movzbl	(%r8,%r10), %r8d
	movzwl	(%r9,%r10,2), %r9d
	addq	%rax, %r8
	salq	%cl, %r9
	movl	$1, %eax
	movl	%edi, %ecx
	salq	%cl, %rax
	orq	%r9, %rsi
	movq	-1368(%rbp), %rcx
	movq	%r8, (%r12)
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	addq	%r13, %rdx
	movq	%rcx, %rax
	movl	%r8d, %ecx
	movzbl	(%rdx), %esi
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %rsi
	movl	%edi, %eax
	addq	%rax, %r8
	movq	-1416(%rbp), %rax
	movq	%rsi, (%rdx)
	movq	%r8, (%r12)
	movl	%r8d, %ecx
	movzbl	64(%rax), %esi
	movq	%r8, %rax
	andl	$7, %ecx
	shrq	$3, %rax
	salq	%cl, %rbx
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edx
	movq	%r8, (%r12)
	orq	%rbx, %rdx
	movq	%rdx, (%rax)
	movq	-1376(%rbp), %rax
	addl	$1, (%rax,%r10,4)
	addl	$1, -1088(%rbp)
	jmp	.L263
.L396:
	leaq	-2(%rdi), %rdi
	movzbl	(%rsi), %r8d
	bsrl	%edi, %ebx
	movq	%rdi, -1536(%rbp)
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %rdi
	leal	(%rbx,%rbx), %ecx
	leaq	42(%rdi,%rcx), %r9
	movq	-1416(%rbp), %rcx
	movq	%rdi, -1552(%rbp)
	movzbl	(%rcx,%r9), %ecx
	movq	%rcx, -1560(%rbp)
	movq	-1424(%rbp), %rcx
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	%r8, (%rsi)
	movq	-1560(%rbp), %rsi
	addq	%rax, %rsi
	movq	%rsi, %rax
	movq	%rsi, (%r12)
	shrq	$3, %rax
	movq	%rax, %rdi
	movq	-1552(%rbp), %rax
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
	jmp	.L388
.L401:
	movzbl	62(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1096(%rbp)
	movzwl	124(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$14, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-6210(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
	jmp	.L293
.L400:
	movq	-1448(%rbp), %rdx
	movq	-1360(%rbp), %rsi
	movq	%r12, %r8
	movq	%r13, %rcx
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	cmpq	$0, -1352(%rbp)
	jne	.L298
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-1488(%rbp), %r8
	movq	-1416(%rbp), %r9
	subq	$3, %r8
	movq	%r8, %r10
	shrq	$5, %r10
	movzbl	30(%r9,%r10), %r9d
	movq	%r9, -1368(%rbp)
	movzwl	60(%rcx,%r10,2), %r9d
	movl	%edi, %ecx
	movq	%r8, %rdi
	addq	-1368(%rbp), %rax
	andl	$31, %edi
	salq	%cl, %r9
	movl	%eax, %ecx
	movq	%rax, (%r12)
	orq	%r9, %rsi
	andl	$7, %ecx
	movq	-1416(%rbp), %r9
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	salq	%cl, %rdi
	addq	$5, %rax
	shrq	$3, %rdx
	movq	%rax, (%r12)
	movl	%eax, %ecx
	addq	%r13, %rdx
	andl	$7, %ecx
	movzbl	(%rdx), %esi
	salq	%cl, %rbx
	movq	%rbx, %rcx
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movzbl	64(%r9), %esi
	shrq	$3, %rdx
	addq	%r13, %rdx
	addq	%rsi, %rax
	movzbl	(%rdx), %edi
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	orq	%rdi, %rcx
	addl	$1, 120(%rax,%r10,4)
	movq	%rcx, (%rdx)
	addl	$1, -1088(%rbp)
	jmp	.L263
.L390:
	movq	-1488(%rbp), %rcx
	addq	%rdi, %rcx
	addq	$8, %rdi
	subq	$1, %r10
	jne	.L245
	movq	%rdi, %r10
.L243:
	andl	$7, %esi
	je	.L300
	movzbl	(%r9,%r10), %eax
	cmpb	%al, (%rcx)
	jne	.L300
	leaq	1(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L246
	movzbl	1(%r9,%r10), %eax
	cmpb	%al, 1(%rcx)
	jne	.L246
	leaq	2(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$2, %rsi
	je	.L246
	movzbl	2(%r9,%r10), %eax
	cmpb	%al, 2(%rcx)
	jne	.L246
	leaq	3(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$3, %rsi
	je	.L246
	movzbl	3(%r9,%r10), %eax
	cmpb	%al, 3(%rcx)
	jne	.L246
	leaq	4(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$4, %rsi
	je	.L246
	movzbl	4(%r9,%r10), %eax
	cmpb	%al, 4(%rcx)
	jne	.L246
	leaq	5(%r10), %rax
	movq	%rax, -1488(%rbp)
	subq	$5, %rsi
	je	.L246
	movzbl	5(%r9,%r10), %eax
	cmpb	%al, 5(%rcx)
	jne	.L246
	leaq	6(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L246
	movzbl	6(%r9,%r10), %eax
	cmpb	%al, 6(%rcx)
	jne	.L246
	leaq	7(%r10), %rax
	movq	%rax, -1488(%rbp)
	jmp	.L246
.L255:
	movq	-1416(%rbp), %rbx
	movzbl	(%rsi), %r8d
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzbl	61(%rbx), %r9d
	movq	-1424(%rbp), %rbx
	movzwl	122(%rbx), %edi
	addq	%r9, %rax
	movq	-1512(%rbp), %rbx
	movq	%rax, (%r12)
	salq	%cl, %rdi
	movl	%eax, %ecx
	orq	%rdi, %r8
	movq	%rax, %rdi
	andl	$7, %ecx
	addq	$12, %rax
	shrq	$3, %rdi
	movq	%r8, (%rsi)
	leaq	-2114(%rbx), %rsi
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L251
.L302:
	movq	%r13, %rax
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	movq	-1504(%rbp), %rax
	movq	%rax, -1368(%rbp)
	jmp	.L238
.L249:
	movq	-1368(%rbp), %rax
	subq	-1440(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, -1512(%rbp)
	jb	.L256
	cmpq	$980, -1464(%rbp)
	jbe	.L256
	movq	-1448(%rbp), %rdx
	movq	%r13, %rax
	movq	-1440(%rbp), %rdi
	movq	%r12, %r13
	movq	%rax, %r8
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%rax, %r12
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	movq	%rbx, %rax
	movq	%rbx, -1360(%rbp)
	subq	-1392(%rbp), %rax
	subq	%rax, -1352(%rbp)
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%r12), %r8
	movq	-1512(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movq	%r8, %rdi
	movl	%r8d, %ecx
	shrq	$3, %rdi
	andl	$7, %ecx
	addq	%r13, %rdi
	cmpq	$22593, %rbx
	ja	.L258
	movzbl	62(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1096(%rbp)
	movzwl	124(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-6210(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$14, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L251
.L266:
	movq	-1416(%rbp), %r10
	movzwl	78(%rcx), %r9d
	movl	%edi, %ecx
	movq	-1488(%rbp), %rdi
	addl	$1, -1188(%rbp)
	movzbl	39(%r10), %r8d
	salq	%cl, %r9
	addl	$1, -1088(%rbp)
	orq	%r9, %rsi
	subq	$2115, %rdi
	addq	%rax, %r8
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	movl	%r8d, %ecx
	movq	%r8, (%r12)
	addq	$24, %r8
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%r8, (%r12)
	addq	%r13, %rdx
	salq	%cl, %rdi
	movl	%r8d, %ecx
	movzbl	(%rdx), %eax
	andl	$7, %ecx
	salq	%cl, %rbx
	orq	%rdi, %rax
	movq	%rax, (%rdx)
	movq	%r8, %rax
	movzbl	64(%r10), %esi
	movq	%rbx, %rdx
	shrq	$3, %rax
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edi
	movq	%r8, (%r12)
	orq	%rdi, %rdx
	movq	%rdx, (%rax)
	jmp	.L263
.L398:
	movq	-1416(%rbp), %rcx
	movzbl	19(%rcx,%rax), %r8d
	movq	-1424(%rbp), %rcx
	movzwl	38(%rcx,%rax,2), %r9d
	movl	%ebx, %ecx
	movq	-1376(%rbp), %rbx
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	addl	$1, 76(%rbx,%rax,4)
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	jmp	.L276
.L258:
	movzbl	63(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1092(%rbp)
	movzwl	126(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-22594(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$24, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L251
.L277:
	cmpq	$2117, %rcx
	ja	.L278
	subq	$65, %rax
	movq	-1416(%rbp), %r8
	movzbl	-1368(%rbp), %ecx
	movq	%rax, -1488(%rbp)
	bsrl	-1488(%rbp), %ebx
	leal	28(%rbx), %r9d
	movzbl	(%r8,%r9), %eax
	movq	-1424(%rbp), %r8
	movzwl	(%r8,%r9,2), %r8d
	addq	%rax, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movl	$1, %r10d
	movq	%rcx, (%rsi)
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	salq	%cl, %r10
	movq	-1488(%rbp), %rcx
	shrq	$3, %rsi
	addq	%r13, %rsi
	subq	%r10, %rcx
	movzbl	(%rsi), %r8d
	movq	%rcx, %rax
	movl	%edi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movq	-1376(%rbp), %rax
	movq	%r8, (%rsi)
	movl	%ebx, %r8d
	addq	%r8, %rdi
	addl	$1, (%rax,%r9,4)
	movq	%rdi, (%r12)
	jmp	.L276
.L300:
	movq	%r10, -1488(%rbp)
	jmp	.L246
.L304:
	movq	%r13, %rax
	movq	%rdx, -1368(%rbp)
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	jmp	.L238
.L397:
	leaq	(%rbx,%rax), %rsi
	addq	$8, %rax
	subq	$1, %r8
	jne	.L270
.L271:
	andl	$7, %ecx
	je	.L272
	movzbl	(%rdi,%rax), %ebx
	cmpb	%bl, (%rsi)
	jne	.L303
	leaq	1(%rax), %r8
	cmpq	$1, %rcx
	je	.L274
	movzbl	1(%rdi,%rax), %ebx
	cmpb	%bl, 1(%rsi)
	jne	.L274
	leaq	2(%rax), %r8
	cmpq	$2, %rcx
	je	.L274
	movzbl	2(%rdi,%rax), %ebx
	cmpb	%bl, 2(%rsi)
	jne	.L274
	leaq	3(%rax), %r8
	cmpq	$3, %rcx
	je	.L274
	movzbl	3(%rdi,%rax), %ebx
	cmpb	%bl, 3(%rsi)
	jne	.L274
	leaq	4(%rax), %r8
	cmpq	$4, %rcx
	je	.L274
	movzbl	4(%rdi,%rax), %ebx
	cmpb	%bl, 4(%rsi)
	jne	.L274
	leaq	5(%rax), %r8
	subq	$5, %rcx
	je	.L274
	movzbl	5(%rdi,%rax), %ebx
	cmpb	%bl, 5(%rsi)
	jne	.L274
	leaq	6(%rax), %r8
	cmpq	$1, %rcx
	je	.L274
	movzbl	6(%rdi,%rax), %ebx
	cmpb	%bl, 6(%rsi)
	jne	.L274
	leaq	7(%rax), %r8
.L274:
	movq	%r8, %rax
	jmp	.L272
.L278:
	movq	-1416(%rbp), %rbx
	movzbl	-1368(%rbp), %ecx
	subq	$2113, %rax
	addl	$1, -1188(%rbp)
	movzbl	39(%rbx), %r8d
	movq	-1424(%rbp), %rbx
	movzwl	78(%rbx), %r9d
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%edi, %ecx
	addq	$24, %rdi
	shrq	$3, %rsi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	addq	%r13, %rsi
	salq	%cl, %rax
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L276
.L303:
	movq	%rax, %r8
	jmp	.L274
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE118:
	.size	BrotliCompressFragmentFastImpl13, .-BrotliCompressFragmentFastImpl13
	.p2align 4
	.type	BrotliCompressFragmentFastImpl15, @function
BrotliCompressFragmentFastImpl15:
.LFB119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r12
	movq	%rdx, -1352(%rbp)
	movl	%ecx, -1524(%rbp)
	movq	40(%rbp), %rbx
	movq	%rax, -1424(%rbp)
	movq	32(%rbp), %rax
	movq	%r8, -1456(%rbp)
	movq	48(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r9, -1416(%rbp)
	movq	%rax, -1496(%rbp)
	movl	$98304, %eax
	movq	%rdi, -1472(%rbp)
	movq	%r12, -1544(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpq	$98304, %rdx
	movq	(%rbx), %rdi
	cmovbe	%rdx, %rax
	movq	%r15, %rdx
	addq	$3, %rdi
	movq	%rdi, -1448(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	(%rbx), %rax
	subq	$8, %rsp
	movq	%r14, %rsi
	leaq	-832(%rbp), %r8
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%rax, %rdx
	addq	$13, %rax
	movq	%r8, -1480(%rbp)
	shrq	$3, %rdx
	movq	%rax, (%rbx)
	leaq	-320(%rbp), %rax
	addq	%r15, %rdx
	movq	%rax, -1432(%rbp)
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	%r13, %rdx
	movq	%rax, %rcx
	pushq	%r15
	call	BuildAndStoreLiteralPrefixCode
	movq	(%r12), %rsi
	popq	%rdi
	movq	(%rbx), %rdx
	popq	%r8
	movq	%rax, -1464(%rbp)
	cmpq	$7, %rsi
	jbe	.L406
	movq	-1496(%rbp), %r9
	xorl	%edi, %edi
	movq	%r12, %r8
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%rdx, %rsi
	movq	%rdi, %rax
	movl	%edx, %ecx
	addq	$8, %rdx
	shrq	$3, %rsi
	shrq	$3, %rax
	andl	$7, %ecx
	movzbl	(%r9,%rax), %eax
	addq	%r15, %rsi
	movq	%rdx, (%rbx)
	movzbl	(%rsi), %r10d
	salq	%cl, %rax
	orq	%r10, %rax
	movq	%rax, (%rsi)
	movq	%rdi, %rax
	movq	(%r8), %rsi
	addq	$8, %rdi
	addq	$15, %rax
	cmpq	%rax, %rsi
	ja	.L405
.L406:
	movq	-1496(%rbp), %rcx
	movq	%rdx, %rdi
	movq	%rsi, %rax
	andl	$7, %esi
	shrq	$3, %rdi
	shrq	$3, %rax
	movq	%r14, -1392(%rbp)
	movq	%r15, %r12
	movzbl	(%rcx,%rax), %eax
	addq	%r15, %rdi
	movl	%edx, %ecx
	addq	%rsi, %rdx
	movzbl	(%rdi), %r8d
	andl	$7, %ecx
	movq	%rdx, (%rbx)
	movq	%r14, %r15
	salq	%cl, %rax
	movq	%r13, -1384(%rbp)
	orq	%r8, %rax
	movq	%r14, -1440(%rbp)
	movq	%rax, (%rdi)
	leaq	-1344(%rbp), %rax
	movq	%rax, -1376(%rbp)
	movq	%r14, -1368(%rbp)
	movq	%r13, %r14
	movq	%rbx, %r13
	movq	-1392(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L407:
	leaq	(%rbx,%r14), %rax
	movq	-1376(%rbp), %rdi
	movl	$64, %ecx
	leaq	kCmdHistoSeed(%rip), %rsi
	rep movsq
	movq	%rax, -1360(%rbp)
	cmpq	$15, %r14
	jbe	.L408
	movq	-1352(%rbp), %rax
	leaq	-5(%r14), %rdx
	movabsq	$8503243848024064, %rsi
	movq	%r14, -1408(%rbp)
	movl	$-1, -1400(%rbp)
	movq	-1456(%rbp), %r14
	subq	$16, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	leaq	(%rbx,%rax), %r11
	movq	%rbx, %rax
	leaq	1(%rbx), %rbx
	imulq	1(%rax), %rsi
	leaq	1(%rbx), %rdi
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	shrq	$49, %rsi
	cmpq	%rdi, %r11
	jb	.L552
.L562:
	movslq	-1400(%rbp), %r8
	movl	$33, %ecx
	negq	%r8
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L410:
	movslq	(%rdx), %rax
	movl	%r9d, (%rdx)
	addq	%r15, %rax
	movl	(%rax), %edx
	cmpl	%edx, (%rbx)
	je	.L559
.L412:
	leal	1(%rcx), %eax
	shrl	$5, %ecx
	movq	%rdi, %rbx
	addq	%rdi, %rcx
	cmpq	%rcx, %r11
	jb	.L552
	movq	%rcx, %rdi
	movl	%eax, %ecx
.L409:
	movl	%esi, %edx
	leaq	(%rbx,%r8), %rax
	movq	%rbx, %r9
	movabsq	$8503243848024064, %rsi
	imulq	(%rdi), %rsi
	movl	(%rax), %r10d
	leaq	(%r14,%rdx,4), %rdx
	subq	%r15, %r9
	shrq	$49, %rsi
	cmpl	%r10d, (%rbx)
	jne	.L410
	movzbl	4(%rax), %r10d
	cmpb	%r10b, 4(%rbx)
	jne	.L410
	cmpq	%rbx, %rax
	jnb	.L410
	movl	%r9d, (%rdx)
.L411:
	movq	%rbx, %rdx
	subq	%rax, %rdx
	cmpq	$262128, %rdx
	jg	.L412
	movq	-1360(%rbp), %rsi
	leaq	5(%rbx), %rcx
	leaq	5(%rax), %r9
	subq	%rbx, %rsi
	subq	$5, %rsi
	movq	%rsi, %r10
	shrq	$3, %r10
	je	.L413
	leaq	13(%rbx), %rcx
	xorl	%edi, %edi
	movq	%rcx, -1488(%rbp)
.L415:
	movq	5(%rbx,%rdi), %rcx
	movq	5(%rax,%rdi), %r8
	cmpq	%r8, %rcx
	je	.L560
	xorq	%r8, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rdi, %rax
	movq	%rax, -1488(%rbp)
.L416:
	movq	-1488(%rbp), %rax
	movq	%rbx, %rdi
	subq	-1368(%rbp), %rdi
	movq	%rdi, -1512(%rbp)
	addq	$5, %rax
	movq	%rax, -1520(%rbp)
	addq	%rbx, %rax
	movq	%rax, -1504(%rbp)
	cmpq	$6209, %rdi
	ja	.L419
	movq	(%r12), %rax
	movq	%rdi, %rbx
	movq	%rax, %rsi
	movl	%eax, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r13, %rsi
	cmpq	$5, %rdi
	ja	.L420
	movq	-1416(%rbp), %rcx
	movzbl	(%rsi), %r9d
	movzbl	40(%rcx,%rdi), %r8d
	movq	-1424(%rbp), %rdi
	movl	%r10d, %ecx
	movzwl	80(%rdi,%rbx,2), %edi
	addq	%r8, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdi
	orq	%rdi, %r9
	movq	-1376(%rbp), %rdi
	movq	%r9, (%rsi)
	addl	$1, 160(%rdi,%rbx,4)
	testq	%rbx, %rbx
	je	.L422
.L421:
	movq	%r11, -1536(%rbp)
	movq	-1512(%rbp), %rbx
	xorl	%r8d, %r8d
	movq	-1368(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L429:
	movzbl	(%r11,%r8), %ecx
	movq	%rax, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %esi
	addq	%r13, %rdi
	movl	%eax, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rsi
	addq	%r9, %rax
	orq	%rsi, %r10
	movq	%rax, (%r12)
	movq	%r10, (%rdi)
	cmpq	%r8, %rbx
	ja	.L429
	movq	-1536(%rbp), %r11
.L422:
	movq	%rax, %r10
	shrq	$3, %r10
	addq	%r13, %r10
	movzbl	(%r10), %ebx
	movq	%rbx, -1368(%rbp)
	movl	%eax, %ebx
	andl	$7, %ebx
	movl	%ebx, -1512(%rbp)
	cmpl	%edx, -1400(%rbp)
	je	.L561
	movslq	%edx, %rdi
	movq	-1416(%rbp), %rbx
	addq	$3, %rdi
	bsrl	%edi, %r8d
	movq	%rdi, %r9
	leal	-1(%r8), %esi
	leal	-4(%r8,%r8), %r8d
	movl	%esi, %ecx
	shrq	%cl, %r9
	movq	-1424(%rbp), %rcx
	andl	$1, %r9d
	addq	%r9, %r8
	addq	$2, %r9
	movzbl	80(%rbx,%r8), %ebx
	movq	%rbx, -1400(%rbp)
	addq	-1400(%rbp), %rax
	movzwl	160(%rcx,%r8,2), %ebx
	movzbl	-1512(%rbp), %ecx
	movq	%rax, (%r12)
	movl	%edx, -1400(%rbp)
	salq	%cl, %rbx
	movq	-1368(%rbp), %rcx
	orq	%rbx, %rcx
	movq	%rcx, (%r10)
	movq	%rax, %r10
	movl	%esi, %ecx
	shrq	$3, %r10
	salq	%cl, %r9
	movl	%eax, %ecx
	addq	%rsi, %rax
	addq	%r13, %r10
	subq	%r9, %rdi
	andl	$7, %ecx
	movq	%rax, (%r12)
	movzbl	(%r10), %ebx
	salq	%cl, %rdi
	orq	%rdi, %rbx
	movq	%rbx, (%r10)
	movq	-1376(%rbp), %rbx
	addl	$1, 320(%rbx,%r8,4)
.L431:
	movq	%rax, %rdx
	movq	-1520(%rbp), %rbx
	movl	%eax, %edi
	shrq	$3, %rdx
	andl	$7, %edi
	addq	%r13, %rdx
	movzbl	(%rdx), %esi
	cmpq	$11, %rbx
	ja	.L432
	movq	-1416(%rbp), %rcx
	movq	-1488(%rbp), %r10
	movzbl	1(%rcx,%r10), %r9d
	movq	-1424(%rbp), %rcx
	addq	%r9, %rax
	movzwl	-8(%rcx,%rbx,2), %r8d
	movl	%edi, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	salq	%cl, %r8
	orq	%r8, %rsi
	addl	$1, -16(%rax,%rbx,4)
	movq	%rsi, (%rdx)
.L433:
	movq	-1504(%rbp), %rbx
	cmpq	%rbx, %r11
	jbe	.L472
	movq	-3(%rbx), %rax
	movq	%rbx, %rcx
	movabsq	$8503243848024064, %rdx
	movabsq	$8503243848024064, %rdi
	subq	%r15, %rcx
	imulq	%rax, %rdx
	leal	-3(%rcx), %esi
	shrq	$49, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	leal	-2(%rcx), %esi
	shrq	$8, %rdx
	imulq	%rdi, %rdx
	shrq	$49, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	shrq	$24, %rax
	leal	-1(%rcx), %esi
	imulq	%rdi, %rax
	shrq	$16, %rdx
	imulq	%rdi, %rdx
	shrq	$49, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$49, %rdx
	movl	%esi, (%r14,%rdx,4)
	movslq	(%rax), %r10
	movq	%rbx, %rdx
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rbx)
	je	.L437
.L438:
	movq	-1504(%rbp), %rax
	movabsq	$8503243848024064, %rsi
	imulq	1(%rax), %rsi
	leaq	1(%rax), %rbx
	movq	%rax, -1368(%rbp)
	leaq	1(%rbx), %rdi
	shrq	$49, %rsi
	cmpq	%rdi, %r11
	jnb	.L562
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r13, %rax
	movq	-1408(%rbp), %r14
	movq	%r12, %r13
	movq	%rax, %r12
.L408:
	subq	%r14, -1352(%rbp)
	movq	-1352(%rbp), %rdi
	je	.L450
	cmpq	$65536, %rdi
	movl	$65536, %eax
	cmovbe	%rdi, %rax
	addq	%rax, -1384(%rbp)
	movq	%rax, %r14
	movq	-1384(%rbp), %rax
	cmpq	$1048576, %rax
	jbe	.L563
.L451:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	ja	.L467
.L468:
	movq	-1352(%rbp), %rax
	movl	$98304, %r14d
	movq	%r12, %rdx
	movq	%r13, %rsi
	cmpq	$98304, %rax
	cmovbe	%rax, %r14
	movq	0(%r13), %rax
	movq	%r14, %rdi
	addq	$3, %rax
	movq	%rax, -1448(%rbp)
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	0(%r13), %rax
	subq	$8, %rsp
	movq	%r13, %r9
	movq	-1360(%rbp), %rbx
	movq	-1480(%rbp), %r8
	movq	%rax, %rdx
	addq	$13, %rax
	movq	-1472(%rbp), %rdi
	shrq	$3, %rdx
	movq	%rax, 0(%r13)
	movq	%rbx, %rsi
	addq	%r12, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	-1432(%rbp), %rcx
	movq	%r14, %rdx
	pushq	%r12
	call	BuildAndStoreLiteralPrefixCode
	movq	-1424(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	-1416(%rbp), %rsi
	movq	-1376(%rbp), %rdi
	movq	%rax, -1464(%rbp)
	call	BuildAndStoreCommandPrefixCode
	popq	%rdx
	popq	%rcx
	movq	%r14, -1384(%rbp)
	movq	%rbx, -1440(%rbp)
	movq	%rbx, -1368(%rbp)
	movq	%rbx, -1392(%rbp)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L559:
	movzbl	4(%rax), %edx
	cmpb	%dl, 4(%rbx)
	jne	.L412
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L450:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	jbe	.L465
.L467:
	movq	-1360(%rbp), %rdx
	subq	-1368(%rbp), %rdx
	cmpq	$6209, %rdx
	ja	.L454
	movq	0(%r13), %rdi
	movq	%rdi, %rsi
	movl	%edi, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	cmpq	$5, %rdx
	ja	.L455
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	movzbl	40(%rax,%rdx), %r11d
	movq	-1424(%rbp), %rax
	movzwl	80(%rax,%rdx,2), %eax
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rax
	orq	%rax, %r9
	movq	-1376(%rbp), %rax
	movq	%r9, (%rsi)
	addl	$1, 160(%rax,%rdx,4)
	testq	%rdx, %rdx
	je	.L427
.L456:
	movq	-1368(%rbp), %r10
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L460:
	movzbl	(%r10,%r8), %ecx
	movq	%rdi, %rsi
	addq	$1, %r8
	shrq	$3, %rsi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rsi
	movl	%edi, %ecx
	movzbl	(%rsi), %r11d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rdi
	orq	%r11, %rax
	movq	%rdi, 0(%r13)
	movq	%rax, (%rsi)
	cmpq	%r8, %rdx
	ja	.L460
.L427:
	cmpq	$0, -1352(%rbp)
	jne	.L468
	.p2align 4,,10
	.p2align 3
.L465:
	movl	-1524(%rbp), %eax
	testl	%eax, %eax
	je	.L564
.L403:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L565
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	cmpq	$129, %rdx
	ja	.L457
	leaq	-2(%rdx), %r8
	movq	-1416(%rbp), %rbx
	bsrl	%r8d, %eax
	movq	%r8, %r11
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %r11
	leal	(%rax,%rax), %ecx
	leaq	42(%r11,%rcx), %r14
	movq	-1424(%rbp), %rcx
	movzbl	(%rbx,%r14), %ebx
	movq	%rbx, -1384(%rbp)
	movzwl	(%rcx,%r14,2), %ebx
	movl	%r10d, %ecx
	addq	-1384(%rbp), %rdi
	salq	%cl, %rbx
	movl	%eax, %ecx
	movq	%rdi, 0(%r13)
	orq	%rbx, %r9
	salq	%cl, %r11
	movl	%edi, %ecx
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	subq	%r11, %r8
	andl	$7, %ecx
	shrq	$3, %rsi
	addq	%rax, %rdi
	salq	%cl, %r8
	movq	-1376(%rbp), %rax
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	orq	%r8, %r9
	movq	%r9, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L563:
	movq	-1432(%rbp), %rdx
	movq	-1360(%rbp), %rdi
	movq	%r14, %rsi
	call	ShouldMergeBlock
	testl	%eax, %eax
	je	.L451
	movl	-1384(%rbp), %eax
	movl	$20, %edi
	movl	$1, %ebx
	movq	-1448(%rbp), %r10
	leal	-1(%rax), %r11d
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r10, %r9
	movl	$8, %eax
	movl	%ebx, %edx
	movl	%ebx, %esi
	andl	$7, %r9d
	movq	%r10, %r8
	subq	%r9, %rax
	cmpq	%rdi, %rax
	cmova	%rdi, %rax
	shrq	$3, %r8
	addq	%r12, %r8
	leaq	(%r9,%rax), %rcx
	addq	%rax, %r10
	sall	%cl, %edx
	movl	%r9d, %ecx
	sall	%cl, %esi
	negl	%edx
	movl	%esi, %ecx
	movl	$-1, %esi
	subl	$1, %ecx
	orl	%ecx, %edx
	movl	%eax, %ecx
	andb	(%r8), %dl
	sall	%cl, %esi
	movl	%r9d, %ecx
	notl	%esi
	andl	%r11d, %esi
	sall	%cl, %esi
	movl	%eax, %ecx
	orl	%edx, %esi
	shrl	%cl, %r11d
	movb	%sil, (%r8)
	subq	%rax, %rdi
	jne	.L452
	movq	-1360(%rbp), %rax
	movq	%rax, -1392(%rbp)
	movq	%rax, %rbx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L457:
	cmpq	$2113, %rdx
	ja	.L459
	leaq	-66(%rdx), %r8
	movq	-1416(%rbp), %rbx
	movl	%r10d, %ecx
	bsrl	%r8d, %eax
	leal	50(%rax), %r14d
	movzbl	(%rbx,%r14), %r11d
	movq	-1424(%rbp), %rbx
	movzwl	(%rbx,%r14,2), %ebx
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rbx
	movl	%eax, %ecx
	movl	%eax, %eax
	orq	%rbx, %r9
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	movl	$1, %r9d
	shrq	$3, %rsi
	salq	%cl, %r9
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r12, %rsi
	subq	%r9, %r8
	andl	$7, %ecx
	movq	-1376(%rbp), %rax
	movzbl	(%rsi), %r10d
	salq	%cl, %r8
	orq	%r8, %r10
	movq	%r10, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L456
.L459:
	movq	-1424(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzwl	122(%rbx), %r8d
	movzbl	61(%rax), %eax
	salq	%cl, %r8
	addq	%rax, %rdi
	leaq	-2114(%rdx), %rax
	orq	%r8, %r9
	movl	%edi, %ecx
	movq	%rdi, 0(%r13)
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	andl	$7, %ecx
	addq	$12, %rdi
	shrq	$3, %rsi
	salq	%cl, %rax
	movq	%rdi, 0(%r13)
	addq	%r12, %rsi
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L456
.L420:
	cmpq	$129, %rdi
	jbe	.L566
	cmpq	$2113, %rdi
	ja	.L425
	subq	$66, %rbx
	movq	-1416(%rbp), %rdi
	movq	-1424(%rbp), %rcx
	movq	%rbx, -1536(%rbp)
	movzbl	(%rsi), %r8d
	bsrl	-1536(%rbp), %ebx
	leal	50(%rbx), %r9d
	movzbl	(%rdi,%r9), %edi
	movq	%rdi, -1552(%rbp)
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	-1552(%rbp), %rdi
	movq	%r8, (%rsi)
	leaq	(%rdi,%rax), %rsi
	movl	$1, %eax
	movq	%rsi, %rdi
	movq	%rsi, (%r12)
	shrq	$3, %rdi
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
.L558:
	movl	%ebx, %ecx
	salq	%cl, %rax
	movq	-1536(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movl	%esi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movl	%ebx, %eax
	movq	-1376(%rbp), %rbx
	addq	%rsi, %rax
	movq	%r8, (%rdi)
	movq	%rax, (%r12)
	addl	$1, (%rbx,%r9,4)
	jmp	.L421
.L569:
	movq	-1360(%rbp), %rcx
	xorl	%eax, %eax
	leaq	5(%rdx), %rsi
	leaq	5(%r10), %rdi
	leaq	13(%rdx), %rbx
	subq	%rdx, %rcx
	subq	$5, %rcx
	movq	%rcx, %r8
	shrq	$3, %r8
	je	.L441
.L440:
	movq	5(%rdx,%rax), %rsi
	movq	5(%r10,%rax), %r9
	cmpq	%r9, %rsi
	je	.L567
	xorq	%r9, %rsi
	xorl	%ecx, %ecx
	rep bsfq	%rsi, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%rcx, %rax
.L442:
	movq	%rdx, %rbx
	leaq	5(%rax), %rcx
	subq	%r10, %rbx
	movq	%rbx, -1504(%rbp)
	cmpq	$262128, %rbx
	jg	.L555
	movq	(%r12), %rdi
	movl	%ebx, -1400(%rbp)
	addq	%rcx, %rdx
	movq	%rdi, %rsi
	movl	%edi, %ebx
	shrq	$3, %rsi
	andl	$7, %ebx
	addq	%r13, %rsi
	movl	%ebx, -1368(%rbp)
	movzbl	(%rsi), %r10d
	cmpq	$9, %rcx
	jbe	.L568
	cmpq	$133, %rcx
	ja	.L447
	leaq	-1(%rax), %r9
	bsrl	%r9d, %eax
	movq	%r9, %rbx
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %rbx
	leal	(%rax,%rax), %ecx
	movq	%rbx, -1488(%rbp)
	leaq	20(%rbx,%rcx), %rbx
	movq	-1416(%rbp), %rcx
	movzbl	(%rcx,%rbx), %ecx
	movq	%rcx, -1512(%rbp)
	movq	-1424(%rbp), %rcx
	addq	-1512(%rbp), %rdi
	movzwl	(%rcx,%rbx,2), %r8d
	movzbl	-1368(%rbp), %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movq	-1488(%rbp), %r10
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%eax, %ecx
	shrq	$3, %rsi
	salq	%cl, %r10
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r13, %rsi
	movq	-1376(%rbp), %rax
	subq	%r10, %r9
	andl	$7, %ecx
	movzbl	(%rsi), %r8d
	salq	%cl, %r9
	movq	%rdi, (%r12)
	addl	$1, (%rax,%rbx,4)
	orq	%r9, %r8
	movq	%r8, (%rsi)
.L446:
	movslq	-1504(%rbp), %rax
	movq	%rdi, %r10
	movq	-1424(%rbp), %r8
	shrq	$3, %r10
	addq	$3, %rax
	addq	%r13, %r10
	bsrl	%eax, %esi
	movq	%rax, %rbx
	leal	-1(%rsi), %r9d
	leal	-4(%rsi,%rsi), %esi
	movl	%r9d, %ecx
	shrq	%cl, %rbx
	movq	-1416(%rbp), %rcx
	andl	$1, %ebx
	addq	%rbx, %rsi
	movq	%rbx, -1368(%rbp)
	movzbl	(%r10), %ebx
	movzwl	160(%r8,%rsi,2), %r8d
	movzbl	80(%rcx,%rsi), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	addq	-1488(%rbp), %rdi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movl	%r9d, %ecx
	orq	%r8, %rbx
	movq	%rdi, %r8
	movq	%rbx, (%r10)
	movq	-1368(%rbp), %rbx
	shrq	$3, %r8
	addq	%r13, %r8
	addq	$2, %rbx
	movzbl	(%r8), %r10d
	salq	%cl, %rbx
	movl	%edi, %ecx
	addq	%r9, %rdi
	subq	%rbx, %rax
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %rax
	orq	%rax, %r10
	movq	-1376(%rbp), %rax
	movq	%r10, (%r8)
	addl	$1, 320(%rax,%rsi,4)
	cmpq	%rdx, %r11
	jbe	.L474
	movq	-3(%rdx), %rax
	movq	%rdx, %rcx
	movabsq	$8503243848024064, %rsi
	movabsq	$8503243848024064, %rbx
	subq	%r15, %rcx
	imulq	%rax, %rsi
	leal	-3(%rcx), %edi
	shrq	$49, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	leal	-2(%rcx), %edi
	shrq	$8, %rsi
	imulq	%rbx, %rsi
	shrq	$49, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	shrq	$24, %rax
	leal	-1(%rcx), %edi
	imulq	%rbx, %rax
	shrq	$16, %rsi
	imulq	%rbx, %rsi
	shrq	$49, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$49, %rsi
	movl	%edi, (%r14,%rsi,4)
	movslq	(%rax), %r10
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rdx)
	jne	.L555
.L437:
	movzbl	4(%r10), %eax
	cmpb	%al, 4(%rdx)
	je	.L569
.L555:
	movq	%rdx, -1504(%rbp)
	jmp	.L438
.L454:
	movq	-1368(%rbp), %rax
	movq	-1440(%rbp), %rdi
	subq	%rdi, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, %rdx
	jb	.L461
	cmpq	$980, -1464(%rbp)
	ja	.L570
.L461:
	movq	0(%r13), %rsi
	movq	-1416(%rbp), %rbx
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$3, %rax
	andl	$7, %ecx
	addq	%r12, %rax
	movzbl	(%rax), %r8d
	cmpq	$22593, %rdx
	jbe	.L571
	movzbl	63(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1092(%rbp)
	movzwl	126(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$24, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-22594(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
.L463:
	movq	-1368(%rbp), %r8
	addq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L464:
	movzbl	(%r8), %ecx
	movq	%rsi, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rdi
	movl	%esi, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rsi
	orq	%r10, %rax
	movq	%rsi, 0(%r13)
	movq	%rax, (%rdi)
	cmpq	%rdx, %r8
	jne	.L464
	cmpq	$0, -1352(%rbp)
	jne	.L468
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L432:
	cmpq	$71, -1520(%rbp)
	ja	.L434
	movq	-1488(%rbp), %r8
	movq	-1424(%rbp), %r9
	subq	$3, %r8
	bsrl	%r8d, %ebx
	movq	%r8, %r10
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %r10
	leal	(%rbx,%rbx), %ecx
	movq	%r10, -1368(%rbp)
	leaq	4(%r10,%rcx), %r10
	movq	-1416(%rbp), %rcx
	movzwl	(%r9,%r10,2), %r9d
	movzbl	(%rcx,%r10), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	movq	-1368(%rbp), %rdi
	salq	%cl, %r9
	addq	-1488(%rbp), %rax
	movl	%ebx, %ecx
	orq	%r9, %rsi
	salq	%cl, %rdi
	movq	%rax, (%r12)
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movq	%rdi, %rcx
	movq	%r8, %rdi
	shrq	$3, %rdx
	subq	%rcx, %rdi
	movl	%eax, %ecx
	addq	%rbx, %rax
	addq	%r13, %rdx
	andl	$7, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	movzbl	(%rdx), %esi
	salq	%cl, %rdi
	addl	$1, (%rax,%r10,4)
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	jmp	.L433
.L564:
	movq	-1544(%rbp), %rax
	movq	-1496(%rbp), %r8
	movq	-1424(%rbp), %rdx
	movq	-1416(%rbp), %rsi
	movb	$0, (%r8)
	movq	-1376(%rbp), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	call	BuildAndStoreCommandPrefixCode
	jmp	.L403
.L561:
	movq	-1416(%rbp), %rbx
	movzbl	-1512(%rbp), %ecx
	addl	$1, -1088(%rbp)
	movzbl	64(%rbx), %esi
	movq	-1424(%rbp), %rbx
	movzwl	128(%rbx), %edx
	addq	%rsi, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdx
	orq	-1368(%rbp), %rdx
	movq	%rdx, (%r10)
	jmp	.L431
.L434:
	movq	-1424(%rbp), %rcx
	cmpq	$135, -1520(%rbp)
	movzwl	128(%rcx), %ebx
	jbe	.L572
	cmpq	$2119, -1520(%rbp)
	ja	.L436
	movq	%rcx, %r9
	movq	-1488(%rbp), %rcx
	movq	-1416(%rbp), %r8
	subq	$67, %rcx
	movq	%rcx, -1368(%rbp)
	movl	%edi, %ecx
	bsrl	-1368(%rbp), %r10d
	movl	%r10d, -1488(%rbp)
	addl	$28, %r10d
	movl	-1488(%rbp), %edi
	movzbl	(%r8,%r10), %r8d
	movzwl	(%r9,%r10,2), %r9d
	addq	%rax, %r8
	salq	%cl, %r9
	movl	$1, %eax
	movl	%edi, %ecx
	salq	%cl, %rax
	orq	%r9, %rsi
	movq	-1368(%rbp), %rcx
	movq	%r8, (%r12)
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	addq	%r13, %rdx
	movq	%rcx, %rax
	movl	%r8d, %ecx
	movzbl	(%rdx), %esi
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %rsi
	movl	%edi, %eax
	addq	%rax, %r8
	movq	-1416(%rbp), %rax
	movq	%rsi, (%rdx)
	movq	%r8, (%r12)
	movl	%r8d, %ecx
	movzbl	64(%rax), %esi
	movq	%r8, %rax
	andl	$7, %ecx
	shrq	$3, %rax
	salq	%cl, %rbx
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edx
	movq	%r8, (%r12)
	orq	%rbx, %rdx
	movq	%rdx, (%rax)
	movq	-1376(%rbp), %rax
	addl	$1, (%rax,%r10,4)
	addl	$1, -1088(%rbp)
	jmp	.L433
.L566:
	leaq	-2(%rdi), %rdi
	movzbl	(%rsi), %r8d
	bsrl	%edi, %ebx
	movq	%rdi, -1536(%rbp)
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %rdi
	leal	(%rbx,%rbx), %ecx
	leaq	42(%rdi,%rcx), %r9
	movq	-1416(%rbp), %rcx
	movq	%rdi, -1552(%rbp)
	movzbl	(%rcx,%r9), %ecx
	movq	%rcx, -1560(%rbp)
	movq	-1424(%rbp), %rcx
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	%r8, (%rsi)
	movq	-1560(%rbp), %rsi
	addq	%rax, %rsi
	movq	%rsi, %rax
	movq	%rsi, (%r12)
	shrq	$3, %rax
	movq	%rax, %rdi
	movq	-1552(%rbp), %rax
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
	jmp	.L558
.L571:
	movzbl	62(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1096(%rbp)
	movzwl	124(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$14, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-6210(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
	jmp	.L463
.L570:
	movq	-1448(%rbp), %rdx
	movq	-1360(%rbp), %rsi
	movq	%r12, %r8
	movq	%r13, %rcx
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	cmpq	$0, -1352(%rbp)
	jne	.L468
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L572:
	movq	-1488(%rbp), %r8
	movq	-1416(%rbp), %r9
	subq	$3, %r8
	movq	%r8, %r10
	shrq	$5, %r10
	movzbl	30(%r9,%r10), %r9d
	movq	%r9, -1368(%rbp)
	movzwl	60(%rcx,%r10,2), %r9d
	movl	%edi, %ecx
	movq	%r8, %rdi
	addq	-1368(%rbp), %rax
	andl	$31, %edi
	salq	%cl, %r9
	movl	%eax, %ecx
	movq	%rax, (%r12)
	orq	%r9, %rsi
	andl	$7, %ecx
	movq	-1416(%rbp), %r9
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	salq	%cl, %rdi
	addq	$5, %rax
	shrq	$3, %rdx
	movq	%rax, (%r12)
	movl	%eax, %ecx
	addq	%r13, %rdx
	andl	$7, %ecx
	movzbl	(%rdx), %esi
	salq	%cl, %rbx
	movq	%rbx, %rcx
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movzbl	64(%r9), %esi
	shrq	$3, %rdx
	addq	%r13, %rdx
	addq	%rsi, %rax
	movzbl	(%rdx), %edi
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	orq	%rdi, %rcx
	addl	$1, 120(%rax,%r10,4)
	movq	%rcx, (%rdx)
	addl	$1, -1088(%rbp)
	jmp	.L433
.L560:
	movq	-1488(%rbp), %rcx
	addq	%rdi, %rcx
	addq	$8, %rdi
	subq	$1, %r10
	jne	.L415
	movq	%rdi, %r10
.L413:
	andl	$7, %esi
	je	.L470
	movzbl	(%r9,%r10), %eax
	cmpb	%al, (%rcx)
	jne	.L470
	leaq	1(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L416
	movzbl	1(%r9,%r10), %eax
	cmpb	%al, 1(%rcx)
	jne	.L416
	leaq	2(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$2, %rsi
	je	.L416
	movzbl	2(%r9,%r10), %eax
	cmpb	%al, 2(%rcx)
	jne	.L416
	leaq	3(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$3, %rsi
	je	.L416
	movzbl	3(%r9,%r10), %eax
	cmpb	%al, 3(%rcx)
	jne	.L416
	leaq	4(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$4, %rsi
	je	.L416
	movzbl	4(%r9,%r10), %eax
	cmpb	%al, 4(%rcx)
	jne	.L416
	leaq	5(%r10), %rax
	movq	%rax, -1488(%rbp)
	subq	$5, %rsi
	je	.L416
	movzbl	5(%r9,%r10), %eax
	cmpb	%al, 5(%rcx)
	jne	.L416
	leaq	6(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L416
	movzbl	6(%r9,%r10), %eax
	cmpb	%al, 6(%rcx)
	jne	.L416
	leaq	7(%r10), %rax
	movq	%rax, -1488(%rbp)
	jmp	.L416
.L425:
	movq	-1416(%rbp), %rbx
	movzbl	(%rsi), %r8d
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzbl	61(%rbx), %r9d
	movq	-1424(%rbp), %rbx
	movzwl	122(%rbx), %edi
	addq	%r9, %rax
	movq	-1512(%rbp), %rbx
	movq	%rax, (%r12)
	salq	%cl, %rdi
	movl	%eax, %ecx
	orq	%rdi, %r8
	movq	%rax, %rdi
	andl	$7, %ecx
	addq	$12, %rax
	shrq	$3, %rdi
	movq	%r8, (%rsi)
	leaq	-2114(%rbx), %rsi
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L421
.L472:
	movq	%r13, %rax
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	movq	-1504(%rbp), %rax
	movq	%rax, -1368(%rbp)
	jmp	.L408
.L419:
	movq	-1368(%rbp), %rax
	subq	-1440(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, -1512(%rbp)
	jb	.L426
	cmpq	$980, -1464(%rbp)
	jbe	.L426
	movq	-1448(%rbp), %rdx
	movq	%r13, %rax
	movq	-1440(%rbp), %rdi
	movq	%r12, %r13
	movq	%rax, %r8
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%rax, %r12
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	movq	%rbx, %rax
	movq	%rbx, -1360(%rbp)
	subq	-1392(%rbp), %rax
	subq	%rax, -1352(%rbp)
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L426:
	movq	(%r12), %r8
	movq	-1512(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movq	%r8, %rdi
	movl	%r8d, %ecx
	shrq	$3, %rdi
	andl	$7, %ecx
	addq	%r13, %rdi
	cmpq	$22593, %rbx
	ja	.L428
	movzbl	62(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1096(%rbp)
	movzwl	124(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-6210(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$14, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L421
.L436:
	movq	-1416(%rbp), %r10
	movzwl	78(%rcx), %r9d
	movl	%edi, %ecx
	movq	-1488(%rbp), %rdi
	addl	$1, -1188(%rbp)
	movzbl	39(%r10), %r8d
	salq	%cl, %r9
	addl	$1, -1088(%rbp)
	orq	%r9, %rsi
	subq	$2115, %rdi
	addq	%rax, %r8
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	movl	%r8d, %ecx
	movq	%r8, (%r12)
	addq	$24, %r8
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%r8, (%r12)
	addq	%r13, %rdx
	salq	%cl, %rdi
	movl	%r8d, %ecx
	movzbl	(%rdx), %eax
	andl	$7, %ecx
	salq	%cl, %rbx
	orq	%rdi, %rax
	movq	%rax, (%rdx)
	movq	%r8, %rax
	movzbl	64(%r10), %esi
	movq	%rbx, %rdx
	shrq	$3, %rax
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edi
	movq	%r8, (%r12)
	orq	%rdi, %rdx
	movq	%rdx, (%rax)
	jmp	.L433
.L568:
	movq	-1416(%rbp), %rcx
	movzbl	19(%rcx,%rax), %r8d
	movq	-1424(%rbp), %rcx
	movzwl	38(%rcx,%rax,2), %r9d
	movl	%ebx, %ecx
	movq	-1376(%rbp), %rbx
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	addl	$1, 76(%rbx,%rax,4)
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	jmp	.L446
.L428:
	movzbl	63(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1092(%rbp)
	movzwl	126(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-22594(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$24, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L421
.L447:
	cmpq	$2117, %rcx
	ja	.L448
	subq	$65, %rax
	movq	-1416(%rbp), %r8
	movzbl	-1368(%rbp), %ecx
	movq	%rax, -1488(%rbp)
	bsrl	-1488(%rbp), %ebx
	leal	28(%rbx), %r9d
	movzbl	(%r8,%r9), %eax
	movq	-1424(%rbp), %r8
	movzwl	(%r8,%r9,2), %r8d
	addq	%rax, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movl	$1, %r10d
	movq	%rcx, (%rsi)
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	salq	%cl, %r10
	movq	-1488(%rbp), %rcx
	shrq	$3, %rsi
	addq	%r13, %rsi
	subq	%r10, %rcx
	movzbl	(%rsi), %r8d
	movq	%rcx, %rax
	movl	%edi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movq	-1376(%rbp), %rax
	movq	%r8, (%rsi)
	movl	%ebx, %r8d
	addq	%r8, %rdi
	addl	$1, (%rax,%r9,4)
	movq	%rdi, (%r12)
	jmp	.L446
.L470:
	movq	%r10, -1488(%rbp)
	jmp	.L416
.L474:
	movq	%r13, %rax
	movq	%rdx, -1368(%rbp)
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	jmp	.L408
.L567:
	leaq	(%rbx,%rax), %rsi
	addq	$8, %rax
	subq	$1, %r8
	jne	.L440
.L441:
	andl	$7, %ecx
	je	.L442
	movzbl	(%rdi,%rax), %ebx
	cmpb	%bl, (%rsi)
	jne	.L473
	leaq	1(%rax), %r8
	cmpq	$1, %rcx
	je	.L444
	movzbl	1(%rdi,%rax), %ebx
	cmpb	%bl, 1(%rsi)
	jne	.L444
	leaq	2(%rax), %r8
	cmpq	$2, %rcx
	je	.L444
	movzbl	2(%rdi,%rax), %ebx
	cmpb	%bl, 2(%rsi)
	jne	.L444
	leaq	3(%rax), %r8
	cmpq	$3, %rcx
	je	.L444
	movzbl	3(%rdi,%rax), %ebx
	cmpb	%bl, 3(%rsi)
	jne	.L444
	leaq	4(%rax), %r8
	cmpq	$4, %rcx
	je	.L444
	movzbl	4(%rdi,%rax), %ebx
	cmpb	%bl, 4(%rsi)
	jne	.L444
	leaq	5(%rax), %r8
	subq	$5, %rcx
	je	.L444
	movzbl	5(%rdi,%rax), %ebx
	cmpb	%bl, 5(%rsi)
	jne	.L444
	leaq	6(%rax), %r8
	cmpq	$1, %rcx
	je	.L444
	movzbl	6(%rdi,%rax), %ebx
	cmpb	%bl, 6(%rsi)
	jne	.L444
	leaq	7(%rax), %r8
.L444:
	movq	%r8, %rax
	jmp	.L442
.L448:
	movq	-1416(%rbp), %rbx
	movzbl	-1368(%rbp), %ecx
	subq	$2113, %rax
	addl	$1, -1188(%rbp)
	movzbl	39(%rbx), %r8d
	movq	-1424(%rbp), %rbx
	movzwl	78(%rbx), %r9d
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%edi, %ecx
	addq	$24, %rdi
	shrq	$3, %rsi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	addq	%r13, %rsi
	salq	%cl, %rax
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L446
.L473:
	movq	%rax, %r8
	jmp	.L444
.L565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE119:
	.size	BrotliCompressFragmentFastImpl15, .-BrotliCompressFragmentFastImpl15
	.p2align 4
	.type	BrotliCompressFragmentFastImpl9, @function
BrotliCompressFragmentFastImpl9:
.LFB116:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r12
	movq	%rdx, -1352(%rbp)
	movl	%ecx, -1524(%rbp)
	movq	40(%rbp), %rbx
	movq	%rax, -1424(%rbp)
	movq	32(%rbp), %rax
	movq	%r8, -1456(%rbp)
	movq	48(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r9, -1416(%rbp)
	movq	%rax, -1496(%rbp)
	movl	$98304, %eax
	movq	%rdi, -1472(%rbp)
	movq	%r12, -1544(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpq	$98304, %rdx
	movq	(%rbx), %rdi
	cmovbe	%rdx, %rax
	movq	%r15, %rdx
	addq	$3, %rdi
	movq	%rdi, -1448(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r13
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	(%rbx), %rax
	subq	$8, %rsp
	movq	%r14, %rsi
	leaq	-832(%rbp), %r8
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%rax, %rdx
	addq	$13, %rax
	movq	%r8, -1480(%rbp)
	shrq	$3, %rdx
	movq	%rax, (%rbx)
	leaq	-320(%rbp), %rax
	addq	%r15, %rdx
	movq	%rax, -1432(%rbp)
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	%r13, %rdx
	movq	%rax, %rcx
	pushq	%r15
	call	BuildAndStoreLiteralPrefixCode
	movq	(%r12), %rsi
	popq	%rdi
	movq	(%rbx), %rdx
	popq	%r8
	movq	%rax, -1464(%rbp)
	cmpq	$7, %rsi
	jbe	.L576
	movq	-1496(%rbp), %r9
	xorl	%edi, %edi
	movq	%r12, %r8
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%rdx, %rsi
	movq	%rdi, %rax
	movl	%edx, %ecx
	addq	$8, %rdx
	shrq	$3, %rsi
	shrq	$3, %rax
	andl	$7, %ecx
	movzbl	(%r9,%rax), %eax
	addq	%r15, %rsi
	movq	%rdx, (%rbx)
	movzbl	(%rsi), %r10d
	salq	%cl, %rax
	orq	%r10, %rax
	movq	%rax, (%rsi)
	movq	%rdi, %rax
	movq	(%r8), %rsi
	addq	$8, %rdi
	addq	$15, %rax
	cmpq	%rax, %rsi
	ja	.L575
.L576:
	movq	-1496(%rbp), %rcx
	movq	%rdx, %rdi
	movq	%rsi, %rax
	andl	$7, %esi
	shrq	$3, %rdi
	shrq	$3, %rax
	movq	%r14, -1392(%rbp)
	movq	%r15, %r12
	movzbl	(%rcx,%rax), %eax
	addq	%r15, %rdi
	movl	%edx, %ecx
	addq	%rsi, %rdx
	movzbl	(%rdi), %r8d
	andl	$7, %ecx
	movq	%rdx, (%rbx)
	movq	%r14, %r15
	salq	%cl, %rax
	movq	%r13, -1384(%rbp)
	orq	%r8, %rax
	movq	%r14, -1440(%rbp)
	movq	%rax, (%rdi)
	leaq	-1344(%rbp), %rax
	movq	%rax, -1376(%rbp)
	movq	%r14, -1368(%rbp)
	movq	%r13, %r14
	movq	%rbx, %r13
	movq	-1392(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L577:
	leaq	(%rbx,%r14), %rax
	movq	-1376(%rbp), %rdi
	movl	$64, %ecx
	leaq	kCmdHistoSeed(%rip), %rsi
	rep movsq
	movq	%rax, -1360(%rbp)
	cmpq	$15, %r14
	jbe	.L578
	movq	-1352(%rbp), %rax
	leaq	-5(%r14), %rdx
	movabsq	$8503243848024064, %rsi
	movq	%r14, -1408(%rbp)
	movl	$-1, -1400(%rbp)
	movq	-1456(%rbp), %r14
	subq	$16, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	leaq	(%rbx,%rax), %r11
	movq	%rbx, %rax
	leaq	1(%rbx), %rbx
	imulq	1(%rax), %rsi
	leaq	1(%rbx), %rdi
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	shrq	$55, %rsi
	cmpq	%rdi, %r11
	jb	.L722
.L732:
	movslq	-1400(%rbp), %r8
	movl	$33, %ecx
	negq	%r8
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L580:
	movslq	(%rdx), %rax
	movl	%r9d, (%rdx)
	addq	%r15, %rax
	movl	(%rax), %edx
	cmpl	%edx, (%rbx)
	je	.L729
.L582:
	leal	1(%rcx), %eax
	shrl	$5, %ecx
	movq	%rdi, %rbx
	addq	%rdi, %rcx
	cmpq	%rcx, %r11
	jb	.L722
	movq	%rcx, %rdi
	movl	%eax, %ecx
.L579:
	movl	%esi, %edx
	leaq	(%rbx,%r8), %rax
	movq	%rbx, %r9
	movabsq	$8503243848024064, %rsi
	imulq	(%rdi), %rsi
	movl	(%rax), %r10d
	leaq	(%r14,%rdx,4), %rdx
	subq	%r15, %r9
	shrq	$55, %rsi
	cmpl	%r10d, (%rbx)
	jne	.L580
	movzbl	4(%rax), %r10d
	cmpb	%r10b, 4(%rbx)
	jne	.L580
	cmpq	%rbx, %rax
	jnb	.L580
	movl	%r9d, (%rdx)
.L581:
	movq	%rbx, %rdx
	subq	%rax, %rdx
	cmpq	$262128, %rdx
	jg	.L582
	movq	-1360(%rbp), %rsi
	leaq	5(%rbx), %rcx
	leaq	5(%rax), %r9
	subq	%rbx, %rsi
	subq	$5, %rsi
	movq	%rsi, %r10
	shrq	$3, %r10
	je	.L583
	leaq	13(%rbx), %rcx
	xorl	%edi, %edi
	movq	%rcx, -1488(%rbp)
.L585:
	movq	5(%rbx,%rdi), %rcx
	movq	5(%rax,%rdi), %r8
	cmpq	%r8, %rcx
	je	.L730
	xorq	%r8, %rcx
	xorl	%eax, %eax
	rep bsfq	%rcx, %rax
	cltq
	shrq	$3, %rax
	addq	%rdi, %rax
	movq	%rax, -1488(%rbp)
.L586:
	movq	-1488(%rbp), %rax
	movq	%rbx, %rdi
	subq	-1368(%rbp), %rdi
	movq	%rdi, -1512(%rbp)
	addq	$5, %rax
	movq	%rax, -1520(%rbp)
	addq	%rbx, %rax
	movq	%rax, -1504(%rbp)
	cmpq	$6209, %rdi
	ja	.L589
	movq	(%r12), %rax
	movq	%rdi, %rbx
	movq	%rax, %rsi
	movl	%eax, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r13, %rsi
	cmpq	$5, %rdi
	ja	.L590
	movq	-1416(%rbp), %rcx
	movzbl	(%rsi), %r9d
	movzbl	40(%rcx,%rdi), %r8d
	movq	-1424(%rbp), %rdi
	movl	%r10d, %ecx
	movzwl	80(%rdi,%rbx,2), %edi
	addq	%r8, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdi
	orq	%rdi, %r9
	movq	-1376(%rbp), %rdi
	movq	%r9, (%rsi)
	addl	$1, 160(%rdi,%rbx,4)
	testq	%rbx, %rbx
	je	.L592
.L591:
	movq	%r11, -1536(%rbp)
	movq	-1512(%rbp), %rbx
	xorl	%r8d, %r8d
	movq	-1368(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L599:
	movzbl	(%r11,%r8), %ecx
	movq	%rax, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %esi
	addq	%r13, %rdi
	movl	%eax, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rsi
	addq	%r9, %rax
	orq	%rsi, %r10
	movq	%rax, (%r12)
	movq	%r10, (%rdi)
	cmpq	%r8, %rbx
	ja	.L599
	movq	-1536(%rbp), %r11
.L592:
	movq	%rax, %r10
	shrq	$3, %r10
	addq	%r13, %r10
	movzbl	(%r10), %ebx
	movq	%rbx, -1368(%rbp)
	movl	%eax, %ebx
	andl	$7, %ebx
	movl	%ebx, -1512(%rbp)
	cmpl	%edx, -1400(%rbp)
	je	.L731
	movslq	%edx, %rdi
	movq	-1416(%rbp), %rbx
	addq	$3, %rdi
	bsrl	%edi, %r8d
	movq	%rdi, %r9
	leal	-1(%r8), %esi
	leal	-4(%r8,%r8), %r8d
	movl	%esi, %ecx
	shrq	%cl, %r9
	movq	-1424(%rbp), %rcx
	andl	$1, %r9d
	addq	%r9, %r8
	addq	$2, %r9
	movzbl	80(%rbx,%r8), %ebx
	movq	%rbx, -1400(%rbp)
	addq	-1400(%rbp), %rax
	movzwl	160(%rcx,%r8,2), %ebx
	movzbl	-1512(%rbp), %ecx
	movq	%rax, (%r12)
	movl	%edx, -1400(%rbp)
	salq	%cl, %rbx
	movq	-1368(%rbp), %rcx
	orq	%rbx, %rcx
	movq	%rcx, (%r10)
	movq	%rax, %r10
	movl	%esi, %ecx
	shrq	$3, %r10
	salq	%cl, %r9
	movl	%eax, %ecx
	addq	%rsi, %rax
	addq	%r13, %r10
	subq	%r9, %rdi
	andl	$7, %ecx
	movq	%rax, (%r12)
	movzbl	(%r10), %ebx
	salq	%cl, %rdi
	orq	%rdi, %rbx
	movq	%rbx, (%r10)
	movq	-1376(%rbp), %rbx
	addl	$1, 320(%rbx,%r8,4)
.L601:
	movq	%rax, %rdx
	movq	-1520(%rbp), %rbx
	movl	%eax, %edi
	shrq	$3, %rdx
	andl	$7, %edi
	addq	%r13, %rdx
	movzbl	(%rdx), %esi
	cmpq	$11, %rbx
	ja	.L602
	movq	-1416(%rbp), %rcx
	movq	-1488(%rbp), %r10
	movzbl	1(%rcx,%r10), %r9d
	movq	-1424(%rbp), %rcx
	addq	%r9, %rax
	movzwl	-8(%rcx,%rbx,2), %r8d
	movl	%edi, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	salq	%cl, %r8
	orq	%r8, %rsi
	addl	$1, -16(%rax,%rbx,4)
	movq	%rsi, (%rdx)
.L603:
	movq	-1504(%rbp), %rbx
	cmpq	%rbx, %r11
	jbe	.L642
	movq	-3(%rbx), %rax
	movq	%rbx, %rcx
	movabsq	$8503243848024064, %rdx
	movabsq	$8503243848024064, %rdi
	subq	%r15, %rcx
	imulq	%rax, %rdx
	leal	-3(%rcx), %esi
	shrq	$55, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	leal	-2(%rcx), %esi
	shrq	$8, %rdx
	imulq	%rdi, %rdx
	shrq	$55, %rdx
	movl	%esi, (%r14,%rdx,4)
	movq	%rax, %rdx
	shrq	$24, %rax
	leal	-1(%rcx), %esi
	imulq	%rdi, %rax
	shrq	$16, %rdx
	imulq	%rdi, %rdx
	shrq	$55, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$55, %rdx
	movl	%esi, (%r14,%rdx,4)
	movslq	(%rax), %r10
	movq	%rbx, %rdx
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rbx)
	je	.L607
.L608:
	movq	-1504(%rbp), %rax
	movabsq	$8503243848024064, %rsi
	imulq	1(%rax), %rsi
	leaq	1(%rax), %rbx
	movq	%rax, -1368(%rbp)
	leaq	1(%rbx), %rdi
	shrq	$55, %rsi
	cmpq	%rdi, %r11
	jnb	.L732
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%r13, %rax
	movq	-1408(%rbp), %r14
	movq	%r12, %r13
	movq	%rax, %r12
.L578:
	subq	%r14, -1352(%rbp)
	movq	-1352(%rbp), %rdi
	je	.L620
	cmpq	$65536, %rdi
	movl	$65536, %eax
	cmovbe	%rdi, %rax
	addq	%rax, -1384(%rbp)
	movq	%rax, %r14
	movq	-1384(%rbp), %rax
	cmpq	$1048576, %rax
	jbe	.L733
.L621:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	ja	.L637
.L638:
	movq	-1352(%rbp), %rax
	movl	$98304, %r14d
	movq	%r12, %rdx
	movq	%r13, %rsi
	cmpq	$98304, %rax
	cmovbe	%rax, %r14
	movq	0(%r13), %rax
	movq	%r14, %rdi
	addq	$3, %rax
	movq	%rax, -1448(%rbp)
	call	BrotliStoreMetaBlockHeader.constprop.0
	movq	0(%r13), %rax
	subq	$8, %rsp
	movq	%r13, %r9
	movq	-1360(%rbp), %rbx
	movq	-1480(%rbp), %r8
	movq	%rax, %rdx
	addq	$13, %rax
	movq	-1472(%rbp), %rdi
	shrq	$3, %rdx
	movq	%rax, 0(%r13)
	movq	%rbx, %rsi
	addq	%r12, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	movq	-1432(%rbp), %rcx
	movq	%r14, %rdx
	pushq	%r12
	call	BuildAndStoreLiteralPrefixCode
	movq	-1424(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	-1416(%rbp), %rsi
	movq	-1376(%rbp), %rdi
	movq	%rax, -1464(%rbp)
	call	BuildAndStoreCommandPrefixCode
	popq	%rdx
	popq	%rcx
	movq	%r14, -1384(%rbp)
	movq	%rbx, -1440(%rbp)
	movq	%rbx, -1368(%rbp)
	movq	%rbx, -1392(%rbp)
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L729:
	movzbl	4(%rax), %edx
	cmpb	%dl, 4(%rbx)
	jne	.L582
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L620:
	movq	-1368(%rbp), %rbx
	cmpq	%rbx, -1360(%rbp)
	jbe	.L635
.L637:
	movq	-1360(%rbp), %rdx
	subq	-1368(%rbp), %rdx
	cmpq	$6209, %rdx
	ja	.L624
	movq	0(%r13), %rdi
	movq	%rdi, %rsi
	movl	%edi, %r10d
	shrq	$3, %rsi
	andl	$7, %r10d
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	cmpq	$5, %rdx
	ja	.L625
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	movzbl	40(%rax,%rdx), %r11d
	movq	-1424(%rbp), %rax
	movzwl	80(%rax,%rdx,2), %eax
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rax
	orq	%rax, %r9
	movq	-1376(%rbp), %rax
	movq	%r9, (%rsi)
	addl	$1, 160(%rax,%rdx,4)
	testq	%rdx, %rdx
	je	.L597
.L626:
	movq	-1368(%rbp), %r10
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L630:
	movzbl	(%r10,%r8), %ecx
	movq	%rdi, %rsi
	addq	$1, %r8
	shrq	$3, %rsi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rsi
	movl	%edi, %ecx
	movzbl	(%rsi), %r11d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rdi
	orq	%r11, %rax
	movq	%rdi, 0(%r13)
	movq	%rax, (%rsi)
	cmpq	%r8, %rdx
	ja	.L630
.L597:
	cmpq	$0, -1352(%rbp)
	jne	.L638
	.p2align 4,,10
	.p2align 3
.L635:
	movl	-1524(%rbp), %eax
	testl	%eax, %eax
	je	.L734
.L573:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L735
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	cmpq	$129, %rdx
	ja	.L627
	leaq	-2(%rdx), %r8
	movq	-1416(%rbp), %rbx
	bsrl	%r8d, %eax
	movq	%r8, %r11
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %r11
	leal	(%rax,%rax), %ecx
	leaq	42(%r11,%rcx), %r14
	movq	-1424(%rbp), %rcx
	movzbl	(%rbx,%r14), %ebx
	movq	%rbx, -1384(%rbp)
	movzwl	(%rcx,%r14,2), %ebx
	movl	%r10d, %ecx
	addq	-1384(%rbp), %rdi
	salq	%cl, %rbx
	movl	%eax, %ecx
	movq	%rdi, 0(%r13)
	orq	%rbx, %r9
	salq	%cl, %r11
	movl	%edi, %ecx
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	subq	%r11, %r8
	andl	$7, %ecx
	shrq	$3, %rsi
	addq	%rax, %rdi
	salq	%cl, %r8
	movq	-1376(%rbp), %rax
	addq	%r12, %rsi
	movzbl	(%rsi), %r9d
	orq	%r8, %r9
	movq	%r9, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L733:
	movq	-1432(%rbp), %rdx
	movq	-1360(%rbp), %rdi
	movq	%r14, %rsi
	call	ShouldMergeBlock
	testl	%eax, %eax
	je	.L621
	movl	-1384(%rbp), %eax
	movl	$20, %edi
	movl	$1, %ebx
	movq	-1448(%rbp), %r10
	leal	-1(%rax), %r11d
	.p2align 4,,10
	.p2align 3
.L622:
	movq	%r10, %r9
	movl	$8, %eax
	movl	%ebx, %edx
	movl	%ebx, %esi
	andl	$7, %r9d
	movq	%r10, %r8
	subq	%r9, %rax
	cmpq	%rdi, %rax
	cmova	%rdi, %rax
	shrq	$3, %r8
	addq	%r12, %r8
	leaq	(%r9,%rax), %rcx
	addq	%rax, %r10
	sall	%cl, %edx
	movl	%r9d, %ecx
	sall	%cl, %esi
	negl	%edx
	movl	%esi, %ecx
	movl	$-1, %esi
	subl	$1, %ecx
	orl	%ecx, %edx
	movl	%eax, %ecx
	andb	(%r8), %dl
	sall	%cl, %esi
	movl	%r9d, %ecx
	notl	%esi
	andl	%r11d, %esi
	sall	%cl, %esi
	movl	%eax, %ecx
	orl	%edx, %esi
	shrl	%cl, %r11d
	movb	%sil, (%r8)
	subq	%rax, %rdi
	jne	.L622
	movq	-1360(%rbp), %rax
	movq	%rax, -1392(%rbp)
	movq	%rax, %rbx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L627:
	cmpq	$2113, %rdx
	ja	.L629
	leaq	-66(%rdx), %r8
	movq	-1416(%rbp), %rbx
	movl	%r10d, %ecx
	bsrl	%r8d, %eax
	leal	50(%rax), %r14d
	movzbl	(%rbx,%r14), %r11d
	movq	-1424(%rbp), %rbx
	movzwl	(%rbx,%r14,2), %ebx
	addq	%r11, %rdi
	movq	%rdi, 0(%r13)
	salq	%cl, %rbx
	movl	%eax, %ecx
	movl	%eax, %eax
	orq	%rbx, %r9
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	movl	$1, %r9d
	shrq	$3, %rsi
	salq	%cl, %r9
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r12, %rsi
	subq	%r9, %r8
	andl	$7, %ecx
	movq	-1376(%rbp), %rax
	movzbl	(%rsi), %r10d
	salq	%cl, %r8
	orq	%r8, %r10
	movq	%r10, (%rsi)
	movq	%rdi, 0(%r13)
	addl	$1, (%rax,%r14,4)
	jmp	.L626
.L629:
	movq	-1424(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzwl	122(%rbx), %r8d
	movzbl	61(%rax), %eax
	salq	%cl, %r8
	addq	%rax, %rdi
	leaq	-2114(%rdx), %rax
	orq	%r8, %r9
	movl	%edi, %ecx
	movq	%rdi, 0(%r13)
	movq	%r9, (%rsi)
	movq	%rdi, %rsi
	andl	$7, %ecx
	addq	$12, %rdi
	shrq	$3, %rsi
	salq	%cl, %rax
	movq	%rdi, 0(%r13)
	addq	%r12, %rsi
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L626
.L590:
	cmpq	$129, %rdi
	jbe	.L736
	cmpq	$2113, %rdi
	ja	.L595
	subq	$66, %rbx
	movq	-1416(%rbp), %rdi
	movq	-1424(%rbp), %rcx
	movq	%rbx, -1536(%rbp)
	movzbl	(%rsi), %r8d
	bsrl	-1536(%rbp), %ebx
	leal	50(%rbx), %r9d
	movzbl	(%rdi,%r9), %edi
	movq	%rdi, -1552(%rbp)
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	-1552(%rbp), %rdi
	movq	%r8, (%rsi)
	leaq	(%rdi,%rax), %rsi
	movl	$1, %eax
	movq	%rsi, %rdi
	movq	%rsi, (%r12)
	shrq	$3, %rdi
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
.L728:
	movl	%ebx, %ecx
	salq	%cl, %rax
	movq	-1536(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movl	%esi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movl	%ebx, %eax
	movq	-1376(%rbp), %rbx
	addq	%rsi, %rax
	movq	%r8, (%rdi)
	movq	%rax, (%r12)
	addl	$1, (%rbx,%r9,4)
	jmp	.L591
.L739:
	movq	-1360(%rbp), %rcx
	xorl	%eax, %eax
	leaq	5(%rdx), %rsi
	leaq	5(%r10), %rdi
	leaq	13(%rdx), %rbx
	subq	%rdx, %rcx
	subq	$5, %rcx
	movq	%rcx, %r8
	shrq	$3, %r8
	je	.L611
.L610:
	movq	5(%rdx,%rax), %rsi
	movq	5(%r10,%rax), %r9
	cmpq	%r9, %rsi
	je	.L737
	xorq	%r9, %rsi
	xorl	%ecx, %ecx
	rep bsfq	%rsi, %rcx
	movslq	%ecx, %rcx
	shrq	$3, %rcx
	addq	%rcx, %rax
.L612:
	movq	%rdx, %rbx
	leaq	5(%rax), %rcx
	subq	%r10, %rbx
	movq	%rbx, -1504(%rbp)
	cmpq	$262128, %rbx
	jg	.L725
	movq	(%r12), %rdi
	movl	%ebx, -1400(%rbp)
	addq	%rcx, %rdx
	movq	%rdi, %rsi
	movl	%edi, %ebx
	shrq	$3, %rsi
	andl	$7, %ebx
	addq	%r13, %rsi
	movl	%ebx, -1368(%rbp)
	movzbl	(%rsi), %r10d
	cmpq	$9, %rcx
	jbe	.L738
	cmpq	$133, %rcx
	ja	.L617
	leaq	-1(%rax), %r9
	bsrl	%r9d, %eax
	movq	%r9, %rbx
	subl	$1, %eax
	movl	%eax, %ecx
	shrq	%cl, %rbx
	leal	(%rax,%rax), %ecx
	movq	%rbx, -1488(%rbp)
	leaq	20(%rbx,%rcx), %rbx
	movq	-1416(%rbp), %rcx
	movzbl	(%rcx,%rbx), %ecx
	movq	%rcx, -1512(%rbp)
	movq	-1424(%rbp), %rcx
	addq	-1512(%rbp), %rdi
	movzwl	(%rcx,%rbx,2), %r8d
	movzbl	-1368(%rbp), %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movq	-1488(%rbp), %r10
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%eax, %ecx
	shrq	$3, %rsi
	salq	%cl, %r10
	movl	%edi, %ecx
	addq	%rax, %rdi
	addq	%r13, %rsi
	movq	-1376(%rbp), %rax
	subq	%r10, %r9
	andl	$7, %ecx
	movzbl	(%rsi), %r8d
	salq	%cl, %r9
	movq	%rdi, (%r12)
	addl	$1, (%rax,%rbx,4)
	orq	%r9, %r8
	movq	%r8, (%rsi)
.L616:
	movslq	-1504(%rbp), %rax
	movq	%rdi, %r10
	movq	-1424(%rbp), %r8
	shrq	$3, %r10
	addq	$3, %rax
	addq	%r13, %r10
	bsrl	%eax, %esi
	movq	%rax, %rbx
	leal	-1(%rsi), %r9d
	leal	-4(%rsi,%rsi), %esi
	movl	%r9d, %ecx
	shrq	%cl, %rbx
	movq	-1416(%rbp), %rcx
	andl	$1, %ebx
	addq	%rbx, %rsi
	movq	%rbx, -1368(%rbp)
	movzbl	(%r10), %ebx
	movzwl	160(%r8,%rsi,2), %r8d
	movzbl	80(%rcx,%rsi), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	addq	-1488(%rbp), %rdi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movl	%r9d, %ecx
	orq	%r8, %rbx
	movq	%rdi, %r8
	movq	%rbx, (%r10)
	movq	-1368(%rbp), %rbx
	shrq	$3, %r8
	addq	%r13, %r8
	addq	$2, %rbx
	movzbl	(%r8), %r10d
	salq	%cl, %rbx
	movl	%edi, %ecx
	addq	%r9, %rdi
	subq	%rbx, %rax
	andl	$7, %ecx
	movq	%rdi, (%r12)
	salq	%cl, %rax
	orq	%rax, %r10
	movq	-1376(%rbp), %rax
	movq	%r10, (%r8)
	addl	$1, 320(%rax,%rsi,4)
	cmpq	%rdx, %r11
	jbe	.L644
	movq	-3(%rdx), %rax
	movq	%rdx, %rcx
	movabsq	$8503243848024064, %rsi
	movabsq	$8503243848024064, %rbx
	subq	%r15, %rcx
	imulq	%rax, %rsi
	leal	-3(%rcx), %edi
	shrq	$55, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	leal	-2(%rcx), %edi
	shrq	$8, %rsi
	imulq	%rbx, %rsi
	shrq	$55, %rsi
	movl	%edi, (%r14,%rsi,4)
	movq	%rax, %rsi
	shrq	$24, %rax
	leal	-1(%rcx), %edi
	imulq	%rbx, %rax
	shrq	$16, %rsi
	imulq	%rbx, %rsi
	shrq	$55, %rax
	leaq	(%r14,%rax,4), %rax
	shrq	$55, %rsi
	movl	%edi, (%r14,%rsi,4)
	movslq	(%rax), %r10
	movl	%ecx, (%rax)
	addq	%r15, %r10
	movl	(%r10), %eax
	cmpl	%eax, (%rdx)
	jne	.L725
.L607:
	movzbl	4(%r10), %eax
	cmpb	%al, 4(%rdx)
	je	.L739
.L725:
	movq	%rdx, -1504(%rbp)
	jmp	.L608
.L624:
	movq	-1368(%rbp), %rax
	movq	-1440(%rbp), %rdi
	subq	%rdi, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, %rdx
	jb	.L631
	cmpq	$980, -1464(%rbp)
	ja	.L740
.L631:
	movq	0(%r13), %rsi
	movq	-1416(%rbp), %rbx
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$3, %rax
	andl	$7, %ecx
	addq	%r12, %rax
	movzbl	(%rax), %r8d
	cmpq	$22593, %rdx
	jbe	.L741
	movzbl	63(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1092(%rbp)
	movzwl	126(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$24, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-22594(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
.L633:
	movq	-1368(%rbp), %r8
	addq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L634:
	movzbl	(%r8), %ecx
	movq	%rsi, %rdi
	addq	$1, %r8
	shrq	$3, %rdi
	movzbl	-320(%rbp,%rcx), %r9d
	movzwl	-832(%rbp,%rcx,2), %eax
	addq	%r12, %rdi
	movl	%esi, %ecx
	movzbl	(%rdi), %r10d
	andl	$7, %ecx
	salq	%cl, %rax
	addq	%r9, %rsi
	orq	%r10, %rax
	movq	%rsi, 0(%r13)
	movq	%rax, (%rdi)
	cmpq	%rdx, %r8
	jne	.L634
	cmpq	$0, -1352(%rbp)
	jne	.L638
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L602:
	cmpq	$71, -1520(%rbp)
	ja	.L604
	movq	-1488(%rbp), %r8
	movq	-1424(%rbp), %r9
	subq	$3, %r8
	bsrl	%r8d, %ebx
	movq	%r8, %r10
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %r10
	leal	(%rbx,%rbx), %ecx
	movq	%r10, -1368(%rbp)
	leaq	4(%r10,%rcx), %r10
	movq	-1416(%rbp), %rcx
	movzwl	(%r9,%r10,2), %r9d
	movzbl	(%rcx,%r10), %ecx
	movq	%rcx, -1488(%rbp)
	movl	%edi, %ecx
	movq	-1368(%rbp), %rdi
	salq	%cl, %r9
	addq	-1488(%rbp), %rax
	movl	%ebx, %ecx
	orq	%r9, %rsi
	salq	%cl, %rdi
	movq	%rax, (%r12)
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movq	%rdi, %rcx
	movq	%r8, %rdi
	shrq	$3, %rdx
	subq	%rcx, %rdi
	movl	%eax, %ecx
	addq	%rbx, %rax
	addq	%r13, %rdx
	andl	$7, %ecx
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	movzbl	(%rdx), %esi
	salq	%cl, %rdi
	addl	$1, (%rax,%r10,4)
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	jmp	.L603
.L734:
	movq	-1544(%rbp), %rax
	movq	-1496(%rbp), %r8
	movq	-1424(%rbp), %rdx
	movq	-1416(%rbp), %rsi
	movb	$0, (%r8)
	movq	-1376(%rbp), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	call	BuildAndStoreCommandPrefixCode
	jmp	.L573
.L731:
	movq	-1416(%rbp), %rbx
	movzbl	-1512(%rbp), %ecx
	addl	$1, -1088(%rbp)
	movzbl	64(%rbx), %esi
	movq	-1424(%rbp), %rbx
	movzwl	128(%rbx), %edx
	addq	%rsi, %rax
	movq	%rax, (%r12)
	salq	%cl, %rdx
	orq	-1368(%rbp), %rdx
	movq	%rdx, (%r10)
	jmp	.L601
.L604:
	movq	-1424(%rbp), %rcx
	cmpq	$135, -1520(%rbp)
	movzwl	128(%rcx), %ebx
	jbe	.L742
	cmpq	$2119, -1520(%rbp)
	ja	.L606
	movq	%rcx, %r9
	movq	-1488(%rbp), %rcx
	movq	-1416(%rbp), %r8
	subq	$67, %rcx
	movq	%rcx, -1368(%rbp)
	movl	%edi, %ecx
	bsrl	-1368(%rbp), %r10d
	movl	%r10d, -1488(%rbp)
	addl	$28, %r10d
	movl	-1488(%rbp), %edi
	movzbl	(%r8,%r10), %r8d
	movzwl	(%r9,%r10,2), %r9d
	addq	%rax, %r8
	salq	%cl, %r9
	movl	$1, %eax
	movl	%edi, %ecx
	salq	%cl, %rax
	orq	%r9, %rsi
	movq	-1368(%rbp), %rcx
	movq	%r8, (%r12)
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	addq	%r13, %rdx
	movq	%rcx, %rax
	movl	%r8d, %ecx
	movzbl	(%rdx), %esi
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %rsi
	movl	%edi, %eax
	addq	%rax, %r8
	movq	-1416(%rbp), %rax
	movq	%rsi, (%rdx)
	movq	%r8, (%r12)
	movl	%r8d, %ecx
	movzbl	64(%rax), %esi
	movq	%r8, %rax
	andl	$7, %ecx
	shrq	$3, %rax
	salq	%cl, %rbx
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edx
	movq	%r8, (%r12)
	orq	%rbx, %rdx
	movq	%rdx, (%rax)
	movq	-1376(%rbp), %rax
	addl	$1, (%rax,%r10,4)
	addl	$1, -1088(%rbp)
	jmp	.L603
.L736:
	leaq	-2(%rdi), %rdi
	movzbl	(%rsi), %r8d
	bsrl	%edi, %ebx
	movq	%rdi, -1536(%rbp)
	subl	$1, %ebx
	movl	%ebx, %ecx
	shrq	%cl, %rdi
	leal	(%rbx,%rbx), %ecx
	leaq	42(%rdi,%rcx), %r9
	movq	-1416(%rbp), %rcx
	movq	%rdi, -1552(%rbp)
	movzbl	(%rcx,%r9), %ecx
	movq	%rcx, -1560(%rbp)
	movq	-1424(%rbp), %rcx
	movzwl	(%rcx,%r9,2), %edi
	movl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, %r8
	movq	%r8, (%rsi)
	movq	-1560(%rbp), %rsi
	addq	%rax, %rsi
	movq	%rsi, %rax
	movq	%rsi, (%r12)
	shrq	$3, %rax
	movq	%rax, %rdi
	movq	-1552(%rbp), %rax
	addq	%r13, %rdi
	movzbl	(%rdi), %r8d
	jmp	.L728
.L741:
	movzbl	62(%rbx), %edi
	movq	-1424(%rbp), %rbx
	addl	$1, -1096(%rbp)
	movzwl	124(%rbx), %r9d
	addq	%rdi, %rsi
	movq	%rsi, %rdi
	movq	%rsi, 0(%r13)
	salq	%cl, %r9
	shrq	$3, %rdi
	movl	%esi, %ecx
	addq	$14, %rsi
	orq	%r9, %r8
	addq	%r12, %rdi
	andl	$7, %ecx
	movq	%r8, (%rax)
	leaq	-6210(%rdx), %rax
	movzbl	(%rdi), %r8d
	salq	%cl, %rax
	movq	%rsi, 0(%r13)
	orq	%rax, %r8
	movq	%r8, (%rdi)
	jmp	.L633
.L740:
	movq	-1448(%rbp), %rdx
	movq	-1360(%rbp), %rsi
	movq	%r12, %r8
	movq	%r13, %rcx
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	cmpq	$0, -1352(%rbp)
	jne	.L638
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L742:
	movq	-1488(%rbp), %r8
	movq	-1416(%rbp), %r9
	subq	$3, %r8
	movq	%r8, %r10
	shrq	$5, %r10
	movzbl	30(%r9,%r10), %r9d
	movq	%r9, -1368(%rbp)
	movzwl	60(%rcx,%r10,2), %r9d
	movl	%edi, %ecx
	movq	%r8, %rdi
	addq	-1368(%rbp), %rax
	andl	$31, %edi
	salq	%cl, %r9
	movl	%eax, %ecx
	movq	%rax, (%r12)
	orq	%r9, %rsi
	andl	$7, %ecx
	movq	-1416(%rbp), %r9
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	salq	%cl, %rdi
	addq	$5, %rax
	shrq	$3, %rdx
	movq	%rax, (%r12)
	movl	%eax, %ecx
	addq	%r13, %rdx
	andl	$7, %ecx
	movzbl	(%rdx), %esi
	salq	%cl, %rbx
	movq	%rbx, %rcx
	orq	%rdi, %rsi
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	movzbl	64(%r9), %esi
	shrq	$3, %rdx
	addq	%r13, %rdx
	addq	%rsi, %rax
	movzbl	(%rdx), %edi
	movq	%rax, (%r12)
	movq	-1376(%rbp), %rax
	orq	%rdi, %rcx
	addl	$1, 120(%rax,%r10,4)
	movq	%rcx, (%rdx)
	addl	$1, -1088(%rbp)
	jmp	.L603
.L730:
	movq	-1488(%rbp), %rcx
	addq	%rdi, %rcx
	addq	$8, %rdi
	subq	$1, %r10
	jne	.L585
	movq	%rdi, %r10
.L583:
	andl	$7, %esi
	je	.L640
	movzbl	(%r9,%r10), %eax
	cmpb	%al, (%rcx)
	jne	.L640
	leaq	1(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L586
	movzbl	1(%r9,%r10), %eax
	cmpb	%al, 1(%rcx)
	jne	.L586
	leaq	2(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$2, %rsi
	je	.L586
	movzbl	2(%r9,%r10), %eax
	cmpb	%al, 2(%rcx)
	jne	.L586
	leaq	3(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$3, %rsi
	je	.L586
	movzbl	3(%r9,%r10), %eax
	cmpb	%al, 3(%rcx)
	jne	.L586
	leaq	4(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$4, %rsi
	je	.L586
	movzbl	4(%r9,%r10), %eax
	cmpb	%al, 4(%rcx)
	jne	.L586
	leaq	5(%r10), %rax
	movq	%rax, -1488(%rbp)
	subq	$5, %rsi
	je	.L586
	movzbl	5(%r9,%r10), %eax
	cmpb	%al, 5(%rcx)
	jne	.L586
	leaq	6(%r10), %rax
	movq	%rax, -1488(%rbp)
	cmpq	$1, %rsi
	je	.L586
	movzbl	6(%r9,%r10), %eax
	cmpb	%al, 6(%rcx)
	jne	.L586
	leaq	7(%r10), %rax
	movq	%rax, -1488(%rbp)
	jmp	.L586
.L595:
	movq	-1416(%rbp), %rbx
	movzbl	(%rsi), %r8d
	movl	%r10d, %ecx
	addl	$1, -1100(%rbp)
	movzbl	61(%rbx), %r9d
	movq	-1424(%rbp), %rbx
	movzwl	122(%rbx), %edi
	addq	%r9, %rax
	movq	-1512(%rbp), %rbx
	movq	%rax, (%r12)
	salq	%cl, %rdi
	movl	%eax, %ecx
	orq	%rdi, %r8
	movq	%rax, %rdi
	andl	$7, %ecx
	addq	$12, %rax
	shrq	$3, %rdi
	movq	%r8, (%rsi)
	leaq	-2114(%rbx), %rsi
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L591
.L642:
	movq	%r13, %rax
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	movq	-1504(%rbp), %rax
	movq	%rax, -1368(%rbp)
	jmp	.L578
.L589:
	movq	-1368(%rbp), %rax
	subq	-1440(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	cmpq	%rax, -1512(%rbp)
	jb	.L596
	cmpq	$980, -1464(%rbp)
	jbe	.L596
	movq	-1448(%rbp), %rdx
	movq	%r13, %rax
	movq	-1440(%rbp), %rdi
	movq	%r12, %r13
	movq	%rax, %r8
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%rax, %r12
	subq	$3, %rdx
	call	EmitUncompressedMetaBlock
	movq	%rbx, %rax
	movq	%rbx, -1360(%rbp)
	subq	-1392(%rbp), %rax
	subq	%rax, -1352(%rbp)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%r12), %r8
	movq	-1512(%rbp), %rbx
	movq	-1416(%rbp), %rax
	movq	%r8, %rdi
	movl	%r8d, %ecx
	shrq	$3, %rdi
	andl	$7, %ecx
	addq	%r13, %rdi
	cmpq	$22593, %rbx
	ja	.L598
	movzbl	62(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1096(%rbp)
	movzwl	124(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-6210(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$14, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L591
.L606:
	movq	-1416(%rbp), %r10
	movzwl	78(%rcx), %r9d
	movl	%edi, %ecx
	movq	-1488(%rbp), %rdi
	addl	$1, -1188(%rbp)
	movzbl	39(%r10), %r8d
	salq	%cl, %r9
	addl	$1, -1088(%rbp)
	orq	%r9, %rsi
	subq	$2115, %rdi
	addq	%rax, %r8
	movq	%rsi, (%rdx)
	movq	%r8, %rdx
	movl	%r8d, %ecx
	movq	%r8, (%r12)
	addq	$24, %r8
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%r8, (%r12)
	addq	%r13, %rdx
	salq	%cl, %rdi
	movl	%r8d, %ecx
	movzbl	(%rdx), %eax
	andl	$7, %ecx
	salq	%cl, %rbx
	orq	%rdi, %rax
	movq	%rax, (%rdx)
	movq	%r8, %rax
	movzbl	64(%r10), %esi
	movq	%rbx, %rdx
	shrq	$3, %rax
	addq	%r13, %rax
	addq	%rsi, %r8
	movzbl	(%rax), %edi
	movq	%r8, (%r12)
	orq	%rdi, %rdx
	movq	%rdx, (%rax)
	jmp	.L603
.L738:
	movq	-1416(%rbp), %rcx
	movzbl	19(%rcx,%rax), %r8d
	movq	-1424(%rbp), %rcx
	movzwl	38(%rcx,%rax,2), %r9d
	movl	%ebx, %ecx
	movq	-1376(%rbp), %rbx
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	addl	$1, 76(%rbx,%rax,4)
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	jmp	.L616
.L598:
	movzbl	63(%rax), %r9d
	movq	-1424(%rbp), %rax
	movzbl	(%rdi), %esi
	addl	$1, -1092(%rbp)
	movzwl	126(%rax), %eax
	salq	%cl, %rax
	orq	%rax, %rsi
	leaq	(%r9,%r8), %rax
	movq	%rsi, (%rdi)
	movq	%rax, %rdi
	movl	%eax, %ecx
	leaq	-22594(%rbx), %rsi
	shrq	$3, %rdi
	movq	%rax, (%r12)
	andl	$7, %ecx
	addq	$24, %rax
	addq	%r13, %rdi
	salq	%cl, %rsi
	movq	%rax, (%r12)
	movzbl	(%rdi), %r8d
	orq	%rsi, %r8
	movq	%r8, (%rdi)
	jmp	.L591
.L617:
	cmpq	$2117, %rcx
	ja	.L618
	subq	$65, %rax
	movq	-1416(%rbp), %r8
	movzbl	-1368(%rbp), %ecx
	movq	%rax, -1488(%rbp)
	bsrl	-1488(%rbp), %ebx
	leal	28(%rbx), %r9d
	movzbl	(%r8,%r9), %eax
	movq	-1424(%rbp), %r8
	movzwl	(%r8,%r9,2), %r8d
	addq	%rax, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r8
	movq	%r8, %rcx
	orq	%r10, %rcx
	movl	$1, %r10d
	movq	%rcx, (%rsi)
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	salq	%cl, %r10
	movq	-1488(%rbp), %rcx
	shrq	$3, %rsi
	addq	%r13, %rsi
	subq	%r10, %rcx
	movzbl	(%rsi), %r8d
	movq	%rcx, %rax
	movl	%edi, %ecx
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%rax, %r8
	movq	-1376(%rbp), %rax
	movq	%r8, (%rsi)
	movl	%ebx, %r8d
	addq	%r8, %rdi
	addl	$1, (%rax,%r9,4)
	movq	%rdi, (%r12)
	jmp	.L616
.L640:
	movq	%r10, -1488(%rbp)
	jmp	.L586
.L644:
	movq	%r13, %rax
	movq	%rdx, -1368(%rbp)
	movq	%r12, %r13
	movq	-1408(%rbp), %r14
	movq	%rax, %r12
	jmp	.L578
.L737:
	leaq	(%rbx,%rax), %rsi
	addq	$8, %rax
	subq	$1, %r8
	jne	.L610
.L611:
	andl	$7, %ecx
	je	.L612
	movzbl	(%rdi,%rax), %ebx
	cmpb	%bl, (%rsi)
	jne	.L643
	leaq	1(%rax), %r8
	cmpq	$1, %rcx
	je	.L614
	movzbl	1(%rdi,%rax), %ebx
	cmpb	%bl, 1(%rsi)
	jne	.L614
	leaq	2(%rax), %r8
	cmpq	$2, %rcx
	je	.L614
	movzbl	2(%rdi,%rax), %ebx
	cmpb	%bl, 2(%rsi)
	jne	.L614
	leaq	3(%rax), %r8
	cmpq	$3, %rcx
	je	.L614
	movzbl	3(%rdi,%rax), %ebx
	cmpb	%bl, 3(%rsi)
	jne	.L614
	leaq	4(%rax), %r8
	cmpq	$4, %rcx
	je	.L614
	movzbl	4(%rdi,%rax), %ebx
	cmpb	%bl, 4(%rsi)
	jne	.L614
	leaq	5(%rax), %r8
	subq	$5, %rcx
	je	.L614
	movzbl	5(%rdi,%rax), %ebx
	cmpb	%bl, 5(%rsi)
	jne	.L614
	leaq	6(%rax), %r8
	cmpq	$1, %rcx
	je	.L614
	movzbl	6(%rdi,%rax), %ebx
	cmpb	%bl, 6(%rsi)
	jne	.L614
	leaq	7(%rax), %r8
.L614:
	movq	%r8, %rax
	jmp	.L612
.L618:
	movq	-1416(%rbp), %rbx
	movzbl	-1368(%rbp), %ecx
	subq	$2113, %rax
	addl	$1, -1188(%rbp)
	movzbl	39(%rbx), %r8d
	movq	-1424(%rbp), %rbx
	movzwl	78(%rbx), %r9d
	addq	%r8, %rdi
	movq	%rdi, (%r12)
	salq	%cl, %r9
	movq	%r9, %rcx
	orq	%r10, %rcx
	movq	%rcx, (%rsi)
	movq	%rdi, %rsi
	movl	%edi, %ecx
	addq	$24, %rdi
	shrq	$3, %rsi
	andl	$7, %ecx
	movq	%rdi, (%r12)
	addq	%r13, %rsi
	salq	%cl, %rax
	movzbl	(%rsi), %r8d
	orq	%rax, %r8
	movq	%r8, (%rsi)
	jmp	.L616
.L643:
	movq	%rax, %r8
	jmp	.L614
.L735:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE116:
	.size	BrotliCompressFragmentFastImpl9, .-BrotliCompressFragmentFastImpl9
	.p2align 4
	.globl	BrotliCompressFragmentFast
	.hidden	BrotliCompressFragmentFast
	.type	BrotliCompressFragmentFast, @function
BrotliCompressFragmentFast:
.LFB120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rbp), %r12
	movq	(%r12), %r13
	testq	%rdx, %rdx
	je	.L757
	bsrl	%r9d, %r9d
	movq	%rsi, %r14
	movq	%rdx, %rbx
	movl	%ecx, %r15d
	xorl	$31, %r9d
	movl	%r9d, %eax
	xorl	$31, %eax
	cmpl	$18, %r9d
	je	.L746
	cmpl	$13, %eax
	ja	.L747
	cmpl	$22, %r9d
	je	.L748
	cmpl	$20, %r9d
	jne	.L750
	subq	$8, %rsp
	pushq	56(%rbp)
	movq	16(%rbp), %r9
	pushq	%r12
	pushq	40(%rbp)
	pushq	32(%rbp)
	pushq	24(%rbp)
	call	BrotliCompressFragmentFastImpl11
	movq	(%r12), %rax
	addq	$48, %rsp
	subq	%r13, %rax
.L752:
	leaq	31(,%rbx,8), %rdx
	cmpq	%rax, %rdx
	jnb	.L750
	movq	56(%rbp), %r8
	leaq	(%r14,%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	EmitUncompressedMetaBlock
.L750:
	testl	%r15d, %r15d
	je	.L743
	movq	(%r12), %rax
	movq	%rax, %rdx
	movl	%eax, %esi
	shrq	$3, %rdx
	addq	56(%rbp), %rdx
	andl	$7, %esi
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	%rcx, (%rdx)
	leaq	1(%rax), %rdx
	addq	$9, %rax
	movq	%rdx, %rcx
	movq	%rdx, (%r12)
	andl	$7, %edx
	andl	$4294967288, %eax
	shrq	$3, %rcx
	addq	56(%rbp), %rcx
	movq	%rcx, %r14
	movzbl	(%rcx), %ecx
	btsq	%rdx, %rcx
	movq	%rcx, (%r14)
	movq	%rax, (%r12)
.L743:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L747:
	.cfi_restore_state
	cmpl	$16, %r9d
	jne	.L750
	subq	$8, %rsp
	pushq	56(%rbp)
	movq	16(%rbp), %r9
	pushq	%r12
	pushq	40(%rbp)
	pushq	32(%rbp)
	pushq	24(%rbp)
	call	BrotliCompressFragmentFastImpl15
	movq	(%r12), %rax
	addq	$48, %rsp
	subq	%r13, %rax
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L757:
	movq	%r13, %rax
	movl	%r13d, %ecx
	shrq	$3, %rax
	addq	56(%rbp), %rax
	andl	$7, %ecx
	movzbl	(%rax), %edx
	btsq	%rcx, %rdx
	movq	%rdx, (%rax)
	leaq	1(%r13), %rax
	addq	$9, %r13
	movq	%rax, %rdx
	movq	%rax, (%r12)
	andl	$7, %eax
	andl	$4294967288, %r13d
	shrq	$3, %rdx
	addq	56(%rbp), %rdx
	movq	%rdx, %r14
	movzbl	(%rdx), %edx
	btsq	%rax, %rdx
	movq	%rdx, (%r14)
	movq	%r13, (%r12)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L748:
	.cfi_restore_state
	subq	$8, %rsp
	pushq	56(%rbp)
	movq	16(%rbp), %r9
	pushq	%r12
	pushq	40(%rbp)
	pushq	32(%rbp)
	pushq	24(%rbp)
	call	BrotliCompressFragmentFastImpl9
	movq	(%r12), %rax
	addq	$48, %rsp
	subq	%r13, %rax
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L746:
	subq	$8, %rsp
	pushq	56(%rbp)
	movq	16(%rbp), %r9
	pushq	%r12
	pushq	40(%rbp)
	pushq	32(%rbp)
	pushq	24(%rbp)
	call	BrotliCompressFragmentFastImpl13
	movq	(%r12), %rax
	addq	$48, %rsp
	subq	%r13, %rax
	jmp	.L752
	.cfi_endproc
.LFE120:
	.size	BrotliCompressFragmentFast, .-BrotliCompressFragmentFast
	.section	.rodata
	.align 32
	.type	kCmdHistoSeed, @object
	.size	kCmdHistoSeed, 512
kCmdHistoSeed:
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1071644672
	.align 8
.LC1:
	.long	0
	.long	1080623104
	.align 8
.LC2:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	11
	.long	11
	.long	11
	.long	11
	.align 16
.LC4:
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.align 16
.LC5:
	.long	1
	.long	1
	.long	1
	.long	1
	.hidden	BrotliStoreHuffmanTree
	.hidden	BrotliConvertBitDepthsToSymbols
	.hidden	BrotliCreateHuffmanTree
	.hidden	BrotliBuildAndStoreHuffmanTreeFast
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
