	.file	"bit_reader.c"
	.text
	.p2align 4
	.globl	BrotliInitBitReader
	.hidden	BrotliInitBitReader
	.type	BrotliInitBitReader, @function
BrotliInitBitReader:
.LFB67:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movl	$64, 8(%rdi)
	ret
	.cfi_endproc
.LFE67:
	.size	BrotliInitBitReader, .-BrotliInitBitReader
	.p2align 4
	.globl	BrotliWarmupBitReader
	.hidden	BrotliWarmupBitReader
	.type	BrotliWarmupBitReader, @function
BrotliWarmupBitReader:
.LFB68:
	.cfi_startproc
	endbr64
	cmpl	$64, 8(%rdi)
	movl	$1, %eax
	je	.L8
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	24(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L3
	movq	(%rdi), %rsi
	movq	16(%rdi), %rcx
	subq	$1, %rdx
	shrq	$8, %rsi
	addq	$1, %rcx
	movq	%rsi, (%rdi)
	movzbl	-1(%rcx), %eax
	movl	$56, 8(%rdi)
	salq	$56, %rax
	movq	%rdx, 24(%rdi)
	orq	%rsi, %rax
	movq	%rcx, 16(%rdi)
	movq	%rax, (%rdi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE68:
	.size	BrotliWarmupBitReader, .-BrotliWarmupBitReader
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
