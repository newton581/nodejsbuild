	.file	"block_splitter.c"
	.text
	.p2align 4
	.type	ClusterBlocksLiteral, @function
ClusterBlocksLiteral:
.LFB103:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	63(%rcx), %r12
	pushq	%rbx
	salq	$4, %r12
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$2264, %rsp
	movq	%rdi, -2240(%rbp)
	movq	%rsi, -2160(%rbp)
	movq	%rcx, -2168(%rbp)
	movq	%r9, -2288(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	leaq	0(,%rcx,4), %rdi
	movq	%rdi, -2296(%rbp)
	movq	%r12, %rdi
	shrq	$6, %rdi
	movq	%rdi, -2256(%rbp)
	testq	%rcx, %rcx
	jne	.L101
	movq	%rdi, %rsi
	movq	-2240(%rbp), %r15
	movq	%rdi, %r14
	leaq	-1872(%rbp), %r12
	salq	$6, %rsi
	addq	%rdi, %rsi
	movq	%r15, %rdi
	salq	$4, %rsi
	call	BrotliAllocate
	leaq	0(,%r14,4), %rsi
	movq	%r15, %rdi
	leaq	-1616(%rbp), %r14
	movq	%rax, -2184(%rbp)
	call	BrotliAllocate
	movl	$49176, %esi
	movq	%r15, %rdi
	movq	%rax, -2248(%rbp)
	call	BrotliAllocate
	leaq	-2128(%rbp), %rdi
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%rax, -2272(%rbp)
	movq	-2168(%rbp), %rax
	movq	%rdi, -2152(%rbp)
	movq	-2296(%rbp), %rdx
	rep stosq
	movl	$32, %ecx
	movq	%r12, %rdi
	rep stosq
	movl	$32, %ecx
	movq	%r14, %rdi
	rep stosq
	leaq	-1360(%rbp), %rdi
	movl	$32, %ecx
	rep stosq
	xorl	%edi, %edi
	call	memset@PLT
	movq	$0, -2208(%rbp)
	movq	$0, -2224(%rbp)
	movq	$0, -2192(%rbp)
	testq	%rbx, %rbx
	je	.L58
.L57:
	movq	-2224(%rbp), %rcx
	xorl	%eax, %eax
	xorl	%edx, %edx
	addq	$1, %rax
	addl	$1, (%rcx,%rdx,4)
	cmpq	%rbx, %rax
	je	.L5
.L102:
	movzbl	0(%r13,%rax), %edi
	xorl	%esi, %esi
	cmpb	%dil, -1(%r13,%rax)
	setne	%sil
	addq	$1, %rax
	addq	%rsi, %rdx
	addl	$1, (%rcx,%rdx,4)
	cmpq	%rbx, %rax
	jne	.L102
.L5:
	cmpq	$0, -2168(%rbp)
	je	.L58
.L4:
	movq	-2208(%rbp), %rax
	xorl	%ebx, %ebx
	movq	$0, -2176(%rbp)
	movq	$0, -2216(%rbp)
	movq	%rax, -2232(%rbp)
	movq	-2168(%rbp), %rax
	movq	$0, -2200(%rbp)
	subq	$1, %rax
	andq	$-64, %rax
	addq	$64, %rax
	movq	%rax, -2280(%rbp)
	movq	-2256(%rbp), %rax
	movq	%rax, -2264(%rbp)
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-2176(%rbp), %rax
	movq	-2168(%rbp), %r13
	movl	$64, %edi
	movq	%r12, -2144(%rbp)
	movq	-2192(%rbp), %rdx
	movq	%rbx, %r12
	subq	%rax, %r13
	cmpq	$64, %r13
	cmova	%rdi, %r13
	movq	-2224(%rbp), %rdi
	xorl	%r15d, %r15d
	leaq	(%rdi,%rax,4), %rsi
	movq	%r13, -2136(%rbp)
	movq	%r15, %r13
	movq	%rdx, %r15
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	xorl	%eax, %eax
	movq	$0, (%r15)
	movq	$0, 1016(%r15)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 1024(%r15)
	movl	(%rbx), %ecx
	movq	%rax, 1032(%r15)
	testl	%ecx, %ecx
	je	.L8
	movq	-2160(%rbp), %rcx
	xorl	%eax, %eax
	leaq	1(%r12), %rdi
	leaq	(%rcx,%r12), %r10
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	(%r10,%rax), %edx
	leaq	(%rdi,%rax), %r9
	addq	$1, %rax
	addl	$1, (%r15,%rdx,4)
	movl	(%rbx), %edx
	movq	%rax, 1024(%r15)
	cmpq	%rax, %rdx
	ja	.L9
	movq	%r9, %r12
.L8:
	movq	%r15, %rdi
	addq	$4, %rbx
	addq	$1040, %r15
	call	BrotliPopulationCostLiteral
	movq	-2144(%rbp), %rax
	movsd	%xmm0, -8(%r15)
	movl	%r13d, (%rax,%r13,4)
	movq	-2152(%rbp), %rax
	movl	%r13d, (%r14,%r13,4)
	movl	$1, (%rax,%r13,4)
	addq	$1, %r13
	cmpq	%r13, -2136(%rbp)
	jne	.L10
	movq	-2136(%rbp), %r13
	movq	%r12, %rbx
	movq	%r14, %rdx
	movq	%rax, %rsi
	subq	$8, %rsp
	movq	-2144(%rbp), %r12
	movq	-2272(%rbp), %r8
	pushq	$2048
	movq	-2192(%rbp), %rdi
	movq	%r13, %r9
	pushq	$64
	movq	%r12, %rcx
	pushq	%r13
	call	BrotliHistogramCombineLiteral
	addq	$32, %rsp
	movq	%rax, %r15
	movq	-2200(%rbp), %rax
	leaq	(%rax,%r15), %r10
	cmpq	-2256(%rbp), %r10
	ja	.L103
.L11:
	cmpq	-2264(%rbp), %r10
	ja	.L104
.L15:
	testq	%r15, %r15
	je	.L19
	movq	-2200(%rbp), %rax
	movq	-2248(%rbp), %rcx
	movq	-2192(%rbp), %r11
	movq	%rax, %rdx
	leaq	(%rcx,%rax,4), %r9
	salq	$6, %rdx
	addq	%rax, %rdx
	xorl	%eax, %eax
	salq	$4, %rdx
	addq	-2184(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L20:
	movl	(%r12,%rax,4), %r8d
	movq	%rdx, %rdi
	movl	$130, %ecx
	addq	$1040, %rdx
	movq	%r8, %rsi
	salq	$6, %rsi
	addq	%r8, %rsi
	salq	$4, %rsi
	addq	%r11, %rsi
	rep movsq
	movl	-2128(%rbp,%r8,4), %ecx
	movl	%ecx, (%r9,%rax,4)
	movl	(%r12,%rax,4), %ecx
	movl	%eax, -1360(%rbp,%rcx,4)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L20
	movq	%r10, -2200(%rbp)
.L19:
	movl	-2216(%rbp), %ecx
	movq	-2232(%rbp), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movl	(%r14,%rax,4), %edx
	movl	-1360(%rbp,%rdx,4), %edi
	addl	%ecx, %edi
	movl	%edi, (%rsi,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L21
	addq	$64, -2176(%rbp)
	movq	-2176(%rbp), %rax
	addq	%r15, -2216(%rbp)
	addq	$256, -2232(%rbp)
	cmpq	%rax, -2280(%rbp)
	jne	.L22
	movq	-2192(%rbp), %rsi
	movq	-2240(%rbp), %rdi
	call	BrotliFree
	movq	-2216(%rbp), %rax
	movq	%rax, %rbx
	shrq	%rbx
	imulq	%rax, %rbx
	salq	$6, %rax
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	leaq	1(%rbx), %r12
	cmpq	$2049, %r12
	jbe	.L23
	movq	-2240(%rbp), %r14
	movq	-2272(%rbp), %rsi
	movq	%r14, %rdi
	call	BrotliFree
	leaq	(%r12,%r12,2), %rsi
	movq	%r14, %rdi
	salq	$3, %rsi
	call	BrotliAllocate
	movq	%rax, -2272(%rbp)
.L23:
	cmpq	$0, -2216(%rbp)
	jne	.L105
	movq	-2248(%rbp), %r14
	pushq	%rax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-2208(%rbp), %rdx
	movq	-2184(%rbp), %rdi
	pushq	%rbx
	movq	-2272(%rbp), %rbx
	pushq	$256
	movq	%r14, %rsi
	pushq	-2168(%rbp)
	movq	%rbx, %r8
	call	BrotliHistogramCombineLiteral
	movq	%rbx, %rsi
	movq	-2240(%rbp), %rbx
	addq	$32, %rsp
	movq	%rax, -2216(%rbp)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	$0, -2200(%rbp)
	movq	$0, -2176(%rbp)
.L51:
	movq	-2200(%rbp), %rax
	movq	-2216(%rbp), %rcx
	movl	$0, -2232(%rbp)
	leaq	-1104(%rbp), %r14
	movq	$0, -2144(%rbp)
	movq	-2224(%rbp), %rbx
	movq	$0, -2192(%rbp)
	leaq	(%rax,%rcx,4), %r12
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%eax, %eax
	movl	$128, %ecx
	movq	%r14, %rdi
	movq	$0, -80(%rbp)
	rep stosq
	movq	.LC1(%rip), %rax
	movl	(%rbx), %edx
	movq	%rax, -72(%rbp)
	testl	%edx, %edx
	je	.L28
	movq	-2192(%rbp), %rcx
	movq	-2160(%rbp), %rsi
	xorl	%eax, %eax
	leaq	1(%rcx), %rdi
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L29:
	movzbl	(%rsi,%rax), %edx
	leaq	(%rdi,%rax), %rcx
	addq	$1, %rax
	addl	$1, -1104(%rbp,%rdx,4)
	movl	(%rbx), %edx
	cmpq	%rax, %rdx
	ja	.L29
	movq	%rax, -80(%rbp)
	movq	%rcx, -2192(%rbp)
.L28:
	cmpq	$0, -2144(%rbp)
	movq	-2208(%rbp), %rax
	je	.L106
	movq	-2144(%rbp), %rdi
	movl	-4(%rax,%rdi,4), %r15d
.L31:
	movl	%r15d, %eax
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, -2152(%rbp)
	salq	$6, %rsi
	addq	%rax, %rsi
	salq	$4, %rsi
	addq	-2184(%rbp), %rsi
	call	BrotliHistogramBitCostDistanceLiteral
	cmpq	$0, -2216(%rbp)
	movq	-2152(%rbp), %rax
	movsd	%xmm0, -2136(%rbp)
	je	.L32
	movq	-2200(%rbp), %r13
	movq	%rbx, -2152(%rbp)
	movq	%r13, %rbx
	movq	-2184(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L35:
	movl	(%rbx), %edx
	movq	%r14, %rdi
	movq	%rdx, %rsi
	salq	$6, %rsi
	addq	%rdx, %rsi
	salq	$4, %rsi
	addq	%r13, %rsi
	call	BrotliHistogramBitCostDistanceLiteral
	movsd	-2136(%rbp), %xmm5
	comisd	%xmm0, %xmm5
	jbe	.L33
	movl	(%rbx), %r15d
	movsd	%xmm0, -2136(%rbp)
.L33:
	addq	$4, %rbx
	cmpq	%r12, %rbx
	jne	.L35
	movq	-2152(%rbp), %rbx
	movl	%r15d, %eax
.L32:
	movq	-2208(%rbp), %rcx
	movq	-2144(%rbp), %rdi
	movl	%r15d, (%rcx,%rdi,4)
	movq	-2176(%rbp), %rcx
	leaq	(%rcx,%rax,4), %rax
	cmpl	$-1, (%rax)
	je	.L107
	addq	$1, -2144(%rbp)
	addq	$4, %rbx
	movq	-2144(%rbp), %rax
	cmpq	%rax, -2168(%rbp)
	jne	.L39
.L38:
	movq	-2240(%rbp), %rbx
	movq	-2200(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-2184(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-2288(%rbp), %rax
	movq	32(%rax), %r12
	movq	-2168(%rbp), %rax
	cmpq	%rax, %r12
	jnb	.L40
	testq	%r12, %r12
	je	.L64
.L42:
	addq	%r12, %r12
	cmpq	%r12, %rax
	ja	.L42
.L41:
	movq	-2240(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-2288(%rbp), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L108
.L43:
	movq	-2288(%rbp), %r14
	movq	-2240(%rbp), %rdi
	movq	16(%r14), %rsi
	call	BrotliFree
	movq	%rbx, 16(%r14)
	movq	%r12, 32(%r14)
.L40:
	movq	-2288(%rbp), %rax
	movq	40(%rax), %r12
	cmpq	-2168(%rbp), %r12
	jnb	.L55
	testq	%r12, %r12
	jne	.L109
	movq	-2168(%rbp), %r12
.L44:
	movq	-2296(%rbp), %rsi
	movq	-2240(%rbp), %rdi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-2288(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L110
.L46:
	movq	-2288(%rbp), %r14
	movq	-2240(%rbp), %rdi
	movq	24(%r14), %rsi
	call	BrotliFree
	cmpq	$0, -2168(%rbp)
	movq	%rbx, 24(%r14)
	movq	%r12, 40(%r14)
	je	.L66
.L55:
	movq	-2208(%rbp), %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	-2288(%rbp), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-2224(%rbp), %rbx
	addl	(%rbx,%rax,4), %ecx
	addq	$1, %rax
	movl	-4(%r8,%rax,4), %edx
	cmpq	-2168(%rbp), %rax
	je	.L48
	cmpl	%edx, (%r8,%rax,4)
	je	.L50
	movq	-2176(%rbp), %rbx
	movq	16(%r9), %r10
	movl	(%rbx,%rdx,4), %edx
	movb	%dl, (%r10,%rdi)
	movq	24(%r9), %r10
	cmpb	%dl, %sil
	cmovb	%edx, %esi
	movl	%ecx, (%r10,%rdi,4)
	addq	$1, %rdi
	xorl	%ecx, %ecx
	jmp	.L50
.L104:
	movq	-2264(%rbp), %rax
	testq	%rax, %rax
	jne	.L111
	movq	-2240(%rbp), %rdi
	leaq	0(,%r10,4), %rsi
	movq	%r10, -2136(%rbp)
	call	BrotliAllocate
	movq	-2136(%rbp), %r10
	movq	%rax, -2144(%rbp)
	movq	%r10, -2264(%rbp)
.L52:
	movq	-2248(%rbp), %rsi
	movq	-2240(%rbp), %rdi
	movq	%r10, -2136(%rbp)
	call	BrotliFree
	movq	-2144(%rbp), %rax
	movq	-2136(%rbp), %r10
	movq	%rax, -2248(%rbp)
	jmp	.L15
.L103:
	movq	-2256(%rbp), %rax
	testq	%rax, %rax
	je	.L12
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rcx, %rax
	addq	%rcx, %rcx
	cmpq	%rcx, %r10
	ja	.L13
	movq	%rax, %rsi
	movq	-2240(%rbp), %rdi
	movq	%r10, -2136(%rbp)
	salq	$6, %rsi
	movq	%rcx, -2304(%rbp)
	addq	%rsi, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	call	BrotliAllocate
	movq	-2256(%rbp), %rcx
	movq	-2184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -2144(%rbp)
	movq	%rcx, %rdx
	salq	$6, %rdx
	addq	%rcx, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	-2304(%rbp), %rcx
	movq	-2136(%rbp), %r10
	movq	%rcx, -2256(%rbp)
.L53:
	movq	-2184(%rbp), %rsi
	movq	-2240(%rbp), %rdi
	movq	%r10, -2136(%rbp)
	call	BrotliFree
	movq	-2144(%rbp), %rax
	movq	-2136(%rbp), %r10
	movq	%rax, -2184(%rbp)
	jmp	.L11
.L12:
	movq	%r10, %rsi
	movq	-2240(%rbp), %rdi
	movq	%r10, -2136(%rbp)
	salq	$6, %rsi
	addq	%r10, %rsi
	salq	$4, %rsi
	call	BrotliAllocate
	movq	-2136(%rbp), %r10
	movq	%rax, -2144(%rbp)
	movq	%r10, -2256(%rbp)
	jmp	.L53
.L111:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rcx, %rax
	addq	%rcx, %rcx
	cmpq	%rcx, %r10
	ja	.L17
	movq	-2240(%rbp), %rdi
	leaq	0(,%rax,8), %rsi
	movq	%rcx, -2304(%rbp)
	movq	%r10, -2136(%rbp)
	call	BrotliAllocate
	movq	-2264(%rbp), %rdx
	movq	-2248(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -2144(%rbp)
	salq	$2, %rdx
	call	memcpy@PLT
	movq	-2304(%rbp), %rcx
	movq	-2136(%rbp), %r10
	movq	%rcx, -2264(%rbp)
	jmp	.L52
.L106:
	movl	(%rax), %r15d
	jmp	.L31
.L107:
	movl	-2232(%rbp), %ecx
	addq	$1, -2144(%rbp)
	addq	$4, %rbx
	movl	%ecx, (%rax)
	leal	1(%rcx), %edx
	movq	-2144(%rbp), %rax
	cmpq	%rax, -2168(%rbp)
	je	.L38
	movl	%edx, -2232(%rbp)
	jmp	.L39
.L105:
	movq	-2216(%rbp), %r14
	movq	-2240(%rbp), %rdi
	leaq	0(,%r14,4), %r12
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%r14, %rdi
	movq	%rax, -2200(%rbp)
	movq	%rax, %rcx
	leaq	-1(%r14), %rax
	cmpq	$2, %rax
	jbe	.L62
	shrq	$2, %rdi
	movdqa	.LC0(%rip), %xmm1
	movq	%rcx, %rax
	movdqa	.LC2(%rip), %xmm4
	movq	%rdi, %rdx
	movdqa	.LC3(%rip), %xmm3
	salq	$4, %rdx
	addq	%rcx, %rdx
.L26:
	movdqa	%xmm1, %xmm0
	addq	$16, %rax
	paddq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm3, %xmm2
	shufps	$136, %xmm2, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L26
	movq	-2216(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-4, %rax
	andl	$3, %edi
	je	.L27
.L25:
	movq	-2200(%rbp), %rdi
	movq	-2216(%rbp), %rcx
	leaq	1(%rax), %rdx
	movl	%eax, (%rdi,%rax,4)
	cmpq	%rdx, %rcx
	jbe	.L27
	addq	$2, %rax
	movl	%edx, (%rdi,%rdx,4)
	cmpq	%rax, %rcx
	jbe	.L27
	movl	%eax, (%rdi,%rax,4)
.L27:
	subq	$8, %rsp
	movq	-2248(%rbp), %r14
	movq	-2208(%rbp), %rdx
	movq	-2216(%rbp), %r9
	movq	-2200(%rbp), %rcx
	pushq	%rbx
	movq	-2272(%rbp), %rbx
	pushq	$256
	movq	%r14, %rsi
	pushq	-2168(%rbp)
	movq	-2184(%rbp), %rdi
	movq	%rbx, %r8
	call	BrotliHistogramCombineLiteral
	movq	%rbx, %rsi
	movq	-2240(%rbp), %rbx
	addq	$32, %rsp
	movq	%rax, -2216(%rbp)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliAllocate
	movq	%r12, %rdx
	movl	$255, %esi
	movq	%rax, %rdi
	movq	%rax, -2176(%rbp)
	call	memset@PLT
	jmp	.L51
.L48:
	movq	-2288(%rbp), %rbx
	movq	-2176(%rbp), %rax
	movl	(%rax,%rdx,4), %eax
	movq	16(%rbx), %rdx
	cmpb	%al, %sil
	movb	%al, (%rdx,%rdi)
	movq	24(%rbx), %rdx
	leaq	1(%rdi), %rbx
	cmovnb	%esi, %eax
	movq	%rbx, -2168(%rbp)
	movl	%ecx, (%rdx,%rdi,4)
	movzbl	%al, %eax
	addq	$1, %rax
.L47:
	movq	-2240(%rbp), %rbx
	movq	%rax, %xmm0
	movq	-2288(%rbp), %rax
	movhps	-2168(%rbp), %xmm0
	movq	-2176(%rbp), %rsi
	movups	%xmm0, (%rax)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-2224(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-2208(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	movq	-2168(%rbp), %rdx
.L45:
	movq	%r12, %rax
	addq	%r12, %r12
	cmpq	%r12, %rdx
	ja	.L45
	salq	$3, %rax
	movq	%rax, -2296(%rbp)
	jmp	.L44
.L64:
	movq	%rax, %r12
	jmp	.L41
.L101:
	movq	-2296(%rbp), %r15
	movq	-2240(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BrotliAllocate
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	call	BrotliAllocate
	movq	%rax, -2224(%rbp)
	cmpq	$63, %r12
	ja	.L3
	movq	$0, -2184(%rbp)
	movq	$0, -2248(%rbp)
.L56:
	movq	-2168(%rbp), %rcx
	movl	$64, %eax
	movq	-2240(%rbp), %r14
	leaq	-1872(%rbp), %r12
	cmpq	$64, %rcx
	movq	%r14, %rdi
	cmovbe	%rcx, %rax
	movq	%rax, %rsi
	salq	$6, %rsi
	addq	%rax, %rsi
	salq	$4, %rsi
	call	BrotliAllocate
	movq	%r14, %rdi
	movl	$49176, %esi
	leaq	-1616(%rbp), %r14
	movq	%rax, -2192(%rbp)
	call	BrotliAllocate
	leaq	-2128(%rbp), %rdi
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%rax, -2272(%rbp)
	xorl	%eax, %eax
	movq	-2296(%rbp), %rdx
	movq	%rdi, -2152(%rbp)
	rep stosq
	movl	$32, %ecx
	movq	%r12, %rdi
	rep stosq
	movl	$32, %ecx
	movq	%r14, %rdi
	rep stosq
	leaq	-1360(%rbp), %rdi
	movl	$32, %ecx
	rep stosq
	movq	-2224(%rbp), %rdi
	call	memset@PLT
	testq	%rbx, %rbx
	jne	.L57
	jmp	.L4
.L108:
	movq	16(%rax), %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	jmp	.L43
.L110:
	leaq	0(,%rax,4), %rdx
	movq	-2288(%rbp), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	jmp	.L46
.L58:
	movq	-2240(%rbp), %rbx
	movq	-2192(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-2272(%rbp), %r12
	movq	-2248(%rbp), %r15
	pushq	$0
	movq	-2184(%rbp), %r14
	pushq	$256
	pushq	-2168(%rbp)
	movq	-2208(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BrotliHistogramCombineLiteral
	addq	$32, %rsp
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movl	$1, %eax
	movq	$0, -2176(%rbp)
	movq	$0, -2168(%rbp)
	jmp	.L47
.L62:
	xorl	%eax, %eax
	jmp	.L25
.L112:
	call	__stack_chk_fail@PLT
.L3:
	movq	-2256(%rbp), %r14
	movq	-2240(%rbp), %r15
	imulq	$1040, %r14, %rsi
	movq	%r15, %rdi
	call	BrotliAllocate
	leaq	0(,%r14,4), %rsi
	movq	%r15, %rdi
	movq	%rax, -2184(%rbp)
	call	BrotliAllocate
	movq	%rax, -2248(%rbp)
	jmp	.L56
.L66:
	movl	$1, %eax
	jmp	.L47
	.cfi_endproc
.LFE103:
	.size	ClusterBlocksLiteral, .-ClusterBlocksLiteral
	.p2align 4
	.type	ClusterBlocksCommand, @function
ClusterBlocksCommand:
.LFB111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	63(%rcx), %r12
	pushq	%rbx
	salq	$4, %r12
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$4056, %rsp
	movq	%rdi, -4032(%rbp)
	movq	%rsi, -3952(%rbp)
	movq	%rcx, -3960(%rbp)
	movq	%r9, -4080(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	leaq	0(,%rcx,4), %rsi
	movq	%rsi, -4088(%rbp)
	movq	%r12, %rsi
	shrq	$6, %rsi
	movq	%rsi, -4048(%rbp)
	testq	%rcx, %rcx
	jne	.L212
	movq	%rsi, %r15
	imulq	$2832, %rsi, %rsi
	movq	%rdi, %r14
	leaq	-3664(%rbp), %r12
	call	BrotliAllocate
	movq	%r14, %rdi
	leaq	0(,%r15,4), %rsi
	movq	%rax, -3976(%rbp)
	call	BrotliAllocate
	movq	%r14, %rdi
	movl	$49176, %esi
	leaq	-3408(%rbp), %r14
	movq	%rax, -4040(%rbp)
	call	BrotliAllocate
	leaq	-3920(%rbp), %rdi
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%rax, -4064(%rbp)
	movq	-3960(%rbp), %rax
	movq	%rdi, -3944(%rbp)
	movq	-4088(%rbp), %rdx
	rep stosq
	movl	$32, %ecx
	movq	%r12, %rdi
	rep stosq
	movl	$32, %ecx
	movq	%r14, %rdi
	rep stosq
	leaq	-3152(%rbp), %rdi
	movl	$32, %ecx
	rep stosq
	xorl	%edi, %edi
	call	memset@PLT
	movq	$0, -4000(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -3984(%rbp)
	testq	%rbx, %rbx
	je	.L170
.L169:
	movq	-4016(%rbp), %rcx
	xorl	%eax, %eax
	xorl	%edx, %edx
	addq	$1, %rax
	addl	$1, (%rcx,%rdx,4)
	cmpq	%rbx, %rax
	je	.L117
.L213:
	movzbl	0(%r13,%rax), %esi
	cmpb	%sil, -1(%r13,%rax)
	setne	%sil
	addq	$1, %rax
	movzbl	%sil, %esi
	addq	%rsi, %rdx
	addl	$1, (%rcx,%rdx,4)
	cmpq	%rbx, %rax
	jne	.L213
.L117:
	cmpq	$0, -3960(%rbp)
	je	.L170
.L116:
	movq	-4000(%rbp), %rax
	xorl	%ebx, %ebx
	movq	$0, -3968(%rbp)
	movq	$0, -4008(%rbp)
	movq	%rax, -4024(%rbp)
	movq	-3960(%rbp), %rax
	movq	$0, -3992(%rbp)
	subq	$1, %rax
	andq	$-64, %rax
	addq	$64, %rax
	movq	%rax, -4072(%rbp)
	movq	-4048(%rbp), %rax
	movq	%rax, -4056(%rbp)
	.p2align 4,,10
	.p2align 3
.L134:
	movq	-3968(%rbp), %rax
	movq	-3960(%rbp), %r13
	movl	$64, %esi
	movq	%r12, -3936(%rbp)
	movq	-3984(%rbp), %rdx
	movq	%rbx, %r12
	subq	%rax, %r13
	cmpq	$64, %r13
	cmova	%rsi, %r13
	movq	-4016(%rbp), %rsi
	xorl	%r15d, %r15d
	movq	%r15, %rbx
	leaq	(%rsi,%rax,4), %rsi
	movq	%r13, -3928(%rbp)
	movq	%rdx, %r13
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	8(%r13), %rdi
	movq	%r13, %rcx
	xorl	%eax, %eax
	movq	$0, 0(%r13)
	movq	$0, 2808(%r13)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2816(%r13)
	movl	(%r15), %ecx
	movq	%rax, 2824(%r13)
	testl	%ecx, %ecx
	je	.L120
	movq	-3952(%rbp), %rax
	leaq	1(%r12), %r10
	leaq	(%rax,%r12,2), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L121:
	movzwl	(%rdi,%rax,2), %edx
	leaq	(%r10,%rax), %r9
	addq	$1, %rax
	addl	$1, 0(%r13,%rdx,4)
	movl	(%r15), %edx
	cmpq	%rax, %rdx
	ja	.L121
	movq	%rax, 2816(%r13)
	movq	%r9, %r12
.L120:
	movq	%r13, %rdi
	addq	$4, %r15
	addq	$2832, %r13
	call	BrotliPopulationCostCommand
	movq	-3936(%rbp), %rax
	movsd	%xmm0, -8(%r13)
	movl	%ebx, (%rax,%rbx,4)
	movq	-3944(%rbp), %rax
	movl	%ebx, (%r14,%rbx,4)
	movl	$1, (%rax,%rbx,4)
	addq	$1, %rbx
	cmpq	%rbx, -3928(%rbp)
	jne	.L122
	movq	-3928(%rbp), %r13
	movq	%r12, %rbx
	movq	%r14, %rdx
	movq	%rax, %rsi
	subq	$8, %rsp
	movq	-3936(%rbp), %r12
	movq	-4064(%rbp), %r8
	pushq	$2048
	movq	-3984(%rbp), %rdi
	movq	%r13, %r9
	pushq	$64
	movq	%r12, %rcx
	pushq	%r13
	call	BrotliHistogramCombineCommand
	addq	$32, %rsp
	movq	%rax, %r15
	movq	-3992(%rbp), %rax
	leaq	(%rax,%r15), %r10
	cmpq	-4048(%rbp), %r10
	ja	.L214
.L123:
	cmpq	-4056(%rbp), %r10
	ja	.L215
.L127:
	testq	%r15, %r15
	je	.L131
	movq	-3992(%rbp), %rax
	movq	-4040(%rbp), %rsi
	movq	-3984(%rbp), %r11
	imulq	$2832, %rax, %rdx
	leaq	(%rsi,%rax,4), %r9
	xorl	%eax, %eax
	addq	-3976(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L132:
	movl	(%r12,%rax,4), %r8d
	movq	%rdx, %rdi
	movl	$354, %ecx
	addq	$2832, %rdx
	imulq	$2832, %r8, %rsi
	addq	%r11, %rsi
	rep movsq
	movl	-3920(%rbp,%r8,4), %ecx
	movl	%ecx, (%r9,%rax,4)
	movl	(%r12,%rax,4), %ecx
	movl	%eax, -3152(%rbp,%rcx,4)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L132
	movq	%r10, -3992(%rbp)
.L131:
	movl	-4008(%rbp), %ecx
	movq	-4024(%rbp), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L133:
	movl	(%r14,%rax,4), %edx
	movl	-3152(%rbp,%rdx,4), %edi
	addl	%ecx, %edi
	movl	%edi, (%rsi,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L133
	addq	$64, -3968(%rbp)
	movq	-3968(%rbp), %rax
	addq	%r15, -4008(%rbp)
	addq	$256, -4024(%rbp)
	cmpq	%rax, -4072(%rbp)
	jne	.L134
	movq	-3984(%rbp), %rsi
	movq	-4032(%rbp), %rdi
	call	BrotliFree
	movq	-4008(%rbp), %rax
	movq	%rax, %rbx
	shrq	%rbx
	imulq	%rax, %rbx
	salq	$6, %rax
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	leaq	1(%rbx), %r12
	cmpq	$2049, %r12
	jbe	.L135
	movq	-4032(%rbp), %r14
	movq	-4064(%rbp), %rsi
	movq	%r14, %rdi
	call	BrotliFree
	leaq	(%r12,%r12,2), %rsi
	movq	%r14, %rdi
	salq	$3, %rsi
	call	BrotliAllocate
	movq	%rax, -4064(%rbp)
.L135:
	cmpq	$0, -4008(%rbp)
	jne	.L216
	movq	-4040(%rbp), %r14
	pushq	%rax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-4000(%rbp), %rdx
	movq	-3976(%rbp), %rdi
	pushq	%rbx
	movq	-4064(%rbp), %rbx
	pushq	$256
	movq	%r14, %rsi
	pushq	-3960(%rbp)
	movq	%rbx, %r8
	call	BrotliHistogramCombineCommand
	movq	%rbx, %rsi
	movq	-4032(%rbp), %rbx
	addq	$32, %rsp
	movq	%rax, -4008(%rbp)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	$0, -3992(%rbp)
	movq	$0, -3968(%rbp)
.L163:
	movq	-3992(%rbp), %rax
	movq	-4008(%rbp), %rsi
	movl	$0, -4024(%rbp)
	leaq	-2896(%rbp), %r14
	movq	$0, -3936(%rbp)
	movq	-4016(%rbp), %rbx
	movq	$0, -3984(%rbp)
	leaq	(%rax,%rsi,4), %r12
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%eax, %eax
	movl	$352, %ecx
	movq	%r14, %rdi
	movq	$0, -80(%rbp)
	rep stosq
	movq	.LC1(%rip), %rax
	movl	(%rbx), %edx
	movq	%rax, -72(%rbp)
	testl	%edx, %edx
	je	.L140
	movq	-3952(%rbp), %rax
	movq	-3984(%rbp), %rsi
	leaq	(%rax,%rsi,2), %rdi
	xorl	%eax, %eax
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L141:
	movzwl	(%rdi,%rax,2), %edx
	leaq	(%rsi,%rax), %rcx
	addq	$1, %rax
	addl	$1, -2896(%rbp,%rdx,4)
	movl	(%rbx), %edx
	cmpq	%rax, %rdx
	ja	.L141
	movq	%rax, -80(%rbp)
	movq	%rcx, -3984(%rbp)
.L140:
	cmpq	$0, -3936(%rbp)
	movq	-4000(%rbp), %rax
	je	.L217
	movq	-3936(%rbp), %rsi
	movl	-4(%rax,%rsi,4), %r15d
.L143:
	movl	%r15d, %eax
	movq	%r14, %rdi
	imulq	$2832, %rax, %rsi
	addq	-3976(%rbp), %rsi
	movq	%rax, -3944(%rbp)
	call	BrotliHistogramBitCostDistanceCommand
	cmpq	$0, -4008(%rbp)
	movq	-3944(%rbp), %rax
	movsd	%xmm0, -3928(%rbp)
	je	.L144
	movq	-3992(%rbp), %r13
	movq	%rbx, -3944(%rbp)
	movq	%r13, %rbx
	movq	-3976(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L147:
	movl	(%rbx), %esi
	movq	%r14, %rdi
	imulq	$2832, %rsi, %rsi
	addq	%r13, %rsi
	call	BrotliHistogramBitCostDistanceCommand
	movsd	-3928(%rbp), %xmm5
	comisd	%xmm0, %xmm5
	jbe	.L145
	movl	(%rbx), %r15d
	movsd	%xmm0, -3928(%rbp)
.L145:
	addq	$4, %rbx
	cmpq	%r12, %rbx
	jne	.L147
	movq	-3944(%rbp), %rbx
	movl	%r15d, %eax
.L144:
	movq	-4000(%rbp), %rsi
	movq	-3936(%rbp), %rcx
	movl	%r15d, (%rsi,%rcx,4)
	movq	-3968(%rbp), %rsi
	leaq	(%rsi,%rax,4), %rax
	cmpl	$-1, (%rax)
	je	.L218
	addq	$1, -3936(%rbp)
	addq	$4, %rbx
	movq	-3936(%rbp), %rax
	cmpq	%rax, -3960(%rbp)
	jne	.L151
.L150:
	movq	-4032(%rbp), %rbx
	movq	-3992(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-3976(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-4080(%rbp), %rax
	movq	32(%rax), %r12
	movq	-3960(%rbp), %rax
	cmpq	%rax, %r12
	jnb	.L152
	testq	%r12, %r12
	je	.L176
.L154:
	addq	%r12, %r12
	cmpq	%r12, %rax
	ja	.L154
.L153:
	movq	-4032(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-4080(%rbp), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L219
.L155:
	movq	-4080(%rbp), %r14
	movq	-4032(%rbp), %rdi
	movq	16(%r14), %rsi
	call	BrotliFree
	movq	%rbx, 16(%r14)
	movq	%r12, 32(%r14)
.L152:
	movq	-4080(%rbp), %rax
	movq	40(%rax), %r12
	cmpq	-3960(%rbp), %r12
	jnb	.L167
	testq	%r12, %r12
	jne	.L220
	movq	-3960(%rbp), %r12
.L156:
	movq	-4088(%rbp), %rsi
	movq	-4032(%rbp), %rdi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-4080(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L221
.L158:
	movq	-4080(%rbp), %r14
	movq	-4032(%rbp), %rdi
	movq	24(%r14), %rsi
	call	BrotliFree
	cmpq	$0, -3960(%rbp)
	movq	%rbx, 24(%r14)
	movq	%r12, 40(%r14)
	je	.L178
.L167:
	movq	-4000(%rbp), %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	-4080(%rbp), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-4016(%rbp), %rbx
	addl	(%rbx,%rax,4), %ecx
	addq	$1, %rax
	movl	-4(%r8,%rax,4), %edx
	cmpq	-3960(%rbp), %rax
	je	.L160
	cmpl	%edx, (%r8,%rax,4)
	je	.L162
	movq	-3968(%rbp), %rbx
	movq	16(%r9), %r10
	movl	(%rbx,%rdx,4), %edx
	movb	%dl, (%r10,%rdi)
	movq	24(%r9), %r10
	cmpb	%dl, %sil
	cmovb	%edx, %esi
	movl	%ecx, (%r10,%rdi,4)
	addq	$1, %rdi
	xorl	%ecx, %ecx
	jmp	.L162
.L215:
	movq	-4056(%rbp), %rax
	testq	%rax, %rax
	jne	.L222
	movq	-4032(%rbp), %rdi
	leaq	0(,%r10,4), %rsi
	movq	%r10, -3928(%rbp)
	call	BrotliAllocate
	movq	-3928(%rbp), %r10
	movq	%rax, -3936(%rbp)
	movq	%r10, -4056(%rbp)
.L164:
	movq	-4040(%rbp), %rsi
	movq	-4032(%rbp), %rdi
	movq	%r10, -3928(%rbp)
	call	BrotliFree
	movq	-3936(%rbp), %rax
	movq	-3928(%rbp), %r10
	movq	%rax, -4040(%rbp)
	jmp	.L127
.L214:
	movq	-4048(%rbp), %rax
	testq	%rax, %rax
	je	.L124
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%rcx, %rax
	addq	%rcx, %rcx
	cmpq	%rcx, %r10
	ja	.L125
	imulq	$5664, %rax, %rsi
	movq	-4032(%rbp), %rdi
	movq	%rcx, -4096(%rbp)
	movq	%r10, -3928(%rbp)
	call	BrotliAllocate
	movq	-3976(%rbp), %rsi
	imulq	$2832, -4048(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -3936(%rbp)
	call	memcpy@PLT
	movq	-4096(%rbp), %rcx
	movq	-3928(%rbp), %r10
	movq	%rcx, -4048(%rbp)
.L165:
	movq	-3976(%rbp), %rsi
	movq	-4032(%rbp), %rdi
	movq	%r10, -3928(%rbp)
	call	BrotliFree
	movq	-3936(%rbp), %rax
	movq	-3928(%rbp), %r10
	movq	%rax, -3976(%rbp)
	jmp	.L123
.L124:
	imulq	$2832, %r10, %rsi
	movq	-4032(%rbp), %rdi
	movq	%r10, -3928(%rbp)
	call	BrotliAllocate
	movq	-3928(%rbp), %r10
	movq	%rax, -3936(%rbp)
	movq	%r10, -4048(%rbp)
	jmp	.L165
.L222:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rcx, %rax
	addq	%rcx, %rcx
	cmpq	%rcx, %r10
	ja	.L129
	movq	-4032(%rbp), %rdi
	leaq	0(,%rax,8), %rsi
	movq	%rcx, -4096(%rbp)
	movq	%r10, -3928(%rbp)
	call	BrotliAllocate
	movq	-4056(%rbp), %rdx
	movq	-4040(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -3936(%rbp)
	salq	$2, %rdx
	call	memcpy@PLT
	movq	-4096(%rbp), %rcx
	movq	-3928(%rbp), %r10
	movq	%rcx, -4056(%rbp)
	jmp	.L164
.L217:
	movl	(%rax), %r15d
	jmp	.L143
.L218:
	movl	-4024(%rbp), %esi
	addq	$1, -3936(%rbp)
	addq	$4, %rbx
	movl	%esi, (%rax)
	leal	1(%rsi), %edx
	movq	-3936(%rbp), %rax
	cmpq	%rax, -3960(%rbp)
	je	.L150
	movl	%edx, -4024(%rbp)
	jmp	.L151
.L216:
	movq	-4008(%rbp), %r14
	movq	-4032(%rbp), %rdi
	leaq	0(,%r14,4), %r12
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%r14, %rsi
	movq	%rax, -3992(%rbp)
	movq	%rax, %rcx
	leaq	-1(%r14), %rax
	cmpq	$2, %rax
	jbe	.L174
	shrq	$2, %rsi
	movdqa	.LC0(%rip), %xmm1
	movq	%rcx, %rax
	movdqa	.LC2(%rip), %xmm4
	movq	%rsi, %rdx
	movdqa	.LC3(%rip), %xmm3
	salq	$4, %rdx
	addq	%rcx, %rdx
.L138:
	movdqa	%xmm1, %xmm0
	addq	$16, %rax
	paddq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm3, %xmm2
	shufps	$136, %xmm2, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L138
	movq	-4008(%rbp), %rsi
	movq	%rsi, %rax
	andq	$-4, %rax
	andl	$3, %esi
	je	.L139
.L137:
	movq	-3992(%rbp), %rsi
	movq	-4008(%rbp), %rcx
	leaq	1(%rax), %rdx
	movl	%eax, (%rsi,%rax,4)
	cmpq	%rdx, %rcx
	jbe	.L139
	addq	$2, %rax
	movl	%edx, (%rsi,%rdx,4)
	cmpq	%rax, %rcx
	jbe	.L139
	movl	%eax, (%rsi,%rax,4)
.L139:
	subq	$8, %rsp
	movq	-4040(%rbp), %r14
	movq	-4000(%rbp), %rdx
	movq	-4008(%rbp), %r9
	movq	-3992(%rbp), %rcx
	pushq	%rbx
	movq	-4064(%rbp), %rbx
	pushq	$256
	movq	%r14, %rsi
	pushq	-3960(%rbp)
	movq	-3976(%rbp), %rdi
	movq	%rbx, %r8
	call	BrotliHistogramCombineCommand
	movq	%rbx, %rsi
	movq	-4032(%rbp), %rbx
	addq	$32, %rsp
	movq	%rax, -4008(%rbp)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliAllocate
	movq	%r12, %rdx
	movl	$255, %esi
	movq	%rax, %rdi
	movq	%rax, -3968(%rbp)
	call	memset@PLT
	jmp	.L163
.L160:
	movq	-4080(%rbp), %rbx
	movq	-3968(%rbp), %rax
	movl	(%rax,%rdx,4), %eax
	movq	16(%rbx), %rdx
	cmpb	%al, %sil
	movb	%al, (%rdx,%rdi)
	movq	24(%rbx), %rdx
	leaq	1(%rdi), %rbx
	cmovnb	%esi, %eax
	movq	%rbx, -3960(%rbp)
	movl	%ecx, (%rdx,%rdi,4)
	movzbl	%al, %eax
	addq	$1, %rax
.L159:
	movq	-4032(%rbp), %rbx
	movq	%rax, %xmm0
	movq	-4080(%rbp), %rax
	movhps	-3960(%rbp), %xmm0
	movq	-3968(%rbp), %rsi
	movups	%xmm0, (%rax)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-4016(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-4000(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	movq	-3960(%rbp), %rdx
.L157:
	movq	%r12, %rax
	addq	%r12, %r12
	cmpq	%r12, %rdx
	ja	.L157
	salq	$3, %rax
	movq	%rax, -4088(%rbp)
	jmp	.L156
.L176:
	movq	%rax, %r12
	jmp	.L153
.L212:
	movq	-4088(%rbp), %r15
	movq	%rdi, %r14
	movq	%r15, %rsi
	call	BrotliAllocate
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -4000(%rbp)
	call	BrotliAllocate
	movq	%rax, -4016(%rbp)
	cmpq	$63, %r12
	ja	.L115
	movq	$0, -3976(%rbp)
	movq	$0, -4040(%rbp)
.L168:
	movq	-3960(%rbp), %rax
	movl	$64, %esi
	movq	-4032(%rbp), %r14
	leaq	-3664(%rbp), %r12
	cmpq	$64, %rax
	movq	%r14, %rdi
	cmovbe	%rax, %rsi
	imulq	$2832, %rsi, %rsi
	call	BrotliAllocate
	movq	%r14, %rdi
	movl	$49176, %esi
	leaq	-3408(%rbp), %r14
	movq	%rax, -3984(%rbp)
	call	BrotliAllocate
	leaq	-3920(%rbp), %rdi
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%rax, -4064(%rbp)
	xorl	%eax, %eax
	movq	-4088(%rbp), %rdx
	movq	%rdi, -3944(%rbp)
	rep stosq
	movl	$32, %ecx
	movq	%r12, %rdi
	rep stosq
	movl	$32, %ecx
	movq	%r14, %rdi
	rep stosq
	leaq	-3152(%rbp), %rdi
	movl	$32, %ecx
	rep stosq
	movq	-4016(%rbp), %rdi
	call	memset@PLT
	testq	%rbx, %rbx
	jne	.L169
	jmp	.L116
.L219:
	movq	16(%rax), %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	jmp	.L155
.L221:
	leaq	0(,%rax,4), %rdx
	movq	-4080(%rbp), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	jmp	.L158
.L170:
	movq	-4032(%rbp), %rbx
	movq	-3984(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-4064(%rbp), %r12
	movq	-4040(%rbp), %r15
	pushq	$0
	movq	-3976(%rbp), %r14
	pushq	$256
	pushq	-3960(%rbp)
	movq	-4000(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BrotliHistogramCombineCommand
	addq	$32, %rsp
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movl	$1, %eax
	movq	$0, -3968(%rbp)
	movq	$0, -3960(%rbp)
	jmp	.L159
.L174:
	xorl	%eax, %eax
	jmp	.L137
.L223:
	call	__stack_chk_fail@PLT
.L115:
	movq	-4048(%rbp), %r14
	movq	-4032(%rbp), %r15
	imulq	$2832, %r14, %rsi
	movq	%r15, %rdi
	call	BrotliAllocate
	leaq	0(,%r14,4), %rsi
	movq	%r15, %rdi
	movq	%rax, -3976(%rbp)
	call	BrotliAllocate
	movq	%rax, -4040(%rbp)
	jmp	.L168
.L178:
	movl	$1, %eax
	jmp	.L159
	.cfi_endproc
.LFE111:
	.size	ClusterBlocksCommand, .-ClusterBlocksCommand
	.p2align 4
	.type	ClusterBlocksDistance, @function
ClusterBlocksDistance:
.LFB119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	63(%rcx), %r12
	pushq	%rbx
	salq	$4, %r12
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$3416, %rsp
	movq	%rdi, -3392(%rbp)
	movq	%rsi, -3312(%rbp)
	movq	%rcx, -3320(%rbp)
	movq	%r9, -3440(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	leaq	0(,%rcx,4), %rsi
	movq	%rsi, -3448(%rbp)
	movq	%r12, %rsi
	shrq	$6, %rsi
	movq	%rsi, -3408(%rbp)
	testq	%rcx, %rcx
	jne	.L323
	movq	%rsi, %r15
	imulq	$2192, %rsi, %rsi
	movq	%rdi, %r14
	leaq	-3024(%rbp), %r12
	call	BrotliAllocate
	movq	%r14, %rdi
	leaq	0(,%r15,4), %rsi
	movq	%rax, -3336(%rbp)
	call	BrotliAllocate
	movq	%r14, %rdi
	movl	$49176, %esi
	leaq	-2768(%rbp), %r14
	movq	%rax, -3400(%rbp)
	call	BrotliAllocate
	leaq	-3280(%rbp), %rdi
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%rax, -3424(%rbp)
	movq	-3320(%rbp), %rax
	movq	%rdi, -3304(%rbp)
	movq	-3448(%rbp), %rdx
	rep stosq
	movl	$32, %ecx
	movq	%r12, %rdi
	rep stosq
	movl	$32, %ecx
	movq	%r14, %rdi
	rep stosq
	leaq	-2512(%rbp), %rdi
	movl	$32, %ecx
	rep stosq
	xorl	%edi, %edi
	call	memset@PLT
	movq	$0, -3360(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3344(%rbp)
	testq	%rbx, %rbx
	je	.L281
.L280:
	movq	-3376(%rbp), %rcx
	xorl	%eax, %eax
	xorl	%edx, %edx
	addq	$1, %rax
	addl	$1, (%rcx,%rdx,4)
	cmpq	%rbx, %rax
	je	.L228
.L324:
	movzbl	0(%r13,%rax), %esi
	cmpb	%sil, -1(%r13,%rax)
	setne	%sil
	addq	$1, %rax
	movzbl	%sil, %esi
	addq	%rsi, %rdx
	addl	$1, (%rcx,%rdx,4)
	cmpq	%rbx, %rax
	jne	.L324
.L228:
	cmpq	$0, -3320(%rbp)
	je	.L281
.L227:
	movq	-3360(%rbp), %rax
	xorl	%ebx, %ebx
	movq	$0, -3328(%rbp)
	movq	$0, -3368(%rbp)
	movq	%rax, -3384(%rbp)
	movq	-3320(%rbp), %rax
	movq	$0, -3352(%rbp)
	subq	$1, %rax
	andq	$-64, %rax
	addq	$64, %rax
	movq	%rax, -3432(%rbp)
	movq	-3408(%rbp), %rax
	movq	%rax, -3416(%rbp)
	.p2align 4,,10
	.p2align 3
.L245:
	movq	-3328(%rbp), %rax
	movq	-3320(%rbp), %r13
	movl	$64, %esi
	movq	%r12, -3296(%rbp)
	movq	-3344(%rbp), %rdx
	movq	%rbx, %r12
	subq	%rax, %r13
	cmpq	$64, %r13
	cmova	%rsi, %r13
	movq	-3376(%rbp), %rsi
	xorl	%r15d, %r15d
	movq	%r15, %rbx
	leaq	(%rsi,%rax,4), %rsi
	movq	%r13, -3288(%rbp)
	movq	%rdx, %r13
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	8(%r13), %rdi
	movq	%r13, %rcx
	xorl	%eax, %eax
	movq	$0, 0(%r13)
	movq	$0, 2168(%r13)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	.LC1(%rip), %rax
	movq	$0, 2176(%r13)
	movl	(%r15), %ecx
	movq	%rax, 2184(%r13)
	testl	%ecx, %ecx
	je	.L231
	movq	-3312(%rbp), %rax
	leaq	1(%r12), %r10
	leaq	(%rax,%r12,2), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L232:
	movzwl	(%rdi,%rax,2), %edx
	leaq	(%r10,%rax), %r9
	addq	$1, %rax
	addl	$1, 0(%r13,%rdx,4)
	movl	(%r15), %edx
	cmpq	%rax, %rdx
	ja	.L232
	movq	%rax, 2176(%r13)
	movq	%r9, %r12
.L231:
	movq	%r13, %rdi
	addq	$4, %r15
	addq	$2192, %r13
	call	BrotliPopulationCostDistance
	movq	-3296(%rbp), %rax
	movsd	%xmm0, -8(%r13)
	movl	%ebx, (%rax,%rbx,4)
	movq	-3304(%rbp), %rax
	movl	%ebx, (%r14,%rbx,4)
	movl	$1, (%rax,%rbx,4)
	addq	$1, %rbx
	cmpq	%rbx, -3288(%rbp)
	jne	.L233
	movq	-3288(%rbp), %r13
	movq	%r12, %rbx
	movq	%r14, %rdx
	movq	%rax, %rsi
	subq	$8, %rsp
	movq	-3296(%rbp), %r12
	movq	-3424(%rbp), %r8
	pushq	$2048
	movq	-3344(%rbp), %rdi
	movq	%r13, %r9
	pushq	$64
	movq	%r12, %rcx
	pushq	%r13
	call	BrotliHistogramCombineDistance
	addq	$32, %rsp
	movq	%rax, %r15
	movq	-3352(%rbp), %rax
	leaq	(%rax,%r15), %r10
	cmpq	-3408(%rbp), %r10
	ja	.L325
.L234:
	cmpq	-3416(%rbp), %r10
	ja	.L326
.L238:
	testq	%r15, %r15
	je	.L242
	movq	-3352(%rbp), %rax
	movq	-3400(%rbp), %rsi
	movq	-3344(%rbp), %r11
	imulq	$2192, %rax, %rdx
	leaq	(%rsi,%rax,4), %r9
	xorl	%eax, %eax
	addq	-3336(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L243:
	movl	(%r12,%rax,4), %r8d
	movq	%rdx, %rdi
	movl	$274, %ecx
	addq	$2192, %rdx
	imulq	$2192, %r8, %rsi
	addq	%r11, %rsi
	rep movsq
	movl	-3280(%rbp,%r8,4), %ecx
	movl	%ecx, (%r9,%rax,4)
	movl	(%r12,%rax,4), %ecx
	movl	%eax, -2512(%rbp,%rcx,4)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L243
	movq	%r10, -3352(%rbp)
.L242:
	movl	-3368(%rbp), %ecx
	movq	-3384(%rbp), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L244:
	movl	(%r14,%rax,4), %edx
	movl	-2512(%rbp,%rdx,4), %edi
	addl	%ecx, %edi
	movl	%edi, (%rsi,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L244
	addq	$64, -3328(%rbp)
	movq	-3328(%rbp), %rax
	addq	%r15, -3368(%rbp)
	addq	$256, -3384(%rbp)
	cmpq	%rax, -3432(%rbp)
	jne	.L245
	movq	-3344(%rbp), %rsi
	movq	-3392(%rbp), %rdi
	call	BrotliFree
	movq	-3368(%rbp), %rax
	movq	%rax, %rbx
	shrq	%rbx
	imulq	%rax, %rbx
	salq	$6, %rax
	cmpq	%rax, %rbx
	cmova	%rax, %rbx
	leaq	1(%rbx), %r12
	cmpq	$2049, %r12
	jbe	.L246
	movq	-3392(%rbp), %r14
	movq	-3424(%rbp), %rsi
	movq	%r14, %rdi
	call	BrotliFree
	leaq	(%r12,%r12,2), %rsi
	movq	%r14, %rdi
	salq	$3, %rsi
	call	BrotliAllocate
	movq	%rax, -3424(%rbp)
.L246:
	cmpq	$0, -3368(%rbp)
	jne	.L327
	movq	-3400(%rbp), %r14
	pushq	%rax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-3360(%rbp), %rdx
	movq	-3336(%rbp), %rdi
	pushq	%rbx
	movq	-3424(%rbp), %rbx
	pushq	$256
	movq	%r14, %rsi
	pushq	-3320(%rbp)
	movq	%rbx, %r8
	call	BrotliHistogramCombineDistance
	movq	%rbx, %rsi
	movq	-3392(%rbp), %rbx
	addq	$32, %rsp
	movq	%rax, -3368(%rbp)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	$0, -3352(%rbp)
	movq	$0, -3328(%rbp)
.L274:
	movq	-3352(%rbp), %rax
	movq	-3368(%rbp), %rsi
	movl	$0, -3384(%rbp)
	leaq	-2256(%rbp), %r14
	movq	$0, -3296(%rbp)
	movq	-3376(%rbp), %rbx
	movq	$0, -3344(%rbp)
	leaq	(%rax,%rsi,4), %r12
	.p2align 4,,10
	.p2align 3
.L262:
	xorl	%eax, %eax
	movl	$272, %ecx
	movq	%r14, %rdi
	movq	$0, -80(%rbp)
	rep stosq
	movq	.LC1(%rip), %rax
	movl	(%rbx), %edx
	movq	%rax, -72(%rbp)
	testl	%edx, %edx
	je	.L251
	movq	-3312(%rbp), %rax
	movq	-3344(%rbp), %rsi
	leaq	(%rax,%rsi,2), %rdi
	xorl	%eax, %eax
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L252:
	movzwl	(%rdi,%rax,2), %edx
	leaq	(%rsi,%rax), %rcx
	addq	$1, %rax
	addl	$1, -2256(%rbp,%rdx,4)
	movl	(%rbx), %edx
	cmpq	%rax, %rdx
	ja	.L252
	movq	%rax, -80(%rbp)
	movq	%rcx, -3344(%rbp)
.L251:
	cmpq	$0, -3296(%rbp)
	movq	-3360(%rbp), %rax
	je	.L328
	movq	-3296(%rbp), %rsi
	movl	-4(%rax,%rsi,4), %r15d
.L254:
	movl	%r15d, %eax
	movq	%r14, %rdi
	imulq	$2192, %rax, %rsi
	addq	-3336(%rbp), %rsi
	movq	%rax, -3304(%rbp)
	call	BrotliHistogramBitCostDistanceDistance
	cmpq	$0, -3368(%rbp)
	movq	-3304(%rbp), %rax
	movsd	%xmm0, -3288(%rbp)
	je	.L255
	movq	-3352(%rbp), %r13
	movq	%rbx, -3304(%rbp)
	movq	%r13, %rbx
	movq	-3336(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L258:
	movl	(%rbx), %esi
	movq	%r14, %rdi
	imulq	$2192, %rsi, %rsi
	addq	%r13, %rsi
	call	BrotliHistogramBitCostDistanceDistance
	movsd	-3288(%rbp), %xmm5
	comisd	%xmm0, %xmm5
	jbe	.L256
	movl	(%rbx), %r15d
	movsd	%xmm0, -3288(%rbp)
.L256:
	addq	$4, %rbx
	cmpq	%r12, %rbx
	jne	.L258
	movq	-3304(%rbp), %rbx
	movl	%r15d, %eax
.L255:
	movq	-3360(%rbp), %rsi
	movq	-3296(%rbp), %rcx
	movl	%r15d, (%rsi,%rcx,4)
	movq	-3328(%rbp), %rsi
	leaq	(%rsi,%rax,4), %rax
	cmpl	$-1, (%rax)
	je	.L329
	addq	$1, -3296(%rbp)
	addq	$4, %rbx
	movq	-3296(%rbp), %rax
	cmpq	%rax, -3320(%rbp)
	jne	.L262
.L261:
	movq	-3392(%rbp), %rbx
	movq	-3352(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-3336(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-3440(%rbp), %rax
	movq	32(%rax), %r12
	movq	-3320(%rbp), %rax
	cmpq	%rax, %r12
	jnb	.L263
	testq	%r12, %r12
	je	.L287
.L265:
	addq	%r12, %r12
	cmpq	%r12, %rax
	ja	.L265
.L264:
	movq	-3392(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-3440(%rbp), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L330
.L266:
	movq	-3440(%rbp), %r14
	movq	-3392(%rbp), %rdi
	movq	16(%r14), %rsi
	call	BrotliFree
	movq	%rbx, 16(%r14)
	movq	%r12, 32(%r14)
.L263:
	movq	-3440(%rbp), %rax
	movq	40(%rax), %r12
	cmpq	-3320(%rbp), %r12
	jnb	.L278
	testq	%r12, %r12
	jne	.L331
	movq	-3320(%rbp), %r12
.L267:
	movq	-3448(%rbp), %rsi
	movq	-3392(%rbp), %rdi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-3440(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L332
.L269:
	movq	-3440(%rbp), %r14
	movq	-3392(%rbp), %rdi
	movq	24(%r14), %rsi
	call	BrotliFree
	cmpq	$0, -3320(%rbp)
	movq	%rbx, 24(%r14)
	movq	%r12, 40(%r14)
	je	.L289
.L278:
	movq	-3360(%rbp), %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	-3440(%rbp), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L273:
	movq	-3376(%rbp), %rbx
	addl	(%rbx,%rax,4), %ecx
	addq	$1, %rax
	movl	-4(%r8,%rax,4), %edx
	cmpq	-3320(%rbp), %rax
	je	.L271
	cmpl	%edx, (%r8,%rax,4)
	je	.L273
	movq	-3328(%rbp), %rbx
	movq	16(%r9), %r10
	movl	(%rbx,%rdx,4), %edx
	movb	%dl, (%r10,%rdi)
	movq	24(%r9), %r10
	cmpb	%dl, %sil
	cmovb	%edx, %esi
	movl	%ecx, (%r10,%rdi,4)
	addq	$1, %rdi
	xorl	%ecx, %ecx
	jmp	.L273
.L326:
	movq	-3416(%rbp), %rax
	testq	%rax, %rax
	jne	.L333
	movq	-3392(%rbp), %rdi
	leaq	0(,%r10,4), %rsi
	movq	%r10, -3288(%rbp)
	call	BrotliAllocate
	movq	-3288(%rbp), %r10
	movq	%rax, -3296(%rbp)
	movq	%r10, -3416(%rbp)
.L275:
	movq	-3400(%rbp), %rsi
	movq	-3392(%rbp), %rdi
	movq	%r10, -3288(%rbp)
	call	BrotliFree
	movq	-3296(%rbp), %rax
	movq	-3288(%rbp), %r10
	movq	%rax, -3400(%rbp)
	jmp	.L238
.L325:
	movq	-3408(%rbp), %rax
	testq	%rax, %rax
	je	.L235
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rcx, %rax
	addq	%rcx, %rcx
	cmpq	%rcx, %r10
	ja	.L236
	imulq	$4384, %rax, %rsi
	movq	-3392(%rbp), %rdi
	movq	%rcx, -3456(%rbp)
	movq	%r10, -3288(%rbp)
	call	BrotliAllocate
	movq	-3336(%rbp), %rsi
	imulq	$2192, -3408(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -3296(%rbp)
	call	memcpy@PLT
	movq	-3456(%rbp), %rcx
	movq	-3288(%rbp), %r10
	movq	%rcx, -3408(%rbp)
.L276:
	movq	-3336(%rbp), %rsi
	movq	-3392(%rbp), %rdi
	movq	%r10, -3288(%rbp)
	call	BrotliFree
	movq	-3296(%rbp), %rax
	movq	-3288(%rbp), %r10
	movq	%rax, -3336(%rbp)
	jmp	.L234
.L235:
	imulq	$2192, %r10, %rsi
	movq	-3392(%rbp), %rdi
	movq	%r10, -3288(%rbp)
	call	BrotliAllocate
	movq	-3288(%rbp), %r10
	movq	%rax, -3296(%rbp)
	movq	%r10, -3408(%rbp)
	jmp	.L276
.L333:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%rcx, %rax
	addq	%rcx, %rcx
	cmpq	%rcx, %r10
	ja	.L240
	movq	-3392(%rbp), %rdi
	leaq	0(,%rax,8), %rsi
	movq	%rcx, -3456(%rbp)
	movq	%r10, -3288(%rbp)
	call	BrotliAllocate
	movq	-3416(%rbp), %rdx
	movq	-3400(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -3296(%rbp)
	salq	$2, %rdx
	call	memcpy@PLT
	movq	-3456(%rbp), %rcx
	movq	-3288(%rbp), %r10
	movq	%rcx, -3416(%rbp)
	jmp	.L275
.L328:
	movl	(%rax), %r15d
	jmp	.L254
.L329:
	movl	-3384(%rbp), %esi
	addq	$1, -3296(%rbp)
	addq	$4, %rbx
	movl	%esi, (%rax)
	leal	1(%rsi), %edx
	movq	-3296(%rbp), %rax
	cmpq	%rax, -3320(%rbp)
	je	.L261
	movl	%edx, -3384(%rbp)
	jmp	.L262
.L327:
	movq	-3368(%rbp), %r14
	movq	-3392(%rbp), %rdi
	leaq	0(,%r14,4), %r12
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%r14, %rsi
	movq	%rax, -3352(%rbp)
	movq	%rax, %rcx
	leaq	-1(%r14), %rax
	cmpq	$2, %rax
	jbe	.L285
	shrq	$2, %rsi
	movdqa	.LC0(%rip), %xmm1
	movq	%rcx, %rax
	movdqa	.LC2(%rip), %xmm4
	movq	%rsi, %rdx
	movdqa	.LC3(%rip), %xmm3
	salq	$4, %rdx
	addq	%rcx, %rdx
.L249:
	movdqa	%xmm1, %xmm0
	addq	$16, %rax
	paddq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm3, %xmm2
	shufps	$136, %xmm2, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L249
	movq	-3368(%rbp), %rsi
	movq	%rsi, %rax
	andq	$-4, %rax
	andl	$3, %esi
	je	.L250
.L248:
	movq	-3352(%rbp), %rsi
	movq	-3368(%rbp), %rcx
	leaq	1(%rax), %rdx
	movl	%eax, (%rsi,%rax,4)
	cmpq	%rdx, %rcx
	jbe	.L250
	addq	$2, %rax
	movl	%edx, (%rsi,%rdx,4)
	cmpq	%rax, %rcx
	jbe	.L250
	movl	%eax, (%rsi,%rax,4)
.L250:
	subq	$8, %rsp
	movq	-3400(%rbp), %r14
	movq	-3360(%rbp), %rdx
	movq	-3368(%rbp), %r9
	movq	-3352(%rbp), %rcx
	pushq	%rbx
	movq	-3424(%rbp), %rbx
	pushq	$256
	movq	%r14, %rsi
	pushq	-3320(%rbp)
	movq	-3336(%rbp), %rdi
	movq	%rbx, %r8
	call	BrotliHistogramCombineDistance
	movq	%rbx, %rsi
	movq	-3392(%rbp), %rbx
	addq	$32, %rsp
	movq	%rax, -3368(%rbp)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliAllocate
	movq	%r12, %rdx
	movl	$255, %esi
	movq	%rax, %rdi
	movq	%rax, -3328(%rbp)
	call	memset@PLT
	jmp	.L274
.L271:
	movq	-3440(%rbp), %rbx
	movq	-3328(%rbp), %rax
	movl	(%rax,%rdx,4), %eax
	movq	16(%rbx), %rdx
	cmpb	%al, %sil
	movb	%al, (%rdx,%rdi)
	movq	24(%rbx), %rdx
	leaq	1(%rdi), %rbx
	cmovnb	%esi, %eax
	movq	%rbx, -3320(%rbp)
	movl	%ecx, (%rdx,%rdi,4)
	movzbl	%al, %eax
	addq	$1, %rax
.L270:
	movq	-3392(%rbp), %rbx
	movq	%rax, %xmm0
	movq	-3440(%rbp), %rax
	movhps	-3320(%rbp), %xmm0
	movq	-3328(%rbp), %rsi
	movups	%xmm0, (%rax)
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-3376(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-3360(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L331:
	.cfi_restore_state
	movq	-3320(%rbp), %rdx
.L268:
	movq	%r12, %rax
	addq	%r12, %r12
	cmpq	%r12, %rdx
	ja	.L268
	salq	$3, %rax
	movq	%rax, -3448(%rbp)
	jmp	.L267
.L287:
	movq	%rax, %r12
	jmp	.L264
.L323:
	movq	-3448(%rbp), %r15
	movq	%rdi, %r14
	movq	%r15, %rsi
	call	BrotliAllocate
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -3360(%rbp)
	call	BrotliAllocate
	movq	%rax, -3376(%rbp)
	cmpq	$63, %r12
	ja	.L226
	movq	$0, -3336(%rbp)
	movq	$0, -3400(%rbp)
.L279:
	movq	-3320(%rbp), %rax
	movl	$64, %esi
	movq	-3392(%rbp), %r14
	leaq	-3024(%rbp), %r12
	cmpq	$64, %rax
	movq	%r14, %rdi
	cmovbe	%rax, %rsi
	imulq	$2192, %rsi, %rsi
	call	BrotliAllocate
	movq	%r14, %rdi
	movl	$49176, %esi
	leaq	-2768(%rbp), %r14
	movq	%rax, -3344(%rbp)
	call	BrotliAllocate
	leaq	-3280(%rbp), %rdi
	movl	$32, %ecx
	xorl	%esi, %esi
	movq	%rax, -3424(%rbp)
	xorl	%eax, %eax
	movq	-3448(%rbp), %rdx
	movq	%rdi, -3304(%rbp)
	rep stosq
	movl	$32, %ecx
	movq	%r12, %rdi
	rep stosq
	movl	$32, %ecx
	movq	%r14, %rdi
	rep stosq
	leaq	-2512(%rbp), %rdi
	movl	$32, %ecx
	rep stosq
	movq	-3376(%rbp), %rdi
	call	memset@PLT
	testq	%rbx, %rbx
	jne	.L280
	jmp	.L227
.L330:
	movq	16(%rax), %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	jmp	.L266
.L332:
	leaq	0(,%rax,4), %rdx
	movq	-3440(%rbp), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	jmp	.L269
.L281:
	movq	-3392(%rbp), %rbx
	movq	-3344(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-3424(%rbp), %r12
	movq	-3400(%rbp), %r15
	pushq	$0
	movq	-3336(%rbp), %r14
	pushq	$256
	pushq	-3320(%rbp)
	movq	-3360(%rbp), %rdx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	BrotliHistogramCombineDistance
	addq	$32, %rsp
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movl	$1, %eax
	movq	$0, -3328(%rbp)
	movq	$0, -3320(%rbp)
	jmp	.L270
.L285:
	xorl	%eax, %eax
	jmp	.L248
.L334:
	call	__stack_chk_fail@PLT
.L226:
	movq	-3408(%rbp), %r14
	movq	-3392(%rbp), %r15
	imulq	$2192, %r14, %rsi
	movq	%r15, %rdi
	call	BrotliAllocate
	leaq	0(,%r14,4), %rsi
	movq	%r15, %rdi
	movq	%rax, -3336(%rbp)
	call	BrotliAllocate
	movq	%rax, -3400(%rbp)
	jmp	.L279
.L289:
	movl	$1, %eax
	jmp	.L270
	.cfi_endproc
.LFE119:
	.size	ClusterBlocksDistance, .-ClusterBlocksDistance
	.p2align 4
	.type	RefineEntropyCodesDistance.constprop.0, @function
RefineEntropyCodesDistance.constprop.0:
.LFB134:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	leaq	(%rsi,%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbx
	subq	$2216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	mulq	%rcx
	shrq	$5, %rdx
	leaq	99(%r9,%rdx), %rax
	xorl	%edx, %edx
	divq	%r9
	imulq	%r9, %rax
	testq	%rax, %rax
	je	.L335
	movq	%rsi, %r10
	movq	%rdi, %r14
	movq	%rax, %r11
	xorl	%r8d, %r8d
	movl	$7, %ebx
	leaq	-2256(%rbp), %rsi
	leaq	-39(%r10), %r13
	xorl	%r15d, %r15d
.L336:
	movl	$272, %ecx
	movq	%rsi, %rdi
	movq	%r15, %rax
	rep stosq
	cmpq	$40, %r10
	jbe	.L337
	imull	$16807, %ebx, %eax
	xorl	%edx, %edx
	movl	$40, %ecx
	movq	%rax, %rbx
	divq	%r13
	leaq	(%r14,%rdx,2), %rdi
.L338:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L340:
	movzwl	(%rdi,%rax,2), %edx
	addq	$1, %rax
	addl	$1, -2256(%rbp,%rdx,4)
	cmpq	%rcx, %rax
	jne	.L340
.L339:
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%r9
	xorl	%eax, %eax
	imulq	$2192, %rdx, %rdx
	addq	%r12, %rdx
	addq	%rcx, 2176(%rdx)
	.p2align 4,,10
	.p2align 3
.L341:
	movdqu	(%rdx,%rax), %xmm0
	paddd	(%rsi,%rax), %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$2176, %rax
	jne	.L341
	addq	$1, %r8
	cmpq	%r8, %r11
	jne	.L336
.L335:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L352
	addq	$2216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L337:
	.cfi_restore_state
	xorl	%ecx, %ecx
	testq	%r10, %r10
	je	.L339
	movq	%r10, %rcx
	movq	%r14, %rdi
	jmp	.L338
.L352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE134:
	.size	RefineEntropyCodesDistance.constprop.0, .-RefineEntropyCodesDistance.constprop.0
	.p2align 4
	.type	RefineEntropyCodesCommand.constprop.0, @function
RefineEntropyCodesCommand.constprop.0:
.LFB131:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	leaq	(%rsi,%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbx
	subq	$2856, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	mulq	%rcx
	shrq	$5, %rdx
	leaq	99(%r9,%rdx), %rax
	xorl	%edx, %edx
	divq	%r9
	imulq	%r9, %rax
	testq	%rax, %rax
	je	.L353
	movq	%rsi, %r10
	movq	%rdi, %r14
	movq	%rax, %r11
	xorl	%r8d, %r8d
	movl	$7, %ebx
	leaq	-2896(%rbp), %rsi
	leaq	-39(%r10), %r13
	xorl	%r15d, %r15d
.L354:
	movl	$352, %ecx
	movq	%rsi, %rdi
	movq	%r15, %rax
	rep stosq
	cmpq	$40, %r10
	jbe	.L355
	imull	$16807, %ebx, %eax
	xorl	%edx, %edx
	movl	$40, %ecx
	movq	%rax, %rbx
	divq	%r13
	leaq	(%r14,%rdx,2), %rdi
.L356:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L358:
	movzwl	(%rdi,%rax,2), %edx
	addq	$1, %rax
	addl	$1, -2896(%rbp,%rdx,4)
	cmpq	%rcx, %rax
	jne	.L358
.L357:
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%r9
	xorl	%eax, %eax
	imulq	$2832, %rdx, %rdx
	addq	%r12, %rdx
	addq	%rcx, 2816(%rdx)
	.p2align 4,,10
	.p2align 3
.L359:
	movdqu	(%rdx,%rax), %xmm0
	paddd	(%rsi,%rax), %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$2816, %rax
	jne	.L359
	addq	$1, %r8
	cmpq	%r8, %r11
	jne	.L354
.L353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L370
	addq	$2856, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L355:
	.cfi_restore_state
	xorl	%ecx, %ecx
	testq	%r10, %r10
	je	.L357
	movq	%r10, %rcx
	movq	%r14, %rdi
	jmp	.L356
.L370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE131:
	.size	RefineEntropyCodesCommand.constprop.0, .-RefineEntropyCodesCommand.constprop.0
	.p2align 4
	.type	RefineEntropyCodesLiteral.constprop.0, @function
RefineEntropyCodesLiteral.constprop.0:
.LFB128:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	leaq	(%rsi,%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movabsq	$-1581149492032247281, %rcx
	pushq	%rbx
	subq	$1064, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	mulq	%rcx
	shrq	$6, %rdx
	leaq	99(%r9,%rdx), %rax
	xorl	%edx, %edx
	divq	%r9
	imulq	%r9, %rax
	testq	%rax, %rax
	je	.L371
	movq	%rsi, %r10
	movq	%rdi, %r14
	movq	%rax, %r11
	xorl	%r8d, %r8d
	movl	$7, %ebx
	leaq	-1104(%rbp), %rsi
	leaq	-69(%r10), %r13
	xorl	%r15d, %r15d
.L372:
	movl	$128, %ecx
	movq	%rsi, %rdi
	movq	%r15, %rax
	rep stosq
	cmpq	$70, %r10
	jbe	.L373
	imull	$16807, %ebx, %eax
	xorl	%edx, %edx
	movl	$70, %ecx
	movq	%rax, %rbx
	divq	%r13
	leaq	(%r14,%rdx), %rax
.L374:
	leaq	(%rax,%rcx), %rdi
	.p2align 4,,10
	.p2align 3
.L376:
	movzbl	(%rax), %edx
	addq	$1, %rax
	addl	$1, -1104(%rbp,%rdx,4)
	cmpq	%rdi, %rax
	jne	.L376
.L375:
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	%rdx, %rax
	salq	$6, %rax
	addq	%rdx, %rax
	salq	$4, %rax
	leaq	(%r12,%rax), %rdx
	xorl	%eax, %eax
	addq	%rcx, 1024(%rdx)
	.p2align 4,,10
	.p2align 3
.L377:
	movdqu	(%rdx,%rax), %xmm0
	paddd	(%rsi,%rax), %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$1024, %rax
	jne	.L377
	addq	$1, %r8
	cmpq	%r8, %r11
	jne	.L372
.L371:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$1064, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L373:
	.cfi_restore_state
	xorl	%ecx, %ecx
	testq	%r10, %r10
	je	.L375
	movq	%r10, %rcx
	movq	%r14, %rax
	jmp	.L374
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE128:
	.size	RefineEntropyCodesLiteral.constprop.0, .-RefineEntropyCodesLiteral.constprop.0
	.p2align 4
	.globl	BrotliInitBlockSplit
	.hidden	BrotliInitBlockSplit
	.type	BrotliInitBlockSplit, @function
BrotliInitBlockSplit:
.LFB121:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE121:
	.size	BrotliInitBlockSplit, .-BrotliInitBlockSplit
	.p2align 4
	.globl	BrotliDestroyBlockSplit
	.hidden	BrotliDestroyBlockSplit
	.type	BrotliDestroyBlockSplit, @function
BrotliDestroyBlockSplit:
.LFB122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	16(%rsi), %rsi
	call	BrotliFree
	movq	$0, 16(%rbx)
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	BrotliFree
	movq	$0, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE122:
	.size	BrotliDestroyBlockSplit, .-BrotliDestroyBlockSplit
	.p2align 4
	.globl	BrotliSplitBlock
	.hidden	BrotliSplitBlock
	.type	BrotliSplitBlock, @function
BrotliSplitBlock:
.LFB123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	%rdi, -144(%rbp)
	movq	%rax, -248(%rbp)
	movq	24(%rbp), %rax
	movq	%rsi, -184(%rbp)
	movq	%rax, -224(%rbp)
	movq	32(%rbp), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, -232(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -240(%rbp)
	testq	%rdx, %rdx
	je	.L393
	leaq	-1(%rdx), %rdi
	movq	%r8, %rbx
	movq	%r9, %r14
	movq	%rdi, -168(%rbp)
	cmpq	$4, %rdi
	jbe	.L595
	shrq	$2, %rdi
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	movq	%rsi, %rax
	movq	%rdi, %rdx
	salq	$6, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L395:
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm5
	addq	$64, %rax
	movdqu	-32(%rax), %xmm1
	movdqu	-16(%rax), %xmm6
	shufps	$136, %xmm5, %xmm0
	shufps	$136, %xmm6, %xmm1
	shufps	$136, %xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	punpckldq	%xmm3, %xmm0
	punpckhdq	%xmm3, %xmm1
	paddq	%xmm1, %xmm0
	paddq	%xmm0, %xmm2
	cmpq	%rax, %rdx
	jne	.L395
	movdqa	%xmm2, %xmm0
	movq	-168(%rbp), %rdx
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm2
	andq	$-4, %rdx
	movq	%xmm2, %rax
.L394:
	movq	-184(%rbp), %rdi
	movq	%rdx, %rcx
	salq	$4, %rcx
	movl	(%rdi,%rcx), %ecx
	leaq	(%rcx,%rax), %rsi
	movq	-88(%rbp), %rcx
	leaq	1(%rdx), %rax
	movq	%rsi, -72(%rbp)
	cmpq	%rax, %rcx
	jbe	.L396
	salq	$4, %rax
	movl	(%rdi,%rax), %eax
	addq	%rax, %rsi
	leaq	2(%rdx), %rax
	movq	%rsi, -72(%rbp)
	cmpq	%rax, %rcx
	jbe	.L396
	salq	$4, %rax
	movl	(%rdi,%rax), %eax
	addq	%rax, %rsi
	leaq	3(%rdx), %rax
	movq	%rsi, -72(%rbp)
	cmpq	%rax, %rcx
	jbe	.L396
	salq	$4, %rax
	addq	$4, %rdx
	movl	(%rdi,%rax), %eax
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L396
	salq	$4, %rdx
	movl	(%rdi,%rdx), %eax
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
.L396:
	cmpq	$0, -72(%rbp)
	movq	$0, -120(%rbp)
	je	.L397
	movq	-72(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	BrotliAllocate
	movq	%rax, -120(%rbp)
.L397:
	movq	-88(%rbp), %r12
	movq	-184(%rbp), %rax
	andq	%r14, %rbx
	xorl	%r13d, %r13d
	salq	$4, %r12
	movq	%rax, %r15
	addq	%rax, %r12
	leaq	1(%r14), %rax
	movq	%r12, -56(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L398:
	testq	%rcx, %rcx
	jne	.L757
.L399:
	movl	4(%r15), %ebx
	addq	$16, %r15
	andl	$33554431, %ebx
	addq	%r12, %rbx
	andq	%r14, %rbx
	cmpq	%r15, -56(%rbp)
	je	.L758
.L589:
	movl	(%r15), %ecx
	leaq	(%rcx,%rbx), %r12
	cmpq	%r12, %r14
	jnb	.L398
	movq	-120(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rcx, -96(%rbp)
	leaq	(%rax,%r13), %rdi
	movq	-64(%rbp), %rax
	subq	%rbx, %r12
	movq	%r12, %rdx
	addq	%r12, %r13
	leaq	(%rax,%rbx), %rsi
	call	memcpy@PLT
	movq	-96(%rbp), %rcx
	subq	-80(%rbp), %rbx
	addq	%rbx, %rcx
	xorl	%ebx, %ebx
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L399
.L757:
	movq	-120(%rbp), %rax
	movq	%rcx, %rdx
	movq	%rcx, -96(%rbp)
	leaq	(%rax,%r13), %rdi
	movq	-64(%rbp), %rax
	leaq	(%rax,%rbx), %rsi
	call	memcpy@PLT
	movq	-96(%rbp), %rcx
	addq	%rcx, %r13
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L758:
	movabsq	$-1085102592571150095, %rdx
	movq	-72(%rbp), %rax
	mulq	%rdx
	movl	$100, %eax
	shrq	$9, %rdx
	leaq	1(%rdx), %r15
	cmpq	$100, %r15
	cmova	%rax, %r15
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L402
	cmpq	$127, %rax
	jbe	.L759
	movq	%r15, %r9
	movq	-144(%rbp), %rdi
	salq	$6, %r9
	addq	%r15, %r9
	movq	%r9, %rbx
	salq	$4, %rbx
	movq	%rbx, %rsi
	call	BrotliAllocate
	xorl	%edx, %edx
	movsd	.LC1(%rip), %xmm6
	movq	%rax, %rsi
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	leaq	(%rbx,%rsi), %r9
	movsd	%xmm6, -56(%rbp)
	divq	%r15
	movq	%rsi, %rdx
	movq	%rax, %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L413:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$1040, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	-56(%rbp), %xmm5
	movsd	%xmm5, -8(%rdx)
	cmpq	%rdx, %r9
	jne	.L413
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	$7, %edi
	xorl	%edx, %edx
	movq	-120(%rbp), %r11
	movq	%r8, %rsi
	leaq	-71(%r8), %rbx
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	70(%rdx), %rax
	cmpq	%r8, %rax
	cmovnb	%rbx, %rdx
	addq	$70, 1024(%rcx)
	addq	%r11, %rdx
	leaq	70(%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L416:
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	addl	$1, (%rcx,%rax,4)
	cmpq	%r12, %rdx
	jne	.L416
	addq	$1040, %rcx
	cmpq	%rcx, %r9
	je	.L760
	movq	%rsi, %rax
	xorl	%edx, %edx
	addq	%r8, %rsi
	divq	%r15
	xorl	%edx, %edx
	movq	%rax, %r12
	imull	$16807, %edi, %eax
	movq	%rax, %rdi
	divq	%r10
	addq	%r12, %rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L759:
	movq	-224(%rbp), %rdi
	movq	8(%rdi), %rdx
	movq	32(%rdi), %rax
	leaq	1(%rdx), %r12
	cmpq	%r12, %rax
	jnb	.L404
	testq	%rax, %rax
	jne	.L406
.L405:
	movq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-224(%rbp), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L761
.L407:
	movq	-224(%rbp), %r14
	movq	-144(%rbp), %rdi
	movq	16(%r14), %rsi
	call	BrotliFree
	movq	8(%r14), %rdx
	movq	%rbx, 16(%r14)
	movq	%r12, 32(%r14)
	leaq	1(%rdx), %r12
.L404:
	movq	-224(%rbp), %rax
	movq	40(%rax), %rax
	cmpq	%r12, %rax
	jb	.L762
.L408:
	movq	-224(%rbp), %rsi
	movl	-72(%rbp), %edi
	movq	16(%rsi), %rax
	movq	$1, (%rsi)
	movb	$0, (%rax,%rdx)
	movq	8(%rsi), %rax
	movq	24(%rsi), %rdx
	movl	%edi, (%rdx,%rax,4)
	addq	$1, %rax
	movq	%rax, 8(%rsi)
.L749:
	movq	-120(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	BrotliFree
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %r14
	movq	%r14, %rsi
	movq	%r14, -216(%rbp)
	call	BrotliAllocate
	movq	-168(%rbp), %rbx
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	leaq	12(%rsi), %rax
	salq	$4, %rdx
	leaq	14(%rsi,%rdx), %rdx
	cmpq	%rdx, %rdi
	setnb	%sil
	addq	%r14, %rcx
	cmpq	%rcx, %rax
	setnb	%dl
	orb	%dl, %sil
	je	.L456
	cmpq	$7, %rbx
	jbe	.L456
	shrq	$3, %rbx
	movq	%rdi, %rdx
	movq	%rbx, %rcx
	salq	$4, %rcx
	addq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L457:
	movdqu	16(%rax), %xmm7
	movdqu	(%rax), %xmm0
	addq	$16, %rdx
	subq	$-128, %rax
	movdqu	-80(%rax), %xmm5
	movdqu	-16(%rax), %xmm6
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm7, %xmm0
	punpckhwd	%xmm7, %xmm1
	movdqa	%xmm0, %xmm2
	movdqu	-48(%rax), %xmm7
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	movdqu	-96(%rax), %xmm1
	punpcklwd	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm5, %xmm1
	punpckhwd	%xmm5, %xmm2
	movdqa	%xmm1, %xmm3
	punpckhwd	%xmm2, %xmm3
	punpcklwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movdqu	-64(%rax), %xmm1
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm7, %xmm1
	punpckhwd	%xmm7, %xmm2
	movdqa	%xmm1, %xmm3
	punpckhwd	%xmm2, %xmm3
	punpcklwd	%xmm2, %xmm1
	movdqu	-32(%rax), %xmm2
	punpcklwd	%xmm3, %xmm1
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm6, %xmm2
	punpckhwd	%xmm6, %xmm3
	movdqa	%xmm2, %xmm4
	punpckhwd	%xmm3, %xmm4
	punpcklwd	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm4, %xmm2
	punpcklwd	%xmm2, %xmm1
	punpckhwd	%xmm2, %xmm3
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rcx
	jne	.L457
	movq	-168(%rbp), %rax
	movq	-184(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	-88(%rbp), %rbx
	andq	$-8, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	movzwl	12(%rsi,%rdx), %edx
	movw	%dx, (%rdi,%rax,2)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L458
	movq	%rdx, %rcx
	salq	$4, %rcx
	movzwl	12(%rsi,%rcx), %ecx
	movw	%cx, (%rdi,%rdx,2)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L458
	movq	%rdx, %rcx
	salq	$4, %rcx
	movzwl	12(%rsi,%rcx), %ecx
	movw	%cx, (%rdi,%rdx,2)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L458
	movq	%rdx, %rcx
	salq	$4, %rcx
	movzwl	12(%rsi,%rcx), %ecx
	movw	%cx, (%rdi,%rdx,2)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L458
	movq	%rdx, %rcx
	salq	$4, %rcx
	movzwl	12(%rsi,%rcx), %ecx
	movw	%cx, (%rdi,%rdx,2)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L458
	movq	%rdx, %rcx
	salq	$4, %rcx
	movzwl	12(%rsi,%rcx), %ecx
	movw	%cx, (%rdi,%rdx,2)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L458
	movq	%rdx, %rcx
	addq	$7, %rax
	salq	$4, %rcx
	movzwl	12(%rsi,%rcx), %ecx
	movw	%cx, (%rdi,%rdx,2)
	cmpq	%rax, %rbx
	jbe	.L458
	movq	%rax, %rdx
	salq	$4, %rdx
	movzwl	12(%rsi,%rdx), %edx
	movw	%dx, (%rdi,%rax,2)
.L458:
	cmpq	$127, -88(%rbp)
	jbe	.L763
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	movabsq	$-626493194956173451, %rdx
	mulq	%rdx
	movl	$50, %eax
	movq	%rax, %r13
	shrq	$9, %rdx
	leaq	1(%rdx), %r9
	cmpq	$50, %r9
	cmovbe	%r9, %r13
	imulq	$2832, %r13, %rbx
	movq	%rbx, %rsi
	call	BrotliAllocate
	xorl	%edx, %edx
	movsd	.LC1(%rip), %xmm7
	movq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	leaq	(%rbx,%rsi), %r8
	movsd	%xmm7, -56(%rbp)
	divq	%r13
	movq	%rsi, %rdx
	movq	%rax, %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$2832, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	-56(%rbp), %xmm6
	movsd	%xmm6, -8(%rdx)
	cmpq	%rdx, %r8
	jne	.L469
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rcx
	movl	$7, %edi
	xorl	%edx, %edx
	movq	-120(%rbp), %r11
	movq	%r9, %rsi
	leaq	-41(%r9), %rbx
	.p2align 4,,10
	.p2align 3
.L470:
	leaq	40(%rdx), %rax
	cmpq	%rax, %r9
	cmovbe	%rbx, %rdx
	addq	$40, 2816(%rcx)
	leaq	(%r11,%rdx,2), %rax
	leaq	80(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L472:
	movzwl	(%rax), %edx
	addq	$2, %rax
	addl	$1, (%rcx,%rdx,4)
	cmpq	%r12, %rax
	jne	.L472
	addq	$2832, %rcx
	cmpq	%rcx, %r8
	je	.L764
	movq	%rsi, %rax
	xorl	%edx, %edx
	addq	%r9, %rsi
	divq	%r13
	xorl	%edx, %edx
	movq	%rax, %r12
	imull	$16807, %edi, %eax
	movq	%rax, %rdi
	divq	%r10
	addq	%r12, %rdx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L760:
	movq	-72(%rbp), %rbx
	movq	-64(%rbp), %rcx
	movq	%r15, %rdx
	movq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	call	RefineEntropyCodesLiteral.constprop.0
	movq	-144(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	BrotliAllocate
	movq	%r15, %rsi
	movq	%r14, %rdi
	salq	$11, %rsi
	movq	%rax, %r12
	call	BrotliAllocate
	leaq	0(,%r15,8), %rsi
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	call	BrotliAllocate
	leaq	7(%r15), %rsi
	movq	$0, -176(%rbp)
	shrq	$3, %rsi
	movq	%rax, %r13
	imulq	%rbx, %rsi
	testq	%rsi, %rsi
	jne	.L765
.L418:
	movq	-144(%rbp), %rdi
	leaq	(%r15,%r15), %rsi
	call	BrotliAllocate
	movl	$3, %edx
	movq	$0, -152(%rbp)
	movq	%rax, -104(%rbp)
	movq	-248(%rbp), %rax
	cmpl	$11, 4(%rax)
	movl	$10, %eax
	cmovl	%rdx, %rax
	movq	%rax, -216(%rbp)
	movq	-72(%rbp), %rax
	addq	%r12, %rax
	movq	%rax, -160(%rbp)
	movq	%r15, %rax
	movq	%r13, %r15
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L455:
	cmpq	$1, %r13
	jbe	.L766
	movq	-112(%rbp), %rbx
	movq	%r13, %rax
	xorl	%esi, %esi
	leaq	0(,%r13,8), %r14
	salq	$11, %rax
	movq	%rax, %rdx
	movq	%rbx, %rdi
	movq	%rax, -96(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %rax
	movq	%r14, -128(%rbp)
	leaq	1024(%rax), %rdx
	leaq	(%r14,%rbx), %rax
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%rax, -80(%rbp)
	movq	%rdx, %r12
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L768:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L754:
	movsd	%xmm0, (%rbx)
	addq	$1040, %r12
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L767
.L426:
	movq	(%r12), %rax
	movl	%eax, %edx
	testl	$4294967040, %eax
	je	.L768
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	call	log2@PLT
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L767:
	movq	%r13, %r12
	movq	%r14, %r13
	movq	-128(%rbp), %r14
	movq	-96(%rbp), %rsi
	movq	%r13, %rax
	movq	%r13, %r10
	addq	-112(%rbp), %rsi
	movq	%r12, -192(%rbp)
	negq	%rax
	movq	%r15, -200(%rbp)
	movq	%rsi, %r12
	salq	$3, %rax
	movq	%r13, -208(%rbp)
	movq	%rax, -136(%rbp)
	movq	-64(%rbp), %rax
	movq	%r14, -128(%rbp)
	leaq	1020(%rax), %rcx
	movq	%r13, %rax
	salq	$8, %rax
	movq	%rcx, %r15
	subq	%rax, %r10
	leaq	0(,%r10,8), %rbx
	movq	%rbx, %r13
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L770:
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L427:
	subsd	%xmm0, %xmm1
	addq	$1040, %r14
	addq	$8, %rbx
	movsd	%xmm1, -8(%rbx)
	cmpq	%rbx, -80(%rbp)
	jne	.L431
	leaq	-4(%r15), %rax
	addq	-128(%rbp), %r13
	cmpq	%r15, -64(%rbp)
	je	.L769
	movq	%rax, %r15
.L432:
	movq	%r12, -80(%rbp)
	addq	-136(%rbp), %r12
	movq	%r15, %r14
	movq	%r12, %rbx
.L431:
	movq	.LC4(%rip), %rdi
	movl	(%r14), %eax
	movsd	(%rbx,%r13), %xmm1
	movq	%rdi, %xmm0
	testq	%rax, %rax
	je	.L427
	cmpq	$255, %rax
	jbe	.L770
	pxor	%xmm0, %xmm0
	movsd	%xmm1, -96(%rbp)
	cvtsi2sdq	%rax, %xmm0
	call	log2@PLT
	movsd	-96(%rbp), %xmm1
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L769:
	movq	-200(%rbp), %r15
	movq	-128(%rbp), %r14
	xorl	%esi, %esi
	movq	-208(%rbp), %r13
	movq	-192(%rbp), %r12
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	memset@PLT
	leaq	7(%r13), %rbx
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	shrq	$3, %rbx
	movq	-176(%rbp), %rdi
	imulq	%rbx, %rdx
	call	memset@PLT
	movsd	.LC5(%rip), %xmm4
	xorl	%esi, %esi
	movsd	.LC6(%rip), %xmm3
	movsd	.LC7(%rip), %xmm7
	movq	-120(%rbp), %r10
	xorl	%edx, %edx
	movl	$1, %edi
	movsd	.LC8(%rip), %xmm6
	movsd	.LC9(%rip), %xmm5
	movq	-176(%rbp), %r9
	movq	-72(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L440:
	movzbl	(%r10,%rdx), %ecx
	movapd	%xmm4, %xmm2
	xorl	%eax, %eax
	imulq	%r14, %rcx
	addq	-112(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L435:
	movsd	(%r15,%rax,8), %xmm0
	addsd	(%rcx,%rax,8), %xmm0
	comisd	%xmm0, %xmm2
	movsd	%xmm0, (%r15,%rax,8)
	jbe	.L433
	movb	%al, (%r12,%rdx)
	movapd	%xmm0, %xmm2
.L433:
	addq	$1, %rax
	cmpq	%r13, %rax
	jne	.L435
	movapd	%xmm3, %xmm0
	cmpq	$1999, %rdx
	ja	.L436
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	mulsd	%xmm7, %xmm0
	divsd	%xmm6, %xmm0
	addsd	%xmm5, %xmm0
	mulsd	%xmm3, %xmm0
.L436:
	movq	%rdx, -80(%rbp)
	xorl	%eax, %eax
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L772:
	movsd	%xmm1, (%r15,%rax,8)
	addq	$1, %rax
	cmpq	%r13, %rax
	je	.L771
.L439:
	movsd	(%r15,%rax,8), %xmm1
	subsd	%xmm2, %xmm1
	comisd	%xmm0, %xmm1
	jb	.L772
	movq	%rax, %r8
	movl	%eax, %ecx
	movsd	%xmm0, (%r15,%rax,8)
	movl	%edi, %edx
	shrq	$3, %r8
	andl	$7, %ecx
	addq	$1, %rax
	addq	%rsi, %r8
	sall	%cl, %edx
	orb	%dl, (%r9,%r8)
	cmpq	%r13, %rax
	jne	.L439
.L771:
	movq	-80(%rbp), %rdx
	addq	%rbx, %rsi
	addq	$1, %rdx
	cmpq	%r11, %rdx
	jne	.L440
	movq	-72(%rbp), %rax
	movq	-176(%rbp), %r8
	movl	$1, %r9d
	movl	$1, %r11d
	leaq	-1(%rax), %rdi
	movzbl	-1(%r12,%rax), %edx
	leaq	-2(%r12,%rax), %rax
	imulq	%rbx, %rdi
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L603:
	movq	%rcx, %rax
.L441:
	movl	%edx, %esi
	subq	%rbx, %rdi
	movl	%edx, %ecx
	movl	%r11d, %r14d
	shrb	$3, %sil
	andl	$7, %ecx
	leaq	(%r8,%rdi), %r10
	movzbl	%sil, %esi
	sall	%cl, %r14d
	testb	%r14b, (%r10,%rsi)
	je	.L444
	movzbl	(%rax), %ecx
	cmpb	%dl, %cl
	je	.L444
	addq	$1, %r9
	movl	%ecx, %edx
.L444:
	movb	%dl, (%rax)
	leaq	-1(%rax), %rcx
	cmpq	%rax, %r12
	jne	.L603
	testq	%r13, %r13
	movl	$1, %ecx
	cmovne	%r13, %rcx
	cmpq	$7, %r13
	jbe	.L773
	movq	%rcx, %rdx
	movq	-104(%rbp), %rdi
	shrq	$3, %rdx
	salq	$4, %rdx
	movq	%rdi, %rax
	addq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L446:
	movdqa	.LC10(%rip), %xmm5
	addq	$16, %rax
	movups	%xmm5, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L446
	movq	%rcx, %rax
	andq	$-8, %rax
	andl	$7, %ecx
	je	.L422
	movq	-104(%rbp), %rsi
	movl	$256, %edi
	leaq	(%rax,%rax), %rdx
	movw	%di, (%rsi,%rax,2)
	addq	$1, %rax
	cmpq	%r13, %rax
	jnb	.L422
.L593:
	movq	-104(%rbp), %rdi
	movl	$256, %ecx
	movw	%cx, 2(%rdi,%rdx)
	leaq	1(%rax), %rcx
	cmpq	%rcx, %r13
	jbe	.L422
	movl	$256, %r14d
	leaq	2(%rax), %rcx
	movw	%r14w, 4(%rdi,%rdx)
	cmpq	%r13, %rcx
	jnb	.L422
	movl	$256, %ebx
	leaq	3(%rax), %rcx
	movw	%bx, 6(%rdi,%rdx)
	cmpq	%r13, %rcx
	jnb	.L422
	movl	$256, %r11d
	leaq	4(%rax), %rcx
	movw	%r11w, 8(%rdi,%rdx)
	cmpq	%r13, %rcx
	jnb	.L422
	movl	$256, %r10d
	addq	$5, %rax
	movw	%r10w, 10(%rdi,%rdx)
	cmpq	%r13, %rax
	jnb	.L422
	movl	$256, %r8d
	movw	%r8w, 12(%rdi,%rdx)
.L422:
	movq	-104(%rbp), %rsi
	movq	%r12, %rax
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	movq	-160(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L449:
	movzbl	(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	cmpw	$256, (%rcx)
	jne	.L448
	movw	%r8w, (%rcx)
	addl	$1, %r8d
.L448:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L449
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L450:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movzwl	(%rcx,%rdx,2), %edx
	movb	%dl, -1(%rax)
	cmpq	%rax, %rsi
	jne	.L450
	movzwl	%r8w, %r13d
	movq	%rcx, -104(%rbp)
	movq	%rsi, -160(%rbp)
	testq	%r13, %r13
	je	.L454
	movq	%r13, %rsi
	movq	-64(%rbp), %rdx
	xorl	%eax, %eax
	salq	$6, %rsi
	addq	%r13, %rsi
	salq	$4, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L453:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$1040, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	-56(%rbp), %xmm6
	movsd	%xmm6, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L453
.L454:
	movq	-120(%rbp), %rcx
	movq	-64(%rbp), %rsi
	xorl	%r14d, %r14d
	movq	-72(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L452:
	movzbl	(%r12,%r14), %edx
	movq	%rdx, %rax
	salq	$6, %rax
	addq	%rdx, %rax
	movzbl	(%rcx,%r14), %edx
	addq	$1, %r14
	salq	$4, %rax
	addq	%rsi, %rax
	addl	$1, (%rax,%rdx,4)
	addq	$1, 1024(%rax)
	cmpq	%rdi, %r14
	jne	.L452
	addq	$1, -152(%rbp)
	movq	-152(%rbp), %rax
	cmpq	-216(%rbp), %rax
	jne	.L455
	movq	-144(%rbp), %rbx
	movq	-112(%rbp), %rsi
	movq	%r15, %r13
	movq	%r9, %r15
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-176(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-104(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r12, %r8
	movq	-224(%rbp), %r9
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	ClusterBlocksLiteral
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L766:
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	memset@PLT
	testq	%r13, %r13
	je	.L748
	movq	-104(%rbp), %rax
	movl	$256, %edx
	movw	%dx, (%rax)
.L748:
	movl	$1, %r9d
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L764:
	movq	-88(%rbp), %r14
	movq	-80(%rbp), %rcx
	movq	%r13, %rdx
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	call	RefineEntropyCodesCommand.constprop.0
	movq	-144(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliAllocate
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	leaq	0(%r13,%r13,4), %rax
	leaq	0(%r13,%rax,2), %rsi
	salq	$9, %rsi
	call	BrotliAllocate
	leaq	0(,%r13,8), %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	BrotliAllocate
	leaq	7(%r13), %rsi
	movq	%rbx, %rdi
	shrq	$3, %rsi
	movq	%rax, %r15
	imulq	%r14, %rsi
	call	BrotliAllocate
	leaq	(%r13,%r13), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	BrotliAllocate
	movl	$3, %edx
	movq	-64(%rbp), %r10
	movq	%r15, %r8
	movq	%rax, -112(%rbp)
	movq	-248(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	%r10, %r15
	cmpl	$11, 4(%rax)
	movl	$10, %eax
	cmovl	%rdx, %rax
	movq	%rax, -208(%rbp)
	leaq	(%r14,%r10), %rax
	movq	%r12, %r14
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L520:
	cmpq	$1, %r13
	jbe	.L774
	leaq	0(%r13,%r13,4), %rax
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	leaq	0(%r13,%rax,2), %rbx
	salq	$9, %rbx
	movq	%rbx, -160(%rbp)
	movq	%rbx, %rdx
	movq	-72(%rbp), %rbx
	movq	%rbx, %rdi
	call	memset@PLT
	movq	-80(%rbp), %rax
	movq	-104(%rbp), %r8
	leaq	2816(%rax), %r12
	leaq	0(,%r13,8), %rax
	movq	%rax, -64(%rbp)
	addq	%rbx, %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L481:
	movq	(%r12), %rax
	movl	%eax, %edx
	testl	$4294967040, %eax
	jne	.L478
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	addq	$2832, %r12
	addq	$8, %rbx
	cvtss2sd	(%rax,%rdx,4), %xmm0
	movsd	%xmm0, -8(%rbx)
	cmpq	%rbx, -96(%rbp)
	jne	.L481
.L480:
	movq	%r13, %r12
	movq	-160(%rbp), %rcx
	addq	-72(%rbp), %rcx
	movq	%r14, -176(%rbp)
	negq	%r12
	movq	%r13, -192(%rbp)
	leaq	0(,%r12,8), %rax
	movq	%r15, -160(%rbp)
	imulq	$-5624, %r13, %r12
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rax
	movq	%r8, -200(%rbp)
	leaq	2812(%rax), %rbx
	movq	%r12, %r13
	movq	%rbx, %r14
	movq	%rcx, %r12
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L776:
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L482:
	subsd	%xmm0, %xmm1
	addq	$2832, %r15
	addq	$8, %rbx
	movsd	%xmm1, -8(%rbx)
	cmpq	%rbx, -96(%rbp)
	jne	.L486
	leaq	-4(%r14), %rax
	addq	-64(%rbp), %r13
	cmpq	%r14, -80(%rbp)
	je	.L775
	movq	%rax, %r14
.L487:
	movq	%r12, -96(%rbp)
	addq	-128(%rbp), %r12
	movq	%r14, %r15
	movq	%r12, %rbx
.L486:
	movq	.LC4(%rip), %rdi
	movl	(%r15), %eax
	movsd	(%rbx,%r13), %xmm1
	movq	%rdi, %xmm0
	testq	%rax, %rax
	je	.L482
	cmpq	$255, %rax
	jbe	.L776
	pxor	%xmm0, %xmm0
	movsd	%xmm1, -104(%rbp)
	cvtsi2sdq	%rax, %xmm0
	call	log2@PLT
	movsd	-104(%rbp), %xmm1
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L478:
	pxor	%xmm0, %xmm0
	movq	%r8, -104(%rbp)
	addq	$2832, %r12
	addq	$8, %rbx
	cvtsi2sdq	%rdx, %xmm0
	call	log2@PLT
	movq	-104(%rbp), %r8
	movsd	%xmm0, -8(%rbx)
	cmpq	%rbx, -96(%rbp)
	jne	.L481
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L775:
	movq	-200(%rbp), %r8
	movq	-64(%rbp), %rdx
	xorl	%esi, %esi
	movl	$1999, %r12d
	movq	-192(%rbp), %r13
	movq	-176(%rbp), %r14
	movq	%r8, %rdi
	movq	%r8, -96(%rbp)
	movq	-160(%rbp), %r15
	call	memset@PLT
	leaq	7(%r13), %rbx
	movq	-88(%rbp), %rdx
	xorl	%esi, %esi
	shrq	$3, %rbx
	movq	%r14, %rdi
	imulq	%rbx, %rdx
	call	memset@PLT
	movq	-168(%rbp), %rax
	movsd	.LC5(%rip), %xmm3
	movl	$1, %r9d
	movsd	.LC7(%rip), %xmm7
	movsd	.LC8(%rip), %xmm6
	cmpq	$1999, %rax
	movsd	.LC9(%rip), %xmm5
	movsd	.LC11(%rip), %xmm2
	cmovbe	%rax, %r12
	movq	-120(%rbp), %r10
	movq	-96(%rbp), %r8
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	leaq	1(%r12), %rsi
	.p2align 4,,10
	.p2align 3
.L494:
	movzwl	(%r10,%rdx,2), %ecx
	movapd	%xmm3, %xmm4
	imulq	-64(%rbp), %rcx
	xorl	%eax, %eax
	addq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L490:
	movsd	(%r8,%rax,8), %xmm0
	addsd	(%rcx,%rax,8), %xmm0
	comisd	%xmm0, %xmm4
	movsd	%xmm0, (%r8,%rax,8)
	jbe	.L488
	movb	%al, (%r15,%rdx)
	movapd	%xmm0, %xmm4
.L488:
	addq	$1, %rax
	cmpq	%r13, %rax
	jne	.L490
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	xorl	%eax, %eax
	cvtsi2sdq	%rdx, %xmm0
	mulsd	%xmm7, %xmm0
	divsd	%xmm6, %xmm0
	addsd	%xmm5, %xmm0
	mulsd	%xmm2, %xmm0
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L778:
	movsd	%xmm1, (%r8,%rax,8)
	addq	$1, %rax
	cmpq	%r13, %rax
	je	.L777
.L493:
	movsd	(%r8,%rax,8), %xmm1
	subsd	%xmm4, %xmm1
	comisd	%xmm0, %xmm1
	jb	.L778
	movq	%rax, %rdi
	movl	%eax, %ecx
	movsd	%xmm0, (%r8,%rax,8)
	movl	%r9d, %edx
	shrq	$3, %rdi
	andl	$7, %ecx
	addq	$1, %rax
	addq	%r11, %rdi
	sall	%cl, %edx
	orb	%dl, (%r14,%rdi)
	cmpq	%r13, %rax
	jne	.L493
.L777:
	movq	-96(%rbp), %rdx
	addq	%rbx, %r11
	leaq	1(%rdx), %rax
	cmpq	%r12, %rdx
	je	.L779
	movq	%rax, %rdx
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L779:
	cmpq	%rsi, -88(%rbp)
	jbe	.L504
	movq	%rbx, %rdi
	movq	-120(%rbp), %r9
	movq	-64(%rbp), %r10
	movl	$1, %r11d
	imulq	%rsi, %rdi
	movq	-88(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L503:
	movzwl	(%r9,%rsi,2), %edx
	movapd	%xmm3, %xmm1
	xorl	%eax, %eax
	imulq	%r10, %rdx
	addq	-72(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L498:
	movsd	(%rdx,%rax,8), %xmm0
	addsd	(%r8,%rax,8), %xmm0
	comisd	%xmm0, %xmm1
	movsd	%xmm0, (%r8,%rax,8)
	jbe	.L496
	movb	%al, (%r15,%rsi)
	movapd	%xmm0, %xmm1
.L496:
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L498
	xorl	%eax, %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%rax, %rdx
	movl	%eax, %ecx
	movsd	%xmm2, (%r8,%rax,8)
	addq	$1, %rax
	shrq	$3, %rdx
	andl	$7, %ecx
	addq	%rdi, %rdx
	movq	%rdx, -64(%rbp)
	movl	%r11d, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	-64(%rbp), %rdx
	orb	%cl, (%r14,%rdx)
	cmpq	%rax, %r13
	jbe	.L780
.L502:
	movsd	(%r8,%rax,8), %xmm0
	subsd	%xmm1, %xmm0
	comisd	%xmm2, %xmm0
	jnb	.L781
	movsd	%xmm0, (%r8,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r13
	ja	.L502
.L780:
	addq	$1, %rsi
	addq	%rbx, %rdi
	cmpq	%rsi, %r12
	jne	.L503
.L504:
	movq	-168(%rbp), %rax
	movl	$1, %r9d
	movl	$1, %r12d
	movq	%rax, %rdi
	movzbl	(%r15,%rax), %edx
	movq	-88(%rbp), %rax
	imulq	%rbx, %rdi
	leaq	-2(%r15,%rax), %rax
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%rcx, %rax
.L506:
	movl	%edx, %esi
	subq	%rbx, %rdi
	movl	%edx, %ecx
	movl	%r12d, %r11d
	shrb	$3, %sil
	andl	$7, %ecx
	leaq	(%r14,%rdi), %r10
	movzbl	%sil, %esi
	sall	%cl, %r11d
	testb	%r11b, (%r10,%rsi)
	je	.L509
	movzbl	(%rax), %ecx
	cmpb	%dl, %cl
	je	.L509
	addq	$1, %r9
	movl	%ecx, %edx
.L509:
	movb	%dl, (%rax)
	leaq	-1(%rax), %rcx
	cmpq	%rax, %r15
	jne	.L609
	testq	%r13, %r13
	movl	$1, %ecx
	cmovne	%r13, %rcx
	cmpq	$7, %r13
	jbe	.L782
	movq	%rcx, %rdx
	movq	-112(%rbp), %rbx
	shrq	$3, %rdx
	salq	$4, %rdx
	movq	%rbx, %rax
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L511:
	movdqa	.LC10(%rip), %xmm7
	addq	$16, %rax
	movups	%xmm7, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L511
	movq	%rcx, %rax
	andq	$-8, %rax
	cmpq	%rax, %rcx
	je	.L477
	movq	-112(%rbp), %rsi
	movl	$256, %edi
	leaq	(%rax,%rax), %rdx
	movw	%di, (%rsi,%rax,2)
	addq	$1, %rax
	cmpq	%r13, %rax
	jnb	.L477
.L591:
	movq	-112(%rbp), %rdi
	movl	$256, %esi
	leaq	1(%rax), %rcx
	movw	%si, 2(%rdi,%rdx)
	cmpq	%r13, %rcx
	jnb	.L477
	movl	$256, %ecx
	movw	%cx, 4(%rdi,%rdx)
	leaq	2(%rax), %rcx
	cmpq	%r13, %rcx
	jnb	.L477
	movl	$256, %r12d
	leaq	3(%rax), %rcx
	movw	%r12w, 6(%rdi,%rdx)
	cmpq	%r13, %rcx
	jnb	.L477
	movl	$256, %ebx
	leaq	4(%rax), %rcx
	movw	%bx, 8(%rdi,%rdx)
	cmpq	%r13, %rcx
	jnb	.L477
	movl	$256, %r11d
	addq	$5, %rax
	movw	%r11w, 10(%rdi,%rdx)
	cmpq	%rax, %r13
	jbe	.L477
	movl	$256, %r10d
	movw	%r10w, 12(%rdi,%rdx)
.L477:
	movq	-112(%rbp), %rsi
	movq	%r15, %rax
	movq	%r15, %rdx
	xorl	%r13d, %r13d
	movq	-152(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L514:
	movzbl	(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	cmpw	$256, (%rcx)
	jne	.L513
	movw	%r13w, (%rcx)
	addl	$1, %r13d
.L513:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L514
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L515:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movzwl	(%rcx,%rdx,2), %edx
	movb	%dl, -1(%rax)
	cmpq	%rax, %rsi
	jne	.L515
	movzwl	%r13w, %r13d
	movq	%rcx, -112(%rbp)
	movq	%rsi, -152(%rbp)
	testq	%r13, %r13
	je	.L519
	imulq	$2832, %r13, %rsi
	movq	-80(%rbp), %rdx
	xorl	%eax, %eax
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$2832, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$2816, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	-56(%rbp), %xmm5
	movsd	%xmm5, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L518
.L519:
	movq	-120(%rbp), %rsi
	movq	-80(%rbp), %rdi
	xorl	%edx, %edx
	movq	-88(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L517:
	movzbl	(%r15,%rdx), %eax
	movzwl	(%rsi,%rdx,2), %ecx
	addq	$1, %rdx
	imulq	$2832, %rax, %rax
	addq	%rdi, %rax
	addl	$1, (%rax,%rcx,4)
	addq	$1, 2816(%rax)
	cmpq	%rdx, %r10
	jne	.L517
	addq	$1, -136(%rbp)
	movq	-136(%rbp), %rax
	cmpq	-208(%rbp), %rax
	jne	.L520
	movq	-144(%rbp), %rbx
	movq	-72(%rbp), %rsi
	movq	%r15, -56(%rbp)
	movq	%r8, %r15
	movq	%r14, %r12
	movq	%r9, %r14
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-56(%rbp), %r10
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	movq	-232(%rbp), %r9
	movq	-88(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r10, %r8
	call	ClusterBlocksCommand
	movq	-56(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	BrotliFree
.L468:
	movq	-144(%rbp), %rbx
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-216(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliAllocate
	movq	-88(%rbp), %rdx
	movq	-184(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, -136(%rbp)
	movq	%rax, %rdi
	salq	$4, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L522:
	testl	$33554431, 4(%rsi)
	je	.L521
	cmpw	$127, 12(%rsi)
	jbe	.L521
	movzwl	14(%rsi), %eax
	andw	$1023, %ax
	movw	%ax, (%rdi,%rcx,2)
	addq	$1, %rcx
.L521:
	addq	$16, %rsi
	cmpq	%rsi, %rdx
	jne	.L522
	movq	%rcx, -64(%rbp)
	movq	%rcx, %rax
	testq	%rcx, %rcx
	je	.L594
	cmpq	$127, -64(%rbp)
	jbe	.L783
	movabsq	$-1085102592571150095, %rdx
	movq	-144(%rbp), %rdi
	mulq	%rdx
	movl	$50, %eax
	movq	%rax, %r15
	shrq	$9, %rdx
	leaq	1(%rdx), %r9
	cmpq	$50, %r9
	cmovbe	%r9, %r15
	imulq	$2192, %r15, %rbx
	movq	%rbx, %rsi
	call	BrotliAllocate
	xorl	%edx, %edx
	movsd	.LC1(%rip), %xmm6
	movq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	leaq	(%rbx,%rsi), %r8
	movsd	%xmm6, -56(%rbp)
	divq	%r15
	movq	%rsi, %rdx
	movq	%rax, %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L534:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$2192, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	-56(%rbp), %xmm5
	movsd	%xmm5, -8(%rdx)
	cmpq	%rdx, %r8
	jne	.L534
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movl	$7, %edi
	xorl	%edx, %edx
	movq	-136(%rbp), %r11
	movq	%r9, %rsi
	leaq	-41(%r9), %rbx
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	40(%rdx), %rax
	cmpq	%rax, %r9
	cmovbe	%rbx, %rdx
	addq	$40, 2176(%rcx)
	leaq	(%r11,%rdx,2), %rax
	leaq	80(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L537:
	movzwl	(%rax), %edx
	addq	$2, %rax
	addl	$1, (%rcx,%rdx,4)
	cmpq	%r12, %rax
	jne	.L537
	addq	$2192, %rcx
	cmpq	%rcx, %r8
	je	.L784
	movq	%rsi, %rax
	xorl	%edx, %edx
	addq	%r9, %rsi
	divq	%r15
	xorl	%edx, %edx
	movq	%rax, %r12
	imull	$16807, %edi, %eax
	movq	%rax, %rdi
	divq	%r10
	addq	%r12, %rdx
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L774:
	movq	-88(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	call	memset@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r8
	je	.L750
	movq	-112(%rbp), %rax
	movl	$256, %ecx
	movw	%cx, (%rax)
.L750:
	movl	$1, %r9d
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L784:
	movq	-64(%rbp), %rbx
	movq	-80(%rbp), %rcx
	movq	%r15, %rdx
	movq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	call	RefineEntropyCodesDistance.constprop.0
	movq	-144(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	BrotliAllocate
	movq	%r15, %rsi
	movq	%r12, %rdi
	salq	$4, %rsi
	movq	%rax, %r14
	addq	%r15, %rsi
	salq	$8, %rsi
	call	BrotliAllocate
	leaq	0(,%r15,8), %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movq	%rax, -72(%rbp)
	call	BrotliAllocate
	leaq	7(%r15), %rsi
	shrq	$3, %rsi
	movq	%rax, %r13
	imulq	%rbx, %rsi
	testq	%rsi, %rsi
	jne	.L785
.L539:
	movq	-144(%rbp), %rdi
	leaq	(%r15,%r15), %rsi
	call	BrotliAllocate
	movl	$3, %edx
	movq	%r14, -112(%rbp)
	movq	%r13, %r8
	movq	%rax, -104(%rbp)
	movq	-248(%rbp), %rax
	movq	$0, -152(%rbp)
	cmpl	$11, 4(%rax)
	movl	$10, %eax
	cmovl	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-64(%rbp), %rax
	addq	%r14, %rax
	movq	%r12, %r14
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L586:
	cmpq	$1, %r15
	jbe	.L786
	movq	%r15, %rbx
	xorl	%esi, %esi
	movq	%r8, -120(%rbp)
	leaq	0(,%r15,8), %r12
	salq	$4, %rbx
	addq	%r15, %rbx
	salq	$8, %rbx
	movq	%rbx, -96(%rbp)
	movq	%rbx, %rdx
	movq	-72(%rbp), %rbx
	movq	%rbx, %rdi
	call	memset@PLT
	movq	-80(%rbp), %rax
	movq	-120(%rbp), %r8
	movq	%r12, -128(%rbp)
	leaq	2176(%rax), %r13
	leaq	(%r12,%rbx), %rax
	movq	%r8, %r12
	movq	%rax, -88(%rbp)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L788:
	leaq	kLog2Table(%rip), %rax
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rax,%rdx,4), %xmm0
.L756:
	movsd	%xmm0, (%rbx)
	addq	$2192, %r13
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	je	.L787
.L547:
	movq	0(%r13), %rax
	movl	%eax, %edx
	testl	$4294967040, %eax
	je	.L788
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	call	log2@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%r15, %r13
	movq	%r12, %r8
	movq	-128(%rbp), %r12
	movq	-96(%rbp), %rcx
	negq	%r13
	addq	-72(%rbp), %rcx
	movq	%r14, -176(%rbp)
	leaq	0(,%r13,8), %rax
	movq	%r15, -184(%rbp)
	imulq	$-4344, %r15, %r13
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rax
	movq	%r8, -168(%rbp)
	leaq	2172(%rax), %rbx
	movq	%r12, -120(%rbp)
	movq	%r13, %r14
	movq	%rbx, %r15
	movq	%rcx, %r13
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L790:
	leaq	kLog2Table(%rip), %rdi
	pxor	%xmm0, %xmm0
	cvtss2sd	(%rdi,%rax,4), %xmm0
.L548:
	subsd	%xmm0, %xmm1
	addq	$2192, %r12
	addq	$8, %rbx
	movsd	%xmm1, -8(%rbx)
	cmpq	%rbx, -88(%rbp)
	jne	.L552
	leaq	-4(%r15), %rax
	addq	-120(%rbp), %r14
	cmpq	%r15, -80(%rbp)
	je	.L789
	movq	%rax, %r15
.L553:
	movq	%r13, -88(%rbp)
	addq	-128(%rbp), %r13
	movq	%r15, %r12
	movq	%r13, %rbx
.L552:
	movq	.LC4(%rip), %rdi
	movl	(%r12), %eax
	movsd	(%rbx,%r14), %xmm1
	movq	%rdi, %xmm0
	testq	%rax, %rax
	je	.L548
	cmpq	$255, %rax
	jbe	.L790
	pxor	%xmm0, %xmm0
	movsd	%xmm1, -96(%rbp)
	cvtsi2sdq	%rax, %xmm0
	call	log2@PLT
	movsd	-96(%rbp), %xmm1
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L789:
	movq	-168(%rbp), %r8
	movq	-120(%rbp), %r12
	xorl	%esi, %esi
	movq	-184(%rbp), %r15
	movq	-176(%rbp), %r14
	movq	%r8, %rdi
	movq	%r12, %rdx
	movq	%r8, -88(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %r13
	leaq	7(%r15), %rbx
	xorl	%esi, %esi
	shrq	$3, %rbx
	movq	%r14, %rdi
	movq	%r13, %rdx
	imulq	%rbx, %rdx
	call	memset@PLT
	leaq	-1(%r13), %r9
	movl	$1999, %r13d
	movsd	.LC5(%rip), %xmm3
	cmpq	$1999, %r9
	movq	%r9, -96(%rbp)
	movsd	.LC7(%rip), %xmm7
	movl	$1, %r10d
	cmovbe	%r9, %r13
	movq	-88(%rbp), %r8
	movq	-112(%rbp), %r9
	xorl	%edx, %edx
	movsd	.LC8(%rip), %xmm6
	movsd	.LC9(%rip), %xmm5
	xorl	%r11d, %r11d
	leaq	1(%r13), %rsi
	movsd	.LC12(%rip), %xmm2
	movq	%rsi, -120(%rbp)
	movq	-136(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L560:
	movzwl	(%rsi,%rdx,2), %ecx
	movapd	%xmm3, %xmm4
	xorl	%eax, %eax
	imulq	%r12, %rcx
	addq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L556:
	movsd	(%r8,%rax,8), %xmm0
	addsd	(%rcx,%rax,8), %xmm0
	comisd	%xmm0, %xmm4
	movsd	%xmm0, (%r8,%rax,8)
	jbe	.L554
	movb	%al, (%r9,%rdx)
	movapd	%xmm0, %xmm4
.L554:
	addq	$1, %rax
	cmpq	%r15, %rax
	jne	.L556
	pxor	%xmm0, %xmm0
	movq	%rdx, -88(%rbp)
	xorl	%eax, %eax
	cvtsi2sdq	%rdx, %xmm0
	mulsd	%xmm7, %xmm0
	divsd	%xmm6, %xmm0
	addsd	%xmm5, %xmm0
	mulsd	%xmm2, %xmm0
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L792:
	movsd	%xmm1, (%r8,%rax,8)
	addq	$1, %rax
	cmpq	%r15, %rax
	je	.L791
.L559:
	movsd	(%r8,%rax,8), %xmm1
	subsd	%xmm4, %xmm1
	comisd	%xmm0, %xmm1
	jb	.L792
	movq	%rax, %rdi
	movl	%eax, %ecx
	movsd	%xmm0, (%r8,%rax,8)
	movl	%r10d, %edx
	shrq	$3, %rdi
	andl	$7, %ecx
	addq	$1, %rax
	addq	%r11, %rdi
	sall	%cl, %edx
	orb	%dl, (%r14,%rdi)
	cmpq	%r15, %rax
	jne	.L559
.L791:
	movq	-88(%rbp), %rdx
	addq	%rbx, %r11
	leaq	1(%rdx), %rax
	cmpq	%r13, %rdx
	je	.L793
	movq	%rax, %rdx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L793:
	movq	-96(%rbp), %r9
	movq	-120(%rbp), %rsi
	cmpq	%rsi, -64(%rbp)
	jbe	.L570
	movq	%rbx, %rdi
	movq	-112(%rbp), %r13
	movq	-136(%rbp), %r10
	movl	$1, %r11d
	imulq	%rsi, %rdi
	.p2align 4,,10
	.p2align 3
.L569:
	movzwl	(%r10,%rsi,2), %edx
	movapd	%xmm3, %xmm1
	xorl	%eax, %eax
	imulq	%r12, %rdx
	addq	-72(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L564:
	movsd	(%rdx,%rax,8), %xmm0
	addsd	(%r8,%rax,8), %xmm0
	comisd	%xmm0, %xmm1
	movsd	%xmm0, (%r8,%rax,8)
	jbe	.L562
	movb	%al, 0(%r13,%rsi)
	movapd	%xmm0, %xmm1
.L562:
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L564
	xorl	%eax, %eax
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L795:
	movq	%rax, %rdx
	movl	%eax, %ecx
	movsd	%xmm2, (%r8,%rax,8)
	addq	$1, %rax
	shrq	$3, %rdx
	andl	$7, %ecx
	addq	%rdi, %rdx
	movq	%rdx, -88(%rbp)
	movl	%r11d, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	movq	-88(%rbp), %rdx
	orb	%cl, (%r14,%rdx)
	cmpq	%rax, %r15
	jbe	.L794
.L568:
	movsd	(%r8,%rax,8), %xmm0
	subsd	%xmm1, %xmm0
	comisd	%xmm2, %xmm0
	jnb	.L795
	movsd	%xmm0, (%r8,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r15
	ja	.L568
.L794:
	addq	$1, %rsi
	addq	%rbx, %rdi
	cmpq	%rsi, -64(%rbp)
	jne	.L569
.L570:
	movq	-112(%rbp), %rax
	movq	%rbx, %rdi
	movl	$1, %r11d
	movl	$1, %r13d
	imulq	%r9, %rdi
	movzbl	(%rax,%r9), %edx
	movq	%rax, %r9
	movq	-64(%rbp), %rax
	leaq	-2(%r9,%rax), %rax
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%rcx, %rax
.L572:
	movl	%edx, %esi
	subq	%rbx, %rdi
	movl	%edx, %ecx
	movl	%r13d, %r12d
	shrb	$3, %sil
	andl	$7, %ecx
	leaq	(%r14,%rdi), %r10
	movzbl	%sil, %esi
	sall	%cl, %r12d
	testb	%r12b, (%r10,%rsi)
	je	.L575
	movzbl	(%rax), %ecx
	cmpb	%cl, %dl
	je	.L575
	addq	$1, %r11
	movl	%ecx, %edx
.L575:
	movb	%dl, (%rax)
	leaq	-1(%rax), %rcx
	cmpq	%r9, %rax
	jne	.L616
	testq	%r15, %r15
	movl	$1, %ecx
	cmovne	%r15, %rcx
	cmpq	$7, %r15
	jbe	.L796
	movq	%rcx, %rdx
	movq	-104(%rbp), %rdi
	shrq	$3, %rdx
	salq	$4, %rdx
	movq	%rdi, %rax
	addq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L577:
	movdqa	.LC10(%rip), %xmm6
	addq	$16, %rax
	movups	%xmm6, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L577
	movq	%rcx, %rax
	andq	$-8, %rax
	cmpq	%rcx, %rax
	je	.L543
	movq	-104(%rbp), %rsi
	movl	$256, %edi
	leaq	(%rax,%rax), %rdx
	movw	%di, (%rsi,%rax,2)
	addq	$1, %rax
	cmpq	%r15, %rax
	jnb	.L543
.L590:
	movq	-104(%rbp), %rbx
	movl	$256, %ecx
	movw	%cx, 2(%rbx,%rdx)
	leaq	1(%rax), %rcx
	cmpq	%r15, %rcx
	jnb	.L543
	movl	$256, %r13d
	leaq	2(%rax), %rcx
	movw	%r13w, 4(%rbx,%rdx)
	cmpq	%r15, %rcx
	jnb	.L543
	movl	$256, %r12d
	leaq	3(%rax), %rcx
	movw	%r12w, 6(%rbx,%rdx)
	cmpq	%rcx, %r15
	jbe	.L543
	movl	$256, %r10d
	leaq	4(%rax), %rcx
	movw	%r10w, 8(%rbx,%rdx)
	cmpq	%rcx, %r15
	jbe	.L543
	movl	$256, %r9d
	addq	$5, %rax
	movw	%r9w, 10(%rbx,%rdx)
	cmpq	%rax, %r15
	jbe	.L543
	movl	$256, %edi
	movw	%di, 12(%rbx,%rdx)
.L543:
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rsi
	xorl	%r15d, %r15d
	movq	-160(%rbp), %rdi
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L580:
	movzbl	(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	cmpw	$256, (%rcx)
	jne	.L579
	movw	%r15w, (%rcx)
	addl	$1, %r15d
.L579:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L580
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L581:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movzwl	(%rcx,%rdx,2), %edx
	movb	%dl, -1(%rax)
	cmpq	%rax, %rsi
	jne	.L581
	movzwl	%r15w, %r15d
	movq	%rcx, -104(%rbp)
	movq	%rsi, -160(%rbp)
	testq	%r15, %r15
	je	.L585
	imulq	$2192, %r15, %rsi
	movq	-80(%rbp), %rdx
	xorl	%eax, %eax
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L584:
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	addq	$2192, %rdx
	andq	$-8, %rdi
	movq	$0, -24(%rdx)
	subq	%rdi, %rcx
	addl	$2176, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, -16(%rdx)
	movsd	-56(%rbp), %xmm7
	movsd	%xmm7, -8(%rdx)
	cmpq	%rdx, %rsi
	jne	.L584
.L585:
	movq	-64(%rbp), %rsi
	movq	-136(%rbp), %rdi
	xorl	%edx, %edx
	movq	-80(%rbp), %r9
	movq	-112(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L583:
	movzbl	(%r10,%rdx), %eax
	movzwl	(%rdi,%rdx,2), %ecx
	addq	$1, %rdx
	imulq	$2192, %rax, %rax
	addq	%r9, %rax
	addl	$1, (%rax,%rcx,4)
	addq	$1, 2176(%rax)
	cmpq	%rdx, %rsi
	jne	.L583
	addq	$1, -152(%rbp)
	movq	-152(%rbp), %rax
	cmpq	-192(%rbp), %rax
	jne	.L586
	movq	-144(%rbp), %rbx
	movq	%r8, %r13
	movq	%r11, %r15
	movq	%r14, %r12
	movq	-72(%rbp), %rsi
	movq	-112(%rbp), %r14
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-104(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	call	BrotliFree
	movq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %r8
	movq	-136(%rbp), %rsi
	movq	-240(%rbp), %r9
	movq	%r15, %rcx
	call	ClusterBlocksDistance
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	BrotliFree
.L524:
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BrotliFree
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	movq	-64(%rbp), %rdx
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r8, -88(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-88(%rbp), %r8
	je	.L752
	movq	-104(%rbp), %rax
	movl	$256, %esi
	movw	%si, (%rax)
.L752:
	movl	$1, %r11d
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L393:
	movq	-224(%rbp), %rax
	xorl	%esi, %esi
	movq	%rdi, %rbx
	movq	$1, (%rax)
	call	BrotliFree
	movq	-232(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	$1, (%rax)
	call	BrotliFree
	movq	$0, -136(%rbp)
.L594:
	movq	-240(%rbp), %rax
	movq	$1, (%rax)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L783:
	movq	-240(%rbp), %rdi
	movq	8(%rdi), %rdx
	movq	32(%rdi), %rax
	leaq	1(%rdx), %r12
	cmpq	%r12, %rax
	jnb	.L526
	testq	%rax, %rax
	jne	.L528
.L527:
	movq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-240(%rbp), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L797
.L529:
	movq	-240(%rbp), %r14
	movq	-144(%rbp), %rdi
	movq	16(%r14), %rsi
	call	BrotliFree
	movq	8(%r14), %rdx
	movq	%rbx, 16(%r14)
	movq	%r12, 32(%r14)
	leaq	1(%rdx), %r12
.L526:
	movq	-240(%rbp), %rax
	movq	40(%rax), %rax
	cmpq	%r12, %rax
	jnb	.L530
	testq	%rax, %rax
	jne	.L532
.L531:
	movq	-144(%rbp), %rdi
	leaq	0(,%r12,4), %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-240(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L798
.L533:
	movq	-240(%rbp), %r14
	movq	-144(%rbp), %rdi
	movq	24(%r14), %rsi
	call	BrotliFree
	movq	%rbx, 24(%r14)
	movq	8(%r14), %rdx
	movq	%r12, 40(%r14)
.L530:
	movq	-240(%rbp), %rsi
	movl	-64(%rbp), %edi
	movq	16(%rsi), %rax
	movq	$1, (%rsi)
	movb	$0, (%rax,%rdx)
	movq	8(%rsi), %rax
	movq	24(%rsi), %rdx
	movl	%edi, (%rdx,%rax,4)
	addq	$1, %rax
	movq	%rax, 8(%rsi)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L763:
	movq	-232(%rbp), %rdi
	movq	8(%rdi), %rdx
	movq	32(%rdi), %rax
	leaq	1(%rdx), %r12
	cmpq	%r12, %rax
	jnb	.L460
	testq	%rax, %rax
	jne	.L462
.L461:
	movq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-232(%rbp), %rax
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L799
.L463:
	movq	-232(%rbp), %r14
	movq	-144(%rbp), %rdi
	movq	16(%r14), %rsi
	call	BrotliFree
	movq	8(%r14), %rdx
	movq	%rbx, 16(%r14)
	movq	%r12, 32(%r14)
	leaq	1(%rdx), %r12
.L460:
	movq	-232(%rbp), %rax
	movq	40(%rax), %rax
	cmpq	%r12, %rax
	jnb	.L464
	testq	%rax, %rax
	jne	.L466
.L465:
	movq	-144(%rbp), %rdi
	leaq	0(,%r12,4), %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-232(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L800
.L467:
	movq	-232(%rbp), %r14
	movq	-144(%rbp), %rdi
	movq	24(%r14), %rsi
	call	BrotliFree
	movq	%rbx, 24(%r14)
	movq	8(%r14), %rdx
	movq	%r12, 40(%r14)
.L464:
	movq	-232(%rbp), %rsi
	movl	-88(%rbp), %edi
	movq	16(%rsi), %rax
	movq	$1, (%rsi)
	movb	$0, (%rax,%rdx)
	movq	8(%rsi), %rax
	movq	24(%rsi), %rdx
	movl	%edi, (%rdx,%rax,4)
	addq	$1, %rax
	movq	%rax, 8(%rsi)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L466:
	addq	%rax, %rax
	cmpq	%r12, %rax
	jb	.L466
	movq	%rax, %r12
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L462:
	addq	%rax, %rax
	cmpq	%rax, %r12
	ja	.L462
	movq	%rax, %r12
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L532:
	addq	%rax, %rax
	cmpq	%r12, %rax
	jb	.L532
	movq	%rax, %r12
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L528:
	addq	%rax, %rax
	cmpq	%rax, %r12
	ja	.L528
	movq	%rax, %r12
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L762:
	testq	%rax, %rax
	jne	.L410
.L409:
	movq	-144(%rbp), %rdi
	leaq	0(,%r12,4), %rsi
	call	BrotliAllocate
	movq	%rax, %rbx
	movq	-224(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L801
.L411:
	movq	-224(%rbp), %r14
	movq	-144(%rbp), %rdi
	movq	24(%r14), %rsi
	call	BrotliFree
	movq	%rbx, 24(%r14)
	movq	8(%r14), %rdx
	movq	%r12, 40(%r14)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L410:
	addq	%rax, %rax
	cmpq	%r12, %rax
	jb	.L410
	movq	%rax, %r12
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L406:
	addq	%rax, %rax
	cmpq	%rax, %r12
	ja	.L406
	movq	%rax, %r12
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L456:
	movq	-120(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L459:
	movzwl	(%rax), %esi
	addq	$2, %rdx
	addq	$16, %rax
	movw	%si, -2(%rdx)
	cmpq	%rdx, %rcx
	jne	.L459
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L765:
	movq	-144(%rbp), %rdi
	call	BrotliAllocate
	movq	%rax, -176(%rbp)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L785:
	movq	-144(%rbp), %rdi
	call	BrotliAllocate
	movq	%rax, %r12
	jmp	.L539
.L595:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L394
.L800:
	leaq	0(,%rax,4), %rdx
	movq	-232(%rbp), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	jmp	.L467
.L761:
	movq	16(%rax), %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	jmp	.L407
.L801:
	leaq	0(,%rax,4), %rdx
	movq	-224(%rbp), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	jmp	.L411
.L797:
	movq	16(%rax), %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	jmp	.L529
.L798:
	leaq	0(,%rax,4), %rdx
	movq	-240(%rbp), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	call	memcpy@PLT
	jmp	.L533
.L799:
	movq	16(%rax), %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	jmp	.L463
.L773:
	movq	-104(%rbp), %rax
	xorl	%edx, %edx
	movw	$256, (%rax)
	movl	$1, %eax
	jmp	.L593
.L796:
	movq	-104(%rbp), %rax
	xorl	%edx, %edx
	movw	$256, (%rax)
	movl	$1, %eax
	jmp	.L590
.L782:
	movq	-112(%rbp), %rax
	xorl	%edx, %edx
	movw	$256, (%rax)
	movl	$1, %eax
	jmp	.L591
.L402:
	movq	-224(%rbp), %rax
	movq	$1, (%rax)
	jmp	.L749
	.cfi_endproc
.LFE123:
	.size	BrotliSplitBlock, .-BrotliSplitBlock
	.section	.rodata
	.align 32
	.type	kLog2Table, @object
	.size	kLog2Table, 1024
kLog2Table:
	.long	0
	.long	0
	.long	1065353216
	.long	1070260237
	.long	1073741824
	.long	1075092088
	.long	1076195335
	.long	1077128116
	.long	1077936128
	.long	1078648845
	.long	1079286392
	.long	1079863124
	.long	1080389639
	.long	1080873985
	.long	1081322420
	.long	1081739903
	.long	1082130432
	.long	1082313855
	.long	1082486791
	.long	1082650374
	.long	1082805564
	.long	1082953181
	.long	1083093930
	.long	1083228421
	.long	1083357187
	.long	1083480696
	.long	1083599361
	.long	1083713546
	.long	1083823578
	.long	1083929749
	.long	1084032319
	.long	1084131527
	.long	1084227584
	.long	1084320685
	.long	1084411007
	.long	1084498710
	.long	1084583943
	.long	1084666840
	.long	1084747526
	.long	1084826116
	.long	1084902716
	.long	1084977425
	.long	1085050333
	.long	1085121526
	.long	1085191082
	.long	1085259075
	.long	1085325573
	.long	1085390641
	.long	1085454339
	.long	1085516724
	.long	1085577848
	.long	1085637762
	.long	1085696513
	.long	1085754144
	.long	1085810698
	.long	1085866214
	.long	1085920730
	.long	1085974281
	.long	1086026901
	.long	1086078621
	.long	1086129471
	.long	1086179482
	.long	1086228679
	.long	1086277089
	.long	1086324736
	.long	1086371645
	.long	1086417837
	.long	1086463335
	.long	1086508159
	.long	1086552328
	.long	1086595862
	.long	1086638778
	.long	1086681095
	.long	1086722827
	.long	1086763992
	.long	1086804604
	.long	1086844678
	.long	1086884228
	.long	1086923268
	.long	1086961810
	.long	1086999868
	.long	1087037453
	.long	1087074577
	.long	1087111251
	.long	1087147485
	.long	1087183291
	.long	1087218678
	.long	1087253656
	.long	1087288234
	.long	1087322421
	.long	1087356227
	.long	1087389659
	.long	1087422725
	.long	1087455434
	.long	1087487793
	.long	1087519810
	.long	1087551491
	.long	1087582844
	.long	1087613876
	.long	1087644592
	.long	1087675000
	.long	1087705106
	.long	1087734914
	.long	1087764432
	.long	1087793665
	.long	1087822617
	.long	1087851296
	.long	1087879705
	.long	1087907850
	.long	1087935735
	.long	1087963366
	.long	1087990747
	.long	1088017882
	.long	1088044776
	.long	1088071433
	.long	1088097857
	.long	1088124053
	.long	1088150023
	.long	1088175773
	.long	1088201305
	.long	1088226623
	.long	1088251732
	.long	1088276634
	.long	1088301332
	.long	1088325831
	.long	1088350132
	.long	1088374241
	.long	1088398158
	.long	1088421888
	.long	1088445433
	.long	1088468797
	.long	1088491981
	.long	1088514989
	.long	1088537824
	.long	1088560487
	.long	1088582982
	.long	1088605311
	.long	1088627476
	.long	1088649480
	.long	1088671326
	.long	1088693014
	.long	1088714548
	.long	1088735930
	.long	1088757162
	.long	1088778247
	.long	1088799185
	.long	1088819979
	.long	1088840631
	.long	1088861144
	.long	1088881518
	.long	1088901756
	.long	1088921859
	.long	1088941830
	.long	1088961669
	.long	1088981380
	.long	1089000963
	.long	1089020420
	.long	1089039752
	.long	1089058962
	.long	1089078051
	.long	1089097020
	.long	1089115871
	.long	1089134605
	.long	1089153224
	.long	1089171729
	.long	1089190121
	.long	1089208403
	.long	1089226574
	.long	1089244637
	.long	1089262593
	.long	1089280443
	.long	1089298188
	.long	1089315830
	.long	1089333369
	.long	1089350808
	.long	1089368146
	.long	1089385386
	.long	1089402528
	.long	1089419573
	.long	1089436523
	.long	1089453379
	.long	1089470141
	.long	1089486811
	.long	1089503389
	.long	1089519877
	.long	1089536276
	.long	1089552586
	.long	1089568809
	.long	1089584945
	.long	1089600996
	.long	1089616962
	.long	1089632844
	.long	1089648643
	.long	1089664360
	.long	1089679996
	.long	1089695552
	.long	1089711028
	.long	1089726425
	.long	1089741744
	.long	1089756987
	.long	1089772152
	.long	1089787242
	.long	1089802258
	.long	1089817199
	.long	1089832066
	.long	1089846861
	.long	1089861584
	.long	1089876236
	.long	1089890817
	.long	1089905328
	.long	1089919769
	.long	1089934143
	.long	1089948448
	.long	1089962686
	.long	1089976857
	.long	1089990962
	.long	1090005002
	.long	1090018977
	.long	1090032887
	.long	1090046734
	.long	1090060518
	.long	1090074239
	.long	1090087899
	.long	1090101497
	.long	1090115034
	.long	1090128511
	.long	1090141928
	.long	1090155286
	.long	1090168585
	.long	1090181826
	.long	1090195009
	.long	1090208135
	.long	1090221205
	.long	1090234218
	.long	1090247175
	.long	1090260077
	.long	1090272925
	.long	1090285718
	.long	1090298457
	.long	1090311143
	.long	1090323775
	.long	1090336356
	.long	1090348884
	.long	1090361360
	.long	1090373786
	.long	1090386160
	.long	1090398484
	.long	1090410758
	.long	1090422983
	.long	1090435158
	.long	1090447284
	.long	1090459363
	.long	1090471393
	.long	1090483375
	.long	1090495310
	.long	1090507198
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	0
	.quad	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	2146435072
	.section	.rodata.cst16
	.align 16
.LC2:
	.quad	4
	.quad	4
	.align 16
.LC3:
	.quad	2
	.quad	2
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	-1073741824
	.align 8
.LC5:
	.long	2726797102
	.long	1417495214
	.align 8
.LC6:
	.long	2576980378
	.long	1077680537
	.align 8
.LC7:
	.long	515396076
	.long	1068624773
	.align 8
.LC8:
	.long	0
	.long	1084178432
	.align 8
.LC9:
	.long	171798692
	.long	1072210903
	.section	.rodata.cst16
	.align 16
.LC10:
	.value	256
	.value	256
	.value	256
	.value	256
	.value	256
	.value	256
	.value	256
	.value	256
	.section	.rodata.cst8
	.align 8
.LC11:
	.long	0
	.long	1076559872
	.align 8
.LC12:
	.long	858993459
	.long	1076704051
	.hidden	BrotliHistogramBitCostDistanceDistance
	.hidden	BrotliHistogramCombineDistance
	.hidden	BrotliPopulationCostDistance
	.hidden	BrotliHistogramBitCostDistanceCommand
	.hidden	BrotliHistogramCombineCommand
	.hidden	BrotliPopulationCostCommand
	.hidden	BrotliHistogramBitCostDistanceLiteral
	.hidden	BrotliFree
	.hidden	BrotliHistogramCombineLiteral
	.hidden	BrotliPopulationCostLiteral
	.hidden	BrotliAllocate
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
