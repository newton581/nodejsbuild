	.file	"utf8_util.c"
	.text
	.p2align 4
	.globl	BrotliIsMostlyUTF8
	.hidden	BrotliIsMostlyUTF8
	.type	BrotliIsMostlyUTF8, @function
BrotliIsMostlyUTF8:
.LFB48:
	.cfi_startproc
	endbr64
	movapd	%xmm0, %xmm2
	testq	%rcx, %rcx
	je	.L13
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L38:
	cmpq	$1, %r10
	je	.L4
	movl	%r9d, %r12d
	movzbl	%r9b, %ebx
	andl	$-32, %r12d
	cmpb	$-64, %r12b
	je	.L35
	cmpq	$2, %r10
	je	.L4
	movl	%r9d, %r12d
	andl	$-16, %r12d
	cmpb	$-32, %r12b
	jne	.L6
	movzbl	1(%rax), %r9d
	movl	%r9d, %r10d
	andl	$-64, %r10d
	cmpb	$-128, %r10b
	je	.L36
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %r8
	cmpq	%r8, %rcx
	jbe	.L37
.L7:
	leaq	(%rsi,%r8), %rax
	movq	%rcx, %r10
	andq	%rdx, %rax
	subq	%r8, %r10
	addq	%rdi, %rax
	movzbl	(%rax), %r9d
	testb	%r9b, %r9b
	jle	.L38
	movl	$1, %eax
.L3:
	addq	%rax, %r8
	addq	%rax, %r11
	cmpq	%r8, %rcx
	ja	.L7
.L37:
	testq	%r11, %r11
	js	.L8
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r11, %xmm1
	testq	%rcx, %rcx
	js	.L10
.L39:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
.L11:
	mulsd	%xmm2, %xmm0
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	comisd	%xmm0, %xmm1
	seta	%al
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movzbl	1(%rax), %eax
	movl	%eax, %r9d
	andl	$-64, %r9d
	cmpb	$-128, %r9b
	jne	.L4
	sall	$6, %ebx
	andl	$63, %eax
	andl	$1984, %ebx
	orl	%ebx, %eax
	cmpl	$127, %eax
	jle	.L4
	movl	$2, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	$3, %r10
	je	.L4
	andl	$-8, %r9d
	cmpb	$-16, %r9b
	jne	.L4
	movzbl	1(%rax), %r9d
	movl	%r9d, %r10d
	andl	$-64, %r10d
	cmpb	$-128, %r10b
	jne	.L4
	movzbl	2(%rax), %r10d
	movl	%r10d, %r12d
	andl	$-64, %r12d
	cmpb	$-128, %r12b
	jne	.L4
	movzbl	3(%rax), %r12d
	movl	%r12d, %eax
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L4
	sall	$12, %r9d
	movl	%ebx, %eax
	andl	$63, %r12d
	sall	$6, %r10d
	sall	$18, %eax
	andl	$258048, %r9d
	andl	$4032, %r10d
	andl	$1835008, %eax
	orl	%r9d, %eax
	orl	%r12d, %eax
	orl	%r10d, %eax
	subl	$65536, %eax
	cmpl	$1048575, %eax
	ja	.L4
	movl	$4, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r11, %rax
	andl	$1, %r11d
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%r11, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	testq	%rcx, %rcx
	jns	.L39
.L10:
	movq	%rcx, %rax
	andl	$1, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L36:
	movzbl	2(%rax), %eax
	movl	%eax, %r10d
	andl	$-64, %r10d
	cmpb	$-128, %r10b
	jne	.L4
	sall	$12, %ebx
	sall	$6, %r9d
	andl	$63, %eax
	movzwl	%bx, %r10d
	andl	$4032, %r9d
	orl	%r10d, %r9d
	orl	%r9d, %eax
	cmpl	$2047, %eax
	jle	.L4
	movl	$3, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	cvtsi2sdq	%rcx, %xmm0
	mulsd	%xmm2, %xmm0
	comisd	%xmm0, %xmm1
	seta	%al
	ret
	.cfi_endproc
.LFE48:
	.size	BrotliIsMostlyUTF8, .-BrotliIsMostlyUTF8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
