	.file	"state.c"
	.text
	.p2align 4
	.type	BrotliDefaultFreeFunc, @function
BrotliDefaultFreeFunc:
.LFB45:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	free@PLT
	.cfi_endproc
.LFE45:
	.size	BrotliDefaultFreeFunc, .-BrotliDefaultFreeFunc
	.p2align 4
	.type	BrotliDefaultAllocFunc, @function
BrotliDefaultAllocFunc:
.LFB44:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	jmp	malloc@PLT
	.cfi_endproc
.LFE44:
	.size	BrotliDefaultAllocFunc, .-BrotliDefaultAllocFunc
	.p2align 4
	.globl	BrotliDecoderStateInit
	.hidden	BrotliDecoderStateInit
	.type	BrotliDecoderStateInit, @function
BrotliDecoderStateInit:
.LFB68:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rdx, -24(%rbp)
	testq	%rsi, %rsi
	je	.L8
	movq	%rsi, %xmm0
	movq	%rcx, 56(%rdi)
	movhps	-24(%rbp), %xmm0
	movups	%xmm0, 40(%rdi)
.L6:
	movl	$0, 116(%rbx)
	leaq	8(%rbx), %rdi
	call	BrotliInitBitReader
	movzbl	5072(%rbx), %eax
	pxor	%xmm0, %xmm0
	movq	$0, (%rbx)
	movups	%xmm0, 384(%rbx)
	andl	$-64, %eax
	movups	%xmm0, 248(%rbx)
	orl	$16, %eax
	movups	%xmm0, 5088(%rbx)
	movb	%al, 5072(%rbx)
	leaq	584(%rbx), %rax
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 176(%rbx)
	movups	%xmm0, 200(%rbx)
	movups	%xmm0, 224(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 5044(%rbx)
	movdqa	.LC0(%rip), %xmm0
	movq	$0, 72(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 344(%rbx)
	movl	$0, 120(%rbx)
	movl	$0, 5060(%rbx)
	movq	$0, 5064(%rbx)
	movq	$0, 5076(%rbx)
	movl	$0, 84(%rbx)
	movq	$0, 88(%rbx)
	movl	$4, 112(%rbx)
	movq	%rax, 544(%rbx)
	movl	$63, 4780(%rbx)
	movups	%xmm0, 96(%rbx)
	call	BrotliGetDictionary@PLT
	movq	%rax, 5104(%rbx)
	call	BrotliGetTransforms@PLT
	movq	%rax, 5112(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	BrotliDefaultFreeFunc(%rip), %rax
	leaq	BrotliDefaultAllocFunc(%rip), %rdx
	movq	$0, 56(%rdi)
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rdi)
	jmp	.L6
	.cfi_endproc
.LFE68:
	.size	BrotliDecoderStateInit, .-BrotliDecoderStateInit
	.p2align 4
	.globl	BrotliDecoderStateMetablockBegin
	.hidden	BrotliDecoderStateMetablockBegin
	.type	BrotliDecoderStateMetablockBegin, @function
BrotliDecoderStateMetablockBegin:
.LFB69:
	.cfi_startproc
	endbr64
	movdqa	.LC1(%rip), %xmm0
	movl	$0, 272(%rdi)
	movb	$0, 360(%rdi)
	movups	%xmm0, 280(%rdi)
	movdqa	.LC2(%rip), %xmm0
	movq	$0, 184(%rdi)
	movups	%xmm0, 296(%rdi)
	movdqa	.LC3(%rip), %xmm0
	movups	%xmm0, 312(%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 5088(%rdi)
	movups	%xmm0, 344(%rdi)
	movups	%xmm0, 152(%rdi)
	movups	%xmm0, 168(%rdi)
	movups	%xmm0, 200(%rdi)
	movups	%xmm0, 224(%rdi)
	ret
	.cfi_endproc
.LFE69:
	.size	BrotliDecoderStateMetablockBegin, .-BrotliDecoderStateMetablockBegin
	.p2align 4
	.globl	BrotliDecoderStateCleanupAfterMetablock
	.hidden	BrotliDecoderStateCleanupAfterMetablock
	.type	BrotliDecoderStateCleanupAfterMetablock, @function
BrotliDecoderStateCleanupAfterMetablock:
.LFB70:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	5096(%rdi), %rsi
	movq	56(%rdi), %rdi
	call	*48(%rbx)
	movq	5088(%rbx), %rsi
	movq	56(%rbx), %rdi
	movq	$0, 5096(%rbx)
	call	*48(%rbx)
	movq	344(%rbx), %rsi
	movq	56(%rbx), %rdi
	movq	$0, 5088(%rbx)
	call	*48(%rbx)
	movq	176(%rbx), %rsi
	movq	56(%rbx), %rdi
	movq	$0, 344(%rbx)
	call	*48(%rbx)
	movq	200(%rbx), %rsi
	movq	56(%rbx), %rdi
	movq	$0, 176(%rbx)
	call	*48(%rbx)
	movq	224(%rbx), %rsi
	movq	56(%rbx), %rdi
	movq	$0, 200(%rbx)
	call	*48(%rbx)
	movq	$0, 224(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE70:
	.size	BrotliDecoderStateCleanupAfterMetablock, .-BrotliDecoderStateCleanupAfterMetablock
	.p2align 4
	.globl	BrotliDecoderStateCleanup
	.hidden	BrotliDecoderStateCleanup
	.type	BrotliDecoderStateCleanup, @function
BrotliDecoderStateCleanup:
.LFB71:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	BrotliDecoderStateCleanupAfterMetablock
	movq	128(%rbx), %rsi
	movq	56(%rbx), %rdi
	call	*48(%rbx)
	movq	248(%rbx), %rsi
	movq	56(%rbx), %rdi
	movq	$0, 128(%rbx)
	call	*48(%rbx)
	movq	$0, 248(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE71:
	.size	BrotliDecoderStateCleanup, .-BrotliDecoderStateCleanup
	.p2align 4
	.globl	BrotliDecoderHuffmanTreeGroupInit
	.hidden	BrotliDecoderHuffmanTreeGroupInit
	.type	BrotliDecoderHuffmanTreeGroupInit, @function
BrotliDecoderHuffmanTreeGroupInit:
.LFB72:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	leaq	kMaxHuffmanTableSize(%rip), %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%r8d, %edx
	pushq	%r13
	leal	31(%r14), %eax
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	shrl	$5, %eax
	.cfi_offset 12, -48
	leaq	0(,%rdx,8), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movzwl	(%rcx,%rax,2), %eax
	movq	56(%rdi), %rdi
	imulq	%rdx, %rax
	leaq	(%r12,%rax,4), %rsi
	call	*40(%r9)
	movw	%r14w, 16(%rbx)
	addq	%rax, %r12
	testq	%rax, %rax
	movw	%r15w, 18(%rbx)
	movw	%r13w, 20(%rbx)
	movq	%rax, (%rbx)
	setne	%al
	movq	%r12, 8(%rbx)
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE72:
	.size	BrotliDecoderHuffmanTreeGroupInit, .-BrotliDecoderHuffmanTreeGroupInit
	.section	.rodata
	.align 32
	.type	kMaxHuffmanTableSize, @object
	.size	kMaxHuffmanTableSize, 74
kMaxHuffmanTableSize:
	.value	256
	.value	402
	.value	436
	.value	468
	.value	500
	.value	534
	.value	566
	.value	598
	.value	630
	.value	662
	.value	694
	.value	726
	.value	758
	.value	790
	.value	822
	.value	854
	.value	886
	.value	920
	.value	952
	.value	984
	.value	1016
	.value	1048
	.value	1080
	.value	1112
	.value	1144
	.value	1176
	.value	1208
	.value	1240
	.value	1272
	.value	1304
	.value	1336
	.value	1368
	.value	1400
	.value	1432
	.value	1464
	.value	1496
	.value	1528
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	16
	.long	15
	.long	11
	.align 16
.LC1:
	.long	16777216
	.long	16777216
	.long	16777216
	.long	1
	.align 16
.LC2:
	.long	1
	.long	1
	.long	1
	.long	0
	.align 16
.LC3:
	.long	1
	.long	0
	.long	1
	.long	0
	.hidden	BrotliInitBitReader
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
