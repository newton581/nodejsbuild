	.file	"brotli_bit_stream.c"
	.text
	.p2align 4
	.type	StoreSimpleHuffmanTree, @function
StoreSimpleHuffmanTree:
.LFB111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r8), %rax
	movq	%rax, %rcx
	movl	%eax, %ebx
	shrq	$3, %rcx
	andl	$7, %ebx
	addq	%r9, %rcx
	movzbl	(%rcx), %r11d
	btsq	%rbx, %r11
	movq	%r11, (%rcx)
	leaq	2(%rax), %rcx
	leaq	-1(%rdx), %r11
	movq	%rcx, %rbx
	movq	%rcx, (%r8)
	andl	$7, %ecx
	shrq	$3, %rbx
	salq	%cl, %r11
	leaq	4(%rax), %rcx
	addq	%r9, %rbx
	movq	%rcx, (%r8)
	movzbl	(%rbx), %r12d
	orq	%r12, %r11
	movq	%r11, (%rbx)
	testq	%rdx, %rdx
	je	.L2
	movl	$1, %ebx
	cmpq	$1, %rdx
	jbe	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	movq	-8(%rsi,%rbx,8), %r11
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rsi,%rax,8), %rcx
	movzbl	(%rdi,%r11), %r15d
	cmpb	%r15b, (%rdi,%rcx)
	jnb	.L5
	movq	%r11, (%rsi,%rax,8)
	movq	%rcx, %r11
	movq	%rcx, -8(%rsi,%rbx,8)
.L5:
	addq	$1, %rax
	cmpq	%rax, %rdx
	ja	.L6
	addq	$1, %rbx
	cmpq	%rbx, %rdx
	jne	.L3
.L4:
	movq	(%r8), %rax
	movq	(%rsi), %r14
	movq	%rax, %rbx
	movl	%eax, %ecx
	addq	%r10, %rax
	shrq	$3, %rbx
	andl	$7, %ecx
	movq	%rax, %r12
	movl	%eax, %r15d
	addq	%r9, %rbx
	salq	%cl, %r14
	andl	$7, %r15d
	leaq	(%r10,%rax), %r13
	movzbl	(%rbx), %r11d
	movq	%r14, %rcx
	shrq	$3, %r12
	addq	%r9, %r12
	orq	%r11, %rcx
	cmpq	$2, %rdx
	je	.L21
	movq	%r13, %r14
	movl	%r13d, %r11d
	andl	$7, %r11d
	shrq	$3, %r14
	movl	%r11d, -44(%rbp)
	addq	%r9, %r14
	leaq	(%r10,%r13), %r11
	cmpq	$3, %rdx
	je	.L22
.L9:
	movq	%rcx, (%rbx)
	movl	%r15d, %ecx
	addq	%r11, %r10
	movq	%rax, (%r8)
	movq	8(%rsi), %rax
	movzbl	(%r12), %edx
	movq	%r13, (%r8)
	salq	%cl, %rax
	movzbl	-44(%rbp), %ecx
	orq	%rdx, %rax
	movq	%rax, (%r12)
	movq	16(%rsi), %rax
	movzbl	(%r14), %edx
	movq	%r11, (%r8)
	salq	%cl, %rax
	movl	%r11d, %ecx
	orq	%rdx, %rax
	movq	%r11, %rdx
	andl	$7, %ecx
	shrq	$3, %rdx
	movq	%rax, (%r14)
	movq	24(%rsi), %rax
	addq	%r9, %rdx
	movq	%r10, (%r8)
	movzbl	(%rdx), %ebx
	salq	%cl, %rax
	movl	%r10d, %ecx
	orq	%rbx, %rax
	movq	%rax, (%rdx)
	movq	%r10, %rax
	shrq	$3, %rax
	addq	%rax, %r9
	movq	(%rsi), %rax
	movzbl	(%r9), %edx
	cmpb	$1, (%rdi,%rax)
	sete	%al
	andl	$7, %ecx
	addq	$1, %r10
	movzbl	%al, %eax
	movq	%r10, (%r8)
	salq	%cl, %rax
	orq	%rdx, %rax
	movq	%rax, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	movq	%rcx, (%rbx)
	movl	%r15d, %ecx
	movq	%rax, (%r8)
	movq	8(%rsi), %rax
	movzbl	(%r12), %edx
	movq	%r13, (%r8)
	salq	%cl, %rax
	movzbl	-44(%rbp), %ecx
	orq	%rdx, %rax
	movq	%rax, (%r12)
	movq	16(%rsi), %rax
	movzbl	(%r14), %edx
	movq	%r11, (%r8)
	salq	%cl, %rax
	orq	%rdx, %rax
	movq	%rax, (%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movq	%rcx, (%rbx)
	movl	%r15d, %ecx
	movq	%rax, (%r8)
	movq	8(%rsi), %rax
	movzbl	(%r12), %edx
	movq	%r13, (%r8)
	salq	%cl, %rax
	orq	%rdx, %rax
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	leaq	(%r10,%rcx), %rax
	movq	(%rsi), %rdx
	leaq	(%r10,%rax), %r13
	movq	%rax, %r12
	movl	%eax, %r15d
	movl	%r13d, %ebx
	movq	%r13, %r14
	shrq	$3, %r12
	leaq	(%r10,%r13), %r11
	andl	$7, %ebx
	shrq	$3, %r14
	addq	%r9, %r12
	andl	$7, %r15d
	movl	%ebx, -44(%rbp)
	movq	%rcx, %rbx
	andl	$7, %ecx
	addq	%r9, %r14
	shrq	$3, %rbx
	salq	%cl, %rdx
	addq	%r9, %rbx
	movq	%rdx, %rcx
	movzbl	(%rbx), %edx
	orq	%rdx, %rcx
	jmp	.L9
	.cfi_endproc
.LFE111:
	.size	StoreSimpleHuffmanTree, .-StoreSimpleHuffmanTree
	.p2align 4
	.type	StoreSymbol, @function
StoreSymbol:
.LFB127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	920(%rdi), %rax
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rax, %rax
	jne	.L24
	movq	912(%rdi), %rcx
	movq	24(%rdi), %r9
	movq	(%rdi), %r11
	leaq	1(%rcx), %r8
	movl	(%r9,%r8,4), %ebx
	movq	%r8, 912(%rdi)
	movq	16(%rdi), %r8
	movq	%rbx, %r9
	movzbl	1(%r8,%rcx), %ecx
	movq	%rbx, 920(%rdi)
	imulq	%rcx, %r11
	movq	%r11, 928(%rdi)
	movq	40(%rdi), %r11
	leaq	1(%r11), %rbx
	cmpq	%rbx, %rcx
	je	.L32
	cmpq	48(%rdi), %rcx
	je	.L25
	leaq	2(%rcx), %rax
.L25:
	movq	%rcx, %xmm0
	movq	%r11, %xmm1
	movzwl	314(%rdi,%rax,2), %r8d
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rdi)
	movq	(%rdx), %rbx
	movzbl	56(%rdi,%rax), %r12d
	movq	%rbx, %r11
	movl	%ebx, %ecx
	addq	%rbx, %r12
	shrq	$3, %r11
	andl	$7, %ecx
	movq	%r12, (%rdx)
	addq	%r10, %r11
	salq	%cl, %r8
	movzbl	(%r11), %r13d
	orq	%r13, %r8
	movq	%r8, (%r11)
	cmpl	$176, %r9d
	jbe	.L26
	cmpl	$753, %r9d
	sbbl	%r8d, %r8d
	andl	$-6, %r8d
	addl	$20, %r8d
.L27:
	leal	1(%r8), %ecx
	leaq	kBlockLengthPrefixCode(%rip), %r11
	movq	%rcx, %rax
	leaq	(%r11,%rcx,8), %rcx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L38:
	addl	$1, %eax
.L29:
	movl	%r8d, %ebx
	movl	%eax, %r8d
	cmpl	(%rcx), %r9d
	jb	.L37
	addq	$8, %rcx
	cmpl	$25, %eax
	jne	.L38
.L30:
	movq	%r12, %r13
	movl	%r12d, %ecx
	subl	(%r11,%r8,8), %r9d
	movzwl	856(%rdi,%r8,2), %ebx
	movzbl	830(%rdi,%r8), %eax
	shrq	$3, %r13
	andl	$7, %ecx
	addq	%r10, %r13
	salq	%cl, %rbx
	movzbl	0(%r13), %r14d
	addq	%r12, %rax
	movl	%eax, %ecx
	movq	%rax, (%rdx)
	orq	%r14, %rbx
	andl	$7, %ecx
	movq	%rbx, 0(%r13)
	movq	%rax, %rbx
	salq	%cl, %r9
	movl	4(%r11,%r8,8), %ecx
	shrq	$3, %rbx
	addq	%r10, %rbx
	addq	%rcx, %rax
	movzbl	(%rbx), %r12d
	movq	%rax, (%rdx)
	orq	%r12, %r9
	movq	%r9, (%rbx)
	movq	920(%rdi), %rax
.L24:
	subq	$1, %rax
	addq	928(%rdi), %rsi
	movq	%rax, 920(%rdi)
	movq	936(%rdi), %rax
	movq	(%rdx), %r9
	movzbl	(%rax,%rsi), %r8d
	movq	944(%rdi), %rax
	movq	%r9, %rcx
	shrq	$3, %rcx
	movzwl	(%rax,%rsi,2), %eax
	addq	%r9, %r8
	addq	%rcx, %r10
	movl	%r9d, %ecx
	movq	%r8, (%rdx)
	movzbl	(%r10), %r11d
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%r11, %rax
	movq	%rax, (%r10)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpl	$41, %r9d
	sbbl	%r8d, %r8d
	notl	%r8d
	andl	$7, %r8d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%ebx, %r8d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1, %eax
	jmp	.L25
	.cfi_endproc
.LFE127:
	.size	StoreSymbol, .-StoreSymbol
	.p2align 4
	.type	StoreCompressedMetaBlockHeader, @function
StoreCompressedMetaBlockHeader:
.LFB107:
	.cfi_startproc
	movq	(%rdx), %r10
	movq	%rcx, %r8
	movslq	%edi, %rax
	movq	%r10, %r9
	shrq	$3, %r9
	addq	%rcx, %r9
	movl	%r10d, %ecx
	movzbl	(%r9), %r11d
	andl	$7, %ecx
	salq	%cl, %rax
	orq	%r11, %rax
	movq	%rax, (%r9)
	leaq	1(%r10), %rax
	movq	%rax, %r11
	movq	%rax, (%rdx)
	shrq	$3, %r11
	addq	%r8, %r11
	testl	%edi, %edi
	je	.L40
	movzbl	(%r11), %eax
	movq	%rax, (%r11)
	leaq	2(%r10), %rax
	movq	%rax, %r11
	movq	%rax, (%rdx)
	shrq	$3, %r11
	addq	%r8, %r11
.L40:
	cmpq	$1, %rsi
	je	.L44
	leal	-1(%rsi), %ecx
	bsrl	%ecx, %ecx
	addl	$1, %ecx
	cmpl	$15, %ecx
	ja	.L48
.L44:
	xorl	%ecx, %ecx
	movl	$16, %r10d
.L41:
	movzbl	(%r11), %r9d
	addq	$2, %rax
	subq	$1, %rsi
	movq	%rax, (%rdx)
	orq	%r9, %rcx
	movq	%rax, %r9
	shrq	$3, %r9
	movq	%rcx, (%r11)
	movl	%eax, %ecx
	addq	%r10, %rax
	addq	%r8, %r9
	andl	$7, %ecx
	movq	%rax, (%rdx)
	movzbl	(%r9), %r11d
	salq	%cl, %rsi
	orq	%r11, %rsi
	movq	%rsi, (%r9)
	testl	%edi, %edi
	jne	.L39
	movq	%rax, %rcx
	addq	$1, %rax
	shrq	$3, %rcx
	movq	%rax, (%rdx)
	addq	%rcx, %r8
	movzbl	(%r8), %ecx
	movq	%rcx, (%r8)
.L39:
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$3, %rcx
	shrq	$2, %rcx
	leaq	-4(%rcx), %r9
	leaq	0(,%rcx,4), %r10
	movl	%eax, %ecx
	andl	$7, %ecx
	salq	%cl, %r9
	movq	%r9, %rcx
	jmp	.L41
	.cfi_endproc
.LFE107:
	.size	StoreCompressedMetaBlockHeader, .-StoreCompressedMetaBlockHeader
	.p2align 4
	.type	StoreDataWithHuffmanCodes, @function
StoreDataWithHuffmanCodes:
.LFB135:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	movq	56(%rbp), %r12
	movq	64(%rbp), %r15
	testq	%r8, %r8
	je	.L49
	salq	$4, %r8
	movq	%rdi, %r13
	movq	(%r12), %rdi
	movq	%rsi, %rbx
	addq	%rcx, %r8
	movq	%r9, %r14
	movq	%rcx, %r10
	movq	%r8, -80(%rbp)
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L61:
	movl	8(%r10), %eax
	movzwl	14(%r10), %edx
	movl	4(%r10), %ecx
	movq	32(%rbp), %r11
	movl	%eax, -68(%rbp)
	movzwl	12(%r10), %eax
	movw	%dx, -48(%rbp)
	movq	24(%rbp), %rdx
	movl	%ecx, -44(%rbp)
	movl	%esi, %ecx
	movl	(%r10), %r9d
	movzbl	(%rdx,%rax), %edi
	movq	%rsi, %rdx
	movw	%ax, -46(%rbp)
	andl	$7, %ecx
	shrq	$3, %rdx
	movzwl	(%r11,%rax,2), %eax
	addq	%r15, %rdx
	addq	%rdi, %rsi
	movzbl	(%rdx), %r8d
	salq	%cl, %rax
	movl	-44(%rbp), %ecx
	movq	%rsi, (%r12)
	orq	%rax, %r8
	movq	%r8, (%rdx)
	movl	%ecx, %edx
	shrl	$25, %edx
	leal	(%rdx,%rdx), %eax
	andl	$-128, %eax
	orl	%edx, %eax
	movl	%ecx, %edx
	movslq	%r9d, %rcx
	andl	$33554431, %edx
	movsbl	%al, %eax
	addl	%edx, %eax
	movl	%r9d, %edx
	cmpl	$5, %r9d
	jbe	.L76
	cmpq	$129, %rdx
	jbe	.L79
	cmpq	$2113, %rdx
	jbe	.L80
	cmpq	$6209, %rdx
	jbe	.L63
	cmpq	$22594, %rdx
	sbbl	%ecx, %ecx
	andl	$-16384, %ecx
	addl	$22594, %ecx
	cmpq	$22594, %rdx
	sbbl	%r8d, %r8d
	movl	%ecx, -64(%rbp)
	andl	$-10, %r8d
	addl	$24, %r8d
.L52:
	movl	%eax, %edi
	cmpl	$9, %eax
	jbe	.L81
.L55:
	cmpq	$133, %rdi
	jbe	.L82
	cmpq	$2117, %rdi
	ja	.L65
	leal	-70(%rax), %edi
	bsrl	%edi, %edi
	addl	$12, %edi
.L78:
	movzwl	%di, %edi
	leaq	kCopyBase(%rip), %rcx
	leaq	kCopyExtra(%rip), %r11
	movl	(%rcx,%rdi,4), %ecx
	movl	(%r11,%rdi,4), %edi
	movl	%edi, -60(%rbp)
.L56:
	movq	%rsi, %rdi
	subl	%ecx, %eax
	movl	%r8d, %ecx
	shrq	$3, %rdi
	salq	%cl, %rax
	movl	%r9d, %ecx
	subl	-64(%rbp), %ecx
	addq	%r15, %rdi
	orq	%rcx, %rax
	movl	%esi, %ecx
	movzbl	(%rdi), %r11d
	andl	$7, %ecx
	salq	%cl, %rax
	movq	%r11, %r9
	movq	%r11, -56(%rbp)
	orq	%rax, %r9
	movl	-60(%rbp), %eax
	movq	%r9, (%rdi)
	addl	%r8d, %eax
	addq	%rax, %rsi
	movq	%rsi, (%r12)
	testq	%rdx, %rdx
	je	.L66
	movq	%r10, -56(%rbp)
	movq	16(%rbp), %r11
	addq	%rbx, %rdx
	movq	-88(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r10, %rax
	movq	%rsi, %rdi
	movl	%esi, %ecx
	andq	%rbx, %rax
	shrq	$3, %rdi
	andl	$7, %ecx
	addq	$1, %rbx
	movzbl	0(%r13,%rax), %eax
	addq	%r15, %rdi
	movzbl	(%rdi), %r9d
	movzbl	(%r14,%rax), %r8d
	movzwl	(%r11,%rax,2), %eax
	salq	%cl, %rax
	addq	%r8, %rsi
	orq	%rax, %r9
	movq	%rsi, (%r12)
	movq	%r9, (%rdi)
	cmpq	%rdx, %rbx
	jne	.L59
	movq	%r11, 16(%rbp)
	movq	-56(%rbp), %r10
.L58:
	movl	-44(%rbp), %eax
	andl	$33554431, %eax
	movl	%eax, %ebx
	addq	%rdx, %rbx
	cmpw	$127, -46(%rbp)
	jbe	.L60
	testl	%eax, %eax
	je	.L60
	movzwl	-48(%rbp), %r11d
	movq	40(%rbp), %rax
	movq	%rsi, %rdi
	movq	48(%rbp), %rcx
	shrq	$3, %rdi
	movq	%r11, %rdx
	addq	%r15, %rdi
	andl	$1023, %edx
	movzbl	(%rdi), %r8d
	movzbl	(%rax,%rdx), %eax
	movzwl	(%rcx,%rdx,2), %edx
	movl	%esi, %ecx
	andl	$7, %ecx
	addq	%rax, %rsi
	salq	%cl, %rdx
	movq	%rsi, %rax
	orq	%rdx, %r8
	movl	%esi, %ecx
	movq	%rsi, (%r12)
	shrq	$3, %rax
	movq	%r8, (%rdi)
	movl	-68(%rbp), %edi
	andl	$7, %ecx
	addq	%r15, %rax
	movzbl	(%rax), %edx
	salq	%cl, %rdi
	orq	%rdi, %rdx
	movq	%rdx, (%rax)
	movl	%r11d, %eax
	shrw	$10, %ax
	movzwl	%ax, %eax
	addq	%rax, %rsi
	movq	%rsi, (%r12)
.L60:
	addq	$16, %r10
	cmpq	%r10, -80(%rbp)
	jne	.L61
.L49:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	leal	-2(%r9), %ecx
	leaq	-2(%rdx), %rdi
	bsrl	%ecx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdi
	leal	2(%rdi,%rcx,2), %ecx
.L77:
	movzwl	%cx, %ecx
.L76:
	leaq	kInsExtra(%rip), %rdi
	movl	(%rdi,%rcx,4), %r8d
	leaq	kInsBase(%rip), %rdi
	movl	(%rdi,%rcx,4), %ecx
	movl	%eax, %edi
	movl	%ecx, -64(%rbp)
	cmpl	$9, %eax
	ja	.L55
.L81:
	leal	-2(%rax), %edi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	leal	-6(%rax), %ecx
	subq	$6, %rdi
	bsrl	%ecx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdi
	leal	4(%rdi,%rcx,2), %edi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$24, -60(%rbp)
	movl	$2118, %ecx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L80:
	leal	-66(%r9), %ecx
	bsrl	%ecx, %ecx
	addl	$10, %ecx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rbx, %rdx
	jmp	.L58
.L63:
	movl	$2114, -64(%rbp)
	movl	$12, %r8d
	jmp	.L52
	.cfi_endproc
.LFE135:
	.size	StoreDataWithHuffmanCodes, .-StoreDataWithHuffmanCodes
	.p2align 4
	.type	StoreVarLenUint8, @function
StoreVarLenUint8:
.LFB106:
	.cfi_startproc
	movq	(%rsi), %rax
	movq	%rax, %r8
	leaq	1(%rax), %rcx
	shrq	$3, %r8
	addq	%rdx, %r8
	movzbl	(%r8), %r9d
	testq	%rdi, %rdi
	jne	.L84
	movq	%r9, (%r8)
	movq	%rcx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	bsrl	%edi, %r11d
	movl	%r11d, %r10d
	movq	%rcx, (%rsi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%eax, %ebx
	addq	$4, %rax
	andl	$7, %ebx
	btsq	%rbx, %r9
	movq	%r10, %rbx
	addq	%rax, %r10
	movq	%r9, (%r8)
	movq	%rcx, %r8
	andl	$7, %ecx
	shrq	$3, %r8
	salq	%cl, %rbx
	movq	%rax, (%rsi)
	addq	%rdx, %r8
	movq	%rbx, %rcx
	movzbl	(%r8), %r9d
	orq	%r9, %rcx
	movq	%rcx, (%r8)
	movq	%rax, %rcx
	movl	$1, %r8d
	shrq	$3, %rcx
	movq	%r10, (%rsi)
	addq	%rcx, %rdx
	movl	%r11d, %ecx
	salq	%cl, %r8
	movl	%eax, %ecx
	movzbl	(%rdx), %r9d
	subq	%r8, %rdi
	andl	$7, %ecx
	salq	%cl, %rdi
	orq	%r9, %rdi
	movq	%rdi, (%rdx)
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE106:
	.size	StoreVarLenUint8, .-StoreVarLenUint8
	.p2align 4
	.globl	BrotliStoreHuffmanTree
	.hidden	BrotliStoreHuffmanTree
	.type	BrotliStoreHuffmanTree, @function
BrotliStoreHuffmanTree:
.LFB112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-768(%rbp), %r15
	leaq	-1472(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r14, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movq	%r15, %r8
	subq	$1640, %rsp
	movq	%rdx, -1656(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%dx, -1488(%rbp)
	leaq	-1640(%rbp), %rdx
	movq	$0, -1640(%rbp)
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1504(%rbp)
	movaps	%xmm0, -1584(%rbp)
	movaps	%xmm0, -1568(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movaps	%xmm0, -1536(%rbp)
	call	BrotliWriteHuffmanTree
	movq	-1640(%rbp), %rdx
	movq	-1656(%rbp), %r10
	movq	%r14, %rax
	testq	%rdx, %rdx
	leaq	(%r14,%rdx), %rcx
	je	.L93
	.p2align 4,,10
	.p2align 3
.L92:
	movzbl	(%rax), %edx
	addq	$1, %rax
	addl	$1, -1584(%rbp,%rdx,4)
	cmpq	%rax, %rcx
	jne	.L92
.L93:
	xorl	%r13d, %r13d
	xorl	%r9d, %r9d
	movq	$0, -1656(%rbp)
	leaq	-1584(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L91:
	movl	(%rdi,%r9,4), %eax
	testl	%eax, %eax
	je	.L94
	testl	%r13d, %r13d
	je	.L125
	leaq	-1504(%rbp), %r13
	movq	%r10, %rcx
	movl	$5, %edx
	movl	$18, %esi
	movq	%r13, %r8
	call	BrotliCreateHuffmanTree
	leaq	-1632(%rbp), %rdx
	movl	$18, %esi
	movq	%r13, %rdi
	call	BrotliConvertBitDepthsToSymbols
	movl	$18, %r9d
	leaq	kStorageOrder.4753(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r9, %rcx
	subq	$1, %r9
	movzbl	(%rdx,%r9), %eax
	cmpb	$0, -1504(%rbp,%rax)
	jne	.L109
	testq	%r9, %r9
	jne	.L98
.L124:
	movl	$2, %r13d
.L97:
	movzbl	-1503(%rbp), %eax
	xorl	%edi, %edi
	orb	-1502(%rbp), %al
	jne	.L99
	xorl	%edi, %edi
	cmpb	$0, -1501(%rbp)
	sete	%dil
	addq	$2, %rdi
.L99:
	movq	(%r12), %rax
	movq	%rdi, %r11
	movq	%rax, %rdx
	movl	%eax, %ecx
	addq	$2, %rax
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%rax, (%r12)
	addq	%rbx, %rdx
	salq	%cl, %r11
	movzbl	(%rdx), %esi
	movq	%r11, %rcx
	orq	%rsi, %rcx
	movq	%rcx, (%rdx)
	cmpq	%r9, %rdi
	jnb	.L100
	leaq	kStorageOrder.4753(%rip), %rdx
	movl	%r13d, -1664(%rbp)
	leaq	kHuffmanBitLengthHuffmanCodeBitLengths.4755(%rip), %r11
	leaq	(%r9,%rdx), %rcx
	addq	%rdx, %rdi
	leaq	kHuffmanBitLengthHuffmanCodeSymbols.4754(%rip), %r10
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L101:
	movzbl	(%rdi), %edx
	movq	%rax, %rsi
	movl	%eax, %ecx
	addq	$1, %rdi
	shrq	$3, %rsi
	andl	$7, %ecx
	movzbl	-1504(%rbp,%rdx), %edx
	addq	%rbx, %rsi
	movzbl	(%rsi), %r9d
	movzbl	(%r11,%rdx), %r8d
	movzbl	(%r10,%rdx), %edx
	salq	%cl, %rdx
	addq	%r8, %rax
	orq	%rdx, %r9
	movq	%r9, (%rsi)
	cmpq	%rdi, %r13
	jne	.L101
	movq	%rax, (%r12)
	movl	-1664(%rbp), %r13d
.L100:
	cmpl	$1, %r13d
	jne	.L102
	movq	-1656(%rbp), %rdi
	movb	$0, -1504(%rbp,%rdi)
.L102:
	movq	-1640(%rbp), %r10
	xorl	%edi, %edi
	testq	%r10, %r10
	jne	.L103
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L126:
	cmpb	$17, %r8b
	jne	.L106
	movq	%rax, %rsi
	movzbl	(%r15,%rdi), %edx
	movl	%eax, %ecx
	addq	$3, %rax
	shrq	$3, %rsi
	andl	$7, %ecx
	addq	%rbx, %rsi
	salq	%cl, %rdx
	movzbl	(%rsi), %r8d
	orq	%r8, %rdx
	movq	%rdx, (%rsi)
	movq	%rax, (%r12)
.L106:
	addq	$1, %rdi
	cmpq	%rdi, %r10
	je	.L89
.L103:
	movzbl	(%r14,%rdi), %edx
	movq	%rax, %rsi
	movl	%eax, %ecx
	shrq	$3, %rsi
	andl	$7, %ecx
	movzbl	-1504(%rbp,%rdx), %r9d
	addq	%rbx, %rsi
	movq	%rdx, %r8
	movzwl	-1632(%rbp,%rdx,2), %edx
	movzbl	(%rsi), %r11d
	salq	%cl, %rdx
	addq	%r9, %rax
	orq	%r11, %rdx
	movq	%rax, (%r12)
	movq	%rdx, (%rsi)
	cmpb	$16, %r8b
	jne	.L126
	movq	%rax, %rsi
	movzbl	(%r15,%rdi), %edx
	movl	%eax, %ecx
	addq	$1, %rdi
	shrq	$3, %rsi
	andl	$7, %ecx
	addq	$2, %rax
	addq	%rbx, %rsi
	salq	%cl, %rdx
	movzbl	(%rsi), %r8d
	orq	%r8, %rdx
	movq	%rdx, (%rsi)
	movq	%rax, (%r12)
	cmpq	%rdi, %r10
	jne	.L103
.L89:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$1640, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r9, -1656(%rbp)
	movl	$1, %r13d
.L94:
	addq	$1, %r9
	cmpq	$18, %r9
	jne	.L91
	leaq	-1504(%rbp), %r8
	movq	%r10, %rcx
	movl	$5, %edx
	movl	$18, %esi
	movq	%r9, -1672(%rbp)
	movq	%r8, -1664(%rbp)
	call	BrotliCreateHuffmanTree
	movq	-1664(%rbp), %r8
	movl	$18, %esi
	leaq	-1632(%rbp), %rdx
	movq	%r8, %rdi
	call	BrotliConvertBitDepthsToSymbols
	movq	-1672(%rbp), %r9
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rcx, %r9
	jmp	.L124
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE112:
	.size	BrotliStoreHuffmanTree, .-BrotliStoreHuffmanTree
	.p2align 4
	.type	BuildAndStoreHuffmanTree, @function
BuildAndStoreHuffmanTree:
.LFB113:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%r9, %r10
	subq	$1, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %r11
	movq	24(%rbp), %r9
	movq	%rcx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rsi, %rsi
	jne	.L134
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%rax, -96(%rbp,%r12,8)
.L132:
	addq	$1, %r12
.L130:
	addq	$1, %rax
	cmpq	%rax, %r13
	je	.L151
.L134:
	movl	(%r15,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.L130
	cmpq	$3, %r12
	jbe	.L152
	cmpq	$4, %r12
	je	.L132
	testq	%rdx, %rdx
	jne	.L136
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	memset@PLT
	movq	-104(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r13, %rsi
	movl	$15, %edx
	movq	%r15, %rdi
	call	BrotliCreateHuffmanTree
	movq	-112(%rbp), %r10
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r10, %rdx
	call	BrotliConvertBitDepthsToSymbols
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-104(%rbp), %rdx
	movq	%r9, %r8
	movq	%r11, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	BrotliStoreHuffmanTree
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L151:
	testq	%rdx, %rdx
	je	.L146
.L136:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L139:
	addq	$1, %r14
	shrq	%rdx
	jne	.L139
.L138:
	cmpq	$1, %r12
	jbe	.L153
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	memset@PLT
	movq	-104(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r13, %rsi
	movl	$15, %edx
	movq	%r15, %rdi
	call	BrotliCreateHuffmanTree
	movq	-112(%rbp), %r10
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r10, %rdx
	call	BrotliConvertBitDepthsToSymbols
	cmpq	$4, %r12
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %r9
	ja	.L142
	leaq	-96(%rbp), %rsi
	movq	%r11, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	StoreSimpleHuffmanTree
.L128:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	-96(%rbp), %rdx
	addq	%rdx, %rbx
	leaq	(%r10,%rdx,2), %r10
.L137:
	movq	(%r11), %rax
	movq	%rax, %rcx
	movl	%eax, %edi
	addq	$4, %rax
	shrq	$3, %rcx
	andl	$7, %edi
	movq	%rax, (%r11)
	addq	%rax, %r14
	addq	%r9, %rcx
	movzbl	(%rcx), %esi
	btsq	%rdi, %rsi
	movq	%rsi, (%rcx)
	movq	%rax, %rcx
	shrq	$3, %rcx
	movq	%r14, (%r11)
	addq	%rcx, %r9
	movl	%eax, %ecx
	xorl	%eax, %eax
	movzbl	(%r9), %esi
	andl	$7, %ecx
	salq	%cl, %rdx
	orq	%rsi, %rdx
	movq	%rdx, (%r9)
	movb	$0, (%rbx)
	movw	%ax, (%r10)
	jmp	.L128
.L146:
	xorl	%r14d, %r14d
	jmp	.L138
.L129:
	xorl	%r14d, %r14d
	testq	%rdx, %rdx
	jne	.L136
	jmp	.L137
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE113:
	.size	BuildAndStoreHuffmanTree, .-BuildAndStoreHuffmanTree
	.p2align 4
	.type	EncodeContextMap, @function
EncodeContextMap:
.LFB120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%r9, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$2008, %rsp
	movq	%rdi, -2000(%rbp)
	movq	16(%rbp), %r15
	leaq	-1(%rcx), %rdi
	movq	%rcx, -2008(%rbp)
	movq	%r8, -2016(%rbp)
	movq	%r15, %rdx
	movq	%r9, -1992(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	StoreVarLenUint8
	cmpq	$1, %r13
	je	.L155
	testq	%rbx, %rbx
	jne	.L242
	leaq	-1424(%rbp), %r10
	movq	%rbx, %rax
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$136, %ecx
	movq	%r10, %rdi
	xorl	%esi, %esi
	movq	$0, -1984(%rbp)
	rep stosq
	leaq	-336(%rbp), %r8
	movl	$0, -1976(%rbp)
.L190:
	movq	-1992(%rbp), %rbx
	movq	-1984(%rbp), %r11
	movq	(%rbx), %rax
	movq	%rax, %rdx
	movl	%eax, %ecx
	shrq	$3, %rdx
	andl	$7, %ecx
	addq	%r15, %rdx
	salq	%cl, %r11
	movzbl	(%rdx), %edi
	movq	%r11, %rcx
	orq	%rdi, %rcx
	movq	%rcx, (%rdx)
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	testl	%esi, %esi
	jne	.L243
.L192:
	movq	-1992(%rbp), %r14
	pushq	%r15
	leaq	-1968(%rbp), %r9
	movq	%r10, %rdi
	movq	-2008(%rbp), %rsi
	movq	-2016(%rbp), %rcx
	pushq	%r14
	movq	%rsi, %rdx
	call	BuildAndStoreHuffmanTree
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L193
	movq	%r12, -1984(%rbp)
	movq	%r12, %r8
	leaq	(%r12,%r13,4), %r13
	movq	(%r14), %rax
	movl	-1976(%rbp), %r12d
	.p2align 4,,10
	.p2align 3
.L195:
	movl	(%r8), %edi
	movq	%rax, %rsi
	movl	%eax, %ecx
	shrq	$3, %rsi
	andl	$7, %ecx
	movl	%edi, %r9d
	addq	%r15, %rsi
	andl	$511, %r9d
	movzbl	(%rsi), %ebx
	movl	%r9d, %r10d
	movzbl	-336(%rbp,%r10), %r11d
	movzwl	-1968(%rbp,%r10,2), %edx
	addq	%r11, %rax
	salq	%cl, %rdx
	orq	%rdx, %rbx
	movq	%rax, %rdx
	movl	%eax, %ecx
	movq	%rax, (%r14)
	shrq	$3, %rdx
	movq	%rbx, (%rsi)
	andl	$7, %ecx
	addq	%r15, %rdx
	testl	%r9d, %r9d
	je	.L194
	cmpl	%r9d, %r12d
	jb	.L194
	movzbl	(%rdx), %esi
	shrl	$9, %edi
	addq	%r10, %rax
	salq	%cl, %rdi
	movq	%rax, (%r14)
	movl	%eax, %ecx
	orq	%rdi, %rsi
	andl	$7, %ecx
	movq	%rsi, (%rdx)
	movq	%rax, %rdx
	shrq	$3, %rdx
	addq	%r15, %rdx
.L194:
	addq	$4, %r8
	cmpq	%r8, %r13
	jne	.L195
	movq	-1984(%rbp), %r12
.L196:
	movzbl	(%rdx), %esi
	movq	-1992(%rbp), %rdi
	addq	$1, %rax
	btsq	%rcx, %rsi
	movq	%rax, (%rdi)
	movq	-2000(%rbp), %rdi
	movq	%rsi, (%rdx)
	movq	%r12, %rsi
	call	BrotliFree
.L155:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	leaq	0(,%rbx,4), %r10
	movq	-2000(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r10, -1976(%rbp)
	call	BrotliAllocate
	cmpq	$1, %rbx
	movl	(%r14), %r13d
	movq	-1976(%rbp), %r10
	movq	%rax, %r12
	jbe	.L159
	leaq	-2(%rbx), %rax
	leaq	-1(%rbx), %rcx
	cmpq	$3, %rax
	jbe	.L200
	movq	%rcx, %rdx
	movd	%r13d, %xmm6
	movdqa	.LC1(%rip), %xmm3
	movq	%r14, %rax
	shrq	$2, %rdx
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rdx
	addq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L161:
	movdqu	4(%rax), %xmm2
	movdqa	%xmm0, %xmm4
	addq	$16, %rax
	psubd	%xmm3, %xmm4
	movdqa	%xmm2, %xmm1
	psubd	%xmm3, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	cmpq	%rax, %rdx
	jne	.L161
	movdqa	%xmm0, %xmm2
	movdqa	%xmm0, %xmm4
	movq	%rcx, %rdx
	psrldq	$8, %xmm2
	psubd	%xmm3, %xmm4
	andq	$-4, %rdx
	movdqa	%xmm2, %xmm1
	leaq	1(%rdx), %rax
	psubd	%xmm3, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	por	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	movdqa	%xmm0, %xmm5
	psrldq	$4, %xmm2
	psubd	%xmm3, %xmm5
	movdqa	%xmm2, %xmm1
	psubd	%xmm3, %xmm1
	pcmpgtd	%xmm5, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movd	%xmm1, %r13d
	cmpq	%rcx, %rdx
	je	.L159
.L160:
	movl	(%r14,%rax,4), %edx
	cmpl	%edx, %r13d
	cmovb	%edx, %r13d
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L159
	movl	(%r14,%rdx,4), %edx
	cmpl	%edx, %r13d
	cmovb	%edx, %r13d
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L159
	movl	(%r14,%rdx,4), %edx
	cmpl	%edx, %r13d
	cmovb	%edx, %r13d
	addq	$3, %rax
	cmpq	%rax, %rbx
	jbe	.L159
	movl	(%r14,%rax,4), %eax
	cmpl	%eax, %r13d
	cmovb	%eax, %r13d
.L159:
	movl	%r13d, %edx
	leaq	1(%rdx), %rcx
	cmpl	$14, %r13d
	jbe	.L201
	movq	%rcx, %rsi
	leaq	-336(%rbp), %r8
	movdqa	.LC0(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm11
	shrq	$4, %rsi
	movq	%r8, %rax
	movdqa	.LC3(%rip), %xmm10
	movdqa	.LC4(%rip), %xmm9
	salq	$4, %rsi
	movdqa	.LC6(%rip), %xmm7
	movdqa	.LC5(%rip), %xmm8
	movdqa	.LC7(%rip), %xmm6
	movdqa	.LC8(%rip), %xmm5
	addq	%r8, %rsi
	movdqa	.LC9(%rip), %xmm4
	movdqa	.LC10(%rip), %xmm3
	.p2align 4,,10
	.p2align 3
.L164:
	movdqa	%xmm2, %xmm12
	addq	$16, %rax
	paddq	%xmm11, %xmm2
	movdqa	%xmm12, %xmm0
	movdqa	%xmm12, %xmm1
	movdqa	%xmm12, %xmm13
	paddq	%xmm10, %xmm0
	paddq	%xmm8, %xmm13
	shufps	$136, %xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	movdqa	%xmm12, %xmm1
	paddq	%xmm9, %xmm1
	shufps	$136, %xmm13, %xmm1
	movdqa	%xmm0, %xmm13
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm13
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm13, %xmm1
	punpcklwd	%xmm13, %xmm0
	movdqa	%xmm12, %xmm13
	punpcklwd	%xmm1, %xmm0
	movdqa	%xmm12, %xmm1
	paddq	%xmm6, %xmm13
	paddq	%xmm7, %xmm1
	pand	%xmm3, %xmm0
	shufps	$136, %xmm13, %xmm1
	movdqa	%xmm12, %xmm13
	paddq	%xmm4, %xmm12
	paddq	%xmm5, %xmm13
	movdqa	%xmm13, %xmm14
	movdqa	%xmm1, %xmm13
	shufps	$136, %xmm12, %xmm14
	punpcklwd	%xmm14, %xmm1
	punpckhwd	%xmm14, %xmm13
	movdqa	%xmm1, %xmm12
	punpcklwd	%xmm13, %xmm1
	punpckhwd	%xmm13, %xmm12
	punpcklwd	%xmm12, %xmm1
	pand	%xmm3, %xmm1
	packuswb	%xmm1, %xmm0
	movaps	%xmm0, -16(%rax)
	cmpq	%rax, %rsi
	jne	.L164
	movq	%rcx, %rax
	andq	$-16, %rax
	cmpq	%rcx, %rax
	je	.L165
.L163:
	leaq	1(%rax), %rcx
	movb	%al, -336(%rbp,%rax)
	cmpq	%rdx, %rcx
	ja	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	2(%rax), %rcx
	cmpq	%rdx, %rcx
	ja	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	3(%rax), %rcx
	cmpq	%rdx, %rcx
	ja	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	5(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	6(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	7(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	8(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	9(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	10(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	11(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	12(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	movb	%cl, -336(%rbp,%rcx)
	leaq	13(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L165
	addq	$14, %rax
	movb	%cl, -336(%rbp,%rcx)
	cmpq	%rax, %rdx
	jb	.L165
	movb	%al, -336(%rbp,%rax)
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r14, %r9
	leal	1(%r13), %r11d
	movzbl	-336(%rbp), %ecx
	movq	%r12, %r14
	leaq	(%r9,%r10), %rax
	movl	%r11d, -1984(%rbp)
	movq	%r12, %r13
	leaq	-335(%rbp), %rdi
	movq	%rax, -1976(%rbp)
	movq	%r12, -2024(%rbp)
	movq	%r9, %r12
	movq	%r14, -2032(%rbp)
	movq	%r11, %r14
	movq	%rbx, -2040(%rbp)
	movl	%ecx, %ebx
	movq	%r15, -2048(%rbp)
	movq	%r13, %r15
	movq	%r8, %r13
	.p2align 4,,10
	.p2align 3
.L171:
	movl	-1984(%rbp), %esi
	movzbl	(%r12), %eax
	testl	%esi, %esi
	je	.L166
	xorl	%edx, %edx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L246:
	addq	$1, %rdx
	cmpq	%rdx, %r14
	je	.L245
.L168:
	movzbl	0(%r13,%rdx), %ebx
	cmpb	%bl, %al
	jne	.L246
	movl	%edx, (%r15)
	testq	%rdx, %rdx
	je	.L170
.L198:
	movq	%r13, %rsi
	call	memmove@PLT
	movq	%rax, %rdi
.L170:
	movb	%bl, -336(%rbp)
	addq	$4, %r12
	addq	$4, %r15
	cmpq	%r12, -1976(%rbp)
	jne	.L171
	movq	-2024(%rbp), %r12
	movq	%r13, %r8
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	-2032(%rbp), %r14
	movq	-2040(%rbp), %rbx
	movq	-2048(%rbp), %r15
	movl	(%r12), %ecx
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%ecx, %edx
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L248:
	movl	(%r12,%rax,4), %edx
.L174:
	testl	%edx, %edx
	je	.L247
	addq	$1, %rax
	cmpq	%rax, %rbx
	ja	.L248
.L197:
	testl	%esi, %esi
	je	.L202
	.p2align 4,,10
	.p2align 3
.L253:
	bsrl	%esi, %eax
	movl	$6, %edx
	movl	$2, %r11d
	xorl	$31, %eax
	movl	%eax, %esi
	xorl	$31, %esi
	cmpl	$6, %esi
	cmovbe	%esi, %edx
	movl	%edx, %edi
	movl	%edx, -1976(%rbp)
	movl	$1, %edx
	movl	%edi, %ecx
	sall	%cl, %edx
	sall	%cl, %r11d
	leal	-1(%rdx), %r10d
	sall	$9, %r10d
	addl	%edi, %r10d
	cmpl	$31, %eax
	setne	%al
	movzbl	%al, %eax
	movq	%rax, -1984(%rbp)
	movl	%edi, %eax
	addq	%rax, -2008(%rbp)
.L179:
	movl	$1, %r9d
	movl	%esi, -2024(%rbp)
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	movl	-1976(%rbp), %esi
	subl	%r11d, %r9d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L250:
	addl	%esi, %ecx
	movq	%rdx, %rdi
	movl	%ecx, (%r12,%r13,4)
	addq	$1, %r13
.L181:
	cmpq	%rdi, %rbx
	jbe	.L249
.L189:
	movl	(%r12,%rdi,4), %ecx
	leaq	1(%rdi), %rdx
	testl	%ecx, %ecx
	jne	.L250
	movl	$1, %eax
	cmpq	%rdx, %rbx
	ja	.L182
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L185:
	addq	$1, %rdx
	addl	$1, %eax
	cmpq	%rdx, %rbx
	je	.L184
.L182:
	movl	(%r12,%rdx,4), %ecx
	testl	%ecx, %ecx
	je	.L185
.L184:
	movl	%eax, %edx
	addq	%rdx, %rdi
	testl	%eax, %eax
	jne	.L188
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L187:
	movl	%r10d, (%r12,%r13,4)
	addq	$1, %r13
	addl	%r9d, %eax
	je	.L181
.L188:
	cmpl	%r11d, %eax
	jnb	.L187
	bsrl	%eax, %ecx
	movl	$1, %edx
	sall	%cl, %edx
	subl	%edx, %eax
	sall	$9, %eax
	addl	%eax, %ecx
.L183:
	movl	%ecx, (%r12,%r13,4)
	addq	$1, %r13
	cmpq	%rdi, %rbx
	ja	.L189
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	-1424(%rbp), %r10
	movl	$136, %ecx
	xorl	%eax, %eax
	movl	-2024(%rbp), %esi
	movq	%r10, %rdi
	rep stosq
	testq	%r13, %r13
	je	.L190
	leaq	(%r12,%r13,4), %rdx
	.p2align 4,,10
	.p2align 3
.L191:
	movl	(%r14), %eax
	addq	$4, %r14
	andl	$511, %eax
	addl	$1, -1424(%rbp,%rax,4)
	cmpq	%r14, %rdx
	jne	.L191
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rcx, %rsi
	movl	-1976(%rbp), %edx
	andl	$7, %ecx
	addq	$5, %rax
	shrq	$3, %rsi
	movq	%rax, (%rbx)
	addq	%r15, %rsi
	subl	$1, %edx
	movzbl	(%rsi), %edi
	salq	%cl, %rdx
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L247:
	cmpq	%rax, %rbx
	ja	.L175
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L178:
	addq	$1, %rax
	addl	$1, %edx
	cmpq	%rax, %rbx
	je	.L252
.L175:
	movl	(%r12,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.L178
	cmpl	%edx, %esi
	cmovb	%edx, %esi
	cmpq	%rax, %rbx
	ja	.L172
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L252:
	cmpl	%edx, %esi
	cmovb	%edx, %esi
	testl	%esi, %esi
	jne	.L253
.L202:
	movl	$0, -1976(%rbp)
	xorl	%r10d, %r10d
	movl	$2, %r11d
	movq	$0, -1984(%rbp)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L245:
	movzbl	-336(%rbp,%r14), %ebx
	movl	%r14d, (%r15)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$0, (%r15)
	jmp	.L170
.L201:
	xorl	%eax, %eax
	leaq	-336(%rbp), %r8
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%rdx, %rdi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-1992(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movl	%eax, %ecx
	shrq	$3, %rdx
	andl	$7, %ecx
	addq	%r15, %rdx
	jmp	.L196
.L200:
	movl	$1, %eax
	jmp	.L160
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE120:
	.size	EncodeContextMap, .-EncodeContextMap
	.p2align 4
	.type	BuildAndStoreBlockSplitCode, @function
BuildAndStoreBlockSplitCode:
.LFB122:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1200(%rbp), %r15
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1240, %rsp
	movq	24(%rbp), %rax
	movq	%rcx, -1256(%rbp)
	movq	%r8, -1264(%rbp)
	movq	16(%rbp), %r13
	movq	%r9, -1248(%rbp)
	movq	%rax, -1240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	2(%rcx), %rax
	movl	$1032, %ecx
	movq	%rax, -1272(%rbp)
	leaq	0(,%rax,4), %rdx
	leaq	-1088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1280(%rbp)
	call	__memset_chk@PLT
	movl	$13, %ecx
	xorl	%eax, %eax
	movq	%r15, %rdi
	rep stosq
	testq	%rbx, %rbx
	je	.L255
	movl	$1, %r9d
	xorl	%r8d, %r8d
	leaq	kBlockLengthPrefixCode(%rip), %r10
	.p2align 4,,10
	.p2align 3
.L264:
	movzbl	(%r12,%r8), %edx
	movq	%r9, %rdi
	movl	$1, %eax
	leaq	1(%rdi), %rsi
	movzbl	%dl, %r9d
	cmpq	%rsi, %r9
	je	.L256
	cmpq	%rcx, %r9
	leaq	2(%r9), %rax
	movl	$0, %edx
	cmove	%rdx, %rax
.L256:
	testq	%r8, %r8
	je	.L257
	addl	$1, -1088(%rbp,%rax,4)
.L257:
	movl	(%r14,%r8,4), %esi
	cmpl	$176, %esi
	jbe	.L258
	cmpl	$753, %esi
	sbbl	%edx, %edx
	andl	$-6, %edx
	addl	$20, %edx
.L259:
	leal	1(%rdx), %ecx
	movq	%rcx, %rax
	leaq	(%r10,%rcx,8), %rcx
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L302:
	addl	$1, %eax
.L261:
	movl	%edx, %r11d
	movl	%eax, %edx
	cmpl	(%rcx), %esi
	jb	.L301
	addq	$8, %rcx
	cmpl	$25, %eax
	jne	.L302
.L262:
	addq	$1, %r8
	addl	$1, -1200(%rbp,%rdx,4)
	movq	%rdi, %rcx
	cmpq	%r8, %rbx
	jne	.L264
.L255:
	movq	-1256(%rbp), %rbx
	movq	-1240(%rbp), %rdx
	movq	%r13, %rsi
	leaq	-1(%rbx), %rdi
	call	StoreVarLenUint8
	cmpq	$1, %rbx
	ja	.L303
.L254:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	cmpl	$41, %esi
	sbbl	%edx, %edx
	notl	%edx
	andl	$7, %edx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L301:
	movl	%r11d, %edx
	jmp	.L262
.L303:
	movq	-1248(%rbp), %rbx
	pushq	-1240(%rbp)
	movq	-1272(%rbp), %rsi
	pushq	%r13
	movq	-1280(%rbp), %rdi
	movq	-1264(%rbp), %rcx
	leaq	274(%rbx), %r9
	leaq	16(%rbx), %r8
	movq	%rsi, %rdx
	call	BuildAndStoreHuffmanTree
	pxor	%xmm0, %xmm0
	popq	%rdi
	xorl	%eax, %eax
	leaq	816(%rbx), %r9
	leaq	790(%rbx), %r10
	popq	%r8
	xorl	%ebx, %ebx
	movaps	%xmm0, -1232(%rbp)
	movaps	%xmm0, -1216(%rbp)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rax, -1232(%rbp,%rbx,8)
.L268:
	addq	$1, %rbx
.L266:
	addq	$1, %rax
	cmpq	$26, %rax
	je	.L305
.L270:
	movl	(%r15,%rax,4), %esi
	testl	%esi, %esi
	je	.L266
	cmpq	$3, %rbx
	jbe	.L306
	cmpq	$4, %rbx
	je	.L268
	pxor	%xmm0, %xmm0
	movq	%r10, %r8
	movl	$15, %edx
	movq	%r15, %rdi
	movq	-1248(%rbp), %rax
	movq	-1264(%rbp), %rcx
	movl	$26, %esi
	movq	%r9, -1272(%rbp)
	movq	%r10, -1256(%rbp)
	movups	%xmm0, 790(%rax)
	xorl	%eax, %eax
	movw	%ax, 24(%r10)
	movq	$0, 16(%r10)
	call	BrotliCreateHuffmanTree
	movq	-1256(%rbp), %r10
	movl	$26, %esi
	movq	-1272(%rbp), %r9
	movq	%r10, %rdi
	movq	%r9, %rdx
	call	BrotliConvertBitDepthsToSymbols
	movq	-1256(%rbp), %r10
.L273:
	movq	-1240(%rbp), %r8
	movq	%r13, %rcx
	movl	$26, %esi
	movq	%r10, %rdi
	movq	-1264(%rbp), %rdx
	call	BrotliStoreHuffmanTree
	jmp	.L272
.L305:
	cmpq	$1, %rbx
	ja	.L271
	movq	0(%r13), %rax
	movq	-1240(%rbp), %rbx
	movq	%rax, %rdx
	movl	%eax, %esi
	shrq	$3, %rdx
	andl	$7, %esi
	addq	%rbx, %rdx
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	%rcx, (%rdx)
	leaq	4(%rax), %rcx
	movq	-1232(%rbp), %rsi
	addq	$9, %rax
	movq	%rcx, %rdx
	movq	%rcx, 0(%r13)
	andl	$7, %ecx
	shrq	$3, %rdx
	movq	%rax, 0(%r13)
	movq	-1248(%rbp), %rax
	addq	%rbx, %rdx
	movq	%rsi, %rbx
	movzbl	(%rdx), %edi
	salq	%cl, %rbx
	movq	%rbx, %rcx
	orq	%rdi, %rcx
	movq	%rcx, (%rdx)
	xorl	%ecx, %ecx
	movb	$0, 790(%rax,%rsi)
	movw	%cx, (%r9,%rsi,2)
.L272:
	movzbl	(%r12), %eax
	movl	(%r14), %esi
	movq	%rax, %xmm0
	movq	-1248(%rbp), %rax
	movhps	(%rax), %xmm0
	movups	%xmm0, (%rax)
	cmpl	$176, %esi
	jbe	.L275
	cmpl	$753, %esi
	sbbl	%ecx, %ecx
	andl	$-6, %ecx
	addl	$20, %ecx
.L276:
	leal	1(%rcx), %edx
	leaq	kBlockLengthPrefixCode(%rip), %r10
	movq	%rdx, %rax
	leaq	(%r10,%rdx,8), %rdx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L308:
	addl	$1, %eax
.L278:
	movl	%ecx, %edi
	movl	%eax, %ecx
	cmpl	(%rdx), %esi
	jb	.L307
	addq	$8, %rdx
	cmpl	$25, %eax
	jne	.L308
.L279:
	movq	0(%r13), %rdx
	movq	-1248(%rbp), %rbx
	movl	%ecx, %eax
	movq	-1240(%rbp), %r15
	subl	(%r10,%rax,8), %esi
	movq	%rdx, %r8
	movl	%edx, %ecx
	movzwl	816(%rbx,%rax,2), %edi
	movzbl	790(%rbx,%rax), %r9d
	shrq	$3, %r8
	andl	$7, %ecx
	movl	4(%r10,%rax,8), %eax
	addq	%r15, %r8
	salq	%cl, %rdi
	addq	%r9, %rdx
	movzbl	(%r8), %r11d
	movl	%edx, %ecx
	movq	%rdx, 0(%r13)
	andl	$7, %ecx
	orq	%r11, %rdi
	salq	%cl, %rsi
	movq	%rdi, (%r8)
	movq	%rdx, %rdi
	addq	%rax, %rdx
	shrq	$3, %rdi
	movq	%rdx, 0(%r13)
	addq	%r15, %rdi
	movzbl	(%rdi), %r8d
	orq	%r8, %rsi
	movq	%rsi, (%rdi)
	jmp	.L254
.L271:
	movq	-1248(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r10, %r8
	movq	-1264(%rbp), %rcx
	movl	$26, %esi
	movq	%r15, %rdi
	movq	%r9, -1272(%rbp)
	movups	%xmm0, 790(%rax)
	movw	%dx, 24(%r10)
	movl	$15, %edx
	movq	$0, 16(%r10)
	movq	%r10, -1256(%rbp)
	call	BrotliCreateHuffmanTree
	movq	-1256(%rbp), %r10
	movl	$26, %esi
	movq	-1272(%rbp), %r9
	movq	%r10, %rdi
	movq	%r9, %rdx
	call	BrotliConvertBitDepthsToSymbols
	cmpq	$4, %rbx
	movq	-1256(%rbp), %r10
	ja	.L273
	movq	-1240(%rbp), %r9
	movq	%r13, %r8
	movq	%rbx, %rdx
	movq	%r10, %rdi
	leaq	-1232(%rbp), %rsi
	movl	$5, %ecx
	call	StoreSimpleHuffmanTree
	jmp	.L272
.L307:
	movl	%edi, %ecx
	jmp	.L279
.L275:
	cmpl	$41, %esi
	sbbl	%ecx, %ecx
	notl	%ecx
	andl	$7, %ecx
	jmp	.L276
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE122:
	.size	BuildAndStoreBlockSplitCode, .-BuildAndStoreBlockSplitCode
	.p2align 4
	.globl	BrotliBuildAndStoreHuffmanTreeFast
	.hidden	BrotliBuildAndStoreHuffmanTreeFast
	.type	BrotliBuildAndStoreHuffmanTreeFast, @function
BrotliBuildAndStoreHuffmanTreeFast:
.LFB115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%rdi, -208(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rbx, -120(%rbp)
	movq	24(%rbp), %rbx
	movq	%rcx, -184(%rbp)
	movq	%r8, -136(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rbx, -112(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rdx, %rdx
	je	.L310
	xorl	%ecx, %ecx
	xorl	%esi, %esi
.L314:
	movq	-152(%rbp), %rbx
	movl	(%rbx,%rcx,4), %edx
	testl	%edx, %edx
	je	.L311
.L419:
	cmpq	$3, %rsi
	ja	.L312
	movq	%rcx, -96(%rbp,%rsi,8)
.L312:
	leaq	1(%rsi), %rdi
	addq	$1, %rcx
	subq	%rdx, %rax
	je	.L313
	movq	-152(%rbp), %rbx
	movq	%rdi, %rsi
	movl	(%rbx,%rcx,4), %edx
	testl	%edx, %edx
	jne	.L419
.L311:
	addq	$1, %rcx
	jmp	.L314
.L313:
	movq	%rdi, -176(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rcx, -160(%rbp)
	cmpq	$1, %rdi
	jbe	.L420
	movq	-136(%rbp), %rdi
	movq	%rcx, %rbx
	movq	%rcx, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	-208(%rbp), %rdi
	leaq	1(%rbx,%rbx), %rsi
	salq	$3, %rsi
	call	BrotliAllocate
	movl	$1, -164(%rbp)
	movq	%rax, %r14
.L345:
	movq	-160(%rbp), %r15
	movl	-164(%rbp), %esi
	movq	%r14, %rdx
	movq	-152(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L320:
	subq	$1, %r15
	movl	(%rcx,%r15,4), %eax
	testl	%eax, %eax
	je	.L317
	cmpl	%esi, %eax
	jb	.L318
	movl	$-1, %r9d
	movl	%eax, (%rdx)
	movw	%r9w, 4(%rdx)
	movw	%r15w, 6(%rdx)
.L319:
	addq	$8, %rdx
.L317:
	testq	%r15, %r15
	jne	.L320
	movq	%rdx, %rax
	movq	%rdx, -144(%rbp)
	subq	%r14, %rax
	sarq	$3, %rax
	movl	%eax, %ebx
	movslq	%eax, %r13
	movq	%rax, -128(%rbp)
	addl	$1, %ebx
	movl	%ebx, -168(%rbp)
	cmpq	$12, %r13
	jbe	.L421
	xorl	%eax, %eax
	cmpq	$57, %r13
	movq	%r15, -200(%rbp)
	setb	%al
	addl	%eax, %eax
	cltq
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	gaps.4286(%rip), %rax
	movq	(%rax,%r15,8), %rdi
	cmpq	%rdi, %r13
	jbe	.L337
	movq	%rdi, %rsi
	leaq	(%r14,%rdi,8), %r10
	movq	%rdi, %r11
	negq	%rsi
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%r10, -104(%rbp)
	movl	(%r10), %r9d
	leaq	(%r10,%rsi), %rax
	movq	%r10, %rcx
	movzwl	4(%r10), %r12d
	movzwl	6(%r10), %ebx
	movq	%r11, %rdx
	.p2align 4,,10
	.p2align 3
.L333:
	subq	%rdi, %rdx
	movq	%rax, %r8
	cmpl	(%rax), %r9d
	jnb	.L422
	movq	(%rax), %r10
	addq	%rsi, %rax
	movq	%r10, (%rcx)
	addq	%rsi, %rcx
	cmpq	%rdx, %rdi
	jbe	.L333
	movq	-104(%rbp), %r10
.L334:
	addq	$1, %r11
	movl	%r9d, (%r8)
	addq	$8, %r10
	movw	%r12w, 4(%r8)
	movw	%bx, 6(%r8)
	cmpq	%r11, %r13
	jne	.L336
.L337:
	addq	$1, %r15
	cmpl	$5, %r15d
	jle	.L332
	movq	-200(%rbp), %r15
.L329:
	movq	-144(%rbp), %rbx
	cmpl	$1, -128(%rbp)
	movq	$-1, (%rbx)
	leaq	16(%rbx), %rax
	movq	$-1, 8(%rbx)
	jle	.L324
	movq	-128(%rbp), %rbx
	movl	-168(%rbp), %r8d
	xorl	%ecx, %ecx
	leal	-2(%rbx), %edx
	movq	-144(%rbp), %rbx
	leaq	24(%rbx,%rdx,8), %r11
	.p2align 4,,10
	.p2align 3
.L344:
	movslq	%ecx, %rdx
	movslq	%r8d, %rsi
	leaq	0(,%rdx,8), %r10
	movl	(%r14,%rsi,8), %r9d
	movl	(%r14,%rdx,8), %edx
	leaq	0(,%rsi,8), %rdi
	cmpl	%r9d, %edx
	ja	.L340
	movl	8(%r14,%r10), %edi
	movl	%r9d, %esi
	leal	1(%rcx), %ebx
	movl	%ecx, %r9d
	movl	%r8d, %r10d
.L341:
	cmpl	%esi, %edi
	ja	.L342
	addl	%edi, %edx
	movw	%r9w, -4(%rax)
	movl	$-1, %esi
	addq	$8, %rax
	movl	%edx, -16(%rax)
	movl	$-1, %edi
	leal	1(%rbx), %ecx
	movw	%bx, -10(%rax)
	movl	$-1, -8(%rax)
	movw	%si, -4(%rax)
	movw	%di, -2(%rax)
	cmpq	%rax, %r11
	je	.L324
	movl	%r10d, %r8d
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L422:
	movq	-104(%rbp), %r10
	movq	%rcx, %r8
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L342:
	addl	%esi, %edx
	movw	%r9w, -4(%rax)
	movl	$-1, %ecx
	addq	$8, %rax
	movl	%edx, -16(%rax)
	movl	$-1, %edx
	leal	1(%r10), %r8d
	movw	%r10w, -10(%rax)
	movl	$-1, -8(%rax)
	movw	%dx, -4(%rax)
	movw	%cx, -2(%rax)
	cmpq	%r11, %rax
	je	.L324
	movl	%ebx, %ecx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L340:
	movl	8(%r14,%rdi), %esi
	leal	1(%r8), %r10d
	movl	%edx, %edi
	movl	%ecx, %ebx
	movl	%r9d, %edx
	movl	%r8d, %r9d
	jmp	.L341
.L324:
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rdx
	movl	$14, %ecx
	movq	%r14, %rsi
	leal	-1(%rax,%rax), %edi
	call	BrotliSetDepth
	testl	%eax, %eax
	jne	.L423
	sall	-164(%rbp)
	jmp	.L345
.L318:
	movl	$-1, %r8d
	movl	%esi, (%rdx)
	movw	%r8w, 4(%rdx)
	movw	%r15w, 6(%rdx)
	jmp	.L319
.L421:
	movq	%r14, %rdi
	leaq	-1(%r13), %r11
	xorl	%ecx, %ecx
	cmpq	$1, %r13
	ja	.L328
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	-8(%rax), %rdx
	cmpl	-8(%rax), %esi
	jnb	.L374
	movq	-8(%rax), %r10
	movq	%r10, (%rax)
	cmpq	$1, %rcx
	je	.L387
	leaq	-16(%rax), %r10
	cmpl	-16(%rax), %esi
	jnb	.L325
	movq	-16(%rax), %rdx
	movq	%rdx, -8(%rax)
	cmpq	$2, %rcx
	je	.L387
	leaq	-24(%rax), %rdx
	cmpl	-24(%rax), %esi
	jnb	.L386
	movq	-24(%rax), %r10
	movq	%r10, -16(%rax)
	cmpq	$3, %rcx
	je	.L387
	leaq	-32(%rax), %r10
	cmpl	-32(%rax), %esi
	jnb	.L325
	movq	-32(%rax), %rdx
	movq	%rdx, -24(%rax)
	cmpq	$4, %rcx
	je	.L387
	leaq	-40(%rax), %rdx
	cmpl	-40(%rax), %esi
	jnb	.L386
	movq	-40(%rax), %r10
	movq	%r10, -32(%rax)
	cmpq	$5, %rcx
	je	.L387
	leaq	-48(%rax), %r10
	cmpl	-48(%rax), %esi
	jnb	.L325
	movq	-48(%rax), %rdx
	movq	%rdx, -40(%rax)
	cmpq	$6, %rcx
	je	.L387
	leaq	-56(%rax), %rdx
	cmpl	-56(%rax), %esi
	jnb	.L386
	movq	-56(%rax), %r10
	movq	%r10, -48(%rax)
	cmpq	$7, %rcx
	je	.L387
	leaq	-64(%rax), %r10
	cmpl	-64(%rax), %esi
	jnb	.L325
	movq	-64(%rax), %rdx
	movq	%rdx, -56(%rax)
	cmpq	$8, %rcx
	je	.L387
	leaq	-72(%rax), %rdx
	cmpl	-72(%rax), %esi
	jnb	.L386
	movq	-72(%rax), %r10
	movq	%r10, -64(%rax)
	cmpq	$9, %rcx
	je	.L387
	cmpl	%esi, -80(%rax)
	jbe	.L325
	movq	-80(%rax), %rdx
	movq	%rdx, -72(%rax)
	movq	%r14, %rdx
.L325:
	addq	$1, %rcx
	movl	%esi, (%rdx)
	movw	%r9w, 4(%rdx)
	movw	%r8w, 6(%rdx)
	cmpq	%rcx, %r11
	je	.L329
.L328:
	movq	%rdi, %rax
	addq	$8, %rdi
	movl	8(%rax), %esi
	movzwl	12(%rax), %r9d
	movq	%rdi, %rdx
	movzwl	14(%rax), %r8d
	cmpl	(%rax), %esi
	jnb	.L325
	movq	(%rax), %rdx
	movq	%rdx, 8(%rax)
	testq	%rcx, %rcx
	jne	.L424
.L387:
	movq	%r14, %rdx
	jmp	.L325
.L386:
	movq	%r10, %rdx
	jmp	.L325
.L374:
	movq	%rax, %rdx
	jmp	.L325
.L423:
	movq	-208(%rbp), %rdi
	movq	%r14, %rsi
	call	BrotliFree
	movq	-136(%rbp), %r14
	movq	-192(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r14, %rdi
	call	BrotliConvertBitDepthsToSymbols
	cmpq	$4, -176(%rbp)
	movq	-120(%rbp), %rbx
	jbe	.L425
	movq	(%rbx), %rdx
	movl	$8, %r10d
	movabsq	$1096648316244, %rax
	leaq	kCodeLengthBits(%rip), %r11
	leaq	kCodeLengthDepth(%rip), %r12
	leaq	kZeroRepsDepth(%rip), %r15
	movq	%rdx, %rsi
	movl	%edx, %ecx
	addq	$40, %rdx
	shrq	$3, %rsi
	addq	-112(%rbp), %rsi
	andl	$7, %ecx
	movq	%rdx, (%rbx)
	movzbl	(%rsi), %edi
	salq	%cl, %rax
	orq	%rdi, %rax
	xorl	%edi, %edi
	movq	%rax, (%rsi)
.L365:
	movq	-136(%rbp), %rax
	movl	%edx, %ecx
	leaq	1(%rdi), %r8
	andl	$7, %ecx
	leaq	(%rax,%rdi), %r9
	movq	-160(%rbp), %rax
	movzbl	(%r9), %esi
	cmpq	%rax, %r8
	jnb	.L353
	subq	%rdi, %rax
	movq	%rax, %r8
	movl	$1, %eax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L359:
	addq	$1, %rax
	cmpq	%r8, %rax
	je	.L358
.L354:
	cmpb	%sil, (%r9,%rax)
	je	.L359
.L358:
	addq	%rax, %rdi
	testb	%sil, %sil
	jne	.L360
	leaq	kZeroRepsBits(%rip), %rbx
	movl	(%r15,%rax,4), %esi
	movq	(%rbx,%rax,8), %rax
	salq	%cl, %rax
	movq	%rax, %rcx
.L355:
	movq	%rdx, %rax
	addq	%rsi, %rdx
	shrq	$3, %rax
	addq	-112(%rbp), %rax
	movzbl	(%rax), %r8d
	orq	%r8, %rcx
	movq	%rcx, (%rax)
	movq	-120(%rbp), %rax
	movq	%rdx, (%rax)
.L361:
	cmpq	%rdi, -160(%rbp)
	ja	.L365
.L309:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L425:
	.cfi_restore_state
	movq	(%rbx), %rsi
	movq	-112(%rbp), %r11
	movq	%r14, %r10
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$3, %rax
	andl	$7, %ecx
	addq	%r11, %rax
	movzbl	(%rax), %edx
	btsq	%rcx, %rdx
	movq	%rdx, (%rax)
	leaq	2(%rsi), %rdx
	movq	-216(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rdx, (%rbx)
	andl	$7, %edx
	shrq	$3, %rdi
	movl	%edx, %ecx
	movl	$1, %edx
	addq	%r11, %rdi
	salq	%cl, %rax
	movq	-96(%rbp), %rcx
	movzbl	(%rdi), %r8d
	orq	%r8, %rax
	movq	%rax, (%rdi)
	leaq	4(%rsi), %rax
	movl	$1, %esi
	movq	%rax, (%rbx)
	movzbl	(%r14,%rcx), %edi
.L347:
	movq	-96(%rbp,%rdx,8), %r8
	movzbl	(%r10,%r8), %r9d
	cmpb	%dil, %r9b
	jnb	.L348
	movq	%rcx, -96(%rbp,%rdx,8)
	movl	%r9d, %edi
	movq	%r8, %rcx
	movq	%r8, -96(%rbp,%r15,8)
.L348:
	addq	$1, %rdx
	cmpq	-176(%rbp), %rdx
	jb	.L347
	leaq	1(%rsi), %rdx
	cmpq	%rdx, -176(%rbp)
	jbe	.L366
	movq	-96(%rbp,%rsi,8), %rcx
	movq	%rsi, %r15
	movq	%rdx, %rsi
	movzbl	(%r10,%rcx), %edi
	jmp	.L347
.L360:
	cmpb	%r10b, %sil
	je	.L362
	movzbl	%sil, %r9d
	movq	%rdx, %r10
	subq	$1, %rax
	movl	(%r11,%r9,4), %r8d
	shrq	$3, %r10
	addq	-112(%rbp), %r10
	movzbl	(%r10), %ebx
	movzbl	(%r12,%r9), %r9d
	movq	%r8, %r13
	salq	%cl, %r13
	addq	%r9, %rdx
	orq	%r13, %rbx
	movl	%edx, %ecx
	movq	%rbx, (%r10)
	movq	-120(%rbp), %rbx
	andl	$7, %ecx
	movq	%rdx, (%rbx)
	cmpq	$2, %rax
	jbe	.L427
.L369:
	leaq	kNonZeroRepsDepth(%rip), %rbx
	movq	%rdx, %r8
	movl	-12(%rbx,%rax,4), %r9d
	leaq	kNonZeroRepsBits(%rip), %rbx
	shrq	$3, %r8
	addq	-112(%rbp), %r8
	movq	-24(%rbx,%rax,8), %rax
	movzbl	(%r8), %r10d
	addq	%r9, %rdx
	salq	%cl, %rax
	orq	%rax, %r10
	movq	%r10, (%r8)
.L418:
	movq	-120(%rbp), %rax
	movl	%esi, %r10d
	movq	%rdx, (%rax)
	jmp	.L361
.L420:
	movq	-96(%rbp), %rax
	movq	-192(%rbp), %rbx
	addq	%rax, -136(%rbp)
	leaq	(%rbx,%rax,2), %rbx
	movq	%rbx, -192(%rbp)
.L310:
	movq	-120(%rbp), %rbx
	movq	-112(%rbp), %r15
	xorl	%r10d, %r10d
	movq	(%rbx), %rdx
	movq	%rdx, %rcx
	movl	%edx, %edi
	addq	$4, %rdx
	shrq	$3, %rcx
	andl	$7, %edi
	movq	%rdx, (%rbx)
	addq	%r15, %rcx
	movzbl	(%rcx), %esi
	btsq	%rdi, %rsi
	movq	%rsi, (%rcx)
	movq	%rdx, %rsi
	movl	%edx, %ecx
	addq	-184(%rbp), %rdx
	shrq	$3, %rsi
	andl	$7, %ecx
	movq	%rdx, (%rbx)
	addq	%r15, %rsi
	salq	%cl, %rax
	movzbl	(%rsi), %edi
	orq	%rdi, %rax
	movq	%rax, (%rsi)
	movq	-136(%rbp), %rax
	movb	$0, (%rax)
	movq	-192(%rbp), %rax
	movw	%r10w, (%rax)
	jmp	.L309
.L362:
	cmpq	$2, %rax
	ja	.L369
	movzbl	%sil, %ecx
	movl	(%r11,%rcx,4), %r8d
	movzbl	(%r12,%rcx), %r9d
.L370:
	movq	%rdx, %r10
	movq	-112(%rbp), %r13
	movl	%edx, %ecx
	movq	%r8, %r14
	shrq	$3, %r10
	andl	$7, %ecx
	addq	%r9, %rdx
	addq	%r13, %r10
	salq	%cl, %r14
	movzbl	(%r10), %ebx
	orq	%r14, %rbx
	movq	%rbx, (%r10)
	cmpq	$1, %rax
	je	.L418
	movq	%rdx, %rax
	movl	%edx, %ecx
	addq	%r9, %rdx
	shrq	$3, %rax
	andl	$7, %ecx
	addq	%r13, %rax
	salq	%cl, %r8
	movzbl	(%rax), %r10d
	orq	%r8, %r10
	movq	%r10, (%rax)
	jmp	.L418
.L366:
	movq	-184(%rbp), %rdi
	movq	-112(%rbp), %r14
	movq	%rax, %r8
	shrq	$3, %r8
	movq	-96(%rbp), %rbx
	movq	-88(%rbp), %r10
	leaq	(%rdi,%rax), %rsi
	addq	%r14, %r8
	andl	$7, %eax
	movl	%esi, %ecx
	movq	%rsi, %rdx
	movq	%rbx, %r11
	addq	%rsi, %rdi
	andl	$7, %ecx
	shrq	$3, %rdx
	salq	%cl, %r10
	movl	%eax, %ecx
	movzbl	(%r8), %eax
	addq	%r14, %rdx
	salq	%cl, %r11
	orq	%rax, %r11
	cmpq	$2, -176(%rbp)
	je	.L428
	movq	-80(%rbp), %rax
	movl	%edi, %ecx
	movq	%rdi, %r9
	andl	$7, %ecx
	shrq	$3, %r9
	addq	-112(%rbp), %r9
	salq	%cl, %rax
	movq	%rax, %rcx
	movq	-184(%rbp), %rax
	addq	%rdi, %rax
	cmpq	$3, -176(%rbp)
	je	.L429
	movq	%r14, %r15
	movq	-120(%rbp), %r14
	movq	%r11, (%r8)
	movq	%rsi, (%r14)
	movzbl	(%rdx), %esi
	movq	%rdi, (%r14)
	orq	%rsi, %r10
	movq	%rax, %rsi
	movq	%r10, (%rdx)
	movzbl	(%r9), %edx
	shrq	$3, %rsi
	addq	%r15, %rsi
	movq	%rax, (%r14)
	orq	%rdx, %rcx
	movq	-72(%rbp), %rdx
	movq	%rcx, (%r9)
	movl	%eax, %ecx
	movzbl	(%rsi), %edi
	andl	$7, %ecx
	addq	-184(%rbp), %rax
	salq	%cl, %rdx
	movq	-136(%rbp), %rcx
	movq	%rax, (%r14)
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	movq	%rax, %rsi
	xorl	%edx, %edx
	shrq	$3, %rsi
	addq	%r15, %rsi
	cmpb	$1, (%rcx,%rbx)
	movl	%eax, %ecx
	sete	%dl
	movzbl	(%rsi), %edi
	andl	$7, %ecx
	addq	$1, %rax
	salq	%cl, %rdx
	movq	%rax, (%r14)
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	jmp	.L309
.L428:
	movq	-120(%rbp), %rbx
	movq	%r11, (%r8)
	movq	%rsi, (%rbx)
	movzbl	(%rdx), %eax
	movq	%rdi, (%rbx)
	orq	%rax, %r10
	movq	%r10, (%rdx)
	jmp	.L309
.L353:
	testb	%sil, %sil
	je	.L390
	movzbl	%sil, %edi
	movl	(%r11,%rdi,4), %eax
	movzbl	(%r12,%rdi), %r9d
	cmpb	%r10b, %sil
	je	.L430
	movq	%rdx, %rdi
	salq	%cl, %rax
	addq	%r9, %rdx
	shrq	$3, %rdi
	addq	-112(%rbp), %rdi
	movzbl	(%rdi), %r10d
	orq	%rax, %r10
	movq	-120(%rbp), %rax
	movq	%r10, (%rdi)
	movq	%r8, %rdi
	movl	%esi, %r10d
	movq	%rdx, (%rax)
	jmp	.L361
.L427:
	movl	%esi, %r10d
	testq	%rax, %rax
	jne	.L370
	jmp	.L361
.L429:
	movq	-120(%rbp), %rbx
	movq	%r11, (%r8)
	movq	%rsi, (%rbx)
	movzbl	(%rdx), %esi
	movq	%rdi, (%rbx)
	orq	%rsi, %r10
	movq	%r10, (%rdx)
	movzbl	(%r9), %edx
	movq	%rax, (%rbx)
	orq	%rdx, %rcx
	movq	%rcx, (%r9)
	jmp	.L309
.L390:
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	movl	$4, %esi
	jmp	.L355
.L426:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rdx, %rdi
	salq	%cl, %rax
	addq	%r9, %rdx
	shrq	$3, %rdi
	addq	-112(%rbp), %rdi
	movzbl	(%rdi), %r10d
	orq	%rax, %r10
	movq	%r10, (%rdi)
	movq	%r8, %rdi
	jmp	.L418
	.cfi_endproc
.LFE115:
	.size	BrotliBuildAndStoreHuffmanTreeFast, .-BrotliBuildAndStoreHuffmanTreeFast
	.p2align 4
	.globl	BrotliStoreMetaBlock
	.hidden	BrotliStoreMetaBlock
	.type	BrotliStoreMetaBlock, @function
BrotliStoreMetaBlock:
.LFB133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$888, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rbp), %rax
	movl	40(%rbp), %r12d
	movq	32(%rbp), %r15
	movq	72(%rbp), %rbx
	movl	28(%r15), %r11d
	movq	%rax, -4920(%rbp)
	movq	64(%rbp), %rax
	movq	%rdx, %r13
	leaq	kContextLookup(%rip), %rdx
	movq	%rdi, -5016(%rbp)
	movl	64(%r15), %edi
	movq	%rax, -4968(%rbp)
	movq	80(%rbp), %rax
	movq	%rsi, -4872(%rbp)
	movl	%edi, %r14d
	movq	%rcx, %rsi
	movq	%rax, -4960(%rbp)
	movzbl	16(%rbp), %eax
	movq	%r8, -4880(%rbp)
	movb	%r9b, -4864(%rbp)
	movb	%al, -4849(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r12d, %eax
	movl	%edi, -4888(%rbp)
	sall	$9, %eax
	addq	%rdx, %rax
	movq	%rax, -4936(%rbp)
	testl	%r11d, %r11d
	je	.L432
	cmpl	$544, %edi
	movl	$544, %r14d
	cmovbe	%edi, %r14d
.L432:
	movq	-4960(%rbp), %rcx
	movl	24(%rbp), %edi
	movq	%rbx, %rdx
	call	StoreCompressedMetaBlockHeader
	movq	-5016(%rbp), %rdi
	movl	$11272, %esi
	call	BrotliAllocate
	movq	$256, -4304(%rbp)
	movq	%rax, -4896(%rbp)
	movq	-4968(%rbp), %rax
	movq	$1, -4264(%rbp)
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rdx
	movq	$0, -4256(%rbp)
	movq	(%rax), %rcx
	movq	24(%rax), %rsi
	movq	$0, -3392(%rbp)
	movq	16(%rax), %rdi
	movq	%rdx, -4272(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -4296(%rbp)
	movaps	%xmm2, -4912(%rbp)
	movaps	%xmm2, -4288(%rbp)
	testq	%rdx, %rdx
	je	.L433
	movl	(%rsi), %eax
.L433:
	movq	-4968(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%rax, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	56(%r9), %rax
	movq	72(%r9), %r8
	movq	$704, -3344(%rbp)
	movdqu	64(%r9), %xmm4
	movq	48(%r9), %r9
	movq	$1, -3304(%rbp)
	movq	%rax, -3312(%rbp)
	movq	%r9, -3336(%rbp)
	movq	$0, -3296(%rbp)
	movq	$0, -2432(%rbp)
	movups	%xmm0, -3368(%rbp)
	movaps	%xmm4, -4912(%rbp)
	movaps	%xmm4, -3328(%rbp)
	testq	%rax, %rax
	je	.L434
	movl	(%r8), %eax
.L434:
	movq	-4968(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%rax, -2424(%rbp)
	movups	%xmm0, -2408(%rbp)
	movd	%r14d, %xmm0
	movdqu	112(%r9), %xmm6
	movq	104(%r9), %rax
	movhps	96(%r9), %xmm0
	movq	$0, -2416(%rbp)
	movq	$1, -2344(%rbp)
	movq	120(%r9), %r8
	movq	%rax, -2352(%rbp)
	movq	$0, -2336(%rbp)
	movq	$0, -1472(%rbp)
	movaps	%xmm6, -4912(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movaps	%xmm6, -2368(%rbp)
	testq	%rax, %rax
	je	.L435
	movl	(%r8), %eax
.L435:
	movq	-4960(%rbp), %r14
	movq	-4896(%rbp), %r8
	pxor	%xmm0, %xmm0
	leaq	-4264(%rbp), %r9
	movq	%rax, -1464(%rbp)
	pushq	%r14
	pushq	%rbx
	movups	%xmm0, -1448(%rbp)
	movq	$0, -1456(%rbp)
	call	BuildAndStoreBlockSplitCode
	pushq	%r14
	movq	-4896(%rbp), %r8
	leaq	-3304(%rbp), %r9
	movq	-3336(%rbp), %rcx
	movq	-3312(%rbp), %rdx
	pushq	%rbx
	movq	-3320(%rbp), %rsi
	movq	-3328(%rbp), %rdi
	call	BuildAndStoreBlockSplitCode
	addq	$32, %rsp
	movq	-4896(%rbp), %r8
	movq	-2376(%rbp), %rcx
	pushq	%r14
	movq	-2352(%rbp), %rdx
	leaq	-2344(%rbp), %r9
	movq	-2360(%rbp), %rsi
	movq	-2368(%rbp), %rdi
	pushq	%rbx
	call	BuildAndStoreBlockSplitCode
	movq	(%rbx), %rax
	movl	56(%r15), %edx
	movq	%rax, %rsi
	movl	%eax, %ecx
	leaq	2(%rax), %r8
	movq	%rdx, %r9
	shrq	$3, %rsi
	andl	$7, %ecx
	movq	%r8, (%rbx)
	addq	$6, %rax
	addq	%r14, %rsi
	salq	%cl, %rdx
	movl	%r9d, %ecx
	movzbl	(%rsi), %edi
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	movl	60(%r15), %edx
	movq	%r8, %rsi
	shrq	$3, %rsi
	movq	%rax, (%rbx)
	addq	%r14, %rsi
	shrl	%cl, %edx
	movl	%r8d, %ecx
	movzbl	(%rsi), %edi
	andl	$7, %ecx
	salq	%cl, %rdx
	orq	%rdi, %rdx
	movq	-4968(%rbp), %rdi
	movq	%rdx, (%rsi)
	xorl	%esi, %esi
	popq	%r9
	cmpq	$0, (%rdi)
	movq	-4960(%rbp), %r9
	movq	%rdi, %r8
	popq	%r10
	je	.L440
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%rax, %rdx
	movl	%eax, %ecx
	movq	%r12, %r14
	addq	$2, %rax
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%rax, (%rbx)
	addq	$1, %rsi
	addq	%r9, %rdx
	salq	%cl, %r14
	movzbl	(%rdx), %edi
	movq	%r14, %rcx
	orq	%rdi, %rcx
	movq	%rcx, (%rdx)
	cmpq	%rsi, (%r8)
	ja	.L439
.L440:
	movq	-4968(%rbp), %rax
	movq	152(%rax), %r12
	movq	184(%rax), %r14
	testq	%r12, %r12
	jne	.L571
	leaq	-1(%r14), %r9
	movq	-4960(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r9, %rdi
	movq	%r9, -4912(%rbp)
	call	StoreVarLenUint8
	cmpq	$1, %r14
	ja	.L572
.L441:
	movq	-4968(%rbp), %rax
	movq	168(%rax), %r12
	movq	216(%rax), %r14
	testq	%r12, %r12
	jne	.L450
	leaq	-1(%r14), %r15
	movq	-4960(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	StoreVarLenUint8
	cmpq	$1, %r14
	ja	.L573
.L451:
	movq	-4968(%rbp), %rax
	movq	-4304(%rbp), %r12
	movq	184(%rax), %r15
	movq	176(%rax), %r14
	imulq	%r15, %r12
	testq	%r12, %r12
	jne	.L574
	movq	$0, -3368(%rbp)
	xorl	%eax, %eax
.L510:
	movq	%rax, -3360(%rbp)
	testq	%r15, %r15
	je	.L460
	xorl	%r12d, %r12d
	movq	%r13, -4912(%rbp)
	movq	%r14, %r13
	movq	%r12, %r14
	movq	-4896(%rbp), %r12
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-3360(%rbp), %rax
.L461:
	movq	-4304(%rbp), %rsi
	pushq	-4960(%rbp)
	movq	%r13, %rdi
	movq	%r12, %rcx
	pushq	%rbx
	movl	$256, %edx
	addq	$1040, %r13
	movq	%rsi, %r8
	imulq	%r14, %r8
	addq	$1, %r14
	leaq	(%rax,%r8,2), %r9
	addq	-3368(%rbp), %r8
	call	BuildAndStoreHuffmanTree
	popq	%r8
	popq	%r9
	cmpq	%r14, %r15
	jne	.L575
	movq	-4912(%rbp), %r13
.L460:
	movq	-4968(%rbp), %rax
	movq	-3344(%rbp), %r12
	movq	200(%rax), %r15
	movq	192(%rax), %r14
	imulq	%r15, %r12
	testq	%r12, %r12
	jne	.L576
	movq	$0, -2408(%rbp)
	xorl	%eax, %eax
.L509:
	movq	%rax, -2400(%rbp)
	testq	%r15, %r15
	je	.L463
	xorl	%r12d, %r12d
	movq	%r13, -4912(%rbp)
	movq	%r14, %r13
	movq	%r12, %r14
	movq	-4896(%rbp), %r12
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L577:
	movq	-2400(%rbp), %rax
.L464:
	movq	-3344(%rbp), %rsi
	pushq	-4960(%rbp)
	movq	%r13, %rdi
	movq	%r12, %rcx
	pushq	%rbx
	movl	$704, %edx
	addq	$2832, %r13
	movq	%rsi, %r8
	imulq	%r14, %r8
	addq	$1, %r14
	leaq	(%rax,%r8,2), %r9
	addq	-2408(%rbp), %r8
	call	BuildAndStoreHuffmanTree
	popq	%rsi
	popq	%rdi
	cmpq	%r14, %r15
	jne	.L577
	movq	-4912(%rbp), %r13
.L463:
	movq	-4968(%rbp), %rdi
	movl	-4888(%rbp), %r12d
	movq	216(%rdi), %rax
	movq	208(%rdi), %r15
	movq	%rax, -4888(%rbp)
	imulq	-2384(%rbp), %rax
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L578
	movq	$0, -1448(%rbp)
	xorl	%eax, %eax
.L508:
	cmpq	$0, -4888(%rbp)
	movq	%rax, -1440(%rbp)
	je	.L466
	xorl	%r14d, %r14d
	movq	%r13, -4912(%rbp)
	movq	%r15, %r13
	movq	%r14, %r15
	movq	-4896(%rbp), %r14
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L579:
	movq	-1440(%rbp), %rax
.L467:
	movq	-2384(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	pushq	-4960(%rbp)
	addq	$2192, %r13
	movq	%rsi, %r8
	pushq	%rbx
	imulq	%r15, %r8
	addq	$1, %r15
	leaq	(%rax,%r8,2), %r9
	addq	-1448(%rbp), %r8
	call	BuildAndStoreHuffmanTree
	popq	%rdx
	popq	%rcx
	cmpq	%r15, -4888(%rbp)
	jne	.L579
	movq	-4912(%rbp), %r13
.L466:
	movq	-4896(%rbp), %rsi
	movq	-5016(%rbp), %rdi
	call	BrotliFree
	cmpq	$0, 56(%rbp)
	je	.L507
	movq	56(%rbp), %rax
	movq	-4920(%rbp), %rdi
	movq	%rbx, %r15
	movq	-4960(%rbp), %r12
	salq	$4, %rax
	movq	%rdi, -4944(%rbp)
	addq	%rdi, %rax
	movq	%rax, -5000(%rbp)
	leaq	-3344(%rbp), %rax
	movq	%rax, -5008(%rbp)
	.p2align 4,,10
	.p2align 3
.L506:
	movq	-4944(%rbp), %rax
	movq	%r15, %rdx
	movq	%r12, %rcx
	movl	8(%rax), %edi
	movl	4(%rax), %ebx
	movl	(%rax), %r14d
	movzwl	12(%rax), %esi
	movl	%edi, -4988(%rbp)
	movzwl	14(%rax), %eax
	movq	-5008(%rbp), %rdi
	movl	%ebx, -4856(%rbp)
	movw	%ax, -4990(%rbp)
	movw	%si, -4852(%rbp)
	call	StoreSymbol
	movl	%ebx, %edx
	andl	$33554431, %ebx
	shrl	$25, %edx
	leal	(%rdx,%rdx), %eax
	andl	$-128, %eax
	orl	%edx, %eax
	movsbl	%al, %eax
	addl	%ebx, %eax
	movl	%r14d, %ebx
	cmpl	$5, %r14d
	jbe	.L580
	cmpq	$129, %rbx
	jbe	.L581
	cmpq	$2113, %rbx
	jbe	.L582
	cmpq	$6209, %rbx
	jbe	.L517
	cmpq	$22594, %rbx
	sbbl	%r8d, %r8d
	andl	$-16384, %r8d
	addl	$22594, %r8d
	cmpq	$22594, %rbx
	sbbl	%edi, %edi
	andl	$-10, %edi
	addl	$24, %edi
.L472:
	movl	%eax, %edx
	cmpl	$9, %eax
	jbe	.L583
	cmpq	$133, %rdx
	jbe	.L584
	cmpq	$2117, %rdx
	ja	.L519
	leal	-70(%rax), %edx
	bsrl	%edx, %edx
	addl	$12, %edx
.L570:
	movzwl	%dx, %edx
	leaq	kCopyBase(%rip), %rcx
	leaq	kCopyExtra(%rip), %rsi
	movl	(%rcx,%rdx,4), %ecx
	movl	(%rsi,%rdx,4), %r10d
.L476:
	movq	(%r15), %rsi
	subl	%ecx, %eax
	movl	%edi, %ecx
	addl	%r10d, %edi
	salq	%cl, %rax
	movl	%r14d, %ecx
	movq	%rsi, %rdx
	subl	%r8d, %ecx
	addq	%rsi, %rdi
	shrq	$3, %rdx
	orq	%rcx, %rax
	movl	%esi, %ecx
	movq	%rdi, (%r15)
	addq	%r12, %rdx
	andl	$7, %ecx
	movzbl	(%rdx), %r9d
	salq	%cl, %rax
	orq	%rax, %r9
	movq	-4968(%rbp), %rax
	movq	%r9, (%rdx)
	cmpq	$0, 152(%rax)
	je	.L478
	testq	%rbx, %rbx
	je	.L520
	movq	144(%rax), %rax
	movzbl	-4864(%rbp), %r8d
	addq	%r13, %rbx
	movzbl	-4849(%rbp), %edx
	movq	%rbx, -4888(%rbp)
	movq	%rax, -4896(%rbp)
	movq	-4280(%rbp), %rax
	movb	%r8b, -4849(%rbp)
	movq	%rax, -4976(%rbp)
	movq	-4288(%rbp), %rax
	movq	%rax, -4984(%rbp)
	movq	-4304(%rbp), %rax
	movq	%rax, -4912(%rbp)
	movq	-3360(%rbp), %rax
	movq	%rax, -4920(%rbp)
	movq	-3368(%rbp), %rax
	movq	%rax, -4928(%rbp)
	movq	-3384(%rbp), %rax
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-3376(%rbp), %r10
.L483:
	movq	-4896(%rbp), %rbx
	movq	%rdi, %rsi
	subq	$1, %rax
	addq	$1, %r13
	addq	-4864(%rbp), %r10
	movq	-4928(%rbp), %rcx
	shrq	$3, %rsi
	movq	%rax, -3384(%rbp)
	movl	(%rbx,%r10,4), %edx
	imulq	-4912(%rbp), %rdx
	addq	%r12, %rsi
	movq	-4920(%rbp), %rbx
	movzbl	(%rsi), %r11d
	addq	%r8, %rdx
	movzbl	(%rcx,%rdx), %r10d
	movzwl	(%rbx,%rdx,2), %edx
	movl	%edi, %ecx
	andl	$7, %ecx
	salq	%cl, %rdx
	addq	%r10, %rdi
	orq	%rdx, %r11
	movq	%rdi, (%r15)
	movzbl	-4849(%rbp), %edx
	movq	%r11, (%rsi)
	cmpq	-4888(%rbp), %r13
	je	.L585
	movb	%r9b, -4849(%rbp)
.L491:
	movq	-4936(%rbp), %rbx
	movzbl	256(%rbx,%rdx), %r10d
	orb	(%rbx,%r8), %r10b
	movzbl	%r10b, %ebx
	movq	-4880(%rbp), %rdx
	movq	%rbx, -4864(%rbp)
	movq	-4872(%rbp), %rbx
	andq	%r13, %rdx
	movzbl	(%rbx,%rdx), %r8d
	movq	%r8, %r9
	testq	%rax, %rax
	jne	.L586
	movq	-3392(%rbp), %rcx
	movq	-4976(%rbp), %rsi
	movq	-4264(%rbp), %r11
	leaq	1(%rcx), %rdx
	movl	(%rsi,%rdx,4), %ebx
	movq	-4984(%rbp), %rsi
	movq	%rdx, -3392(%rbp)
	movzbl	1(%rsi,%rcx), %ecx
	movq	%rbx, -4952(%rbp)
	movq	%rbx, %rdx
	movq	%rbx, -3384(%rbp)
	leaq	1(%r11), %rbx
	movq	%rcx, %r10
	salq	$6, %r10
	movq	%r10, -3376(%rbp)
	cmpq	%rbx, %rcx
	je	.L521
	cmpq	-4256(%rbp), %rcx
	je	.L484
	leaq	2(%rcx), %rax
.L484:
	movq	%r11, %xmm1
	movq	%rdi, %r11
	movq	%rcx, %xmm0
	movzbl	-4248(%rbp,%rax), %esi
	shrq	$3, %r11
	movzwl	-3990(%rbp,%rax,2), %eax
	movl	%edi, %ecx
	punpcklqdq	%xmm1, %xmm0
	addq	%r12, %r11
	andl	$7, %ecx
	addq	%rdi, %rsi
	movups	%xmm0, -4264(%rbp)
	movzbl	(%r11), %ebx
	salq	%cl, %rax
	movq	%rsi, (%r15)
	orq	%rax, %rbx
	movq	%rbx, (%r11)
	cmpl	$176, %edx
	jbe	.L485
	cmpl	$753, %edx
	sbbl	%eax, %eax
	andl	$-6, %eax
	addl	$20, %eax
.L486:
	leal	1(%rax), %edi
	leaq	kBlockLengthPrefixCode(%rip), %rbx
	movq	%rdi, %rcx
	leaq	(%rbx,%rdi,8), %rdi
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L588:
	addl	$1, %ecx
.L488:
	movl	%eax, %r11d
	movl	%ecx, %eax
	cmpl	(%rdi), %edx
	jb	.L587
	addq	$8, %rdi
	cmpl	$25, %ecx
	jne	.L588
.L489:
	movq	%rsi, %r11
	movzbl	-3474(%rbp,%rax), %ebx
	movzwl	-3448(%rbp,%rax,2), %edi
	movl	%esi, %ecx
	shrq	$3, %r11
	andl	$7, %ecx
	addq	%r12, %r11
	addq	%rbx, %rsi
	salq	%cl, %rdi
	leaq	kBlockLengthPrefixCode(%rip), %rbx
	movzbl	(%r11), %r14d
	movl	%esi, %ecx
	subl	(%rbx,%rax,8), %edx
	movq	%rsi, (%r15)
	andl	$7, %ecx
	orq	%rdi, %r14
	movq	%rsi, %rdi
	salq	%cl, %rdx
	shrq	$3, %rdi
	movq	%r14, (%r11)
	addq	%r12, %rdi
	movzbl	(%rdi), %r11d
	orq	%rdx, %r11
	movq	%r11, (%rdi)
	movl	4(%rbx,%rax,8), %edi
	movq	-4952(%rbp), %rax
	addq	%rsi, %rdi
	movq	%rdi, (%r15)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L485:
	cmpl	$41, %edx
	sbbl	%eax, %eax
	notl	%eax
	andl	$7, %eax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L587:
	movl	%r11d, %eax
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L521:
	movl	$1, %eax
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L478:
	testq	%rbx, %rbx
	je	.L520
	movq	%r15, %rdx
	movq	%r12, %r15
	movq	-4872(%rbp), %r14
	movq	-4880(%rbp), %r12
	addq	%r13, %rbx
	leaq	-4304(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%r12, %rax
	movq	%r15, %rcx
	andq	%r13, %rax
	addq	$1, %r13
	movzbl	(%r14,%rax), %esi
	call	StoreSymbol
	cmpq	%rbx, %r13
	jne	.L481
	movq	%r15, %r12
	movq	%rdx, %r15
.L480:
	movl	-4856(%rbp), %eax
	andl	$33554431, %eax
	movl	%eax, %r13d
	addq	%rbx, %r13
	testl	%eax, %eax
	je	.L492
	movq	-4880(%rbp), %rdi
	movq	-4872(%rbp), %rsi
	leaq	-2(%r13), %rax
	andq	%rdi, %rax
	movzbl	(%rsi,%rax), %eax
	movb	%al, -4849(%rbp)
	leaq	-1(%r13), %rax
	andq	%rdi, %rax
	cmpw	$127, -4852(%rbp)
	movzbl	(%rsi,%rax), %eax
	movb	%al, -4864(%rbp)
	jbe	.L492
	movzwl	-4990(%rbp), %eax
	movl	-4988(%rbp), %ebx
	movq	%rax, %r11
	shrw	$10, %ax
	movw	%ax, -4888(%rbp)
	movq	-4968(%rbp), %rax
	andl	$1023, %r11d
	cmpq	$0, 168(%rax)
	je	.L589
	movzwl	-4852(%rbp), %eax
	movl	%eax, %edx
	andl	$7, %eax
	shrw	$6, %dx
	leal	-2(%rdx), %ecx
	testw	$-3, %cx
	je	.L495
	movl	$3, %r14d
	cmpw	$7, %dx
	jne	.L496
.L495:
	movzwl	%ax, %r14d
	cmpw	$3, %ax
	movl	$3, %eax
	cmovnb	%rax, %r14
.L496:
	movq	-4968(%rbp), %rax
	movq	(%r15), %r8
	movq	160(%rax), %rax
	movq	%rax, -4896(%rbp)
	movq	-1464(%rbp), %rax
	testq	%rax, %rax
	je	.L497
	movq	-1456(%rbp), %r10
.L498:
	movq	-4896(%rbp), %rdi
	subq	$1, %rax
	movq	-1440(%rbp), %rcx
	movq	%r8, %rdx
	movq	%rax, -1464(%rbp)
	leaq	(%r14,%r10), %rax
	shrq	$3, %rdx
	movl	(%rdi,%rax,4), %esi
	imulq	-2384(%rbp), %rsi
	addq	%r12, %rdx
	movq	-1448(%rbp), %rax
	movzbl	(%rdx), %edi
	addq	%r11, %rsi
	movzbl	(%rax,%rsi), %eax
	movzwl	(%rcx,%rsi,2), %esi
	movl	%r8d, %ecx
	andl	$7, %ecx
	salq	%cl, %rsi
	orq	%rsi, %rdi
	leaq	(%rax,%r8), %rsi
	movq	%rdi, (%rdx)
	movq	%rsi, (%r15)
.L494:
	movq	%rsi, %rax
	movl	%esi, %ecx
	shrq	$3, %rax
	andl	$7, %ecx
	addq	%r12, %rax
	salq	%cl, %rbx
	movzbl	(%rax), %edx
	orq	%rbx, %rdx
	movq	%rdx, (%rax)
	movzwl	-4888(%rbp), %eax
	addq	%rsi, %rax
	movq	%rax, (%r15)
.L492:
	addq	$16, -4944(%rbp)
	movq	-4944(%rbp), %rax
	cmpq	%rax, -5000(%rbp)
	jne	.L506
	movq	%r15, %rbx
.L507:
	movq	-5016(%rbp), %r14
	movq	-1448(%rbp), %rsi
	movq	%r14, %rdi
	call	BrotliFree
	movq	-1440(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -1448(%rbp)
	call	BrotliFree
	movq	-2408(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -1440(%rbp)
	call	BrotliFree
	movq	-2400(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -2408(%rbp)
	call	BrotliFree
	movq	-3368(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -2400(%rbp)
	call	BrotliFree
	movq	-3360(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -3368(%rbp)
	call	BrotliFree
	movl	24(%rbp), %eax
	movq	$0, -3360(%rbp)
	testl	%eax, %eax
	je	.L431
	movq	(%rbx), %rax
	movq	-4960(%rbp), %rdi
	addq	$7, %rax
	andl	$4294967288, %eax
	movq	%rax, (%rbx)
	shrq	$3, %rax
	movb	$0, (%rdi,%rax)
.L431:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L590
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	movb	%r9b, -4864(%rbp)
	movq	-4888(%rbp), %rbx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L583:
	leal	-2(%rax), %edx
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L580:
	movslq	%r14d, %rdx
	leaq	kInsExtra(%rip), %rdi
	leaq	kInsBase(%rip), %rsi
	movl	(%rdi,%rdx,4), %edi
	movl	(%rsi,%rdx,4), %r8d
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L584:
	leal	-6(%rax), %ecx
	subq	$6, %rdx
	bsrl	%ecx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	4(%rdx,%rcx,2), %edx
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L581:
	leal	-2(%r14), %ecx
	leaq	-2(%rbx), %rdx
	bsrl	%ecx, %ecx
	subl	$1, %ecx
	shrq	%cl, %rdx
	leal	2(%rdx,%rcx,2), %edx
.L569:
	movzwl	%dx, %edx
	leaq	kInsExtra(%rip), %rdi
	leaq	kInsBase(%rip), %rsi
	movl	(%rdi,%rdx,4), %edi
	movl	(%rsi,%rdx,4), %r8d
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L519:
	movl	$24, %r10d
	movl	$2118, %ecx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L582:
	leal	-66(%r14), %edx
	bsrl	%edx, %edx
	addl	$10, %edx
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L520:
	movq	%r13, %rbx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L497:
	movq	-1472(%rbp), %rdx
	movq	-2360(%rbp), %rsi
	leaq	1(%rdx), %rcx
	movl	(%rsi,%rcx,4), %edi
	movq	%rcx, -1472(%rbp)
	movq	-2368(%rbp), %rcx
	movq	%rdi, -4912(%rbp)
	movq	%rdi, %r9
	movzbl	1(%rcx,%rdx), %edx
	movq	%rdi, -1464(%rbp)
	movq	-2344(%rbp), %rdi
	leaq	0(,%rdx,4), %r10
	leaq	1(%rdi), %rcx
	movq	%r10, -1456(%rbp)
	cmpq	%rcx, %rdx
	je	.L527
	cmpq	-2336(%rbp), %rdx
	je	.L499
	leaq	2(%rdx), %rax
.L499:
	movq	%rdx, %xmm0
	movq	%r8, %rdx
	movq	%rdi, %xmm2
	movl	%r8d, %ecx
	shrq	$3, %rdx
	movzbl	-2328(%rbp,%rax), %esi
	andl	$7, %ecx
	punpcklqdq	%xmm2, %xmm0
	addq	%r12, %rdx
	movzwl	-2070(%rbp,%rax,2), %eax
	movups	%xmm0, -2344(%rbp)
	movzbl	(%rdx), %edi
	addq	%rsi, %r8
	salq	%cl, %rax
	movq	%r8, (%r15)
	orq	%rax, %rdi
	movq	%rdi, (%rdx)
	cmpl	$176, %r9d
	jbe	.L500
	cmpl	$753, %r9d
	sbbl	%eax, %eax
	andl	$-6, %eax
	addl	$20, %eax
.L501:
	leal	1(%rax), %ecx
	leaq	kBlockLengthPrefixCode(%rip), %rdi
	movq	%rcx, %rdx
	leaq	(%rdi,%rcx,8), %rcx
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L592:
	addl	$1, %edx
.L503:
	movl	%eax, %esi
	movl	%edx, %eax
	cmpl	(%rcx), %r9d
	jb	.L591
	addq	$8, %rcx
	cmpl	$25, %edx
	jne	.L592
.L504:
	movq	%r8, %rsi
	movzbl	-1554(%rbp,%rax), %edi
	movzwl	-1528(%rbp,%rax,2), %edx
	movl	%r8d, %ecx
	shrq	$3, %rsi
	andl	$7, %ecx
	addq	%r12, %rsi
	movq	%rdi, -4920(%rbp)
	salq	%cl, %rdx
	leaq	kBlockLengthPrefixCode(%rip), %rcx
	movzbl	(%rsi), %edi
	orq	%rdx, %rdi
	movl	%r9d, %edx
	subl	(%rcx,%rax,8), %edx
	movq	%rdi, (%rsi)
	movq	-4920(%rbp), %rdi
	leaq	(%r8,%rdi), %rsi
	movq	%rsi, %rdi
	movl	%esi, %r9d
	movq	%rsi, (%r15)
	shrq	$3, %rdi
	andl	$7, %r9d
	addq	%r12, %rdi
	movl	%r9d, %ecx
	movzbl	(%rdi), %r8d
	salq	%cl, %rdx
	leaq	kBlockLengthPrefixCode(%rip), %rcx
	orq	%rdx, %r8
	movq	%r8, (%rdi)
	movl	4(%rcx,%rax,8), %r8d
	movq	-4912(%rbp), %rax
	addq	%rsi, %r8
	movq	%r8, (%r15)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L589:
	movq	%r11, %rsi
	leaq	-2384(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	call	StoreSymbol
	movq	(%r15), %rsi
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L517:
	movl	$2114, %r8d
	movl	$12, %edi
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L450:
	subq	$8, %rsp
	movq	%rbx, %r9
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	160(%rax), %rsi
	pushq	-4960(%rbp)
	movq	-4896(%rbp), %r8
	movq	-5016(%rbp), %rdi
	call	EncodeContextMap
	popq	%r10
	popq	%r11
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L571:
	subq	$8, %rsp
	movq	%r14, %rcx
	movq	%rbx, %r9
	movq	%r12, %rdx
	movq	144(%rax), %rsi
	pushq	-4960(%rbp)
	movq	-4896(%rbp), %r8
	movq	-5016(%rbp), %rdi
	call	EncodeContextMap
	popq	%rcx
	popq	%rsi
	jmp	.L441
.L527:
	movl	$1, %eax
	jmp	.L499
.L572:
	leaq	5(%r14), %r15
	xorl	%esi, %esi
	movl	$1088, %ecx
	leaq	0(,%r15,4), %rdx
	leaq	-1424(%rbp), %rdi
	call	__memset_chk@PLT
	movq	-4960(%rbp), %r9
	movl	%r14d, -1404(%rbp)
	movl	$1, -1424(%rbp)
	movq	%rax, %rdi
	movq	(%rbx), %rax
	movq	%rax, %rsi
	shrq	$3, %rsi
	movq	%rsi, %rdx
	movl	%eax, %esi
	addq	%r9, %rdx
	andl	$7, %esi
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	%rcx, (%rdx)
	leaq	1(%rax), %rcx
	movl	$4, %edx
	addq	$5, %rax
	movq	%rcx, %rsi
	movq	%rcx, (%rbx)
	andl	$7, %ecx
	shrq	$3, %rsi
	salq	%cl, %rdx
	movq	%rax, (%rbx)
	addq	%r9, %rsi
	movq	-4912(%rbp), %r9
	movzbl	(%rsi), %r8d
	orq	%r8, %rdx
	cmpq	$6, %r15
	movq	%rdx, (%rsi)
	jbe	.L447
	subq	$2, %r14
	cmpq	$2, %r14
	jbe	.L514
	movq	%r9, %rdx
	leaq	-1400(%rbp), %rax
	movdqa	.LC11(%rip), %xmm0
	shrq	$2, %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L445:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L445
	movq	%r9, %rdx
	andq	$-4, %rdx
	leaq	6(%rdx), %rax
	cmpq	%rdx, %r9
	je	.L447
.L444:
	movl	$1, -1424(%rbp,%rax,4)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L447
	movl	$1, -1424(%rbp,%rdx,4)
	addq	$2, %rax
	cmpq	%rax, %r15
	jbe	.L447
	movl	$1, -1424(%rbp,%rax,4)
.L447:
	pushq	-4960(%rbp)
	movq	-4896(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	pushq	%rbx
	leaq	-336(%rbp), %r8
	leaq	-4848(%rbp), %r9
	movl	$31, %r14d
	call	BuildAndStoreHuffmanTree
	popq	%rdi
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	popq	%r8
	movzwl	-4838(%rbp), %r10d
	movl	$6, %r8d
	movzbl	-331(%rbp), %r11d
	movq	-4960(%rbp), %rdi
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%r8, %rdx
	addq	$1, %r8
.L443:
	movq	%rsi, %r9
	movzbl	-336(%rbp,%rdx), %eax
	movl	%esi, %ecx
	movzwl	-4848(%rbp,%rdx,2), %edx
	shrq	$3, %r9
	andl	$7, %ecx
	addq	%rdi, %r9
	salq	%cl, %rdx
	addq	%rsi, %rax
	movzbl	(%r9), %r12d
	movl	%eax, %ecx
	movq	%rax, (%rbx)
	andl	$7, %ecx
	orq	%r12, %rdx
	movq	%rdx, (%r9)
	movq	%rax, %rdx
	movq	%r10, %r9
	addq	%r11, %rax
	shrq	$3, %rdx
	salq	%cl, %r9
	movq	%rax, (%rbx)
	addq	%rdi, %rdx
	movq	%r9, %rcx
	movq	%r14, %r9
	movzbl	(%rdx), %esi
	orq	%rsi, %rcx
	movq	%rcx, (%rdx)
	movq	%rax, %rdx
	movl	%eax, %ecx
	shrq	$3, %rdx
	andl	$7, %ecx
	addq	%rdi, %rdx
	salq	%cl, %r9
	movzbl	(%rdx), %esi
	movq	%r9, %rcx
	orq	%rsi, %rcx
	leaq	5(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	%rsi, (%rbx)
	cmpq	%r8, %r15
	jne	.L449
	movq	%rsi, %rdx
	andl	$7, %esi
	addq	$6, %rax
	shrq	$3, %rdx
	addq	-4960(%rbp), %rdx
	movq	%rax, (%rbx)
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	%rcx, (%rdx)
	jmp	.L441
.L573:
	leaq	1(%r14), %r10
	leaq	-1424(%rbp), %rdi
	movl	$1088, %ecx
	xorl	%esi, %esi
	leaq	0(,%r10,4), %rdx
	movq	%r10, -4912(%rbp)
	call	__memset_chk@PLT
	movq	(%rbx), %rdx
	movq	-4960(%rbp), %r9
	movl	%r14d, -1420(%rbp)
	movq	%rax, %rdi
	movq	-4912(%rbp), %r10
	movl	$1, -1424(%rbp)
	movq	%rdx, %rax
	movl	%edx, %esi
	shrq	$3, %rax
	andl	$7, %esi
	addq	%r9, %rax
	movzbl	(%rax), %ecx
	btsq	%rsi, %rcx
	movq	%rcx, (%rax)
	leaq	1(%rdx), %rax
	addq	$5, %rdx
	movq	%rax, (%rbx)
	shrq	$3, %rax
	addq	%r9, %rax
	movq	%rdx, (%rbx)
	movzbl	(%rax), %ecx
	movq	%rcx, (%rax)
	cmpq	$2, %r10
	jbe	.L457
	leaq	-2(%r14), %rax
	cmpq	$2, %rax
	jbe	.L515
	movq	%r15, %rdx
	leaq	-1416(%rbp), %rax
	movdqa	.LC11(%rip), %xmm0
	shrq	$2, %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L455:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L455
	movq	%r15, %rdx
	andq	$-4, %rdx
	leaq	2(%rdx), %rax
	cmpq	%rdx, %r15
	je	.L457
.L454:
	movl	$1, -1424(%rbp,%rax,4)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %r10
	jbe	.L457
	movl	$1, -1424(%rbp,%rdx,4)
	addq	$2, %rax
	cmpq	%r10, %rax
	jnb	.L457
	movl	$1, -1424(%rbp,%rax,4)
.L457:
	movq	-4960(%rbp), %r15
	movq	-4896(%rbp), %rcx
	movq	%r10, %rdx
	movq	%r10, %rsi
	leaq	-336(%rbp), %r8
	leaq	-4848(%rbp), %r9
	pushq	%r15
	pushq	%rbx
	call	BuildAndStoreHuffmanTree
	popq	%rax
	movq	(%rbx), %rsi
	movl	$1, %r8d
	movzwl	-4846(%rbp), %r10d
	movzbl	-335(%rbp), %r11d
	movq	%r15, %rdi
	popq	%rdx
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%r12, %r8
.L453:
	movzwl	-4848(%rbp,%r12,2), %eax
	movq	%rsi, %r9
	movl	%esi, %ecx
	movzbl	-336(%rbp,%r12), %edx
	shrq	$3, %r9
	andl	$7, %ecx
	leaq	1(%r8), %r12
	addq	%rdi, %r9
	salq	%cl, %rax
	movzbl	(%r9), %r15d
	orq	%r15, %rax
	movq	%r10, %r15
	movq	%rax, (%r9)
	leaq	(%rdx,%rsi), %rax
	movq	%rax, %rdx
	movl	%eax, %ecx
	movq	%rax, (%rbx)
	addq	%r11, %rax
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%rax, (%rbx)
	addq	%rdi, %rdx
	salq	%cl, %r15
	movzbl	(%rdx), %esi
	movq	%r15, %rcx
	orq	%rsi, %rcx
	movl	%eax, %esi
	movq	%rcx, (%rdx)
	movq	%rax, %rdx
	andl	$7, %esi
	shrq	$3, %rdx
	addq	%rdi, %rdx
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	leaq	1(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	%rsi, (%rbx)
	cmpq	%r14, %r8
	jne	.L516
	movq	%rsi, %rdx
	andl	$7, %esi
	addq	$2, %rax
	shrq	$3, %rdx
	addq	-4960(%rbp), %rdx
	movq	%rax, (%rbx)
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	%rcx, (%rdx)
	jmp	.L451
.L500:
	cmpl	$41, %r9d
	sbbl	%eax, %eax
	notl	%eax
	andl	$7, %eax
	jmp	.L501
.L591:
	movl	%esi, %eax
	jmp	.L504
.L574:
	movq	-5016(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	-5016(%rbp), %rdi
	leaq	(%r12,%r12), %rsi
	movq	%rax, -3368(%rbp)
	call	BrotliAllocate
	jmp	.L510
.L578:
	movq	-5016(%rbp), %rdi
	movq	%rax, %rsi
	call	BrotliAllocate
	movq	-5016(%rbp), %rdi
	leaq	(%r14,%r14), %rsi
	movq	%rax, -1448(%rbp)
	call	BrotliAllocate
	jmp	.L508
.L576:
	movq	-5016(%rbp), %rdi
	movq	%r12, %rsi
	call	BrotliAllocate
	movq	-5016(%rbp), %rdi
	leaq	(%r12,%r12), %rsi
	movq	%rax, -2408(%rbp)
	call	BrotliAllocate
	jmp	.L509
.L515:
	movl	$2, %eax
	jmp	.L454
.L514:
	movl	$6, %eax
	jmp	.L444
.L590:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE133:
	.size	BrotliStoreMetaBlock, .-BrotliStoreMetaBlock
	.p2align 4
	.globl	BrotliStoreMetaBlockTrivial
	.hidden	BrotliStoreMetaBlockTrivial
	.type	BrotliStoreMetaBlockTrivial, @function
BrotliStoreMetaBlockTrivial:
.LFB136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1352, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %r14
	movq	40(%rbp), %rbx
	movq	48(%rbp), %r15
	movq	%rdi, -9480(%rbp)
	movq	%rdx, %r13
	movl	%r9d, %edi
	leaq	-8640(%rbp), %r12
	movq	%rsi, -9488(%rbp)
	movq	%rcx, %rsi
	movq	%r15, %rcx
	movq	%rdx, -9544(%rbp)
	movq	%rbx, %rdx
	movq	%r8, -9496(%rbp)
	movl	%r9d, -9548(%rbp)
	movq	%r14, -9560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbp), %rax
	movl	64(%rax), %eax
	movl	%eax, -9536(%rbp)
	call	StoreCompressedMetaBlockHeader
	xorl	%eax, %eax
	movl	$128, %ecx
	movq	%r12, %rdi
	rep stosq
	movl	$352, %ecx
	movsd	.LC12(%rip), %xmm0
	leaq	-4000(%rbp), %rdi
	movq	%rdi, -9512(%rbp)
	cmpq	$0, 32(%rbp)
	rep stosq
	movl	$272, %ecx
	leaq	-6192(%rbp), %rdi
	movq	$0, -7616(%rbp)
	movq	%rdi, -9520(%rbp)
	movq	$0, -1184(%rbp)
	rep stosq
	movsd	%xmm0, -7608(%rbp)
	movq	$0, -4016(%rbp)
	movsd	%xmm0, -1176(%rbp)
	movsd	%xmm0, -4008(%rbp)
	je	.L595
	movq	32(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rax
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
	xorl	%r9d, %r9d
	salq	$4, %r8
	addq	%r14, %r8
	xorl	%r14d, %r14d
	movq	%r8, -9528(%rbp)
	.p2align 4,,10
	.p2align 3
.L599:
	movzwl	12(%rdx), %ecx
	movl	4(%rdx), %edi
	movzwl	14(%rdx), %esi
	addl	$1, -4000(%rbp,%rcx,4)
	movq	%rcx, %r8
	movl	(%rdx), %ecx
	testq	%rcx, %rcx
	je	.L596
	movq	%rdx, -9504(%rbp)
	leaq	(%rax,%rcx), %r13
	.p2align 4,,10
	.p2align 3
.L597:
	movq	-9496(%rbp), %r11
	movq	-9488(%rbp), %rdx
	andq	%rax, %r11
	addq	$1, %rax
	movzbl	(%rdx,%r11), %r11d
	addl	$1, -8640(%rbp,%r11,4)
	cmpq	%r13, %rax
	jne	.L597
	movq	-9504(%rbp), %rdx
	addq	%rcx, %r9
	movl	$1, %r13d
.L596:
	andl	$33554431, %edi
	movl	%edi, %ecx
	addq	%rcx, %rax
	cmpw	$127, %r8w
	seta	%cl
	testl	%edi, %edi
	setne	%dil
	andb	%dil, %cl
	jne	.L661
.L598:
	addq	$16, %rdx
	cmpq	-9528(%rbp), %rdx
	jne	.L599
	movq	32(%rbp), %rax
	movq	%rax, -1184(%rbp)
	testb	%r13b, %r13b
	je	.L600
	movq	%r9, -7616(%rbp)
.L600:
	testb	%r14b, %r14b
	je	.L595
	movq	%r10, -4016(%rbp)
.L595:
	movq	(%rbx), %rax
	movq	-9480(%rbp), %rdi
	movl	$11272, %esi
	xorl	%r13d, %r13d
	movq	%rax, %rdx
	addq	$13, %rax
	shrq	$3, %rdx
	movq	%rax, (%rbx)
	addq	%r15, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	call	BrotliAllocate
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movaps	%xmm0, -7600(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -7584(%rbp)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%rax, -7600(%rbp,%r13,8)
.L604:
	addq	$1, %r13
.L602:
	addq	$1, %rax
	cmpq	$256, %rax
	je	.L662
.L606:
	movl	(%r12,%rax,4), %r10d
	testl	%r10d, %r10d
	je	.L602
	cmpq	$3, %r13
	jbe	.L663
	cmpq	$4, %r13
	je	.L604
	leaq	-1024(%rbp), %rdi
	xorl	%eax, %eax
	movl	$32, %ecx
	movl	$15, %edx
	movq	%rdi, %r13
	movq	%rdi, -9504(%rbp)
	movl	$256, %esi
	rep stosq
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	%r14, %rcx
	call	BrotliCreateHuffmanTree
	leaq	-9152(%rbp), %rax
	movl	$256, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rax, -9568(%rbp)
	call	BrotliConvertBitDepthsToSymbols
.L609:
	movq	-9504(%rbp), %rdi
	movq	%r15, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movl	$256, %esi
	call	BrotliStoreHuffmanTree
	leaq	-7600(%rbp), %rax
	movq	%rax, -9528(%rbp)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L661:
	andl	$1023, %esi
	addq	$1, %r10
	movl	%ecx, %r14d
	addl	$1, -6192(%rbp,%rsi,4)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L662:
	cmpq	$1, %r13
	ja	.L664
	movq	(%rbx), %rax
	movq	%rax, %rdx
	movl	%eax, %esi
	shrq	$3, %rdx
	andl	$7, %esi
	addq	%r15, %rdx
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	-7600(%rbp), %rsi
	movq	%rcx, (%rdx)
	leaq	4(%rax), %rcx
	addq	$12, %rax
	movq	%rcx, %rdx
	movq	%rcx, (%rbx)
	movq	%rsi, %r9
	andl	$7, %ecx
	shrq	$3, %rdx
	salq	%cl, %r9
	movq	%rax, (%rbx)
	leaq	-1024(%rbp), %rax
	addq	%r15, %rdx
	movq	%r9, %rcx
	movq	%rax, -9504(%rbp)
	leaq	-9152(%rbp), %rax
	movzbl	(%rdx), %edi
	movq	%rax, -9568(%rbp)
	xorl	%r9d, %r9d
	leaq	-7600(%rbp), %rax
	movb	$0, -1024(%rbp,%rsi)
	orq	%rdi, %rcx
	movq	%rax, -9528(%rbp)
	movq	%rcx, (%rdx)
	movw	%r9w, -9152(%rbp,%rsi,2)
.L610:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movaps	%xmm0, -9440(%rbp)
	movaps	%xmm0, -9424(%rbp)
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%rax, -9440(%rbp,%r12,8)
.L614:
	addq	$1, %r12
.L612:
	addq	$1, %rax
	cmpq	$704, %rax
	je	.L665
.L616:
	movq	-9512(%rbp), %rsi
	movl	(%rsi,%rax,4), %r8d
	testl	%r8d, %r8d
	je	.L612
	cmpq	$3, %r12
	jbe	.L666
	cmpq	$4, %r12
	je	.L614
	leaq	-768(%rbp), %r13
	xorl	%eax, %eax
	movl	$88, %ecx
	movl	$15, %edx
	movq	%r13, %rdi
	movl	$704, %esi
	movq	%r13, %r8
	rep stosq
	movq	-9512(%rbp), %rdi
	movq	%r14, %rcx
	call	BrotliCreateHuffmanTree
	movq	-9528(%rbp), %rdx
	movl	$704, %esi
	movq	%r13, %rdi
	call	BrotliConvertBitDepthsToSymbols
.L619:
	movq	%r15, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movl	$704, %esi
	movq	%r13, %rdi
	call	BrotliStoreHuffmanTree
	leaq	-9440(%rbp), %rax
	movq	%rax, -9512(%rbp)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L664:
	xorl	%eax, %eax
	movl	$32, %ecx
	movl	$15, %edx
	leaq	-1024(%rbp), %rdi
	movq	%rdi, %rsi
	rep stosq
	movq	%rsi, -9504(%rbp)
	movq	%r12, %rdi
	movq	%rsi, %r8
	movq	%r14, %rcx
	movl	$256, %esi
	call	BrotliCreateHuffmanTree
	movq	-9504(%rbp), %r12
	movl	$256, %esi
	leaq	-9152(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -9568(%rbp)
	movq	%r12, %rdi
	call	BrotliConvertBitDepthsToSymbols
	cmpq	$4, %r13
	ja	.L609
	leaq	-7600(%rbp), %rax
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$8, %ecx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, -9528(%rbp)
	call	StoreSimpleHuffmanTree
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L665:
	cmpq	$1, %r12
	ja	.L667
	movq	(%rbx), %rax
	leaq	-768(%rbp), %r13
	movq	%rax, %rdx
	movl	%eax, %esi
	shrq	$3, %rdx
	andl	$7, %esi
	addq	%r15, %rdx
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	-9440(%rbp), %rsi
	movq	%rcx, (%rdx)
	leaq	4(%rax), %rcx
	addq	$14, %rax
	movq	%rcx, %rdx
	movq	%rcx, (%rbx)
	movq	%rsi, %r9
	andl	$7, %ecx
	shrq	$3, %rdx
	salq	%cl, %r9
	movq	%rax, (%rbx)
	leaq	-9440(%rbp), %rax
	addq	%r15, %rdx
	movq	%r9, %rcx
	movb	$0, -768(%rbp,%rsi)
	movzbl	(%rdx), %edi
	movq	%rax, -9512(%rbp)
	orq	%rdi, %rcx
	xorl	%edi, %edi
	movq	%rcx, (%rdx)
	movw	%di, -7600(%rbp,%rsi,2)
.L620:
	pxor	%xmm0, %xmm0
	movl	-9536(%rbp), %eax
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	movaps	%xmm0, -9472(%rbp)
	movaps	%xmm0, -9456(%rbp)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%rdx, -9472(%rbp,%r11,8)
.L624:
	addq	$1, %r11
.L622:
	addq	$1, %rdx
	cmpq	$140, %rdx
	je	.L668
.L626:
	movq	-9520(%rbp), %rsi
	movl	(%rsi,%rdx,4), %ecx
	testl	%ecx, %ecx
	je	.L622
	cmpq	$3, %r11
	jbe	.L669
	cmpq	$4, %r11
	je	.L624
	subq	$1, %rax
	jne	.L634
	movl	$17, %ecx
	movl	$15, %edx
	movl	$140, %esi
	leaq	-1168(%rbp), %r12
	movq	%r12, %rdi
	movq	%r12, %r8
	rep stosq
	movq	%r14, %rcx
	movl	$0, (%rdi)
	movq	-9520(%rbp), %rdi
	call	BrotliCreateHuffmanTree
	movq	-9512(%rbp), %rdx
	movl	$140, %esi
	movq	%r12, %rdi
	call	BrotliConvertBitDepthsToSymbols
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r15, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movl	$140, %esi
	movq	%r12, %rdi
	call	BrotliStoreHuffmanTree
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L668:
	subq	$1, %rax
	je	.L636
.L634:
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L628:
	addq	$1, %r10
	shrq	%rax
	jne	.L628
.L627:
	cmpq	$1, %r11
	jbe	.L670
	leaq	-1168(%rbp), %r12
	xorl	%eax, %eax
	movl	$17, %ecx
	movl	$15, %edx
	movq	%r12, %rdi
	movq	%r12, %r8
	movl	$140, %esi
	movq	%r11, -9536(%rbp)
	rep stosq
	movq	%r14, %rcx
	movq	%r10, -9576(%rbp)
	movl	$0, (%rdi)
	movq	-9520(%rbp), %rdi
	call	BrotliCreateHuffmanTree
	movq	-9512(%rbp), %rdx
	movl	$140, %esi
	movq	%r12, %rdi
	call	BrotliConvertBitDepthsToSymbols
	movq	-9536(%rbp), %r11
	cmpq	$4, %r11
	ja	.L631
	movq	-9576(%rbp), %r10
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r11, %rdx
	leaq	-9472(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r10, %rcx
	call	StoreSimpleHuffmanTree
.L630:
	movq	-9480(%rbp), %rdi
	movq	%r14, %rsi
	call	BrotliFree
	subq	$8, %rsp
	movq	32(%rbp), %r8
	movq	-9504(%rbp), %r9
	movq	-9560(%rbp), %rcx
	movq	-9496(%rbp), %rdx
	pushq	%r15
	movq	-9544(%rbp), %rsi
	movq	-9488(%rbp), %rdi
	pushq	%rbx
	pushq	-9512(%rbp)
	pushq	%r12
	pushq	-9528(%rbp)
	pushq	%r13
	pushq	-9568(%rbp)
	call	StoreDataWithHuffmanCodes
	movl	-9548(%rbp), %eax
	addq	$64, %rsp
	testl	%eax, %eax
	je	.L593
	movq	(%rbx), %rax
	addq	$7, %rax
	andl	$4294967288, %eax
	movq	%rax, (%rbx)
	shrq	$3, %rax
	movb	$0, (%r15,%rax)
.L593:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L671
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_restore_state
	movq	(%rbx), %rax
	leaq	-1168(%rbp), %r12
	movq	%rax, %rdx
	movl	%eax, %esi
	addq	$4, %rax
	shrq	$3, %rdx
	andl	$7, %esi
	movq	%rax, (%rbx)
	addq	%rax, %r10
	addq	%r15, %rdx
	movzbl	(%rdx), %ecx
	btsq	%rsi, %rcx
	movq	-9472(%rbp), %rsi
	movq	%rcx, (%rdx)
	movq	%rax, %rdx
	movl	%eax, %ecx
	shrq	$3, %rdx
	andl	$7, %ecx
	movq	%rsi, %r9
	movq	%r10, (%rbx)
	addq	%r15, %rdx
	salq	%cl, %r9
	movb	$0, -1168(%rbp,%rsi)
	movzbl	(%rdx), %edi
	movq	%r9, %rcx
	orq	%rdi, %rcx
	movq	%rcx, (%rdx)
	xorl	%edx, %edx
	movw	%dx, -9440(%rbp,%rsi,2)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L667:
	leaq	-768(%rbp), %r13
	xorl	%eax, %eax
	movl	$88, %ecx
	movl	$15, %edx
	movq	%r13, %rdi
	movl	$704, %esi
	movq	%r13, %r8
	rep stosq
	movq	-9512(%rbp), %rdi
	movq	%r14, %rcx
	call	BrotliCreateHuffmanTree
	movq	-9528(%rbp), %rdx
	movl	$704, %esi
	movq	%r13, %rdi
	call	BrotliConvertBitDepthsToSymbols
	cmpq	$4, %r12
	ja	.L619
	leaq	-9440(%rbp), %rax
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r12, %rdx
	movl	$10, %ecx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rax, -9512(%rbp)
	call	StoreSimpleHuffmanTree
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L636:
	xorl	%r10d, %r10d
	jmp	.L627
.L671:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE136:
	.size	BrotliStoreMetaBlockTrivial, .-BrotliStoreMetaBlockTrivial
	.p2align 4
	.globl	BrotliStoreMetaBlockFast
	.hidden	BrotliStoreMetaBlockFast
	.type	BrotliStoreMetaBlockFast, @function
BrotliStoreMetaBlockFast:
.LFB137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1304, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %r15
	movq	16(%rbp), %rax
	movq	40(%rbp), %rbx
	movq	48(%rbp), %r14
	movq	%rdi, -9456(%rbp)
	movq	%rsi, %r12
	movq	%rcx, %rsi
	movl	%r9d, %edi
	movq	%rdx, -9448(%rbp)
	movq	%rbx, %rdx
	movq	%r8, %r13
	movl	%r9d, -9468(%rbp)
	movq	%r15, -9464(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	64(%rax), %ecx
	movl	%ecx, -9488(%rbp)
	movq	%r14, %rcx
	call	StoreCompressedMetaBlockHeader
	movq	(%rbx), %rax
	movq	%rax, %rsi
	addq	$13, %rax
	shrq	$3, %rsi
	movq	%rax, (%rbx)
	movq	%rsi, %rdx
	addq	%r14, %rdx
	cmpq	$128, 32(%rbp)
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	ja	.L673
	leaq	-4000(%rbp), %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	cmpq	$0, 32(%rbp)
	movq	%rdi, -9480(%rbp)
	rep stosq
	je	.L687
	movq	32(%rbp), %r9
	movq	-9448(%rbp), %rax
	movq	%r15, %rdi
	xorl	%r10d, %r10d
	salq	$4, %r9
	addq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L677:
	movl	(%rdi), %r8d
	movl	4(%rdi), %esi
	testq	%r8, %r8
	je	.L688
	leaq	(%r8,%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L676:
	movq	%r13, %rdx
	andq	%rax, %rdx
	addq	$1, %rax
	movzbl	(%r12,%rdx), %edx
	addl	$1, -4000(%rbp,%rdx,4)
	cmpq	%rcx, %rax
	jne	.L676
.L675:
	movl	%esi, %eax
	addq	$16, %rdi
	addq	%r8, %r10
	andl	$33554431, %eax
	addq	%rcx, %rax
	cmpq	%rdi, %r9
	jne	.L677
.L674:
	pushq	%r14
	movq	-9480(%rbp), %rsi
	leaq	-768(%rbp), %r15
	leaq	-6192(%rbp), %r9
	pushq	%rbx
	movq	-9456(%rbp), %rdi
	movl	$8, %ecx
	movq	%r10, %rdx
	movq	%r15, %r8
	movq	%r9, -9488(%rbp)
	call	BrotliBuildAndStoreHuffmanTreeFast
	movq	(%rbx), %rax
	movq	-9488(%rbp), %r9
	movabsq	$41203391169327107, %rdx
	movq	%rax, %rsi
	movl	%eax, %ecx
	shrq	$3, %rsi
	andl	$7, %ecx
	addq	%r14, %rsi
	salq	%cl, %rdx
	movzbl	(%rsi), %edi
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	leaq	56(%rax), %rdx
	movq	%rdx, (%rbx)
	shrq	$3, %rdx
	addq	%r14, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	leaq	59(%rax), %rcx
	movl	$57269251, %edx
	addq	$87, %rax
	movq	%rcx, %rsi
	movq	%rcx, (%rbx)
	andl	$7, %ecx
	shrq	$3, %rsi
	salq	%cl, %rdx
	movq	%rax, (%rbx)
	leaq	kStaticDistanceCodeBits(%rip), %rax
	addq	%r14, %rsi
	movzbl	(%rsi), %edi
	orq	%rdi, %rdx
	movq	%rdx, (%rsi)
	movq	%r14, (%rsp)
	pushq	%rbx
	pushq	%rax
	leaq	kStaticDistanceCodeDepth(%rip), %rax
	pushq	%rax
	leaq	kStaticCommandCodeBits(%rip), %rax
	pushq	%rax
	leaq	kStaticCommandCodeDepth(%rip), %rax
	pushq	%rax
	pushq	%r9
.L705:
	movq	32(%rbp), %r8
	movq	%r15, %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-9464(%rbp), %rcx
	movq	-9448(%rbp), %rsi
	call	StoreDataWithHuffmanCodes
	movl	-9468(%rbp), %eax
	addq	$64, %rsp
	testl	%eax, %eax
	je	.L672
	movq	(%rbx), %rax
	addq	$7, %rax
	andl	$4294967288, %eax
	movq	%rax, (%rbx)
	shrq	$3, %rax
	movb	$0, (%r14,%rax)
.L672:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L706
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movq	%rax, %rcx
	jmp	.L675
.L673:
	xorl	%eax, %eax
	leaq	-8640(%rbp), %rdi
	movl	$128, %ecx
	xorl	%edx, %edx
	movq	%rdi, -9520(%rbp)
	movq	32(%rbp), %r15
	xorl	%r10d, %r10d
	rep stosq
	leaq	-4000(%rbp), %rdi
	movl	$352, %ecx
	movsd	.LC12(%rip), %xmm0
	movq	%rdi, -9480(%rbp)
	salq	$4, %r15
	rep stosq
	movl	$272, %ecx
	leaq	-6192(%rbp), %rdi
	movq	$0, -7616(%rbp)
	movq	%rdi, -9512(%rbp)
	rep stosq
	movq	-9464(%rbp), %rax
	movq	$0, -1184(%rbp)
	movq	$0, -4016(%rbp)
	movb	$0, -9496(%rbp)
	movq	%rax, %rsi
	addq	%rax, %r15
	movq	-9448(%rbp), %rax
	movq	$0, -9504(%rbp)
	movsd	%xmm0, -7608(%rbp)
	movsd	%xmm0, -1176(%rbp)
	movsd	%xmm0, -4008(%rbp)
	.p2align 4,,10
	.p2align 3
.L682:
	movzwl	12(%rsi), %ecx
	movl	(%rsi), %r8d
	movl	4(%rsi), %edi
	movzwl	14(%rsi), %r11d
	addl	$1, -4000(%rbp,%rcx,4)
	movq	%rcx, %r9
	testq	%r8, %r8
	je	.L689
	leaq	(%r8,%rax), %rcx
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%r13, %rdx
	andq	%rax, %rdx
	addq	$1, %rax
	movzbl	(%r12,%rdx), %edx
	addl	$1, -8640(%rbp,%rdx,4)
	cmpq	%rcx, %rax
	jne	.L680
	addq	%r8, %r10
	movl	$1, %edx
.L679:
	andl	$33554431, %edi
	movl	%edi, %eax
	addq	%rcx, %rax
	testl	%edi, %edi
	setne	%cl
	cmpw	$127, %r9w
	seta	%dil
	andb	%dil, %cl
	jne	.L707
.L681:
	addq	$16, %rsi
	cmpq	%rsi, %r15
	jne	.L682
	movq	32(%rbp), %rax
	movq	%rax, -1184(%rbp)
	testb	%dl, %dl
	je	.L690
	movq	%r10, -7616(%rbp)
.L683:
	cmpb	$0, -9496(%rbp)
	je	.L684
	movq	-9504(%rbp), %rax
	movq	%rax, -4016(%rbp)
.L684:
	pushq	%r14
	leaq	-9152(%rbp), %r11
	movq	-9520(%rbp), %rsi
	leaq	-1024(%rbp), %r15
	pushq	%rbx
	movq	-9456(%rbp), %rdi
	movq	%r11, %r9
	movq	%r10, %rdx
	movq	%r15, %r8
	movl	$8, %ecx
	movq	%r11, -9528(%rbp)
	call	BrotliBuildAndStoreHuffmanTreeFast
	pushq	%r14
	leaq	-7600(%rbp), %rax
	leaq	-768(%rbp), %r10
	pushq	%rbx
	movq	-1184(%rbp), %rdx
	movq	%rax, %r9
	movq	%r10, %r8
	movq	-9480(%rbp), %rsi
	movq	-9456(%rbp), %rdi
	movl	$10, %ecx
	movq	%rax, -9504(%rbp)
	movq	%r10, -9496(%rbp)
	call	BrotliBuildAndStoreHuffmanTreeFast
	movl	-9488(%rbp), %edx
	addq	$32, %rsp
	movq	-9512(%rbp), %rsi
	pushq	%r14
	movq	-9456(%rbp), %rdi
	leaq	-9440(%rbp), %r9
	leaq	-1168(%rbp), %r8
	subl	$1, %edx
	pushq	%rbx
	bsrl	%edx, %edx
	movq	%r9, -9488(%rbp)
	leal	1(%rdx), %ecx
	movq	-4016(%rbp), %rdx
	movq	%r8, -9480(%rbp)
	call	BrotliBuildAndStoreHuffmanTreeFast
	movq	-9488(%rbp), %r9
	movq	%r14, (%rsp)
	movq	-9480(%rbp), %r8
	movq	-9504(%rbp), %rax
	pushq	%rbx
	movq	-9496(%rbp), %r10
	movq	-9528(%rbp), %r11
	pushq	%r9
	pushq	%r8
	pushq	%rax
	pushq	%r10
	pushq	%r11
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L707:
	andl	$1023, %r11d
	addq	$1, -9504(%rbp)
	addl	$1, -6192(%rbp,%r11,4)
	movb	%cl, -9496(%rbp)
	jmp	.L681
.L689:
	movq	%rax, %rcx
	jmp	.L679
.L690:
	xorl	%r10d, %r10d
	jmp	.L683
.L687:
	xorl	%r10d, %r10d
	jmp	.L674
.L706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE137:
	.size	BrotliStoreMetaBlockFast, .-BrotliStoreMetaBlockFast
	.p2align 4
	.globl	BrotliStoreUncompressedMetaBlock
	.hidden	BrotliStoreUncompressedMetaBlock
	.type	BrotliStoreUncompressedMetaBlock, @function
BrotliStoreUncompressedMetaBlock:
.LFB138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andq	%rcx, %rdx
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	movq	%rdx, %r8
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%r9), %rbx
	movq	16(%rbp), %r14
	movl	%edi, -52(%rbp)
	movq	%rbx, %rdx
	leaq	1(%rbx), %rsi
	shrq	$3, %rdx
	addq	%r14, %rdx
	movzbl	(%rdx), %ecx
	movq	%rcx, (%rdx)
	cmpq	$1, %r12
	je	.L714
	leal	-1(%r12), %edx
	bsrl	%edx, %edx
	addl	$1, %edx
	cmpl	$15, %edx
	ja	.L716
.L714:
	xorl	%edx, %edx
	movl	$16, %edi
.L709:
	shrq	$3, %rsi
	addq	$3, %rbx
	addq	%r14, %rsi
	movzbl	(%rsi), %ecx
	orq	%rcx, %rdx
	movl	%ebx, %ecx
	movq	%rdx, (%rsi)
	movq	%rbx, %rsi
	andl	$7, %ecx
	leaq	-1(%r12), %rdx
	shrq	$3, %rsi
	salq	%cl, %rdx
	addq	%rdi, %rbx
	addq	%r14, %rsi
	movzbl	(%rsi), %r10d
	orq	%r10, %rdx
	movq	%rdx, (%rsi)
	movq	%rbx, %rdx
	movl	%ebx, %esi
	addq	$8, %rbx
	shrq	$3, %rdx
	andl	$4294967288, %ebx
	andl	$7, %esi
	addq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rbx, 0(%r13)
	movzbl	(%rdx), %ecx
	shrq	$3, %rdi
	addq	%r14, %rdi
	btsq	%rsi, %rcx
	leaq	(%r15,%r8), %rsi
	movq	%rcx, (%rdx)
	leaq	1(%rax), %rcx
	leaq	(%r8,%r12), %rax
	movb	$0, (%rdi)
	cmpq	%rcx, %rax
	ja	.L717
.L710:
	movq	%r12, %rdx
	call	memcpy@PLT
	leaq	(%rbx,%r12,8), %rax
	movl	-52(%rbp), %ecx
	movq	%rax, %rdx
	movq	%rax, 0(%r13)
	shrq	$3, %rdx
	addq	%r14, %rdx
	testl	%ecx, %ecx
	jne	.L711
	movb	$0, (%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L711:
	.cfi_restore_state
	movl	%eax, %ecx
	movl	$1, %esi
	andl	$7, %ecx
	salq	%cl, %rsi
	leaq	1(%rax), %rcx
	addq	$9, %rax
	movq	%rsi, (%rdx)
	movq	%rcx, %rdx
	andl	$4294967288, %eax
	andl	$7, %ecx
	shrq	$3, %rdx
	movq	%rax, 0(%r13)
	shrq	$3, %rax
	addq	%r14, %rdx
	movzbl	(%rdx), %esi
	btsq	%rcx, %rsi
	movq	%rsi, (%rdx)
	movb	$0, (%r14,%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	addq	$3, %rdx
	movl	%esi, %ecx
	shrq	$2, %rdx
	andl	$7, %ecx
	leaq	0(,%rdx,4), %rdi
	subq	$4, %rdx
	salq	%cl, %rdx
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%rcx, %rdx
	movq	%rcx, -80(%rbp)
	subq	%r8, %rdx
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%r15, %rsi
	movq	-80(%rbp), %rcx
	leaq	(%rbx,%rdx,8), %rbx
	movq	%rbx, %rdi
	subq	%rcx, %r8
	shrq	$3, %rdi
	addq	%r8, %r12
	addq	%r14, %rdi
	jmp	.L710
	.cfi_endproc
.LFE138:
	.size	BrotliStoreUncompressedMetaBlock, .-BrotliStoreUncompressedMetaBlock
	.section	.rodata
	.align 32
	.type	gaps.4286, @object
	.size	gaps.4286, 48
gaps.4286:
	.quad	132
	.quad	57
	.quad	23
	.quad	10
	.quad	4
	.quad	1
	.type	kHuffmanBitLengthHuffmanCodeBitLengths.4755, @object
	.size	kHuffmanBitLengthHuffmanCodeBitLengths.4755, 6
kHuffmanBitLengthHuffmanCodeBitLengths.4755:
	.ascii	"\002\004\003\002\002\004"
	.type	kHuffmanBitLengthHuffmanCodeSymbols.4754, @object
	.size	kHuffmanBitLengthHuffmanCodeSymbols.4754, 6
kHuffmanBitLengthHuffmanCodeSymbols.4754:
	.string	""
	.ascii	"\007\003\002\001\017"
	.align 16
	.type	kStorageOrder.4753, @object
	.size	kStorageOrder.4753, 18
kStorageOrder.4753:
	.string	"\001\002\003\004"
	.ascii	"\005\021\006\020\007\b\t\n\013\f\r\016\017"
	.align 32
	.type	kBlockLengthPrefixCode, @object
	.size	kBlockLengthPrefixCode, 208
kBlockLengthPrefixCode:
	.long	1
	.long	2
	.long	5
	.long	2
	.long	9
	.long	2
	.long	13
	.long	2
	.long	17
	.long	3
	.long	25
	.long	3
	.long	33
	.long	3
	.long	41
	.long	3
	.long	49
	.long	4
	.long	65
	.long	4
	.long	81
	.long	4
	.long	97
	.long	4
	.long	113
	.long	5
	.long	145
	.long	5
	.long	177
	.long	5
	.long	209
	.long	5
	.long	241
	.long	6
	.long	305
	.long	6
	.long	369
	.long	7
	.long	497
	.long	8
	.long	753
	.long	9
	.long	1265
	.long	10
	.long	2289
	.long	11
	.long	4337
	.long	12
	.long	8433
	.long	13
	.long	16625
	.long	24
	.align 32
	.type	kStaticDistanceCodeBits, @object
	.size	kStaticDistanceCodeBits, 128
kStaticDistanceCodeBits:
	.value	0
	.value	32
	.value	16
	.value	48
	.value	8
	.value	40
	.value	24
	.value	56
	.value	4
	.value	36
	.value	20
	.value	52
	.value	12
	.value	44
	.value	28
	.value	60
	.value	2
	.value	34
	.value	18
	.value	50
	.value	10
	.value	42
	.value	26
	.value	58
	.value	6
	.value	38
	.value	22
	.value	54
	.value	14
	.value	46
	.value	30
	.value	62
	.value	1
	.value	33
	.value	17
	.value	49
	.value	9
	.value	41
	.value	25
	.value	57
	.value	5
	.value	37
	.value	21
	.value	53
	.value	13
	.value	45
	.value	29
	.value	61
	.value	3
	.value	35
	.value	19
	.value	51
	.value	11
	.value	43
	.value	27
	.value	59
	.value	7
	.value	39
	.value	23
	.value	55
	.value	15
	.value	47
	.value	31
	.value	63
	.align 32
	.type	kStaticCommandCodeBits, @object
	.size	kStaticCommandCodeBits, 1408
kStaticCommandCodeBits:
	.value	0
	.value	256
	.value	128
	.value	384
	.value	64
	.value	320
	.value	192
	.value	448
	.value	32
	.value	288
	.value	160
	.value	416
	.value	96
	.value	352
	.value	224
	.value	480
	.value	16
	.value	272
	.value	144
	.value	400
	.value	80
	.value	336
	.value	208
	.value	464
	.value	48
	.value	304
	.value	176
	.value	432
	.value	112
	.value	368
	.value	240
	.value	496
	.value	8
	.value	264
	.value	136
	.value	392
	.value	72
	.value	328
	.value	200
	.value	456
	.value	40
	.value	296
	.value	168
	.value	424
	.value	104
	.value	360
	.value	232
	.value	488
	.value	24
	.value	280
	.value	152
	.value	408
	.value	88
	.value	344
	.value	216
	.value	472
	.value	56
	.value	312
	.value	184
	.value	440
	.value	120
	.value	376
	.value	248
	.value	504
	.value	4
	.value	260
	.value	132
	.value	388
	.value	68
	.value	324
	.value	196
	.value	452
	.value	36
	.value	292
	.value	164
	.value	420
	.value	100
	.value	356
	.value	228
	.value	484
	.value	20
	.value	276
	.value	148
	.value	404
	.value	84
	.value	340
	.value	212
	.value	468
	.value	52
	.value	308
	.value	180
	.value	436
	.value	116
	.value	372
	.value	244
	.value	500
	.value	12
	.value	268
	.value	140
	.value	396
	.value	76
	.value	332
	.value	204
	.value	460
	.value	44
	.value	300
	.value	172
	.value	428
	.value	108
	.value	364
	.value	236
	.value	492
	.value	28
	.value	284
	.value	156
	.value	412
	.value	92
	.value	348
	.value	220
	.value	476
	.value	60
	.value	316
	.value	188
	.value	444
	.value	124
	.value	380
	.value	252
	.value	508
	.value	2
	.value	258
	.value	130
	.value	386
	.value	66
	.value	322
	.value	194
	.value	450
	.value	34
	.value	290
	.value	162
	.value	418
	.value	98
	.value	354
	.value	226
	.value	482
	.value	18
	.value	274
	.value	146
	.value	402
	.value	82
	.value	338
	.value	210
	.value	466
	.value	50
	.value	306
	.value	178
	.value	434
	.value	114
	.value	370
	.value	242
	.value	498
	.value	10
	.value	266
	.value	138
	.value	394
	.value	74
	.value	330
	.value	202
	.value	458
	.value	42
	.value	298
	.value	170
	.value	426
	.value	106
	.value	362
	.value	234
	.value	490
	.value	26
	.value	282
	.value	154
	.value	410
	.value	90
	.value	346
	.value	218
	.value	474
	.value	58
	.value	314
	.value	186
	.value	442
	.value	122
	.value	378
	.value	250
	.value	506
	.value	6
	.value	262
	.value	134
	.value	390
	.value	70
	.value	326
	.value	198
	.value	454
	.value	38
	.value	294
	.value	166
	.value	422
	.value	102
	.value	358
	.value	230
	.value	486
	.value	22
	.value	278
	.value	150
	.value	406
	.value	86
	.value	342
	.value	214
	.value	470
	.value	54
	.value	310
	.value	182
	.value	438
	.value	118
	.value	374
	.value	246
	.value	502
	.value	14
	.value	270
	.value	142
	.value	398
	.value	78
	.value	334
	.value	206
	.value	462
	.value	46
	.value	302
	.value	174
	.value	430
	.value	110
	.value	366
	.value	238
	.value	494
	.value	30
	.value	286
	.value	158
	.value	414
	.value	94
	.value	350
	.value	222
	.value	478
	.value	62
	.value	318
	.value	190
	.value	446
	.value	126
	.value	382
	.value	254
	.value	510
	.value	1
	.value	257
	.value	129
	.value	385
	.value	65
	.value	321
	.value	193
	.value	449
	.value	33
	.value	289
	.value	161
	.value	417
	.value	97
	.value	353
	.value	225
	.value	481
	.value	17
	.value	273
	.value	145
	.value	401
	.value	81
	.value	337
	.value	209
	.value	465
	.value	49
	.value	305
	.value	177
	.value	433
	.value	113
	.value	369
	.value	241
	.value	497
	.value	9
	.value	265
	.value	137
	.value	393
	.value	73
	.value	329
	.value	201
	.value	457
	.value	41
	.value	297
	.value	169
	.value	425
	.value	105
	.value	361
	.value	233
	.value	489
	.value	25
	.value	281
	.value	153
	.value	409
	.value	89
	.value	345
	.value	217
	.value	473
	.value	57
	.value	313
	.value	185
	.value	441
	.value	121
	.value	377
	.value	249
	.value	505
	.value	5
	.value	261
	.value	133
	.value	389
	.value	69
	.value	325
	.value	197
	.value	453
	.value	37
	.value	293
	.value	165
	.value	421
	.value	101
	.value	357
	.value	229
	.value	485
	.value	21
	.value	277
	.value	149
	.value	405
	.value	85
	.value	341
	.value	213
	.value	469
	.value	53
	.value	309
	.value	181
	.value	437
	.value	117
	.value	373
	.value	245
	.value	501
	.value	13
	.value	269
	.value	141
	.value	397
	.value	77
	.value	333
	.value	205
	.value	461
	.value	45
	.value	301
	.value	173
	.value	429
	.value	109
	.value	365
	.value	237
	.value	493
	.value	29
	.value	285
	.value	157
	.value	413
	.value	93
	.value	349
	.value	221
	.value	477
	.value	61
	.value	317
	.value	189
	.value	445
	.value	125
	.value	381
	.value	253
	.value	509
	.value	3
	.value	259
	.value	131
	.value	387
	.value	67
	.value	323
	.value	195
	.value	451
	.value	35
	.value	291
	.value	163
	.value	419
	.value	99
	.value	355
	.value	227
	.value	483
	.value	19
	.value	275
	.value	147
	.value	403
	.value	83
	.value	339
	.value	211
	.value	467
	.value	51
	.value	307
	.value	179
	.value	435
	.value	115
	.value	371
	.value	243
	.value	499
	.value	11
	.value	267
	.value	139
	.value	395
	.value	75
	.value	331
	.value	203
	.value	459
	.value	43
	.value	299
	.value	171
	.value	427
	.value	107
	.value	363
	.value	235
	.value	491
	.value	27
	.value	283
	.value	155
	.value	411
	.value	91
	.value	347
	.value	219
	.value	475
	.value	59
	.value	315
	.value	187
	.value	443
	.value	123
	.value	379
	.value	251
	.value	507
	.value	7
	.value	1031
	.value	519
	.value	1543
	.value	263
	.value	1287
	.value	775
	.value	1799
	.value	135
	.value	1159
	.value	647
	.value	1671
	.value	391
	.value	1415
	.value	903
	.value	1927
	.value	71
	.value	1095
	.value	583
	.value	1607
	.value	327
	.value	1351
	.value	839
	.value	1863
	.value	199
	.value	1223
	.value	711
	.value	1735
	.value	455
	.value	1479
	.value	967
	.value	1991
	.value	39
	.value	1063
	.value	551
	.value	1575
	.value	295
	.value	1319
	.value	807
	.value	1831
	.value	167
	.value	1191
	.value	679
	.value	1703
	.value	423
	.value	1447
	.value	935
	.value	1959
	.value	103
	.value	1127
	.value	615
	.value	1639
	.value	359
	.value	1383
	.value	871
	.value	1895
	.value	231
	.value	1255
	.value	743
	.value	1767
	.value	487
	.value	1511
	.value	999
	.value	2023
	.value	23
	.value	1047
	.value	535
	.value	1559
	.value	279
	.value	1303
	.value	791
	.value	1815
	.value	151
	.value	1175
	.value	663
	.value	1687
	.value	407
	.value	1431
	.value	919
	.value	1943
	.value	87
	.value	1111
	.value	599
	.value	1623
	.value	343
	.value	1367
	.value	855
	.value	1879
	.value	215
	.value	1239
	.value	727
	.value	1751
	.value	471
	.value	1495
	.value	983
	.value	2007
	.value	55
	.value	1079
	.value	567
	.value	1591
	.value	311
	.value	1335
	.value	823
	.value	1847
	.value	183
	.value	1207
	.value	695
	.value	1719
	.value	439
	.value	1463
	.value	951
	.value	1975
	.value	119
	.value	1143
	.value	631
	.value	1655
	.value	375
	.value	1399
	.value	887
	.value	1911
	.value	247
	.value	1271
	.value	759
	.value	1783
	.value	503
	.value	1527
	.value	1015
	.value	2039
	.value	15
	.value	1039
	.value	527
	.value	1551
	.value	271
	.value	1295
	.value	783
	.value	1807
	.value	143
	.value	1167
	.value	655
	.value	1679
	.value	399
	.value	1423
	.value	911
	.value	1935
	.value	79
	.value	1103
	.value	591
	.value	1615
	.value	335
	.value	1359
	.value	847
	.value	1871
	.value	207
	.value	1231
	.value	719
	.value	1743
	.value	463
	.value	1487
	.value	975
	.value	1999
	.value	47
	.value	1071
	.value	559
	.value	1583
	.value	303
	.value	1327
	.value	815
	.value	1839
	.value	175
	.value	1199
	.value	687
	.value	1711
	.value	431
	.value	1455
	.value	943
	.value	1967
	.value	111
	.value	1135
	.value	623
	.value	1647
	.value	367
	.value	1391
	.value	879
	.value	1903
	.value	239
	.value	1263
	.value	751
	.value	1775
	.value	495
	.value	1519
	.value	1007
	.value	2031
	.value	31
	.value	1055
	.value	543
	.value	1567
	.value	287
	.value	1311
	.value	799
	.value	1823
	.value	159
	.value	1183
	.value	671
	.value	1695
	.value	415
	.value	1439
	.value	927
	.value	1951
	.value	95
	.value	1119
	.value	607
	.value	1631
	.value	351
	.value	1375
	.value	863
	.value	1887
	.value	223
	.value	1247
	.value	735
	.value	1759
	.value	479
	.value	1503
	.value	991
	.value	2015
	.value	63
	.value	1087
	.value	575
	.value	1599
	.value	319
	.value	1343
	.value	831
	.value	1855
	.value	191
	.value	1215
	.value	703
	.value	1727
	.value	447
	.value	1471
	.value	959
	.value	1983
	.value	127
	.value	1151
	.value	639
	.value	1663
	.value	383
	.value	1407
	.value	895
	.value	1919
	.value	255
	.value	1279
	.value	767
	.value	1791
	.value	511
	.value	1535
	.value	1023
	.value	2047
	.align 32
	.type	kNonZeroRepsDepth, @object
	.size	kNonZeroRepsDepth, 2816
kNonZeroRepsDepth:
	.long	6
	.long	6
	.long	6
	.long	6
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	12
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	18
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	24
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.long	30
	.align 32
	.type	kNonZeroRepsBits, @object
	.size	kNonZeroRepsBits, 5632
kNonZeroRepsBits:
	.quad	11
	.quad	27
	.quad	43
	.quad	59
	.quad	715
	.quad	1739
	.quad	2763
	.quad	3787
	.quad	731
	.quad	1755
	.quad	2779
	.quad	3803
	.quad	747
	.quad	1771
	.quad	2795
	.quad	3819
	.quad	763
	.quad	1787
	.quad	2811
	.quad	3835
	.quad	45771
	.quad	111307
	.quad	176843
	.quad	242379
	.quad	46795
	.quad	112331
	.quad	177867
	.quad	243403
	.quad	47819
	.quad	113355
	.quad	178891
	.quad	244427
	.quad	48843
	.quad	114379
	.quad	179915
	.quad	245451
	.quad	45787
	.quad	111323
	.quad	176859
	.quad	242395
	.quad	46811
	.quad	112347
	.quad	177883
	.quad	243419
	.quad	47835
	.quad	113371
	.quad	178907
	.quad	244443
	.quad	48859
	.quad	114395
	.quad	179931
	.quad	245467
	.quad	45803
	.quad	111339
	.quad	176875
	.quad	242411
	.quad	46827
	.quad	112363
	.quad	177899
	.quad	243435
	.quad	47851
	.quad	113387
	.quad	178923
	.quad	244459
	.quad	48875
	.quad	114411
	.quad	179947
	.quad	245483
	.quad	45819
	.quad	111355
	.quad	176891
	.quad	242427
	.quad	46843
	.quad	112379
	.quad	177915
	.quad	243451
	.quad	47867
	.quad	113403
	.quad	178939
	.quad	244475
	.quad	48891
	.quad	114427
	.quad	179963
	.quad	245499
	.quad	2929355
	.quad	7123659
	.quad	11317963
	.quad	15512267
	.quad	2994891
	.quad	7189195
	.quad	11383499
	.quad	15577803
	.quad	3060427
	.quad	7254731
	.quad	11449035
	.quad	15643339
	.quad	3125963
	.quad	7320267
	.quad	11514571
	.quad	15708875
	.quad	2930379
	.quad	7124683
	.quad	11318987
	.quad	15513291
	.quad	2995915
	.quad	7190219
	.quad	11384523
	.quad	15578827
	.quad	3061451
	.quad	7255755
	.quad	11450059
	.quad	15644363
	.quad	3126987
	.quad	7321291
	.quad	11515595
	.quad	15709899
	.quad	2931403
	.quad	7125707
	.quad	11320011
	.quad	15514315
	.quad	2996939
	.quad	7191243
	.quad	11385547
	.quad	15579851
	.quad	3062475
	.quad	7256779
	.quad	11451083
	.quad	15645387
	.quad	3128011
	.quad	7322315
	.quad	11516619
	.quad	15710923
	.quad	2932427
	.quad	7126731
	.quad	11321035
	.quad	15515339
	.quad	2997963
	.quad	7192267
	.quad	11386571
	.quad	15580875
	.quad	3063499
	.quad	7257803
	.quad	11452107
	.quad	15646411
	.quad	3129035
	.quad	7323339
	.quad	11517643
	.quad	15711947
	.quad	2929371
	.quad	7123675
	.quad	11317979
	.quad	15512283
	.quad	2994907
	.quad	7189211
	.quad	11383515
	.quad	15577819
	.quad	3060443
	.quad	7254747
	.quad	11449051
	.quad	15643355
	.quad	3125979
	.quad	7320283
	.quad	11514587
	.quad	15708891
	.quad	2930395
	.quad	7124699
	.quad	11319003
	.quad	15513307
	.quad	2995931
	.quad	7190235
	.quad	11384539
	.quad	15578843
	.quad	3061467
	.quad	7255771
	.quad	11450075
	.quad	15644379
	.quad	3127003
	.quad	7321307
	.quad	11515611
	.quad	15709915
	.quad	2931419
	.quad	7125723
	.quad	11320027
	.quad	15514331
	.quad	2996955
	.quad	7191259
	.quad	11385563
	.quad	15579867
	.quad	3062491
	.quad	7256795
	.quad	11451099
	.quad	15645403
	.quad	3128027
	.quad	7322331
	.quad	11516635
	.quad	15710939
	.quad	2932443
	.quad	7126747
	.quad	11321051
	.quad	15515355
	.quad	2997979
	.quad	7192283
	.quad	11386587
	.quad	15580891
	.quad	3063515
	.quad	7257819
	.quad	11452123
	.quad	15646427
	.quad	3129051
	.quad	7323355
	.quad	11517659
	.quad	15711963
	.quad	2929387
	.quad	7123691
	.quad	11317995
	.quad	15512299
	.quad	2994923
	.quad	7189227
	.quad	11383531
	.quad	15577835
	.quad	3060459
	.quad	7254763
	.quad	11449067
	.quad	15643371
	.quad	3125995
	.quad	7320299
	.quad	11514603
	.quad	15708907
	.quad	2930411
	.quad	7124715
	.quad	11319019
	.quad	15513323
	.quad	2995947
	.quad	7190251
	.quad	11384555
	.quad	15578859
	.quad	3061483
	.quad	7255787
	.quad	11450091
	.quad	15644395
	.quad	3127019
	.quad	7321323
	.quad	11515627
	.quad	15709931
	.quad	2931435
	.quad	7125739
	.quad	11320043
	.quad	15514347
	.quad	2996971
	.quad	7191275
	.quad	11385579
	.quad	15579883
	.quad	3062507
	.quad	7256811
	.quad	11451115
	.quad	15645419
	.quad	3128043
	.quad	7322347
	.quad	11516651
	.quad	15710955
	.quad	2932459
	.quad	7126763
	.quad	11321067
	.quad	15515371
	.quad	2997995
	.quad	7192299
	.quad	11386603
	.quad	15580907
	.quad	3063531
	.quad	7257835
	.quad	11452139
	.quad	15646443
	.quad	3129067
	.quad	7323371
	.quad	11517675
	.quad	15711979
	.quad	2929403
	.quad	7123707
	.quad	11318011
	.quad	15512315
	.quad	2994939
	.quad	7189243
	.quad	11383547
	.quad	15577851
	.quad	3060475
	.quad	7254779
	.quad	11449083
	.quad	15643387
	.quad	3126011
	.quad	7320315
	.quad	11514619
	.quad	15708923
	.quad	2930427
	.quad	7124731
	.quad	11319035
	.quad	15513339
	.quad	2995963
	.quad	7190267
	.quad	11384571
	.quad	15578875
	.quad	3061499
	.quad	7255803
	.quad	11450107
	.quad	15644411
	.quad	3127035
	.quad	7321339
	.quad	11515643
	.quad	15709947
	.quad	2931451
	.quad	7125755
	.quad	11320059
	.quad	15514363
	.quad	2996987
	.quad	7191291
	.quad	11385595
	.quad	15579899
	.quad	3062523
	.quad	7256827
	.quad	11451131
	.quad	15645435
	.quad	3128059
	.quad	7322363
	.quad	11516667
	.quad	15710971
	.quad	2932475
	.quad	7126779
	.quad	11321083
	.quad	15515387
	.quad	2998011
	.quad	7192315
	.quad	11386619
	.quad	15580923
	.quad	3063547
	.quad	7257851
	.quad	11452155
	.quad	15646459
	.quad	3129083
	.quad	7323387
	.quad	11517691
	.quad	15711995
	.quad	187478731
	.quad	455914187
	.quad	724349643
	.quad	992785099
	.quad	191673035
	.quad	460108491
	.quad	728543947
	.quad	996979403
	.quad	195867339
	.quad	464302795
	.quad	732738251
	.quad	1001173707
	.quad	200061643
	.quad	468497099
	.quad	736932555
	.quad	1005368011
	.quad	187544267
	.quad	455979723
	.quad	724415179
	.quad	992850635
	.quad	191738571
	.quad	460174027
	.quad	728609483
	.quad	997044939
	.quad	195932875
	.quad	464368331
	.quad	732803787
	.quad	1001239243
	.quad	200127179
	.quad	468562635
	.quad	736998091
	.quad	1005433547
	.quad	187609803
	.quad	456045259
	.quad	724480715
	.quad	992916171
	.quad	191804107
	.quad	460239563
	.quad	728675019
	.quad	997110475
	.quad	195998411
	.quad	464433867
	.quad	732869323
	.quad	1001304779
	.quad	200192715
	.quad	468628171
	.quad	737063627
	.quad	1005499083
	.quad	187675339
	.quad	456110795
	.quad	724546251
	.quad	992981707
	.quad	191869643
	.quad	460305099
	.quad	728740555
	.quad	997176011
	.quad	196063947
	.quad	464499403
	.quad	732934859
	.quad	1001370315
	.quad	200258251
	.quad	468693707
	.quad	737129163
	.quad	1005564619
	.quad	187479755
	.quad	455915211
	.quad	724350667
	.quad	992786123
	.quad	191674059
	.quad	460109515
	.quad	728544971
	.quad	996980427
	.quad	195868363
	.quad	464303819
	.quad	732739275
	.quad	1001174731
	.quad	200062667
	.quad	468498123
	.quad	736933579
	.quad	1005369035
	.quad	187545291
	.quad	455980747
	.quad	724416203
	.quad	992851659
	.quad	191739595
	.quad	460175051
	.quad	728610507
	.quad	997045963
	.quad	195933899
	.quad	464369355
	.quad	732804811
	.quad	1001240267
	.quad	200128203
	.quad	468563659
	.quad	736999115
	.quad	1005434571
	.quad	187610827
	.quad	456046283
	.quad	724481739
	.quad	992917195
	.quad	191805131
	.quad	460240587
	.quad	728676043
	.quad	997111499
	.quad	195999435
	.quad	464434891
	.quad	732870347
	.quad	1001305803
	.quad	200193739
	.quad	468629195
	.quad	737064651
	.quad	1005500107
	.quad	187676363
	.quad	456111819
	.quad	724547275
	.quad	992982731
	.quad	191870667
	.quad	460306123
	.quad	728741579
	.quad	997177035
	.quad	196064971
	.quad	464500427
	.quad	732935883
	.quad	1001371339
	.quad	200259275
	.quad	468694731
	.quad	737130187
	.quad	1005565643
	.quad	187480779
	.quad	455916235
	.quad	724351691
	.quad	992787147
	.quad	191675083
	.quad	460110539
	.quad	728545995
	.quad	996981451
	.quad	195869387
	.quad	464304843
	.quad	732740299
	.quad	1001175755
	.quad	200063691
	.quad	468499147
	.quad	736934603
	.quad	1005370059
	.quad	187546315
	.quad	455981771
	.quad	724417227
	.quad	992852683
	.quad	191740619
	.quad	460176075
	.quad	728611531
	.quad	997046987
	.quad	195934923
	.quad	464370379
	.quad	732805835
	.quad	1001241291
	.quad	200129227
	.quad	468564683
	.quad	737000139
	.quad	1005435595
	.quad	187611851
	.quad	456047307
	.quad	724482763
	.quad	992918219
	.quad	191806155
	.quad	460241611
	.quad	728677067
	.quad	997112523
	.quad	196000459
	.quad	464435915
	.quad	732871371
	.quad	1001306827
	.quad	200194763
	.quad	468630219
	.quad	737065675
	.quad	1005501131
	.quad	187677387
	.quad	456112843
	.quad	724548299
	.quad	992983755
	.quad	191871691
	.quad	460307147
	.quad	728742603
	.quad	997178059
	.quad	196065995
	.quad	464501451
	.quad	732936907
	.quad	1001372363
	.quad	200260299
	.quad	468695755
	.quad	737131211
	.quad	1005566667
	.quad	187481803
	.quad	455917259
	.quad	724352715
	.quad	992788171
	.quad	191676107
	.quad	460111563
	.quad	728547019
	.quad	996982475
	.quad	195870411
	.quad	464305867
	.quad	732741323
	.quad	1001176779
	.quad	200064715
	.quad	468500171
	.quad	736935627
	.quad	1005371083
	.quad	187547339
	.quad	455982795
	.quad	724418251
	.quad	992853707
	.quad	191741643
	.quad	460177099
	.quad	728612555
	.quad	997048011
	.quad	195935947
	.quad	464371403
	.quad	732806859
	.quad	1001242315
	.quad	200130251
	.quad	468565707
	.quad	737001163
	.quad	1005436619
	.quad	187612875
	.quad	456048331
	.quad	724483787
	.quad	992919243
	.quad	191807179
	.quad	460242635
	.quad	728678091
	.quad	997113547
	.quad	196001483
	.quad	464436939
	.quad	732872395
	.quad	1001307851
	.quad	200195787
	.quad	468631243
	.quad	737066699
	.quad	1005502155
	.quad	187678411
	.quad	456113867
	.quad	724549323
	.quad	992984779
	.quad	191872715
	.quad	460308171
	.quad	728743627
	.quad	997179083
	.quad	196067019
	.quad	464502475
	.quad	732937931
	.quad	1001373387
	.quad	200261323
	.quad	468696779
	.quad	737132235
	.quad	1005567691
	.quad	187478747
	.quad	455914203
	.quad	724349659
	.quad	992785115
	.quad	191673051
	.quad	460108507
	.quad	728543963
	.quad	996979419
	.quad	195867355
	.quad	464302811
	.quad	732738267
	.quad	1001173723
	.quad	200061659
	.quad	468497115
	.quad	736932571
	.quad	1005368027
	.quad	187544283
	.quad	455979739
	.quad	724415195
	.quad	992850651
	.quad	191738587
	.quad	460174043
	.quad	728609499
	.quad	997044955
	.quad	195932891
	.quad	464368347
	.quad	732803803
	.quad	1001239259
	.quad	200127195
	.quad	468562651
	.quad	736998107
	.quad	1005433563
	.quad	187609819
	.quad	456045275
	.quad	724480731
	.quad	992916187
	.quad	191804123
	.quad	460239579
	.quad	728675035
	.quad	997110491
	.quad	195998427
	.quad	464433883
	.quad	732869339
	.quad	1001304795
	.quad	200192731
	.quad	468628187
	.quad	737063643
	.quad	1005499099
	.quad	187675355
	.quad	456110811
	.quad	724546267
	.quad	992981723
	.quad	191869659
	.quad	460305115
	.quad	728740571
	.quad	997176027
	.quad	196063963
	.quad	464499419
	.quad	732934875
	.quad	1001370331
	.quad	200258267
	.quad	468693723
	.quad	737129179
	.quad	1005564635
	.quad	187479771
	.quad	455915227
	.quad	724350683
	.quad	992786139
	.quad	191674075
	.quad	460109531
	.quad	728544987
	.quad	996980443
	.quad	195868379
	.quad	464303835
	.quad	732739291
	.quad	1001174747
	.quad	200062683
	.quad	468498139
	.quad	736933595
	.quad	1005369051
	.quad	187545307
	.quad	455980763
	.quad	724416219
	.quad	992851675
	.quad	191739611
	.quad	460175067
	.quad	728610523
	.quad	997045979
	.quad	195933915
	.quad	464369371
	.quad	732804827
	.quad	1001240283
	.quad	200128219
	.quad	468563675
	.quad	736999131
	.quad	1005434587
	.quad	187610843
	.quad	456046299
	.quad	724481755
	.quad	992917211
	.quad	191805147
	.quad	460240603
	.quad	728676059
	.quad	997111515
	.quad	195999451
	.quad	464434907
	.quad	732870363
	.quad	1001305819
	.align 32
	.type	kZeroRepsDepth, @object
	.size	kZeroRepsDepth, 2816
kZeroRepsDepth:
	.long	0
	.long	4
	.long	8
	.long	7
	.long	7
	.long	7
	.long	7
	.long	7
	.long	7
	.long	7
	.long	7
	.long	11
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	14
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	21
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.long	28
	.align 32
	.type	kZeroRepsBits, @object
	.size	kZeroRepsBits, 5632
kZeroRepsBits:
	.quad	0
	.quad	0
	.quad	0
	.quad	7
	.quad	23
	.quad	39
	.quad	55
	.quad	71
	.quad	87
	.quad	103
	.quad	119
	.quad	1904
	.quad	2951
	.quad	4999
	.quad	7047
	.quad	9095
	.quad	11143
	.quad	13191
	.quad	15239
	.quad	919
	.quad	2967
	.quad	5015
	.quad	7063
	.quad	9111
	.quad	11159
	.quad	13207
	.quad	15255
	.quad	935
	.quad	2983
	.quad	5031
	.quad	7079
	.quad	9127
	.quad	11175
	.quad	13223
	.quad	15271
	.quad	951
	.quad	2999
	.quad	5047
	.quad	7095
	.quad	9143
	.quad	11191
	.quad	13239
	.quad	15287
	.quad	967
	.quad	3015
	.quad	5063
	.quad	7111
	.quad	9159
	.quad	11207
	.quad	13255
	.quad	15303
	.quad	983
	.quad	3031
	.quad	5079
	.quad	7127
	.quad	9175
	.quad	11223
	.quad	13271
	.quad	15319
	.quad	999
	.quad	3047
	.quad	5095
	.quad	7143
	.quad	9191
	.quad	11239
	.quad	13287
	.quad	15335
	.quad	1015
	.quad	3063
	.quad	5111
	.quad	7159
	.quad	9207
	.quad	11255
	.quad	13303
	.quad	15351
	.quad	115591
	.quad	377735
	.quad	639879
	.quad	902023
	.quad	1164167
	.quad	1426311
	.quad	1688455
	.quad	1950599
	.quad	117639
	.quad	379783
	.quad	641927
	.quad	904071
	.quad	1166215
	.quad	1428359
	.quad	1690503
	.quad	1952647
	.quad	119687
	.quad	381831
	.quad	643975
	.quad	906119
	.quad	1168263
	.quad	1430407
	.quad	1692551
	.quad	1954695
	.quad	121735
	.quad	383879
	.quad	646023
	.quad	908167
	.quad	1170311
	.quad	1432455
	.quad	1694599
	.quad	1956743
	.quad	123783
	.quad	385927
	.quad	648071
	.quad	910215
	.quad	1172359
	.quad	1434503
	.quad	1696647
	.quad	1958791
	.quad	125831
	.quad	387975
	.quad	650119
	.quad	912263
	.quad	1174407
	.quad	1436551
	.quad	1698695
	.quad	1960839
	.quad	127879
	.quad	390023
	.quad	652167
	.quad	914311
	.quad	1176455
	.quad	1438599
	.quad	1700743
	.quad	1962887
	.quad	129927
	.quad	392071
	.quad	654215
	.quad	916359
	.quad	1178503
	.quad	1440647
	.quad	1702791
	.quad	1964935
	.quad	115607
	.quad	377751
	.quad	639895
	.quad	902039
	.quad	1164183
	.quad	1426327
	.quad	1688471
	.quad	1950615
	.quad	117655
	.quad	379799
	.quad	641943
	.quad	904087
	.quad	1166231
	.quad	1428375
	.quad	1690519
	.quad	1952663
	.quad	119703
	.quad	381847
	.quad	643991
	.quad	906135
	.quad	1168279
	.quad	1430423
	.quad	1692567
	.quad	1954711
	.quad	121751
	.quad	383895
	.quad	646039
	.quad	908183
	.quad	1170327
	.quad	1432471
	.quad	1694615
	.quad	1956759
	.quad	123799
	.quad	385943
	.quad	648087
	.quad	910231
	.quad	1172375
	.quad	1434519
	.quad	1696663
	.quad	1958807
	.quad	125847
	.quad	387991
	.quad	650135
	.quad	912279
	.quad	1174423
	.quad	1436567
	.quad	1698711
	.quad	1960855
	.quad	127895
	.quad	390039
	.quad	652183
	.quad	914327
	.quad	1176471
	.quad	1438615
	.quad	1700759
	.quad	1962903
	.quad	129943
	.quad	392087
	.quad	654231
	.quad	916375
	.quad	1178519
	.quad	1440663
	.quad	1702807
	.quad	1964951
	.quad	115623
	.quad	377767
	.quad	639911
	.quad	902055
	.quad	1164199
	.quad	1426343
	.quad	1688487
	.quad	1950631
	.quad	117671
	.quad	379815
	.quad	641959
	.quad	904103
	.quad	1166247
	.quad	1428391
	.quad	1690535
	.quad	1952679
	.quad	119719
	.quad	381863
	.quad	644007
	.quad	906151
	.quad	1168295
	.quad	1430439
	.quad	1692583
	.quad	1954727
	.quad	121767
	.quad	383911
	.quad	646055
	.quad	908199
	.quad	1170343
	.quad	1432487
	.quad	1694631
	.quad	1956775
	.quad	123815
	.quad	385959
	.quad	648103
	.quad	910247
	.quad	1172391
	.quad	1434535
	.quad	1696679
	.quad	1958823
	.quad	125863
	.quad	388007
	.quad	650151
	.quad	912295
	.quad	1174439
	.quad	1436583
	.quad	1698727
	.quad	1960871
	.quad	127911
	.quad	390055
	.quad	652199
	.quad	914343
	.quad	1176487
	.quad	1438631
	.quad	1700775
	.quad	1962919
	.quad	129959
	.quad	392103
	.quad	654247
	.quad	916391
	.quad	1178535
	.quad	1440679
	.quad	1702823
	.quad	1964967
	.quad	115639
	.quad	377783
	.quad	639927
	.quad	902071
	.quad	1164215
	.quad	1426359
	.quad	1688503
	.quad	1950647
	.quad	117687
	.quad	379831
	.quad	641975
	.quad	904119
	.quad	1166263
	.quad	1428407
	.quad	1690551
	.quad	1952695
	.quad	119735
	.quad	381879
	.quad	644023
	.quad	906167
	.quad	1168311
	.quad	1430455
	.quad	1692599
	.quad	1954743
	.quad	121783
	.quad	383927
	.quad	646071
	.quad	908215
	.quad	1170359
	.quad	1432503
	.quad	1694647
	.quad	1956791
	.quad	123831
	.quad	385975
	.quad	648119
	.quad	910263
	.quad	1172407
	.quad	1434551
	.quad	1696695
	.quad	1958839
	.quad	125879
	.quad	388023
	.quad	650167
	.quad	912311
	.quad	1174455
	.quad	1436599
	.quad	1698743
	.quad	1960887
	.quad	127927
	.quad	390071
	.quad	652215
	.quad	914359
	.quad	1176503
	.quad	1438647
	.quad	1700791
	.quad	1962935
	.quad	129975
	.quad	392119
	.quad	654263
	.quad	916407
	.quad	1178551
	.quad	1440695
	.quad	1702839
	.quad	1964983
	.quad	115655
	.quad	377799
	.quad	639943
	.quad	902087
	.quad	1164231
	.quad	1426375
	.quad	1688519
	.quad	1950663
	.quad	117703
	.quad	379847
	.quad	641991
	.quad	904135
	.quad	1166279
	.quad	1428423
	.quad	1690567
	.quad	1952711
	.quad	119751
	.quad	381895
	.quad	644039
	.quad	906183
	.quad	1168327
	.quad	1430471
	.quad	1692615
	.quad	1954759
	.quad	121799
	.quad	383943
	.quad	646087
	.quad	908231
	.quad	1170375
	.quad	1432519
	.quad	1694663
	.quad	1956807
	.quad	123847
	.quad	385991
	.quad	648135
	.quad	910279
	.quad	1172423
	.quad	1434567
	.quad	1696711
	.quad	1958855
	.quad	125895
	.quad	388039
	.quad	650183
	.quad	912327
	.quad	1174471
	.quad	1436615
	.quad	1698759
	.quad	1960903
	.quad	127943
	.quad	390087
	.quad	652231
	.quad	914375
	.quad	1176519
	.quad	1438663
	.quad	1700807
	.quad	1962951
	.quad	129991
	.quad	392135
	.quad	654279
	.quad	916423
	.quad	1178567
	.quad	1440711
	.quad	1702855
	.quad	1964999
	.quad	115671
	.quad	377815
	.quad	639959
	.quad	902103
	.quad	1164247
	.quad	1426391
	.quad	1688535
	.quad	1950679
	.quad	117719
	.quad	379863
	.quad	642007
	.quad	904151
	.quad	1166295
	.quad	1428439
	.quad	1690583
	.quad	1952727
	.quad	119767
	.quad	381911
	.quad	644055
	.quad	906199
	.quad	1168343
	.quad	1430487
	.quad	1692631
	.quad	1954775
	.quad	121815
	.quad	383959
	.quad	646103
	.quad	908247
	.quad	1170391
	.quad	1432535
	.quad	1694679
	.quad	1956823
	.quad	123863
	.quad	386007
	.quad	648151
	.quad	910295
	.quad	1172439
	.quad	1434583
	.quad	1696727
	.quad	1958871
	.quad	125911
	.quad	388055
	.quad	650199
	.quad	912343
	.quad	1174487
	.quad	1436631
	.quad	1698775
	.quad	1960919
	.quad	127959
	.quad	390103
	.quad	652247
	.quad	914391
	.quad	1176535
	.quad	1438679
	.quad	1700823
	.quad	1962967
	.quad	130007
	.quad	392151
	.quad	654295
	.quad	916439
	.quad	1178583
	.quad	1440727
	.quad	1702871
	.quad	1965015
	.quad	115687
	.quad	377831
	.quad	639975
	.quad	902119
	.quad	1164263
	.quad	1426407
	.quad	1688551
	.quad	1950695
	.quad	117735
	.quad	379879
	.quad	642023
	.quad	904167
	.quad	1166311
	.quad	1428455
	.quad	1690599
	.quad	1952743
	.quad	119783
	.quad	381927
	.quad	644071
	.quad	906215
	.quad	1168359
	.quad	1430503
	.quad	1692647
	.quad	1954791
	.quad	121831
	.quad	383975
	.quad	646119
	.quad	908263
	.quad	1170407
	.quad	1432551
	.quad	1694695
	.quad	1956839
	.quad	123879
	.quad	386023
	.quad	648167
	.quad	910311
	.quad	1172455
	.quad	1434599
	.quad	1696743
	.quad	1958887
	.quad	125927
	.quad	388071
	.quad	650215
	.quad	912359
	.quad	1174503
	.quad	1436647
	.quad	1698791
	.quad	1960935
	.quad	127975
	.quad	390119
	.quad	652263
	.quad	914407
	.quad	1176551
	.quad	1438695
	.quad	1700839
	.quad	1962983
	.quad	130023
	.quad	392167
	.quad	654311
	.quad	916455
	.quad	1178599
	.quad	1440743
	.quad	1702887
	.quad	1965031
	.quad	115703
	.quad	377847
	.quad	639991
	.quad	902135
	.quad	1164279
	.quad	1426423
	.quad	1688567
	.quad	1950711
	.quad	117751
	.quad	379895
	.quad	642039
	.quad	904183
	.quad	1166327
	.quad	1428471
	.quad	1690615
	.quad	1952759
	.quad	119799
	.quad	381943
	.quad	644087
	.quad	906231
	.quad	1168375
	.quad	1430519
	.quad	1692663
	.quad	1954807
	.quad	121847
	.quad	383991
	.quad	646135
	.quad	908279
	.quad	1170423
	.quad	1432567
	.quad	1694711
	.quad	1956855
	.quad	123895
	.quad	386039
	.quad	648183
	.quad	910327
	.quad	1172471
	.quad	1434615
	.quad	1696759
	.quad	1958903
	.quad	125943
	.quad	388087
	.quad	650231
	.quad	912375
	.quad	1174519
	.quad	1436663
	.quad	1698807
	.quad	1960951
	.quad	127991
	.quad	390135
	.quad	652279
	.quad	914423
	.quad	1176567
	.quad	1438711
	.quad	1700855
	.quad	1962999
	.quad	130039
	.quad	392183
	.quad	654327
	.quad	916471
	.quad	1178615
	.quad	1440759
	.quad	1702903
	.quad	1965047
	.quad	14795655
	.quad	48350087
	.quad	81904519
	.quad	115458951
	.quad	149013383
	.quad	182567815
	.quad	216122247
	.quad	249676679
	.quad	15057799
	.quad	48612231
	.quad	82166663
	.quad	115721095
	.quad	149275527
	.quad	182829959
	.quad	216384391
	.quad	249938823
	.quad	15319943
	.quad	48874375
	.quad	82428807
	.quad	115983239
	.quad	149537671
	.quad	183092103
	.quad	216646535
	.quad	250200967
	.quad	15582087
	.quad	49136519
	.quad	82690951
	.quad	116245383
	.quad	149799815
	.quad	183354247
	.quad	216908679
	.quad	250463111
	.quad	15844231
	.quad	49398663
	.quad	82953095
	.quad	116507527
	.quad	150061959
	.quad	183616391
	.quad	217170823
	.quad	250725255
	.quad	16106375
	.quad	49660807
	.quad	83215239
	.quad	116769671
	.quad	150324103
	.quad	183878535
	.quad	217432967
	.quad	250987399
	.quad	16368519
	.quad	49922951
	.quad	83477383
	.quad	117031815
	.quad	150586247
	.quad	184140679
	.quad	217695111
	.quad	251249543
	.quad	16630663
	.quad	50185095
	.quad	83739527
	.quad	117293959
	.quad	150848391
	.quad	184402823
	.quad	217957255
	.quad	251511687
	.quad	14797703
	.quad	48352135
	.quad	81906567
	.quad	115460999
	.quad	149015431
	.quad	182569863
	.quad	216124295
	.quad	249678727
	.quad	15059847
	.quad	48614279
	.quad	82168711
	.quad	115723143
	.quad	149277575
	.quad	182832007
	.quad	216386439
	.quad	249940871
	.quad	15321991
	.quad	48876423
	.quad	82430855
	.quad	115985287
	.quad	149539719
	.quad	183094151
	.quad	216648583
	.quad	250203015
	.quad	15584135
	.quad	49138567
	.quad	82692999
	.quad	116247431
	.quad	149801863
	.quad	183356295
	.quad	216910727
	.quad	250465159
	.quad	15846279
	.quad	49400711
	.quad	82955143
	.quad	116509575
	.quad	150064007
	.quad	183618439
	.quad	217172871
	.quad	250727303
	.quad	16108423
	.quad	49662855
	.quad	83217287
	.quad	116771719
	.quad	150326151
	.quad	183880583
	.quad	217435015
	.quad	250989447
	.quad	16370567
	.quad	49924999
	.quad	83479431
	.quad	117033863
	.quad	150588295
	.align 32
	.type	kCodeLengthBits, @object
	.size	kCodeLengthBits, 72
kCodeLengthBits:
	.long	0
	.long	8
	.long	4
	.long	12
	.long	2
	.long	10
	.long	6
	.long	14
	.long	1
	.long	9
	.long	5
	.long	13
	.long	3
	.long	15
	.long	31
	.long	0
	.long	11
	.long	7
	.align 32
	.type	kStaticDistanceCodeDepth, @object
	.size	kStaticDistanceCodeDepth, 64
kStaticDistanceCodeDepth:
	.ascii	"\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006"
	.ascii	"\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006"
	.ascii	"\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006"
	.ascii	"\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006"
	.ascii	"\006\006\006\006"
	.align 32
	.type	kStaticCommandCodeDepth, @object
	.size	kStaticCommandCodeDepth, 704
kStaticCommandCodeDepth:
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
	.ascii	"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.ascii	"\013\013\013\013\013\013\013\013\013\013\013\013\013\013\013"
	.align 16
	.type	kCodeLengthDepth, @object
	.size	kCodeLengthDepth, 18
kCodeLengthDepth:
	.string	"\004\004\004\004\004\004\004\004\004\004\004\004\004\005\005"
	.ascii	"\004\004"
	.align 32
	.type	kCopyExtra, @object
	.size	kCopyExtra, 96
kCopyExtra:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	24
	.align 32
	.type	kCopyBase, @object
	.size	kCopyBase, 96
kCopyBase:
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	12
	.long	14
	.long	18
	.long	22
	.long	30
	.long	38
	.long	54
	.long	70
	.long	102
	.long	134
	.long	198
	.long	326
	.long	582
	.long	1094
	.long	2118
	.align 32
	.type	kInsExtra, @object
	.size	kInsExtra, 96
kInsExtra:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	4
	.long	4
	.long	5
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	12
	.long	14
	.long	24
	.align 32
	.type	kInsBase, @object
	.size	kInsBase, 96
kInsBase:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	8
	.long	10
	.long	14
	.long	18
	.long	26
	.long	34
	.long	50
	.long	66
	.long	98
	.long	130
	.long	194
	.long	322
	.long	578
	.long	1090
	.long	2114
	.long	6210
	.long	22594
	.align 32
	.type	kContextLookup, @object
	.size	kContextLookup, 2048
kContextLookup:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	30
	.byte	30
	.byte	30
	.byte	30
	.byte	31
	.byte	31
	.byte	31
	.byte	31
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	33
	.byte	33
	.byte	33
	.byte	33
	.byte	34
	.byte	34
	.byte	34
	.byte	34
	.byte	35
	.byte	35
	.byte	35
	.byte	35
	.byte	36
	.byte	36
	.byte	36
	.byte	36
	.byte	37
	.byte	37
	.byte	37
	.byte	37
	.byte	38
	.byte	38
	.byte	38
	.byte	38
	.byte	39
	.byte	39
	.byte	39
	.byte	39
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	41
	.byte	41
	.byte	41
	.byte	41
	.byte	42
	.byte	42
	.byte	42
	.byte	42
	.byte	43
	.byte	43
	.byte	43
	.byte	43
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	45
	.byte	45
	.byte	45
	.byte	45
	.byte	46
	.byte	46
	.byte	46
	.byte	46
	.byte	47
	.byte	47
	.byte	47
	.byte	47
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	49
	.byte	49
	.byte	49
	.byte	49
	.byte	50
	.byte	50
	.byte	50
	.byte	50
	.byte	51
	.byte	51
	.byte	51
	.byte	51
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	53
	.byte	53
	.byte	53
	.byte	53
	.byte	54
	.byte	54
	.byte	54
	.byte	54
	.byte	55
	.byte	55
	.byte	55
	.byte	55
	.byte	56
	.byte	56
	.byte	56
	.byte	56
	.byte	57
	.byte	57
	.byte	57
	.byte	57
	.byte	58
	.byte	58
	.byte	58
	.byte	58
	.byte	59
	.byte	59
	.byte	59
	.byte	59
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	61
	.byte	61
	.byte	61
	.byte	61
	.byte	62
	.byte	62
	.byte	62
	.byte	62
	.byte	63
	.byte	63
	.byte	63
	.byte	63
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	8
	.byte	12
	.byte	16
	.byte	12
	.byte	12
	.byte	20
	.byte	12
	.byte	16
	.byte	24
	.byte	28
	.byte	12
	.byte	12
	.byte	32
	.byte	12
	.byte	36
	.byte	12
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	44
	.byte	32
	.byte	32
	.byte	24
	.byte	40
	.byte	28
	.byte	12
	.byte	12
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	48
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	52
	.byte	24
	.byte	12
	.byte	28
	.byte	12
	.byte	12
	.byte	12
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	56
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	60
	.byte	24
	.byte	12
	.byte	28
	.byte	12
	.byte	0
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	2
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	0
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	40
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	56
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	0
	.quad	1
	.align 16
.LC1:
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.align 16
.LC2:
	.quad	16
	.quad	16
	.align 16
.LC3:
	.quad	2
	.quad	2
	.align 16
.LC4:
	.quad	4
	.quad	4
	.align 16
.LC5:
	.quad	6
	.quad	6
	.align 16
.LC6:
	.quad	8
	.quad	8
	.align 16
.LC7:
	.quad	10
	.quad	10
	.align 16
.LC8:
	.quad	12
	.quad	12
	.align 16
.LC9:
	.quad	14
	.quad	14
	.align 16
.LC10:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.align 16
.LC11:
	.long	1
	.long	1
	.long	1
	.long	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC12:
	.long	0
	.long	2146435072
	.hidden	BrotliSetDepth
	.hidden	BrotliAllocate
	.hidden	BrotliFree
	.hidden	BrotliConvertBitDepthsToSymbols
	.hidden	BrotliCreateHuffmanTree
	.hidden	BrotliWriteHuffmanTree
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
