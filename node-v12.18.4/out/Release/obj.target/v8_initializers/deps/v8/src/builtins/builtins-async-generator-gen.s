	.file	"builtins-async-generator-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation:
.LFB26795:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26795:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB26798:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE26798:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation:
.LFB26799:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L12
	cmpl	$3, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L17
.L13:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26799:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movl	$1064, %edx
	movq	(%rax), %rsi
	movq	8(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler15HasInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26794:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE, @function
_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE:
.LFB22082:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -136(%rbp)
	movl	$2, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$56, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r13, %r9
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	movq	%rbx, %xmm1
	movq	-96(%rbp), %rax
	pushq	%r15
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-136(%rbp), %xmm0
	movl	$1, %ecx
	movq	%r10, -128(%rbp)
	movq	%rax, -120(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-144(%rbp), %r11
	movl	$678, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	-96(%rbp), %rax
	movl	$1, %r9d
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%rbx, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22082:
	.size	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE, .-_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE
	.section	.text._ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb, @function
_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb:
.LFB24717:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	xorl	%esi, %esi
	subq	$24, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$80, %edx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE(%rip), %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movzbl	%bl, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15BooleanConstantEb@PLT
	movq	%r12, %rdi
	movl	$20, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$21, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r10
	movq	%r14, %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %r9
	pushq	%rdx
	movq	%r10, %r8
	movq	%r13, %rdx
	pushq	%rbx
	call	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE24717:
	.size	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb, .-_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb
	.set	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_35AsyncGeneratorAwaitCaughtDescriptorEEEvb,_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb
	.section	.text._ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc, @function
_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc:
.LFB22080:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$1272, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -1256(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, -1200(%rbp)
	movq	%rcx, -1192(%rbp)
	movq	%rax, -1264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1216(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, -1232(%rbp)
	leaq	-1168(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1040(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-272(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler14TaggedIsNotSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movdqa	-1216(%rbp), %xmm0
	movq	%r12, %rdi
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation(%rip), %rcx
	movq	%rax, -400(%rbp)
	movq	%rax, %rsi
	leaq	-400(%rbp), %rax
	leaq	-144(%rbp), %r9
	movq	%rax, -1216(%rbp)
	movl	$4, %r8d
	movq	%r9, %rdx
	movq	%rax, -112(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -272(%rbp)
	movq	%rcx, %xmm0
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_5BoolTEZNS1_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorES4_EUlvE_ZNSA_22TaggedIsAsyncGeneratorES4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeIS8_EERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_5BoolTEZNS2_12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler22TaggedIsAsyncGeneratorEPNS2_8compiler4NodeEEUlvE_ZNS7_22TaggedIsAsyncGeneratorESA_EUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeIS5_EERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSQ_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm3
	movq	%r14, -144(%rbp)
	leaq	-112(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, %rcx
	movq	%rax, -1224(%rbp)
	movq	%r9, -1272(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, %r13
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L31
	movq	-1272(%rbp), %r9
	movl	$3, %edx
	movq	%r9, %rsi
	movq	%r9, %rdi
	call	*%rax
.L31:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L32
	movq	-1224(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L32:
	movq	-1240(%rbp), %rcx
	movq	-1248(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-912(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r13, -1272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$495, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	leaq	-1184(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movl	$24, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	-656(%rbp), %rbx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movl	$32, %edx
	movq	%r12, %rdi
	movq	-1232(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$4, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%r15, %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-784(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-528(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r9, %rdi
	movq	%r12, %rsi
	movq	%r9, -1288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1192(%rbp), %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$80, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1280(%rbp), %r10
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1280(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1304(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movl	$80, %edx
	movq	%r12, %rdi
	movq	-1192(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-1288(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movq	-1216(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1288(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1216(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	-1280(%rbp), %r11
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-1296(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1216(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1288(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	movq	%r13, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1304(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1192(%rbp), %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$-2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1272(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$678, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$1, %edi
	movq	-1224(%rbp), %rsi
	movq	-1216(%rbp), %r15
	movq	%rax, %r8
	movq	-256(%rbp), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	movq	-1200(%rbp), %r9
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%rax, -392(%rbp)
	movq	-1192(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, -400(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1232(%rbp), %rsi
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	-1200(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, -1264(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$231, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$62, %esi
	movq	%r12, %rdi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$6, %edi
	movq	%r13, %r9
	movq	%r15, %rdx
	movq	-1224(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movl	$1, %ecx
	movq	-1272(%rbp), %xmm0
	movq	-256(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, -400(%rbp)
	pushq	%rsi
	xorl	%esi, %esi
	movhps	-1280(%rbp), %xmm0
	movq	%rax, -392(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-1216(%rbp), %xmm0
	movhps	-1200(%rbp), %xmm0
	movq	%r13, -1200(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-1264(%rbp), %xmm0
	movhps	-1192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$494, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1224(%rbp), %rcx
	movq	%r15, %rdx
	movq	-1232(%rbp), %r14
	movq	%rbx, -400(%rbp)
	movq	%rax, %r8
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$3, %ebx
	movq	-1200(%rbp), %r9
	movq	-256(%rbp), %rax
	movq	%r14, %xmm0
	pushq	%rbx
	movhps	-1192(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -392(%rbp)
	movq	%r13, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1256(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22080:
	.size	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc, .-_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc
	.section	.rodata._ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"[AsyncGenerator].prototype.next"
	.section	.text._ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv
	.type	_ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv, @function
_ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv:
.LFB22100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%rax, %rdx
	leaq	.LC1(%rip), %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	pushq	%rax
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22100:
	.size	_ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv, .-_ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/v8/src/builtins/builtins-async-generator-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"AsyncGeneratorPrototypeNext"
	.section	.text._ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE:
.LFB22096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$317, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$680, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L50
.L47:
	movq	%r13, %rdi
	call	_ZN2v88internal36AsyncGeneratorPrototypeNextAssembler39GenerateAsyncGeneratorPrototypeNextImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L47
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22096:
	.size	_ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"[AsyncGenerator].prototype.return"
	.section	.text._ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv
	.type	_ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv, @function
_ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv:
.LFB22109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%rax, %rdx
	leaq	.LC4(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rax
	movl	$1, %r9d
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22109:
	.size	_ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv, .-_ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv
	.section	.rodata._ZN2v88internal8Builtins38Generate_AsyncGeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"AsyncGeneratorPrototypeReturn"
	.section	.text._ZN2v88internal8Builtins38Generate_AsyncGeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins38Generate_AsyncGeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins38Generate_AsyncGeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins38Generate_AsyncGeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE:
.LFB22105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$335, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$681, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L60
.L57:
	movq	%r13, %rdi
	call	_ZN2v88internal38AsyncGeneratorPrototypeReturnAssembler41GenerateAsyncGeneratorPrototypeReturnImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L57
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22105:
	.size	_ZN2v88internal8Builtins38Generate_AsyncGeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins38Generate_AsyncGeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"[AsyncGenerator].prototype.throw"
	.section	.text._ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv
	.type	_ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv, @function
_ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv:
.LFB22118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%rax, %rdx
	leaq	.LC6(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rax
	movl	$2, %r9d
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler21AsyncGeneratorEnqueueEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES7_S7_NS0_17JSGeneratorObject10ResumeModeEPKc
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22118:
	.size	_ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv, .-_ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv
	.section	.rodata._ZN2v88internal8Builtins37Generate_AsyncGeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"AsyncGeneratorPrototypeThrow"
	.section	.text._ZN2v88internal8Builtins37Generate_AsyncGeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_AsyncGeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_AsyncGeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_AsyncGeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$353, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$682, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L70
.L67:
	movq	%r13, %rdi
	call	_ZN2v88internal37AsyncGeneratorPrototypeThrowAssembler40GenerateAsyncGeneratorPrototypeThrowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L67
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22114:
	.size	_ZN2v88internal8Builtins37Generate_AsyncGeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_AsyncGeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins42Generate_AsyncGeneratorAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"AsyncGeneratorAwaitResolveClosure"
	.section	.text._ZN2v88internal8Builtins42Generate_AsyncGeneratorAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins42Generate_AsyncGeneratorAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins42Generate_AsyncGeneratorAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins42Generate_AsyncGeneratorAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$369, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$685, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L76
.L73:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L73
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22123:
	.size	_ZN2v88internal8Builtins42Generate_AsyncGeneratorAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins42Generate_AsyncGeneratorAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal42AsyncGeneratorAwaitResolveClosureAssembler45GenerateAsyncGeneratorAwaitResolveClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal42AsyncGeneratorAwaitResolveClosureAssembler45GenerateAsyncGeneratorAwaitResolveClosureImplEv
	.type	_ZN2v88internal42AsyncGeneratorAwaitResolveClosureAssembler45GenerateAsyncGeneratorAwaitResolveClosureImplEv, @function
_ZN2v88internal42AsyncGeneratorAwaitResolveClosureAssembler45GenerateAsyncGeneratorAwaitResolveClosureImplEv:
.LFB22127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE
	.cfi_endproc
.LFE22127:
	.size	_ZN2v88internal42AsyncGeneratorAwaitResolveClosureAssembler45GenerateAsyncGeneratorAwaitResolveClosureImplEv, .-_ZN2v88internal42AsyncGeneratorAwaitResolveClosureAssembler45GenerateAsyncGeneratorAwaitResolveClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins41Generate_AsyncGeneratorAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"AsyncGeneratorAwaitRejectClosure"
	.section	.text._ZN2v88internal8Builtins41Generate_AsyncGeneratorAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_AsyncGeneratorAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins41Generate_AsyncGeneratorAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins41Generate_AsyncGeneratorAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$376, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$686, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L84
.L81:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L81
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22132:
	.size	_ZN2v88internal8Builtins41Generate_AsyncGeneratorAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins41Generate_AsyncGeneratorAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal41AsyncGeneratorAwaitRejectClosureAssembler44GenerateAsyncGeneratorAwaitRejectClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal41AsyncGeneratorAwaitRejectClosureAssembler44GenerateAsyncGeneratorAwaitRejectClosureImplEv
	.type	_ZN2v88internal41AsyncGeneratorAwaitRejectClosureAssembler44GenerateAsyncGeneratorAwaitRejectClosureImplEv, @function
_ZN2v88internal41AsyncGeneratorAwaitRejectClosureAssembler44GenerateAsyncGeneratorAwaitRejectClosureImplEv:
.LFB22136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$2, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE
	.cfi_endproc
.LFE22136:
	.size	_ZN2v88internal41AsyncGeneratorAwaitRejectClosureAssembler44GenerateAsyncGeneratorAwaitRejectClosureImplEv, .-_ZN2v88internal41AsyncGeneratorAwaitRejectClosureAssembler44GenerateAsyncGeneratorAwaitRejectClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_AsyncGeneratorAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"AsyncGeneratorAwaitUncaught"
	.section	.text._ZN2v88internal8Builtins36Generate_AsyncGeneratorAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_AsyncGeneratorAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_AsyncGeneratorAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_AsyncGeneratorAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE:
.LFB22141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$383, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$684, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L92
.L89:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L89
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22141:
	.size	_ZN2v88internal8Builtins36Generate_AsyncGeneratorAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_AsyncGeneratorAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36AsyncGeneratorAwaitUncaughtAssembler39GenerateAsyncGeneratorAwaitUncaughtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36AsyncGeneratorAwaitUncaughtAssembler39GenerateAsyncGeneratorAwaitUncaughtImplEv
	.type	_ZN2v88internal36AsyncGeneratorAwaitUncaughtAssembler39GenerateAsyncGeneratorAwaitUncaughtImplEv, @function
_ZN2v88internal36AsyncGeneratorAwaitUncaughtAssembler39GenerateAsyncGeneratorAwaitUncaughtImplEv:
.LFB22145:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_37AsyncGeneratorAwaitUncaughtDescriptorEEEvb
	.cfi_endproc
.LFE22145:
	.size	_ZN2v88internal36AsyncGeneratorAwaitUncaughtAssembler39GenerateAsyncGeneratorAwaitUncaughtImplEv, .-_ZN2v88internal36AsyncGeneratorAwaitUncaughtAssembler39GenerateAsyncGeneratorAwaitUncaughtImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_AsyncGeneratorAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"AsyncGeneratorAwaitCaught"
	.section	.text._ZN2v88internal8Builtins34Generate_AsyncGeneratorAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_AsyncGeneratorAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_AsyncGeneratorAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_AsyncGeneratorAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE:
.LFB22150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$388, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$683, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L99
.L96:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_35AsyncGeneratorAwaitCaughtDescriptorEEEvb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L96
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22150:
	.size	_ZN2v88internal8Builtins34Generate_AsyncGeneratorAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_AsyncGeneratorAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal34AsyncGeneratorAwaitCaughtAssembler37GenerateAsyncGeneratorAwaitCaughtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34AsyncGeneratorAwaitCaughtAssembler37GenerateAsyncGeneratorAwaitCaughtImplEv
	.type	_ZN2v88internal34AsyncGeneratorAwaitCaughtAssembler37GenerateAsyncGeneratorAwaitCaughtImplEv, @function
_ZN2v88internal34AsyncGeneratorAwaitCaughtAssembler37GenerateAsyncGeneratorAwaitCaughtImplEv:
.LFB22154:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler19AsyncGeneratorAwaitINS0_35AsyncGeneratorAwaitCaughtDescriptorEEEvb
	.cfi_endproc
.LFE22154:
	.size	_ZN2v88internal34AsyncGeneratorAwaitCaughtAssembler37GenerateAsyncGeneratorAwaitCaughtImplEv, .-_ZN2v88internal34AsyncGeneratorAwaitCaughtAssembler37GenerateAsyncGeneratorAwaitCaughtImplEv
	.section	.text._ZN2v88internal33AsyncGeneratorResumeNextAssembler36GenerateAsyncGeneratorResumeNextImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33AsyncGeneratorResumeNextAssembler36GenerateAsyncGeneratorResumeNextImplEv
	.type	_ZN2v88internal33AsyncGeneratorResumeNextAssembler36GenerateAsyncGeneratorResumeNextImplEv, @function
_ZN2v88internal33AsyncGeneratorResumeNextAssembler36GenerateAsyncGeneratorResumeNextImplEv:
.LFB22163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1072(%rbp), %r14
	leaq	-992(%rbp), %r15
	pushq	%r13
	movq	%r14, %xmm0
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1056(%rbp), %rbx
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	subq	$1176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	movq	%rax, -1080(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$80, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rbx, %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	%rbx, -1088(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-96(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movdqa	-1104(%rbp), %xmm0
	movl	$1, %r8d
	movl	$2, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$88, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1104(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8ReturnIfEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8ReturnIfEPNS1_4NodeES4_@PLT
	movq	-1088(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-864(%rbp), %rcx
	movq	%rax, -1136(%rbp)
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rbx, -1144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-736(%rbp), %rcx
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -1152(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-608(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1144(%rbp), %rbx
	movq	-1152(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%rbx, -1144(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-480(%rbp), %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-352(%rbp), %rdx
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-224(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1112(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1168(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1128(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$64, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$6, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1160(%rbp), %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1128(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	movq	%rdx, %rcx
	movl	$1, %r8d
	movl	$107, %esi
	movq	%rdx, -1128(%rbp)
	movq	-1080(%rbp), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-1024(%rbp), %r11
	movl	$677, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -1208(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1176(%rbp), %rcx
	movq	%r13, %xmm0
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-1040(%rbp), %r10
	movq	-1128(%rbp), %r8
	movq	%rax, -1040(%rbp)
	movq	-1008(%rbp), %rax
	movhps	-1168(%rbp), %xmm0
	movq	%r10, %rsi
	movq	%rcx, -64(%rbp)
	movq	-1080(%rbp), %rcx
	movl	$3, %r9d
	movq	%r10, -1184(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -1200(%rbp)
	movq	%rax, -1032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1176(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1168(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1208(%rbp), %r11
	movl	$675, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	-1128(%rbp), %rsi
	movq	-1184(%rbp), %r10
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	movdqa	-1200(%rbp), %xmm0
	movq	%r10, %rdx
	xorl	%esi, %esi
	movq	-1080(%rbp), %r9
	movq	%rax, -1040(%rbp)
	movq	%r12, %rdi
	movq	-1008(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -1032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$80, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1168(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1112(%rbp), %rdi
	movl	$674, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	movl	$3, %ebx
	xorl	%esi, %esi
	movq	-1128(%rbp), %rcx
	movq	%rax, %r8
	pushq	%rbx
	movq	%r13, %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	movl	$2, %ebx
	movq	-1080(%rbp), %r9
	pushq	%rcx
	movq	-1104(%rbp), %rdx
	movhps	-1168(%rbp), %xmm0
	movl	$1, %ecx
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$64, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$80, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$56, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-1136(%rbp), %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-1160(%rbp), %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1128(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r13, %xmm1
	movq	-1136(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rcx
	movq	%r12, %rdi
	movq	-1080(%rbp), %r9
	movl	$1, %ecx
	movq	%rax, -352(%rbp)
	movq	-1104(%rbp), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	-208(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$80, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1088(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1152(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1144(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22163:
	.size	_ZN2v88internal33AsyncGeneratorResumeNextAssembler36GenerateAsyncGeneratorResumeNextImplEv, .-_ZN2v88internal33AsyncGeneratorResumeNextAssembler36GenerateAsyncGeneratorResumeNextImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_AsyncGeneratorResumeNextEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"AsyncGeneratorResumeNext"
	.section	.text._ZN2v88internal8Builtins33Generate_AsyncGeneratorResumeNextEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_AsyncGeneratorResumeNextEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_AsyncGeneratorResumeNextEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_AsyncGeneratorResumeNextEPNS0_8compiler18CodeAssemblerStateE:
.LFB22159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$393, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$678, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L110
.L107:
	movq	%r13, %rdi
	call	_ZN2v88internal33AsyncGeneratorResumeNextAssembler36GenerateAsyncGeneratorResumeNextImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L107
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22159:
	.size	_ZN2v88internal8Builtins33Generate_AsyncGeneratorResumeNextEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_AsyncGeneratorResumeNextEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30AsyncGeneratorResolveAssembler33GenerateAsyncGeneratorResolveImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30AsyncGeneratorResolveAssembler33GenerateAsyncGeneratorResolveImplEv
	.type	_ZN2v88internal30AsyncGeneratorResolveAssembler33GenerateAsyncGeneratorResolveImplEv, @function
_ZN2v88internal30AsyncGeneratorResolveAssembler33GenerateAsyncGeneratorResolveImplEv:
.LFB22172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$80, %edx
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rbx, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	leaq	-336(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%edx, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	-528(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r9, %rsi
	movq	%r9, -536(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$80, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	%rbx, %rsi
	movl	$29, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%rbx, %rsi
	movl	$29, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movl	$24, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	-464(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movl	$32, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	-208(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19GotoIfForceSlowPathEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20IsPromiseHookEnabledEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler33IsPromiseThenProtectorCellInvalidEv@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-496(%rbp), %r11
	movl	$493, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -544(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	xorl	%esi, %esi
	movq	-520(%rbp), %xmm0
	movq	%rax, %r8
	movq	-480(%rbp), %rax
	pushq	%rdi
	movq	%rbx, %xmm1
	movq	-536(%rbp), %r9
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, %rdi
	leaq	-512(%rbp), %rbx
	movq	%rax, -504(%rbp)
	leaq	-80(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdx
	pushq	%rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r10, -512(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -560(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-544(%rbp), %r11
	movl	$495, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-528(%rbp), %rsi
	movq	%rbx, %rdx
	movl	$2, %edi
	pushq	%rdi
	movdqa	-560(%rbp), %xmm0
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rsi
	movq	-536(%rbp), %r9
	xorl	%esi, %esi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	-480(%rbp), %rax
	movl	$1, %ecx
	movq	%r10, -512(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22172:
	.size	_ZN2v88internal30AsyncGeneratorResolveAssembler33GenerateAsyncGeneratorResolveImplEv, .-_ZN2v88internal30AsyncGeneratorResolveAssembler33GenerateAsyncGeneratorResolveImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_AsyncGeneratorResolveEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"AsyncGeneratorResolve"
	.section	.text._ZN2v88internal8Builtins30Generate_AsyncGeneratorResolveEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_AsyncGeneratorResolveEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_AsyncGeneratorResolveEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_AsyncGeneratorResolveEPNS0_8compiler18CodeAssemblerStateE:
.LFB22168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$487, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$674, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L120
.L117:
	movq	%r13, %rdi
	call	_ZN2v88internal30AsyncGeneratorResolveAssembler33GenerateAsyncGeneratorResolveImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L117
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22168:
	.size	_ZN2v88internal8Builtins30Generate_AsyncGeneratorResolveEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_AsyncGeneratorResolveEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29AsyncGeneratorRejectAssembler32GenerateAsyncGeneratorRejectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AsyncGeneratorRejectAssembler32GenerateAsyncGeneratorRejectImplEv
	.type	_ZN2v88internal29AsyncGeneratorRejectAssembler32GenerateAsyncGeneratorRejectImplEv, @function
_ZN2v88internal29AsyncGeneratorRejectAssembler32GenerateAsyncGeneratorRejectImplEv:
.LFB22181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$80, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$80, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$494, %edx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%rbx, %r9
	movl	$3, %edi
	movq	%rax, %r8
	movq	-120(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -104(%rbp)
	movq	%r13, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22181:
	.size	_ZN2v88internal29AsyncGeneratorRejectAssembler32GenerateAsyncGeneratorRejectImplEv, .-_ZN2v88internal29AsyncGeneratorRejectAssembler32GenerateAsyncGeneratorRejectImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_AsyncGeneratorRejectEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"AsyncGeneratorReject"
	.section	.text._ZN2v88internal8Builtins29Generate_AsyncGeneratorRejectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_AsyncGeneratorRejectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_AsyncGeneratorRejectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_AsyncGeneratorRejectEPNS0_8compiler18CodeAssemblerStateE:
.LFB22177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$556, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$675, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L130
.L127:
	movq	%r13, %rdi
	call	_ZN2v88internal29AsyncGeneratorRejectAssembler32GenerateAsyncGeneratorRejectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L127
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22177:
	.size	_ZN2v88internal8Builtins29Generate_AsyncGeneratorRejectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_AsyncGeneratorRejectEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28AsyncGeneratorYieldAssembler31GenerateAsyncGeneratorYieldImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28AsyncGeneratorYieldAssembler31GenerateAsyncGeneratorYieldImplEv
	.type	_ZN2v88internal28AsyncGeneratorYieldAssembler31GenerateAsyncGeneratorYieldImplEv, @function
_ZN2v88internal28AsyncGeneratorYieldAssembler31GenerateAsyncGeneratorYieldImplEv:
.LFB22190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$80, %edx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	movl	$20, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$22, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %r9
	pushq	%r10
	pushq	%rdx
	movq	%r13, %rdx
	call	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22190:
	.size	_ZN2v88internal28AsyncGeneratorYieldAssembler31GenerateAsyncGeneratorYieldImplEv, .-_ZN2v88internal28AsyncGeneratorYieldAssembler31GenerateAsyncGeneratorYieldImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_AsyncGeneratorYieldEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"AsyncGeneratorYield"
	.section	.text._ZN2v88internal8Builtins28Generate_AsyncGeneratorYieldEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_AsyncGeneratorYieldEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_AsyncGeneratorYieldEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_AsyncGeneratorYieldEPNS0_8compiler18CodeAssemblerStateE:
.LFB22186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$569, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$676, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L138
.L135:
	movq	%r13, %rdi
	call	_ZN2v88internal28AsyncGeneratorYieldAssembler31GenerateAsyncGeneratorYieldImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L135
.L139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22186:
	.size	_ZN2v88internal8Builtins28Generate_AsyncGeneratorYieldEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_AsyncGeneratorYieldEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal42AsyncGeneratorYieldResolveClosureAssembler45GenerateAsyncGeneratorYieldResolveClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal42AsyncGeneratorYieldResolveClosureAssembler45GenerateAsyncGeneratorYieldResolveClosureImplEv
	.type	_ZN2v88internal42AsyncGeneratorYieldResolveClosureAssembler45GenerateAsyncGeneratorYieldResolveClosureImplEv, @function
_ZN2v88internal42AsyncGeneratorYieldResolveClosureAssembler45GenerateAsyncGeneratorYieldResolveClosureImplEv:
.LFB22199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	movl	$674, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	movq	%r14, -64(%rbp)
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	-128(%rbp), %r14
	movq	-96(%rbp), %rax
	pushq	%r15
	movq	%rbx, %xmm0
	movq	%r13, %r9
	movl	$1, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movhps	-136(%rbp), %xmm0
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-144(%rbp), %r11
	movl	$678, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	-96(%rbp), %rax
	movl	$1, %r9d
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%rbx, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L143:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22199:
	.size	_ZN2v88internal42AsyncGeneratorYieldResolveClosureAssembler45GenerateAsyncGeneratorYieldResolveClosureImplEv, .-_ZN2v88internal42AsyncGeneratorYieldResolveClosureAssembler45GenerateAsyncGeneratorYieldResolveClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins42Generate_AsyncGeneratorYieldResolveClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"AsyncGeneratorYieldResolveClosure"
	.section	.text._ZN2v88internal8Builtins42Generate_AsyncGeneratorYieldResolveClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins42Generate_AsyncGeneratorYieldResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins42Generate_AsyncGeneratorYieldResolveClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins42Generate_AsyncGeneratorYieldResolveClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$587, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$687, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L148
.L145:
	movq	%r13, %rdi
	call	_ZN2v88internal42AsyncGeneratorYieldResolveClosureAssembler45GenerateAsyncGeneratorYieldResolveClosureImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L145
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22195:
	.size	_ZN2v88internal8Builtins42Generate_AsyncGeneratorYieldResolveClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins42Generate_AsyncGeneratorYieldResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29AsyncGeneratorReturnAssembler32GenerateAsyncGeneratorReturnImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AsyncGeneratorReturnAssembler32GenerateAsyncGeneratorReturnImplEv
	.type	_ZN2v88internal29AsyncGeneratorReturnAssembler32GenerateAsyncGeneratorReturnImplEv, @function
_ZN2v88internal29AsyncGeneratorReturnAssembler32GenerateAsyncGeneratorReturnImplEv:
.LFB22208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-224(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-208(%rbp), %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$80, %edx
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$25, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-248(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-240(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$23, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$20, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$6, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-264(%rbp), %r9
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%rax, -256(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, -248(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-240(%rbp), %rdx
	movq	%r12, %rdi
	pushq	-232(%rbp)
	movq	-272(%rbp), %r11
	movq	%rax, %r9
	movq	-256(%rbp), %r10
	movq	-248(%rbp), %r8
	pushq	%rdx
	movq	%r13, %rdx
	movq	%r11, %rcx
	movq	%r10, %rsi
	call	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22208:
	.size	_ZN2v88internal29AsyncGeneratorReturnAssembler32GenerateAsyncGeneratorReturnImplEv, .-_ZN2v88internal29AsyncGeneratorReturnAssembler32GenerateAsyncGeneratorReturnImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_AsyncGeneratorReturnEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"AsyncGeneratorReturn"
	.section	.text._ZN2v88internal8Builtins29Generate_AsyncGeneratorReturnEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_AsyncGeneratorReturnEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_AsyncGeneratorReturnEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_AsyncGeneratorReturnEPNS0_8compiler18CodeAssemblerStateE:
.LFB22204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$603, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$677, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L158
.L155:
	movq	%r13, %rdi
	call	_ZN2v88internal29AsyncGeneratorReturnAssembler32GenerateAsyncGeneratorReturnImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L155
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22204:
	.size	_ZN2v88internal8Builtins29Generate_AsyncGeneratorReturnEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_AsyncGeneratorReturnEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins43Generate_AsyncGeneratorReturnResolveClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"AsyncGeneratorReturnResolveClosure"
	.section	.text._ZN2v88internal8Builtins43Generate_AsyncGeneratorReturnResolveClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins43Generate_AsyncGeneratorReturnResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins43Generate_AsyncGeneratorReturnResolveClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins43Generate_AsyncGeneratorReturnResolveClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$657, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$690, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L164
.L161:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L161
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22213:
	.size	_ZN2v88internal8Builtins43Generate_AsyncGeneratorReturnResolveClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins43Generate_AsyncGeneratorReturnResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal43AsyncGeneratorReturnResolveClosureAssembler46GenerateAsyncGeneratorReturnResolveClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal43AsyncGeneratorReturnResolveClosureAssembler46GenerateAsyncGeneratorReturnResolveClosureImplEv
	.type	_ZN2v88internal43AsyncGeneratorReturnResolveClosureAssembler46GenerateAsyncGeneratorReturnResolveClosureImplEv, @function
_ZN2v88internal43AsyncGeneratorReturnResolveClosureAssembler46GenerateAsyncGeneratorReturnResolveClosureImplEv:
.LFB22217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12_GLOBAL__N_131AsyncGeneratorBuiltinsAssembler32AsyncGeneratorAwaitResumeClosureEPNS0_8compiler4NodeES5_NS0_17JSGeneratorObject10ResumeModeE
	.cfi_endproc
.LFE22217:
	.size	_ZN2v88internal43AsyncGeneratorReturnResolveClosureAssembler46GenerateAsyncGeneratorReturnResolveClosureImplEv, .-_ZN2v88internal43AsyncGeneratorReturnResolveClosureAssembler46GenerateAsyncGeneratorReturnResolveClosureImplEv
	.section	.text._ZN2v88internal49AsyncGeneratorReturnClosedResolveClosureAssembler52GenerateAsyncGeneratorReturnClosedResolveClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal49AsyncGeneratorReturnClosedResolveClosureAssembler52GenerateAsyncGeneratorReturnClosedResolveClosureImplEv
	.type	_ZN2v88internal49AsyncGeneratorReturnClosedResolveClosureAssembler52GenerateAsyncGeneratorReturnClosedResolveClosureImplEv, @function
_ZN2v88internal49AsyncGeneratorReturnClosedResolveClosureAssembler52GenerateAsyncGeneratorReturnClosedResolveClosureImplEv:
.LFB22226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	movl	$674, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	movq	%r14, -64(%rbp)
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	-128(%rbp), %r14
	movq	-96(%rbp), %rax
	pushq	%r15
	movq	%rbx, %xmm0
	movq	%r13, %r9
	movl	$1, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movhps	-136(%rbp), %xmm0
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-144(%rbp), %r11
	movl	$678, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	-96(%rbp), %rax
	movl	$1, %r9d
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%rbx, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L171:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22226:
	.size	_ZN2v88internal49AsyncGeneratorReturnClosedResolveClosureAssembler52GenerateAsyncGeneratorReturnClosedResolveClosureImplEv, .-_ZN2v88internal49AsyncGeneratorReturnClosedResolveClosureAssembler52GenerateAsyncGeneratorReturnClosedResolveClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins49Generate_AsyncGeneratorReturnClosedResolveClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"AsyncGeneratorReturnClosedResolveClosure"
	.section	.text._ZN2v88internal8Builtins49Generate_AsyncGeneratorReturnClosedResolveClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins49Generate_AsyncGeneratorReturnClosedResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins49Generate_AsyncGeneratorReturnClosedResolveClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins49Generate_AsyncGeneratorReturnClosedResolveClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$667, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$688, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L176
.L173:
	movq	%r13, %rdi
	call	_ZN2v88internal49AsyncGeneratorReturnClosedResolveClosureAssembler52GenerateAsyncGeneratorReturnClosedResolveClosureImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L173
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22222:
	.size	_ZN2v88internal8Builtins49Generate_AsyncGeneratorReturnClosedResolveClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins49Generate_AsyncGeneratorReturnClosedResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal48AsyncGeneratorReturnClosedRejectClosureAssembler51GenerateAsyncGeneratorReturnClosedRejectClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal48AsyncGeneratorReturnClosedRejectClosureAssembler51GenerateAsyncGeneratorReturnClosedRejectClosureImplEv
	.type	_ZN2v88internal48AsyncGeneratorReturnClosedRejectClosureAssembler51GenerateAsyncGeneratorReturnClosedRejectClosureImplEv, @function
_ZN2v88internal48AsyncGeneratorReturnClosedRejectClosureAssembler51GenerateAsyncGeneratorReturnClosedRejectClosureImplEv:
.LFB22235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	movl	$675, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r13, %r9
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	movq	%rbx, %xmm0
	movq	-96(%rbp), %rax
	pushq	%r15
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r14, %rdx
	movq	%r12, %rdi
	movhps	-136(%rbp), %xmm0
	movl	$1, %ecx
	movq	%r10, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-144(%rbp), %r11
	movl	$678, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	-96(%rbp), %rax
	movl	$1, %r9d
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%rbx, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L181:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22235:
	.size	_ZN2v88internal48AsyncGeneratorReturnClosedRejectClosureAssembler51GenerateAsyncGeneratorReturnClosedRejectClosureImplEv, .-_ZN2v88internal48AsyncGeneratorReturnClosedRejectClosureAssembler51GenerateAsyncGeneratorReturnClosedRejectClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins48Generate_AsyncGeneratorReturnClosedRejectClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"AsyncGeneratorReturnClosedRejectClosure"
	.section	.text._ZN2v88internal8Builtins48Generate_AsyncGeneratorReturnClosedRejectClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins48Generate_AsyncGeneratorReturnClosedRejectClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins48Generate_AsyncGeneratorReturnClosedRejectClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins48Generate_AsyncGeneratorReturnClosedRejectClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$685, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$689, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L186
.L183:
	movq	%r13, %rdi
	call	_ZN2v88internal48AsyncGeneratorReturnClosedRejectClosureAssembler51GenerateAsyncGeneratorReturnClosedRejectClosureImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L183
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22231:
	.size	_ZN2v88internal8Builtins48Generate_AsyncGeneratorReturnClosedRejectClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins48Generate_AsyncGeneratorReturnClosedRejectClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE:
.LFB28509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28509:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_AsyncGeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
