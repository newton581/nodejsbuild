	.file	"builtins-microtask-queue-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB9034:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9034:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation:
.LFB26797:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26797:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB26800:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE26800:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation:
.LFB26801:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L12
	cmpl	$3, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L17
.L13:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26801:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26796:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$1800, %ecx
	movl	$8, %edx
	movq	8(%rax), %rdi
	movq	(%rax), %rsi
	jmp	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	.cfi_endproc
.LFE26796:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9028:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE9028:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB9027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9027:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB9036:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9036:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE:
.LFB22019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE(%rip), %ecx
	movl	$1968, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22019:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskRingBufferENS0_8compiler5TNodeINS0_7RawPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskRingBufferENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskRingBufferENS0_8compiler5TNodeINS0_7RawPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskRingBufferENS0_8compiler5TNodeINS0_7RawPtrTEEE:
.LFB22020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	_ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22020:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskRingBufferENS0_8compiler5TNodeINS0_7RawPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskRingBufferENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler25GetMicrotaskQueueCapacityENS0_8compiler5TNodeINS0_7RawPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25GetMicrotaskQueueCapacityENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25GetMicrotaskQueueCapacityENS0_8compiler5TNodeINS0_7RawPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25GetMicrotaskQueueCapacityENS0_8compiler5TNodeINS0_7RawPtrTEEE:
.LFB22021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	_ZN2v88internal14MicrotaskQueue15kCapacityOffsetE(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1029, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22021:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25GetMicrotaskQueueCapacityENS0_8compiler5TNodeINS0_7RawPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25GetMicrotaskQueueCapacityENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler21GetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21GetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21GetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21GetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEE:
.LFB22022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1029, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22022:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21GetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21GetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler21SetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21SetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21SetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21SetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE:
.LFB22023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	movl	$5, %esi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	.cfi_endproc
.LFE22023:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21SetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21SetMicrotaskQueueSizeENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEE:
.LFB22024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	_ZN2v88internal14MicrotaskQueue12kStartOffsetE(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1029, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22024:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler22SetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22SetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22SetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22SetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE:
.LFB22025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	_ZN2v88internal14MicrotaskQueue12kStartOffsetE(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	movl	$5, %esi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	.cfi_endproc
.LFE22025:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22SetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22SetMicrotaskQueueStartENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_:
.LFB22026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22026:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler31IncrementFinishedMicrotaskCountENS0_8compiler5TNodeINS0_7RawPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler31IncrementFinishedMicrotaskCountENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler31IncrementFinishedMicrotaskCountENS0_8compiler5TNodeINS0_7RawPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler31IncrementFinishedMicrotaskCountENS0_8compiler5TNodeINS0_7RawPtrTEEE:
.LFB22032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	_ZN2v88internal14MicrotaskQueue29kFinishedMicrotaskCountOffsetE(%rip), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1029, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	movl	$5, %esi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	.cfi_endproc
.LFE22032:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler31IncrementFinishedMicrotaskCountENS0_8compiler5TNodeINS0_7RawPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler31IncrementFinishedMicrotaskCountENS0_8compiler5TNodeINS0_7RawPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetCurrentContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetCurrentContextEv
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetCurrentContextEv, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetCurrentContextEv:
.LFB22033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22033:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetCurrentContextEv, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetCurrentContextEv
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler17SetCurrentContextENS0_8compiler5TNodeINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17SetCurrentContextENS0_8compiler5TNodeINS0_7ContextEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17SetCurrentContextENS0_8compiler5TNodeINS0_7ContextEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17SetCurrentContextENS0_8compiler5TNodeINS0_7ContextEEE:
.LFB22034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE22034:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17SetCurrentContextENS0_8compiler5TNodeINS0_7ContextEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17SetCurrentContextENS0_8compiler5TNodeINS0_7ContextEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetEnteredContextCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetEnteredContextCountEv
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetEnteredContextCountEv, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetEnteredContextCountEv:
.LFB22035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	_ZN2v88internal20DetachableVectorBase11kSizeOffsetE(%rip), %rsi
	addq	_ZN2v88internal22HandleScopeImplementer22kEnteredContextsOffsetE(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1029, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22035:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetEnteredContextCountEv, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetEnteredContextCountEv
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler21EnterMicrotaskContextENS0_8compiler5TNodeINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21EnterMicrotaskContextENS0_8compiler5TNodeINS0_7ContextEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21EnterMicrotaskContextENS0_8compiler5TNodeINS0_7ContextEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21EnterMicrotaskContextENS0_8compiler5TNodeINS0_7ContextEEE:
.LFB22036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r15
	leaq	-224(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	_ZN2v88internal22HandleScopeImplementer22kEnteredContextsOffsetE(%rip), %rax
	addq	_ZN2v88internal20DetachableVectorBase15kCapacityOffsetE(%rip), %rax
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	_ZN2v88internal22HandleScopeImplementer22kEnteredContextsOffsetE(%rip), %rax
	addq	_ZN2v88internal20DetachableVectorBase11kSizeOffsetE(%rip), %rax
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movl	$1029, %esi
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rbx, %rdx
	movl	$1029, %esi
	movq	%r12, %rdi
	movq	-496(%rbp), %r11
	movl	$2, %r8d
	movq	%rax, -504(%rbp)
	movq	%r11, %rcx
	movq	%r11, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-352(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r9, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-504(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-496(%rbp), %r9
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	movq	%r9, -512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	_ZN2v88internal22HandleScopeImplementer22kEnteredContextsOffsetE(%rip), %rax
	movq	%r12, %rdi
	addq	_ZN2v88internal20DetachableVectorBase11kDataOffsetE(%rip), %rax
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-496(%rbp), %r8
	movq	-488(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	-520(%rbp), %r11
	movq	%rax, %r8
	movq	%rax, -504(%rbp)
	movq	%r11, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	_ZN2v88internal20DetachableVectorBase11kDataOffsetE(%rip), %rax
	movq	%r12, %rdi
	addq	_ZN2v88internal22HandleScopeImplementer25kIsMicrotaskContextOffsetE(%rip), %rax
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-496(%rbp), %rdx
	movq	%rax, %r8
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	_ZN2v88internal20DetachableVectorBase11kSizeOffsetE(%rip), %rax
	movq	%r12, %rdi
	addq	_ZN2v88internal22HandleScopeImplementer25kIsMicrotaskContextOffsetE(%rip), %rax
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	-504(%rbp), %r10
	movq	%rax, %rcx
	movq	%r10, %r8
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-512(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	call	_ZN2v88internal17ExternalReference27call_enter_context_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-488(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$5, %ecx
	movl	$2, %r8d
	movw	%dx, -96(%rbp)
	movl	$516, %edx
	movw	%cx, -80(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rbx, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22036:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21EnterMicrotaskContextENS0_8compiler5TNodeINS0_7ContextEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21EnterMicrotaskContextENS0_8compiler5TNodeINS0_7ContextEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE:
.LFB22027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1968, %edx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE(%rip), %ecx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler21EnterMicrotaskContextENS0_8compiler5TNodeINS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE22027:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE:
.LFB22059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference32handle_scope_implementer_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	_ZN2v88internal20DetachableVectorBase11kSizeOffsetE(%rip), %r14
	movq	_ZN2v88internal22HandleScopeImplementer22kEnteredContextsOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	addq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	_ZN2v88internal22HandleScopeImplementer25kIsMicrotaskContextOffsetE(%rip), %rsi
	movq	%r12, %rdi
	addq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	movl	$5, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	.cfi_endproc
.LFE22059:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE:
.LFB22060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	xorl	%ecx, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-384(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation(%rip), %rbx
	subq	$408, %rsp
	movl	%esi, -420(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%rdx, -432(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -408(%rbp)
	movq	%r15, -400(%rbp)
	leaq	-96(%rbp), %r15
	movq	%r12, -392(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19IsPromiseCapabilityENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-128(%rbp), %r9
	leaq	-408(%rbp), %rax
	movl	$7, %r8d
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r9, %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10HeapObjectEZNS2_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS2_7Runtime10FunctionIdENS2_8compiler5TNodeINS2_7ContextEEENS9_11SloppyTNodeIS5_EEEUlvE_ZNS6_14RunPromiseHookES8_SC_SE_EUlvE0_EENSA_IT_EENSD_INS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation(%rip), %rbx
	movq	%rax, %xmm1
	leaq	-400(%rbp), %rax
	movq	%r9, -440(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -128(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10HeapObjectEZNS1_31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS1_7Runtime10FunctionIdENS2_5TNodeINS1_7ContextEEENS2_11SloppyTNodeIS8_EEEUlvE_ZNS9_14RunPromiseHookESB_SE_SG_EUlvE0_EENSC_IT_EENSF_INS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, %xmm2
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, %rbx
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L56
	movq	-440(%rbp), %r9
	movl	$3, %edx
	movq	%r9, %rsi
	movq	%r9, %rdi
	call	*%rax
.L56:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L57
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L57:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	-432(%rbp), %rdx
	movl	-420(%rbp), %esi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22060:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal31MicrotaskQueueBuiltinsAssembler18RunSingleMicrotaskENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_9MicrotaskEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler18RunSingleMicrotaskENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_9MicrotaskEEE
	.type	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler18RunSingleMicrotaskENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_9MicrotaskEEE, @function
_ZN2v88internal31MicrotaskQueueBuiltinsAssembler18RunSingleMicrotaskENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_9MicrotaskEEE:
.LFB22028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-672(%rbp), %rcx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1056(%rbp), %r14
	pushq	%r13
	movq	%r14, %xmm1
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	-1184(%rbp), %r15
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-800(%rbp), %rbx
	movq	%rbx, %xmm0
	subq	$1352, %rsp
	movq	%rsi, -1264(%rbp)
	movl	$587, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-928(%rbp), %rax
	movq	%rcx, -1288(%rbp)
	movq	%rax, -1280(%rbp)
	movq	%rbx, -1296(%rbp)
	leaq	-1248(%rbp), %rbx
	movhps	-1288(%rbp), %xmm0
	movhps	-1280(%rbp), %xmm1
	movaps	%xmm0, -1328(%rbp)
	movaps	%xmm1, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9StoreRootENS0_9RootIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler22GetEnteredContextCountEv
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -1256(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, -1336(%rbp)
	leaq	-288(%rbp), %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1280(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1296(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1288(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-544(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-416(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -1272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1272(%rbp), %r10
	movq	%r12, %rdi
	movq	-1312(%rbp), %r11
	movq	-1304(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm2
	movl	$5, %r9d
	leaq	-96(%rbp), %r8
	movdqa	-1360(%rbp), %xmm1
	movdqa	-1328(%rbp), %xmm0
	movq	%r11, %rsi
	leaq	-160(%rbp), %rcx
	movq	%rdx, -64(%rbp)
	movq	%r10, %rdx
	movq	%r10, -1384(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	$114, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-1336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE
	movq	%r13, %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10JSReceiverEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1360(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-1216(%rbp), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -1312(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1360(%rbp), %rcx
	xorl	%esi, %esi
	movl	$3, %edi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-1272(%rbp), %xmm0
	movq	%rcx, -112(%rbp)
	leaq	-128(%rbp), %rcx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r9
	movq	-1200(%rbp), %rax
	pushq	%rcx
	leaq	-1232(%rbp), %r11
	movhps	-1328(%rbp), %xmm0
	movq	%r9, -1232(%rbp)
	movq	-1344(%rbp), %r9
	movq	%r11, %rdx
	movq	%rcx, -1272(%rbp)
	movl	$1, %ecx
	movq	%r11, -1376(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-1264(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1272(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	-1328(%rbp), %xmm0
	movq	%rax, %xmm3
	movl	$157, %esi
	movq	-1264(%rbp), %rdx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%rax, -1360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1312(%rbp), %rdi
	movl	$508, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	movq	-1272(%rbp), %rsi
	movq	-1376(%rbp), %r11
	movq	%rax, %r8
	movq	-1200(%rbp), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r9
	movq	%r11, %rdx
	xorl	%esi, %esi
	movq	-1344(%rbp), %xmm0
	movq	%r9, -1232(%rbp)
	movq	%r12, %rdi
	movq	-1368(%rbp), %r9
	movq	%rax, -1224(%rbp)
	movq	-1360(%rbp), %rax
	movhps	-1328(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-1264(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%rax, -1360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1368(%rbp), %r9
	movl	$274, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -1328(%rbp)
	movq	%r9, %rdx
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1312(%rbp), %rdi
	movl	$507, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	movq	-1272(%rbp), %rsi
	movq	-1376(%rbp), %r11
	movq	%rax, %r8
	movq	-1200(%rbp), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r9
	movq	%r11, %rdx
	xorl	%esi, %esi
	movq	-1344(%rbp), %xmm0
	movq	%r9, -1232(%rbp)
	movq	%r12, %rdi
	movq	-1368(%rbp), %r9
	movq	%rax, -1224(%rbp)
	movq	-1328(%rbp), %rax
	movhps	-1360(%rbp), %xmm0
	movq	%r11, -1368(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%r9, -1360(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1360(%rbp), %r9
	movl	$273, %esi
	movq	%r12, %rdi
	movq	-1328(%rbp), %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-1264(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler17PrepareForContextENS0_8compiler5TNodeINS0_7ContextEEEPNS2_18CodeAssemblerLabelE
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%rax, -1360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1344(%rbp), %r9
	movl	$274, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %r13
	movq	%r9, %rdx
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1312(%rbp), %rdi
	movl	$506, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, -112(%rbp)
	movl	$3, %edi
	movq	-1368(%rbp), %r11
	pushq	%rdi
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r9
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-1328(%rbp), %xmm0
	movq	%r9, -1232(%rbp)
	movq	%r11, %rdx
	movq	-1344(%rbp), %r9
	movq	-1200(%rbp), %rax
	movhps	-1360(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r9, -1312(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r13, %rcx
	movl	$273, %esi
	movq	%r12, %rdi
	movq	-1312(%rbp), %r9
	movq	%r9, %rdx
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler14RunPromiseHookENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_7ContextEEENS4_11SloppyTNodeINS0_10HeapObjectEEE
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-1264(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1384(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1264(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movl	$155, %esi
	movq	%r12, %rdi
	movq	-1272(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler20RewindEnteredContextENS0_8compiler5TNodeINS0_7IntPtrTEEE
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1296(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22028:
	.size	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler18RunSingleMicrotaskENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_9MicrotaskEEE, .-_ZN2v88internal31MicrotaskQueueBuiltinsAssembler18RunSingleMicrotaskENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_9MicrotaskEEE
	.section	.text._ZN2v88internal25EnqueueMicrotaskAssembler28GenerateEnqueueMicrotaskImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25EnqueueMicrotaskAssembler28GenerateEnqueueMicrotaskImplEv
	.type	_ZN2v88internal25EnqueueMicrotaskAssembler28GenerateEnqueueMicrotaskImplEv, @function
_ZN2v88internal25EnqueueMicrotaskAssembler28GenerateEnqueueMicrotaskImplEv:
.LFB22074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE(%rip), %ecx
	movl	$1968, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	_ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	_ZN2v88internal14MicrotaskQueue15kCapacityOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$1029, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$1029, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	_ZN2v88internal14MicrotaskQueue12kStartOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$1029, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-384(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r10, -392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-400(%rbp), %r11
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-392(%rbp), %r10
	movq	%rax, -384(%rbp)
	movq	%r11, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_
	movq	-408(%rbp), %r9
	movl	$5, %esi
	movq	%r12, %rdi
	movq	-384(%rbp), %r8
	movq	%rax, %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %r8
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, %r13
	call	_ZN2v88internal17ExternalReference31call_enqueue_microtask_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1029, %edx
	movl	$1800, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$5, %eax
	movl	$3, %r8d
	movw	%dx, -96(%rbp)
	movw	%ax, -112(%rbp)
	movq	-376(%rbp), %rax
	movl	$1800, %edx
	movw	%cx, -80(%rbp)
	leaq	-112(%rbp), %rcx
	movq	%r13, -104(%rbp)
	movq	%rbx, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L74
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L74:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22074:
	.size	_ZN2v88internal25EnqueueMicrotaskAssembler28GenerateEnqueueMicrotaskImplEv, .-_ZN2v88internal25EnqueueMicrotaskAssembler28GenerateEnqueueMicrotaskImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/v8/src/builtins/builtins-microtask-queue-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"EnqueueMicrotask"
	.section	.text._ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE:
.LFB22070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$448, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$156, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L79
.L76:
	movq	%r13, %rdi
	call	_ZN2v88internal25EnqueueMicrotaskAssembler28GenerateEnqueueMicrotaskImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L76
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22070:
	.size	_ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_EnqueueMicrotaskEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal22RunMicrotasksAssembler25GenerateRunMicrotasksImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22RunMicrotasksAssembler25GenerateRunMicrotasksImplEv
	.type	_ZN2v88internal22RunMicrotasksAssembler25GenerateRunMicrotasksImplEv, @function
_ZN2v88internal22RunMicrotasksAssembler25GenerateRunMicrotasksImplEv:
.LFB22125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$3, %edi
	movq	%rax, %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1029, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -328(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	_ZN2v88internal14MicrotaskQueue17kRingBufferOffsetE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	_ZN2v88internal14MicrotaskQueue15kCapacityOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1029, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	_ZN2v88internal14MicrotaskQueue12kStartOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1029, %esi
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-336(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%r9, %rsi
	movq	%r9, -352(%rbp)
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler25CalculateRingBufferOffsetENS0_8compiler5TNodeINS0_7IntPtrTEEES5_S5_
	movq	-344(%rbp), %r11
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastWordToTaggedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-328(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-352(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-328(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	_ZN2v88internal14MicrotaskQueue11kSizeOffsetE(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	-336(%rbp), %r8
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	_ZN2v88internal14MicrotaskQueue12kStartOffsetE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	-344(%rbp), %r11
	movq	-360(%rbp), %r10
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler18RunSingleMicrotaskENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_9MicrotaskEEE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31MicrotaskQueueBuiltinsAssembler31IncrementFinishedMicrotaskCountENS0_8compiler5TNodeINS0_7RawPtrTEEE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$587, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9StoreRootENS0_9RootIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22125:
	.size	_ZN2v88internal22RunMicrotasksAssembler25GenerateRunMicrotasksImplEv, .-_ZN2v88internal22RunMicrotasksAssembler25GenerateRunMicrotasksImplEv
	.section	.rodata._ZN2v88internal8Builtins22Generate_RunMicrotasksEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"RunMicrotasks"
	.section	.text._ZN2v88internal8Builtins22Generate_RunMicrotasksEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_RunMicrotasksEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_RunMicrotasksEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_RunMicrotasksEPNS0_8compiler18CodeAssemblerStateE:
.LFB22121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$498, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$158, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L89
.L86:
	movq	%r13, %rdi
	call	_ZN2v88internal22RunMicrotasksAssembler25GenerateRunMicrotasksImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L86
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22121:
	.size	_ZN2v88internal8Builtins22Generate_RunMicrotasksEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_RunMicrotasksEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE, @function
_GLOBAL__sub_I__ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE:
.LFB28427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28427:
	.size	_GLOBAL__sub_I__ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE, .-_GLOBAL__sub_I__ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal31MicrotaskQueueBuiltinsAssembler17GetMicrotaskQueueENS0_8compiler5TNodeINS0_7ContextEEE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10JSReceiverEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10JSReceiverEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10JSReceiverEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10JSReceiverEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10JSReceiverEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10JSReceiverEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7ContextEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE:
	.byte	5
	.byte	0
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	110
	.long	111
	.long	112
	.long	113
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
