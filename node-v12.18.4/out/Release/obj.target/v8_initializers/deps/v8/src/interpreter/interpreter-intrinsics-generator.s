	.file	"interpreter-intrinsics-generator.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation:
.LFB26468:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26468:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation:
.LFB26472:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26472:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation:
.LFB26477:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26477:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation:
.LFB26481:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L25
	cmpl	$3, %edx
	je	.L26
	cmpl	$1, %edx
	je	.L30
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26481:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rax), %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26467:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rax), %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26476:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB26480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	16(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26480:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB26471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	(%rdi), %rax
	movq	(%rax), %r14
	movq	8(%rax), %rsi
	movl	16(%rax), %r15d
	movq	16(%r14), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	16(%r14), %r14
	movl	%r15d, %esi
	movq	%rax, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26471:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator19CompareInstanceTypeEPNS0_8compiler4NodeEiNS2_23InstanceTypeCompareModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator19CompareInstanceTypeEPNS0_8compiler4NodeEiNS2_23InstanceTypeCompareModeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator19CompareInstanceTypeEPNS0_8compiler4NodeEiNS2_23InstanceTypeCompareModeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator19CompareInstanceTypeEPNS0_8compiler4NodeEiNS2_23InstanceTypeCompareModeE:
.LFB21893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	testl	%r14d, %r14d
	movq	16(%rbx), %r14
	movl	%r13d, %esi
	movq	%rax, %r12
	movq	%r14, %rdi
	jne	.L44
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler23Int32GreaterThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21893:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator19CompareInstanceTypeEPNS0_8compiler4NodeEiNS2_23InstanceTypeCompareModeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator19CompareInstanceTypeEPNS0_8compiler4NodeEiNS2_23InstanceTypeCompareModeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS0_8compiler4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS0_8compiler4NodeEi
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS0_8compiler4NodeEi, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS0_8compiler4NodeEi:
.LFB21894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r13
	pushq	%r12
	subq	$136, %rsp
	.cfi_offset 12, -40
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -152(%rbp)
	movq	%r12, %rdi
	movl	%edx, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rsi
	leaq	-144(%rbp), %rax
	movq	%rcx, %xmm0
	movl	$7, %r8d
	movq	%rax, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm2
	leaq	-152(%rbp), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -112(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, %xmm3
	movq	%rcx, %xmm0
	movq	%r13, %rcx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, %r12
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L48
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L48:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L47
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L47:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21894:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS0_8compiler4NodeEi, .-_ZN2v88internal11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS0_8compiler4NodeEi
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %r12
	movq	%rbx, %xmm0
	movq	%rbx, -136(%rbp)
	movq	%rax, %xmm1
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, %rdi
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rsi
	leaq	-128(%rbp), %rax
	movq	%rcx, %xmm0
	movl	$7, %r8d
	movq	%rax, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS6_20InterpreterAssembler15RegListNodePairEPNS2_8compiler4NodeEEUlvE_ZNS7_12IsJSReceiverESB_SE_EUlvE0_EENSC_5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm2
	leaq	-136(%rbp), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -112(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS9_20InterpreterAssembler15RegListNodePairES4_EUlvE_ZNSA_12IsJSReceiverESE_S4_EUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, %xmm3
	movq	%rcx, %xmm0
	movq	%r13, %rcx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, %r12
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L60
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L60:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L59
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L59:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21897:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator7IsArrayERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator7IsArrayERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator7IsArrayERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator7IsArrayERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %r12
	movq	%rbx, %xmm0
	movl	$1061, -128(%rbp)
	movq	%rax, %xmm1
	movq	%rax, %rsi
	movq	%rbx, -152(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, %rdi
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rsi
	leaq	-144(%rbp), %rax
	movq	%rcx, %xmm0
	movl	$7, %r8d
	movq	%rax, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_7OddballEZNS2_11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS2_8compiler4NodeEiEUlvE_ZNS7_14IsInstanceTypeESA_iEUlvE0_EENS8_5TNodeIT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm2
	leaq	-152(%rbp), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -112(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_7OddballEZNS1_11interpreter19IntrinsicsGenerator14IsInstanceTypeES4_iEUlvE_ZNSA_14IsInstanceTypeES4_iEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, %xmm3
	movq	%rcx, %xmm0
	movq	%r13, %rcx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, %r12
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L72
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L72:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L71
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L71:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	subq	$-128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21900:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator7IsArrayERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator7IsArrayERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator5IsSmiERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator5IsSmiERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator5IsSmiERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator5IsSmiERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %r12
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21901:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator5IsSmiERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator5IsSmiERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE:
.LFB21902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	16(%rcx), %rax
	movq	8(%rdi), %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	8(%rax), %ebx
	leal	2(%rbx), %eax
	movslq	%eax, %rsi
	movl	%eax, -100(%rbp)
	movq	16(%rdi), %rax
	salq	$3, %rsi
	movq	%rax, %rcx
	movq	%rax, -88(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L94
	addq	-88(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L87:
	movq	-96(%rbp), %rax
	movq	16(%r15), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, (%rcx)
	testl	%ebx, %ebx
	jle	.L91
	leaq	8(%rcx), %r13
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L89:
	movq	16(%r15), %rdi
	movl	%r14d, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	addq	$8, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	%rax, -8(%r13)
	cmpl	%ebx, %r14d
	jne	.L89
	leal	1(%r14), %eax
	salq	$3, %rax
.L88:
	movq	-88(%rbp), %r9
	movq	-112(%rbp), %rbx
	leaq	-80(%rbp), %rdx
	xorl	%esi, %esi
	movl	-100(%rbp), %r8d
	movl	$1, %ecx
	movq	%rbx, (%r9,%rax)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	16(%r15), %rdi
	movq	%rax, -80(%rbp)
	movq	-96(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9CallStubNENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmiPKPNS1_4NodeE@PLT
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L95
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	$8, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L94:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -88(%rbp)
	jmp	.L87
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21902:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator22IntrinsicAsBuiltinCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeENS0_8Builtins4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator22IntrinsicAsBuiltinCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeENS0_8Builtins4NameE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator22IntrinsicAsBuiltinCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeENS0_8Builtins4NameE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator22IntrinsicAsBuiltinCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeENS0_8Builtins4NameE:
.LFB21903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	%ecx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L99
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21903:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator22IntrinsicAsBuiltinCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeENS0_8Builtins4NameE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator22IntrinsicAsBuiltinCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeENS0_8Builtins4NameE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator18CopyDataPropertiesERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator18CopyDataPropertiesERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator18CopyDataPropertiesERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator18CopyDataPropertiesERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$161, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L103
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L103:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21910:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator18CopyDataPropertiesERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator18CopyDataPropertiesERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator22CreateIterResultObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator22CreateIterResultObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator22CreateIterResultObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator22CreateIterResultObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$350, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L107
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L107:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21911:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator22CreateIterResultObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator22CreateIterResultObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator11HasPropertyERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator11HasPropertyERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator11HasPropertyERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator11HasPropertyERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$159, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L111
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L111:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21912:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator11HasPropertyERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator11HasPropertyERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator10ToStringRTERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator10ToStringRTERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator10ToStringRTERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator10ToStringRTERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$785, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L115
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21913:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator10ToStringRTERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator10ToStringRTERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator8ToLengthERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator8ToLengthERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator8ToLengthERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator8ToLengthERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$108, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L119
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21914:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator8ToLengthERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator8ToLengthERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator8ToObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator8ToObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator8ToObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator8ToObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$91, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L123
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21915:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator8ToObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator8ToObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator4CallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator4CallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator4CallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator4CallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %r15
	movl	$1, %esi
	movq	%rax, %r13
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	8(%r12), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	16(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler30RegisterLocationInRegisterListERKNS2_15RegListNodePairEi@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	movq	%rax, %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	jne	.L128
.L125:
	movq	16(%rbx), %rdi
	movl	$2, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	-208(%rbp), %rcx
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$184, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	leaq	-192(%rbp), %r12
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %r15
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-200(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movl	$53, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L125
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21916:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator4CallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator4CallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator27CreateAsyncFromSyncIteratorERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator27CreateAsyncFromSyncIteratorERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator27CreateAsyncFromSyncIteratorERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator27CreateAsyncFromSyncIteratorERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	leaq	-400(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$392, %rsp
	movq	%rdx, -424(%rbp)
	movq	16(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	movl	$8, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %r12
	movq	%rax, %rsi
	movq	%rax, -408(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %r12
	movq	-408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rcx
	movq	16(%rbx), %r12
	leaq	2912(%rcx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -416(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	leaq	-368(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movl	$2, %edi
	movq	-424(%rbp), %r10
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-408(%rbp), %xmm0
	pushq	%rsi
	movq	-352(%rbp), %rax
	movq	%r10, %r9
	xorl	%esi, %esi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movhps	-416(%rbp), %xmm0
	leaq	-384(%rbp), %rdx
	movq	%r10, -416(%rbp)
	movq	%rcx, -384(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-416(%rbp), %r10
	movq	16(%rbx), %rdi
	movq	%rax, -424(%rbp)
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	16(%rbx), %rdi
	movl	$13, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	16(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	-408(%rbp), %rcx
	movq	16(%rbx), %rdi
	movl	$8, %r8d
	movq	%rax, %rsi
	movl	$24, %edx
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-424(%rbp), %r11
	movq	16(%rbx), %rdi
	movl	$8, %r8d
	movq	-408(%rbp), %rsi
	movl	$32, %edx
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-408(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	-416(%rbp), %r10
	movl	$176, %esi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L133:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21917:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator27CreateAsyncFromSyncIteratorERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator27CreateAsyncFromSyncIteratorERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator23CreateJSGeneratorObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator23CreateJSGeneratorObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator23CreateJSGeneratorObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator23CreateJSGeneratorObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$351, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L137
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L137:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21918:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator23CreateJSGeneratorObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator23CreateJSGeneratorObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator22GeneratorGetResumeModeERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator22GeneratorGetResumeModeERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator22GeneratorGetResumeModeERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator22GeneratorGetResumeModeERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	movl	$1800, %ecx
	popq	%rbx
	movq	%rax, %rsi
	movl	$56, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	.cfi_endproc
.LFE21919:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator22GeneratorGetResumeModeERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator22GeneratorGetResumeModeERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator14GeneratorCloseERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator14GeneratorCloseERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator14GeneratorCloseERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator14GeneratorCloseERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %r13
	movl	$-1, %esi
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$64, %edx
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21920:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator14GeneratorCloseERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator14GeneratorCloseERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator19GetImportMetaObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator19GetImportMetaObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator19GetImportMetaObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator19GetImportMetaObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-208(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler17LoadModuleContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	16(%rbx), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	16(%rbx), %rdi
	movl	$1800, %ecx
	movl	$88, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	16(%rbx), %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler9IsTheHoleENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-216(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movl	$192, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L145:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21921:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator19GetImportMetaObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator19GetImportMetaObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator24AsyncFunctionAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator24AsyncFunctionAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator24AsyncFunctionAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator24AsyncFunctionAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$230, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L149
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L149:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21922:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator24AsyncFunctionAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator24AsyncFunctionAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator26AsyncFunctionAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator26AsyncFunctionAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator26AsyncFunctionAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator26AsyncFunctionAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$231, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L153
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21923:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator26AsyncFunctionAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator26AsyncFunctionAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator18AsyncFunctionEnterERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator18AsyncFunctionEnterERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator18AsyncFunctionEnterERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator18AsyncFunctionEnterERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$226, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L157
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L157:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21924:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator18AsyncFunctionEnterERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator18AsyncFunctionEnterERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncFunctionRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncFunctionRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncFunctionRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncFunctionRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$227, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L161
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21925:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncFunctionRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncFunctionRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncFunctionResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncFunctionResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncFunctionResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncFunctionResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$228, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L165
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L165:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21926:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncFunctionResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncFunctionResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator25AsyncGeneratorAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator25AsyncGeneratorAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator25AsyncGeneratorAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator25AsyncGeneratorAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$683, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L169
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21927:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator25AsyncGeneratorAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator25AsyncGeneratorAwaitCaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator27AsyncGeneratorAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator27AsyncGeneratorAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator27AsyncGeneratorAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator27AsyncGeneratorAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$684, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L173
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L173:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21928:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator27AsyncGeneratorAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator27AsyncGeneratorAwaitUncaughtERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncGeneratorRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncGeneratorRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncGeneratorRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncGeneratorRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$675, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L177
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L177:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21929:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncGeneratorRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator20AsyncGeneratorRejectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator21AsyncGeneratorResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator21AsyncGeneratorResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator21AsyncGeneratorResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator21AsyncGeneratorResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$674, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L181
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L181:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21930:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator21AsyncGeneratorResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator21AsyncGeneratorResolveERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncGeneratorYieldERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncGeneratorYieldERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncGeneratorYieldERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncGeneratorYieldERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE:
.LFB21931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdx, %r14
	movl	$676, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L185
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21931:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncGeneratorYieldERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator19AsyncGeneratorYieldERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE:
.LFB21932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	16(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %r15
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movl	$53, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L189:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21932:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	.section	.text._ZN2v88internal11interpreter19IntrinsicsGenerator15InvokeIntrinsicEPNS0_8compiler4NodeES5_RKNS1_20InterpreterAssembler15RegListNodePairE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter19IntrinsicsGenerator15InvokeIntrinsicEPNS0_8compiler4NodeES5_RKNS1_20InterpreterAssembler15RegListNodePairE
	.type	_ZN2v88internal11interpreter19IntrinsicsGenerator15InvokeIntrinsicEPNS0_8compiler4NodeES5_RKNS1_20InterpreterAssembler15RegListNodePairE, @function
_ZN2v88internal11interpreter19IntrinsicsGenerator15InvokeIntrinsicEPNS0_8compiler4NodeES5_RKNS1_20InterpreterAssembler15RegListNodePairE:
.LFB21892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-3056(%rbp), %r10
	movq	%rdx, %r14
	movq	%rsi, %r13
	leaq	-3568(%rbp), %rax
	leaq	-3312(%rbp), %rdx
	movq	%r10, -4024(%rbp)
	movq	%r10, %xmm9
	leaq	-2800(%rbp), %r9
	movq	%rax, %xmm11
	movq	%rcx, %r12
	movq	%rdi, %rbx
	movq	%rax, -3984(%rbp)
	leaq	-3440(%rbp), %rcx
	leaq	-3184(%rbp), %rsi
	movq	%rdx, %xmm10
	movq	%rdx, -4056(%rbp)
	leaq	-2928(%rbp), %r11
	leaq	-2672(%rbp), %r15
	movq	%r9, %xmm8
	leaq	-2416(%rbp), %rax
	leaq	-2160(%rbp), %rdx
	movq	%rcx, -3920(%rbp)
	leaq	-1904(%rbp), %r10
	movq	%rsi, -3888(%rbp)
	leaq	-2544(%rbp), %r8
	movq	%r11, -3952(%rbp)
	leaq	-2288(%rbp), %rcx
	leaq	-2032(%rbp), %rsi
	movq	%r8, %xmm7
	movq	%r15, -3928(%rbp)
	leaq	-1776(%rbp), %r11
	movq	%rcx, %xmm6
	movq	%rsi, %xmm5
	movq	%rax, -3896(%rbp)
	movhps	-3920(%rbp), %xmm11
	movhps	-3888(%rbp), %xmm10
	movq	%r11, %xmm4
	movq	%rdx, -3960(%rbp)
	movhps	-3952(%rbp), %xmm9
	leaq	-1264(%rbp), %rax
	leaq	-1520(%rbp), %r15
	movq	%r10, -3936(%rbp)
	movhps	-3928(%rbp), %xmm8
	leaq	-752(%rbp), %r10
	leaq	-1008(%rbp), %rdx
	movq	%r9, -3992(%rbp)
	leaq	-1648(%rbp), %r9
	movhps	-3896(%rbp), %xmm7
	movq	%r10, %xmm0
	movq	%r9, -3904(%rbp)
	movhps	-3960(%rbp), %xmm6
	movq	%rax, %xmm2
	leaq	-3824(%rbp), %r9
	movhps	-3936(%rbp), %xmm5
	movq	%r8, -4064(%rbp)
	leaq	-1392(%rbp), %r8
	movq	%r15, %xmm3
	movq	%rcx, -4032(%rbp)
	leaq	-1136(%rbp), %rcx
	movq	%rdx, %xmm1
	movq	%rsi, -4000(%rbp)
	leaq	-880(%rbp), %rsi
	movq	%r11, -4072(%rbp)
	leaq	-624(%rbp), %r11
	movaps	%xmm11, -4288(%rbp)
	movaps	%xmm10, -4272(%rbp)
	movaps	%xmm9, -4256(%rbp)
	movaps	%xmm8, -4240(%rbp)
	movaps	%xmm7, -4224(%rbp)
	movaps	%xmm6, -4208(%rbp)
	movaps	%xmm5, -4192(%rbp)
	movhps	-3904(%rbp), %xmm4
	movq	%r11, -3976(%rbp)
	movq	%r8, -3968(%rbp)
	movl	$1, %r8d
	movq	%rcx, -3944(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -3912(%rbp)
	movq	16(%rdi), %rsi
	movhps	-3976(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-3968(%rbp), %xmm3
	movq	%rdx, -4080(%rbp)
	xorl	%edx, %edx
	movhps	-3944(%rbp), %xmm2
	movq	%r10, -4048(%rbp)
	movhps	-3912(%rbp), %xmm1
	movaps	%xmm4, -4176(%rbp)
	movaps	%xmm3, -4160(%rbp)
	movaps	%xmm2, -4144(%rbp)
	movaps	%xmm1, -4128(%rbp)
	movaps	%xmm0, -4112(%rbp)
	movq	%rax, -4008(%rbp)
	movq	%r15, -4040(%rbp)
	leaq	-3872(%rbp), %r15
	movq	%r9, -4088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-3696(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	16(%rbx), %rsi
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r9, -3880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	movl	$8, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3984(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3920(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4056(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3888(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4024(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3952(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3992(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	-3928(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4064(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3896(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4032(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3960(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4000(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3936(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4072(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3904(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4040(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3968(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4008(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3944(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4080(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3912(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-4048(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	-3976(%rbp), %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-496(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	16(%rbx), %rsi
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%rax, -4016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movdqa	-4112(%rbp), %xmm0
	movq	%r13, %rsi
	movdqa	-4288(%rbp), %xmm11
	movdqa	-4272(%rbp), %xmm10
	movdqa	-4224(%rbp), %xmm7
	leaq	-368(%rbp), %rcx
	movl	$25, %r9d
	movaps	%xmm0, -80(%rbp)
	movdqa	.LC1(%rip), %xmm0
	movdqa	-4256(%rbp), %xmm9
	leaq	-256(%rbp), %r8
	movdqa	-4240(%rbp), %xmm8
	movdqa	-4208(%rbp), %xmm6
	movaps	%xmm11, -256(%rbp)
	movaps	%xmm0, -368(%rbp)
	movdqa	.LC2(%rip), %xmm0
	movdqa	-4192(%rbp), %xmm5
	movdqa	-4176(%rbp), %xmm4
	movdqa	-4160(%rbp), %xmm3
	movaps	%xmm10, -240(%rbp)
	movdqa	-4144(%rbp), %xmm2
	movdqa	-4128(%rbp), %xmm1
	movaps	%xmm0, -352(%rbp)
	movq	-4016(%rbp), %rax
	movdqa	.LC3(%rip), %xmm0
	movaps	%xmm9, -224(%rbp)
	movaps	%xmm8, -208(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -336(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movq	16(%rbx), %rdi
	movq	-4088(%rbp), %rdx
	movl	$24, -272(%rbp)
	movaps	%xmm0, -320(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm0, -304(%rbp)
	movdqa	.LC6(%rip), %xmm0
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	16(%rbx), %rdi
	movq	-3984(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L317
.L191:
	movq	(%rbx), %rsi
	leaq	-3856(%rbp), %r13
	movl	$230, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L192
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L192:
	movq	16(%rbx), %rdi
	movq	-3920(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L318
.L193:
	movq	(%rbx), %rsi
	movl	$231, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L194
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L194:
	movq	16(%rbx), %rdi
	movq	-4056(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L319
.L195:
	movq	(%rbx), %rsi
	movl	$226, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L196
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L196:
	movq	16(%rbx), %rdi
	movq	-3888(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L320
.L197:
	movq	(%rbx), %rsi
	movl	$227, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L198
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L198:
	movq	16(%rbx), %rdi
	movq	-4024(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L321
.L199:
	movq	(%rbx), %rsi
	movl	$228, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L200
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L200:
	movq	16(%rbx), %rdi
	movq	-3952(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L322
.L201:
	movq	(%rbx), %rsi
	movl	$683, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L202
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L202:
	movq	16(%rbx), %rdi
	movq	-3992(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L323
.L203:
	movq	(%rbx), %rsi
	movl	$684, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L204
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L204:
	movq	16(%rbx), %rdi
	movq	-3928(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L324
.L205:
	movq	(%rbx), %rsi
	movl	$675, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L206
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L206:
	movq	16(%rbx), %rdi
	movq	-4064(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L325
.L207:
	movq	(%rbx), %rsi
	movl	$674, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L208
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L208:
	movq	16(%rbx), %rdi
	movq	-3896(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L326
.L209:
	movq	(%rbx), %rsi
	movl	$676, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L210
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L210:
	movq	16(%rbx), %rdi
	movq	-4032(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L327
.L211:
	movq	(%rbx), %rsi
	movl	$351, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L212
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L212:
	movq	16(%rbx), %rdi
	movq	-3960(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L328
.L213:
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %rdi
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L214
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L214:
	movq	16(%rbx), %rdi
	movq	-4000(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L329
.L215:
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %rdi
	movl	$-1, %esi
	movq	%rax, -4128(%rbp)
	movq	%rdi, -4112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$6, %r8d
	movl	$64, %edx
	movq	-4128(%rbp), %r9
	movq	-4112(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L216
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L216:
	movq	16(%rbx), %rdi
	movq	-3936(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L330
.L217:
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19GetImportMetaObjectERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L218
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L218:
	movq	16(%rbx), %rdi
	movq	-4072(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator4CallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L219
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L219:
	movq	16(%rbx), %rdi
	movq	-3904(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L331
.L220:
	movq	(%rbx), %rsi
	movl	$161, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L221
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L221:
	movq	16(%rbx), %rdi
	movq	-4040(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L332
.L222:
	movq	(%rbx), %rsi
	movl	$350, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L223
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L223:
	movq	16(%rbx), %rdi
	movq	-3968(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L333
.L224:
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator27CreateAsyncFromSyncIteratorERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L225
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L225:
	movq	16(%rbx), %rdi
	movq	-4008(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L334
.L226:
	movq	(%rbx), %rsi
	movl	$159, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L227
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L227:
	movq	16(%rbx), %rdi
	movq	-3944(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L335
.L228:
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movl	$1061, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator14IsInstanceTypeEPNS0_8compiler4NodeEi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L229
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L229:
	movq	16(%rbx), %rdi
	movq	-4080(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L336
.L230:
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator12IsJSReceiverERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L231
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L231:
	movq	16(%rbx), %rdi
	movq	-3912(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L337
.L232:
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rdi, -4112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-4112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L233
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L233:
	movq	16(%rbx), %rdi
	movq	-4048(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L338
.L234:
	movq	(%rbx), %rsi
	movl	$785, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L235
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L235:
	movq	16(%rbx), %rdi
	movq	-3976(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L339
.L236:
	movq	(%rbx), %rsi
	movl	$108, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L237
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L237:
	movq	16(%rbx), %rdi
	movq	-4016(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L340
.L238:
	movq	(%rbx), %rsi
	movl	$91, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator19IntrinsicAsStubCallERKNS1_20InterpreterAssembler15RegListNodePairEPNS0_8compiler4NodeERKNS0_8CallableE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L239
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-3880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L239:
	movq	-4088(%rbp), %r13
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movl	$40, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3880(%rbp), %r14
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-4016(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3976(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4048(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3944(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3968(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4040(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3936(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4000(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3960(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3928(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3992(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3952(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3984(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L341
	addq	$4248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L340:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L339:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L338:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L337:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L336:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L335:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L334:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L333:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L332:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L331:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L330:
	movq	8(%r12), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L329:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L328:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L327:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L326:
	movq	8(%r12), %rdx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L325:
	movq	8(%r12), %rdx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L324:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L323:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L322:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L321:
	movq	8(%r12), %rdx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L320:
	movq	8(%r12), %rdx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L319:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L318:
	movq	8(%r12), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator23AbortIfArgCountMismatchEiNS0_8compiler5TNodeINS0_7Word32TEEE
	jmp	.L193
.L341:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21892:
	.size	_ZN2v88internal11interpreter19IntrinsicsGenerator15InvokeIntrinsicEPNS0_8compiler4NodeES5_RKNS1_20InterpreterAssembler15RegListNodePairE, .-_ZN2v88internal11interpreter19IntrinsicsGenerator15InvokeIntrinsicEPNS0_8compiler4NodeES5_RKNS1_20InterpreterAssembler15RegListNodePairE
	.section	.text._ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE
	.type	_ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE, @function
_ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE:
.LFB21891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%rax, %xmm0
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -56(%rbp)
	call	_ZN2v88internal11interpreter19IntrinsicsGenerator15InvokeIntrinsicEPNS0_8compiler4NodeES5_RKNS1_20InterpreterAssembler15RegListNodePairE
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L345
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L345:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21891:
	.size	_ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE, .-_ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE:
.LFB28102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28102:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC2:
	.long	4
	.long	5
	.long	6
	.long	7
	.align 16
.LC3:
	.long	8
	.long	9
	.long	10
	.long	11
	.align 16
.LC4:
	.long	12
	.long	13
	.long	14
	.long	15
	.align 16
.LC5:
	.long	16
	.long	17
	.long	18
	.long	19
	.align 16
.LC6:
	.long	20
	.long	21
	.long	22
	.long	23
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
