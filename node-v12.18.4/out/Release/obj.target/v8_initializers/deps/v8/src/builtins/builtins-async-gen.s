	.file	"builtins-async-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_
	.type	_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_, @function
_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_:
.LFB21896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$161, %edx
	pushq	%r13
	movq	%r14, %rsi
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$29, %ecx
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$29, %ecx
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movl	$553, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %rcx
	movl	$7, %r8d
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$8, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11LoadBuiltinENS0_8compiler5TNodeINS0_3SmiEEE@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rcx
	popq	%r12
	movl	$7, %r8d
	popq	%r13
	movl	$48, %edx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	.cfi_endproc
.LFE21896:
	.size	_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_, .-_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_
	.section	.text._ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.type	_ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, @function
_ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_:
.LFB21893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$440, %rsp
	movq	24(%rbp), %r9
	movq	16(%rbp), %r11
	movq	%rcx, -440(%rbp)
	movq	%r8, -448(%rbp)
	movq	%r9, -464(%rbp)
	movq	%r11, -432(%rbp)
	movq	%rdx, -424(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$208, %esi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$40, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	movl	$6, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	movq	-424(%rbp), %r10
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$3, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$212, %edx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$56, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$48, %edx
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEEi@PLT
	movq	-424(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -424(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	-424(%rbp), %rsi
	movq	%r12, %rdi
	movl	$29, %ecx
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	-424(%rbp), %rsi
	movl	$29, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$96, %edx
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEEi@PLT
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	leaq	-352(%rbp), %r15
	call	_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$152, %edx
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-432(%rbp), %r11
	movq	%rax, %rcx
	movq	%rax, -432(%rbp)
	leaq	-416(%rbp), %r14
	leaq	-224(%rbp), %r13
	movq	%r11, %r8
	call	_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rsi
	movl	$7, %edx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-440(%rbp), %xmm0
	movl	$5, %r8d
	movl	$277, %esi
	movq	-464(%rbp), %r9
	movhps	-424(%rbp), %xmm0
	movq	%r9, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-448(%rbp), %xmm0
	movq	%rcx, -448(%rbp)
	movhps	-432(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-384(%rbp), %r10
	movl	$495, %edx
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -472(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-448(%rbp), %rsi
	movq	%rbx, %r9
	movl	$2, %edi
	pushq	%rdi
	leaq	-400(%rbp), %r11
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	movq	-368(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-424(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movq	%r11, -464(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%r11, %rdx
	movhps	-440(%rbp), %xmm0
	movq	%rax, -392(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -440(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-472(%rbp), %r10
	movl	$504, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-448(%rbp), %rsi
	movq	%rbx, %r9
	movq	-424(%rbp), %xmm0
	movl	$4, %edi
	movq	%rax, %r8
	movq	-464(%rbp), %r11
	movq	-368(%rbp), %rax
	movhps	-456(%rbp), %xmm0
	pushq	%rdi
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movq	-432(%rbp), %xmm0
	movq	%rdx, -400(%rbp)
	movq	%r11, %rdx
	movhps	-440(%rbp), %xmm0
	movq	%rax, -392(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21893:
	.size	_ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, .-_ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.section	.text._ZN2v88internal22AsyncBuiltinsAssembler14AwaitOptimizedEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22AsyncBuiltinsAssembler14AwaitOptimizedEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.type	_ZN2v88internal22AsyncBuiltinsAssembler14AwaitOptimizedEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, @function
_ZN2v88internal22AsyncBuiltinsAssembler14AwaitOptimizedEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_:
.LFB21894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %r11
	movq	16(%rbp), %r9
	movq	%r8, -432(%rbp)
	movq	%r11, -480(%rbp)
	movq	%r9, -424(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -464(%rbp)
	movq	%rcx, -448(%rbp)
	movq	%rsi, -472(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$160, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$40, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	movl	$6, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movl	$1, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	-352(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$3, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$48, %edx
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEEi@PLT
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rbx, %rdx
	movq	%rax, -440(%rbp)
	leaq	-416(%rbp), %r14
	call	_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$104, %edx
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEEi@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-424(%rbp), %r9
	movq	%rax, %rcx
	movq	%rax, -424(%rbp)
	leaq	-224(%rbp), %r13
	movq	%r9, %r8
	call	_ZN2v88internal22AsyncBuiltinsAssembler23InitializeNativeClosureEPNS0_8compiler4NodeES4_S4_S4_
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rsi
	movl	$7, %edx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movdqa	-464(%rbp), %xmm0
	leaq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	-472(%rbp), %r10
	movl	$5, %r8d
	movq	-480(%rbp), %r11
	movl	$276, %esi
	movaps	%xmm0, -96(%rbp)
	movq	-432(%rbp), %xmm0
	movq	%r10, %rdx
	movq	%r11, -64(%rbp)
	movhps	-424(%rbp), %xmm0
	movq	%rcx, -464(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$504, %edx
	leaq	-384(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	xorl	%esi, %esi
	movq	%rbx, %r9
	movq	-448(%rbp), %xmm0
	movq	-464(%rbp), %rcx
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-400(%rbp), %rdx
	movq	%r12, %rdi
	movhps	-440(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	-424(%rbp), %xmm0
	movq	%rax, -400(%rbp)
	movq	-368(%rbp), %rax
	movhps	-432(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21894:
	.size	_ZN2v88internal22AsyncBuiltinsAssembler14AwaitOptimizedEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, .-_ZN2v88internal22AsyncBuiltinsAssembler14AwaitOptimizedEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.section	.text._ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.type	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, @function
_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_:
.LFB21895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-656(%rbp), %r15
	leaq	-336(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-592(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$712, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %rbx
	movq	%r9, -696(%rbp)
	movq	%rsi, -672(%rbp)
	movq	%r12, %rsi
	movq	%rax, -704(%rbp)
	movq	%rdx, -680(%rbp)
	movl	$8, %edx
	movq	%rcx, -664(%rbp)
	movq	%r8, -688(%rbp)
	movq	%rbx, -712(%rbp)
	leaq	-464(%rbp), %rbx
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-208(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r11, -744(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal17CodeStubAssembler14IsJSPromiseMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$127, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-728(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -720(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-720(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-744(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler36IsPromiseSpeciesProtectorCellInvalidEv@PLT
	movq	-720(%rbp), %r11
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-720(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -728(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	2304(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -720(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	leaq	-624(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movl	$2, %edi
	movq	-664(%rbp), %xmm0
	pushq	%rdi
	movq	%rax, %r8
	movq	-672(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	pushq	%rsi
	movq	-608(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movhps	-720(%rbp), %xmm0
	movq	%rcx, -640(%rbp)
	leaq	-640(%rbp), %rdx
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-736(%rbp), %r10
	movq	%r12, %rdi
	movl	$212, %edx
	movq	%rax, -720(%rbp)
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-720(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	-712(%rbp)
	movq	%r12, %rdi
	movq	-696(%rbp), %r9
	movq	-688(%rbp), %r8
	pushq	-704(%rbp)
	movq	-664(%rbp), %rcx
	movq	-680(%rbp), %rdx
	movq	-672(%rbp), %rsi
	call	_ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	-712(%rbp)
	movq	%r12, %rdi
	movq	-696(%rbp), %r9
	movq	-688(%rbp), %r8
	pushq	-704(%rbp)
	movq	-664(%rbp), %rcx
	movq	-680(%rbp), %rdx
	movq	-672(%rbp), %rsi
	call	_ZN2v88internal22AsyncBuiltinsAssembler14AwaitOptimizedEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-728(%rbp), %r11
	movq	%rax, %r12
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21895:
	.size	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, .-_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.section	.text._ZN2v88internal22AsyncBuiltinsAssembler19CreateUnwrapClosureEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22AsyncBuiltinsAssembler19CreateUnwrapClosureEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal22AsyncBuiltinsAssembler19CreateUnwrapClosureEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal22AsyncBuiltinsAssembler19CreateUnwrapClosureEPNS0_8compiler4NodeES4_:
.LFB21897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$161, %edx
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$19, %edx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$5, %edx
	movq	%rax, %r15
	call	_ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movl	$4, %edx
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	.cfi_endproc
.LFE21897:
	.size	_ZN2v88internal22AsyncBuiltinsAssembler19CreateUnwrapClosureEPNS0_8compiler4NodeES4_, .-_ZN2v88internal22AsyncBuiltinsAssembler19CreateUnwrapClosureEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal22AsyncBuiltinsAssembler39AllocateAsyncIteratorValueUnwrapContextEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22AsyncBuiltinsAssembler39AllocateAsyncIteratorValueUnwrapContextEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal22AsyncBuiltinsAssembler39AllocateAsyncIteratorValueUnwrapContextEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal22AsyncBuiltinsAssembler39AllocateAsyncIteratorValueUnwrapContextEPNS0_8compiler4NodeES4_:
.LFB21898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	movl	$5, %edx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	_ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi@PLT
	movq	%r14, %rcx
	movq	%r13, %rdi
	movl	$4, %edx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21898:
	.size	_ZN2v88internal22AsyncBuiltinsAssembler39AllocateAsyncIteratorValueUnwrapContextEPNS0_8compiler4NodeES4_, .-_ZN2v88internal22AsyncBuiltinsAssembler39AllocateAsyncIteratorValueUnwrapContextEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal33AsyncIteratorValueUnwrapAssembler36GenerateAsyncIteratorValueUnwrapImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33AsyncIteratorValueUnwrapAssembler36GenerateAsyncIteratorValueUnwrapImplEv
	.type	_ZN2v88internal33AsyncIteratorValueUnwrapAssembler36GenerateAsyncIteratorValueUnwrapImplEv, @function
_ZN2v88internal33AsyncIteratorValueUnwrapAssembler36GenerateAsyncIteratorValueUnwrapImplEv:
.LFB21910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$350, %edx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%rbx, %r9
	movl	$2, %edi
	movq	%rax, %r8
	movq	-104(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-112(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21910:
	.size	_ZN2v88internal33AsyncIteratorValueUnwrapAssembler36GenerateAsyncIteratorValueUnwrapImplEv, .-_ZN2v88internal33AsyncIteratorValueUnwrapAssembler36GenerateAsyncIteratorValueUnwrapImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-async-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"AsyncIteratorValueUnwrap"
	.section	.text._ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE:
.LFB21906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$303, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$694, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L33
.L30:
	movq	%r13, %rdi
	call	_ZN2v88internal33AsyncIteratorValueUnwrapAssembler36GenerateAsyncIteratorValueUnwrapImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L30
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21906:
	.size	_ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_AsyncIteratorValueUnwrapEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, @function
_GLOBAL__sub_I__ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_:
.LFB28058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28058:
	.size	_GLOBAL__sub_I__ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_, .-_GLOBAL__sub_I__ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22AsyncBuiltinsAssembler8AwaitOldEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
