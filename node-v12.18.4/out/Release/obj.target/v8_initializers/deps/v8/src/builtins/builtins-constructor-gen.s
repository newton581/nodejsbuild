	.file	"builtins-constructor-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextES4_S4_S4_NS1_9ScopeTypeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextES4_S4_S4_NS1_9ScopeTypeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextES4_S4_S4_NS1_9ScopeTypeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB29403:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %r8
	movq	(%r8), %rdx
	movl	$7, %r8d
	movq	8(%rax), %rdi
	movq	(%rax), %rsi
	movq	16(%rax), %rcx
	jmp	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_7IntPtrTEEES4_NS0_21MachineRepresentationE@PLT
	.cfi_endproc
.LFE29403:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextES4_S4_S4_NS1_9ScopeTypeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextES4_S4_S4_NS1_9ScopeTypeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS2_8compiler4NodeES6_S6_NS2_9ScopeTypeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS2_8compiler4NodeES6_S6_NS2_9ScopeTypeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS2_8compiler4NodeES6_S6_NS2_9ScopeTypeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation:
.LFB29405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L9
	cmpl	$3, %edx
	je	.L10
	cmpl	$1, %edx
	je	.L16
.L11:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29405:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS2_8compiler4NodeES6_S6_NS2_9ScopeTypeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS2_8compiler4NodeES6_S6_NS2_9ScopeTypeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation
	.section	.text._ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE:
.LFB24896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$23, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE@PLT
	.cfi_endproc
.LFE24896:
	.size	_ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins32Generate_ConstructForwardVarargsEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_ConstructForwardVarargsEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins32Generate_ConstructForwardVarargsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins32Generate_ConstructForwardVarargsEPNS0_14MacroAssemblerE:
.LFB24897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$23, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE@PLT
	.cfi_endproc
.LFE24897:
	.size	_ZN2v88internal8Builtins32Generate_ConstructForwardVarargsEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins32Generate_ConstructForwardVarargsEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins40Generate_ConstructFunctionForwardVarargsEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_ConstructFunctionForwardVarargsEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins40Generate_ConstructFunctionForwardVarargsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins40Generate_ConstructFunctionForwardVarargsEPNS0_14MacroAssemblerE:
.LFB24898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$20, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE@PLT
	.cfi_endproc
.LFE24898:
	.size	_ZN2v88internal8Builtins40Generate_ConstructFunctionForwardVarargsEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins40Generate_ConstructFunctionForwardVarargsEPNS0_14MacroAssemblerE
	.section	.rodata._ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-constructor-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"ConstructWithArrayLike"
	.section	.text._ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE:
.LFB24906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$39, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$26, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L27
.L24:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L24
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24906:
	.size	_ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_ConstructWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31ConstructWithArrayLikeAssembler34GenerateConstructWithArrayLikeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31ConstructWithArrayLikeAssembler34GenerateConstructWithArrayLikeImplEv
	.type	_ZN2v88internal31ConstructWithArrayLikeAssembler34GenerateConstructWithArrayLikeImplEv, @function
_ZN2v88internal31ConstructWithArrayLikeAssembler34GenerateConstructWithArrayLikeImplEv:
.LFB24910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE@PLT
	.cfi_endproc
.LFE24910:
	.size	_ZN2v88internal31ConstructWithArrayLikeAssembler34GenerateConstructWithArrayLikeImplEv, .-_ZN2v88internal31ConstructWithArrayLikeAssembler34GenerateConstructWithArrayLikeImplEv
	.section	.text._ZN2v88internal28ConstructWithSpreadAssembler31GenerateConstructWithSpreadImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructWithSpreadAssembler31GenerateConstructWithSpreadImplEv
	.type	_ZN2v88internal28ConstructWithSpreadAssembler31GenerateConstructWithSpreadImplEv, @function
_ZN2v88internal28ConstructWithSpreadAssembler31GenerateConstructWithSpreadImplEv:
.LFB24919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r15, %rcx
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r9
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE@PLT
	.cfi_endproc
.LFE24919:
	.size	_ZN2v88internal28ConstructWithSpreadAssembler31GenerateConstructWithSpreadImplEv, .-_ZN2v88internal28ConstructWithSpreadAssembler31GenerateConstructWithSpreadImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_ConstructWithSpreadEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ConstructWithSpread"
	.section	.text._ZN2v88internal8Builtins28Generate_ConstructWithSpreadEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_ConstructWithSpreadEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_ConstructWithSpreadEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_ConstructWithSpreadEPNS0_8compiler18CodeAssemblerStateE:
.LFB24915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$47, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$25, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L37
.L34:
	movq	%r13, %rdi
	call	_ZN2v88internal28ConstructWithSpreadAssembler31GenerateConstructWithSpreadImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L34
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24915:
	.size	_ZN2v88internal8Builtins28Generate_ConstructWithSpreadEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_ConstructWithSpreadEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23FastNewClosureAssembler26GenerateFastNewClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23FastNewClosureAssembler26GenerateFastNewClosureImplEv
	.type	_ZN2v88internal23FastNewClosureAssembler26GenerateFastNewClosureImplEv, @function
_ZN2v88internal23FastNewClosureAssembler26GenerateFastNewClosureImplEv:
.LFB24931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-448(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	40960(%rax), %rsi
	addq	$7848, %rsi
	call	_ZN2v88internal17CodeStubAssembler16IncrementCounterEPNS0_12StatsCounterEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-456(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler19IsNoClosuresCellMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-456(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler19IsOneClosureCellMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$61, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$56, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-472(%rbp), %r11
	movl	$772, %ecx
	movq	%r12, %rdi
	movl	$48, %edx
	movq	%r11, %rsi
	movq	%r11, -480(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$154, %esi
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-456(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$1015808, %ecx
	movl	$15, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-464(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%r10, %rsi
	movq	%r10, -472(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal17CodeStubAssembler26LoadMapInstanceSizeInWordsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15TimesTaggedSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateENS0_8compiler5TNodeINS0_7IntPtrTEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	-456(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	-456(%rbp), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-464(%rbp), %rcx
	movl	$56, %r8d
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler37InitializeJSObjectBodyNoSlackTrackingEPNS0_8compiler4NodeES4_S4_i@PLT
	movl	$29, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movl	$29, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-456(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler30IsFunctionWithPrototypeSlotMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$56, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rcx
	movl	$40, %edx
	movq	%r13, %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$24, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-480(%rbp), %r11
	movl	$8, %r8d
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-472(%rbp), %r10
	movl	$8, %r8d
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$67, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$48, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24931:
	.size	_ZN2v88internal23FastNewClosureAssembler26GenerateFastNewClosureImplEv, .-_ZN2v88internal23FastNewClosureAssembler26GenerateFastNewClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_FastNewClosureEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"FastNewClosure"
	.section	.text._ZN2v88internal8Builtins23Generate_FastNewClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_FastNewClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_FastNewClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_FastNewClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB24927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$59, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$32, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L47
.L44:
	movq	%r13, %rdi
	call	_ZN2v88internal23FastNewClosureAssembler26GenerateFastNewClosureImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L44
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24927:
	.size	_ZN2v88internal8Builtins23Generate_FastNewClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_FastNewClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE:
.LFB24945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	movl	$1, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-576(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$568, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1105, %edx
	call	_ZN2v88internal17CodeStubAssembler15HasInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$56, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$68, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler22DoesntHaveInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r15
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-592(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movl	$8, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-192(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r11, %rdi
	movq	%r12, %rsi
	movq	%r11, -600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler15IsDictionaryMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-600(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal17CodeStubAssembler22AllocateNameDictionaryEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	-600(%rbp), %r11
	movq	%rax, %r12
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-608(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$568, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L52:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24945:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal22FastNewObjectAssembler25GenerateFastNewObjectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22FastNewObjectAssembler25GenerateFastNewObjectImplEv
	.type	_ZN2v88internal22FastNewObjectAssembler25GenerateFastNewObjectImplEv, @function
_ZN2v88internal22FastNewObjectAssembler25GenerateFastNewObjectImplEv:
.LFB24940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %r15
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %xmm1
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	movq	%rax, %rdx
	leaq	-80(%rbp), %r8
	movl	$2, %r9d
	punpcklqdq	%xmm1, %xmm0
	movl	$237, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24940:
	.size	_ZN2v88internal22FastNewObjectAssembler25GenerateFastNewObjectImplEv, .-_ZN2v88internal22FastNewObjectAssembler25GenerateFastNewObjectImplEv
	.section	.rodata._ZN2v88internal8Builtins22Generate_FastNewObjectEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"FastNewObject"
	.section	.text._ZN2v88internal8Builtins22Generate_FastNewObjectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_FastNewObjectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_FastNewObjectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_FastNewObjectEPNS0_8compiler18CodeAssemblerStateE:
.LFB24936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$149, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$31, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L61
.L58:
	movq	%r13, %rdi
	call	_ZN2v88internal22FastNewObjectAssembler25GenerateFastNewObjectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L58
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24936:
	.size	_ZN2v88internal8Builtins22Generate_FastNewObjectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_FastNewObjectEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEE
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEE, @function
_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEE:
.LFB24941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r15
	leaq	-208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$328, %rsp
	movq	%rdx, -360(%rbp)
	movl	$7, %edx
	movq	%rcx, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	-368(%rbp), %rcx
	movq	-360(%rbp), %rdx
	call	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEEPNS2_18CodeAssemblerLabelE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-360(%rbp), %xmm0
	movl	$2, %r8d
	movl	$237, %esi
	movhps	-368(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$328, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24941:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEE, .-_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE, @function
_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE:
.LFB24946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%rdx, %rsi
	movl	%r8d, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$48, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceENS0_8compiler5TNodeINS0_7IntPtrTEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	-144(%rbp), %r9d
	movq	%rax, %r13
	cmpb	$1, %r9b
	je	.L71
	cmpb	$2, %r9b
	jne	.L77
	movl	$21, %edx
.L68:
	movq	%r12, %xmm1
	movq	%r13, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-152(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rcx
	movl	$24, %edx
	movq	%r13, %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$40, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movl	$24, %edi
	movq	$0, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	leaq	-128(%rbp), %rsi
	movdqa	-144(%rbp), %xmm0
	movq	%rbx, 16(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS2_8compiler4NodeES6_S6_NS2_9ScopeTypeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation(%rip), %rbx
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movl	$8, %r9d
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextES4_S4_S4_NS1_9ScopeTypeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	pushq	$1
	movq	%rax, %xmm2
	movq	%rbx, %r8
	pushq	$1
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13BuildFastLoopERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEEPNS3_4NodeESA_RKSt8functionIFvSA_EEiNS1_13ParameterModeENS1_16IndexAdvanceModeE@PLT
	movq	-80(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L67
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L67:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	$38, %edx
	jmp	.L68
.L77:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24946:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE, .-_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE
	.section	.rodata._ZN2v88internal8Builtins35Generate_FastNewFunctionContextEvalEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"FastNewFunctionContextEval"
	.section	.text._ZN2v88internal8Builtins35Generate_FastNewFunctionContextEvalEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_FastNewFunctionContextEvalEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_FastNewFunctionContextEvalEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_FastNewFunctionContextEvalEPNS0_8compiler18CodeAssemblerStateE:
.LFB24955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$276, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$33, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L83
.L80:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L80
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24955:
	.size	_ZN2v88internal8Builtins35Generate_FastNewFunctionContextEvalEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_FastNewFunctionContextEvalEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35FastNewFunctionContextEvalAssembler38GenerateFastNewFunctionContextEvalImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35FastNewFunctionContextEvalAssembler38GenerateFastNewFunctionContextEvalImplEv
	.type	_ZN2v88internal35FastNewFunctionContextEvalAssembler38GenerateFastNewFunctionContextEvalImplEv, @function
_ZN2v88internal35FastNewFunctionContextEvalAssembler38GenerateFastNewFunctionContextEvalImplEv:
.LFB24959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE24959:
	.size	_ZN2v88internal35FastNewFunctionContextEvalAssembler38GenerateFastNewFunctionContextEvalImplEv, .-_ZN2v88internal35FastNewFunctionContextEvalAssembler38GenerateFastNewFunctionContextEvalImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_FastNewFunctionContextFunctionEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"FastNewFunctionContextFunction"
	.section	.text._ZN2v88internal8Builtins39Generate_FastNewFunctionContextFunctionEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_FastNewFunctionContextFunctionEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_FastNewFunctionContextFunctionEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_FastNewFunctionContextFunctionEPNS0_8compiler18CodeAssemblerStateE:
.LFB24964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$284, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$34, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L91
.L88:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L88
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24964:
	.size	_ZN2v88internal8Builtins39Generate_FastNewFunctionContextFunctionEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_FastNewFunctionContextFunctionEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal39FastNewFunctionContextFunctionAssembler42GenerateFastNewFunctionContextFunctionImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39FastNewFunctionContextFunctionAssembler42GenerateFastNewFunctionContextFunctionImplEv
	.type	_ZN2v88internal39FastNewFunctionContextFunctionAssembler42GenerateFastNewFunctionContextFunctionImplEv, @function
_ZN2v88internal39FastNewFunctionContextFunctionAssembler42GenerateFastNewFunctionContextFunctionImplEv:
.LFB24968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE24968:
	.size	_ZN2v88internal39FastNewFunctionContextFunctionAssembler42GenerateFastNewFunctionContextFunctionImplEv, .-_ZN2v88internal39FastNewFunctionContextFunctionAssembler42GenerateFastNewFunctionContextFunctionImplEv
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_, @function
_ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_:
.LFB24969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	leaq	-368(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$392, %rsp
	movq	%r9, -408(%rbp)
	movq	%rsi, -376(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -392(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -400(%rbp)
	xorl	%r8d, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-224(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NotHasBoilerplateENS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L96:
	movl	%r14d, %edx
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	addl	$8, %r14d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	cmpl	$56, %r14d
	jne	.L96
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-416(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-424(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-376(%rbp), %rdx
	leaq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	movq	-392(%rbp), %rax
	movl	$4, %r8d
	movl	$190, %esi
	movq	%rdx, -96(%rbp)
	movq	-408(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$392, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24969:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_, .-_ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_
	.section	.text._ZN2v88internal28CreateRegExpLiteralAssembler31GenerateCreateRegExpLiteralImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28CreateRegExpLiteralAssembler31GenerateCreateRegExpLiteralImplEv
	.type	_ZN2v88internal28CreateRegExpLiteralAssembler31GenerateCreateRegExpLiteralImplEv, @function
_ZN2v88internal28CreateRegExpLiteralAssembler31GenerateCreateRegExpLiteralImplEv:
.LFB24978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rax, %r9
	call	_ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE24978:
	.size	_ZN2v88internal28CreateRegExpLiteralAssembler31GenerateCreateRegExpLiteralImplEv, .-_ZN2v88internal28CreateRegExpLiteralAssembler31GenerateCreateRegExpLiteralImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_CreateRegExpLiteralEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"CreateRegExpLiteral"
	.section	.text._ZN2v88internal8Builtins28Generate_CreateRegExpLiteralEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_CreateRegExpLiteralEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_CreateRegExpLiteralEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_CreateRegExpLiteralEPNS0_8compiler18CodeAssemblerStateE:
.LFB24974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$327, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$35, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L107
.L104:
	movq	%r13, %rdi
	call	_ZN2v88internal28CreateRegExpLiteralAssembler31GenerateCreateRegExpLiteralImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L104
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24974:
	.size	_ZN2v88internal8Builtins28Generate_CreateRegExpLiteralEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_CreateRegExpLiteralEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE, @function
_ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE:
.LFB24979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	leaq	-320(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-576(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$600, %rsp
	movl	%r9d, -604(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movq	%r8, -600(%rbp)
	movl	$1, %r8d
	movq	%rcx, -632(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-192(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-592(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	-640(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NotHasBoilerplateENS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	-600(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadBoilerplateENS0_8compiler5TNodeINS0_14AllocationSiteEEE@PLT
	xorl	%r9d, %r9d
	cmpl	$1, -604(%rbp)
	movq	%rbx, %r8
	movq	-632(%rbp), %r11
	movq	%rax, %rdx
	je	.L114
	xorl	%r8d, %r8d
.L114:
	movl	$1, %ecx
	movq	%r11, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16CloneFastJSArrayEPNS0_8compiler4NodeES4_NS1_13ParameterModeES4_NS1_18HoleConversionModeE@PLT
	movq	-624(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-616(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$600, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24979:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE, .-_ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE
	.section	.text._ZN2v88internal34CreateShallowArrayLiteralAssembler37GenerateCreateShallowArrayLiteralImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34CreateShallowArrayLiteralAssembler37GenerateCreateShallowArrayLiteralImplEv
	.type	_ZN2v88internal34CreateShallowArrayLiteralAssembler37GenerateCreateShallowArrayLiteralImplEv, @function
_ZN2v88internal34CreateShallowArrayLiteralAssembler37GenerateCreateShallowArrayLiteralImplEv:
.LFB24988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	leaq	-96(%rbp), %rcx
	jne	.L122
.L117:
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-240(%rbp), %rdx
	movl	$4, %r8d
	movq	-248(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	-232(%rbp), %rax
	movl	$186, %esi
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r14, %rdx
	movq	%rbx, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	leaq	-80(%rbp), %rax
	movq	%rcx, %rsi
	movq	%r12, %rdi
	movb	$0, -68(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -248(%rbp)
	movabsq	$7959393400030781795, %rax
	movq	%rax, -80(%rbp)
	movq	%rcx, -240(%rbp)
	movl	$1701669236, -72(%rbp)
	movq	$12, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-248(%rbp), %rax
	movq	-240(%rbp), %rcx
	cmpq	%rax, %rdi
	je	.L117
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rcx
	jmp	.L117
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24988:
	.size	_ZN2v88internal34CreateShallowArrayLiteralAssembler37GenerateCreateShallowArrayLiteralImplEv, .-_ZN2v88internal34CreateShallowArrayLiteralAssembler37GenerateCreateShallowArrayLiteralImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_CreateShallowArrayLiteralEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"CreateShallowArrayLiteral"
	.section	.text._ZN2v88internal8Builtins34Generate_CreateShallowArrayLiteralEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_CreateShallowArrayLiteralEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_CreateShallowArrayLiteralEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_CreateShallowArrayLiteralEPNS0_8compiler18CodeAssemblerStateE:
.LFB24984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$360, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$37, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L128
.L125:
	movq	%r13, %rdi
	call	_ZN2v88internal34CreateShallowArrayLiteralAssembler37GenerateCreateShallowArrayLiteralImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L125
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24984:
	.size	_ZN2v88internal8Builtins34Generate_CreateShallowArrayLiteralEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_CreateShallowArrayLiteralEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_, @function
_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_:
.LFB24989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r14
	leaq	-352(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-496(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$504, %rsp
	movq	%rcx, -544(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -536(%rbp)
	movq	%rsi, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-520(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-536(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-528(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler36CreateAllocationSiteInFeedbackVectorENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS2_5TNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler5TNodeINS0_14AllocationSiteEEE@PLT
	movq	-544(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, -520(%rbp)
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %r8
	jne	.L137
.L131:
	movq	-520(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_8compiler11SloppyTNodeINS0_6Int32TEEENS3_INS0_13NativeContextEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %rcx
	jne	.L138
.L133:
	movq	%r13, %rdi
	movq	%rcx, -528(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	pushq	$0
	movq	-528(%rbp), %rcx
	xorl	%esi, %esi
	pushq	$0
	movq	-520(%rbp), %rdx
	movq	%rax, %r9
	movq	%r12, %rdi
	movq	%rcx, %r8
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-520(%rbp), %rax
	jne	.L139
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	leaq	-96(%rbp), %r9
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, -544(%rbp)
	movq	%r9, %rdi
	leaq	-504(%rbp), %rsi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%r9, -528(%rbp)
	movq	$22, -504(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-504(%rbp), %rdx
	movdqa	.LC10(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-528(%rbp), %r9
	movq	%rdx, -80(%rbp)
	movl	$28769, %edx
	movl	$1299412078, 16(%rax)
	movq	%r9, %rsi
	movw	%dx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-504(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-536(%rbp), %rcx
	movq	-544(%rbp), %r8
	cmpq	%rcx, %rdi
	je	.L131
	movq	%r8, -528(%rbp)
	call	_ZdlPv@PLT
	movq	-528(%rbp), %r8
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L138:
	leaq	-96(%rbp), %r9
	leaq	-80(%rbp), %r8
	xorl	%edx, %edx
	movq	%rax, -544(%rbp)
	movq	%r9, %rdi
	leaq	-504(%rbp), %rsi
	movq	%r8, -96(%rbp)
	movq	%r8, -536(%rbp)
	movq	%r9, -528(%rbp)
	movq	$16, -504(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-504(%rbp), %rdx
	movdqa	.LC11(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-528(%rbp), %r9
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movq	-504(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%r9, %rsi
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-536(%rbp), %r8
	movq	-544(%rbp), %rcx
	cmpq	%r8, %rdi
	je	.L133
	movq	%rcx, -528(%rbp)
	call	_ZdlPv@PLT
	movq	-528(%rbp), %rcx
	jmp	.L133
.L139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24989:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_, .-_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_
	.section	.rodata._ZN2v88internal8Builtins32Generate_CreateEmptyArrayLiteralEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"CreateEmptyArrayLiteral"
	.section	.text._ZN2v88internal8Builtins32Generate_CreateEmptyArrayLiteralEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_CreateEmptyArrayLiteralEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_CreateEmptyArrayLiteralEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_CreateEmptyArrayLiteralEPNS0_8compiler18CodeAssemblerStateE:
.LFB24997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$420, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$36, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L144
.L141:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L141
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24997:
	.size	_ZN2v88internal8Builtins32Generate_CreateEmptyArrayLiteralEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_CreateEmptyArrayLiteralEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32CreateEmptyArrayLiteralAssembler35GenerateCreateEmptyArrayLiteralImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32CreateEmptyArrayLiteralAssembler35GenerateCreateEmptyArrayLiteralImplEv
	.type	_ZN2v88internal32CreateEmptyArrayLiteralAssembler35GenerateCreateEmptyArrayLiteralImplEv, @function
_ZN2v88internal32CreateEmptyArrayLiteralAssembler35GenerateCreateEmptyArrayLiteralImplEv:
.LFB25001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE25001:
	.size	_ZN2v88internal32CreateEmptyArrayLiteralAssembler35GenerateCreateEmptyArrayLiteralImplEv, .-_ZN2v88internal32CreateEmptyArrayLiteralAssembler35GenerateCreateEmptyArrayLiteralImplEv
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE:
.LFB25002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	subq	$584, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NotHasBoilerplateENS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadBoilerplateENS0_8compiler5TNodeINS0_14AllocationSiteEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	leaq	-528(%rbp), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rax, %r14
	movq	%rcx, -568(%rbp)
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-352(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$16777216, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-552(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-224(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$2097152, %esi
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-576(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-552(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-496(%rbp), %rsi
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rsi, -552(%rbp)
	jne	.L168
.L149:
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadSlowPropertiesENS0_8compiler11SloppyTNodeINS0_8JSObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18CopyNameDictionaryENS0_8compiler5TNodeINS0_14NameDictionaryEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	-568(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadFastPropertiesENS0_8compiler11SloppyTNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17IsEmptyFixedArrayENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	-568(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	leaq	-512(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-560(%rbp), %rsi
	leaq	8(%r12), %rdi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17IsEmptyFixedArrayENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-576(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movq	%rax, %rdx
	movl	$15, %r9d
	movq	%rbx, %rsi
	pushq	$0
	movq	%r12, %rdi
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler17ExtractFixedArrayEPNS0_8compiler4NodeES4_S4_S4_NS_4base5FlagsINS1_21ExtractFixedArrayFlagEiEENS1_13ParameterModeEPNS2_26TypedCodeAssemblerVariableINS0_5BoolTEEES4_@PLT
	movq	-576(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler26LoadMapInstanceSizeInWordsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15TimesTaggedSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%rax, -584(%rbp)
	movzbl	_ZN2v88internal32FLAG_allocation_site_pretenuringE(%rip), %eax
	movb	%al, -600(%rbp)
	testb	%al, %al
	jne	.L169
	movq	-584(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceENS0_8compiler5TNodeINS0_7IntPtrTEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %rbx
	jne	.L157
	movq	-592(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	-568(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$8, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-576(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
.L154:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-552(%rbp), %rdi
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	je	.L155
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -600(%rbp)
	movq	%r8, -592(%rbp)
	movq	$30, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC15(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7307499905062301554, %rcx
	movq	%rax, -96(%rbp)
	movq	-592(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movl	$29811, %edx
	movq	%rcx, 16(%rax)
	movq	%r8, %rsi
	movl	$1634082931, 24(%rax)
	movw	%dx, 28(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-600(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	-552(%rbp), %rax
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	leaq	-536(%rbp), %rcx
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-552(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-584(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE(%rip), %ecx
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS3_INS0_7IntPtrTEEENS0_11MachineTypeE@PLT
	movq	-552(%rbp), %rdi
	movq	%rax, -560(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-560(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$5, %r8d
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_7IntPtrTEEES4_NS0_21MachineRepresentationE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-552(%rbp), %rdi
	movq	%rax, -560(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-560(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-552(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-552(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-584(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-576(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-568(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-584(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceENS0_8compiler5TNodeINS0_7IntPtrTEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %rbx
	je	.L152
.L157:
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -624(%rbp)
	movq	%r8, -616(%rbp)
	movq	$23, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC14(%rip), %xmm0
	movl	$28783, %ecx
	movq	%rax, -96(%rbp)
	movq	-616(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movw	%cx, 20(%rax)
	movq	%r8, %rsi
	movl	$1126198369, 16(%rax)
	movb	$121, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-624(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	movq	-592(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	-568(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$8, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-576(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	cmpb	$0, -600(%rbp)
	je	.L154
.L159:
	movq	-608(%rbp), %rcx
	movq	-584(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27InitializeAllocationMementoEPNS0_8compiler4NodeES4_S4_@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	$26, -496(%rbp)
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%r8, -576(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-496(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC13(%rip), %xmm0
	movl	$29541, %esi
	movq	%rax, -96(%rbp)
	movabsq	$7598824251284484720, %rcx
	movq	-576(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movw	%si, 24(%rax)
	movq	%r8, %rsi
	movups	%xmm0, (%rax)
	movq	-496(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-584(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L149
	call	_ZdlPv@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-592(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	-568(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-576(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	jmp	.L159
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25002:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE, .-_ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal35CreateShallowObjectLiteralAssembler38GenerateCreateShallowObjectLiteralImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35CreateShallowObjectLiteralAssembler38GenerateCreateShallowObjectLiteralImplEv
	.type	_ZN2v88internal35CreateShallowObjectLiteralAssembler38GenerateCreateShallowObjectLiteralImplEv, @function
_ZN2v88internal35CreateShallowObjectLiteralAssembler38GenerateCreateShallowObjectLiteralImplEv:
.LFB25019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm0
	movq	%r15, %rcx
	movq	%r12, %rdi
	movhps	-248(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	-96(%rbp), %r8
	movl	$4, %r9d
	movaps	%xmm0, -96(%rbp)
	movl	$188, %esi
	movq	-240(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L174:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25019:
	.size	_ZN2v88internal35CreateShallowObjectLiteralAssembler38GenerateCreateShallowObjectLiteralImplEv, .-_ZN2v88internal35CreateShallowObjectLiteralAssembler38GenerateCreateShallowObjectLiteralImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_CreateShallowObjectLiteralEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"CreateShallowObjectLiteral"
	.section	.text._ZN2v88internal8Builtins35Generate_CreateShallowObjectLiteralEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_CreateShallowObjectLiteralEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_CreateShallowObjectLiteralEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_CreateShallowObjectLiteralEPNS0_8compiler18CodeAssemblerStateE:
.LFB25015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$604, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$38, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L179
.L176:
	movq	%r13, %rdi
	call	_ZN2v88internal35CreateShallowObjectLiteralAssembler38GenerateCreateShallowObjectLiteralImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L176
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25015:
	.size	_ZN2v88internal8Builtins35Generate_CreateShallowObjectLiteralEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_CreateShallowObjectLiteralEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE
	.type	_ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE, @function
_ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE:
.LFB25020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$108, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movl	$1, %r9d
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25020:
	.size	_ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE, .-_ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal26ObjectConstructorAssembler29GenerateObjectConstructorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ObjectConstructorAssembler29GenerateObjectConstructorImplEv
	.type	_ZN2v88internal26ObjectConstructorAssembler29GenerateObjectConstructorImplEv, @function
_ZN2v88internal26ObjectConstructorAssembler29GenerateObjectConstructorImplEv:
.LFB25029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-592(%rbp), %r15
	leaq	-464(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-832(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$856, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	leaq	-768(%rbp), %r10
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rax, -880(%rbp)
	movq	%r10, %rdi
	movq	%r10, -872(%rbp)
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-720(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-848(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$31, %edx
	movq	%rax, %rsi
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm1
	leaq	-80(%rbp), %rbx
	movl	$2, %edi
	movq	%rbx, %rsi
	pushq	%rdi
	movq	-864(%rbp), %r9
	movq	%rax, %r8
	pushq	%rsi
	movq	-192(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-856(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rbx, -856(%rbp)
	leaq	-336(%rbp), %rbx
	movq	%rbx, %rdx
	movq	%rcx, -336(%rbp)
	movl	$1, %ecx
	punpcklqdq	%xmm1, %xmm0
	movq	%r9, -888(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-840(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-880(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -864(%rbp)
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler25UintPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-872(%rbp), %r10
	movl	$1, %edx
	movq	-864(%rbp), %r8
	movq	%r10, %rdi
	movq	%r8, %rsi
	movq	%r10, -880(%rbp)
	call	_ZNK2v88internal17CodeStubArguments7AtIndexEPNS0_8compiler4NodeENS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-864(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -872(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-840(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-888(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -864(%rbp)
	call	_ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$91, %edx
	leaq	-800(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-856(%rbp), %rsi
	movl	$1, %edi
	movq	-872(%rbp), %r11
	pushq	%rdi
	movq	%rax, %r8
	movq	-864(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	pushq	%rsi
	movq	-784(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-816(%rbp), %rdx
	movq	%rcx, -816(%rbp)
	movl	$1, %ecx
	movq	%r11, -80(%rbp)
	movq	%rax, -808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-880(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25029:
	.size	_ZN2v88internal26ObjectConstructorAssembler29GenerateObjectConstructorImplEv, .-_ZN2v88internal26ObjectConstructorAssembler29GenerateObjectConstructorImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_ObjectConstructorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"ObjectConstructor"
	.section	.text._ZN2v88internal8Builtins26Generate_ObjectConstructorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_ObjectConstructorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_ObjectConstructorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_ObjectConstructorEPNS0_8compiler18CodeAssemblerStateE:
.LFB25025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$640, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$458, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L191
.L188:
	movq	%r13, %rdi
	call	_ZN2v88internal26ObjectConstructorAssembler29GenerateObjectConstructorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L188
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25025:
	.size	_ZN2v88internal8Builtins26Generate_ObjectConstructorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_ObjectConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26NumberConstructorAssembler29GenerateNumberConstructorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NumberConstructorAssembler29GenerateNumberConstructorImplEv
	.type	_ZN2v88internal26NumberConstructorAssembler29GenerateNumberConstructorImplEv, @function
_ZN2v88internal26NumberConstructorAssembler29GenerateNumberConstructorImplEv:
.LFB25038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	leaq	-464(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-576(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	leaq	-512(%rbp), %r10
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	%r10, %rdi
	movq	%r10, -584(%rbp)
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r13, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %rbx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-584(%rbp), %r10
	xorl	%esi, %esi
	movq	%r10, %rdi
	movq	%r10, -600(%rbp)
	call	_ZNK2v88internal17CodeStubArguments7AtIndexEi@PLT
	movq	-592(%rbp), %r11
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	movq	%r11, -616(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8ToNumberENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_14BigIntHandlingE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %r10
	movq	-592(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler19LoadTargetFromFrameEv@PLT
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$31, %edx
	leaq	-544(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movl	$2, %edi
	movq	-616(%rbp), %r11
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	movq	%r11, %r9
	xorl	%esi, %esi
	leaq	-560(%rbp), %rdx
	movq	-600(%rbp), %xmm0
	movq	%rax, -560(%rbp)
	movq	%r12, %rdi
	movq	-528(%rbp), %rax
	movhps	-584(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-592(%rbp), %rcx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-608(%rbp), %r10
	movq	-584(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L196:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25038:
	.size	_ZN2v88internal26NumberConstructorAssembler29GenerateNumberConstructorImplEv, .-_ZN2v88internal26NumberConstructorAssembler29GenerateNumberConstructorImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_NumberConstructorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"NumberConstructor"
	.section	.text._ZN2v88internal8Builtins26Generate_NumberConstructorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_NumberConstructorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_NumberConstructorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_NumberConstructorEPNS0_8compiler18CodeAssemblerStateE:
.LFB25034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$693, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$420, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L201
.L198:
	movq	%r13, %rdi
	call	_ZN2v88internal26NumberConstructorAssembler29GenerateNumberConstructorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L198
.L202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25034:
	.size	_ZN2v88internal8Builtins26Generate_NumberConstructorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_NumberConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins37Generate_GenericLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"GenericLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins37Generate_GenericLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_GenericLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_GenericLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_GenericLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB25043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$742, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$612, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L207
.L204:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L204
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25043:
	.size	_ZN2v88internal8Builtins37Generate_GenericLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_GenericLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal37GenericLazyDeoptContinuationAssembler40GenerateGenericLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37GenericLazyDeoptContinuationAssembler40GenerateGenericLazyDeoptContinuationImplEv
	.type	_ZN2v88internal37GenericLazyDeoptContinuationAssembler40GenerateGenericLazyDeoptContinuationImplEv, @function
_ZN2v88internal37GenericLazyDeoptContinuationAssembler40GenerateGenericLazyDeoptContinuationImplEv:
.LFB25047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE25047:
	.size	_ZN2v88internal37GenericLazyDeoptContinuationAssembler40GenerateGenericLazyDeoptContinuationImplEv, .-_ZN2v88internal37GenericLazyDeoptContinuationAssembler40GenerateGenericLazyDeoptContinuationImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE:
.LFB32152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE32152:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ConstructVarargsEPNS0_14MacroAssemblerE
	.weak	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE:
	.byte	5
	.byte	4
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.quad	8232953172786769740
	.quad	7308609285988639090
	.align 16
.LC11:
	.quad	7310575174828190785
	.quad	8746397786378029600
	.align 16
.LC13:
	.quad	7163366772948627267
	.quad	2340027244185872756
	.align 16
.LC14:
	.quad	7596553777019711049
	.quad	8243122688569140602
	.align 16
.LC15:
	.quad	3273669567342210883
	.quad	8079585701754724975
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
