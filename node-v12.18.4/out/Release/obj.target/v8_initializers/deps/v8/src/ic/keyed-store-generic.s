	.file	"keyed-store-generic.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_10JSReceiverEEENS5_INS2_5BoolTEEENS5_INS2_4NameEEENS5_INS2_6ObjectEEENS2_12LanguageModeEEUlPNS4_4NodeEE_E10_M_managerERSt9_Any_dataRKSL_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_10JSReceiverEEENS5_INS2_5BoolTEEENS5_INS2_4NameEEENS5_INS2_6ObjectEEENS2_12LanguageModeEEUlPNS4_4NodeEE_E10_M_managerERSt9_Any_dataRKSL_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_10JSReceiverEEENS5_INS2_5BoolTEEENS5_INS2_4NameEEENS5_INS2_6ObjectEEENS2_12LanguageModeEEUlPNS4_4NodeEE_E10_M_managerERSt9_Any_dataRKSL_St18_Manager_operation:
.LFB26089:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26089:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_10JSReceiverEEENS5_INS2_5BoolTEEENS5_INS2_4NameEEENS5_INS2_6ObjectEEENS2_12LanguageModeEEUlPNS4_4NodeEE_E10_M_managerERSt9_Any_dataRKSL_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_10JSReceiverEEENS5_INS2_5BoolTEEENS5_INS2_4NameEEENS5_INS2_6ObjectEEENS2_12LanguageModeEEUlPNS4_4NodeEE_E10_M_managerERSt9_Any_dataRKSL_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26KeyedStoreGenericAssembler11SetPropertyENS2_5TNodeINS1_7ContextEEENS7_INS1_10JSReceiverEEENS7_INS1_5BoolTEEENS7_INS1_4NameEEENS7_INS1_6ObjectEEENS1_12LanguageModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26KeyedStoreGenericAssembler11SetPropertyENS2_5TNodeINS1_7ContextEEENS7_INS1_10JSReceiverEEENS7_INS1_5BoolTEEENS7_INS1_4NameEEENS7_INS1_6ObjectEEENS1_12LanguageModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26KeyedStoreGenericAssembler11SetPropertyENS2_5TNodeINS1_7ContextEEENS7_INS1_10JSReceiverEEENS7_INS1_5BoolTEEENS7_INS1_4NameEEENS7_INS1_6ObjectEEENS1_12LanguageModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26088:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	jmp	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	.cfi_endproc
.LFE26088:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26KeyedStoreGenericAssembler11SetPropertyENS2_5TNodeINS1_7ContextEEENS7_INS1_10JSReceiverEEENS7_INS1_5BoolTEEENS7_INS1_4NameEEENS7_INS1_6ObjectEEENS1_12LanguageModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26KeyedStoreGenericAssembler11SetPropertyENS2_5TNodeINS1_7ContextEEENS7_INS1_10JSReceiverEEENS7_INS1_5BoolTEEENS7_INS1_4NameEEENS7_INS1_6ObjectEEENS1_12LanguageModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_
	.type	_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_, @function
_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_:
.LFB21960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$8, %edx
	subq	$200, %rsp
	movq	%rcx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-192(%rbp), %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-216(%rbp), %rcx
	movl	$1, %r8d
	movq	%r13, -216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-232(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler36IsCustomElementsReceiverInstanceTypeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18IsFastElementsKindENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$28, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21960:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_, .-_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE:
.LFB21961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%rsi, -360(%rbp)
	movq	%rdx, -352(%rbp)
	movl	16(%rbp), %r11d
	movq	%rcx, -400(%rbp)
	movq	%rax, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	%r9b, %eax
	movl	$82, -372(%rbp)
	movl	%eax, -340(%rbp)
	testb	%r9b, %r9b
	je	.L20
	cmpb	$4, %r9b
	je	.L26
	cmpb	$2, %r9b
	je	.L27
	leal	81(%rax), %edx
	cmpb	$6, %r9b
	movl	$88, %eax
	cmovne	%edx, %eax
	movl	%eax, -372(%rbp)
.L20:
	movl	$82, -344(%rbp)
	movzbl	%r11b, %eax
	leal	81(%rax), %ecx
	movl	%eax, -376(%rbp)
	movl	%ecx, -380(%rbp)
	testb	%r11b, %r11b
	je	.L21
	cmpb	$4, %r11b
	je	.L30
	cmpb	$2, %r11b
	je	.L31
	cmpb	$6, %r11b
	movl	$88, %eax
	cmovne	%ecx, %eax
	movl	%eax, -344(%rbp)
.L21:
	movl	-376(%rbp), %esi
	movl	-340(%rbp), %edi
	movl	%r11d, -388(%rbp)
	movl	%r9d, -384(%rbp)
	call	_ZN2v88internal14AllocationSite11ShouldTrackENS0_12ElementsKindES2_@PLT
	movl	-384(%rbp), %r9d
	movl	-388(%rbp), %r11d
	testb	%al, %al
	jne	.L37
.L22:
	leaq	-320(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-192(%rbp), %r15
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	%r11d, -388(%rbp)
	movl	%r9d, -384(%rbp)
	leaq	-336(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	-340(%rbp), %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	-380(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	-372(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-368(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	-344(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	-384(%rbp), %r9d
	movl	-388(%rbp), %r11d
	subl	$4, %r9d
	cmpb	$1, %r9b
	setbe	%dl
	subl	$4, %r11d
	cmpb	$1, %r11b
	setbe	%al
	cmpb	%al, %dl
	je	.L23
	movq	-400(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler24LoadFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	subq	$8, %rsp
	pushq	-368(%rbp)
	movl	-376(%rbp), %r8d
	pushq	$1
	movq	%rax, %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	-340(%rbp), %ecx
	movq	-360(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal17CodeStubAssembler20GrowElementsCapacityEPNS0_8compiler4NodeES4_NS0_12ElementsKindES5_S4_S4_NS1_13ParameterModeEPNS2_18CodeAssemblerLabelE@PLT
	addq	$32, %rsp
.L23:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-360(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler8StoreMapEPNS0_8compiler4NodeES4_@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	-368(%rbp), %rdx
	movq	-360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21TrapAllocationMementoEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE@PLT
	movl	-388(%rbp), %r11d
	movl	-384(%rbp), %r9d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$86, -344(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$86, -372(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$84, -372(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$84, -344(%rbp)
	jmp	.L21
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21961:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE, .-_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_
	.type	_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_, @function
_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_:
.LFB21962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%r8b, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	%r8d, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	movzbl	%bl, %ebx
	subq	$40, %rsp
	movq	24(%rbp), %r9
	movq	32(%rbp), %rax
	movq	%rdx, -72(%rbp)
	movq	%rcx, %rdx
	movl	%r8d, -52(%rbp)
	movq	16(%rbp), %r14
	movq	%r9, -64(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	-72(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-64(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	-52(%rbp), %r8d
	movl	%ebx, %esi
	movl	%r8d, %edi
	call	_ZN2v88internal14AllocationSite11ShouldTrackENS0_12ElementsKindES2_@PLT
	testb	%al, %al
	jne	.L45
.L40:
	leal	81(%rbx), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler8StoreMapEPNS0_8compiler4NodeES4_@PLT
	addq	$40, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21TrapAllocationMementoEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE@PLT
	jmp	.L40
	.cfi_endproc
.LFE21962:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_, .-_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE:
.LFB21963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r9d, %eax
	movq	%rcx, %r10
	movzbl	%al, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -200(%rbp)
	movq	16(%rbp), %r9
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	$1, %ebx
	testb	%al, %al
	je	.L47
	cmpb	$4, %al
	je	.L50
	cmpb	$2, %al
	je	.L51
	cmpb	$6, %al
	movl	$7, %ebx
	cmovne	%r11d, %ebx
.L47:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -224(%rbp)
	leaq	-192(%rbp), %r13
	movl	$1, %r8d
	movl	%r11d, -204(%rbp)
	movq	%r13, %rdi
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-216(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-224(%rbp), %r9
	movl	-204(%rbp), %r11d
	movq	%rax, %rcx
	movq	-200(%rbp), %rsi
	pushq	%r9
	movl	%r11d, %r8d
	pushq	%r9
	movl	%ebx, %r9d
	pushq	%r13
	call	_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movl	$5, %ebx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$3, %ebx
	jmp	.L47
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21963:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler24TryChangeToHoleyMapMultiEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindESA_PNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler24TryChangeToHoleyMapMultiEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindESA_PNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler24TryChangeToHoleyMapMultiEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindESA_PNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal26KeyedStoreGenericAssembler24TryChangeToHoleyMapMultiEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindESA_PNS2_18CodeAssemblerLabelE:
.LFB21964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r9d, %eax
	movzbl	%r9b, %r11d
	movl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movl	16(%rbp), %edx
	movq	%r8, -352(%rbp)
	movq	%rbx, -344(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testb	%al, %al
	je	.L56
	cmpb	$4, %al
	je	.L60
	cmpb	$2, %al
	je	.L61
	cmpb	$6, %al
	movl	$7, %r9d
	cmovne	%r11d, %r9d
.L56:
	movzbl	%dl, %r10d
	movl	$1, %ebx
	testb	%dl, %dl
	je	.L57
	cmpb	$4, %dl
	je	.L64
	cmpb	$2, %dl
	je	.L65
	cmpb	$6, %dl
	movl	$7, %eax
	cmovne	%r10d, %eax
	movl	%eax, %ebx
.L57:
	leaq	-320(%rbp), %r13
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-192(%rbp), %r14
	movl	$1, %r8d
	movq	%r13, %rdi
	movl	%r10d, -364(%rbp)
	movl	%r11d, -360(%rbp)
	movl	%r9d, -356(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	-356(%rbp), %r9d
	movq	%r12, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-336(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-336(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	subq	$8, %rsp
	movl	-360(%rbp), %r11d
	pushq	-344(%rbp)
	pushq	%r14
	movq	%rax, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%r13
	movl	-356(%rbp), %r9d
	movl	%r11d, %r8d
	movq	-328(%rbp), %rdx
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-344(%rbp), %rax
	subq	$8, %rsp
	movl	-364(%rbp), %r10d
	movq	-336(%rbp), %rcx
	movl	%ebx, %r9d
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%rax
	movq	-328(%rbp), %rdx
	movl	%r10d, %r8d
	pushq	%rax
	pushq	%r13
	call	_ZN2v88internal26KeyedStoreGenericAssembler25TryChangeToHoleyMapHelperEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelESA_SA_
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$5, %r9d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$5, %ebx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$3, %r9d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$3, %ebx
	jmp	.L57
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21964:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler24TryChangeToHoleyMapMultiEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindESA_PNS2_18CodeAssemblerLabelE, .-_ZN2v88internal26KeyedStoreGenericAssembler24TryChangeToHoleyMapMultiEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindESA_PNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler26MaybeUpdateLengthAndReturnEPNS0_8compiler4NodeES4_S4_NS1_12UpdateLengthE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler26MaybeUpdateLengthAndReturnEPNS0_8compiler4NodeES4_S4_NS1_12UpdateLengthE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler26MaybeUpdateLengthAndReturnEPNS0_8compiler4NodeES4_S4_NS1_12UpdateLengthE, @function
_ZN2v88internal26KeyedStoreGenericAssembler26MaybeUpdateLengthAndReturnEPNS0_8compiler4NodeES4_S4_NS1_12UpdateLengthE:
.LFB21965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	testl	%r8d, %r8d
	jne	.L75
.L70:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%rsi, %r15
	movl	$1, %esi
	movq	%rdx, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movl	$24, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	jmp	.L70
	.cfi_endproc
.LFE21965:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler26MaybeUpdateLengthAndReturnEPNS0_8compiler4NodeES4_S4_NS1_12UpdateLengthE, .-_ZN2v88internal26KeyedStoreGenericAssembler26MaybeUpdateLengthAndReturnEPNS0_8compiler4NodeES4_S4_NS1_12UpdateLengthE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE, @function
_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE:
.LFB21966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$648, %rsp
	movq	24(%rbp), %rax
	movq	%rsi, -592(%rbp)
	movq	%rdx, -608(%rbp)
	movq	16(%rbp), %r14
	movq	%rax, -624(%rbp)
	movq	32(%rbp), %rax
	movq	%r8, -656(%rbp)
	movq	%r9, -600(%rbp)
	movq	%rax, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	40(%rbp), %eax
	testl	%eax, %eax
	jne	.L113
.L77:
	leaq	-576(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	%r15, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayMapConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-192(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %rsi
	movl	$15, %r8d
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$2, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	40(%rbp), %r15d
	testl	%r15d, %r15d
	je	.L114
.L78:
	movq	-584(%rbp), %rdx
	movq	-608(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpl	$2, 40(%rbp)
	je	.L115
	movq	-616(%rbp), %rcx
	movq	%r14, %r8
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$6, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	40(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L80
.L81:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler20Int32LessThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpl	$2, 40(%rbp)
	je	.L116
	movq	-616(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler5StoreEPNS1_4NodeES4_S4_@PLT
	movl	40(%rbp), %esi
	testl	%esi, %esi
	jne	.L83
.L84:
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%eax, %eax
	cmpl	$2, 40(%rbp)
	movq	%rbx, %rcx
	sete	%al
	pushq	-584(%rbp)
	movq	-648(%rbp), %r8
	xorl	%r9d, %r9d
	addl	$4, %eax
	movq	-608(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rax
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE
	leaq	8(%r12), %rax
	movq	-592(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	-600(%rbp), %rsi
	movl	$15, %r8d
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$4, %edx
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17Float64SilenceNaNENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	-688(%rbp), %r9
	movl	$13, %esi
	movq	%r12, %rdi
	movq	-680(%rbp), %rcx
	movq	%rax, %r8
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	40(%rbp), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jne	.L117
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	-584(%rbp)
	xorl	%r9d, %r9d
	movq	-648(%rbp), %r8
	movq	-608(%rbp), %rdx
	pushq	$2
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-616(%rbp), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler5StoreEPNS1_4NodeES4_S4_@PLT
	popq	%rcx
	popq	%rsi
	movl	$2, -616(%rbp)
.L94:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27FixedDoubleArrayMapConstantEv@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-632(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %rsi
	movl	$15, %r8d
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$4, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	40(%rbp), %eax
	testl	%eax, %eax
	je	.L118
	movq	-584(%rbp), %rdx
	movq	-608(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18TryTaggedToFloat64EPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17Float64SilenceNaNENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	cmpl	$2, 40(%rbp)
	movq	%rax, %r15
	je	.L119
	movq	-648(%rbp), %rcx
	movq	%rax, %r8
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L119:
	subq	$8, %rsp
	pushq	-584(%rbp)
	movq	-624(%rbp), %r8
	movl	$4, %r9d
	movq	-656(%rbp), %rcx
	movq	%r12, %rdi
	movq	-608(%rbp), %rdx
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE
	movq	%r15, %r8
	movq	%rbx, %rdx
	movl	$13, %esi
	movq	-648(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	popq	%r15
	popq	%rax
.L95:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-592(%rbp), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
.L90:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	pushq	-584(%rbp)
	movq	%rbx, %rcx
	movq	-592(%rbp), %rbx
	movq	%rax, %r8
	movl	-616(%rbp), %eax
	movl	$4, %r9d
	movq	%r12, %rdi
	movq	-608(%rbp), %rdx
	movq	%rbx, %rsi
	pushq	%rax
	call	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE
	movq	-672(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	-600(%rbp), %rsi
	movl	$15, %r8d
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$2, %edx
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler5StoreEPNS1_4NodeES4_S4_@PLT
	movl	40(%rbp), %r11d
	popq	%r9
	popq	%r10
	testl	%r11d, %r11d
	jne	.L120
.L92:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-632(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-592(%rbp), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpl	$2, 40(%rbp)
	jne	.L122
	pushq	-584(%rbp)
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-648(%rbp), %r8
	pushq	$3
	movq	-608(%rbp), %rdx
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	-616(%rbp), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler5StoreEPNS1_4NodeES4_S4_@PLT
	popq	%rdi
	popq	%r8
	movl	$3, -616(%rbp)
.L93:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-592(%rbp), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L115:
	pushq	-584(%rbp)
	movq	-624(%rbp), %r8
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	-656(%rbp), %rcx
	pushq	$2
	movq	-608(%rbp), %rdx
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler24TryChangeToHoleyMapMultiEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindESA_PNS2_18CodeAssemblerLabelE
	movq	%r14, %r8
	movq	%rbx, %rdx
	movl	$6, %esi
	movq	-616(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	popq	%r10
	popq	%r11
.L80:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-592(%rbp), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-616(%rbp), %rcx
	movq	%rbx, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rdx, %rsi
	movq	%rdx, %r15
	call	_ZN2v88internal17CodeStubAssembler15IsDictionaryMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-584(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadMapDescriptorsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler28LoadDetailsByDescriptorEntryENS0_8compiler5TNodeINS0_15DescriptorArrayEEEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$8, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-584(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L116:
	subq	$8, %rsp
	pushq	-584(%rbp)
	movq	-624(%rbp), %r8
	movl	$2, %r9d
	movq	-656(%rbp), %rcx
	movq	%r12, %rdi
	movq	-608(%rbp), %rdx
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler19TryChangeToHoleyMapEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS5_INS0_7Word32TEEES4_NS0_12ElementsKindEPNS2_18CodeAssemblerLabelE
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	-616(%rbp), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler5StoreEPNS1_4NodeES4_S4_@PLT
	popq	%rdi
	popq	%r8
.L83:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-592(%rbp), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	-648(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23LoadDoubleWithHoleCheckENS0_8compiler11SloppyTNodeINS0_6ObjectEEENS3_INS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelENS0_11MachineTypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-584(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-608(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler37BranchIfPrototypesHaveNonFastElementsENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelES7_
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18TryTaggedToFloat64EPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17Float64SilenceNaNENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdx
	movl	$13, %esi
	movq	%r12, %rdi
	movq	-648(%rbp), %rcx
	movq	%rax, %r8
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-592(%rbp), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	jmp	.L92
.L121:
	call	__stack_chk_fail@PLT
.L122:
	pushq	-584(%rbp)
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-648(%rbp), %r8
	pushq	$2
	movq	-608(%rbp), %rdx
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler18TryRewriteElementsEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_S4_NS0_12ElementsKindES8_PNS2_18CodeAssemblerLabelE
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-616(%rbp), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler5StoreEPNS1_4NodeES4_S4_@PLT
	popq	%rax
	popq	%rdx
	movl	$2, -616(%rbp)
	jmp	.L93
	.cfi_endproc
.LFE21966:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE, .-_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler23EmitGenericElementStoreEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler23EmitGenericElementStoreEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler23EmitGenericElementStoreEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal26KeyedStoreGenericAssembler23EmitGenericElementStoreEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelE:
.LFB21967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movl	$1, %r8d
	subq	$1480, %rsp
	movq	16(%rbp), %rax
	movq	%r9, -1416(%rbp)
	movq	%rdx, -1408(%rbp)
	xorl	%edx, %edx
	movq	24(%rbp), %r15
	movq	%rax, -1440(%rbp)
	leaq	-1376(%rbp), %rax
	movq	%rcx, -1512(%rbp)
	movq	%rax, %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1248(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-1120(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-992(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -1464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-864(%rbp), %rcx
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r13, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-736(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-608(%rbp), %rcx
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -1480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r10
	movq	%rsi, -1488(%rbp)
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-352(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r9
	movl	$1, %r8d
	movq	%rsi, -1504(%rbp)
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	8(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	-1408(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1400(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18IsFastElementsKindENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	-1480(%rbp), %rcx
	movq	-1448(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-224(%rbp), %rcx
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -1496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1512(%rbp), %r10
	movl	$1061, %edx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1520(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1496(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24LoadFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1456(%rbp), %rcx
	movq	-1424(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1424(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24LoadFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler25UintPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1432(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1512(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1472(%rbp), %rcx
	movq	-1464(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	$0
	movq	%rbx, %r9
	movq	%r13, %r8
	pushq	%r15
	movq	-1400(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-1440(%rbp)
	movq	-1408(%rbp), %rdx
	pushq	-1416(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE
	movq	-1456(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1520(%rbp), %r10
	movl	$1086, %edx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1432(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	$1
	movq	%rbx, %r9
	movq	%r13, %r8
	pushq	%r15
	movq	-1400(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-1440(%rbp)
	movq	-1408(%rbp), %rdx
	pushq	-1416(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE
	movq	-1472(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	$2
	movq	%rbx, %r9
	movq	%r13, %r8
	pushq	%r15
	movq	-1400(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	-1440(%rbp)
	movq	-1408(%rbp), %rdx
	pushq	-1416(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler24StoreElementWithCapacityEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEENS2_11SloppyTNodeINS0_14FixedArrayBaseEEENS5_INS0_7Word32TEEENS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelENS1_12UpdateLengthE
	movq	-1432(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L132
.L124:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler23Int32GreaterThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-1488(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1504(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L133
.L126:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L134
.L128:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	leaq	-1384(%rbp), %rsi
	xorl	%edx, %edx
	movq	$18, -1384(%rbp)
	movq	%r14, %rdi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movl	$25970, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	-1384(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movw	%cx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-1384(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L124
	call	_ZdlPv@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rbx
	movb	$121, -70(%rbp)
	movabsq	$8241904408635406676, %rax
	movq	%rbx, -96(%rbp)
	movq	%rax, -80(%rbp)
	movl	$24946, %eax
	movw	%ax, -72(%rbp)
	movq	$11, -88(%rbp)
	movb	$0, -69(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L128
	call	_ZdlPv@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$31090, %edx
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	movb	$0, -70(%rbp)
	movabsq	$7020671367832103236, %rax
	leaq	-80(%rbp), %rbx
	movw	%dx, -72(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$10, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L126
	call	_ZdlPv@PLT
	jmp	.L126
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21967:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler23EmitGenericElementStoreEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelE, .-_ZN2v88internal26KeyedStoreGenericAssembler23EmitGenericElementStoreEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler30LookupPropertyOnPrototypeChainENS0_8compiler5TNodeINS0_3MapEEEPNS2_4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableESB_S9_S9_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler30LookupPropertyOnPrototypeChainENS0_8compiler5TNodeINS0_3MapEEEPNS2_4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableESB_S9_S9_
	.type	_ZN2v88internal26KeyedStoreGenericAssembler30LookupPropertyOnPrototypeChainENS0_8compiler5TNodeINS0_3MapEEEPNS2_4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableESB_S9_S9_, @function
_ZN2v88internal26KeyedStoreGenericAssembler30LookupPropertyOnPrototypeChainENS0_8compiler5TNodeINS0_3MapEEEPNS2_4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableESB_S9_S9_:
.LFB21968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-1040(%rbp), %rax
	leaq	-1056(%rbp), %r15
	pushq	%r13
	movq	%r15, %xmm0
	.cfi_offset 13, -40
	leaq	-976(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1176, %rsp
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rsi
	movq	%r9, -1168(%rbp)
	movq	%rcx, -1088(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -1160(%rbp)
	movl	$1, %r8d
	movq	%rdx, -1112(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -1184(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rax, -1144(%rbp)
	movq	%r13, %rdi
	movhps	-1144(%rbp), %xmm0
	movaps	%xmm0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1144(%rbp), %r14
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movdqa	-1072(%rbp), %xmm0
	leaq	-80(%rbp), %rcx
	leaq	-848(%rbp), %rax
	movl	$1, %r8d
	movl	$2, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1080(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	leaq	-208(%rbp), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1104(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	leaq	-720(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1176(%rbp)
	movq	%rdi, -1096(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-592(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-464(%rbp), %rdx
	movq	%rdx, %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r11, -1152(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-336(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r14, -1128(%rbp)
	movq	%r12, %rsi
	leaq	-1024(%rbp), %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1008(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r11
	movl	$5, %edx
	movq	%r11, %rdi
	movq	%r11, -1072(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	pushq	-1184(%rbp)
	movq	%rbx, %r8
	movq	-1152(%rbp), %r9
	movq	-1176(%rbp), %rcx
	pushq	-1096(%rbp)
	movq	%r12, %rdi
	movq	-1104(%rbp), %rdx
	pushq	-1072(%rbp)
	movq	-1080(%rbp), %rsi
	pushq	%r14
	pushq	-1128(%rbp)
	pushq	-1120(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17TryLookupPropertyENS0_8compiler11SloppyTNodeINS0_8JSObjectEEENS3_INS0_3MapEEENS3_INS0_6Int32TEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelESD_SD_PNS2_26TypedCodeAssemblerVariableINS0_10HeapObjectEEEPNSE_INS0_7IntPtrTEEESD_SD_@PLT
	movq	-1152(%rbp), %rsi
	addq	$48, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1072(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -1208(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21LoadDetailsByKeyIndexENS0_8compiler5TNodeINS0_15DescriptorArrayEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-1112(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17AccessorAssembler18JumpIfDataPropertyEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelES6_@PLT
	leaq	-992(%rbp), %r9
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, -1200(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	subq	$8, %rsp
	movq	%rbx, %rcx
	movq	%r12, %rdi
	pushq	-1160(%rbp)
	movq	-1080(%rbp), %rbx
	movq	-1208(%rbp), %r8
	movq	-1104(%rbp), %rdx
	movq	-1200(%rbp), %r9
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler26LoadPropertyFromFastObjectEPNS0_8compiler4NodeES4_NS2_5TNodeINS0_15DescriptorArrayEEES4_PNS2_21CodeAssemblerVariableES9_@PLT
	movq	-1168(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rbx, -1080(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1200(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1072(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$16, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler37LoadAndUntagToWord32FixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	movq	-1112(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17AccessorAssembler18JumpIfDataPropertyEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelES6_@PLT
	cmpq	$0, -1088(%rbp)
	popq	%rdi
	popq	%r8
	je	.L137
	subq	$8, %rsp
	movl	$8, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	pushq	$0
	movq	-1200(%rbp), %r10
	movl	$2, %r9d
	movl	$1, %r8d
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movq	-1160(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1080(%rbp), %rsi
	movq	-1168(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%rcx
	popq	%rsi
.L138:
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1072(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	pushq	$0
	movl	$2, %r9d
	movq	%rax, %rdx
	movl	$8, %ecx
	movl	$1, %r8d
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1096(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1200(%rbp), %r8
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler31LoadAndUntagToWord32ObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	movq	-1112(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17AccessorAssembler18JumpIfDataPropertyEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelES6_@PLT
	cmpq	$0, -1088(%rbp)
	popq	%rax
	popq	%rdx
	je	.L139
	movq	-1160(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1080(%rbp), %rsi
	movq	-1168(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L140:
	movq	-1072(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1152(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1192(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1176(%rbp), %rsi
	movl	$1086, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1184(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1144(%rbp), %r14
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1136(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L140
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21968:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler30LookupPropertyOnPrototypeChainENS0_8compiler5TNodeINS0_3MapEEEPNS2_4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableESB_S9_S9_, .-_ZN2v88internal26KeyedStoreGenericAssembler30LookupPropertyOnPrototypeChainENS0_8compiler5TNodeINS0_3MapEEEPNS2_4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableESB_S9_S9_
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler40FindCandidateStoreICTransitionMapHandlerENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler40FindCandidateStoreICTransitionMapHandlerENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler40FindCandidateStoreICTransitionMapHandlerENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal26KeyedStoreGenericAssembler40FindCandidateStoreICTransitionMapHandlerENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelE:
.LFB21975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-624(%rbp), %r14
	leaq	-320(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$632, %rsp
	movq	%rdx, -648(%rbp)
	movl	$7, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-576(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	leaq	-448(%rbp), %rcx
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-608(%rbp), %r13
	movl	$72, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rsi
	movl	$8, %edx
	movq	%r13, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	subq	$8, %rsp
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	pushq	%r13
	movq	-656(%rbp), %r10
	movq	%r12, %rdi
	movq	-640(%rbp), %r9
	movq	-632(%rbp), %r8
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler19DispatchMaybeObjectENS0_8compiler5TNodeINS0_11MaybeObjectEEEPNS2_18CodeAssemblerLabelES7_S7_S7_PNS2_26TypedCodeAssemblerVariableINS0_6ObjectEEE@PLT
	movq	-632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20IsTransitionArrayMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-592(%rbp), %rax
	movq	%r12, %rsi
	movl	$5, %edx
	movq	%rax, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-192(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r11, -664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %r9
	movq	-664(%rbp), %r11
	movq	%r12, %rdi
	movq	-656(%rbp), %rbx
	movq	-648(%rbp), %rsi
	movq	%rax, %rdx
	movq	%rax, -656(%rbp)
	movq	%r11, %rcx
	movq	%rbx, %r8
	call	_ZN2v88internal17CodeStubAssembler16TransitionLookupENS0_8compiler11SloppyTNodeINS0_4NameEEENS3_INS0_15TransitionArrayEEEPNS2_18CodeAssemblerLabelEPNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEES9_@PLT
	movq	-664(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -648(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-656(%rbp), %r10
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	movl	$16, %edx
	movl	$2, (%rsp)
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadArrayElementINS0_15TransitionArrayENS0_11MaybeObjectEEENS0_8compiler5TNodeIT0_EENS6_IT_EEiPNS5_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23GetHeapObjectAssumeWeakENS0_8compiler5TNodeINS0_11MaybeObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L147:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21975:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler40FindCandidateStoreICTransitionMapHandlerENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal26KeyedStoreGenericAssembler40FindCandidateStoreICTransitionMapHandlerENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE, @function
_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE:
.LFB21982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1376(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$1608, %rsp
	movq	%r9, -1496(%rbp)
	movzwl	16(%rbp), %eax
	movq	%rsi, -1552(%rbp)
	movq	%rdi, %rsi
	movq	%rdx, -1544(%rbp)
	movl	$8, %edx
	movq	%rcx, -1488(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movb	%al, -1641(%rbp)
	movzbl	%ah, %eax
	movw	%ax, -1644(%rbp)
	leaq	-1456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1592(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1440(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r14, -1640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1248(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1120(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	16(%r13), %rax
	movq	%rax, -1480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$2097152, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1576(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	leaq	-96(%rbp), %rax
	jne	.L200
	movq	%rax, -1568(%rbp)
	leaq	-224(%rbp), %r13
.L149:
	movq	-1544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadMapDescriptorsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %r14
	leaq	-480(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-352(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1520(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-736(%rbp), %rcx
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -1528(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	subq	$8, %rsp
	pushq	-1520(%rbp)
	movq	-1528(%rbp), %r9
	movq	-1504(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-1480(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16DescriptorLookupENS0_8compiler11SloppyTNodeINS0_4NameEEENS3_INS0_15DescriptorArrayEEENS3_INS0_7Uint32TEEEPNS2_18CodeAssemblerLabelEPNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEESB_@PLT
	movq	-1504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1528(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler21LoadDetailsByKeyIndexENS0_8compiler5TNodeINS0_15DescriptorArrayEEENS3_INS0_7IntPtrTEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	cmpl	$1, 16(%r12)
	popq	%r8
	movq	%r13, %rdx
	movq	-1584(%rbp), %rsi
	movl	$0, %ecx
	popq	%r9
	movq	%r12, %rdi
	cmovne	-1560(%rbp), %rcx
	call	_ZN2v88internal17AccessorAssembler18JumpIfDataPropertyEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelES6_@PLT
	movl	16(%r12), %r10d
	testl	%r10d, %r10d
	jne	.L152
	leaq	-608(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1536(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	subq	$8, %rsp
	pushq	-1592(%rbp)
	movq	-1536(%rbp), %r9
	movq	-1544(%rbp), %rdx
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	-1552(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler26LoadPropertyFromFastObjectEPNS0_8compiler4NodeES4_NS2_5TNodeINS0_15DescriptorArrayEEES4_PNS2_21CodeAssemblerVariableES9_@PLT
	movq	-1552(%rbp), %rsi
	movq	-1608(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1536(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rsi
	popq	%rdi
.L153:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rdx
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27CheckForAssociatedProtectorENS0_8compiler11SloppyTNodeINS0_4NameEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	-1488(%rbp), %rax
	subq	$8, %rsp
	movq	-1552(%rbp), %rsi
	pushq	$0
	movq	-1584(%rbp), %r9
	movq	%r15, %r8
	movq	%r14, %rcx
	pushq	-1496(%rbp)
	movq	-1544(%rbp), %rdx
	movq	%r12, %rdi
	pushq	48(%rax)
	call	_ZN2v88internal17AccessorAssembler33OverwriteExistingFastDataPropertyEPNS0_8compiler4NodeES4_S4_S4_S4_NS2_5TNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelEb@PLT
	movq	-1488(%rbp), %rax
	addq	$32, %rsp
	cmpq	$0, 24(%rbx)
	movq	48(%rax), %rsi
	je	.L205
	movq	%rsi, -608(%rbp)
	leaq	8(%rbx), %rdi
	movq	-1536(%rbp), %rsi
	call	*32(%rbx)
.L155:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L206
.L156:
	movq	-1496(%rbp), %r15
	movq	-1480(%rbp), %rdx
	movq	%r12, %rdi
	movq	-1544(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN2v88internal26KeyedStoreGenericAssembler40FindCandidateStoreICTransitionMapHandlerENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_4NameEEEPNS2_18CodeAssemblerLabelE
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	cmpl	$1, 16(%r12)
	movq	-1488(%rbp), %r15
	movq	%rax, %rdx
	setne	%r8b
	addl	$2, %r8d
	movq	%r15, %rsi
	call	_ZN2v88internal17AccessorAssembler37HandleStoreICTransitionMapHandlerCaseEPKNS1_17StoreICParametersENS0_8compiler5TNodeINS0_3MapEEEPNS5_18CodeAssemblerLabelENS1_23StoreTransitionMapFlagsE@PLT
	cmpq	$0, 24(%rbx)
	movq	48(%r15), %rsi
	je	.L207
	movq	%rsi, -224(%rbp)
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	*32(%rbx)
.L160:
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1520(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L208
.L161:
	leaq	-1424(%rbp), %r15
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-864(%rbp), %rax
	movl	$1, %r8d
	movq	%r15, -224(%rbp)
	movq	%rax, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1528(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadSlowPropertiesENS0_8compiler11SloppyTNodeINS0_8JSObjectEEE@PLT
	subq	$8, %rsp
	movq	%r15, %r8
	movq	%r12, %rdi
	pushq	$0
	movq	-1528(%rbp), %r9
	movq	%rax, %rsi
	movq	%rax, %r14
	movq	-1584(%rbp), %rcx
	movq	-1480(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler20NameDictionaryLookupINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEEPNS4_18CodeAssemblerLabelEPNS4_26TypedCodeAssemblerVariableINS0_7IntPtrTEEESB_NS1_10LookupModeE@PLT
	movq	-1584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$16, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	call	_ZN2v88internal17CodeStubAssembler37LoadAndUntagToWord32FixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	cmpl	$1, 16(%r12)
	movl	$0, %ecx
	movq	%r12, %rdi
	cmovne	-1560(%rbp), %rcx
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	movq	%r13, %rdx
	call	_ZN2v88internal17AccessorAssembler18JumpIfDataPropertyEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelES6_@PLT
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	je	.L209
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L165:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rdx
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27CheckForAssociatedProtectorENS0_8compiler11SloppyTNodeINS0_4NameEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	-1488(%rbp), %rax
	movq	%r15, %rdi
	movq	48(%rax), %r10
	movq	%r10, -1616(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$8, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	-1616(%rbp), %r10
	pushq	$1
	movq	-1632(%rbp), %rdx
	movl	$8, %r9d
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	-1488(%rbp), %rax
	cmpq	$0, 24(%rbx)
	popq	%r8
	popq	%r9
	movq	48(%rax), %rsi
	je	.L210
	movq	%rsi, -352(%rbp)
	leaq	8(%rbx), %rdi
	movq	-1520(%rbp), %rsi
	call	*32(%rbx)
.L167:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rdx
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27CheckForAssociatedProtectorENS0_8compiler11SloppyTNodeINS0_4NameEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	-1536(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1504(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15IsPrivateSymbolENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1504(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$134217728, %esi
	movq	%r12, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-1616(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1496(%rbp), %rcx
	movq	-1536(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsPrivateNameENS0_8compiler11SloppyTNodeINS0_6SymbolEEE@PLT
	movq	-1536(%rbp), %rcx
	movq	-1496(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	16(%r12), %edi
	testl	%edi, %edi
	je	.L211
.L168:
	movq	-1520(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1632(%rbp), %rdx
	movq	-1544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler33InvalidateValidityCellIfPrototypeEPNS0_8compiler4NodeES4_@PLT
	movq	-1488(%rbp), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-1520(%rbp), %r8
	movq	-1480(%rbp), %rdx
	movq	48(%rax), %rcx
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	-1488(%rbp), %rax
	cmpq	$0, 24(%rbx)
	movq	48(%rax), %rsi
	je	.L212
	movq	%rsi, -224(%rbp)
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	*32(%rbx)
.L170:
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1488(%rbp), %rdx
	cmpq	$0, 24(%rbx)
	movq	48(%rdx), %rcx
	movq	8(%rdx), %rax
	movq	(%rdx), %r14
	je	.L213
	movq	%rcx, -80(%rbp)
	movq	%rax, %xmm0
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	-1568(%rbp), %rcx
	movl	$3, %r8d
	movl	$203, %esi
	movhps	-1480(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, -1472(%rbp)
	je	.L181
	leaq	-1472(%rbp), %rsi
	leaq	8(%rbx), %rdi
	call	*32(%rbx)
.L172:
	movq	-1520(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1536(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L214
.L174:
	cmpl	$1, %eax
	je	.L184
	movq	-1560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, -1641(%rbp)
	je	.L185
	cmpb	$1, -1644(%rbp)
	je	.L215
	movq	-1488(%rbp), %rax
	cmpq	$0, 24(%rbx)
	movq	48(%rax), %rsi
	je	.L188
.L202:
	movq	%rsi, -224(%rbp)
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	call	*32(%rbx)
.L184:
	movq	-1560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1576(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1608(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1592(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	pushq	$0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$8, %ecx
	movl	$1, %r8d
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movq	-1592(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1552(%rbp), %rsi
	movq	-1608(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%r10
	popq	%r11
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-608(%rbp), %rax
	movq	%rax, -1536(%rbp)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L200:
	leaq	-224(%rbp), %r13
	xorl	%edx, %edx
	leaq	-80(%rbp), %r14
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%r14, -96(%rbp)
	movq	$19, -224(%rbp)
	movq	%rax, -1568(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC2(%rip), %xmm0
	movl	$29295, %r11d
	movq	%rax, -96(%rbp)
	movq	-1568(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movw	%r11w, 16(%rax)
	movb	$101, 18(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L149
	call	_ZdlPv@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L206:
	movq	-1568(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	$17, -224(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movb	$110, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L156
	call	_ZdlPv@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L208:
	movq	-1568(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	$25, -224(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8245937480553559154, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movb	$101, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L161
	call	_ZdlPv@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L211:
	pushq	-1496(%rbp)
	movq	-1600(%rbp), %rcx
	movq	%r12, %rdi
	movq	-1544(%rbp), %rsi
	pushq	-1560(%rbp)
	movq	-1608(%rbp), %r9
	movq	-1592(%rbp), %r8
	movq	-1480(%rbp), %rdx
	call	_ZN2v88internal26KeyedStoreGenericAssembler30LookupPropertyOnPrototypeChainENS0_8compiler5TNodeINS0_3MapEEEPNS2_4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableESB_S9_S9_
	popq	%rcx
	popq	%rsi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-1600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1592(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17IsAccessorInfoMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1496(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler25IsFunctionTemplateInfoMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1496(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsCallableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-1408(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	-1488(%rbp), %rax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	48(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -1496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1408(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %xmm0
	xorl	%esi, %esi
	movq	%r15, %r9
	movq	%rax, %r8
	movl	$4, %edx
	movq	-1392(%rbp), %rax
	movhps	-1504(%rbp), %xmm0
	pushq	%rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	-1520(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	movq	-1568(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-1552(%rbp), %xmm0
	pushq	%rax
	movq	%rcx, -352(%rbp)
	movhps	-1496(%rbp), %xmm0
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1488(%rbp), %rax
	cmpq	$0, 24(%rbx)
	movq	48(%rax), %rsi
	popq	%rax
	popq	%rdx
	je	.L217
	movq	%rsi, -352(%rbp)
	leaq	8(%rbx), %rdi
	movq	-1520(%rbp), %rsi
	call	*32(%rbx)
.L176:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, -1641(%rbp)
	je	.L177
	cmpb	$1, -1644(%rbp)
	je	.L218
	movq	-1488(%rbp), %rax
	cmpq	$0, 24(%rbx)
	movq	48(%rax), %rsi
	je	.L190
.L203:
	movq	%rsi, -352(%rbp)
	leaq	8(%rbx), %rdi
	movq	-1520(%rbp), %rsi
	call	*32(%rbx)
.L182:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movl	16(%r12), %eax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%rbx), %rdi
	movq	%rax, %xmm0
	movl	$3, %esi
	movq	%rcx, -1616(%rbp)
	movhps	-1480(%rbp), %xmm0
	movq	%rdi, -1544(%rbp)
	movaps	%xmm0, -1632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1616(%rbp), %rcx
	movdqa	-1632(%rbp), %xmm0
	movl	$3, %r9d
	movq	-1568(%rbp), %r8
	movq	-1544(%rbp), %rdi
	movq	%rax, %rdx
	movl	$203, %esi
	movq	%rcx, -80(%rbp)
	movq	%r14, %rcx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-1608(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$87, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, %xmm0
	movq	-1488(%rbp), %rax
	movq	(%rax), %r14
	je	.L219
	movq	(%rbx), %rdi
	movl	$3, %r8d
	movq	%r14, %rdx
	movq	%r15, -80(%rbp)
	movq	-1568(%rbp), %rcx
	movhps	-1480(%rbp), %xmm0
	movl	$178, %esi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, -1464(%rbp)
	je	.L181
	leaq	-1464(%rbp), %rsi
	leaq	8(%rbx), %rdi
	call	*32(%rbx)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L185:
	movq	-1488(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler6TypeofEPNS0_8compiler4NodeE@PLT
	movl	$165, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1488(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, -80(%rbp)
	movq	-1480(%rbp), %rsi
	movl	$4, %r8d
	movq	%rax, -96(%rbp)
	movq	(%rcx), %rdx
	movq	-1568(%rbp), %rcx
	movq	%r14, -72(%rbp)
	movq	%rsi, -88(%rbp)
	movl	$179, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-1488(%rbp), %rcx
	cmpq	$0, 24(%rbx)
	movq	48(%rcx), %rsi
	jne	.L202
.L188:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L212:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L210:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L207:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L205:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-1488(%rbp), %rbx
	movq	%r12, %rdi
	movq	8(%rbx), %rsi
	call	_ZN2v88internal17CodeStubAssembler6TypeofEPNS0_8compiler4NodeE@PLT
	movq	8(%rbx), %r9
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	-1480(%rbp), %rcx
	movq	%rax, %r8
	movl	$165, %edx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L177:
	movq	-1608(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$87, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1488(%rbp), %r15
	movl	$179, %esi
	movq	-1568(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	-1480(%rbp), %rax
	movl	$3, %r8d
	movq	%r12, %rdi
	movq	(%r15), %rdx
	movq	%r14, -80(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	cmpq	$0, 24(%rbx)
	movq	48(%r15), %rsi
	jne	.L203
.L190:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L217:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L219:
	movq	(%rbx), %rdi
	movhps	-1480(%rbp), %xmm0
	movl	$3, %esi
	movaps	%xmm0, -1520(%rbp)
	movq	%rdi, -1496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movdqa	-1520(%rbp), %xmm0
	movq	%r14, %rcx
	movq	-1568(%rbp), %r8
	movq	-1496(%rbp), %rdi
	movq	%rax, %rdx
	movl	$3, %r9d
	movl	$178, %esi
	movq	%r15, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	jmp	.L182
.L216:
	call	__stack_chk_fail@PLT
.L181:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE21982:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE, .-_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE, @function
_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE:
.LFB21989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-704(%rbp), %r15
	leaq	-480(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$760, %rsp
	movw	%r9w, -786(%rbp)
	movq	%r8, -752(%rbp)
	movq	%rsi, -736(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rdx, -720(%rbp)
	movl	$5, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	leaq	-688(%rbp), %rax
	movq	%rbx, -744(%rbp)
	leaq	-608(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-352(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal17CodeStubAssembler36IsCustomElementsReceiverInstanceTypeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdx
	movq	%r14, %r8
	movq	%r15, %rcx
	pushq	-760(%rbp)
	movq	-728(%rbp), %r9
	movq	%r12, %rdi
	movq	-744(%rbp), %rsi
	pushq	%r13
	call	_ZN2v88internal17CodeStubAssembler9TryToNameEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %rbx
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%rax
	popq	%rdx
	jne	.L237
.L221:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	pushq	%r13
	movq	%r12, %rdi
	movq	-752(%rbp), %r9
	pushq	-736(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rax, %r8
	movq	-768(%rbp), %rdx
	movq	-720(%rbp), %rsi
	call	_ZN2v88internal26KeyedStoreGenericAssembler23EmitGenericElementStoreEPNS0_8compiler4NodeENS2_5TNodeINS0_3MapEEES4_NS5_INS0_7IntPtrTEEES4_S4_PNS2_18CodeAssemblerLabelE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-672(%rbp), %rsi
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%r11
	movq	%rsi, -784(%rbp)
	popq	%rax
	jne	.L238
.L223:
	movq	-728(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%rbx, %r8
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r12, -96(%rbp)
	movq	%rax, -656(%rbp)
	movq	-752(%rbp), %rax
	movq	-736(%rbp), %xmm0
	movq	%rsi, %xmm1
	movq	-784(%rbp), %rcx
	movq	$0, -648(%rbp)
	movq	%rax, -624(%rbp)
	movzwl	-786(%rbp), %eax
	movq	-768(%rbp), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	$0, -640(%rbp)
	pushq	%rax
	movq	%rsi, -632(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -672(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE
	movq	-72(%rbp), %rax
	popq	%r8
	popq	%r9
	testq	%rax, %rax
	je	.L225
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L225:
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	%r13
	movq	%r15, %rcx
	movq	%r14, %r8
	pushq	%r13
	movq	-728(%rbp), %r9
	movq	%r12, %rdi
	movq	-776(%rbp), %rdx
	movq	-744(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler20TryInternalizeStringEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	16(%r12), %edi
	popq	%rcx
	popq	%rsi
	testl	%edi, %edi
	jne	.L226
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L239
.L227:
	movq	-720(%rbp), %xmm0
	movl	$3, %esi
	movq	%r12, %rdi
	movhps	-744(%rbp), %xmm0
	movaps	%xmm0, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movl	$251, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movdqa	-720(%rbp), %xmm0
	movq	-752(%rbp), %rax
	movl	$3, %r9d
	movq	-736(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
.L229:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	-784(%rbp), %rsi
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -768(%rbp)
	movq	$22, -672(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movdqa	.LC6(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$30575, %edx
	movl	$1819500387, 16(%rax)
	movw	%dx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-672(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-768(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L227
	call	_ZdlPv@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	$18, -672(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rcx, -800(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movdqa	.LC5(%rip), %xmm0
	movl	$25965, %r10d
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movw	%r10w, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-672(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-800(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L223
	call	_ZdlPv@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	-80(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movb	$120, -68(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -800(%rbp)
	movabsq	$2338042651316874857, %rax
	movq	%rax, -80(%rbp)
	movl	$1701080681, -72(%rbp)
	movq	$13, -88(%rbp)
	movb	$0, -67(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-800(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L221
	call	_ZdlPv@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-720(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$3, %esi
	movhps	-744(%rbp), %xmm0
	movaps	%xmm0, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movl	$253, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movdqa	-720(%rbp), %xmm0
	movq	-752(%rbp), %rax
	movl	$3, %r9d
	movq	-736(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	jmp	.L229
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21989:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE, .-_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE
	.section	.text._ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE
	.type	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE, @function
_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE:
.LFB21958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	%ebx, %r9d
	movq	-88(%rbp), %r8
	movq	%r15, %rcx
	sall	$8, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	orl	$1, %r9d
	movl	$0, -64(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L244:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21958:
	.size	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE, .-_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE
	.section	.text._ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_
	.type	_ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_, @function
_ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_:
.LFB21959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$257, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, -64(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L248:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21959:
	.size	_ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_, .-_ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_
	.section	.text._ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE:
.LFB21952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L252:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21952:
	.size	_ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericEv
	.type	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericEv, @function
_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericEv:
.LFB21990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	xorl	%r9d, %r9d
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE
	.cfi_endproc
.LFE21990:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericEv, .-_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericEv
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS0_12LanguageModeE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS0_12LanguageModeE, @function
_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS0_12LanguageModeE:
.LFB21991:
	.cfi_startproc
	endbr64
	sall	$8, %r9d
	orl	$1, %r9d
	jmp	_ZN2v88internal26KeyedStoreGenericAssembler17KeyedStoreGenericENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS_5MaybeINS0_12LanguageModeEEE
	.cfi_endproc
.LFE21991:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS0_12LanguageModeE, .-_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_S7_NS0_12LanguageModeE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler18StoreIC_NoFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler18StoreIC_NoFeedbackEv
	.type	_ZN2v88internal26KeyedStoreGenericAssembler18StoreIC_NoFeedbackEv, @function
_ZN2v88internal26KeyedStoreGenericAssembler18StoreIC_NoFeedbackEv:
.LFB21992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler29IsSpecialReceiverInstanceTypeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r15, %xmm0
	movq	%r13, %r9
	movq	%rbx, %rsi
	movq	%rbx, %xmm1
	subq	$8, %rsp
	leaq	-96(%rbp), %r15
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm0
	pushq	$0
	movq	-464(%rbp), %r10
	movq	%r15, %r8
	movaps	%xmm0, -416(%rbp)
	leaq	-416(%rbp), %rcx
	movq	-424(%rbp), %xmm0
	movq	%rax, -384(%rbp)
	movq	-432(%rbp), %rax
	movq	%r10, %rdx
	movhps	-448(%rbp), %xmm0
	movq	%rbx, -376(%rbp)
	movq	%rax, -368(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE
	movq	-72(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L257
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L257:
	movq	-432(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movhps	-448(%rbp), %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm2
	movq	%r15, %r8
	movq	%r12, %rdi
	movdqa	-448(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$129, %esi
	movq	-424(%rbp), %rax
	movq	-456(%rbp), %rcx
	movl	$5, %r9d
	movaps	%xmm0, -96(%rbp)
	movq	-432(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L263:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21992:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler18StoreIC_NoFeedbackEv, .-_ZN2v88internal26KeyedStoreGenericAssembler18StoreIC_NoFeedbackEv
	.section	.text._ZN2v88internal26StoreICNoFeedbackGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26StoreICNoFeedbackGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal26StoreICNoFeedbackGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal26StoreICNoFeedbackGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE:
.LFB21956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$0, -32(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler18StoreIC_NoFeedbackEv
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L267:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21956:
	.size	_ZN2v88internal26StoreICNoFeedbackGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal26StoreICNoFeedbackGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_5BoolTEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_5BoolTEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS0_12LanguageModeE
	.type	_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_5BoolTEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS0_12LanguageModeE, @function
_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_5BoolTEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS0_12LanguageModeE:
.LFB21993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-384(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_10JSReceiverEEENS5_INS2_5BoolTEEENS5_INS2_4NameEEENS5_INS2_6ObjectEEENS2_12LanguageModeEEUlPNS4_4NodeEE_E10_M_managerERSt9_Any_dataRKSL_St18_Manager_operation(%rip), %rdx
	movq	%rbx, %xmm3
	movq	%rdx, %xmm0
	xorl	%edx, %edx
	punpcklqdq	%xmm3, %xmm1
	subq	$472, %rsp
	movl	16(%rbp), %eax
	movq	%rsi, -464(%rbp)
	movq	%rdi, %rsi
	movq	%r8, -456(%rbp)
	movq	%r13, %rdi
	movl	%eax, -468(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26KeyedStoreGenericAssembler11SetPropertyENS2_5TNodeINS1_7ContextEEENS7_INS1_10JSReceiverEEENS7_INS1_5BoolTEEENS7_INS1_4NameEEENS7_INS1_6ObjectEEENS1_12LanguageModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rcx, -504(%rbp)
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -432(%rbp)
	movl	$1, %r8d
	movaps	%xmm1, -448(%rbp)
	movq	%r9, -400(%rbp)
	movaps	%xmm0, -496(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rbx, -408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-128(%rbp), %r10
	movq	%r13, -128(%rbp)
	movdqa	-496(%rbp), %xmm0
	movq	%r12, -120(%rbp)
	movdqa	-128(%rbp), %xmm4
	movq	%r10, %rsi
	movq	%r10, %rdi
	movl	$3, %edx
	movq	%r10, -480(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm0, -72(%rbp)
	movups	%xmm4, -88(%rbp)
	movq	%r12, -96(%rbp)
	call	_ZNSt14_Function_base13_Base_managerIZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_10JSReceiverEEENS5_INS2_5BoolTEEENS5_INS2_4NameEEENS5_INS2_6ObjectEEENS2_12LanguageModeEEUlPNS4_4NodeEE_E10_M_managerERSt9_Any_dataRKSL_St18_Manager_operation
	movq	-504(%rbp), %r11
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movzwl	-468(%rbp), %eax
	movb	%al, %ch
	movl	%ecx, -496(%rbp)
	movl	%ecx, -468(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	subq	$8, %rsp
	leaq	-96(%rbp), %r8
	movq	%r14, %r9
	movl	-496(%rbp), %ecx
	movq	%rax, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-448(%rbp), %r11
	pushq	%rcx
	movq	%r11, %rcx
	call	_ZN2v88internal26KeyedStoreGenericAssembler24EmitGenericPropertyStoreENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_3MapEEEPKNS0_17AccessorAssembler17StoreICParametersEPNS0_9ExitPointEPNS2_18CodeAssemblerLabelENS_5MaybeINS0_12LanguageModeEEE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm0
	popq	%rax
	movq	%r15, -112(%rbp)
	movq	-480(%rbp), %r10
	movhps	-456(%rbp), %xmm0
	popq	%rdx
	movl	$3, %r8d
	cmpl	$1, 16(%r12)
	movq	-464(%rbp), %rdx
	movaps	%xmm0, -128(%rbp)
	movq	%r10, %rcx
	je	.L277
	movl	$251, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
.L270:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L271
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L271:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movl	$253, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	jmp	.L270
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21993:
	.size	_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_5BoolTEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS0_12LanguageModeE, .-_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_5BoolTEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS0_12LanguageModeE
	.section	.text._ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_10JSReceiverEEENS5_INS0_5BoolTEEENS5_INS0_4NameEEENS5_INS0_6ObjectEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_10JSReceiverEEENS5_INS0_5BoolTEEENS5_INS0_4NameEEENS5_INS0_6ObjectEEENS0_12LanguageModeE
	.type	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_10JSReceiverEEENS5_INS0_5BoolTEEENS5_INS0_4NameEEENS5_INS0_6ObjectEEENS0_12LanguageModeE, @function
_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_10JSReceiverEEENS5_INS0_5BoolTEEENS5_INS0_4NameEEENS5_INS0_6ObjectEEENS0_12LanguageModeE:
.LFB21957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movl	16(%rbp), %eax
	movq	%r9, -96(%rbp)
	movl	%eax, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	-84(%rbp), %eax
	subq	$8, %rsp
	movq	-96(%rbp), %r9
	movq	%r14, %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	pushq	%rax
	movq	%r12, %rdi
	movl	$0, -64(%rbp)
	call	_ZN2v88internal26KeyedStoreGenericAssembler11SetPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_5BoolTEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS0_12LanguageModeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L282:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21957:
	.size	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_10JSReceiverEEENS5_INS0_5BoolTEEENS5_INS0_4NameEEENS5_INS0_6ObjectEEENS0_12LanguageModeE, .-_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_10JSReceiverEEENS5_INS0_5BoolTEEENS5_INS0_4NameEEENS5_INS0_6ObjectEEENS0_12LanguageModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE:
.LFB28389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28389:
	.size	_GLOBAL__sub_I__ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	7161112774078067271
	.quad	8031170618519873899
	.align 16
.LC2:
	.quad	8030604370249998694
	.quad	8391086236738151792
	.align 16
.LC3:
	.quad	8367811757270331244
	.quad	8028075807021621618
	.align 16
.LC4:
	.quad	7020671367832103268
	.quad	7309464732520446322
	.align 16
.LC5:
	.quad	8439872597045896555
	.quad	7020584489251334510
	.align 16
.LC6:
	.quad	8031135726172726603
	.quad	7598247046147433842
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
