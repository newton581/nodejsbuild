	.file	"builtins-interpreter-gen.cc"
	.text
	.section	.text._ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE:
.LFB9264:
	.cfi_startproc
	endbr64
	movl	$2, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE@PLT
	.cfi_endproc
.LFE9264:
	.size	_ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins48Generate_InterpreterPushUndefinedAndArgsThenCallEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins48Generate_InterpreterPushUndefinedAndArgsThenCallEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins48Generate_InterpreterPushUndefinedAndArgsThenCallEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins48Generate_InterpreterPushUndefinedAndArgsThenCallEPNS0_14MacroAssemblerE:
.LFB9265:
	.cfi_startproc
	endbr64
	movl	$2, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE@PLT
	.cfi_endproc
.LFE9265:
	.size	_ZN2v88internal8Builtins48Generate_InterpreterPushUndefinedAndArgsThenCallEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins48Generate_InterpreterPushUndefinedAndArgsThenCallEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins51Generate_InterpreterPushArgsThenCallWithFinalSpreadEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins51Generate_InterpreterPushArgsThenCallWithFinalSpreadEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins51Generate_InterpreterPushArgsThenCallWithFinalSpreadEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins51Generate_InterpreterPushArgsThenCallWithFinalSpreadEPNS0_14MacroAssemblerE:
.LFB9266:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE@PLT
	.cfi_endproc
.LFE9266:
	.size	_ZN2v88internal8Builtins51Generate_InterpreterPushArgsThenCallWithFinalSpreadEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins51Generate_InterpreterPushArgsThenCallWithFinalSpreadEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins41Generate_InterpreterPushArgsThenConstructEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_InterpreterPushArgsThenConstructEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins41Generate_InterpreterPushArgsThenConstructEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins41Generate_InterpreterPushArgsThenConstructEPNS0_14MacroAssemblerE:
.LFB9267:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE@PLT
	.cfi_endproc
.LFE9267:
	.size	_ZN2v88internal8Builtins41Generate_InterpreterPushArgsThenConstructEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins41Generate_InterpreterPushArgsThenConstructEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins56Generate_InterpreterPushArgsThenConstructWithFinalSpreadEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins56Generate_InterpreterPushArgsThenConstructWithFinalSpreadEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins56Generate_InterpreterPushArgsThenConstructWithFinalSpreadEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins56Generate_InterpreterPushArgsThenConstructWithFinalSpreadEPNS0_14MacroAssemblerE:
.LFB9268:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE@PLT
	.cfi_endproc
.LFE9268:
	.size	_ZN2v88internal8Builtins56Generate_InterpreterPushArgsThenConstructWithFinalSpreadEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins56Generate_InterpreterPushArgsThenConstructWithFinalSpreadEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins54Generate_InterpreterPushArgsThenConstructArrayFunctionEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins54Generate_InterpreterPushArgsThenConstructArrayFunctionEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins54Generate_InterpreterPushArgsThenConstructArrayFunctionEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins54Generate_InterpreterPushArgsThenConstructArrayFunctionEPNS0_14MacroAssemblerE:
.LFB9269:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE@PLT
	.cfi_endproc
.LFE9269:
	.size	_ZN2v88internal8Builtins54Generate_InterpreterPushArgsThenConstructArrayFunctionEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins54Generate_InterpreterPushArgsThenConstructArrayFunctionEPNS0_14MacroAssemblerE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE:
.LFB10520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10520:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_InterpreterPushArgsThenCallEPNS0_14MacroAssemblerE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
