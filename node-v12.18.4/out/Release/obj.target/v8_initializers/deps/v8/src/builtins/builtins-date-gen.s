	.file	"builtins-date-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.type	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i, @function
_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i:
.LFB13299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	xorl	%ecx, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	$1066, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	testl	%r15d, %r15d
	je	.L13
	cmpl	$7, %r15d
	jle	.L14
.L10:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%rax, %r15
	call	_ZN2v88internal17ExternalReference23get_date_field_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	leaq	-96(%rbp), %rcx
	movq	%r13, -88(%rbp)
	movl	$1800, %edx
	movq	%rax, %rsi
	movl	$2, %r8d
	movq	%r15, -72(%rbp)
	movw	%dx, -80(%rbp)
	movl	$1800, %eax
	movl	$1800, %edx
	movw	%ax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
.L9:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$91, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	leaq	-224(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r12, %rsi
	movq	%r9, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference16date_cache_stampEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r12, %rdi
	movl	$2, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$88, %edx
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-368(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leal	24(,%r15,8), %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-360(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-360(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L9
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13299:
	.size	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i, .-_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.section	.rodata._ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-date-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"DatePrototypeGetDate"
	.section	.text._ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE:
.LFB13339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$68, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$287, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L20
.L17:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L17
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13339:
	.size	_ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_DatePrototypeGetDateEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29DatePrototypeGetDateAssembler32GenerateDatePrototypeGetDateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29DatePrototypeGetDateAssembler32GenerateDatePrototypeGetDateImplEv
	.type	_ZN2v88internal29DatePrototypeGetDateAssembler32GenerateDatePrototypeGetDateImplEv, @function
_ZN2v88internal29DatePrototypeGetDateAssembler32GenerateDatePrototypeGetDateImplEv:
.LFB13343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$3, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13343:
	.size	_ZN2v88internal29DatePrototypeGetDateAssembler32GenerateDatePrototypeGetDateImplEv, .-_ZN2v88internal29DatePrototypeGetDateAssembler32GenerateDatePrototypeGetDateImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_DatePrototypeGetDayEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"DatePrototypeGetDay"
	.section	.text._ZN2v88internal8Builtins28Generate_DatePrototypeGetDayEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_DatePrototypeGetDayEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_DatePrototypeGetDayEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_DatePrototypeGetDayEPNS0_8compiler18CodeAssemblerStateE:
.LFB13348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$74, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$288, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L28
.L25:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L25
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13348:
	.size	_ZN2v88internal8Builtins28Generate_DatePrototypeGetDayEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_DatePrototypeGetDayEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28DatePrototypeGetDayAssembler31GenerateDatePrototypeGetDayImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28DatePrototypeGetDayAssembler31GenerateDatePrototypeGetDayImplEv
	.type	_ZN2v88internal28DatePrototypeGetDayAssembler31GenerateDatePrototypeGetDayImplEv, @function
_ZN2v88internal28DatePrototypeGetDayAssembler31GenerateDatePrototypeGetDayImplEv:
.LFB13352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13352:
	.size	_ZN2v88internal28DatePrototypeGetDayAssembler31GenerateDatePrototypeGetDayImplEv, .-_ZN2v88internal28DatePrototypeGetDayAssembler31GenerateDatePrototypeGetDayImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_DatePrototypeGetFullYearEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"DatePrototypeGetFullYear"
	.section	.text._ZN2v88internal8Builtins33Generate_DatePrototypeGetFullYearEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_DatePrototypeGetFullYearEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_DatePrototypeGetFullYearEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_DatePrototypeGetFullYearEPNS0_8compiler18CodeAssemblerStateE:
.LFB13357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$80, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$289, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L36
.L33:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L33
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13357:
	.size	_ZN2v88internal8Builtins33Generate_DatePrototypeGetFullYearEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_DatePrototypeGetFullYearEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33DatePrototypeGetFullYearAssembler36GenerateDatePrototypeGetFullYearImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33DatePrototypeGetFullYearAssembler36GenerateDatePrototypeGetFullYearImplEv
	.type	_ZN2v88internal33DatePrototypeGetFullYearAssembler36GenerateDatePrototypeGetFullYearImplEv, @function
_ZN2v88internal33DatePrototypeGetFullYearAssembler36GenerateDatePrototypeGetFullYearImplEv:
.LFB13361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13361:
	.size	_ZN2v88internal33DatePrototypeGetFullYearAssembler36GenerateDatePrototypeGetFullYearImplEv, .-_ZN2v88internal33DatePrototypeGetFullYearAssembler36GenerateDatePrototypeGetFullYearImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_DatePrototypeGetHoursEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"DatePrototypeGetHours"
	.section	.text._ZN2v88internal8Builtins30Generate_DatePrototypeGetHoursEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_DatePrototypeGetHoursEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_DatePrototypeGetHoursEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_DatePrototypeGetHoursEPNS0_8compiler18CodeAssemblerStateE:
.LFB13366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$86, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$290, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L44
.L41:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L41
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13366:
	.size	_ZN2v88internal8Builtins30Generate_DatePrototypeGetHoursEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_DatePrototypeGetHoursEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30DatePrototypeGetHoursAssembler33GenerateDatePrototypeGetHoursImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30DatePrototypeGetHoursAssembler33GenerateDatePrototypeGetHoursImplEv
	.type	_ZN2v88internal30DatePrototypeGetHoursAssembler33GenerateDatePrototypeGetHoursImplEv, @function
_ZN2v88internal30DatePrototypeGetHoursAssembler33GenerateDatePrototypeGetHoursImplEv:
.LFB13370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$5, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13370:
	.size	_ZN2v88internal30DatePrototypeGetHoursAssembler33GenerateDatePrototypeGetHoursImplEv, .-_ZN2v88internal30DatePrototypeGetHoursAssembler33GenerateDatePrototypeGetHoursImplEv
	.section	.rodata._ZN2v88internal8Builtins37Generate_DatePrototypeGetMillisecondsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"DatePrototypeGetMilliseconds"
	.section	.text._ZN2v88internal8Builtins37Generate_DatePrototypeGetMillisecondsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_DatePrototypeGetMillisecondsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_DatePrototypeGetMillisecondsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_DatePrototypeGetMillisecondsEPNS0_8compiler18CodeAssemblerStateE:
.LFB13375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$92, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$291, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L52
.L49:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L49
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13375:
	.size	_ZN2v88internal8Builtins37Generate_DatePrototypeGetMillisecondsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_DatePrototypeGetMillisecondsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal37DatePrototypeGetMillisecondsAssembler40GenerateDatePrototypeGetMillisecondsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37DatePrototypeGetMillisecondsAssembler40GenerateDatePrototypeGetMillisecondsImplEv
	.type	_ZN2v88internal37DatePrototypeGetMillisecondsAssembler40GenerateDatePrototypeGetMillisecondsImplEv, @function
_ZN2v88internal37DatePrototypeGetMillisecondsAssembler40GenerateDatePrototypeGetMillisecondsImplEv:
.LFB13379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13379:
	.size	_ZN2v88internal37DatePrototypeGetMillisecondsAssembler40GenerateDatePrototypeGetMillisecondsImplEv, .-_ZN2v88internal37DatePrototypeGetMillisecondsAssembler40GenerateDatePrototypeGetMillisecondsImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_DatePrototypeGetMinutesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"DatePrototypeGetMinutes"
	.section	.text._ZN2v88internal8Builtins32Generate_DatePrototypeGetMinutesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_DatePrototypeGetMinutesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_DatePrototypeGetMinutesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_DatePrototypeGetMinutesEPNS0_8compiler18CodeAssemblerStateE:
.LFB13384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$98, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$292, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L60
.L57:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L57
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13384:
	.size	_ZN2v88internal8Builtins32Generate_DatePrototypeGetMinutesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_DatePrototypeGetMinutesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32DatePrototypeGetMinutesAssembler35GenerateDatePrototypeGetMinutesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32DatePrototypeGetMinutesAssembler35GenerateDatePrototypeGetMinutesImplEv
	.type	_ZN2v88internal32DatePrototypeGetMinutesAssembler35GenerateDatePrototypeGetMinutesImplEv, @function
_ZN2v88internal32DatePrototypeGetMinutesAssembler35GenerateDatePrototypeGetMinutesImplEv:
.LFB13388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$6, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13388:
	.size	_ZN2v88internal32DatePrototypeGetMinutesAssembler35GenerateDatePrototypeGetMinutesImplEv, .-_ZN2v88internal32DatePrototypeGetMinutesAssembler35GenerateDatePrototypeGetMinutesImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_DatePrototypeGetMonthEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"DatePrototypeGetMonth"
	.section	.text._ZN2v88internal8Builtins30Generate_DatePrototypeGetMonthEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_DatePrototypeGetMonthEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_DatePrototypeGetMonthEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_DatePrototypeGetMonthEPNS0_8compiler18CodeAssemblerStateE:
.LFB13393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$104, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$293, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L68
.L65:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L65
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13393:
	.size	_ZN2v88internal8Builtins30Generate_DatePrototypeGetMonthEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_DatePrototypeGetMonthEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30DatePrototypeGetMonthAssembler33GenerateDatePrototypeGetMonthImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30DatePrototypeGetMonthAssembler33GenerateDatePrototypeGetMonthImplEv
	.type	_ZN2v88internal30DatePrototypeGetMonthAssembler33GenerateDatePrototypeGetMonthImplEv, @function
_ZN2v88internal30DatePrototypeGetMonthAssembler33GenerateDatePrototypeGetMonthImplEv:
.LFB13397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13397:
	.size	_ZN2v88internal30DatePrototypeGetMonthAssembler33GenerateDatePrototypeGetMonthImplEv, .-_ZN2v88internal30DatePrototypeGetMonthAssembler33GenerateDatePrototypeGetMonthImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_DatePrototypeGetSecondsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"DatePrototypeGetSeconds"
	.section	.text._ZN2v88internal8Builtins32Generate_DatePrototypeGetSecondsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_DatePrototypeGetSecondsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_DatePrototypeGetSecondsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_DatePrototypeGetSecondsEPNS0_8compiler18CodeAssemblerStateE:
.LFB13402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$110, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$294, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L76
.L73:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$7, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L73
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13402:
	.size	_ZN2v88internal8Builtins32Generate_DatePrototypeGetSecondsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_DatePrototypeGetSecondsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32DatePrototypeGetSecondsAssembler35GenerateDatePrototypeGetSecondsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32DatePrototypeGetSecondsAssembler35GenerateDatePrototypeGetSecondsImplEv
	.type	_ZN2v88internal32DatePrototypeGetSecondsAssembler35GenerateDatePrototypeGetSecondsImplEv, @function
_ZN2v88internal32DatePrototypeGetSecondsAssembler35GenerateDatePrototypeGetSecondsImplEv:
.LFB13406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$7, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13406:
	.size	_ZN2v88internal32DatePrototypeGetSecondsAssembler35GenerateDatePrototypeGetSecondsImplEv, .-_ZN2v88internal32DatePrototypeGetSecondsAssembler35GenerateDatePrototypeGetSecondsImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_DatePrototypeGetTimeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"DatePrototypeGetTime"
	.section	.text._ZN2v88internal8Builtins29Generate_DatePrototypeGetTimeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_DatePrototypeGetTimeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_DatePrototypeGetTimeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_DatePrototypeGetTimeEPNS0_8compiler18CodeAssemblerStateE:
.LFB13411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$116, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$295, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L84
.L81:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L81
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13411:
	.size	_ZN2v88internal8Builtins29Generate_DatePrototypeGetTimeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_DatePrototypeGetTimeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29DatePrototypeGetTimeAssembler32GenerateDatePrototypeGetTimeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29DatePrototypeGetTimeAssembler32GenerateDatePrototypeGetTimeImplEv
	.type	_ZN2v88internal29DatePrototypeGetTimeAssembler32GenerateDatePrototypeGetTimeImplEv, @function
_ZN2v88internal29DatePrototypeGetTimeAssembler32GenerateDatePrototypeGetTimeImplEv:
.LFB13415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13415:
	.size	_ZN2v88internal29DatePrototypeGetTimeAssembler32GenerateDatePrototypeGetTimeImplEv, .-_ZN2v88internal29DatePrototypeGetTimeAssembler32GenerateDatePrototypeGetTimeImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_DatePrototypeGetTimezoneOffsetEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"DatePrototypeGetTimezoneOffset"
	.section	.text._ZN2v88internal8Builtins39Generate_DatePrototypeGetTimezoneOffsetEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_DatePrototypeGetTimezoneOffsetEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_DatePrototypeGetTimezoneOffsetEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_DatePrototypeGetTimezoneOffsetEPNS0_8compiler18CodeAssemblerStateE:
.LFB13420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$122, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$296, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L92
.L89:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$21, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L89
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13420:
	.size	_ZN2v88internal8Builtins39Generate_DatePrototypeGetTimezoneOffsetEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_DatePrototypeGetTimezoneOffsetEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal39DatePrototypeGetTimezoneOffsetAssembler42GenerateDatePrototypeGetTimezoneOffsetImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39DatePrototypeGetTimezoneOffsetAssembler42GenerateDatePrototypeGetTimezoneOffsetImplEv
	.type	_ZN2v88internal39DatePrototypeGetTimezoneOffsetAssembler42GenerateDatePrototypeGetTimezoneOffsetImplEv, @function
_ZN2v88internal39DatePrototypeGetTimezoneOffsetAssembler42GenerateDatePrototypeGetTimezoneOffsetImplEv:
.LFB13424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$21, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13424:
	.size	_ZN2v88internal39DatePrototypeGetTimezoneOffsetAssembler42GenerateDatePrototypeGetTimezoneOffsetImplEv, .-_ZN2v88internal39DatePrototypeGetTimezoneOffsetAssembler42GenerateDatePrototypeGetTimezoneOffsetImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_DatePrototypeGetUTCDateEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"DatePrototypeGetUTCDate"
	.section	.text._ZN2v88internal8Builtins32Generate_DatePrototypeGetUTCDateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_DatePrototypeGetUTCDateEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_DatePrototypeGetUTCDateEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_DatePrototypeGetUTCDateEPNS0_8compiler18CodeAssemblerStateE:
.LFB13429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$128, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$297, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L100
.L97:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$13, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L97
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13429:
	.size	_ZN2v88internal8Builtins32Generate_DatePrototypeGetUTCDateEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_DatePrototypeGetUTCDateEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32DatePrototypeGetUTCDateAssembler35GenerateDatePrototypeGetUTCDateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32DatePrototypeGetUTCDateAssembler35GenerateDatePrototypeGetUTCDateImplEv
	.type	_ZN2v88internal32DatePrototypeGetUTCDateAssembler35GenerateDatePrototypeGetUTCDateImplEv, @function
_ZN2v88internal32DatePrototypeGetUTCDateAssembler35GenerateDatePrototypeGetUTCDateImplEv:
.LFB13433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$13, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13433:
	.size	_ZN2v88internal32DatePrototypeGetUTCDateAssembler35GenerateDatePrototypeGetUTCDateImplEv, .-_ZN2v88internal32DatePrototypeGetUTCDateAssembler35GenerateDatePrototypeGetUTCDateImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_DatePrototypeGetUTCDayEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"DatePrototypeGetUTCDay"
	.section	.text._ZN2v88internal8Builtins31Generate_DatePrototypeGetUTCDayEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_DatePrototypeGetUTCDayEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_DatePrototypeGetUTCDayEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_DatePrototypeGetUTCDayEPNS0_8compiler18CodeAssemblerStateE:
.LFB13438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$134, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$298, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L108
.L105:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$14, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L105
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13438:
	.size	_ZN2v88internal8Builtins31Generate_DatePrototypeGetUTCDayEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_DatePrototypeGetUTCDayEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31DatePrototypeGetUTCDayAssembler34GenerateDatePrototypeGetUTCDayImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31DatePrototypeGetUTCDayAssembler34GenerateDatePrototypeGetUTCDayImplEv
	.type	_ZN2v88internal31DatePrototypeGetUTCDayAssembler34GenerateDatePrototypeGetUTCDayImplEv, @function
_ZN2v88internal31DatePrototypeGetUTCDayAssembler34GenerateDatePrototypeGetUTCDayImplEv:
.LFB13442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$14, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13442:
	.size	_ZN2v88internal31DatePrototypeGetUTCDayAssembler34GenerateDatePrototypeGetUTCDayImplEv, .-_ZN2v88internal31DatePrototypeGetUTCDayAssembler34GenerateDatePrototypeGetUTCDayImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_DatePrototypeGetUTCFullYearEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"DatePrototypeGetUTCFullYear"
	.section	.text._ZN2v88internal8Builtins36Generate_DatePrototypeGetUTCFullYearEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_DatePrototypeGetUTCFullYearEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_DatePrototypeGetUTCFullYearEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_DatePrototypeGetUTCFullYearEPNS0_8compiler18CodeAssemblerStateE:
.LFB13447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$140, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$299, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L116
.L113:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$11, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L113
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13447:
	.size	_ZN2v88internal8Builtins36Generate_DatePrototypeGetUTCFullYearEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_DatePrototypeGetUTCFullYearEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36DatePrototypeGetUTCFullYearAssembler39GenerateDatePrototypeGetUTCFullYearImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36DatePrototypeGetUTCFullYearAssembler39GenerateDatePrototypeGetUTCFullYearImplEv
	.type	_ZN2v88internal36DatePrototypeGetUTCFullYearAssembler39GenerateDatePrototypeGetUTCFullYearImplEv, @function
_ZN2v88internal36DatePrototypeGetUTCFullYearAssembler39GenerateDatePrototypeGetUTCFullYearImplEv:
.LFB13451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$11, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13451:
	.size	_ZN2v88internal36DatePrototypeGetUTCFullYearAssembler39GenerateDatePrototypeGetUTCFullYearImplEv, .-_ZN2v88internal36DatePrototypeGetUTCFullYearAssembler39GenerateDatePrototypeGetUTCFullYearImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCHoursEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"DatePrototypeGetUTCHours"
	.section	.text._ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCHoursEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCHoursEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCHoursEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCHoursEPNS0_8compiler18CodeAssemblerStateE:
.LFB13456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$146, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$300, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L124
.L121:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$15, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L121
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13456:
	.size	_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCHoursEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCHoursEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33DatePrototypeGetUTCHoursAssembler36GenerateDatePrototypeGetUTCHoursImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33DatePrototypeGetUTCHoursAssembler36GenerateDatePrototypeGetUTCHoursImplEv
	.type	_ZN2v88internal33DatePrototypeGetUTCHoursAssembler36GenerateDatePrototypeGetUTCHoursImplEv, @function
_ZN2v88internal33DatePrototypeGetUTCHoursAssembler36GenerateDatePrototypeGetUTCHoursImplEv:
.LFB13460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$15, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13460:
	.size	_ZN2v88internal33DatePrototypeGetUTCHoursAssembler36GenerateDatePrototypeGetUTCHoursImplEv, .-_ZN2v88internal33DatePrototypeGetUTCHoursAssembler36GenerateDatePrototypeGetUTCHoursImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_DatePrototypeGetUTCMillisecondsEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"DatePrototypeGetUTCMilliseconds"
	.section	.text._ZN2v88internal8Builtins40Generate_DatePrototypeGetUTCMillisecondsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_DatePrototypeGetUTCMillisecondsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_DatePrototypeGetUTCMillisecondsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_DatePrototypeGetUTCMillisecondsEPNS0_8compiler18CodeAssemblerStateE:
.LFB13465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$152, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$301, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L132
.L129:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$18, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L129
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13465:
	.size	_ZN2v88internal8Builtins40Generate_DatePrototypeGetUTCMillisecondsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_DatePrototypeGetUTCMillisecondsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40DatePrototypeGetUTCMillisecondsAssembler43GenerateDatePrototypeGetUTCMillisecondsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40DatePrototypeGetUTCMillisecondsAssembler43GenerateDatePrototypeGetUTCMillisecondsImplEv
	.type	_ZN2v88internal40DatePrototypeGetUTCMillisecondsAssembler43GenerateDatePrototypeGetUTCMillisecondsImplEv, @function
_ZN2v88internal40DatePrototypeGetUTCMillisecondsAssembler43GenerateDatePrototypeGetUTCMillisecondsImplEv:
.LFB13469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$18, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13469:
	.size	_ZN2v88internal40DatePrototypeGetUTCMillisecondsAssembler43GenerateDatePrototypeGetUTCMillisecondsImplEv, .-_ZN2v88internal40DatePrototypeGetUTCMillisecondsAssembler43GenerateDatePrototypeGetUTCMillisecondsImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCMinutesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"DatePrototypeGetUTCMinutes"
	.section	.text._ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCMinutesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCMinutesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCMinutesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCMinutesEPNS0_8compiler18CodeAssemblerStateE:
.LFB13474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$158, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$302, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L140
.L137:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$16, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L137
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13474:
	.size	_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCMinutesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCMinutesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35DatePrototypeGetUTCMinutesAssembler38GenerateDatePrototypeGetUTCMinutesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35DatePrototypeGetUTCMinutesAssembler38GenerateDatePrototypeGetUTCMinutesImplEv
	.type	_ZN2v88internal35DatePrototypeGetUTCMinutesAssembler38GenerateDatePrototypeGetUTCMinutesImplEv, @function
_ZN2v88internal35DatePrototypeGetUTCMinutesAssembler38GenerateDatePrototypeGetUTCMinutesImplEv:
.LFB13478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13478:
	.size	_ZN2v88internal35DatePrototypeGetUTCMinutesAssembler38GenerateDatePrototypeGetUTCMinutesImplEv, .-_ZN2v88internal35DatePrototypeGetUTCMinutesAssembler38GenerateDatePrototypeGetUTCMinutesImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCMonthEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"DatePrototypeGetUTCMonth"
	.section	.text._ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCMonthEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCMonthEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCMonthEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCMonthEPNS0_8compiler18CodeAssemblerStateE:
.LFB13483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$164, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$303, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L148
.L145:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$12, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L145
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13483:
	.size	_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCMonthEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_DatePrototypeGetUTCMonthEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33DatePrototypeGetUTCMonthAssembler36GenerateDatePrototypeGetUTCMonthImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33DatePrototypeGetUTCMonthAssembler36GenerateDatePrototypeGetUTCMonthImplEv
	.type	_ZN2v88internal33DatePrototypeGetUTCMonthAssembler36GenerateDatePrototypeGetUTCMonthImplEv, @function
_ZN2v88internal33DatePrototypeGetUTCMonthAssembler36GenerateDatePrototypeGetUTCMonthImplEv:
.LFB13487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$12, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13487:
	.size	_ZN2v88internal33DatePrototypeGetUTCMonthAssembler36GenerateDatePrototypeGetUTCMonthImplEv, .-_ZN2v88internal33DatePrototypeGetUTCMonthAssembler36GenerateDatePrototypeGetUTCMonthImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCSecondsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"DatePrototypeGetUTCSeconds"
	.section	.text._ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCSecondsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCSecondsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCSecondsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCSecondsEPNS0_8compiler18CodeAssemblerStateE:
.LFB13492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$170, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$304, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L156
.L153:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$17, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L153
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13492:
	.size	_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCSecondsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_DatePrototypeGetUTCSecondsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35DatePrototypeGetUTCSecondsAssembler38GenerateDatePrototypeGetUTCSecondsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35DatePrototypeGetUTCSecondsAssembler38GenerateDatePrototypeGetUTCSecondsImplEv
	.type	_ZN2v88internal35DatePrototypeGetUTCSecondsAssembler38GenerateDatePrototypeGetUTCSecondsImplEv, @function
_ZN2v88internal35DatePrototypeGetUTCSecondsAssembler38GenerateDatePrototypeGetUTCSecondsImplEv:
.LFB13496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$17, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13496:
	.size	_ZN2v88internal35DatePrototypeGetUTCSecondsAssembler38GenerateDatePrototypeGetUTCSecondsImplEv, .-_ZN2v88internal35DatePrototypeGetUTCSecondsAssembler38GenerateDatePrototypeGetUTCSecondsImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_DatePrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"DatePrototypeValueOf"
	.section	.text._ZN2v88internal8Builtins29Generate_DatePrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_DatePrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_DatePrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_DatePrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB13501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$176, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$305, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L164
.L161:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L161
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13501:
	.size	_ZN2v88internal8Builtins29Generate_DatePrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_DatePrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29DatePrototypeValueOfAssembler32GenerateDatePrototypeValueOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29DatePrototypeValueOfAssembler32GenerateDatePrototypeValueOfImplEv
	.type	_ZN2v88internal29DatePrototypeValueOfAssembler32GenerateDatePrototypeValueOfImplEv, @function
_ZN2v88internal29DatePrototypeValueOfAssembler32GenerateDatePrototypeValueOfImplEv:
.LFB13505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.cfi_endproc
.LFE13505:
	.size	_ZN2v88internal29DatePrototypeValueOfAssembler32GenerateDatePrototypeValueOfImplEv, .-_ZN2v88internal29DatePrototypeValueOfAssembler32GenerateDatePrototypeValueOfImplEv
	.section	.rodata._ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"Date.prototype [ @@toPrimitive ]"
	.section	.text._ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv
	.type	_ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv, @function
_ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv:
.LFB13514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$680, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-592(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -656(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-656(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-656(%rbp), %r10
	leaq	-464(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20numberStringConstantEv@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-656(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21DefaultStringConstantEv@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20StringStringConstantEv@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$49, %edx
	movq	%rax, %rsi
	leaq	-624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %r9
	movl	$2, %edi
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rbx, %xmm1
	movq	%rax, %r8
	leaq	-640(%rbp), %rbx
	pushq	%r9
	movq	-608(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-648(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%r9, -672(%rbp)
	movq	%r14, %r9
	movq	%rcx, -640(%rbp)
	movl	$1, %ecx
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -632(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-704(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-656(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-664(%rbp), %rdi
	movl	$49, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r14, %r9
	movq	%rbx, %rdx
	movq	-672(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	-648(%rbp), %xmm0
	movq	-608(%rbp), %rax
	movq	%rcx, -640(%rbp)
	movq	%r12, %rdi
	pushq	%rsi
	movl	$1, %ecx
	xorl	%esi, %esi
	movhps	-688(%rbp), %xmm0
	movq	%rax, -632(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-704(%rbp), %r11
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-664(%rbp), %rdi
	movl	$49, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r14, %r9
	movq	%rbx, %rdx
	movq	-672(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	-648(%rbp), %xmm0
	movq	-608(%rbp), %rax
	movq	%rcx, -640(%rbp)
	movq	%r12, %rdi
	pushq	%rsi
	movl	$1, %ecx
	xorl	%esi, %esi
	movhps	-696(%rbp), %xmm0
	movq	%rax, -632(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-688(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-664(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-672(%rbp), %rsi
	movq	%r14, %r9
	movl	$1, %edi
	pushq	%rdi
	movq	-680(%rbp), %r11
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	movq	-608(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movq	%r11, -80(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-664(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-672(%rbp), %rsi
	movq	%r14, %r9
	movl	$1, %edi
	pushq	%rdi
	movq	-680(%rbp), %r11
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	movq	-608(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movq	%r11, -80(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$196, %edx
	movq	-648(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	-712(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -648(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	xorl	%r9d, %r9d
	movl	$62, %edx
	movq	%r14, %rsi
	movq	-680(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-648(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L171:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13514:
	.size	_ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv, .-_ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_DatePrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"DatePrototypeToPrimitive"
	.section	.text._ZN2v88internal8Builtins33Generate_DatePrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_DatePrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_DatePrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_DatePrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE:
.LFB13510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$182, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$306, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L176
.L173:
	movq	%r13, %rdi
	call	_ZN2v88internal33DatePrototypeToPrimitiveAssembler36GenerateDatePrototypeToPrimitiveImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L173
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13510:
	.size	_ZN2v88internal8Builtins33Generate_DatePrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_DatePrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i, @function
_GLOBAL__sub_I__ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i:
.LFB17248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17248:
	.size	_GLOBAL__sub_I__ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i, .-_GLOBAL__sub_I__ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21DateBuiltinsAssembler31Generate_DatePrototype_GetFieldEPNS0_8compiler4NodeES4_i
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
