	.file	"builtins-conversion-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26640:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE26640:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation:
.LFB26641:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L5
	cmpl	$3, %edx
	je	.L6
	cmpl	$1, %edx
	je	.L10
.L6:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26641:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation:
.LFB26645:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L12
	cmpl	$3, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L17
.L13:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26645:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB26644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler18NonNumberToNumericENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26644:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	.type	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE, @function
_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE:
.LFB21880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -532(%rbp)
	movq	%rdx, -520(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	$2, %ebx
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3920(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -528(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %r10
	pushq	%rbx
	movq	%r15, %r9
	pushq	%r10
	movq	%rax, %r8
	movq	-208(%rbp), %rax
	xorl	%esi, %esi
	movq	-520(%rbp), %xmm0
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	leaq	-480(%rbp), %rbx
	movq	%r10, -560(%rbp)
	movhps	-528(%rbp), %xmm0
	movq	%r11, -352(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17IsNullOrUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1, %edx
	leaq	-512(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movl	-532(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	movq	%r15, %r9
	xorl	%esi, %esi
	movq	-528(%rbp), %xmm0
	pushq	%rdi
	movq	%rax, %r8
	movq	%r14, %rdx
	movq	-560(%rbp), %r10
	movq	-496(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	movhps	-552(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	pushq	%r10
	movaps	%xmm0, -96(%rbp)
	movq	-520(%rbp), %xmm0
	movq	%r11, -224(%rbp)
	movhps	-544(%rbp), %xmm0
	movq	%rax, -216(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r10, -544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-528(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-528(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23IsPrimitiveInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-528(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$30, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	cmpl	$2, -532(%rbp)
	movq	%r14, %rdi
	sete	%dl
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	-208(%rbp), %rax
	movq	-544(%rbp), %r10
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movl	$1, %r9d
	movq	%r11, -352(%rbp)
	movq	%rax, -344(%rbp)
	movq	-520(%rbp), %rax
	movq	%r10, %r8
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21880:
	.size	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE, .-_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	.section	.rodata._ZN2v88internal8Builtins40Generate_NonPrimitiveToPrimitive_DefaultEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-conversion-gen.cc"
	.align 8
.LC2:
	.string	"NonPrimitiveToPrimitive_Default"
	.section	.text._ZN2v88internal8Builtins40Generate_NonPrimitiveToPrimitive_DefaultEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_NonPrimitiveToPrimitive_DefaultEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_NonPrimitiveToPrimitive_DefaultEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_NonPrimitiveToPrimitive_DefaultEPNS0_8compiler18CodeAssemblerStateE:
.LFB21894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$80, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$95, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L32
.L29:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L29
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21894:
	.size	_ZN2v88internal8Builtins40Generate_NonPrimitiveToPrimitive_DefaultEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_NonPrimitiveToPrimitive_DefaultEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40NonPrimitiveToPrimitive_DefaultAssembler43GenerateNonPrimitiveToPrimitive_DefaultImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40NonPrimitiveToPrimitive_DefaultAssembler43GenerateNonPrimitiveToPrimitive_DefaultImplEv
	.type	_ZN2v88internal40NonPrimitiveToPrimitive_DefaultAssembler43GenerateNonPrimitiveToPrimitive_DefaultImplEv, @function
_ZN2v88internal40NonPrimitiveToPrimitive_DefaultAssembler43GenerateNonPrimitiveToPrimitive_DefaultImplEv:
.LFB21898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	.cfi_endproc
.LFE21898:
	.size	_ZN2v88internal40NonPrimitiveToPrimitive_DefaultAssembler43GenerateNonPrimitiveToPrimitive_DefaultImplEv, .-_ZN2v88internal40NonPrimitiveToPrimitive_DefaultAssembler43GenerateNonPrimitiveToPrimitive_DefaultImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"NonPrimitiveToPrimitive_Number"
	.section	.text._ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB21903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$87, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$96, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L40
.L37:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L37
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21903:
	.size	_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal39NonPrimitiveToPrimitive_NumberAssembler42GenerateNonPrimitiveToPrimitive_NumberImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39NonPrimitiveToPrimitive_NumberAssembler42GenerateNonPrimitiveToPrimitive_NumberImplEv
	.type	_ZN2v88internal39NonPrimitiveToPrimitive_NumberAssembler42GenerateNonPrimitiveToPrimitive_NumberImplEv, @function
_ZN2v88internal39NonPrimitiveToPrimitive_NumberAssembler42GenerateNonPrimitiveToPrimitive_NumberImplEv:
.LFB21907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	.cfi_endproc
.LFE21907:
	.size	_ZN2v88internal39NonPrimitiveToPrimitive_NumberAssembler42GenerateNonPrimitiveToPrimitive_NumberImplEv, .-_ZN2v88internal39NonPrimitiveToPrimitive_NumberAssembler42GenerateNonPrimitiveToPrimitive_NumberImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"NonPrimitiveToPrimitive_String"
	.section	.text._ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE:
.LFB21912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$94, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$97, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L48
.L45:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L45
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21912:
	.size	_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_NonPrimitiveToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal39NonPrimitiveToPrimitive_StringAssembler42GenerateNonPrimitiveToPrimitive_StringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39NonPrimitiveToPrimitive_StringAssembler42GenerateNonPrimitiveToPrimitive_StringImplEv
	.type	_ZN2v88internal39NonPrimitiveToPrimitive_StringAssembler42GenerateNonPrimitiveToPrimitive_StringImplEv, @function
_ZN2v88internal39NonPrimitiveToPrimitive_StringAssembler42GenerateNonPrimitiveToPrimitive_StringImplEv:
.LFB21916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	.cfi_endproc
.LFE21916:
	.size	_ZN2v88internal39NonPrimitiveToPrimitive_StringAssembler42GenerateNonPrimitiveToPrimitive_StringImplEv, .-_ZN2v88internal39NonPrimitiveToPrimitive_StringAssembler42GenerateNonPrimitiveToPrimitive_StringImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_StringToNumberEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"StringToNumber"
	.section	.text._ZN2v88internal8Builtins23Generate_StringToNumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_StringToNumberEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_StringToNumberEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_StringToNumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB21921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$101, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$98, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L56
.L53:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14StringToNumberENS0_8compiler5TNodeINS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L53
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21921:
	.size	_ZN2v88internal8Builtins23Generate_StringToNumberEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_StringToNumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23StringToNumberAssembler26GenerateStringToNumberImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringToNumberAssembler26GenerateStringToNumberImplEv
	.type	_ZN2v88internal23StringToNumberAssembler26GenerateStringToNumberImplEv, @function
_ZN2v88internal23StringToNumberAssembler26GenerateStringToNumberImplEv:
.LFB21925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14StringToNumberENS0_8compiler5TNodeINS0_6StringEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21925:
	.size	_ZN2v88internal23StringToNumberAssembler26GenerateStringToNumberImplEv, .-_ZN2v88internal23StringToNumberAssembler26GenerateStringToNumberImplEv
	.section	.text._ZN2v88internal15ToNameAssembler18GenerateToNameImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15ToNameAssembler18GenerateToNameImplEv
	.type	_ZN2v88internal15ToNameAssembler18GenerateToNameImplEv, @function
_ZN2v88internal15ToNameAssembler18GenerateToNameImplEv:
.LFB21934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-832(%rbp), %r14
	leaq	-448(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-896(%rbp), %rbx
	subq	$920, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%rbx, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	movq	%rbx, -936(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-704(%rbp), %rcx
	movq	%rax, %rbx
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-576(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -920(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-320(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -944(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18IsNameInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-920(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-944(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler24IsJSReceiverInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-944(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler24IsHeapNumberInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-944(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler20IsBigIntInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-928(%rbp), %rcx
	movq	-912(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-64(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	-904(%rbp), %rcx
	movl	$1, %r9d
	movq	%rax, %rdx
	movl	$261, %esi
	movq	%r8, -944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-864(%rbp), %r11
	movl	$105, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -960(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-944(%rbp), %r8
	movq	%r12, %rdi
	leaq	-880(%rbp), %r10
	movq	%rax, %rdx
	movq	%r10, %rsi
	movl	$1, %r9d
	movq	%rbx, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-904(%rbp), %rcx
	movq	%r8, -952(%rbp)
	movq	%rax, -880(%rbp)
	movq	-848(%rbp), %rax
	movq	%r10, -944(%rbp)
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-928(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-960(%rbp), %r11
	movl	$97, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-952(%rbp), %r8
	movq	%rbx, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movl	$1, %ebx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-944(%rbp), %r10
	movq	-904(%rbp), %r9
	pushq	%rbx
	movq	%rdx, -880(%rbp)
	movq	-848(%rbp), %rdx
	pushq	%r8
	movq	%rax, %r8
	movq	%rdx, -872(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-936(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-928(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21934:
	.size	_ZN2v88internal15ToNameAssembler18GenerateToNameImplEv, .-_ZN2v88internal15ToNameAssembler18GenerateToNameImplEv
	.section	.rodata._ZN2v88internal8Builtins15Generate_ToNameEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"ToName"
	.section	.text._ZN2v88internal8Builtins15Generate_ToNameEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins15Generate_ToNameEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins15Generate_ToNameEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins15Generate_ToNameEPNS0_8compiler18CodeAssemblerStateE:
.LFB21930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$107, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$99, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L68
.L65:
	movq	%r13, %rdi
	call	_ZN2v88internal15ToNameAssembler18GenerateToNameImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L65
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21930:
	.size	_ZN2v88internal8Builtins15Generate_ToNameEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins15Generate_ToNameEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins26Generate_NonNumberToNumberEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"NonNumberToNumber"
	.section	.text._ZN2v88internal8Builtins26Generate_NonNumberToNumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_NonNumberToNumberEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_NonNumberToNumberEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_NonNumberToNumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB21939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$169, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$100, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L74
.L71:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17NonNumberToNumberENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEENS1_14BigIntHandlingE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L71
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21939:
	.size	_ZN2v88internal8Builtins26Generate_NonNumberToNumberEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_NonNumberToNumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26NonNumberToNumberAssembler29GenerateNonNumberToNumberImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26NonNumberToNumberAssembler29GenerateNonNumberToNumberImplEv
	.type	_ZN2v88internal26NonNumberToNumberAssembler29GenerateNonNumberToNumberImplEv, @function
_ZN2v88internal26NonNumberToNumberAssembler29GenerateNonNumberToNumberImplEv:
.LFB21943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17NonNumberToNumberENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEENS1_14BigIntHandlingE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21943:
	.size	_ZN2v88internal26NonNumberToNumberAssembler29GenerateNonNumberToNumberImplEv, .-_ZN2v88internal26NonNumberToNumberAssembler29GenerateNonNumberToNumberImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_NonNumberToNumericEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"NonNumberToNumeric"
	.section	.text._ZN2v88internal8Builtins27Generate_NonNumberToNumericEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_NonNumberToNumericEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_NonNumberToNumericEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_NonNumberToNumericEPNS0_8compiler18CodeAssemblerStateE:
.LFB21948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$176, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$101, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L82
.L79:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler18NonNumberToNumericENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L79
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21948:
	.size	_ZN2v88internal8Builtins27Generate_NonNumberToNumericEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_NonNumberToNumericEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal27NonNumberToNumericAssembler30GenerateNonNumberToNumericImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27NonNumberToNumericAssembler30GenerateNonNumberToNumericImplEv
	.type	_ZN2v88internal27NonNumberToNumericAssembler30GenerateNonNumberToNumericImplEv, @function
_ZN2v88internal27NonNumberToNumericAssembler30GenerateNonNumberToNumericImplEv:
.LFB21952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler18NonNumberToNumericENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21952:
	.size	_ZN2v88internal27NonNumberToNumericAssembler30GenerateNonNumberToNumericImplEv, .-_ZN2v88internal27NonNumberToNumericAssembler30GenerateNonNumberToNumericImplEv
	.section	.text._ZN2v88internal18ToNumericAssembler21GenerateToNumericImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ToNumericAssembler21GenerateToNumericImplEv
	.type	_ZN2v88internal18ToNumericAssembler21GenerateToNumericImplEv, @function
_ZN2v88internal18ToNumericAssembler21GenerateToNumericImplEv:
.LFB21961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %xmm2
	movq	%r12, %rdi
	movq	%r12, -128(%rbp)
	movq	-168(%rbp), %xmm0
	movq	%rax, %xmm1
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8IsNumberENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS5_INS2_3SmiENS2_10HeapNumberEEENS2_6BigIntEEEZNS2_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSB_21GenerateToNumericImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENSE_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKST_St18_Manager_operation(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-144(%rbp), %rax
	movq	%rcx, %xmm0
	movl	$8, %r8d
	movq	%rax, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, %rcx
	movq	%rax, %xmm3
	leaq	-160(%rbp), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, -112(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS8_INS1_3SmiENS1_10HeapNumberEEENS1_6BigIntEEEZNS1_18ToNumericAssembler21GenerateToNumericImplEvEUlvE_ZNSE_21GenerateToNumericImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, %xmm4
	movq	%rdx, %xmm0
	movq	%r15, %rdx
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, %r13
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L87
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L87:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L88
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L88:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$144, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21961:
	.size	_ZN2v88internal18ToNumericAssembler21GenerateToNumericImplEv, .-_ZN2v88internal18ToNumericAssembler21GenerateToNumericImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_ToNumericEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"ToNumeric"
	.section	.text._ZN2v88internal8Builtins18Generate_ToNumericEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_ToNumericEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_ToNumericEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_ToNumericEPNS0_8compiler18CodeAssemblerStateE:
.LFB21957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$183, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$104, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L102
.L99:
	movq	%r13, %rdi
	call	_ZN2v88internal18ToNumericAssembler21GenerateToNumericImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L99
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21957:
	.size	_ZN2v88internal8Builtins18Generate_ToNumericEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_ToNumericEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins17Generate_ToNumberEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"ToNumber"
	.section	.text._ZN2v88internal8Builtins17Generate_ToNumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_ToNumberEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_ToNumberEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_ToNumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB21968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$193, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$102, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L108
.L105:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler8ToNumberENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_14BigIntHandlingE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L105
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21968:
	.size	_ZN2v88internal8Builtins17Generate_ToNumberEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_ToNumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17ToNumberAssembler20GenerateToNumberImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ToNumberAssembler20GenerateToNumberImplEv
	.type	_ZN2v88internal17ToNumberAssembler20GenerateToNumberImplEv, @function
_ZN2v88internal17ToNumberAssembler20GenerateToNumberImplEv:
.LFB21972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler8ToNumberENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_14BigIntHandlingE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21972:
	.size	_ZN2v88internal17ToNumberAssembler20GenerateToNumberImplEv, .-_ZN2v88internal17ToNumberAssembler20GenerateToNumberImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_ToNumberConvertBigIntEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"ToNumberConvertBigInt"
	.section	.text._ZN2v88internal8Builtins30Generate_ToNumberConvertBigIntEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_ToNumberConvertBigIntEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_ToNumberConvertBigIntEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_ToNumberConvertBigIntEPNS0_8compiler18CodeAssemblerStateE:
.LFB21977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$201, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$103, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L116
.L113:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler8ToNumberENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_14BigIntHandlingE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L113
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21977:
	.size	_ZN2v88internal8Builtins30Generate_ToNumberConvertBigIntEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_ToNumberConvertBigIntEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30ToNumberConvertBigIntAssembler33GenerateToNumberConvertBigIntImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30ToNumberConvertBigIntAssembler33GenerateToNumberConvertBigIntImplEv
	.type	_ZN2v88internal30ToNumberConvertBigIntAssembler33GenerateToNumberConvertBigIntImplEv, @function
_ZN2v88internal30ToNumberConvertBigIntAssembler33GenerateToNumberConvertBigIntImplEv:
.LFB21981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler8ToNumberENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_14BigIntHandlingE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21981:
	.size	_ZN2v88internal30ToNumberConvertBigIntAssembler33GenerateToNumberConvertBigIntImplEv, .-_ZN2v88internal30ToNumberConvertBigIntAssembler33GenerateToNumberConvertBigIntImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_NumberToStringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"NumberToString"
	.section	.text._ZN2v88internal8Builtins23Generate_NumberToStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_NumberToStringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_NumberToStringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_NumberToStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB21986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$209, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$105, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L124
.L121:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14NumberToStringENS0_8compiler5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L121
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21986:
	.size	_ZN2v88internal8Builtins23Generate_NumberToStringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_NumberToStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23NumberToStringAssembler26GenerateNumberToStringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23NumberToStringAssembler26GenerateNumberToStringImplEv
	.type	_ZN2v88internal23NumberToStringAssembler26GenerateNumberToStringImplEv, @function
_ZN2v88internal23NumberToStringAssembler26GenerateNumberToStringImplEv:
.LFB21990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14NumberToStringENS0_8compiler5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21990:
	.size	_ZN2v88internal23NumberToStringAssembler26GenerateNumberToStringImplEv, .-_ZN2v88internal23NumberToStringAssembler26GenerateNumberToStringImplEv
	.section	.text._ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE
	.type	_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE, @function
_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE:
.LFB21991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -560(%rbp)
	movq	%rdi, %rsi
	movq	%rdx, -600(%rbp)
	movl	$8, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-544(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rbx, -224(%rbp)
	leaq	-224(%rbp), %rbx
	movl	$1, %r8d
	movq	%rbx, %rcx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	testl	%r12d, %r12d
	je	.L129
	cmpl	$1, %r12d
	jne	.L131
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r15, %rdi
	addq	$3456, %rax
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	addq	$3552, %rax
	movq	%rax, -88(%rbp)
.L131:
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %r11
	movq	%rax, -552(%rbp)
	leaq	-512(%rbp), %rax
	leaq	-352(%rbp), %r13
	movq	%rax, -640(%rbp)
	leaq	-528(%rbp), %rax
	movq	%rax, -632(%rbp)
.L132:
	movq	(%r11), %rsi
	movq	%r15, %rdi
	movq	%r11, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -608(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r15, %rdi
	movq	$2, -584(%rbp)
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	pushq	-584(%rbp)
	movq	-560(%rbp), %r9
	movq	-600(%rbp), %xmm0
	movq	%rax, -344(%rbp)
	movq	-552(%rbp), %rax
	movhps	-608(%rbp), %xmm0
	movq	%rax, -592(%rbp)
	pushq	-592(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler13IsCallableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-640(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	movq	-496(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	$3, -568(%rbp)
	movq	%rcx, -528(%rbp)
	movq	-600(%rbp), %rcx
	movhps	-608(%rbp), %xmm0
	movq	%rax, -520(%rbp)
	movq	-552(%rbp), %rax
	pushq	-568(%rbp)
	movq	-560(%rbp), %r9
	movq	-632(%rbp), %rdx
	movq	%rax, -576(%rbp)
	pushq	-576(%rbp)
	movq	%rcx, -64(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-616(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23IsPrimitiveInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %r11
	addq	$8, %r11
	cmpq	%r11, -552(%rbp)
	jne	.L132
	movq	-560(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	$30, %edx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-616(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L136
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r15, %rdi
	addq	$3552, %rax
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	addq	$3456, %rax
	movq	%rax, -88(%rbp)
	jmp	.L131
.L136:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21991:
	.size	_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE, .-_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE
	.section	.rodata._ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"OrdinaryToPrimitive_Number"
	.section	.text._ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB21996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$269, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$93, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L141
.L138:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L138
.L142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21996:
	.size	_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_NumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35OrdinaryToPrimitive_NumberAssembler38GenerateOrdinaryToPrimitive_NumberImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35OrdinaryToPrimitive_NumberAssembler38GenerateOrdinaryToPrimitive_NumberImplEv
	.type	_ZN2v88internal35OrdinaryToPrimitive_NumberAssembler38GenerateOrdinaryToPrimitive_NumberImplEv, @function
_ZN2v88internal35OrdinaryToPrimitive_NumberAssembler38GenerateOrdinaryToPrimitive_NumberImplEv:
.LFB22000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE
	.cfi_endproc
.LFE22000:
	.size	_ZN2v88internal35OrdinaryToPrimitive_NumberAssembler38GenerateOrdinaryToPrimitive_NumberImplEv, .-_ZN2v88internal35OrdinaryToPrimitive_NumberAssembler38GenerateOrdinaryToPrimitive_NumberImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"OrdinaryToPrimitive_String"
	.section	.text._ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE:
.LFB22005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$276, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$94, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L149
.L146:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L146
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22005:
	.size	_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_OrdinaryToPrimitive_StringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35OrdinaryToPrimitive_StringAssembler38GenerateOrdinaryToPrimitive_StringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35OrdinaryToPrimitive_StringAssembler38GenerateOrdinaryToPrimitive_StringImplEv
	.type	_ZN2v88internal35OrdinaryToPrimitive_StringAssembler38GenerateOrdinaryToPrimitive_StringImplEv, @function
_ZN2v88internal35OrdinaryToPrimitive_StringAssembler38GenerateOrdinaryToPrimitive_StringImplEv:
.LFB22009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal27ConversionBuiltinsAssembler28Generate_OrdinaryToPrimitiveEPNS0_8compiler4NodeES4_NS0_23OrdinaryToPrimitiveHintE
	.cfi_endproc
.LFE22009:
	.size	_ZN2v88internal35OrdinaryToPrimitive_StringAssembler38GenerateOrdinaryToPrimitive_StringImplEv, .-_ZN2v88internal35OrdinaryToPrimitive_StringAssembler38GenerateOrdinaryToPrimitive_StringImplEv
	.section	.text._ZN2v88internal18ToBooleanAssembler21GenerateToBooleanImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ToBooleanAssembler21GenerateToBooleanImplEv
	.type	_ZN2v88internal18ToBooleanAssembler21GenerateToBooleanImplEv, @function
_ZN2v88internal18ToBooleanAssembler21GenerateToBooleanImplEv:
.LFB22018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$272, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$272, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22018:
	.size	_ZN2v88internal18ToBooleanAssembler21GenerateToBooleanImplEv, .-_ZN2v88internal18ToBooleanAssembler21GenerateToBooleanImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_ToBooleanEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"ToBoolean"
	.section	.text._ZN2v88internal8Builtins18Generate_ToBooleanEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_ToBooleanEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_ToBooleanEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_ToBooleanEPNS0_8compiler18CodeAssemblerStateE:
.LFB22014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$284, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$92, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L161
.L158:
	movq	%r13, %rdi
	call	_ZN2v88internal18ToBooleanAssembler21GenerateToBooleanImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L158
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22014:
	.size	_ZN2v88internal8Builtins18Generate_ToBooleanEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_ToBooleanEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal39ToBooleanLazyDeoptContinuationAssembler42GenerateToBooleanLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39ToBooleanLazyDeoptContinuationAssembler42GenerateToBooleanLazyDeoptContinuationImplEv
	.type	_ZN2v88internal39ToBooleanLazyDeoptContinuationAssembler42GenerateToBooleanLazyDeoptContinuationImplEv, @function
_ZN2v88internal39ToBooleanLazyDeoptContinuationAssembler42GenerateToBooleanLazyDeoptContinuationImplEv:
.LFB22027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$272, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$272, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L166:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22027:
	.size	_ZN2v88internal39ToBooleanLazyDeoptContinuationAssembler42GenerateToBooleanLazyDeoptContinuationImplEv, .-_ZN2v88internal39ToBooleanLazyDeoptContinuationAssembler42GenerateToBooleanLazyDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_ToBooleanLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"ToBooleanLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins39Generate_ToBooleanLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_ToBooleanLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_ToBooleanLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_ToBooleanLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$300, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$115, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L171
.L168:
	movq	%r13, %rdi
	call	_ZN2v88internal39ToBooleanLazyDeoptContinuationAssembler42GenerateToBooleanLazyDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L168
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22023:
	.size	_ZN2v88internal8Builtins39Generate_ToBooleanLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_ToBooleanLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17ToLengthAssembler20GenerateToLengthImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ToLengthAssembler20GenerateToLengthImplEv
	.type	_ZN2v88internal17ToLengthAssembler20GenerateToLengthImplEv, @function
_ZN2v88internal17ToLengthAssembler20GenerateToLengthImplEv:
.LFB22036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-832(%rbp), %r14
	leaq	-448(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-896(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-704(%rbp), %rbx
	subq	$920, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-192(%rbp), %rax
	movl	$1, %r8d
	movq	%r13, -192(%rbp)
	movq	%rax, %rcx
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-576(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler19TaggedIsPositiveSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-912(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-320(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -920(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-904(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-920(%rbp), %r11
	movq	-904(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-920(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -936(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15Float64ConstantEd@PLT
	movq	-920(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18Float64GreaterThanENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movsd	.LC18(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Float64ConstantEd@PLT
	movq	-920(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	movq	%r8, -928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25Float64GreaterThanOrEqualENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	-944(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-928(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler12Float64FloorENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21ChangeFloat64ToTaggedENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$100, %edx
	leaq	-864(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	movq	%rbx, -64(%rbp)
	movl	$1, %edi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	movq	-952(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -880(%rbp)
	movq	-848(%rbp), %rax
	leaq	-880(%rbp), %rdx
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-912(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-920(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -912(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	.LC18(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler13CodeAssembler14NumberConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-936(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22036:
	.size	_ZN2v88internal17ToLengthAssembler20GenerateToLengthImplEv, .-_ZN2v88internal17ToLengthAssembler20GenerateToLengthImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_ToLengthEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"ToLength"
	.section	.text._ZN2v88internal8Builtins17Generate_ToLengthEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_ToLengthEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_ToLengthEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_ToLengthEPNS0_8compiler18CodeAssemblerStateE:
.LFB22032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$313, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$108, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L181
.L178:
	movq	%r13, %rdi
	call	_ZN2v88internal17ToLengthAssembler20GenerateToLengthImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L182
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L178
.L182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22032:
	.size	_ZN2v88internal8Builtins17Generate_ToLengthEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_ToLengthEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins18Generate_ToIntegerEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"ToInteger"
	.section	.text._ZN2v88internal8Builtins18Generate_ToIntegerEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_ToIntegerEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_ToIntegerEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_ToIntegerEPNS0_8compiler18CodeAssemblerStateE:
.LFB22041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$379, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$106, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L187
.L184:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler9ToIntegerENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L184
.L188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22041:
	.size	_ZN2v88internal8Builtins18Generate_ToIntegerEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_ToIntegerEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18ToIntegerAssembler21GenerateToIntegerImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ToIntegerAssembler21GenerateToIntegerImplEv
	.type	_ZN2v88internal18ToIntegerAssembler21GenerateToIntegerImplEv, @function
_ZN2v88internal18ToIntegerAssembler21GenerateToIntegerImplEv:
.LFB22045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler9ToIntegerENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22045:
	.size	_ZN2v88internal18ToIntegerAssembler21GenerateToIntegerImplEv, .-_ZN2v88internal18ToIntegerAssembler21GenerateToIntegerImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_ToInteger_TruncateMinusZeroEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"ToInteger_TruncateMinusZero"
	.section	.text._ZN2v88internal8Builtins36Generate_ToInteger_TruncateMinusZeroEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_ToInteger_TruncateMinusZeroEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_ToInteger_TruncateMinusZeroEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_ToInteger_TruncateMinusZeroEPNS0_8compiler18CodeAssemblerStateE:
.LFB22050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$386, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$107, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L195
.L192:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler9ToIntegerENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L192
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22050:
	.size	_ZN2v88internal8Builtins36Generate_ToInteger_TruncateMinusZeroEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_ToInteger_TruncateMinusZeroEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36ToInteger_TruncateMinusZeroAssembler39GenerateToInteger_TruncateMinusZeroImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ToInteger_TruncateMinusZeroAssembler39GenerateToInteger_TruncateMinusZeroImplEv
	.type	_ZN2v88internal36ToInteger_TruncateMinusZeroAssembler39GenerateToInteger_TruncateMinusZeroImplEv, @function
_ZN2v88internal36ToInteger_TruncateMinusZeroAssembler39GenerateToInteger_TruncateMinusZeroImplEv:
.LFB22054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler9ToIntegerENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22054:
	.size	_ZN2v88internal36ToInteger_TruncateMinusZeroAssembler39GenerateToInteger_TruncateMinusZeroImplEv, .-_ZN2v88internal36ToInteger_TruncateMinusZeroAssembler39GenerateToInteger_TruncateMinusZeroImplEv
	.section	.rodata._ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv.str1.1,"aMS",@progbits,1
.LC22:
	.string	"ToObject"
	.section	.text._ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv
	.type	_ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv, @function
_ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv:
.LFB22063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-592(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-576(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$584, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler24IsJSReceiverInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-624(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler31LoadMapConstructorFunctionIndexENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-600(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-608(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$107, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-616(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -600(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	-600(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -600(%rbp)
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	-600(%rbp), %rsi
	movl	$29, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	-600(%rbp), %rsi
	movl	$29, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%r13, %rcx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	-600(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-608(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$173, %edx
	movq	%r12, %rdi
	movq	-616(%rbp), %r11
	leaq	.LC22(%rip), %rcx
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	-624(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -600(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-608(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-600(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L202:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22063:
	.size	_ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv, .-_ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv
	.section	.text._ZN2v88internal8Builtins17Generate_ToObjectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_ToObjectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_ToObjectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_ToObjectEPNS0_8compiler18CodeAssemblerStateE:
.LFB22059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$394, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$91, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L207
.L204:
	movq	%r13, %rdi
	call	_ZN2v88internal17ToObjectAssembler20GenerateToObjectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L204
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22059:
	.size	_ZN2v88internal8Builtins17Generate_ToObjectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_ToObjectEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins15Generate_TypeofEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"Typeof"
	.section	.text._ZN2v88internal8Builtins15Generate_TypeofEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins15Generate_TypeofEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins15Generate_TypeofEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins15Generate_TypeofEPNS0_8compiler18CodeAssemblerStateE:
.LFB22068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$451, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$109, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L213
.L210:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6TypeofEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L210
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22068:
	.size	_ZN2v88internal8Builtins15Generate_TypeofEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins15Generate_TypeofEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal15TypeofAssembler18GenerateTypeofImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15TypeofAssembler18GenerateTypeofImplEv
	.type	_ZN2v88internal15TypeofAssembler18GenerateTypeofImplEv, @function
_ZN2v88internal15TypeofAssembler18GenerateTypeofImplEv:
.LFB22072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6TypeofEPNS0_8compiler4NodeE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22072:
	.size	_ZN2v88internal15TypeofAssembler18GenerateTypeofImplEv, .-_ZN2v88internal15TypeofAssembler18GenerateTypeofImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE, @function
_GLOBAL__sub_I__ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE:
.LFB28261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28261:
	.size	_GLOBAL__sub_I__ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE, .-_GLOBAL__sub_I__ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27ConversionBuiltinsAssembler32Generate_NonPrimitiveToPrimitiveEPNS0_8compiler4NodeES4_NS0_15ToPrimitiveHintE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC18:
	.long	4294967295
	.long	1128267775
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
