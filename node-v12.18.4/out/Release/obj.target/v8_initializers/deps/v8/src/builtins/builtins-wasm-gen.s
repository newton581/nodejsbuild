	.file	"builtins-wasm-gen.cc"
	.text
	.section	.rodata._ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/builtins/builtins-wasm-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"WasmAllocateHeapNumber"
	.section	.text._ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB21892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$63, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$633, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L6
.L2:
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$28288, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	leaq	-48(%rbp), %rsi
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal28AllocateHeapNumberDescriptorE(%rip), %rdx
	leaq	80+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r13, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L2
.L7:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21892:
	.size	_ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31WasmAllocateHeapNumberAssembler34GenerateWasmAllocateHeapNumberImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31WasmAllocateHeapNumberAssembler34GenerateWasmAllocateHeapNumberImplEv
	.type	_ZN2v88internal31WasmAllocateHeapNumberAssembler34GenerateWasmAllocateHeapNumberImplEv, @function
_ZN2v88internal31WasmAllocateHeapNumberAssembler34GenerateWasmAllocateHeapNumberImplEv:
.LFB21896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$28288, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	leaq	-48(%rbp), %rsi
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal28AllocateHeapNumberDescriptorE(%rip), %rdx
	leaq	80+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r13, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21896:
	.size	_ZN2v88internal31WasmAllocateHeapNumberAssembler34GenerateWasmAllocateHeapNumberImplEv, .-_ZN2v88internal31WasmAllocateHeapNumberAssembler34GenerateWasmAllocateHeapNumberImplEv
	.section	.text._ZN2v88internal24WasmRecordWriteAssembler27GenerateWasmRecordWriteImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24WasmRecordWriteAssembler27GenerateWasmRecordWriteImplEv
	.type	_ZN2v88internal24WasmRecordWriteAssembler27GenerateWasmRecordWriteImplEv, @function
_ZN2v88internal24WasmRecordWriteAssembler27GenerateWasmRecordWriteImplEv:
.LFB21905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$24936, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	leaq	-80(%rbp), %rsi
	leaq	-64(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal21RecordWriteDescriptorE(%rip), %rdx
	leaq	2000+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movl	$4, %r9d
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r13, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-112(%rbp), %xmm0
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	-96(%rbp), %xmm0
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21905:
	.size	_ZN2v88internal24WasmRecordWriteAssembler27GenerateWasmRecordWriteImplEv, .-_ZN2v88internal24WasmRecordWriteAssembler27GenerateWasmRecordWriteImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_WasmRecordWriteEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"WasmRecordWrite"
	.section	.text._ZN2v88internal8Builtins24Generate_WasmRecordWriteEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_WasmRecordWriteEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_WasmRecordWriteEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_WasmRecordWriteEPNS0_8compiler18CodeAssemblerStateE:
.LFB21901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$68, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$640, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L20
.L17:
	movq	%r13, %rdi
	call	_ZN2v88internal24WasmRecordWriteAssembler27GenerateWasmRecordWriteImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L17
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21901:
	.size	_ZN2v88internal8Builtins24Generate_WasmRecordWriteEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_WasmRecordWriteEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins21Generate_WasmToNumberEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"WasmToNumber"
	.section	.text._ZN2v88internal8Builtins21Generate_WasmToNumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_WasmToNumberEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_WasmToNumberEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_WasmToNumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB21910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$78, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$643, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L26
.L23:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$25752, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	leaq	-64(%rbp), %rsi
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal24TypeConversionDescriptorE(%rip), %rcx
	leaq	2480+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rbx, -48(%rbp)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movl	$1, %r9d
	movq	%r13, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L23
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21910:
	.size	_ZN2v88internal8Builtins21Generate_WasmToNumberEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_WasmToNumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21WasmToNumberAssembler24GenerateWasmToNumberImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21WasmToNumberAssembler24GenerateWasmToNumberImplEv
	.type	_ZN2v88internal21WasmToNumberAssembler24GenerateWasmToNumberImplEv, @function
_ZN2v88internal21WasmToNumberAssembler24GenerateWasmToNumberImplEv:
.LFB21914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$25752, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	leaq	-64(%rbp), %rsi
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal24TypeConversionDescriptorE(%rip), %rcx
	leaq	2480+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rbx, -48(%rbp)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movl	$1, %r9d
	movq	%r13, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L31:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21914:
	.size	_ZN2v88internal21WasmToNumberAssembler24GenerateWasmToNumberImplEv, .-_ZN2v88internal21WasmToNumberAssembler24GenerateWasmToNumberImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_WasmStackGuardEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"WasmStackGuard"
	.section	.text._ZN2v88internal8Builtins23Generate_WasmStackGuardEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_WasmStackGuardEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_WasmStackGuardEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_WasmStackGuardEPNS0_8compiler18CodeAssemblerStateE:
.LFB21919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$85, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$641, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L36
.L33:
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	pushq	$0
	movq	%r13, %r8
	movq	%r14, %rcx
	pushq	$0
	movq	%rax, %rdx
	movl	$453, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L33
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21919:
	.size	_ZN2v88internal8Builtins23Generate_WasmStackGuardEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_WasmStackGuardEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23WasmStackGuardAssembler26GenerateWasmStackGuardImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23WasmStackGuardAssembler26GenerateWasmStackGuardImplEv
	.type	_ZN2v88internal23WasmStackGuardAssembler26GenerateWasmStackGuardImplEv, @function
_ZN2v88internal23WasmStackGuardAssembler26GenerateWasmStackGuardImplEv:
.LFB21923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$119, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movl	$30496, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1799, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movl	$151, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1800, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	pushq	$0
	movq	%r13, %r8
	movq	%r14, %rcx
	pushq	$0
	movq	%rax, %rdx
	movq	%r12, %rdi
	movl	$453, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21923:
	.size	_ZN2v88internal23WasmStackGuardAssembler26GenerateWasmStackGuardImplEv, .-_ZN2v88internal23WasmStackGuardAssembler26GenerateWasmStackGuardImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_WasmStackOverflowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"WasmStackOverflow"
	.section	.text._ZN2v88internal8Builtins26Generate_WasmStackOverflowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_WasmStackOverflowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_WasmStackOverflowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_WasmStackOverflowEPNS0_8compiler18CodeAssemblerStateE:
.LFB21928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$92, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$642, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L44
.L41:
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	pushq	$0
	movq	%r13, %r8
	movq	%r14, %rcx
	pushq	$0
	movq	%rax, %rdx
	movl	$445, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L41
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21928:
	.size	_ZN2v88internal8Builtins26Generate_WasmStackOverflowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_WasmStackOverflowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26WasmStackOverflowAssembler29GenerateWasmStackOverflowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26WasmStackOverflowAssembler29GenerateWasmStackOverflowImplEv
	.type	_ZN2v88internal26WasmStackOverflowAssembler29GenerateWasmStackOverflowImplEv, @function
_ZN2v88internal26WasmStackOverflowAssembler29GenerateWasmStackOverflowImplEv:
.LFB21932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$119, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movl	$30496, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1799, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movl	$151, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1800, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	pushq	$0
	movq	%r13, %r8
	movq	%r14, %rcx
	pushq	$0
	movq	%rax, %rdx
	movq	%r12, %rdi
	movl	$445, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21932:
	.size	_ZN2v88internal26WasmStackOverflowAssembler29GenerateWasmStackOverflowImplEv, .-_ZN2v88internal26WasmStackOverflowAssembler29GenerateWasmStackOverflowImplEv
	.section	.text._ZN2v88internal18WasmThrowAssembler21GenerateWasmThrowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18WasmThrowAssembler21GenerateWasmThrowImplEv
	.type	_ZN2v88internal18WasmThrowAssembler21GenerateWasmThrowImplEv, @function
_ZN2v88internal18WasmThrowAssembler21GenerateWasmThrowImplEv:
.LFB21941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$160, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21941:
	.size	_ZN2v88internal18WasmThrowAssembler21GenerateWasmThrowImplEv, .-_ZN2v88internal18WasmThrowAssembler21GenerateWasmThrowImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_WasmThrowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"WasmThrow"
	.section	.text._ZN2v88internal8Builtins18Generate_WasmThrowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_WasmThrowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_WasmThrowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_WasmThrowEPNS0_8compiler18CodeAssemblerStateE:
.LFB21937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$99, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$644, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L56
.L53:
	movq	%r13, %rdi
	call	_ZN2v88internal18WasmThrowAssembler21GenerateWasmThrowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L53
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21937:
	.size	_ZN2v88internal8Builtins18Generate_WasmThrowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_WasmThrowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20WasmRethrowAssembler23GenerateWasmRethrowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20WasmRethrowAssembler23GenerateWasmRethrowImplEv
	.type	_ZN2v88internal20WasmRethrowAssembler23GenerateWasmRethrowImplEv, @function
_ZN2v88internal20WasmRethrowAssembler23GenerateWasmRethrowImplEv:
.LFB21950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$156, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21950:
	.size	_ZN2v88internal20WasmRethrowAssembler23GenerateWasmRethrowImplEv, .-_ZN2v88internal20WasmRethrowAssembler23GenerateWasmRethrowImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_WasmRethrowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"WasmRethrow"
	.section	.text._ZN2v88internal8Builtins20Generate_WasmRethrowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_WasmRethrowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_WasmRethrowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_WasmRethrowEPNS0_8compiler18CodeAssemblerStateE:
.LFB21946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$107, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$645, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L66
.L63:
	movq	%r13, %rdi
	call	_ZN2v88internal20WasmRethrowAssembler23GenerateWasmRethrowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L63
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21946:
	.size	_ZN2v88internal8Builtins20Generate_WasmRethrowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_WasmRethrowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal25WasmAtomicNotifyAssembler28GenerateWasmAtomicNotifyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25WasmAtomicNotifyAssembler28GenerateWasmAtomicNotifyImplEv
	.type	_ZN2v88internal25WasmAtomicNotifyAssembler28GenerateWasmAtomicNotifyImplEv, @function
_ZN2v88internal25WasmAtomicNotifyAssembler28GenerateWasmAtomicNotifyImplEv:
.LFB21959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal28AllocateHeapNumberDescriptorE(%rip), %rdx
	xorl	%esi, %esi
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%rbx, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$28288, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%rbx, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler21ChangeUint32ToFloat64ENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%rbx, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-136(%rbp), %r10
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler21ChangeUint32ToFloat64ENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	leaq	-80(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-112(%rbp), %xmm0
	movq	%rax, %rcx
	movl	$3, %r9d
	movl	$448, %esi
	movq	%r13, -64(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25CallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9ReturnRawEPNS1_4NodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21959:
	.size	_ZN2v88internal25WasmAtomicNotifyAssembler28GenerateWasmAtomicNotifyImplEv, .-_ZN2v88internal25WasmAtomicNotifyAssembler28GenerateWasmAtomicNotifyImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_WasmAtomicNotifyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"WasmAtomicNotify"
	.section	.text._ZN2v88internal8Builtins25Generate_WasmAtomicNotifyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_WasmAtomicNotifyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_WasmAtomicNotifyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_WasmAtomicNotifyEPNS0_8compiler18CodeAssemblerStateE:
.LFB21955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$115, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$634, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L76
.L73:
	movq	%r13, %rdi
	call	_ZN2v88internal25WasmAtomicNotifyAssembler28GenerateWasmAtomicNotifyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L73
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21955:
	.size	_ZN2v88internal8Builtins25Generate_WasmAtomicNotifyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_WasmAtomicNotifyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26WasmI32AtomicWaitAssembler29GenerateWasmI32AtomicWaitImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26WasmI32AtomicWaitAssembler29GenerateWasmI32AtomicWaitImplEv
	.type	_ZN2v88internal26WasmI32AtomicWaitAssembler29GenerateWasmI32AtomicWaitImplEv, @function
_ZN2v88internal26WasmI32AtomicWaitAssembler29GenerateWasmI32AtomicWaitImplEv:
.LFB21968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal28AllocateHeapNumberDescriptorE(%rip), %rdx
	xorl	%esi, %esi
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%r14, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$28288, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	leaq	-112(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%r14, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler21ChangeUint32ToFloat64ENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%r14, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-160(%rbp), %r11
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler20ChangeInt32ToFloat64ENS1_11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%r14, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	-152(%rbp), %r10
	movq	%r13, %xmm3
	movq	-144(%rbp), %xmm0
	movq	%rax, %rcx
	leaq	-96(%rbp), %r8
	movl	$4, %r9d
	movq	%r12, %rdi
	movhps	-136(%rbp), %xmm0
	movq	%r10, %rdx
	movl	$446, %esi
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25CallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9ReturnRawEPNS1_4NodeE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21968:
	.size	_ZN2v88internal26WasmI32AtomicWaitAssembler29GenerateWasmI32AtomicWaitImplEv, .-_ZN2v88internal26WasmI32AtomicWaitAssembler29GenerateWasmI32AtomicWaitImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_WasmI32AtomicWaitEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"WasmI32AtomicWait"
	.section	.text._ZN2v88internal8Builtins26Generate_WasmI32AtomicWaitEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_WasmI32AtomicWaitEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_WasmI32AtomicWaitEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_WasmI32AtomicWaitEPNS0_8compiler18CodeAssemblerStateE:
.LFB21964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$140, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$635, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L86
.L83:
	movq	%r13, %rdi
	call	_ZN2v88internal26WasmI32AtomicWaitAssembler29GenerateWasmI32AtomicWaitImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L83
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21964:
	.size	_ZN2v88internal8Builtins26Generate_WasmI32AtomicWaitEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_WasmI32AtomicWaitEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26WasmI64AtomicWaitAssembler29GenerateWasmI64AtomicWaitImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26WasmI64AtomicWaitAssembler29GenerateWasmI64AtomicWaitImplEv
	.type	_ZN2v88internal26WasmI64AtomicWaitAssembler29GenerateWasmI64AtomicWaitImplEv, @function
_ZN2v88internal26WasmI64AtomicWaitAssembler29GenerateWasmI64AtomicWaitImplEv:
.LFB21977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal28AllocateHeapNumberDescriptorE(%rip), %rdx
	xorl	%esi, %esi
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	80+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%r15, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$28288, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	leaq	-112(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler21ChangeUint32ToFloat64ENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler21ChangeUint32ToFloat64ENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-168(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler21ChangeUint32ToFloat64ENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	pushq	$0
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %r8
	pushq	$0
	movq	%rax, %r9
	movl	$1, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-160(%rbp), %r10
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler20StoreHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapNumberEEENS3_INS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	%r13, %xmm3
	leaq	-96(%rbp), %r8
	movq	-144(%rbp), %xmm0
	movq	%rax, %rcx
	movl	$5, %r9d
	movq	%r12, %rdi
	movq	%r14, -64(%rbp)
	movhps	-136(%rbp), %xmm0
	movq	-152(%rbp), %rdx
	movl	$447, %esi
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25CallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9ReturnRawEPNS1_4NodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21977:
	.size	_ZN2v88internal26WasmI64AtomicWaitAssembler29GenerateWasmI64AtomicWaitImplEv, .-_ZN2v88internal26WasmI64AtomicWaitAssembler29GenerateWasmI64AtomicWaitImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_WasmI64AtomicWaitEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"WasmI64AtomicWait"
	.section	.text._ZN2v88internal8Builtins26Generate_WasmI64AtomicWaitEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_WasmI64AtomicWaitEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_WasmI64AtomicWaitEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_WasmI64AtomicWaitEPNS0_8compiler18CodeAssemblerStateE:
.LFB21973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$173, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$636, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L96
.L93:
	movq	%r13, %rdi
	call	_ZN2v88internal26WasmI64AtomicWaitAssembler29GenerateWasmI64AtomicWaitImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L93
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21973:
	.size	_ZN2v88internal8Builtins26Generate_WasmI64AtomicWaitEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_WasmI64AtomicWaitEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23WasmMemoryGrowAssembler26GenerateWasmMemoryGrowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23WasmMemoryGrowAssembler26GenerateWasmMemoryGrowImplEv
	.type	_ZN2v88internal23WasmMemoryGrowAssembler26GenerateWasmMemoryGrowImplEv, @function
_ZN2v88internal23WasmMemoryGrowAssembler26GenerateWasmMemoryGrowImplEv:
.LFB21986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18IsValidPositiveSmiENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	leaq	-80(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r9d
	movl	$451, %esi
	movq	%rbx, -80(%rbp)
	movq	%r15, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25CallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9ReturnRawEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9ReturnRawEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21986:
	.size	_ZN2v88internal23WasmMemoryGrowAssembler26GenerateWasmMemoryGrowImplEv, .-_ZN2v88internal23WasmMemoryGrowAssembler26GenerateWasmMemoryGrowImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_WasmMemoryGrowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"WasmMemoryGrow"
	.section	.text._ZN2v88internal8Builtins23Generate_WasmMemoryGrowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_WasmMemoryGrowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_WasmMemoryGrowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_WasmMemoryGrowEPNS0_8compiler18CodeAssemblerStateE:
.LFB21982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$214, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$637, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L106
.L103:
	movq	%r13, %rdi
	call	_ZN2v88internal23WasmMemoryGrowAssembler26GenerateWasmMemoryGrowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L103
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21982:
	.size	_ZN2v88internal8Builtins23Generate_WasmMemoryGrowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_WasmMemoryGrowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21WasmTableGetAssembler24GenerateWasmTableGetImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21WasmTableGetAssembler24GenerateWasmTableGetImplEv
	.type	_ZN2v88internal21WasmTableGetAssembler24GenerateWasmTableGetImplEv, @function
_ZN2v88internal21WasmTableGetAssembler24GenerateWasmTableGetImplEv:
.LFB21995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	leaq	-208(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-216(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18IsValidPositiveSmiENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-216(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm0
	movl	$3, %edi
	movq	%r15, %r8
	leaq	-80(%rbp), %rbx
	pushq	%rdi
	movq	-224(%rbp), %rcx
	movq	%rax, %rdx
	pushq	%rbx
	movhps	-216(%rbp), %xmm0
	movl	$457, %esi
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r14, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$11, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$1, %edi
	movq	%r15, %r8
	movq	-216(%rbp), %rcx
	pushq	%rdi
	movq	%rax, %rdx
	movl	$444, %esi
	movq	%r12, %rdi
	pushq	%rbx
	movq	%rcx, -80(%rbp)
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L111:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21995:
	.size	_ZN2v88internal21WasmTableGetAssembler24GenerateWasmTableGetImplEv, .-_ZN2v88internal21WasmTableGetAssembler24GenerateWasmTableGetImplEv
	.section	.rodata._ZN2v88internal8Builtins21Generate_WasmTableGetEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"WasmTableGet"
	.section	.text._ZN2v88internal8Builtins21Generate_WasmTableGetEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_WasmTableGetEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_WasmTableGetEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_WasmTableGetEPNS0_8compiler18CodeAssemblerStateE:
.LFB21991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$236, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$638, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L116
.L113:
	movq	%r13, %rdi
	call	_ZN2v88internal21WasmTableGetAssembler24GenerateWasmTableGetImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L113
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21991:
	.size	_ZN2v88internal8Builtins21Generate_WasmTableGetEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_WasmTableGetEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21WasmTableSetAssembler24GenerateWasmTableSetImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21WasmTableSetAssembler24GenerateWasmTableSetImplEv
	.type	_ZN2v88internal21WasmTableSetAssembler24GenerateWasmTableSetImplEv, @function
_ZN2v88internal21WasmTableSetAssembler24GenerateWasmTableSetImplEv:
.LFB22004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-232(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18IsValidPositiveSmiENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-232(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm0
	movq	%r15, %r8
	movq	%r14, %rcx
	movhps	-240(%rbp), %xmm0
	leaq	-96(%rbp), %rbx
	movl	$4, %edi
	movq	%rax, %rdx
	pushq	%rdi
	movl	$458, %esi
	movq	%r12, %rdi
	pushq	%rbx
	movaps	%xmm0, -96(%rbp)
	movq	-248(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$11, %edi
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$1, %edi
	movq	%r15, %r8
	movq	-232(%rbp), %rcx
	pushq	%rdi
	movq	%rax, %rdx
	movl	$444, %esi
	movq	%r12, %rdi
	pushq	%rbx
	movq	%rcx, -96(%rbp)
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L121:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22004:
	.size	_ZN2v88internal21WasmTableSetAssembler24GenerateWasmTableSetImplEv, .-_ZN2v88internal21WasmTableSetAssembler24GenerateWasmTableSetImplEv
	.section	.rodata._ZN2v88internal8Builtins21Generate_WasmTableSetEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"WasmTableSet"
	.section	.text._ZN2v88internal8Builtins21Generate_WasmTableSetEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_WasmTableSetEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_WasmTableSetEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_WasmTableSetEPNS0_8compiler18CodeAssemblerStateE:
.LFB22000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$262, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$639, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L126
.L123:
	movq	%r13, %rdi
	call	_ZN2v88internal21WasmTableSetAssembler24GenerateWasmTableSetImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L123
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22000:
	.size	_ZN2v88internal8Builtins21Generate_WasmTableSetEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_WasmTableSetEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24WasmI64ToBigIntAssembler27GenerateWasmI64ToBigIntImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24WasmI64ToBigIntAssembler27GenerateWasmI64ToBigIntImplEv
	.type	_ZN2v88internal24WasmI64ToBigIntAssembler27GenerateWasmI64ToBigIntImplEv, @function
_ZN2v88internal24WasmI64ToBigIntAssembler27GenerateWasmI64ToBigIntImplEv:
.LFB22013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L133
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$25840, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	leaq	-64(%rbp), %rsi
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal21I64ToBigIntDescriptorE(%rip), %rdx
	movq	%rax, %rcx
	leaq	520+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rbx, -48(%rbp)
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	movl	$1, %r9d
	movq	%r13, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L128:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L128
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22013:
	.size	_ZN2v88internal24WasmI64ToBigIntAssembler27GenerateWasmI64ToBigIntImplEv, .-_ZN2v88internal24WasmI64ToBigIntAssembler27GenerateWasmI64ToBigIntImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_WasmI64ToBigIntEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"WasmI64ToBigInt"
	.section	.text._ZN2v88internal8Builtins24Generate_WasmI64ToBigIntEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_WasmI64ToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_WasmI64ToBigIntEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_WasmI64ToBigIntEPNS0_8compiler18CodeAssemblerStateE:
.LFB22009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$288, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$658, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L139
.L136:
	movq	%r13, %rdi
	call	_ZN2v88internal24WasmI64ToBigIntAssembler27GenerateWasmI64ToBigIntImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L136
.L140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22009:
	.size	_ZN2v88internal8Builtins24Generate_WasmI64ToBigIntEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_WasmI64ToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28WasmI32PairToBigIntAssembler31GenerateWasmI32PairToBigIntImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28WasmI32PairToBigIntAssembler31GenerateWasmI32PairToBigIntImplEv
	.type	_ZN2v88internal28WasmI32PairToBigIntAssembler31GenerateWasmI32PairToBigIntImplEv, @function
_ZN2v88internal28WasmI32PairToBigIntAssembler31GenerateWasmI32PairToBigIntImplEv:
.LFB22022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler4Is32Ev@PLT
	testb	%al, %al
	je	.L146
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$25848, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	leaq	-64(%rbp), %rsi
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal25I32PairToBigIntDescriptorE(%rip), %rdx
	movq	%rax, %rcx
	movl	$2, %r9d
	leaq	560+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r13, %rdx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	-80(%rbp), %xmm0
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L141:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L141
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22022:
	.size	_ZN2v88internal28WasmI32PairToBigIntAssembler31GenerateWasmI32PairToBigIntImplEv, .-_ZN2v88internal28WasmI32PairToBigIntAssembler31GenerateWasmI32PairToBigIntImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_WasmI32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"WasmI32PairToBigInt"
	.section	.text._ZN2v88internal8Builtins28Generate_WasmI32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_WasmI32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_WasmI32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_WasmI32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE:
.LFB22018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$301, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$659, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L152
.L149:
	movq	%r13, %rdi
	call	_ZN2v88internal28WasmI32PairToBigIntAssembler31GenerateWasmI32PairToBigIntImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L149
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22018:
	.size	_ZN2v88internal8Builtins28Generate_WasmI32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_WasmI32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24WasmBigIntToI64Assembler27GenerateWasmBigIntToI64ImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24WasmBigIntToI64Assembler27GenerateWasmBigIntToI64ImplEv
	.type	_ZN2v88internal24WasmBigIntToI64Assembler27GenerateWasmBigIntToI64ImplEv, @function
_ZN2v88internal24WasmBigIntToI64Assembler27GenerateWasmBigIntToI64ImplEv:
.LFB22031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L159
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$25824, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-64(%rbp), %rsi
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	leaq	440+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rdx
	leaq	16+_ZTVN2v88internal21BigIntToI64DescriptorE(%rip), %rcx
	movl	$1, %r9d
	movq	%rax, -48(%rbp)
	movq	%rcx, %xmm0
	movq	%rdx, %xmm1
	movq	%r14, %rcx
	movq	%r13, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L154:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L154
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22031:
	.size	_ZN2v88internal24WasmBigIntToI64Assembler27GenerateWasmBigIntToI64ImplEv, .-_ZN2v88internal24WasmBigIntToI64Assembler27GenerateWasmBigIntToI64ImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_WasmBigIntToI64EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"WasmBigIntToI64"
	.section	.text._ZN2v88internal8Builtins24Generate_WasmBigIntToI64EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_WasmBigIntToI64EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_WasmBigIntToI64EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_WasmBigIntToI64EPNS0_8compiler18CodeAssemblerStateE:
.LFB22027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$315, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$660, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L165
.L162:
	movq	%r13, %rdi
	call	_ZN2v88internal24WasmBigIntToI64Assembler27GenerateWasmBigIntToI64ImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L162
.L166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22027:
	.size	_ZN2v88internal8Builtins24Generate_WasmBigIntToI64EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_WasmBigIntToI64EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28WasmBigIntToI32PairAssembler31GenerateWasmBigIntToI32PairImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28WasmBigIntToI32PairAssembler31GenerateWasmBigIntToI32PairImplEv
	.type	_ZN2v88internal28WasmBigIntToI32PairAssembler31GenerateWasmBigIntToI32PairImplEv, @function
_ZN2v88internal28WasmBigIntToI32PairAssembler31GenerateWasmBigIntToI32PairImplEv:
.LFB22040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler4Is32Ev@PLT
	testb	%al, %al
	je	.L172
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$25832, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-64(%rbp), %rsi
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	leaq	480+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rdx
	leaq	16+_ZTVN2v88internal25BigIntToI32PairDescriptorE(%rip), %rcx
	movl	$1, %r9d
	movq	%rax, -48(%rbp)
	movq	%rcx, %xmm0
	movq	%rdx, %xmm1
	movq	%r14, %rcx
	movq	%r13, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L167:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L167
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22040:
	.size	_ZN2v88internal28WasmBigIntToI32PairAssembler31GenerateWasmBigIntToI32PairImplEv, .-_ZN2v88internal28WasmBigIntToI32PairAssembler31GenerateWasmBigIntToI32PairImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_WasmBigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"WasmBigIntToI32Pair"
	.section	.text._ZN2v88internal8Builtins28Generate_WasmBigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_WasmBigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_WasmBigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_WasmBigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE:
.LFB22036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$330, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$661, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L178
.L175:
	movq	%r13, %rdi
	call	_ZN2v88internal28WasmBigIntToI32PairAssembler31GenerateWasmBigIntToI32PairImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L175
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22036:
	.size	_ZN2v88internal8Builtins28Generate_WasmBigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_WasmBigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33ThrowWasmTrapUnreachableAssembler36GenerateThrowWasmTrapUnreachableImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33ThrowWasmTrapUnreachableAssembler36GenerateThrowWasmTrapUnreachableImplEv
	.type	_ZN2v88internal33ThrowWasmTrapUnreachableAssembler36GenerateThrowWasmTrapUnreachableImplEv, @function
_ZN2v88internal33ThrowWasmTrapUnreachableAssembler36GenerateThrowWasmTrapUnreachableImplEv:
.LFB22049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%edi, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L183:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22049:
	.size	_ZN2v88internal33ThrowWasmTrapUnreachableAssembler36GenerateThrowWasmTrapUnreachableImplEv, .-_ZN2v88internal33ThrowWasmTrapUnreachableAssembler36GenerateThrowWasmTrapUnreachableImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_ThrowWasmTrapUnreachableEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"ThrowWasmTrapUnreachable"
	.section	.text._ZN2v88internal8Builtins33Generate_ThrowWasmTrapUnreachableEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_ThrowWasmTrapUnreachableEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_ThrowWasmTrapUnreachableEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_ThrowWasmTrapUnreachableEPNS0_8compiler18CodeAssemblerStateE:
.LFB22045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$646, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L188
.L185:
	movq	%r13, %rdi
	call	_ZN2v88internal33ThrowWasmTrapUnreachableAssembler36GenerateThrowWasmTrapUnreachableImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L185
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22045:
	.size	_ZN2v88internal8Builtins33Generate_ThrowWasmTrapUnreachableEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_ThrowWasmTrapUnreachableEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36ThrowWasmTrapMemOutOfBoundsAssembler39GenerateThrowWasmTrapMemOutOfBoundsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ThrowWasmTrapMemOutOfBoundsAssembler39GenerateThrowWasmTrapMemOutOfBoundsImplEv
	.type	_ZN2v88internal36ThrowWasmTrapMemOutOfBoundsAssembler39GenerateThrowWasmTrapMemOutOfBoundsImplEv, @function
_ZN2v88internal36ThrowWasmTrapMemOutOfBoundsAssembler39GenerateThrowWasmTrapMemOutOfBoundsImplEv:
.LFB22058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$1, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L193:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22058:
	.size	_ZN2v88internal36ThrowWasmTrapMemOutOfBoundsAssembler39GenerateThrowWasmTrapMemOutOfBoundsImplEv, .-_ZN2v88internal36ThrowWasmTrapMemOutOfBoundsAssembler39GenerateThrowWasmTrapMemOutOfBoundsImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_ThrowWasmTrapMemOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"ThrowWasmTrapMemOutOfBounds"
	.section	.text._ZN2v88internal8Builtins36Generate_ThrowWasmTrapMemOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_ThrowWasmTrapMemOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_ThrowWasmTrapMemOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_ThrowWasmTrapMemOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$647, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L198
.L195:
	movq	%r13, %rdi
	call	_ZN2v88internal36ThrowWasmTrapMemOutOfBoundsAssembler39GenerateThrowWasmTrapMemOutOfBoundsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L195
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22054:
	.size	_ZN2v88internal8Builtins36Generate_ThrowWasmTrapMemOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_ThrowWasmTrapMemOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal37ThrowWasmTrapUnalignedAccessAssembler40GenerateThrowWasmTrapUnalignedAccessImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37ThrowWasmTrapUnalignedAccessAssembler40GenerateThrowWasmTrapUnalignedAccessImplEv
	.type	_ZN2v88internal37ThrowWasmTrapUnalignedAccessAssembler40GenerateThrowWasmTrapUnalignedAccessImplEv, @function
_ZN2v88internal37ThrowWasmTrapUnalignedAccessAssembler40GenerateThrowWasmTrapUnalignedAccessImplEv:
.LFB22067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$2, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L203:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22067:
	.size	_ZN2v88internal37ThrowWasmTrapUnalignedAccessAssembler40GenerateThrowWasmTrapUnalignedAccessImplEv, .-_ZN2v88internal37ThrowWasmTrapUnalignedAccessAssembler40GenerateThrowWasmTrapUnalignedAccessImplEv
	.section	.rodata._ZN2v88internal8Builtins37Generate_ThrowWasmTrapUnalignedAccessEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"ThrowWasmTrapUnalignedAccess"
	.section	.text._ZN2v88internal8Builtins37Generate_ThrowWasmTrapUnalignedAccessEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_ThrowWasmTrapUnalignedAccessEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_ThrowWasmTrapUnalignedAccessEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_ThrowWasmTrapUnalignedAccessEPNS0_8compiler18CodeAssemblerStateE:
.LFB22063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$648, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L208
.L205:
	movq	%r13, %rdi
	call	_ZN2v88internal37ThrowWasmTrapUnalignedAccessAssembler40GenerateThrowWasmTrapUnalignedAccessImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L209
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L205
.L209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22063:
	.size	_ZN2v88internal8Builtins37Generate_ThrowWasmTrapUnalignedAccessEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_ThrowWasmTrapUnalignedAccessEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31ThrowWasmTrapDivByZeroAssembler34GenerateThrowWasmTrapDivByZeroImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31ThrowWasmTrapDivByZeroAssembler34GenerateThrowWasmTrapDivByZeroImplEv
	.type	_ZN2v88internal31ThrowWasmTrapDivByZeroAssembler34GenerateThrowWasmTrapDivByZeroImplEv, @function
_ZN2v88internal31ThrowWasmTrapDivByZeroAssembler34GenerateThrowWasmTrapDivByZeroImplEv:
.LFB22076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$3, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L213
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L213:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22076:
	.size	_ZN2v88internal31ThrowWasmTrapDivByZeroAssembler34GenerateThrowWasmTrapDivByZeroImplEv, .-_ZN2v88internal31ThrowWasmTrapDivByZeroAssembler34GenerateThrowWasmTrapDivByZeroImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_ThrowWasmTrapDivByZeroEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"ThrowWasmTrapDivByZero"
	.section	.text._ZN2v88internal8Builtins31Generate_ThrowWasmTrapDivByZeroEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_ThrowWasmTrapDivByZeroEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_ThrowWasmTrapDivByZeroEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_ThrowWasmTrapDivByZeroEPNS0_8compiler18CodeAssemblerStateE:
.LFB22072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$649, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L218
.L215:
	movq	%r13, %rdi
	call	_ZN2v88internal31ThrowWasmTrapDivByZeroAssembler34GenerateThrowWasmTrapDivByZeroImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L219
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L215
.L219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22072:
	.size	_ZN2v88internal8Builtins31Generate_ThrowWasmTrapDivByZeroEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_ThrowWasmTrapDivByZeroEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40ThrowWasmTrapDivUnrepresentableAssembler43GenerateThrowWasmTrapDivUnrepresentableImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40ThrowWasmTrapDivUnrepresentableAssembler43GenerateThrowWasmTrapDivUnrepresentableImplEv
	.type	_ZN2v88internal40ThrowWasmTrapDivUnrepresentableAssembler43GenerateThrowWasmTrapDivUnrepresentableImplEv, @function
_ZN2v88internal40ThrowWasmTrapDivUnrepresentableAssembler43GenerateThrowWasmTrapDivUnrepresentableImplEv:
.LFB22085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$4, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L223:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22085:
	.size	_ZN2v88internal40ThrowWasmTrapDivUnrepresentableAssembler43GenerateThrowWasmTrapDivUnrepresentableImplEv, .-_ZN2v88internal40ThrowWasmTrapDivUnrepresentableAssembler43GenerateThrowWasmTrapDivUnrepresentableImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_ThrowWasmTrapDivUnrepresentableEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"ThrowWasmTrapDivUnrepresentable"
	.section	.text._ZN2v88internal8Builtins40Generate_ThrowWasmTrapDivUnrepresentableEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDivUnrepresentableEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDivUnrepresentableEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDivUnrepresentableEPNS0_8compiler18CodeAssemblerStateE:
.LFB22081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$650, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L228
.L225:
	movq	%r13, %rdi
	call	_ZN2v88internal40ThrowWasmTrapDivUnrepresentableAssembler43GenerateThrowWasmTrapDivUnrepresentableImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L225
.L229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22081:
	.size	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDivUnrepresentableEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDivUnrepresentableEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31ThrowWasmTrapRemByZeroAssembler34GenerateThrowWasmTrapRemByZeroImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31ThrowWasmTrapRemByZeroAssembler34GenerateThrowWasmTrapRemByZeroImplEv
	.type	_ZN2v88internal31ThrowWasmTrapRemByZeroAssembler34GenerateThrowWasmTrapRemByZeroImplEv, @function
_ZN2v88internal31ThrowWasmTrapRemByZeroAssembler34GenerateThrowWasmTrapRemByZeroImplEv:
.LFB22094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$5, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L233:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22094:
	.size	_ZN2v88internal31ThrowWasmTrapRemByZeroAssembler34GenerateThrowWasmTrapRemByZeroImplEv, .-_ZN2v88internal31ThrowWasmTrapRemByZeroAssembler34GenerateThrowWasmTrapRemByZeroImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_ThrowWasmTrapRemByZeroEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"ThrowWasmTrapRemByZero"
	.section	.text._ZN2v88internal8Builtins31Generate_ThrowWasmTrapRemByZeroEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_ThrowWasmTrapRemByZeroEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_ThrowWasmTrapRemByZeroEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_ThrowWasmTrapRemByZeroEPNS0_8compiler18CodeAssemblerStateE:
.LFB22090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$651, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L238
.L235:
	movq	%r13, %rdi
	call	_ZN2v88internal31ThrowWasmTrapRemByZeroAssembler34GenerateThrowWasmTrapRemByZeroImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L235
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22090:
	.size	_ZN2v88internal8Builtins31Generate_ThrowWasmTrapRemByZeroEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_ThrowWasmTrapRemByZeroEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal42ThrowWasmTrapFloatUnrepresentableAssembler45GenerateThrowWasmTrapFloatUnrepresentableImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal42ThrowWasmTrapFloatUnrepresentableAssembler45GenerateThrowWasmTrapFloatUnrepresentableImplEv
	.type	_ZN2v88internal42ThrowWasmTrapFloatUnrepresentableAssembler45GenerateThrowWasmTrapFloatUnrepresentableImplEv, @function
_ZN2v88internal42ThrowWasmTrapFloatUnrepresentableAssembler45GenerateThrowWasmTrapFloatUnrepresentableImplEv:
.LFB22103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$6, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L243:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22103:
	.size	_ZN2v88internal42ThrowWasmTrapFloatUnrepresentableAssembler45GenerateThrowWasmTrapFloatUnrepresentableImplEv, .-_ZN2v88internal42ThrowWasmTrapFloatUnrepresentableAssembler45GenerateThrowWasmTrapFloatUnrepresentableImplEv
	.section	.rodata._ZN2v88internal8Builtins42Generate_ThrowWasmTrapFloatUnrepresentableEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"ThrowWasmTrapFloatUnrepresentable"
	.section	.text._ZN2v88internal8Builtins42Generate_ThrowWasmTrapFloatUnrepresentableEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins42Generate_ThrowWasmTrapFloatUnrepresentableEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins42Generate_ThrowWasmTrapFloatUnrepresentableEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins42Generate_ThrowWasmTrapFloatUnrepresentableEPNS0_8compiler18CodeAssemblerStateE:
.LFB22099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$652, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L248
.L245:
	movq	%r13, %rdi
	call	_ZN2v88internal42ThrowWasmTrapFloatUnrepresentableAssembler45GenerateThrowWasmTrapFloatUnrepresentableImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L249
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L245
.L249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22099:
	.size	_ZN2v88internal8Builtins42Generate_ThrowWasmTrapFloatUnrepresentableEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins42Generate_ThrowWasmTrapFloatUnrepresentableEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33ThrowWasmTrapFuncInvalidAssembler36GenerateThrowWasmTrapFuncInvalidImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33ThrowWasmTrapFuncInvalidAssembler36GenerateThrowWasmTrapFuncInvalidImplEv
	.type	_ZN2v88internal33ThrowWasmTrapFuncInvalidAssembler36GenerateThrowWasmTrapFuncInvalidImplEv, @function
_ZN2v88internal33ThrowWasmTrapFuncInvalidAssembler36GenerateThrowWasmTrapFuncInvalidImplEv:
.LFB22112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$7, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L253
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L253:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22112:
	.size	_ZN2v88internal33ThrowWasmTrapFuncInvalidAssembler36GenerateThrowWasmTrapFuncInvalidImplEv, .-_ZN2v88internal33ThrowWasmTrapFuncInvalidAssembler36GenerateThrowWasmTrapFuncInvalidImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_ThrowWasmTrapFuncInvalidEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"ThrowWasmTrapFuncInvalid"
	.section	.text._ZN2v88internal8Builtins33Generate_ThrowWasmTrapFuncInvalidEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_ThrowWasmTrapFuncInvalidEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_ThrowWasmTrapFuncInvalidEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_ThrowWasmTrapFuncInvalidEPNS0_8compiler18CodeAssemblerStateE:
.LFB22108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$653, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L258
.L255:
	movq	%r13, %rdi
	call	_ZN2v88internal33ThrowWasmTrapFuncInvalidAssembler36GenerateThrowWasmTrapFuncInvalidImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L255
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22108:
	.size	_ZN2v88internal8Builtins33Generate_ThrowWasmTrapFuncInvalidEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_ThrowWasmTrapFuncInvalidEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal37ThrowWasmTrapFuncSigMismatchAssembler40GenerateThrowWasmTrapFuncSigMismatchImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37ThrowWasmTrapFuncSigMismatchAssembler40GenerateThrowWasmTrapFuncSigMismatchImplEv
	.type	_ZN2v88internal37ThrowWasmTrapFuncSigMismatchAssembler40GenerateThrowWasmTrapFuncSigMismatchImplEv, @function
_ZN2v88internal37ThrowWasmTrapFuncSigMismatchAssembler40GenerateThrowWasmTrapFuncSigMismatchImplEv:
.LFB22121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$8, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L263:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22121:
	.size	_ZN2v88internal37ThrowWasmTrapFuncSigMismatchAssembler40GenerateThrowWasmTrapFuncSigMismatchImplEv, .-_ZN2v88internal37ThrowWasmTrapFuncSigMismatchAssembler40GenerateThrowWasmTrapFuncSigMismatchImplEv
	.section	.rodata._ZN2v88internal8Builtins37Generate_ThrowWasmTrapFuncSigMismatchEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"ThrowWasmTrapFuncSigMismatch"
	.section	.text._ZN2v88internal8Builtins37Generate_ThrowWasmTrapFuncSigMismatchEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_ThrowWasmTrapFuncSigMismatchEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_ThrowWasmTrapFuncSigMismatchEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_ThrowWasmTrapFuncSigMismatchEPNS0_8compiler18CodeAssemblerStateE:
.LFB22117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$654, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L268
.L265:
	movq	%r13, %rdi
	call	_ZN2v88internal37ThrowWasmTrapFuncSigMismatchAssembler40GenerateThrowWasmTrapFuncSigMismatchImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L269
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L265
.L269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22117:
	.size	_ZN2v88internal8Builtins37Generate_ThrowWasmTrapFuncSigMismatchEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_ThrowWasmTrapFuncSigMismatchEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40ThrowWasmTrapDataSegmentDroppedAssembler43GenerateThrowWasmTrapDataSegmentDroppedImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40ThrowWasmTrapDataSegmentDroppedAssembler43GenerateThrowWasmTrapDataSegmentDroppedImplEv
	.type	_ZN2v88internal40ThrowWasmTrapDataSegmentDroppedAssembler43GenerateThrowWasmTrapDataSegmentDroppedImplEv, @function
_ZN2v88internal40ThrowWasmTrapDataSegmentDroppedAssembler43GenerateThrowWasmTrapDataSegmentDroppedImplEv:
.LFB22130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$9, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L273
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L273:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22130:
	.size	_ZN2v88internal40ThrowWasmTrapDataSegmentDroppedAssembler43GenerateThrowWasmTrapDataSegmentDroppedImplEv, .-_ZN2v88internal40ThrowWasmTrapDataSegmentDroppedAssembler43GenerateThrowWasmTrapDataSegmentDroppedImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_ThrowWasmTrapDataSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"ThrowWasmTrapDataSegmentDropped"
	.section	.text._ZN2v88internal8Builtins40Generate_ThrowWasmTrapDataSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDataSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDataSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDataSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE:
.LFB22126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$655, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L278
.L275:
	movq	%r13, %rdi
	call	_ZN2v88internal40ThrowWasmTrapDataSegmentDroppedAssembler43GenerateThrowWasmTrapDataSegmentDroppedImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L275
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22126:
	.size	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDataSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_ThrowWasmTrapDataSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40ThrowWasmTrapElemSegmentDroppedAssembler43GenerateThrowWasmTrapElemSegmentDroppedImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40ThrowWasmTrapElemSegmentDroppedAssembler43GenerateThrowWasmTrapElemSegmentDroppedImplEv
	.type	_ZN2v88internal40ThrowWasmTrapElemSegmentDroppedAssembler43GenerateThrowWasmTrapElemSegmentDroppedImplEv, @function
_ZN2v88internal40ThrowWasmTrapElemSegmentDroppedAssembler43GenerateThrowWasmTrapElemSegmentDroppedImplEv:
.LFB22139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$10, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L283:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22139:
	.size	_ZN2v88internal40ThrowWasmTrapElemSegmentDroppedAssembler43GenerateThrowWasmTrapElemSegmentDroppedImplEv, .-_ZN2v88internal40ThrowWasmTrapElemSegmentDroppedAssembler43GenerateThrowWasmTrapElemSegmentDroppedImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_ThrowWasmTrapElemSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"ThrowWasmTrapElemSegmentDropped"
	.section	.text._ZN2v88internal8Builtins40Generate_ThrowWasmTrapElemSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapElemSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapElemSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_ThrowWasmTrapElemSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE:
.LFB22135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$656, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L288
.L285:
	movq	%r13, %rdi
	call	_ZN2v88internal40ThrowWasmTrapElemSegmentDroppedAssembler43GenerateThrowWasmTrapElemSegmentDroppedImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L285
.L289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22135:
	.size	_ZN2v88internal8Builtins40Generate_ThrowWasmTrapElemSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_ThrowWasmTrapElemSegmentDroppedEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal38ThrowWasmTrapTableOutOfBoundsAssembler41GenerateThrowWasmTrapTableOutOfBoundsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal38ThrowWasmTrapTableOutOfBoundsAssembler41GenerateThrowWasmTrapTableOutOfBoundsImplEv
	.type	_ZN2v88internal38ThrowWasmTrapTableOutOfBoundsAssembler41GenerateThrowWasmTrapTableOutOfBoundsImplEv, @function
_ZN2v88internal38ThrowWasmTrapTableOutOfBoundsAssembler41GenerateThrowWasmTrapTableOutOfBoundsImplEv:
.LFB22148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %edx
	movl	$-16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19LoadFromParentFrameEiNS0_11MachineTypeE@PLT
	movl	$119, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$30496, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$11, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r8
	movq	%r14, %rcx
	movl	$1, %edi
	movq	%rax, %rdx
	movq	%rbx, -48(%rbp)
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movl	$444, %esi
	call	_ZN2v88internal8compiler13CodeAssembler29TailCallRuntimeWithCEntryImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_4CodeEEENS5_INS0_6ObjectEEESt16initializer_listISB_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L293
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L293:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22148:
	.size	_ZN2v88internal38ThrowWasmTrapTableOutOfBoundsAssembler41GenerateThrowWasmTrapTableOutOfBoundsImplEv, .-_ZN2v88internal38ThrowWasmTrapTableOutOfBoundsAssembler41GenerateThrowWasmTrapTableOutOfBoundsImplEv
	.section	.rodata._ZN2v88internal8Builtins38Generate_ThrowWasmTrapTableOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"ThrowWasmTrapTableOutOfBounds"
	.section	.text._ZN2v88internal8Builtins38Generate_ThrowWasmTrapTableOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins38Generate_ThrowWasmTrapTableOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins38Generate_ThrowWasmTrapTableOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins38Generate_ThrowWasmTrapTableOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$657, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L298
.L295:
	movq	%r13, %rdi
	call	_ZN2v88internal38ThrowWasmTrapTableOutOfBoundsAssembler41GenerateThrowWasmTrapTableOutOfBoundsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L295
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22144:
	.size	_ZN2v88internal8Builtins38Generate_ThrowWasmTrapTableOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins38Generate_ThrowWasmTrapTableOutOfBoundsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE:
.LFB28282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28282:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_WasmAllocateHeapNumberEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
