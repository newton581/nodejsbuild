	.file	"builtins-math-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21MathBuiltinsAssembler10MathMaxMinES4_S4_MNS1_17CodeStubAssemblerEFNS2_5TNodeINS1_8Float64TEEENS2_11SloppyTNodeIS9_EESC_EdEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21MathBuiltinsAssembler10MathMaxMinES4_S4_MNS1_17CodeStubAssemblerEFNS2_5TNodeINS1_8Float64TEEENS2_11SloppyTNodeIS9_EESC_EdEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21MathBuiltinsAssembler10MathMaxMinES4_S4_MNS1_17CodeStubAssemblerEFNS2_5TNodeINS1_8Float64TEEENS2_11SloppyTNodeIS9_EESC_EdEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB15409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	(%r8), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdi
	call	_ZN2v88internal17CodeStubAssembler23TruncateTaggedToFloat64EPNS0_8compiler4NodeES4_@PLT
	movq	32(%rbx), %r12
	movq	(%rbx), %r14
	addq	16(%rbx), %r12
	movq	24(%rbx), %rbx
	movq	%rax, %r13
	testb	$1, %bl
	je	.L8
	movq	(%r12), %rax
	movq	-1(%rax,%rbx), %rbx
.L8:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	*%rbx
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE15409:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21MathBuiltinsAssembler10MathMaxMinES4_S4_MNS1_17CodeStubAssemblerEFNS2_5TNodeINS1_8Float64TEEENS2_11SloppyTNodeIS9_EESC_EdEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21MathBuiltinsAssembler10MathMaxMinES4_S4_MNS1_17CodeStubAssemblerEFNS2_5TNodeINS1_8Float64TEEENS2_11SloppyTNodeIS9_EESC_EdEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS2_8compiler4NodeES6_MNS2_17CodeStubAssemblerEFNS4_5TNodeINS2_8Float64TEEENS4_11SloppyTNodeIS9_EESC_EdEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS2_8compiler4NodeES6_MNS2_17CodeStubAssemblerEFNS4_5TNodeINS2_8Float64TEEENS4_11SloppyTNodeIS9_EESC_EdEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS2_8compiler4NodeES6_MNS2_17CodeStubAssemblerEFNS4_5TNodeINS2_8Float64TEEENS4_11SloppyTNodeIS9_EESC_EdEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB15411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L14
	cmpl	$3, %edx
	je	.L15
	cmpl	$1, %edx
	je	.L21
.L16:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L16
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15411:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS2_8compiler4NodeES6_MNS2_17CodeStubAssemblerEFNS4_5TNodeINS2_8Float64TEEENS4_11SloppyTNodeIS9_EESC_EdEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS2_8compiler4NodeES6_MNS2_17CodeStubAssemblerEFNS4_5TNodeINS2_8Float64TEEENS4_11SloppyTNodeIS9_EESC_EdEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZN2v88internal16MathAbsAssembler19GenerateMathAbsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathAbsAssembler19GenerateMathAbsImplEv
	.type	_ZN2v88internal16MathAbsAssembler19GenerateMathAbsImplEv, @function
_ZN2v88internal16MathAbsAssembler19GenerateMathAbsImplEv:
.LFB13307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-864(%rbp), %r14
	leaq	-704(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-832(%rbp), %rbx
	subq	$888, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -888(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -904(%rbp)
	movq	%r14, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%r15, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-576(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-880(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-448(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler32IsIntPtrAbsWithOverflowSupportedEv@PLT
	testb	%al, %al
	je	.L23
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrAbsWithOverflowENS1_11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-872(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	leaq	-320(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastWordToTaggedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
.L24:
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movsd	.LC1(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14NumberConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64AbsENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-872(%rbp), %rdi
	movl	$100, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-64(%rbp), %rcx
	xorl	%esi, %esi
	movl	$1, %ebx
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-888(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-848(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -848(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-904(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	leaq	-320(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-920(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -912(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-912(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-872(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9TrySmiSubENS0_8compiler5TNodeINS0_3SmiEEES5_PNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L24
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13307:
	.size	_ZN2v88internal16MathAbsAssembler19GenerateMathAbsImplEv, .-_ZN2v88internal16MathAbsAssembler19GenerateMathAbsImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/v8/src/builtins/builtins-math-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"MathAbs"
	.section	.text._ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE:
.LFB13303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$20, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$409, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L32
.L29:
	movq	%r13, %rdi
	call	_ZN2v88internal16MathAbsAssembler19GenerateMathAbsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L29
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13303:
	.size	_ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	.type	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE, @function
_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE:
.LFB13308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-768(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-704(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$776, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -800(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%r8, -808(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -816(%rbp)
	movq	%rdx, %rcx
	movl	$8, %edx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	%r15, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rbx
	leaq	-576(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-448(%rbp), %rdx
	movq	%rdx, %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r11, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-784(%rbp), %rcx
	movq	-776(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-784(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-320(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-792(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-808(%rbp), %rdi
	movq	-816(%rbp), %r9
	movq	%rax, %rsi
	addq	%r12, %rdi
	testb	$1, %r9b
	je	.L35
	movq	(%rdi), %rax
	movq	-1(%rax,%r9), %r9
.L35:
	call	*%r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21ChangeFloat64ToTaggedENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$100, %edx
	leaq	-736(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-64(%rbp), %rcx
	xorl	%esi, %esi
	movl	$1, %ebx
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-800(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-752(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -752(%rbp)
	movq	-720(%rbp), %rax
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13308:
	.size	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE, .-_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	.section	.text._ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed
	.type	_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed, @function
_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed:
.LFB13309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	leaq	-144(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	%r8, -208(%rbp)
	movsd	%xmm0, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movl	$13, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movsd	-200(%rbp), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Float64ConstantEd@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	$0, -152(%rbp)
	movq	$0, -160(%rbp)
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rax, -176(%rbp)
	movq	$0, -168(%rbp)
	subq	%rdx, %rcx
	cmpq	$7, %rcx
	jbe	.L51
	leaq	8(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L44:
	movq	%rdx, -168(%rbp)
	movl	$40, %edi
	movq	%rcx, -152(%rbp)
	movq	%r13, (%rdx)
	movq	%rcx, -160(%rbp)
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, 24(%rax)
	movq	-208(%rbp), %rbx
	leaq	-176(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r14, 8(%rax)
	leaq	-96(%rbp), %r14
	movq	%rbx, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS2_8compiler4NodeES6_MNS2_17CodeStubAssemblerEFNS4_5TNodeINS2_8Float64TEEENS4_11SloppyTNodeIS9_EESC_EdEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rbx
	movq	%r14, %rdx
	movq	%r13, (%rax)
	movq	%rbx, %xmm0
	movq	%r12, 16(%rax)
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21MathBuiltinsAssembler10MathMaxMinES4_S4_MNS1_17CodeStubAssemblerEFNS2_5TNodeINS1_8Float64TEEENS2_11SloppyTNodeIS9_EESC_EdEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L45
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L45:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21ChangeFloat64ToTaggedENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	8(%rax), %rcx
	jmp	.L44
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13309:
	.size	_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed, .-_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathCeilEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"MathCeil"
	.section	.text._ZN2v88internal8Builtins17Generate_MathCeilEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathCeilEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathCeilEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathCeilEPNS0_8compiler18CodeAssemblerStateE:
.LFB13321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$166, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$410, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L57
.L54:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	_ZN2v88internal17CodeStubAssembler11Float64CeilENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L54
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13321:
	.size	_ZN2v88internal8Builtins17Generate_MathCeilEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathCeilEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathCeilAssembler20GenerateMathCeilImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathCeilAssembler20GenerateMathCeilImplEv
	.type	_ZN2v88internal17MathCeilAssembler20GenerateMathCeilImplEv, @function
_ZN2v88internal17MathCeilAssembler20GenerateMathCeilImplEv:
.LFB13325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	_ZN2v88internal17CodeStubAssembler11Float64CeilENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	.cfi_endproc
.LFE13325:
	.size	_ZN2v88internal17MathCeilAssembler20GenerateMathCeilImplEv, .-_ZN2v88internal17MathCeilAssembler20GenerateMathCeilImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathFloorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"MathFloor"
	.section	.text._ZN2v88internal8Builtins18Generate_MathFloorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathFloorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathFloorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathFloorEPNS0_8compiler18CodeAssemblerStateE:
.LFB13330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$173, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$411, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L65
.L62:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	_ZN2v88internal17CodeStubAssembler12Float64FloorENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L62
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13330:
	.size	_ZN2v88internal8Builtins18Generate_MathFloorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathFloorEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathFloorAssembler21GenerateMathFloorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathFloorAssembler21GenerateMathFloorImplEv
	.type	_ZN2v88internal18MathFloorAssembler21GenerateMathFloorImplEv, @function
_ZN2v88internal18MathFloorAssembler21GenerateMathFloorImplEv:
.LFB13334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	_ZN2v88internal17CodeStubAssembler12Float64FloorENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	.cfi_endproc
.LFE13334:
	.size	_ZN2v88internal18MathFloorAssembler21GenerateMathFloorImplEv, .-_ZN2v88internal18MathFloorAssembler21GenerateMathFloorImplEv
	.section	.text._ZN2v88internal17MathImulAssembler20GenerateMathImulImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathImulAssembler20GenerateMathImulImplEv
	.type	_ZN2v88internal17MathImulAssembler20GenerateMathImulImplEv, @function
_ZN2v88internal17MathImulAssembler20GenerateMathImulImplEv:
.LFB13343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32MulENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19ChangeInt32ToTaggedENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13343:
	.size	_ZN2v88internal17MathImulAssembler20GenerateMathImulImplEv, .-_ZN2v88internal17MathImulAssembler20GenerateMathImulImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathImulEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"MathImul"
	.section	.text._ZN2v88internal8Builtins17Generate_MathImulEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathImulEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathImulEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathImulEPNS0_8compiler18CodeAssemblerStateE:
.LFB13339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$180, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$412, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L75
.L72:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathImulAssembler20GenerateMathImulImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L72
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13339:
	.size	_ZN2v88internal8Builtins17Generate_MathImulEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathImulEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21MathBuiltinsAssembler7MathPowEPNS0_8compiler4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21MathBuiltinsAssembler7MathPowEPNS0_8compiler4NodeES4_S4_
	.type	_ZN2v88internal21MathBuiltinsAssembler7MathPowEPNS0_8compiler4NodeES4_S4_, @function
_ZN2v88internal21MathBuiltinsAssembler7MathPowEPNS0_8compiler4NodeES4_S4_:
.LFB13344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler23TruncateTaggedToFloat64EPNS0_8compiler4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler23TruncateTaggedToFloat64EPNS0_8compiler4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10Float64PowENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21ChangeFloat64ToTaggedENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13344:
	.size	_ZN2v88internal21MathBuiltinsAssembler7MathPowEPNS0_8compiler4NodeES4_S4_, .-_ZN2v88internal21MathBuiltinsAssembler7MathPowEPNS0_8compiler4NodeES4_S4_
	.section	.text._ZN2v88internal16MathPowAssembler19GenerateMathPowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathPowAssembler19GenerateMathPowImplEv
	.type	_ZN2v88internal16MathPowAssembler19GenerateMathPowImplEv, @function
_ZN2v88internal16MathPowAssembler19GenerateMathPowImplEv:
.LFB13353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23TruncateTaggedToFloat64EPNS0_8compiler4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler23TruncateTaggedToFloat64EPNS0_8compiler4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10Float64PowENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21ChangeFloat64ToTaggedENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13353:
	.size	_ZN2v88internal16MathPowAssembler19GenerateMathPowImplEv, .-_ZN2v88internal16MathPowAssembler19GenerateMathPowImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathPowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"MathPow"
	.section	.text._ZN2v88internal8Builtins16Generate_MathPowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathPowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathPowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathPowEPNS0_8compiler18CodeAssemblerStateE:
.LFB13349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$201, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$415, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L85
.L82:
	movq	%r13, %rdi
	call	_ZN2v88internal16MathPowAssembler19GenerateMathPowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L82
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13349:
	.size	_ZN2v88internal8Builtins16Generate_MathPowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathPowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal19MathRandomAssembler22GenerateMathRandomImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19MathRandomAssembler22GenerateMathRandomImplEv
	.type	_ZN2v88internal19MathRandomAssembler22GenerateMathRandomImplEv, @function
_ZN2v88internal19MathRandomAssembler22GenerateMathRandomImplEv:
.LFB13362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-240(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$102, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-248(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18UintPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	call	_ZN2v88internal17ExternalReference18refill_math_randomEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$5, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$2, %r8d
	movw	%dx, -96(%rbp)
	movl	$1800, %edx
	movw	%cx, -80(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%r14, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-248(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movl	$102, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler19StoreContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movl	$104, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movl	$1549, %ecx
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27LoadFixedDoubleArrayElementENS0_8compiler11SloppyTNodeINS0_16FixedDoubleArrayEEEPNS2_4NodeENS0_11MachineTypeEiNS1_13ParameterModeEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	popq	%rsi
	popq	%rdi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L90:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13362:
	.size	_ZN2v88internal19MathRandomAssembler22GenerateMathRandomImplEv, .-_ZN2v88internal19MathRandomAssembler22GenerateMathRandomImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_MathRandomEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"MathRandom"
	.section	.text._ZN2v88internal8Builtins19Generate_MathRandomEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_MathRandomEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_MathRandomEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_MathRandomEPNS0_8compiler18CodeAssemblerStateE:
.LFB13358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$207, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$416, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L95
.L92:
	movq	%r13, %rdi
	call	_ZN2v88internal19MathRandomAssembler22GenerateMathRandomImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L92
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13358:
	.size	_ZN2v88internal8Builtins19Generate_MathRandomEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_MathRandomEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathRoundEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"MathRound"
	.section	.text._ZN2v88internal8Builtins18Generate_MathRoundEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathRoundEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathRoundEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathRoundEPNS0_8compiler18CodeAssemblerStateE:
.LFB13399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$248, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$417, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L101
.L98:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	_ZN2v88internal17CodeStubAssembler12Float64RoundENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L98
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13399:
	.size	_ZN2v88internal8Builtins18Generate_MathRoundEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathRoundEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathRoundAssembler21GenerateMathRoundImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathRoundAssembler21GenerateMathRoundImplEv
	.type	_ZN2v88internal18MathRoundAssembler21GenerateMathRoundImplEv, @function
_ZN2v88internal18MathRoundAssembler21GenerateMathRoundImplEv:
.LFB13403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	_ZN2v88internal17CodeStubAssembler12Float64RoundENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	.cfi_endproc
.LFE13403:
	.size	_ZN2v88internal18MathRoundAssembler21GenerateMathRoundImplEv, .-_ZN2v88internal18MathRoundAssembler21GenerateMathRoundImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathTruncEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"MathTrunc"
	.section	.text._ZN2v88internal8Builtins18Generate_MathTruncEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathTruncEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathTruncEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathTruncEPNS0_8compiler18CodeAssemblerStateE:
.LFB13408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$255, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$418, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L109
.L106:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	_ZN2v88internal17CodeStubAssembler12Float64TruncENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L106
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13408:
	.size	_ZN2v88internal8Builtins18Generate_MathTruncEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathTruncEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathTruncAssembler21GenerateMathTruncImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathTruncAssembler21GenerateMathTruncImplEv
	.type	_ZN2v88internal18MathTruncAssembler21GenerateMathTruncImplEv, @function
_ZN2v88internal18MathTruncAssembler21GenerateMathTruncImplEv:
.LFB13412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	_ZN2v88internal17CodeStubAssembler12Float64TruncENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@GOTPCREL(%rip), %rcx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21MathBuiltinsAssembler21MathRoundingOperationEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EEE
	.cfi_endproc
.LFE13412:
	.size	_ZN2v88internal18MathTruncAssembler21GenerateMathTruncImplEv, .-_ZN2v88internal18MathTruncAssembler21GenerateMathTruncImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathMaxEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"MathMax"
	.section	.text._ZN2v88internal8Builtins16Generate_MathMaxEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathMaxEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathMaxEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathMaxEPNS0_8compiler18CodeAssemblerStateE:
.LFB13417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$262, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$413, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L117
.L114:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	.LC12(%rip), %xmm0
	movq	_ZN2v88internal8compiler13CodeAssembler10Float64MaxENS1_11SloppyTNodeINS0_8Float64TEEES5_@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L114
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13417:
	.size	_ZN2v88internal8Builtins16Generate_MathMaxEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathMaxEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal16MathMaxAssembler19GenerateMathMaxImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathMaxAssembler19GenerateMathMaxImplEv
	.type	_ZN2v88internal16MathMaxAssembler19GenerateMathMaxImplEv, @function
_ZN2v88internal16MathMaxAssembler19GenerateMathMaxImplEv:
.LFB13421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movsd	.LC12(%rip), %xmm0
	movq	_ZN2v88internal8compiler13CodeAssembler10Float64MaxENS1_11SloppyTNodeINS0_8Float64TEEES5_@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed
	.cfi_endproc
.LFE13421:
	.size	_ZN2v88internal16MathMaxAssembler19GenerateMathMaxImplEv, .-_ZN2v88internal16MathMaxAssembler19GenerateMathMaxImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathMinEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"MathMin"
	.section	.text._ZN2v88internal8Builtins16Generate_MathMinEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathMinEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathMinEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathMinEPNS0_8compiler18CodeAssemblerStateE:
.LFB13426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$271, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$414, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L125
.L122:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	.LC14(%rip), %xmm0
	movq	_ZN2v88internal8compiler13CodeAssembler10Float64MinENS1_11SloppyTNodeINS0_8Float64TEEES5_@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L122
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13426:
	.size	_ZN2v88internal8Builtins16Generate_MathMinEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathMinEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal16MathMinAssembler19GenerateMathMinImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathMinAssembler19GenerateMathMinImplEv
	.type	_ZN2v88internal16MathMinAssembler19GenerateMathMinImplEv, @function
_ZN2v88internal16MathMinAssembler19GenerateMathMinImplEv:
.LFB13430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movsd	.LC14(%rip), %xmm0
	movq	_ZN2v88internal8compiler13CodeAssembler10Float64MinENS1_11SloppyTNodeINS0_8Float64TEEES5_@GOTPCREL(%rip), %rcx
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21MathBuiltinsAssembler10MathMaxMinEPNS0_8compiler4NodeES4_MNS0_17CodeStubAssemblerEFNS2_5TNodeINS0_8Float64TEEENS2_11SloppyTNodeIS7_EESA_Ed
	.cfi_endproc
.LFE13430:
	.size	_ZN2v88internal16MathMinAssembler19GenerateMathMinImplEv, .-_ZN2v88internal16MathMinAssembler19GenerateMathMinImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE:
.LFB17226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17226:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_MathAbsEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1105199104
	.align 8
.LC12:
	.long	0
	.long	-1048576
	.align 8
.LC14:
	.long	0
	.long	2146435072
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
