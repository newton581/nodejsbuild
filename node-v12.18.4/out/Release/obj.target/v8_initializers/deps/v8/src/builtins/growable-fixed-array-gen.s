	.file	"growable-fixed-array-gen.cc"
	.text
	.section	.text._ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE
	.type	_ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE, @function
_ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE:
.LFB13309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rdi), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r10, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$312, %rsp
	movq	%rsi, -352(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r10, -344(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	48(%r12), %r9
	movq	%r9, %rdi
	movq	%r9, -336(%rbp)
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	16(%r12), %r15
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-328(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-336(%rbp), %r9
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%r9, -328(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-328(%rbp), %r9
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, -328(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	%r13, %rcx
	pushq	$0
	movq	-328(%rbp), %r8
	movq	%rax, %rsi
	movq	%r12, %rdi
	pushq	$0
	movl	$1, %r9d
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler17ExtractFixedArrayEPNS0_8compiler4NodeES4_S4_S4_NS_4base5FlagsINS1_21ExtractFixedArrayFlagEiEENS1_13ParameterModeEPNS2_26TypedCodeAssemblerVariableINS0_5BoolTEEES4_@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	pushq	$1
	movq	-352(%rbp), %r11
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-344(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13309:
	.size	_ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE, .-_ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE
	.section	.text._ZN2v88internal18GrowableFixedArray9ToJSArrayENS0_8compiler5TNodeINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18GrowableFixedArray9ToJSArrayENS0_8compiler5TNodeINS0_7ContextEEE
	.type	_ZN2v88internal18GrowableFixedArray9ToJSArrayENS0_8compiler5TNodeINS0_7ContextEEE, @function
_ZN2v88internal18GrowableFixedArray9ToJSArrayENS0_8compiler5TNodeINS0_7ContextEEE:
.LFB13310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16(%r12), %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	32(%r12), %r10
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	48(%r12), %r11
	movq	%r11, %rdi
	movq	%r11, -200(%rbp)
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%r13, %r8
	movq	%r13, %rcx
	pushq	$0
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%rax, %rsi
	pushq	$0
	movq	%r12, %rdi
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler17ExtractFixedArrayEPNS0_8compiler4NodeES4_S4_S4_NS_4base5FlagsINS1_21ExtractFixedArrayFlagEiEENS1_13ParameterModeEPNS2_26TypedCodeAssemblerVariableINS0_5BoolTEEES4_@PLT
	addq	$32, %rsp
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-200(%rbp), %r11
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rax, %rdx
	movl	$32, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_14FixedArrayBaseEEENS3_INS0_3SmiEEEPNS2_4NodeEi@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L9
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13310:
	.size	_ZN2v88internal18GrowableFixedArray9ToJSArrayENS0_8compiler5TNodeINS0_7ContextEEE, .-_ZN2v88internal18GrowableFixedArray9ToJSArrayENS0_8compiler5TNodeINS0_7ContextEEE
	.section	.text._ZN2v88internal18GrowableFixedArray11NewCapacityENS0_8compiler5TNodeINS0_7IntPtrTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18GrowableFixedArray11NewCapacityENS0_8compiler5TNodeINS0_7IntPtrTEEE
	.type	_ZN2v88internal18GrowableFixedArray11NewCapacityENS0_8compiler5TNodeINS0_7IntPtrTEEE, @function
_ZN2v88internal18GrowableFixedArray11NewCapacityENS0_8compiler5TNodeINS0_7IntPtrTEEE:
.LFB13311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$16, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13311:
	.size	_ZN2v88internal18GrowableFixedArray11NewCapacityENS0_8compiler5TNodeINS0_7IntPtrTEEE, .-_ZN2v88internal18GrowableFixedArray11NewCapacityENS0_8compiler5TNodeINS0_7IntPtrTEEE
	.section	.text._ZN2v88internal18GrowableFixedArray16ResizeFixedArrayENS0_8compiler5TNodeINS0_7IntPtrTEEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18GrowableFixedArray16ResizeFixedArrayENS0_8compiler5TNodeINS0_7IntPtrTEEES5_
	.type	_ZN2v88internal18GrowableFixedArray16ResizeFixedArrayENS0_8compiler5TNodeINS0_7IntPtrTEEES5_, @function
_ZN2v88internal18GrowableFixedArray16ResizeFixedArrayENS0_8compiler5TNodeINS0_7IntPtrTEEES5_:
.LFB13312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	pushq	$0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %r9d
	pushq	$0
	xorl	%edx, %edx
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler17ExtractFixedArrayEPNS0_8compiler4NodeES4_S4_S4_NS_4base5FlagsINS1_21ExtractFixedArrayFlagEiEENS1_13ParameterModeEPNS2_26TypedCodeAssemblerVariableINS0_5BoolTEEES4_@PLT
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13312:
	.size	_ZN2v88internal18GrowableFixedArray16ResizeFixedArrayENS0_8compiler5TNodeINS0_7IntPtrTEEES5_, .-_ZN2v88internal18GrowableFixedArray16ResizeFixedArrayENS0_8compiler5TNodeINS0_7IntPtrTEEES5_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE, @function
_GLOBAL__sub_I__ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE:
.LFB17028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17028:
	.size	_GLOBAL__sub_I__ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE, .-_GLOBAL__sub_I__ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
