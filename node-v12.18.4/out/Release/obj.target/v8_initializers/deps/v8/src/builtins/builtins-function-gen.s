	.file	"builtins-function-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB15299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %r15
	movq	16(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	8(%rbx), %r14
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%rax, %r12
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	movq	%r15, %rcx
	movq	%r12, %rdx
	pushq	$1
	movq	%r14, %rsi
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	movl	$4, %r8d
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	16(%rbx), %rsi
	popq	%rax
	movl	$1, %ecx
	movq	(%rbx), %rdi
	popq	%rdx
	leaq	-40(%rbp), %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	.cfi_endproc
.LFE15299:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB15301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L10
	cmpl	$3, %edx
	je	.L11
	cmpl	$1, %edx
	je	.L17
.L12:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L12
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15301:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEv
	.type	_ZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEv, @function
_ZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEv:
.LFB13326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$664, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	leaq	-528(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$1104, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movl	$1105, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	leaq	-96(%rbp), %rax
	jne	.L45
	movq	%rax, -616(%rbp)
	leaq	-224(%rbp), %r13
.L19:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsDictionaryMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L47
.L21:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler26LoadNumberOfOwnDescriptorsENS0_8compiler5TNodeINS0_3MapEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L48
.L23:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadMapDescriptorsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler24LoadKeyByDescriptorEntryENS0_8compiler5TNodeINS0_15DescriptorArrayEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal17CodeStubAssembler20LengthStringConstantEv@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler26LoadValueByDescriptorEntryENS0_8compiler5TNodeINS0_15DescriptorArrayEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-624(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17IsAccessorInfoMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24LoadKeyByDescriptorEntryENS0_8compiler5TNodeINS0_15DescriptorArrayEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18NameStringConstantEv@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler26LoadValueByDescriptorEntryENS0_8compiler5TNodeINS0_15DescriptorArrayEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17IsAccessorInfoMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L49
.L25:
	leaq	-608(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	-352(%rbp), %r15
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	16(%rax), %rcx
	movq	24(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$7, %rdx
	jbe	.L50
	leaq	8(%rcx), %rdx
	movq	%rdx, 16(%rax)
.L28:
	movq	-624(%rbp), %rax
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rcx, -640(%rbp)
	movq	%rax, (%rcx)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-640(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsConstructorMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-632(%rbp), %r9
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-624(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-632(%rbp), %r9
	movl	$31, %edx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-624(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L51
.L29:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-624(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L52
.L31:
	leaq	-592(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-560(%rbp), %rax
	movl	$1, %r8d
	movq	%rbx, -560(%rbp)
	movq	%rax, %rcx
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-648(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler21Uint32LessThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-576(%rbp), %rbx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$1, %ecx
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%rax, %rdx
	movl	$2, %esi
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	$0, -552(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -536(%rbp)
	movq	16(%rax), %rdx
	movq	24(%rax), %rcx
	subq	%rdx, %rcx
	cmpq	$7, %rcx
	jbe	.L53
	leaq	8(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L34:
	movq	%rdx, -552(%rbp)
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rcx, -536(%rbp)
	movq	%rbx, (%rdx)
	movq	%rcx, -544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	movq	%rax, -696(%rbp)
	call	_Znwm@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movhps	-672(%rbp), %xmm0
	movq	%rbx, 16(%rax)
	movq	-616(%rbp), %rdx
	movl	$1, %r9d
	movups	%xmm0, (%rax)
	movq	-632(%rbp), %rsi
	movq	%rcx, %xmm0
	movq	-656(%rbp), %rdi
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	-696(%rbp), %rcx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L35
	movq	-616(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L35:
	movq	-672(%rbp), %rsi
	movq	-640(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	-640(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L54
.L36:
	movq	-632(%rbp), %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-632(%rbp), %rax
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-632(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-656(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal17CodeStubArguments7AtIndexEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L55
.L38:
	xorl	%edx, %edx
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	-624(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movl	$24, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-688(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-632(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$32, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-640(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$40, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movl	$8, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-672(%rbp), %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-656(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	8(%r12), %rdi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler19LoadTargetFromFrameEv@PLT
	movq	%r12, %rdi
	movq	%rax, -656(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$345, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-616(%rbp), %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-656(%rbp), %xmm0
	movq	-664(%rbp), %rcx
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movl	$3, %r9d
	movhps	-680(%rbp), %xmm0
	movq	%rax, -344(%rbp)
	movq	-648(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-640(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-624(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	leaq	-224(%rbp), %r13
	xorl	%edx, %edx
	leaq	-80(%rbp), %r15
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%r15, -96(%rbp)
	movq	$39, -224(%rbp)
	movq	%rax, -616(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movl	$28271, %esi
	movq	%r12, %rdi
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC2(%rip), %xmm0
	movw	%si, 36(%rax)
	movq	-616(%rbp), %rsi
	movl	$1769235310, 32(%rax)
	movb	$115, 38(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L19
	call	_ZdlPv@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-616(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	$35, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC6(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movl	$24941, %ecx
	movq	-616(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC7(%rip), %xmm0
	movw	%cx, 32(%rax)
	movb	$112, 34(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L25
	call	_ZdlPv@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-616(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	$32, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC5(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L23
	call	_ZdlPv@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-616(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	$29, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC3(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7791360814789845280, %rcx
	movq	%rax, -96(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$1952935525, 24(%rax)
	movb	$104, 28(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L21
	call	_ZdlPv@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-616(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$28, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC11(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$6998720764839292277, %rcx
	movq	%rax, -96(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$2036429426, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L31
	call	_ZdlPv@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-616(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -632(%rbp)
	movq	$51, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC8(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$28521, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC9(%rip), %xmm0
	movw	%dx, 48(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC10(%rip), %xmm0
	movb	$110, 50(%rax)
	movups	%xmm0, 32(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-632(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L29
	call	_ZdlPv@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-616(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$37, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC13(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC14(%rip), %xmm0
	movl	$1869182051, 32(%rax)
	movb	$110, 36(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L38
	call	_ZdlPv@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-616(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -672(%rbp)
	movq	$24, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$8243124888016217458, %rcx
	movq	%rax, -96(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-672(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L36
	call	_ZdlPv@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	8(%rax), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L28
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13326:
	.size	_ZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEv, .-_ZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"../deps/v8/src/builtins/builtins-function-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"FastFunctionPrototypeBind"
	.section	.text._ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE:
.LFB13322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$15, %ecx
	leaq	.LC15(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$346, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L61
.L58:
	movq	%r13, %rdi
	call	_ZN2v88internal34FastFunctionPrototypeBindAssembler37GenerateFastFunctionPrototypeBindImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L58
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13322:
	.size	_ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins37Generate_FunctionPrototypeHasInstanceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"FunctionPrototypeHasInstance"
	.section	.text._ZN2v88internal8Builtins37Generate_FunctionPrototypeHasInstanceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_FunctionPrototypeHasInstanceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_FunctionPrototypeHasInstanceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_FunctionPrototypeHasInstanceEPNS0_8compiler18CodeAssemblerStateE:
.LFB13335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$198, %ecx
	leaq	.LC15(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$348, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L67
.L64:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler19OrdinaryHasInstanceEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L64
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13335:
	.size	_ZN2v88internal8Builtins37Generate_FunctionPrototypeHasInstanceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_FunctionPrototypeHasInstanceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal37FunctionPrototypeHasInstanceAssembler40GenerateFunctionPrototypeHasInstanceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37FunctionPrototypeHasInstanceAssembler40GenerateFunctionPrototypeHasInstanceImplEv
	.type	_ZN2v88internal37FunctionPrototypeHasInstanceAssembler40GenerateFunctionPrototypeHasInstanceImplEv, @function
_ZN2v88internal37FunctionPrototypeHasInstanceAssembler40GenerateFunctionPrototypeHasInstanceImplEv:
.LFB13339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler19OrdinaryHasInstanceEPNS0_8compiler4NodeES4_S4_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13339:
	.size	_ZN2v88internal37FunctionPrototypeHasInstanceAssembler40GenerateFunctionPrototypeHasInstanceImplEv, .-_ZN2v88internal37FunctionPrototypeHasInstanceAssembler40GenerateFunctionPrototypeHasInstanceImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE:
.LFB17112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17112:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_FastFunctionPrototypeBindEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	8606216625675528516
	.quad	7453010313414795808
	.align 16
.LC2:
	.quad	8606216654648930080
	.quad	8459484569910865197
	.align 16
.LC3:
	.quad	7306000141102966851
	.quad	8245937468892930931
	.align 16
.LC4:
	.quad	7020584514718361667
	.quad	7791337780589651309
	.align 16
.LC5:
	.quad	8246126550717001317
	.quad	8315168235865862255
	.align 16
.LC6:
	.quad	8367799654119598147
	.quad	8388068008561501544
	.align 16
.LC7:
	.quad	7358992216702804512
	.quad	2336927755366788725
	.align 16
.LC8:
	.quad	8367821588417111382
	.quad	8246195779886408040
	.align 16
.LC9:
	.quad	7020302988135593071
	.quad	7022273398972113780
	.align 16
.LC10:
	.quad	8462090116383729522
	.quad	8386668381597623406
	.align 16
.LC11:
	.quad	7310575174828190785
	.quad	7454127125019784224
	.align 16
.LC12:
	.quad	7956010554822518084
	.quad	2334111957543690341
	.align 16
.LC13:
	.quad	7310575174828190785
	.quad	8315177770475353120
	.align 16
.LC14:
	.quad	7070765138884979829
	.quad	7959380205757166959
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
