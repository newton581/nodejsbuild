	.file	"builtins-sharedarraybuffer-gen.cc"
	.text
	.section	.text._ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	.type	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_, @function
_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_:
.LFB13299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -368(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -328(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -336(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17IsJSTypedArrayMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27LoadJSArrayBufferViewBufferENS0_8compiler5TNodeINS0_17JSArrayBufferViewEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler25LoadJSArrayBufferBitFieldENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-352(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-344(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$23, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$25, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler16Int32GreaterThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-368(%rbp), %r11
	movl	$103, %edx
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-328(%rbp), %rax
	movq	-360(%rbp), %r10
	movq	%r12, %rdi
	movq	%rbx, (%rax)
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler29LoadJSArrayBufferBackingStoreENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler31LoadJSArrayBufferViewByteOffsetENS0_8compiler5TNodeINS0_17JSArrayBufferViewEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-336(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rax, (%rcx)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13299:
	.size	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_, .-_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	.section	.text._ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_
	.type	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_, @function
_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_:
.LFB13300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$4, %edx
	subq	$312, %rsp
	movq	%rsi, -352(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rcx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	-352(%rbp), %r10
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler10ToSmiIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	-344(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, (%r9)
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$190, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13300:
	.size	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_, .-_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_
	.section	.text._ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_
	.type	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_, @function
_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_:
.LFB13301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdi, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadJSTypedArrayLengthENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$190, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13301:
	.size	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_, .-_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_
	.section	.text._ZN2v88internal34SharedArrayBufferBuiltinsAssembler18BigIntFromSigned64EPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler18BigIntFromSigned64EPNS0_8compiler4NodeE
	.type	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler18BigIntFromSigned64EPNS0_8compiler4NodeE, @function
_ZN2v88internal34SharedArrayBufferBuiltinsAssembler18BigIntFromSigned64EPNS0_8compiler4NodeE:
.LFB13302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L15
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler15BigIntFromInt64ENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler19BigIntFromInt32PairENS0_8compiler5TNodeINS0_7IntPtrTEEES5_@PLT
	.cfi_endproc
.LFE13302:
	.size	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler18BigIntFromSigned64EPNS0_8compiler4NodeE, .-_ZN2v88internal34SharedArrayBufferBuiltinsAssembler18BigIntFromSigned64EPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal34SharedArrayBufferBuiltinsAssembler20BigIntFromUnsigned64EPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler20BigIntFromUnsigned64EPNS0_8compiler4NodeE
	.type	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler20BigIntFromUnsigned64EPNS0_8compiler4NodeE, @function
_ZN2v88internal34SharedArrayBufferBuiltinsAssembler20BigIntFromUnsigned64EPNS0_8compiler4NodeE:
.LFB13303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L18
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler16BigIntFromUint64ENS0_8compiler5TNodeINS0_8UintPtrTEEE@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler20BigIntFromUint32PairENS0_8compiler5TNodeINS0_8UintPtrTEEES5_@PLT
	.cfi_endproc
.LFE13303:
	.size	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler20BigIntFromUnsigned64EPNS0_8compiler4NodeE, .-_ZN2v88internal34SharedArrayBufferBuiltinsAssembler20BigIntFromUnsigned64EPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal20AtomicsLoadAssembler23GenerateAtomicsLoadImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20AtomicsLoadAssembler23GenerateAtomicsLoadImplEv
	.type	_ZN2v88internal20AtomicsLoadAssembler23GenerateAtomicsLoadImplEv, @function
_ZN2v88internal20AtomicsLoadAssembler23GenerateAtomicsLoadImplEv:
.LFB13315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-928(%rbp), %rcx
	leaq	-416(%rbp), %rsi
	pushq	%r13
	leaq	-1056(%rbp), %rdx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-800(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %xmm2
	leaq	-672(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1184(%rbp), %rbx
	leaq	-544(%rbp), %r15
	movq	%r14, %xmm1
	movq	%r15, %xmm0
	subq	$1432, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -1376(%rbp)
	leaq	-1312(%rbp), %rax
	movq	%rbx, -1368(%rbp)
	movq	%rax, %xmm3
	movq	%rdi, -1352(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -1360(%rbp)
	movhps	-1376(%rbp), %xmm2
	movl	$1, %esi
	movhps	-1368(%rbp), %xmm3
	movaps	%xmm2, -1456(%rbp)
	movhps	-1352(%rbp), %xmm1
	movaps	%xmm3, -1472(%rbp)
	movhps	-1360(%rbp), %xmm0
	movaps	%xmm1, -1440(%rbp)
	movaps	%xmm0, -1424(%rbp)
	movq	%rdx, -1392(%rbp)
	movq	%rax, -1384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -1400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-1400(%rbp), %r9
	leaq	-1328(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	leaq	-1336(%rbp), %rcx
	movq	$0, -1336(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-1320(%rbp), %rcx
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_
	movq	-1400(%rbp), %r9
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-288(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-1384(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1368(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1392(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1376(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1352(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1360(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movdqa	.LC0(%rip), %xmm4
	movq	%rbx, %rdx
	movdqa	-1472(%rbp), %xmm3
	movq	-1336(%rbp), %rsi
	movl	$8, %r9d
	movq	%r12, %rdi
	movdqa	-1456(%rbp), %xmm2
	movdqa	-1440(%rbp), %xmm1
	movdqa	-1424(%rbp), %xmm0
	leaq	-128(%rbp), %r8
	movaps	%xmm4, -160(%rbp)
	movdqa	.LC1(%rip), %xmm4
	leaq	-160(%rbp), %rcx
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movl	$514, %esi
	movq	%r12, %rdi
	movq	-1328(%rbp), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movl	$770, %esi
	movq	%r12, %rdi
	movq	-1328(%rbp), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-1328(%rbp), %rdx
	movl	$515, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-1328(%rbp), %rdx
	movl	$771, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-1328(%rbp), %rdx
	movl	$516, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19ChangeInt32ToTaggedENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-1328(%rbp), %rdx
	movl	$772, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20ChangeUint32ToTaggedENS0_8compiler11SloppyTNodeINS0_7Uint32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-1328(%rbp), %rdx
	movl	$1285, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1400(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	movq	-1400(%rbp), %rdx
	testb	%al, %al
	je	.L21
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15BigIntFromInt64ENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, %rsi
.L22:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-1328(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1285, %esi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler10AtomicLoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L23
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16BigIntFromUint64ENS0_8compiler5TNodeINS0_8UintPtrTEEE@PLT
	movq	%rax, %rsi
.L24:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1360(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1352(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1376(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1392(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1384(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$1432, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -1424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1424(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1400(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler19BigIntFromInt32PairENS0_8compiler5TNodeINS0_7IntPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1400(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler20BigIntFromUint32PairENS0_8compiler5TNodeINS0_8UintPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L24
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13315:
	.size	_ZN2v88internal20AtomicsLoadAssembler23GenerateAtomicsLoadImplEv, .-_ZN2v88internal20AtomicsLoadAssembler23GenerateAtomicsLoadImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/v8/src/builtins/builtins-sharedarraybuffer-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"AtomicsLoad"
	.section	.text._ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE:
.LFB13311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$165, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$575, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L32
.L29:
	movq	%r13, %rdi
	call	_ZN2v88internal20AtomicsLoadAssembler23GenerateAtomicsLoadImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L29
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13311:
	.size	_ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_AtomicsLoadEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21AtomicsStoreAssembler24GenerateAtomicsStoreImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21AtomicsStoreAssembler24GenerateAtomicsStoreImplEv
	.type	_ZN2v88internal21AtomicsStoreAssembler24GenerateAtomicsStoreImplEv, @function
_ZN2v88internal21AtomicsStoreAssembler24GenerateAtomicsStoreImplEv:
.LFB13324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-528(%rbp), %rdx
	pushq	%r13
	movq	%rdx, %xmm0
	pushq	%r12
	punpcklqdq	%xmm0, %xmm0
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-784(%rbp), %rbx
	movq	%rbx, %xmm2
	punpcklqdq	%xmm2, %xmm2
	subq	$904, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-656(%rbp), %rax
	movaps	%xmm2, -944(%rbp)
	movq	%rax, %xmm1
	movaps	%xmm0, -912(%rbp)
	punpcklqdq	%xmm1, %xmm1
	movq	%rdx, -864(%rbp)
	movaps	%xmm1, -928(%rbp)
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-832(%rbp), %r8
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, %rdx
	leaq	-840(%rbp), %rcx
	movq	$0, -840(%rbp)
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-824(%rbp), %rcx
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	leaq	-272(%rbp), %r14
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	%rbx, -888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-856(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-400(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-864(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$22, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-840(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler16Int32GreaterThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rbx, -880(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-872(%rbp), %r11
	movq	%r11, %rdx
	movq	%r11, -896(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movdqa	.LC0(%rip), %xmm3
	movq	%r14, %rdx
	movdqa	-944(%rbp), %xmm2
	movq	-840(%rbp), %rsi
	movl	$6, %r9d
	movq	%r12, %rdi
	movdqa	-928(%rbp), %xmm1
	movdqa	-912(%rbp), %xmm0
	leaq	-112(%rbp), %r8
	leaq	-144(%rbp), %rcx
	movq	%rax, -872(%rbp)
	movabsq	$90194313238, %rax
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	movq	%r13, %rcx
	movl	$2, %esi
	movq	-872(%rbp), %r10
	movq	-832(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r10, %r8
	call	_ZN2v88internal8compiler13CodeAssembler11AtomicStoreENS0_21MachineRepresentationEPNS1_4NodeES5_S5_S5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	xorl	%r9d, %r9d
	movl	$3, %esi
	movq	%r12, %rdi
	movq	-872(%rbp), %r10
	movq	-832(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r10, %r8
	call	_ZN2v88internal8compiler13CodeAssembler11AtomicStoreENS0_21MachineRepresentationEPNS1_4NodeES5_S5_S5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	xorl	%r9d, %r9d
	movl	$4, %esi
	movq	%r12, %rdi
	movq	-872(%rbp), %r10
	movq	-832(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r10, %r8
	call	_ZN2v88internal8compiler13CodeAssembler11AtomicStoreENS0_21MachineRepresentationEPNS1_4NodeES5_S5_S5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-800(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-896(%rbp), %r11
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-816(%rbp), %r15
	movq	%r11, %rdx
	call	_ZN2v88internal17CodeStubAssembler8ToBigIntENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-872(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16BigIntToRawBytesENS0_8compiler5TNodeINS0_6BigIntEEEPNS2_26TypedCodeAssemblerVariableINS0_8UintPtrTEEES9_@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	xorl	%r9d, %r9d
	testb	%al, %al
	je	.L40
.L35:
	movq	%r15, %rdi
	movq	%r9, -912(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -896(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-912(%rbp), %r9
	movl	$5, %esi
	movq	%r12, %rdi
	movq	-896(%rbp), %r8
	movq	-832(%rbp), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler11AtomicStoreENS0_21MachineRepresentationEPNS1_4NodeES5_S5_S5_@PLT
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$904, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %r9
	jmp	.L35
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13324:
	.size	_ZN2v88internal21AtomicsStoreAssembler24GenerateAtomicsStoreImplEv, .-_ZN2v88internal21AtomicsStoreAssembler24GenerateAtomicsStoreImplEv
	.section	.rodata._ZN2v88internal8Builtins21Generate_AtomicsStoreEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"AtomicsStore"
	.section	.text._ZN2v88internal8Builtins21Generate_AtomicsStoreEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_AtomicsStoreEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_AtomicsStoreEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_AtomicsStoreEPNS0_8compiler18CodeAssemblerStateE:
.LFB13320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$236, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$576, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L46
.L43:
	movq	%r13, %rdi
	call	_ZN2v88internal21AtomicsStoreAssembler24GenerateAtomicsStoreImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L43
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13320:
	.size	_ZN2v88internal8Builtins21Generate_AtomicsStoreEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_AtomicsStoreEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24AtomicsExchangeAssembler27GenerateAtomicsExchangeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24AtomicsExchangeAssembler27GenerateAtomicsExchangeImplEv
	.type	_ZN2v88internal24AtomicsExchangeAssembler27GenerateAtomicsExchangeImplEv, @function
_ZN2v88internal24AtomicsExchangeAssembler27GenerateAtomicsExchangeImplEv:
.LFB13336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-1040(%rbp), %rdx
	leaq	-784(%rbp), %rsi
	pushq	%r13
	leaq	-1168(%rbp), %rcx
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-912(%rbp), %rdi
	movq	%rcx, %xmm1
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1296(%rbp), %rbx
	movq	%rdi, %xmm0
	subq	$1576, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -1504(%rbp)
	leaq	-1424(%rbp), %rax
	movq	%rbx, -1512(%rbp)
	movq	%rax, %xmm2
	movq	%rsi, -1496(%rbp)
	movl	$1, %esi
	movhps	-1504(%rbp), %xmm1
	movq	%rdi, -1536(%rbp)
	movq	%r12, %rdi
	movhps	-1512(%rbp), %xmm2
	movaps	%xmm1, -1600(%rbp)
	movhps	-1496(%rbp), %xmm0
	movaps	%xmm2, -1616(%rbp)
	movaps	%xmm0, -1584(%rbp)
	movq	%rcx, -1544(%rbp)
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-1472(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	leaq	-1480(%rbp), %rcx
	movq	$0, -1480(%rbp)
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-1464(%rbp), %rcx
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, %rdx
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-400(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-1520(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1512(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1544(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1504(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1536(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1496(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-656(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	leaq	-528(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-272(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$22, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler16Int32GreaterThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rbx, -1568(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movdqa	.LC0(%rip), %xmm3
	movq	%r12, %rdi
	movdqa	-1616(%rbp), %xmm2
	movdqa	-1600(%rbp), %xmm1
	movq	%rax, %rbx
	movl	$6, %r9d
	movdqa	-1584(%rbp), %xmm0
	movq	-1560(%rbp), %rdx
	leaq	-112(%rbp), %r8
	movq	-1480(%rbp), %rsi
	movabsq	$90194313238, %rax
	leaq	-144(%rbp), %rcx
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	-1472(%rbp), %rdx
	movl	$514, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1512(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	-1472(%rbp), %rdx
	movl	$770, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %r8
	xorl	%r9d, %r9d
	movl	$515, %esi
	movq	-1472(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %r8
	xorl	%r9d, %r9d
	movl	$771, %esi
	movq	-1472(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %r8
	xorl	%r9d, %r9d
	movl	$516, %esi
	movq	-1472(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19ChangeInt32ToTaggedENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %r8
	xorl	%r9d, %r9d
	movl	$772, %esi
	movq	-1472(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20ChangeUint32ToTaggedENS0_8compiler11SloppyTNodeINS0_7Uint32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-1456(%rbp), %r14
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler8ToBigIntENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal17CodeStubAssembler16BigIntToRawBytesENS0_8compiler5TNodeINS0_6BigIntEEEPNS2_26TypedCodeAssemblerVariableINS0_8UintPtrTEEES9_@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L58
.L49:
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1528(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1552(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-1528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %r9
	movl	$1285, %esi
	movq	%r12, %rdi
	movq	-1472(%rbp), %rdx
	movq	-1584(%rbp), %r8
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	movq	-1584(%rbp), %rdx
	testb	%al, %al
	je	.L50
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15BigIntFromInt64ENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, %rsi
.L51:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%r12, %rdi
	movq	%rbx, %r9
	movl	$1285, %esi
	movq	-1584(%rbp), %r8
	movq	-1472(%rbp), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14AtomicExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L52
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16BigIntFromUint64ENS0_8compiler5TNodeINS0_8UintPtrTEEE@PLT
	movq	%rax, %rsi
.L53:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1560(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1552(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1536(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1544(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1512(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1520(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$1576, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -1600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1600(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1584(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler19BigIntFromInt32PairENS0_8compiler5TNodeINS0_7IntPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20BigIntFromUint32PairENS0_8compiler5TNodeINS0_8UintPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %rbx
	jmp	.L49
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13336:
	.size	_ZN2v88internal24AtomicsExchangeAssembler27GenerateAtomicsExchangeImplEv, .-_ZN2v88internal24AtomicsExchangeAssembler27GenerateAtomicsExchangeImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_AtomicsExchangeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"AtomicsExchange"
	.section	.text._ZN2v88internal8Builtins24Generate_AtomicsExchangeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_AtomicsExchangeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_AtomicsExchangeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_AtomicsExchangeEPNS0_8compiler18CodeAssemblerStateE:
.LFB13332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$310, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$577, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L64
.L61:
	movq	%r13, %rdi
	call	_ZN2v88internal24AtomicsExchangeAssembler27GenerateAtomicsExchangeImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L61
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13332:
	.size	_ZN2v88internal8Builtins24Generate_AtomicsExchangeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_AtomicsExchangeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31AtomicsCompareExchangeAssembler34GenerateAtomicsCompareExchangeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31AtomicsCompareExchangeAssembler34GenerateAtomicsCompareExchangeImplEv
	.type	_ZN2v88internal31AtomicsCompareExchangeAssembler34GenerateAtomicsCompareExchangeImplEv, @function
_ZN2v88internal31AtomicsCompareExchangeAssembler34GenerateAtomicsCompareExchangeImplEv:
.LFB13345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-1040(%rbp), %rdx
	leaq	-784(%rbp), %rsi
	pushq	%r13
	leaq	-1168(%rbp), %rcx
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-912(%rbp), %rdi
	movq	%rcx, %xmm1
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1424(%rbp), %rbx
	movq	%rdi, %xmm0
	movq	%rbx, %xmm2
	subq	$1624, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1296(%rbp), %rax
	movq	%rdx, -1528(%rbp)
	movq	%rsi, -1536(%rbp)
	movl	$1, %esi
	movq	%rax, -1544(%rbp)
	movhps	-1528(%rbp), %xmm1
	movq	%rdi, -1576(%rbp)
	movq	%r12, %rdi
	movhps	-1536(%rbp), %xmm0
	movaps	%xmm1, -1648(%rbp)
	movhps	-1544(%rbp), %xmm2
	movaps	%xmm0, -1616(%rbp)
	movaps	%xmm2, -1664(%rbp)
	movq	%rcx, -1568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -1592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-1504(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, %rdx
	leaq	-1512(%rbp), %rcx
	movq	$0, -1512(%rbp)
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-1496(%rbp), %rcx
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	%rbx, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1544(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-400(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1568(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1528(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1536(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-656(%rbp), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	leaq	-528(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-272(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$22, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler16Int32GreaterThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rbx, -1624(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-1592(%rbp), %r10
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-1584(%rbp), %r11
	movq	%rax, %rbx
	movq	%r11, %rdx
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movdqa	.LC0(%rip), %xmm3
	movq	%r12, %rdi
	movdqa	-1664(%rbp), %xmm2
	movdqa	-1648(%rbp), %xmm1
	movq	%rax, %r14
	movl	$6, %r9d
	movdqa	-1616(%rbp), %xmm0
	movq	-1600(%rbp), %rdx
	leaq	-112(%rbp), %r8
	movq	-1512(%rbp), %rsi
	movabsq	$90194313238, %rax
	leaq	-144(%rbp), %rcx
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	$0
	movq	%r14, %r9
	movq	%rbx, %r8
	pushq	$0
	movq	-1504(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$514, %esi
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	$0
	movq	%r14, %r9
	movq	%rbx, %r8
	pushq	$0
	movq	-1504(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$770, %esi
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	pushq	$0
	movq	%r14, %r9
	movq	%rbx, %r8
	pushq	$0
	movq	-1504(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$515, %esi
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	pushq	$0
	movq	%r14, %r9
	movq	%rbx, %r8
	pushq	$0
	movq	-1504(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$771, %esi
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	pushq	$0
	movq	%r14, %r9
	movq	%rbx, %r8
	pushq	$0
	movq	-1504(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$516, %esi
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19ChangeInt32ToTaggedENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	pushq	$0
	movq	%rbx, %r8
	movq	%r14, %r9
	pushq	$0
	movq	-1504(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$772, %esi
	leaq	-1488(%rbp), %r14
	leaq	-1440(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20ChangeUint32ToTaggedENS0_8compiler11SloppyTNodeINS0_7Uint32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1592(%rbp), %r10
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler8ToBigIntENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	-1584(%rbp), %r11
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -1592(%rbp)
	leaq	-1456(%rbp), %r15
	movq	%r11, %rdx
	call	_ZN2v88internal17CodeStubAssembler8ToBigIntENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1472(%rbp), %rax
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-1592(%rbp), %r9
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-1616(%rbp), %rcx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler16BigIntToRawBytesENS0_8compiler5TNodeINS0_6BigIntEEEPNS2_26TypedCodeAssemblerVariableINS0_8UintPtrTEEES9_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-1584(%rbp), %r8
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler16BigIntToRawBytesENS0_8compiler5TNodeINS0_6BigIntEEEPNS2_26TypedCodeAssemblerVariableINS0_8UintPtrTEEES9_@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	movq	$0, -1584(%rbp)
	testb	%al, %al
	je	.L77
.L67:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	movq	$0, -1592(%rbp)
	testb	%al, %al
	je	.L78
.L68:
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1560(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1552(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-1560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -1664(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	pushq	-1592(%rbp)
	movq	%r12, %rdi
	movq	-1504(%rbp), %rdx
	pushq	-1584(%rbp)
	movl	$1285, %esi
	movq	%rax, %rcx
	movq	-1664(%rbp), %r9
	movq	-1648(%rbp), %r8
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1648(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	popq	%rsi
	movq	-1648(%rbp), %rdx
	testb	%al, %al
	popq	%rdi
	je	.L69
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15BigIntFromInt64ENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, %rsi
.L70:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -1664(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	pushq	-1592(%rbp)
	movq	%r12, %rdi
	movq	-1504(%rbp), %rdx
	pushq	-1584(%rbp)
	movq	%rax, %rcx
	movl	$1285, %esi
	movq	-1664(%rbp), %r9
	movq	-1648(%rbp), %r8
	call	_ZN2v88internal8compiler13CodeAssembler21AtomicCompareExchangeENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_S5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L71
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16BigIntFromUint64ENS0_8compiler5TNodeINS0_8UintPtrTEEE@PLT
	movq	%rax, %rsi
.L72:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1600(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1616(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1552(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1536(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1576(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1544(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -1664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1664(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1648(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler19BigIntFromInt32PairENS0_8compiler5TNodeINS0_7IntPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1584(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler20BigIntFromUint32PairENS0_8compiler5TNodeINS0_8UintPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-1616(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, -1584(%rbp)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, -1592(%rbp)
	jmp	.L68
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13345:
	.size	_ZN2v88internal31AtomicsCompareExchangeAssembler34GenerateAtomicsCompareExchangeImplEv, .-_ZN2v88internal31AtomicsCompareExchangeAssembler34GenerateAtomicsCompareExchangeImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_AtomicsCompareExchangeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"AtomicsCompareExchange"
	.section	.text._ZN2v88internal8Builtins31Generate_AtomicsCompareExchangeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_AtomicsCompareExchangeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_AtomicsCompareExchangeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_AtomicsCompareExchangeEPNS0_8compiler18CodeAssemblerStateE:
.LFB13341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$411, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$578, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L84
.L81:
	movq	%r13, %rdi
	call	_ZN2v88internal31AtomicsCompareExchangeAssembler34GenerateAtomicsCompareExchangeImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L81
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13341:
	.size	_ZN2v88internal8Builtins31Generate_AtomicsCompareExchangeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_AtomicsCompareExchangeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE:
.LFB13391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	leaq	-1040(%rbp), %r11
	.cfi_offset 14, -32
	movq	%r8, %r14
	leaq	-784(%rbp), %r10
	pushq	%r13
	leaq	-912(%rbp), %r9
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%r14, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-1168(%rbp), %rsi
	movq	%r9, %xmm0
	pushq	%rbx
	movq	%rsi, %xmm1
	leaq	-1472(%rbp), %r8
	subq	$1608, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movq	16(%rbp), %rax
	movq	%rcx, -1568(%rbp)
	leaq	-1480(%rbp), %rcx
	movq	%rbx, -1504(%rbp)
	leaq	-1424(%rbp), %rbx
	movq	%rbx, %xmm2
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	leaq	-1296(%rbp), %rdi
	movq	%r11, -1528(%rbp)
	movq	%r10, -1512(%rbp)
	movq	%rdi, -1520(%rbp)
	movq	%r12, %rdi
	movhps	-1528(%rbp), %xmm1
	movq	%rsi, -1544(%rbp)
	movq	%r13, %rsi
	movhps	-1512(%rbp), %xmm0
	movaps	%xmm1, -1632(%rbp)
	movhps	-1520(%rbp), %xmm2
	movaps	%xmm0, -1616(%rbp)
	movaps	%xmm2, -1648(%rbp)
	movq	%r9, -1552(%rbp)
	movq	%rbx, -1600(%rbp)
	movq	%rax, -1496(%rbp)
	movq	%rax, -1592(%rbp)
	movq	$0, -1480(%rbp)
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-1464(%rbp), %rcx
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler32ConvertTaggedAtomicIndexToWord32EPNS0_8compiler4NodeES4_PS4_
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, %rdx
	movq	%rbx, %r13
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler19ValidateAtomicIndexEPNS0_8compiler4NodeES4_S4_
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-656(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1544(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1528(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1552(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1512(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbx, -1536(%rbp)
	movl	$1, %r8d
	leaq	-528(%rbp), %rbx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbx, -1560(%rbp)
	movl	$1, %r8d
	leaq	-400(%rbp), %rbx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, -1584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-272(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -1576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$22, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler16Int32GreaterThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-1568(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movdqa	.LC0(%rip), %xmm3
	movq	%r12, %rdi
	movdqa	-1648(%rbp), %xmm2
	movdqa	-1632(%rbp), %xmm1
	movdqa	-1616(%rbp), %xmm0
	movabsq	$90194313238, %r9
	leaq	-144(%rbp), %rcx
	movq	-1576(%rbp), %rdx
	movq	%r9, -128(%rbp)
	leaq	-112(%rbp), %r8
	movq	%rax, %rbx
	movq	-1480(%rbp), %rsi
	movl	$6, %r9d
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %r11
	movq	-1504(%rbp), %rcx
	movq	%r11, %rax
	leaq	(%r12,%rcx), %r13
	andl	$1, %eax
	movq	%rax, -1504(%rbp)
	je	.L87
	movq	0(%r13), %rax
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-1472(%rbp), %rdx
	movl	$514, %esi
	movq	%r13, %rdi
	call	*-1(%rax,%r11)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	0(%r13), %rax
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-1496(%rbp), %r11
	movq	%r15, %rcx
	movl	$770, %esi
	movq	%r13, %rdi
	movq	-1472(%rbp), %rdx
	call	*-1(%rax,%r11)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	0(%r13), %rax
	movq	-1496(%rbp), %r11
	movq	-1(%rax,%r11), %r10
.L98:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movl	$515, %esi
	movq	-1616(%rbp), %r10
	movq	%rax, %rcx
	movq	-1472(%rbp), %rdx
	movq	%r13, %rdi
	call	*%r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rcx
	cmpq	$0, -1504(%rbp)
	movq	%rcx, %r10
	je	.L88
	movq	0(%r13), %rax
	movq	-1(%rax,%rcx), %r10
.L88:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movl	$771, %esi
	movq	-1616(%rbp), %r10
	movq	%rax, %rcx
	movq	-1472(%rbp), %rdx
	movq	%r13, %rdi
	call	*%r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rcx
	cmpq	$0, -1504(%rbp)
	movq	%rcx, %r10
	je	.L89
	movq	0(%r13), %rax
	movq	-1(%rax,%rcx), %r10
.L89:
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movl	$516, %esi
	movq	-1616(%rbp), %r10
	movq	%rax, %rcx
	movq	-1472(%rbp), %rdx
	movq	%r13, %rdi
	call	*%r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19ChangeInt32ToTaggedENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1512(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rcx
	cmpq	$0, -1504(%rbp)
	movq	%rcx, %r10
	je	.L90
	movq	0(%r13), %rax
	movq	-1(%rax,%rcx), %r10
.L90:
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r10, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %r8
	xorl	%r9d, %r9d
	movl	$772, %esi
	movq	-1616(%rbp), %r10
	movq	%rax, %rcx
	movq	-1472(%rbp), %rdx
	movq	%r13, %rdi
	call	*%r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20ChangeUint32ToTaggedENS0_8compiler11SloppyTNodeINS0_7Uint32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1568(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-1456(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler8ToBigIntENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1440(%rbp), %rax
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1568(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-1568(%rbp), %rcx
	xorl	%ebx, %ebx
	call	_ZN2v88internal17CodeStubAssembler16BigIntToRawBytesENS0_8compiler5TNodeINS0_6BigIntEEEPNS2_26TypedCodeAssemblerVariableINS0_8UintPtrTEEES9_@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L114
.L91:
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1536(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1560(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-1536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %rcx
	cmpq	$0, -1504(%rbp)
	movq	%rcx, %r10
	je	.L92
	movq	0(%r13), %rax
	movq	-1(%rax,%rcx), %r10
.L92:
	movq	%r14, %rdi
	movq	%r10, -1632(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %r9
	movl	$1285, %esi
	movq	%r13, %rdi
	movq	-1472(%rbp), %rdx
	movq	-1616(%rbp), %r8
	movq	%rax, %rcx
	movq	-1632(%rbp), %r10
	call	*%r10
	movq	%r12, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	movq	-1616(%rbp), %rdx
	testb	%al, %al
	je	.L93
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15BigIntFromInt64ENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, %rsi
.L94:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, -1504(%rbp)
	je	.L95
	movq	0(%r13), %rax
	movq	-1496(%rbp), %rcx
	movq	-1(%rax,%rcx), %rax
	movq	%rax, -1592(%rbp)
.L95:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -1496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%r13, %rdi
	movq	%rbx, %r9
	movl	$1285, %esi
	movq	%rax, %rcx
	movq	-1496(%rbp), %r8
	movq	-1472(%rbp), %rdx
	movq	-1592(%rbp), %rax
	call	*%rax
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L96
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16BigIntFromUint64ENS0_8compiler5TNodeINS0_8UintPtrTEEE@PLT
	movq	%rax, %rsi
.L97:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1576(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1536(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1512(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1552(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1544(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1520(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$1608, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -1632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1632(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-1616(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler19BigIntFromInt32PairENS0_8compiler5TNodeINS0_7IntPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-1472(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	$514, %esi
	movq	%r13, %rdi
	call	*%r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-1472(%rbp), %rdx
	movl	$770, %esi
	movq	%r13, %rdi
	movq	-1496(%rbp), %rax
	call	*%rax
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1496(%rbp), %r10
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler20BigIntFromUint32PairENS0_8compiler5TNodeINS0_8UintPtrTEEES5_@PLT
	movq	%rax, %rsi
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-1568(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %rbx
	jmp	.L91
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13391:
	.size	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE, .-_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	.section	.text._ZN2v88internal19AtomicsAddAssembler22GenerateAtomicsAddImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AtomicsAddAssembler22GenerateAtomicsAddImplEv
	.type	_ZN2v88internal19AtomicsAddAssembler22GenerateAtomicsAddImplEv, @function
_ZN2v88internal19AtomicsAddAssembler22GenerateAtomicsAddImplEv:
.LFB13354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicAddENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movl	$14, %r9d
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13354:
	.size	_ZN2v88internal19AtomicsAddAssembler22GenerateAtomicsAddImplEv, .-_ZN2v88internal19AtomicsAddAssembler22GenerateAtomicsAddImplEv
	.section	.text._ZN2v88internal19AtomicsSubAssembler22GenerateAtomicsSubImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AtomicsSubAssembler22GenerateAtomicsSubImplEv
	.type	_ZN2v88internal19AtomicsSubAssembler22GenerateAtomicsSubImplEv, @function
_ZN2v88internal19AtomicsSubAssembler22GenerateAtomicsSubImplEv:
.LFB13363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicSubENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movl	$20, %r9d
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13363:
	.size	_ZN2v88internal19AtomicsSubAssembler22GenerateAtomicsSubImplEv, .-_ZN2v88internal19AtomicsSubAssembler22GenerateAtomicsSubImplEv
	.section	.text._ZN2v88internal19AtomicsAndAssembler22GenerateAtomicsAndImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AtomicsAndAssembler22GenerateAtomicsAndImplEv
	.type	_ZN2v88internal19AtomicsAndAssembler22GenerateAtomicsAndImplEv, @function
_ZN2v88internal19AtomicsAndAssembler22GenerateAtomicsAndImplEv:
.LFB13372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicAndENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movl	$15, %r9d
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13372:
	.size	_ZN2v88internal19AtomicsAndAssembler22GenerateAtomicsAndImplEv, .-_ZN2v88internal19AtomicsAndAssembler22GenerateAtomicsAndImplEv
	.section	.text._ZN2v88internal18AtomicsOrAssembler21GenerateAtomicsOrImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18AtomicsOrAssembler21GenerateAtomicsOrImplEv
	.type	_ZN2v88internal18AtomicsOrAssembler21GenerateAtomicsOrImplEv, @function
_ZN2v88internal18AtomicsOrAssembler21GenerateAtomicsOrImplEv:
.LFB13381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	_ZN2v88internal8compiler13CodeAssembler8AtomicOrENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movl	$19, %r9d
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13381:
	.size	_ZN2v88internal18AtomicsOrAssembler21GenerateAtomicsOrImplEv, .-_ZN2v88internal18AtomicsOrAssembler21GenerateAtomicsOrImplEv
	.section	.text._ZN2v88internal19AtomicsXorAssembler22GenerateAtomicsXorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19AtomicsXorAssembler22GenerateAtomicsXorImplEv
	.type	_ZN2v88internal19AtomicsXorAssembler22GenerateAtomicsXorImplEv, @function
_ZN2v88internal19AtomicsXorAssembler22GenerateAtomicsXorImplEv:
.LFB13390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicXorENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movl	$21, %r9d
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13390:
	.size	_ZN2v88internal19AtomicsXorAssembler22GenerateAtomicsXorImplEv, .-_ZN2v88internal19AtomicsXorAssembler22GenerateAtomicsXorImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_AtomicsAddEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"AtomicsAdd"
	.section	.text._ZN2v88internal8Builtins19Generate_AtomicsAddEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_AtomicsAddEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_AtomicsAddEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_AtomicsAddEPNS0_8compiler18CodeAssemblerStateE:
.LFB13350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$538, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$579, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L130
.L127:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movl	$14, %r9d
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicAddENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L127
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13350:
	.size	_ZN2v88internal8Builtins19Generate_AtomicsAddEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_AtomicsAddEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins19Generate_AtomicsSubEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"AtomicsSub"
	.section	.text._ZN2v88internal8Builtins19Generate_AtomicsSubEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_AtomicsSubEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_AtomicsSubEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_AtomicsSubEPNS0_8compiler18CodeAssemblerStateE:
.LFB13359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$539, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$580, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L136
.L133:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movl	$20, %r9d
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicSubENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L133
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13359:
	.size	_ZN2v88internal8Builtins19Generate_AtomicsSubEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_AtomicsSubEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins19Generate_AtomicsAndEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"AtomicsAnd"
	.section	.text._ZN2v88internal8Builtins19Generate_AtomicsAndEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_AtomicsAndEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_AtomicsAndEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_AtomicsAndEPNS0_8compiler18CodeAssemblerStateE:
.LFB13368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$540, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$581, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L142
.L139:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movl	$15, %r9d
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicAndENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L139
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13368:
	.size	_ZN2v88internal8Builtins19Generate_AtomicsAndEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_AtomicsAndEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins18Generate_AtomicsOrEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"AtomicsOr"
	.section	.text._ZN2v88internal8Builtins18Generate_AtomicsOrEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_AtomicsOrEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_AtomicsOrEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_AtomicsOrEPNS0_8compiler18CodeAssemblerStateE:
.LFB13377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$541, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$582, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L148
.L145:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movl	$19, %r9d
	movq	_ZN2v88internal8compiler13CodeAssembler8AtomicOrENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L145
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13377:
	.size	_ZN2v88internal8Builtins18Generate_AtomicsOrEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_AtomicsOrEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins19Generate_AtomicsXorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"AtomicsXor"
	.section	.text._ZN2v88internal8Builtins19Generate_AtomicsXorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_AtomicsXorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_AtomicsXorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_AtomicsXorEPNS0_8compiler18CodeAssemblerStateE:
.LFB13386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$542, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$583, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L154
.L151:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	movl	$21, %r9d
	movq	_ZN2v88internal8compiler13CodeAssembler9AtomicXorENS0_11MachineTypeEPNS1_4NodeES5_S5_S5_@GOTPCREL(%rip), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	call	_ZN2v88internal34SharedArrayBufferBuiltinsAssembler24AtomicBinopBuiltinCommonEPNS0_8compiler4NodeES4_S4_S4_MNS2_13CodeAssemblerEFS4_NS0_11MachineTypeES4_S4_S4_S4_ENS0_7Runtime10FunctionIdE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L151
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13386:
	.size	_ZN2v88internal8Builtins19Generate_AtomicsXorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_AtomicsXorEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_, @function
_GLOBAL__sub_I__ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_:
.LFB17147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17147:
	.size	_GLOBAL__sub_I__ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_, .-_GLOBAL__sub_I__ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal34SharedArrayBufferBuiltinsAssembler24ValidateSharedTypedArrayEPNS0_8compiler4NodeES4_PNS2_5TNodeINS0_6Int32TEEEPS4_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	18
	.long	17
	.long	20
	.long	19
	.align 16
.LC1:
	.long	22
	.long	21
	.long	27
	.long	26
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
