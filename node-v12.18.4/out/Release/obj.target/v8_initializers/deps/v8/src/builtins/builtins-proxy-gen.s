	.file	"builtins-proxy-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%rsi), %r13
	movq	(%rbx), %rdi
	movq	16(%rbx), %r12
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	movq	%r13, %r8
	movq	%rax, %rcx
	movl	$8, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	16(%rbx), %rdi
	movq	(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	movl	$1, %ecx
	popq	%r12
	movl	$8, %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	.cfi_endproc
.LFE26415:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%rsi), %r13
	movq	(%rbx), %rdi
	movq	16(%rbx), %r12
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	8(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler5StoreEPNS1_4NodeES4_S4_@PLT
	movq	16(%rbx), %rdi
	movq	(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	movl	$1, %ecx
	popq	%r12
	movl	$8, %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	.cfi_endproc
.LFE26420:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation:
.LFB26417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L12
	cmpl	$3, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L19
.L14:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26417:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation:
.LFB26421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L21
	cmpl	$3, %edx
	je	.L22
	cmpl	$1, %edx
	je	.L28
.L23:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L23
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26421:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.section	.text._ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_
	.type	_ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_, @function
_ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_:
.LFB22441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-592(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$8, %edx
	subq	$600, %rsp
	movq	%rcx, -600(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-576(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-608(%rbp), %r11
	movq	-624(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rcx
	movq	%r9, %rdx
	movq	%r11, -632(%rbp)
	movq	%r9, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-608(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -624(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsConstructorENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-616(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$111, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-608(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -616(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$112, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-632(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -608(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$114, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movl	$125, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%rbx, %rcx
	movl	$16, %edx
	movq	%r13, %rsi
	movl	$7, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$24, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-600(%rbp), %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-608(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-616(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$600, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22441:
	.size	_ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_, .-_ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_
	.section	.text._ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE
	.type	_ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE, @function
_ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE:
.LFB22442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$568, %rsp
	movq	%rsi, -600(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L51
	leaq	-224(%rbp), %rax
	movq	%rax, -552(%rbp)
.L34:
	leaq	-480(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	$1, %r8d
	leaq	-352(%rbp), %r13
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-544(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r15, -560(%rbp)
	testl	%ebx, %ebx
	jne	.L53
.L36:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-592(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-568(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r9d, %r9d
	movl	$4, %r8d
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	movq	-576(%rbp), %rdi
	leaq	-528(%rbp), %r15
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	$0, -488(%rbp)
	movq	$0, -496(%rbp)
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rax, -512(%rbp)
	movq	$0, -504(%rbp)
	subq	%rdx, %rcx
	cmpq	$7, %rcx
	jbe	.L54
	leaq	8(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L38:
	movq	%rcx, -488(%rbp)
	movl	$16382, %esi
	movq	%r12, %rdi
	movq	%rdx, -504(%rbp)
	movq	%r15, (%rdx)
	movq	%rcx, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-592(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-552(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r15, (%rax)
	movq	%rcx, %xmm0
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	%rbx, 8(%rax)
	movq	-584(%rbp), %rdi
	movq	%r12, 16(%rax)
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm1
	leaq	-512(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %rsi
	movq	%rax, -592(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L39
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L39:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, 8(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS2_8compiler4NodeERNS2_17CodeStubArgumentsES6_NS2_17CodeStubAssembler13ParameterModeEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation(%rip), %rbx
	movq	-592(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, (%rax)
	movq	-584(%rbp), %rdi
	movq	%rbx, %xmm0
	movq	%r12, 16(%rax)
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsES4_RNS1_17CodeStubArgumentsES4_NS1_17CodeStubAssembler13ParameterModeEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L40
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L40:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-552(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-568(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	-576(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-560(%rbp), %rcx
	movq	%rax, %rdx
	movl	$32, %r9d
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_14FixedArrayBaseEEENS3_INS0_3SmiEEEPNS2_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$568, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	leaq	-224(%rbp), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %r13
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r13, -96(%rbp)
	movq	$35, -224(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$29806, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC2(%rip), %xmm0
	movw	%dx, 32(%rax)
	movb	$115, 34(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L34
	call	_ZdlPv@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, -560(%rbp)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	8(%rax), %rcx
	jmp	.L38
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE, .-_ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE
	.section	.text._ZN2v88internal24ProxiesCodeStubAssembler32CreateProxyRevokeFunctionContextEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ProxiesCodeStubAssembler32CreateProxyRevokeFunctionContextEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal24ProxiesCodeStubAssembler32CreateProxyRevokeFunctionContextEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal24ProxiesCodeStubAssembler32CreateProxyRevokeFunctionContextEPNS0_8compiler4NodeES4_:
.LFB22451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$56, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r13, %rdi
	movl	$21, %edx
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$5, %ecx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$4, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22451:
	.size	_ZN2v88internal24ProxiesCodeStubAssembler32CreateProxyRevokeFunctionContextEPNS0_8compiler4NodeES4_, .-_ZN2v88internal24ProxiesCodeStubAssembler32CreateProxyRevokeFunctionContextEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal24ProxiesCodeStubAssembler27AllocateProxyRevokeFunctionENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_7JSProxyEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ProxiesCodeStubAssembler27AllocateProxyRevokeFunctionENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_7JSProxyEEE
	.type	_ZN2v88internal24ProxiesCodeStubAssembler27AllocateProxyRevokeFunctionENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_7JSProxyEEE, @function
_ZN2v88internal24ProxiesCodeStubAssembler27AllocateProxyRevokeFunctionENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_7JSProxyEEE:
.LFB22452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$56, %esi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r12, %rdi
	movl	$21, %edx
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$5, %ecx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$161, %edx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$116, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22452:
	.size	_ZN2v88internal24ProxiesCodeStubAssembler27AllocateProxyRevokeFunctionENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_7JSProxyEEE, .-_ZN2v88internal24ProxiesCodeStubAssembler27AllocateProxyRevokeFunctionENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_7JSProxyEEE
	.section	.rodata._ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"apply"
	.section	.text._ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv
	.type	_ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv, @function
_ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv:
.LFB22464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	leaq	-416(%rbp), %rbx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -472(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-480(%rbp), %r10
	leaq	2024(%rax), %rcx
	movq	%r10, %rdx
	movq	%r10, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssembler9GetMethodEPNS0_8compiler4NodeES4_NS0_6HandleINS0_4NameEEEPNS2_18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	-488(%rbp), %r11
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -480(%rbp)
	movq	%r11, %rdx
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-488(%rbp), %r11
	movl	$1, %r8d
	movq	%rax, -504(%rbp)
	movq	%r11, %rcx
	call	_ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-448(%rbp), %r11
	movl	$2, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -520(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-112(%rbp), %rcx
	movl	$6, %edi
	xorl	%esi, %esi
	movq	-480(%rbp), %xmm0
	pushq	%rdi
	movq	%rax, %r8
	movq	%r13, %r9
	movq	-528(%rbp), %r10
	pushq	%rcx
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-512(%rbp), %xmm0
	movq	%rax, -464(%rbp)
	movq	-432(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%r10, %xmm0
	leaq	-464(%rbp), %r10
	movhps	-472(%rbp), %xmm0
	movq	%r10, %rdx
	movq	%rcx, -480(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	-504(%rbp), %xmm0
	movq	%rax, -456(%rbp)
	movhps	-488(%rbp), %xmm0
	movq	%r10, -488(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-520(%rbp), %r11
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-488(%rbp), %r10
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-472(%rbp), %xmm0
	movq	-480(%rbp), %r8
	movq	%rax, -464(%rbp)
	movq	%r10, %rsi
	movq	-432(%rbp), %rax
	movl	$2, %r9d
	movhps	-496(%rbp), %xmm0
	movq	%rax, -456(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$145, %edx
	movq	%r13, %rsi
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22464:
	.size	_ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv, .-_ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../deps/v8/src/builtins/builtins-proxy-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"CallProxy"
	.section	.text._ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$143, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$11, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L68
.L65:
	movq	%r13, %rdi
	call	_ZN2v88internal18CallProxyAssembler21GenerateCallProxyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L65
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22460:
	.size	_ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_CallProxyEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"construct"
	.section	.text._ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv
	.type	_ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv, @function
_ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv:
.LFB22476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r15
	leaq	-240(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$632, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r13
	leaq	-496(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-600(%rbp), %r9
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-616(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-600(%rbp), %r9
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%rbx, %rdx
	movq	%r15, %r8
	movq	%r13, %rsi
	leaq	2296(%rax), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9GetMethodEPNS0_8compiler4NodeES4_NS0_6HandleINS0_4NameEEEPNS2_18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	-632(%rbp), %r10
	leaq	-544(%rbp), %r11
	movq	%r12, %rsi
	movq	%rax, -624(%rbp)
	movq	%r11, %rdi
	movq	%r11, -632(%rbp)
	movq	%r10, %rdx
	movq	%r10, -648(%rbp)
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	-632(%rbp), %r11
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-648(%rbp), %r10
	movl	$1, %r8d
	movq	%r11, %rdx
	movq	%r11, -664(%rbp)
	movq	%r10, %rcx
	call	_ZN2v88internal24ProxiesCodeStubAssembler35AllocateJSArrayForCodeStubArgumentsEPNS0_8compiler4NodeERNS0_17CodeStubArgumentsES4_NS0_17CodeStubAssembler13ParameterModeE
	movq	%r12, %rdi
	movq	%rax, -632(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-576(%rbp), %r10
	movl	$2, %edx
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -656(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-112(%rbp), %rcx
	movl	$6, %edi
	xorl	%esi, %esi
	movq	-624(%rbp), %xmm0
	pushq	%rdi
	movq	%rax, %r8
	movq	%r13, %r9
	pushq	%rcx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	leaq	-592(%rbp), %r11
	movhps	-648(%rbp), %xmm0
	movq	%rax, -592(%rbp)
	movq	-560(%rbp), %rax
	movq	%r11, %rdx
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-600(%rbp), %xmm0
	movq	%rcx, -624(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	-632(%rbp), %xmm0
	movq	%rax, -584(%rbp)
	movhps	-608(%rbp), %xmm0
	movq	%r11, -632(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$118, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-656(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE@PLT
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-624(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-600(%rbp), %xmm0
	movq	-632(%rbp), %rsi
	movq	%rax, -592(%rbp)
	movq	-560(%rbp), %rax
	movl	$3, %r9d
	movhps	-608(%rbp), %xmm0
	movq	%rax, -584(%rbp)
	movq	-640(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-616(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$145, %edx
	movq	%r13, %rsi
	leaq	.LC6(%rip), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22476:
	.size	_ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv, .-_ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_ConstructProxyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"ConstructProxy"
	.section	.text._ZN2v88internal8Builtins23Generate_ConstructProxyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_ConstructProxyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_ConstructProxyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_ConstructProxyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$197, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$39, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L78
.L75:
	movq	%r13, %rdi
	call	_ZN2v88internal23ConstructProxyAssembler26GenerateConstructProxyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L75
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22472:
	.size	_ZN2v88internal8Builtins23Generate_ConstructProxyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_ConstructProxyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE
	.type	_ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE, @function
_ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE:
.LFB22477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1152(%rbp), %r15
	leaq	-992(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-864(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1240, %rsp
	.cfi_offset 3, -56
	movq	%r9, -1216(%rbp)
	movq	%r8, -1184(%rbp)
	movq	%rsi, -1192(%rbp)
	movq	%rdx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -1232(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rbx
	leaq	-1168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r15, %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1136(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1208(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-1120(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19IsUniqueNameNoIndexENS0_8compiler5TNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	pushq	$1
	movq	%rbx, %r8
	movq	%r12, %rdi
	pushq	%r14
	movq	-1232(%rbp), %rdx
	movq	%rax, %r9
	leaq	-736(%rbp), %rbx
	pushq	%r13
	movq	-1192(%rbp), %rsi
	pushq	-1208(%rbp)
	movq	%rdx, %rcx
	pushq	%r15
	movq	%r15, -1200(%rbp)
	movq	-1256(%rbp), %r15
	pushq	-1224(%rbp)
	pushq	%r15
	pushq	-1184(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17TryGetOwnPropertyEPNS0_8compiler4NodeES4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES8_S8_S6_S6_NS1_18GetOwnPropertyModeE@PLT
	addq	$64, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbx, -1240(%rbp)
	movq	%r12, %rsi
	leaq	-608(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-352(%rbp), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-480(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1200(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -1264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1272(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-1264(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1208(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1264(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1264(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler14IsAccessorPairENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1248(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1200(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -1264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1272(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-1264(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1224(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1240(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-1216(%rbp), %rsi
	movl	$1, %r9d
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17BranchIfSameValueENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_18CodeAssemblerLabelES7_NS1_13SameValueModeE@PLT
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1208(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %r9
	movl	16(%rbp), %eax
	testl	%eax, %eax
	jne	.L81
	leaq	-224(%rbp), %r10
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -1264(%rbp)
	movq	%r9, -1272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1272(%rbp), %r9
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1264(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1272(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1264(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1264(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1264(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1224(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1216(%rbp), %r9
	movl	$125, %edx
	movq	%r12, %rdi
	movq	-1184(%rbp), %rcx
	movq	-1192(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	movl	$126, %edx
	movq	%r12, %rdi
	movq	-1216(%rbp), %r8
	movq	-1184(%rbp), %rcx
	movq	-1192(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
.L82:
	movq	-1184(%rbp), %xmm0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movhps	-1232(%rbp), %xmm0
	movaps	%xmm0, -1184(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	16(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1216(%rbp), %rcx
	movl	$286, %esi
	movdqa	-1184(%rbp), %xmm0
	movq	-1192(%rbp), %rdx
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	movq	%rcx, -80(%rbp)
	leaq	-96(%rbp), %rcx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1208(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1200(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1224(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	%r9, %rsi
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1264(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1264(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$146, %edx
	movq	-1184(%rbp), %rcx
	movq	-1192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$147, %edx
	movq	-1184(%rbp), %rcx
	movq	-1192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	jmp	.L82
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22477:
	.size	_ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE, .-_ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE
	.section	.text._ZN2v88internal24ProxiesCodeStubAssembler18CheckHasTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ProxiesCodeStubAssembler18CheckHasTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE
	.type	_ZN2v88internal24ProxiesCodeStubAssembler18CheckHasTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE, @function
_ZN2v88internal24ProxiesCodeStubAssembler18CheckHasTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE:
.LFB22478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$808, %rsp
	movq	%r8, -776(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	leaq	-768(%rbp), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rax, -800(%rbp)
	movq	%rcx, -808(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-752(%rbp), %rcx
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -784(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-736(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r15
	movl	$8, %edx
	movq	%r15, %rdi
	movq	%r15, -816(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-720(%rbp), %rdx
	movq	%rdx, %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r15, -792(%rbp)
	leaq	-208(%rbp), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-592(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-464(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r11, -840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19IsUniqueNameNoIndexENS0_8compiler5TNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	pushq	$1
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	pushq	%r15
	movq	-800(%rbp), %r8
	movq	%rax, %r9
	movq	%r14, %rsi
	pushq	%r13
	movq	%r12, %rdi
	pushq	-816(%rbp)
	pushq	-784(%rbp)
	pushq	-808(%rbp)
	pushq	-792(%rbp)
	pushq	-776(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17TryGetOwnPropertyEPNS0_8compiler4NodeES4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES8_S8_S6_S6_NS1_18GetOwnPropertyModeE@PLT
	movq	-792(%rbp), %rsi
	addq	$64, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-784(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-832(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-824(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-848(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsExtensibleMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-840(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-824(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$136, %edx
	movq	-776(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	-800(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$137, %edx
	movq	-776(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm1
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	movl	$2, %r8d
	movl	$287, %esi
	movq	-776(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-800(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-824(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-816(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-784(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-808(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22478:
	.size	_ZN2v88internal24ProxiesCodeStubAssembler18CheckHasTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE, .-_ZN2v88internal24ProxiesCodeStubAssembler18CheckHasTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE
	.section	.text._ZN2v88internal24ProxiesCodeStubAssembler21CheckDeleteTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24ProxiesCodeStubAssembler21CheckDeleteTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE
	.type	_ZN2v88internal24ProxiesCodeStubAssembler21CheckDeleteTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE, @function
_ZN2v88internal24ProxiesCodeStubAssembler21CheckDeleteTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE:
.LFB22479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$808, %rsp
	movq	%r8, -776(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	leaq	-768(%rbp), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rax, -800(%rbp)
	movq	%rcx, -808(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-752(%rbp), %rcx
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -784(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-736(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r15
	movl	$8, %edx
	movq	%r15, %rdi
	movq	%r15, -816(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-720(%rbp), %rdx
	movq	%rdx, %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r15, -792(%rbp)
	leaq	-208(%rbp), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-592(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-464(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r11, -840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19IsUniqueNameNoIndexENS0_8compiler5TNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	pushq	$1
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	pushq	%r15
	movq	-800(%rbp), %r8
	movq	%rax, %r9
	movq	%r14, %rsi
	pushq	%r13
	movq	%r12, %rdi
	pushq	-816(%rbp)
	pushq	-784(%rbp)
	pushq	-808(%rbp)
	pushq	-792(%rbp)
	pushq	-776(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17TryGetOwnPropertyEPNS0_8compiler4NodeES4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES8_S8_S6_S6_NS1_18GetOwnPropertyModeE@PLT
	movq	-792(%rbp), %rsi
	addq	$64, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-784(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-832(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-824(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-848(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsExtensibleMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-840(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-824(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$123, %edx
	movq	-776(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	-800(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$124, %edx
	movq	-776(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm1
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	movl	$2, %r8d
	movl	$288, %esi
	movq	-776(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-800(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-824(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-816(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-784(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-808(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22479:
	.size	_ZN2v88internal24ProxiesCodeStubAssembler21CheckDeleteTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE, .-_ZN2v88internal24ProxiesCodeStubAssembler21CheckDeleteTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_, @function
_GLOBAL__sub_I__ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_:
.LFB28723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28723:
	.size	_GLOBAL__sub_I__ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_, .-_GLOBAL__sub_I__ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	7310575174828190785
	.quad	5078197199007994698
	.align 16
.LC2:
	.quad	8382154813504451183
	.quad	7308626857451151989
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
