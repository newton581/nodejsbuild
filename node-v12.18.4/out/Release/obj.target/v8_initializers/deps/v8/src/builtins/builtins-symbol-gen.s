	.file	"builtins-symbol-gen.cc"
	.text
	.section	.rodata._ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/builtins/builtins-symbol-gen.cc"
	.align 8
.LC1:
	.string	"SymbolPrototypeDescriptionGetter"
	.section	.rodata._ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Symbol.prototype.description"
	.section	.text._ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB13300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$15, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$607, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L6
.L2:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC2(%rip), %r8
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L2
.L7:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13300:
	.size	_ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal41SymbolPrototypeDescriptionGetterAssembler44GenerateSymbolPrototypeDescriptionGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal41SymbolPrototypeDescriptionGetterAssembler44GenerateSymbolPrototypeDescriptionGetterImplEv
	.type	_ZN2v88internal41SymbolPrototypeDescriptionGetterAssembler44GenerateSymbolPrototypeDescriptionGetterImplEv, @function
_ZN2v88internal41SymbolPrototypeDescriptionGetterAssembler44GenerateSymbolPrototypeDescriptionGetterImplEv:
.LFB13304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$3, %ecx
	movq	%rax, %rdx
	leaq	.LC2(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13304:
	.size	_ZN2v88internal41SymbolPrototypeDescriptionGetterAssembler44GenerateSymbolPrototypeDescriptionGetterImplEv, .-_ZN2v88internal41SymbolPrototypeDescriptionGetterAssembler44GenerateSymbolPrototypeDescriptionGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"SymbolPrototypeToPrimitive"
	.section	.rodata._ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Symbol.prototype [ @@toPrimitive ]"
	.section	.text._ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE:
.LFB13309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$27, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$608, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L14
.L11:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC4(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L11
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13309:
	.size	_ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_SymbolPrototypeToPrimitiveEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35SymbolPrototypeToPrimitiveAssembler38GenerateSymbolPrototypeToPrimitiveImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35SymbolPrototypeToPrimitiveAssembler38GenerateSymbolPrototypeToPrimitiveImplEv
	.type	_ZN2v88internal35SymbolPrototypeToPrimitiveAssembler38GenerateSymbolPrototypeToPrimitiveImplEv, @function
_ZN2v88internal35SymbolPrototypeToPrimitiveAssembler38GenerateSymbolPrototypeToPrimitiveImplEv:
.LFB13313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$3, %ecx
	movq	%rax, %rdx
	leaq	.LC4(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13313:
	.size	_ZN2v88internal35SymbolPrototypeToPrimitiveAssembler38GenerateSymbolPrototypeToPrimitiveImplEv, .-_ZN2v88internal35SymbolPrototypeToPrimitiveAssembler38GenerateSymbolPrototypeToPrimitiveImplEv
	.section	.rodata._ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Symbol.prototype.toString"
	.section	.text._ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv
	.type	_ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv, @function
_ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv:
.LFB13322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC5(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	leaq	-32(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$346, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13322:
	.size	_ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv, .-_ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_SymbolPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"SymbolPrototypeToString"
	.section	.text._ZN2v88internal8Builtins32Generate_SymbolPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_SymbolPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_SymbolPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_SymbolPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB13318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$37, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$609, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L26
.L23:
	movq	%r13, %rdi
	call	_ZN2v88internal32SymbolPrototypeToStringAssembler35GenerateSymbolPrototypeToStringImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L23
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13318:
	.size	_ZN2v88internal8Builtins32Generate_SymbolPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_SymbolPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins31Generate_SymbolPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"SymbolPrototypeValueOf"
.LC8:
	.string	"Symbol.prototype.valueOf"
	.section	.text._ZN2v88internal8Builtins31Generate_SymbolPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_SymbolPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_SymbolPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_SymbolPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB13327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$49, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$610, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L32
.L29:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC8(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L29
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13327:
	.size	_ZN2v88internal8Builtins31Generate_SymbolPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_SymbolPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31SymbolPrototypeValueOfAssembler34GenerateSymbolPrototypeValueOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31SymbolPrototypeValueOfAssembler34GenerateSymbolPrototypeValueOfImplEv
	.type	_ZN2v88internal31SymbolPrototypeValueOfAssembler34GenerateSymbolPrototypeValueOfImplEv, @function
_ZN2v88internal31SymbolPrototypeValueOfAssembler34GenerateSymbolPrototypeValueOfImplEv:
.LFB13331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$3, %ecx
	movq	%rax, %rdx
	leaq	.LC8(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13331:
	.size	_ZN2v88internal31SymbolPrototypeValueOfAssembler34GenerateSymbolPrototypeValueOfImplEv, .-_ZN2v88internal31SymbolPrototypeValueOfAssembler34GenerateSymbolPrototypeValueOfImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB17043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17043:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins41Generate_SymbolPrototypeDescriptionGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
