	.file	"builtins-ic-gen.cc"
	.text
	.section	.text._ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler14GenerateLoadICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13358:
	.size	_ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins27Generate_LoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_LoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_LoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_LoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE:
.LFB13362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler26GenerateLoadIC_MegamorphicEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13362:
	.size	_ZN2v88internal8Builtins27Generate_LoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_LoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins26Generate_LoadIC_NoninlinedEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_LoadIC_NoninlinedEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_LoadIC_NoninlinedEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_LoadIC_NoninlinedEPNS0_8compiler18CodeAssemblerStateE:
.LFB13363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler25GenerateLoadIC_NoninlinedEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13363:
	.size	_ZN2v88internal8Builtins26Generate_LoadIC_NoninlinedEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_LoadIC_NoninlinedEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins26Generate_LoadIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_LoadIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_LoadIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_LoadIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE:
.LFB13364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler25GenerateLoadIC_NoFeedbackEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13364:
	.size	_ZN2v88internal8Builtins26Generate_LoadIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_LoadIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins25Generate_LoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_LoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_LoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_LoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB13365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler24GenerateLoadICTrampolineEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13365:
	.size	_ZN2v88internal8Builtins25Generate_LoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_LoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins37Generate_LoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_LoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_LoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_LoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE:
.LFB13366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler36GenerateLoadICTrampoline_MegamorphicEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13366:
	.size	_ZN2v88internal8Builtins37Generate_LoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_LoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins20Generate_KeyedLoadICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_KeyedLoadICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_KeyedLoadICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_KeyedLoadICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler19GenerateKeyedLoadICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13367:
	.size	_ZN2v88internal8Builtins20Generate_KeyedLoadICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_KeyedLoadICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins32Generate_KeyedLoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_KeyedLoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_KeyedLoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_KeyedLoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE:
.LFB13368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler31GenerateKeyedLoadIC_MegamorphicEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13368:
	.size	_ZN2v88internal8Builtins32Generate_KeyedLoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_KeyedLoadIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins36Generate_KeyedLoadIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_KeyedLoadIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_KeyedLoadIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_KeyedLoadIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE:
.LFB13369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler35GenerateKeyedLoadIC_PolymorphicNameEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13369:
	.size	_ZN2v88internal8Builtins36Generate_KeyedLoadIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_KeyedLoadIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins30Generate_KeyedLoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_KeyedLoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_KeyedLoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_KeyedLoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB13370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler29GenerateKeyedLoadICTrampolineEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13370:
	.size	_ZN2v88internal8Builtins30Generate_KeyedLoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_KeyedLoadICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins42Generate_KeyedLoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins42Generate_KeyedLoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins42Generate_KeyedLoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins42Generate_KeyedLoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE:
.LFB13371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler41GenerateKeyedLoadICTrampoline_MegamorphicEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13371:
	.size	_ZN2v88internal8Builtins42Generate_KeyedLoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins42Generate_KeyedLoadICTrampoline_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins22Generate_StoreGlobalICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_StoreGlobalICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_StoreGlobalICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_StoreGlobalICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler21GenerateStoreGlobalICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L49:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13372:
	.size	_ZN2v88internal8Builtins22Generate_StoreGlobalICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_StoreGlobalICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins32Generate_StoreGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_StoreGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_StoreGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_StoreGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB13373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler31GenerateStoreGlobalICTrampolineEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13373:
	.size	_ZN2v88internal8Builtins32Generate_StoreGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_StoreGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins16Generate_StoreICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_StoreICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_StoreICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_StoreICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler15GenerateStoreICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13374:
	.size	_ZN2v88internal8Builtins16Generate_StoreICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_StoreICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins26Generate_StoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_StoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_StoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_StoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB13375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler25GenerateStoreICTrampolineEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13375:
	.size	_ZN2v88internal8Builtins26Generate_StoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_StoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins21Generate_KeyedStoreICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_KeyedStoreICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_KeyedStoreICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_KeyedStoreICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler20GenerateKeyedStoreICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13376:
	.size	_ZN2v88internal8Builtins21Generate_KeyedStoreICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_KeyedStoreICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins31Generate_KeyedStoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_KeyedStoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_KeyedStoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_KeyedStoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB13377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler30GenerateKeyedStoreICTrampolineEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13377:
	.size	_ZN2v88internal8Builtins31Generate_KeyedStoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_KeyedStoreICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins30Generate_StoreInArrayLiteralICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StoreInArrayLiteralICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StoreInArrayLiteralICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StoreInArrayLiteralICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler29GenerateStoreInArrayLiteralICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13378:
	.size	_ZN2v88internal8Builtins30Generate_StoreInArrayLiteralICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StoreInArrayLiteralICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins22Generate_CloneObjectICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_CloneObjectICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_CloneObjectICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_CloneObjectICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler21GenerateCloneObjectICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13379:
	.size	_ZN2v88internal8Builtins22Generate_CloneObjectICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_CloneObjectICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins27Generate_CloneObjectIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_CloneObjectIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_CloneObjectIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_CloneObjectIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB13380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler26GenerateCloneObjectIC_SlowEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13380:
	.size	_ZN2v88internal8Builtins27Generate_CloneObjectIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_CloneObjectIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins19Generate_KeyedHasICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_KeyedHasICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_KeyedHasICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_KeyedHasICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler18GenerateKeyedHasICEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13381:
	.size	_ZN2v88internal8Builtins19Generate_KeyedHasICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_KeyedHasICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins31Generate_KeyedHasIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_KeyedHasIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_KeyedHasIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_KeyedHasIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE:
.LFB13382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler30GenerateKeyedHasIC_MegamorphicEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13382:
	.size	_ZN2v88internal8Builtins31Generate_KeyedHasIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_KeyedHasIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins35Generate_KeyedHasIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_KeyedHasIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_KeyedHasIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_KeyedHasIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE:
.LFB13383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler34GenerateKeyedHasIC_PolymorphicNameEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13383:
	.size	_ZN2v88internal8Builtins35Generate_KeyedHasIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_KeyedHasIC_PolymorphicNameEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins21Generate_LoadGlobalICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_LoadGlobalICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_LoadGlobalICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_LoadGlobalICEPNS0_8compiler18CodeAssemblerStateE:
.LFB13384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler20GenerateLoadGlobalICENS0_10TypeofModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13384:
	.size	_ZN2v88internal8Builtins21Generate_LoadGlobalICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_LoadGlobalICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins33Generate_LoadGlobalICInsideTypeofEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_LoadGlobalICInsideTypeofEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_LoadGlobalICInsideTypeofEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_LoadGlobalICInsideTypeofEPNS0_8compiler18CodeAssemblerStateE:
.LFB13385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler20GenerateLoadGlobalICENS0_10TypeofModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13385:
	.size	_ZN2v88internal8Builtins33Generate_LoadGlobalICInsideTypeofEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_LoadGlobalICInsideTypeofEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins31Generate_LoadGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_LoadGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_LoadGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_LoadGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB13386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler30GenerateLoadGlobalICTrampolineENS0_10TypeofModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13386:
	.size	_ZN2v88internal8Builtins31Generate_LoadGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_LoadGlobalICTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins43Generate_LoadGlobalICInsideTypeofTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins43Generate_LoadGlobalICInsideTypeofTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins43Generate_LoadGlobalICInsideTypeofTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins43Generate_LoadGlobalICInsideTypeofTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB13387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler30GenerateLoadGlobalICTrampolineENS0_10TypeofModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13387:
	.size	_ZN2v88internal8Builtins43Generate_LoadGlobalICInsideTypeofTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins43Generate_LoadGlobalICInsideTypeofTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE:
.LFB17173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17173:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins15Generate_LoadICEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
