	.file	"setup-heap-internal.cc"
	.text
	.section	.text._ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,"axG",@progbits,_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.type	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, @function
_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv:
.LFB9508:
	.cfi_startproc
	endbr64
	cmpl	$2, 72(%rdi)
	je	.L10
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*152(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE9508:
	.size	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, .-_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.section	.text._ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,"axG",@progbits,_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,comdat
	.p2align 4
	.weak	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.type	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, @function
_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_:
.LFB11233:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L11
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L16
.L11:
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE11233:
	.size	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, .-_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.section	.text._ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,"axG",@progbits,_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.type	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, @function
_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE:
.LFB11244:
	.cfi_startproc
	endbr64
	testb	$1, %dl
	je	.L17
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L23
.L17:
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE11244:
	.size	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, .-_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.section	.text._ZN2v88internal10FixedArray3setEiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal10FixedArray3setEiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.type	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, @function
_ZN2v88internal10FixedArray3setEiNS0_6ObjectE:
.LFB12002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	16(,%rsi,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rdx, -1(%rbx,%rax)
	testb	$1, %dl
	je	.L24
	movq	%rdx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rdx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L36
	testb	$24, %al
	je	.L24
.L38:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L37
.L24:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L38
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L37:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE12002:
	.size	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, .-_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.section	.rodata._ZN2v88internal3Map27SetConstructorFunctionIndexEi.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsPrimitiveMap()"
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal3Map27SetConstructorFunctionIndexEi.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"static_cast<unsigned>(value) < 256"
	.section	.text._ZN2v88internal3Map27SetConstructorFunctionIndexEi,"axG",@progbits,_ZN2v88internal3Map27SetConstructorFunctionIndexEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	.type	_ZN2v88internal3Map27SetConstructorFunctionIndexEi, @function
_ZN2v88internal3Map27SetConstructorFunctionIndexEi:
.LFB17750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	cmpw	$67, 11(%rax)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	ja	.L43
	cmpl	$255, %esi
	ja	.L44
	movb	%sil, 8(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17750:
	.size	_ZN2v88internal3Map27SetConstructorFunctionIndexEi, .-_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	.section	.text._ZN2v88internal3Map13mark_unstableEv,"axG",@progbits,_ZN2v88internal3Map13mark_unstableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal3Map13mark_unstableEv
	.type	_ZN2v88internal3Map13mark_unstableEv, @function
_ZN2v88internal3Map13mark_unstableEv:
.LFB17792:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	15(%rdx), %eax
	orl	$33554432, %eax
	movl	%eax, 15(%rdx)
	ret
	.cfi_endproc
.LFE17792:
	.size	_ZN2v88internal3Map13mark_unstableEv, .-_ZN2v88internal3Map13mark_unstableEv
	.section	.rodata._ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"!object.IsSmi()"
.LC4:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE,"axG",@progbits,_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	.type	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE, @function
_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE:
.LFB19452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testb	%dl, %dl
	jne	.L47
	cmpl	$131072, %esi
	jle	.L48
	cmpb	$0, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	jne	.L153
.L49:
	movq	280(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal16LargeObjectSpace11AllocateRawEi@PLT
	movq	%rax, %r12
	notq	%rax
.L50:
	testb	$1, %al
	jne	.L109
.L91:
	movq	3184(%rbx), %rax
	movq	3176(%rbx), %r14
	leaq	-1(%r12), %r15
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r14
	je	.L96
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%r14), %rdi
	movl	%r13d, %edx
	movq	%r15, %rsi
	addq	$8, %r14
	movq	(%rdi), %rax
	call	*(%rax)
	cmpq	%r14, -56(%rbp)
	jne	.L95
.L96:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L154
	addl	$1, 400(%rbx)
.L109:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	248(%rdi), %r15
	movq	104(%r15), %r12
	cmpq	%r12, 120(%r15)
	jbe	.L51
	movq	%r12, 120(%r15)
.L51:
	movslq	%r13d, %rcx
	movq	%rcx, -56(%rbp)
	leaq	(%rcx,%r12), %rax
	cmpq	%rax, 112(%r15)
	jnb	.L52
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE@PLT
	testb	%al, %al
	je	.L101
	movq	104(%r15), %r12
	movq	-56(%rbp), %rcx
	leaq	(%rcx,%r12), %rax
.L52:
	addq	$1, %r12
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rax, 104(%r15)
	jne	.L155
.L54:
	movq	%r12, %rax
	notq	%rax
	testb	$1, %r12b
	jne	.L50
.L65:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	cmpb	$1, %dl
	je	.L156
	cmpb	$2, %dl
	je	.L157
	cmpb	$3, %dl
	je	.L158
	cmpb	$4, %dl
	jne	.L81
	movq	304(%rdi), %r15
	movq	120(%r15), %rcx
	movq	104(%r15), %r12
	testq	%rcx, %rcx
	je	.L82
.L152:
	cmpq	%r12, %rcx
	jbe	.L83
	movq	(%r15), %rax
	leaq	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv(%rip), %rsi
	movq	128(%rax), %rdx
	cmpq	%rsi, %rdx
	jne	.L84
	cmpl	$2, 72(%r15)
	je	.L159
.L83:
	movq	%r12, %rax
	subq	%rcx, %rax
	movq	%rax, %rcx
.L82:
	movslq	%r13d, %r8
	movq	%r8, -56(%rbp)
	leaq	(%r8,%r12), %rax
	cmpq	112(%r15), %rax
	jbe	.L87
	movq	%rcx, -64(%rbp)
	movq	(%r15), %rax
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	*184(%rax)
	movq	-64(%rbp), %rcx
	testb	%al, %al
	je	.L88
	movq	104(%r15), %r12
	movq	-56(%rbp), %r8
	leaq	(%r8,%r12), %rax
.L87:
	movq	%rax, 104(%r15)
	addq	$1, %r12
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	jne	.L160
.L90:
	movq	%r12, %rax
	notq	%rax
	testb	$1, %r12b
	je	.L65
.L89:
	movq	%rcx, -56(%rbp)
	testb	$1, %al
	jne	.L109
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*152(%rax)
	movq	-56(%rbp), %rcx
	testb	%al, %al
	jne	.L91
	leal	0(%r13,%rcx), %esi
	movq	%r15, %rdi
	leaq	-1(%r12), %rdx
	movl	%r13d, %ecx
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*48(%rax)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L154:
	movl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L109
	movl	400(%rbx), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	movl	%eax, 400(%rbx)
	divl	%ecx
	testl	%edx, %edx
	jne	.L109
	movq	stdout(%rip), %rsi
	leaq	-37592(%rbx), %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L153:
	movq	296(%rdi), %rdi
	call	_ZN2v88internal19NewLargeObjectSpace11AllocateRawEi@PLT
	movq	%rax, %r12
	notq	%rax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L156:
	cmpl	$131072, %esi
	jg	.L49
	movq	256(%rdi), %r15
	movq	120(%r15), %rcx
	movq	104(%r15), %r12
	testq	%rcx, %rcx
	je	.L82
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L157:
	movq	264(%rdi), %r14
	movl	$131072, %edx
	movq	160(%r14), %rax
	cmpl	$131072, %eax
	cmovg	%edx, %eax
	cmpl	%eax, %esi
	jle	.L161
	movq	288(%rdi), %rdi
	call	_ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi@PLT
	movq	%rax, %r12
	testb	$1, %al
	je	.L109
	movq	%rax, %rsi
	movq	%rbx, %rdi
	leaq	-1(%r12), %r14
	call	_ZN2v88internal4Heap31UnprotectAndRegisterMemoryChunkENS0_10HeapObjectE@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap13ZapCodeObjectEmi@PLT
	cmpl	$131072, %r13d
	jg	.L91
.L100:
	movq	%r12, %rax
	movq	%r14, %rsi
	andq	$-262144, %rax
	movq	272(%rax), %rdi
	call	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L88:
	movslq	72(%r15), %r12
	salq	$32, %r12
	movq	%r12, %rax
	notq	%rax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L160:
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	movq	-56(%rbp), %rcx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L158:
	movq	272(%rdi), %r14
	movslq	%esi, %r15
	movq	104(%r14), %r12
	leaq	(%r12,%r15), %rax
	cmpq	112(%r14), %rax
	jbe	.L77
	movq	(%r14), %rax
	movl	$1, %edx
	movq	%r14, %rdi
	call	*184(%rax)
	testb	%al, %al
	je	.L78
	movq	104(%r14), %r12
	leaq	(%r15,%r12), %rax
.L77:
	addq	$1, %r12
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rax, 104(%r14)
	je	.L54
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r15, %rdi
	call	*152(%rax)
	testb	%al, %al
	je	.L85
.L132:
	movq	120(%r15), %rcx
	movq	104(%r15), %r12
.L86:
	testq	%rcx, %rcx
	jne	.L83
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L101:
	movabsq	$4294967296, %r12
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r15, %rdi
	call	*%rdx
	testb	%al, %al
	je	.L132
.L85:
	movq	104(%r15), %rcx
	movq	%rcx, 120(%r15)
	movq	%rcx, %r12
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L161:
	movq	104(%r14), %r12
	movslq	%esi, %r15
	leaq	(%r12,%r15), %rax
	cmpq	112(%r14), %rax
	jbe	.L70
	movq	(%r14), %rax
	movl	$1, %edx
	movq	%r14, %rdi
	call	*184(%rax)
	testb	%al, %al
	je	.L71
	movq	104(%r14), %r12
	leaq	(%r15,%r12), %rax
.L70:
	addq	$1, %r12
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rax, 104(%r14)
	jne	.L162
.L73:
	movq	%r12, %rax
	notq	%rax
	testb	$1, %r12b
	je	.L65
.L72:
	testb	$1, %al
	jne	.L109
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-1(%r12), %r14
	call	_ZN2v88internal4Heap31UnprotectAndRegisterMemoryChunkENS0_10HeapObjectE@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap13ZapCodeObjectEmi@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L78:
	movslq	72(%r14), %r12
	salq	$32, %r12
	movq	%r12, %rax
	notq	%rax
	jmp	.L50
.L71:
	movslq	72(%r14), %r12
	salq	$32, %r12
	movq	%r12, %rax
	notq	%rax
	jmp	.L72
.L162:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L73
.L81:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19452:
	.size	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE, .-_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	.section	.text._ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	.type	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi, @function
_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi:
.LFB19699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	movl	$1, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpw	$1025, %si
	movl	$80, %esi
	setb	%dl
	xorl	%r8d, %r8d
	addl	$3, %edx
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %rsi
	testb	$1, %al
	je	.L166
	movzwl	%r13w, %edx
	movl	%r15d, %r9d
	movzbl	%r12b, %r8d
	movl	%r14d, %ecx
	movq	-37456(%rbx), %rax
	leaq	-37592(%rbx), %rdi
	movq	%rax, -1(%rsi)
	call	_ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi@PLT
	movq	%rax, %rdx
	andl	$1, %edx
	je	.L171
.L166:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19699:
	.size	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi, .-_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	.section	.rodata._ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsAligned(value, kTaggedSize)"
	.section	.rodata._ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"static_cast<unsigned>(id) < 256"
	.align 8
.LC7:
	.string	"static_cast<unsigned>(value) <= 255"
	.section	.text._ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	.type	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi, @function
_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi:
.LFB19700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	304(%rdi), %r15
	movl	%esi, -64(%rbp)
	movl	%edx, -60(%rbp)
	movq	120(%r15), %r12
	movq	104(%r15), %r8
	testq	%r12, %r12
	je	.L173
	cmpq	%r8, %r12
	ja	.L216
.L174:
	movq	%r8, %rax
	subq	%r12, %rax
	movq	%rax, %r12
.L173:
	leaq	80(%r8), %rax
	cmpq	112(%r15), %rax
	jbe	.L178
	movq	(%r15), %rax
	movl	$1, %edx
	movl	$80, %esi
	movq	%r15, %rdi
	call	*184(%rax)
	testb	%al, %al
	jne	.L217
	movslq	72(%r15), %r8
	salq	$32, %r8
	movq	%r8, %rax
	movq	%r8, %r13
	notq	%rax
	movq	%rax, -56(%rbp)
	testb	$1, -56(%rbp)
	jne	.L207
.L223:
	movq	(%r15), %rax
	movq	%r15, %rdi
	leaq	-1(%r13), %r14
	call	*152(%rax)
	testb	%al, %al
	je	.L218
.L183:
	movq	3184(%rbx), %r12
	movq	3176(%rbx), %r15
	cmpq	%r12, %r15
	je	.L198
	.p2align 4,,10
	.p2align 3
.L184:
	movq	(%r15), %rdi
	addq	$8, %r15
	movl	$80, %edx
	movq	%r14, %rsi
	movq	(%rdi), %rcx
	call	*(%rcx)
	cmpq	%r15, %r12
	jne	.L184
.L198:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	leaq	-37592(%rbx), %r15
	je	.L185
	addl	$1, 400(%rbx)
.L186:
	movq	136(%r15), %rax
	leaq	11(%r13), %r15
	movq	%rax, (%r14)
	movzwl	-64(%rbp), %eax
	movw	%ax, 11(%r13)
	testb	$7, -60(%rbp)
	jne	.L219
	movl	-60(%rbp), %ecx
	movl	%ecx, %eax
	sarl	$3, %eax
	cmpl	$2047, %ecx
	ja	.L220
	movb	%al, 7(%r13)
	movq	%r13, %rdi
	leaq	7(%r13), %rbx
	movq	$0, 47(%r13)
	call	_ZN2v88internal3Map12GetVisitorIdES1_@PLT
	cmpl	$255, %eax
	ja	.L221
	movb	%al, 10(%r13)
	movb	$0, 8(%r13)
	movq	$0, 63(%r13)
	cmpw	$1024, (%r15)
	ja	.L192
	movb	$0, 9(%r13)
.L193:
	movb	$0, 13(%r13)
	movl	$4195327, 15(%r13)
	movl	$0, 19(%r13)
	movb	$24, 14(%r13)
	testb	$1, -56(%rbp)
	jne	.L195
.L207:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	104(%r15), %r8
	leaq	80(%r8), %rax
.L178:
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rax, 104(%r15)
	leaq	1(%r8), %r13
	jne	.L222
.L181:
	movq	%r13, %rax
	notq	%rax
	movq	%rax, -56(%rbp)
	testb	$1, %r13b
	je	.L195
	testb	$1, -56(%rbp)
	jne	.L207
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L216:
	movq	(%r15), %rax
	leaq	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv(%rip), %rsi
	movq	128(%rax), %rdx
	cmpq	%rsi, %rdx
	jne	.L175
	cmpl	$2, 72(%r15)
	jne	.L174
	movq	%r15, %rdi
	call	*152(%rax)
	testb	%al, %al
	jne	.L215
.L176:
	movq	104(%r15), %r12
	movq	%r12, 120(%r15)
	movq	%r12, %r8
.L177:
	testq	%r12, %r12
	jne	.L174
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L185:
	movl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L186
	movl	400(%rbx), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	movl	%eax, 400(%rbx)
	divl	%ecx
	testl	%edx, %edx
	jne	.L186
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r15, %rdi
	call	*%rdx
	testb	%al, %al
	jne	.L176
.L215:
	movq	120(%r15), %r12
	movq	104(%r15), %r8
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	movzbl	(%rbx), %eax
	movzbl	8(%r13), %ecx
	movzbl	8(%r13), %edx
	subl	%ecx, %eax
	addl	%edx, %eax
	cmpl	$255, %eax
	ja	.L224
	movb	%al, 9(%r13)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r15, %rdi
	leal	80(%r12), %esi
	movl	$80, %ecx
	movq	%r14, %rdx
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*48(%rax)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L219:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	.LC7(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19700:
	.size	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi, .-_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	.section	.text._ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	.type	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE, @function
_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE:
.LFB19704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	-37592(%rdi), %rbx
	subq	$24, %rsp
	movq	1072(%rbx), %r12
	movq	%rsi, -40(%rbp)
	movq	%r12, 55(%rsi)
	movq	-40(%rbp), %rdi
	testb	$1, %r12b
	je	.L237
	movq	%r12, %r13
	leaq	55(%rdi), %rsi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L257
	testb	$24, %al
	je	.L237
.L263:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L258
	.p2align 4,,10
	.p2align 3
.L237:
	movq	$0, 71(%rdi)
	xorl	%ecx, %ecx
	leaq	-40(%rbp), %rdi
	movq	%rbx, %rsi
	movq	296(%rbx), %rdx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi@PLT
	movq	-40(%rbp), %rax
	movq	$0, 47(%rax)
	movq	104(%rbx), %r12
	movq	-40(%rbp), %rax
	movq	%r12, 23(%rax)
	movq	-40(%rbp), %rdi
	testb	$1, %r12b
	je	.L236
	movq	%r12, %r13
	leaq	23(%rdi), %rsi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L259
	testb	$24, %al
	je	.L236
.L265:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L260
	.p2align 4,,10
	.p2align 3
.L236:
	movq	104(%rbx), %r12
	movq	%r12, 31(%rdi)
	testb	$1, %r12b
	je	.L225
	movq	%r12, %rbx
	movq	-40(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	31(%rdi), %rsi
	testl	$262144, %eax
	jne	.L261
	testb	$24, %al
	je	.L225
.L264:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L262
.L225:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-40(%rbp), %rdi
	movq	8(%r13), %rax
	leaq	55(%rdi), %rsi
	testb	$24, %al
	jne	.L263
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-40(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	31(%rdi), %rsi
	testb	$24, %al
	jne	.L264
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-40(%rbp), %rdi
	movq	8(%r13), %rax
	leaq	23(%rdi), %rsi
	testb	$24, %al
	jne	.L265
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-40(%rbp), %rdi
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-40(%rbp), %rdi
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19704:
	.size	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE, .-_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	.section	.text._ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	.type	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE, @function
_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE:
.LFB19705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	movzbl	7(%rsi), %esi
	sall	$3, %esi
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	testb	$1, %al
	je	.L268
	movq	%r12, -1(%rax)
	movq	%rax, %r13
	notq	%r13
	testb	%bl, %bl
	jne	.L269
.L270:
	andl	$1, %r13d
	jne	.L279
.L268:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	testb	$1, %r12b
	je	.L270
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	je	.L270
	movq	%rax, %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-40(%rbp), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19705:
	.size	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE, .-_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	.section	.text._ZN2v88internal4Heap17CreateInitialMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap17CreateInitialMapsEv
	.type	_ZN2v88internal4Heap17CreateInitialMapsEv, @function
_ZN2v88internal4Heap17CreateInitialMapsEv:
.LFB19706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$80, %edx
	movl	$68, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andl	$1, %r14d
	jne	.L623
	movq	%rax, -37456(%r12)
	movq	%rax, %rdi
	leaq	-37592(%r12), %rbx
	movq	%rax, -1(%rax)
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L624
.L283:
	xorl	%edx, %edx
	movl	$123, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	testb	$1, %al
	je	.L280
	movq	%rax, 152(%rbx)
	xorl	%edx, %edx
	movl	$148, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andl	$1, %r14d
	jne	.L285
	movq	%rax, 696(%rbx)
	xorl	%edx, %edx
	movl	$167, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	testb	$1, %al
	je	.L280
	movq	%rax, 704(%rbx)
	xorl	%edx, %edx
	movl	$123, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andl	$1, %r14d
	jne	.L289
	movq	%rax, 160(%rbx)
	xorl	%edx, %edx
	movl	$153, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	testb	$1, %al
	je	.L280
	movq	%rax, 480(%rbx)
	movl	$48, %edx
	movl	$67, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andl	$1, %r14d
	jne	.L293
	movq	%rax, 872(%rbx)
	movl	$48, %edx
	movl	$67, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	testb	$1, %al
	je	.L280
	movq	%rax, 888(%rbx)
	movl	$48, %edx
	movl	$67, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andl	$1, %r14d
	jne	.L297
	movq	%rax, 880(%rbx)
	movq	304(%r12), %r15
	movq	120(%r15), %rcx
	movq	104(%r15), %r13
	testq	%rcx, %rcx
	je	.L300
	cmpq	%r13, %rcx
	jbe	.L301
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*128(%rax)
	movq	104(%r15), %r13
	testb	%al, %al
	jne	.L302
	movq	120(%r15), %rcx
.L303:
	testq	%rcx, %rcx
	je	.L300
.L301:
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rax, %rcx
.L300:
	leaq	16(%r13), %rax
	cmpq	112(%r15), %rax
	jbe	.L304
	movq	(%r15), %rax
	movq	%rcx, -72(%rbp)
	movl	$1, %edx
	movq	%r15, %rdi
	movl	$16, %esi
	call	*184(%rax)
	movq	-72(%rbp), %rcx
	testb	%al, %al
	je	.L305
	movq	104(%r15), %r13
	leaq	16(%r13), %rax
.L304:
	movq	%rax, 104(%r15)
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	leaq	1(%r13), %rax
	movq	%rax, -72(%rbp)
	jne	.L625
.L307:
	movq	-72(%rbp), %rdx
	movq	%rdx, %rax
	andl	$1, %edx
	notq	%rax
	je	.L324
.L306:
	movq	%rcx, -80(%rbp)
	testb	$1, %al
	jne	.L280
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*152(%rax)
	movq	-72(%rbp), %rcx
	testb	%al, %al
	leaq	-1(%rcx), %r14
	movq	-80(%rbp), %rcx
	je	.L626
.L309:
	movq	3184(%r12), %r15
	movq	3176(%r12), %r13
	cmpq	%r15, %r13
	je	.L540
	.p2align 4,,10
	.p2align 3
.L310:
	movq	0(%r13), %rdi
	addq	$8, %r13
	movl	$16, %edx
	movq	%r14, %rsi
	movq	(%rdi), %rcx
	call	*(%rcx)
	cmpq	%r13, %r15
	jne	.L310
.L540:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L311
	addl	$1, 400(%r12)
.L312:
	movq	152(%rbx), %rax
	movq	%rax, (%r14)
	movq	-72(%rbp), %rax
	movq	$0, 7(%rax)
	movq	%rax, 288(%rbx)
	movq	304(%r12), %rdi
	movq	120(%rdi), %rcx
	movq	104(%rdi), %r13
	testq	%rcx, %rcx
	je	.L316
	cmpq	%r13, %rcx
	jbe	.L317
	movq	(%rdi), %rax
	movq	%rdi, -72(%rbp)
	call	*128(%rax)
	movq	-72(%rbp), %rdi
	testb	%al, %al
	movq	104(%rdi), %r13
	jne	.L318
	movq	120(%rdi), %rcx
.L319:
	testq	%rcx, %rcx
	je	.L316
.L317:
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rax, %rcx
.L316:
	leaq	16(%r13), %rax
	cmpq	112(%rdi), %rax
	jbe	.L320
	movq	(%rdi), %rax
	movq	%rcx, -80(%rbp)
	movl	$1, %edx
	movl	$16, %esi
	movq	%rdi, -72(%rbp)
	call	*184(%rax)
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rcx
	testb	%al, %al
	je	.L321
	movq	104(%rdi), %r13
	leaq	16(%r13), %rax
.L320:
	movq	%rax, 104(%rdi)
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	leaq	1(%r13), %rax
	movq	%rax, -80(%rbp)
	jne	.L627
.L323:
	movq	-80(%rbp), %rax
	movq	%rax, %r9
	notq	%r9
	testb	$1, %al
	je	.L324
.L322:
	movl	%r9d, %r14d
	movq	%rcx, -72(%rbp)
	andl	$1, %r14d
	je	.L628
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L280:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L629
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movq	%rdi, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L623:
	xorl	%r14d, %r14d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L285:
	xorl	%r14d, %r14d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L289:
	xorl	%r14d, %r14d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L293:
	xorl	%r14d, %r14d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L297:
	xorl	%r14d, %r14d
	jmp	.L280
.L305:
	movslq	72(%r15), %r13
	salq	$32, %r13
	movq	%r13, %rax
	movq	%r13, -72(%rbp)
	notq	%rax
	jmp	.L306
.L311:
	movl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L312
	movl	400(%r12), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	movl	%eax, 400(%r12)
	divl	%ecx
	testl	%edx, %edx
	jne	.L312
	movq	stdout(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	jmp	.L312
.L628:
	movq	(%rdi), %rax
	movq	%rdi, -88(%rbp)
	call	*152(%rax)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdi
	testb	%al, %al
	leaq	-1(%rcx), %r15
	movq	-72(%rbp), %rcx
	je	.L630
.L326:
	movq	3184(%r12), %rax
	movq	3176(%r12), %r13
	movq	%rax, -72(%rbp)
	cmpq	%rax, %r13
	je	.L538
	.p2align 4,,10
	.p2align 3
.L327:
	movq	0(%r13), %rdi
	movl	$16, %edx
	movq	%r15, %rsi
	addq	$8, %r13
	movq	(%rdi), %rcx
	call	*(%rcx)
	cmpq	%r13, -72(%rbp)
	jne	.L327
.L538:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L328
	addl	$1, 400(%r12)
.L329:
	movq	696(%rbx), %rax
	movq	%rax, (%r15)
	movq	-80(%rbp), %rax
	movq	$0, 7(%rax)
	movq	%rax, 1072(%rbx)
	movq	304(%r12), %r15
	movq	120(%r15), %rcx
	movq	104(%r15), %r13
	testq	%rcx, %rcx
	je	.L333
	cmpq	%r13, %rcx
	jbe	.L334
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*128(%rax)
	movq	104(%r15), %r13
	testb	%al, %al
	jne	.L335
	movq	120(%r15), %rcx
.L336:
	testq	%rcx, %rcx
	je	.L333
.L334:
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rax, %rcx
.L333:
	leaq	24(%r13), %rax
	cmpq	112(%r15), %rax
	jbe	.L337
	movq	(%r15), %rax
	movq	%rcx, -72(%rbp)
	movl	$1, %edx
	movq	%r15, %rdi
	movl	$24, %esi
	call	*184(%rax)
	movq	-72(%rbp), %rcx
	testb	%al, %al
	je	.L338
	movq	104(%r15), %r13
	leaq	24(%r13), %rax
.L337:
	movq	%rax, 104(%r15)
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	leaq	1(%r13), %rax
	movq	%rax, -72(%rbp)
	jne	.L631
.L340:
	movq	-72(%rbp), %rsi
	movq	%rsi, %rax
	andl	$1, %esi
	notq	%rax
	je	.L324
.L339:
	movq	%rcx, -80(%rbp)
	testb	$1, %al
	jne	.L280
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*152(%rax)
	movq	-72(%rbp), %rcx
	testb	%al, %al
	leaq	-1(%rcx), %r14
	movq	-80(%rbp), %rcx
	je	.L632
.L342:
	movq	3184(%r12), %r15
	movq	3176(%r12), %r13
	cmpq	%r15, %r13
	je	.L536
	.p2align 4,,10
	.p2align 3
.L343:
	movq	0(%r13), %rdi
	addq	$8, %r13
	movl	$24, %edx
	movq	%r14, %rsi
	movq	(%rdi), %rcx
	call	*(%rcx)
	cmpq	%r13, %r15
	jne	.L343
.L536:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L344
	addl	$1, 400(%r12)
.L345:
	movq	704(%rbx), %rax
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, (%r14)
	movq	-72(%rbp), %rax
	movq	$0, 7(%rax)
	movq	$0, 15(%rax)
	movq	%rax, 1080(%rbx)
	movq	888(%rbx), %rsi
	call	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andl	$1, %r14d
	jne	.L348
	movabsq	$12884901888, %rdx
	movq	%rax, 104(%rbx)
	movq	%r12, %rdi
	movq	%rdx, 39(%rax)
	movl	$4, %edx
	movq	872(%rbx), %rsi
	call	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	testb	$1, %al
	je	.L280
	movabsq	$21474836480, %rdx
	movq	%rax, 88(%rbx)
	movq	%r12, %rdi
	movq	%rdx, 39(%rax)
	movl	$4, %edx
	movq	880(%rbx), %rsi
	call	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andl	$1, %r14d
	jne	.L351
	movabsq	$8589934592, %rdx
	movq	%rax, 96(%rbx)
	leaq	612+_ZN2v88internal4Heap12struct_tableE(%rip), %r13
	movq	%rdx, 39(%rax)
	leaq	-612(%r13), %r15
	movq	104(%rbx), %rax
	movq	%rax, 312(%rbx)
.L353:
	movl	4(%r15), %edx
	movzwl	(%r15), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18AllocatePartialMapENS0_12InstanceTypeEi
	testb	$1, %al
	je	.L280
	movzwl	8(%r15), %edx
	addq	$12, %r15
	movq	%rax, 56(%rbx,%rdx,8)
	cmpq	%r15, %r13
	jne	.L353
	movq	4040(%rbx), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	movq	%rax, %r9
	movq	%rax, %r13
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L356
	movq	288(%rbx), %rdx
	movq	%rax, 960(%rbx)
	leaq	7(%rax), %r15
	movq	%rdx, 7(%rax)
	testb	$1, %dl
	je	.L534
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L358
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	8(%rcx), %rax
.L358:
	testb	$24, %al
	je	.L534
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L534
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L534:
	movq	288(%rbx), %rdx
	leaq	15(%r13), %r15
	movq	%rdx, 15(%r13)
	testb	$1, %dl
	je	.L533
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	je	.L361
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	8(%rcx), %rax
.L361:
	testb	$24, %al
	je	.L533
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L533
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L533:
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$24, %esi
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r15
	testb	$1, %al
	je	.L280
	movq	480(%rbx), %rax
	leaq	-64(%rbp), %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	8+_ZN2v88internal4Heap12struct_tableE(%rip), %r14
	movq	%rax, -1(%r15)
	movq	%r15, -64(%rbp)
	movq	88(%rbx), %rdx
	movq	960(%rbx), %rsi
	call	_ZN2v88internal15DescriptorArray10InitializeENS0_9EnumCacheENS0_10HeapObjectEii@PLT
	movq	%r15, 296(%rbx)
	movq	136(%rbx), %rsi
	movq	%r12, %rdi
	leaq	612(%r14), %r15
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	696(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	704(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	480(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	872(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	872(%rbx), %rax
	movq	%r12, %rdi
	orb	$16, 13(%rax)
	movq	888(%rbx), %rsi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	movq	888(%rbx), %rax
	movq	%r12, %rdi
	orb	$16, 13(%rax)
	movq	880(%rbx), %rsi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
.L365:
	movzwl	(%r14), %eax
	movq	%r12, %rdi
	addq	$12, %r14
	movq	56(%rbx,%rax,8), %rsi
	call	_ZN2v88internal4Heap18FinalizePartialMapENS0_3MapE
	cmpq	%r14, %r15
	jne	.L365
	xorl	%r8d, %r8d
	movl	$3, %ecx
	xorl	%edx, %edx
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L366
	movq	%rax, 200(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$123, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 512(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$125, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L369
	movq	%rax, 424(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$155, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, 272(%rbx)
	movl	$65, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L373
	movq	%rax, 256(%rbx)
	movl	$107, %esi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	$64, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 176(%rbx)
	movl	$180, %esi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	movl	$71, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L377
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, 248(%rbx)
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 896(%rbx)
	movl	$30, %esi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$48, %edx
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L381
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, 904(%rbx)
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, 912(%rbx)
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L385
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, 920(%rbx)
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, 928(%rbx)
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L389
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, 936(%rbx)
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, 944(%rbx)
	movl	$67, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L393
	movq	%rax, 952(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$66, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 448(%rbx)
	leaq	_ZN2v88internal4Heap17string_type_tableE(%rip), %r15
.L397:
	movzwl	(%r15), %esi
	movl	4(%r15), %edx
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movw	%si, -72(%rbp)
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L398
	movl	$178, %esi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	movzwl	-72(%rbp), %eax
	andl	$7, %eax
	subw	$1, %ax
	je	.L633
.L400:
	movzwl	8(%r15), %eax
	movq	-64(%rbp), %rdx
	addq	$12, %r15
	movq	%rdx, 56(%rbx,%rax,8)
	leaq	216+_ZN2v88internal4Heap17string_type_tableE(%rip), %rax
	cmpq	%r15, %rax
	jne	.L397
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movl	$58, %esi
	movq	$0, -64(%rbp)
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movl	$178, %esi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	movq	-64(%rbp), %rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$3, %ecx
	movl	$74, %esi
	movq	%r12, %rdi
	movq	%rax, 736(%rbx)
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L402
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$75, %esi
	movq	%r12, %rdi
	movq	%rax, 488(%rbx)
	movzbl	14(%rax), %edx
	andb	$7, %dl
	orl	$40, %edx
	movb	%dl, 14(%rax)
	xorl	%edx, %edx
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 432(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$70, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L406
	movq	%rax, 144(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$72, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 464(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$73, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L410
	movq	%rax, 56(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$3, %ecx
	movl	$158, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 584(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$161, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L414
	movq	%rax, 632(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$162, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 640(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$163, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L418
	movq	%rax, 648(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$123, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 624(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$69, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L422
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, 216(%rbx)
	movl	$151, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, 232(%rbx)
	movl	$16, %esi
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r9
	movq	%rax, %r15
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L426
	movq	232(%rbx), %rax
	movl	$1, %edx
	leaq	7(%r15), %rsi
	movq	%r15, %rdi
	salq	$32, %rdx
	movq	%rax, -1(%r15)
	movq	%rdx, 7(%r15)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movq	%r15, 4488(%rbx)
	movl	$40, %edx
	movl	$159, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, 240(%rbx)
	movl	$76, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L430
	movq	%rax, 64(%rbx)
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	movl	$76, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 72(%rbx)
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	$154, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L434
	movq	%rax, 528(%rbx)
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map13mark_unstableEv
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	$154, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 544(%rbx)
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map13mark_unstableEv
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movl	$154, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L438
	movq	%rax, 504(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$149, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 264(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$126, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L442
	movq	%rax, 168(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$127, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 552(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$128, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L446
	movq	%rax, 560(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$129, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 568(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$130, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L450
	movq	%rax, 520(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$131, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 496(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$132, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L454
	movq	%rax, 536(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$133, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 616(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$134, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L458
	movq	%rax, 664(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$122, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 720(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$135, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L462
	movq	%rax, 712(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$123, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 440(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$143, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L466
	movq	%rax, 224(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$140, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 392(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$147, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L470
	movq	%rax, 400(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$141, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 408(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$138, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L474
	movq	%rax, 376(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$139, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 384(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$144, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L478
	movq	%rax, 352(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$145, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 344(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$142, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L482
	movq	%rax, 360(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$146, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	movq	%rax, 368(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$137, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L486
	movq	%rax, 416(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$124, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, 456(%rbx)
	movl	$150, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L490
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, 592(%rbx)
	movl	$150, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, 600(%rbx)
	movl	$150, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L494
	movq	%rax, 608(%rbx)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$3, %ecx
	movl	$157, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, 576(%rbx)
	movl	$165, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L498
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, 680(%rbx)
	movl	$166, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, 688(%rbx)
	movl	$160, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L502
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$112, %edx
	movq	%r12, %rdi
	movq	%rax, 208(%rbx)
	movl	$119, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$72, %edx
	movq	%r12, %rdi
	movq	%rax, 656(%rbx)
	movl	$120, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L506
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, 672(%rbx)
	movl	$152, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$72, %edx
	movq	%r12, %rdi
	movq	%rax, 472(%rbx)
	movl	$168, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L510
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$96, %edx
	movq	%r12, %rdi
	movq	%rax, 728(%rbx)
	movl	$1073, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, 4464(%rbx)
	movl	$1057, %esi
	call	_ZN2v88internal4Heap11AllocateMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L514
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, 4456(%rbx)
	movl	15(%rax), %edx
	andl	$-134217729, %edx
	movl	%edx, 15(%rax)
	movl	$4, %edx
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$24, %esi
	movq	%r12, %rdi
	movq	200(%rbx), %rdx
	movq	%rdx, -1(%rax)
	movl	$4, %edx
	movq	$0, 7(%rax)
	movq	%rax, 280(%rbx)
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r9
	movq	%rax, %r15
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L518
	movq	456(%rbx), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -1(%r15)
	movl	$1, %eax
	salq	$32, %rax
	movq	%rax, 7(%r15)
	movq	%r15, -64(%rbp)
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	call	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	movq	%r15, 984(%rbx)
	movl	$4, %edx
	movq	%r12, %rdi
	movq	4000(%rbx), %rsi
	call	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	testb	$1, %al
	je	.L280
	movq	288(%rbx), %r15
	leaq	15(%rax), %r14
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%r15, 15(%rax)
	movq	%r15, %rdx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	movq	-72(%rbp), %rax
	je	.L522
	movq	%rax, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	movq	-72(%rbp), %rax
.L522:
	movq	$0, 7(%rax)
	movq	%rax, 992(%rbx)
	movl	$4, %edx
	movq	%r12, %rdi
	movq	896(%rbx), %rsi
	call	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L523
	movl	$1, %edx
	movq	%rax, 112(%rbx)
	movq	%r12, %rdi
	salq	$32, %rdx
	movq	%rdx, 39(%rax)
	movl	$4, %edx
	movq	896(%rbx), %rsi
	call	_ZN2v88internal4Heap8AllocateENS0_3MapENS0_14AllocationTypeE
	testb	$1, %al
	je	.L280
	movq	%rax, 120(%rbx)
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	$0, 39(%rax)
	movl	$4, %edx
	movl	$16, %esi
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	testb	$1, %al
	je	.L280
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$16, %esi
	movq	%r12, %rdi
	movq	144(%rbx), %rdx
	movq	%rdx, -1(%rax)
	movl	$4, %edx
	movq	$0, 7(%rax)
	movq	%rax, 976(%rbx)
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	movq	%rax, %r9
	notq	%r9
	movl	%r9d, %r14d
	andb	$1, %r14b
	jne	.L529
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$16, %esi
	movq	%r12, %rdi
	movq	584(%rbx), %rdx
	movq	%rdx, -1(%rax)
	movl	$4, %edx
	movq	$0, 7(%rax)
	movq	%rax, 968(%rbx)
	call	_ZN2v88internal4Heap11AllocateRawEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE
	testb	$1, %al
	je	.L280
	movq	424(%rbx), %rdx
	movl	$27, %esi
	movq	%r13, %rdi
	movl	$1, %r14d
	movq	%rdx, -1(%rax)
	movq	$0, 7(%rax)
	movq	%rax, 1000(%rbx)
	movq	448(%rbx), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal3Map27SetConstructorFunctionIndexEi
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	movq	-80(%rbp), %rcx
	jmp	.L307
.L321:
	movslq	72(%rdi), %r13
	salq	$32, %r13
	movq	%r13, %r9
	movq	%r13, -80(%rbp)
	notq	%r9
	jmp	.L322
.L328:
	movl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L329
	movl	400(%r12), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	movl	%eax, 400(%r12)
	divl	%ecx
	testl	%edx, %edx
	jne	.L329
	movq	stdout(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	jmp	.L329
.L626:
	leal	16(%rcx), %esi
	movq	%r15, %rdi
	movl	$16, %ecx
	movq	%r14, %rdx
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*48(%rax)
	jmp	.L309
.L324:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L627:
	movl	$1, %esi
	movq	%rcx, -88(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	movq	-88(%rbp), %rcx
	movq	-72(%rbp), %rdi
	jmp	.L323
.L302:
	movq	%r13, 120(%r15)
	movq	%r13, %rcx
	jmp	.L303
.L338:
	movslq	72(%r15), %r13
	salq	$32, %r13
	movq	%r13, %rax
	movq	%r13, -72(%rbp)
	notq	%rax
	jmp	.L339
.L344:
	movl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L345
	movl	400(%r12), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	movl	%eax, 400(%r12)
	divl	%ecx
	testl	%edx, %edx
	jne	.L345
	movq	stdout(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	jmp	.L345
.L630:
	leal	16(%rcx), %esi
	movq	%r15, %rdx
	movl	$16, %ecx
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L326
.L631:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	movq	-80(%rbp), %rcx
	jmp	.L340
.L348:
	xorl	%r14d, %r14d
	jmp	.L280
.L318:
	movq	%r13, 120(%rdi)
	movq	%r13, %rcx
	jmp	.L319
.L632:
	leal	24(%rcx), %esi
	movq	%r15, %rdi
	movl	$24, %ecx
	movq	%r14, %rdx
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*48(%rax)
	jmp	.L342
.L335:
	movq	%r13, 120(%r15)
	movq	%r13, %rcx
	jmp	.L336
.L351:
	xorl	%r14d, %r14d
	jmp	.L280
.L629:
	call	__stack_chk_fail@PLT
.L356:
	xorl	%r14d, %r14d
	jmp	.L280
.L366:
	xorl	%r14d, %r14d
	jmp	.L280
.L518:
	xorl	%r14d, %r14d
	jmp	.L280
.L529:
	xorl	%r14d, %r14d
	jmp	.L280
.L523:
	xorl	%r14d, %r14d
	jmp	.L280
.L514:
	xorl	%r14d, %r14d
	jmp	.L280
.L510:
	xorl	%r14d, %r14d
	jmp	.L280
.L506:
	xorl	%r14d, %r14d
	jmp	.L280
.L502:
	xorl	%r14d, %r14d
	jmp	.L280
.L498:
	xorl	%r14d, %r14d
	jmp	.L280
.L494:
	xorl	%r14d, %r14d
	jmp	.L280
.L490:
	xorl	%r14d, %r14d
	jmp	.L280
.L486:
	xorl	%r14d, %r14d
	jmp	.L280
.L482:
	xorl	%r14d, %r14d
	jmp	.L280
.L478:
	xorl	%r14d, %r14d
	jmp	.L280
.L474:
	xorl	%r14d, %r14d
	jmp	.L280
.L470:
	xorl	%r14d, %r14d
	jmp	.L280
.L466:
	xorl	%r14d, %r14d
	jmp	.L280
.L462:
	xorl	%r14d, %r14d
	jmp	.L280
.L458:
	xorl	%r14d, %r14d
	jmp	.L280
.L454:
	xorl	%r14d, %r14d
	jmp	.L280
.L450:
	xorl	%r14d, %r14d
	jmp	.L280
.L446:
	xorl	%r14d, %r14d
	jmp	.L280
.L442:
	xorl	%r14d, %r14d
	jmp	.L280
.L438:
	xorl	%r14d, %r14d
	jmp	.L280
.L434:
	xorl	%r14d, %r14d
	jmp	.L280
.L430:
	xorl	%r14d, %r14d
	jmp	.L280
.L426:
	xorl	%r14d, %r14d
	jmp	.L280
.L422:
	xorl	%r14d, %r14d
	jmp	.L280
.L418:
	xorl	%r14d, %r14d
	jmp	.L280
.L414:
	xorl	%r14d, %r14d
	jmp	.L280
.L410:
	xorl	%r14d, %r14d
	jmp	.L280
.L406:
	xorl	%r14d, %r14d
	jmp	.L280
.L402:
	xorl	%r14d, %r14d
	jmp	.L280
.L633:
	movq	%r13, %rdi
	call	_ZN2v88internal3Map13mark_unstableEv
	jmp	.L400
.L398:
	xorl	%r14d, %r14d
	jmp	.L280
.L393:
	xorl	%r14d, %r14d
	jmp	.L280
.L389:
	xorl	%r14d, %r14d
	jmp	.L280
.L385:
	xorl	%r14d, %r14d
	jmp	.L280
.L381:
	xorl	%r14d, %r14d
	jmp	.L280
.L377:
	xorl	%r14d, %r14d
	jmp	.L280
.L373:
	xorl	%r14d, %r14d
	jmp	.L280
.L369:
	xorl	%r14d, %r14d
	jmp	.L280
	.cfi_endproc
.LFE19706:
	.size	_ZN2v88internal4Heap17CreateInitialMapsEv, .-_ZN2v88internal4Heap17CreateInitialMapsEv
	.section	.text._ZN2v88internal4Heap16CreateApiObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap16CreateApiObjectsEv
	.type	_ZN2v88internal4Heap16CreateApiObjectsEv, @function
_ZN2v88internal4Heap16CreateApiObjectsEv:
.LFB19707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-37592(%rdi), %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	3496(%rdi), %r13
	movq	3504(%rdi), %rbx
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12TemplateList3NewEPNS0_7IsolateEi@PLT
	movl	$4, %edx
	movl	$90, %esi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4744(%r12)
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	$0, 71(%rdx)
	movq	(%rax), %rax
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, 1064(%r12)
	cmpq	41096(%r12), %rbx
	je	.L634
	movq	%rbx, 41096(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19707:
	.size	_ZN2v88internal4Heap16CreateApiObjectsEv, .-_ZN2v88internal4Heap16CreateApiObjectsEv
	.section	.rodata._ZN2v88internal4Heap20CreateInitialObjectsEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"undefined"
.LC9:
	.string	"object"
.LC10:
	.string	"null"
.LC11:
	.string	"hole"
.LC12:
	.string	"boolean"
.LC13:
	.string	"true"
.LC14:
	.string	"false"
.LC15:
	.string	"uninitialized"
.LC16:
	.string	"arguments_marker"
.LC17:
	.string	"termination_exception"
.LC18:
	.string	"exception"
.LC19:
	.string	"optimized_out"
.LC20:
	.string	"stale_register"
.LC21:
	.string	"Symbol.asyncIterator"
.LC22:
	.string	"Symbol.iterator"
.LC23:
	.string	"IntlFallback"
.LC24:
	.string	"Symbol.matchAll"
.LC25:
	.string	"Symbol.match"
.LC26:
	.string	"Symbol.replace"
.LC27:
	.string	"Symbol.search"
.LC28:
	.string	"Symbol.species"
.LC29:
	.string	"Symbol.split"
.LC30:
	.string	"Symbol.toPrimitive"
.LC31:
	.string	"Symbol.unscopables"
.LC32:
	.string	"Symbol.hasInstance"
.LC33:
	.string	"Symbol.isConcatSpreadable"
.LC34:
	.string	"Symbol.toStringTag"
	.section	.text._ZN2v88internal4Heap20CreateInitialObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap20CreateInitialObjectsEv
	.type	_ZN2v88internal4Heap20CreateInitialObjectsEv, @function
_ZN2v88internal4Heap20CreateInitialObjectsEv:
.LFB19708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZN2v88internal4Heap21constant_string_tableE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	4880(%r14), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-37592(%rdi), %rbx
	subq	$104, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rbx), %rax
	addl	$1, 41104(%rbx)
	movq	%rax, -112(%rbp)
	movq	41096(%rbx), %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movabsq	$-9223372036854775808, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 7(%rdx)
	movq	(%rax), %rax
	movq	%rax, 1112(%rbx)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movabsq	$9221120237041090560, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 7(%rdx)
	movq	(%rax), %rax
	movq	%rax, 1088(%rbx)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movabsq	$-2251799814209537, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 7(%rdx)
	movq	(%rax), %rax
	movq	%rax, 1096(%rbx)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movabsq	$9218868437227405312, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 7(%rdx)
	movq	(%rax), %rax
	movq	%rax, 1104(%rbx)
	call	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE@PLT
	movl	$8, %esi
	movq	%rbx, %rdi
	movabsq	$-4503599627370496, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 7(%rdx)
	movq	(%rax), %rax
	movl	$4, %edx
	movq	%rax, 1120(%rbx)
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	movq	%r15, %rdi
	movq	(%rax), %rax
	movq	%rax, 1176(%rbx)
	call	_ZN2v88internal4Heap18InitializeHashSeedEv@PLT
	movq	88(%rbx), %rax
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	$256, %esi
	movq	%rax, 4752(%rbx)
	movq	%rax, 4760(%rbx)
	movq	%rax, 4768(%rbx)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2048, %esi
	movq	(%rax), %rax
	movq	%rbx, %rdi
	movq	%rax, 4616(%rbx)
	call	_ZN2v88internal9HashTableINS0_11StringTableENS0_16StringTableShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	(%rax), %rax
	movq	%rax, 4792(%rbx)
	xorl	%eax, %eax
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L862:
	movq	(%r14), %rdi
	call	strlen@PLT
.L639:
	movq	(%r14), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	addq	$16, %r14
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	(%rax), %rdx
	movzwl	-8(%r14), %eax
	movq	%rdx, 56(%rbx,%rax,8)
	cmpq	%r14, %r13
	jne	.L862
	leaq	.LC8(%rip), %r8
	leaq	88(%rbx), %r15
	movq	%rbx, %rdi
	movl	$5, %r9d
	movq	%r15, %rsi
	leaq	1088(%rbx), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal7Oddball10InitializeEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEES7_h@PLT
	movq	41112(%rbx), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L640
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L641:
	leaq	104(%rbx), %rsi
	movl	$3, %r9d
	movq	%rbx, %rdi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rdx
	call	_ZN2v88internal7Oddball10InitializeEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEES7_h@PLT
	movq	%rbx, %rdi
	leaq	96(%rbx), %rsi
	leaq	1096(%rbx), %rcx
	movl	$2, %r9d
	leaq	.LC8(%rip), %r8
	leaq	.LC11(%rip), %rdx
	call	_ZN2v88internal7Oddball10InitializeEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEES7_h@PLT
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L643
	movabsq	$4294967296, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L644:
	leaq	112(%rbx), %rsi
	movq	%rbx, %rdi
	movl	$1, %r9d
	leaq	.LC12(%rip), %r8
	leaq	.LC13(%rip), %rdx
	call	_ZN2v88internal7Oddball10InitializeEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEES7_h@PLT
	movq	41112(%rbx), %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rsi
	testq	%rdi, %rdi
	je	.L646
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L647:
	movq	%rbx, %rdi
	leaq	120(%rbx), %rsi
	xorl	%r9d, %r9d
	leaq	.LC12(%rip), %r8
	leaq	.LC14(%rip), %rdx
	call	_ZN2v88internal7Oddball10InitializeEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEES7_h@PLT
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L649
	movabsq	$-4294967296, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L650:
	subq	$8, %rsp
	leaq	904(%rbx), %rsi
	movq	%rbx, %rdi
	movl	$6, %r9d
	pushq	$4
	leaq	.LC8(%rip), %r8
	leaq	.LC15(%rip), %rdx
	call	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE@PLT
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	%rax, 80(%rbx)
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L652
	movabsq	$-17179869184, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L653:
	subq	$8, %rsp
	movq	%rbx, %rdi
	leaq	.LC16(%rip), %rdx
	movl	$4, %r9d
	pushq	$4
	leaq	912(%rbx), %rsi
	leaq	.LC8(%rip), %r8
	call	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE@PLT
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	%rax, 304(%rbx)
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L655
	movabsq	$-12884901888, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L656:
	subq	$8, %rsp
	movq	%rbx, %rdi
	leaq	928(%rbx), %rsi
	movl	$7, %r9d
	pushq	$4
	leaq	.LC8(%rip), %r8
	leaq	.LC17(%rip), %rdx
	call	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE@PLT
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	%rax, 320(%rbx)
	popq	%r13
	popq	%r14
	testq	%rdi, %rdi
	je	.L658
	movabsq	$-21474836480, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L659:
	subq	$8, %rsp
	movq	%rbx, %rdi
	leaq	920(%rbx), %rsi
	movl	$8, %r9d
	pushq	$4
	leaq	.LC8(%rip), %r8
	leaq	.LC18(%rip), %rdx
	call	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE@PLT
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	%rax, 312(%rbx)
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L661
	movabsq	$-25769803776, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L662:
	subq	$8, %rsp
	movl	$9, %r9d
	movq	%rbx, %rdi
	pushq	$4
	leaq	.LC8(%rip), %r8
	leaq	936(%rbx), %rsi
	leaq	.LC19(%rip), %rdx
	call	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE@PLT
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	%rax, 328(%rbx)
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L664
	movabsq	$-30064771072, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L665:
	subq	$8, %rsp
	movl	$10, %r9d
	leaq	.LC8(%rip), %r8
	movq	%rbx, %rdi
	pushq	$4
	leaq	.LC20(%rip), %rdx
	leaq	944(%rbx), %rsi
	call	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 336(%rbx)
	call	_ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE@PLT
	movq	41096(%rbx), %rdx
	movq	%rbx, %rdi
	movl	$4, %esi
	movq	(%rax), %rax
	addl	$1, 41104(%rbx)
	movq	%rdx, -88(%rbp)
	movq	41088(%rbx), %r14
	movq	%rax, 1128(%rbx)
	movq	88(%rbx), %rax
	movq	%rax, 4776(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3616(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3624(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3632(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3640(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3648(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3656(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3664(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3672(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3680(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3688(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3696(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3704(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3712(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3720(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3728(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3736(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3744(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3752(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3760(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3768(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3776(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3784(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3792(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3800(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3808(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3816(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3824(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 3832(%rbx)
	call	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE@PLT
	movq	41096(%rbx), %r13
	movq	-88(%rbp), %rdx
	movq	(%rax), %rax
	movq	%r14, 41088(%rbx)
	movq	%rax, 3840(%rbx)
	movl	41104(%rbx), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 41104(%rbx)
	popq	%rsi
	popq	%rdi
	cmpq	%r13, %rdx
	je	.L667
	movq	%rdx, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	41104(%rbx), %eax
	movq	41088(%rbx), %r14
	movq	41096(%rbx), %r13
	addl	$1, %eax
.L667:
	movl	%eax, 41104(%rbx)
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$20, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC21(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L755
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L863
	testb	$24, %al
	je	.L755
.L897:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L864
.L755:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3848(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$15, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC22(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L754
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L865
	testb	$24, %al
	je	.L754
.L894:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L866
.L754:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3856(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$12, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC23(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L753
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L867
	testb	$24, %al
	je	.L753
.L896:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L868
.L753:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3864(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$15, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC24(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L752
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L869
	testb	$24, %al
	je	.L752
.L895:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L870
.L752:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3872(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$12, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC25(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L751
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L871
	testb	$24, %al
	je	.L751
.L899:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L872
.L751:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3880(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$14, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC26(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L750
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L873
	testb	$24, %al
	je	.L750
.L898:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L874
.L750:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3888(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$13, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC27(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L749
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L875
	testb	$24, %al
	je	.L749
.L901:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L876
.L749:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3896(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$14, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC28(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L748
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L877
	testb	$24, %al
	je	.L748
.L900:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L878
.L748:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3904(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$12, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC29(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L747
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L879
	testb	$24, %al
	je	.L747
.L903:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L880
.L747:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3912(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$18, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC30(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L746
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L881
	testb	$24, %al
	je	.L746
.L902:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L882
.L746:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3920(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$18, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC31(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	movq	%rdx, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %dl
	je	.L745
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L883
	testb	$24, %al
	je	.L745
.L905:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L884
.L745:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3928(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$18, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC32(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rcx), %rax
	orl	$2, 11(%rax)
	movq	(%rcx), %rdi
	movq	(%r8), %rdx
	leaq	15(%rdi), %rsi
	movq	%rdx, 15(%rdi)
	testb	$1, %dl
	je	.L744
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L885
	testb	$24, %al
	je	.L744
.L904:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L886
.L744:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3936(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$25, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC33(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rcx), %rax
	orl	$2, 11(%rax)
	movq	(%rcx), %rdi
	movq	(%r8), %rdx
	leaq	15(%rdi), %rsi
	movq	%rdx, 15(%rdi)
	testb	$1, %dl
	je	.L743
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L887
	testb	$24, %al
	je	.L743
.L906:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L888
.L743:
	movq	(%rcx), %rax
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	%rax, 3944(%rbx)
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	$18, -72(%rbp)
	movq	%rax, -88(%rbp)
	leaq	.LC34(%rip), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rcx), %rax
	orl	$2, 11(%rax)
	movq	(%rcx), %rdi
	movq	(%r8), %rdx
	leaq	15(%rdi), %rsi
	movq	%rdx, 15(%rdi)
	testb	$1, %dl
	je	.L742
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -88(%rbp)
	testl	$262144, %eax
	jne	.L889
	testb	$24, %al
	je	.L742
.L908:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L890
.L742:
	movq	(%rcx), %rax
	movq	%rax, 3952(%rbx)
	movq	(%rcx), %rax
	orl	$8, 11(%rax)
	movq	%r14, 41088(%rbx)
	subl	$1, 41104(%rbx)
	cmpq	%r13, 41096(%rbx)
	je	.L710
	movq	%r13, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L710:
	movl	$1, %ecx
	movl	$4, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movl	$512, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movq	%rdx, 1056(%rbx)
	movq	%rdx, 4656(%rbx)
	movq	(%rax), %rax
	movl	$1, %edx
	movq	%rax, 4664(%rbx)
	movq	%rax, 4672(%rbx)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movl	$256, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4648(%rbx)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movl	$1, %edx
	movl	$256, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4624(%rbx)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4632(%rbx)
	call	_ZN2v88internal7Factory19NewManyClosuresCellENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4480(%rbx)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	624(%rbx), %rcx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movq	%rcx, -1(%rdx)
	movl	$1, %ecx
	movq	(%rax), %rax
	movq	88(%rbx), %rdx
	movq	%rax, 1008(%rbx)
	movq	1080(%rbx), %rax
	movq	%rdx, 4720(%rbx)
	movq	%rdx, 4784(%rbx)
	movl	$4, %edx
	movq	%rax, 4696(%rbx)
	movq	%rax, 4712(%rbx)
	movq	%rax, 4704(%rbx)
	movq	%rax, 4680(%rbx)
	call	_ZN2v88internal9HashTableINS0_16NumberDictionaryENS0_21NumberDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movabsq	$4294967296, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 39(%rdx)
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%rax, 1016(%rbx)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movl	$3, %esi
	movups	%xmm0, 4800(%rbx)
	movq	$0, 4816(%rbx)
	movq	%rax, 4688(%rbx)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	552(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rcx, -1(%rdx)
	movq	(%rax), %r13
	movl	11(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L711
	leaq	_ZN2v88internal3Smi5kZeroE(%rip), %rcx
	movq	%rbx, -128(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, %rbx
	movq	%r12, -136(%rbp)
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L715:
	movq	(%r12), %r14
	leaq	15(%r13,%r15,8), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L739
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rdx
	movq	%r8, -88(%rbp)
	testl	$262144, %edx
	je	.L713
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rsi
	movq	8(%r8), %rdx
.L713:
	andl	$24, %edx
	je	.L739
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L739
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L739:
	movq	(%rbx), %r13
	addq	$1, %r15
	cmpl	%r15d, 11(%r13)
	jg	.L715
	movq	-128(%rbp), %rbx
	movq	-136(%rbp), %r12
.L711:
	movq	%r13, 1024(%rbx)
	movl	$4, %edx
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	560(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rcx, -1(%rdx)
	movq	(%rax), %r13
	movl	11(%r13), %edx
	testl	%edx, %edx
	jle	.L716
	leaq	_ZN2v88internal3Smi5kZeroE(%rip), %rcx
	movq	%rbx, -128(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, %rbx
	movq	%r12, -136(%rbp)
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L720:
	movq	(%r12), %r14
	leaq	15(%r13,%r15,8), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L740
	movq	%r14, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rdx
	movq	%r8, -88(%rbp)
	testl	$262144, %edx
	je	.L718
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rsi
	movq	8(%r8), %rdx
.L718:
	andl	$24, %edx
	je	.L740
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L740
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L740:
	movq	(%rbx), %r13
	addq	$1, %r15
	cmpl	%r15d, 11(%r13)
	jg	.L720
	movq	-128(%rbp), %rbx
	movq	-136(%rbp), %r12
.L716:
	movq	%r13, 1032(%rbx)
	movl	$4, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	128(%rbx), %r13
	call	_ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 1040(%rbx)
	call	_ZN2v88internal9ScopeInfo23CreateGlobalThisBindingEPNS0_7IsolateE@PLT
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 1160(%rbx)
	call	_ZN2v88internal9ScopeInfo22CreateForEmptyFunctionEPNS0_7IsolateE@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	%rax, 1168(%rbx)
	call	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	$0, 47(%rdx)
	movq	(%rax), %rcx
	movslq	99(%rcx), %rdx
	andl	$-61, %edx
	orl	$4, %edx
	salq	$32, %rdx
	movq	%rdx, 95(%rcx)
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	%rax, 4472(%rbx)
	testq	%rdi, %rdi
	je	.L721
	movabsq	$4294967296, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L722:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4496(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movabsq	$4294967296, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 23(%rdx)
	movl	$4, %edx
	movq	(%rax), %rax
	movq	%rax, 4504(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	96(%rbx), %r15
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	%r15, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r15b
	je	.L741
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L891
	testb	$24, %al
	je	.L741
.L907:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L892
.L741:
	movq	(%r14), %rax
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movabsq	$4294967296, %r14
	movq	%rax, 1048(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%rax, 4552(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%rax, 4584(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	%rax, 4600(%rbx)
	testq	%rdi, %rdi
	je	.L727
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L728:
	movabsq	$4294967296, %r14
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4512(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%rax, 4520(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%rax, 4528(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%rax, 4536(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	%rax, 4608(%rbx)
	testq	%rdi, %rdi
	je	.L730
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L731:
	movabsq	$4294967296, %r14
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4544(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movl	$1, %edx
	movq	(%rax), %rax
	movq	%rax, 4560(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rdx
	movq	%r14, 23(%rdx)
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	%rax, 4568(%rbx)
	testq	%rdi, %rdi
	je	.L733
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L734:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 4576(%rbx)
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE@PLT
	movabsq	$4294967296, %rcx
	movq	(%rax), %rdx
	movq	%rcx, 23(%rdx)
	movq	288(%rbx), %xmm0
	movq	(%rax), %rax
	movq	%rax, 4592(%rbx)
	movq	-104(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 4728(%rbx)
	movq	64(%rax), %rdi
	call	_ZN2v88internal8Builtins34GenerateOffHeapTrampolineRelocInfoEPNS0_7IsolateE@PLT
	movl	$4, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 1136(%rbx)
	call	_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE@PLT
	movl	$4, %edx
	movl	$16, %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	%rax, 1144(%rbx)
	call	_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %rax
	movq	%rax, 1152(%rbx)
	movq	3608(%rbx), %rax
	testb	$1, 7(%rax)
	je	.L736
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
.L736:
	movq	3040(%rbx), %rax
	testb	$1, 7(%rax)
	je	.L737
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
.L737:
	movq	288(%rbx), %rax
	movq	-104(%rbp), %r15
	movq	%rax, 4640(%rbx)
	movq	64(%r15), %rax
	movq	41080(%rax), %rdi
	call	_ZN2v88internal21DescriptorLookupCache5ClearEv@PLT
	movq	64(%r15), %rax
	movq	40952(%rax), %rdi
	call	_ZN2v88internal16CompilationCache5ClearEv@PLT
	movq	-112(%rbp), %rax
	subl	$1, 41104(%rbx)
	movq	%rax, 41088(%rbx)
	movq	-120(%rbp), %rax
	cmpq	41096(%rbx), %rax
	je	.L637
	movq	%rax, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L637:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L893
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L865:
	.cfi_restore_state
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L894
	jmp	.L754
.L869:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L895
	jmp	.L752
.L867:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L896
	jmp	.L753
.L863:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L897
	jmp	.L755
.L873:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L898
	jmp	.L750
.L871:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L899
	jmp	.L751
.L877:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L900
	jmp	.L748
.L875:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L901
	jmp	.L749
.L881:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L902
	jmp	.L746
.L879:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L903
	jmp	.L747
.L885:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L904
	jmp	.L744
.L883:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L905
	jmp	.L745
.L887:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L906
	jmp	.L743
.L891:
	movq	%r15, %rdx
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-128(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L907
	jmp	.L741
.L889:
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-96(%rbp), %rdi
	testb	$24, %al
	jne	.L908
	jmp	.L742
.L664:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L909
.L666:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$-30064771072, %rax
	movq	%rax, (%rcx)
	jmp	.L665
.L721:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L910
.L723:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, (%rsi)
	jmp	.L722
.L733:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L911
.L735:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, (%rsi)
	jmp	.L734
.L730:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L912
.L732:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, (%rsi)
	jmp	.L731
.L727:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L913
.L729:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, (%rsi)
	jmp	.L728
.L661:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L914
.L663:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$-25769803776, %rax
	movq	%rax, (%rcx)
	jmp	.L662
.L658:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L915
.L660:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$-21474836480, %rax
	movq	%rax, (%rcx)
	jmp	.L659
.L655:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L916
.L657:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$-12884901888, %rax
	movq	%rax, (%rcx)
	jmp	.L656
.L652:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L917
.L654:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$-17179869184, %rax
	movq	%rax, (%rcx)
	jmp	.L653
.L649:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L918
.L651:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$-4294967296, %rax
	movq	%rax, (%rcx)
	jmp	.L650
.L646:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L919
.L648:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L647
.L643:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L920
.L645:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, (%rcx)
	jmp	.L644
.L640:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L921
.L642:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L641
.L864:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L755
.L866:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L754
.L868:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L753
.L870:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L752
.L872:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L751
.L874:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L750
.L876:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L749
.L878:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L748
.L880:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L747
.L882:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L746
.L884:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L745
.L890:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L742
.L886:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L744
.L888:
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	jmp	.L743
.L892:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L741
.L921:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L642
.L920:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L645
.L919:
	movq	%rbx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L648
.L918:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L651
.L917:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L654
.L916:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L657
.L915:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L660
.L914:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L663
.L913:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L729
.L912:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L732
.L911:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L735
.L910:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L723
.L909:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L666
.L893:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19708:
	.size	_ZN2v88internal4Heap20CreateInitialObjectsEv, .-_ZN2v88internal4Heap20CreateInitialObjectsEv
	.section	.text._ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv
	.type	_ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv, @function
_ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv:
.LFB19718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-37592(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	41088(%r12), %r13
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	call	_ZN2v88internal9Accessors25MakeArgumentsIteratorInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4368(%r12)
	call	_ZN2v88internal9Accessors19MakeArrayLengthInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4376(%r12)
	call	_ZN2v88internal9Accessors27MakeBoundFunctionLengthInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4384(%r12)
	call	_ZN2v88internal9Accessors25MakeBoundFunctionNameInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4392(%r12)
	call	_ZN2v88internal9Accessors18MakeErrorStackInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4400(%r12)
	call	_ZN2v88internal9Accessors25MakeFunctionArgumentsInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4408(%r12)
	call	_ZN2v88internal9Accessors22MakeFunctionCallerInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4416(%r12)
	call	_ZN2v88internal9Accessors20MakeFunctionNameInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4424(%r12)
	call	_ZN2v88internal9Accessors22MakeFunctionLengthInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4432(%r12)
	call	_ZN2v88internal9Accessors25MakeFunctionPrototypeInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, 4440(%r12)
	call	_ZN2v88internal9Accessors20MakeStringLengthInfoEPNS0_7IsolateE@PLT
	movq	4368(%r12), %rdx
	movq	(%rax), %rax
	movq	%rax, 4448(%r12)
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4368(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4376(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4376(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4384(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4384(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4392(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4392(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4400(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$64, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4400(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4408(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4408(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4416(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4416(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4424(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4424(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4432(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4432(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4440(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4440(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4448(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-97, %eax
	orl	$32, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	4448(%r12), %rdx
	movslq	19(%rdx), %rax
	andl	$-385, %eax
	orb	$1, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L922
	movq	%rbx, 41096(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19718:
	.size	_ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv, .-_ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv
	.section	.rodata._ZN2v88internal4Heap17CreateHeapObjectsEv.str1.1,"aMS",@progbits,1
.LC35:
	.string	"0u == gc_count_"
	.section	.text._ZN2v88internal4Heap17CreateHeapObjectsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Heap17CreateHeapObjectsEv
	.type	_ZN2v88internal4Heap17CreateHeapObjectsEv, @function
_ZN2v88internal4Heap17CreateHeapObjectsEv:
.LFB19698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal4Heap17CreateInitialMapsEv
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L932
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap16CreateApiObjectsEv
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap20CreateInitialObjectsEv
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv
	movl	452(%rbx), %eax
	testl	%eax, %eax
	jne	.L933
	movq	-37504(%rbx), %xmm0
	movl	%r12d, %eax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 1528(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L933:
	.cfi_restore_state
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19698:
	.size	_ZN2v88internal4Heap17CreateHeapObjectsEv, .-_ZN2v88internal4Heap17CreateHeapObjectsEv
	.section	.text._ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE
	.type	_ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE, @function
_ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE:
.LFB19697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal4Heap17CreateInitialMapsEv
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L941
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L941:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap16CreateApiObjectsEv
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap20CreateInitialObjectsEv
	movq	%rbx, %rdi
	call	_ZN2v88internal4Heap33CreateInternalAccessorInfoObjectsEv
	movl	452(%rbx), %eax
	testl	%eax, %eax
	jne	.L942
	movq	-37504(%rbx), %xmm0
	movl	%r12d, %eax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 1528(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19697:
	.size	_ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE, .-_ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE, @function
_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE:
.LFB24700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24700:
	.size	_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE, .-_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE
	.globl	_ZN2v88internal4Heap12struct_tableE
	.section	.rodata._ZN2v88internal4Heap12struct_tableE,"a"
	.align 32
	.type	_ZN2v88internal4Heap12struct_tableE, @object
	.size	_ZN2v88internal4Heap12struct_tableE, 612
_ZN2v88internal4Heap12struct_tableE:
	.value	77
	.zero	2
	.long	40
	.value	488
	.zero	2
	.value	78
	.zero	2
	.long	64
	.value	489
	.zero	2
	.value	79
	.zero	2
	.long	24
	.value	490
	.zero	2
	.value	80
	.zero	2
	.long	16
	.value	491
	.zero	2
	.value	81
	.zero	2
	.long	16
	.value	492
	.zero	2
	.value	82
	.zero	2
	.long	24
	.value	493
	.zero	2
	.value	83
	.zero	2
	.long	40
	.value	494
	.zero	2
	.value	84
	.zero	2
	.long	40
	.value	495
	.zero	2
	.value	85
	.zero	2
	.long	24
	.value	496
	.zero	2
	.value	86
	.zero	2
	.long	72
	.value	497
	.zero	2
	.value	87
	.zero	2
	.long	24
	.value	498
	.zero	2
	.value	88
	.zero	2
	.long	112
	.value	499
	.zero	2
	.value	89
	.zero	2
	.long	72
	.value	500
	.zero	2
	.value	90
	.zero	2
	.long	80
	.value	501
	.zero	2
	.value	91
	.zero	2
	.long	24
	.value	502
	.zero	2
	.value	92
	.zero	2
	.long	64
	.value	503
	.zero	2
	.value	93
	.zero	2
	.long	32
	.value	504
	.zero	2
	.value	94
	.zero	2
	.long	40
	.value	505
	.zero	2
	.value	95
	.zero	2
	.long	56
	.value	506
	.zero	2
	.value	96
	.zero	2
	.long	128
	.value	507
	.zero	2
	.value	97
	.zero	2
	.long	24
	.value	508
	.zero	2
	.value	98
	.zero	2
	.long	64
	.value	509
	.zero	2
	.value	99
	.zero	2
	.long	112
	.value	510
	.zero	2
	.value	100
	.zero	2
	.long	40
	.value	511
	.zero	2
	.value	101
	.zero	2
	.long	24
	.value	512
	.zero	2
	.value	102
	.zero	2
	.long	24
	.value	513
	.zero	2
	.value	103
	.zero	2
	.long	32
	.value	514
	.zero	2
	.value	104
	.zero	2
	.long	40
	.value	515
	.zero	2
	.value	105
	.zero	2
	.long	48
	.value	516
	.zero	2
	.value	106
	.zero	2
	.long	16
	.value	517
	.zero	2
	.value	107
	.zero	2
	.long	64
	.value	518
	.zero	2
	.value	108
	.zero	2
	.long	48
	.value	519
	.zero	2
	.value	109
	.zero	2
	.long	48
	.value	520
	.zero	2
	.value	110
	.zero	2
	.long	24
	.value	521
	.zero	2
	.value	111
	.zero	2
	.long	24
	.value	522
	.zero	2
	.value	112
	.zero	2
	.long	40
	.value	523
	.zero	2
	.value	113
	.zero	2
	.long	40
	.value	524
	.zero	2
	.value	114
	.zero	2
	.long	40
	.value	525
	.zero	2
	.value	115
	.zero	2
	.long	136
	.value	526
	.zero	2
	.value	116
	.zero	2
	.long	24
	.value	527
	.zero	2
	.value	117
	.zero	2
	.long	24
	.value	528
	.zero	2
	.value	118
	.zero	2
	.long	24
	.value	529
	.zero	2
	.value	121
	.zero	2
	.long	48
	.value	530
	.zero	2
	.value	121
	.zero	2
	.long	40
	.value	531
	.zero	2
	.value	156
	.zero	2
	.long	32
	.value	532
	.zero	2
	.value	156
	.zero	2
	.long	40
	.value	533
	.zero	2
	.value	156
	.zero	2
	.long	48
	.value	534
	.zero	2
	.value	164
	.zero	2
	.long	24
	.value	535
	.zero	2
	.value	164
	.zero	2
	.long	32
	.value	536
	.zero	2
	.value	164
	.zero	2
	.long	40
	.value	537
	.zero	2
	.value	164
	.zero	2
	.long	48
	.value	538
	.zero	2
	.globl	_ZN2v88internal4Heap21constant_string_tableE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC36:
	.string	""
.LC37:
	.string	"adoptText"
.LC38:
	.string	"baseName"
.LC39:
	.string	"accounting"
.LC40:
	.string	"breakType"
.LC41:
	.string	"calendar"
.LC42:
	.string	"cardinal"
.LC43:
	.string	"caseFirst"
.LC44:
	.string	"compare"
.LC45:
	.string	"current"
.LC46:
	.string	"collation"
.LC47:
	.string	"compact"
.LC48:
	.string	"compactDisplay"
.LC49:
	.string	"currency"
.LC50:
	.string	"currencyDisplay"
.LC51:
	.string	"currencySign"
.LC52:
	.string	"dateStyle"
.LC53:
	.string	"day"
.LC54:
	.string	"dayPeriod"
.LC55:
	.string	"decimal"
.LC56:
	.string	"endRange"
.LC57:
	.string	"engineering"
.LC58:
	.string	"era"
.LC59:
	.string	"exceptZero"
.LC60:
	.string	"exponentInteger"
.LC61:
	.string	"exponentMinusSign"
.LC62:
	.string	"exponentSeparator"
.LC63:
	.string	"first"
.LC64:
	.string	"format"
.LC65:
	.string	"fraction"
.LC66:
	.string	"fractionalSecond"
.LC67:
	.string	"fractionalSecondDigits"
.LC68:
	.string	"full"
.LC69:
	.string	"granularity"
.LC70:
	.string	"grapheme"
.LC71:
	.string	"group"
.LC72:
	.string	"h11"
.LC73:
	.string	"h12"
.LC74:
	.string	"h23"
.LC75:
	.string	"h24"
.LC76:
	.string	"hour"
.LC77:
	.string	"hour12"
.LC78:
	.string	"hourCycle"
.LC79:
	.string	"ideo"
.LC80:
	.string	"ignorePunctuation"
.LC81:
	.string	"Invalid Date"
.LC82:
	.string	"integer"
.LC83:
	.string	"kana"
.LC84:
	.string	"language"
.LC85:
	.string	"letter"
.LC86:
	.string	"list"
.LC87:
	.string	"literal"
.LC88:
	.string	"locale"
.LC89:
	.string	"loose"
.LC90:
	.string	"lower"
.LC91:
	.string	"maximumFractionDigits"
.LC92:
	.string	"maximumSignificantDigits"
.LC93:
	.string	"minimumFractionDigits"
.LC94:
	.string	"minimumIntegerDigits"
.LC95:
	.string	"minimumSignificantDigits"
.LC96:
	.string	"minusSign"
.LC97:
	.string	"minute"
.LC98:
	.string	"month"
.LC99:
	.string	"nan"
.LC100:
	.string	"narrowSymbol"
.LC101:
	.string	"never"
.LC102:
	.string	"none"
.LC103:
	.string	"notation"
.LC104:
	.string	"normal"
.LC105:
	.string	"numberingSystem"
.LC106:
	.string	"numeric"
.LC107:
	.string	"ordinal"
.LC108:
	.string	"percentSign"
.LC109:
	.string	"plusSign"
.LC110:
	.string	"quarter"
.LC111:
	.string	"region"
.LC112:
	.string	"scientific"
.LC113:
	.string	"second"
.LC114:
	.string	"segment"
.LC115:
	.string	"Segment Iterator"
.LC116:
	.string	"sensitivity"
.LC117:
	.string	"sep"
.LC118:
	.string	"shared"
.LC119:
	.string	"signDisplay"
.LC120:
	.string	"standard"
.LC121:
	.string	"startRange"
.LC122:
	.string	"strict"
.LC123:
	.string	"style"
.LC124:
	.string	"term"
.LC125:
	.string	"timeStyle"
.LC126:
	.string	"timeZone"
.LC127:
	.string	"timeZoneName"
.LC128:
	.string	"type"
.LC129:
	.string	"unknown"
.LC130:
	.string	"upper"
.LC131:
	.string	"usage"
.LC132:
	.string	"useGrouping"
.LC133:
	.string	"UTC"
.LC134:
	.string	"unit"
.LC135:
	.string	"unitDisplay"
.LC136:
	.string	"weekday"
.LC137:
	.string	"year"
.LC138:
	.string	"add"
.LC139:
	.string	"always"
.LC140:
	.string	"(anonymous function)"
.LC141:
	.string	"anonymous"
.LC142:
	.string	"apply"
.LC143:
	.string	"Arguments"
.LC144:
	.string	"arguments"
.LC145:
	.string	"[object Arguments]"
.LC146:
	.string	"Array"
.LC147:
	.string	"[object Array]"
.LC148:
	.string	"ArrayBuffer"
.LC149:
	.string	"Array Iterator"
.LC150:
	.string	"as"
.LC151:
	.string	"async"
.LC152:
	.string	"auto"
.LC153:
	.string	"await"
.LC154:
	.string	"BigInt"
.LC155:
	.string	"bigint"
.LC156:
	.string	"BigInt64Array"
.LC157:
	.string	"BigUint64Array"
.LC158:
	.string	"bind"
.LC159:
	.string	"Boolean"
.LC160:
	.string	"[object Boolean]"
.LC161:
	.string	"bound "
.LC162:
	.string	"buffer"
.LC163:
	.string	"byteLength"
.LC164:
	.string	"byteOffset"
.LC165:
	.string	"CompileError"
.LC166:
	.string	"callee"
.LC167:
	.string	"caller"
.LC168:
	.string	"character"
.LC169:
	.string	"(closure)"
.LC170:
	.string	"code"
.LC171:
	.string	"column"
.LC172:
	.string	"<computed>"
.LC173:
	.string	"configurable"
.LC174:
	.string	"conjunction"
.LC175:
	.string	"construct"
.LC176:
	.string	"constructor"
.LC177:
	.string	"Date"
.LC178:
	.string	"[object Date]"
.LC179:
	.string	"default"
.LC180:
	.string	"defineProperty"
.LC181:
	.string	"deleteProperty"
.LC182:
	.string	"disjunction"
.LC183:
	.string	"displayName"
.LC184:
	.string	"done"
.LC185:
	.string	".brand"
.LC186:
	.string	".catch"
.LC187:
	.string	".default"
.LC188:
	.string	".for"
.LC189:
	.string	".generator_object"
.LC190:
	.string	".iterator"
.LC191:
	.string	".promise"
.LC192:
	.string	".result"
.LC193:
	.string	"."
.LC194:
	.string	".switch_tag"
.LC195:
	.string	"dotAll"
.LC196:
	.string	"enumerable"
.LC197:
	.string	"element"
.LC198:
	.string	"Error"
.LC199:
	.string	"[object Error]"
.LC200:
	.string	"eval"
.LC201:
	.string	"EvalError"
.LC202:
	.string	"exec"
.LC203:
	.string	"flags"
.LC204:
	.string	"Float32Array"
.LC205:
	.string	"Float64Array"
.LC206:
	.string	"from"
.LC207:
	.string	"Function"
.LC208:
	.string	"function () { [native code] }"
.LC209:
	.string	"function"
.LC210:
	.string	"[object Function]"
.LC211:
	.string	"Generator"
.LC212:
	.string	"get "
.LC213:
	.string	"get"
.LC214:
	.string	"getOwnPropertyDescriptor"
.LC215:
	.string	"getPrototypeOf"
.LC216:
	.string	"global"
.LC217:
	.string	"globalThis"
.LC218:
	.string	"groups"
.LC219:
	.string	"has"
.LC220:
	.string	"ignoreCase"
.LC221:
	.string	"illegal access"
.LC222:
	.string	"illegal argument"
.LC223:
	.string	"index"
.LC224:
	.string	"Infinity"
.LC225:
	.string	"infinity"
.LC226:
	.string	"input"
.LC227:
	.string	"Int16Array"
.LC228:
	.string	"Int32Array"
.LC229:
	.string	"Int8Array"
.LC230:
	.string	"isExtensible"
.LC231:
	.string	"keys"
.LC232:
	.string	"lastIndex"
.LC233:
	.string	"length"
.LC234:
	.string	"let"
.LC235:
	.string	"line"
.LC236:
	.string	"LinkError"
.LC237:
	.string	"long"
.LC238:
	.string	"Map"
.LC239:
	.string	"Map Iterator"
.LC240:
	.string	"medium"
.LC241:
	.string	"message"
.LC242:
	.string	"meta"
.LC243:
	.string	"-Infinity"
.LC244:
	.string	"Module"
.LC245:
	.string	"multiline"
.LC246:
	.string	"name"
.LC247:
	.string	"NaN"
.LC248:
	.string	"narrow"
.LC249:
	.string	"native"
.LC250:
	.string	".new.target"
.LC251:
	.string	"next"
.LC252:
	.string	"NFC"
.LC253:
	.string	"NFD"
.LC254:
	.string	"NFKC"
.LC255:
	.string	"NFKD"
.LC256:
	.string	"not-equal"
.LC257:
	.string	"[object Null]"
.LC258:
	.string	"Number"
.LC259:
	.string	"number"
.LC260:
	.string	"[object Number]"
.LC261:
	.string	"Object"
.LC262:
	.string	"[object Object]"
.LC263:
	.string	"of"
.LC264:
	.string	"ok"
.LC265:
	.string	"1"
.LC266:
	.string	"ownKeys"
.LC267:
	.string	"percent"
.LC268:
	.string	"position"
.LC269:
	.string	"preventExtensions"
.LC270:
	.string	"#constructor"
.LC271:
	.string	"Promise"
.LC272:
	.string	"__proto__"
.LC273:
	.string	"prototype"
.LC274:
	.string	"proxy"
.LC275:
	.string	"Proxy"
.LC276:
	.string	"(?:)"
.LC277:
	.string	"RangeError"
.LC278:
	.string	"raw"
.LC279:
	.string	"ReferenceError"
.LC280:
	.string	"Reflect.get"
.LC281:
	.string	"Reflect.has"
.LC282:
	.string	"RegExp"
.LC283:
	.string	"[object RegExp]"
.LC284:
	.string	"resolve"
.LC285:
	.string	"return"
.LC286:
	.string	"revoke"
.LC287:
	.string	"RuntimeError"
.LC288:
	.string	"Script"
.LC289:
	.string	"script"
.LC290:
	.string	"short"
.LC291:
	.string	"Set"
.LC292:
	.string	"sentence"
.LC293:
	.string	"set "
.LC294:
	.string	"set"
.LC295:
	.string	"Set Iterator"
.LC296:
	.string	"setPrototypeOf"
.LC297:
	.string	"SharedArrayBuffer"
.LC298:
	.string	"source"
.LC299:
	.string	"sourceText"
.LC300:
	.string	"stack"
.LC301:
	.string	"stackTraceLimit"
.LC302:
	.string	"sticky"
.LC303:
	.string	"String"
.LC304:
	.string	"string"
.LC305:
	.string	"[object String]"
.LC306:
	.string	"[Symbol.species]"
.LC307:
	.string	"Symbol"
.LC308:
	.string	"symbol"
.LC309:
	.string	"SyntaxError"
.LC310:
	.string	"target"
.LC311:
	.string	"then"
.LC312:
	.string	".this_function"
.LC313:
	.string	"this"
.LC314:
	.string	"throw"
.LC315:
	.string	"timed-out"
.LC316:
	.string	"toJSON"
.LC317:
	.string	"toString"
.LC318:
	.string	"TypeError"
.LC319:
	.string	"Uint16Array"
.LC320:
	.string	"Uint32Array"
.LC321:
	.string	"Uint8Array"
.LC322:
	.string	"Uint8ClampedArray"
.LC323:
	.string	"[object Undefined]"
.LC324:
	.string	"unicode"
.LC325:
	.string	"URIError"
.LC326:
	.string	"value"
.LC327:
	.string	"valueOf"
.LC328:
	.string	"WeakMap"
.LC329:
	.string	"WeakRef"
.LC330:
	.string	"WeakSet"
.LC331:
	.string	"week"
.LC332:
	.string	"word"
.LC333:
	.string	"writable"
.LC334:
	.string	"0"
	.section	.data.rel.ro.local._ZN2v88internal4Heap21constant_string_tableE,"aw"
	.align 32
	.type	_ZN2v88internal4Heap21constant_string_tableE, @object
	.size	_ZN2v88internal4Heap21constant_string_tableE, 4880
_ZN2v88internal4Heap21constant_string_tableE:
	.quad	.LC36
	.value	9
	.zero	6
	.quad	.LC37
	.value	141
	.zero	6
	.quad	.LC38
	.value	142
	.zero	6
	.quad	.LC39
	.value	143
	.zero	6
	.quad	.LC40
	.value	144
	.zero	6
	.quad	.LC41
	.value	145
	.zero	6
	.quad	.LC42
	.value	146
	.zero	6
	.quad	.LC43
	.value	147
	.zero	6
	.quad	.LC44
	.value	148
	.zero	6
	.quad	.LC45
	.value	149
	.zero	6
	.quad	.LC46
	.value	150
	.zero	6
	.quad	.LC47
	.value	151
	.zero	6
	.quad	.LC48
	.value	152
	.zero	6
	.quad	.LC49
	.value	153
	.zero	6
	.quad	.LC50
	.value	154
	.zero	6
	.quad	.LC51
	.value	155
	.zero	6
	.quad	.LC52
	.value	156
	.zero	6
	.quad	.LC53
	.value	157
	.zero	6
	.quad	.LC54
	.value	158
	.zero	6
	.quad	.LC55
	.value	159
	.zero	6
	.quad	.LC56
	.value	160
	.zero	6
	.quad	.LC57
	.value	161
	.zero	6
	.quad	.LC58
	.value	162
	.zero	6
	.quad	.LC59
	.value	163
	.zero	6
	.quad	.LC60
	.value	164
	.zero	6
	.quad	.LC61
	.value	165
	.zero	6
	.quad	.LC62
	.value	166
	.zero	6
	.quad	.LC63
	.value	167
	.zero	6
	.quad	.LC64
	.value	168
	.zero	6
	.quad	.LC65
	.value	169
	.zero	6
	.quad	.LC66
	.value	170
	.zero	6
	.quad	.LC67
	.value	171
	.zero	6
	.quad	.LC68
	.value	172
	.zero	6
	.quad	.LC69
	.value	173
	.zero	6
	.quad	.LC70
	.value	174
	.zero	6
	.quad	.LC71
	.value	175
	.zero	6
	.quad	.LC72
	.value	176
	.zero	6
	.quad	.LC73
	.value	177
	.zero	6
	.quad	.LC74
	.value	178
	.zero	6
	.quad	.LC75
	.value	179
	.zero	6
	.quad	.LC76
	.value	180
	.zero	6
	.quad	.LC77
	.value	181
	.zero	6
	.quad	.LC78
	.value	182
	.zero	6
	.quad	.LC79
	.value	183
	.zero	6
	.quad	.LC80
	.value	184
	.zero	6
	.quad	.LC81
	.value	185
	.zero	6
	.quad	.LC82
	.value	186
	.zero	6
	.quad	.LC83
	.value	187
	.zero	6
	.quad	.LC84
	.value	188
	.zero	6
	.quad	.LC85
	.value	189
	.zero	6
	.quad	.LC86
	.value	190
	.zero	6
	.quad	.LC87
	.value	191
	.zero	6
	.quad	.LC88
	.value	192
	.zero	6
	.quad	.LC89
	.value	193
	.zero	6
	.quad	.LC90
	.value	194
	.zero	6
	.quad	.LC91
	.value	195
	.zero	6
	.quad	.LC92
	.value	196
	.zero	6
	.quad	.LC93
	.value	197
	.zero	6
	.quad	.LC94
	.value	198
	.zero	6
	.quad	.LC95
	.value	199
	.zero	6
	.quad	.LC96
	.value	200
	.zero	6
	.quad	.LC97
	.value	201
	.zero	6
	.quad	.LC98
	.value	202
	.zero	6
	.quad	.LC99
	.value	203
	.zero	6
	.quad	.LC100
	.value	204
	.zero	6
	.quad	.LC101
	.value	205
	.zero	6
	.quad	.LC102
	.value	206
	.zero	6
	.quad	.LC103
	.value	207
	.zero	6
	.quad	.LC104
	.value	208
	.zero	6
	.quad	.LC105
	.value	209
	.zero	6
	.quad	.LC106
	.value	210
	.zero	6
	.quad	.LC107
	.value	211
	.zero	6
	.quad	.LC108
	.value	212
	.zero	6
	.quad	.LC109
	.value	213
	.zero	6
	.quad	.LC110
	.value	214
	.zero	6
	.quad	.LC111
	.value	215
	.zero	6
	.quad	.LC112
	.value	216
	.zero	6
	.quad	.LC113
	.value	217
	.zero	6
	.quad	.LC114
	.value	218
	.zero	6
	.quad	.LC115
	.value	219
	.zero	6
	.quad	.LC116
	.value	220
	.zero	6
	.quad	.LC117
	.value	221
	.zero	6
	.quad	.LC118
	.value	222
	.zero	6
	.quad	.LC119
	.value	223
	.zero	6
	.quad	.LC120
	.value	224
	.zero	6
	.quad	.LC121
	.value	225
	.zero	6
	.quad	.LC122
	.value	226
	.zero	6
	.quad	.LC123
	.value	227
	.zero	6
	.quad	.LC124
	.value	228
	.zero	6
	.quad	.LC125
	.value	229
	.zero	6
	.quad	.LC126
	.value	230
	.zero	6
	.quad	.LC127
	.value	231
	.zero	6
	.quad	.LC128
	.value	232
	.zero	6
	.quad	.LC129
	.value	233
	.zero	6
	.quad	.LC130
	.value	234
	.zero	6
	.quad	.LC131
	.value	235
	.zero	6
	.quad	.LC132
	.value	236
	.zero	6
	.quad	.LC133
	.value	237
	.zero	6
	.quad	.LC134
	.value	238
	.zero	6
	.quad	.LC135
	.value	239
	.zero	6
	.quad	.LC136
	.value	240
	.zero	6
	.quad	.LC137
	.value	241
	.zero	6
	.quad	.LC138
	.value	242
	.zero	6
	.quad	.LC139
	.value	243
	.zero	6
	.quad	.LC140
	.value	244
	.zero	6
	.quad	.LC141
	.value	245
	.zero	6
	.quad	.LC142
	.value	246
	.zero	6
	.quad	.LC143
	.value	247
	.zero	6
	.quad	.LC144
	.value	248
	.zero	6
	.quad	.LC145
	.value	249
	.zero	6
	.quad	.LC146
	.value	250
	.zero	6
	.quad	.LC147
	.value	251
	.zero	6
	.quad	.LC148
	.value	252
	.zero	6
	.quad	.LC149
	.value	253
	.zero	6
	.quad	.LC150
	.value	254
	.zero	6
	.quad	.LC151
	.value	255
	.zero	6
	.quad	.LC152
	.value	256
	.zero	6
	.quad	.LC153
	.value	257
	.zero	6
	.quad	.LC154
	.value	258
	.zero	6
	.quad	.LC155
	.value	259
	.zero	6
	.quad	.LC156
	.value	260
	.zero	6
	.quad	.LC157
	.value	261
	.zero	6
	.quad	.LC158
	.value	262
	.zero	6
	.quad	.LC159
	.value	263
	.zero	6
	.quad	.LC12
	.value	264
	.zero	6
	.quad	.LC160
	.value	265
	.zero	6
	.quad	.LC161
	.value	266
	.zero	6
	.quad	.LC162
	.value	267
	.zero	6
	.quad	.LC163
	.value	268
	.zero	6
	.quad	.LC164
	.value	269
	.zero	6
	.quad	.LC165
	.value	270
	.zero	6
	.quad	.LC166
	.value	271
	.zero	6
	.quad	.LC167
	.value	272
	.zero	6
	.quad	.LC168
	.value	273
	.zero	6
	.quad	.LC169
	.value	274
	.zero	6
	.quad	.LC170
	.value	275
	.zero	6
	.quad	.LC171
	.value	276
	.zero	6
	.quad	.LC172
	.value	277
	.zero	6
	.quad	.LC173
	.value	278
	.zero	6
	.quad	.LC174
	.value	279
	.zero	6
	.quad	.LC175
	.value	280
	.zero	6
	.quad	.LC176
	.value	281
	.zero	6
	.quad	.LC177
	.value	282
	.zero	6
	.quad	.LC178
	.value	283
	.zero	6
	.quad	.LC179
	.value	284
	.zero	6
	.quad	.LC180
	.value	285
	.zero	6
	.quad	.LC181
	.value	286
	.zero	6
	.quad	.LC182
	.value	287
	.zero	6
	.quad	.LC183
	.value	288
	.zero	6
	.quad	.LC184
	.value	289
	.zero	6
	.quad	.LC185
	.value	290
	.zero	6
	.quad	.LC186
	.value	291
	.zero	6
	.quad	.LC187
	.value	292
	.zero	6
	.quad	.LC188
	.value	293
	.zero	6
	.quad	.LC189
	.value	294
	.zero	6
	.quad	.LC190
	.value	295
	.zero	6
	.quad	.LC191
	.value	296
	.zero	6
	.quad	.LC192
	.value	297
	.zero	6
	.quad	.LC193
	.value	298
	.zero	6
	.quad	.LC194
	.value	299
	.zero	6
	.quad	.LC195
	.value	300
	.zero	6
	.quad	.LC196
	.value	301
	.zero	6
	.quad	.LC197
	.value	302
	.zero	6
	.quad	.LC198
	.value	303
	.zero	6
	.quad	.LC199
	.value	304
	.zero	6
	.quad	.LC200
	.value	305
	.zero	6
	.quad	.LC201
	.value	306
	.zero	6
	.quad	.LC202
	.value	307
	.zero	6
	.quad	.LC14
	.value	308
	.zero	6
	.quad	.LC203
	.value	309
	.zero	6
	.quad	.LC204
	.value	310
	.zero	6
	.quad	.LC205
	.value	311
	.zero	6
	.quad	.LC206
	.value	312
	.zero	6
	.quad	.LC207
	.value	313
	.zero	6
	.quad	.LC208
	.value	314
	.zero	6
	.quad	.LC209
	.value	315
	.zero	6
	.quad	.LC210
	.value	316
	.zero	6
	.quad	.LC211
	.value	317
	.zero	6
	.quad	.LC212
	.value	318
	.zero	6
	.quad	.LC213
	.value	319
	.zero	6
	.quad	.LC214
	.value	320
	.zero	6
	.quad	.LC215
	.value	321
	.zero	6
	.quad	.LC216
	.value	322
	.zero	6
	.quad	.LC217
	.value	323
	.zero	6
	.quad	.LC218
	.value	324
	.zero	6
	.quad	.LC219
	.value	325
	.zero	6
	.quad	.LC220
	.value	326
	.zero	6
	.quad	.LC221
	.value	327
	.zero	6
	.quad	.LC222
	.value	328
	.zero	6
	.quad	.LC223
	.value	329
	.zero	6
	.quad	.LC224
	.value	330
	.zero	6
	.quad	.LC225
	.value	331
	.zero	6
	.quad	.LC226
	.value	332
	.zero	6
	.quad	.LC227
	.value	333
	.zero	6
	.quad	.LC228
	.value	334
	.zero	6
	.quad	.LC229
	.value	335
	.zero	6
	.quad	.LC230
	.value	336
	.zero	6
	.quad	.LC231
	.value	337
	.zero	6
	.quad	.LC232
	.value	338
	.zero	6
	.quad	.LC233
	.value	339
	.zero	6
	.quad	.LC234
	.value	340
	.zero	6
	.quad	.LC235
	.value	341
	.zero	6
	.quad	.LC236
	.value	342
	.zero	6
	.quad	.LC237
	.value	343
	.zero	6
	.quad	.LC238
	.value	344
	.zero	6
	.quad	.LC239
	.value	345
	.zero	6
	.quad	.LC240
	.value	346
	.zero	6
	.quad	.LC241
	.value	347
	.zero	6
	.quad	.LC242
	.value	348
	.zero	6
	.quad	.LC243
	.value	349
	.zero	6
	.quad	.LC244
	.value	350
	.zero	6
	.quad	.LC245
	.value	351
	.zero	6
	.quad	.LC246
	.value	352
	.zero	6
	.quad	.LC247
	.value	353
	.zero	6
	.quad	.LC248
	.value	354
	.zero	6
	.quad	.LC249
	.value	355
	.zero	6
	.quad	.LC250
	.value	356
	.zero	6
	.quad	.LC251
	.value	357
	.zero	6
	.quad	.LC252
	.value	358
	.zero	6
	.quad	.LC253
	.value	359
	.zero	6
	.quad	.LC254
	.value	360
	.zero	6
	.quad	.LC255
	.value	361
	.zero	6
	.quad	.LC256
	.value	362
	.zero	6
	.quad	.LC10
	.value	363
	.zero	6
	.quad	.LC257
	.value	364
	.zero	6
	.quad	.LC258
	.value	365
	.zero	6
	.quad	.LC259
	.value	366
	.zero	6
	.quad	.LC260
	.value	367
	.zero	6
	.quad	.LC261
	.value	368
	.zero	6
	.quad	.LC9
	.value	369
	.zero	6
	.quad	.LC262
	.value	370
	.zero	6
	.quad	.LC263
	.value	371
	.zero	6
	.quad	.LC264
	.value	372
	.zero	6
	.quad	.LC265
	.value	373
	.zero	6
	.quad	.LC266
	.value	374
	.zero	6
	.quad	.LC267
	.value	375
	.zero	6
	.quad	.LC268
	.value	376
	.zero	6
	.quad	.LC269
	.value	377
	.zero	6
	.quad	.LC270
	.value	378
	.zero	6
	.quad	.LC271
	.value	379
	.zero	6
	.quad	.LC272
	.value	380
	.zero	6
	.quad	.LC273
	.value	381
	.zero	6
	.quad	.LC274
	.value	382
	.zero	6
	.quad	.LC275
	.value	383
	.zero	6
	.quad	.LC276
	.value	384
	.zero	6
	.quad	.LC277
	.value	385
	.zero	6
	.quad	.LC278
	.value	386
	.zero	6
	.quad	.LC279
	.value	387
	.zero	6
	.quad	.LC280
	.value	388
	.zero	6
	.quad	.LC281
	.value	389
	.zero	6
	.quad	.LC282
	.value	390
	.zero	6
	.quad	.LC283
	.value	391
	.zero	6
	.quad	.LC284
	.value	392
	.zero	6
	.quad	.LC285
	.value	393
	.zero	6
	.quad	.LC286
	.value	394
	.zero	6
	.quad	.LC287
	.value	395
	.zero	6
	.quad	.LC288
	.value	396
	.zero	6
	.quad	.LC289
	.value	397
	.zero	6
	.quad	.LC290
	.value	398
	.zero	6
	.quad	.LC291
	.value	399
	.zero	6
	.quad	.LC292
	.value	400
	.zero	6
	.quad	.LC293
	.value	401
	.zero	6
	.quad	.LC294
	.value	402
	.zero	6
	.quad	.LC295
	.value	403
	.zero	6
	.quad	.LC296
	.value	404
	.zero	6
	.quad	.LC297
	.value	405
	.zero	6
	.quad	.LC298
	.value	406
	.zero	6
	.quad	.LC299
	.value	407
	.zero	6
	.quad	.LC300
	.value	408
	.zero	6
	.quad	.LC301
	.value	409
	.zero	6
	.quad	.LC302
	.value	410
	.zero	6
	.quad	.LC303
	.value	411
	.zero	6
	.quad	.LC304
	.value	412
	.zero	6
	.quad	.LC305
	.value	413
	.zero	6
	.quad	.LC306
	.value	414
	.zero	6
	.quad	.LC307
	.value	415
	.zero	6
	.quad	.LC308
	.value	416
	.zero	6
	.quad	.LC309
	.value	417
	.zero	6
	.quad	.LC310
	.value	418
	.zero	6
	.quad	.LC311
	.value	419
	.zero	6
	.quad	.LC312
	.value	420
	.zero	6
	.quad	.LC313
	.value	421
	.zero	6
	.quad	.LC314
	.value	422
	.zero	6
	.quad	.LC315
	.value	423
	.zero	6
	.quad	.LC316
	.value	424
	.zero	6
	.quad	.LC317
	.value	425
	.zero	6
	.quad	.LC13
	.value	426
	.zero	6
	.quad	.LC318
	.value	427
	.zero	6
	.quad	.LC319
	.value	428
	.zero	6
	.quad	.LC320
	.value	429
	.zero	6
	.quad	.LC321
	.value	430
	.zero	6
	.quad	.LC322
	.value	431
	.zero	6
	.quad	.LC8
	.value	432
	.zero	6
	.quad	.LC323
	.value	433
	.zero	6
	.quad	.LC324
	.value	434
	.zero	6
	.quad	.LC325
	.value	435
	.zero	6
	.quad	.LC326
	.value	436
	.zero	6
	.quad	.LC327
	.value	437
	.zero	6
	.quad	.LC328
	.value	438
	.zero	6
	.quad	.LC329
	.value	439
	.zero	6
	.quad	.LC330
	.value	440
	.zero	6
	.quad	.LC331
	.value	441
	.zero	6
	.quad	.LC332
	.value	442
	.zero	6
	.quad	.LC333
	.value	443
	.zero	6
	.quad	.LC334
	.value	444
	.zero	6
	.globl	_ZN2v88internal4Heap17string_type_tableE
	.section	.rodata._ZN2v88internal4Heap17string_type_tableE,"a"
	.align 32
	.type	_ZN2v88internal4Heap17string_type_tableE, @object
	.size	_ZN2v88internal4Heap17string_type_tableE, 216
_ZN2v88internal4Heap17string_type_tableE:
	.value	32
	.zero	2
	.long	0
	.value	86
	.zero	2
	.value	40
	.zero	2
	.long	0
	.value	16
	.zero	2
	.value	33
	.zero	2
	.long	32
	.value	88
	.zero	2
	.value	41
	.zero	2
	.long	32
	.value	87
	.zero	2
	.value	35
	.zero	2
	.long	32
	.value	91
	.zero	2
	.value	43
	.zero	2
	.long	32
	.value	92
	.zero	2
	.value	34
	.zero	2
	.long	32
	.value	93
	.zero	2
	.value	42
	.zero	2
	.long	32
	.value	94
	.zero	2
	.value	50
	.zero	2
	.long	24
	.value	95
	.zero	2
	.value	58
	.zero	2
	.long	24
	.value	101
	.zero	2
	.value	0
	.zero	2
	.long	0
	.value	96
	.zero	2
	.value	8
	.zero	2
	.long	0
	.value	17
	.zero	2
	.value	2
	.zero	2
	.long	32
	.value	97
	.zero	2
	.value	10
	.zero	2
	.long	32
	.value	98
	.zero	2
	.value	18
	.zero	2
	.long	24
	.value	99
	.zero	2
	.value	26
	.zero	2
	.long	24
	.value	100
	.zero	2
	.value	37
	.zero	2
	.long	24
	.value	90
	.zero	2
	.value	45
	.zero	2
	.long	24
	.value	89
	.zero	2
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
