	.file	"builtins-generator-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc
	.type	_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc, @function
_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc:
.LFB21880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm2
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 3, -56
	movl	%r9d, -712(%rbp)
	movq	16(%rbp), %r8
	movq	%rsi, -664(%rbp)
	movq	%r14, %rsi
	movq	%rcx, -768(%rbp)
	movl	$1068, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -704(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ThrowIfNotInstanceTypeEPNS0_8compiler4NodeES4_NS0_12InstanceTypeEPKc@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rbx
	leaq	-592(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-464(%rbp), %rcx
	movq	%rcx, %r15
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r15, -680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-688(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-208(%rbp), %rbx
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-688(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-680(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	-712(%rbp), %r10d
	movq	%r12, %rdi
	movl	%r10d, %esi
	movl	%r10d, -756(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$56, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$6, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	leaq	-656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-336(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-624(%rbp), %r11
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -752(%rbp)
	call	_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r14, %r9
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%rax, -640(%rbp)
	movq	-608(%rbp), %rax
	leaq	-640(%rbp), %r11
	movq	%r12, %rdi
	movdqa	-704(%rbp), %xmm0
	movq	%r11, %rdx
	movq	%r11, -720(%rbp)
	movq	%rax, -632(%rbp)
	leaq	-80(%rbp), %rax
	pushq	%rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-712(%rbp), %rcx
	movq	-688(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$-2, %esi
	movq	%r12, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-736(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -728(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-728(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-744(%rbp), %r9
	movq	-664(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rcx
	movl	$64, %edx
	movq	%r13, %rsi
	movl	$6, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -728(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-752(%rbp), %r11
	movl	$350, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	movq	%r11, -736(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-704(%rbp), %rsi
	movq	-744(%rbp), %r9
	movl	$2, %edi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-720(%rbp), %rdx
	pushq	%rsi
	movq	-608(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	%r9, %xmm0
	movq	%rcx, -640(%rbp)
	movq	%r14, %r9
	movl	$1, %ecx
	movhps	-728(%rbp), %xmm0
	movq	%rax, -632(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-664(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	-756(%rbp), %r10d
	movq	-736(%rbp), %r11
	cmpl	$1, %r10d
	je	.L8
	cmpl	$2, %r10d
	je	.L9
	xorl	%esi, %esi
	testl	%r10d, %r10d
	je	.L15
.L10:
	movq	-664(%rbp), %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$55, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rcx
	movl	$64, %edx
	movq	%r13, %rsi
	movl	$6, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-712(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdx
	movl	$156, %esi
	movq	%r12, %rdi
	movq	-704(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r11, -736(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -728(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-736(%rbp), %r11
	movl	$350, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-768(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -640(%rbp)
	movq	-608(%rbp), %rax
	movhps	-728(%rbp), %xmm0
	movq	%rax, -632(%rbp)
.L14:
	movq	-704(%rbp), %rsi
	movl	$2, %edi
	movq	%r14, %r9
	movl	$1, %ecx
	pushq	%rdi
	movq	-720(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r12, %rdi
	movq	%r11, -744(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -728(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-744(%rbp), %r11
	movl	$350, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-728(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -640(%rbp)
	movq	-608(%rbp), %rax
	movhps	-736(%rbp), %xmm0
	movq	%rax, -632(%rbp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-768(%rbp), %rax
	movq	-704(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$160, %esi
	movl	$1, %r8d
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%rax, %rsi
	jmp	.L10
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21880:
	.size	_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc, .-_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc
	.section	.rodata._ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"[Generator].prototype.next"
	.section	.text._ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv
	.type	_ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv, @function
_ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv:
.LFB21895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%rax, %r8
	leaq	.LC1(%rip), %rax
	movq	%r15, %rcx
	movq	%r13, %rsi
	pushq	%rax
	movq	%r12, %rdi
	call	_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21895:
	.size	_ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv, .-_ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/v8/src/builtins/builtins-generator-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"GeneratorPrototypeNext"
	.section	.text._ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE:
.LFB21891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$115, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$353, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L25
.L22:
	movq	%r13, %rdi
	call	_ZN2v88internal31GeneratorPrototypeNextAssembler34GenerateGeneratorPrototypeNextImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L22
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21891:
	.size	_ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_GeneratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"[Generator].prototype.return"
	.section	.text._ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv
	.type	_ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv, @function
_ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv:
.LFB21904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%rax, %r8
	leaq	.LC4(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rax
	movl	$1, %r9d
	call	_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21904:
	.size	_ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv, .-_ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_GeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"GeneratorPrototypeReturn"
	.section	.text._ZN2v88internal8Builtins33Generate_GeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_GeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_GeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_GeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE:
.LFB21900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$132, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$354, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L35
.L32:
	movq	%r13, %rdi
	call	_ZN2v88internal33GeneratorPrototypeReturnAssembler36GenerateGeneratorPrototypeReturnImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L32
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21900:
	.size	_ZN2v88internal8Builtins33Generate_GeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_GeneratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"[Generator].prototype.throw"
	.section	.text._ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv
	.type	_ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv, @function
_ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv:
.LFB21913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%rax, %r8
	leaq	.LC6(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%rax
	movl	$2, %r9d
	call	_ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21913:
	.size	_ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv, .-_ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_GeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"GeneratorPrototypeThrow"
	.section	.text._ZN2v88internal8Builtins32Generate_GeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_GeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_GeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_GeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE:
.LFB21909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$149, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$355, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L45
.L42:
	movq	%r13, %rdi
	call	_ZN2v88internal32GeneratorPrototypeThrowAssembler35GenerateGeneratorPrototypeThrowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L42
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21909:
	.size	_ZN2v88internal8Builtins32Generate_GeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_GeneratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc, @function
_GLOBAL__sub_I__ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc:
.LFB27987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27987:
	.size	_GLOBAL__sub_I__ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc, .-_GLOBAL__sub_I__ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26GeneratorBuiltinsAssembler24GeneratorPrototypeResumeEPNS0_17CodeStubArgumentsEPNS0_8compiler4NodeES6_S6_NS0_17JSGeneratorObject10ResumeModeEPKc
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
