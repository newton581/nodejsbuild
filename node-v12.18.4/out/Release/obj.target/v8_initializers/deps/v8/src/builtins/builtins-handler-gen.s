	.file	"builtins-handler-gen.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal12ElementsKindES2_EZNS1_24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS1_20KeyedAccessStoreModeEEUlS2_S2_E_E9_M_invokeERKSt9_Any_dataOS2_SB_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindES2_EZNS1_24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS1_20KeyedAccessStoreModeEEUlS2_S2_E_E9_M_invokeERKSt9_Any_dataOS2_SB_, @function
_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindES2_EZNS1_24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS1_20KeyedAccessStoreModeEEUlS2_S2_E_E9_M_invokeERKSt9_Any_dataOS2_SB_:
.LFB26196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movzbl	(%rdx), %r12d
	movzbl	(%rsi), %ecx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	24(%rbx), %rdi
	movq	(%rbx), %r9
	movl	%r12d, %r8d
	call	_ZN2v88internal17CodeStubAssembler22TransitionElementsKindEPNS0_8compiler4NodeES4_NS0_12ElementsKindES5_PNS2_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movl	48(%rbx), %r9d
	movl	%r12d, %r8d
	movq	40(%rbx), %rcx
	movq	32(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	24(%rbx), %rdi
	pushq	$0
	pushq	56(%rbx)
	pushq	(%rbx)
	call	_ZN2v88internal17CodeStubAssembler16EmitElementStoreEPNS0_8compiler4NodeES4_S4_NS0_12ElementsKindENS0_20KeyedAccessStoreModeEPNS2_18CodeAssemblerLabelES4_PNS2_21CodeAssemblerVariableE@PLT
	addq	$32, %rsp
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26196:
	.size	_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindES2_EZNS1_24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS1_20KeyedAccessStoreModeEEUlS2_S2_E_E9_M_invokeERKSt9_Any_dataOS2_SB_, .-_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindES2_EZNS1_24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS1_20KeyedAccessStoreModeEEUlS2_S2_E_E9_M_invokeERKSt9_Any_dataOS2_SB_
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal12ElementsKindEEZNS1_24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS1_20KeyedAccessStoreModeEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindEEZNS1_24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS1_20KeyedAccessStoreModeEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindEEZNS1_24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS1_20KeyedAccessStoreModeEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB26200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movzbl	(%rsi), %r8d
	movl	40(%rax), %r9d
	movq	32(%rax), %rcx
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	movq	56(%rax), %rdi
	pushq	8(%rax)
	pushq	48(%rax)
	pushq	(%rax)
	call	_ZN2v88internal17CodeStubAssembler16EmitElementStoreEPNS0_8compiler4NodeES4_S4_NS0_12ElementsKindENS0_20KeyedAccessStoreModeEPNS2_18CodeAssemblerLabelES4_PNS2_21CodeAssemblerVariableE@PLT
	addq	$32, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26200:
	.size	_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindEEZNS1_24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS1_20KeyedAccessStoreModeEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindEEZNS1_24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS1_20KeyedAccessStoreModeEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindES5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindES5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindES5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB26197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L7
	cmpl	$3, %edx
	je	.L8
	cmpl	$1, %edx
	je	.L14
.L9:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$64, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%r12), %xmm3
	movq	%rax, (%rbx)
	movups	%xmm3, 48(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L9
	movl	$64, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26197:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindES5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindES5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindEE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindEE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindEE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB26201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L16
	cmpl	$3, %edx
	je	.L17
	cmpl	$1, %edx
	je	.L23
.L18:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$64, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%r12), %xmm3
	movq	%rax, (%rbx)
	movups	%xmm3, 48(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L18
	movl	$64, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26201:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindEE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindEE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.rodata._ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/builtins/builtins-handler-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"LoadIC_StringLength"
	.section	.text._ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE:
.LFB21966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$45, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$123, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L28
.L25:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L25
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21966:
	.size	_ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28LoadIC_StringLengthAssembler31GenerateLoadIC_StringLengthImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28LoadIC_StringLengthAssembler31GenerateLoadIC_StringLengthImplEv
	.type	_ZN2v88internal28LoadIC_StringLengthAssembler31GenerateLoadIC_StringLengthImplEv, @function
_ZN2v88internal28LoadIC_StringLengthAssembler31GenerateLoadIC_StringLengthImplEv:
.LFB21970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21970:
	.size	_ZN2v88internal28LoadIC_StringLengthAssembler31GenerateLoadIC_StringLengthImplEv, .-_ZN2v88internal28LoadIC_StringLengthAssembler31GenerateLoadIC_StringLengthImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_LoadIC_StringWrapperLengthEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"LoadIC_StringWrapperLength"
	.section	.text._ZN2v88internal8Builtins35Generate_LoadIC_StringWrapperLengthEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_LoadIC_StringWrapperLengthEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_LoadIC_StringWrapperLengthEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_LoadIC_StringWrapperLengthEPNS0_8compiler18CodeAssemblerStateE:
.LFB21975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$50, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$124, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L36
.L33:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27LoadJSPrimitiveWrapperValueEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L33
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21975:
	.size	_ZN2v88internal8Builtins35Generate_LoadIC_StringWrapperLengthEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_LoadIC_StringWrapperLengthEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35LoadIC_StringWrapperLengthAssembler38GenerateLoadIC_StringWrapperLengthImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35LoadIC_StringWrapperLengthAssembler38GenerateLoadIC_StringWrapperLengthImplEv
	.type	_ZN2v88internal35LoadIC_StringWrapperLengthAssembler38GenerateLoadIC_StringWrapperLengthImplEv, @function
_ZN2v88internal35LoadIC_StringWrapperLengthAssembler38GenerateLoadIC_StringWrapperLengthImplEv:
.LFB21979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27LoadJSPrimitiveWrapperValueEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21979:
	.size	_ZN2v88internal35LoadIC_StringWrapperLengthAssembler38GenerateLoadIC_StringWrapperLengthImplEv, .-_ZN2v88internal35LoadIC_StringWrapperLengthAssembler38GenerateLoadIC_StringWrapperLengthImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_KeyedLoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"KeyedLoadIC_Slow"
	.section	.text._ZN2v88internal8Builtins25Generate_KeyedLoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_KeyedLoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_KeyedLoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_KeyedLoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB21984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$56, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$117, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L44
.L41:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-72(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$224, %esi
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L41
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21984:
	.size	_ZN2v88internal8Builtins25Generate_KeyedLoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_KeyedLoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal25KeyedLoadIC_SlowAssembler28GenerateKeyedLoadIC_SlowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25KeyedLoadIC_SlowAssembler28GenerateKeyedLoadIC_SlowImplEv
	.type	_ZN2v88internal25KeyedLoadIC_SlowAssembler28GenerateKeyedLoadIC_SlowImplEv, @function
_ZN2v88internal25KeyedLoadIC_SlowAssembler28GenerateKeyedLoadIC_SlowImplEv:
.LFB21988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-64(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$224, %esi
	movhps	-56(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L49:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21988:
	.size	_ZN2v88internal25KeyedLoadIC_SlowAssembler28GenerateKeyedLoadIC_SlowImplEv, .-_ZN2v88internal25KeyedLoadIC_SlowAssembler28GenerateKeyedLoadIC_SlowImplEv
	.section	.text._ZN2v88internal8Builtins33Generate_KeyedStoreIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_KeyedStoreIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_KeyedStoreIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_KeyedStoreIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE:
.LFB21989:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal26KeyedStoreGenericGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE@PLT
	.cfi_endproc
.LFE21989:
	.size	_ZN2v88internal8Builtins33Generate_KeyedStoreIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_KeyedStoreIC_MegamorphicEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8Builtins27Generate_StoreIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_StoreIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_StoreIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_StoreIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE:
.LFB21990:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal26StoreICNoFeedbackGenerator8GenerateEPNS0_8compiler18CodeAssemblerStateE@PLT
	.cfi_endproc
.LFE21990:
	.size	_ZN2v88internal8Builtins27Generate_StoreIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_StoreIC_NoFeedbackEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	.type	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv, @function
_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv:
.LFB21991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-64(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-80(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$3, %r9d
	movl	$119, %esi
	movq	%rbx, -48(%rbp)
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21991:
	.size	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv, .-_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_KeyedStoreIC_SlowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"KeyedStoreIC_Slow"
	.section	.text._ZN2v88internal8Builtins26Generate_KeyedStoreIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_KeyedStoreIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_KeyedStoreIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_KeyedStoreIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB21999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$88, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$119, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L60
.L57:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L57
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21999:
	.size	_ZN2v88internal8Builtins26Generate_KeyedStoreIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_KeyedStoreIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26KeyedStoreIC_SlowAssembler29GenerateKeyedStoreIC_SlowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26KeyedStoreIC_SlowAssembler29GenerateKeyedStoreIC_SlowImplEv
	.type	_ZN2v88internal26KeyedStoreIC_SlowAssembler29GenerateKeyedStoreIC_SlowImplEv, @function
_ZN2v88internal26KeyedStoreIC_SlowAssembler29GenerateKeyedStoreIC_SlowImplEv:
.LFB22003:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	.cfi_endproc
.LFE22003:
	.size	_ZN2v88internal26KeyedStoreIC_SlowAssembler29GenerateKeyedStoreIC_SlowImplEv, .-_ZN2v88internal26KeyedStoreIC_SlowAssembler29GenerateKeyedStoreIC_SlowImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_KeyedStoreIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"KeyedStoreIC_Slow_Standard"
	.section	.text._ZN2v88internal8Builtins35Generate_KeyedStoreIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_KeyedStoreIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_KeyedStoreIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_KeyedStoreIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE:
.LFB22008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$92, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$144, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L67
.L64:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L64
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22008:
	.size	_ZN2v88internal8Builtins35Generate_KeyedStoreIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_KeyedStoreIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35KeyedStoreIC_Slow_StandardAssembler38GenerateKeyedStoreIC_Slow_StandardImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35KeyedStoreIC_Slow_StandardAssembler38GenerateKeyedStoreIC_Slow_StandardImplEv
	.type	_ZN2v88internal35KeyedStoreIC_Slow_StandardAssembler38GenerateKeyedStoreIC_Slow_StandardImplEv, @function
_ZN2v88internal35KeyedStoreIC_Slow_StandardAssembler38GenerateKeyedStoreIC_Slow_StandardImplEv:
.LFB22012:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	.cfi_endproc
.LFE22012:
	.size	_ZN2v88internal35KeyedStoreIC_Slow_StandardAssembler38GenerateKeyedStoreIC_Slow_StandardImplEv, .-_ZN2v88internal35KeyedStoreIC_Slow_StandardAssembler38GenerateKeyedStoreIC_Slow_StandardImplEv
	.section	.rodata._ZN2v88internal8Builtins52Generate_KeyedStoreIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"KeyedStoreIC_Slow_GrowNoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins52Generate_KeyedStoreIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins52Generate_KeyedStoreIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins52Generate_KeyedStoreIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins52Generate_KeyedStoreIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$96, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$145, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L74
.L71:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L71
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22017:
	.size	_ZN2v88internal8Builtins52Generate_KeyedStoreIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins52Generate_KeyedStoreIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal52KeyedStoreIC_Slow_GrowNoTransitionHandleCOWAssembler55GenerateKeyedStoreIC_Slow_GrowNoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal52KeyedStoreIC_Slow_GrowNoTransitionHandleCOWAssembler55GenerateKeyedStoreIC_Slow_GrowNoTransitionHandleCOWImplEv
	.type	_ZN2v88internal52KeyedStoreIC_Slow_GrowNoTransitionHandleCOWAssembler55GenerateKeyedStoreIC_Slow_GrowNoTransitionHandleCOWImplEv, @function
_ZN2v88internal52KeyedStoreIC_Slow_GrowNoTransitionHandleCOWAssembler55GenerateKeyedStoreIC_Slow_GrowNoTransitionHandleCOWImplEv:
.LFB22021:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	.cfi_endproc
.LFE22021:
	.size	_ZN2v88internal52KeyedStoreIC_Slow_GrowNoTransitionHandleCOWAssembler55GenerateKeyedStoreIC_Slow_GrowNoTransitionHandleCOWImplEv, .-_ZN2v88internal52KeyedStoreIC_Slow_GrowNoTransitionHandleCOWAssembler55GenerateKeyedStoreIC_Slow_GrowNoTransitionHandleCOWImplEv
	.section	.rodata._ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"KeyedStoreIC_Slow_NoTransitionIgnoreOOB"
	.section	.text._ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE:
.LFB22026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$101, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$146, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L81
.L78:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L78
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22026:
	.size	_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal48KeyedStoreIC_Slow_NoTransitionIgnoreOOBAssembler51GenerateKeyedStoreIC_Slow_NoTransitionIgnoreOOBImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionIgnoreOOBAssembler51GenerateKeyedStoreIC_Slow_NoTransitionIgnoreOOBImplEv
	.type	_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionIgnoreOOBAssembler51GenerateKeyedStoreIC_Slow_NoTransitionIgnoreOOBImplEv, @function
_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionIgnoreOOBAssembler51GenerateKeyedStoreIC_Slow_NoTransitionIgnoreOOBImplEv:
.LFB22030:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	.cfi_endproc
.LFE22030:
	.size	_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionIgnoreOOBAssembler51GenerateKeyedStoreIC_Slow_NoTransitionIgnoreOOBImplEv, .-_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionIgnoreOOBAssembler51GenerateKeyedStoreIC_Slow_NoTransitionIgnoreOOBImplEv
	.section	.rodata._ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"KeyedStoreIC_Slow_NoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$105, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$147, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L88
.L85:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L85
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22035:
	.size	_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins48Generate_KeyedStoreIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal48KeyedStoreIC_Slow_NoTransitionHandleCOWAssembler51GenerateKeyedStoreIC_Slow_NoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionHandleCOWAssembler51GenerateKeyedStoreIC_Slow_NoTransitionHandleCOWImplEv
	.type	_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionHandleCOWAssembler51GenerateKeyedStoreIC_Slow_NoTransitionHandleCOWImplEv, @function
_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionHandleCOWAssembler51GenerateKeyedStoreIC_Slow_NoTransitionHandleCOWImplEv:
.LFB22039:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler26Generate_KeyedStoreIC_SlowEv
	.cfi_endproc
.LFE22039:
	.size	_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionHandleCOWAssembler51GenerateKeyedStoreIC_Slow_NoTransitionHandleCOWImplEv, .-_ZN2v88internal48KeyedStoreIC_Slow_NoTransitionHandleCOWAssembler51GenerateKeyedStoreIC_Slow_NoTransitionHandleCOWImplEv
	.section	.text._ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	.type	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv, @function
_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv:
.LFB22040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-64(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-80(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$3, %r9d
	movl	$130, %esi
	movq	%rbx, -48(%rbp)
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22040:
	.size	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv, .-_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_StoreInArrayLiteralIC_SlowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"StoreInArrayLiteralIC_Slow"
	.section	.text._ZN2v88internal8Builtins35Generate_StoreInArrayLiteralIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_StoreInArrayLiteralIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_StoreInArrayLiteralIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_StoreInArrayLiteralIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$119, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$128, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L99
.L96:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L96
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22045:
	.size	_ZN2v88internal8Builtins35Generate_StoreInArrayLiteralIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_StoreInArrayLiteralIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35StoreInArrayLiteralIC_SlowAssembler38GenerateStoreInArrayLiteralIC_SlowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35StoreInArrayLiteralIC_SlowAssembler38GenerateStoreInArrayLiteralIC_SlowImplEv
	.type	_ZN2v88internal35StoreInArrayLiteralIC_SlowAssembler38GenerateStoreInArrayLiteralIC_SlowImplEv, @function
_ZN2v88internal35StoreInArrayLiteralIC_SlowAssembler38GenerateStoreInArrayLiteralIC_SlowImplEv:
.LFB22049:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	.cfi_endproc
.LFE22049:
	.size	_ZN2v88internal35StoreInArrayLiteralIC_SlowAssembler38GenerateStoreInArrayLiteralIC_SlowImplEv, .-_ZN2v88internal35StoreInArrayLiteralIC_SlowAssembler38GenerateStoreInArrayLiteralIC_SlowImplEv
	.section	.rodata._ZN2v88internal8Builtins44Generate_StoreInArrayLiteralIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"StoreInArrayLiteralIC_Slow_Standard"
	.section	.text._ZN2v88internal8Builtins44Generate_StoreInArrayLiteralIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins44Generate_StoreInArrayLiteralIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins44Generate_StoreInArrayLiteralIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins44Generate_StoreInArrayLiteralIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE:
.LFB22054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$123, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$136, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L106
.L103:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L103
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22054:
	.size	_ZN2v88internal8Builtins44Generate_StoreInArrayLiteralIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins44Generate_StoreInArrayLiteralIC_Slow_StandardEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal44StoreInArrayLiteralIC_Slow_StandardAssembler47GenerateStoreInArrayLiteralIC_Slow_StandardImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal44StoreInArrayLiteralIC_Slow_StandardAssembler47GenerateStoreInArrayLiteralIC_Slow_StandardImplEv
	.type	_ZN2v88internal44StoreInArrayLiteralIC_Slow_StandardAssembler47GenerateStoreInArrayLiteralIC_Slow_StandardImplEv, @function
_ZN2v88internal44StoreInArrayLiteralIC_Slow_StandardAssembler47GenerateStoreInArrayLiteralIC_Slow_StandardImplEv:
.LFB22058:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	.cfi_endproc
.LFE22058:
	.size	_ZN2v88internal44StoreInArrayLiteralIC_Slow_StandardAssembler47GenerateStoreInArrayLiteralIC_Slow_StandardImplEv, .-_ZN2v88internal44StoreInArrayLiteralIC_Slow_StandardAssembler47GenerateStoreInArrayLiteralIC_Slow_StandardImplEv
	.section	.rodata._ZN2v88internal8Builtins61Generate_StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins61Generate_StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins61Generate_StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins61Generate_StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins61Generate_StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$127, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$141, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L113
.L110:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L110
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22063:
	.size	_ZN2v88internal8Builtins61Generate_StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins61Generate_StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal61StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWAssembler64GenerateStoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal61StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWAssembler64GenerateStoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWImplEv
	.type	_ZN2v88internal61StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWAssembler64GenerateStoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWImplEv, @function
_ZN2v88internal61StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWAssembler64GenerateStoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWImplEv:
.LFB22067:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	.cfi_endproc
.LFE22067:
	.size	_ZN2v88internal61StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWAssembler64GenerateStoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWImplEv, .-_ZN2v88internal61StoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWAssembler64GenerateStoreInArrayLiteralIC_Slow_GrowNoTransitionHandleCOWImplEv
	.section	.rodata._ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOB"
	.section	.text._ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE:
.LFB22072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$132, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$142, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L120
.L117:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L117
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22072:
	.size	_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBImplEv
	.type	_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBImplEv, @function
_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBImplEv:
.LFB22076:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	.cfi_endproc
.LFE22076:
	.size	_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBImplEv, .-_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionIgnoreOOBImplEv
	.section	.rodata._ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"StoreInArrayLiteralIC_Slow_NoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$137, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$143, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L127
.L124:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L124
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22081:
	.size	_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins57Generate_StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionHandleCOWImplEv
	.type	_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionHandleCOWImplEv, @function
_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionHandleCOWImplEv:
.LFB22085:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_StoreInArrayLiteralIC_SlowEv
	.cfi_endproc
.LFE22085:
	.size	_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionHandleCOWImplEv, .-_ZN2v88internal57StoreInArrayLiteralIC_Slow_NoTransitionHandleCOWAssembler60GenerateStoreInArrayLiteralIC_Slow_NoTransitionHandleCOWImplEv
	.section	.text._ZN2v88internal24HandlerBuiltinsAssembler33DispatchForElementsKindTransitionENS0_8compiler5TNodeINS0_6Int32TEEES5_RKSt8functionIFvNS0_12ElementsKindES7_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24HandlerBuiltinsAssembler33DispatchForElementsKindTransitionENS0_8compiler5TNodeINS0_6Int32TEEES5_RKSt8functionIFvNS0_12ElementsKindES7_EE
	.type	_ZN2v88internal24HandlerBuiltinsAssembler33DispatchForElementsKindTransitionENS0_8compiler5TNodeINS0_6Int32TEEES5_RKSt8functionIFvNS0_12ElementsKindES7_EE, @function
_ZN2v88internal24HandlerBuiltinsAssembler33DispatchForElementsKindTransitionENS0_8compiler5TNodeINS0_6Int32TEEES5_RKSt8functionIFvNS0_12ElementsKindES7_EE:
.LFB22086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	leaq	-848(%rbp), %r11
	leaq	-1360(%rbp), %rcx
	pushq	%r13
	leaq	-592(%rbp), %r8
	leaq	-976(%rbp), %r10
	pushq	%r12
	leaq	-720(%rbp), %r9
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-1744(%rbp), %r14
	pushq	%rbx
	movq	%r10, %xmm2
	movq	%r9, %xmm1
	movq	%r14, %xmm5
	.cfi_offset 3, -56
	leaq	-1488(%rbp), %rbx
	leaq	-464(%rbp), %r13
	movq	%rbx, %xmm4
	movq	%r13, %xmm0
	subq	$2200, %rsp
	movq	%rsi, -2232(%rbp)
	leaq	-1104(%rbp), %rsi
	movq	%rdx, -2128(%rbp)
	leaq	-1232(%rbp), %rdx
	movq	%rdx, %xmm3
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1616(%rbp), %rax
	movq	%r11, -2064(%rbp)
	movq	%rax, -2040(%rbp)
	movq	%rcx, -2048(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -2056(%rbp)
	movhps	-2064(%rbp), %xmm2
	movq	%rdi, %rsi
	movq	%r8, -2072(%rbp)
	movhps	-2040(%rbp), %xmm5
	movl	$1, %r8d
	movq	%rbx, -2088(%rbp)
	leaq	-336(%rbp), %rbx
	movhps	-2048(%rbp), %xmm4
	movq	%rbx, -2080(%rbp)
	movhps	-2056(%rbp), %xmm3
	leaq	-1872(%rbp), %rbx
	movhps	-2072(%rbp), %xmm1
	movq	%r13, -2120(%rbp)
	leaq	-2000(%rbp), %r13
	movq	%r10, -2104(%rbp)
	movq	%r13, %rdi
	movq	%r9, -2112(%rbp)
	movaps	%xmm5, -2224(%rbp)
	movaps	%xmm4, -2208(%rbp)
	movaps	%xmm3, -2192(%rbp)
	movaps	%xmm2, -2176(%rbp)
	movaps	%xmm1, -2160(%rbp)
	movq	%rdx, -2096(%rbp)
	movhps	-2080(%rbp), %xmm0
	xorl	%edx, %edx
	movaps	%xmm0, -2144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movdqa	.LC14(%rip), %xmm6
	movq	%r12, %rsi
	movq	%r14, %rdi
	movaps	%xmm6, -208(%rbp)
	movdqa	.LC15(%rip), %xmm6
	movaps	%xmm6, -192(%rbp)
	movdqa	.LC16(%rip), %xmm6
	movaps	%xmm6, -176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2040(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2088(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2048(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2096(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2104(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2064(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2112(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2072(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2080(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movdqa	-2224(%rbp), %xmm5
	movq	%r12, %rdi
	movdqa	-2208(%rbp), %xmm4
	movdqa	-2192(%rbp), %xmm3
	movdqa	-2176(%rbp), %xmm2
	movl	$8, %esi
	movdqa	-2160(%rbp), %xmm1
	movdqa	-2144(%rbp), %xmm0
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-2232(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShlENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-2128(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	leaq	-208(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$12, %r9d
	leaq	-160(%rbp), %r8
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$0, -2002(%rbp)
	movb	$1, -2001(%rbp)
	je	.L132
	leaq	-2001(%rbp), %rdx
	leaq	-2002(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$0, -2004(%rbp)
	movb	$4, -2003(%rbp)
	je	.L132
	leaq	-2003(%rbp), %rdx
	leaq	-2004(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$0, -2006(%rbp)
	movb	$5, -2005(%rbp)
	je	.L132
	leaq	-2005(%rbp), %rdx
	leaq	-2006(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2048(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$0, -2008(%rbp)
	movb	$2, -2007(%rbp)
	je	.L132
	leaq	-2007(%rbp), %rdx
	leaq	-2008(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$0, -2010(%rbp)
	movb	$3, -2009(%rbp)
	je	.L132
	leaq	-2009(%rbp), %rdx
	leaq	-2010(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2056(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$1, -2012(%rbp)
	movb	$5, -2011(%rbp)
	je	.L132
	leaq	-2011(%rbp), %rdx
	leaq	-2012(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$1, -2014(%rbp)
	movb	$3, -2013(%rbp)
	je	.L132
	leaq	-2013(%rbp), %rdx
	leaq	-2014(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$4, -2016(%rbp)
	movb	$5, -2015(%rbp)
	je	.L132
	leaq	-2015(%rbp), %rdx
	leaq	-2016(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$4, -2018(%rbp)
	movb	$2, -2017(%rbp)
	je	.L132
	leaq	-2017(%rbp), %rdx
	leaq	-2018(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$4, -2020(%rbp)
	movb	$3, -2019(%rbp)
	je	.L132
	leaq	-2019(%rbp), %rdx
	leaq	-2020(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$5, -2022(%rbp)
	movb	$3, -2021(%rbp)
	je	.L132
	leaq	-2021(%rbp), %rdx
	leaq	-2022(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r15)
	movb	$2, -2024(%rbp)
	movb	$3, -2023(%rbp)
	je	.L132
	leaq	-2023(%rbp), %rdx
	leaq	-2024(%rbp), %rsi
	movq	%r15, %rdi
	call	*24(%r15)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-2080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2104(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2048(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2088(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2040(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$2200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L132:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22086:
	.size	_ZN2v88internal24HandlerBuiltinsAssembler33DispatchForElementsKindTransitionENS0_8compiler5TNodeINS0_6Int32TEEES5_RKSt8functionIFvNS0_12ElementsKindES7_EE, .-_ZN2v88internal24HandlerBuiltinsAssembler33DispatchForElementsKindTransitionENS0_8compiler5TNodeINS0_6Int32TEEES5_RKSt8functionIFvNS0_12ElementsKindES7_EE
	.section	.rodata._ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"ElementsTransitionAndStore: store_mode="
	.section	.text._ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE:
.LFB22087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	xorl	%esi, %esi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-432(%rbp), %rbx
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %r13
	jne	.L153
.L137:
	leaq	-592(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	cmpb	$0, _ZN2v88internal31FLAG_trace_elements_transitionsE(%rip)
	je	.L143
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L144:
	movq	-608(%rbp), %xmm1
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-624(%rbp), %xmm0
	movq	-656(%rbp), %xmm2
	movhps	-640(%rbp), %xmm1
	movhps	-648(%rbp), %xmm0
	movhps	-664(%rbp), %xmm2
	movaps	%xmm1, -640(%rbp)
	movaps	%xmm0, -624(%rbp)
	movaps	%xmm2, -608(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movdqa	-640(%rbp), %xmm1
	movq	%rbx, %r8
	movq	%r13, %rcx
	movdqa	-624(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$115, %esi
	movdqa	-608(%rbp), %xmm2
	movl	$6, %r9d
	movq	%r12, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm2, -400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	$0, -416(%rbp)
	movl	$64, %edi
	call	_Znwm@PLT
	movq	-640(%rbp), %rdi
	movq	-648(%rbp), %rdx
	movq	-608(%rbp), %rcx
	movl	%r15d, 48(%rax)
	movq	%rdi, 32(%rax)
	movq	-624(%rbp), %rdi
	movq	%rdx, %rsi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rdi, 40(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindES5_E_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rdi
	movq	%r14, (%rax)
	movq	%rdi, %xmm0
	movq	%r12, %rdi
	movq	%r12, 24(%rax)
	movq	%r13, 56(%rax)
	movq	%rax, -432(%rbp)
	leaq	_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindES2_EZNS1_24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS1_20KeyedAccessStoreModeEEUlS2_S2_E_E9_M_invokeERKSt9_Any_dataOS2_SB_(%rip), %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -416(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-608(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24HandlerBuiltinsAssembler33DispatchForElementsKindTransitionENS0_8compiler5TNodeINS0_6Int32TEEES5_RKSt8functionIFvNS0_12ElementsKindES7_EE
	movq	-416(%rbp), %rax
	testq	%rax, %rax
	je	.L145
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L145:
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L153:
	movq	.LC18(%rip), %xmm1
	leaq	-320(%rbp), %r14
	movq	%r14, %rdi
	movhps	.LC19(%rip), %xmm1
	movaps	%xmm1, -688(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-688(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -696(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$39, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZNSolsEi@PLT
	leaq	-448(%rbp), %rax
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %r9
	movq	%rax, -672(%rbp)
	movq	%rax, -464(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L138
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L139
	subq	%rcx, %rax
	movq	%rax, %r8
.L152:
	movq	%r9, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, -704(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-704(%rbp), %r9
.L140:
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	-672(%rbp), %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	.LC18(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC20(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-688(%rbp), %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	-696(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L139:
	subq	%rcx, %r8
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r9, %rdi
	leaq	-352(%rbp), %rsi
	movq	%r9, -704(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-704(%rbp), %r9
	jmp	.L140
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22087:
	.size	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	.section	.rodata._ZN2v88internal8Builtins44Generate_ElementsTransitionAndStore_StandardEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"ElementsTransitionAndStore_Standard"
	.section	.text._ZN2v88internal8Builtins44Generate_ElementsTransitionAndStore_StandardEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins44Generate_ElementsTransitionAndStore_StandardEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins44Generate_ElementsTransitionAndStore_StandardEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins44Generate_ElementsTransitionAndStore_StandardEPNS0_8compiler18CodeAssemblerStateE:
.LFB22096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$241, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$148, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L159
.L156:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L156
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22096:
	.size	_ZN2v88internal8Builtins44Generate_ElementsTransitionAndStore_StandardEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins44Generate_ElementsTransitionAndStore_StandardEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal44ElementsTransitionAndStore_StandardAssembler47GenerateElementsTransitionAndStore_StandardImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal44ElementsTransitionAndStore_StandardAssembler47GenerateElementsTransitionAndStore_StandardImplEv
	.type	_ZN2v88internal44ElementsTransitionAndStore_StandardAssembler47GenerateElementsTransitionAndStore_StandardImplEv, @function
_ZN2v88internal44ElementsTransitionAndStore_StandardAssembler47GenerateElementsTransitionAndStore_StandardImplEv:
.LFB22100:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22100:
	.size	_ZN2v88internal44ElementsTransitionAndStore_StandardAssembler47GenerateElementsTransitionAndStore_StandardImplEv, .-_ZN2v88internal44ElementsTransitionAndStore_StandardAssembler47GenerateElementsTransitionAndStore_StandardImplEv
	.section	.rodata._ZN2v88internal8Builtins61Generate_ElementsTransitionAndStore_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"ElementsTransitionAndStore_GrowNoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins61Generate_ElementsTransitionAndStore_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins61Generate_ElementsTransitionAndStore_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins61Generate_ElementsTransitionAndStore_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins61Generate_ElementsTransitionAndStore_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$245, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$149, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L166
.L163:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L163
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22105:
	.size	_ZN2v88internal8Builtins61Generate_ElementsTransitionAndStore_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins61Generate_ElementsTransitionAndStore_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal61ElementsTransitionAndStore_GrowNoTransitionHandleCOWAssembler64GenerateElementsTransitionAndStore_GrowNoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal61ElementsTransitionAndStore_GrowNoTransitionHandleCOWAssembler64GenerateElementsTransitionAndStore_GrowNoTransitionHandleCOWImplEv
	.type	_ZN2v88internal61ElementsTransitionAndStore_GrowNoTransitionHandleCOWAssembler64GenerateElementsTransitionAndStore_GrowNoTransitionHandleCOWImplEv, @function
_ZN2v88internal61ElementsTransitionAndStore_GrowNoTransitionHandleCOWAssembler64GenerateElementsTransitionAndStore_GrowNoTransitionHandleCOWImplEv:
.LFB22109:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22109:
	.size	_ZN2v88internal61ElementsTransitionAndStore_GrowNoTransitionHandleCOWAssembler64GenerateElementsTransitionAndStore_GrowNoTransitionHandleCOWImplEv, .-_ZN2v88internal61ElementsTransitionAndStore_GrowNoTransitionHandleCOWAssembler64GenerateElementsTransitionAndStore_GrowNoTransitionHandleCOWImplEv
	.section	.rodata._ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"ElementsTransitionAndStore_NoTransitionIgnoreOOB"
	.section	.text._ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE:
.LFB22114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$250, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$150, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L173
.L170:
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L170
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22114:
	.size	_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal57ElementsTransitionAndStore_NoTransitionIgnoreOOBAssembler60GenerateElementsTransitionAndStore_NoTransitionIgnoreOOBImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal57ElementsTransitionAndStore_NoTransitionIgnoreOOBAssembler60GenerateElementsTransitionAndStore_NoTransitionIgnoreOOBImplEv
	.type	_ZN2v88internal57ElementsTransitionAndStore_NoTransitionIgnoreOOBAssembler60GenerateElementsTransitionAndStore_NoTransitionIgnoreOOBImplEv, @function
_ZN2v88internal57ElementsTransitionAndStore_NoTransitionIgnoreOOBAssembler60GenerateElementsTransitionAndStore_NoTransitionIgnoreOOBImplEv:
.LFB22118:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22118:
	.size	_ZN2v88internal57ElementsTransitionAndStore_NoTransitionIgnoreOOBAssembler60GenerateElementsTransitionAndStore_NoTransitionIgnoreOOBImplEv, .-_ZN2v88internal57ElementsTransitionAndStore_NoTransitionIgnoreOOBAssembler60GenerateElementsTransitionAndStore_NoTransitionIgnoreOOBImplEv
	.section	.rodata._ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"ElementsTransitionAndStore_NoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$255, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$151, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L180
.L177:
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L177
.L181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22123:
	.size	_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins57Generate_ElementsTransitionAndStore_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal57ElementsTransitionAndStore_NoTransitionHandleCOWAssembler60GenerateElementsTransitionAndStore_NoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal57ElementsTransitionAndStore_NoTransitionHandleCOWAssembler60GenerateElementsTransitionAndStore_NoTransitionHandleCOWImplEv
	.type	_ZN2v88internal57ElementsTransitionAndStore_NoTransitionHandleCOWAssembler60GenerateElementsTransitionAndStore_NoTransitionHandleCOWImplEv, @function
_ZN2v88internal57ElementsTransitionAndStore_NoTransitionHandleCOWAssembler60GenerateElementsTransitionAndStore_NoTransitionHandleCOWImplEv:
.LFB22127:
	.cfi_startproc
	endbr64
	movl	$3, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler35Generate_ElementsTransitionAndStoreENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22127:
	.size	_ZN2v88internal57ElementsTransitionAndStore_NoTransitionHandleCOWAssembler60GenerateElementsTransitionAndStore_NoTransitionHandleCOWImplEv, .-_ZN2v88internal57ElementsTransitionAndStore_NoTransitionHandleCOWAssembler60GenerateElementsTransitionAndStore_NoTransitionHandleCOWImplEv
	.section	.text._ZN2v88internal24HandlerBuiltinsAssembler22DispatchByElementsKindENS0_8compiler5TNodeINS0_6Int32TEEERKSt8functionIFvNS0_12ElementsKindEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24HandlerBuiltinsAssembler22DispatchByElementsKindENS0_8compiler5TNodeINS0_6Int32TEEERKSt8functionIFvNS0_12ElementsKindEEEb
	.type	_ZN2v88internal24HandlerBuiltinsAssembler22DispatchByElementsKindENS0_8compiler5TNodeINS0_6Int32TEEERKSt8functionIFvNS0_12ElementsKindEEEb, @function
_ZN2v88internal24HandlerBuiltinsAssembler22DispatchByElementsKindENS0_8compiler5TNodeINS0_6Int32TEEERKSt8functionIFvNS0_12ElementsKindEEEb:
.LFB22128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-2112(%rbp), %r9
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-1856(%rbp), %r8
	pushq	%r13
	leaq	-2624(%rbp), %rdx
	leaq	-3008(%rbp), %r15
	pushq	%r12
	movq	%r15, %xmm9
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-2240(%rbp), %r11
	pushq	%rbx
	leaq	-1984(%rbp), %r10
	movq	%r11, %xmm6
	.cfi_offset 3, -56
	leaq	-2752(%rbp), %rbx
	leaq	-1728(%rbp), %r13
	movq	%rbx, %xmm8
	movq	%r10, %xmm5
	movq	%r13, %xmm4
	subq	$3592, %rsp
	movq	%rsi, -3624(%rbp)
	leaq	-2368(%rbp), %rsi
	movl	%ecx, -3628(%rbp)
	leaq	-2496(%rbp), %rcx
	movq	%rcx, %xmm7
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-2880(%rbp), %rax
	movq	%rdx, -3328(%rbp)
	leaq	-1344(%rbp), %rdx
	movq	%rax, -3304(%rbp)
	leaq	-1600(%rbp), %rax
	movq	%rsi, -3312(%rbp)
	leaq	-1088(%rbp), %rsi
	movq	%r9, -3320(%rbp)
	movhps	-3328(%rbp), %xmm8
	leaq	-832(%rbp), %r9
	movq	%r8, -3336(%rbp)
	movhps	-3304(%rbp), %xmm9
	leaq	-576(%rbp), %r8
	movq	%rax, -3344(%rbp)
	movhps	-3312(%rbp), %xmm7
	movhps	-3320(%rbp), %xmm6
	movaps	%xmm9, -3616(%rbp)
	movhps	-3336(%rbp), %xmm5
	movaps	%xmm8, -3600(%rbp)
	movaps	%xmm7, -3584(%rbp)
	movaps	%xmm6, -3568(%rbp)
	movaps	%xmm5, -3552(%rbp)
	movq	%rbx, -3384(%rbp)
	leaq	-1472(%rbp), %rbx
	movq	%rcx, -3400(%rbp)
	leaq	-1216(%rbp), %rcx
	movq	%rbx, %xmm3
	movq	%r11, -3408(%rbp)
	leaq	-960(%rbp), %r11
	movq	%rcx, %xmm2
	movq	%r10, -3392(%rbp)
	leaq	-704(%rbp), %r10
	movq	%r11, %xmm1
	movq	%r13, -3424(%rbp)
	movhps	-3344(%rbp), %xmm4
	movq	%r10, %xmm0
	leaq	-3264(%rbp), %r13
	movq	%r9, -3360(%rbp)
	movq	%rdx, -3368(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3352(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%r8, -3376(%rbp)
	movhps	-3360(%rbp), %xmm1
	movl	$1, %r8d
	movhps	-3368(%rbp), %xmm3
	movq	%rbx, -3416(%rbp)
	leaq	-3136(%rbp), %rbx
	movhps	-3352(%rbp), %xmm2
	movq	%rcx, -3440(%rbp)
	xorl	%ecx, %ecx
	movhps	-3376(%rbp), %xmm0
	movq	%r11, -3448(%rbp)
	movq	%r10, -3432(%rbp)
	movaps	%xmm4, -3536(%rbp)
	movaps	%xmm3, -3520(%rbp)
	movaps	%xmm2, -3504(%rbp)
	movaps	%xmm1, -3488(%rbp)
	movaps	%xmm0, -3472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movdqa	.LC25(%rip), %xmm10
	movq	%r15, -3456(%rbp)
	movl	$27, -240(%rbp)
	leaq	-448(%rbp), %r15
	movaps	%xmm10, -320(%rbp)
	movdqa	.LC26(%rip), %xmm10
	movaps	%xmm10, -304(%rbp)
	movdqa	.LC27(%rip), %xmm10
	movaps	%xmm10, -288(%rbp)
	movdqa	.LC28(%rip), %xmm10
	movaps	%xmm10, -272(%rbp)
	movdqa	.LC29(%rip), %xmm10
	movaps	%xmm10, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3304(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3384(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3328(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3400(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3312(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3320(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3392(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3336(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3344(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3416(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3368(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3440(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3352(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3448(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movq	-3432(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3376(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3624(%rbp), %r10
	movq	%rbx, %rdx
	movdqa	-3616(%rbp), %xmm9
	movdqa	-3600(%rbp), %xmm8
	movdqa	-3584(%rbp), %xmm7
	leaq	-320(%rbp), %rcx
	movl	$21, %r9d
	movdqa	-3568(%rbp), %xmm6
	movdqa	-3552(%rbp), %xmm5
	movq	%r10, %rsi
	leaq	-224(%rbp), %r8
	movdqa	-3536(%rbp), %xmm4
	movdqa	-3520(%rbp), %xmm3
	movq	%r12, %rdi
	movaps	%xmm9, -224(%rbp)
	movdqa	-3504(%rbp), %xmm2
	movdqa	-3488(%rbp), %xmm1
	movaps	%xmm8, -208(%rbp)
	movdqa	-3472(%rbp), %xmm0
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-3456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$0, -3281(%rbp)
	je	.L185
	leaq	-3281(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$1, -3280(%rbp)
	je	.L185
	leaq	-3280(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$2, -3279(%rbp)
	je	.L185
	leaq	-3279(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$3, -3278(%rbp)
	je	.L185
	leaq	-3278(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$4, -3277(%rbp)
	je	.L185
	leaq	-3277(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$5, -3276(%rbp)
	je	.L185
	leaq	-3276(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, -3628(%rbp)
	je	.L190
	cmpq	$0, 16(%r14)
	movb	$17, -3275(%rbp)
	je	.L185
	leaq	-3275(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$18, -3274(%rbp)
	je	.L185
	leaq	-3274(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$19, -3273(%rbp)
	je	.L185
	leaq	-3273(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$20, -3272(%rbp)
	je	.L185
	leaq	-3272(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$21, -3271(%rbp)
	je	.L185
	leaq	-3271(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$22, -3270(%rbp)
	je	.L185
	leaq	-3270(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$23, -3269(%rbp)
	je	.L185
	leaq	-3269(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$24, -3268(%rbp)
	je	.L185
	leaq	-3268(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$25, -3267(%rbp)
	je	.L185
	leaq	-3267(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$26, -3266(%rbp)
	je	.L185
	leaq	-3266(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	movb	$27, -3265(%rbp)
	je	.L185
	leaq	-3265(%rbp), %rsi
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
.L187:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3376(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3360(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3352(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3344(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3392(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3312(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3400(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3384(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$3592, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-3376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L187
.L185:
	call	_ZSt25__throw_bad_function_callv@PLT
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22128:
	.size	_ZN2v88internal24HandlerBuiltinsAssembler22DispatchByElementsKindENS0_8compiler5TNodeINS0_6Int32TEEERKSt8functionIFvNS0_12ElementsKindEEEb, .-_ZN2v88internal24HandlerBuiltinsAssembler22DispatchByElementsKindENS0_8compiler5TNodeINS0_6Int32TEEERKSt8functionIFvNS0_12ElementsKindEEEb
	.section	.rodata._ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"StoreFastElementStub: store_mode="
	.section	.text._ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE:
.LFB22129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-432(%rbp), %rbx
	subq	$664, %rsp
	movl	%esi, -616(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, -648(%rbp)
	jne	.L207
.L193:
	leaq	-608(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-592(%rbp), %r15
	movq	%r14, %xmm3
	movl	$1, %r8d
	movq	%r15, %xmm0
	movq	%r15, %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$64, %edi
	movq	$0, -416(%rbp)
	call	_Znwm@PLT
	movdqa	-688(%rbp), %xmm0
	movq	%r12, %rdi
	movq	-640(%rbp), %xmm2
	movl	-616(%rbp), %edx
	movq	%r12, %xmm4
	movq	%r13, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS2_20KeyedAccessStoreModeEEUlNS2_12ElementsKindEE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	movups	%xmm0, (%rax)
	movdqa	%xmm2, %xmm0
	movq	%xmm2, %rsi
	movhps	-624(%rbp), %xmm0
	movl	%edx, 40(%rax)
	movups	%xmm0, 16(%rax)
	movq	-648(%rbp), %xmm0
	movq	%rax, -432(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 48(%rax)
	leaq	_ZNSt17_Function_handlerIFvN2v88internal12ElementsKindEEZNS1_24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS1_20KeyedAccessStoreModeEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -416(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	testl	$-3, -616(%rbp)
	movq	%rax, %rsi
	sete	%cl
	call	_ZN2v88internal24HandlerBuiltinsAssembler22DispatchByElementsKindENS0_8compiler5TNodeINS0_6Int32TEEERKSt8functionIFvNS0_12ElementsKindEEEb
	movq	-416(%rbp), %rax
	testq	%rax, %rax
	je	.L199
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L199:
	movq	-664(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movhps	-640(%rbp), %xmm0
	movaps	%xmm0, -640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movl	$117, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movdqa	-640(%rbp), %xmm0
	movq	-616(%rbp), %xmm1
	movl	$5, %r9d
	movq	-624(%rbp), %rax
	movq	-648(%rbp), %rcx
	movhps	-656(%rbp), %xmm1
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	.LC18(%rip), %xmm1
	leaq	-320(%rbp), %r14
	leaq	-368(%rbp), %r15
	movq	%r14, %rdi
	movhps	.LC19(%rip), %xmm1
	movaps	%xmm1, -688(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-688(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$33, %edx
	leaq	.LC30(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-616(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZNSolsEi@PLT
	leaq	-448(%rbp), %rax
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %r9
	movq	%rax, -672(%rbp)
	movq	%rax, -464(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L194
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L195
	subq	%rcx, %rax
	movq	%rax, %r8
.L206:
	movq	%r9, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, -696(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-696(%rbp), %r9
.L196:
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	-672(%rbp), %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	.LC18(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC20(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-688(%rbp), %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L195:
	subq	%rcx, %r8
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r9, %rdi
	leaq	-352(%rbp), %rsi
	movq	%r9, -696(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-696(%rbp), %r9
	jmp	.L196
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22129:
	.size	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	.section	.rodata._ZN2v88internal8Builtins36Generate_StoreFastElementIC_StandardEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"StoreFastElementIC_Standard"
	.section	.text._ZN2v88internal8Builtins36Generate_StoreFastElementIC_StandardEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_StoreFastElementIC_StandardEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_StoreFastElementIC_StandardEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_StoreFastElementIC_StandardEPNS0_8compiler18CodeAssemblerStateE:
.LFB22138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$374, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$137, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L213
.L210:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L210
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22138:
	.size	_ZN2v88internal8Builtins36Generate_StoreFastElementIC_StandardEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_StoreFastElementIC_StandardEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36StoreFastElementIC_StandardAssembler39GenerateStoreFastElementIC_StandardImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36StoreFastElementIC_StandardAssembler39GenerateStoreFastElementIC_StandardImplEv
	.type	_ZN2v88internal36StoreFastElementIC_StandardAssembler39GenerateStoreFastElementIC_StandardImplEv, @function
_ZN2v88internal36StoreFastElementIC_StandardAssembler39GenerateStoreFastElementIC_StandardImplEv:
.LFB22142:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22142:
	.size	_ZN2v88internal36StoreFastElementIC_StandardAssembler39GenerateStoreFastElementIC_StandardImplEv, .-_ZN2v88internal36StoreFastElementIC_StandardAssembler39GenerateStoreFastElementIC_StandardImplEv
	.section	.rodata._ZN2v88internal8Builtins53Generate_StoreFastElementIC_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"StoreFastElementIC_GrowNoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins53Generate_StoreFastElementIC_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins53Generate_StoreFastElementIC_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins53Generate_StoreFastElementIC_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins53Generate_StoreFastElementIC_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$378, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$138, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L220
.L217:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L217
.L221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22147:
	.size	_ZN2v88internal8Builtins53Generate_StoreFastElementIC_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins53Generate_StoreFastElementIC_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal53StoreFastElementIC_GrowNoTransitionHandleCOWAssembler56GenerateStoreFastElementIC_GrowNoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal53StoreFastElementIC_GrowNoTransitionHandleCOWAssembler56GenerateStoreFastElementIC_GrowNoTransitionHandleCOWImplEv
	.type	_ZN2v88internal53StoreFastElementIC_GrowNoTransitionHandleCOWAssembler56GenerateStoreFastElementIC_GrowNoTransitionHandleCOWImplEv, @function
_ZN2v88internal53StoreFastElementIC_GrowNoTransitionHandleCOWAssembler56GenerateStoreFastElementIC_GrowNoTransitionHandleCOWImplEv:
.LFB22151:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22151:
	.size	_ZN2v88internal53StoreFastElementIC_GrowNoTransitionHandleCOWAssembler56GenerateStoreFastElementIC_GrowNoTransitionHandleCOWImplEv, .-_ZN2v88internal53StoreFastElementIC_GrowNoTransitionHandleCOWAssembler56GenerateStoreFastElementIC_GrowNoTransitionHandleCOWImplEv
	.section	.rodata._ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"StoreFastElementIC_NoTransitionIgnoreOOB"
	.section	.text._ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE:
.LFB22156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$383, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC33(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$139, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L227
.L224:
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L224
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22156:
	.size	_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal49StoreFastElementIC_NoTransitionIgnoreOOBAssembler52GenerateStoreFastElementIC_NoTransitionIgnoreOOBImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal49StoreFastElementIC_NoTransitionIgnoreOOBAssembler52GenerateStoreFastElementIC_NoTransitionIgnoreOOBImplEv
	.type	_ZN2v88internal49StoreFastElementIC_NoTransitionIgnoreOOBAssembler52GenerateStoreFastElementIC_NoTransitionIgnoreOOBImplEv, @function
_ZN2v88internal49StoreFastElementIC_NoTransitionIgnoreOOBAssembler52GenerateStoreFastElementIC_NoTransitionIgnoreOOBImplEv:
.LFB22160:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22160:
	.size	_ZN2v88internal49StoreFastElementIC_NoTransitionIgnoreOOBAssembler52GenerateStoreFastElementIC_NoTransitionIgnoreOOBImplEv, .-_ZN2v88internal49StoreFastElementIC_NoTransitionIgnoreOOBAssembler52GenerateStoreFastElementIC_NoTransitionIgnoreOOBImplEv
	.section	.rodata._ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"StoreFastElementIC_NoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$387, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC34(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$140, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L234
.L231:
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L231
.L235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22165:
	.size	_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins49Generate_StoreFastElementIC_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal49StoreFastElementIC_NoTransitionHandleCOWAssembler52GenerateStoreFastElementIC_NoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal49StoreFastElementIC_NoTransitionHandleCOWAssembler52GenerateStoreFastElementIC_NoTransitionHandleCOWImplEv
	.type	_ZN2v88internal49StoreFastElementIC_NoTransitionHandleCOWAssembler52GenerateStoreFastElementIC_NoTransitionHandleCOWImplEv, @function
_ZN2v88internal49StoreFastElementIC_NoTransitionHandleCOWAssembler52GenerateStoreFastElementIC_NoTransitionHandleCOWImplEv:
.LFB22169:
	.cfi_startproc
	endbr64
	movl	$3, %esi
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler27Generate_StoreFastElementICENS0_20KeyedAccessStoreModeE
	.cfi_endproc
.LFE22169:
	.size	_ZN2v88internal49StoreFastElementIC_NoTransitionHandleCOWAssembler52GenerateStoreFastElementIC_NoTransitionHandleCOWImplEv, .-_ZN2v88internal49StoreFastElementIC_NoTransitionHandleCOWAssembler52GenerateStoreFastElementIC_NoTransitionHandleCOWImplEv
	.section	.text._ZN2v88internal26LoadGlobalIC_SlowAssembler29GenerateLoadGlobalIC_SlowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26LoadGlobalIC_SlowAssembler29GenerateLoadGlobalIC_SlowImplEv
	.type	_ZN2v88internal26LoadGlobalIC_SlowAssembler29GenerateLoadGlobalIC_SlowImplEv, @function
_ZN2v88internal26LoadGlobalIC_SlowAssembler29GenerateLoadGlobalIC_SlowImplEv:
.LFB22178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-64(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-80(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$3, %r9d
	movl	$122, %esi
	movq	%rbx, -48(%rbp)
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L240:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22178:
	.size	_ZN2v88internal26LoadGlobalIC_SlowAssembler29GenerateLoadGlobalIC_SlowImplEv, .-_ZN2v88internal26LoadGlobalIC_SlowAssembler29GenerateLoadGlobalIC_SlowImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_LoadGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"LoadGlobalIC_Slow"
	.section	.text._ZN2v88internal8Builtins26Generate_LoadGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_LoadGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_LoadGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_LoadGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$391, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$120, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L245
.L242:
	movq	%r13, %rdi
	call	_ZN2v88internal26LoadGlobalIC_SlowAssembler29GenerateLoadGlobalIC_SlowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L246
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L242
.L246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22174:
	.size	_ZN2v88internal8Builtins26Generate_LoadGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_LoadGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33LoadIC_FunctionPrototypeAssembler36GenerateLoadIC_FunctionPrototypeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33LoadIC_FunctionPrototypeAssembler36GenerateLoadIC_FunctionPrototypeImplEv
	.type	_ZN2v88internal33LoadIC_FunctionPrototypeAssembler36GenerateLoadIC_FunctionPrototypeImplEv, @function
_ZN2v88internal33LoadIC_FunctionPrototypeAssembler36GenerateLoadIC_FunctionPrototypeImplEv:
.LFB22187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23LoadJSFunctionPrototypeENS0_8compiler5TNodeINS0_10JSFunctionEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm0
	movq	%r14, %rcx
	movq	%r12, %rdi
	movhps	-232(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	-80(%rbp), %r8
	movl	$4, %r9d
	movaps	%xmm0, -80(%rbp)
	movl	$123, %esi
	movq	-224(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L250:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22187:
	.size	_ZN2v88internal33LoadIC_FunctionPrototypeAssembler36GenerateLoadIC_FunctionPrototypeImplEv, .-_ZN2v88internal33LoadIC_FunctionPrototypeAssembler36GenerateLoadIC_FunctionPrototypeImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_LoadIC_FunctionPrototypeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"LoadIC_FunctionPrototype"
	.section	.text._ZN2v88internal8Builtins33Generate_LoadIC_FunctionPrototypeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_LoadIC_FunctionPrototypeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_LoadIC_FunctionPrototypeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_LoadIC_FunctionPrototypeEPNS0_8compiler18CodeAssemblerStateE:
.LFB22183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$400, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$121, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L255
.L252:
	movq	%r13, %rdi
	call	_ZN2v88internal33LoadIC_FunctionPrototypeAssembler36GenerateLoadIC_FunctionPrototypeImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L252
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22183:
	.size	_ZN2v88internal8Builtins33Generate_LoadIC_FunctionPrototypeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_LoadIC_FunctionPrototypeEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins20Generate_LoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"LoadIC_Slow"
	.section	.text._ZN2v88internal8Builtins20Generate_LoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_LoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_LoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_LoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$414, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC37(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$122, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L261
.L258:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-72(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$224, %esi
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L258
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22192:
	.size	_ZN2v88internal8Builtins20Generate_LoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_LoadIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20LoadIC_SlowAssembler23GenerateLoadIC_SlowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20LoadIC_SlowAssembler23GenerateLoadIC_SlowImplEv
	.type	_ZN2v88internal20LoadIC_SlowAssembler23GenerateLoadIC_SlowImplEv, @function
_ZN2v88internal20LoadIC_SlowAssembler23GenerateLoadIC_SlowImplEv:
.LFB22196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-64(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$224, %esi
	movhps	-56(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L266:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22196:
	.size	_ZN2v88internal20LoadIC_SlowAssembler23GenerateLoadIC_SlowImplEv, .-_ZN2v88internal20LoadIC_SlowAssembler23GenerateLoadIC_SlowImplEv
	.section	.text._ZN2v88internal27StoreGlobalIC_SlowAssembler30GenerateStoreGlobalIC_SlowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27StoreGlobalIC_SlowAssembler30GenerateStoreGlobalIC_SlowImplEv
	.type	_ZN2v88internal27StoreGlobalIC_SlowAssembler30GenerateStoreGlobalIC_SlowImplEv, @function
_ZN2v88internal27StoreGlobalIC_SlowAssembler30GenerateStoreGlobalIC_SlowImplEv:
.LFB22205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$5, %r9d
	movl	$128, %esi
	movq	%rbx, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-88(%rbp), %xmm0
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L270:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22205:
	.size	_ZN2v88internal27StoreGlobalIC_SlowAssembler30GenerateStoreGlobalIC_SlowImplEv, .-_ZN2v88internal27StoreGlobalIC_SlowAssembler30GenerateStoreGlobalIC_SlowImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_StoreGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"StoreGlobalIC_Slow"
	.section	.text._ZN2v88internal8Builtins27Generate_StoreGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_StoreGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_StoreGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_StoreGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$422, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$126, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L275
.L272:
	movq	%r13, %rdi
	call	_ZN2v88internal27StoreGlobalIC_SlowAssembler30GenerateStoreGlobalIC_SlowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L272
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22201:
	.size	_ZN2v88internal8Builtins27Generate_StoreGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_StoreGlobalIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36KeyedLoadIC_SloppyArgumentsAssembler39GenerateKeyedLoadIC_SloppyArgumentsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36KeyedLoadIC_SloppyArgumentsAssembler39GenerateKeyedLoadIC_SloppyArgumentsImplEv
	.type	_ZN2v88internal36KeyedLoadIC_SloppyArgumentsAssembler39GenerateKeyedLoadIC_SloppyArgumentsImplEv, @function
_ZN2v88internal36KeyedLoadIC_SloppyArgumentsAssembler39GenerateKeyedLoadIC_SloppyArgumentsImplEv:
.LFB22214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %r8
	movq	%rbx, %rsi
	leaq	-96(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24EmitKeyedSloppyArgumentsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS1_19ArgumentsAccessModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L283
.L278:
	movq	%rbx, %xmm0
	movl	$4, %esi
	movq	%r12, %rdi
	movq	-248(%rbp), %xmm1
	movhps	-272(%rbp), %xmm0
	movhps	-240(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm1, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$116, %esi
	movdqa	-272(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$4, %r9d
	movq	%r12, %rdi
	movdqa	-240(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	leaq	-80(%rbp), %rax
	movq	%r12, %rdi
	movq	%r14, %rsi
	movb	$0, -76(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -256(%rbp)
	movl	$1936943437, -80(%rbp)
	movq	$4, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-256(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L278
	call	_ZdlPv@PLT
	jmp	.L278
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22214:
	.size	_ZN2v88internal36KeyedLoadIC_SloppyArgumentsAssembler39GenerateKeyedLoadIC_SloppyArgumentsImplEv, .-_ZN2v88internal36KeyedLoadIC_SloppyArgumentsAssembler39GenerateKeyedLoadIC_SloppyArgumentsImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_KeyedLoadIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"KeyedLoadIC_SloppyArguments"
	.section	.text._ZN2v88internal8Builtins36Generate_KeyedLoadIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_KeyedLoadIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_KeyedLoadIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_KeyedLoadIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$436, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC39(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$129, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L289
.L286:
	movq	%r13, %rdi
	call	_ZN2v88internal36KeyedLoadIC_SloppyArgumentsAssembler39GenerateKeyedLoadIC_SloppyArgumentsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L286
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22210:
	.size	_ZN2v88internal8Builtins36Generate_KeyedLoadIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_KeyedLoadIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	.type	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv, @function
_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv:
.LFB22215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	-232(%rbp), %rsi
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24EmitKeyedSloppyArgumentsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS1_19ArgumentsAccessModeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm0
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	-96(%rbp), %r8
	movl	$117, %esi
	movq	%r14, -64(%rbp)
	movhps	-240(%rbp), %xmm0
	movl	$5, %r9d
	movaps	%xmm0, -96(%rbp)
	movq	-248(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L294
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L294:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22215:
	.size	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv, .-_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	.section	.rodata._ZN2v88internal8Builtins46Generate_KeyedStoreIC_SloppyArguments_StandardEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"KeyedStoreIC_SloppyArguments_Standard"
	.section	.text._ZN2v88internal8Builtins46Generate_KeyedStoreIC_SloppyArguments_StandardEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins46Generate_KeyedStoreIC_SloppyArguments_StandardEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins46Generate_KeyedStoreIC_SloppyArguments_StandardEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins46Generate_KeyedStoreIC_SloppyArguments_StandardEPNS0_8compiler18CodeAssemblerStateE:
.LFB22220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$475, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$132, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L299
.L296:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L300
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L296
.L300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22220:
	.size	_ZN2v88internal8Builtins46Generate_KeyedStoreIC_SloppyArguments_StandardEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins46Generate_KeyedStoreIC_SloppyArguments_StandardEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal46KeyedStoreIC_SloppyArguments_StandardAssembler49GenerateKeyedStoreIC_SloppyArguments_StandardImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal46KeyedStoreIC_SloppyArguments_StandardAssembler49GenerateKeyedStoreIC_SloppyArguments_StandardImplEv
	.type	_ZN2v88internal46KeyedStoreIC_SloppyArguments_StandardAssembler49GenerateKeyedStoreIC_SloppyArguments_StandardImplEv, @function
_ZN2v88internal46KeyedStoreIC_SloppyArguments_StandardAssembler49GenerateKeyedStoreIC_SloppyArguments_StandardImplEv:
.LFB22224:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	.cfi_endproc
.LFE22224:
	.size	_ZN2v88internal46KeyedStoreIC_SloppyArguments_StandardAssembler49GenerateKeyedStoreIC_SloppyArguments_StandardImplEv, .-_ZN2v88internal46KeyedStoreIC_SloppyArguments_StandardAssembler49GenerateKeyedStoreIC_SloppyArguments_StandardImplEv
	.section	.rodata._ZN2v88internal8Builtins63Generate_KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins63Generate_KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins63Generate_KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins63Generate_KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins63Generate_KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$479, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC41(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$133, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L306
.L303:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L307
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L303
.L307:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22229:
	.size	_ZN2v88internal8Builtins63Generate_KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins63Generate_KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal63KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWAssembler66GenerateKeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal63KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWAssembler66GenerateKeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWImplEv
	.type	_ZN2v88internal63KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWAssembler66GenerateKeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWImplEv, @function
_ZN2v88internal63KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWAssembler66GenerateKeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWImplEv:
.LFB22233:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	.cfi_endproc
.LFE22233:
	.size	_ZN2v88internal63KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWAssembler66GenerateKeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWImplEv, .-_ZN2v88internal63KeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWAssembler66GenerateKeyedStoreIC_SloppyArguments_GrowNoTransitionHandleCOWImplEv
	.section	.rodata._ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOB"
	.section	.text._ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE:
.LFB22238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$484, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$134, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L313
.L310:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L310
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22238:
	.size	_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBImplEv
	.type	_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBImplEv, @function
_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBImplEv:
.LFB22242:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	.cfi_endproc
.LFE22242:
	.size	_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBImplEv, .-_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionIgnoreOOBImplEv
	.section	.rodata._ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"KeyedStoreIC_SloppyArguments_NoTransitionHandleCOW"
	.section	.text._ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE:
.LFB22247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$489, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC43(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$135, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L320
.L317:
	movq	%r13, %rdi
	call	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L317
.L321:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22247:
	.size	_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins59Generate_KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionHandleCOWImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionHandleCOWImplEv
	.type	_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionHandleCOWImplEv, @function
_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionHandleCOWImplEv:
.LFB22251:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal24HandlerBuiltinsAssembler37Generate_KeyedStoreIC_SloppyArgumentsEv
	.cfi_endproc
.LFE22251:
	.size	_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionHandleCOWImplEv, .-_ZN2v88internal59KeyedStoreIC_SloppyArguments_NoTransitionHandleCOWAssembler62GenerateKeyedStoreIC_SloppyArguments_NoTransitionHandleCOWImplEv
	.section	.text._ZN2v88internal27StoreInterceptorICAssembler30GenerateStoreInterceptorICImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27StoreInterceptorICAssembler30GenerateStoreInterceptorICImplEv
	.type	_ZN2v88internal27StoreInterceptorICAssembler30GenerateStoreInterceptorICImplEv, @function
_ZN2v88internal27StoreInterceptorICAssembler30GenerateStoreInterceptorICImplEv:
.LFB22260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$5, %r9d
	movl	$131, %esi
	movq	%rbx, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-88(%rbp), %xmm0
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L326
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L326:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22260:
	.size	_ZN2v88internal27StoreInterceptorICAssembler30GenerateStoreInterceptorICImplEv, .-_ZN2v88internal27StoreInterceptorICAssembler30GenerateStoreInterceptorICImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_StoreInterceptorICEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC44:
	.string	"StoreInterceptorIC"
	.section	.text._ZN2v88internal8Builtins27Generate_StoreInterceptorICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_StoreInterceptorICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_StoreInterceptorICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_StoreInterceptorICEPNS0_8compiler18CodeAssemblerStateE:
.LFB22256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$494, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC44(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$131, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L331
.L328:
	movq	%r13, %rdi
	call	_ZN2v88internal27StoreInterceptorICAssembler30GenerateStoreInterceptorICImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L332
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L328
.L332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22256:
	.size	_ZN2v88internal8Builtins27Generate_StoreInterceptorICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_StoreInterceptorICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33LoadIndexedInterceptorICAssembler36GenerateLoadIndexedInterceptorICImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33LoadIndexedInterceptorICAssembler36GenerateLoadIndexedInterceptorICImplEv
	.type	_ZN2v88internal33LoadIndexedInterceptorICAssembler36GenerateLoadIndexedInterceptorICImplEv, @function
_ZN2v88internal33LoadIndexedInterceptorICAssembler36GenerateLoadIndexedInterceptorICImplEv:
.LFB22269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19TaggedIsPositiveSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm1
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-384(%rbp), %xmm0
	leaq	-96(%rbp), %rbx
	movq	%rax, %rdx
	movl	$2, %r9d
	movq	%rbx, %r8
	movl	$120, %esi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	$116, %esi
	movdqa	-384(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$4, %r9d
	movq	%r12, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-368(%rbp), %xmm0
	movhps	-360(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L336:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22269:
	.size	_ZN2v88internal33LoadIndexedInterceptorICAssembler36GenerateLoadIndexedInterceptorICImplEv, .-_ZN2v88internal33LoadIndexedInterceptorICAssembler36GenerateLoadIndexedInterceptorICImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_LoadIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"LoadIndexedInterceptorIC"
	.section	.text._ZN2v88internal8Builtins33Generate_LoadIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_LoadIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_LoadIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_LoadIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE:
.LFB22265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$505, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC45(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$130, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L341
.L338:
	movq	%r13, %rdi
	call	_ZN2v88internal33LoadIndexedInterceptorICAssembler36GenerateLoadIndexedInterceptorICImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L342
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L338
.L342:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22265:
	.size	_ZN2v88internal8Builtins33Generate_LoadIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_LoadIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35KeyedHasIC_SloppyArgumentsAssembler38GenerateKeyedHasIC_SloppyArgumentsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35KeyedHasIC_SloppyArgumentsAssembler38GenerateKeyedHasIC_SloppyArgumentsImplEv
	.type	_ZN2v88internal35KeyedHasIC_SloppyArgumentsAssembler38GenerateKeyedHasIC_SloppyArgumentsImplEv, @function
_ZN2v88internal35KeyedHasIC_SloppyArgumentsAssembler38GenerateKeyedHasIC_SloppyArgumentsImplEv:
.LFB22278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$2, %r9d
	movq	%r13, %r8
	movq	%rbx, %rsi
	leaq	-96(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24EmitKeyedSloppyArgumentsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS1_19ArgumentsAccessModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L349
.L344:
	movq	%rbx, %xmm0
	movl	$4, %esi
	movq	%r12, %rdi
	movq	-248(%rbp), %xmm1
	movhps	-272(%rbp), %xmm0
	movhps	-240(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm1, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$133, %esi
	movdqa	-272(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$4, %r9d
	movq	%r12, %rdi
	movdqa	-240(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	leaq	-80(%rbp), %rax
	movq	%r12, %rdi
	movq	%r14, %rsi
	movb	$0, -76(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -256(%rbp)
	movl	$1936943437, -80(%rbp)
	movq	$4, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-256(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L344
	call	_ZdlPv@PLT
	jmp	.L344
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22278:
	.size	_ZN2v88internal35KeyedHasIC_SloppyArgumentsAssembler38GenerateKeyedHasIC_SloppyArgumentsImplEv, .-_ZN2v88internal35KeyedHasIC_SloppyArgumentsAssembler38GenerateKeyedHasIC_SloppyArgumentsImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_KeyedHasIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"KeyedHasIC_SloppyArguments"
	.section	.text._ZN2v88internal8Builtins35Generate_KeyedHasIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_KeyedHasIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_KeyedHasIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_KeyedHasIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$522, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC46(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$153, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L355
.L352:
	movq	%r13, %rdi
	call	_ZN2v88internal35KeyedHasIC_SloppyArgumentsAssembler38GenerateKeyedHasIC_SloppyArgumentsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L352
.L356:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22274:
	.size	_ZN2v88internal8Builtins35Generate_KeyedHasIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_KeyedHasIC_SloppyArgumentsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32HasIndexedInterceptorICAssembler35GenerateHasIndexedInterceptorICImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32HasIndexedInterceptorICAssembler35GenerateHasIndexedInterceptorICImplEv
	.type	_ZN2v88internal32HasIndexedInterceptorICAssembler35GenerateHasIndexedInterceptorICImplEv, @function
_ZN2v88internal32HasIndexedInterceptorICAssembler35GenerateHasIndexedInterceptorICImplEv:
.LFB22287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19TaggedIsPositiveSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm1
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-384(%rbp), %xmm0
	leaq	-96(%rbp), %rbx
	movq	%rax, %rdx
	movl	$2, %r9d
	movq	%rbx, %r8
	movl	$134, %esi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	$133, %esi
	movdqa	-384(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$4, %r9d
	movq	%r12, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-368(%rbp), %xmm0
	movhps	-360(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L360
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L360:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22287:
	.size	_ZN2v88internal32HasIndexedInterceptorICAssembler35GenerateHasIndexedInterceptorICImplEv, .-_ZN2v88internal32HasIndexedInterceptorICAssembler35GenerateHasIndexedInterceptorICImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_HasIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"HasIndexedInterceptorIC"
	.section	.text._ZN2v88internal8Builtins32Generate_HasIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_HasIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_HasIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_HasIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE:
.LFB22283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$542, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC47(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$154, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L365
.L362:
	movq	%r13, %rdi
	call	_ZN2v88internal32HasIndexedInterceptorICAssembler35GenerateHasIndexedInterceptorICImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L366
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L362
.L366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22283:
	.size	_ZN2v88internal8Builtins32Generate_HasIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_HasIndexedInterceptorICEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins19Generate_HasIC_SlowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"HasIC_Slow"
	.section	.text._ZN2v88internal8Builtins19Generate_HasIC_SlowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_HasIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_HasIC_SlowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_HasIC_SlowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$559, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$155, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L371
.L368:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-72(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$227, %esi
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L368
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22292:
	.size	_ZN2v88internal8Builtins19Generate_HasIC_SlowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_HasIC_SlowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal19HasIC_SlowAssembler22GenerateHasIC_SlowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19HasIC_SlowAssembler22GenerateHasIC_SlowImplEv
	.type	_ZN2v88internal19HasIC_SlowAssembler22GenerateHasIC_SlowImplEv, @function
_ZN2v88internal19HasIC_SlowAssembler22GenerateHasIC_SlowImplEv:
.LFB22296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-64(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$227, %esi
	movhps	-56(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L376:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22296:
	.size	_ZN2v88internal19HasIC_SlowAssembler22GenerateHasIC_SlowImplEv, .-_ZN2v88internal19HasIC_SlowAssembler22GenerateHasIC_SlowImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE:
.LFB28576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28576:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_LoadIC_StringLengthEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC14:
	.long	1
	.long	4
	.long	5
	.long	2
	.align 16
.LC15:
	.long	3
	.long	261
	.long	259
	.long	1029
	.align 16
.LC16:
	.long	1026
	.long	1027
	.long	1283
	.long	515
	.section	.data.rel.ro,"aw"
	.align 8
.LC18:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC19:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC20:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16
	.align 16
.LC25:
	.long	0
	.long	1
	.long	2
	.long	6
	.align 16
.LC26:
	.long	8
	.long	3
	.long	7
	.long	9
	.align 16
.LC27:
	.long	4
	.long	5
	.long	17
	.long	18
	.align 16
.LC28:
	.long	19
	.long	20
	.long	21
	.long	22
	.align 16
.LC29:
	.long	23
	.long	24
	.long	25
	.long	26
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
