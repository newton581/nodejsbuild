	.file	"builtins-lazy-gen.cc"
	.text
	.section	.text._ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE
	.type	_ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE, @function
_ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE:
.LFB13401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$2, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r9
	movq	%r15, %rcx
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	.cfi_endproc
.LFE13401:
	.size	_ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE, .-_ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE
	.section	.text._ZN2v88internal21LazyBuiltinsAssembler30GenerateTailCallToReturnedCodeENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LazyBuiltinsAssembler30GenerateTailCallToReturnedCodeENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_10JSFunctionEEE
	.type	_ZN2v88internal21LazyBuiltinsAssembler30GenerateTailCallToReturnedCodeENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_10JSFunctionEEE, @function
_ZN2v88internal21LazyBuiltinsAssembler30GenerateTailCallToReturnedCodeENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_10JSFunctionEEE:
.LFB13402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movl	$3, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-64(%rbp), %rcx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %r9
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rax, %r8
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13402:
	.size	_ZN2v88internal21LazyBuiltinsAssembler30GenerateTailCallToReturnedCodeENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_10JSFunctionEEE, .-_ZN2v88internal21LazyBuiltinsAssembler30GenerateTailCallToReturnedCodeENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE
	.type	_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE, @function
_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE:
.LFB13403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	xorl	%ecx, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	movl	$1, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	xorl	%edx, %edx
	subq	$168, %rsp
	movq	%rsi, -200(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-200(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-64(%rbp), %rcx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	-200(%rbp), %r9
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13403:
	.size	_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE, .-_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE
	.section	.text._ZN2v88internal21LazyBuiltinsAssembler30MaybeTailCallOptimizedCodeSlotENS0_8compiler5TNodeINS0_10JSFunctionEEENS3_INS0_14FeedbackVectorEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LazyBuiltinsAssembler30MaybeTailCallOptimizedCodeSlotENS0_8compiler5TNodeINS0_10JSFunctionEEENS3_INS0_14FeedbackVectorEEE
	.type	_ZN2v88internal21LazyBuiltinsAssembler30MaybeTailCallOptimizedCodeSlotENS0_8compiler5TNodeINS0_10JSFunctionEEENS3_INS0_14FeedbackVectorEEE, @function
_ZN2v88internal21LazyBuiltinsAssembler30MaybeTailCallOptimizedCodeSlotENS0_8compiler5TNodeINS0_10JSFunctionEEENS3_INS0_14FeedbackVectorEEE:
.LFB13404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-576(%rbp), %r15
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$568, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler5TNodeINS0_11MaybeObjectEEE@PLT
	movq	-584(%rbp), %r10
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	movq	%r10, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-584(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %r8
	movl	$58, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE
	movq	%r14, %rsi
	movq	%r13, %r8
	movl	$56, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE
	movq	%r13, %r8
	movl	$55, %ecx
	movq	%r14, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal21LazyBuiltinsAssembler29TailCallRuntimeIfMarkerEqualsENS0_8compiler5TNodeINS0_3SmiEEENS0_18OptimizationMarkerENS0_7Runtime10FunctionIdENS3_INS0_10JSFunctionEEE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-592(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23GetHeapObjectAssumeWeakENS0_8compiler5TNodeINS0_11MaybeObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	leaq	-192(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r11, -600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE(%rip), %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-592(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-584(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-600(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rcx
	movl	$48, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-592(%rbp), %r9
	movq	-584(%rbp), %rdx
	movq	%rax, %r8
	call	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	movq	-600(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -584(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdx
	movl	$57, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal21LazyBuiltinsAssembler30GenerateTailCallToReturnedCodeENS0_7Runtime10FunctionIdENS0_8compiler5TNodeINS0_10JSFunctionEEE
	movq	-584(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-608(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13404:
	.size	_ZN2v88internal21LazyBuiltinsAssembler30MaybeTailCallOptimizedCodeSlotENS0_8compiler5TNodeINS0_10JSFunctionEEENS3_INS0_14FeedbackVectorEEE, .-_ZN2v88internal21LazyBuiltinsAssembler30MaybeTailCallOptimizedCodeSlotENS0_8compiler5TNodeINS0_10JSFunctionEEENS3_INS0_14FeedbackVectorEEE
	.section	.text._ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE
	.type	_ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE, @function
_ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE:
.LFB13405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler25GetSharedFunctionInfoCodeENS0_8compiler11SloppyTNodeINS0_18SharedFunctionInfoEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21LoadFeedbackCellValueENS0_8compiler11SloppyTNodeINS0_10JSFunctionEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movl	$125, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15HasInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21LazyBuiltinsAssembler30MaybeTailCallOptimizedCodeSlotENS0_8compiler5TNodeINS0_10JSFunctionEEENS3_INS0_14FeedbackVectorEEE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$48, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %r10
	movq	%r10, %rcx
	movq	%r10, -336(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-336(%rbp), %r10
	movq	-328(%rbp), %r9
	movq	%rax, %r8
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, -64(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$54, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-336(%rbp), %r9
	movq	-328(%rbp), %rdx
	movq	%rax, %r8
	call	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13405:
	.size	_ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE, .-_ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/builtins/builtins-lazy-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"CompileLazy"
	.section	.text._ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE:
.LFB13413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$158, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$67, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L24
.L21:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L21
.L25:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13413:
	.size	_ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_CompileLazyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20CompileLazyAssembler23GenerateCompileLazyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20CompileLazyAssembler23GenerateCompileLazyImplEv
	.type	_ZN2v88internal20CompileLazyAssembler23GenerateCompileLazyImplEv, @function
_ZN2v88internal20CompileLazyAssembler23GenerateCompileLazyImplEv:
.LFB13417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21LazyBuiltinsAssembler11CompileLazyENS0_8compiler5TNodeINS0_10JSFunctionEEE
	.cfi_endproc
.LFE13417:
	.size	_ZN2v88internal20CompileLazyAssembler23GenerateCompileLazyImplEv, .-_ZN2v88internal20CompileLazyAssembler23GenerateCompileLazyImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_CompileLazyDeoptimizedCodeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"CompileLazyDeoptimizedCode"
	.section	.text._ZN2v88internal8Builtins35Generate_CompileLazyDeoptimizedCodeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_CompileLazyDeoptimizedCodeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_CompileLazyDeoptimizedCodeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_CompileLazyDeoptimizedCodeEPNS0_8compiler18CodeAssemblerStateE:
.LFB13422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$164, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$68, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L32
.L29:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$67, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$48, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %r9
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rax, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L29
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13422:
	.size	_ZN2v88internal8Builtins35Generate_CompileLazyDeoptimizedCodeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_CompileLazyDeoptimizedCodeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35CompileLazyDeoptimizedCodeAssembler38GenerateCompileLazyDeoptimizedCodeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35CompileLazyDeoptimizedCodeAssembler38GenerateCompileLazyDeoptimizedCodeImplEv
	.type	_ZN2v88internal35CompileLazyDeoptimizedCodeAssembler38GenerateCompileLazyDeoptimizedCodeImplEv, @function
_ZN2v88internal35CompileLazyDeoptimizedCodeAssembler38GenerateCompileLazyDeoptimizedCodeImplEv:
.LFB13426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$67, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$48, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r9
	movq	%r14, %rcx
	popq	%rbx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	.cfi_endproc
.LFE13426:
	.size	_ZN2v88internal35CompileLazyDeoptimizedCodeAssembler38GenerateCompileLazyDeoptimizedCodeImplEv, .-_ZN2v88internal35CompileLazyDeoptimizedCodeAssembler38GenerateCompileLazyDeoptimizedCodeImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE, @function
_GLOBAL__sub_I__ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE:
.LFB17297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17297:
	.size	_GLOBAL__sub_I__ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE, .-_GLOBAL__sub_I__ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21LazyBuiltinsAssembler24GenerateTailCallToJSCodeENS0_8compiler5TNodeINS0_4CodeEEENS3_INS0_10JSFunctionEEE
	.weak	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE:
	.byte	4
	.byte	2
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
