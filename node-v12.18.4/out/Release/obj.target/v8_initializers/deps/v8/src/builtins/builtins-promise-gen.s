	.file	"builtins-promise-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB25613:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE25613:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB25614:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L5
	cmpl	$3, %edx
	je	.L6
	cmpl	$1, %edx
	je	.L10
.L6:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25614:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS2_8compiler4NodeES6_S6_RKNS2_26TorqueStructIteratorRecordERKSt8functionIFNS4_5TNodeINS2_6ObjectEEENSB_INS2_7ContextEEENSB_INS2_3SmiEEENSB_INS2_13NativeContextEEENSB_INS2_17PromiseCapabilityEEEEESP_PNS4_18CodeAssemblerLabelEPNS4_21CodeAssemblerVariableEEUlvE_E10_M_managerERSt9_Any_dataRKSW_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS2_8compiler4NodeES6_S6_RKNS2_26TorqueStructIteratorRecordERKSt8functionIFNS4_5TNodeINS2_6ObjectEEENSB_INS2_7ContextEEENSB_INS2_3SmiEEENSB_INS2_13NativeContextEEENSB_INS2_17PromiseCapabilityEEEEESP_PNS4_18CodeAssemblerLabelEPNS4_21CodeAssemblerVariableEEUlvE_E10_M_managerERSt9_Any_dataRKSW_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS2_8compiler4NodeES6_S6_RKNS2_26TorqueStructIteratorRecordERKSt8functionIFNS4_5TNodeINS2_6ObjectEEENSB_INS2_7ContextEEENSB_INS2_3SmiEEENSB_INS2_13NativeContextEEENSB_INS2_17PromiseCapabilityEEEEESP_PNS4_18CodeAssemblerLabelEPNS4_21CodeAssemblerVariableEEUlvE_E10_M_managerERSt9_Any_dataRKSW_St18_Manager_operation:
.LFB26441:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L12
	cmpl	$3, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L17
.L13:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26441:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS2_8compiler4NodeES6_S6_RKNS2_26TorqueStructIteratorRecordERKSt8functionIFNS4_5TNodeINS2_6ObjectEEENSB_INS2_7ContextEEENSB_INS2_3SmiEEENSB_INS2_13NativeContextEEENSB_INS2_17PromiseCapabilityEEEEESP_PNS4_18CodeAssemblerLabelEPNS4_21CodeAssemblerVariableEEUlvE_E10_M_managerERSt9_Any_dataRKSW_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS2_8compiler4NodeES6_S6_RKNS2_26TorqueStructIteratorRecordERKSt8functionIFNS4_5TNodeINS2_6ObjectEEENSB_INS2_7ContextEEENSB_INS2_3SmiEEENSB_INS2_13NativeContextEEENSB_INS2_17PromiseCapabilityEEEEESP_PNS4_18CodeAssemblerLabelEPNS4_21CodeAssemblerVariableEEUlvE_E10_M_managerERSt9_Any_dataRKSW_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation:
.LFB26448:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L19
	cmpl	$3, %edx
	je	.L20
	cmpl	$1, %edx
	je	.L24
.L20:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26448:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation:
.LFB26452:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L26
	cmpl	$3, %edx
	je	.L27
	cmpl	$1, %edx
	je	.L31
.L27:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26452:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation:
.LFB26456:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L33
	cmpl	$3, %edx
	je	.L34
	cmpl	$1, %edx
	je	.L38
.L34:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26456:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation:
.LFB26460:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L40
	cmpl	$3, %edx
	je	.L41
	cmpl	$1, %edx
	je	.L45
.L41:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26460:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlS5_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlS5_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlS5_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_:
.LFB26476:
	.cfi_startproc
	endbr64
	movq	(%rcx), %rax
	ret
	.cfi_endproc
.LFE26476:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlS5_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlS5_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_6ObjectEEENS5_INS2_13NativeContextEEES7_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_6ObjectEEENS5_INS2_13NativeContextEEES7_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_6ObjectEEENS5_INS2_13NativeContextEEES7_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB26477:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L49
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26477:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_6ObjectEEENS5_INS2_13NativeContextEEES7_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_6ObjectEEENS5_INS2_13NativeContextEEES7_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB26486:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L51
	cmpl	$3, %edx
	je	.L52
	cmpl	$1, %edx
	je	.L56
.L52:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26486:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB26490:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L58
	cmpl	$3, %edx
	je	.L59
	cmpl	$1, %edx
	je	.L63
.L59:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26490:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler17PerformPromiseAllES4_S4_S4_RKNS1_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS1_6ObjectEEENSB_INS1_7ContextEEENSB_INS1_3SmiEEENSB_INS1_13NativeContextEEENSB_INS1_17PromiseCapabilityEEEEESP_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler17PerformPromiseAllES4_S4_S4_RKNS1_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS1_6ObjectEEENSB_INS1_7ContextEEENSB_INS1_3SmiEEENSB_INS1_13NativeContextEEENSB_INS1_17PromiseCapabilityEEEEESP_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler17PerformPromiseAllES4_S4_S4_RKNS1_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS1_6ObjectEEENSB_INS1_7ContextEEENSB_INS1_3SmiEEENSB_INS1_13NativeContextEEENSB_INS1_17PromiseCapabilityEEEEESP_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26440:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rsi
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r8, %rdi
	jmp	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	.cfi_endproc
.LFE26440:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler17PerformPromiseAllES4_S4_S4_RKNS1_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS1_6ObjectEEENSB_INS1_7ContextEEENSB_INS1_3SmiEEENSB_INS1_13NativeContextEEENSB_INS1_17PromiseCapabilityEEEEESP_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler17PerformPromiseAllES4_S4_S4_RKNS1_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS1_6ObjectEEENSB_INS1_7ContextEEENSB_INS1_3SmiEEENSB_INS1_13NativeContextEEENSB_INS1_17PromiseCapabilityEEEEESP_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_:
.LFB26451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%r8), %rsi
	movl	$1800, %ecx
	movl	$24, %edx
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26451:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26494:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$1800, %ecx
	movl	$8, %edx
	movq	8(%rax), %rdi
	movq	(%rax), %rsi
	jmp	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	.cfi_endproc
.LFE26494:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation:
.LFB26495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L73
	cmpl	$3, %edx
	je	.L74
	cmpl	$1, %edx
	je	.L80
.L75:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$16, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movq	%rax, (%rbx)
	movups	%xmm0, (%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L75
	movl	$16, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26495:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation
	.section	.rodata._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"rejected"
.LC2:
	.string	"status"
.LC3:
	.string	"reason"
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_:
.LFB26489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rsi), %r14
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rsi
	movq	(%rcx), %rax
	movl	$108, %edx
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	(%rbx), %rdi
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	(%rbx), %r12
	leaq	.LC1(%rip), %rsi
	movq	%rax, -136(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	movl	$786, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -160(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	movq	%r13, -64(%rbp)
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	-128(%rbp), %r13
	movq	-96(%rbp), %rax
	pushq	%r15
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r14, %r9
	movq	%r13, %rdx
	movq	-136(%rbp), %xmm0
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movq	%rax, -120(%rbp)
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	(%rbx), %r12
	leaq	.LC3(%rip), %rsi
	movl	$3, %ebx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-160(%rbp), %r11
	movl	$786, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	pushq	%r15
	movq	%r14, %r9
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movq	-136(%rbp), %xmm0
	movq	%rax, -120(%rbp)
	movq	-144(%rbp), %rax
	movhps	-152(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	movq	-136(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26489:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_
	.section	.rodata._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"fulfilled"
.LC5:
	.string	"value"
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_:
.LFB26485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rsi), %r14
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rsi
	movq	(%rcx), %rax
	movl	$108, %edx
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	(%rbx), %rdi
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	(%rbx), %r12
	leaq	.LC4(%rip), %rsi
	movq	%rax, -136(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	movl	$786, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -160(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	movq	%r13, -64(%rbp)
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	-128(%rbp), %r13
	movq	-96(%rbp), %rax
	pushq	%r15
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r14, %r9
	movq	%r13, %rdx
	movq	-136(%rbp), %xmm0
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movq	%rax, -120(%rbp)
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	(%rbx), %r12
	leaq	.LC5(%rip), %rsi
	movl	$3, %ebx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-160(%rbp), %r11
	movl	$786, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	pushq	%r15
	movq	%r14, %r9
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movq	-136(%rbp), %xmm0
	movq	%rax, -120(%rbp)
	movq	-144(%rbp), %rax
	movhps	-152(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	movq	-136(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26485:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_:
.LFB26447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rcx), %r15
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movq	(%rdx), %r14
	movl	$161, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$124, %edx
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdi
	movl	$8, %edx
	movq	%rax, %rsi
	movl	$6, %r8d
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26447:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_:
.LFB26455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rcx), %r15
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movq	(%rdx), %r14
	movl	$161, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$125, %edx
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdi
	movl	$8, %edx
	movq	%rax, %rsi
	movl	$6, %r8d
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26455:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_:
.LFB26459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rcx), %r15
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movq	(%rdx), %r14
	movl	$161, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$126, %edx
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdi
	movl	$8, %edx
	movq	%rax, %rsi
	movl	$6, %r8d
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26459:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE:
.LFB21892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$212, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$56, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$48, %esi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$29, %ecx
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$29, %ecx
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21892:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE, .-_ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE:
.LFB21893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movl	$24, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$40, %edx
	popq	%r12
	movq	%rax, %rcx
	popq	%r13
	movl	$6, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	.cfi_endproc
.LFE21893:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE, .-_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_:
.LFB21895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$176, %rsp
	movq	%rdx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler43IsPromiseHookEnabledOrHasAsyncEventDelegateEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm0
	movq	%r15, %rdx
	movq	%r12, %rdi
	movhps	-200(%rbp), %xmm0
	leaq	-64(%rbp), %rcx
	movl	$2, %r8d
	movl	$275, %esi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$176, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21895:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_, .-_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE:
.LFB21894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	.cfi_endproc
.LFE21894:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE, .-_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler23AllocateAndSetJSPromiseEPNS0_8compiler4NodeENS_7Promise12PromiseStateES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler23AllocateAndSetJSPromiseEPNS0_8compiler4NodeENS_7Promise12PromiseStateES4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler23AllocateAndSetJSPromiseEPNS0_8compiler4NodeENS_7Promise12PromiseStateES4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler23AllocateAndSetJSPromiseEPNS0_8compiler4NodeENS_7Promise12PromiseStateES4_:
.LFB21896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE
	movq	%rbx, %rcx
	movl	$24, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	-208(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$40, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler43IsPromiseHookEnabledOrHasAsyncEventDelegateEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	leaq	-80(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$2, %r8d
	movl	$275, %esi
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L108:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21896:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler23AllocateAndSetJSPromiseEPNS0_8compiler4NodeENS_7Promise12PromiseStateES4_, .-_ZN2v88internal24PromiseBuiltinsAssembler23AllocateAndSetJSPromiseEPNS0_8compiler4NodeENS_7Promise12PromiseStateES4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE:
.LFB21915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-736(%rbp), %r14
	leaq	-608(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-752(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-352(%rbp), %rbx
	subq	$760, %rsp
	movq	%rdx, -792(%rbp)
	movl	$8, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -760(%rbp)
	leaq	-480(%rbp), %rax
	leaq	-224(%rbp), %rbx
	movq	%rax, %xmm0
	movq	%rax, -768(%rbp)
	movhps	-760(%rbp), %xmm0
	movaps	%xmm0, -784(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r13, -224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-768(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-760(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-784(%rbp), %xmm0
	movq	%r13, %rdi
	movq	%rbx, -64(%rbp)
	movabsq	$4741643895889, %rdx
	movl	$1024, -84(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rdx, -92(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	leaq	-80(%rbp), %r8
	leaq	-92(%rbp), %rcx
	movq	%r15, %rdx
	movl	$3, %r9d
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-792(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	addq	$760, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L112:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21915:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE, .-_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi
	.type	_ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi, @function
_ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi:
.LFB21936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leal	16(,%rdx,8), %esi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21936:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi, .-_ZN2v88internal24PromiseBuiltinsAssembler20CreatePromiseContextEPNS0_8compiler4NodeEi
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler37CreatePromiseAllResolveElementContextEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler37CreatePromiseAllResolveElementContextEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler37CreatePromiseAllResolveElementContextEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler37CreatePromiseAllResolveElementContextEPNS0_8compiler4NodeES4_:
.LFB21937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$83, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r14, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	pushq	$0
	movq	%r15, %r8
	xorl	%r9d, %r9d
	pushq	$1
	movq	%rax, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$72, %esi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$7, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$5, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$6, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21937:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler37CreatePromiseAllResolveElementContextEPNS0_8compiler4NodeES4_, .-_ZN2v88internal24PromiseBuiltinsAssembler37CreatePromiseAllResolveElementContextEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseAllResolveElementFunctionEPNS0_8compiler4NodeENS2_5TNodeINS0_3SmiEEES4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseAllResolveElementFunctionEPNS0_8compiler4NodeENS2_5TNodeINS0_3SmiEEES4_i
	.type	_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseAllResolveElementFunctionEPNS0_8compiler4NodeENS2_5TNodeINS0_3SmiEEES4_i, @function
_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseAllResolveElementFunctionEPNS0_8compiler4NodeENS2_5TNodeINS0_3SmiEEES4_i:
.LFB21938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$161, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rcx, %rsi
	subq	$24, %rsp
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	-52(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	movl	%r8d, %edx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$8, %edx
	movq	%rax, %rsi
	movl	$6, %r8d
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21938:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseAllResolveElementFunctionEPNS0_8compiler4NodeENS2_5TNodeINS0_3SmiEEES4_i, .-_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseAllResolveElementFunctionEPNS0_8compiler4NodeENS2_5TNodeINS0_3SmiEEES4_i
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseResolvingFunctionsContextEPNS0_8compiler4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseResolvingFunctionsContextEPNS0_8compiler4NodeES4_S4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseResolvingFunctionsContextEPNS0_8compiler4NodeES4_S4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseResolvingFunctionsContextEPNS0_8compiler4NodeES4_S4_:
.LFB21939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$72, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$7, %ecx
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$5, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$6, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21939:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseResolvingFunctionsContextEPNS0_8compiler4NodeES4_S4_, .-_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseResolvingFunctionsContextEPNS0_8compiler4NodeES4_S4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_:
.LFB21897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal24PromiseBuiltinsAssembler38CreatePromiseResolvingFunctionsContextEPNS0_8compiler4NodeES4_S4_
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$161, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$119, %edx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$118, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	addq	$8, %rsp
	movq	%rax, %rdx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21897:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_, .-_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_
	.section	.text._ZN2v88internal29NewPromiseCapabilityAssembler32GenerateNewPromiseCapabilityImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29NewPromiseCapabilityAssembler32GenerateNewPromiseCapabilityImplEv
	.type	_ZN2v88internal29NewPromiseCapabilityAssembler32GenerateNewPromiseCapabilityImplEv, @function
_ZN2v88internal29NewPromiseCapabilityAssembler32GenerateNewPromiseCapabilityImplEv:
.LFB21927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-608(%rbp), %r15
	leaq	-480(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$680, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -672(%rbp)
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-352(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-224(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r11, %rdi
	movq	%r12, %rsi
	movq	%r11, -680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16IsConstructorMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movl	$212, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-680(%rbp), %r11
	movq	-688(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%r11, -704(%rbp)
	movq	%r10, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-680(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -712(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$504, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r13, %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	-696(%rbp), %r9
	movl	$8, %r8d
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	movq	-680(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$24, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-688(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-704(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$504, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movl	$4, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movl	$4, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movl	$4, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	xorl	%edx, %edx
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%rbx, %rsi
	movl	$5, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r13, %rcx
	movl	$4, %edx
	movq	%r12, %rdi
	movq	-680(%rbp), %r8
	movq	%r8, %rsi
	movq	%r8, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rsi
	movl	$117, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rsi
	movl	$161, %edx
	movq	%r12, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-688(%rbp), %r8
	movq	-680(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, -696(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-640(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler8LoadRootENS0_9RootIndexE@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rsi
	movl	$5, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	-696(%rbp), %rdx
	movl	$1, %ecx
	movq	-664(%rbp), %xmm0
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%esi, %esi
	movq	%rax, -656(%rbp)
	movq	-624(%rbp), %rax
	movq	%r12, %rdi
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	leaq	-656(%rbp), %rdx
	movaps	%xmm0, -96(%rbp)
	movq	-680(%rbp), %xmm0
	movq	%rax, -648(%rbp)
	movhps	-688(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$90, %edx
	movq	-672(%rbp), %rbx
	movq	-664(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$113, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	-704(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-712(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21927:
	.size	_ZN2v88internal29NewPromiseCapabilityAssembler32GenerateNewPromiseCapabilityImplEv, .-_ZN2v88internal29NewPromiseCapabilityAssembler32GenerateNewPromiseCapabilityImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../deps/v8/src/builtins/builtins-promise-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"NewPromiseCapability"
	.section	.text._ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE:
.LFB21923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$181, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$499, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L131
.L128:
	movq	%r13, %rdi
	call	_ZN2v88internal29NewPromiseCapabilityAssembler32GenerateNewPromiseCapabilityImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L128
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21923:
	.size	_ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_NewPromiseCapabilityEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler43CreatePromiseGetCapabilitiesExecutorContextEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler43CreatePromiseGetCapabilitiesExecutorContextEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler43CreatePromiseGetCapabilitiesExecutorContextEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler43CreatePromiseGetCapabilitiesExecutorContextEPNS0_8compiler4NodeES4_:
.LFB21940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$56, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$5, %ecx
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$4, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21940:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler43CreatePromiseGetCapabilitiesExecutorContextEPNS0_8compiler4NodeES4_, .-_ZN2v88internal24PromiseBuiltinsAssembler43CreatePromiseGetCapabilitiesExecutorContextEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE:
.LFB21941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21941:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE, .-_ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler20PromiseSetHasHandlerEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler20PromiseSetHasHandlerEPNS0_8compiler4NodeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler20PromiseSetHasHandlerEPNS0_8compiler4NodeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler20PromiseSetHasHandlerEPNS0_8compiler4NodeE:
.LFB21942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	popq	%r12
	movq	%rax, %rcx
	popq	%r13
	movl	$6, %r8d
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	.cfi_endproc
.LFE21942:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler20PromiseSetHasHandlerEPNS0_8compiler4NodeE, .-_ZN2v88internal24PromiseBuiltinsAssembler20PromiseSetHasHandlerEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler15IsPromiseStatusENS0_8compiler5TNodeINS0_7Word32TEEENS_7Promise12PromiseStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler15IsPromiseStatusENS0_8compiler5TNodeINS0_7Word32TEEENS_7Promise12PromiseStateE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler15IsPromiseStatusENS0_8compiler5TNodeINS0_7Word32TEEENS_7Promise12PromiseStateE, @function
_ZN2v88internal24PromiseBuiltinsAssembler15IsPromiseStatusENS0_8compiler5TNodeINS0_7Word32TEEENS_7Promise12PromiseStateE:
.LFB21943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	.cfi_endproc
.LFE21943:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler15IsPromiseStatusENS0_8compiler5TNodeINS0_7Word32TEEENS_7Promise12PromiseStateE, .-_ZN2v88internal24PromiseBuiltinsAssembler15IsPromiseStatusENS0_8compiler5TNodeINS0_7Word32TEEENS_7Promise12PromiseStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler13PromiseStatusEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler13PromiseStatusEPNS0_8compiler4NodeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler13PromiseStatusEPNS0_8compiler4NodeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler13PromiseStatusEPNS0_8compiler4NodeE:
.LFB21944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21944:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler13PromiseStatusEPNS0_8compiler4NodeE, .-_ZN2v88internal24PromiseBuiltinsAssembler13PromiseStatusEPNS0_8compiler4NodeE
	.section	.rodata._ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"status != v8::Promise::kPending"
	.section	.rodata._ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE, @function
_ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE:
.LFB21945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testl	%edx, %edx
	je	.L146
	movq	%rdi, %r12
	movq	%rsi, %r13
	movl	%edx, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	popq	%r12
	movq	%rax, %rcx
	popq	%r13
	movl	$6, %r8d
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21945:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE, .-_ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler21PromiseSetHandledHintEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler21PromiseSetHandledHintEPNS0_8compiler4NodeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler21PromiseSetHandledHintEPNS0_8compiler4NodeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler21PromiseSetHandledHintEPNS0_8compiler4NodeE:
.LFB21946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$8, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	popq	%r12
	movq	%rax, %rcx
	popq	%r13
	movl	$6, %r8d
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	.cfi_endproc
.LFE21946:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler21PromiseSetHandledHintEPNS0_8compiler4NodeE, .-_ZN2v88internal24PromiseBuiltinsAssembler21PromiseSetHandledHintEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_:
.LFB21947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	leaq	-1088(%rbp), %r10
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movl	$1, %r8d
	subq	$1224, %rsp
	movq	%r9, -1208(%rbp)
	movq	%rsi, -1176(%rbp)
	movq	%rdi, %rsi
	movq	%r10, %rdi
	movq	%rcx, -1160(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r10, -1152(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-960(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-832(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1144(%rbp), %r11
	movq	-1152(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%r11, -1168(%rbp)
	movq	%r10, -1144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1144(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1256(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r15, -1152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%edx, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$505, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	$8, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	-1136(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movl	$32, %edx
	movq	%r12, %rdi
	movq	-1208(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	-1160(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movl	$16, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rcx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	-1152(%rbp), %rsi
	leaq	-1104(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1168(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -1248(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1120(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-704(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-576(%rbp), %rdx
	movq	%rdx, %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r11, -1200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1192(%rbp), %r14
	movq	-1200(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1192(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	leaq	-192(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler40PromiseFulfillReactionJobTaskMapConstantEv@PLT
	movq	%r15, %rdi
	movq	%r15, -1184(%rbp)
	leaq	-320(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1160(%rbp), %rsi
	movq	-1168(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1160(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1176(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler39PromiseRejectReactionJobTaskMapConstantEv@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1168(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1176(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1160(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1152(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE
	movq	-1144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %r9
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	-1176(%rbp), %rdx
	movq	%r9, %rcx
	movl	$1, %r8d
	movl	$280, %esi
	movq	%r9, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rbx, -1152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1168(%rbp), %rdi
	movq	%rax, -1240(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, -1232(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%edx, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	-1176(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	%rbx, %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	-1240(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	movq	-1232(%rbp), %r9
	movl	$8, %r8d
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	-1160(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movl	$32, %edx
	movq	%r12, %rdi
	movq	-1208(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$156, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	movl	$1, %ebx
	xorl	%esi, %esi
	movq	-1224(%rbp), %rcx
	pushq	%rbx
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-1160(%rbp), %r9
	movq	-176(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rdx, -320(%rbp)
	movq	%r15, %rdx
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1216(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1144(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1200(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1192(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1168(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1184(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler20PromiseSetHasHandlerEPNS0_8compiler4NodeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1256(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L152:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21947:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_, .-_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_
	.section	.rodata._ZN2v88internal8Builtins27Generate_PerformPromiseThenEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"PerformPromiseThen"
	.section	.text._ZN2v88internal8Builtins27Generate_PerformPromiseThenEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_PerformPromiseThenEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_PerformPromiseThenEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_PerformPromiseThenEPNS0_8compiler18CodeAssemblerStateE:
.LFB21952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$491, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$504, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L157
.L154:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-88(%rbp), %r8
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%rax, %r13
	movq	%rax, %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L154
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21952:
	.size	_ZN2v88internal8Builtins27Generate_PerformPromiseThenEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_PerformPromiseThenEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal27PerformPromiseThenAssembler30GeneratePerformPromiseThenImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27PerformPromiseThenAssembler30GeneratePerformPromiseThenImplEv
	.type	_ZN2v88internal27PerformPromiseThenAssembler30GeneratePerformPromiseThenImplEv, @function
_ZN2v88internal27PerformPromiseThenAssembler30GeneratePerformPromiseThenImplEv:
.LFB21956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-56(%rbp), %r8
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%rax, %r9
	movq	%rax, %r13
	call	_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21956:
	.size	_ZN2v88internal27PerformPromiseThenAssembler30GeneratePerformPromiseThenImplEv, .-_ZN2v88internal27PerformPromiseThenAssembler30GeneratePerformPromiseThenImplEv
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler23AllocatePromiseReactionEPNS0_8compiler4NodeES4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler23AllocatePromiseReactionEPNS0_8compiler4NodeES4_S4_S4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler23AllocatePromiseReactionEPNS0_8compiler4NodeES4_S4_S4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler23AllocatePromiseReactionEPNS0_8compiler4NodeES4_S4_S4_:
.LFB21957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movl	$40, %esi
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r13, %rdi
	movl	$505, %edx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	-56(%rbp), %r9
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$8, %edx
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21957:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler23AllocatePromiseReactionEPNS0_8compiler4NodeES4_S4_S4_, .-_ZN2v88internal24PromiseBuiltinsAssembler23AllocatePromiseReactionEPNS0_8compiler4NodeES4_S4_S4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler30AllocatePromiseReactionJobTaskEPNS0_8compiler4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler30AllocatePromiseReactionJobTaskEPNS0_8compiler4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler30AllocatePromiseReactionJobTaskEPNS0_8compiler4NodeES4_S4_S4_S4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler30AllocatePromiseReactionJobTaskEPNS0_8compiler4NodeES4_S4_S4_S4_:
.LFB21958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$24, %rsp
	movq	%rsi, -64(%rbp)
	movl	$40, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	-64(%rbp), %r10
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21958:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler30AllocatePromiseReactionJobTaskEPNS0_8compiler4NodeES4_S4_S4_S4_, .-_ZN2v88internal24PromiseBuiltinsAssembler30AllocatePromiseReactionJobTaskEPNS0_8compiler4NodeES4_S4_S4_S4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler37AllocatePromiseResolveThenableJobTaskEPNS0_8compiler4NodeES4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler37AllocatePromiseResolveThenableJobTaskEPNS0_8compiler4NodeES4_S4_S4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler37AllocatePromiseResolveThenableJobTaskEPNS0_8compiler4NodeES4_S4_S4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler37AllocatePromiseResolveThenableJobTaskEPNS0_8compiler4NodeES4_S4_S4_:
.LFB21959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$40, %esi
	subq	$24, %rsp
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r13, %rdi
	movl	$525, %edx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$8, %r8d
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21959:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler37AllocatePromiseResolveThenableJobTaskEPNS0_8compiler4NodeES4_S4_S4_, .-_ZN2v88internal24PromiseBuiltinsAssembler37AllocatePromiseResolveThenableJobTaskEPNS0_8compiler4NodeES4_S4_S4_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE:
.LFB21960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r14
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-592(%rbp), %r13
	pushq	%r12
	movq	%r13, %xmm0
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-464(%rbp), %rbx
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	subq	$664, %rsp
	movl	%r8d, -648(%rbp)
	movq	%rsi, -688(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rcx, -696(%rbp)
	movq	%r9, %rcx
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movl	$8, %edx
	movaps	%xmm0, -672(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	%rbx, -640(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler26PromiseReactionMapConstantEv@PLT
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-672(%rbp), %xmm0
	movq	%rax, -632(%rbp)
	leaq	-80(%rbp), %rax
	movl	$1, %r8d
	movq	%rax, %rcx
	movq	%rax, -672(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-632(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9FastCheckENS0_8compiler5TNodeINS0_5BoolTEEE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-640(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$8, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%rbx, %rsi
	movq	-640(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	movq	%rbx, -640(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	leaq	-624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-672(%rbp), %rcx
	movl	$1, %r8d
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-640(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	movq	%rbx, -656(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-640(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	-648(%rbp), %edx
	movl	$1800, %ecx
	testl	%edx, %edx
	jne	.L168
	movl	$24, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rax, %r9
.L169:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r9, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-632(%rbp), %rdx
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	movq	-632(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-688(%rbp), %rsi
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-704(%rbp), %r9
	movq	-632(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal24PromiseBuiltinsAssembler21ExtractHandlerContextEPNS0_8compiler4NodeEPNS2_21CodeAssemblerVariableE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jne	.L170
	movl	$523, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movl	$8, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-696(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-632(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
.L171:
	movq	-632(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$156, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-672(%rbp), %rsi
	movl	$1, %edi
	movq	%r14, %rdx
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	movq	-648(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	movq	-192(%rbp), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-640(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L174
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rax, %r9
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$524, %edx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%rbx, %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	-696(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-632(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$24, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-680(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	jmp	.L171
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21960:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE, .-_ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE:
.LFB21962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r15
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-544(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r8, -592(%rbp)
	movq	%r9, -568(%rbp)
	movq	%rax, -576(%rbp)
	movq	%rsi, -552(%rbp)
	movhps	-592(%rbp), %xmm1
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rcx, -560(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movaps	%xmm1, -592(%rbp)
	leaq	-352(%rbp), %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-512(%rbp), %r11
	movl	$1, %r8d
	movq	%r13, -512(%rbp)
	movq	%r11, %rcx
	movq	%r11, -600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-600(%rbp), %r11
	movl	$510, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	movq	%r11, -624(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rdx
	movl	$2, %edi
	xorl	%esi, %esi
	pushq	%rdi
	leaq	-528(%rbp), %r10
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rdx
	movdqa	-592(%rbp), %xmm1
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	-552(%rbp), %r9
	movq	-496(%rbp), %rax
	movq	%rcx, -528(%rbp)
	movl	$1, %ecx
	movq	%rdx, -608(%rbp)
	movq	%r10, %rdx
	movq	%r10, -616(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-576(%rbp), %rcx
	movq	-568(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-600(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-624(%rbp), %r11
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-608(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdi
	movq	-616(%rbp), %r10
	movq	%rdi, -528(%rbp)
	movl	$4, %edi
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rdi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	-560(%rbp), %xmm0
	movdqa	-592(%rbp), %xmm1
	movq	-552(%rbp), %r9
	pushq	%rsi
	xorl	%esi, %esi
	movq	-496(%rbp), %rax
	movhps	-600(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-576(%rbp), %rcx
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	-568(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-552(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21962:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, .-_ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES9_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES9_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES9_, @function
_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES9_:
.LFB21966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r8, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler19GotoIfForceSlowPathEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$212, %edx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler36IsPromiseResolveProtectorCellInvalidEv@PLT
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	.cfi_endproc
.LFE21966:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES9_, .-_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES9_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE:
.LFB21967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19GotoIfForceSlowPathEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$212, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler36IsPromiseResolveProtectorCellInvalidEv@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21967:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_, @function
_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_:
.LFB21968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$127, %edx
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler19GotoIfForceSlowPathEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler36IsPromiseSpeciesProtectorCellInvalidEv@PLT
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	.cfi_endproc
.LFE21968:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_, .-_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_, @function
_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_:
.LFB21969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r8, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler19GotoIfForceSlowPathEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14IsJSPromiseMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$127, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler33IsPromiseThenProtectorCellInvalidEv@PLT
	addq	$8, %rsp
	movq	%r15, %rcx
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	.cfi_endproc
.LFE21969:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_, .-_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler25BranchIfAccessCheckFailedENS0_8compiler11SloppyTNodeINS0_7ContextEEES5_PNS2_4NodeES7_PNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler25BranchIfAccessCheckFailedENS0_8compiler11SloppyTNodeINS0_7ContextEEES5_PNS2_4NodeES7_PNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler25BranchIfAccessCheckFailedENS0_8compiler11SloppyTNodeINS0_7ContextEEES5_PNS2_4NodeES7_PNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal24PromiseBuiltinsAssembler25BranchIfAccessCheckFailedENS0_8compiler11SloppyTNodeINS0_7ContextEEES5_PNS2_4NodeES7_PNS2_18CodeAssemblerLabelE:
.LFB21970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	leaq	-448(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-592(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 3, -56
	movq	%r9, -624(%rbp)
	movq	%rcx, -616(%rbp)
	movq	%rsi, -656(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rdx, -648(%rbp)
	movl	$8, %edx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-576(%rbp), %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-600(%rbp), %rcx
	movl	$1, %r8d
	movq	%r13, -600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	$1105, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-632(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-640(%rbp), %r8
	movl	$1104, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-632(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -640(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-648(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	-656(%rbp), %r10
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -632(%rbp)
	leaq	-64(%rbp), %rcx
	movq	-616(%rbp), %rax
	movl	$141, %esi
	movq	%r10, %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-632(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-624(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L192:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21970:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler25BranchIfAccessCheckFailedENS0_8compiler11SloppyTNodeINS0_7ContextEEES5_PNS2_4NodeES7_PNS2_18CodeAssemblerLabelE, .-_ZN2v88internal24PromiseBuiltinsAssembler25BranchIfAccessCheckFailedENS0_8compiler11SloppyTNodeINS0_7ContextEEES5_PNS2_4NodeES7_PNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE, @function
_ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE:
.LFB21971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3784(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -264(%rbp)
	je	.L197
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	%r12, %rdi
	movl	$3, %ebx
	movq	%rax, -272(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	leaq	-240(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rcx
	pushq	%rbx
	movq	%r14, %r9
	pushq	%rcx
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	leaq	-256(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	movq	-272(%rbp), %xmm0
	movq	%rax, -256(%rbp)
	movq	-224(%rbp), %rax
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L197:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21971:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE, .-_ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE, @function
_ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE:
.LFB21972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdi, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	movl	$1, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	subq	$232, %rsp
	movq	%rdx, -264(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-264(%rbp), %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1074, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15HasInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, 16(%r14)
	je	.L203
	movq	%r14, %rdi
	call	*24(%r14)
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3792(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	leaq	-240(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%esi, %esi
	movl	$3, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	movq	%r12, %rdi
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-264(%rbp), %xmm0
	leaq	-256(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -256(%rbp)
	movq	-224(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -248(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L203:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21972:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE, .-_ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE
	.section	.text._ZN2v88internal39PromiseCapabilityDefaultRejectAssembler42GeneratePromiseCapabilityDefaultRejectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39PromiseCapabilityDefaultRejectAssembler42GeneratePromiseCapabilityDefaultRejectImplEv
	.type	_ZN2v88internal39PromiseCapabilityDefaultRejectAssembler42GeneratePromiseCapabilityDefaultRejectImplEv, @function
_ZN2v88internal39PromiseCapabilityDefaultRejectAssembler42GeneratePromiseCapabilityDefaultRejectImplEv:
.LFB21981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6IsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movl	$6, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$494, %edx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, -48(%rbp)
	movl	$3, %edi
	xorl	%esi, %esi
	leaq	-64(%rbp), %r15
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%r15
	movq	%r13, %r9
	leaq	-240(%rbp), %rdx
	movq	%r12, %rdi
	movq	-264(%rbp), %xmm0
	movq	%rax, -240(%rbp)
	movl	$1, %ecx
	movq	-208(%rbp), %rax
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -256(%rbp)
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdx
	movq	%r15, %rcx
	movl	$284, %esi
	movdqa	-256(%rbp), %xmm0
	movl	$2, %r8d
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L208:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21981:
	.size	_ZN2v88internal39PromiseCapabilityDefaultRejectAssembler42GeneratePromiseCapabilityDefaultRejectImplEv, .-_ZN2v88internal39PromiseCapabilityDefaultRejectAssembler42GeneratePromiseCapabilityDefaultRejectImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_PromiseCapabilityDefaultRejectEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"PromiseCapabilityDefaultReject"
	.section	.text._ZN2v88internal8Builtins39Generate_PromiseCapabilityDefaultRejectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_PromiseCapabilityDefaultRejectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_PromiseCapabilityDefaultRejectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_PromiseCapabilityDefaultRejectEPNS0_8compiler18CodeAssemblerStateE:
.LFB21977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$886, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$496, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L213
.L210:
	movq	%r13, %rdi
	call	_ZN2v88internal39PromiseCapabilityDefaultRejectAssembler42GeneratePromiseCapabilityDefaultRejectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L210
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21977:
	.size	_ZN2v88internal8Builtins39Generate_PromiseCapabilityDefaultRejectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_PromiseCapabilityDefaultRejectEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40PromiseCapabilityDefaultResolveAssembler43GeneratePromiseCapabilityDefaultResolveImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40PromiseCapabilityDefaultResolveAssembler43GeneratePromiseCapabilityDefaultResolveImplEv
	.type	_ZN2v88internal40PromiseCapabilityDefaultResolveAssembler43GeneratePromiseCapabilityDefaultResolveImplEv, @function
_ZN2v88internal40PromiseCapabilityDefaultResolveAssembler43GeneratePromiseCapabilityDefaultResolveImplEv:
.LFB21990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-64(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6IsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$495, %edx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r13, %r9
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%r15
	leaq	-240(%rbp), %rdx
	movq	%r12, %rdi
	movq	-264(%rbp), %xmm0
	movq	%rax, -240(%rbp)
	movq	-208(%rbp), %rax
	movhps	-256(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -256(%rbp)
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdx
	movq	%r15, %rcx
	movl	$285, %esi
	movdqa	-256(%rbp), %xmm0
	movl	$2, %r8d
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21990:
	.size	_ZN2v88internal40PromiseCapabilityDefaultResolveAssembler43GeneratePromiseCapabilityDefaultResolveImplEv, .-_ZN2v88internal40PromiseCapabilityDefaultResolveAssembler43GeneratePromiseCapabilityDefaultResolveImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_PromiseCapabilityDefaultResolveEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"PromiseCapabilityDefaultResolve"
	.section	.text._ZN2v88internal8Builtins40Generate_PromiseCapabilityDefaultResolveEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_PromiseCapabilityDefaultResolveEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_PromiseCapabilityDefaultResolveEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_PromiseCapabilityDefaultResolveEPNS0_8compiler18CodeAssemblerStateE:
.LFB21986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$920, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$497, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L223
.L220:
	movq	%r13, %rdi
	call	_ZN2v88internal40PromiseCapabilityDefaultResolveAssembler43GeneratePromiseCapabilityDefaultResolveImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L220
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21986:
	.size	_ZN2v88internal8Builtins40Generate_PromiseCapabilityDefaultResolveEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_PromiseCapabilityDefaultResolveEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal48PromiseConstructorLazyDeoptContinuationAssembler51GeneratePromiseConstructorLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal48PromiseConstructorLazyDeoptContinuationAssembler51GeneratePromiseConstructorLazyDeoptContinuationImplEv
	.type	_ZN2v88internal48PromiseConstructorLazyDeoptContinuationAssembler51GeneratePromiseConstructorLazyDeoptContinuationImplEv, @function
_ZN2v88internal48PromiseConstructorLazyDeoptContinuationAssembler51GeneratePromiseConstructorLazyDeoptContinuationImplEv:
.LFB21999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IsTheHoleENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1, %edx
	leaq	-256(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-256(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm1
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$4, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	movq	%r12, %rdi
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-280(%rbp), %xmm0
	leaq	-272(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movhps	-296(%rbp), %xmm0
	movq	%rax, -272(%rbp)
	movq	-240(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-288(%rbp), %xmm0
	movq	%rax, -264(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L228:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21999:
	.size	_ZN2v88internal48PromiseConstructorLazyDeoptContinuationAssembler51GeneratePromiseConstructorLazyDeoptContinuationImplEv, .-_ZN2v88internal48PromiseConstructorLazyDeoptContinuationAssembler51GeneratePromiseConstructorLazyDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins48Generate_PromiseConstructorLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"PromiseConstructorLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins48Generate_PromiseConstructorLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins48Generate_PromiseConstructorLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins48Generate_PromiseConstructorLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins48Generate_PromiseConstructorLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB21995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$951, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$500, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L233
.L230:
	movq	%r13, %rdi
	call	_ZN2v88internal48PromiseConstructorLazyDeoptContinuationAssembler51GeneratePromiseConstructorLazyDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L230
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21995:
	.size	_ZN2v88internal8Builtins48Generate_PromiseConstructorLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins48Generate_PromiseConstructorLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal27PromiseConstructorAssembler30GeneratePromiseConstructorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27PromiseConstructorAssembler30GeneratePromiseConstructorImplEv
	.type	_ZN2v88internal27PromiseConstructorAssembler30GeneratePromiseConstructorImplEv, @function
_ZN2v88internal27PromiseConstructorAssembler30GeneratePromiseConstructorImplEv:
.LFB22008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1376(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1592, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1504(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	-1248(%rbp), %r14
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, -1600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1512(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler13IsCallableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r14, -1608(%rbp)
	movq	%rax, %rsi
	leaq	-736(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$212, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1480(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-1120(%rbp), %rcx
	movq	%rax, -1496(%rbp)
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -1520(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-992(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r9, -1528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -1536(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-608(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %r9
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	-1480(%rbp), %r11
	movq	-1512(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -1568(%rbp)
	leaq	-1472(%rbp), %rbx
	movq	%r11, %rdx
	movq	%r11, -1584(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler25BranchIfAccessCheckFailedENS0_8compiler11SloppyTNodeINS0_7ContextEEES5_PNS2_4NodeES7_PNS2_18CodeAssemblerLabelE
	movq	-1504(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1528(%rbp), %rcx
	movq	-1520(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1456(%rbp), %r10
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -1632(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1440(%rbp), %rsi
	movl	$8, %edx
	movq	%rsi, %r10
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -1544(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1528(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	-1504(%rbp), %rcx
	call	_ZN2v88internal28ConstructorBuiltinsAssembler17EmitFastNewObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_10JSReceiverEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, %rsi
	call	_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler43IsPromiseHookEnabledOrHasAsyncEventDelegateEv@PLT
	movq	-1480(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	leaq	-96(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r10, %rcx
	movl	$275, %esi
	movq	%r15, -96(%rbp)
	movl	$2, %r8d
	movq	%r10, -1488(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-1480(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1480(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1536(%rbp), %r15
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movl	$75, %esi
	movq	%r12, %rdi
	movq	-1488(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r15, -1536(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-480(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-352(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -1576(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1584(%rbp), %r11
	movq	-1576(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rcx
	call	_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_
	movq	%r12, %rdi
	movq	%rdx, -1576(%rbp)
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	leaq	-1408(%rbp), %r11
	movq	-1560(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r11, -1624(%rbp)
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1408(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$5, %edi
	movl	$1, %ecx
	movq	-1512(%rbp), %xmm0
	movq	-1576(%rbp), %r9
	movq	-1488(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movhps	-1592(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%r9, -64(%rbp)
	leaq	-1424(%rbp), %r9
	movaps	%xmm0, -96(%rbp)
	movq	%r9, %r10
	movq	%r13, %r9
	movq	-1584(%rbp), %xmm0
	movq	%rax, -1424(%rbp)
	movq	-1392(%rbp), %rax
	movq	%r10, %rdx
	movhps	-1616(%rbp), %xmm0
	movq	%r10, -1616(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1544(%rbp), %rcx
	movq	-1552(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-1496(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1544(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-1624(%rbp), %r11
	movq	-1560(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -1592(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1408(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1488(%rbp), %rsi
	movl	$4, %edi
	movq	%r13, %r9
	movq	%rax, %r8
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-1576(%rbp), %xmm0
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -1424(%rbp)
	movq	-1616(%rbp), %rdx
	movhps	-1560(%rbp), %xmm0
	movq	-1392(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-1592(%rbp), %xmm0
	movq	%rax, -1416(%rbp)
	movhps	-1584(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	$73, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1552(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$89, %edx
	movq	-1504(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	-1608(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$158, %edx
	movq	-1512(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$38, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdx
	movl	$148, %esi
	movq	%r12, %rdi
	movq	-1488(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1544(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1632(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1536(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1520(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L238:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22008:
	.size	_ZN2v88internal27PromiseConstructorAssembler30GeneratePromiseConstructorImplEv, .-_ZN2v88internal27PromiseConstructorAssembler30GeneratePromiseConstructorImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_PromiseConstructorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"PromiseConstructor"
	.section	.text._ZN2v88internal8Builtins27Generate_PromiseConstructorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_PromiseConstructorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_PromiseConstructorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_PromiseConstructorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$969, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$501, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L243
.L240:
	movq	%r13, %rdi
	call	_ZN2v88internal27PromiseConstructorAssembler30GeneratePromiseConstructorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L240
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22004:
	.size	_ZN2v88internal8Builtins27Generate_PromiseConstructorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_PromiseConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins35Generate_PromiseInternalConstructorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"PromiseInternalConstructor"
	.section	.text._ZN2v88internal8Builtins35Generate_PromiseInternalConstructorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_PromiseInternalConstructorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_PromiseInternalConstructorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_PromiseInternalConstructorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1082, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$523, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L249
.L246:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L246
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22016:
	.size	_ZN2v88internal8Builtins35Generate_PromiseInternalConstructorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_PromiseInternalConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal35PromiseInternalConstructorAssembler38GeneratePromiseInternalConstructorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35PromiseInternalConstructorAssembler38GeneratePromiseInternalConstructorImplEv
	.type	_ZN2v88internal35PromiseInternalConstructorAssembler38GeneratePromiseInternalConstructorImplEv, @function
_ZN2v88internal35PromiseInternalConstructorAssembler38GeneratePromiseInternalConstructorImplEv:
.LFB22020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22020:
	.size	_ZN2v88internal35PromiseInternalConstructorAssembler38GeneratePromiseInternalConstructorImplEv, .-_ZN2v88internal35PromiseInternalConstructorAssembler38GeneratePromiseInternalConstructorImplEv
	.section	.text._ZN2v88internal30PromiseInternalRejectAssembler33GeneratePromiseInternalRejectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PromiseInternalRejectAssembler33GeneratePromiseInternalRejectImplEv
	.type	_ZN2v88internal30PromiseInternalRejectAssembler33GeneratePromiseInternalRejectImplEv, @function
_ZN2v88internal30PromiseInternalRejectAssembler33GeneratePromiseInternalRejectImplEv:
.LFB22029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-272(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$494, %edx
	leaq	-240(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, -64(%rbp)
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	leaq	-80(%rbp), %r14
	movl	$3, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%r14
	movhps	-264(%rbp), %xmm0
	leaq	-256(%rbp), %rdx
	movq	%rax, -256(%rbp)
	movq	-224(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$30, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	%r14, %rcx
	movl	$348, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L256:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22029:
	.size	_ZN2v88internal30PromiseInternalRejectAssembler33GeneratePromiseInternalRejectImplEv, .-_ZN2v88internal30PromiseInternalRejectAssembler33GeneratePromiseInternalRejectImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_PromiseInternalRejectEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"PromiseInternalReject"
	.section	.text._ZN2v88internal8Builtins30Generate_PromiseInternalRejectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_PromiseInternalRejectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_PromiseInternalRejectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_PromiseInternalRejectEPNS0_8compiler18CodeAssemblerStateE:
.LFB22025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1089, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$524, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L261
.L258:
	movq	%r13, %rdi
	call	_ZN2v88internal30PromiseInternalRejectAssembler33GeneratePromiseInternalRejectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L258
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22025:
	.size	_ZN2v88internal8Builtins30Generate_PromiseInternalRejectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_PromiseInternalRejectEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31PromiseInternalResolveAssembler34GeneratePromiseInternalResolveImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31PromiseInternalResolveAssembler34GeneratePromiseInternalResolveImplEv
	.type	_ZN2v88internal31PromiseInternalResolveAssembler34GeneratePromiseInternalResolveImplEv, @function
_ZN2v88internal31PromiseInternalResolveAssembler34GeneratePromiseInternalResolveImplEv:
.LFB22038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-272(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-80(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$495, %edx
	leaq	-240(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r15, %r9
	xorl	%esi, %esi
	movl	$2, %ebx
	movq	%rax, %r8
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-264(%rbp), %xmm0
	leaq	-256(%rbp), %rdx
	pushq	%r14
	movq	%rax, -256(%rbp)
	movq	-224(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$30, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	%r14, %rcx
	movl	$348, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L266:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22038:
	.size	_ZN2v88internal31PromiseInternalResolveAssembler34GeneratePromiseInternalResolveImplEv, .-_ZN2v88internal31PromiseInternalResolveAssembler34GeneratePromiseInternalResolveImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_PromiseInternalResolveEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"PromiseInternalResolve"
	.section	.text._ZN2v88internal8Builtins31Generate_PromiseInternalResolveEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_PromiseInternalResolveEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_PromiseInternalResolveEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_PromiseInternalResolveEPNS0_8compiler18CodeAssemblerStateE:
.LFB22034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1109, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$525, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L271
.L268:
	movq	%r13, %rdi
	call	_ZN2v88internal31PromiseInternalResolveAssembler34GeneratePromiseInternalResolveImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L268
.L272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22034:
	.size	_ZN2v88internal8Builtins31Generate_PromiseInternalResolveEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_PromiseInternalResolveEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv.str1.1,"aMS",@progbits,1
.LC18:
	.string	"Promise.prototype.then"
	.section	.text._ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv
	.type	_ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv, @function
_ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv:
.LFB22047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1240, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -1248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, -1208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdx
	movl	$1074, %ecx
	movq	%r12, %rdi
	movq	%rbx, -1176(%rbp)
	leaq	.LC18(%rip), %r8
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler22ThrowIfNotInstanceTypeEPNS0_8compiler4NodeES4_NS0_12InstanceTypeEPKc@PLT
	leaq	-1104(%rbp), %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-976(%rbp), %rcx
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-848(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r9
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, -1224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$212, %edx
	movq	%r12, %rdi
	movq	%rbx, %r15
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-1176(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-1216(%rbp), %r8
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-1176(%rbp), %rdx
	leaq	-720(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler18SpeciesConstructorENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSReceiverEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1224(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1168(%rbp), %r9
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, -1184(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1152(%rbp), %r11
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -1192(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1240(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1200(%rbp), %r15
	movq	-1176(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	movq	-1192(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1184(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-336(%rbp), %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1224(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-208(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1200(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$499, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r15, %r9
	xorl	%esi, %esi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rax, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rcx, -336(%rbp)
	movl	$2, %ebx
	leaq	-80(%rbp), %rcx
	movq	-192(%rbp), %rax
	pushq	%rbx
	movhps	-1200(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -1200(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1192(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-592(%rbp), %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1232(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1232(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1248(%rbp), %r10
	movl	$8, %edx
	movq	%r12, %rsi
	leaq	-1136(%rbp), %r11
	leaq	-464(%rbp), %rbx
	movq	%r11, %rdi
	movq	%r10, %rcx
	movq	%r11, -1256(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1248(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1248(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-1256(%rbp), %r11
	movq	%rax, %rsi
	movq	%r11, %rdi
	movq	%r11, -1272(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-1120(%rbp), %r10
	movl	$8, %edx
	movq	%r12, %rsi
	movq	-1208(%rbp), %rcx
	movq	%r10, %rdi
	movq	%r10, -1248(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-1248(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -1208(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1192(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1208(%rbp), %r10
	movq	%rax, -1264(%rbp)
	movq	%r10, %rdi
	movq	%r10, -1256(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1272(%rbp), %r11
	movq	%rax, -1248(%rbp)
	movq	%r11, %rdi
	movq	%r11, -1208(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1176(%rbp), %rdx
	movq	%r12, %rdi
	movq	-1264(%rbp), %r9
	movq	-1248(%rbp), %r8
	movq	-1200(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	-1184(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1256(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1208(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1192(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1184(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L276:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22047:
	.size	_ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv, .-_ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_PromisePrototypeThenEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"PromisePrototypeThen"
	.section	.text._ZN2v88internal8Builtins29Generate_PromisePrototypeThenEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_PromisePrototypeThenEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_PromisePrototypeThenEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_PromisePrototypeThenEPNS0_8compiler18CodeAssemblerStateE:
.LFB22043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1128, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$503, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L281
.L278:
	movq	%r13, %rdi
	call	_ZN2v88internal29PromisePrototypeThenAssembler32GeneratePromisePrototypeThenImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L278
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22043:
	.size	_ZN2v88internal8Builtins29Generate_PromisePrototypeThenEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_PromisePrototypeThenEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal34PromiseResolveThenableJobAssembler37GeneratePromiseResolveThenableJobImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34PromiseResolveThenableJobAssembler37GeneratePromiseResolveThenableJobImplEv
	.type	_ZN2v88internal34PromiseResolveThenableJobAssembler37GeneratePromiseResolveThenableJobImplEv, @function
_ZN2v88internal34PromiseResolveThenableJobAssembler37GeneratePromiseResolveThenableJobImplEv:
.LFB22065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$240, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -584(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler14IsJSPromiseMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -568(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$504, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-512(%rbp), %r10
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%r12, %rdi
	movq	-552(%rbp), %xmm0
	movq	%rax, %rdx
	movq	-208(%rbp), %rax
	movq	%r10, %rsi
	movl	$4, %r9d
	movq	%rcx, -512(%rbp)
	movq	%rbx, %rcx
	movhps	-568(%rbp), %xmm0
	movq	%rax, -504(%rbp)
	leaq	-96(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %r8
	movq	-576(%rbp), %xmm0
	movq	%r10, -592(%rbp)
	movhps	-560(%rbp), %xmm0
	movq	%rax, -568(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	-560(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler31CreatePromiseResolvingFunctionsEPNS0_8compiler4NodeES4_S4_
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rdx, -576(%rbp)
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	leaq	-544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-592(%rbp), %r10
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-568(%rbp), %rsi
	movq	%rbx, %r9
	movq	-584(%rbp), %xmm0
	movl	$5, %edi
	movq	-576(%rbp), %rdx
	movq	%rax, %r8
	leaq	-528(%rbp), %r11
	movhps	-592(%rbp), %xmm0
	pushq	%rdi
	movq	-496(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movq	-552(%rbp), %xmm0
	movq	%rcx, -528(%rbp)
	movl	$1, %ecx
	movhps	-600(%rbp), %xmm0
	movq	%rdx, -64(%rbp)
	movq	%r11, %rdx
	movq	%r11, -600(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-560(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-560(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-608(%rbp), %r10
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-568(%rbp), %rsi
	movq	%rbx, %r9
	movq	-576(%rbp), %xmm0
	movl	$4, %edi
	movq	-600(%rbp), %r11
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movhps	-592(%rbp), %xmm0
	pushq	%rdi
	movq	-496(%rbp), %rax
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r11, %rdx
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movq	-584(%rbp), %xmm0
	movq	%rcx, -528(%rbp)
	movl	$1, %ecx
	movhps	-552(%rbp), %xmm0
	movq	%rax, -520(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-560(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L286:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22065:
	.size	_ZN2v88internal34PromiseResolveThenableJobAssembler37GeneratePromiseResolveThenableJobImplEv, .-_ZN2v88internal34PromiseResolveThenableJobAssembler37GeneratePromiseResolveThenableJobImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_PromiseResolveThenableJobEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"PromiseResolveThenableJob"
	.section	.text._ZN2v88internal8Builtins34Generate_PromiseResolveThenableJobEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_PromiseResolveThenableJobEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_PromiseResolveThenableJobEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_PromiseResolveThenableJobEPNS0_8compiler18CodeAssemblerStateE:
.LFB22061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1234, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$508, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L291
.L288:
	movq	%r13, %rdi
	call	_ZN2v88internal34PromiseResolveThenableJobAssembler37GeneratePromiseResolveThenableJobImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L288
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22061:
	.size	_ZN2v88internal8Builtins34Generate_PromiseResolveThenableJobEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_PromiseResolveThenableJobEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE, @function
_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE:
.LFB22066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1072(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-736(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movq	%rdx, %rcx
	movl	$8, %edx
	subq	$1144, %rsp
	movl	%r9d, -1136(%rbp)
	movq	%rsi, -1088(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%r8, -1080(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1112(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-864(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1096(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-608(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	-1136(%rbp), %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r13, -1104(%rbp)
	testl	%ecx, %ecx
	cmove	-1096(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1112(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	xorl	%esi, %esi
	movhps	-1144(%rbp), %xmm0
	movl	$4, %edi
	movq	%r15, %xmm1
	movq	%rax, %r8
	pushq	%rdi
	movq	-1088(%rbp), %r9
	leaq	-352(%rbp), %r15
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	pushq	%rbx
	movq	-208(%rbp), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-1128(%rbp), %xmm0
	movq	%rcx, -352(%rbp)
	movl	$1, %ecx
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -344(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1104(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1128(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1096(%rbp), %rcx
	movq	-1120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19IsPromiseCapabilityENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-480(%rbp), %r10
	movl	$495, %edx
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -1168(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rbx, %r8
	movq	-1080(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	-1024(%rbp), %rdi
	movq	-464(%rbp), %rax
	movq	%rcx, -1024(%rbp)
	movq	-1088(%rbp), %rcx
	movhps	-1128(%rbp), %xmm0
	movl	$2, %r9d
	movq	%rdi, %rsi
	movq	%rdi, -1144(%rbp)
	movq	%r12, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -1016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1080(%rbp), %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1168(%rbp), %r10
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -1176(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-480(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	movq	-1088(%rbp), %r9
	xorl	%esi, %esi
	movq	%rax, %r8
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-1152(%rbp), %xmm0
	pushq	%rbx
	movq	%r12, %rdi
	movq	%rax, -1024(%rbp)
	movq	-1144(%rbp), %rdx
	movhps	-1168(%rbp), %xmm0
	movq	-464(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-1160(%rbp), %xmm0
	movq	%rax, -1016(%rbp)
	movhps	-1128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	-1104(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpl	$1, -1136(%rbp)
	movq	-1176(%rbp), %r10
	je	.L300
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$506, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-1128(%rbp), %xmm0
	movq	-1088(%rbp), %rcx
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movl	$3, %r9d
	movhps	-1136(%rbp), %xmm0
	movq	%rax, -344(%rbp)
	movq	-1080(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L296:
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	%r10, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19IsPromiseCapabilityENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1136(%rbp), %r10
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1136(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$494, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1136(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-1080(%rbp), %xmm0
	movq	-1144(%rbp), %rsi
	movq	%rax, -1024(%rbp)
	movq	-208(%rbp), %rax
	movl	$3, %r9d
	movq	%rcx, -80(%rbp)
	movq	-1088(%rbp), %rcx
	movhps	-1128(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -1016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	leaq	-1056(%rbp), %r11
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%rax, %rcx
	movq	%r11, -1152(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	-1080(%rbp), %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -1080(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1144(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1024(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	xorl	%esi, %esi
	movq	-1080(%rbp), %xmm0
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movhps	-1144(%rbp), %xmm0
	pushq	%rbx
	movq	-1088(%rbp), %r9
	leaq	-1040(%rbp), %rdx
	movaps	%xmm0, -96(%rbp)
	movq	%r12, %rdi
	movq	-1136(%rbp), %xmm0
	movq	%rax, -1040(%rbp)
	movq	-1008(%rbp), %rax
	movhps	-1128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -1032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1152(%rbp), %r11
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1080(%rbp)
	movq	%r11, %rcx
	movq	%r11, -1128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1128(%rbp), %r11
	movq	%r11, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1080(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movl	$155, %esi
	movq	%r12, %rdi
	movq	-1080(%rbp), %rcx
	movq	%rax, %rdx
	movl	$1, %r9d
	movq	%rcx, -96(%rbp)
	movq	-1088(%rbp), %rcx
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-1128(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1160(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L296
.L301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22066:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE, .-_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE
	.section	.rodata._ZN2v88internal8Builtins34Generate_PromiseFulfillReactionJobEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"PromiseFulfillReactionJob"
	.section	.text._ZN2v88internal8Builtins34Generate_PromiseFulfillReactionJobEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_PromiseFulfillReactionJobEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_PromiseFulfillReactionJobEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_PromiseFulfillReactionJobEPNS0_8compiler18CodeAssemblerStateE:
.LFB22071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1435, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$507, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L306
.L303:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rax, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L307
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L303
.L307:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22071:
	.size	_ZN2v88internal8Builtins34Generate_PromiseFulfillReactionJobEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_PromiseFulfillReactionJobEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal34PromiseFulfillReactionJobAssembler37GeneratePromiseFulfillReactionJobImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34PromiseFulfillReactionJobAssembler37GeneratePromiseFulfillReactionJobImplEv
	.type	_ZN2v88internal34PromiseFulfillReactionJobAssembler37GeneratePromiseFulfillReactionJobImplEv, @function
_ZN2v88internal34PromiseFulfillReactionJobAssembler37GeneratePromiseFulfillReactionJobImplEv:
.LFB22075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	xorl	%r9d, %r9d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE
	.cfi_endproc
.LFE22075:
	.size	_ZN2v88internal34PromiseFulfillReactionJobAssembler37GeneratePromiseFulfillReactionJobImplEv, .-_ZN2v88internal34PromiseFulfillReactionJobAssembler37GeneratePromiseFulfillReactionJobImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_PromiseRejectReactionJobEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"PromiseRejectReactionJob"
	.section	.text._ZN2v88internal8Builtins33Generate_PromiseRejectReactionJobEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_PromiseRejectReactionJobEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_PromiseRejectReactionJobEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_PromiseRejectReactionJobEPNS0_8compiler18CodeAssemblerStateE:
.LFB22080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1447, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$506, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L314
.L311:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %r8
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L311
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22080:
	.size	_ZN2v88internal8Builtins33Generate_PromiseRejectReactionJobEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_PromiseRejectReactionJobEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33PromiseRejectReactionJobAssembler36GeneratePromiseRejectReactionJobImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PromiseRejectReactionJobAssembler36GeneratePromiseRejectReactionJobImplEv
	.type	_ZN2v88internal33PromiseRejectReactionJobAssembler36GeneratePromiseRejectReactionJobImplEv, @function
_ZN2v88internal33PromiseRejectReactionJobAssembler36GeneratePromiseRejectReactionJobImplEv:
.LFB22084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	movl	$1, %r9d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal24PromiseBuiltinsAssembler18PromiseReactionJobEPNS0_8compiler4NodeES4_S4_S4_NS0_15PromiseReaction4TypeE
	.cfi_endproc
.LFE22084:
	.size	_ZN2v88internal33PromiseRejectReactionJobAssembler36GeneratePromiseRejectReactionJobImplEv, .-_ZN2v88internal33PromiseRejectReactionJobAssembler36GeneratePromiseRejectReactionJobImplEv
	.section	.rodata._ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv.str1.1,"aMS",@progbits,1
.LC23:
	.string	"PromiseResolve"
	.section	.text._ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv
	.type	_ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv, @function
_ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv:
.LFB22093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$26, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	.LC23(%rip), %r8
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler20ThrowIfNotJSReceiverENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_15MessageTemplateEPKc@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$510, %edx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-64(%rbp), %rcx
	xorl	%esi, %esi
	movl	$2, %ebx
	movq	%rax, %r8
	movhps	-120(%rbp), %xmm0
	movq	%r13, %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L321:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22093:
	.size	_ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv, .-_ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_PromiseResolveTrampolineEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"PromiseResolveTrampoline"
	.section	.text._ZN2v88internal8Builtins33Generate_PromiseResolveTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_PromiseResolveTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_PromiseResolveTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_PromiseResolveTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB22089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1458, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$509, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L326
.L323:
	movq	%r13, %rdi
	call	_ZN2v88internal33PromiseResolveTrampolineAssembler36GeneratePromiseResolveTrampolineImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L327
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L323
.L327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22089:
	.size	_ZN2v88internal8Builtins33Generate_PromiseResolveTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_PromiseResolveTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23PromiseResolveAssembler26GeneratePromiseResolveImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23PromiseResolveAssembler26GeneratePromiseResolveImplEv
	.type	_ZN2v88internal23PromiseResolveAssembler26GeneratePromiseResolveImplEv, @function
_ZN2v88internal23PromiseResolveAssembler26GeneratePromiseResolveImplEv:
.LFB22102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-608(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$680, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$212, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -664(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler14IsJSPromiseMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$127, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-224(%rbp), %rbx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler36IsPromiseSpeciesProtectorCellInvalidEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-680(%rbp), %r10
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	2304(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -680(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rdi
	movq	%r15, %xmm0
	xorl	%esi, %esi
	movhps	-680(%rbp), %xmm0
	movq	%rdi, %r11
	movq	%rdi, -680(%rbp)
	movl	$2, %edi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%r11
	movq	-688(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movq	%r15, -664(%rbp)
	leaq	-352(%rbp), %r15
	movq	%r15, %rdx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %r10
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-688(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -704(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeES4_
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-640(%rbp), %r10
	movl	$495, %edx
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -696(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-680(%rbp), %rsi
	movl	$2, %edi
	movq	-688(%rbp), %xmm0
	pushq	%rdi
	movq	-704(%rbp), %r9
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rsi
	leaq	-656(%rbp), %r11
	xorl	%esi, %esi
	movq	-624(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movhps	-664(%rbp), %xmm0
	movq	%r11, %rdx
	movq	%r9, -712(%rbp)
	movq	%rcx, -656(%rbp)
	movl	$1, %ecx
	movq	%r11, -704(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-688(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-696(%rbp), %rdi
	movl	$499, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-680(%rbp), %rsi
	movl	$2, %edi
	movq	-704(%rbp), %r11
	pushq	%rdi
	movq	-712(%rbp), %r9
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rsi
	movq	-624(%rbp), %rax
	movq	%r11, %rdx
	xorl	%esi, %esi
	movq	-672(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%r9, -720(%rbp)
	movq	%rcx, -656(%rbp)
	movl	$1, %ecx
	movhps	-688(%rbp), %xmm0
	movq	%r11, -712(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-696(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-680(%rbp), %rsi
	movq	-672(%rbp), %xmm0
	movl	$4, %edi
	movq	-712(%rbp), %r11
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movhps	-696(%rbp), %xmm0
	pushq	%rsi
	xorl	%esi, %esi
	movq	-720(%rbp), %r9
	movaps	%xmm0, -96(%rbp)
	movq	%r11, %rdx
	movq	%r12, %rdi
	movq	-688(%rbp), %xmm0
	movq	-624(%rbp), %rax
	movq	%rcx, -656(%rbp)
	movl	$1, %ecx
	movhps	-664(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-704(%rbp), %r10
	addq	$32, %rsp
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L331:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22102:
	.size	_ZN2v88internal23PromiseResolveAssembler26GeneratePromiseResolveImplEv, .-_ZN2v88internal23PromiseResolveAssembler26GeneratePromiseResolveImplEv
	.section	.text._ZN2v88internal8Builtins23Generate_PromiseResolveEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_PromiseResolveEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_PromiseResolveEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_PromiseResolveEPNS0_8compiler18CodeAssemblerStateE:
.LFB22098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1472, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$510, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L336
.L333:
	movq	%r13, %rdi
	call	_ZN2v88internal23PromiseResolveAssembler26GeneratePromiseResolveImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L333
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22098:
	.size	_ZN2v88internal8Builtins23Generate_PromiseResolveEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_PromiseResolveEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal39PromiseGetCapabilitiesExecutorAssembler42GeneratePromiseGetCapabilitiesExecutorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39PromiseGetCapabilitiesExecutorAssembler42GeneratePromiseGetCapabilitiesExecutorImplEv
	.type	_ZN2v88internal39PromiseGetCapabilitiesExecutorAssembler42GeneratePromiseGetCapabilitiesExecutorImplEv, @function
_ZN2v88internal39PromiseGetCapabilitiesExecutorAssembler42GeneratePromiseGetCapabilitiesExecutorImplEv:
.LFB22111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-200(%rbp), %r9
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%rbx, %rcx
	movl	$24, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$112, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L341
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L341:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22111:
	.size	_ZN2v88internal39PromiseGetCapabilitiesExecutorAssembler42GeneratePromiseGetCapabilitiesExecutorImplEv, .-_ZN2v88internal39PromiseGetCapabilitiesExecutorAssembler42GeneratePromiseGetCapabilitiesExecutorImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_PromiseGetCapabilitiesExecutorEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"PromiseGetCapabilitiesExecutor"
	.section	.text._ZN2v88internal8Builtins39Generate_PromiseGetCapabilitiesExecutorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_PromiseGetCapabilitiesExecutorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_PromiseGetCapabilitiesExecutorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_PromiseGetCapabilitiesExecutorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1550, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$498, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L346
.L343:
	movq	%r13, %rdi
	call	_ZN2v88internal39PromiseGetCapabilitiesExecutorAssembler42GeneratePromiseGetCapabilitiesExecutorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L343
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22107:
	.size	_ZN2v88internal8Builtins39Generate_PromiseGetCapabilitiesExecutorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_PromiseGetCapabilitiesExecutorEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv.str1.1,"aMS",@progbits,1
.LC26:
	.string	"PromiseReject"
	.section	.text._ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv
	.type	_ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv, @function
_ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv:
.LFB22120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdx
	movl	$26, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	.LC26(%rip), %r8
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler20ThrowIfNotJSReceiverENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_15MessageTemplateEPKc@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$212, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-408(%rbp), %rcx
	call	_ZN2v88internal24PromiseBuiltinsAssembler23AllocateAndSetJSPromiseEPNS0_8compiler4NodeENS_7Promise12PromiseStateES4_
	movq	%r15, %rdx
	movl	$279, %esi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	%rax, %xmm0
	leaq	-96(%rbp), %rax
	movl	$2, %r8d
	movhps	-408(%rbp), %xmm0
	movq	%rax, %rcx
	movq	%rax, -416(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-424(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-384(%rbp), %r11
	movl	$499, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -448(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-416(%rbp), %rsi
	movq	%r15, %r9
	movl	$2, %edi
	pushq	%rdi
	movq	%rax, %r8
	movq	%rbx, %xmm0
	movq	-368(%rbp), %rax
	pushq	%rsi
	leaq	-400(%rbp), %rbx
	xorl	%esi, %esi
	movl	$1, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movhps	-424(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -400(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -440(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-448(%rbp), %r11
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	movq	%r15, %r9
	movq	%rbx, %rdx
	movq	-416(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movl	$1, %ecx
	movq	-424(%rbp), %xmm0
	movq	-368(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movhps	-448(%rbp), %xmm0
	movq	%r10, -400(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-440(%rbp), %xmm0
	movq	%rax, -392(%rbp)
	movhps	-408(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-432(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L351:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22120:
	.size	_ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv, .-_ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv
	.section	.text._ZN2v88internal8Builtins22Generate_PromiseRejectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_PromiseRejectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_PromiseRejectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_PromiseRejectEPNS0_8compiler18CodeAssemblerStateE:
.LFB22116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1575, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$511, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L356
.L353:
	movq	%r13, %rdi
	call	_ZN2v88internal22PromiseRejectAssembler25GeneratePromiseRejectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L357
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L353
.L357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22116:
	.size	_ZN2v88internal8Builtins22Generate_PromiseRejectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_PromiseRejectEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler29CreatePromiseFinallyFunctionsEPNS0_8compiler4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler29CreatePromiseFinallyFunctionsEPNS0_8compiler4NodeES4_S4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler29CreatePromiseFinallyFunctionsEPNS0_8compiler4NodeES4_S4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler29CreatePromiseFinallyFunctionsEPNS0_8compiler4NodeES4_S4_:
.LFB22121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$64, %esi
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$6, %ecx
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$5, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$161, %edx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$120, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$121, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	addq	$8, %rsp
	movq	%rax, %rdx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22121:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler29CreatePromiseFinallyFunctionsEPNS0_8compiler4NodeES4_S4_, .-_ZN2v88internal24PromiseBuiltinsAssembler29CreatePromiseFinallyFunctionsEPNS0_8compiler4NodeES4_S4_
	.section	.rodata._ZN2v88internal8Builtins33Generate_PromiseValueThunkFinallyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"PromiseValueThunkFinally"
	.section	.text._ZN2v88internal8Builtins33Generate_PromiseValueThunkFinallyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_PromiseValueThunkFinallyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_PromiseValueThunkFinallyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_PromiseValueThunkFinallyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1643, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$515, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L364
.L361:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L365
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L361
.L365:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22126:
	.size	_ZN2v88internal8Builtins33Generate_PromiseValueThunkFinallyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_PromiseValueThunkFinallyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33PromiseValueThunkFinallyAssembler36GeneratePromiseValueThunkFinallyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33PromiseValueThunkFinallyAssembler36GeneratePromiseValueThunkFinallyImplEv
	.type	_ZN2v88internal33PromiseValueThunkFinallyAssembler36GeneratePromiseValueThunkFinallyImplEv, @function
_ZN2v88internal33PromiseValueThunkFinallyAssembler36GeneratePromiseValueThunkFinallyImplEv:
.LFB22130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22130:
	.size	_ZN2v88internal33PromiseValueThunkFinallyAssembler36GeneratePromiseValueThunkFinallyImplEv, .-_ZN2v88internal33PromiseValueThunkFinallyAssembler36GeneratePromiseValueThunkFinallyImplEv
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler24CreateValueThunkFunctionEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler24CreateValueThunkFunctionEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler24CreateValueThunkFunctionEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler24CreateValueThunkFunctionEPNS0_8compiler4NodeES4_:
.LFB22131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$56, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$5, %ecx
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$161, %edx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$122, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	.cfi_endproc
.LFE22131:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler24CreateValueThunkFunctionEPNS0_8compiler4NodeES4_, .-_ZN2v88internal24PromiseBuiltinsAssembler24CreateValueThunkFunctionEPNS0_8compiler4NodeES4_
	.section	.rodata._ZN2v88internal8Builtins30Generate_PromiseThrowerFinallyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"PromiseThrowerFinally"
	.section	.text._ZN2v88internal8Builtins30Generate_PromiseThrowerFinallyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_PromiseThrowerFinallyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_PromiseThrowerFinallyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_PromiseThrowerFinallyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1703, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$516, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L374
.L371:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	leaq	-32(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$160, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L371
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22145:
	.size	_ZN2v88internal8Builtins30Generate_PromiseThrowerFinallyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_PromiseThrowerFinallyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30PromiseThrowerFinallyAssembler33GeneratePromiseThrowerFinallyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PromiseThrowerFinallyAssembler33GeneratePromiseThrowerFinallyImplEv
	.type	_ZN2v88internal30PromiseThrowerFinallyAssembler33GeneratePromiseThrowerFinallyImplEv, @function
_ZN2v88internal30PromiseThrowerFinallyAssembler33GeneratePromiseThrowerFinallyImplEv:
.LFB22149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	leaq	-32(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$160, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L379
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L379:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22149:
	.size	_ZN2v88internal30PromiseThrowerFinallyAssembler33GeneratePromiseThrowerFinallyImplEv, .-_ZN2v88internal30PromiseThrowerFinallyAssembler33GeneratePromiseThrowerFinallyImplEv
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler21CreateThrowerFunctionEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler21CreateThrowerFunctionEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler21CreateThrowerFunctionEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal24PromiseBuiltinsAssembler21CreateThrowerFunctionEPNS0_8compiler4NodeES4_:
.LFB22150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$56, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$5, %ecx
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler25InitializeFunctionContextEPNS0_8compiler4NodeES4_i@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4, %edx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$161, %edx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$123, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler33AllocateFunctionWithMapAndContextEPNS0_8compiler4NodeES4_S4_@PLT
	.cfi_endproc
.LFE22150:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler21CreateThrowerFunctionEPNS0_8compiler4NodeES4_, .-_ZN2v88internal24PromiseBuiltinsAssembler21CreateThrowerFunctionEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal23FulfillPromiseAssembler26GenerateFulfillPromiseImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23FulfillPromiseAssembler26GenerateFulfillPromiseImplEv
	.type	_ZN2v88internal23FulfillPromiseAssembler26GenerateFulfillPromiseImplEv, @function
_ZN2v88internal23FulfillPromiseAssembler26GenerateFulfillPromiseImplEv:
.LFB22177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	xorl	%r8d, %r8d
	call	_ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22177:
	.size	_ZN2v88internal23FulfillPromiseAssembler26GenerateFulfillPromiseImplEv, .-_ZN2v88internal23FulfillPromiseAssembler26GenerateFulfillPromiseImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_FulfillPromiseEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"FulfillPromise"
	.section	.text._ZN2v88internal8Builtins23Generate_FulfillPromiseEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_FulfillPromiseEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_FulfillPromiseEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_FulfillPromiseEPNS0_8compiler18CodeAssemblerStateE:
.LFB22173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1842, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$493, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L388
.L385:
	movq	%r13, %rdi
	call	_ZN2v88internal23FulfillPromiseAssembler26GenerateFulfillPromiseImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L389
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L385
.L389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22173:
	.size	_ZN2v88internal8Builtins23Generate_FulfillPromiseEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_FulfillPromiseEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal22RejectPromiseAssembler25GenerateRejectPromiseImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22RejectPromiseAssembler25GenerateRejectPromiseImplEv
	.type	_ZN2v88internal22RejectPromiseAssembler25GenerateRejectPromiseImplEv, @function
_ZN2v88internal22RejectPromiseAssembler25GenerateRejectPromiseImplEv:
.LFB22186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler17PromiseHasHandlerEPNS0_8compiler4NodeE
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rcx
	movl	$24, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler16PromiseSetStatusEPNS0_8compiler4NodeENS_7Promise12PromiseStateE
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-216(%rbp), %r10
	movl	$1, %r8d
	movq	%r10, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler23TriggerPromiseReactionsEPNS0_8compiler4NodeES4_S4_NS0_15PromiseReaction4TypeE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %xmm1
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-224(%rbp), %r9
	movq	%rbx, %xmm0
	movq	%rax, %rdx
	leaq	-80(%rbp), %r8
	punpcklqdq	%xmm1, %xmm0
	movl	$282, %esi
	movq	%r9, -64(%rbp)
	movl	$3, %r9d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L393
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L393:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22186:
	.size	_ZN2v88internal22RejectPromiseAssembler25GenerateRejectPromiseImplEv, .-_ZN2v88internal22RejectPromiseAssembler25GenerateRejectPromiseImplEv
	.section	.rodata._ZN2v88internal8Builtins22Generate_RejectPromiseEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"RejectPromise"
	.section	.text._ZN2v88internal8Builtins22Generate_RejectPromiseEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_RejectPromiseEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_RejectPromiseEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_RejectPromiseEPNS0_8compiler18CodeAssemblerStateE:
.LFB22182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1868, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC30(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$494, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L398
.L395:
	movq	%r13, %rdi
	call	_ZN2v88internal22RejectPromiseAssembler25GenerateRejectPromiseImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L395
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22182:
	.size	_ZN2v88internal8Builtins22Generate_RejectPromiseEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_RejectPromiseEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23ResolvePromiseAssembler26GenerateResolvePromiseImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ResolvePromiseAssembler26GenerateResolvePromiseImplEv
	.type	_ZN2v88internal23ResolvePromiseAssembler26GenerateResolvePromiseImplEv, @function
_ZN2v88internal23ResolvePromiseAssembler26GenerateResolvePromiseImplEv:
.LFB22195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-592(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-848(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1072(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-976(%rbp), %rcx
	movq	%rax, -1064(%rbp)
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -1080(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-720(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -1160(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1056(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r14
	movl	$8, %edx
	movq	%r14, %rdi
	movq	%r14, -1112(%rbp)
	leaq	-208(%rbp), %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1040(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r9
	movl	$8, %edx
	movq	%r9, %rdi
	movq	%r9, -1088(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1072(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r15, -1152(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1104(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler15IsJSReceiverMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	leaq	-464(%rbp), %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-336(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1128(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler19GotoIfForceSlowPathEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler33IsPromiseThenProtectorCellInvalidEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14IsJSPromiseMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-1128(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$127, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-1136(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1120(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$240, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$2, %ebx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3408(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%rax, %rsi
	leaq	-1008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	pushq	%rbx
	leaq	-1024(%rbp), %r11
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	-992(%rbp), %rax
	movq	%r11, %rdx
	movq	%r12, %rdi
	movq	-1104(%rbp), %xmm0
	movq	-1064(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%r11, -1176(%rbp)
	movq	%rax, -1016(%rbp)
	leaq	-80(%rbp), %rax
	pushq	%rax
	movhps	-1144(%rbp), %xmm0
	movq	%rcx, -1024(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1160(%rbp), %r10
	movq	-1112(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler13IsCallableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1080(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1080(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1088(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%edx, %edx
	movl	$40, %esi
	movq	%r12, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8AllocateEiNS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$525, %edx
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeENS0_9RootIndexE@PLT
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movl	$8, %edx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	movq	-1072(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	-1160(%rbp), %r9
	movl	$8, %r8d
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movl	$32, %edx
	movq	%r12, %rdi
	movq	-1104(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1136(%rbp), %rdi
	movl	$156, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -80(%rbp)
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-1176(%rbp), %r11
	movq	%rax, %rdx
	movq	-1144(%rbp), %rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	-992(%rbp), %rax
	movl	$1, %r9d
	movq	%r10, -1024(%rbp)
	movq	%rbx, %r8
	movq	%r11, %rsi
	movq	%r11, -1144(%rbp)
	movq	%rax, -1016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1136(%rbp), %rdi
	movl	$493, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1144(%rbp), %r11
	movq	%rbx, %r8
	movq	-1072(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movq	-992(%rbp), %rax
	movq	-1064(%rbp), %rcx
	movq	%r11, %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r12, %rdi
	movq	%r11, -1160(%rbp)
	movhps	-1104(%rbp), %xmm0
	movq	%r10, -1024(%rbp)
	movq	%rax, -1016(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-1152(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rcx
	movl	$283, %esi
	movq	%r12, %rdi
	movdqa	-1104(%rbp), %xmm0
	movq	-1064(%rbp), %rdx
	movl	$2, %r8d
	movq	%rbx, -1144(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1168(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	-1112(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1104(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1136(%rbp), %rdi
	movl	$494, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1160(%rbp), %r11
	movq	%r12, %rdi
	movq	-1072(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$3, %r9d
	movq	%rbx, -64(%rbp)
	movq	-992(%rbp), %rax
	movq	-1144(%rbp), %r8
	movq	-1064(%rbp), %rcx
	movq	%r11, %rsi
	movhps	-1104(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%rax, -1016(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r10, -1024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1088(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1112(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1152(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L403:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22195:
	.size	_ZN2v88internal23ResolvePromiseAssembler26GenerateResolvePromiseImplEv, .-_ZN2v88internal23ResolvePromiseAssembler26GenerateResolvePromiseImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_ResolvePromiseEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"ResolvePromise"
	.section	.text._ZN2v88internal8Builtins23Generate_ResolvePromiseEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_ResolvePromiseEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_ResolvePromiseEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_ResolvePromiseEPNS0_8compiler18CodeAssemblerStateE:
.LFB22191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1914, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$495, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L408
.L405:
	movq	%r13, %rdi
	call	_ZN2v88internal23ResolvePromiseAssembler26GenerateResolvePromiseImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L405
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22191:
	.size	_ZN2v88internal8Builtins23Generate_ResolvePromiseEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_ResolvePromiseEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"resolve"
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE:
.LFB22199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1240, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r8, -1264(%rbp)
	movq	%r9, -1272(%rbp)
	movq	(%rdi), %rsi
	movq	%rax, -1280(%rbp)
	movq	24(%rbp), %rax
	movq	%rdx, -1216(%rbp)
	movq	%rax, -1176(%rbp)
	movq	32(%rbp), %rax
	movq	%rcx, -1160(%rbp)
	movq	%rax, -1136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1184(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r15, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %rbx
	leaq	-96(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	movq	%rax, %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, -1168(%rbp)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%r15, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L411
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L411:
	movq	-1160(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal24PromiseBuiltinsAssembler37CreatePromiseAllResolveElementContextEPNS0_8compiler4NodeES4_
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	leaq	-1072(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r15
	movq	%rax, %rcx
	movl	$6, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-992(%rbp), %rcx
	movl	$1, %edx
	movq	%r15, -1192(%rbp)
	movq	%r15, -224(%rbp)
	movq	%rcx, %r15
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-864(%rbp), %rcx
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -1240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-736(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r11, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-608(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -1152(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -1224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	leaq	-1056(%rbp), %r11
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%rax, %rcx
	movq	%r11, -1200(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	-1224(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-1216(%rbp), %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3192(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movl	$1, %ecx
	movq	-1168(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	-208(%rbp), %rax
	movq	-1216(%rbp), %xmm0
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%r9, -352(%rbp)
	leaq	-352(%rbp), %r9
	movq	%r9, %r10
	movhps	-1144(%rbp), %xmm0
	movq	%r9, -1144(%rbp)
	movq	%rbx, %r9
	movq	%r10, %rdx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1136(%rbp), %rcx
	movq	-1152(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC32(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler18ThrowIfNotCallableENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movq	-1200(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1208(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$80, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-1136(%rbp), %r15
	movq	%rbx, %rsi
	movq	-1240(%rbp), %rcx
	movq	-1264(%rbp), %rdx
	movq	-1184(%rbp), %rdi
	movq	%rax, %r9
	movq	%rax, %r14
	pushq	%r15
	movl	$1, %r8d
	pushq	-1176(%rbp)
	call	_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE@PLT
	addq	$24, %rsp
	movq	%r14, %r8
	movq	%rbx, %rsi
	pushq	%r15
	movq	-1176(%rbp), %r9
	movl	$1, %ecx
	movq	%rax, %rdx
	movq	-1184(%rbp), %rdi
	call	_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	movq	-1192(%rbp), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$2097151, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -1248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1232(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -1248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-1192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1128(%rbp), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1256(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -1248(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	movl	$4, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	movq	-1128(%rbp), %rax
	popq	%rsi
	movq	%r14, -1024(%rbp)
	movq	%rbx, -352(%rbp)
	popq	%rdi
	movq	%rax, -1040(%rbp)
	movq	-1160(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-1272(%rbp), %rax
	cmpq	$0, 16(%rax)
	je	.L413
	movq	%rax, %r11
	leaq	-1040(%rbp), %rdi
	leaq	-1024(%rbp), %rax
	movq	%r13, %r8
	movq	%rdi, -1248(%rbp)
	movq	%rdi, %rsi
	movq	%rax, %rdx
	movq	%r11, %rdi
	movq	%rax, -1256(%rbp)
	movq	-1144(%rbp), %rcx
	call	*24(%r11)
	movq	-1280(%rbp), %r10
	movq	-1128(%rbp), %r11
	movq	%r14, -1112(%rbp)
	movq	-1160(%rbp), %r9
	movq	%rax, -1272(%rbp)
	cmpq	$0, 16(%r10)
	movq	%r11, -1120(%rbp)
	movq	%rbx, -1104(%rbp)
	movq	%r9, -1096(%rbp)
	je	.L413
	movq	%r10, %rdi
	leaq	-1104(%rbp), %rcx
	leaq	-1112(%rbp), %rdx
	leaq	-1120(%rbp), %rsi
	leaq	-1096(%rbp), %r8
	call	*24(%r10)
	movq	-1144(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1200(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler36IsPromiseSpeciesProtectorCellInvalidEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-1144(%rbp), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r14, %r8
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-1272(%rbp), %rcx
	movq	%rax, %r9
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler18PerformPromiseThenEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	-1208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1200(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%r15, %r8
	movq	%rbx, %rsi
	pushq	-1136(%rbp)
	movq	-1152(%rbp), %r9
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-1216(%rbp), %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3408(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1280(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1256(%rbp), %rdi
	movl	$710, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1168(%rbp), %rsi
	movq	%rbx, %r9
	movl	$2, %edi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-1216(%rbp), %xmm0
	pushq	%rsi
	movq	-1008(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	-1248(%rbp), %rdx
	movhps	-1280(%rbp), %xmm0
	movq	%rcx, -1040(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -1032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1136(%rbp), %rcx
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	-1152(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1256(%rbp), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1024(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %xmm0
	movq	%rbx, %r9
	xorl	%esi, %esi
	movq	-1168(%rbp), %r15
	movl	$5, %edi
	movq	%rax, %r8
	movl	$1, %ecx
	movhps	-1280(%rbp), %xmm0
	pushq	%rdi
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%r15
	movq	-1248(%rbp), %rdx
	movaps	%xmm0, -96(%rbp)
	movq	-1216(%rbp), %xmm0
	movq	%rax, -1040(%rbp)
	movq	-1008(%rbp), %rax
	movhps	-1272(%rbp), %xmm0
	movq	%r14, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -1032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1152(%rbp), %rdx
	movq	-1136(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %xmm2
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS2_8compiler4NodeES6_S6_RKNS2_26TorqueStructIteratorRecordERKSt8functionIFNS4_5TNodeINS2_6ObjectEEENSB_INS2_7ContextEEENSB_INS2_3SmiEEENSB_INS2_13NativeContextEEENSB_INS2_17PromiseCapabilityEEEEESP_PNS4_18CodeAssemblerLabelEPNS4_21CodeAssemblerVariableEEUlvE_E10_M_managerERSt9_Any_dataRKSW_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	-1160(%rbp), %xmm0
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler17PerformPromiseAllES4_S4_S4_RKNS1_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS1_6ObjectEEENSB_INS1_7ContextEEENSB_INS1_3SmiEEENSB_INS1_13NativeContextEEENSB_INS1_17PromiseCapabilityEEEEESP_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	movq	%r14, %rcx
	movq	%r15, %r8
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE
	movq	-80(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L414
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L414:
	movq	-1208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1144(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$310, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movl	$171, %esi
	movq	-1168(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-1136(%rbp), %r14
	movq	-1152(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1176(%rbp), %rcx
	movq	%r14, %r8
	movq	%rbx, %rsi
	movq	-1264(%rbp), %rdx
	movq	-1184(%rbp), %rdi
	movq	%r14, -1136(%rbp)
	call	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r15, -1144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1128(%rbp), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-1128(%rbp), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler33StoreContextElementNoWriteBarrierENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1128(%rbp), %rsi
	movl	$6, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	leaq	8(%r12), %rdi
	movq	%rax, %rsi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1264(%rbp)
	call	_ZN2v88internal17CodeStubAssembler24LoadFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	movq	-1192(%rbp), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1216(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	pushq	$0
	movq	%r12, %rdi
	movl	$2, %ecx
	movq	-1264(%rbp), %r11
	pushq	$0
	movq	%rax, %r9
	movl	$2, %esi
	pushq	$0
	movq	-1216(%rbp), %r8
	pushq	$4
	movq	%r11, %rdx
	pushq	%r14
	pushq	%r15
	call	_ZN2v88internal17CodeStubAssembler22CopyFixedArrayElementsENS0_12ElementsKindEPNS0_8compiler4NodeES2_S5_S5_S5_S5_NS0_16WriteBarrierModeENS1_13ParameterModeENS1_18HoleConversionModeEPNS3_26TypedCodeAssemblerVariableINS0_5BoolTEEE@PLT
	movq	-1272(%rbp), %r10
	addq	$48, %rsp
	movq	%r12, %rdi
	movq	-1216(%rbp), %r8
	movl	$16, %edx
	movq	%r10, %rsi
	movq	%r8, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1144(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1160(%rbp), %r14
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1128(%rbp), %rsi
	movl	$6, %edx
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1256(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1024(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1168(%rbp), %rsi
	movl	$4, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	movq	-1144(%rbp), %xmm0
	pushq	%rsi
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	%rax, -1040(%rbp)
	movq	-1248(%rbp), %rdx
	movhps	-1216(%rbp), %xmm0
	movq	-1008(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-1128(%rbp), %xmm0
	movq	%rax, -1032(%rbp)
	movhps	-1160(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1136(%rbp), %rcx
	movq	-1176(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movl	$1800, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1152(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1208(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1192(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1184(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L423
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L413:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22199:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, .-_ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.section	.rodata._ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_.str1.1,"aMS",@progbits,1
.LC33:
	.string	"Promise.all"
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_, @function
_ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_:
.LFB22207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	leaq	-320(%rbp), %r10
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-304(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$344, %rsp
	movq	%r9, -360(%rbp)
	movq	(%rdi), %rsi
	movq	%r10, %rdi
	movq	%rcx, -344(%rbp)
	movq	%r8, -352(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$26, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	leaq	.LC33(%rip), %r8
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20ThrowIfNotJSReceiverENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_15MessageTemplateEPKc@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$499, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rdi
	movq	%r15, %xmm0
	xorl	%esi, %esi
	movq	%rdi, %r10
	movq	%rdi, -368(%rbp)
	movq	%rax, %r8
	movq	%rbx, %r9
	movl	$2, %edi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-256(%rbp), %r11
	movl	$1, %ecx
	pushq	%rdi
	movhps	-328(%rbp), %xmm0
	movq	%r11, %rdx
	movq	%r12, %rdi
	pushq	%r10
	movq	%rax, -256(%rbp)
	movq	-208(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%r11, -328(%rbp)
	movq	%r15, -336(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-328(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, -256(%rbp)
	movq	%r11, %rcx
	movq	%r11, -384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	-376(%rbp), %r10
	movq	-344(%rbp), %rdx
	movq	%r10, %rdi
	call	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	movq	%r13, (%rsp)
	movq	%r15, %rcx
	movq	%rbx, %rsi
	pushq	%r14
	movq	-352(%rbp), %r9
	movq	%r12, %rdi
	leaq	-288(%rbp), %r8
	pushq	-360(%rbp)
	movq	%rdx, -280(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler17PerformPromiseAllEPNS0_8compiler4NodeES4_S4_RKNS0_26TorqueStructIteratorRecordERKSt8functionIFNS2_5TNodeINS0_6ObjectEEENS9_INS0_7ContextEEENS9_INS0_3SmiEEENS9_INS0_13NativeContextEEENS9_INS0_17PromiseCapabilityEEEEESN_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, -336(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-384(%rbp), %r11
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-256(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-368(%rbp), %rsi
	movq	%rbx, %r9
	movq	-336(%rbp), %xmm0
	movl	$4, %edi
	movq	%rax, %r8
	movl	$1, %ecx
	movhps	-352(%rbp), %xmm0
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-272(%rbp), %rdx
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movq	-344(%rbp), %xmm0
	movq	%rax, -272(%rbp)
	movq	-240(%rbp), %rax
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-376(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L427:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22207:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_, .-_ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_
	.section	.text._ZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEv
	.type	_ZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEv, @function
_ZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEv:
.LFB22216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation(%rip), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_(%rip), %rax
	movq	%r12, -96(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	movq	%r12, -128(%rbp)
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_19PromiseAllAssembler22GeneratePromiseAllImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -80(%rbp)
	movq	%rbx, %xmm0
	leaq	-128(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, %r8
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L429
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L429:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L428
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L428:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L439
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L439:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22216:
	.size	_ZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEv, .-_ZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_PromiseAllEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"PromiseAll"
	.section	.text._ZN2v88internal8Builtins19Generate_PromiseAllEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_PromiseAllEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_PromiseAllEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_PromiseAllEPNS0_8compiler18CodeAssemblerStateE:
.LFB22212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2334, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC34(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$517, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L444
.L441:
	movq	%r13, %rdi
	call	_ZN2v88internal19PromiseAllAssembler22GeneratePromiseAllImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L445
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L441
.L445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22212:
	.size	_ZN2v88internal8Builtins19Generate_PromiseAllEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_PromiseAllEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEv
	.type	_ZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEv, @function
_ZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEv:
.LFB22230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation(%rip), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_3SmiEEENS5_INS2_13NativeContextEEENS5_INS2_17PromiseCapabilityEEEE0_E10_M_managerERSt9_Any_dataRKSG_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E0_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_(%rip), %rax
	movq	%r12, -96(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	movq	%r12, -128(%rbp)
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_3SmiEEENS3_INS1_13NativeContextEEENS3_INS1_17PromiseCapabilityEEEEZNS1_26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEvEUlS7_S9_SB_SD_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OSB_OSD_(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -80(%rbp)
	movq	%rbx, %xmm0
	leaq	-128(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, %r8
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler19Generate_PromiseAllENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_RKSt8functionIFS7_S5_NS3_INS0_3SmiEEENS3_INS0_13NativeContextEEENS3_INS0_17PromiseCapabilityEEEEESI_
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L447
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L447:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L446
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L446:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L457:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22230:
	.size	_ZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEv, .-_ZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_PromiseAllSettledEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"PromiseAllSettled"
	.section	.text._ZN2v88internal8Builtins26Generate_PromiseAllSettledEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_PromiseAllSettledEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_PromiseAllSettledEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_PromiseAllSettledEPNS0_8compiler18CodeAssemblerStateE:
.LFB22226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2356, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$520, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L462
.L459:
	movq	%r13, %rdi
	call	_ZN2v88internal26PromiseAllSettledAssembler29GeneratePromiseAllSettledImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L463
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L459
.L463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22226:
	.size	_ZN2v88internal8Builtins26Generate_PromiseAllSettledEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_PromiseAllSettledEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE
	.type	_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE, @function
_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE:
.LFB22233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	xorl	%r8d, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	subq	$1192, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1120(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsNativeContextENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1192(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-1128(%rbp), %rax
	cmpq	$0, 16(%r14)
	movq	%rbx, -480(%rbp)
	movq	%r15, -224(%rbp)
	movq	%rax, -352(%rbp)
	je	.L468
	leaq	-480(%rbp), %r15
	leaq	-224(%rbp), %rax
	movq	%r14, %rdi
	leaq	-352(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r15, %rsi
	movq	%rax, -1136(%rbp)
	movq	%rdx, -1144(%rbp)
	call	*24(%r14)
	leaq	-864(%rbp), %r10
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%rax, -1160(%rbp)
	movq	%r10, -1128(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1128(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r10, -1224(%rbp)
	call	_ZN2v88internal17CodeStubAssembler26LoadJSReceiverIdentityHashENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$6, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	leaq	8(%r12), %rdi
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r14, %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%r14, -1152(%rbp)
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23LoadAndUntagObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	leaq	-736(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r14
	movq	%rdi, -1176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-608(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1184(%rbp), %r14
	movq	-1176(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1184(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1128(%rbp), %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23LoadAndUntagObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	movq	-1144(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1136(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1200(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1144(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, -1136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2097152, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1168(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler28CalculateNewElementsCapacityEPNS0_8compiler4NodeENS1_13ParameterModeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMinENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%rax, %rdx
	movl	$2, %esi
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	pushq	$0
	movq	%r14, %r8
	movl	$2, %ecx
	movq	-1216(%rbp), %r11
	pushq	$0
	movq	%rax, %r9
	movq	%r12, %rdi
	pushq	$1
	movq	-1128(%rbp), %rdx
	movl	$2, %esi
	pushq	$4
	pushq	%r11
	pushq	-1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22CopyFixedArrayElementsENS0_12ElementsKindEPNS0_8compiler4NodeES2_S5_S5_S5_S5_NS0_16WriteBarrierModeENS1_13ParameterModeENS1_18HoleConversionModeEPNS3_26TypedCodeAssemblerVariableINS0_5BoolTEEE@PLT
	addq	$48, %rsp
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	pushq	$1
	movq	-1160(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4, %r8d
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r14, %rcx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	-1152(%rbp), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	-1168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movl	$24, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	movq	%r14, -1152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1136(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-1152(%rbp), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-1160(%rbp), %rcx
	movq	-1128(%rbp), %rsi
	movl	$4, %r8d
	movl	$1, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%r14, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1144(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	-1128(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	-1160(%rbp), %rcx
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$4, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movl	$4, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler19StoreContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEiNS3_INS0_6ObjectEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1208(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1136(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rsi
	movl	$4, %edi
	movq	%rbx, %r9
	movq	%rax, %r8
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-1128(%rbp), %xmm0
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	-1144(%rbp), %rdx
	movq	%rax, -352(%rbp)
	movhps	-1136(%rbp), %xmm0
	movq	-208(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-1160(%rbp), %xmm0
	movq	%rax, -344(%rbp)
	movhps	-1152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1192(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1224(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1184(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1176(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1128(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L469
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L468:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22233:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE, .-_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE
	.section	.text._ZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEv
	.type	_ZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEv, @function
_ZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEv:
.LFB22242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_6ObjectEEENS5_INS2_13NativeContextEEES7_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEvEUlS5_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L470
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L470:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L477
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L477:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22242:
	.size	_ZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEv, .-_ZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_PromiseAllResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"PromiseAllResolveElementClosure"
	.section	.text._ZN2v88internal8Builtins40Generate_PromiseAllResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_PromiseAllResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_PromiseAllResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_PromiseAllResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2494, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$518, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L482
.L479:
	movq	%r13, %rdi
	call	_ZN2v88internal40PromiseAllResolveElementClosureAssembler43GeneratePromiseAllResolveElementClosureImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L483
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L479
.L483:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22238:
	.size	_ZN2v88internal8Builtins40Generate_PromiseAllResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_PromiseAllResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEv
	.type	_ZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEv, @function
_ZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEv:
.LFB22257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_(%rip), %rax
	movq	%r12, -80(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L484
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L484:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L491
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L491:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22257:
	.size	_ZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEv, .-_ZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins47Generate_PromiseAllSettledResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"PromiseAllSettledResolveElementClosure"
	.section	.text._ZN2v88internal8Builtins47Generate_PromiseAllSettledResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins47Generate_PromiseAllSettledResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins47Generate_PromiseAllSettledResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins47Generate_PromiseAllSettledResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2506, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC37(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$521, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L496
.L493:
	movq	%r13, %rdi
	call	_ZN2v88internal47PromiseAllSettledResolveElementClosureAssembler50GeneratePromiseAllSettledResolveElementClosureImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L497
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L493
.L497:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22253:
	.size	_ZN2v88internal8Builtins47Generate_PromiseAllSettledResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins47Generate_PromiseAllSettledResolveElementClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEv
	.type	_ZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEv, @function
_ZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEv:
.LFB22267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlNS2_8compiler5TNodeINS2_7ContextEEENS5_INS2_13NativeContextEEENS5_INS2_6ObjectEEEE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEENS3_INS1_7ContextEEENS3_INS1_13NativeContextEEES5_EZNS1_46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEvEUlS7_S9_S5_E_E9_M_invokeERKSt9_Any_dataOS7_OS9_OS5_(%rip), %rax
	movq	%r12, -80(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler40Generate_PromiseAllResolveElementClosureENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSFunctionEEERKSt8functionIFS7_S5_NS3_INS0_13NativeContextEEES7_EE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L498
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L498:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L505:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22267:
	.size	_ZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEv, .-_ZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins46Generate_PromiseAllSettledRejectElementClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"PromiseAllSettledRejectElementClosure"
	.section	.text._ZN2v88internal8Builtins46Generate_PromiseAllSettledRejectElementClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins46Generate_PromiseAllSettledRejectElementClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins46Generate_PromiseAllSettledRejectElementClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins46Generate_PromiseAllSettledRejectElementClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB22263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2536, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$522, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L510
.L507:
	movq	%r13, %rdi
	call	_ZN2v88internal46PromiseAllSettledRejectElementClosureAssembler49GeneratePromiseAllSettledRejectElementClosureImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L507
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22263:
	.size	_ZN2v88internal8Builtins46Generate_PromiseAllSettledRejectElementClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins46Generate_PromiseAllSettledRejectElementClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv.str1.1,"aMS",@progbits,1
.LC39:
	.string	"Promise.race"
	.section	.text._ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv
	.type	_ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv, @function
_ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv:
.LFB22277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-832(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$936, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -888(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$26, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	.LC39(%rip), %r8
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler20ThrowIfNotJSReceiverENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_15MessageTemplateEPKc@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$499, %edx
	movq	%rax, %rsi
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -880(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %xmm0
	leaq	-96(%rbp), %r14
	xorl	%esi, %esi
	movq	%rax, %r8
	movl	$2, %edi
	movq	%rbx, %r9
	movl	$1, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movhps	-856(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	pushq	%r14
	movq	%rax, -344(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, %rdx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -896(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, %rsi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r15, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, -936(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-736(%rbp), %rcx
	movq	%rax, %r15
	movq	%rax, -944(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -864(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-608(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -856(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, -96(%rbp)
	movq	%rax, %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS2_8compiler4NodeES6_S6_EUlvE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueES4_S4_S4_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%r14, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler26SetForwardingHandlerIfTrueEPNS0_8compiler4NodeES4_RKSt8functionIFS4_vEE
	movq	-80(%rbp), %rax
	popq	%rdi
	popq	%r8
	testq	%rax, %rax
	je	.L513
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L513:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-856(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	-888(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -816(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -808(%rbp)
	xorl	%edx, %edx
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-896(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-880(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	leaq	-800(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r10
	movq	%rax, %rcx
	movl	$8, %edx
	movq	%r10, %rdi
	movq	%r10, -912(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	-880(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-904(%rbp), %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler40GotoIfNotPromiseResolveLookupChainIntactEPNS0_8compiler4NodeENS2_11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelE
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3192(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -928(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-768(%rbp), %r11
	movl	$710, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -968(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	leaq	-784(%rbp), %r11
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r9
	movq	%r11, %rdx
	pushq	%r14
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-904(%rbp), %xmm0
	movq	-752(%rbp), %rax
	movq	%r9, -784(%rbp)
	movq	%r15, %r9
	movhps	-928(%rbp), %xmm0
	movq	%r11, -928(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-864(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-952(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	.LC32(%rip), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler18ThrowIfNotCallableENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movq	-952(%rbp), %r8
	movq	-912(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	pushq	%r13
	leaq	-816(%rbp), %rsi
	movq	-896(%rbp), %rcx
	pushq	-856(%rbp)
	movq	%rsi, %rdx
	movl	$1, %r8d
	movq	%rax, %r9
	movq	-888(%rbp), %rdi
	movq	%rsi, -960(%rbp)
	movq	%rbx, %rsi
	movq	%rax, -952(%rbp)
	call	_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE@PLT
	addq	$24, %rsp
	movq	%rbx, %rsi
	movq	-952(%rbp), %r10
	pushq	%r13
	movq	-856(%rbp), %r9
	movq	%rax, %rdx
	movl	$1, %ecx
	movq	-888(%rbp), %rdi
	movq	%r10, %r8
	call	_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	movq	-912(%rbp), %rdi
	movq	%rax, -952(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r13, (%rsp)
	movq	-864(%rbp), %r9
	movq	-952(%rbp), %r8
	movq	%rax, %rcx
	movq	-904(%rbp), %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler11CallResolveEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	movq	%r12, %rdi
	movq	%rax, -904(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3408(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -952(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-968(%rbp), %r11
	movl	$710, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%rbx, %r9
	xorl	%esi, %esi
	movq	%rax, %r8
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%r14
	movq	-928(%rbp), %rdx
	movq	%r12, %rdi
	movq	-904(%rbp), %xmm0
	movq	%rax, -784(%rbp)
	movq	-752(%rbp), %rax
	movhps	-952(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-864(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-968(%rbp), %r11
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-768(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %xmm0
	movq	%rbx, %r9
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-952(%rbp), %xmm0
	movl	$5, %edi
	pushq	%rdi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-928(%rbp), %rdx
	movq	%rax, -784(%rbp)
	movq	-752(%rbp), %rax
	pushq	%r14
	movaps	%xmm0, -96(%rbp)
	movq	-904(%rbp), %xmm0
	movq	%rax, -776(%rbp)
	movq	-944(%rbp), %rax
	movhps	-936(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-864(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movl	$16, %edi
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movq	%r12, %xmm2
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	-920(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_20PromiseRaceAssembler23GeneratePromiseRaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%r14, %r8
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler25SetPromiseHandledByIfTrueEPNS0_8compiler4NodeES4_S4_RKSt8functionIFS4_vEE
	movq	-80(%rbp), %rax
	popq	%rcx
	popq	%rsi
	testq	%rax, %rax
	je	.L514
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L514:
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-920(%rbp), %r15
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-912(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	-960(%rbp), %rdx
	movq	-888(%rbp), %rdi
	call	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	movq	-856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, -904(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -912(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-880(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	movq	%rbx, %r9
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-904(%rbp), %xmm0
	pushq	%r14
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	movq	-896(%rbp), %rdx
	movhps	-880(%rbp), %xmm0
	movq	-208(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-912(%rbp), %xmm0
	movq	%rax, -344(%rbp)
	movhps	-872(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-888(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L523
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L523:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22277:
	.size	_ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv, .-_ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_PromiseRaceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"PromiseRace"
	.section	.text._ZN2v88internal8Builtins20Generate_PromiseRaceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_PromiseRaceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_PromiseRaceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_PromiseRaceEPNS0_8compiler18CodeAssemblerStateE:
.LFB22273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2568, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$519, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L528
.L525:
	movq	%r13, %rdi
	call	_ZN2v88internal20PromiseRaceAssembler23GeneratePromiseRaceImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L525
.L529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22273:
	.size	_ZN2v88internal8Builtins20Generate_PromiseRaceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_PromiseRaceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_,"axG",@progbits,_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_, @function
_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_:
.LFB24858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-544(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$584, %rsp
	movq	%rdx, -552(%rbp)
	movl	$8, %edx
	movq	%rcx, -592(%rbp)
	movq	%r8, -568(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-480(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-512(%rbp), %r10
	movl	$1, %r8d
	movq	%r13, -512(%rbp)
	movq	%r10, %rcx
	movq	%r10, -584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	-560(%rbp), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$240, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-584(%rbp), %r10
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -600(%rbp)
	call	_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$5, %edi
	movq	%rbx, %r9
	xorl	%esi, %esi
	movq	-568(%rbp), %rcx
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-576(%rbp), %xmm0
	leaq	-528(%rbp), %r11
	movq	%r12, %rdi
	movq	-552(%rbp), %xmm1
	movq	%rcx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%r11, %rdx
	pushq	%rcx
	movhps	-592(%rbp), %xmm1
	movhps	-584(%rbp), %xmm0
	movq	%rax, -528(%rbp)
	movq	-496(%rbp), %rax
	movq	%rcx, -576(%rbp)
	movl	$1, %ecx
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm1, -624(%rbp)
	movq	%r11, -592(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3408(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-600(%rbp), %r10
	movl	$710, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-576(%rbp), %rsi
	movl	$2, %edi
	movq	%rbx, %r9
	movq	-592(%rbp), %r11
	movq	%rax, %r8
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-552(%rbp), %xmm0
	movq	%rax, -528(%rbp)
	movq	%r11, %rdx
	movq	-496(%rbp), %rax
	movhps	-584(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-600(%rbp), %r10
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-576(%rbp), %rsi
	movq	%rbx, %r9
	movl	$5, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	-592(%rbp), %r11
	movq	%rax, -528(%rbp)
	movq	-496(%rbp), %rax
	movq	-552(%rbp), %xmm0
	movdqa	-624(%rbp), %xmm1
	movq	%r11, %rdx
	movq	%rax, -520(%rbp)
	movq	-568(%rbp), %rax
	movhps	-584(%rbp), %xmm0
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L533
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L533:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24858:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_, .-_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_
	.section	.text._ZN2v88internal30PromisePrototypeCatchAssembler33GeneratePromisePrototypeCatchImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30PromisePrototypeCatchAssembler33GeneratePromisePrototypeCatchImplEv
	.type	_ZN2v88internal30PromisePrototypeCatchAssembler33GeneratePromisePrototypeCatchImplEv, @function
_ZN2v88internal30PromisePrototypeCatchAssembler33GeneratePromisePrototypeCatchImplEv:
.LFB22056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJNS0_8compiler5TNodeINS0_7OddballEEEPNS3_4NodeEEEES8_S8_S8_DpT_
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22056:
	.size	_ZN2v88internal30PromisePrototypeCatchAssembler33GeneratePromisePrototypeCatchImplEv, .-_ZN2v88internal30PromisePrototypeCatchAssembler33GeneratePromisePrototypeCatchImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_PromisePrototypeCatchEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"PromisePrototypeCatch"
	.section	.text._ZN2v88internal8Builtins30Generate_PromisePrototypeCatchEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_PromisePrototypeCatchEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_PromisePrototypeCatchEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_PromisePrototypeCatchEPNS0_8compiler18CodeAssemblerStateE:
.LFB22052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1221, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC41(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$505, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L540
.L537:
	movq	%r13, %rdi
	call	_ZN2v88internal30PromisePrototypeCatchAssembler33GeneratePromisePrototypeCatchImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L537
.L541:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22052:
	.size	_ZN2v88internal8Builtins30Generate_PromisePrototypeCatchEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_PromisePrototypeCatchEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_,"axG",@progbits,_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_, @function
_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_:
.LFB24880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-544(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$568, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -552(%rbp)
	movl	$8, %edx
	movaps	%xmm1, -576(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-480(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-512(%rbp), %r10
	movl	$1, %r8d
	movq	%r13, -512(%rbp)
	movq	%r10, %rcx
	movq	%r10, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	-560(%rbp), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$240, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-592(%rbp), %r10
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rcx
	movl	$4, %edi
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rbx, %r9
	pushq	%rcx
	movdqa	-576(%rbp), %xmm1
	movq	%r12, %rdi
	leaq	-528(%rbp), %r11
	movq	-584(%rbp), %xmm0
	movq	%rax, -528(%rbp)
	movq	%r11, %rdx
	movq	-496(%rbp), %rax
	movq	%rcx, -584(%rbp)
	movl	$1, %ecx
	movhps	-592(%rbp), %xmm0
	movaps	%xmm1, -80(%rbp)
	movq	%r11, -600(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3408(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-608(%rbp), %r10
	movl	$710, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-584(%rbp), %rsi
	movl	$2, %edi
	movq	%rbx, %r9
	movq	-600(%rbp), %r11
	movq	%rax, %r8
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-552(%rbp), %xmm0
	movq	%rax, -528(%rbp)
	movq	%r11, %rdx
	movq	-496(%rbp), %rax
	movhps	-592(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-608(%rbp), %r10
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-584(%rbp), %rsi
	movq	%rbx, %r9
	movl	$4, %edi
	pushq	%rdi
	movq	-600(%rbp), %r11
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	-552(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-576(%rbp), %xmm1
	movq	%r11, %rdx
	movq	%rax, -528(%rbp)
	movq	-496(%rbp), %rax
	movhps	-592(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L545
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L545:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24880:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_, .-_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_
	.section	.text._ZN2v88internal27PromiseThenFinallyAssembler30GeneratePromiseThenFinallyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27PromiseThenFinallyAssembler30GeneratePromiseThenFinallyImplEv
	.type	_ZN2v88internal27PromiseThenFinallyAssembler30GeneratePromiseThenFinallyImplEv, @function
_ZN2v88internal27PromiseThenFinallyAssembler30GeneratePromiseThenFinallyImplEv:
.LFB22140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -152(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	movl	$3, %edi
	xorl	%esi, %esi
	leaq	-80(%rbp), %rbx
	pushq	%rdi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r13, %r9
	movl	$1, %ecx
	movq	-136(%rbp), %xmm0
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movq	%rax, -120(%rbp)
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-152(%rbp), %r11
	movl	$510, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r13, %r9
	movq	%r14, %rdx
	pushq	%rdi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	pushq	%rbx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-144(%rbp), %xmm0
	movq	%r10, -128(%rbp)
	movq	%rax, -120(%rbp)
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal24PromiseBuiltinsAssembler24CreateValueThunkFunctionEPNS0_8compiler4NodeES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L549
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L549:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22140:
	.size	_ZN2v88internal27PromiseThenFinallyAssembler30GeneratePromiseThenFinallyImplEv, .-_ZN2v88internal27PromiseThenFinallyAssembler30GeneratePromiseThenFinallyImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_PromiseThenFinallyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"PromiseThenFinally"
	.section	.text._ZN2v88internal8Builtins27Generate_PromiseThenFinallyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_PromiseThenFinallyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_PromiseThenFinallyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_PromiseThenFinallyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1666, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$513, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L554
.L551:
	movq	%r13, %rdi
	call	_ZN2v88internal27PromiseThenFinallyAssembler30GeneratePromiseThenFinallyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L555
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L551
.L555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22136:
	.size	_ZN2v88internal8Builtins27Generate_PromiseThenFinallyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_PromiseThenFinallyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28PromiseCatchFinallyAssembler31GeneratePromiseCatchFinallyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28PromiseCatchFinallyAssembler31GeneratePromiseCatchFinallyImplEv
	.type	_ZN2v88internal28PromiseCatchFinallyAssembler31GeneratePromiseCatchFinallyImplEv, @function
_ZN2v88internal28PromiseCatchFinallyAssembler31GeneratePromiseCatchFinallyImplEv:
.LFB22159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -152(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	movl	$3, %edi
	xorl	%esi, %esi
	leaq	-80(%rbp), %rbx
	pushq	%rdi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r13, %r9
	movl	$1, %ecx
	movq	-136(%rbp), %xmm0
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	movq	%rax, -120(%rbp)
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-152(%rbp), %r11
	movl	$510, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r13, %r9
	movq	%r14, %rdx
	pushq	%rdi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	pushq	%rbx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-144(%rbp), %xmm0
	movq	%r10, -128(%rbp)
	movq	%rax, -120(%rbp)
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal24PromiseBuiltinsAssembler21CreateThrowerFunctionEPNS0_8compiler4NodeES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeEEEES5_S5_S5_DpT_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L559:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22159:
	.size	_ZN2v88internal28PromiseCatchFinallyAssembler31GeneratePromiseCatchFinallyImplEv, .-_ZN2v88internal28PromiseCatchFinallyAssembler31GeneratePromiseCatchFinallyImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_PromiseCatchFinallyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"PromiseCatchFinally"
	.section	.text._ZN2v88internal8Builtins28Generate_PromiseCatchFinallyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_PromiseCatchFinallyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_PromiseCatchFinallyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_PromiseCatchFinallyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1727, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC43(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$514, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L564
.L561:
	movq	%r13, %rdi
	call	_ZN2v88internal28PromiseCatchFinallyAssembler31GeneratePromiseCatchFinallyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L565
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L561
.L565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22155:
	.size	_ZN2v88internal8Builtins28Generate_PromiseCatchFinallyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_PromiseCatchFinallyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_,"axG",@progbits,_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_
	.type	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_, @function
_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_:
.LFB24882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-544(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$584, %rsp
	movq	%r8, -568(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -552(%rbp)
	movl	$8, %edx
	movaps	%xmm1, -592(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-480(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-512(%rbp), %r10
	movl	$1, %r8d
	movq	%r13, -512(%rbp)
	movq	%r10, %rcx
	movq	%r10, -600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	-560(%rbp), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler36BranchIfPromiseThenLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$240, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-600(%rbp), %r10
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -616(%rbp)
	call	_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$5, %edi
	movq	%rbx, %r9
	xorl	%esi, %esi
	movq	-568(%rbp), %rcx
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-576(%rbp), %xmm0
	movdqa	-592(%rbp), %xmm1
	movq	%r12, %rdi
	leaq	-528(%rbp), %r11
	movq	%rcx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%r11, %rdx
	pushq	%rcx
	movhps	-600(%rbp), %xmm0
	movq	%rax, -528(%rbp)
	movq	-496(%rbp), %rax
	movq	%rcx, -576(%rbp)
	movl	$1, %ecx
	movaps	%xmm1, -80(%rbp)
	movq	%r11, -608(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3408(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-616(%rbp), %r10
	movl	$710, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-576(%rbp), %rsi
	movl	$2, %edi
	movq	%rbx, %r9
	movq	-608(%rbp), %r11
	movq	%rax, %r8
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-552(%rbp), %xmm0
	movq	%rax, -528(%rbp)
	movq	%r11, %rdx
	movq	-496(%rbp), %rax
	movhps	-600(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-616(%rbp), %r10
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-576(%rbp), %rsi
	movq	%rbx, %r9
	movl	$5, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	-608(%rbp), %r11
	movq	%rax, -528(%rbp)
	movq	-496(%rbp), %rax
	movq	-552(%rbp), %xmm0
	movdqa	-592(%rbp), %xmm1
	movq	%r11, %rdx
	movq	%rax, -520(%rbp)
	movq	-568(%rbp), %rax
	movhps	-600(%rbp), %xmm0
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L569
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L569:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24882:
	.size	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_, .-_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_
	.section	.rodata._ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv.str1.1,"aMS",@progbits,1
.LC44:
	.string	"Promise.prototype.finally"
	.section	.text._ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv
	.type	_ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv, @function
_ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv:
.LFB22168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$26, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rbx
	leaq	.LC44(%rip), %r8
	movq	%rax, %rsi
	movq	%r14, -640(%rbp)
	call	_ZN2v88internal17CodeStubAssembler20ThrowIfNotJSReceiverENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_15MessageTemplateEPKc@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-576(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$212, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	leaq	-624(%rbp), %r11
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%rax, %rcx
	movq	%r11, -656(%rbp)
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal17CodeStubAssembler14IsJSPromiseMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-632(%rbp), %r10
	movq	-648(%rbp), %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler39BranchIfPromiseSpeciesLookupChainIntactEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-640(%rbp), %rdx
	leaq	-608(%rbp), %rbx
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler18SpeciesConstructorENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSReceiverEEE@PLT
	movq	-656(%rbp), %r11
	movq	%rax, %rsi
	movq	%r11, %rdi
	movq	%r11, -632(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-632(%rbp), %r11
	movq	%r11, %rdi
	movq	%r11, -680(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-592(%rbp), %r9
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, -672(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-320(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-192(%rbp), %rcx
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-664(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-664(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-656(%rbp), %rdx
	call	_ZN2v88internal24PromiseBuiltinsAssembler29CreatePromiseFinallyFunctionsEPNS0_8compiler4NodeES4_S4_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rdx, -656(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-656(%rbp), %r8
	movq	-672(%rbp), %r9
	movq	%r8, %rsi
	movq	%r9, %rdi
	movq	%r9, -656(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-656(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-632(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -632(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-656(%rbp), %r9
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-640(%rbp), %rdx
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	-648(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal24PromiseBuiltinsAssembler10InvokeThenIJPNS0_8compiler4NodeES5_EEES5_S5_S5_DpT_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-664(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-680(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L573
	addq	$648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L573:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22168:
	.size	_ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv, .-_ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_PromisePrototypeFinallyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"PromisePrototypeFinally"
	.section	.text._ZN2v88internal8Builtins32Generate_PromisePrototypeFinallyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_PromisePrototypeFinallyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_PromisePrototypeFinallyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_PromisePrototypeFinallyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1764, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC45(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$512, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L578
.L575:
	movq	%r13, %rdi
	call	_ZN2v88internal32PromisePrototypeFinallyAssembler35GeneratePromisePrototypeFinallyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L579
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L575
.L579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22164:
	.size	_ZN2v88internal8Builtins32Generate_PromisePrototypeFinallyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_PromisePrototypeFinallyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE:
.LFB28894:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28894:
	.size	_GLOBAL__sub_I__ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE, .-_GLOBAL__sub_I__ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24PromiseBuiltinsAssembler17AllocateJSPromiseEPNS0_8compiler4NodeE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
