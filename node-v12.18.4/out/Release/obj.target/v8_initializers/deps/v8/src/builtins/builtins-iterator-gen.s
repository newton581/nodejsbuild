	.file	"builtins-iterator-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_:
.LFB21907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$96, %rsp
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3856(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%rbx, %r9
	movl	$2, %edi
	movq	-112(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-104(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L10
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21907:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_, .-_ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE:
.LFB21909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%r8, %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movq	%r9, %rcx
	movq	%rbx, %rsi
	subq	$680, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	leaq	-592(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r11, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-464(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-688(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-704(%rbp), %r10
	movq	-688(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r11, %rcx
	movq	%r10, -688(%rbp)
	movq	%r11, -696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-696(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -720(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%rax, %rcx
	movl	$167, %esi
	movq	%r15, -80(%rbp)
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-680(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-688(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -712(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-656(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r15, -64(%rbp)
	xorl	%esi, %esi
	movq	-696(%rbp), %rcx
	movq	%rax, %r8
	movq	%r14, %r9
	movq	%r12, %rdi
	movl	$3, %ebx
	leaq	-208(%rbp), %r15
	movq	-640(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	pushq	%rbx
	movhps	-688(%rbp), %xmm0
	leaq	-336(%rbp), %rbx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rdx, -208(%rbp)
	movq	%r15, %rdx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-680(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$176, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-680(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	2912(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	leaq	-624(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-696(%rbp), %rsi
	movl	$2, %edi
	movq	%r14, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movl	$1, %ecx
	pushq	%rsi
	movq	-608(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-688(%rbp), %xmm0
	movq	%rdx, -672(%rbp)
	leaq	-672(%rbp), %rdx
	movq	%rax, -664(%rbp)
	movhps	-704(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-680(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-712(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	movq	-688(%rbp), %rax
	leaq	-40(%rbp), %rsp
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21909:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, .-_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE:
.LFB21908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3856(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r13, %r9
	movl	$2, %edi
	movq	%rax, %r8
	movq	%r14, %xmm0
	movl	$1, %ecx
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-136(%rbp), %xmm0
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %r8
	movq	%r15, %r9
	movq	%r14, %rdx
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L18
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21908:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, .-_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE:
.LFB21916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$536, %rsp
	movq	16(%rbp), %r15
	movq	24(%rbp), %r14
	movq	%rcx, -552(%rbp)
	movl	%r8d, -560(%rbp)
	movq	%r9, -568(%rbp)
	movq	%rsi, -520(%rbp)
	movq	%r15, -528(%rbp)
	movq	%r14, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-496(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	(%rbx), %r13
	movq	8(%rbx), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	xorl	%esi, %esi
	movq	%r13, -64(%rbp)
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movq	%rbx, %xmm0
	movq	%rax, -208(%rbp)
	movq	-480(%rbp), %rax
	leaq	-208(%rbp), %rbx
	movhps	-544(%rbp), %xmm0
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-80(%rbp), %rax
	movq	-520(%rbp), %r9
	pushq	%rax
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-464(%rbp), %r14
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	leaq	-336(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	-560(%rbp), %r10d
	movq	%rax, %r9
	popq	%rax
	popq	%rdx
	testb	%r10b, %r10b
	jne	.L23
.L20:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsJSReceiverMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	2368(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -560(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%rbx, %rdi
	movl	$2, %ebx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-544(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r13, %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-520(%rbp), %r9
	pushq	%rcx
	movq	%r12, %rdi
	movhps	-560(%rbp), %xmm0
	leaq	-512(%rbp), %rdx
	movq	%rax, -512(%rbp)
	movq	-192(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-536(%rbp), %rcx
	movq	-528(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-552(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-544(%rbp), %rcx
	movl	$1, %r8d
	movq	-520(%rbp), %rdx
	movl	$168, %esi
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-536(%rbp), %rcx
	movq	-528(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r9, -560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-560(%rbp), %r9
	movq	-568(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-552(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-560(%rbp), %r9
	jmp	.L20
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21916:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE, .-_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE:
.LFB21918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$376, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -376(%rbp)
	xorl	%edx, %edx
	movl	%ecx, -384(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -408(%rbp)
	movl	$1, %r8d
	movq	%r9, -392(%rbp)
	movq	%rax, -400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	-384(%rbp), %eax
	testb	%al, %al
	jne	.L30
.L26:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3544(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movq	%rbx, %r9
	movl	$2, %edi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	pushq	%rsi
	leaq	-352(%rbp), %rdx
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	-376(%rbp), %xmm0
	movq	%rax, -352(%rbp)
	movq	-192(%rbp), %rax
	movhps	-384(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-400(%rbp), %rcx
	movq	-392(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-408(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-376(%rbp), %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L26
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21918:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, .-_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.type	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, @function
_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE:
.LFB21922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%r8, -144(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	movq	(%r14), %rbx
	leaq	3200(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r10
	movl	$710, %edx
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -176(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-128(%rbp), %r11
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	movl	$2, %ebx
	movq	%r11, %rdx
	pushq	%rbx
	movq	-160(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	leaq	-80(%rbp), %rax
	movhps	-136(%rbp), %xmm0
	pushq	%rax
	movq	%r9, -168(%rbp)
	movq	%r11, -160(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-144(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	(%r14), %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-176(%rbp), %r10
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-152(%rbp), %rcx
	movl	$3, %ebx
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	movq	-160(%rbp), %r11
	pushq	%rbx
	movhps	-136(%rbp), %xmm0
	pushq	%rcx
	movq	-168(%rbp), %r9
	movl	$1, %ecx
	movq	%r11, %rdx
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21922:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE, .-_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordENS2_5TNodeINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordENS2_5TNodeINS0_6ObjectEEE
	.type	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordENS2_5TNodeINS0_6ObjectEEE, @function
_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordENS2_5TNodeINS0_6ObjectEEE:
.LFB21923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdi, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$184, %rsp
	movq	%rcx, -216(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-216(%rbp), %r9
	movq	%r9, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$156, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L39:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21923:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordENS2_5TNodeINS0_6ObjectEEE, .-_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordENS2_5TNodeINS0_6ObjectEEE
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_
	.type	_ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_, @function
_ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_:
.LFB21924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-400(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-384(%rbp), %rbx
	movq	%rbx, %r15
	movq	%r15, %xmm0
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-368(%rbp), %rax
	movq	%rax, %xmm1
	movq	%rax, %rbx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movq	%rax, -416(%rbp)
	movq	%rdx, -408(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%rbx, %rdi
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%rbx, -424(%rbp)
	leaq	-352(%rbp), %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r15, %rdi
	movq	%r15, -432(%rbp)
	leaq	-336(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-424(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	leaq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movl	$1, %r8d
	movdqa	-448(%rbp), %xmm0
	movl	$3, %edx
	movq	%rbx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-208(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
	movq	-448(%rbp), %r11
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	-416(%rbp), %rdx
	movq	%r11, %rcx
	call	_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	$0, (%rsp)
	call	_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal18GrowableFixedArray4PushENS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-448(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18GrowableFixedArray9ToJSArrayENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	-448(%rbp), %r11
	movq	%rax, %r12
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-424(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-432(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21924:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_, .-_ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_
	.section	.rodata._ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-iterator-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"IterableToList"
	.section	.text._ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE:
.LFB21935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$236, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$392, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L48
.L45:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L45
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21935:
	.size	_ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_IterableToListEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23IterableToListAssembler26GenerateIterableToListImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23IterableToListAssembler26GenerateIterableToListImplEv
	.type	_ZN2v88internal23IterableToListAssembler26GenerateIterableToListImplEv, @function
_ZN2v88internal23IterableToListAssembler26GenerateIterableToListImplEv:
.LFB21939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal25IteratorBuiltinsAssembler14IterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21939:
	.size	_ZN2v88internal23IterableToListAssembler26GenerateIterableToListImplEv, .-_ZN2v88internal23IterableToListAssembler26GenerateIterableToListImplEv
	.section	.text._ZN2v88internal39IterableToListMayPreserveHolesAssembler42GenerateIterableToListMayPreserveHolesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39IterableToListMayPreserveHolesAssembler42GenerateIterableToListMayPreserveHolesImplEv
	.type	_ZN2v88internal39IterableToListMayPreserveHolesAssembler42GenerateIterableToListMayPreserveHolesImplEv, @function
_ZN2v88internal39IterableToListMayPreserveHolesAssembler42GenerateIterableToListMayPreserveHolesImplEv:
.LFB21948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	leaq	-256(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	8(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler34IsFastJSArrayWithNoCustomIterationENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-240(%rbp), %r11
	movl	$210, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -280(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rax, %rdx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movq	-224(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movq	%r8, -272(%rbp)
	movq	%r10, -256(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-280(%rbp), %r11
	movl	$392, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	-272(%rbp), %r8
	movq	-224(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movhps	-264(%rbp), %xmm0
	movl	$2, %r9d
	movq	%r12, %rdi
	movq	%r10, -256(%rbp)
	movq	%rax, -248(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21948:
	.size	_ZN2v88internal39IterableToListMayPreserveHolesAssembler42GenerateIterableToListMayPreserveHolesImplEv, .-_ZN2v88internal39IterableToListMayPreserveHolesAssembler42GenerateIterableToListMayPreserveHolesImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_IterableToListMayPreserveHolesEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"IterableToListMayPreserveHoles"
	.section	.text._ZN2v88internal8Builtins39Generate_IterableToListMayPreserveHolesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_IterableToListMayPreserveHolesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_IterableToListMayPreserveHolesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_IterableToListMayPreserveHolesEPNS0_8compiler18CodeAssemblerStateE:
.LFB21944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$253, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$394, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L60
.L57:
	movq	%r13, %rdi
	call	_ZN2v88internal39IterableToListMayPreserveHolesAssembler42GenerateIterableToListMayPreserveHolesImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L57
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21944:
	.size	_ZN2v88internal8Builtins39Generate_IterableToListMayPreserveHolesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_IterableToListMayPreserveHolesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal25IteratorBuiltinsAssembler18FastIterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_26TypedCodeAssemblerVariableIS6_EEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25IteratorBuiltinsAssembler18FastIterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_26TypedCodeAssemblerVariableIS6_EEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal25IteratorBuiltinsAssembler18FastIterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_26TypedCodeAssemblerVariableIS6_EEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal25IteratorBuiltinsAssembler18FastIterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_26TypedCodeAssemblerVariableIS6_EEPNS2_18CodeAssemblerLabelE:
.LFB21949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-704(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$808, %rsp
	movq	%rcx, -776(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -824(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-576(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-448(%rbp), %rdx
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-320(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	8(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rdi, -808(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler41IsFastJSArrayForReadWithNoCustomIterationENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	-808(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler34IsFastJSArrayWithNoCustomIterationENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-192(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-784(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$211, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %r11
	movq	%r14, %r9
	movq	%rbx, -64(%rbp)
	movl	$1, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	movq	-176(%rbp), %rax
	pushq	%rdi
	leaq	-736(%rbp), %rcx
	movq	%r12, %rdi
	pushq	%r11
	movq	%rcx, %rdx
	movl	$1, %ecx
	movq	%rsi, -736(%rbp)
	xorl	%esi, %esi
	movq	%r11, -808(%rbp)
	movq	%rdx, -816(%rbp)
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-776(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-784(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-768(%rbp), %r11
	movq	(%r12), %rsi
	movq	%r11, %rdi
	movq	%r11, -832(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	-832(%rbp), %r11
	movq	-792(%rbp), %r8
	movq	%r11, %rdi
	movq	%r11, -840(%rbp)
	call	_ZN2v88internal23StringBuiltinsAssembler44BranchIfStringPrimitiveWithNoCustomIterationENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelES9_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movl	$33554432, %esi
	movq	%r12, %rdi
	movq	%rax, -832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-832(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-824(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-816(%rbp), %rdi
	movl	$883, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-808(%rbp), %rsi
	movl	$1, %edi
	movq	%r14, %r9
	movq	%rax, %r8
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	leaq	-752(%rbp), %r10
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -752(%rbp)
	movq	-720(%rbp), %rax
	movq	%r10, %rdx
	movq	%r10, -832(%rbp)
	movq	%rbx, -64(%rbp)
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-776(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-840(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	-800(%rbp), %r8
	movq	%rbx, %rsi
	call	_ZN2v88internal49BranchIfIterableWithOriginalKeyOrValueMapIteratorEPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEENS4_INS0_7ContextEEEPNS1_18CodeAssemblerLabelESA_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-816(%rbp), %rdi
	movl	$408, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-808(%rbp), %rsi
	movl	$1, %edi
	movq	%r14, %r9
	pushq	%rdi
	movq	-832(%rbp), %r10
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -752(%rbp)
	movq	-720(%rbp), %rax
	movq	%r10, %rdx
	movq	%rbx, -64(%rbp)
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-776(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	-824(%rbp), %r8
	movq	%rbx, %rsi
	call	_ZN2v88internal44BranchIfIterableWithOriginalValueSetIteratorEPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEENS4_INS0_7ContextEEEPNS1_18CodeAssemblerLabelESA_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-816(%rbp), %rdi
	movl	$572, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	movl	$1, %ebx
	xorl	%esi, %esi
	movq	-808(%rbp), %rcx
	movq	%rax, %r8
	pushq	%rbx
	movq	%r14, %r9
	movq	-832(%rbp), %r10
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -752(%rbp)
	movq	-720(%rbp), %rax
	movq	%r10, %rdx
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-776(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-800(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21949:
	.size	_ZN2v88internal25IteratorBuiltinsAssembler18FastIterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_26TypedCodeAssemblerVariableIS6_EEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal25IteratorBuiltinsAssembler18FastIterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_26TypedCodeAssemblerVariableIS6_EEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal39IterableToListWithSymbolLookupAssembler42GenerateIterableToListWithSymbolLookupImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39IterableToListWithSymbolLookupAssembler42GenerateIterableToListWithSymbolLookupImplEv
	.type	_ZN2v88internal39IterableToListWithSymbolLookupAssembler42GenerateIterableToListWithSymbolLookupImplEv, @function
_ZN2v88internal39IterableToListWithSymbolLookupAssembler42GenerateIterableToListWithSymbolLookupImplEv:
.LFB21958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-256(%rbp), %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19GotoIfForceSlowPathEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	-280(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal25IteratorBuiltinsAssembler18FastIterableToListENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_26TypedCodeAssemblerVariableIS6_EEPNS2_18CodeAssemblerLabelE
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3856(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-240(%rbp), %r11
	movl	$710, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -304(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %r10
	movl	$2, %edi
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r14, %r9
	pushq	%r10
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-280(%rbp), %xmm0
	movq	%rax, -256(%rbp)
	movq	-224(%rbp), %rax
	movq	%r10, -296(%rbp)
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-304(%rbp), %r11
	movl	$392, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	-296(%rbp), %r10
	movq	-280(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -256(%rbp)
	movq	-224(%rbp), %rax
	movl	$2, %r9d
	movhps	-288(%rbp), %xmm0
	movq	%r10, %r8
	movq	%rax, -248(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21958:
	.size	_ZN2v88internal39IterableToListWithSymbolLookupAssembler42GenerateIterableToListWithSymbolLookupImplEv, .-_ZN2v88internal39IterableToListWithSymbolLookupAssembler42GenerateIterableToListWithSymbolLookupImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_IterableToListWithSymbolLookupEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"IterableToListWithSymbolLookup"
	.section	.text._ZN2v88internal8Builtins39Generate_IterableToListWithSymbolLookupEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_IterableToListWithSymbolLookupEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_IterableToListWithSymbolLookupEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_IterableToListWithSymbolLookupEPNS0_8compiler18CodeAssemblerStateE:
.LFB21954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$338, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$393, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L74
.L71:
	movq	%r13, %rdi
	call	_ZN2v88internal39IterableToListWithSymbolLookupAssembler42GenerateIterableToListWithSymbolLookupImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L71
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21954:
	.size	_ZN2v88internal8Builtins39Generate_IterableToListWithSymbolLookupEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_IterableToListWithSymbolLookupEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_, @function
_GLOBAL__sub_I__ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_:
.LFB28088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28088:
	.size	_GLOBAL__sub_I__ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_, .-_GLOBAL__sub_I__ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25IteratorBuiltinsAssembler17GetIteratorMethodEPNS0_8compiler4NodeES4_
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
