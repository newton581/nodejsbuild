	.file	"builtins-internal-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB9034:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9034:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB30467:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE30467:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB30472:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE30472:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB30478:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE30478:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB30482:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L25
	cmpl	$3, %edx
	je	.L26
	cmpl	$1, %edx
	je	.L30
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE30482:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9028:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE9028:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB9027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9027:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB9036:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9036:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E0_E9_M_invokeERKSt9_Any_dataOS5_OS7_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E0_E9_M_invokeERKSt9_Any_dataOS5_OS7_, @function
_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E0_E9_M_invokeERKSt9_Any_dataOS5_OS7_:
.LFB30443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	movq	(%rdx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %r12
	movq	(%rax), %r14
	movq	8(%rax), %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$713, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %r9
	movl	$3, %edi
	movq	%rax, %r8
	movq	%r13, %xmm1
	movl	$1, %ecx
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	pushq	%rsi
	punpcklqdq	%xmm1, %xmm0
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	movq	%r15, -64(%rbp)
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30443:
	.size	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E0_E9_M_invokeERKSt9_Any_dataOS5_OS7_, .-_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E0_E9_M_invokeERKSt9_Any_dataOS5_OS7_
	.section	.text._ZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEPNS3_18CodeAssemblerLabelEb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEPNS3_18CodeAssemblerLabelEb, @function
_ZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEPNS3_18CodeAssemblerLabelEb:
.LFB25600:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	xorl	%ecx, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	movl	$1, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-480(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-352(%rbp), %rbx
	subq	$520, %rsp
	movl	%r9d, -552(%rbp)
	movq	%rsi, -504(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rdx, -528(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-224(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22IsJSObjectInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-488(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	8(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17IsEmptyFixedArrayENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-512(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler28IsEmptySlowElementDictionaryENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15IsDeprecatedMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	-552(%rbp), %r9d
	movq	-544(%rbp), %r10
	testb	%r9b, %r9b
	je	.L40
	movq	-512(%rbp), %r8
	movq	%r12, %xmm0
	movq	%r12, %rdi
	movq	%r10, -552(%rbp)
	movhps	-504(%rbp), %xmm0
	movq	%r8, %rsi
	movaps	%xmm0, -544(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17IsSimpleObjectMapENS0_8compiler5TNodeINS0_3MapEEE@PLT
	movl	$32, %edi
	movq	$0, -80(%rbp)
	movq	%rax, -512(%rbp)
	call	_Znwm@PLT
	leaq	-96(%rbp), %r9
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movdqa	-544(%rbp), %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	-552(%rbp), %r10
	movups	%xmm0, (%rax)
	movq	-528(%rbp), %xmm0
	movq	%r9, -528(%rbp)
	movhps	-512(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	leaq	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E_E9_M_invokeERKSt9_Any_dataOS5_OS7_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%r15, %rcx
	movq	%rax, %xmm1
	pushq	%r14
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
.L54:
	movq	-504(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler28ForEachEnumerableOwnPropertyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_3MapEEENS3_INS0_8JSObjectEEENS1_22ForEachEnumerationModeERKSt8functionIFvNS3_INS0_4NameEEENS3_INS0_6ObjectEEEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	-80(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L42
	movq	-528(%rbp), %r9
	movl	$3, %edx
	movq	%r9, %rsi
	movq	%r9, %rdi
	call	*%rax
.L42:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24IsJSReceiverInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20IsStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-488(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	-504(%rbp), %xmm0
	movl	$24, %edi
	movq	%r10, -512(%rbp)
	movq	$0, -80(%rbp)
	movhps	-528(%rbp), %xmm0
	movaps	%xmm0, -528(%rbp)
	call	_Znwm@PLT
	leaq	-96(%rbp), %r9
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movdqa	-528(%rbp), %xmm0
	movq	%r12, 16(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E0_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	-512(%rbp), %r10
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E0_E9_M_invokeERKSt9_Any_dataOS5_OS7_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%r15, %rcx
	movq	%rax, %xmm2
	pushq	%r14
	punpcklqdq	%xmm2, %xmm0
	movq	%r9, -528(%rbp)
	movaps	%xmm0, -80(%rbp)
	jmp	.L54
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25600:
	.size	_ZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEPNS3_18CodeAssemblerLabelEb, .-_ZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEPNS3_18CodeAssemblerLabelEb
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E_E9_M_invokeERKSt9_Any_dataOS5_OS7_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E_E9_M_invokeERKSt9_Any_dataOS5_OS7_, @function
_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E_E9_M_invokeERKSt9_Any_dataOS5_OS7_:
.LFB30437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	(%rsi), %r8
	movq	(%rdx), %r9
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	movq	8(%rax), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	(%rax), %rax
	subq	$8, %rsp
	pushq	$1
	movq	(%rax), %rdi
	call	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_10JSReceiverEEENS5_INS0_5BoolTEEENS5_INS0_4NameEEENS5_INS0_6ObjectEEENS0_12LanguageModeE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30437:
	.size	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E_E9_M_invokeERKSt9_Any_dataOS5_OS7_, .-_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_4NameEEENS3_INS1_6ObjectEEEEZNS1_12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS3_INS1_7ContextEEENS3_INS1_10JSReceiverEEES7_PNS2_18CodeAssemblerLabelEbEUlS5_S7_E_E9_M_invokeERKSt9_Any_dataOS5_OS7_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_:
.LFB30471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	movq	24(%rbp), %rax
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rax), %r12
	movq	16(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal17CodeStubAssembler3UseEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	.cfi_endproc
.LFE30471:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_:
.LFB30481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	movq	24(%rbp), %rax
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rax), %r12
	movq	16(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal17CodeStubAssembler3UseEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	.cfi_endproc
.LFE30481:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation:
.LFB30439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L63
	cmpl	$3, %edx
	je	.L64
	cmpl	$1, %edx
	je	.L70
.L65:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$32, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movq	%rax, (%rbx)
	movups	%xmm1, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L65
	movl	$32, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30439:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E0_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E0_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E0_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation:
.LFB30444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L72
	cmpl	$3, %edx
	je	.L73
	cmpl	$1, %edx
	je	.L79
.L74:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L74
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30444:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E0_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS2_8compiler5TNodeINS2_7ContextEEENS6_INS2_10JSReceiverEEENS6_INS2_6ObjectEEEPNS5_18CodeAssemblerLabelEbEUlNS6_INS2_4NameEEESC_E0_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_:
.LFB30465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-208(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rsi), %r14
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rbp), %rax
	movq	(%rdx), %r15
	movl	$8, %edx
	movq	(%rax), %r11
	movq	16(%rbp), %rax
	movq	(%rax), %r10
	movq	(%r9), %rax
	movq	%r11, -248(%rbp)
	movq	(%r8), %r9
	movq	%rax, -216(%rbp)
	movq	(%rcx), %rax
	movq	%r10, -240(%rbp)
	movq	%r9, -232(%rbp)
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-248(%rbp), %r11
	subq	$8, %rsp
	movq	-240(%rbp), %r10
	movq	-224(%rbp), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	-232(%rbp), %r9
	movq	8(%rbx), %rsi
	pushq	%r11
	pushq	%r10
	movq	%rax, %r8
	pushq	%r12
	pushq	%r13
	pushq	-216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17TryGetOwnPropertyEPNS0_8compiler4NodeES4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S6_@PLT
	movq	(%rbx), %rdi
	addq	$48, %rsp
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%rbx), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L83:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30465:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_:
.LFB30477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-208(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rsi), %r14
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rbp), %rax
	movq	(%rdx), %r15
	movl	$8, %edx
	movq	(%rax), %r11
	movq	16(%rbp), %rax
	movq	(%rax), %r10
	movq	(%r9), %rax
	movq	%r11, -248(%rbp)
	movq	(%r8), %r9
	movq	%rax, -216(%rbp)
	movq	(%rcx), %rax
	movq	%r10, -240(%rbp)
	movq	%r9, -232(%rbp)
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-248(%rbp), %r11
	subq	$8, %rsp
	movq	-240(%rbp), %r10
	movq	-224(%rbp), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	-232(%rbp), %r9
	movq	8(%rbx), %rsi
	pushq	%r11
	pushq	%r10
	movq	%rax, %r8
	pushq	%r12
	pushq	%r13
	pushq	-216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17TryGetOwnPropertyEPNS0_8compiler4NodeES4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S6_@PLT
	movq	(%rbx), %rdi
	addq	$48, %rsp
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%rbx), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30477:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_
	.section	.text._ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE:
.LFB25469:
	.cfi_startproc
	endbr64
	movl	$159, %esi
	jmp	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE@PLT
	.cfi_endproc
.LFE25469:
	.size	_ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal36CopyFastSmiOrObjectElementsAssembler39GenerateCopyFastSmiOrObjectElementsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36CopyFastSmiOrObjectElementsAssembler39GenerateCopyFastSmiOrObjectElementsImplEv
	.type	_ZN2v88internal36CopyFastSmiOrObjectElementsAssembler39GenerateCopyFastSmiOrObjectElementsImplEv, @function
_ZN2v88internal36CopyFastSmiOrObjectElementsAssembler39GenerateCopyFastSmiOrObjectElementsImplEv:
.LFB25478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$16, %edx
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	$0
	movq	%rax, %rdx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%ecx, %ecx
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler17ExtractFixedArrayEPNS0_8compiler4NodeES4_S4_S4_NS_4base5FlagsINS1_21ExtractFixedArrayFlagEiEENS1_13ParameterModeEPNS2_26TypedCodeAssemblerVariableINS0_5BoolTEEES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	movq	%rax, %rcx
	movl	$16, %edx
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	leaq	-24(%rbp), %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE25478:
	.size	_ZN2v88internal36CopyFastSmiOrObjectElementsAssembler39GenerateCopyFastSmiOrObjectElementsImplEv, .-_ZN2v88internal36CopyFastSmiOrObjectElementsAssembler39GenerateCopyFastSmiOrObjectElementsImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-internal-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"CopyFastSmiOrObjectElements"
	.section	.text._ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE:
.LFB25474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$34, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$84, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L95
.L92:
	movq	%r13, %rdi
	call	_ZN2v88internal36CopyFastSmiOrObjectElementsAssembler39GenerateCopyFastSmiOrObjectElementsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L92
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25474:
	.size	_ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_CopyFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31GrowFastDoubleElementsAssembler34GenerateGrowFastDoubleElementsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31GrowFastDoubleElementsAssembler34GenerateGrowFastDoubleElementsImplEv
	.type	_ZN2v88internal31GrowFastDoubleElementsAssembler34GenerateGrowFastDoubleElementsImplEv, @function
_ZN2v88internal31GrowFastDoubleElementsAssembler34GenerateGrowFastDoubleElementsImplEv:
.LFB25487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r13, %r9
	movq	%r14, %r8
	movl	$4, %ecx
	movq	%rax, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23TryGrowElementsCapacityEPNS0_8compiler4NodeES4_NS0_12ElementsKindES4_PNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %xmm1
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	movq	%rax, %rdx
	leaq	-80(%rbp), %r8
	movl	$2, %r9d
	punpcklqdq	%xmm1, %xmm0
	movl	$6, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25487:
	.size	_ZN2v88internal31GrowFastDoubleElementsAssembler34GenerateGrowFastDoubleElementsImplEv, .-_ZN2v88internal31GrowFastDoubleElementsAssembler34GenerateGrowFastDoubleElementsImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_GrowFastDoubleElementsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"GrowFastDoubleElements"
	.section	.text._ZN2v88internal8Builtins31Generate_GrowFastDoubleElementsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_GrowFastDoubleElementsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_GrowFastDoubleElementsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_GrowFastDoubleElementsEPNS0_8compiler18CodeAssemblerStateE:
.LFB25483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$44, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$85, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L105
.L102:
	movq	%r13, %rdi
	call	_ZN2v88internal31GrowFastDoubleElementsAssembler34GenerateGrowFastDoubleElementsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L102
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25483:
	.size	_ZN2v88internal8Builtins31Generate_GrowFastDoubleElementsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_GrowFastDoubleElementsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36GrowFastSmiOrObjectElementsAssembler39GenerateGrowFastSmiOrObjectElementsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36GrowFastSmiOrObjectElementsAssembler39GenerateGrowFastSmiOrObjectElementsImplEv
	.type	_ZN2v88internal36GrowFastSmiOrObjectElementsAssembler39GenerateGrowFastSmiOrObjectElementsImplEv, @function
_ZN2v88internal36GrowFastSmiOrObjectElementsAssembler39GenerateGrowFastSmiOrObjectElementsImplEv:
.LFB25496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r13, %r9
	movq	%r14, %r8
	movl	$2, %ecx
	movq	%rax, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23TryGrowElementsCapacityEPNS0_8compiler4NodeES4_NS0_12ElementsKindES4_PNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %xmm1
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	movq	%rax, %rdx
	leaq	-80(%rbp), %r8
	movl	$2, %r9d
	punpcklqdq	%xmm1, %xmm0
	movl	$6, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25496:
	.size	_ZN2v88internal36GrowFastSmiOrObjectElementsAssembler39GenerateGrowFastSmiOrObjectElementsImplEv, .-_ZN2v88internal36GrowFastSmiOrObjectElementsAssembler39GenerateGrowFastSmiOrObjectElementsImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_GrowFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"GrowFastSmiOrObjectElements"
	.section	.text._ZN2v88internal8Builtins36Generate_GrowFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_GrowFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_GrowFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_GrowFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE:
.LFB25492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$59, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$86, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L115
.L112:
	movq	%r13, %rdi
	call	_ZN2v88internal36GrowFastSmiOrObjectElementsAssembler39GenerateGrowFastSmiOrObjectElementsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L112
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25492:
	.size	_ZN2v88internal8Builtins36Generate_GrowFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_GrowFastSmiOrObjectElementsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29NewArgumentsElementsAssembler32GenerateNewArgumentsElementsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29NewArgumentsElementsAssembler32GenerateNewArgumentsElementsImplEv
	.type	_ZN2v88internal29NewArgumentsElementsAssembler32GenerateNewArgumentsElementsImplEv, @function
_ZN2v88internal29NewArgumentsElementsAssembler32GenerateNewArgumentsElementsImplEv:
.LFB25505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-976(%rbp), %r14
	leaq	-720(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1176, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movl	$2, %edi
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal14FixedArrayBase33GetMaxLengthForNewSpaceAllocationENS0_12ElementsKindE@PLT
	leaq	-1104(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movl	%eax, %ebx
	movq	%r10, -1128(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movslq	%ebx, %rsi
	movq	%r12, %rdi
	leaq	-848(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1128(%rbp), %r10
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, -1168(%rbp)
	leaq	-1120(%rbp), %r14
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1128(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1176(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	leaq	-208(%rbp), %rbx
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1184(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	leaq	-592(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMinENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdi
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movl	$1, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-464(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1200(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1136(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$1, %r8d
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	movq	-1192(%rbp), %r11
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-1128(%rbp), %rsi
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-336(%rbp), %r11
	movl	$1, %r8d
	movq	%rax, -1208(%rbp)
	movq	%r11, %rdi
	movq	%r11, -1192(%rbp)
	movq	%r14, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1192(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1192(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1208(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-1144(%rbp), %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastWordToTaggedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r15, %rdx
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	-1192(%rbp), %rax
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, (%rsp)
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1200(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -1192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1192(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1136(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1160(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1184(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1176(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1168(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastWordToTaggedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-1136(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$3, %r9d
	movl	$305, %esi
	movq	%rbx, -64(%rbp)
	movhps	-1128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L120:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25505:
	.size	_ZN2v88internal29NewArgumentsElementsAssembler32GenerateNewArgumentsElementsImplEv, .-_ZN2v88internal29NewArgumentsElementsAssembler32GenerateNewArgumentsElementsImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_NewArgumentsElementsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"NewArgumentsElements"
	.section	.text._ZN2v88internal8Builtins29Generate_NewArgumentsElementsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_NewArgumentsElementsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_NewArgumentsElementsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_NewArgumentsElementsEPNS0_8compiler18CodeAssemblerStateE:
.LFB25501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$74, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$87, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L125
.L122:
	movq	%r13, %rdi
	call	_ZN2v88internal29NewArgumentsElementsAssembler32GenerateNewArgumentsElementsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L122
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25501:
	.size	_ZN2v88internal8Builtins29Generate_NewArgumentsElementsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_NewArgumentsElementsEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins23Generate_ReturnReceiverEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"ReturnReceiver"
	.section	.text._ZN2v88internal8Builtins23Generate_ReturnReceiverEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_ReturnReceiverEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_ReturnReceiverEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_ReturnReceiverEPNS0_8compiler18CodeAssemblerStateE:
.LFB25513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$171, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$169, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L131
.L128:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L128
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25513:
	.size	_ZN2v88internal8Builtins23Generate_ReturnReceiverEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_ReturnReceiverEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23ReturnReceiverAssembler26GenerateReturnReceiverImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ReturnReceiverAssembler26GenerateReturnReceiverImplEv
	.type	_ZN2v88internal23ReturnReceiverAssembler26GenerateReturnReceiverImplEv, @function
_ZN2v88internal23ReturnReceiverAssembler26GenerateReturnReceiverImplEv:
.LFB25517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE25517:
	.size	_ZN2v88internal23ReturnReceiverAssembler26GenerateReturnReceiverImplEv, .-_ZN2v88internal23ReturnReceiverAssembler26GenerateReturnReceiverImplEv
	.section	.text._ZN2v88internal29DebugBreakTrampolineAssembler32GenerateDebugBreakTrampolineImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29DebugBreakTrampolineAssembler32GenerateDebugBreakTrampolineImplEv
	.type	_ZN2v88internal29DebugBreakTrampolineAssembler32GenerateDebugBreakTrampolineImplEv, @function
_ZN2v88internal29DebugBreakTrampolineAssembler32GenerateDebugBreakTrampolineImplEv:
.LFB25526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movl	$86, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15HasInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$56, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-200(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$69, %esi
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-208(%rbp), %r10
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler25GetSharedFunctionInfoCodeENS0_8compiler11SloppyTNodeINS0_18SharedFunctionInfoEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-224(%rbp), %r11
	movq	-216(%rbp), %r9
	movq	%rax, %rsi
	movq	%r11, %r8
	call	_ZN2v88internal8compiler13CodeAssembler14TailCallJSCodeENS1_5TNodeINS0_4CodeEEENS3_INS0_7ContextEEENS3_INS0_10JSFunctionEEENS3_INS0_6ObjectEEENS3_INS0_6Int32TEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L138:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25526:
	.size	_ZN2v88internal29DebugBreakTrampolineAssembler32GenerateDebugBreakTrampolineImplEv, .-_ZN2v88internal29DebugBreakTrampolineAssembler32GenerateDebugBreakTrampolineImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_DebugBreakTrampolineEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"DebugBreakTrampoline"
	.section	.text._ZN2v88internal8Builtins29Generate_DebugBreakTrampolineEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_DebugBreakTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_DebugBreakTrampolineEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_DebugBreakTrampolineEPNS0_8compiler18CodeAssemblerStateE:
.LFB25522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$175, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$88, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L143
.L140:
	movq	%r13, %rdi
	call	_ZN2v88internal29DebugBreakTrampolineAssembler32GenerateDebugBreakTrampolineImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L140
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25522:
	.size	_ZN2v88internal8Builtins29Generate_DebugBreakTrampolineEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_DebugBreakTrampolineEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20RecordWriteAssembler23GenerateRecordWriteImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20RecordWriteAssembler23GenerateRecordWriteImplEv
	.type	_ZN2v88internal20RecordWriteAssembler23GenerateRecordWriteImplEv, @function
_ZN2v88internal20RecordWriteAssembler23GenerateRecordWriteImplEv:
.LFB25568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1264(%rbp), %r14
	leaq	-1008(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1136(%rbp), %rbx
	subq	$1320, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r14, -1312(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-880(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-752(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-624(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -1288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference28heap_is_marking_flag_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %ecx
	movl	$770, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	-1272(%rbp), %r11
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1320(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$1799, %esi
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15PageFromAddressENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1280(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15PageFromAddressENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1288(%rbp), %r10
	movq	-1280(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	movq	%r10, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1272(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -1352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1296(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%rbx, %rcx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %r14
	leaq	-496(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_@PLT
	call	_ZN2v88internal4Heap26store_buffer_mask_constantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-240(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	call	_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	leaq	-368(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r15
	movq	%r11, -1272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1296(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1272(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %eax
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movw	%ax, -112(%rbp)
	movq	-1304(%rbp), %rax
	movl	$1, %r9d
	movq	%r12, %rdi
	movl	$516, %edx
	movq	%rax, -104(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, %r8
	movq	%rax, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-1304(%rbp), %rcx
	movq	-1296(%rbp), %r8
	movl	$1, %r9d
	movw	%dx, -112(%rbp)
	movl	$516, %edx
	movq	%rcx, -104(%rbp)
	movl	$1, %ecx
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1328(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1336(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference16store_buffer_topEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%rbx, %rcx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1328(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_@PLT
	call	_ZN2v88internal4Heap26store_buffer_mask_constantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1280(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	call	_ZN2v88internal17ExternalReference30store_buffer_overflow_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	xorl	%ecx, %ecx
	movq	-1272(%rbp), %rbx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r15
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1336(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-1304(%rbp), %rdx
	movq	-1296(%rbp), %r8
	movl	$1, %r9d
	movw	%cx, -112(%rbp)
	xorl	%ecx, %ecx
	movq	%rdx, -104(%rbp)
	movl	$516, %edx
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1304(%rbp), %rdx
	movl	$5, %esi
	movq	-1296(%rbp), %r8
	movl	$1, %r9d
	movl	$1, %ecx
	movw	%si, -112(%rbp)
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	movl	$516, %edx
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	-1280(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	movq	%rbx, -1272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1344(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1352(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1280(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %ecx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15PageFromAddressENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movl	$4092, %esi
	movq	%r12, %rdi
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1304(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -1320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movl	$31, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21TruncateIntPtrToInt32ENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1320(%rbp), %r9
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$516, %esi
	movq	%rax, %rbx
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	-1304(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15PageFromAddressENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1288(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15PageFromAddressENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$88, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1288(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	call	_ZN2v88internal17ExternalReference41incremental_marking_record_write_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1272(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1320(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1272(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1304(%rbp), %rcx
	movl	$5, %r8d
	movq	-1328(%rbp), %r10
	movw	%r8w, -96(%rbp)
	movl	$5, %edi
	movl	$5, %r9d
	movq	%r15, %rsi
	movq	-1296(%rbp), %r8
	movl	$516, %edx
	movw	%di, -112(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	xorl	%ecx, %ecx
	movw	%r9w, -80(%rbp)
	movl	$3, %r9d
	movq	%r10, -88(%rbp)
	movq	%r10, -1320(%rbp)
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1304(%rbp), %rcx
	movl	$516, %edx
	movl	$5, %r10d
	movw	%r10w, -112(%rbp)
	movq	-1296(%rbp), %r8
	movl	$5, %eax
	movl	$5, %r11d
	movl	$3, %r9d
	movq	%rcx, -104(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-1320(%rbp), %r10
	movl	$1, %ecx
	movw	%r11w, -96(%rbp)
	movq	%rbx, -72(%rbp)
	movq	%r10, -88(%rbp)
	movw	%ax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	-1288(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	40960(%rax), %rsi
	addq	$7784, %rsi
	call	_ZN2v88internal17CodeStubAssembler16IncrementCounterEPNS0_12StatsCounterEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1312(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$1320, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L148:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25568:
	.size	_ZN2v88internal20RecordWriteAssembler23GenerateRecordWriteImplEv, .-_ZN2v88internal20RecordWriteAssembler23GenerateRecordWriteImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_RecordWriteEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"RecordWrite"
	.section	.text._ZN2v88internal8Builtins20Generate_RecordWriteEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_RecordWriteEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_RecordWriteEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_RecordWriteEPNS0_8compiler18CodeAssemblerStateE:
.LFB25564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$355, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%edi, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L153
.L150:
	movq	%r13, %rdi
	call	_ZN2v88internal20RecordWriteAssembler23GenerateRecordWriteImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L150
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25564:
	.size	_ZN2v88internal8Builtins20Generate_RecordWriteEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_RecordWriteEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28EphemeronKeyBarrierAssembler31GenerateEphemeronKeyBarrierImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28EphemeronKeyBarrierAssembler31GenerateEphemeronKeyBarrierImplEv
	.type	_ZN2v88internal28EphemeronKeyBarrierAssembler31GenerateEphemeronKeyBarrierImplEv, @function
_ZN2v88internal28EphemeronKeyBarrierAssembler31GenerateEphemeronKeyBarrierImplEv:
.LFB25577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-496(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	call	_ZN2v88internal17ExternalReference36ephemeron_key_write_barrier_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-528(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-520(%rbp), %rcx
	movw	%dx, -96(%rbp)
	movl	$5, %eax
	leaq	-112(%rbp), %r8
	movq	-504(%rbp), %rdx
	movw	%ax, -112(%rbp)
	movl	$3, %r9d
	movq	%rcx, -104(%rbp)
	movq	-512(%rbp), %rax
	movl	$5, %ecx
	movq	%rdx, -88(%rbp)
	movl	$516, %edx
	movw	%cx, -80(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -528(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %r8d
	movq	-520(%rbp), %rcx
	movq	-504(%rbp), %rdx
	movw	%r8w, -80(%rbp)
	movl	$5, %esi
	movq	-528(%rbp), %r8
	movl	$5, %edi
	movq	-512(%rbp), %rax
	movl	$3, %r9d
	movw	%si, -112(%rbp)
	movq	%rbx, %rsi
	movq	%rcx, -104(%rbp)
	movl	$1, %ecx
	movw	%di, -96(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -88(%rbp)
	movl	$516, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler37CallCFunctionWithCallerSavedRegistersEPNS1_4NodeENS0_11MachineTypeENS0_14SaveFPRegsModeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	40960(%rax), %rsi
	addq	$7784, %rsi
	call	_ZN2v88internal17CodeStubAssembler16IncrementCounterEPNS0_12StatsCounterEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L158:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25577:
	.size	_ZN2v88internal28EphemeronKeyBarrierAssembler31GenerateEphemeronKeyBarrierImplEv, .-_ZN2v88internal28EphemeronKeyBarrierAssembler31GenerateEphemeronKeyBarrierImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_EphemeronKeyBarrierEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"EphemeronKeyBarrier"
	.section	.text._ZN2v88internal8Builtins28Generate_EphemeronKeyBarrierEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_EphemeronKeyBarrierEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_EphemeronKeyBarrierEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_EphemeronKeyBarrierEPNS0_8compiler18CodeAssemblerStateE:
.LFB25573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$458, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$1, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L163
.L160:
	movq	%r13, %rdi
	call	_ZN2v88internal28EphemeronKeyBarrierAssembler31GenerateEphemeronKeyBarrierImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L160
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25573:
	.size	_ZN2v88internal8Builtins28Generate_EphemeronKeyBarrierEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_EphemeronKeyBarrierEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_,"axG",@progbits,_ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_
	.type	_ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_, @function
_ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_:
.LFB25584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$5, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%r9, -384(%rbp)
	movq	16(%rbp), %r9
	movq	%rsi, -344(%rbp)
	movq	%rdi, %rsi
	movq	%r9, -368(%rbp)
	movq	%r8, -352(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	leaq	-336(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-320(%rbp), %r10
	movl	$1, %r8d
	movq	%rbx, -192(%rbp)
	movq	%r10, %rdi
	movq	%r10, -360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%rbx, %r8
	pushq	$0
	movq	-360(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-368(%rbp), %r9
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler20NameDictionaryLookupINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEEPNS4_18CodeAssemblerLabelEPNS4_26TypedCodeAssemblerVariableINS0_7IntPtrTEEESB_NS1_10LookupModeE@PLT
	movq	-360(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$16, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler37LoadAndUntagToWord32FixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-368(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-384(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	-360(%rbp), %r11
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, (%rsp)
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$8, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	-360(%rbp), %r11
	movl	$8, %r9d
	movq	%r12, %rdi
	movl	$1, (%rsp)
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	$16, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-360(%rbp), %r11
	movl	$16, %r9d
	movl	$1, %r8d
	movl	$1, (%rsp)
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-360(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-368(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-360(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	-368(%rbp), %r11
	movq	-360(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, (%rsp)
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-352(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	movq	-344(%rbp), %rax
	movl	$1, %r8d
	movl	$254, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-376(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25584:
	.size	_ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_, .-_ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_
	.section	.text._ZN2v88internal23DeletePropertyAssembler26GenerateDeletePropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23DeletePropertyAssembler26GenerateDeletePropertyImplEv
	.type	_ZN2v88internal23DeletePropertyAssembler26GenerateDeletePropertyImplEv, @function
_ZN2v88internal23DeletePropertyAssembler26GenerateDeletePropertyImplEv:
.LFB25596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-608(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1136(%rbp), %rbx
	subq	$1240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -1240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-1152(%rbp), %rcx
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rax, -1208(%rbp)
	movq	%rcx, -1216(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movl	$8, %edx
	movq	%rbx, -1176(%rbp)
	movq	%r12, %rsi
	leaq	-1120(%rbp), %rbx
	movq	%r14, -1160(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rbx, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-992(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-864(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-736(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-480(%rbp), %rdx
	movq	%rdx, %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r11, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$1024, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1232(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler36IsCustomElementsReceiverInstanceTypeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	pushq	-1224(%rbp)
	movq	%rbx, %rdx
	movq	-1176(%rbp), %r9
	movq	-1184(%rbp), %r8
	pushq	%r13
	movq	%r12, %rdi
	movq	-1216(%rbp), %rcx
	movq	-1160(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler9TryToNameEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%rsi
	movq	%rax, -1200(%rbp)
	popq	%rdi
	jne	.L178
.L170:
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L179
.L172:
	movq	-1168(%rbp), %xmm1
	movq	-1176(%rbp), %rdi
	leaq	-352(%rbp), %rbx
	movhps	-1160(%rbp), %xmm1
	movaps	%xmm1, -1280(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1256(%rbp)
	call	_ZN2v88internal17CodeStubAssembler27CheckForAssociatedProtectorENS0_8compiler11SloppyTNodeINS0_4NameEEEPNS2_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsDictionaryMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17AccessorAssembler33InvalidateValidityCellIfPrototypeEPNS0_8compiler4NodeES4_@PLT
	movq	-1168(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadSlowPropertiesENS0_8compiler11SloppyTNodeINS0_8JSObjectEEE@PLT
	subq	$8, %rsp
	movq	%r14, %r9
	movq	%r15, %rsi
	movq	-1256(%rbp), %r11
	pushq	-1192(%rbp)
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	-1208(%rbp), %r8
	movq	%r15, -1168(%rbp)
	movq	%r11, %rcx
	call	_ZN2v88internal27DeletePropertyBaseAssembler24DeleteDictionaryPropertyENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_14NameDictionaryEEENS3_INS0_4NameEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelESD_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1192(%rbp), %r15
	pushq	%r13
	movq	%r12, %rdi
	movq	-1176(%rbp), %r9
	movq	-1184(%rbp), %r8
	movq	-1216(%rbp), %rcx
	movq	-1248(%rbp), %rdx
	pushq	%r15
	movq	-1160(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler20TryInternalizeStringEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1192(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r15
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$99, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1200(%rbp), %rsi
	movl	$1, %edi
	movq	%rbx, %rdx
	movq	%rax, %r8
	movq	-208(%rbp), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	movq	-1208(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	movq	-1160(%rbp), %rax
	movq	%r15, -352(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15IsPrivateSymbolENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$851, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, -352(%rbp)
	movq	%rbx, %rsi
	movq	-1208(%rbp), %r15
	movq	-1168(%rbp), %xmm0
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	-208(%rbp), %rax
	movq	-1200(%rbp), %r8
	movq	-1240(%rbp), %r14
	movl	$3, %r9d
	movq	%r15, %rcx
	movhps	-1160(%rbp), %xmm0
	movq	%rax, -344(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rcx
	movl	$219, %esi
	movq	%r12, %rdi
	movdqa	-1280(%rbp), %xmm1
	movq	-1200(%rbp), %r8
	movq	%rax, %rdx
	movl	$3, %r9d
	movq	%r14, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-1232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1192(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1184(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1176(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1216(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-80(%rbp), %r14
	movq	%rax, %rsi
	movb	$120, -68(%rbp)
	movabsq	$2338042651316874857, %rcx
	movq	%r14, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movl	$1701080681, -72(%rbp)
	movq	$13, -88(%rbp)
	movb	$0, -67(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L170
	call	_ZdlPv@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-1200(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$18, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC10(%rip), %xmm0
	movl	$25965, %ecx
	movq	%rax, -96(%rbp)
	movq	-1200(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movw	%cx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L172
	call	_ZdlPv@PLT
	jmp	.L172
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25596:
	.size	_ZN2v88internal23DeletePropertyAssembler26GenerateDeletePropertyImplEv, .-_ZN2v88internal23DeletePropertyAssembler26GenerateDeletePropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_DeletePropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"DeleteProperty"
	.section	.text._ZN2v88internal8Builtins23Generate_DeletePropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_DeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_DeletePropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_DeletePropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB25592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$529, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$160, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L185
.L182:
	movq	%r13, %rdi
	call	_ZN2v88internal23DeletePropertyAssembler26GenerateDeletePropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L182
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25592:
	.size	_ZN2v88internal8Builtins23Generate_DeletePropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_DeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal27CopyDataPropertiesAssembler30GenerateCopyDataPropertiesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27CopyDataPropertiesAssembler30GenerateCopyDataPropertiesImplEv
	.type	_ZN2v88internal27CopyDataPropertiesAssembler30GenerateCopyDataPropertiesImplEv, @function
_ZN2v88internal27CopyDataPropertiesAssembler30GenerateCopyDataPropertiesImplEv:
.LFB25617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r9d, %r9d
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEPNS3_18CodeAssemblerLabelEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %xmm1
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	movq	%rax, %rdx
	leaq	-80(%rbp), %r8
	movl	$2, %r9d
	punpcklqdq	%xmm1, %xmm0
	movl	$210, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25617:
	.size	_ZN2v88internal27CopyDataPropertiesAssembler30GenerateCopyDataPropertiesImplEv, .-_ZN2v88internal27CopyDataPropertiesAssembler30GenerateCopyDataPropertiesImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_CopyDataPropertiesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"CopyDataProperties"
	.section	.text._ZN2v88internal8Builtins27Generate_CopyDataPropertiesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_CopyDataPropertiesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_CopyDataPropertiesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_CopyDataPropertiesEPNS0_8compiler18CodeAssemblerStateE:
.LFB25613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$695, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$161, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L195
.L192:
	movq	%r13, %rdi
	call	_ZN2v88internal27CopyDataPropertiesAssembler30GenerateCopyDataPropertiesImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L192
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25613:
	.size	_ZN2v88internal8Builtins27Generate_CopyDataPropertiesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_CopyDataPropertiesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26SetDataPropertiesAssembler29GenerateSetDataPropertiesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SetDataPropertiesAssembler29GenerateSetDataPropertiesImplEv
	.type	_ZN2v88internal26SetDataPropertiesAssembler29GenerateSetDataPropertiesImplEv, @function
_ZN2v88internal26SetDataPropertiesAssembler29GenerateSetDataPropertiesImplEv:
.LFB25626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movl	$1, %r9d
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_132SetOrCopyDataPropertiesAssembler23SetOrCopyDataPropertiesENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEEPNS3_18CodeAssemblerLabelEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %xmm1
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	movq	%rax, %rdx
	leaq	-80(%rbp), %r8
	movl	$2, %r9d
	punpcklqdq	%xmm1, %xmm0
	movl	$250, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L200:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25626:
	.size	_ZN2v88internal26SetDataPropertiesAssembler29GenerateSetDataPropertiesImplEv, .-_ZN2v88internal26SetDataPropertiesAssembler29GenerateSetDataPropertiesImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_SetDataPropertiesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"SetDataProperties"
	.section	.text._ZN2v88internal8Builtins26Generate_SetDataPropertiesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_SetDataPropertiesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_SetDataPropertiesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_SetDataPropertiesEPNS0_8compiler18CodeAssemblerStateE:
.LFB25622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$709, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$162, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L205
.L202:
	movq	%r13, %rdi
	call	_ZN2v88internal26SetDataPropertiesAssembler29GenerateSetDataPropertiesImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L202
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25622:
	.size	_ZN2v88internal8Builtins26Generate_SetDataPropertiesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_SetDataPropertiesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23ForInEnumerateAssembler26GenerateForInEnumerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ForInEnumerateAssembler26GenerateForInEnumerateImplEv
	.type	_ZN2v88internal23ForInEnumerateAssembler26GenerateForInEnumerateImplEv, @function
_ZN2v88internal23ForInEnumerateAssembler26GenerateForInEnumerateImplEv:
.LFB25635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14CheckEnumCacheEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelES6_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-64(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r9d
	movl	$91, %esi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L210:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25635:
	.size	_ZN2v88internal23ForInEnumerateAssembler26GenerateForInEnumerateImplEv, .-_ZN2v88internal23ForInEnumerateAssembler26GenerateForInEnumerateImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_ForInEnumerateEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"ForInEnumerate"
	.section	.text._ZN2v88internal8Builtins23Generate_ForInEnumerateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_ForInEnumerateEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_ForInEnumerateEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_ForInEnumerateEPNS0_8compiler18CodeAssemblerStateE:
.LFB25631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$721, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$491, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L215
.L212:
	movq	%r13, %rdi
	call	_ZN2v88internal23ForInEnumerateAssembler26GenerateForInEnumerateImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L212
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25631:
	.size	_ZN2v88internal8Builtins23Generate_ForInEnumerateEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_ForInEnumerateEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20ForInFilterAssembler23GenerateForInFilterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ForInFilterAssembler23GenerateForInFilterImplEv
	.type	_ZN2v88internal20ForInFilterAssembler23GenerateForInFilterImplEv, @function
_ZN2v88internal20ForInFilterAssembler23GenerateForInFilterImplEv:
.LFB25644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %r9
	movl	$1, %r8d
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler11HasPropertyENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_NS1_21HasPropertyLookupModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6IsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25644:
	.size	_ZN2v88internal20ForInFilterAssembler23GenerateForInFilterImplEv, .-_ZN2v88internal20ForInFilterAssembler23GenerateForInFilterImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_ForInFilterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"ForInFilter"
	.section	.text._ZN2v88internal8Builtins20Generate_ForInFilterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_ForInFilterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_ForInFilterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_ForInFilterEPNS0_8compiler18CodeAssemblerStateE:
.LFB25640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$736, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$492, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L225
.L222:
	movq	%r13, %rdi
	call	_ZN2v88internal20ForInFilterAssembler23GenerateForInFilterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L222
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25640:
	.size	_ZN2v88internal8Builtins20Generate_ForInFilterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_ForInFilterEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18SameValueAssembler21GenerateSameValueImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SameValueAssembler21GenerateSameValueImplEv
	.type	_ZN2v88internal18SameValueAssembler21GenerateSameValueImplEv, @function
_ZN2v88internal18SameValueAssembler21GenerateSameValueImplEv:
.LFB25653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movl	$1, %r9d
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17BranchIfSameValueENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_18CodeAssemblerLabelES7_NS1_13SameValueModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L230:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25653:
	.size	_ZN2v88internal18SameValueAssembler21GenerateSameValueImplEv, .-_ZN2v88internal18SameValueAssembler21GenerateSameValueImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_SameValueEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"SameValue"
	.section	.text._ZN2v88internal8Builtins18Generate_SameValueEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_SameValueEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_SameValueEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_SameValueEPNS0_8compiler18CodeAssemblerStateE:
.LFB25649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$754, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$451, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L235
.L232:
	movq	%r13, %rdi
	call	_ZN2v88internal18SameValueAssembler21GenerateSameValueImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L236
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L232
.L236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25649:
	.size	_ZN2v88internal8Builtins18Generate_SameValueEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_SameValueEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29SameValueNumbersOnlyAssembler32GenerateSameValueNumbersOnlyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29SameValueNumbersOnlyAssembler32GenerateSameValueNumbersOnlyImplEv
	.type	_ZN2v88internal29SameValueNumbersOnlyAssembler32GenerateSameValueNumbersOnlyImplEv, @function
_ZN2v88internal29SameValueNumbersOnlyAssembler32GenerateSameValueNumbersOnlyImplEv:
.LFB25662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17BranchIfSameValueENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_18CodeAssemblerLabelES7_NS1_13SameValueModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L240:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25662:
	.size	_ZN2v88internal29SameValueNumbersOnlyAssembler32GenerateSameValueNumbersOnlyImplEv, .-_ZN2v88internal29SameValueNumbersOnlyAssembler32GenerateSameValueNumbersOnlyImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_SameValueNumbersOnlyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"SameValueNumbersOnly"
	.section	.text._ZN2v88internal8Builtins29Generate_SameValueNumbersOnlyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_SameValueNumbersOnlyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_SameValueNumbersOnlyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_SameValueNumbersOnlyEPNS0_8compiler18CodeAssemblerStateE:
.LFB25658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$768, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$452, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L245
.L242:
	movq	%r13, %rdi
	call	_ZN2v88internal29SameValueNumbersOnlyAssembler32GenerateSameValueNumbersOnlyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L246
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L242
.L246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25658:
	.size	_ZN2v88internal8Builtins29Generate_SameValueNumbersOnlyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_SameValueNumbersOnlyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal36AdaptorWithBuiltinExitFrameAssembler39GenerateAdaptorWithBuiltinExitFrameImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36AdaptorWithBuiltinExitFrameAssembler39GenerateAdaptorWithBuiltinExitFrameImplEv
	.type	_ZN2v88internal36AdaptorWithBuiltinExitFrameAssembler39GenerateAdaptorWithBuiltinExitFrameImplEv, @function
_ZN2v88internal36AdaptorWithBuiltinExitFrameAssembler39GenerateAdaptorWithBuiltinExitFrameImplEv:
.LFB25671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32AddENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	leaq	-128(%rbp), %rsi
	leaq	-112(%rbp), %r8
	movq	%r12, %rdi
	leaq	880+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rdx
	leaq	16+_ZTVN2v88internal28CEntry1ArgvOnStackDescriptorE(%rip), %rcx
	movl	$6, %r9d
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	movq	%r14, %rdx
	movq	%r15, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rax, %xmm0
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L250:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25671:
	.size	_ZN2v88internal36AdaptorWithBuiltinExitFrameAssembler39GenerateAdaptorWithBuiltinExitFrameImplEv, .-_ZN2v88internal36AdaptorWithBuiltinExitFrameAssembler39GenerateAdaptorWithBuiltinExitFrameImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_AdaptorWithBuiltinExitFrameEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"AdaptorWithBuiltinExitFrame"
	.section	.text._ZN2v88internal8Builtins36Generate_AdaptorWithBuiltinExitFrameEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_AdaptorWithBuiltinExitFrameEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_AdaptorWithBuiltinExitFrameEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_AdaptorWithBuiltinExitFrameEPNS0_8compiler18CodeAssemblerStateE:
.LFB25667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$782, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$2, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L255
.L252:
	movq	%r13, %rdi
	call	_ZN2v88internal36AdaptorWithBuiltinExitFrameAssembler39GenerateAdaptorWithBuiltinExitFrameImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L252
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25667:
	.size	_ZN2v88internal8Builtins36Generate_AdaptorWithBuiltinExitFrameEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_AdaptorWithBuiltinExitFrameEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal34AllocateInYoungGenerationAssembler37GenerateAllocateInYoungGenerationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34AllocateInYoungGenerationAssembler37GenerateAllocateInYoungGenerationImplEv
	.type	_ZN2v88internal34AllocateInYoungGenerationAssembler37GenerateAllocateInYoungGenerationImplEv, @function
_ZN2v88internal34AllocateInYoungGenerationAssembler37GenerateAllocateInYoungGenerationImplEv:
.LFB25680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18IsValidPositiveSmiENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9FastCheckENS0_8compiler5TNodeINS0_5BoolTEEE@PLT
	movq	%r12, %rdi
	movabsq	$8589934592, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-56(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$137, %esi
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L260:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25680:
	.size	_ZN2v88internal34AllocateInYoungGenerationAssembler37GenerateAllocateInYoungGenerationImplEv, .-_ZN2v88internal34AllocateInYoungGenerationAssembler37GenerateAllocateInYoungGenerationImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_AllocateInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"AllocateInYoungGeneration"
	.section	.text._ZN2v88internal8Builtins34Generate_AllocateInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_AllocateInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_AllocateInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_AllocateInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE:
.LFB25676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$821, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$80, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L265
.L262:
	movq	%r13, %rdi
	call	_ZN2v88internal34AllocateInYoungGenerationAssembler37GenerateAllocateInYoungGenerationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L262
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25676:
	.size	_ZN2v88internal8Builtins34Generate_AllocateInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_AllocateInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal41AllocateRegularInYoungGenerationAssembler44GenerateAllocateRegularInYoungGenerationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal41AllocateRegularInYoungGenerationAssembler44GenerateAllocateRegularInYoungGenerationImplEv
	.type	_ZN2v88internal41AllocateRegularInYoungGenerationAssembler44GenerateAllocateRegularInYoungGenerationImplEv, @function
_ZN2v88internal41AllocateRegularInYoungGenerationAssembler44GenerateAllocateRegularInYoungGenerationImplEv:
.LFB25690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18IsValidPositiveSmiENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9FastCheckENS0_8compiler5TNodeINS0_5BoolTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-56(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$137, %esi
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L270:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25690:
	.size	_ZN2v88internal41AllocateRegularInYoungGenerationAssembler44GenerateAllocateRegularInYoungGenerationImplEv, .-_ZN2v88internal41AllocateRegularInYoungGenerationAssembler44GenerateAllocateRegularInYoungGenerationImplEv
	.section	.rodata._ZN2v88internal8Builtins41Generate_AllocateRegularInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"AllocateRegularInYoungGeneration"
	.section	.text._ZN2v88internal8Builtins41Generate_AllocateRegularInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_AllocateRegularInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins41Generate_AllocateRegularInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins41Generate_AllocateRegularInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE:
.LFB25686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$833, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$81, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L275
.L272:
	movq	%r13, %rdi
	call	_ZN2v88internal41AllocateRegularInYoungGenerationAssembler44GenerateAllocateRegularInYoungGenerationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L272
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25686:
	.size	_ZN2v88internal8Builtins41Generate_AllocateRegularInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins41Generate_AllocateRegularInYoungGenerationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32AllocateInOldGenerationAssembler35GenerateAllocateInOldGenerationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32AllocateInOldGenerationAssembler35GenerateAllocateInOldGenerationImplEv
	.type	_ZN2v88internal32AllocateInOldGenerationAssembler35GenerateAllocateInOldGenerationImplEv, @function
_ZN2v88internal32AllocateInOldGenerationAssembler35GenerateAllocateInOldGenerationImplEv:
.LFB25699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18IsValidPositiveSmiENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9FastCheckENS0_8compiler5TNodeINS0_5BoolTEEE@PLT
	movq	%r12, %rdi
	movabsq	$8589934592, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-56(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$138, %esi
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L280:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25699:
	.size	_ZN2v88internal32AllocateInOldGenerationAssembler35GenerateAllocateInOldGenerationImplEv, .-_ZN2v88internal32AllocateInOldGenerationAssembler35GenerateAllocateInOldGenerationImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_AllocateInOldGenerationEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"AllocateInOldGeneration"
	.section	.text._ZN2v88internal8Builtins32Generate_AllocateInOldGenerationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_AllocateInOldGenerationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_AllocateInOldGenerationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_AllocateInOldGenerationEPNS0_8compiler18CodeAssemblerStateE:
.LFB25695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$845, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$82, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L285
.L282:
	movq	%r13, %rdi
	call	_ZN2v88internal32AllocateInOldGenerationAssembler35GenerateAllocateInOldGenerationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L282
.L286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25695:
	.size	_ZN2v88internal8Builtins32Generate_AllocateInOldGenerationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_AllocateInOldGenerationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal39AllocateRegularInOldGenerationAssembler42GenerateAllocateRegularInOldGenerationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39AllocateRegularInOldGenerationAssembler42GenerateAllocateRegularInOldGenerationImplEv
	.type	_ZN2v88internal39AllocateRegularInOldGenerationAssembler42GenerateAllocateRegularInOldGenerationImplEv, @function
_ZN2v88internal39AllocateRegularInOldGenerationAssembler42GenerateAllocateRegularInOldGenerationImplEv:
.LFB25708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler18IsValidPositiveSmiENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9FastCheckENS0_8compiler5TNodeINS0_5BoolTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-56(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$138, %esi
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L290:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25708:
	.size	_ZN2v88internal39AllocateRegularInOldGenerationAssembler42GenerateAllocateRegularInOldGenerationImplEv, .-_ZN2v88internal39AllocateRegularInOldGenerationAssembler42GenerateAllocateRegularInOldGenerationImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_AllocateRegularInOldGenerationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"AllocateRegularInOldGeneration"
	.section	.text._ZN2v88internal8Builtins39Generate_AllocateRegularInOldGenerationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_AllocateRegularInOldGenerationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_AllocateRegularInOldGenerationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_AllocateRegularInOldGenerationEPNS0_8compiler18CodeAssemblerStateE:
.LFB25704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$857, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$83, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L295
.L292:
	movq	%r13, %rdi
	call	_ZN2v88internal39AllocateRegularInOldGenerationAssembler42GenerateAllocateRegularInOldGenerationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L296
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L292
.L296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25704:
	.size	_ZN2v88internal8Builtins39Generate_AllocateRegularInOldGenerationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_AllocateRegularInOldGenerationEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins14Generate_AbortEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"Abort"
	.section	.text._ZN2v88internal8Builtins14Generate_AbortEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins14Generate_AbortEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins14Generate_AbortEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins14Generate_AbortEPNS0_8compiler18CodeAssemblerStateE:
.LFB25713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$869, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$163, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L301
.L298:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r9d
	movl	$348, %esi
	movq	%rbx, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L298
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25713:
	.size	_ZN2v88internal8Builtins14Generate_AbortEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins14Generate_AbortEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal14AbortAssembler17GenerateAbortImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14AbortAssembler17GenerateAbortImplEv
	.type	_ZN2v88internal14AbortAssembler17GenerateAbortImplEv, @function
_ZN2v88internal14AbortAssembler17GenerateAbortImplEv:
.LFB25717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r9d
	movl	$348, %esi
	movq	%rbx, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L306:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25717:
	.size	_ZN2v88internal14AbortAssembler17GenerateAbortImplEv, .-_ZN2v88internal14AbortAssembler17GenerateAbortImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_AbortCSAAssertEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"AbortCSAAssert"
	.section	.text._ZN2v88internal8Builtins23Generate_AbortCSAAssertEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_AbortCSAAssertEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_AbortCSAAssertEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_AbortCSAAssertEPNS0_8compiler18CodeAssemblerStateE:
.LFB25722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$874, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$164, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L311
.L308:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r9d
	movl	$350, %esi
	movq	%rbx, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L308
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25722:
	.size	_ZN2v88internal8Builtins23Generate_AbortCSAAssertEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_AbortCSAAssertEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23AbortCSAAssertAssembler26GenerateAbortCSAAssertImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23AbortCSAAssertAssembler26GenerateAbortCSAAssertImplEv
	.type	_ZN2v88internal23AbortCSAAssertAssembler26GenerateAbortCSAAssertImplEv, @function
_ZN2v88internal23AbortCSAAssertAssembler26GenerateAbortCSAAssertImplEv:
.LFB25726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r9d
	movl	$350, %esi
	movq	%rbx, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L316:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25726:
	.size	_ZN2v88internal23AbortCSAAssertAssembler26GenerateAbortCSAAssertImplEv, .-_ZN2v88internal23AbortCSAAssertAssembler26GenerateAbortCSAAssertImplEv
	.section	.text._ZN2v88internal8Builtins64Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins64Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins64Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins64Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE:
.LFB25727:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25727:
	.size	_ZN2v88internal8Builtins64Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins64Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins62Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins62Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins62Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins62Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE:
.LFB25728:
	.cfi_startproc
	endbr64
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25728:
	.size	_ZN2v88internal8Builtins62Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins62Generate_CEntry_Return1_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins67Generate_CEntry_Return1_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins67Generate_CEntry_Return1_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins67Generate_CEntry_Return1_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins67Generate_CEntry_Return1_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE:
.LFB25729:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25729:
	.size	_ZN2v88internal8Builtins67Generate_CEntry_Return1_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins67Generate_CEntry_Return1_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins60Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins60Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins60Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins60Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE:
.LFB25730:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25730:
	.size	_ZN2v88internal8Builtins60Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins60Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins58Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins58Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins58Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins58Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE:
.LFB25731:
	.cfi_startproc
	endbr64
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25731:
	.size	_ZN2v88internal8Builtins58Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins58Generate_CEntry_Return1_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins64Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins64Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins64Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins64Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE:
.LFB25732:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25732:
	.size	_ZN2v88internal8Builtins64Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins64Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins62Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins62Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins62Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins62Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE:
.LFB25733:
	.cfi_startproc
	endbr64
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25733:
	.size	_ZN2v88internal8Builtins62Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins62Generate_CEntry_Return2_DontSaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins67Generate_CEntry_Return2_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins67Generate_CEntry_Return2_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins67Generate_CEntry_Return2_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins67Generate_CEntry_Return2_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE:
.LFB25734:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25734:
	.size	_ZN2v88internal8Builtins67Generate_CEntry_Return2_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins67Generate_CEntry_Return2_DontSaveFPRegs_ArgvInRegister_NoBuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins60Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins60Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins60Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins60Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE:
.LFB25735:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25735:
	.size	_ZN2v88internal8Builtins60Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins60Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_NoBuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins58Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins58Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins58Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins58Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE:
.LFB25736:
	.cfi_startproc
	endbr64
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE25736:
	.size	_ZN2v88internal8Builtins58Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins58Generate_CEntry_Return2_SaveFPRegs_ArgvOnStack_BuiltinExitEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins26Generate_MemCopyUint8Uint8EPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_MemCopyUint8Uint8EPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins26Generate_MemCopyUint8Uint8EPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins26Generate_MemCopyUint8Uint8EPNS0_14MacroAssemblerE:
.LFB25737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$166, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	.cfi_endproc
.LFE25737:
	.size	_ZN2v88internal8Builtins26Generate_MemCopyUint8Uint8EPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins26Generate_MemCopyUint8Uint8EPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins27Generate_MemCopyUint16Uint8EPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_MemCopyUint16Uint8EPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins27Generate_MemCopyUint16Uint8EPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins27Generate_MemCopyUint16Uint8EPNS0_14MacroAssemblerE:
.LFB33964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$166, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	.cfi_endproc
.LFE33964:
	.size	_ZN2v88internal8Builtins27Generate_MemCopyUint16Uint8EPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins27Generate_MemCopyUint16Uint8EPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins16Generate_MemMoveEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MemMoveEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins16Generate_MemMoveEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins16Generate_MemMoveEPNS0_14MacroAssemblerE:
.LFB33966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$166, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	.cfi_endproc
.LFE33966:
	.size	_ZN2v88internal8Builtins16Generate_MemMoveEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins16Generate_MemMoveEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEv
	.type	_ZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEv, @function
_ZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEv:
.LFB25748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-416(%rbp), %r14
	leaq	-288(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-544(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	subq	$8, %rsp
	leaq	-128(%rbp), %r10
	movq	%r12, %rdi
	movq	%r12, %xmm0
	pushq	%r14
	movq	%r10, %r9
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_(%rip), %rax
	movhps	-600(%rbp), %xmm0
	movq	%rax, %xmm2
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	movq	-608(%rbp), %xmm1
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_20GetPropertyAssembler23GenerateGetPropertyImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_(%rip), %rax
	pushq	%r15
	leaq	-160(%rbp), %r8
	pushq	%r13
	movq	%rax, %xmm3
	movq	%xmm1, %rdx
	movq	%xmm1, %rsi
	movaps	%xmm0, -160(%rbp)
	movq	%rcx, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%r10, -656(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rcx, %xmm0
	movq	%rbx, %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%r8, -648(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%r12, -128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23TryPrototypeChainLookupEPNS0_8compiler4NodeES4_S4_RKSt8functionIFvS4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelES7_EESB_S7_S7_S7_@PLT
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm4
	movl	$2, %r9d
	movq	%r12, %rdi
	movq	-600(%rbp), %rcx
	movq	%rax, %rdx
	leaq	-96(%rbp), %rax
	movq	-608(%rbp), %xmm0
	movq	%rax, %r8
	movl	$224, %esi
	movq	%rax, -616(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-576(%rbp), %r11
	movl	$99, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -640(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-616(%rbp), %rsi
	movl	$1, %edi
	movq	-600(%rbp), %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movl	$1, %ecx
	pushq	%rsi
	movq	-560(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	leaq	-592(%rbp), %rbx
	movq	%rdx, -592(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, -632(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-640(%rbp), %r11
	movl	$852, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-600(%rbp), %rcx
	movq	%rbx, %rsi
	movq	-608(%rbp), %xmm1
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdi
	movq	-560(%rbp), %rax
	movq	-616(%rbp), %r8
	movdqa	%xmm1, %xmm0
	movq	%rdi, -592(%rbp)
	movq	%r12, %rdi
	movl	$4, %r9d
	movhps	-624(%rbp), %xmm0
	movq	%rax, -584(%rbp)
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-632(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-112(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L334
	movq	-656(%rbp), %r10
	movl	$3, %edx
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	*%rax
.L334:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L335
	movq	-648(%rbp), %rsi
	movl	$3, %edx
	movq	%rsi, %rdi
	call	*%rax
.L335:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L344:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25748:
	.size	_ZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEv, .-_ZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_GetPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"GetProperty"
	.section	.text._ZN2v88internal8Builtins20Generate_GetPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_GetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_GetPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_GetPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB25744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$950, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$710, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L349
.L346:
	movq	%r13, %rdi
	call	_ZN2v88internal20GetPropertyAssembler23GenerateGetPropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L346
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25744:
	.size	_ZN2v88internal8Builtins20Generate_GetPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_GetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEv
	.type	_ZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEv, @function
_ZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEv:
.LFB25762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-544(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-416(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$760, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-672(%rbp), %rcx
	movq	%rax, -752(%rbp)
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, -728(%rbp)
	subq	$8, %rsp
	movq	%r12, %xmm0
	movq	-768(%rbp), %r15
	pushq	%r14
	leaq	-128(%rbp), %r10
	movq	%r12, %rdi
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	pushq	%r13
	movq	-736(%rbp), %rdx
	movhps	-728(%rbp), %xmm0
	movq	%rax, %xmm2
	pushq	%r15
	movq	%r10, %r9
	movaps	%xmm0, -160(%rbp)
	movq	%rcx, %xmm0
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_S4_S4_S4_PNS2_18CodeAssemblerLabelES6_EZNS1_32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlS4_S4_S4_S4_S4_S6_S6_E0_E9_M_invokeERKSt9_Any_dataOS4_SE_SE_SE_SE_OS6_SF_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEvEUlPNS2_8compiler4NodeES6_S6_S6_S6_PNS4_18CodeAssemblerLabelES8_E0_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	movq	-760(%rbp), %rsi
	leaq	-160(%rbp), %r8
	movaps	%xmm0, -144(%rbp)
	movq	%rcx, %xmm0
	movq	%rbx, %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%r10, -800(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%r8, -792(%rbp)
	movq	%r12, -128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23TryPrototypeChainLookupEPNS0_8compiler4NodeES4_S4_RKSt8functionIFvS4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelES7_EESB_S7_S7_S7_@PLT
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-288(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-752(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	-728(%rbp), %rdx
	movq	%rax, %rcx
	movl	$1, %r8d
	movl	$172, %esi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %xmm4
	movl	$289, %esi
	movq	-760(%rbp), %xmm0
	movq	-736(%rbp), %xmm1
	movq	-744(%rbp), %r8
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	-728(%rbp), %rcx
	movhps	-752(%rbp), %xmm0
	movl	$4, %r9d
	punpcklqdq	%xmm4, %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-704(%rbp), %r11
	movl	$99, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -760(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-744(%rbp), %rsi
	movl	$1, %edi
	movq	-728(%rbp), %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movl	$1, %ecx
	pushq	%rsi
	movq	-688(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	leaq	-720(%rbp), %rbx
	movq	%rdx, -720(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15IsPrivateSymbolENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-760(%rbp), %r11
	movl	$852, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-784(%rbp), %xmm0
	movq	%rbx, %rsi
	movq	-736(%rbp), %xmm1
	movq	%rax, %rdx
	movq	-728(%rbp), %rcx
	movq	-688(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdi
	movq	-744(%rbp), %r8
	movl	$4, %r9d
	movaps	%xmm0, -80(%rbp)
	movhps	-752(%rbp), %xmm1
	movq	%rdi, -720(%rbp)
	movq	%r12, %rdi
	movq	%rax, -712(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-112(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L352
	movq	-800(%rbp), %r10
	movl	$3, %edx
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	*%rax
.L352:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L353
	movq	-792(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L353:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L362
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L362:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25762:
	.size	_ZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEv, .-_ZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_GetPropertyWithReceiverEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"GetPropertyWithReceiver"
	.section	.text._ZN2v88internal8Builtins32Generate_GetPropertyWithReceiverEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_GetPropertyWithReceiverEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_GetPropertyWithReceiverEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_GetPropertyWithReceiverEPNS0_8compiler18CodeAssemblerStateE:
.LFB25758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1005, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$711, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L367
.L364:
	movq	%r13, %rdi
	call	_ZN2v88internal32GetPropertyWithReceiverAssembler35GenerateGetPropertyWithReceiverImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L368
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L364
.L368:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25758:
	.size	_ZN2v88internal8Builtins32Generate_GetPropertyWithReceiverEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_GetPropertyWithReceiverEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins20Generate_SetPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"SetProperty"
	.section	.text._ZN2v88internal8Builtins20Generate_SetPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_SetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_SetPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_SetPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB25769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1073, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$712, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L373
.L370:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-64(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rax, %r8
	movl	$1, %r9d
	movq	%r13, %rsi
	call	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L374
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L370
.L374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25769:
	.size	_ZN2v88internal8Builtins20Generate_SetPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_SetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20SetPropertyAssembler23GenerateSetPropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SetPropertyAssembler23GenerateSetPropertyImplEv
	.type	_ZN2v88internal20SetPropertyAssembler23GenerateSetPropertyImplEv, @function
_ZN2v88internal20SetPropertyAssembler23GenerateSetPropertyImplEv:
.LFB25773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %r8
	popq	%r12
	movl	$1, %r9d
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal26KeyedStoreGenericGenerator11SetPropertyEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_6ObjectEEES9_S9_NS0_12LanguageModeE@PLT
	.cfi_endproc
.LFE25773:
	.size	_ZN2v88internal20SetPropertyAssembler23GenerateSetPropertyImplEv, .-_ZN2v88internal20SetPropertyAssembler23GenerateSetPropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"SetPropertyInLiteral"
	.section	.text._ZN2v88internal8Builtins29Generate_SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateE:
.LFB25778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1087, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$713, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L381
.L378:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-64(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rax, %r8
	movq	%r13, %rsi
	call	_ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L378
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25778:
	.size	_ZN2v88internal8Builtins29Generate_SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29SetPropertyInLiteralAssembler32GenerateSetPropertyInLiteralImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29SetPropertyInLiteralAssembler32GenerateSetPropertyInLiteralImplEv
	.type	_ZN2v88internal29SetPropertyInLiteralAssembler32GenerateSetPropertyInLiteralImplEv, @function
_ZN2v88internal29SetPropertyInLiteralAssembler32GenerateSetPropertyInLiteralImplEv:
.LFB25782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal26KeyedStoreGenericGenerator20SetPropertyInLiteralEPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_7ContextEEENS5_INS0_8JSObjectEEENS5_INS0_6ObjectEEESB_@PLT
	.cfi_endproc
.LFE25782:
	.size	_ZN2v88internal29SetPropertyInLiteralAssembler32GenerateSetPropertyInLiteralImplEv, .-_ZN2v88internal29SetPropertyInLiteralAssembler32GenerateSetPropertyInLiteralImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE:
.LFB33820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE33820:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_StackCheckEPNS0_14MacroAssemblerE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.quad	8439872597045896555
	.quad	7020584489251334510
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
