	.file	"interpreter-assembler.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB26023:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26023:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB26026:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE26026:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB26027:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L12
	cmpl	$3, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L17
.L13:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26027:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal11interpreter20InterpreterAssemblerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev
	.type	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev, @function
_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev:
.LFB21901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler33UnregisterCallGenerationCallbacksEv@PLT
	leaq	88(%r12), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	leaq	56(%r12), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	40(%r12), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	leaq	24(%r12), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	.cfi_endproc
.LFE21901:
	.size	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev, .-_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev
	.globl	_ZN2v88internal11interpreter20InterpreterAssemblerD1Ev
	.set	_ZN2v88internal11interpreter20InterpreterAssemblerD1Ev,_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv:
.LFB21903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable7IsBoundEv@PLT
	testb	%al, %al
	jne	.L25
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler22LoadParentFramePointerEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
.L26:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movzbl	16(%r12), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L26
	cmpb	$0, 105(%r12)
	je	.L26
	cmpb	$0, 106(%r12)
	jne	.L26
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler22LoadParentFramePointerEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 106(%r12)
	jmp	.L26
	.cfi_endproc
.LFE21903:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv, .-_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv:
.LFB21905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal11interpreter8Register15bytecode_offsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r12, %rdi
	movq	%rax, %r13
	movl	$-5, %eax
	subl	%ebx, %eax
	leal	4(,%rax,8), %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$516, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	cmpb	$1, 17(%r12)
	movq	%rax, %r13
	je	.L32
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %r13
.L32:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21905:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv, .-_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv:
.LFB21904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	leaq	56(%r12), %r13
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L36
	cmpb	$0, 105(%r12)
	jne	.L39
.L36:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L36
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L36
	.cfi_endproc
.LFE21904:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv, .-_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv:
.LFB21906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	56(%r12), %r14
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L42
	cmpb	$0, 105(%r12)
	jne	.L49
.L42:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	cmpb	$1, 17(%r12)
	movq	%rax, %r13
	jne	.L50
.L44:
	call	_ZN2v88internal11interpreter8Register15bytecode_offsetEv@PLT
	movl	$-5, %ebx
	movq	%r12, %rdi
	subl	%eax, %ebx
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	sall	$3, %ebx
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movslq	%ebx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$4, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21TruncateIntPtrToInt32ENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	leal	4(%rbx), %esi
	movq	%r12, %rdi
	movslq	%esi, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	addq	$8, %rsp
	movq	%r13, %r8
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	movl	$4, %esi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L46
	cmpb	$0, 105(%r12)
	jne	.L51
.L46:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %r13
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L42
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L46
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L46
	.cfi_endproc
.LFE21906:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv, .-_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE:
.LFB21898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	leaq	-112(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movb	%r13b, 17(%r12)
	movl	$5, %edx
	leaq	24(%r12), %rdi
	movb	%bl, 16(%r12)
	movq	%r12, %rsi
	leaq	-80(%rbp), %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %edx
	leaq	40(%r12), %rdi
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %edx
	leaq	56(%r12), %rdi
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %edx
	leaq	72(%r12), %rdi
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	88(%r12), %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r12, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rdx
	movq	%r12, -112(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movl	$16777216, 104(%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rdx
	movq	%rax, %xmm2
	movaps	%xmm0, -64(%rbp)
	movq	%rdx, %xmm0
	movq	%r13, %rdx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler31RegisterCallGenerationCallbacksERKSt8functionIFvvEES7_@PLT
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L53
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L53:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L54
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L54:
	movl	%ebx, %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L55
	cmpb	$-85, %bl
	je	.L55
	cmpb	$-80, %bl
	je	.L55
.L52:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv
	jmp	.L52
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21898:
	.size	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE, .-_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE
	.globl	_ZN2v88internal11interpreter20InterpreterAssemblerC1EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE
	.set	_ZN2v88internal11interpreter20InterpreterAssemblerC1EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE,_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB26022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movzbl	16(%rbx), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L73
	movb	$0, 107(%rbx)
	movb	$1, 105(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv
	movb	$0, 107(%rbx)
	movb	$1, 105(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26022:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal11interpreter20InterpreterAssemblerC4EPNS2_8compiler18CodeAssemblerStateENS3_8BytecodeENS3_12OperandScaleEEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler26BytecodeArrayTaggedPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeArrayTaggedPointerEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeArrayTaggedPointerEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeArrayTaggedPointerEv:
.LFB21907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	40(%rdi), %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 107(%rdi)
	je	.L77
.L75:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	%rdi, %rbx
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%rbx, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%rbx)
	jmp	.L75
	.cfi_endproc
.LFE21907:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeArrayTaggedPointerEv, .-_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeArrayTaggedPointerEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv:
.LFB21908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	leaq	72(%r12), %r13
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L80
	cmpb	$0, 105(%r12)
	jne	.L83
.L80:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L80
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L80
	.cfi_endproc
.LFE21908:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv, .-_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler23GetAccumulatorUncheckedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler23GetAccumulatorUncheckedEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler23GetAccumulatorUncheckedEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler23GetAccumulatorUncheckedEv:
.LFB21909:
	.cfi_startproc
	endbr64
	addq	$88, %rdi
	jmp	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	.cfi_endproc
.LFE21909:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler23GetAccumulatorUncheckedEv, .-_ZN2v88internal11interpreter20InterpreterAssembler23GetAccumulatorUncheckedEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv:
.LFB21910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	orb	$1, 104(%rdi)
	leaq	88(%rdi), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler25TaggedPoisonOnSpeculationENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21910:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv, .-_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE:
.LFB21911:
	.cfi_startproc
	endbr64
	orb	$2, 104(%rdi)
	addq	$88, %rdi
	jmp	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE21911:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv:
.LFB21912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$2, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21912:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv, .-_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE, @function
_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE:
.LFB21913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	.cfi_endproc
.LFE21913:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE, .-_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE, @function
_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE:
.LFB21914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	leaq	-336(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	movq	%r14, %xmm0
	pushq	%r12
	movq	%r13, %xmm1
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -56
	leaq	-208(%rbp), %rbx
	subq	$360, %rsp
	movq	%rdx, -376(%rbp)
	movl	$7, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-376(%rbp), %r9
	movq	%r9, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-80(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movdqa	-400(%rbp), %xmm0
	movl	$1, %r8d
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-376(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, -376(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-376(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, -376(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-376(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21914:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE, .-_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE, @function
_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE:
.LFB21921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$7, %edx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-240(%rbp), %r13
	movq	%r14, %xmm0
	pushq	%r12
	movq	%r13, %xmm1
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -256(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r15, %rcx
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-80(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movdqa	-256(%rbp), %xmm0
	movl	$1, %r8d
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	-256(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21921:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE, .-_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationEPNS0_8compiler4NodeE:
.LFB21922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21922:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationENS1_8RegisterE, @function
_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationENS1_8RegisterE:
.LFB21923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movl	$-5, %esi
	subl	%r8d, %esi
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21923:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationENS1_8RegisterE, .-_ZN2v88internal11interpreter20InterpreterAssembler16RegisterLocationENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler19RegisterFrameOffsetEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler19RegisterFrameOffsetEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler19RegisterFrameOffsetEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler19RegisterFrameOffsetEPNS0_8compiler4NodeE:
.LFB21924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21924:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler19RegisterFrameOffsetEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler19RegisterFrameOffsetEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterEPNS0_8compiler4NodeE:
.LFB21925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	24(%r12), %r14
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable7IsBoundEv@PLT
	testb	%al, %al
	jne	.L107
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler22LoadParentFramePointerEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
.L108:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	xorl	%ecx, %ecx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movzbl	16(%r12), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L108
	cmpb	$0, 105(%r12)
	je	.L108
	cmpb	$0, 106(%r12)
	jne	.L108
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler22LoadParentFramePointerEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 106(%r12)
	jmp	.L108
	.cfi_endproc
.LFE21925:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE, @function
_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE:
.LFB21926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movl	$-5, %esi
	subl	%r8d, %esi
	sall	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	movslq	%esi, %rsi
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$2, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	.cfi_endproc
.LFE21926:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE, .-_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler20LoadAndUntagRegisterENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler20LoadAndUntagRegisterENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler20LoadAndUntagRegisterENS1_8RegisterE, @function
_ZN2v88internal11interpreter20InterpreterAssembler20LoadAndUntagRegisterENS1_8RegisterE:
.LFB21927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r12, %rdi
	movq	%rax, %r13
	movl	$-5, %eax
	subl	%ebx, %eax
	leal	4(,%rax,8), %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movl	$516, %esi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	.cfi_endproc
.LFE21927:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler20LoadAndUntagRegisterENS1_8RegisterE, .-_ZN2v88internal11interpreter20InterpreterAssembler20LoadAndUntagRegisterENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi:
.LFB21935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movslq	%edx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21935:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi, .-_ZN2v88internal11interpreter20InterpreterAssembler28LoadRegisterFromRegisterListERKNS2_15RegListNodePairEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler30RegisterLocationInRegisterListERKNS2_15RegListNodePairEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler30RegisterLocationInRegisterListERKNS2_15RegListNodePairEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler30RegisterLocationInRegisterListERKNS2_15RegListNodePairEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler30RegisterLocationInRegisterListERKNS2_15RegListNodePairEi:
.LFB21936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movslq	%edx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21936:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler30RegisterLocationInRegisterListERKNS2_15RegListNodePairEi, .-_ZN2v88internal11interpreter20InterpreterAssembler30RegisterLocationInRegisterListERKNS2_15RegListNodePairEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeENS1_8RegisterE, @function
_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeENS1_8RegisterE:
.LFB21937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movl	$-5, %esi
	pushq	%r13
	subl	%edx, %esi
	pushq	%r12
	sall	$3, %esi
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movslq	%esi, %rsi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	.cfi_endproc
.LFE21937:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeENS1_8RegisterE, .-_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeES5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeES5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeES5_:
.LFB21938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	24(%r12), %r15
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable7IsBoundEv@PLT
	testb	%al, %al
	jne	.L124
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler22LoadParentFramePointerEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
.L125:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movzbl	16(%r12), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L125
	cmpb	$0, 105(%r12)
	je	.L125
	cmpb	$0, 106(%r12)
	jne	.L125
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler22LoadParentFramePointerEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 106(%r12)
	jmp	.L125
	.cfi_endproc
.LFE21938:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeES5_, .-_ZN2v88internal11interpreter20InterpreterAssembler13StoreRegisterEPNS0_8compiler4NodeES5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12NextRegisterEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12NextRegisterEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12NextRegisterEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler12NextRegisterEPNS0_8compiler4NodeE:
.LFB21942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	$-1, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21942:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12NextRegisterEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler12NextRegisterEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler13OperandOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler13OperandOffsetEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler13OperandOffsetEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler13OperandOffsetEi:
.LFB21943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21943:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler13OperandOffsetEi, .-_ZN2v88internal11interpreter20InterpreterAssembler13OperandOffsetEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedByteEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedByteEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedByteEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedByteEiNS0_15LoadSensitivityE:
.LFB21944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	56(%r12), %r14
	subq	$8, %rsp
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L136
	cmpb	$0, 105(%r12)
	jne	.L140
.L136:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L141
.L138:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L136
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L136
	.cfi_endproc
.LFE21944:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedByteEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedByteEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedByteEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedByteEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedByteEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedByteEiNS0_15LoadSensitivityE:
.LFB21945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	56(%r12), %r14
	subq	$8, %rsp
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L144
	cmpb	$0, 105(%r12)
	jne	.L148
.L144:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L149
.L146:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L144
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L144
	.cfi_endproc
.LFE21945:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedByteEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedByteEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandReadUnalignedEiNS0_11MachineTypeENS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandReadUnalignedEiNS0_11MachineTypeENS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandReadUnalignedEiNS0_11MachineTypeENS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandReadUnalignedEiNS0_11MachineTypeENS0_15LoadSensitivityE:
.LFB21946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movl	%ecx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$3, %dl
	je	.L164
	cmpb	$4, %dl
	jne	.L170
	movq	$24, -144(%rbp)
	movl	$3, %eax
	movl	$2, -148(%rbp)
	movl	$4, -116(%rbp)
.L151:
	movzbl	%dh, %edx
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %r15
	subl	$2, %edx
	movaps	%xmm0, -96(%rbp)
	andl	$253, %edx
	movaps	%xmm0, -80(%rbp)
	setne	%dl
	addl	%eax, %esi
	leaq	40(%rbx), %rax
	xorl	%r12d, %r12d
	movq	%rax, -104(%rbp)
	leaq	56(%rbx), %rax
	leal	2(%rdx), %ecx
	movq	%rax, -112(%rbp)
	movslq	%esi, %rax
	movb	%cl, -150(%rbp)
	movq	%rax, -128(%rbp)
.L161:
	testq	%r12, %r12
	je	.L171
	movl	$770, %r14d
.L154:
	movq	-128(%rbp), %rsi
	movq	%rbx, %rdi
	subq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%rbx), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L156
	cmpb	$0, 105(%rbx)
	jne	.L172
.L156:
	movq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%rbx)
	movq	%rax, %r13
	je	.L173
.L158:
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-120(%rbp), %r8d
	movq	%r13, %rcx
	movl	%r14d, %esi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, (%r15,%r12,8)
	addq	$1, %r12
	cmpl	%r12d, -116(%rbp)
	jg	.L161
	movq	-144(%rbp), %rax
	movslq	-148(%rbp), %r13
	movl	$8, %r14d
	movq	-96(%rbp,%rax), %r12
.L162:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	(%r15,%r13,8), %rsi
	movq	%rbx, %rdi
	subq	$1, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShlENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rax, %r12
	cmpl	$-1, %r13d
	jne	.L162
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movb	$2, %r14b
	movzwl	-150(%rbp), %ecx
	movl	%r14d, %eax
	movb	%cl, %ah
	movl	%eax, %r14d
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L173:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%rbx, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-136(%rbp), %rdx
	movl	$2, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	-104(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%rbx)
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, -136(%rbp)
	jne	.L156
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L164:
	movq	$8, -144(%rbp)
	movl	$1, %eax
	movl	$0, -148(%rbp)
	movl	$2, -116(%rbp)
	jmp	.L151
.L170:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21946:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandReadUnalignedEiNS0_11MachineTypeENS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandReadUnalignedEiNS0_11MachineTypeENS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandUnsignedShortEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandUnsignedShortEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandUnsignedShortEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandUnsignedShortEiNS0_15LoadSensitivityE:
.LFB21947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	56(%r12), %r14
	subq	$8, %rsp
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L177
	cmpb	$0, 105(%r12)
	jne	.L181
.L177:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L182
.L179:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L177
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L177
	.cfi_endproc
.LFE21947:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandUnsignedShortEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler28BytecodeOperandUnsignedShortEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandSignedShortEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandSignedShortEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandSignedShortEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandSignedShortEiNS0_15LoadSensitivityE:
.LFB21948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	56(%r12), %r14
	subq	$8, %rsp
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L185
	cmpb	$0, 105(%r12)
	jne	.L189
.L185:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L190
.L187:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L185
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L185
	.cfi_endproc
.LFE21948:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandSignedShortEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandSignedShortEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedQuadEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedQuadEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedQuadEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedQuadEiNS0_15LoadSensitivityE:
.LFB21949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	56(%r12), %r14
	subq	$8, %rsp
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L193
	cmpb	$0, 105(%r12)
	jne	.L197
.L193:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L198
.L195:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L193
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L193
	.cfi_endproc
.LFE21949:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedQuadEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler27BytecodeOperandUnsignedQuadEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedQuadEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedQuadEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedQuadEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedQuadEiNS0_15LoadSensitivityE:
.LFB21950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	56(%r12), %r14
	subq	$8, %rsp
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L201
	cmpb	$0, 105(%r12)
	jne	.L205
.L201:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L206
.L203:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L201
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L201
	.cfi_endproc
.LFE21950:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedQuadEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler25BytecodeOperandSignedQuadEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE:
.LFB21951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	cmpb	$2, %dl
	je	.L208
	ja	.L209
	testb	%dl, %dl
	je	.L210
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L225
.L212:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L226
.L214:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L221
	cmpb	$0, 105(%r12)
	je	.L221
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	je	.L227
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L228
.L223:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L217
	cmpb	$0, 105(%r12)
	je	.L217
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	je	.L229
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L230
.L219:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	cmpb	$0, 105(%r12)
	je	.L212
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L212
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L228:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L226:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L230:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L219
.L227:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L221
.L229:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L217
.L210:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21951:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	.section	.rodata._ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"i < NumberOfOperands(bytecode)"
	.section	.rodata._ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi:
.LFB21928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L234
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	movl	$2, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21928:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi, .-_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi:
.LFB21939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	%edx, %esi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L238
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	movl	$2, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21939:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi, .-_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler31StoreRegisterPairAtOperandIndexEPNS0_8compiler4NodeES5_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler31StoreRegisterPairAtOperandIndexEPNS0_8compiler4NodeES5_i
	.type	_ZN2v88internal11interpreter20InterpreterAssembler31StoreRegisterPairAtOperandIndexEPNS0_8compiler4NodeES5_i, @function
_ZN2v88internal11interpreter20InterpreterAssembler31StoreRegisterPairAtOperandIndexEPNS0_8compiler4NodeES5_i:
.LFB21940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	%ecx, %esi
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L242
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	movl	$2, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	$-1, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21940:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler31StoreRegisterPairAtOperandIndexEPNS0_8compiler4NodeES5_i, .-_ZN2v88internal11interpreter20InterpreterAssembler31StoreRegisterPairAtOperandIndexEPNS0_8compiler4NodeES5_i
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler30LoadRegisterPairAtOperandIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler30LoadRegisterPairAtOperandIndexEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler30LoadRegisterPairAtOperandIndexEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler30LoadRegisterPairAtOperandIndexEi:
.LFB21929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L246
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	movl	$2, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	$-1, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21929:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler30LoadRegisterPairAtOperandIndexEi, .-_ZN2v88internal11interpreter20InterpreterAssembler30LoadRegisterPairAtOperandIndexEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i
	.type	_ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i, @function
_ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i:
.LFB21941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %r8d
	jge	.L250
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rsi, %r15
	movl	%r8d, %esi
	andl	$127, %eax
	movq	%rdi, %r12
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%r8d, %rdx
	movq	(%rcx,%rax,8), %rax
	movl	$2, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	$-1, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	$-1, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	addq	$24, %rsp
	movq	%r13, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21941:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i, .-_ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE:
.LFB21952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	cmpb	$2, %dl
	je	.L252
	ja	.L253
	testb	%dl, %dl
	je	.L254
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L269
.L256:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L270
.L258:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L265
	cmpb	$0, 105(%r12)
	je	.L265
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	je	.L271
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L272
.L267:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movzbl	17(%rdi), %edx
	movzbl	16(%rdi), %edi
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L261
	cmpb	$0, 105(%r12)
	je	.L261
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	je	.L273
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L274
.L263:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	cmpb	$0, 105(%r12)
	je	.L256
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L256
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L272:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L270:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L274:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L263
.L271:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L265
.L273:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L261
.L254:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21952:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi:
.LFB21934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	0(%r13,%rdx,4), %esi
	jge	.L277
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %r14
	movq	%rdi, %r12
	movl	%esi, %ebx
	andl	$127, %eax
	movl	$2, %ecx
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%r14,%rax,8), %rax
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movzbl	16(%r12), %edx
	leal	1(%rbx), %esi
	movq	%rax, %r15
	movzbl	17(%r12), %eax
	cmpl	0(%r13,%rdx,4), %esi
	jge	.L277
	shrq	%rax
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%r14,%rax,8), %rax
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	addq	$8, %rsp
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	movq	%r15, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21934:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi, .-_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler20BytecodeOperandCountEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler20BytecodeOperandCountEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler20BytecodeOperandCountEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler20BytecodeOperandCountEi:
.LFB21953:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %edx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movzbl	17(%rdi), %eax
	cmpl	%esi, (%rcx,%rdx,4)
	jle	.L284
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	.p2align 4,,10
	.p2align 3
.L284:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21953:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler20BytecodeOperandCountEi, .-_ZN2v88internal11interpreter20InterpreterAssembler20BytecodeOperandCountEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi:
.LFB21954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	16(%rdi), %edx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movzbl	17(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	%esi, (%rcx,%rdx,4)
	jle	.L288
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21954:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi, .-_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi:
.LFB21955:
	.cfi_startproc
	endbr64
	movzbl	16(%rdi), %edx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movzbl	17(%rdi), %eax
	cmpl	%esi, (%rcx,%rdx,4)
	jle	.L294
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	.p2align 4,,10
	.p2align 3
.L294:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21955:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi, .-_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi:
.LFB21956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L298
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21956:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi, .-_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler22BytecodeOperandUImmSmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler22BytecodeOperandUImmSmiEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler22BytecodeOperandUImmSmiEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler22BytecodeOperandUImmSmiEi:
.LFB21957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L302
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21957:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler22BytecodeOperandUImmSmiEi, .-_ZN2v88internal11interpreter20InterpreterAssembler22BytecodeOperandUImmSmiEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandImmEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandImmEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandImmEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandImmEi:
.LFB21958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %r8d
	cmpl	%esi, (%rax,%rdx,4)
	jle	.L322
	movq	%r8, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	movq	%rdx, %rdi
	shrq	%rax
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rcx
	movzbl	(%rcx,%rdx), %eax
	cmpb	$2, %al
	je	.L305
	ja	.L306
	testb	%al, %al
	je	.L307
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L323
.L309:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L324
.L311:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE(%rip), %esi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rdx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L306:
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L325
.L317:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L326
.L319:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE(%rip), %esi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rdx
.L321:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L327
.L313:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L328
.L315:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE(%rip), %esi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rdx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L327:
	cmpb	$0, 105(%r12)
	je	.L313
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L313
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L323:
	cmpb	$0, 105(%r12)
	je	.L309
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L309
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L325:
	cmpb	$0, 105(%r12)
	je	.L317
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L317
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L324:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L326:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L319
.L307:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21958:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandImmEi, .-_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandImmEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi:
.LFB21959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %r8d
	cmpl	(%rax,%rdx,4), %esi
	jge	.L348
	movq	%r8, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	movq	%rdx, %rdi
	shrq	%rax
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rcx
	movzbl	(%rcx,%rdx), %eax
	cmpb	$2, %al
	je	.L331
	ja	.L332
	testb	%al, %al
	je	.L333
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L349
.L335:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L350
.L337:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, %rsi
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L332:
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L351
.L344:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L352
.L346:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, %rsi
.L338:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L353
.L340:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L354
.L342:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, %rsi
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L353:
	cmpb	$0, 105(%r12)
	je	.L340
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L340
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L349:
	cmpb	$0, 105(%r12)
	je	.L335
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L335
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L351:
	cmpb	$0, 105(%r12)
	je	.L344
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L344
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L350:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L352:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L346
.L333:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21959:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi, .-_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi:
.LFB21960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %r8d
	cmpl	(%rax,%rdx,4), %esi
	jge	.L374
	movq	%r8, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	movq	%rdx, %rdi
	shrq	%rax
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rcx
	movzbl	(%rcx,%rdx), %eax
	cmpb	$2, %al
	je	.L357
	ja	.L358
	testb	%al, %al
	je	.L359
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L375
.L361:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L376
.L363:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, %rsi
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L358:
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L377
.L370:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L378
.L372:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, %rsi
.L364:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movl	%r8d, %edx
	leaq	56(%r12), %r14
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	movslq	%eax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L379
.L366:
	movq	%r14, %rdi
	leaq	40(%r12), %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L380
.L368:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE(%rip), %esi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, %rsi
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L379:
	cmpb	$0, 105(%r12)
	je	.L366
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L366
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L375:
	cmpb	$0, 105(%r12)
	je	.L361
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L361
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L377:
	cmpb	$0, 105(%r12)
	je	.L370
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L370
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L380:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L376:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L372
.L359:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21960:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi, .-_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandIdxInt32Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandIdxInt32Ei
	.type	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandIdxInt32Ei, @function
_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandIdxInt32Ei:
.LFB21961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	16(%rdi), %edx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movzbl	17(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	%esi, (%rcx,%rdx,4)
	jle	.L384
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21961:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandIdxInt32Ei, .-_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandIdxInt32Ei
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi:
.LFB21962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L388
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21962:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi, .-_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandIdxSmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandIdxSmiEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandIdxSmiEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandIdxSmiEi:
.LFB21963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L392
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21963:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandIdxSmiEi, .-_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandIdxSmiEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler30BytecodeOperandConstantPoolIdxEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler30BytecodeOperandConstantPoolIdxEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler30BytecodeOperandConstantPoolIdxEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler30BytecodeOperandConstantPoolIdxEiNS0_15LoadSensitivityE:
.LFB21964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rdi
	cmpl	(%rdi,%rdx,4), %esi
	jge	.L396
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rdi
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rdi,%rax,8), %rax
	movq	%r12, %rdi
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21964:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler30BytecodeOperandConstantPoolIdxEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler30BytecodeOperandConstantPoolIdxEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandRegEiNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandRegEiNS0_15LoadSensitivityE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandRegEiNS0_15LoadSensitivityE, @function
_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandRegEiNS0_15LoadSensitivityE:
.LFB21965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rdi
	cmpl	(%rdi,%rdx,4), %esi
	jge	.L400
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rdi
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rdi,%rax,8), %rax
	movq	%r12, %rdi
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21965:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandRegEiNS0_15LoadSensitivityE, .-_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandRegEiNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi:
.LFB28498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	16(%rdi), %edx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movzbl	17(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L404
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE28498:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi, .-_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler33BytecodeOperandNativeContextIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler33BytecodeOperandNativeContextIndexEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler33BytecodeOperandNativeContextIndexEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler33BytecodeOperandNativeContextIndexEi:
.LFB21967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L408
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21967:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler33BytecodeOperandNativeContextIndexEi, .-_ZN2v88internal11interpreter20InterpreterAssembler33BytecodeOperandNativeContextIndexEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandIntrinsicIdEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandIntrinsicIdEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandIntrinsicIdEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandIntrinsicIdEi:
.LFB28500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	16(%rdi), %edx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movzbl	17(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L412
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	xorl	%ecx, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE28500:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandIntrinsicIdEi, .-_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandIntrinsicIdEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE:
.LFB21969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	40(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	cmpb	$0, 107(%rdi)
	movq	%rdi, %r12
	je	.L416
.L414:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	pushq	$1
	movq	%rax, %rsi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L414
	.cfi_endproc
.LFE21969:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE:
.LFB21970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	40(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	cmpb	$0, 107(%rdi)
	movq	%rdi, %r12
	je	.L420
.L418:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	pushq	$1
	movq	%rax, %rsi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L418
	.cfi_endproc
.LFE21970:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi:
.LFB21971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L425
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	leaq	40(%r12), %r14
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	movl	$2, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L426
.L423:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	pushq	$1
	movq	%rax, %rsi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21971:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi, .-_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi:
.LFB21972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movzbl	16(%rdi), %edx
	movzbl	17(%rdi), %eax
	cmpl	(%rcx,%rdx,4), %esi
	jge	.L431
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rcx
	movq	%rdi, %r12
	andl	$127, %eax
	leaq	40(%r12), %r14
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	movslq	%esi, %rdx
	movq	(%rcx,%rax,8), %rax
	movl	$2, %ecx
	movzbl	(%rax,%rdx), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r13
	je	.L432
.L429:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	pushq	$1
	movq	%rax, %rsi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L431:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21972:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi, .-_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv:
.LFB21973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r12, %rdi
	movq	%r13, %rdx
	movl	$2, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler18LoadFeedbackVectorENS0_8compiler11SloppyTNodeINS0_10JSFunctionEEE@PLT
	.cfi_endproc
.LFE21973:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv, .-_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12CallPrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12CallPrologueEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12CallPrologueEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler12CallPrologueEv:
.LFB21974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	16(%rdi), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L438
	movb	$0, 107(%rbx)
	movb	$1, 105(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18SaveBytecodeOffsetEv
	movb	$0, 107(%rbx)
	movb	$1, 105(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21974:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12CallPrologueEv, .-_ZN2v88internal11interpreter20InterpreterAssembler12CallPrologueEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12CallEpilogueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12CallEpilogueEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12CallEpilogueEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler12CallEpilogueEv:
.LFB21975:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21975:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12CallEpilogueEv, .-_ZN2v88internal11interpreter20InterpreterAssembler12CallEpilogueEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_:
.LFB21976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L445
.L441:
	movl	$1, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %ecx
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	pushq	$1
	movq	%rax, %rcx
	movl	$8, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23StoreFeedbackVectorSlotEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	$20, -104(%rbp)
	movq	%r15, %rdi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movq	-96(%rbp), %rdx
	movl	$1953396079, 16(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L441
	call	_ZdlPv@PLT
	jmp	.L441
.L446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21976:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_, .-_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_
	.section	.rodata._ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Call:Initialize"
.LC5:
	.string	"Call:TransitionMegamorphic"
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_:
.LFB21977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-992(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$1192, %rsp
	movq	%rsi, -1144(%rbp)
	movq	%rdi, %rsi
	movq	%rdx, -1184(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1152(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -1160(%rbp)
	xorl	%r8d, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1120(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %rbx
	jne	.L463
.L448:
	movq	-1144(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17IsWeakReferenceToENS0_8compiler5TNodeINS0_11MaybeObjectEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L464
.L450:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3736(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1176(%rbp), %r15
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-736(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-864(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L465
.L452:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3840(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1168(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L466
.L454:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IsClearedENS0_8compiler5TNodeINS0_11MaybeObjectEEE@PLT
	movq	-1168(%rbp), %rbx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L467
.L456:
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1136(%rbp), %rbx
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-1144(%rbp), %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-608(%rbp), %r10
	movl	$1, %r8d
	movq	%rbx, -224(%rbp)
	movq	%r10, %rdi
	movq	%r10, -1192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1224(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-352(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -1208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	$1104, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1208(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	movq	%r9, -1216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1192(%rbp), %rcx
	movl	$1105, %edx
	movq	%r12, %rdi
	movq	%rcx, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1200(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%r8, %rsi
	movq	%r8, -1208(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-1184(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-1192(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1232(%rbp), %r11
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -1200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1216(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -1184(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1208(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1224(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1184(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1200(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -1184(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1144(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-1160(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movl	$1, %r9d
	call	_ZN2v88internal17CodeStubAssembler34StoreWeakReferenceInFeedbackVectorENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEEPNS2_4NodeENS3_INS0_10HeapObjectEEEiNS1_13ParameterModeE@PLT
	movq	-1160(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler20ReportFeedbackUpdateENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS3_INS0_7IntPtrTEEEPKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1184(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1192(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L468
.L458:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3736(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	movq	-1160(%rbp), %r13
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-1152(%rbp), %rbx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler23StoreFeedbackVectorSlotEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler20ReportFeedbackUpdateENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS3_INS0_7IntPtrTEEEPKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1168(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1176(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L469
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	leaq	-80(%rbp), %r15
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r15, -96(%rbp)
	movq	%r8, -1168(%rbp)
	movq	$20, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC6(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-1168(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movl	$1667852400, 16(%rax)
	movq	%r8, %rsi
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L448
	call	_ZdlPv@PLT
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L468:
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	leaq	-80(%rbp), %rbx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rbx, -96(%rbp)
	movq	%r8, -1144(%rbp)
	movq	$25, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7595444408358363495, %rcx
	movq	%rax, -96(%rbp)
	movq	-1144(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movq	%r8, %rsi
	movb	$99, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L458
	call	_ZdlPv@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	leaq	-80(%rbp), %rbx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rbx, -96(%rbp)
	movq	%r8, -1192(%rbp)
	movq	$40, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC10(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-1192(%rbp), %r8
	movabsq	$8392569456348324640, %rcx
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC11(%rip), %xmm0
	movq	%r8, %rsi
	movq	%rcx, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L456
	call	_ZdlPv@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -1200(%rbp)
	movq	%r8, -1192(%rbp)
	movq	$34, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC8(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movl	$25701, %ecx
	movq	-1192(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC9(%rip), %xmm0
	movq	%r8, %rsi
	movw	%cx, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-1200(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L454
	call	_ZdlPv@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -1200(%rbp)
	movq	%r8, -1192(%rbp)
	movq	$23, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC8(%rip), %xmm0
	movl	$25454, %esi
	movq	%rax, -96(%rbp)
	movq	-1192(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movw	%si, 20(%rax)
	movq	%r8, %rsi
	movl	$1701995878, 16(%rax)
	movb	$101, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-1200(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L452
	call	_ZdlPv@PLT
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	leaq	-80(%rbp), %r15
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r15, -96(%rbp)
	movq	%r8, -1168(%rbp)
	movq	$20, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC7(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-1168(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movl	$1667852400, 16(%rax)
	movq	%r8, %rsi
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L450
	call	_ZdlPv@PLT
	jmp	.L450
.L469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21977:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_, .-_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_:
.LFB21978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdi, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	movl	$1, %r8d
	subq	$168, %rsp
	movq	%rdx, -200(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	-200(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L473
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L473:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21978:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_, .-_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE:
.LFB21979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jne	.L475
	movq	8(%rcx), %r8
.L476:
	movq	%rbx, %rdi
	movq	%r8, -136(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %ecx
	movl	%r13d, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE@PLT
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r14, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	-136(%rbp), %r8
	movq	%rax, %rdx
	movq	(%r12), %rax
	movq	%rcx, -128(%rbp)
	movq	-96(%rbp), %rcx
	movl	$3, %r9d
	movq	%r8, -80(%rbp)
	leaq	-80(%rbp), %r8
	movq	%rcx, -120(%rbp)
	movq	%r15, %rcx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
	orb	$2, 104(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L479
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rax, %r8
	jmp	.L476
.L479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21979:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE, .-_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_,"axG",@progbits,_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_, @function
_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_:
.LFB24593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-96(%rbp), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	-96(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	testl	%r14d, %r14d
	jne	.L481
	movq	-128(%rbp), %xmm0
	movq	%rbx, %rdi
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-80(%rbp), %rdx
	leaq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movdqa	-128(%rbp), %xmm0
	leaq	-64(%rbp), %r8
	movl	$3, %r9d
	movq	%rcx, -112(%rbp)
	movq	%r13, %rcx
	movq	%rdx, -104(%rbp)
	movq	%r12, %rdx
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
.L482:
	orb	$2, 104(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L485
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	-128(%rbp), %xmm0
	leaq	-112(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-64(%rbp), %r8
	movl	$2, %r9d
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rax
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
	jmp	.L482
.L485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24593:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_, .-_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_,"axG",@progbits,_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_, @function
_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_:
.LFB24594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rcx, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-128(%rbp), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	-128(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	testl	%r15d, %r15d
	jne	.L487
	movq	-160(%rbp), %xmm0
	movq	%rbx, %rdi
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-112(%rbp), %rdx
	leaq	-96(%rbp), %r8
	movq	%rbx, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$4, %r9d
	movq	%rax, -80(%rbp)
	movdqa	-160(%rbp), %xmm0
	movq	%rcx, -144(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%r13, %rcx
	movq	%rdx, -136(%rbp)
	movq	%r12, %rdx
	movq	%r14, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
.L488:
	orb	$2, 104(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L491
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	leaq	-144(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	-112(%rbp), %rax
	leaq	-96(%rbp), %r8
	movq	-160(%rbp), %xmm0
	movl	$3, %r9d
	movq	%r14, -80(%rbp)
	movq	%rax, -136(%rbp)
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
	jmp	.L488
.L491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24594:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_, .-_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_,"axG",@progbits,_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_, @function
_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_:
.LFB24595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-128(%rbp), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	-128(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r13
	testl	%r15d, %r15d
	jne	.L493
	movq	-160(%rbp), %xmm0
	movq	%rbx, %rdi
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-112(%rbp), %rdx
	leaq	-96(%rbp), %r8
	movq	%rbx, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rax, -80(%rbp)
	movdqa	-160(%rbp), %xmm0
	leaq	-144(%rbp), %rsi
	movq	-176(%rbp), %rax
	movl	$5, %r9d
	movq	%r12, -72(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r14, %rcx
	movq	%rdx, -136(%rbp)
	movq	%r13, %rdx
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
.L494:
	orb	$2, 104(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L497
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-160(%rbp), %xmm0
	movq	%rax, -144(%rbp)
	leaq	-96(%rbp), %r8
	leaq	-144(%rbp), %rsi
	movq	-112(%rbp), %rax
	movl	$4, %r9d
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r12, %xmm0
	movhps	-176(%rbp), %xmm0
	movq	%rax, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
	jmp	.L494
.L497:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24595:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_, .-_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_,"axG",@progbits,_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_, @function
_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_:
.LFB24596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	24(%rbp), %r14
	movq	%rsi, -176(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-144(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	-144(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %rdx
	testl	%r15d, %r15d
	jne	.L499
	movq	-176(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -200(%rbp)
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-112(%rbp), %r8
	movdqa	-176(%rbp), %xmm0
	movq	%rcx, -160(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rbx, %rdi
	leaq	-160(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-200(%rbp), %rdx
	movl	$6, %r9d
	movq	-192(%rbp), %rax
	movq	%rcx, -152(%rbp)
	movq	%r13, %rcx
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%r14, -72(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
.L500:
	orb	$2, 104(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L503
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	leaq	-112(%rbp), %r8
	movl	$5, %r9d
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-176(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-160(%rbp), %rsi
	movq	%r14, -80(%rbp)
	movq	%rax, -160(%rbp)
	movq	-128(%rbp), %rax
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r12, %xmm0
	movhps	-192(%rbp), %xmm0
	movq	%rax, -152(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
	jmp	.L500
.L503:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24596:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_, .-_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler27CallJSWithSpreadAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler27CallJSWithSpreadAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairES5_S5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler27CallJSWithSpreadAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairES5_S5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler27CallJSWithSpreadAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairES5_S5_:
.LFB21987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	xorl	%ecx, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-224(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rsi, -248(%rbp)
	movq	%rdi, %rsi
	movq	%r12, %rdi
	movq	%r8, -256(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-256(%rbp), %r10
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	-256(%rbp), %r10
	movq	-248(%rbp), %rsi
	leaq	-96(%rbp), %r14
	movq	%r10, %r8
	call	_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L510
.L505:
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE@PLT
	movq	-224(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	8(%r15), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-208(%rbp), %rdx
	movq	%r14, %r8
	movq	%rbx, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rax, %xmm0
	movq	-248(%rbp), %rax
	leaq	-240(%rbp), %rsi
	movq	%rcx, -240(%rbp)
	movhps	(%r15), %xmm0
	movl	$3, %r9d
	movq	%r13, %rcx
	movq	%rdx, -232(%rbp)
	movq	%r12, %rdx
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler36TailCallStubThenBytecodeDispatchImplERKNS0_23CallInterfaceDescriptorEPNS1_4NodeES7_St16initializer_listIS7_E@PLT
	orb	$2, 104(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -256(%rbp)
	movq	$33, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movdqa	.LC13(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC14(%rip), %xmm0
	movb	$110, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-256(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L505
	call	_ZdlPv@PLT
	jmp	.L505
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21987:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler27CallJSWithSpreadAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairES5_S5_, .-_ZN2v88internal11interpreter20InterpreterAssembler27CallJSWithSpreadAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairES5_S5_
	.section	.rodata._ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"Construct:CreateAllocationSite"
	.section	.rodata._ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Construct:StoreWeakReference"
	.section	.rodata._ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_.str1.8
	.align 8
.LC17:
	.string	"Construct:TransitionMegamorphic"
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_, @function
_ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_:
.LFB21988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1680(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1816, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%r9, -1728(%rbp)
	movq	%rcx, -1712(%rbp)
	movq	%r8, -1800(%rbp)
	movq	%rsi, -1760(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%rdx, -1736(%rbp)
	movl	$8, %edx
	movq	%rbx, -1744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -1816(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1664(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1752(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r15, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1504(%rbp), %rsi
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rsi, %rdi
	movq	%r14, -224(%rbp)
	movl	$1, %r8d
	leaq	-1376(%rbp), %r14
	movq	%rsi, -1808(%rbp)
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-1752(%rbp), %rax
	movl	$1, %r8d
	movq	%rax, -224(%rbp)
	leaq	-1248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r14, -1696(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1728(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	movq	-1712(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17IsWeakReferenceToENS0_8compiler5TNodeINS0_11MaybeObjectEEENS3_INS0_6ObjectEEE@PLT
	movq	-1696(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-736(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-1120(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-864(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-96(%rbp), %rax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, -1688(%rbp)
	jne	.L534
.L513:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3736(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1696(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L535
.L515:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsWeakOrClearedENS0_8compiler5TNodeINS0_11MaybeObjectEEE@PLT
	movq	-1792(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L536
.L517:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IsClearedENS0_8compiler5TNodeINS0_11MaybeObjectEEE@PLT
	movq	-1784(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L537
.L519:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsAllocationSiteENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1776(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$11, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1712(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1752(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L538
.L521:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27UninitializedSymbolConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1784(%rbp), %rbx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L539
.L523:
	movq	-1712(%rbp), %r14
	movq	%r12, %rdi
	leaq	-1648(%rbp), %rbx
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r14, -1712(%rbp)
	leaq	-352(%rbp), %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-608(%rbp), %r10
	movl	$1, %r8d
	movq	%rbx, -224(%rbp)
	movq	%r10, %rdi
	movq	%r10, -1832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1832(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1832(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1848(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1840(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	$1104, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1832(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1832(%rbp), %r9
	movl	$1105, %edx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1840(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1832(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-1832(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1856(%rbp), %r11
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -1832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1840(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1848(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1832(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1712(%rbp), %rdx
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$11, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler36CreateAllocationSiteInFeedbackVectorENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS2_5TNodeINS0_3SmiEEE@PLT
	movq	-1752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1728(%rbp), %rdx
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler20ReportFeedbackUpdateENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS3_INS0_7IntPtrTEEEPKc@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1712(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-1728(%rbp), %rdx
	movq	-1744(%rbp), %rsi
	movl	$1, %r9d
	call	_ZN2v88internal17CodeStubAssembler34StoreWeakReferenceInFeedbackVectorENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEEPNS2_4NodeENS3_INS0_10HeapObjectEEEiNS1_13ParameterModeE@PLT
	movq	-1728(%rbp), %rdx
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler20ReportFeedbackUpdateENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS3_INS0_7IntPtrTEEEPKc@PLT
	movq	-1696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1832(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1840(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L540
.L525:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3736(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	movq	-1744(%rbp), %rbx
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-1728(%rbp), %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler23StoreFeedbackVectorSlotEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	-1728(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	.LC17(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler20ReportFeedbackUpdateENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS3_INS0_7IntPtrTEEEPKc@PLT
	movq	-1696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%rcx
	popq	%rsi
	jne	.L541
.L527:
	movq	-1760(%rbp), %xmm1
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rbx
	movhps	-1712(%rbp), %xmm1
	movaps	%xmm1, -1712(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1752(%rbp), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-208(%rbp), %rdx
	movq	%r15, %r8
	movq	-1800(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	-1688(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -344(%rbp)
	movl	$5, %edx
	movdqu	(%rcx), %xmm2
	movl	$1, %ecx
	movdqa	-1712(%rbp), %xmm1
	movq	-1736(%rbp), %r9
	pushq	%rdx
	movq	%r14, %rdx
	pushq	%rax
	movdqa	%xmm2, %xmm0
	shufpd	$1, %xmm2, %xmm0
	movaps	%xmm2, -1728(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movq	%rbx, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1816(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%rax
	popq	%rdx
	jne	.L542
.L529:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-208(%rbp), %rdx
	movq	%r13, %r8
	movq	-1800(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	-1688(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -344(%rbp)
	movl	$5, %edx
	movdqu	(%rcx), %xmm0
	movl	$1, %ecx
	movdqa	-1712(%rbp), %xmm4
	movq	-1736(%rbp), %r9
	pushq	%rdx
	movq	%r14, %rdx
	pushq	%rax
	shufpd	$1, %xmm0, %xmm0
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1816(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1808(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1768(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1752(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L543
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rbx, -96(%rbp)
	movq	$20, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC7(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-1688(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$1667852400, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L513
	call	_ZdlPv@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L542:
	movq	-1688(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	$28, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC20(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7599087837788730738, %rcx
	movq	%rax, -96(%rbp)
	movq	-1688(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$1852404844, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L529
	call	_ZdlPv@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L541:
	movq	-1688(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$33, -224(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC20(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC21(%rip), %xmm0
	movb	$110, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L527
	call	_ZdlPv@PLT
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L540:
	movq	-1688(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$25, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7595444408358363495, %rcx
	movq	%rax, -96(%rbp)
	movq	-1688(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movb	$99, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L525
	call	_ZdlPv@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-1688(%rbp), %r14
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$40, -224(%rbp)
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	.LC10(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8392569456348324640, %rcx
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC11(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L523
	call	_ZdlPv@PLT
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L538:
	movq	-1688(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$22, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC19(%rip), %xmm0
	movl	$25701, %edi
	movq	%rax, -96(%rbp)
	movq	-1688(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movw	%di, 20(%rax)
	movq	%r12, %rdi
	movl	$2053729377, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L521
	call	_ZdlPv@PLT
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L537:
	movq	-1688(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$24, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC18(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7310584038315421545, %rcx
	movq	%rax, -96(%rbp)
	movq	-1688(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L519
	call	_ZdlPv@PLT
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L536:
	movq	-1688(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$34, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC8(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movl	$25701, %r8d
	movq	-1688(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC9(%rip), %xmm0
	movw	%r8w, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L517
	call	_ZdlPv@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L535:
	movq	-1688(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$23, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC8(%rip), %xmm0
	movl	$25454, %r9d
	movq	%rax, -96(%rbp)
	movq	-1688(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$1701995878, 16(%rax)
	movw	%r9w, 20(%rax)
	movb	$101, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L515
	call	_ZdlPv@PLT
	jmp	.L515
.L543:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21988:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_, .-_ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_
	.section	.rodata._ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"ConstructWithSpread:Initialize"
	.align 8
.LC23:
	.string	"ConstructWithSpread:TransitionMegamorphic"
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_:
.LFB21989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1248(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1120(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1352, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%r9, -1280(%rbp)
	movq	%rsi, -1344(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%rdx, -1336(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1296(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -1352(%rbp)
	xorl	%r8d, %r8d
	movq	%rbx, -1312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -1360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r13, -1304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18IncrementCallCountEPNS0_8compiler4NodeES5_
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	movq	-1296(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler17IsWeakReferenceToENS0_8compiler5TNodeINS0_11MaybeObjectEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-736(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-864(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	leaq	-96(%rbp), %rax
	jne	.L561
	movq	%rax, -1272(%rbp)
	leaq	-224(%rbp), %r13
.L545:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3736(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1304(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L563
.L547:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsWeakOrClearedENS0_8compiler5TNodeINS0_11MaybeObjectEEE@PLT
	movq	-1328(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L564
.L549:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IsClearedENS0_8compiler5TNodeINS0_11MaybeObjectEEE@PLT
	movq	-1320(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L565
.L551:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27UninitializedSymbolConstantEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1320(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L566
.L553:
	movq	-1296(%rbp), %r15
	movq	%r12, %rdi
	leaq	-1264(%rbp), %rbx
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r15, -1296(%rbp)
	leaq	-352(%rbp), %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-608(%rbp), %r10
	movl	$1, %r8d
	movq	%rbx, -224(%rbp)
	movq	%r10, %rdi
	movq	%r10, -1368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1368(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1368(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1384(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1376(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	$1104, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1368(%rbp), %r9
	movl	$1105, %edx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1376(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-1336(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-1368(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1392(%rbp), %r11
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -1368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1376(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1384(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1368(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1296(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-1280(%rbp), %rdx
	movq	-1312(%rbp), %rsi
	movl	$1, %r9d
	call	_ZN2v88internal17CodeStubAssembler34StoreWeakReferenceInFeedbackVectorENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEEPNS2_4NodeENS3_INS0_10HeapObjectEEEiNS1_13ParameterModeE@PLT
	movq	-1280(%rbp), %rdx
	movq	-1312(%rbp), %rsi
	movq	%r12, %rdi
	leaq	.LC22(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler20ReportFeedbackUpdateENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS3_INS0_7IntPtrTEEEPKc@PLT
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1368(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1376(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L567
.L555:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3736(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	movq	-1312(%rbp), %rbx
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-1280(%rbp), %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler23StoreFeedbackVectorSlotEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	-1280(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	.LC23(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler20ReportFeedbackUpdateENS0_8compiler11SloppyTNodeINS0_14FeedbackVectorEEENS3_INS0_7IntPtrTEEEPKc@PLT
	movq	-1304(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%rcx
	popq	%rsi
	jne	.L568
.L557:
	movq	-1344(%rbp), %xmm1
	movq	%r12, %rdi
	movhps	-1296(%rbp), %xmm1
	movaps	%xmm1, -1296(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-208(%rbp), %rdx
	xorl	%esi, %esi
	movq	-1352(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	-1272(%rbp), %rax
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rdx, -344(%rbp)
	movl	$5, %edx
	movdqu	(%rcx), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movdqa	-1296(%rbp), %xmm1
	movq	-1336(%rbp), %r9
	pushq	%rdx
	movq	%r15, %rdx
	pushq	%rax
	shufpd	$1, %xmm0, %xmm0
	movq	%rcx, -352(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1304(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1360(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L569
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L561:
	.cfi_restore_state
	leaq	-224(%rbp), %r13
	xorl	%edx, %edx
	leaq	-80(%rbp), %rbx
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, -96(%rbp)
	movq	$20, -224(%rbp)
	movq	%rax, -1272(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC7(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-1272(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$1667852400, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L545
	call	_ZdlPv@PLT
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L568:
	movq	-1272(%rbp), %r14
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$38, -224(%rbp)
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	.LC20(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$28265, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC24(%rip), %xmm0
	movl	$1953261941, 32(%rax)
	movw	%dx, 36(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L557
	call	_ZdlPv@PLT
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L567:
	movq	-1272(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$25, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7595444408358363495, %rcx
	movq	%rax, -96(%rbp)
	movq	-1272(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movb	$99, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L555
	call	_ZdlPv@PLT
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L566:
	movq	-1272(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$40, -224(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC10(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8392569456348324640, %rcx
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC11(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L553
	call	_ZdlPv@PLT
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L565:
	movq	-1272(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$22, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC19(%rip), %xmm0
	movl	$25701, %edi
	movq	%rax, -96(%rbp)
	movq	-1272(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movw	%di, 20(%rax)
	movq	%r12, %rdi
	movl	$2053729377, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L551
	call	_ZdlPv@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L564:
	movq	-1272(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$34, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC8(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movl	$25701, %r8d
	movq	-1272(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC9(%rip), %xmm0
	movw	%r8w, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L549
	call	_ZdlPv@PLT
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L563:
	movq	-1272(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$23, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	.LC8(%rip), %xmm0
	movl	$25454, %r9d
	movq	%rax, -96(%rbp)
	movq	-1272(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$1701995878, 16(%rax)
	movw	%r9w, 20(%rax)
	movb	$101, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L547
	call	_ZdlPv@PLT
	jmp	.L547
.L569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21989:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_, .-_ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi:
.LFB21990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$104, %rsp
	movq	%rsi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	%ebx, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory17InterpreterCEntryEPNS0_7IsolateEi@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference30runtime_function_table_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-144(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Int32MulENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-136(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-136(%rbp), %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	-96(%rbp), %rdx
	movdqu	(%r15), %xmm0
	xorl	%esi, %esi
	movq	%rax, -64(%rbp)
	leaq	-80(%rbp), %rax
	leaq	-128(%rbp), %r10
	movq	%r14, %r9
	movq	%rdx, -120(%rbp)
	movl	$3, %edx
	movq	%r13, %r8
	movq	%r12, %rdi
	pushq	%rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%r10, %rdx
	shufpd	$1, %xmm0, %xmm0
	pushq	%rax
	movq	%rcx, -128(%rbp)
	movslq	%ebx, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L573
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L573:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21990:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi, .-_ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb
	.type	_ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb, @function
_ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb:
.LFB21991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$760, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L583
.L575:
	leaq	-736(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%rax, -776(%rbp)
	leaq	-752(%rbp), %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-608(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r13, %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_12FeedbackCellEvE5valueE(%rip), %ecx
	movl	$40, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -792(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE(%rip), %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movzbl	16(%r12), %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movzbl	17(%r12), %eax
	sarl	%eax
	cltq
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	movl	(%rdx,%rax,4), %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	testb	%bl, %bl
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-784(%rbp), %r10
	movq	%r10, %rsi
	je	.L577
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdi
	leaq	-224(%rbp), %rbx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-352(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23Int32GreaterThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-784(%rbp), %r10
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-784(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	-792(%rbp), %r9
	leaq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$149, %esi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
.L578:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-760(%rbp), %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$4, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L584
.L579:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L585
	addq	$760, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	call	_ZN2v88internal8compiler13CodeAssembler8Int32AddENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L584:
	leaq	-96(%rbp), %r15
	leaq	-224(%rbp), %rsi
	xorl	%edx, %edx
	movq	$23, -224(%rbp)
	movq	%r15, %rdi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC26(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$25959, %edx
	movl	$1685406324, 16(%rax)
	movw	%dx, 20(%rax)
	movb	$116, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L579
	call	_ZdlPv@PLT
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	-96(%rbp), %r14
	leaq	-224(%rbp), %rsi
	xorl	%edx, %edx
	movq	$23, -224(%rbp)
	movq	%r14, %rdi
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movl	$25959, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	-224(%rbp), %rdx
	movdqa	.LC25(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$1685406324, 16(%rax)
	movw	%cx, 20(%rax)
	movb	$116, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L575
	call	_ZdlPv@PLT
	jmp	.L575
.L585:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21991:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb, .-_ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEv:
.LFB21995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	17(%rdi), %eax
	leaq	56(%r12), %r14
	movzbl	16(%rdi), %edx
	sarl	%eax
	cltq
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	movslq	(%rdx,%rax,4), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L588
	cmpb	$0, 105(%r12)
	jne	.L591
.L588:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L588
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L588
	.cfi_endproc
.LFE21995:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEv, .-_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEi
	.type	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEi, @function
_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEi:
.LFB21996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	56(%r12), %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L594
	cmpb	$0, 105(%r12)
	jne	.L597
.L594:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L594
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L594
	.cfi_endproc
.LFE21996:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEi, .-_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceEi
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler7AdvanceENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEEb
	.type	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEEb, @function
_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEEb:
.LFB21997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	56(%r12), %r14
	movzbl	16(%rdi), %edi
	testb	%dl, %dl
	je	.L599
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L608
.L601:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %r12
.L603:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L605
	cmpb	$0, 105(%r12)
	jne	.L609
.L605:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %r12
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L608:
	cmpb	$0, 105(%r12)
	je	.L601
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L601
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L605
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L605
	.cfi_endproc
.LFE21997:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEEb, .-_ZN2v88internal11interpreter20InterpreterAssembler7AdvanceENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEEb
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12LoadBytecodeEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12LoadBytecodeEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12LoadBytecodeEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler12LoadBytecodeEPNS0_8compiler4NodeE:
.LFB22004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	40(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	cmpb	$0, 107(%rdi)
	movq	%rdi, %r12
	je	.L613
.L611:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$770, %esi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L613:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L611
	.cfi_endproc
.LFE22004:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12LoadBytecodeEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler12LoadBytecodeEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler10InlineStarEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler10InlineStarEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler10InlineStarEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler10InlineStarEv:
.LFB22009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	16(%rdi), %r13d
	movzbl	17(%rdi), %eax
	movb	$38, 16(%rdi)
	movzbl	104(%rdi), %r12d
	movb	$0, 104(%rdi)
	movl	152+_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %edx
	testl	%edx, %edx
	jle	.L620
	shrq	%rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rdx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	andl	$127, %eax
	movl	$2, %ecx
	imulq	$1464, %rax, %rax
	movq	304(%rdx,%rax), %rax
	movzbl	(%rax), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeSignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	orb	$1, 104(%rbx)
	leaq	88(%rbx), %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25TaggedPoisonOnSpeculationENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	leaq	56(%rbx), %r15
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	movzbl	17(%rbx), %eax
	movzbl	16(%rbx), %edx
	movq	%rbx, %rdi
	sarl	%eax
	cltq
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	movslq	(%rdx,%rax,4), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%rbx), %edi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L617
	cmpb	$0, 105(%rbx)
	jne	.L621
.L617:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	%r13b, 16(%rbx)
	movb	%r12b, 104(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, -56(%rbp)
	jne	.L617
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L620:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22009:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler10InlineStarEv, .-_ZN2v88internal11interpreter20InterpreterAssembler10InlineStarEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler21StarDispatchLookaheadENS0_8compiler5TNodeINS0_5WordTEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler21StarDispatchLookaheadENS0_8compiler5TNodeINS0_5WordTEEE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler21StarDispatchLookaheadENS0_8compiler5TNodeINS0_5WordTEEE, @function
_ZN2v88internal11interpreter20InterpreterAssembler21StarDispatchLookaheadENS0_8compiler5TNodeINS0_5WordTEEE:
.LFB22005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-336(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rcx
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$38, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19TruncateWordToInt32ENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-344(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10InlineStarEv
	movzbl	16(%r12), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	leaq	56(%r12), %r8
	testb	%al, %al
	je	.L624
	cmpb	$0, 105(%r12)
	jne	.L629
.L624:
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	cmpb	$0, 107(%r12)
	leaq	40(%r12), %r8
	movq	%rax, %rbx
	je	.L630
.L626:
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movl	$770, %esi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L631
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-344(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	-352(%rbp), %r8
	movq	%rax, %rsi
	movq	%r8, %rdi
	movq	%r8, -344(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	movq	-344(%rbp), %r8
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%r8, %rdi
	movq	%r8, -344(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-344(%rbp), %r8
	cmpq	%rax, %rbx
	jne	.L624
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	-344(%rbp), %r8
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-344(%rbp), %r8
	jmp	.L624
.L631:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22005:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler21StarDispatchLookaheadENS0_8compiler5TNodeINS0_5WordTEEE, .-_ZN2v88internal11interpreter20InterpreterAssembler21StarDispatchLookaheadENS0_8compiler5TNodeINS0_5WordTEEE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler25DispatchToBytecodeHandlerEPNS0_8compiler4NodeES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler25DispatchToBytecodeHandlerEPNS0_8compiler4NodeES5_S5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler25DispatchToBytecodeHandlerEPNS0_8compiler4NodeES5_S5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler25DispatchToBytecodeHandlerEPNS0_8compiler4NodeES5_S5_:
.LFB22012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$63, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	40(%r12), %r15
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	cmpb	$0, 107(%r12)
	movq	%rax, %r14
	je	.L636
.L633:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	88(%r12), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	leaq	-80(%rbp), %rsi
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal29InterpreterDispatchDescriptorE(%rip), %rdx
	pushq	%r14
	movq	%r15, %r9
	leaq	1600+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler24TailCallBytecodeDispatchIJPNS1_4NodeES5_S5_S5_EEES5_RKNS0_23CallInterfaceDescriptorES5_DpT_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L637
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-88(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L633
.L637:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22012:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler25DispatchToBytecodeHandlerEPNS0_8compiler4NodeES5_S5_, .-_ZN2v88internal11interpreter20InterpreterAssembler25DispatchToBytecodeHandlerEPNS0_8compiler4NodeES5_S5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler30DispatchToBytecodeHandlerEntryEPNS0_8compiler4NodeES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler30DispatchToBytecodeHandlerEntryEPNS0_8compiler4NodeES5_S5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler30DispatchToBytecodeHandlerEntryEPNS0_8compiler4NodeES5_S5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler30DispatchToBytecodeHandlerEntryEPNS0_8compiler4NodeES5_S5_:
.LFB22013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	40(%r12), %r15
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	cmpb	$0, 107(%r12)
	movq	%rax, %r14
	je	.L642
.L639:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	88(%r12), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	leaq	-80(%rbp), %rsi
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal29InterpreterDispatchDescriptorE(%rip), %rdx
	pushq	%r14
	movq	%r15, %r9
	leaq	1600+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler24TailCallBytecodeDispatchIJPNS1_4NodeES5_S5_S5_EEES5_RKNS0_23CallInterfaceDescriptorES5_DpT_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L643
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-88(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L639
.L643:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22013:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler30DispatchToBytecodeHandlerEntryEPNS0_8compiler4NodeES5_S5_, .-_ZN2v88internal11interpreter20InterpreterAssembler30DispatchToBytecodeHandlerEntryEPNS0_8compiler4NodeES5_S5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv:
.LFB22015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$53, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	56(%r12), %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L646
	cmpb	$0, 105(%r12)
	jne	.L649
.L646:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21TruncateIntPtrToInt32ENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Int32SubENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb
	.p2align 4,,10
	.p2align 3
.L649:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L646
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L646
	.cfi_endproc
.LFE22015:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv, .-_ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler19LoadOsrNestingLevelEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler19LoadOsrNestingLevelEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler19LoadOsrNestingLevelEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler19LoadOsrNestingLevelEv:
.LFB22016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	40(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 107(%rdi)
	je	.L653
.L651:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$514, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	movl	$52, %edx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L651
	.cfi_endproc
.LFE22016:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler19LoadOsrNestingLevelEv, .-_ZN2v88internal11interpreter20InterpreterAssembler19LoadOsrNestingLevelEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE, @function
_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE:
.LFB22017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	leaq	-48(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$348, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L657
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L657:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22017:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE, .-_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler19AbortIfWordNotEqualENS0_8compiler5TNodeINS0_5WordTEEES6_NS0_11AbortReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler19AbortIfWordNotEqualENS0_8compiler5TNodeINS0_5WordTEEES6_NS0_11AbortReasonE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler19AbortIfWordNotEqualENS0_8compiler5TNodeINS0_5WordTEEES6_NS0_11AbortReasonE, @function
_ZN2v88internal11interpreter20InterpreterAssembler19AbortIfWordNotEqualENS0_8compiler5TNodeINS0_5WordTEEES6_NS0_11AbortReasonE:
.LFB22018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdi, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	subq	$296, %rsp
	movq	%rdx, -328(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-328(%rbp), %r9
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L661
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L661:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22018:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler19AbortIfWordNotEqualENS0_8compiler5TNodeINS0_5WordTEEES6_NS0_11AbortReasonE, .-_ZN2v88internal11interpreter20InterpreterAssembler19AbortIfWordNotEqualENS0_8compiler5TNodeINS0_5WordTEEES6_NS0_11AbortReasonE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE:
.LFB22019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE(%rip), %esi
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-376(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-352(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory22FrameDropperTrampolineEPNS0_7IsolateE@PLT
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-64(%rbp), %rcx
	movq	%r15, %r9
	movl	$1, %ebx
	movq	%rax, %r8
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-368(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -368(%rbp)
	movq	-336(%rbp), %rax
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$45, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L665
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L665:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22019:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler13TraceBytecodeENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler13TraceBytecodeENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler13TraceBytecodeENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal11interpreter20InterpreterAssembler13TraceBytecodeENS0_7Runtime10FunctionIdE:
.LFB22020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$88, %rdi
	pushq	%rbx
	leaq	56(%r12), %r14
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L668
	cmpb	$0, 105(%r12)
	jne	.L673
.L668:
	movq	%r14, %rdi
	leaq	40(%r12), %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	cmpb	$0, 107(%r12)
	movq	%rax, %r14
	je	.L674
.L670:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-88(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	leaq	-80(%rbp), %rcx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$3, %r8d
	movq	%r15, -80(%rbp)
	movq	%r14, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L675
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-88(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %r15
	jne	.L668
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L668
.L675:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22020:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler13TraceBytecodeENS0_7Runtime10FunctionIdE, .-_ZN2v88internal11interpreter20InterpreterAssembler13TraceBytecodeENS0_7Runtime10FunctionIdE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE:
.LFB22021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference29interpreter_dispatch_countersEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movzbl	16(%r12), %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	imull	$183, %esi, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-192(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE(%rip), %esi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rcx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	$-1, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-328(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-328(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L679
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L679:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22021:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_:
.LFB22011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal30FLAG_trace_ignition_dispatchesE(%rip)
	jne	.L685
.L681:
	movq	%r12, %rdi
	leaq	40(%r12), %r15
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	movq	%r13, %rcx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	cmpb	$0, 107(%r12)
	movq	%rax, %r14
	je	.L686
.L682:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	88(%r12), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	leaq	-80(%rbp), %rsi
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal29InterpreterDispatchDescriptorE(%rip), %rdx
	pushq	%r14
	movq	%r15, %r9
	leaq	1600+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler24TailCallBytecodeDispatchIJPNS1_4NodeES5_S5_S5_EEES5_RKNS0_23CallInterfaceDescriptorES5_DpT_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L687
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE
	movq	-88(%rbp), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L686:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-88(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L682
.L687:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22011:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_, .-_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv:
.LFB22010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L704
.L689:
	movzbl	17(%r12), %eax
	movzbl	16(%r12), %edx
	movq	%r12, %rdi
	leaq	56(%r12), %r14
	sarl	%eax
	cltq
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	movslq	(%rdx,%rax,4), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L692
	cmpb	$0, 105(%r12)
	jne	.L705
.L692:
	movq	%r14, %rdi
	leaq	40(%r12), %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	cmpb	$0, 107(%r12)
	je	.L706
.L694:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rcx
	movl	$770, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movzbl	17(%r12), %esi
	movzbl	16(%r12), %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter9Bytecodes15IsStarLookaheadENS1_8BytecodeENS1_12OperandScaleE@PLT
	testb	%al, %al
	jne	.L707
.L695:
	movzbl	16(%r12), %edi
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L708
.L697:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L709
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore_state
	cmpb	$0, 105(%r12)
	je	.L697
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L697
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	-96(%rbp), %r14
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	$18, -104(%rbp)
	movq	%r14, %rdi
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	.LC27(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$26723, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L689
	call	_ZdlPv@PLT
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21StarDispatchLookaheadENS0_8compiler5TNodeINS0_5WordTEEE
	movq	%rax, %r13
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L706:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L692
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L692
.L709:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22010:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv, .-_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE:
.LFB22014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	56(%r12), %r15
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movzbl	16(%r12), %edi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L712
	cmpb	$0, 105(%r12)
	jne	.L724
.L712:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	leaq	40(%r12), %r15
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	cmpb	$0, 107(%r12)
	je	.L725
.L714:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rcx
	movl	$770, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_ignition_dispatchesE(%rip)
	movq	%rax, %r14
	jne	.L726
.L715:
	cmpb	$2, %r13b
	je	.L716
	cmpb	$4, %r13b
	jne	.L727
	movl	$512, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rax, %rsi
.L719:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	movq	%r13, %rcx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	cmpb	$0, 107(%r12)
	movq	%rax, %r14
	je	.L728
.L720:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	88(%r12), %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rsi
	movq	%r15, %r9
	pushq	%r14
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal29InterpreterDispatchDescriptorE(%rip), %rdx
	movq	%rbx, %r8
	leaq	1600+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler24TailCallBytecodeDispatchIJPNS1_4NodeES5_S5_S5_EEES5_RKNS0_23CallInterfaceDescriptorES5_DpT_@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L729
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	movl	$256, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rax, %rsi
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L725:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L728:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-88(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L724:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L712
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L712
.L727:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22014:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE, .-_ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb
	.type	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb, @function
_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb:
.LFB21998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	56(%r12), %r14
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler21TruncateIntPtrToInt32ENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movzbl	%bl, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21UpdateInterruptBudgetEPNS0_8compiler4NodeEb
	movzbl	16(%r12), %edi
	testb	%bl, %bl
	je	.L731
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L744
.L733:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %rbx
.L735:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	leaq	40(%r12), %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	cmpb	$0, 107(%r12)
	je	.L745
.L739:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rcx
	movl	$770, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_ignition_dispatchesE(%rip)
	movq	%rax, %r13
	jne	.L746
.L740:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	movq	%r13, %rcx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler23WordPoisonOnSpeculationENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23DispatchTableRawPointerEv
	cmpb	$0, 107(%r12)
	movq	%rax, %r15
	je	.L747
.L741:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	88(%r12), %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	leaq	-80(%rbp), %rsi
	movq	%r14, %r9
	movq	%rax, %rcx
	leaq	16+_ZTVN2v88internal29InterpreterDispatchDescriptorE(%rip), %rdx
	pushq	%r15
	movq	%rbx, %r8
	leaq	1600+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler24TailCallBytecodeDispatchIJPNS1_4NodeES5_S5_S5_EEES5_RKNS0_23CallInterfaceDescriptorES5_DpT_@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L748
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter9Bytecodes26MakesCallAlongCriticalPathENS1_8BytecodeE@PLT
	testb	%al, %al
	je	.L737
	cmpb	$0, 105(%r12)
	jne	.L749
.L737:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %rbx
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L744:
	cmpb	$0, 105(%r12)
	je	.L733
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L733
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L745:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L747:
	call	_ZN2v88internal11interpreter8Register14bytecode_arrayEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-88(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movb	$1, 107(%r12)
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21TraceBytecodeDispatchEPNS0_8compiler4NodeE
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L749:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	cmpq	%rax, %rbx
	jne	.L737
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20ReloadBytecodeOffsetEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L737
.L748:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21998:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb, .-_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE:
.LFB21999:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb
	.cfi_endproc
.LFE21999:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE:
.LFB22000:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb
	.cfi_endproc
.LFE22000:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_:
.LFB22001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeEb
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L755
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L755:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22001:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_, .-_ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE:
.LFB22002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_
	.cfi_endproc
.LFE22002:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE, @function
_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE:
.LFB22003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler15JumpConditionalEPNS0_8compiler4NodeES5_
	.cfi_endproc
.LFE22003:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE, .-_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler29TargetSupportsUnalignedAccessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler29TargetSupportsUnalignedAccessEv
	.type	_ZN2v88internal11interpreter20InterpreterAssembler29TargetSupportsUnalignedAccessEv, @function
_ZN2v88internal11interpreter20InterpreterAssembler29TargetSupportsUnalignedAccessEv:
.LFB22022:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22022:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler29TargetSupportsUnalignedAccessEv, .-_ZN2v88internal11interpreter20InterpreterAssembler29TargetSupportsUnalignedAccessEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_
	.type	_ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_, @function
_ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_:
.LFB22023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$296, %rsp
	movq	%rcx, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler32LoadAndUntagFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-328(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler22UintPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L764
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L764:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22023:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_, .-_ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler31ExportParametersAndRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler31ExportParametersAndRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler31ExportParametersAndRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE, @function
_ZN2v88internal11interpreter20InterpreterAssembler31ExportParametersAndRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE:
.LFB22024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rcx, %rsi
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	8(%r13), %rsi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	movq	%rax, %r11
	jne	.L769
.L766:
	leaq	-336(%rbp), %r13
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r11, -376(%rbp)
	movq	%r13, %rdi
	leaq	-192(%rbp), %r15
	leaq	-320(%rbp), %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r13, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edi, %edi
	movl	$1, %esi
	call	_ZN2v88internal11interpreter8Register18FromParameterIndexEii@PLT
	movl	$-6, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-344(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-344(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-352(%rbp), %r10
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-352(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rbx, %rsi
	movq	-368(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	movq	%r10, %rdx
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	pushq	$1
	movq	-352(%rbp), %r10
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	-360(%rbp), %rax
	movq	%r10, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-352(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r13, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-376(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -352(%rbp)
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	$-5, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-352(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-352(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	-368(%rbp), %r10
	movq	-344(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	movq	%r10, %rdx
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-360(%rbp), %r11
	movq	-344(%rbp), %rdx
	movl	$4, %r8d
	movl	$1, (%rsp)
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-352(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L770
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movq	-344(%rbp), %rdx
	movq	%rax, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_
	movq	-352(%rbp), %r11
	jmp	.L766
.L770:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22024:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler31ExportParametersAndRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE, .-_ZN2v88internal11interpreter20InterpreterAssembler31ExportParametersAndRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler18ImportRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler18ImportRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler18ImportRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE, @function
_ZN2v88internal11interpreter20InterpreterAssembler18ImportRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE:
.LFB22025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	movq	%rax, %r9
	jne	.L775
.L772:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r9, -360(%rbp)
	leaq	-336(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	leaq	-320(%rbp), %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-192(%rbp), %r11
	movl	$1, %r8d
	movq	%r15, -192(%rbp)
	movq	%r11, %rcx
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-344(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-360(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -344(%rbp)
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-352(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-344(%rbp), %r10
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	pushq	$0
	movl	$2, %r9d
	movq	%rax, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movq	$-5, %rsi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-344(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler22TimesSystemPointerSizeENS0_8compiler11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-360(%rbp), %rcx
	movq	-344(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_S4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21StaleRegisterConstantEv@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	-344(%rbp), %rax
	movl	$4, %r8d
	movq	%r12, %rdi
	movl	$1, (%rsp)
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-352(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-368(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-344(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L776
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	.cfi_restore_state
	movq	%rax, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler27AbortIfRegisterCountInvalidEPNS0_8compiler4NodeES5_S5_
	movq	-344(%rbp), %r9
	jmp	.L772
.L776:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22025:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler18ImportRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE, .-_ZN2v88internal11interpreter20InterpreterAssembler18ImportRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE
	.section	.text._ZNK2v88internal11interpreter20InterpreterAssembler19CurrentBytecodeSizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20InterpreterAssembler19CurrentBytecodeSizeEv
	.type	_ZNK2v88internal11interpreter20InterpreterAssembler19CurrentBytecodeSizeEv, @function
_ZNK2v88internal11interpreter20InterpreterAssembler19CurrentBytecodeSizeEv:
.LFB22026:
	.cfi_startproc
	endbr64
	movzbl	17(%rdi), %eax
	movzbl	16(%rdi), %edx
	sarl	%eax
	cltq
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	ret
	.cfi_endproc
.LFE22026:
	.size	_ZNK2v88internal11interpreter20InterpreterAssembler19CurrentBytecodeSizeEv, .-_ZNK2v88internal11interpreter20InterpreterAssembler19CurrentBytecodeSizeEv
	.section	.text._ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE
	.type	_ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE, @function
_ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE:
.LFB22027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	88(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-752(%rbp), %r14
	leaq	-736(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$776, %rsp
	.cfi_offset 3, -56
	movl	%esi, -792(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	orb	$1, 104(%rdi)
	movq	%rax, %rdi
	movq	%rax, -808(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25TaggedPoisonOnSpeculationENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter8Register15current_contextEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-704(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-576(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-448(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-320(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-768(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-776(%rbp), %rdx
	movq	-760(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpl	$1, -792(%rbp)
	movl	$100, %edx
	leaq	-192(%rbp), %r9
	je	.L785
.L779:
	movq	%r12, %rdi
	movq	%r9, -800(%rbp)
	movl	%edx, -792(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-800(%rbp), %r9
	movl	-792(%rbp), %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-64(%rbp), %rcx
	xorl	%esi, %esi
	movl	$1, %ebx
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-784(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-720(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -720(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$127, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movzbl	16(%r12), %edx
	popq	%rcx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rcx
	movzbl	17(%r12), %eax
	popq	%rsi
	movl	(%rcx,%rdx,4), %edi
	testl	%edi, %edi
	jle	.L786
	shrq	%rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	andl	$127, %eax
	imulq	$183, %rax, %rax
	addq	%rdx, %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandSizesE(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	movzbl	(%rax), %edx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeUnsignedOperandEiNS1_11OperandSizeENS0_15LoadSensitivityE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movl	$-5, %esi
	movq	%r12, %rdi
	subl	%eax, %esi
	sall	$3, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26GetInterpretedFramePointerEv
	movq	-784(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFullTaggedEPNS1_4NodeES4_NS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadFeedbackVectorENS0_8compiler11SloppyTNodeINS0_10JSFunctionEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -784(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-784(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	orb	$2, 104(%r12)
	movq	-808(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv
	movq	-760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L787
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsBigIntENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-792(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-792(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-792(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-792(%rbp), %r9
	movl	$101, %edx
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L786:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L787:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22027:
	.size	_ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE, .-_ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE:
.LFB28370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28370:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE
	.weak	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7IntPtrTEvE5valueE:
	.byte	5
	.byte	4
	.weak	_ZN2v88internal13MachineTypeOfINS0_12FeedbackCellEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_12FeedbackCellEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_12FeedbackCellEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_12FeedbackCellEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_12FeedbackCellEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_12FeedbackCellEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6Int32TEvE5valueE:
	.byte	4
	.byte	2
	.weak	_ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7Uint32TEvE5valueE:
	.byte	4
	.byte	3
	.weak	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6Int16TEvE5valueE:
	.byte	3
	.byte	2
	.weak	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE:
	.byte	3
	.byte	3
	.weak	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_5Int8TEvE5valueE:
	.byte	2
	.byte	2
	.weak	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE:
	.byte	2
	.byte	3
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	7954884599298092649
	.quad	7142828221755760756
	.align 16
.LC6:
	.quad	7379465110024448099
	.quad	8245929768100261152
	.align 16
.LC7:
	.quad	7379465110024448099
	.quad	8245929707852623136
	.align 16
.LC8:
	.quad	7379465110024448099
	.quad	7309940790743365408
	.align 16
.LC9:
	.quad	2334381307661018470
	.quad	8241980309258662761
	.align 16
.LC10:
	.quad	7379465110024448099
	.quad	8028075781168391712
	.align 16
.LC11:
	.quad	7881707406003609710
	.quad	7311146993654308965
	.align 16
.LC12:
	.quad	7598814415774904948
	.quad	7308533433353924207
	.align 16
.LC13:
	.quad	7598545778422604131
	.quad	6299529143778305902
	.align 16
.LC14:
	.quad	7018141421088044137
	.quad	7598817671477600356
	.align 16
.LC18:
	.quad	7379465110024448099
	.quad	8386093311352135968
	.align 16
.LC19:
	.quad	7379465110024448099
	.quad	7598814394217035040
	.align 16
.LC20:
	.quad	7598545778422604131
	.quad	8391171954867988334
	.align 16
.LC21:
	.quad	7021800394475140466
	.quad	7598817671477600377
	.align 16
.LC24:
	.quad	7526756701578687858
	.quad	7070761784448282707
	.align 16
.LC25:
	.quad	7310575179138408539
	.quad	8103508940177043017
	.align 16
.LC26:
	.quad	7310575179138408541
	.quad	8103508940177043017
	.align 16
.LC27:
	.quad	4412750543122677053
	.quad	8386107622130196541
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
