	.file	"builtins-bigint-gen.cc"
	.text
	.section	.text._ZN2v88internal20BigIntToI64Assembler23GenerateBigIntToI64ImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20BigIntToI64Assembler23GenerateBigIntToI64ImplEv
	.type	_ZN2v88internal20BigIntToI64Assembler23GenerateBigIntToI64ImplEv, @function
_ZN2v88internal20BigIntToI64Assembler23GenerateBigIntToI64ImplEv:
.LFB13328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L7
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-64(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-80(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8ToBigIntENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16BigIntToRawBytesENS0_8compiler5TNodeINS0_6BigIntEEEPNS2_26TypedCodeAssemblerVariableINS0_8UintPtrTEEES9_@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9ReturnRawEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L1
.L8:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13328:
	.size	_ZN2v88internal20BigIntToI64Assembler23GenerateBigIntToI64ImplEv, .-_ZN2v88internal20BigIntToI64Assembler23GenerateBigIntToI64ImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/builtins/builtins-bigint-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"BigIntToI64"
	.section	.text._ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE:
.LFB13324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$14, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$111, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L13
.L10:
	movq	%r13, %rdi
	call	_ZN2v88internal20BigIntToI64Assembler23GenerateBigIntToI64ImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L10
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13324:
	.size	_ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24BigIntToI32PairAssembler27GenerateBigIntToI32PairImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24BigIntToI32PairAssembler27GenerateBigIntToI32PairImplEv
	.type	_ZN2v88internal24BigIntToI32PairAssembler27GenerateBigIntToI32PairImplEv, @function
_ZN2v88internal24BigIntToI32PairAssembler27GenerateBigIntToI32PairImplEv:
.LFB13340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler4Is32Ev@PLT
	testb	%al, %al
	je	.L20
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-64(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-80(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8ToBigIntENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16BigIntToRawBytesENS0_8compiler5TNodeINS0_6BigIntEEEPNS2_26TypedCodeAssemblerVariableINS0_8UintPtrTEEES9_@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEES5_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L15
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13340:
	.size	_ZN2v88internal24BigIntToI32PairAssembler27GenerateBigIntToI32PairImplEv, .-_ZN2v88internal24BigIntToI32PairAssembler27GenerateBigIntToI32PairImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_BigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"BigIntToI32Pair"
	.section	.text._ZN2v88internal8Builtins24Generate_BigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_BigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_BigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_BigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE:
.LFB13336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$32, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$112, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L26
.L23:
	movq	%r13, %rdi
	call	_ZN2v88internal24BigIntToI32PairAssembler27GenerateBigIntToI32PairImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L23
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13336:
	.size	_ZN2v88internal8Builtins24Generate_BigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_BigIntToI32PairEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins20Generate_I64ToBigIntEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"I64ToBigInt"
	.section	.text._ZN2v88internal8Builtins20Generate_I64ToBigIntEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_I64ToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_I64ToBigIntEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_I64ToBigIntEPNS0_8compiler18CodeAssemblerStateE:
.LFB13345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$51, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$113, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L34
.L29:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L35
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15BigIntFromInt64ENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
.L31:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L29
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13345:
	.size	_ZN2v88internal8Builtins20Generate_I64ToBigIntEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_I64ToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20I64ToBigIntAssembler23GenerateI64ToBigIntImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20I64ToBigIntAssembler23GenerateI64ToBigIntImplEv
	.type	_ZN2v88internal20I64ToBigIntAssembler23GenerateI64ToBigIntImplEv, @function
_ZN2v88internal20I64ToBigIntAssembler23GenerateI64ToBigIntImplEv:
.LFB13349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler13CodeAssembler4Is64Ev@PLT
	testb	%al, %al
	je	.L40
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15BigIntFromInt64ENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	.cfi_endproc
.LFE13349:
	.size	_ZN2v88internal20I64ToBigIntAssembler23GenerateI64ToBigIntImplEv, .-_ZN2v88internal20I64ToBigIntAssembler23GenerateI64ToBigIntImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_I32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"I32PairToBigInt"
	.section	.text._ZN2v88internal8Builtins24Generate_I32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_I32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_I32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_I32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE:
.LFB13354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$64, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$114, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L47
.L42:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4Is32Ev@PLT
	testb	%al, %al
	je	.L48
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler19BigIntFromInt32PairENS0_8compiler5TNodeINS0_7IntPtrTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
.L44:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L42
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13354:
	.size	_ZN2v88internal8Builtins24Generate_I32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_I32PairToBigIntEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24I32PairToBigIntAssembler27GenerateI32PairToBigIntImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24I32PairToBigIntAssembler27GenerateI32PairToBigIntImplEv
	.type	_ZN2v88internal24I32PairToBigIntAssembler27GenerateI32PairToBigIntImplEv, @function
_ZN2v88internal24I32PairToBigIntAssembler27GenerateI32PairToBigIntImplEv:
.LFB13358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZNK2v88internal8compiler13CodeAssembler4Is32Ev@PLT
	testb	%al, %al
	je	.L53
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler19BigIntFromInt32PairENS0_8compiler5TNodeINS0_7IntPtrTEEES5_@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	.cfi_endproc
.LFE13358:
	.size	_ZN2v88internal24I32PairToBigIntAssembler27GenerateI32PairToBigIntImplEv, .-_ZN2v88internal24I32PairToBigIntAssembler27GenerateI32PairToBigIntImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE:
.LFB17092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17092:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_BigIntToI64EPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
