	.file	"builtins-x64.cc"
	.text
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"js_entry_handler_offset_ == 0 || js_entry_handler_offset_ == offset"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE.str1.1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE, @function
_ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE:
.LFB24363:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$136, %rsp
	movl	%edx, -172(%rbp)
	movzbl	528(%rdi), %r13d
	movl	_ZN2v88internalL3rbpE(%rip), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 528(%rdi)
	movl	%r15d, %esi
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$8, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	leal	(%rbx,%rbx), %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rbx
	orq	%rbx, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movq	%rbx, %rcx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	orq	$8, %rcx
	movl	$5, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	_ZN2v88internalL3r12E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r13E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r14E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r15E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL9arg_reg_1E(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movb	%r13b, 528(%r12)
	movl	$1, %edi
	movq	512(%r12), %rsi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -168(%rbp)
	movl	%r14d, %edx
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	512(%r12), %rsi
	movl	$3, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE@PLT
	leaq	-80(%rbp), %r11
	movl	$-16, %edx
	movl	%r15d, %esi
	movq	%r11, %rdi
	movq	%r11, -160(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movl	%r14d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, -60(%rbp)
	movq	%rsi, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	512(%r12), %rsi
	movl	$11, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	movl	%r13d, %esi
	call	_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	leaq	-104(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r9, %rdx
	movl	$5, %esi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rax
	movq	%r12, %rdi
	orq	$2, %rax
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movl	$8, %ecx
	movl	%r15d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-136(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE@PLT
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r8, -144(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-152(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movq	-144(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-120(%rbp), %eax
	movq	512(%r12), %rdx
	movq	-160(%rbp), %r11
	movq	-168(%rbp), %r10
	testl	%eax, %eax
	js	.L15
	je	.L4
	subl	$1, %eax
.L3:
	movl	41196(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L5
	cmpl	%eax, %ecx
	jne	.L16
.L5:
	movl	%eax, 41196(%rdx)
	movq	512(%r12), %rsi
	movl	$4, %edi
	movq	%r10, -144(%rbp)
	movq	%r11, -152(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movq	(%r12), %rax
	movl	%r13d, %esi
	leaq	-112(%rbp), %r13
	movl	$32, %edx
	movq	%r12, %rdi
	call	*88(%rax)
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movabsq	$81604378624, %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler16PushStackHandlerEv@PLT
	movq	512(%r12), %rax
	movl	-172(%rbp), %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15PopStackHandlerEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	orq	$2, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-136(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movq	-152(%rbp), %r11
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r11, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movq	%rbx, %rcx
	movl	$8, %r8d
	movq	%r12, %rdi
	orq	$16, %rbx
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-144(%rbp), %r10
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler3PopENS0_7OperandE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r15E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r14E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r13E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r12E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	%rbx, %rcx
	movl	$8, %r8d
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	notl	%eax
	jmp	.L3
.L16:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L17:
	call	__stack_chk_fail@PLT
.L4:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24363:
	.size	_ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE, .-_ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE
	.section	.text._ZN2v88internalL28Generate_InterpreterPushArgsEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Generate_InterpreterPushArgsEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_, @function
_ZN2v88internalL28Generate_InterpreterPushArgsEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_:
.LFB24379:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%esi, %edx
	movl	%ecx, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	leaq	-84(%rbp), %r15
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movabsq	$81604378624, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_@PLT
	movq	%rbx, %rdx
	movl	$4, %ecx
	movl	%r13d, %esi
	orq	$3, %rdx
	movl	$8, %r8d
	movq	%r12, %rdi
	orq	$8, %rbx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movl	$8, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_negENS0_8RegisterEi@PLT
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movl	$3, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	leaq	-76(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$16, %esi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	movq	$0, -84(%rbp)
	movq	$0, -76(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%edx, %edx
	leaq	-68(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	%rbx, %rcx
	movl	%r14d, %edx
	movl	$5, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-104(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %ecx
	movl	%r14d, %edx
	movl	$59, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24379:
	.size	_ZN2v88internalL28Generate_InterpreterPushArgsEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_, .-_ZN2v88internalL28Generate_InterpreterPushArgsEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_
	.section	.text._ZN2v88internalL33Generate_InterpreterEnterBytecodeEPNS0_14MacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Generate_InterpreterEnterBytecodeEPNS0_14MacroAssemblerE, @function
_ZN2v88internalL33Generate_InterpreterEnterBytecodeEPNS0_14MacroAssemblerE:
.LFB24382:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-68(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internalL3rbpE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	leaq	-80(%rbp), %rdi
	movq	$0, -180(%rbp)
	movq	$0, -172(%rbp)
	movl	%r14d, %esi
	movq	4848(%rax), %rbx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	movl	$8, %r8d
	shrq	$32, %rbx
	movl	%ecx, -60(%rbp)
	movl	%r13d, %esi
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r15, %rdi
	movl	$23, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%ecx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r15, %rdi
	movl	$7, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$91, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r10d
	movl	%r10d, %ecx
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	leaq	-180(%rbp), %r11
	movq	%r11, %rdx
	movq	%r11, -208(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r15, %rdi
	movl	$15, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movl	%r13d, %esi
	movq	-68(%rbp), %rdx
	movabsq	$81604378624, %r15
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r15, %rcx
	movl	$8, %r8d
	xorl	%esi, %esi
	orq	$63, %rcx
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	-172(%rbp), %r9
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-208(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference57address_of_interpreter_entry_trampoline_instruction_startEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r10d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%r10d, %edx
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-200(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rcx
	movl	$8, %r8d
	xorl	%esi, %esi
	orq	%r15, %rcx
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	leaq	-164(%rbp), %rbx
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL33kInterpreterDispatchTableRegisterE(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	$-24, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL33kInterpreterBytecodeArrayRegisterE(%rip), %r15d
	movq	%r12, %rdi
	movq	-164(%rbp), %rdx
	movl	-156(%rbp), %ecx
	movl	$8, %r8d
	movl	%r15d, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L26
.L23:
	movl	$-32, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL34kInterpreterBytecodeOffsetRegisterE(%rip), %r13d
	movq	%r12, %rdi
	movl	-156(%rbp), %ecx
	movq	-164(%rbp), %rdx
	movl	$8, %r8d
	movl	%r13d, %esi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	_ZN2v88internalL3r11E(%rip), %r13d
	movq	%r12, %rdi
	movq	-164(%rbp), %rdx
	movl	-156(%rbp), %ecx
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movl	$3, %ecx
	movl	_ZN2v88internalL33kInterpreterDispatchTableRegisterE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	_ZN2v88internalL32kJavaScriptCallCodeStartRegisterE(%rip), %r13d
	movq	%r12, %rdi
	movq	-164(%rbp), %rdx
	movl	-156(%rbp), %ecx
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%r13d, %ecx
	movl	$72, %edx
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	movl	$7, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE@PLT
	jmp	.L23
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24382:
	.size	_ZN2v88internalL33Generate_InterpreterEnterBytecodeEPNS0_14MacroAssemblerE, .-_ZN2v88internalL33Generate_InterpreterEnterBytecodeEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb, @function
_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb:
.LFB24386:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movb	%sil, -97(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RegisterConfiguration7DefaultEv@PLT
	movslq	24(%rax), %r13
	movq	%rax, %rbx
	leaq	-80(%rbp), %rax
	movq	%rax, -112(%rbp)
	testb	%r14b, %r14b
	jne	.L44
.L29:
	movl	%r13d, %edx
	subl	$1, %edx
	js	.L32
	movslq	%edx, %rax
	movl	%edx, %edx
	subq	%rdx, %r13
	leaq	0(,%rax,4), %r14
	leaq	-8(,%r13,4), %r13
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L31:
	subq	$4, %r14
	cmpq	%r14, %r13
	je	.L32
.L33:
	movq	56(%rbx), %rdx
	movq	%r12, %rdi
	movl	(%rdx,%r14), %r15d
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	%r15d, %r15d
	jne	.L31
	cmpb	$0, -97(%rbp)
	je	.L31
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$4, %r14
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_@PLT
	cmpq	%r14, %r13
	jne	.L33
	.p2align 4,,10
	.p2align 3
.L32:
	movq	-112(%rbp), %rbx
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$40, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-68(%rbp), %rdi
	movl	$32, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %edx
	movq	-68(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler4DropEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movl	$8, %r8d
	movq	%rdx, -68(%rbp)
	movl	%r13d, %esi
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler30EntryFromBuiltinIndexAsOperandENS0_8RegisterE@PLT
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -72(%rbp)
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	leaq	-92(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rsi
	movl	-84(%rbp), %edx
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3RetEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leal	56(,%r13,8), %edx
	movq	%rax, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %ecx
	movl	$8, %r8d
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L29
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24386:
	.size	_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb, .-_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb
	.section	.text._ZN2v88internal12_GLOBAL__N_124CallApiFunctionAndReturnEPNS0_14MacroAssemblerENS0_8RegisterENS0_17ExternalReferenceES4_iPNS0_7OperandES6_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_124CallApiFunctionAndReturnEPNS0_14MacroAssemblerENS0_8RegisterENS0_17ExternalReferenceES4_iPNS0_7OperandES6_, @function
_ZN2v88internal12_GLOBAL__N_124CallApiFunctionAndReturnEPNS0_14MacroAssemblerENS0_8RegisterENS0_17ExternalReferenceES4_iPNS0_7OperandES6_:
.LFB24415:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$184, %rsp
	movq	%r9, -224(%rbp)
	movq	512(%rdi), %r14
	movl	%ecx, -196(%rbp)
	movl	%r8d, -200(%rbp)
	movq	%r14, %rdi
	movl	%esi, -160(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -124(%rbp)
	movq	$0, -140(%rbp)
	movq	$0, -132(%rbp)
	movq	$0, -116(%rbp)
	call	_ZN2v88internal17ExternalReference25handle_scope_next_addressEPNS0_7IsolateE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17ExternalReference26handle_scope_limit_addressEPNS0_7IsolateE@PLT
	movq	%r14, %rdi
	subq	%r13, %rax
	movq	%rax, %r15
	call	_ZN2v88internal17ExternalReference26handle_scope_level_addressEPNS0_7IsolateE@PLT
	movq	%r14, %rdi
	subq	%r13, %rax
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal17ExternalReference27scheduled_exception_addressEPNS0_7IsolateE@PLT
	movq	%r13, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$15, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$14, %esi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r15d, %edx
	movq	%rbx, %rdi
	movl	$15, %esi
	movq	%r15, -168(%rbp)
	leaq	-92(%rbp), %r15
	movq	%rbx, -152(%rbp)
	movabsq	$81604378624, %rbx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	-176(%rbp), %edx
	movq	%r15, %rdi
	movl	$15, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %ecx
	movq	-92(%rbp), %rdx
	movq	%rbx, %r8
	movl	$4, %r9d
	orq	$1, %r8
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%ecx, -72(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	movq	%r14, %rdi
	movq	$0, -108(%rbp)
	movq	$0, -100(%rbp)
	call	_ZN2v88internal17ExternalReference20is_profiling_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movq	-152(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %r8
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_7OperandENS0_9ImmediateE@PLT
	leaq	-108(%rbp), %r10
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movl	$5, %esi
	movq	%r10, -208(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	call	_ZN2v88internal17ExternalReference29address_of_runtime_stats_flagEv@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movq	%rbx, %r8
	movl	$4, %r9d
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	movq	-208(%rbp), %r10
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%r10, %rdx
	movq	%r10, -216(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	-160(%rbp), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_@PLT
	leaq	-100(%rbp), %r8
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -208(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-216(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-160(%rbp), %edx
	movl	-196(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_@PLT
	movq	-192(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movq	-208(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movl	24(%rbp), %ecx
	movq	16(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-140(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-176(%rbp), %edx
	movq	%r15, %rdi
	movl	$15, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %ecx
	movq	-92(%rbp), %rdx
	movq	%rbx, %r8
	orq	$1, %r8
	movl	$5, %esi
	leaq	-116(%rbp), %rbx
	movq	%r12, %rdi
	movl	$4, %r9d
	movl	%ecx, -72(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	movq	-152(%rbp), %rdi
	xorl	%edx, %edx
	movl	$15, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$14, %ecx
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	-168(%rbp), %edx
	movq	%r15, %rdi
	movl	$15, %esi
	leaq	-124(%rbp), %r15
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %r8d
	movq	-92(%rbp), %rcx
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$3, %edx
	movl	$59, %esi
	movl	%r8d, -72(%rbp)
	movl	%r8d, -60(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rcx, -68(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-224(%rbp), %r11
	leaq	96(%r14), %r9
	movq	%r9, -160(%rbp)
	testq	%r11, %r11
	je	.L47
	movq	(%r11), %rdx
	movl	8(%r11), %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	-184(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-68(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	movq	-160(%rbp), %r9
	movq	%r9, %rcx
	call	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE@PLT
	leaq	-132(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r9, %rdx
	movl	$5, %esi
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r10d
	movq	%r12, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$8, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$4, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r10d
	movq	%r12, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	movq	-160(%rbp), %r9
.L48:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$154, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-168(%rbp), %edx
	movl	$15, %esi
	movq	-152(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %ecx
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL9arg_reg_1E(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE@PLT
	call	_ZN2v88internal17ExternalReference30delete_handle_scope_extensionsEv@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movl	$8, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$3, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	-184(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-68(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	movq	-160(%rbp), %r9
	movq	%r9, %rcx
	call	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE@PLT
	leaq	-132(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r9, %rdx
	movl	$5, %esi
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	-200(%rbp), %esi
	movq	%r12, %rdi
	sall	$3, %esi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-160(%rbp), %r9
	jmp	.L48
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24415:
	.size	_ZN2v88internal12_GLOBAL__N_124CallApiFunctionAndReturnEPNS0_14MacroAssemblerENS0_8RegisterENS0_17ExternalReferenceES4_iPNS0_7OperandES6_, .-_ZN2v88internal12_GLOBAL__N_124CallApiFunctionAndReturnEPNS0_14MacroAssemblerENS0_8RegisterENS0_17ExternalReferenceES4_iPNS0_7OperandES6_
	.section	.text._ZN2v88internalL29AdvanceBytecodeOffsetOrReturnEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_S3_PNS0_5LabelE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29AdvanceBytecodeOffsetOrReturnEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_S3_PNS0_5LabelE.constprop.0, @function
_ZN2v88internalL29AdvanceBytecodeOffsetOrReturnEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_S3_PNS0_5LabelE.constprop.0:
.LFB30764:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-68(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movabsq	$81604378624, %rbx
	subq	$104, %rsp
	movq	%rsi, -136(%rbp)
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	movl	_ZN2v88internalL34kInterpreterBytecodeOffsetRegisterE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference27bytecode_size_table_addressEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	$7, %esi
	orq	$3, %rcx
	movq	%r12, %rdi
	movq	$0, -108(%rbp)
	movq	$0, -100(%rbp)
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	leaq	-108(%rbp), %r11
	xorl	%ecx, %ecx
	movl	$7, %esi
	movq	%r11, %rdx
	movq	%r12, %rdi
	movq	%r11, -120(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	orq	$1, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	leaq	-100(%rbp), %r10
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL33kInterpreterBytecodeArrayRegisterE(%rip), %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	movq	%rbx, %rcx
	movl	$8, %r8d
	xorl	%esi, %esi
	orq	$732, %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-120(%rbp), %r11
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-128(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$4, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL33kInterpreterBytecodeArrayRegisterE(%rip), %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	movq	%rbx, %rcx
	movl	$8, %r8d
	xorl	%esi, %esi
	orq	$1464, %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-120(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	$7, %esi
	orb	$-85, %cl
	movq	%r12, %rdi
	orb	$-80, %bl
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	movq	-136(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%r9, %rdx
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_8RegisterENS0_9ImmediateE@PLT
	movq	-120(%rbp), %r9
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%r8d, %r8d
	movl	$2, %ecx
	movl	%r13d, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rcx
	movl	-60(%rbp), %r8d
	movq	%r12, %rdi
	movl	$4, %r9d
	movl	$9, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30764:
	.size	_ZN2v88internalL29AdvanceBytecodeOffsetOrReturnEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_S3_PNS0_5LabelE.constprop.0, .-_ZN2v88internalL29AdvanceBytecodeOffsetOrReturnEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_S3_PNS0_5LabelE.constprop.0
	.section	.text._ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE, @function
_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE:
.LFB24352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movl	$17, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movzbl	536(%rdi), %ebx
	movb	$1, 536(%rdi)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %r15d
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r14d, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r14d
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movl	$17, %esi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movq	(%r12), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	movb	%bl, 536(%r12)
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE24352:
	.size	_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE, .-_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"is_int32(offset)"
	.section	.text._ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE, @function
_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE:
.LFB24355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$88, %rsp
	movq	512(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L64
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$10, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$7, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movabsq	$81604378627, %rdx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movl	$8, %r8d
	movl	%ebx, %ecx
	movl	%r13d, %edx
	movl	$59, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24355:
	.size	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE, .-_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	.section	.text._ZN2v88internalL32Generate_JSEntryTrampolineHelperEPNS0_14MacroAssemblerEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Generate_JSEntryTrampolineHelperEPNS0_14MacroAssemblerEb, @function
_ZN2v88internalL32Generate_JSEntryTrampolineHelperEPNS0_14MacroAssemblerEb:
.LFB24367:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$104, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r15d
	movl	_ZN2v88internalL9arg_reg_3E(%rip), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL9arg_reg_2E(%rip), %edx
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %r9d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	movzbl	536(%r12), %r13d
	movb	$1, 536(%r12)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movq	512(%r12), %rsi
	movl	$3, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%r14d, %edx
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %r9d
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -72(%rbp)
	movl	%r9d, %esi
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL9arg_reg_4E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r10d
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL2r8E(%rip), %edx
	movl	%r10d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r11d
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL2r9E(%rip), %edx
	movl	%r11d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	leaq	-116(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r10d
	movl	_ZN2v88internalL3rcxE(%rip), %r15d
	movq	%r9, %rcx
	movq	%r9, -144(%rbp)
	movq	$0, -124(%rbp)
	movl	%r10d, %esi
	movl	%r15d, %edx
	movq	$0, -116(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	leaq	-124(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-144(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$174, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	-136(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	$0, -108(%rbp)
	movq	$0, -100(%rbp)
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	leaq	-100(%rbp), %r10
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -144(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-108(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%r8d, %r8d
	leaq	-92(%rbp), %rdi
	movl	%r15d, %edx
	movl	_ZN2v88internalL3rbxE(%rip), %r11d
	movl	$3, %ecx
	movl	%r11d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-84(%rbp), %ecx
	movq	-92(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	leaq	-68(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movl	$8, %r8d
	xorl	%esi, %esi
	movq	%r12, %rdi
	movabsq	$81604378625, %rcx
	movl	$1, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-144(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	-136(%rbp), %r9
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	512(%r12), %rax
	leaq	41184(%rax), %rdi
	testb	%bl, %bl
	je	.L67
	movl	$23, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %rsi
.L68:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movb	%r13b, 536(%r12)
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	$2, %esi
	call	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE@PLT
	movq	%rax, %rsi
	jmp	.L68
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24367:
	.size	_ZN2v88internalL32Generate_JSEntryTrampolineHelperEPNS0_14MacroAssemblerEb, .-_ZN2v88internalL32Generate_JSEntryTrampolineHelperEPNS0_14MacroAssemblerEb
	.section	.text._ZN2v88internal12_GLOBAL__N_127Generate_PushBoundArgumentsEPNS0_14MacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_127Generate_PushBoundArgumentsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal12_GLOBAL__N_127Generate_PushBoundArgumentsEPNS0_14MacroAssemblerE:
.LFB24403:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$39, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-68(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -240(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r14d
	movl	%ecx, -84(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -204(%rbp)
	movq	%rdx, -92(%rbp)
	movq	%rdx, -212(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r13, %rdi
	movl	$7, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	movq	%rdx, -104(%rbp)
	movl	%r13d, %esi
	movl	%ecx, -96(%rbp)
	movq	%rdx, -200(%rbp)
	movl	%ecx, -192(%rbp)
	call	_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$4, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	leaq	-240(%rbp), %r10
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movl	$4, %esi
	movq	%r10, -264(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movabsq	$81604378627, %rdx
	movl	$8, %r8d
	movq	$0, -232(%rbp)
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ebx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %edx
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$3, %ecx
	movl	$10, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	512(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE@PLT
	leaq	-248(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	-264(%rbp), %r10
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L77
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-80(%rbp), %rdi
	movq	%r10, -272(%rbp)
	leaq	-232(%rbp), %r15
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %r8d
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$10, %edx
	movl	$59, %esi
	movl	%r8d, -60(%rbp)
	movl	%r8d, -180(%rbp)
	movl	%r8d, -108(%rbp)
	movq	%rcx, -68(%rbp)
	movq	%rcx, -188(%rbp)
	movq	%rcx, -116(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	movzbl	536(%r12), %r8d
	movb	$1, 536(%r12)
	movb	%r8b, -264(%rbp)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	$174, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movzbl	-264(%rbp), %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	%r8b, 536(%r12)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%ebx, %esi
	movl	$8, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%r12, %rdi
	movl	$4, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	$0, -232(%rbp)
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movl	$8, %r8d
	movl	$4, %ecx
	movq	%r12, %rdi
	movl	$3, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-116(%rbp), %r11
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%r11, %rdi
	movl	$3, %ecx
	movl	%r13d, %esi
	movq	%r11, -264(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-116(%rbp), %rdx
	movl	-108(%rbp), %ecx
	movl	%ebx, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movl	$3, %ecx
	movq	-264(%rbp), %r11
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movq	%r11, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-116(%rbp), %rsi
	movl	-108(%rbp), %edx
	movl	%ebx, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	-224(%rbp), %rbx
	movq	%rsi, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	%r14d, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movl	$4, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$39, %edx
	movq	-264(%rbp), %r11
	movq	$0, -232(%rbp)
	movq	%r11, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-108(%rbp), %ecx
	movq	-116(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	%ecx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r14d, %esi
	leaq	-128(%rbp), %rdi
	movl	$7, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-128(%rbp), %rdx
	movl	-120(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rdx, -116(%rbp)
	movl	%ecx, -108(%rbp)
	call	_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r14d, %esi
	movl	$3, %ecx
	movl	%r13d, %edx
	leaq	-140(%rbp), %rdi
	movl	$7, %r8d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	_ZN2v88internalL3r12E(%rip), %r14d
	movq	%r12, %rdi
	movq	-140(%rbp), %rdx
	movl	-132(%rbp), %ecx
	movl	$-1, %r8d
	movl	%r14d, %esi
	movq	%rdx, -128(%rbp)
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	_ZN2v88internalL3rspE(%rip), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-224(%rbp), %rsi
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movl	-216(%rbp), %edx
	movl	$8, %r8d
	movq	%rsi, -140(%rbp)
	movl	%edx, -132(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-216(%rbp), %ecx
	movq	%r12, %rdi
	movq	-224(%rbp), %rdx
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %r8d
	movl	%ecx, -132(%rbp)
	movq	%rdx, -140(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$4, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$15, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movq	-272(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24403:
	.size	_ZN2v88internal12_GLOBAL__N_127Generate_PushBoundArgumentsEPNS0_14MacroAssemblerE, .-_ZN2v88internal12_GLOBAL__N_127Generate_PushBoundArgumentsEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm
	.type	_ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm, @function
_ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm:
.LFB24351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal17ExternalReference6CreateEm@PLT
	movl	_ZN2v88internalL32kJavaScriptCallExtraArg1RegisterE(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rax
	movl	$2, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movq	%rax, %rsi
	movl	$16, %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	.cfi_endproc
.LFE24351:
	.size	_ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm, .-_ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm
	.section	.text._ZN2v88internal8Builtins31Generate_JSConstructStubGenericEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_JSConstructStubGenericEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins31Generate_JSConstructStubGenericEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins31Generate_JSConstructStubGenericEPNS0_14MacroAssemblerE:
.LFB24360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-200(%rbp), %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	536(%rdi), %eax
	movb	$1, 536(%rdi)
	movb	%al, -209(%rbp)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movl	_ZN2v88internalL3rcxE(%rip), %r14d
	movq	%r12, %rdi
	movq	$0, -208(%rbp)
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	$0, -200(%rbp)
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-80(%rbp), %rdi
	movl	$23, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r15d
	movl	%ecx, -60(%rbp)
	movl	%r15d, %esi
	movl	%ecx, -108(%rbp)
	movq	%rdx, -68(%rbp)
	movq	%rdx, -116(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	leaq	-92(%rbp), %r10
	movl	$47, %edx
	movl	%r15d, %esi
	movq	%r10, %rdi
	movq	%r10, -232(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movl	%r15d, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rdx, -104(%rbp)
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	movabsq	$81604378655, %rcx
	movl	$4, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movl	$5, %ecx
	movl	$4, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15JumpIfIsInRangeENS0_8RegisterEjjPNS0_5LabelENS3_8DistanceE@PLT
	movq	512(%r12), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	40960(%rax), %rsi
	addq	$7816, %rsi
	call	_ZN2v88internal14MacroAssembler16IncrementCounterEPNS0_12StatsCounterEi@PLT
	movq	512(%r12), %rcx
	movl	$31, %esi
	leaq	41184(%rcx), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	leaq	-208(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	(%r12), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$5, %edx
	call	*88(%rax)
	movq	512(%r12), %rbx
	movq	32(%r12), %rsi
	subq	16(%r12), %rsi
	leaq	37592(%rbx), %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal4Heap35SetConstructStubCreateDeoptPCOffsetEi@PLT
	movq	-224(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-32, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -84(%rbp)
	movq	%rdx, -92(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-24, %edx
	movq	-232(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %ecx
	movq	-92(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$16, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-128(%rbp), %rdx
	movl	-120(%rbp), %ecx
	movl	%r15d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -92(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movl	%r13d, %esi
	leaq	-184(%rbp), %r11
	movq	%r12, %rdi
	movq	$0, -192(%rbp)
	movq	%r11, %rcx
	movq	%r11, -232(%rbp)
	movq	$0, -184(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	leaq	-192(%rbp), %r9
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-232(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-16, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-128(%rbp), %rdx
	movl	-120(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -92(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$174, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	-224(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	$0, -176(%rbp)
	movq	%r12, %rdi
	movq	$0, -168(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	leaq	-168(%rbp), %r11
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-176(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	%r14d, %edx
	movq	-240(%rbp), %r10
	movl	%r15d, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-84(%rbp), %edx
	movq	-92(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	-232(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$8, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	xorl	%ecx, %ecx
	movl	$13, %esi
	movq	%r12, %rdi
	movq	-224(%rbp), %r9
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %edx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-160(%rbp), %rcx
	movq	%r12, %rdi
	movw	%ax, -156(%rbp)
	movl	$0, -160(%rbp)
	call	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag@PLT
	movq	512(%r12), %rcx
	movq	32(%r12), %rsi
	subq	16(%r12), %rsi
	leaq	37592(%rcx), %rdi
	call	_ZN2v88internal4Heap35SetConstructStubInvokeDeoptPCOffsetEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-16, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -84(%rbp)
	movq	%rdx, -92(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$4, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-152(%rbp), %r8
	movq	%r8, %rdx
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	-224(%rbp), %r8
	movq	%r8, %rdx
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r14d, %ecx
	movl	$1024, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	xorl	%ecx, %ecx
	movl	$3, %esi
	movq	%r12, %rdi
	leaq	-136(%rbp), %r10
	movq	%r10, %rdx
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-224(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-144(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$164, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r9d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -84(%rbp)
	movq	%rdx, -92(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$5, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	-224(%rbp), %r11
	movq	%r11, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-240(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-24, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-128(%rbp), %rdx
	movl	-120(%rbp), %ecx
	movl	%r15d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -92(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$18, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movzbl	-209(%rbp), %eax
	movl	%r14d, %esi
	movq	%r12, %rdi
	movb	%al, 536(%r12)
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$3, %ecx
	movl	%r15d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r9d
	movl	$8, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	salq	$24, %rcx
	movl	%r9d, %esi
	sarq	$56, %rcx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-128(%rbp), %rdx
	movl	-120(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %r9d
	movl	$8, %r8d
	movq	%rdx, -92(%rbp)
	movl	%r9d, %esi
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24360:
	.size	_ZN2v88internal8Builtins31Generate_JSConstructStubGenericEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins31Generate_JSConstructStubGenericEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins32Generate_JSBuiltinsConstructStubEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_JSBuiltinsConstructStubEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins32Generate_JSBuiltinsConstructStubEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins32Generate_JSBuiltinsConstructStubEPNS0_14MacroAssemblerE:
.LFB24361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-112(%rbp), %r10
	pushq	%r13
	movq	%r10, %rcx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-68(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movl	_ZN2v88internalL3rcxE(%rip), %r12d
	movl	_ZN2v88internalL3raxE(%rip), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r10, -144(%rbp)
	movl	%r12d, %edx
	movl	%r9d, %esi
	movq	$0, -112(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	movzbl	536(%rbx), %eax
	movl	$18, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	movb	%al, -113(%rbp)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r9d
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%r9d, %edx
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r15d
	movl	$16, %edx
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movl	$8, %r8d
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r9d
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	%r9d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	leaq	-96(%rbp), %r11
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r11, %rsi
	movq	%r11, -136(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-104(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-60(%rbp), %edx
	movq	-68(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	-136(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r12d, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movq	-128(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$13, %esi
	movq	%rbx, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %edx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-88(%rbp), %rcx
	movq	%rbx, %rdi
	movw	%ax, -84(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag@PLT
	movl	$-16, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-24, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$18, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movzbl	-113(%rbp), %eax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movb	%al, 536(%rbx)
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	%r14d, %edx
	movl	%r14d, %esi
	movl	$3, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r14d
	movl	$8, %r8d
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	salq	$24, %rcx
	movl	%r14d, %esi
	sarq	$56, %rcx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-144(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r12d
	movl	$17, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	$174, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movb	%r12b, 536(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24361:
	.size	_ZN2v88internal8Builtins32Generate_JSBuiltinsConstructStubEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins32Generate_JSBuiltinsConstructStubEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins36Generate_ConstructedNonConstructableEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_ConstructedNonConstructableEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins36Generate_ConstructedNonConstructableEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins36Generate_ConstructedNonConstructableEPNS0_14MacroAssemblerE:
.LFB24362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$17, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	536(%rdi), %r12d
	movq	%rdi, %rbx
	movb	$1, 536(%rdi)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$163, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	%rbx, %rdi
	movl	$17, %esi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movb	%r12b, 536(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24362:
	.size	_ZN2v88internal8Builtins36Generate_ConstructedNonConstructableEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins36Generate_ConstructedNonConstructableEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins16Generate_JSEntryEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_JSEntryEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins16Generate_JSEntryEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins16Generate_JSEntryEPNS0_14MacroAssemblerE:
.LFB24364:
	.cfi_startproc
	endbr64
	movl	$43, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE
	.cfi_endproc
.LFE24364:
	.size	_ZN2v88internal8Builtins16Generate_JSEntryEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins16Generate_JSEntryEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins25Generate_JSConstructEntryEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_JSConstructEntryEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins25Generate_JSConstructEntryEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins25Generate_JSConstructEntryEPNS0_14MacroAssemblerE:
.LFB24365:
	.cfi_startproc
	endbr64
	movl	$44, %edx
	movl	$2, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE
	.cfi_endproc
.LFE24365:
	.size	_ZN2v88internal8Builtins25Generate_JSConstructEntryEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins25Generate_JSConstructEntryEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins29Generate_JSRunMicrotasksEntryEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_JSRunMicrotasksEntryEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins29Generate_JSRunMicrotasksEntryEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins29Generate_JSRunMicrotasksEntryEPNS0_14MacroAssemblerE:
.LFB24366:
	.cfi_startproc
	endbr64
	movl	$157, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_123Generate_JSEntryVariantEPNS0_14MacroAssemblerENS0_10StackFrame4TypeENS0_8Builtins4NameE
	.cfi_endproc
.LFE24366:
	.size	_ZN2v88internal8Builtins29Generate_JSRunMicrotasksEntryEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins29Generate_JSRunMicrotasksEntryEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins26Generate_JSEntryTrampolineEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_JSEntryTrampolineEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins26Generate_JSEntryTrampolineEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins26Generate_JSEntryTrampolineEPNS0_14MacroAssemblerE:
.LFB24368:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internalL32Generate_JSEntryTrampolineHelperEPNS0_14MacroAssemblerEb
	.cfi_endproc
.LFE24368:
	.size	_ZN2v88internal8Builtins26Generate_JSEntryTrampolineEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins26Generate_JSEntryTrampolineEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins35Generate_JSConstructEntryTrampolineEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_JSConstructEntryTrampolineEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins35Generate_JSConstructEntryTrampolineEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins35Generate_JSConstructEntryTrampolineEPNS0_14MacroAssemblerE:
.LFB24369:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internalL32Generate_JSEntryTrampolineHelperEPNS0_14MacroAssemblerEb
	.cfi_endproc
.LFE24369:
	.size	_ZN2v88internal8Builtins35Generate_JSConstructEntryTrampolineEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins35Generate_JSConstructEntryTrampolineEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins32Generate_RunMicrotasksTrampolineEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_RunMicrotasksTrampolineEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins32Generate_RunMicrotasksTrampolineEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins32Generate_RunMicrotasksTrampolineEPNS0_14MacroAssemblerE:
.LFB24370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal23RunMicrotasksDescriptor22MicrotaskQueueRegisterEv@PLT
	movl	_ZN2v88internalL9arg_reg_2E(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	512(%r12), %rax
	movl	$158, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movq	%rax, %rsi
	movl	$16, %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	.cfi_endproc
.LFE24370:
	.size	_ZN2v88internal8Builtins32Generate_RunMicrotasksTrampolineEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins32Generate_RunMicrotasksTrampolineEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins34Generate_ResumeGeneratorTrampolineEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_ResumeGeneratorTrampolineEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins34Generate_ResumeGeneratorTrampolineEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins34Generate_ResumeGeneratorTrampolineEPNS0_14MacroAssemblerE:
.LFB24372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-68(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$552, %rsp
	movl	_ZN2v88internalL3rdxE(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r12d, %esi
	call	_ZN2v88internal14MacroAssembler21AssertGeneratorObjectENS0_8RegisterE@PLT
	movl	$47, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %ecx
	movq	%rsi, -200(%rbp)
	movl	%edx, -192(%rbp)
	movq	%rsi, -464(%rbp)
	movl	%edx, -456(%rbp)
	call	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE@PLT
	pushq	$0
	movl	_ZN2v88internalL3raxE(%rip), %ecx
	xorl	%r9d, %r9d
	pushq	$0
	movl	_ZN2v88internalL3rcxE(%rip), %r14d
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	$48, %edx
	movl	%r14d, %r8d
	call	_ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE@PLT
	movl	$23, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %r15d
	movl	%ecx, -204(%rbp)
	movl	%r15d, %esi
	movl	%ecx, -444(%rbp)
	movq	%rdx, -212(%rbp)
	movq	%rdx, -452(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$31, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	%ecx, -216(%rbp)
	movl	%ecx, -432(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rdx, -440(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	512(%rbx), %rdi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	call	_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	$7, %esi
	movq	%rbx, %rdi
	movabsq	$81604378624, %r8
	movl	%edx, %ecx
	movl	%edx, -480(%rbp)
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movq	%rax, -488(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_7OperandENS0_9ImmediateE@PLT
	leaq	-536(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$5, %esi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	512(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference33debug_suspended_generator_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	$8, %r9d
	movl	$59, %esi
	movq	%rbx, %rdi
	movq	%rdx, %r8
	movq	%rax, %rcx
	movl	%edx, -468(%rbp)
	movl	%edx, -72(%rbp)
	movl	%edx, -60(%rbp)
	movl	$2, %edx
	movq	%rax, -476(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	leaq	-528(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$4, %esi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-520(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	512(%rbx), %rdi
	movq	$0, -512(%rbp)
	movq	%rdi, -560(%rbp)
	call	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE@PLT
	movq	-560(%rbp), %rdi
	movq	%rax, -496(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	popq	%rcx
	movl	$4294967295, %ecx
	popq	%rsi
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L104
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %r8d
	movq	-68(%rbp), %rcx
	movq	%rbx, %rdi
	movl	$8, %r9d
	movl	$4, %edx
	movl	$59, %esi
	movl	%r8d, -84(%rbp)
	movl	%r8d, -420(%rbp)
	movl	%r8d, -72(%rbp)
	movq	%rcx, -92(%rbp)
	movq	%rcx, -428(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	leaq	-512(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$2, %esi
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$39, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	movl	$-1, %ecx
	movq	%rsi, -236(%rbp)
	movl	%edx, -228(%rbp)
	movq	%rsi, -416(%rbp)
	movl	%edx, -408(%rbp)
	call	_ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE@PLT
	movl	$23, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%ecx, -240(%rbp)
	movl	%ecx, -396(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -404(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$41, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	%ecx, -252(%rbp)
	movl	%ecx, -384(%rbp)
	movq	%rdx, -260(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	movl	$71, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r10d
	movl	%ecx, -264(%rbp)
	movl	%r10d, %esi
	movl	%ecx, -372(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rdx, -380(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	_ZN2v88internalL2r9E(%rip), %r11d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movl	%r11d, %esi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	-552(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$4, %r8d
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	$9, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$13, %esi
	movq	%rbx, %rdi
	leaq	-504(%rbp), %r9
	movq	%r9, %rdx
	movq	%r9, -592(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL2r9E(%rip), %r11d
	movl	$15, %r8d
	movq	%r13, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r10d
	movl	$3, %ecx
	movl	%r11d, %edx
	movl	%r10d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	movl	$-1, %r8d
	movl	$-1, %ecx
	movq	%rsi, -284(%rbp)
	movl	%edx, -276(%rbp)
	movq	%rsi, -368(%rbp)
	movl	%edx, -360(%rbp)
	call	_ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_@PLT
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movabsq	$81604378624, %rax
	movl	$4, %r8d
	movl	$9, %edx
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-552(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-592(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L105
.L101:
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	leaq	-116(%rbp), %rdi
	movl	$23, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-108(%rbp), %ecx
	movq	-116(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	%ecx, -96(%rbp)
	movl	%ecx, -324(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdx, -332(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	leaq	-128(%rbp), %rdi
	movl	$41, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -108(%rbp)
	movl	%ecx, -312(%rbp)
	movq	%rdx, -116(%rbp)
	movq	%rdx, -320(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-140(%rbp), %rdi
	movl	$47, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-140(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	-132(%rbp), %ecx
	movq	%rdx, -128(%rbp)
	movl	%ecx, -120(%rbp)
	movq	%rdx, -308(%rbp)
	movl	%ecx, -300(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*48(%rax)
	movq	-568(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r13d
	movl	$17, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE@PLT
	movl	$72, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	%r12d, %esi
	movl	$23, %edx
	leaq	-152(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-144(%rbp), %ecx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	-152(%rbp), %rdx
	movl	%ecx, -132(%rbp)
	movl	%ecx, -288(%rbp)
	movq	%rdx, -140(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movq	-584(%rbp), %r14
	movl	$1, %edx
	movq	%rbx, %rdi
	movb	%r13b, 536(%rbx)
	movq	%r14, %rsi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-576(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r13d
	movl	$17, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$74, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	%r12d, %esi
	movl	$23, %edx
	leaq	-164(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-156(%rbp), %ecx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	-164(%rbp), %rdx
	movl	%ecx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movb	%r13b, 536(%rbx)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-560(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r12d
	movl	$17, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	$174, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movb	%r12b, 536(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	%r15d, %esi
	movq	%r13, %rdi
	movl	$23, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%ecx, -168(%rbp)
	movl	%ecx, -348(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rdx, -356(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	$7, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	%rdx, -188(%rbp)
	movl	%ecx, -180(%rbp)
	movq	%rdx, -344(%rbp)
	movl	%ecx, -336(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	$91, %edx
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movq	$0, -496(%rbp)
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$5, %esi
	movq	-552(%rbp), %r13
	movq	%r13, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-80(%rbp), %rdi
	movl	%r14d, %esi
	movl	$7, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%r14d, %ecx
	movl	$72, %edx
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	movl	$15, %edx
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE@PLT
	jmp	.L101
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24372:
	.size	_ZN2v88internal8Builtins34Generate_ResumeGeneratorTrampolineEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins34Generate_ResumeGeneratorTrampolineEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins35Generate_InterpreterEntryTrampolineEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_InterpreterEntryTrampolineEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins35Generate_InterpreterEntryTrampolineEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins35Generate_InterpreterEntryTrampolineEPNS0_14MacroAssemblerE:
.LFB24378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$23, %edx
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-68(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-464(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	%ecx, -228(%rbp)
	movl	%ecx, -444(%rbp)
	movq	%rdx, -236(%rbp)
	movq	%rdx, -452(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$7, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL33kInterpreterBytecodeArrayRegisterE(%rip), %r15d
	movq	%rdx, -248(%rbp)
	movl	%r15d, %esi
	movl	%ecx, -240(%rbp)
	movq	%rdx, -440(%rbp)
	movl	%ecx, -432(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$91, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	movq	$0, -464(%rbp)
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$7, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%rdx, -92(%rbp)
	movl	%ecx, -84(%rbp)
	movq	%rdx, -104(%rbp)
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$72, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %ecx
	movq	$0, -496(%rbp)
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	leaq	-496(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$5, %esi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$39, %edx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	$3, %esi
	movl	%ecx, -252(%rbp)
	movl	%ecx, -420(%rbp)
	movq	%rdx, -260(%rbp)
	movq	%rdx, -428(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$7, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	$3, %esi
	movl	%ecx, -264(%rbp)
	movl	%ecx, -408(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rdx, -416(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$-1, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	movq	$0, -488(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r14d
	movl	%ecx, -276(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -396(%rbp)
	movq	%rdx, -284(%rbp)
	movq	%rdx, -404(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$155, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	leaq	-488(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$5, %esi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$15, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	movq	$0, -480(%rbp)
	movq	$0, -472(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%rbx, %rdi
	movl	$-1, %r8d
	movl	$1, %esi
	movq	%rdx, -140(%rbp)
	movl	%ecx, -132(%rbp)
	movq	%rdx, -176(%rbp)
	movl	%ecx, -168(%rbp)
	call	_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_@PLT
	leaq	-480(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$1, %esi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movabsq	$4294967296, %rdx
	call	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE@PLT
	leaq	-472(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$4, %esi
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	$0, -464(%rbp)
	call	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$58, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movabsq	$8589934592, %rdx
	movq	$0, -464(%rbp)
	call	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$56, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movabsq	$12884901888, %rdx
	movq	$0, -464(%rbp)
	call	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$55, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L113
.L108:
	movq	-504(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-512(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-504(%rbp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler13LoadWeakValueENS0_8RegisterEPNS0_5LabelE@PLT
	movq	%r13, %rdi
	movl	$31, %edx
	movl	$1, %esi
	movq	$0, -464(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movl	%ecx, -108(%rbp)
	movl	%ecx, -156(%rbp)
	movq	%rdx, -116(%rbp)
	movq	%rdx, -164(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movq	%r13, %rdi
	movl	$15, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	movabsq	$81604378624, %rcx
	movl	$4, %r8d
	orq	$1, %rcx
	movq	%rsi, -128(%rbp)
	movl	%edx, -120(%rbp)
	movq	%rsi, -152(%rbp)
	movl	%edx, -144(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rdi
	movl	$47, %edx
	movl	$7, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	movq	-68(%rbp), %rsi
	movabsq	$81604378624, %r13
	movl	$1, %ecx
	movq	%rsi, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	$1, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	pushq	$1
	movl	_ZN2v88internalL3r11E(%rip), %ecx
	xorl	%r9d, %r9d
	pushq	$1
	movl	$48, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3r15E(%rip), %r8d
	call	_ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE@PLT
	movl	$1, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_@PLT
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*48(%rax)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$57, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE
	movq	-504(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$35, %edx
	movl	$3, %esi
	leaq	-188(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -528(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-180(%rbp), %edx
	movl	$4, %ecx
	movq	%rbx, %rdi
	movq	-188(%rbp), %rsi
	movl	%edx, -288(%rbp)
	movl	%edx, -384(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rsi, -392(%rbp)
	call	_ZN2v88internal9Assembler8emit_incENS0_7OperandEi@PLT
	movq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %eax
	movb	$1, 536(%rbx)
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movb	%al, -529(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r9d
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	%r9d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	-528(%rbp), %r8
	movl	$51, %edx
	movl	%r15d, %esi
	movq	%r8, %rdi
	movq	%r8, -520(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-188(%rbp), %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movl	-180(%rbp), %edx
	movq	%rsi, -308(%rbp)
	movl	%edx, -300(%rbp)
	movq	%rsi, -380(%rbp)
	movl	%edx, -372(%rbp)
	call	_ZN2v88internal9Assembler4movwENS0_7OperandENS0_9ImmediateE@PLT
	movl	_ZN2v88internalL34kInterpreterBytecodeOffsetRegisterE(%rip), %r13d
	movl	$8, %ecx
	movq	%rbx, %rdi
	movabsq	$81604378624, %rdx
	orq	$53, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	-520(%rbp), %r8
	movl	$39, %edx
	movl	%r15d, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-188(%rbp), %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	-180(%rbp), %ecx
	movl	$4, %r8d
	movq	%rdx, -320(%rbp)
	movl	%ecx, -312(%rbp)
	movq	%rdx, -368(%rbp)
	movl	%ecx, -360(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r9d
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	$0, -480(%rbp)
	movl	%r9d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$8, %r8d
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	512(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal17ExternalReference23address_of_real_jslimitEPNS0_7IsolateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	popq	%rcx
	movl	$4294967295, %ecx
	popq	%rsi
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L114
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	-200(%rbp), %r10
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movq	%r10, %rdi
	movq	%r10, -520(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	$8, %r9d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	-192(%rbp), %r8d
	movq	-200(%rbp), %rcx
	movl	$59, %esi
	movl	%r8d, -180(%rbp)
	movl	%r8d, -348(%rbp)
	movl	%r8d, -204(%rbp)
	movq	%rcx, -188(%rbp)
	movq	%rcx, -356(%rbp)
	movq	%rcx, -212(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	xorl	%ecx, %ecx
	movl	$3, %esi
	movq	%rbx, %rdi
	movq	-512(%rbp), %r14
	movq	%r14, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$174, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r14, -512(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	(%rbx), %rax
	movl	$4, %edx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	call	*88(%rax)
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$16, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-504(%rbp), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$8, %ecx
	movl	$8, %r8d
	movq	%rbx, %rdi
	movabsq	$81604378624, %rax
	movl	$1, %edx
	movl	$5, %esi
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$13, %esi
	movq	%rbx, %rdi
	movq	%r14, -504(%rbp)
	leaq	-212(%rbp), %r14
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-520(%rbp), %r10
	movl	$47, %edx
	movl	%r15d, %esi
	movq	$0, -480(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-200(%rbp), %rdx
	movl	-192(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%rdx, -332(%rbp)
	movl	%ecx, -324(%rbp)
	movq	%rdx, -344(%rbp)
	movl	%ecx, -336(%rbp)
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$4, %ecx
	movq	%rbx, %rdi
	movl	%edx, %esi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%rbx, %rdi
	movq	-512(%rbp), %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r14, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %ecx
	movq	%rbx, %rdi
	movl	-204(%rbp), %edx
	movq	-212(%rbp), %rsi
	movl	$8, %r8d
	movl	%edx, -192(%rbp)
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-512(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	(%rbx), %rax
	movl	$4, %edx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL31kInterpreterAccumulatorRegisterE(%rip), %esi
	call	*88(%rax)
	movq	-504(%rbp), %rsi
	movq	%rbx, %rdi
	movq	$0, -472(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	512(%rbx), %rdi
	call	_ZN2v88internal17ExternalReference34interpreter_dispatch_table_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL33kInterpreterDispatchTableRegisterE(%rip), %r9d
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	%r9d, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-212(%rbp), %rdx
	movq	%rbx, %rdi
	movl	-204(%rbp), %ecx
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -200(%rbp)
	movl	%ecx, -192(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r14, %rdi
	movl	_ZN2v88internalL33kInterpreterDispatchTableRegisterE(%rip), %r9d
	movl	_ZN2v88internalL3r11E(%rip), %edx
	movl	%r9d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-204(%rbp), %ecx
	movq	%rbx, %rdi
	movq	-212(%rbp), %rdx
	movl	_ZN2v88internalL32kJavaScriptCallCodeStartRegisterE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -192(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL32kJavaScriptCallCodeStartRegisterE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movq	512(%rbx), %rax
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap33SetInterpreterEntryReturnPCOffsetEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-24, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-204(%rbp), %ecx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	-212(%rbp), %rdx
	movl	$8, %r8d
	movl	%ecx, -192(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-32, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-204(%rbp), %ecx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	-212(%rbp), %rdx
	movl	$8, %r8d
	movl	%ecx, -192(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	movq	$0, -464(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-204(%rbp), %ecx
	movq	%rbx, %rdi
	movq	-212(%rbp), %rdx
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -192(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internalL29AdvanceBytecodeOffsetOrReturnEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_S3_PNS0_5LabelE.constprop.0
	movq	-504(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$-24, %edx
	movq	-520(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-192(%rbp), %ecx
	movl	$8, %r8d
	movq	%rbx, %rdi
	movq	-200(%rbp), %rdx
	movl	$3, %esi
	movl	%ecx, -204(%rbp)
	movq	%rdx, -212(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$43, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-212(%rbp), %rdx
	movl	-204(%rbp), %ecx
	movq	%rbx, %rdi
	movl	$4, %r8d
	movl	$3, %esi
	movq	%rdx, -200(%rbp)
	movl	%ecx, -192(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5leaveEv@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$8, %r8d
	movl	$3, %ecx
	movq	%rbx, %rdi
	movl	$4, %edx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-544(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$54, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internalL30GenerateTailCallToReturnedCodeEPNS0_14MacroAssemblerENS0_7Runtime10FunctionIdE
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movzbl	-529(%rbp), %eax
	movb	%al, 536(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%rbx, %rdi
	movl	$1, %esi
	movabsq	$17179869184, %rdx
	call	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE@PLT
	movl	$5, %edx
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE@PLT
	jmp	.L108
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24378:
	.size	_ZN2v88internal8Builtins35Generate_InterpreterEntryTrampolineEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins35Generate_InterpreterEntryTrampolineEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE
	.type	_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE, @function
_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE:
.LFB24380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-88(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movl	$1, %edx
	subq	$56, %rsp
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r14d
	movl	$4, %r8d
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%r15, %rcx
	movl	_ZN2v88internalL3rdxE(%rip), %edx
	movl	$1, %r8d
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	%r13d, %r13d
	je	.L124
.L117:
	movl	_ZN2v88internalL3rdxE(%rip), %ecx
	movl	_ZN2v88internalL3rbxE(%rip), %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internalL28Generate_InterpreterPushArgsEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_
	cmpl	$1, %ebx
	je	.L125
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	512(%r12), %rax
	movl	%r13d, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE@PLT
.L123:
	movq	%rax, %rsi
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$174, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	512(%r12), %rax
	movl	$13, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE@PLT
	movl	$4, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	jmp	.L117
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24380:
	.size	_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE, .-_ZN2v88internal8Builtins40Generate_InterpreterPushArgsThenCallImplEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE
	.section	.text._ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE
	.type	_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE, @function
_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE:
.LFB24381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	movq	%r14, %rcx
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	_ZN2v88internalL2r8E(%rip), %r9d
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movl	%r9d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r15d
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movabsq	$81604378624, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movl	_ZN2v88internalL2r8E(%rip), %r9d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %edx
	movl	%r9d, %ecx
	call	_ZN2v88internalL28Generate_InterpreterPushArgsEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_
	cmpl	$1, %ebx
	je	.L134
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler31AssertUndefinedOrAllocationSiteENS0_8RegisterE@PLT
	testl	%ebx, %ebx
	je	.L135
	movq	512(%r12), %rax
	movl	$23, %esi
	leaq	41184(%rax), %rdi
.L133:
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$174, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L136
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE@PLT
	movq	512(%r12), %rax
	movl	$171, %esi
	leaq	41184(%rax), %rdi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L134:
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	$4, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	512(%r12), %rax
	movl	$25, %esi
	leaq	41184(%rax), %rdi
	jmp	.L133
.L136:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24381:
	.size	_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE, .-_ZN2v88internal8Builtins45Generate_InterpreterPushArgsThenConstructImplEPNS0_14MacroAssemblerENS0_23InterpreterPushArgsModeE
	.section	.text._ZN2v88internal8Builtins40Generate_InterpreterEnterBytecodeAdvanceEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_InterpreterEnterBytecodeAdvanceEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins40Generate_InterpreterEnterBytecodeAdvanceEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins40Generate_InterpreterEnterBytecodeAdvanceEPNS0_14MacroAssemblerE:
.LFB24383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-24, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internalL3rbpE(%rip), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ebx, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL33kInterpreterBytecodeArrayRegisterE(%rip), %r15d
	movl	$8, %r8d
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$-32, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL34kInterpreterBytecodeOffsetRegisterE(%rip), %r13d
	movl	$8, %r8d
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r15d
	movl	$8, %r8d
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-88(%rbp), %r9
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	movq	%r9, %rsi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internalL29AdvanceBytecodeOffsetOrReturnEPNS0_14MacroAssemblerENS0_8RegisterES3_S3_S3_PNS0_5LabelE.constprop.0
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	$-32, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movl	%r15d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internalL33Generate_InterpreterEnterBytecodeEPNS0_14MacroAssemblerE
	movq	-104(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L140:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24383:
	.size	_ZN2v88internal8Builtins40Generate_InterpreterEnterBytecodeAdvanceEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins40Generate_InterpreterEnterBytecodeAdvanceEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins41Generate_InterpreterEnterBytecodeDispatchEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_InterpreterEnterBytecodeDispatchEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins41Generate_InterpreterEnterBytecodeDispatchEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins41Generate_InterpreterEnterBytecodeDispatchEPNS0_14MacroAssemblerE:
.LFB24384:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internalL33Generate_InterpreterEnterBytecodeEPNS0_14MacroAssemblerE
	.cfi_endproc
.LFE24384:
	.size	_ZN2v88internal8Builtins41Generate_InterpreterEnterBytecodeDispatchEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins41Generate_InterpreterEnterBytecodeDispatchEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins25Generate_InstantiateAsmJsEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_InstantiateAsmJsEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins25Generate_InstantiateAsmJsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins25Generate_InstantiateAsmJsEPNS0_14MacroAssemblerE:
.LFB24385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$17, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$3, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	536(%rdi), %eax
	movb	$1, 536(%rdi)
	movq	$0, -104(%rbp)
	movb	%al, -129(%rbp)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movq	%r12, %rdi
	movl	%edx, %esi
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	leaq	-96(%rbp), %rax
	movq	$0, -96(%rbp)
	movl	_ZN2v88internalL3rbpE(%rip), %r14d
	movq	%rax, -128(%rbp)
.L151:
	movl	$3, %eax
	movl	$2, %r15d
	movq	$0, -88(%rbp)
	subl	%ebx, %eax
	subl	%ebx, %r15d
	cmpl	$3, %eax
	je	.L143
	movl	$1, %edx
	movl	$7, %esi
	movq	%r12, %rdi
	movabsq	$-4294967296, %r13
	andq	-120(%rbp), %r13
	movl	$8, %r8d
	orq	%rax, %r13
	movabsq	$-1095216660481, %rax
	andq	%rax, %r13
	movabsq	$81604378624, %rax
	orq	%rax, %r13
	movq	%r13, %rcx
	movq	%r13, -120(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	leaq	-88(%rbp), %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpl	$-1, %r15d
	je	.L144
.L143:
	movl	%ebx, %eax
	leaq	-68(%rbp), %r15
	negl	%eax
	leal	32(,%rax,8), %r13d
.L148:
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	subl	$8, %r13d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	cmpl	$8, %r13d
	jne	.L148
	testl	%ebx, %ebx
	je	.L145
.L144:
	xorl	%r15d, %r15d
.L150:
	movl	$4, %esi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE@PLT
	cmpl	%r15d, %ebx
	jg	.L150
	testl	%ebx, %ebx
	je	.L145
	movq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	subl	$1, %ebx
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L145:
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-104(%rbp), %r14
	leaq	-80(%rbp), %r13
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$59, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler4DropEi@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %edx
	movq	%r12, %rdi
	movl	%edx, %esi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %ebx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	_ZN2v88internalL3rcxE(%rip), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movq	%r12, %rdi
	movl	%edx, %esi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movzbl	-129(%rbp), %eax
	movl	$47, %edx
	movq	%r13, %rdi
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movb	%al, 536(%r12)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	(%r12), %rax
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%r12, %rdi
	call	*48(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L163:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24385:
	.size	_ZN2v88internal8Builtins25Generate_InstantiateAsmJsEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins25Generate_InstantiateAsmJsEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins34Generate_ContinueToCodeStubBuiltinEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_ContinueToCodeStubBuiltinEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins34Generate_ContinueToCodeStubBuiltinEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins34Generate_ContinueToCodeStubBuiltinEPNS0_14MacroAssemblerE:
.LFB24387:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb
	.cfi_endproc
.LFE24387:
	.size	_ZN2v88internal8Builtins34Generate_ContinueToCodeStubBuiltinEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins34Generate_ContinueToCodeStubBuiltinEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins44Generate_ContinueToCodeStubBuiltinWithResultEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins44Generate_ContinueToCodeStubBuiltinWithResultEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins44Generate_ContinueToCodeStubBuiltinWithResultEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins44Generate_ContinueToCodeStubBuiltinWithResultEPNS0_14MacroAssemblerE:
.LFB24388:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb
	.cfi_endproc
.LFE24388:
	.size	_ZN2v88internal8Builtins44Generate_ContinueToCodeStubBuiltinWithResultEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins44Generate_ContinueToCodeStubBuiltinWithResultEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins36Generate_ContinueToJavaScriptBuiltinEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_ContinueToJavaScriptBuiltinEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins36Generate_ContinueToJavaScriptBuiltinEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins36Generate_ContinueToJavaScriptBuiltinEPNS0_14MacroAssemblerE:
.LFB24389:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb
	.cfi_endproc
.LFE24389:
	.size	_ZN2v88internal8Builtins36Generate_ContinueToJavaScriptBuiltinEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins36Generate_ContinueToJavaScriptBuiltinEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins46Generate_ContinueToJavaScriptBuiltinWithResultEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins46Generate_ContinueToJavaScriptBuiltinWithResultEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins46Generate_ContinueToJavaScriptBuiltinWithResultEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins46Generate_ContinueToJavaScriptBuiltinWithResultEPNS0_14MacroAssemblerE:
.LFB24390:
	.cfi_startproc
	endbr64
	movl	$1, %edx
	movl	$1, %esi
	jmp	_ZN2v88internal12_GLOBAL__N_132Generate_ContinueToBuiltinHelperEPNS0_14MacroAssemblerEbb
	.cfi_endproc
.LFE24390:
	.size	_ZN2v88internal8Builtins46Generate_ContinueToJavaScriptBuiltinWithResultEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins46Generate_ContinueToJavaScriptBuiltinWithResultEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins26Generate_NotifyDeoptimizedEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_NotifyDeoptimizedEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins26Generate_NotifyDeoptimizedEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins26Generate_NotifyDeoptimizedEPNS0_14MacroAssemblerE:
.LFB24391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$17, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movzbl	536(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$1, 536(%rdi)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	$60, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-48(%rbp), %rdi
	movb	%bl, 536(%r12)
	movl	$8, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L171:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24391:
	.size	_ZN2v88internal8Builtins26Generate_NotifyDeoptimizedEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins26Generate_NotifyDeoptimizedEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins31Generate_FunctionPrototypeApplyEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_FunctionPrototypeApplyEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins31Generate_FunctionPrototypeApplyEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins31Generate_FunctionPrototypeApplyEPNS0_14MacroAssemblerE:
.LFB24392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internalL3rdxE(%rip), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movl	%ebx, %esi
	movq	$4, -160(%rbp)
	movq	$0, -152(%rbp)
	movl	$0, -144(%rbp)
	call	*88(%rax)
	movl	_ZN2v88internalL3rbxE(%rip), %r15d
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r14d
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	%r14d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-168(%rbp), %r9
	movq	%r9, %rdx
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movabsq	$81604378625, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-176(%rbp), %r10
	movq	%r10, %rdx
	movq	%r10, -184(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-184(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-192(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r9d
	movq	%r12, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-128(%rbp), %rdi
	movl	%r14d, %edx
	movl	$8, %r8d
	movl	$3, %ecx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r9d
	movq	%r12, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$6, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	$0, -160(%rbp)
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	512(%r12), %rax
	movl	$14, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	512(%r12), %rax
	movl	$2, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L175:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24392:
	.size	_ZN2v88internal8Builtins31Generate_FunctionPrototypeApplyEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins31Generate_FunctionPrototypeApplyEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins30Generate_FunctionPrototypeCallEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_FunctionPrototypeCallEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins30Generate_FunctionPrototypeCallEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins30Generate_FunctionPrototypeCallEPNS0_14MacroAssemblerE:
.LFB24393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-152(%rbp), %rbx
	subq	$120, %rsp
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -144(%rbp)
	movl	%r13d, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$8, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	$4, -144(%rbp)
	movq	$0, -136(%rbp)
	movl	$0, -128(%rbp)
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r9d
	movq	$0, -152(%rbp)
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movabsq	$4294967300, %rax
	movq	%rax, -144(%rbp)
	movq	$0, -136(%rbp)
	movl	$0, -128(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r9d
	movl	$8, %edx
	movq	%r12, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r14d, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler22DropUnderReturnAddressEiNS0_8RegisterE@PLT
	movl	$8, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movq	512(%r12), %rax
	movl	$2, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L179:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24393:
	.size	_ZN2v88internal8Builtins30Generate_FunctionPrototypeCallEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins30Generate_FunctionPrototypeCallEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins21Generate_ReflectApplyEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_ReflectApplyEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins21Generate_ReflectApplyEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins21Generate_ReflectApplyEPNS0_14MacroAssemblerE:
.LFB24394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-152(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-144(%rbp), %rbx
	subq	$120, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -128(%rbp)
	movq	$0, -152(%rbp)
	movl	%r14d, %esi
	movq	$4, -144(%rbp)
	movq	$0, -136(%rbp)
	call	*88(%rax)
	movl	_ZN2v88internalL3rdxE(%rip), %r15d
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r14d, %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r9d
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movabsq	$81604378624, %r10
	movl	$7, %esi
	movq	%r10, %rcx
	orq	$1, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movabsq	$81604378624, %r10
	movl	$7, %esi
	movq	%r10, %rcx
	orq	$3, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r9d
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r14d
	leaq	-116(%rbp), %rdi
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$8, %r8d
	movl	$3, %ecx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-108(%rbp), %ecx
	movq	-116(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	512(%r12), %rax
	movl	$14, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L183:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24394:
	.size	_ZN2v88internal8Builtins21Generate_ReflectApplyEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins21Generate_ReflectApplyEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins25Generate_ReflectConstructEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_ReflectConstructEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins25Generate_ReflectConstructEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins25Generate_ReflectConstructEPNS0_14MacroAssemblerE:
.LFB24395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-152(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-144(%rbp), %rbx
	subq	$120, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$0, -128(%rbp)
	movq	$0, -152(%rbp)
	movl	%r13d, %esi
	movq	$4, -144(%rbp)
	movq	$0, -136(%rbp)
	call	*88(%rax)
	movl	_ZN2v88internalL3rdxE(%rip), %r15d
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r13d, %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r10d
	movl	%r10d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movabsq	$81604378624, %r9
	movl	$7, %esi
	movq	%r9, %rcx
	orq	$1, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movl	$8, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r10d
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	movl	%r10d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movabsq	$81604378624, %r9
	movl	$7, %esi
	movq	%r9, %rcx
	orq	$3, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r14d
	leaq	-116(%rbp), %rdi
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$8, %r8d
	movl	$3, %ecx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-108(%rbp), %ecx
	movq	-116(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	512(%r12), %rax
	movl	$26, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movl	$16, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L187:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24395:
	.size	_ZN2v88internal8Builtins25Generate_ReflectConstructEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins25Generate_ReflectConstructEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins33Generate_InternalArrayConstructorEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_InternalArrayConstructorEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins33Generate_InternalArrayConstructorEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins33Generate_InternalArrayConstructorEPNS0_14MacroAssemblerE:
.LFB24396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L192
.L189:
	movq	512(%r12), %rax
	movl	$190, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-48(%rbp), %rdi
	movl	$55, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movl	$42, %edx
	xorl	$1, %eax
	movl	%eax, %esi
	call	_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$68, %edx
	movl	_ZN2v88internalL3rcxE(%rip), %ecx
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	movl	$42, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE@PLT
	jmp	.L189
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24396:
	.size	_ZN2v88internal8Builtins33Generate_InternalArrayConstructorEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins33Generate_InternalArrayConstructorEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins35Generate_ArgumentsAdaptorTrampolineEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_ArgumentsAdaptorTrampolineEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins35Generate_ArgumentsAdaptorTrampolineEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins35Generate_ArgumentsAdaptorTrampolineEPNS0_14MacroAssemblerE:
.LFB24399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movl	$3, %edx
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-68(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$81604378624, %r13
	pushq	%r12
	movq	%r13, %rcx
	pushq	%rbx
	orq	$65535, %rcx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	-216(%rbp), %r11
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r11, %rdx
	movl	$4, %esi
	movq	%r11, -288(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r14, %rdi
	movl	$23, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r12d
	movl	%ecx, -84(%rbp)
	movl	%r12d, %esi
	movl	%ecx, -144(%rbp)
	movq	%rdx, -92(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	$47, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rcx
	movl	$4, %r8d
	orq	$1073741824, %rcx
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%rsi, -140(%rbp)
	movl	%edx, -132(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	leaq	-200(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$5, %esi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %edx
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%r13, %rax
	movq	%rbx, %rdi
	orq	$38, %rax
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL2r8E(%rip), %r15d
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r13, -248(%rbp)
	call	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE@PLT
	leaq	-208(%rbp), %r9
	movl	%r12d, %edx
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r9, %rcx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r9, -280(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$3, %ecx
	movl	$59, %esi
	movq	$0, -192(%rbp)
	leaq	-160(%rbp), %r13
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$12, %esi
	movq	%rbx, %rdi
	leaq	-192(%rbp), %r10
	movq	%r10, %rdx
	movq	%r10, -264(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-184(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-80(%rbp), %rax
	movl	$3, %ecx
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%rax, %rdi
	movl	$16, %r8d
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r15d, %esi
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	$-1, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	$0, -160(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	-248(%rbp), %rax
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	$8, %edx
	movl	$5, %esi
	movq	%rax, %rcx
	movq	%rax, -272(%rbp)
	orq	$8, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$3, %ecx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	$12, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-176(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -248(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-264(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	$3, %ecx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	-240(%rbp), %rdi
	movl	$16, %r8d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL2r9E(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	$-1, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	leaq	-168(%rbp), %r10
	movq	%rbx, %rdi
	movq	$0, -168(%rbp)
	movq	%r10, %rsi
	movq	%r10, -264(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r15d, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movl	_ZN2v88internalL2r9E(%rip), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movq	-272(%rbp), %rax
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	$9, %edx
	movl	$5, %esi
	movq	%rax, %r9
	orq	$8, %r9
	movq	%r9, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$8, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$8, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-264(%rbp), %r10
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	$12, %esi
	movq	%r10, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	(%rbx), %rax
	movl	$4, %edx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r15d
	movq	$0, -160(%rbp)
	movl	%r15d, %esi
	call	*88(%rax)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$3, %ecx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	$12, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-248(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r14, %rdi
	movl	$47, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%ecx, -108(%rbp)
	movl	%ecx, -120(%rbp)
	movq	%rdx, -116(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	(%rbx), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	movq	512(%rbx), %r11
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	leaq	37592(%r11), %rdi
	call	_ZN2v88internal4Heap32SetArgumentsAdaptorDeoptPCOffsetEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%r14, %rdi
	movl	$-24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %edx
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %edx
	movl	$3, %ecx
	movq	%rbx, %rdi
	movl	%edx, %esi
	call	_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movq	%r14, %rdi
	movl	$8, %r8d
	movq	%rax, %rcx
	movq	%rax, %rdx
	salq	$24, %rcx
	sarq	$56, %rcx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-256(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$3, %ecx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$12, %esi
	movq	%rbx, %rdi
	movq	-232(%rbp), %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-248(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %edx
	movl	$8, %ecx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %esi
	call	_ZN2v88internal9Assembler9emit_xchgENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	$3, %edx
	movl	$43, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-240(%rbp), %r14
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	_ZN2v88internalL3rbxE(%rip), %edx
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	-264(%rbp), %r10
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r10, %rsi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-232(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	(%rbx), %rax
	movl	$4, %edx
	movl	%r15d, %esi
	movq	$0, -160(%rbp)
	movq	%rbx, %rdi
	call	*88(%rax)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$3, %ecx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	$12, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-240(%rbp), %r10
	movq	%rbx, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	-288(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	$47, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	(%rbx), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	*48(%rax)
	movq	-280(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r12d
	movl	$174, %edi
	movb	$1, 536(%rbx)
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movb	%r12b, 536(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L197:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24399:
	.size	_ZN2v88internal8Builtins35Generate_ArgumentsAdaptorTrampolineEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins35Generate_ArgumentsAdaptorTrampolineEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE
	.type	_ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE, @function
_ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE:
.LFB24400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L202
	movl	_ZN2v88internalL3rbxE(%rip), %r11d
	leaq	-120(%rbp), %rbx
	leaq	-112(%rbp), %r13
.L199:
	movl	_ZN2v88internalL2r8E(%rip), %r14d
	leaq	-136(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%r10, %rcx
	movq	%r10, -168(%rbp)
	movl	%r14d, %edx
	movl	%r11d, -172(%rbp)
	movq	$0, -136(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL2r9E(%rip), %r15d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$4, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$9, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	leaq	-128(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r9, -160(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	-172(%rbp), %r11d
	leaq	-80(%rbp), %rdi
	movl	%r15d, %edx
	movl	$15, %r8d
	movl	$3, %ecx
	movl	%r11d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	$-1, %r8d
	movl	$11, %esi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_@PLT
	movl	$5, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	(%r12), %rcx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$11, %esi
	call	*88(%rcx)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$4, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_incENS0_8RegisterEi@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-160(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$9, %ecx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	-152(%rbp), %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-168(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$174, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	_ZN2v88internalL3rbxE(%rip), %r11d
	movq	$0, -120(%rbp)
	leaq	-120(%rbp), %rbx
	leaq	-112(%rbp), %r13
	movq	$0, -112(%rbp)
	movl	%r11d, %esi
	movl	%r11d, -160(%rbp)
	call	_ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE@PLT
	movl	-160(%rbp), %r11d
	leaq	-68(%rbp), %rdi
	movl	$-1, %edx
	movl	%r11d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	$9, %esi
	movl	%ecx, -84(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%rdx, -92(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$123, %edx
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$74, %edx
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movabsq	$81604378624, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-160(%rbp), %r11d
	jmp	.L199
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24400:
	.size	_ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE, .-_ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE
	.section	.text._ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE
	.type	_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE, @function
_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE:
.LFB24401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L209
	leaq	-200(%rbp), %rax
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	leaq	-192(%rbp), %r11
	movq	%rax, -248(%rbp)
.L206:
	leaq	-80(%rbp), %rbx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	xorl	%edx, %edx
	leaq	-92(%rbp), %r15
	movq	%rbx, %rdi
	movq	%r10, -264(%rbp)
	movq	%r11, -272(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$-8, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movq	%r12, %rdi
	movabsq	$81604378662, %r8
	movl	$8, %r9d
	movl	$7, %esi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-232(%rbp), %r9
	movq	%r9, %rdx
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%rbx, %rdi
	movl	$-16, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL2r8E(%rip), %r13d
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movl	%r13d, %esi
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%rbx, %rdi
	movl	$23, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	leaq	-224(%rbp), %rbx
	movl	%ecx, -60(%rbp)
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r15, %rdi
	movl	$41, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-256(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	$-24, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %ecx
	movq	-92(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	leaq	-216(%rbp), %r14
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-208(%rbp), %rbx
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$4, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$8, %edx
	movl	$43, %esi
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$14, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	%r13d, %esi
	movl	_ZN2v88internalL3rcxE(%rip), %r15d
	movq	%r12, %rdi
	movl	%r15d, %edx
	call	_ZN2v88internal12_GLOBAL__N_127Generate_StackOverflowCheckEPNS0_14MacroAssemblerENS0_8RegisterES4_PNS0_5LabelENS5_8DistanceE
	movl	$4, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	$3, %esi
	movq	$0, -200(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	-248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-272(%rbp), %r11
	xorl	%esi, %esi
	movabsq	$34359738371, %rax
	movq	%rax, -192(%rbp)
	movabsq	$4294967296, %rax
	movq	%r11, %rdi
	movq	%rax, -184(%rbp)
	movl	$0, -176(%rbp)
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -92(%rbp)
	movl	%edx, -84(%rbp)
	call	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE@PLT
	movl	$4, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	movq	-248(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$174, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	-264(%rbp), %r10
	movq	%r10, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movl	_ZN2v88internalL3rdxE(%rip), %r13d
	leaq	-192(%rbp), %r11
	xorl	%ecx, %ecx
	leaq	-68(%rbp), %r15
	movq	%rdx, -264(%rbp)
	movq	%r11, %rdx
	movl	%r13d, %esi
	movq	%r11, -256(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r15, %rdi
	movl	$-1, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movl	%ecx, -96(%rbp)
	movl	%r14d, %esi
	movl	%ecx, -156(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdx, -164(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r15, %rdi
	movl	$13, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r12, %rdi
	leaq	-200(%rbp), %r15
	movabsq	$81604378688, %rcx
	movq	%rsi, -116(%rbp)
	movl	%edx, -108(%rbp)
	movq	%rsi, -152(%rbp)
	movl	%edx, -144(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%r15, -248(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-256(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	movzbl	536(%r12), %ebx
	movb	$1, 536(%r12)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$169, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movb	%bl, 536(%r12)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-264(%rbp), %r10
	movq	-256(%rbp), %r11
	jmp	.L206
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24401:
	.size	_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE, .-_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE
	.section	.text._ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE, @function
_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE:
.LFB24402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-68(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$81604378624, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movl	%esi, -344(%rbp)
	movl	_ZN2v88internalL3rdiE(%rip), %r12d
	movl	%r12d, %esi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	$4, -288(%rbp)
	movq	$0, -280(%rbp)
	movl	$0, -272(%rbp)
	call	_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE@PLT
	movl	$23, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	$0, -328(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %r15d
	movl	%ecx, -120(%rbp)
	movl	%r15d, %esi
	movl	%ecx, -252(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdx, -260(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$47, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rcx
	movl	$4, %r8d
	orb	$4, %ch
	movq	%rbx, %rdi
	movq	%rsi, -140(%rbp)
	movl	%edx, -132(%rbp)
	movq	%rsi, -248(%rbp)
	movl	%edx, -240(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	leaq	-328(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$5, %esi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$31, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	%ecx, -144(%rbp)
	movl	%ecx, -228(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rdx, -236(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	$47, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	movq	$0, -320(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%r13, %rcx
	orq	$96, %rcx
	movl	$4, %r8d
	movq	%rbx, %rdi
	movq	%rsi, -164(%rbp)
	movl	%edx, -156(%rbp)
	movq	%rsi, -224(%rbp)
	movl	%edx, -216(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	leaq	-320(%rbp), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movl	$5, %esi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	-344(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L213
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movl	$4, %esi
	movq	%rbx, %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %eax
	leaq	-288(%rbp), %r9
	movl	%eax, -372(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, -360(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -344(%rbp)
.L214:
	movq	%r9, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -68(%rbp)
	movq	%rax, -188(%rbp)
	movl	%edx, -60(%rbp)
	movl	%edx, -180(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-352(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-92(%rbp), %rdi
	movl	$41, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movq	%rbx, %rdi
	movl	-372(%rbp), %esi
	movl	$8, %r8d
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rdx, -176(%rbp)
	movl	%ecx, -168(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movq	-344(%rbp), %r8
	xorl	%eax, %eax
	movq	-360(%rbp), %rcx
	movw	%dx, -292(%rbp)
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL6no_regE(%rip), %edx
	movl	$1, %r9d
	movw	%ax, -300(%rbp)
	movl	$0, -304(%rbp)
	movl	$3, -296(%rbp)
	call	_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag@PLT
	movq	-368(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r13d
	movl	$17, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$40, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movb	%r13b, 536(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	leaq	-288(%rbp), %r9
	xorl	%esi, %esi
	movq	$0, -312(%rbp)
	movq	%r9, %rdi
	movq	%r9, -384(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movl	$8, %r8d
	movq	%rbx, %rdi
	movl	%edx, %ecx
	movl	%edx, -108(%rbp)
	movl	%edx, -204(%rbp)
	movl	%r13d, %esi
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movq	%rax, -116(%rbp)
	movq	%rax, -212(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	leaq	-312(%rbp), %r8
	movq	%r8, %rdx
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1024, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %eax
	movl	%eax, %ecx
	movl	%eax, -372(%rbp)
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	movq	-352(%rbp), %rdx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpl	$1, -344(%rbp)
	movq	-360(%rbp), %r8
	movq	-384(%rbp), %r9
	jne	.L219
	leaq	-296(%rbp), %rax
	movq	%rax, -360(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -344(%rbp)
.L215:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%r9, -392(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r10d
	movl	$17, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	movb	%r10b, -384(%rbp)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r8d
	movq	%rbx, %rdi
	movl	%r8d, %edx
	movl	%r8d, %esi
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r8d
	movq	%rbx, %rdi
	movl	%r8d, %esi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r8d
	movl	%r8d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	512(%rbx), %rax
	movl	$91, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	$8, %ecx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r8d
	movl	%r8d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r8d
	movq	%rbx, %rdi
	movl	%r8d, %esi
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r8d
	movq	%rbx, %rdi
	movl	%r8d, %edx
	movl	%r8d, %esi
	call	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_@PLT
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movl	$23, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movzbl	-384(%rbp), %r10d
	movb	%r10b, 536(%rbx)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%rdx, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%rdx, -200(%rbp)
	movl	%ecx, -192(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	-344(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-392(%rbp), %r9
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$4, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%r9, -392(%rbp)
	movq	%r8, -344(%rbp)
	movq	$0, -296(%rbp)
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%rbx, %rdi
	leaq	-296(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$6, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	-344(%rbp), %r8
	movq	%r8, %rdx
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-360(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %edx
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE@PLT
	leaq	-304(%rbp), %rax
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-392(%rbp), %r9
	movq	-384(%rbp), %r8
	jmp	.L215
.L221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24402:
	.size	_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE, .-_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE
	.section	.text._ZN2v88internal8Builtins30Generate_CallBoundFunctionImplEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_CallBoundFunctionImplEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins30Generate_CallBoundFunctionImplEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins30Generate_CallBoundFunctionImplEPNS0_14MacroAssemblerE:
.LFB24404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$120, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r13d, %esi
	call	_ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE@PLT
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	movl	$31, %edx
	movq	$4, -144(%rbp)
	movq	$0, -136(%rbp)
	movl	$0, -128(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movl	$-1, %r8d
	movl	%r14d, %esi
	call	_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_@PLT
	xorl	%esi, %esi
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_127Generate_PushBoundArgumentsEPNS0_14MacroAssemblerE
	leaq	-88(%rbp), %rdi
	movl	$23, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	512(%r12), %rax
	movl	$10, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$120, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24404:
	.size	_ZN2v88internal8Builtins30Generate_CallBoundFunctionImplEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins30Generate_CallBoundFunctionImplEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE, @function
_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE:
.LFB24405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-152(%rbp), %r14
	pushq	%r13
	movq	%r14, %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -128(%rbp)
	movq	$4, -144(%rbp)
	movl	%r12d, %esi
	movq	$0, -136(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1105, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movl	%r13d, %ecx
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	movq	512(%rbx), %rax
	movl	%r15d, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE@PLT
	movl	$4, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movl	$1104, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	movq	512(%rbx), %rax
	movl	$7, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movl	$4, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movl	%r13d, %esi
	leaq	-80(%rbp), %rdi
	movl	$13, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%rbx, %rdi
	movabsq	$81604378626, %rcx
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	movq	%rsi, -116(%rbp)
	movl	%edx, -108(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1024, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	movq	512(%rbx), %rax
	movl	$11, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	xorl	%esi, %esi
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -92(%rbp)
	movl	%edx, -72(%rbp)
	movl	%edx, -96(%rbp)
	movl	%edx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	%r12d, %edx
	movl	$34, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE@PLT
	movq	512(%rbx), %rax
	movl	$1, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movzbl	536(%rbx), %r13d
	movl	$17, %esi
	movq	%rbx, %rdi
	movb	$1, 536(%rbx)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$162, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movb	%r13b, 536(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L229:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24405:
	.size	_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE, .-_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE
	.section	.text._ZN2v88internal8Builtins26Generate_ConstructFunctionEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_ConstructFunctionEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins26Generate_ConstructFunctionEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins26Generate_ConstructFunctionEPNS0_14MacroAssemblerE:
.LFB24406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$80, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	%r13d, %esi
	call	_ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE@PLT
	movq	(%r12), %rax
	movl	$4, %edx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	call	*88(%rax)
	movl	%r13d, %esi
	leaq	-48(%rbp), %rdi
	movl	$23, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	leaq	-60(%rbp), %rdi
	movl	$47, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-52(%rbp), %edx
	movq	-60(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$81637933056, %rcx
	movl	$4, %r8d
	movl	%edx, -40(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	movq	512(%r12), %rax
	movl	$30, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$5, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	512(%r12), %rax
	movl	$29, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L233:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24406:
	.size	_ZN2v88internal8Builtins26Generate_ConstructFunctionEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins26Generate_ConstructFunctionEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins31Generate_ConstructBoundFunctionEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_ConstructBoundFunctionEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins31Generate_ConstructBoundFunctionEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins31Generate_ConstructBoundFunctionEPNS0_14MacroAssemblerE:
.LFB24407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r13d, %esi
	call	_ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_127Generate_PushBoundArgumentsEPNS0_14MacroAssemblerE
	movl	$8, %r8d
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$7, %edx
	movl	$59, %esi
	movq	$0, -96(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-64(%rbp), %rdi
	movl	$23, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-76(%rbp), %rdi
	movl	$23, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-76(%rbp), %rdx
	movl	-68(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movq	512(%r12), %rax
	movl	$23, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L237:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24407:
	.size	_ZN2v88internal8Builtins31Generate_ConstructBoundFunctionEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins31Generate_ConstructBoundFunctionEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins18Generate_ConstructEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_ConstructEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins18Generate_ConstructEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins18Generate_ConstructEPNS0_14MacroAssemblerE:
.LFB24408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-152(%rbp), %r15
	pushq	%r13
	movq	%r15, %rdx
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$-128, %rsp
	movl	_ZN2v88internalL3rdiE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -128(%rbp)
	movq	$4, -144(%rbp)
	movl	%r14d, %esi
	movq	$0, -136(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-64(%rbp), %rdi
	movl	$-1, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	leaq	-76(%rbp), %rdi
	movl	$13, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-76(%rbp), %rsi
	movl	-68(%rbp), %edx
	movq	%r12, %rdi
	movabsq	$81604378688, %rcx
	movq	%rsi, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$1105, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	movq	512(%r12), %rax
	movl	$20, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movl	$4, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movl	$1104, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	movq	512(%r12), %rax
	movl	$21, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movl	$4, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movl	$1024, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE@PLT
	movq	512(%r12), %rax
	movl	$39, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	xorl	%esi, %esi
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi@PLT
	movl	$8, %r8d
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -76(%rbp)
	movl	%edx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	%r14d, %edx
	movl	$33, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE@PLT
	movq	512(%r12), %rax
	movl	$2, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	512(%r12), %rax
	movl	$22, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L241:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24408:
	.size	_ZN2v88internal8Builtins18Generate_ConstructEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins18Generate_ConstructEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins38Generate_InterpreterOnStackReplacementEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins38Generate_InterpreterOnStackReplacementEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins38Generate_InterpreterOnStackReplacementEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins38Generate_InterpreterOnStackReplacementEPNS0_14MacroAssemblerE:
.LFB24409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	addq	$-128, %rsp
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movl	$8, %r8d
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r14, %rdi
	movl	$-16, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -52(%rbp)
	movl	%ecx, -44(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	movzbl	536(%r12), %r15d
	movb	$1, 536(%r12)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$53, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movsbl	24(%rax), %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movb	%r15b, 536(%r12)
	movq	%r12, %rdi
	leaq	-156(%rbp), %r15
	movq	$0, -156(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5leaveEv@PLT
	movq	%r14, %rdi
	movl	$15, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movl	%r14d, %esi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	leaq	-76(%rbp), %rdi
	movl	$47, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-76(%rbp), %rdx
	movl	-68(%rbp), %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE@PLT
	xorl	%ecx, %ecx
	leaq	-88(%rbp), %rdi
	movl	%r14d, %edx
	movl	$63, %r8d
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	movq	%rdx, -76(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	leaq	-100(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-100(%rbp), %rsi
	movl	-92(%rbp), %edx
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	movl	%edx, -80(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L245
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L245:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24409:
	.size	_ZN2v88internal8Builtins38Generate_InterpreterOnStackReplacementEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins38Generate_InterpreterOnStackReplacementEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins24Generate_WasmCompileLazyEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_WasmCompileLazyEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins24Generate_WasmCompileLazyEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins24Generate_WasmCompileLazyEPNS0_14MacroAssemblerE:
.LFB24410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	_ZN2v88internal4wasmL17kGpParamRegistersE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	24(%r13), %rbx
	subq	$72, %rsp
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r11E(%rip), %edx
	movq	%r12, %rdi
	movl	%edx, %esi
	call	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_@PLT
	movl	$11, %esi
	movq	%r12, %rdi
	movzbl	530(%r12), %eax
	movb	$1, 530(%r12)
	movb	%al, -101(%rbp)
	movzbl	536(%r12), %eax
	movb	$1, 536(%r12)
	movb	%al, -102(%rbp)
	call	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	movl	0(%r13), %esi
	movq	%r12, %rdi
	addq	$4, %r13
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	cmpq	%rbx, %r13
	jne	.L247
	movl	$4, %edx
	movl	$5, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movl	$8, %r8d
	leaq	_ZN2v88internal4wasmL17kFpParamRegistersE(%rip), %r14
	leaq	-68(%rbp), %r13
	movabsq	$81604378720, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %ebx
	.p2align 4,,10
	.p2align 3
.L248:
	movl	(%r14), %ecx
	movl	%r15d, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	addl	$16, %r15d
	addq	$4, %r14
	movl	%ecx, -100(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-100(%rbp), %ecx
	movq	-68(%rbp), %rsi
	movq	%r12, %rdi
	movl	-60(%rbp), %edx
	call	_ZN2v88internal9Assembler6movdquENS0_7OperandENS0_11XMMRegisterE@PLT
	cmpl	$96, %r15d
	jne	.L248
	movl	_ZN2v88internalL21kWasmInstanceRegisterE(%rip), %r14d
	movq	%r12, %rdi
	leaq	-80(%rbp), %r13
	movl	%r14d, %esi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r14d, %esi
	movl	$119, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r14d
	movl	$8, %r8d
	movl	%ecx, -60(%rbp)
	movl	%r14d, %esi
	movq	%rdx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r14d, %esi
	movl	$30496, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	_ZN2v88internalL16kContextRegisterE(%rip), %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_3SmiE@PLT
	movl	%r14d, %edx
	movl	$464, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler21CallRuntimeWithCEntryENS0_7Runtime10FunctionIdENS0_8RegisterE@PLT
	movl	_ZN2v88internalL16kReturnRegister0E(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3r11E(%rip), %esi
	leaq	20+_ZN2v88internal4wasmL17kFpParamRegistersE(%rip), %r14
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L249:
	movl	(%r14), %r9d
	subl	$16, %r15d
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	%r15d, %edx
	subq	$4, %r14
	movl	%r9d, -100(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-100(%rbp), %r9d
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movq	-80(%rbp), %rdx
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler6movdquENS0_11XMMRegisterENS0_7OperandE@PLT
	testl	%r15d, %r15d
	jne	.L249
	movl	$8, %r8d
	movl	$4, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movabsq	$81604378720, %rcx
	leaq	24+_ZN2v88internal4wasmL17kGpParamRegistersE(%rip), %rbx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	movl	-4(%rbx), %esi
	movq	%r12, %rdi
	subq	$4, %rbx
	call	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE@PLT
	leaq	_ZN2v88internal4wasmL17kGpParamRegistersE(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L250
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE@PLT
	movzbl	-102(%rbp), %eax
	movl	_ZN2v88internalL3r11E(%rip), %esi
	movq	%r12, %rdi
	movb	%al, 536(%r12)
	movzbl	-101(%rbp), %eax
	movb	%al, 530(%r12)
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L257:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24410:
	.size	_ZN2v88internal8Builtins24Generate_WasmCompileLazyEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins24Generate_WasmCompileLazyEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.type	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, @function
_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb:
.LFB24411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$216, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpl	$2, %esi
	movl	$0, %esi
	movl	%eax, -216(%rbp)
	cmovg	%eax, %esi
	xorl	%r15d, %r15d
	cmpl	$1, %edx
	sete	%r15b
	cmpl	$1, %ebx
	je	.L273
	cmpb	$1, %r8b
	movl	%r15d, %edx
	sbbl	%ecx, %ecx
	andl	$-18, %ecx
	addl	$21, %ecx
	call	_ZN2v88internal14MacroAssembler14EnterExitFrameEibNS0_10StackFrame4TypeE@PLT
	movl	_ZN2v88internalL3r14E(%rip), %r13d
	movl	_ZN2v88internalL3raxE(%rip), %r14d
	movl	-216(%rbp), %eax
.L261:
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L274
.L263:
	cmpl	$2, %eax
	jg	.L264
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3r15E(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	$6, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %eax
	movl	%eax, -216(%rbp)
.L265:
	movl	%r14d, %esi
	movl	$32, %edx
	movq	%r12, %rdi
	movq	$0, -204(%rbp)
	call	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE@PLT
	leaq	-204(%rbp), %r14
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L271
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	leaq	-196(%rbp), %r9
.L266:
	xorl	%edx, %edx
	testl	%ebx, %ebx
	movl	%r15d, %esi
	movq	%r12, %rdi
	sete	%dl
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal14MacroAssembler14LeaveExitFrameEbb@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	512(%r12), %rsi
	movl	$5, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	512(%r12), %rsi
	movl	$6, %edi
	movq	%rax, %r14
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	512(%r12), %rsi
	movl	$8, %edi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movq	512(%r12), %rsi
	movl	$9, %edi
	movq	%rax, %r15
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	$181, %edi
	movq	%rax, %rbx
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	_ZN2v88internalL9arg_reg_1E(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	movzbl	536(%r12), %r11d
	movq	%rax, -248(%rbp)
	movabsq	$81604378624, %rdx
	movb	$1, 536(%r12)
	movb	%r11b, -233(%rbp)
	movq	%rdx, -232(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-232(%rbp), %rdx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL9arg_reg_2E(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL9arg_reg_3E(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi@PLT
	movq	-248(%rbp), %r10
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movzbl	-233(%rbp), %r11d
	movb	%r11b, 536(%r12)
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %r14d
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movl	%r14d, %esi
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	-216(%rbp), %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r15d
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movl	%r15d, %esi
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	$0, -196(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	-256(%rbp), %r9
	movq	%r9, %rdx
	movq	%r9, -216(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-80(%rbp), %rdi
	movl	$-8, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movl	%r14d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, -60(%rbp)
	movq	%rsi, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-216(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv@PLT
	movq	-224(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %r13d
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movl	%edx, -72(%rbp)
	movl	%edx, -60(%rbp)
	movl	%r13d, %esi
	movq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movl	_ZN2v88internalL3rspE(%rip), %eax
	leaq	-68(%rbp), %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -224(%rbp)
	movl	%eax, %esi
	movl	%eax, -216(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %esi
	movq	%rdx, -116(%rbp)
	movl	%ecx, -108(%rbp)
	movq	%rdx, -188(%rbp)
	movl	%ecx, -180(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	$6, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3r15E(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movq	-224(%rbp), %r9
	movl	-216(%rbp), %esi
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL16kReturnRegister0E(%rip), %esi
	movl	$8, %r8d
	movl	%ecx, -84(%rbp)
	movl	%ecx, -168(%rbp)
	movq	%rdx, -92(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	-216(%rbp), %esi
	movl	$8, %edx
	movq	-224(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL16kReturnRegister1E(%rip), %esi
	movl	$8, %r8d
	movq	%rdx, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%rdx, -164(%rbp)
	movl	%ecx, -156(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L271:
	movq	(%r12), %rax
	movl	$5, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	$0, -196(%rbp)
	call	*88(%rax)
	movq	512(%r12), %rsi
	movl	$4, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%r13d, %edx
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE@PLT
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rdx, %r8
	movl	%edx, -72(%rbp)
	movl	%edx, -60(%rbp)
	movl	$14, %edx
	movq	%rax, -80(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-196(%rbp), %r9
	movq	%r9, %rdx
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	-224(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-224(%rbp), %r9
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%r12, %rdi
	movl	%eax, -216(%rbp)
	call	_ZN2v88internal14TurboAssembler19CheckStackAlignmentEv@PLT
	movl	-216(%rbp), %eax
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L273:
	call	_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi@PLT
	movl	_ZN2v88internalL3r14E(%rip), %r13d
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3raxE(%rip), %r14d
	movl	%r13d, %esi
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	-216(%rbp), %eax
	jmp	.L261
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24411:
	.size	_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, .-_ZN2v88internal8Builtins15Generate_CEntryEPNS0_14MacroAssemblerEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.section	.text._ZN2v88internal8Builtins18Generate_DoubleToIEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_DoubleToIEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins18Generate_DoubleToIEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins18Generate_DoubleToIEPNS0_14MacroAssemblerE:
.LFB24412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-140(%rbp), %rdi
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internalL3rspE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -164(%rbp)
	movq	$0, -156(%rbp)
	movl	%r13d, %esi
	movq	$0, -148(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	$36, %edx
	movl	%r13d, %esi
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-140(%rbp), %rax
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, -116(%rbp)
	movl	-132(%rbp), %eax
	movl	%r13d, %esi
	movl	%eax, -108(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	-132(%rbp), %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	-140(%rbp), %rdx
	movl	$3, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	-132(%rbp), %r9d
	movq	-140(%rbp), %r8
	movl	%r9d, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L277
	subq	$8, %rsp
	movl	$15, %edx
	movq	%r12, %rdi
	movq	%r8, -80(%rbp)
	pushq	$0
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$16, %esi
	pushq	$1
	pushq	$3
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L278:
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movl	$4, %r8d
	movl	%r13d, %esi
	movabsq	$81604378624, %rbx
	movq	%r12, %rdi
	leaq	-148(%rbp), %r14
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	orq	$2146435072, %rcx
	movl	$4, %r8d
	movl	$4, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%rbx, %rdx
	movl	$5, %ecx
	movl	%r13d, %esi
	orq	$20, %rdx
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	leaq	-80(%rbp), %rdi
	movl	%r13d, %esi
	movl	$-1023, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	xorl	%esi, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	leaq	-164(%rbp), %r15
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	%rbx, %rcx
	movl	$4, %r8d
	xorl	%edx, %edx
	orq	$52, %rcx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-156(%rbp), %r9
	movq	%r9, %rdx
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	orq	$1075, %rcx
	movl	$4, %r8d
	movl	$5, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$4, %r8d
	movl	$51, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	movl	$4, %r8d
	orq	$31, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterEii@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	-184(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$4, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$3, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_negENS0_8RegisterEi@PLT
	movq	-128(%rbp), %rdx
	movl	-120(%rbp), %ecx
	movq	%rbx, %r8
	movl	$4, %r9d
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	xorl	%edx, %edx
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	$15, %esi
	call	_ZN2v88internal9Assembler5cmovlENS0_9ConditionENS0_8RegisterES3_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-108(%rbp), %edx
	movq	-116(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L278
.L281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24412:
	.size	_ZN2v88internal8Builtins18Generate_DoubleToIEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins18Generate_DoubleToIEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins37Generate_InternalArrayConstructorImplEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_InternalArrayConstructorImplEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins37Generate_InternalArrayConstructorImplEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins37Generate_InternalArrayConstructorImplEPNS0_14MacroAssemblerE:
.LFB24413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	jne	.L286
.L283:
	movq	512(%r12), %rax
	movl	$191, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movl	_ZN2v88internalL3rdiE(%rip), %r14d
	leaq	-64(%rbp), %rdi
	movl	$55, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE@PLT
	movq	%r12, %rdi
	movl	$41, %edx
	xorl	$1, %eax
	movl	%eax, %esi
	call	_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE@PLT
	movl	%r13d, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$68, %edx
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_@PLT
	movq	%r12, %rdi
	movl	$41, %edx
	movl	$4, %esi
	call	_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE@PLT
	leaq	-76(%rbp), %rdi
	movl	%r14d, %esi
	movl	$55, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-68(%rbp), %ecx
	movq	-76(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%ecx, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	leaq	-88(%rbp), %rdi
	movl	%r13d, %esi
	movl	$14, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-88(%rbp), %rdx
	movl	-80(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movabsq	$81604378624, %r13
	movq	%rdx, -76(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal9Assembler11emit_movzxbENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %ecx
	orq	$3, %rdx
	movl	$8, %r8d
	movl	$1, %esi
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$1, %edx
	orq	$31, %rcx
	movl	$8, %r8d
	movl	$4, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$1, %edx
	orq	$2, %rcx
	movl	$4, %r8d
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r12, %rdi
	movl	$11, %edx
	movl	$4, %esi
	call	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	movl	$56, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE@PLT
	jmp	.L283
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24413:
	.size	_ZN2v88internal8Builtins37Generate_InternalArrayConstructorImplEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins37Generate_InternalArrayConstructorImplEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins24Generate_CallApiCallbackEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_CallApiCallbackEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins24Generate_CallApiCallbackEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins24Generate_CallApiCallbackEPNS0_14MacroAssemblerE:
.LFB24416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$176, %rsp
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$4, %edx
	movl	%r14d, %esi
	call	*88(%rax)
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r13d
	movq	%r15, %rdi
	movl	$8, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	leaq	-76(%rbp), %r15
	movl	$8, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	%r15, %rdi
	movl	$40, %r8d
	movl	$3, %ecx
	movl	$1, %edx
	movl	$3, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-68(%rbp), %ecx
	movq	-76(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	movl	%ecx, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r15, %rdi
	movl	$8, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-76(%rbp), %rsi
	movl	-68(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %ecx
	leaq	-100(%rbp), %r15
	movq	%rsi, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	leaq	-88(%rbp), %rdi
	movl	$16, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-88(%rbp), %rsi
	movl	-80(%rbp), %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$1, %ecx
	movq	%rsi, -76(%rbp)
	movl	%edx, -68(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	$56, %ecx
	movl	$3, %edx
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterENS0_11ScaleFactorEi@PLT
	movl	-92(%rbp), %ecx
	movq	-100(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$24, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-100(%rbp), %rsi
	movl	-92(%rbp), %edx
	movl	%r14d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	movl	%edx, -80(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	xorl	%edx, %edx
	leaq	-112(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-104(%rbp), %ecx
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %esi
	movl	%ecx, -92(%rbp)
	movq	%rdx, -100(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	call	_ZN2v88internal17ExternalReference24invoke_function_callbackEv@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$40, %edx
	leaq	-208(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	leaq	-124(%rbp), %rdi
	movl	$24, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-116(%rbp), %eax
	subq	$16, %rsp
	movq	-124(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$6, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	movl	%eax, -104(%rbp)
	leaq	-196(%rbp), %r9
	movl	%eax, -188(%rbp)
	movq	-208(%rbp), %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, (%rsp)
	movl	-200(%rbp), %eax
	movq	%rdx, -196(%rbp)
	movq	%r14, %rdx
	movl	%eax, 8(%rsp)
	call	_ZN2v88internal12_GLOBAL__N_124CallApiFunctionAndReturnEPNS0_14MacroAssemblerENS0_8RegisterENS0_17ExternalReferenceES4_iPNS0_7OperandES6_
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L291:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24416:
	.size	_ZN2v88internal8Builtins24Generate_CallApiCallbackEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins24Generate_CallApiCallbackEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins22Generate_CallApiGetterEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_CallApiGetterEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins22Generate_CallApiGetterEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins22Generate_CallApiGetterEPNS0_14MacroAssemblerE:
.LFB24417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal19ApiGetterDescriptor16ReceiverRegisterEv@PLT
	movl	%eax, %r15d
	call	_ZN2v88internal19ApiGetterDescriptor14HolderRegisterEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal19ApiGetterDescriptor16CallbackRegisterEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r13d, %esi
	leaq	-64(%rbp), %rdi
	movl	$55, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	movq	%r12, %rdi
	movl	$-1, %r8d
	movl	$-1, %ecx
	movq	%rsi, -52(%rbp)
	movl	%edx, -44(%rbp)
	movq	%rsi, -160(%rbp)
	movl	%edx, -152(%rbp)
	call	_ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r15d
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$4, %edx
	movl	%r15d, %esi
	call	*88(%rax)
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	-88(%rbp), %r14
	call	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4PushENS0_3SmiE@PLT
	movl	%r13d, %esi
	leaq	-76(%rbp), %rdi
	movl	$7, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-68(%rbp), %edx
	movq	-76(%rbp), %rsi
	movq	%r12, %rdi
	movl	$-1, %ecx
	movl	%edx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r15d
	movq	%r14, %rdi
	movl	$16, %edx
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %rdx
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	movq	%rdx, -76(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-88(%rbp), %rsi
	movl	-80(%rbp), %edx
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	leaq	-100(%rbp), %r15
	movq	%rsi, -76(%rbp)
	movl	%edx, -68(%rbp)
	movq	%rsi, -172(%rbp)
	movl	%edx, -164(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	xorl	%esi, %esi
	movl	$-8, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-100(%rbp), %rdx
	movl	-92(%rbp), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %esi
	movq	%rdx, -88(%rbp)
	movl	%ecx, -80(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	-164(%rbp), %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	-172(%rbp), %rdx
	movl	$6, %esi
	movl	%ecx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	call	_ZN2v88internal17ExternalReference31invoke_accessor_getter_callbackEv@PLT
	movl	%r13d, %esi
	movl	$47, %edx
	movq	%r15, %rdi
	movq	%rax, %r14
	leaq	-112(%rbp), %r13
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-92(%rbp), %ecx
	movq	-100(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%ecx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE@PLT
	xorl	%esi, %esi
	movl	$7, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-104(%rbp), %ecx
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$8, %esi
	movl	%ecx, -92(%rbp)
	movq	%rdx, -100(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$56, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-112(%rbp), %rax
	subq	$16, %rsp
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movl	$2, %ecx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, (%rsp)
	movl	-104(%rbp), %eax
	movl	$8, %r8d
	movl	%eax, 8(%rsp)
	call	_ZN2v88internal12_GLOBAL__N_124CallApiFunctionAndReturnEPNS0_14MacroAssemblerENS0_8RegisterENS0_17ExternalReferenceES4_iPNS0_7OperandES6_
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L295:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24417:
	.size	_ZN2v88internal8Builtins22Generate_CallApiGetterEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins22Generate_CallApiGetterEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins21Generate_DirectCEntryEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_DirectCEntryEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins21Generate_DirectCEntryEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins21Generate_DirectCEntryEPNS0_14MacroAssemblerE:
.LFB24418:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal9Assembler4int3Ev@PLT
	.cfi_endproc
.LFE24418:
	.size	_ZN2v88internal8Builtins21Generate_DirectCEntryEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins21Generate_DirectCEntryEPNS0_14MacroAssemblerE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm:
.LFB30732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30732:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm, .-_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins16Generate_AdaptorEPNS0_14MacroAssemblerEm
	.section	.rodata._ZN2v88internal4wasmL17kFpParamRegistersE,"a"
	.align 16
	.type	_ZN2v88internal4wasmL17kFpParamRegistersE, @object
	.size	_ZN2v88internal4wasmL17kFpParamRegistersE, 24
_ZN2v88internal4wasmL17kFpParamRegistersE:
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.section	.rodata._ZN2v88internal4wasmL17kGpParamRegistersE,"a"
	.align 16
	.type	_ZN2v88internal4wasmL17kGpParamRegistersE, @object
	.size	_ZN2v88internal4wasmL17kGpParamRegistersE, 24
_ZN2v88internal4wasmL17kGpParamRegistersE:
	.long	6
	.long	0
	.long	2
	.long	1
	.long	3
	.long	9
	.section	.rodata._ZN2v88internalL17kScratchDoubleRegE,"a"
	.align 4
	.type	_ZN2v88internalL17kScratchDoubleRegE, @object
	.size	_ZN2v88internalL17kScratchDoubleRegE, 4
_ZN2v88internalL17kScratchDoubleRegE:
	.long	15
	.section	.rodata._ZN2v88internalL16kScratchRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL16kScratchRegisterE, @object
	.size	_ZN2v88internalL16kScratchRegisterE, 4
_ZN2v88internalL16kScratchRegisterE:
	.long	10
	.section	.rodata._ZN2v88internalL4xmm0E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm0E, @object
	.size	_ZN2v88internalL4xmm0E, 4
_ZN2v88internalL4xmm0E:
	.zero	4
	.section	.rodata._ZN2v88internalL6no_regE,"a"
	.align 4
	.type	_ZN2v88internalL6no_regE, @object
	.size	_ZN2v88internalL6no_regE, 4
_ZN2v88internalL6no_regE:
	.long	-1
	.section	.rodata._ZN2v88internalL3r15E,"a"
	.align 4
	.type	_ZN2v88internalL3r15E, @object
	.size	_ZN2v88internalL3r15E, 4
_ZN2v88internalL3r15E:
	.long	15
	.set	_ZN2v88internalL33kInterpreterDispatchTableRegisterE,_ZN2v88internalL3r15E
	.section	.rodata._ZN2v88internalL3r14E,"a"
	.align 4
	.type	_ZN2v88internalL3r14E, @object
	.size	_ZN2v88internalL3r14E, 4
_ZN2v88internalL3r14E:
	.long	14
	.set	_ZN2v88internalL33kInterpreterBytecodeArrayRegisterE,_ZN2v88internalL3r14E
	.section	.rodata._ZN2v88internalL3r13E,"a"
	.align 4
	.type	_ZN2v88internalL3r13E, @object
	.size	_ZN2v88internalL3r13E, 4
_ZN2v88internalL3r13E:
	.long	13
	.set	_ZN2v88internalL13kRootRegisterE,_ZN2v88internalL3r13E
	.section	.rodata._ZN2v88internalL3r12E,"a"
	.align 4
	.type	_ZN2v88internalL3r12E, @object
	.size	_ZN2v88internalL3r12E, 4
_ZN2v88internalL3r12E:
	.long	12
	.section	.rodata._ZN2v88internalL3r11E,"a"
	.align 4
	.type	_ZN2v88internalL3r11E, @object
	.size	_ZN2v88internalL3r11E, 4
_ZN2v88internalL3r11E:
	.long	11
	.section	.rodata._ZN2v88internalL2r9E,"a"
	.align 4
	.type	_ZN2v88internalL2r9E, @object
	.size	_ZN2v88internalL2r9E, 4
_ZN2v88internalL2r9E:
	.long	9
	.set	_ZN2v88internalL34kInterpreterBytecodeOffsetRegisterE,_ZN2v88internalL2r9E
	.section	.rodata._ZN2v88internalL2r8E,"a"
	.align 4
	.type	_ZN2v88internalL2r8E, @object
	.size	_ZN2v88internalL2r8E, 4
_ZN2v88internalL2r8E:
	.long	8
	.section	.rodata._ZN2v88internalL3rdiE,"a"
	.align 4
	.type	_ZN2v88internalL3rdiE, @object
	.size	_ZN2v88internalL3rdiE, 4
_ZN2v88internalL3rdiE:
	.long	7
	.set	_ZN2v88internalL9arg_reg_1E,_ZN2v88internalL3rdiE
	.section	.rodata._ZN2v88internalL3rsiE,"a"
	.align 4
	.type	_ZN2v88internalL3rsiE, @object
	.size	_ZN2v88internalL3rsiE, 4
_ZN2v88internalL3rsiE:
	.long	6
	.set	_ZN2v88internalL21kWasmInstanceRegisterE,_ZN2v88internalL3rsiE
	.set	_ZN2v88internalL16kContextRegisterE,_ZN2v88internalL3rsiE
	.set	_ZN2v88internalL9arg_reg_2E,_ZN2v88internalL3rsiE
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.section	.rodata._ZN2v88internalL3rbxE,"a"
	.align 4
	.type	_ZN2v88internalL3rbxE, @object
	.size	_ZN2v88internalL3rbxE, 4
_ZN2v88internalL3rbxE:
	.long	3
	.set	_ZN2v88internalL32kJavaScriptCallExtraArg1RegisterE,_ZN2v88internalL3rbxE
	.section	.rodata._ZN2v88internalL3rdxE,"a"
	.align 4
	.type	_ZN2v88internalL3rdxE, @object
	.size	_ZN2v88internalL3rdxE, 4
_ZN2v88internalL3rdxE:
	.long	2
	.set	_ZN2v88internalL16kReturnRegister1E,_ZN2v88internalL3rdxE
	.set	_ZN2v88internalL9arg_reg_3E,_ZN2v88internalL3rdxE
	.section	.rodata._ZN2v88internalL3rcxE,"a"
	.align 4
	.type	_ZN2v88internalL3rcxE, @object
	.size	_ZN2v88internalL3rcxE, 4
_ZN2v88internalL3rcxE:
	.long	1
	.set	_ZN2v88internalL32kJavaScriptCallCodeStartRegisterE,_ZN2v88internalL3rcxE
	.set	_ZN2v88internalL9arg_reg_4E,_ZN2v88internalL3rcxE
	.section	.rodata._ZN2v88internalL3raxE,"a"
	.align 4
	.type	_ZN2v88internalL3raxE, @object
	.size	_ZN2v88internalL3raxE, 4
_ZN2v88internalL3raxE:
	.zero	4
	.set	_ZN2v88internalL31kInterpreterAccumulatorRegisterE,_ZN2v88internalL3raxE
	.set	_ZN2v88internalL16kReturnRegister0E,_ZN2v88internalL3raxE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
