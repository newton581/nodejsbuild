	.file	"builtins-boolean-gen.cc"
	.text
	.section	.rodata._ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/builtins/builtins-boolean-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"BooleanPrototypeToString"
.LC2:
	.string	"Boolean.prototype.toString"
	.section	.text._ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB13300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$17, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$240, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L6
.L2:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC2(%rip), %r8
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L2
.L7:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13300:
	.size	_ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33BooleanPrototypeToStringAssembler36GenerateBooleanPrototypeToStringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33BooleanPrototypeToStringAssembler36GenerateBooleanPrototypeToStringImplEv
	.type	_ZN2v88internal33BooleanPrototypeToStringAssembler36GenerateBooleanPrototypeToStringImplEv, @function
_ZN2v88internal33BooleanPrototypeToStringAssembler36GenerateBooleanPrototypeToStringImplEv:
.LFB13304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	leaq	.LC2(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13304:
	.size	_ZN2v88internal33BooleanPrototypeToStringAssembler36GenerateBooleanPrototypeToStringImplEv, .-_ZN2v88internal33BooleanPrototypeToStringAssembler36GenerateBooleanPrototypeToStringImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_BooleanPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"BooleanPrototypeValueOf"
.LC4:
	.string	"Boolean.prototype.valueOf"
	.section	.text._ZN2v88internal8Builtins32Generate_BooleanPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_BooleanPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_BooleanPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_BooleanPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB13309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$29, %ecx
	leaq	.LC0(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$241, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L14
.L11:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC4(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L11
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13309:
	.size	_ZN2v88internal8Builtins32Generate_BooleanPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_BooleanPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32BooleanPrototypeValueOfAssembler35GenerateBooleanPrototypeValueOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32BooleanPrototypeValueOfAssembler35GenerateBooleanPrototypeValueOfImplEv
	.type	_ZN2v88internal32BooleanPrototypeValueOfAssembler35GenerateBooleanPrototypeValueOfImplEv, @function
_ZN2v88internal32BooleanPrototypeValueOfAssembler35GenerateBooleanPrototypeValueOfImplEv:
.LFB13313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	leaq	.LC4(%rip), %r8
	call	_ZN2v88internal17CodeStubAssembler11ToThisValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_13PrimitiveTypeEPKc@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE13313:
	.size	_ZN2v88internal32BooleanPrototypeValueOfAssembler35GenerateBooleanPrototypeValueOfImplEv, .-_ZN2v88internal32BooleanPrototypeValueOfAssembler35GenerateBooleanPrototypeValueOfImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB17034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17034:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins33Generate_BooleanPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
