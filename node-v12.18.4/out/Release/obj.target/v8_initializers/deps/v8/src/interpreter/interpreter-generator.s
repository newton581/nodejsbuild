	.file	"interpreter-generator.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8880:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8880:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB27806:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27806:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB27810:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27810:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB27814:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27814:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB27821:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L25
	cmpl	$3, %edx
	je	.L26
	cmpl	$1, %edx
	je	.L30
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27821:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB27825:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L32
	cmpl	$3, %edx
	je	.L33
	cmpl	$1, %edx
	je	.L37
.L33:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27825:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8874:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8874:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8873:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8882:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8882:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD2Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD2Ev:
.LFB29559:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	jmp	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	.cfi_endproc
.LFE29559:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD2Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD2Ev
	.set	_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD1Ev,_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD2Ev
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD0Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD0Ev:
.LFB29561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29561:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD0Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD0Ev
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD2Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD2Ev:
.LFB29555:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	jmp	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	.cfi_endproc
.LFE29555:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD2Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD2Ev
	.set	_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD1Ev,_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD2Ev
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD0Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD0Ev:
.LFB29557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29557:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD0Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD0Ev
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD2Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD2Ev:
.LFB29563:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	jmp	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	.cfi_endproc
.LFE29563:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD2Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD2Ev
	.set	_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD1Ev,_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD2Ev
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD0Ev, @function
_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD0Ev:
.LFB29565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29565:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD0Ev, .-_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD0Ev
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB27809:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	.cfi_endproc
.LFE27809:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB27824:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	.cfi_endproc
.LFE27824:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/interpreter/interpreter-generator.cc"
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"DebugBreakWide"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23541:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$2, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23541:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128DebugBreakExtraWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"DebugBreakExtraWide"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128DebugBreakExtraWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128DebugBreakExtraWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128DebugBreakExtraWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23549:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$3, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L60:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23549:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128DebugBreakExtraWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128DebugBreakExtraWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak0Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"DebugBreak0"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak0Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak0Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak0Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23485:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$4, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23485:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak0Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak0Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak1Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"DebugBreak1"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak1Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak1Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak1Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23493:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$5, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L68:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23493:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak1Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak1Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak2Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"DebugBreak2"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak2Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak2Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak2Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23501:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$6, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23501:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak2Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak2Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak3Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"DebugBreak3"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak3Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak3Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak3Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23509:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$7, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23509:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak3Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak3Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak4Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"DebugBreak4"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak4Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak4Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak4Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23517:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$8, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L80:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23517:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak4Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak4Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak5Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"DebugBreak5"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak5Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak5Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak5Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23525:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$9, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23525:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak5Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak5Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak6Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"DebugBreak6"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak6Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak6Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak6Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23533:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$10, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3004, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler15MaybeDropFramesEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18DispatchToBytecodeEPNS0_8compiler4NodeES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23533:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak6Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak6Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	.cfi_endproc
.LFE27805:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_122LdaLookupSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"LdaLookupSlot"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_122LdaLookupSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_122LdaLookupSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_122LdaLookupSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22253:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$30, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$317, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-32(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, -32(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$303, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22253:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_122LdaLookupSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_122LdaLookupSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_134LdaLookupSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"LdaLookupSlotInsideTypeof"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_134LdaLookupSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_134LdaLookupSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_134LdaLookupSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22261:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$33, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$329, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-32(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, -32(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$304, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L98:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22261:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_134LdaLookupSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_134LdaLookupSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_127CreateBlockContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"CreateBlockContext"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_127CreateBlockContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateBlockContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_127CreateBlockContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23338:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-126, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2729, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-32(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, -32(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$314, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23338:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateBlockContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_127CreateBlockContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data:
.LFB27813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %r8
	movl	(%rdi), %esi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27813:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB27820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27820:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_127CollectTypeProfileAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"CollectTypeProfile"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_127CollectTypeProfileAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_127CollectTypeProfileAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_127CollectTypeProfileAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22416:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$51, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$710, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, -64(%rbp)
	movq	%rax, %rdx
	movl	$3, %r8d
	movl	$208, %esi
	movq	%rbx, -56(%rbp)
	movq	%r13, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22416:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_127CollectTypeProfileAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_127CollectTypeProfileAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_118StaGlobalAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"StaGlobal"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_118StaGlobalAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_118StaGlobalAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_118StaGlobalAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22197:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$21, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-464(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$504, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$212, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC15(%rip), %rsi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$377, %edx
	leaq	-496(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm1
	leaq	-96(%rbp), %r10
	xorl	%esi, %esi
	movl	$4, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	movl	$1, %ecx
	movq	-520(%rbp), %xmm0
	pushq	%rbx
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%r10
	leaq	-512(%rbp), %rdx
	movhps	-528(%rbp), %xmm0
	movq	%rax, -512(%rbp)
	movq	-480(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-536(%rbp), %xmm0
	movq	%rax, -504(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r10, -536(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	movl	$127, %esi
	movq	%r12, %rdi
	movq	-536(%rbp), %r10
	movl	$2, %r8d
	movq	-528(%rbp), %xmm0
	movq	%r10, %rcx
	movhps	-520(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22197:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_118StaGlobalAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_118StaGlobalAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_117ToObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"ToObject"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_117ToObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_117ToObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_117ToObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22728:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$119, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-160(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1357, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$91, %edx
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r9
	movq	%rbx, -48(%rbp)
	movl	$1, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rdi
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -208(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L118:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22728:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_117ToObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_117ToObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_127CreateCatchContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"CreateCatchContext"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_127CreateCatchContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateCatchContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_127CreateCatchContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23346:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-125, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2740, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %rdx
	movl	$2, %r8d
	movl	$315, %esi
	movq	%r13, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23346:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateCatchContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_127CreateCatchContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_126CreateWithContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"CreateWithContext"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126CreateWithContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126CreateWithContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126CreateWithContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-122, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2781, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %rdx
	movl	$2, %r8d
	movl	$317, %esi
	movq	%r13, -56(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23370:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126CreateWithContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_126CreateWithContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_135StaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"StaNamedPropertyNoFeedback"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_135StaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_135StaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_135StaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22384:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$46, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$624, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, -64(%rbp)
	movq	%rax, %rdx
	movl	$3, %r8d
	movl	$252, %esi
	movq	%rbx, -56(%rbp)
	movq	%r13, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L130:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22384:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_135StaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_135StaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_135LdaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"LdaNamedPropertyNoFeedback"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_135LdaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_135LdaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_135LdaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22336:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$41, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$541, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	movl	$2, %edi
	movq	%r13, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	leaq	-208(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-224(%rbp), %xmm0
	movq	%rax, -208(%rbp)
	movq	-176(%rbp), %rax
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L134:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22336:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_135LdaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_135LdaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120GetIteratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"GetIterator"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120GetIteratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120GetIteratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120GetIteratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23613:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-78, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3202, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$819, %edx
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	movq	%r13, %r9
	movq	%rbx, -48(%rbp)
	movl	$3, %edi
	movq	%rax, %r8
	movq	-232(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	leaq	-224(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-240(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -224(%rbp)
	movq	-192(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L138:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23613:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120GetIteratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120GetIteratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_123LdaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"LdaContextSlot"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_123LdaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_123LdaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_123LdaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22205:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$24, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$241, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$136, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L142:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22205:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_123LdaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_123LdaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_132LdaImmutableContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"LdaImmutableContextSlot"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_132LdaImmutableContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_132LdaImmutableContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_132LdaImmutableContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22213:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$25, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$255, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$136, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L146:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22213:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_132LdaImmutableContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_132LdaImmutableContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_123StaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"StaContextSlot"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_123StaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_123StaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_123StaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22237:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$28, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$291, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19StoreContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L150:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22237:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_123StaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_123StaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_137InterpreterLookupContextSlotAssembler17LookupContextSlotENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_137InterpreterLookupContextSlotAssembler17LookupContextSlotENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_137InterpreterLookupContextSlotAssembler17LookupContextSlotENS0_7Runtime10FunctionIdE:
.LFB22269:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r15
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE@PLT
	movq	-200(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	leaq	-64(%rbp), %rcx
	movq	%r13, %rdx
	movl	%ebx, %esi
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L154:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22269:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_137InterpreterLookupContextSlotAssembler17LookupContextSlotENS0_7Runtime10FunctionIdE, .-_ZN2v88internal11interpreter12_GLOBAL__N_137InterpreterLookupContextSlotAssembler17LookupContextSlotENS0_7Runtime10FunctionIdE
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"axG",@progbits,_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movq	(%rsi), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	.cfi_endproc
.LFE27065:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_115TestInAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"TestIn"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_115TestInAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_115TestInAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_115TestInAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23023:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$111, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-192(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$264, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1894, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	leaq	-256(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$390, %edx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movl	$4, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-272(%rbp), %xmm0
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -240(%rbp)
	movq	-208(%rbp), %rax
	leaq	-240(%rbp), %rdx
	movhps	-264(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-280(%rbp), %xmm0
	movq	%rax, -232(%rbp)
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L160:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23023:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_115TestInAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_115TestInAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_126StaInArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"StaInArrayLiteral"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126StaInArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126StaInArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126StaInArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22400:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$49, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-192(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$256, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$666, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	leaq	-256(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$383, %edx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movq	%rbx, %r9
	movq	%r14, -48(%rbp)
	movl	$5, %edi
	movq	%rax, %r8
	movq	-264(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	leaq	-240(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movhps	-272(%rbp), %xmm0
	pushq	%rsi
	xorl	%esi, %esi
	movaps	%xmm0, -80(%rbp)
	movq	-280(%rbp), %xmm0
	movq	%rax, -240(%rbp)
	movq	-208(%rbp), %rax
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L164:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22400:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126StaInArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_126StaInArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data:
.LFB27829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	cmpq	$0, 16(%rax)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L168
	movq	%rax, %rdi
	call	*24(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE27829:
	.size	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126LdaModuleVariableAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126LdaModuleVariableAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126LdaModuleVariableAssembler12GenerateImplEv:
.LFB22428:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-192(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE@PLT
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-456(%rbp), %r10
	movl	$56, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE(%rip), %ecx
	movq	%r10, %rsi
	movq	%r10, -464(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	pushq	$0
	movq	-456(%rbp), %r11
	movl	$2, %r9d
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-464(%rbp), %r10
	movl	$64, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE(%rip), %ecx
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	$-1, %rsi
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	-456(%rbp), %r10
	movl	$1, %r8d
	movq	%rax, %rdx
	movl	$0, (%rsp)
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movl	$8, %edx
	movl	$1800, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L172:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22428:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126LdaModuleVariableAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_126LdaModuleVariableAssembler12GenerateImplEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126StaModuleVariableAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126StaModuleVariableAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126StaModuleVariableAssembler12GenerateImplEv:
.LFB22436:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	leaq	-320(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-192(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17GetContextAtDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEE@PLT
	movl	$2, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-456(%rbp), %r9
	movl	$56, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE(%rip), %ecx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	pushq	$0
	movq	-456(%rbp), %r11
	movl	$2, %r9d
	movq	%rax, %rdx
	movl	$1, %r8d
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movq	-464(%rbp), %r10
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$50, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22436:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126StaModuleVariableAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_126StaModuleVariableAssembler12GenerateImplEv
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_133StaDataPropertyInLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"StaDataPropertyInLiteral"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_133StaDataPropertyInLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_133StaDataPropertyInLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_133StaDataPropertyInLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22408:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$50, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-192(%rbp), %r12
	movq	%r12, %rdi
	subq	$224, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$695, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC27(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-80(%rbp), %rcx
	movl	$216, %esi
	movq	%r12, %rdi
	movq	-240(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$6, %r8d
	movq	-216(%rbp), %xmm1
	movq	-200(%rbp), %xmm2
	movhps	-232(%rbp), %xmm0
	movhps	-224(%rbp), %xmm1
	movhps	-208(%rbp), %xmm2
	movaps	%xmm1, -64(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$224, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L180:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22408:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_133StaDataPropertyInLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_133StaDataPropertyInLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler8BigIntOpEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler8BigIntOpEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler8BigIntOpEPNS0_8compiler4NodeE:
.LFB22747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	subq	$56, %rsp
	movq	%rsi, -64(%rbp)
	movl	116(%rdi), %esi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-64(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	movl	$32, %esi
	movhps	-56(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L184
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22747:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler8BigIntOpEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler8BigIntOpEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl8BigIntOpEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl8BigIntOpEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl8BigIntOpEPNS0_8compiler4NodeE:
.LFB22691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$56, %rsp
	movq	%rsi, -64(%rbp)
	movl	$13, %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-64(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$2, %r8d
	movl	$32, %esi
	movhps	-56(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L188
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L188:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22691:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl8BigIntOpEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl8BigIntOpEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler7FloatOpEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler7FloatOpEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler7FloatOpEPNS0_8compiler4NodeE:
.LFB22746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	.LC28(%rip), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	cmpl	$14, 116(%rdi)
	movq	%rsi, %r12
	movq	%r13, %rdi
	je	.L193
	call	_ZN2v88internal8compiler13CodeAssembler15Float64ConstantEd@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10Float64SubENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	call	_ZN2v88internal8compiler13CodeAssembler15Float64ConstantEd@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10Float64AddENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22746:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler7FloatOpEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler7FloatOpEPNS0_8compiler4NodeE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_, @function
_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_:
.LFB22745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$1, %esi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$312, %rsp
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -352(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-352(%rbp), %r9
	movq	-344(%rbp), %r10
	cmpl	$14, 116(%r9)
	movq	%r10, %rdx
	je	.L199
	call	_ZN2v88internal17CodeStubAssembler9TrySmiSubENS0_8compiler5TNodeINS0_3SmiEEES5_PNS2_18CodeAssemblerLabelE@PLT
	movq	%rax, %r8
.L196:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-328(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15CombineFeedbackEPNS0_8compiler21CodeAssemblerVariableEi@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-344(%rbp), %r8
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$312, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	call	_ZN2v88internal17CodeStubAssembler9TrySmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_PNS2_18CodeAssemblerLabelE@PLT
	movq	%rax, %r8
	jmp	.L196
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22745:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_, .-_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv:
.LFB22678:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1184(%rbp), %r13
	pushq	%r12
	movq	%r13, %xmm1
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1232(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	subq	$1288, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1264(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%rbx, %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1200(%rbp), %rax
	movl	$13, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1240(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-1120(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movdqa	-1264(%rbp), %xmm0
	movq	%rdx, %r9
	movl	$1, %r8d
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%r9, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-992(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$1, %edx
	leaq	-864(%rbp), %r11
	leaq	-224(%rbp), %rcx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -224(%rbp)
	movq	%rcx, -1272(%rbp)
	movq	%r11, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-736(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-608(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -1320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-480(%rbp), %rdx
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -1288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-352(%rbp), %rdx
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r10, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1272(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rdi
	movq	%rbx, -1304(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1312(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15IsHeapNumberMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1320(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1312(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal17CodeStubAssembler20IsBigIntInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-1296(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1312(%rbp), %r8
	movl	$67, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1272(%rbp), %rcx
	movq	-1288(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1328(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%r14), %rax
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	-1240(%rbp), %r8
	movq	-1280(%rbp), %rcx
	movq	%r14, %rdi
	call	*16(%rax)
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1320(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1240(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*32(%rax)
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15CombineFeedbackEPNS0_8compiler21CodeAssemblerVariableEi@PLT
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$15, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17OverwriteFeedbackEPNS0_8compiler21CodeAssemblerVariableEi@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1304(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$127, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17OverwriteFeedbackEPNS0_8compiler21CodeAssemblerVariableEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1312(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$101, %edx
	leaq	-1152(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-88(%rbp), %rsi
	movq	%rbx, -88(%rbp)
	movl	$1, %edi
	pushq	%rdi
	movq	%rax, %r8
	movq	-1312(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	pushq	%rsi
	movq	-1136(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-1168(%rbp), %rdx
	movq	%rcx, -1168(%rbp)
	movl	$1, %ecx
	movq	%rax, -1160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1304(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1296(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1320(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1328(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15CombineFeedbackEPNS0_8compiler21CodeAssemblerVariableEi@PLT
	movq	(%r14), %rax
	movq	-1240(%rbp), %rdi
	movq	24(%rax), %rdx
	movq	%rdx, -1272(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1272(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	*%rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1248(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1248(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1272(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L204:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22678:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl7FloatOpEPNS0_8compiler4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl7FloatOpEPNS0_8compiler4NodeE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl7FloatOpEPNS0_8compiler4NodeE:
.LFB22690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13CodeAssembler10Float64NegENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22690:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl7FloatOpEPNS0_8compiler4NodeE, .-_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl7FloatOpEPNS0_8compiler4NodeE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_119LogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"LogicalNot"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119LogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119LogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119LogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22775:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$81, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-560(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$568, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1467, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC29(%rip), %rsi
	leaq	-576(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	-600(%rbp), %r10
	movq	-584(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -592(%rbp)
	movq	%r10, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-592(%rbp), %r9
	movq	%r13, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-584(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L210
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L210:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22775:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119LogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_119LogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_122ForInContinueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"ForInContinue"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_122ForInContinueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_122ForInContinueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_122ForInContinueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23597:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-92, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	leaq	-320(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-560(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3163, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC30(%rip), %rsi
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-568(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L214:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23597:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_122ForInContinueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_122ForInContinueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128ToBooleanLogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"ToBooleanLogicalNot"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128ToBooleanLogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128ToBooleanLogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128ToBooleanLogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22767:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$80, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-560(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$552, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1443, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC31(%rip), %rsi
	leaq	-576(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-584(%rbp), %r9
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22767:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128ToBooleanLogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128ToBooleanLogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertySloppyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"DeletePropertySloppy"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertySloppyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertySloppyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertySloppyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22799:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$84, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1520, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	movq	%r13, %r9
	movq	%rbx, -48(%rbp)
	movl	$3, %edi
	movq	%rax, %r8
	movq	-232(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	leaq	-224(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-240(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -224(%rbp)
	movq	-192(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L222:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22799:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertySloppyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertySloppyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_121CallPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"CallProperty"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_121CallPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_121CallPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_121CallPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22832:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$87, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1637, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC33(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-192(%rbp), %rcx
	movl	$1, %r8d
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L226:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22832:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_121CallPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_121CallPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE:
.LFB22817:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leal	(%rdx,%rsi), %ebx
	subq	$40, %rsp
	movl	%esi, -68(%rbp)
	xorl	%esi, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	leal	1(%rbx), %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_@PLT
	cmpl	$2, %ebx
	movl	-68(%rbp), %r10d
	je	.L228
	cmpl	$3, %ebx
	je	.L229
	cmpl	$1, %ebx
	je	.L230
	movq	%r12, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-40(%rbp), %rsp
	movl	%r15d, %r8d
	movq	%r14, %rdx
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJEEEvPNS0_8compiler4NodeES6_S6_NS0_19ConvertReceiverModeEDpT_@PLT
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%r10d, -64(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	-64(%rbp), %r10d
	movq	%r12, %rdi
	movq	%rax, %rbx
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-56(%rbp), %rdx
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movq	%r12, %rdi
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_@PLT
	popq	%rcx
	popq	%rsi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%r10d, -56(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	-56(%rbp), %r10d
	movq	%r12, %rdi
	movq	%rax, %rbx
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-40(%rbp), %rsp
	movq	%rbx, %r9
	movl	%r15d, %r8d
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeEEEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_@PLT
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$3, %esi
	movl	%r10d, -68(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	-68(%rbp), %r10d
	movq	%r12, %rdi
	movq	%rax, %rbx
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdx
	movq	%rbx, %r9
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	%r8
	movl	%r15d, %r8d
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchIJPNS0_8compiler4NodeES6_S6_EEEvS6_S6_S6_NS0_19ConvertReceiverModeEDpT_@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22817:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE, .-_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_123CallWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"CallWithSpread"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_123CallWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_123CallWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_123CallWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22936:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$96, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$144, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1744, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC34(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%rax, %rdx
	leaq	-176(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler27CallJSWithSpreadAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairES5_S5_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L236
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L236:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22936:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_123CallWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_123CallWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120CallRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"CallRuntime"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120CallRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120CallRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120CallRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22904:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$97, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1678, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-160(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	call	_ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$144, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L240:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22904:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120CallRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120CallRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_127CallRuntimeForPairAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"CallRuntimeForPair"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_127CallRuntimeForPairAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_127CallRuntimeForPairAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_127CallRuntimeForPairAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22920:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$98, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1707, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandRuntimeIdEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$2, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-176(%rbp), %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler12CallRuntimeNEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler10ProjectionEiPNS1_4NodeE@PLT
	movl	$3, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler31StoreRegisterPairAtOperandIndexEPNS0_8compiler4NodeES5_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$152, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L244:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22920:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_127CallRuntimeForPairAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_127CallRuntimeForPairAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_122CallJSRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"CallJSRuntime"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_122CallJSRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_122CallJSRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_122CallJSRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22928:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$99, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1724, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC37(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler33BytecodeOperandNativeContextIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-176(%rbp), %rcx
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$152, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L248:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22928:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_122CallJSRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_122CallJSRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_118ConstructAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"Construct"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_118ConstructAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_118ConstructAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_118ConstructAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22952:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$101, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1781, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r9
	movq	%r14, %rcx
	pushq	%r15
	movq	%rax, %rdx
	leaq	-192(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler9ConstructENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS3_4NodeES6_RKNS2_15RegListNodePairES8_S8_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L252:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22952:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_118ConstructAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_118ConstructAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128ConstructWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"ConstructWithSpread"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128ConstructWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128ConstructWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128ConstructWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22944:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$102, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1762, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC39(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r9
	movq	%r14, %rcx
	pushq	%r15
	movq	%rax, %rdx
	leaq	-192(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler19ConstructWithSpreadEPNS0_8compiler4NodeES5_S5_RKNS2_15RegListNodePairES5_S5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L256:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22944:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128ConstructWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128ConstructWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE:
.LFB22960:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	cmpl	$17, %r13d
	je	.L258
	leal	-18(%r13), %eax
	movq	-88(%rbp), %rcx
	cmpl	$3, %eax
	ja	.L259
	movq	%rcx, %r8
	movl	%r13d, %esi
	movq	%r14, %r9
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20RelationalComparisonENS0_9OperationENS0_8compiler11SloppyTNodeINS0_6ObjectEEES6_NS4_INS0_7ContextEEEPNS3_21CodeAssemblerVariableE@PLT
	movq	%rax, %r13
.L260:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11StrictEqualENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_21CodeAssemblerVariableE@PLT
	movq	%rax, %r13
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r14, %r8
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler5EqualENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_NS3_INS0_7ContextEEEPNS2_21CodeAssemblerVariableE@PLT
	movq	%rax, %r13
	jmp	.L260
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22960:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE, .-_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_127TestReferenceEqualAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"TestReferenceEqual"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_127TestReferenceEqualAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_127TestReferenceEqualAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_127TestReferenceEqualAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23015:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$109, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1882, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L267:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23015:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_127TestReferenceEqualAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_127TestReferenceEqualAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_117TestNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"TestNull"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_117TestNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_117TestNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_117TestNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23047:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$113, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1958, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC41(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler12NullConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L271:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23047:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_117TestNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_117TestNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_122TestUndefinedAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"TestUndefined"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_122TestUndefinedAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_122TestUndefinedAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_122TestUndefinedAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23055:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$114, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1969, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC42(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L275:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23055:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_122TestUndefinedAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_122TestUndefinedAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_123TestInstanceOfAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"TestInstanceOf"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_123TestInstanceOfAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_123TestInstanceOfAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_123TestInstanceOfAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23031:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$110, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-304(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1913, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC43(%rip), %rsi
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-312(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %r8
	call	_ZN2v88internal11interpreter20InterpreterAssembler23CollectCallableFeedbackEPNS0_8compiler4NodeES5_S5_S5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-320(%rbp), %r9
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler10InstanceOfEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L279
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L279:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23031:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_123TestInstanceOfAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_123TestInstanceOfAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_125TestUndetectableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC44:
	.string	"TestUndetectable"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_125TestUndetectableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_125TestUndetectableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_125TestUndetectableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23039:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$112, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-304(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-416(%rbp), %r12
	movq	%r12, %rdi
	subq	$384, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movq	%r13, %rdi
	movl	$1937, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC44(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17IsUndetectableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$384, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L283:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23039:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_125TestUndetectableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_125TestUndetectableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128CreateRegExpLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"CreateRegExpLiteral"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128CreateRegExpLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRegExpLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRegExpLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$121, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2444, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC45(%rip), %rsi
	leaq	-208(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-176(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %r8
	movq	-232(%rbp), %rcx
	call	_ZN2v88internal28ConstructorBuiltinsAssembler23EmitCreateRegExpLiteralEPNS0_8compiler4NodeES4_S4_S4_S4_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L287:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23263:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRegExpLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRegExpLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_120CloneObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"CloneObject"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_120CloneObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_120CloneObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_120CloneObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23314:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$127, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-192(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$264, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2612, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC46(%rip), %rsi
	leaq	-256(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$31, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$388, %edx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movl	$4, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-264(%rbp), %xmm0
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -240(%rbp)
	movq	-208(%rbp), %rax
	leaq	-240(%rbp), %rdx
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-280(%rbp), %xmm0
	movq	%rax, -232(%rbp)
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L291:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23314:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_120CloneObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_120CloneObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_132CreateEmptyArrayLiteralAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateEmptyArrayLiteralAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_132CreateEmptyArrayLiteralAssembler12GenerateImplEv:
.LFB23286:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	leaq	-352(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-360(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-336(%rbp), %r10
	movq	(%r12), %rsi
	movq	%r10, %rdi
	movq	%r10, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-376(%rbp), %r11
	movq	-360(%rbp), %r10
	movq	%rbx, %rcx
	movq	-368(%rbp), %r9
	movq	%r11, %rdx
	movq	%r10, %rdi
	movq	%r10, -368(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler27EmitCreateEmptyArrayLiteralEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	pushq	$0
	movq	-360(%rbp), %r8
	xorl	%esi, %esi
	pushq	$0
	movq	%rbx, %rdx
	movq	%rax, %rcx
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-368(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L295:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23286:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateEmptyArrayLiteralAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_132CreateEmptyArrayLiteralAssembler12GenerateImplEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126GetTemplateObjectAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126GetTemplateObjectAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126GetTemplateObjectAssembler12GenerateImplEv:
.LFB23326:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadFeedbackVectorSlotEPNS0_8compiler4NodeES4_iNS1_13ParameterModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE(%rip), %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r15, -64(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r15
	movq	%rax, %rdx
	movl	$147, %esi
	movq	-344(%rbp), %xmm0
	movl	$3, %r8d
	movhps	-352(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%rbx, %rdx
	xorl	%r9d, %r9d
	pushq	$1
	movq	-344(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler23StoreFeedbackVectorSlotEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-344(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L299:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23326:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126GetTemplateObjectAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_126GetTemplateObjectAssembler12GenerateImplEv
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_130CreateFunctionContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"CreateFunctionContext"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_130CreateFunctionContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_130CreateFunctionContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_130CreateFunctionContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23354:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-124, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2752, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC47(%rip), %rsi
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-176(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$2, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23354:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_130CreateFunctionContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_130CreateFunctionContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_126CreateEvalContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"CreateEvalContext"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126CreateEvalContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126CreateEvalContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126CreateEvalContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-123, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2766, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21LoadConstantPoolEntryEPNS0_8compiler4NodeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-176(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler26EmitFastNewFunctionContextEPNS0_8compiler4NodeES4_S4_NS0_9ScopeTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L307
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L307:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23362:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126CreateEvalContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_126CreateEvalContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_132CreateUnmappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"CreateUnmappedArguments"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_132CreateUnmappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateUnmappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_132CreateUnmappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23389:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-120, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2833, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC49(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewStrictArgumentsEPNS0_8compiler4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L311
	addq	$144, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L311:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23389:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateUnmappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_132CreateUnmappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128CreateRestParameterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"CreateRestParameter"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128CreateRestParameterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRestParameterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRestParameterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23397:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-119, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2846, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC50(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler24EmitFastNewRestParameterEPNS0_8compiler4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$144, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L315:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23397:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRestParameterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRestParameterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_117JumpLoopAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"JumpLoop"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_117JumpLoopAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_117JumpLoopAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_117JumpLoopAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23247:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-118, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$456, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2385, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC51(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandImmEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler19LoadOsrNestingLevelEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-488(%rbp), %r9
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler23Int32GreaterThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-464(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory29InterpreterOnStackReplacementEPNS0_7IsolateE@PLT
	movq	-464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	pushq	$0
	leaq	-480(%rbp), %rdx
	xorl	%esi, %esi
	pushq	$0
	movq	%rax, %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rbx, %r8
	movq	%rax, -480(%rbp)
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-448(%rbp), %rax
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12JumpBackwardEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L319:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23247:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_117JumpLoopAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_117JumpLoopAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128JumpIfToBooleanTrueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC52:
	.string	"JumpIfToBooleanTrue"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128JumpIfToBooleanTrueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128JumpIfToBooleanTrueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128JumpIfToBooleanTrueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-105, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2165, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC52(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L323:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23119:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128JumpIfToBooleanTrueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128JumpIfToBooleanTrueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_129JumpIfToBooleanFalseAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC53:
	.string	"JumpIfToBooleanFalse"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_129JumpIfToBooleanFalseAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_129JumpIfToBooleanFalseAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_129JumpIfToBooleanFalseAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23135:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-104, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2196, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC53(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L327
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L327:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23135:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_129JumpIfToBooleanFalseAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_129JumpIfToBooleanFalseAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_130JumpIfUndefinedOrNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC54:
	.string	"JumpIfUndefinedOrNull"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_130JumpIfUndefinedOrNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_130JumpIfUndefinedOrNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_130JumpIfUndefinedOrNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23215:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-97, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-288(%rbp), %r12
	movq	%r12, %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2308, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC54(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$264, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L331:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23215:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_130JumpIfUndefinedOrNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_130JumpIfUndefinedOrNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_136JumpIfToBooleanTrueConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC55:
	.string	"JumpIfToBooleanTrueConstant"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_136JumpIfToBooleanTrueConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_136JumpIfToBooleanTrueConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_136JumpIfToBooleanTrueConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-107, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2181, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC55(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L335:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23127:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_136JumpIfToBooleanTrueConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_136JumpIfToBooleanTrueConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_137JumpIfToBooleanFalseConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC56:
	.string	"JumpIfToBooleanFalseConstant"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_137JumpIfToBooleanFalseConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_137JumpIfToBooleanFalseConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_137JumpIfToBooleanFalseConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23143:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-106, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2212, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC56(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23BranchIfToBooleanIsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L339:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23143:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_137JumpIfToBooleanFalseConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_137JumpIfToBooleanFalseConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_138JumpIfUndefinedOrNullConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC57:
	.string	"JumpIfUndefinedOrNullConstant"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_138JumpIfUndefinedOrNullConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_138JumpIfUndefinedOrNullConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_138JumpIfUndefinedOrNullConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23223:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-111, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-288(%rbp), %r12
	movq	%r12, %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2326, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC57(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	addq	$264, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L343:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23223:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_138JumpIfUndefinedOrNullConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_138JumpIfUndefinedOrNullConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_133JumpIfJSReceiverConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC58:
	.string	"JumpIfJSReceiverConstant"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_133JumpIfJSReceiverConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_133JumpIfJSReceiverConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_133JumpIfJSReceiverConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23239:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-108, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-560(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2363, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC58(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-568(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L347:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23239:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_133JumpIfJSReceiverConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_133JumpIfJSReceiverConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_125JumpIfJSReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC59:
	.string	"JumpIfJSReceiver"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_125JumpIfJSReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_125JumpIfJSReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_125JumpIfJSReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23231:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-96, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-560(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2343, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC59(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-568(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L351:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23231:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_125JumpIfJSReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_125JumpIfJSReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_118ForInStepAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC60:
	.string	"ForInStep"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_118ForInStepAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_118ForInStepAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_118ForInStepAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23605:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-90, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3188, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC60(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L355
	addq	$136, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L355:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23605:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_118ForInStepAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_118ForInStepAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_130SwitchOnSmiNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC61:
	.string	"SwitchOnSmiNoFeedback"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_130SwitchOnSmiNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_130SwitchOnSmiNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_130SwitchOnSmiNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23255:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-95, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-304(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2416, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC61(%rip), %rsi
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler24BytecodeOperandImmIntPtrEi@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-312(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L359
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L359:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23255:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_130SwitchOnSmiNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_130SwitchOnSmiNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_131SwitchOnGeneratorStateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC62:
	.string	"SwitchOnGeneratorState"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_131SwitchOnGeneratorStateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_131SwitchOnGeneratorStateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_131SwitchOnGeneratorStateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23653:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-81, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-288(%rbp), %r12
	movq	%r12, %rdi
	subq	$256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3282, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC62(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$-2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler29LoadAndUntagConstantPoolEntryEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L363
	addq	$256, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L363:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23653:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_131SwitchOnGeneratorStateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_131SwitchOnGeneratorStateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_123ForInEnumerateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC63:
	.string	"ForInEnumerate"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_123ForInEnumerateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_123ForInEnumerateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_123ForInEnumerateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23565:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-94, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3027, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC63(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14CheckEnumCacheEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelES6_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$91, %esi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L367:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23565:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_123ForInEnumerateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_123ForInEnumerateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_121ForInPrepareAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_121ForInPrepareAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_121ForInPrepareAssembler12GenerateImplEv:
.LFB23577:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler5IsMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadMapEnumLengthENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadMapDescriptorsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_9EnumCacheEvE5valueE(%rip), %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE(%rip), %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -328(%rbp)
	movl	%ecx, -336(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	-336(%rbp), %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	-328(%rbp), %rsi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler32LoadAndUntagFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	movq	-352(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	movq	%r8, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movabsq	$12884901888, %rcx
	movabsq	$4294967296, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17SelectSmiConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEENS0_3SmiES6_@PLT
	movq	-360(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rcx
	movq	%r9, -328(%rbp)
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	-336(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-344(%rbp), %r10
	movq	%rax, %rcx
	movq	%r10, %rdx
	call	_ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-328(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24LoadFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler33StoreRegisterTripleAtOperandIndexEPNS0_8compiler4NodeES5_S5_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L371
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L371:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23577:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_121ForInPrepareAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_121ForInPrepareAssembler12GenerateImplEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_118ForInNextAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_118ForInNextAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_118ForInNextAssembler12GenerateImplEv:
.LFB23585:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler30LoadRegisterPairAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, %r13
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movl	$2, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %r13
	leaq	-208(%rbp), %r14
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-408(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-400(%rbp), %r10
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$492, %edx
	leaq	-368(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%esi, %esi
	movl	$2, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	movq	%r12, %rdi
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-392(%rbp), %xmm0
	leaq	-384(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -384(%rbp)
	movq	-352(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L375:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23585:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_118ForInNextAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_118ForInNextAssembler12GenerateImplEv
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_126SetPendingMessageAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC64:
	.string	"SetPendingMessage"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_126SetPendingMessageAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_126SetPendingMessageAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_126SetPendingMessageAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23413:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-88, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2868, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC64(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference30address_of_pending_message_objEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$2, %ecx
	movl	$1799, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler29StoreFullTaggedNoWriteBarrierEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L379
	addq	$136, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L379:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23413:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_126SetPendingMessageAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_126SetPendingMessageAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_114ThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC65:
	.string	"Throw"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_114ThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_114ThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_114ThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23421:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-87, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2881, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC65(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-32(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, -32(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$160, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L383
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L383:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23421:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_114ThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_114ThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_116ReThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC66:
	.string	"ReThrow"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_116ReThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_116ReThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_116ReThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23429:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-86, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2893, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC66(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-32(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, -32(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$156, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L387:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23429:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_116ReThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_116ReThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_134ThrowReferenceErrorIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC67:
	.string	"ThrowReferenceErrorIfHole"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_134ThrowReferenceErrorIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowReferenceErrorIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowReferenceErrorIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23453:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-84, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-288(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$256, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2923, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC67(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-48(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$173, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L391
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L391:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23453:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowReferenceErrorIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowReferenceErrorIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_134ThrowSuperNotCalledIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC68:
	.string	"ThrowSuperNotCalledIfHole"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_134ThrowSuperNotCalledIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowSuperNotCalledIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowSuperNotCalledIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23461:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-83, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-288(%rbp), %r12
	movq	%r12, %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2944, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC68(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$44, %esi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$264, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L395:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23461:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowSuperNotCalledIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowSuperNotCalledIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_141ThrowSuperAlreadyCalledIfNotHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.8,"aMS",@progbits,1
	.align 8
.LC69:
	.string	"ThrowSuperAlreadyCalledIfNotHole"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_141ThrowSuperAlreadyCalledIfNotHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_141ThrowSuperAlreadyCalledIfNotHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_141ThrowSuperAlreadyCalledIfNotHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23469:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-82, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-288(%rbp), %r12
	movq	%r12, %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2964, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC69(%rip), %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$43, %esi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$264, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L399:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23469:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_141ThrowSuperAlreadyCalledIfNotHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_141ThrowSuperAlreadyCalledIfNotHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_125SuspendGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC70:
	.string	"SuspendGenerator"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_125SuspendGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_125SuspendGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_125SuspendGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23645:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-80, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3245, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC70(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1800, %ecx
	movl	$72, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler22BytecodeOperandUImmSmiEi@PLT
	movq	-200(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$771, %ecx
	movl	$42, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	leaq	-192(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal11interpreter20InterpreterAssembler31ExportParametersAndRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE@PLT
	movq	%r15, %rcx
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r14, %rcx
	movl	$64, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14BytecodeOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movl	$48, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L403:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23645:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_125SuspendGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_125SuspendGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_124ResumeGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC71:
	.string	"ResumeGenerator"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_124ResumeGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_124ResumeGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_124ResumeGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23661:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-79, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3326, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC71(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movq	%r14, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movq	%rdx, -168(%rbp)
	movl	$24, %edx
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$771, %ecx
	movl	$42, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$72, %edx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	leaq	-176(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18ImportRegisterFileENS0_8compiler5TNodeINS0_10FixedArrayEEERKNS2_15RegListNodePairENS4_INS0_6Int32TEEE@PLT
	movl	$1800, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$48, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L407
	addq	$152, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L407:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23661:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_124ResumeGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_124ResumeGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_114AbortAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC72:
	.string	"Abort"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_114AbortAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_114AbortAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_114AbortAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23437:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-75, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2905, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC72(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	leaq	-32(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, -32(%rbp)
	movq	%rax, %rdx
	movl	$1, %r8d
	movl	$348, %esi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	subq	$-128, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L411:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23437:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_114AbortAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_114AbortAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB29568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE29568:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB27830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$2, %edx
	je	.L415
	cmpl	$3, %edx
	je	.L416
	cmpl	$1, %edx
	je	.L430
.L417:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movq	(%rsi), %r13
	movl	$32, %edi
	call	_Znwm@PLT
	movq	$0, 16(%rax)
	movq	%rax, %r12
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L418
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 16(%r12)
.L418:
	movq	%r12, (%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L417
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L419
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L419:
	movq	%r12, %rdi
	movl	$32, %esi
	call	_ZdlPvm@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27830:
	.size	_ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_138InterpreterStoreNamedPropertyAssembler16StaNamedPropertyENS0_8CallableENS0_17NamedPropertyTypeE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_138InterpreterStoreNamedPropertyAssembler16StaNamedPropertyENS0_8CallableENS0_17NamedPropertyTypeE.isra.0, @function
_ZN2v88internal11interpreter12_GLOBAL__N_138InterpreterStoreNamedPropertyAssembler16StaNamedPropertyENS0_8CallableENS0_17NamedPropertyTypeE.isra.0:
.LFB30551:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	16(%rbx), %rax
	movq	%r15, %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movq	-160(%rbp), %r10
	movq	%rdx, -112(%rbp)
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	movl	$5, %edx
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rcx
	pushq	%rdx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	-168(%rbp), %r11
	pushq	%rax
	movq	%r10, -80(%rbp)
	leaq	-112(%rbp), %r10
	movq	%r10, %rdx
	movq	%r8, -72(%rbp)
	movq	%r14, %r8
	movq	%rcx, -64(%rbp)
	movl	$1, %ecx
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L434
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L434:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30551:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_138InterpreterStoreNamedPropertyAssembler16StaNamedPropertyENS0_8CallableENS0_17NamedPropertyTypeE.isra.0, .-_ZN2v88internal11interpreter12_GLOBAL__N_138InterpreterStoreNamedPropertyAssembler16StaNamedPropertyENS0_8CallableENS0_17NamedPropertyTypeE.isra.0
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0:
.LFB30560:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	-88(%rbp), %r9
	pushq	$1
	movq	-96(%rbp), %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-104(%rbp), %rcx
	call	*%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L438:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30560:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0, .-_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0:
.LFB30559:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	-88(%rbp), %r9
	pushq	$0
	movq	-96(%rbp), %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-104(%rbp), %rcx
	call	*%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L442
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L442:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30559:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0, .-_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS0_10TypeofModeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS0_10TypeofModeE.constprop.0, @function
_ZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS0_10TypeofModeE.constprop.0:
.LFB30558:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movl	%esi, -196(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movdqa	-192(%rbp), %xmm0
	movq	%rbx, -128(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rbx, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	pushq	$1
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	movq	%r12, %r8
	movq	%r14, %rsi
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, -120(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rdx
	movq	%rax, %xmm3
	leaq	-96(%rbp), %rax
	movq	%rbx, -160(%rbp)
	movl	-196(%rbp), %r9d
	pushq	%rax
	leaq	-160(%rbp), %rbx
	movq	%r13, %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rdx, %xmm0
	movq	%rbx, %rcx
	movq	%r15, %rdx
	punpcklqdq	%xmm3, %xmm0
	movl	$0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal17AccessorAssembler12LoadGlobalICEPNS0_8compiler4NodeES4_RKSt8functionIFNS2_5TNodeINS0_7ContextEEEvEERKS5_IFNS6_INS0_4NameEEEvEENS0_10TypeofModeEPNS0_9ExitPointENS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-112(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L444
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L444:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L445
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L445:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L446
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L446:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L458
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L458:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30558:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS0_10TypeofModeE.constprop.0, .-_ZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS0_10TypeofModeE.constprop.0
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEv:
.LFB22327:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-496(%rbp), %r15
	leaq	-528(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	movq	%rax, %r13
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, -368(%rbp)
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEvEUlvE0_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm2
	movq	%r12, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	movq	%rcx, %xmm0
	xorl	%ecx, %ecx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$24, %edi
	movq	$0, -144(%rbp)
	call	_Znwm@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	movl	$24, %edi
	movq	%r12, -208(%rbp)
	movq	%rax, %rbx
	movq	%r14, (%rax)
	movq	%rcx, %xmm0
	movq	%r12, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%rax, -160(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_9ExitPointC4EPNS1_17CodeStubAssemblerEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	$0, -184(%rbp)
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_Znwm@PLT
	movdqu	(%rbx), %xmm4
	movq	16(%rbx), %rdx
	leaq	-160(%rbp), %rbx
	movq	%rax, -200(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	%rdx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	-144(%rbp), %rax
	movups	%xmm5, -184(%rbp)
	testq	%rax, %rax
	je	.L460
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L460:
	movq	-352(%rbp), %rax
	movq	$0, -256(%rbp)
	testq	%rax, %rax
	je	.L512
	leaq	-368(%rbp), %rsi
	leaq	-272(%rbp), %rdi
	movl	$2, %edx
	call	*%rax
	movdqa	-352(%rbp), %xmm6
	cmpq	$0, -352(%rbp)
	movq	$0, -224(%rbp)
	movaps	%xmm6, -256(%rbp)
	je	.L462
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-256(%rbp), %rdx
	movdqa	-272(%rbp), %xmm0
	movq	$0, -256(%rbp)
	movq	24(%rax), %rcx
	movdqu	(%rax), %xmm1
	movq	%rax, -240(%rbp)
	movq	%rdx, 16(%rax)
	movq	-248(%rbp), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, -248(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerISt8functionIFN2v88internal8compiler5TNodeINS3_4NameEEEvEEE10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	movq	%rdx, 24(%rax)
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_6ObjectEEEvESt8functionIFNS3_INS1_4NameEEEvEEE9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	movaps	%xmm1, -272(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -224(%rbp)
.L462:
	movq	-320(%rbp), %rax
	movq	$0, -288(%rbp)
	testq	%rax, %rax
	je	.L513
	leaq	-304(%rbp), %r8
	leaq	-336(%rbp), %rsi
	movl	$2, %edx
	movq	%r8, -552(%rbp)
	movq	%r8, %rdi
	call	*%rax
	movq	-320(%rbp), %rax
	movq	-552(%rbp), %r8
	movq	$0, -144(%rbp)
	movdqa	-320(%rbp), %xmm7
	testq	%rax, %rax
	movaps	%xmm7, -288(%rbp)
	je	.L511
	movq	%r8, -552(%rbp)
	movq	%r8, %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	*%rax
	movq	-224(%rbp), %rax
	movq	%r13, -128(%rbp)
	movdqa	-288(%rbp), %xmm6
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	-288(%rbp), %rcx
	movq	-552(%rbp), %r8
	movaps	%xmm6, -144(%rbp)
	je	.L467
.L464:
	leaq	-240(%rbp), %rsi
	leaq	-120(%rbp), %rdi
	movl	$2, %edx
	call	*%rax
	movdqa	-224(%rbp), %xmm7
	movq	-288(%rbp), %rcx
	movq	%r13, -72(%rbp)
	movq	-544(%rbp), %xmm0
	movq	-224(%rbp), %rax
	movups	%xmm7, -104(%rbp)
	movhps	-536(%rbp), %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rcx, %rcx
	je	.L468
	leaq	-304(%rbp), %r8
.L476:
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rcx
	movq	-224(%rbp), %rax
.L468:
	testq	%rax, %rax
	je	.L469
	leaq	-240(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L469:
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L470
	leaq	-272(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L470:
	leaq	-512(%rbp), %r13
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-208(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17AccessorAssembler22LoadIC_BytecodeHandlerEPKNS1_20LazyLoadICParametersEPNS0_9ExitPointE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L471
	leaq	-120(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L471:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L472
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L472:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L473
	leaq	-200(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L473:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-320(%rbp), %rax
	testq	%rax, %rax
	je	.L474
	leaq	-336(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L474:
	movq	-352(%rbp), %rax
	testq	%rax, %rax
	je	.L459
	leaq	-368(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L459:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L514
	addq	$520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	movq	$0, -144(%rbp)
.L511:
	movq	-224(%rbp), %rax
	movq	%r13, -128(%rbp)
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	jne	.L464
	movq	-544(%rbp), %xmm0
	movq	%r13, -72(%rbp)
	movhps	-536(%rbp), %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L512:
	movq	$0, -224(%rbp)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L467:
	movq	-544(%rbp), %xmm0
	movq	%r13, -72(%rbp)
	movhps	-536(%rbp), %xmm0
	movups	%xmm0, -88(%rbp)
	testq	%rcx, %rcx
	jne	.L476
	jmp	.L469
.L514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22327:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEv
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB27066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L516
	cmpl	$3, %edx
	je	.L517
	cmpl	$1, %edx
	je	.L523
.L518:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L518
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27066:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal9ExitPointC4EPNS2_17CodeStubAssemblerEPNS2_8compiler18CodeAssemblerLabelEPNS6_21CodeAssemblerVariableEEUlPNS6_4NodeEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_125LdaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC73:
	.string	"LdaKeyedProperty"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_125LdaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_125LdaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_125LdaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22344:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$42, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-192(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$264, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$555, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC73(%rip), %rsi
	leaq	-256(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$373, %edx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movl	$4, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-264(%rbp), %xmm0
	pushq	%rsi
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -240(%rbp)
	movq	-208(%rbp), %rax
	leaq	-240(%rbp), %rdx
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-280(%rbp), %xmm0
	movq	%rax, -232(%rbp)
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L527
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L527:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22344:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_125LdaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_125LdaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_125StaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC74:
	.string	"StaKeyedProperty"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_125StaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_125StaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_125StaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22392:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$48, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-192(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$256, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$641, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC74(%rip), %rsi
	leaq	-256(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$381, %edx
	leaq	-224(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	movq	%rbx, %r9
	movq	%r14, -48(%rbp)
	movl	$5, %edi
	movq	%rax, %r8
	movq	-264(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	leaq	-240(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movhps	-272(%rbp), %xmm0
	pushq	%rsi
	xorl	%esi, %esi
	movaps	%xmm0, -80(%rbp)
	movq	-280(%rbp), %xmm0
	movq	%rax, -240(%rbp)
	movq	-208(%rbp), %rax
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L531:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22392:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_125StaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_125StaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_117DebuggerAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC75:
	.string	"Debugger"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_117DebuggerAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_117DebuggerAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_117DebuggerAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23477:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-77, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	movq	%r12, %rdi
	subq	$176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2983, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC75(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-176(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	pushq	$0
	leaq	-192(%rbp), %rdx
	xorl	%esi, %esi
	pushq	$0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r13, %r9
	movq	%rax, -192(%rbp)
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-160(%rbp), %rax
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L535
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L535:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23477:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_117DebuggerAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_117DebuggerAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertyStrictAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC76:
	.string	"DeletePropertyStrict"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertyStrictAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertyStrictAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertyStrictAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22791:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$83, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1505, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC76(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movabsq	$4294967296, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantENS0_3SmiE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	movq	%r13, %r9
	movq	%rbx, -48(%rbp)
	movl	$3, %edi
	movq	%rax, %r8
	movq	-232(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	leaq	-224(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-240(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -224(%rbp)
	movq	-192(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L539:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22791:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertyStrictAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertyStrictAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_124IncBlockCounterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC77:
	.string	"IncBlockCounter"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_124IncBlockCounterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_124IncBlockCounterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_124IncBlockCounterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23557:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-76, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3011, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC77(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandIdxSmiEi@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$818, %edx
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	movl	$2, %edi
	movq	%r13, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	leaq	-208(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-224(%rbp), %xmm0
	movq	%rax, -208(%rbp)
	movq	-176(%rbp), %rax
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L543
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L543:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23557:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_124IncBlockCounterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_124IncBlockCounterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB30554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30554:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB29569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29569:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB30555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE30555:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_132CreateArrayFromIterableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC78:
	.string	"CreateArrayFromIterable"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_132CreateArrayFromIterableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateArrayFromIterableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_132CreateArrayFromIterableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23290:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$123, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-160(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2539, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC78(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$393, %edx
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r9
	movq	%rbx, -48(%rbp)
	movl	$1, %edi
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -208(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L553
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L553:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23290:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateArrayFromIterableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_132CreateArrayFromIterableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_115ToNameAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC79:
	.string	"ToName"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_115ToNameAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_115ToNameAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_115ToNameAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22704:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$116, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-160(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1332, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC79(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$99, %edx
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	movq	%r13, %r9
	movq	%rbx, -48(%rbp)
	movl	$1, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rdi
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -208(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L557
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L557:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22704:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_115ToNameAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_115ToNameAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_:
.LFB22686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	movq	%r14, %rdi
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$472, %rsp
	movq	%rdx, -472(%rbp)
	movl	$8, %edx
	movq	%rcx, -512(%rbp)
	movq	%r8, -504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r9, -488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-480(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$-2147483648, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-480(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-488(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	movq	%r9, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-472(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15CombineFeedbackEPNS0_8compiler21CodeAssemblerVariableEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-488(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -480(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-480(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-472(%rbp), %rsi
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15CombineFeedbackEPNS0_8compiler21CodeAssemblerVariableEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17MinusZeroConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-496(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -472(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-504(%rbp), %r11
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-512(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-472(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	addq	$472, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L561:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22686:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_, .-_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_130CallUndefinedReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC80:
	.string	"CallUndefinedReceiver"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_130CallUndefinedReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_130CallUndefinedReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_130CallUndefinedReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22864:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$91, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1653, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC80(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	-192(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L565
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L565:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22864:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_130CallUndefinedReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_130CallUndefinedReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_124CallAnyReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC81:
	.string	"CallAnyReceiver"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_124CallAnyReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_124CallAnyReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_124CallAnyReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22824:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$86, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-176(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1633, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC81(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rdx, -184(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler19CollectCallFeedbackEPNS0_8compiler4NodeES5_S5_S5_@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-192(%rbp), %rcx
	movl	$2, %r8d
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L569
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L569:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22824:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_124CallAnyReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_124CallAnyReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_127CreateArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC82:
	.string	"CreateArrayLiteral"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_127CreateArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_127CreateArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23274:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$122, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-464(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2464, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC82(%rip), %rsi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-504(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-496(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-480(%rbp), %r11
	movq	-464(%rbp), %rsi
	movq	%r11, %rdi
	movq	%r11, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-496(%rbp), %r11
	movq	-488(%rbp), %rcx
	movl	$1, %r9d
	movq	%r11, %rdi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler29EmitCreateShallowArrayLiteralEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelENS0_18AllocationSiteModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-496(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-504(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$31, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-504(%rbp), %rdx
	movl	$4, %r8d
	movq	-496(%rbp), %rcx
	movl	$186, %esi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-488(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L573
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L573:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23274:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_127CreateArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_130CreateMappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC83:
	.string	"CreateMappedArguments"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_130CreateMappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_130CreateMappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_130CreateMappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23378:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$-121, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2793, %ecx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC83(%rip), %rsi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$24, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$772, %ecx
	movl	$48, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$2048, %esi
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-464(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-456(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-448(%rbp), %r8
	movq	-432(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -456(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-456(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS0_8compiler4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-456(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$312, %esi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L577
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L577:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23378:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_130CreateMappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_130CreateMappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128CreateObjectLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC84:
	.string	"CreateObjectLiteral"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128CreateObjectLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateObjectLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128CreateObjectLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB23298:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$125, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-464(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2552, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC84(%rip), %rsi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-496(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-488(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-480(%rbp), %r8
	movq	-464(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -488(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-488(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler30EmitCreateShallowObjectLiteralEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-488(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-512(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%edx, %edx
	movl	$31, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-496(%rbp), %r10
	leaq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	-504(%rbp), %r8
	movq	-488(%rbp), %rdx
	movl	$188, %esi
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	$4, %r8d
	movq	%rdx, -72(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L581
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L581:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23298:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateObjectLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128CreateObjectLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE:
.LFB22567:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	leaq	-320(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-192(%rbp), %rbx
	subq	$392, %rsp
	movl	%esi, -428(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-336(%rbp), %r11
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -416(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	%r13
	movq	%r14, %r8
	movq	%rbx, %r9
	movq	-416(%rbp), %r11
	movq	-408(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-392(%rbp), %rsi
	pushq	%r11
	movq	%r11, -424(%rbp)
	call	_ZN2v88internal17CodeStubAssembler34TaggedToWord32OrBigIntWithFeedbackEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S8_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-428(%rbp), %r10d
	movq	-400(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%r10d, %ecx
	call	_ZN2v88internal17CodeStubAssembler9BitwiseOpEPNS0_8compiler4NodeES4_NS0_9OperationE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movabsq	$30064771072, %rcx
	movabsq	$4294967296, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17SelectSmiConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEENS0_3SmiES6_@PLT
	movq	%r13, %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-408(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-400(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	-416(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$21, %edx
	movq	-392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-424(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L585
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L585:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22567:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE, .-_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119BitwiseNotAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119BitwiseNotAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119BitwiseNotAssembler12GenerateImplEv:
.LFB22650:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	leaq	-352(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-384(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-336(%rbp), %rbx
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-208(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	%r14
	movq	%r13, %r8
	movq	%rbx, %rcx
	pushq	%r15
	movq	-416(%rbp), %r11
	movq	%r12, %rdi
	movq	-408(%rbp), %r9
	movq	-400(%rbp), %rsi
	movq	%r11, %rdx
	movq	%r9, -432(%rbp)
	call	_ZN2v88internal17CodeStubAssembler34TaggedToWord32OrBigIntWithFeedbackEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S8_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16Word32BitwiseNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19ChangeInt32ToTaggedENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movabsq	$30064771072, %rcx
	movabsq	$4294967296, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17SelectSmiConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEENS0_3SmiES6_@PLT
	movq	%r14, %rdi
	movq	%rax, -416(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-416(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, -408(%rbp)
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-408(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-440(%rbp), %r10
	movq	-392(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	movq	%r10, -416(%rbp)
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	-424(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-432(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -408(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-416(%rbp), %r10
	movq	-392(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, -392(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-400(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %xmm0
	movl	$2, %r8d
	movl	$32, %esi
	movhps	-392(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-408(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L589:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22650:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119BitwiseNotAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_119BitwiseNotAssembler12GenerateImplEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_122StaLookupSlotAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_122StaLookupSlotAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_122StaLookupSlotAssembler12GenerateImplEv:
.LFB22319:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-720(%rbp), %r15
	leaq	-464(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-736(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-592(%rbp), %rbx
	subq	$776, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	movq	%r12, %rdi
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-768(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-752(%rbp), %rcx
	leaq	-80(%rbp), %r11
	movq	-744(%rbp), %rdx
	movl	$2, %r8d
	movl	$320, %esi
	movq	%r12, %rdi
	movq	%r11, -808(%rbp)
	movq	%rcx, -80(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rcx, -72(%rbp)
	movq	%r11, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-336(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	leaq	-208(%rbp), %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-768(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-792(%rbp), %r10
	movq	-800(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	movq	%r9, %rdx
	movq	%r9, -760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-760(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -768(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-808(%rbp), %r11
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	-752(%rbp), %xmm0
	movq	-744(%rbp), %rdx
	movl	$319, %esi
	movq	%r11, %rcx
	movq	%r11, -760(%rbp)
	movhps	-784(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-792(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -752(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-760(%rbp), %r11
	movl	$318, %esi
	movdqa	-784(%rbp), %xmm0
	movq	-744(%rbp), %rdx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%r11, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-752(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-768(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L593
	addq	$776, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L593:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22319:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_122StaLookupSlotAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_122StaLookupSlotAssembler12GenerateImplEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_122CreateClosureAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_122CreateClosureAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_122CreateClosureAssembler12GenerateImplEv:
.LFB23334:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-592(%rbp), %rbx
	subq	$744, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	leaq	-720(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	%r11, -768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	call	_ZN2v88internal11interpreter8Register16function_closureEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12LoadRegisterENS1_8RegisterE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler24LoadClosureFeedbackArrayENS0_8compiler11SloppyTNodeINS0_10JSFunctionEEE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	pushq	$0
	movl	$2, %r9d
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-464(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r10, -760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -752(%rbp)
	movq	%rax, %rdx
	leaq	-208(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-336(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-760(%rbp), %r10
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$32, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r15, %r9
	movq	%r13, %rdx
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%rax, -336(%rbp)
	movq	-192(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-728(%rbp), %xmm0
	movq	%rax, -328(%rbp)
	leaq	-80(%rbp), %rax
	pushq	%rax
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -784(%rbp)
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-760(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-752(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-736(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	movl	$306, %esi
	movq	%r12, %rdi
	movdqa	-784(%rbp), %xmm0
	movq	-728(%rbp), %rcx
	movl	$2, %r8d
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	movl	$307, %esi
	movq	%r12, %rdi
	movdqa	-752(%rbp), %xmm0
	movq	-728(%rbp), %rcx
	movl	$2, %r8d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-760(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-768(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L597
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L597:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23334:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_122CreateClosureAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_122CreateClosureAssembler12GenerateImplEv
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE:
.LFB22563:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-624(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-672(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-688(%rbp), %rbx
	subq	$776, %rsp
	movl	%esi, -764(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movq	%rbx, %rdi
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, -760(%rbp)
	movq	%rbx, -696(%rbp)
	leaq	-608(%rbp), %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-656(%rbp), %r11
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -776(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-640(%rbp), %r10
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -784(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r15, %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	movq	-744(%rbp), %rcx
	movq	%r15, -704(%rbp)
	leaq	-208(%rbp), %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-592(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -712(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-464(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-336(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -728(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	-696(%rbp)
	movq	%r14, %rsi
	movq	-776(%rbp), %r11
	movq	-728(%rbp), %r9
	pushq	-704(%rbp)
	movq	%r12, %rdi
	movq	-712(%rbp), %rcx
	movq	%r11, %r8
	movq	-744(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler34TaggedToWord32OrBigIntWithFeedbackEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S8_@PLT
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	pushq	%r13
	movq	%r15, %r9
	movq	%r14, %rsi
	movq	-784(%rbp), %r10
	pushq	%rbx
	movq	%r12, %rdi
	movq	-720(%rbp), %rcx
	movq	-752(%rbp), %rdx
	movq	%r10, %r8
	movq	%r10, -744(%rbp)
	call	_ZN2v88internal17CodeStubAssembler34TaggedToWord32OrBigIntWithFeedbackEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S8_@PLT
	movq	-720(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-744(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -808(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-776(%rbp), %r11
	movq	%rax, -744(%rbp)
	movq	%r11, %rdi
	movq	%r11, -800(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-764(%rbp), %ecx
	movq	-744(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9BitwiseOpEPNS0_8compiler4NodeES4_NS0_9OperationE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -792(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movabsq	$4294967296, %rdx
	movabsq	$30064771072, %rcx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17SelectSmiConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEENS0_3SmiES6_@PLT
	movq	%r13, %rdi
	movq	%rax, -784(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-696(%rbp), %rdi
	movq	%rax, -744(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-744(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-776(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, -744(%rbp)
	movq	%rcx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-744(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-784(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, -744(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-744(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-736(%rbp), %rcx
	movq	-760(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	-792(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %r9
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-752(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27TaggedToNumericWithFeedbackEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES8_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	-764(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -752(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, -744(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-752(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %xmm0
	movl	$3, %r8d
	movl	$23, %esi
	movhps	-744(%rbp), %xmm0
	movq	%rdx, -64(%rbp)
	movq	%r14, %rdx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-696(%rbp), %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-744(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6WordOrENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	-736(%rbp), %rcx
	movq	-760(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-704(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-808(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-800(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-696(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L601
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L601:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22563:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE, .-_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_119TestTypeOfAssembler12GenerateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_119TestTypeOfAssembler12GenerateImplEv, @function
_ZN2v88internal11interpreter12_GLOBAL__N_119TestTypeOfAssembler12GenerateImplEv:
.LFB23067:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-848(%rbp), %r11
	leaq	-1104(%rbp), %rdx
	pushq	%r13
	leaq	-1232(%rbp), %rcx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-1616(%rbp), %r15
	pushq	%r12
	leaq	-1488(%rbp), %r13
	movq	%r15, %xmm4
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rcx, %xmm1
	movq	%r13, %xmm2
	.cfi_offset 3, -56
	leaq	-1744(%rbp), %rbx
	leaq	-976(%rbp), %rsi
	movq	%rbx, %xmm3
	movq	%rsi, %xmm0
	punpcklqdq	%xmm4, %xmm3
	subq	$1848, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1360(%rbp), %rax
	movq	%r11, -1760(%rbp)
	movq	%rdx, -1752(%rbp)
	movq	%rax, -1768(%rbp)
	movhps	-1760(%rbp), %xmm0
	movaps	%xmm3, -1888(%rbp)
	movhps	-1752(%rbp), %xmm1
	movaps	%xmm0, -1840(%rbp)
	movhps	-1768(%rbp), %xmm2
	movaps	%xmm1, -1856(%rbp)
	movaps	%xmm2, -1872(%rbp)
	movq	%rcx, -1776(%rbp)
	movq	%r15, -1800(%rbp)
	movq	%r13, -1792(%rbp)
	movq	%rsi, -1784(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandFlagEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1824(%rbp)
	movq	%rbx, -1808(%rbp)
	leaq	-720(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-592(%rbp), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-464(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1768(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1776(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1752(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1784(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1760(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rbx, -1816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movdqa	-1840(%rbp), %xmm0
	xorl	%ecx, %ecx
	movdqa	-1888(%rbp), %xmm3
	movdqa	-1872(%rbp), %xmm2
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	movdqa	.LC85(%rip), %xmm0
	movl	$1, %r8d
	movdqa	-1856(%rbp), %xmm1
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -176(%rbp)
	movdqa	.LC86(%rip), %xmm0
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, -64(%rbp)
	leaq	-336(%rbp), %rbx
	movl	$8, -144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-64(%rbp), %rdx
	leaq	-128(%rbp), %r8
	movq	%r12, %rdi
	movq	-1824(%rbp), %r10
	leaq	-176(%rbp), %rcx
	movl	$8, %r9d
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	-1808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L621
.L603:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12GotoIfNumberEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L622
.L605:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L623
.L607:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsSymbolENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L624
.L609:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L625
.L611:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsBigIntENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L626
.L613:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17IsUndetectableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1784(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L627
.L615:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadMapBitFieldENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$18, %esi
	movq	%r12, %rdi
	movq	%rax, -1824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1824(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -1824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1824(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L628
.L617:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15IsJSReceiverMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadMapBitFieldENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$18, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1816(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1800(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L629
	addq	$1848, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movq	$8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -1824(%rbp)
	movabsq	$8243102915230590537, %rax
	movq	%rax, -192(%rbp)
	movb	$0, -184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L603
	call	_ZdlPv@PLT
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movq	$8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -1824(%rbp)
	movabsq	$8386658438684304969, %rax
	movq	%rax, -192(%rbp)
	movb	$0, -184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L617
	call	_ZdlPv@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movq	$10, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -1824(%rbp)
	movabsq	$7598807797345969737, %rax
	movq	%rax, -192(%rbp)
	movl	$28271, %eax
	movw	%ax, -184(%rbp)
	movb	$0, -182(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L615
	call	_ZdlPv@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	-192(%rbp), %rax
	movl	$25966, %edx
	movq	%r12, %rdi
	movb	$100, -182(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-208(%rbp), %rsi
	movq	%rax, -1824(%rbp)
	movabsq	$7594869303629145673, %rax
	movq	%rax, -192(%rbp)
	movw	%dx, -184(%rbp)
	movq	$11, -200(%rbp)
	movb	$0, -181(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L613
	call	_ZdlPv@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L625:
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movq	$8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -1824(%rbp)
	movabsq	$8389723864334231113, %rax
	movq	%rax, -192(%rbp)
	movb	$0, -184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L611
	call	_ZdlPv@PLT
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movb	$110, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -1824(%rbp)
	movabsq	$7018134820190578249, %rax
	movq	%rax, -192(%rbp)
	movq	$9, -200(%rbp)
	movb	$0, -183(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L609
	call	_ZdlPv@PLT
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movq	$8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -1824(%rbp)
	movabsq	$7813572100837566025, %rax
	movq	%rax, -192(%rbp)
	movb	$0, -184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L607
	call	_ZdlPv@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	-192(%rbp), %rax
	movq	%r12, %rdi
	leaq	-208(%rbp), %rsi
	movq	$8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -1824(%rbp)
	movabsq	$7453010373643560521, %rax
	movq	%rax, -192(%rbp)
	movb	$0, -184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-208(%rbp), %rdi
	movq	-1824(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L605
	call	_ZdlPv@PLT
	jmp	.L605
.L629:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23067:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_119TestTypeOfAssembler12GenerateImplEv, .-_ZN2v88internal11interpreter12_GLOBAL__N_119TestTypeOfAssembler12GenerateImplEv
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_140LdaLookupGlobalSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.8,"aMS",@progbits,1
	.align 8
.LC87:
	.string	"LdaLookupGlobalSlotInsideTypeof"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_140LdaLookupGlobalSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_140LdaLookupGlobalSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_140LdaLookupGlobalSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22307:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$35, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rbx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$442, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC87(%rip), %rsi
	leaq	-288(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r15
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-416(%rbp), %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	-400(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-128(%rbp), %r8
	movl	$3, %edx
	movdqa	-448(%rbp), %xmm0
	movq	%r8, %rsi
	movq	%r8, %rdi
	movq	%r8, -448(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%r12, -128(%rbp)
	movq	%r12, -96(%rbp)
	movq	%r12, -88(%rbp)
	call	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	movq	%rbx, %xmm0
	pushq	$1
	xorl	%r9d, %r9d
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rbx
	movq	-432(%rbp), %r11
	movq	-448(%rbp), %r8
	movq	%rax, %xmm2
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	-160(%rbp), %r10
	movq	%r15, %rdi
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	movq	%r10, %rcx
	movq	%r11, %rdx
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	movq	-424(%rbp), %rsi
	pushq	%rbx
	punpcklqdq	%xmm3, %xmm0
	movq	%r10, -448(%rbp)
	movq	%r12, -160(%rbp)
	movl	$0, -128(%rbp)
	movq	%r12, -120(%rbp)
	movq	%r8, -456(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal17AccessorAssembler12LoadGlobalICEPNS0_8compiler4NodeES4_RKSt8functionIFNS2_5TNodeINS0_7ContextEEEvEERKS5_IFNS6_INS0_4NameEEEvEENS0_10TypeofModeEPNS0_9ExitPointENS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-112(%rbp), %rax
	popq	%rdx
	movq	-448(%rbp), %r10
	popq	%rcx
	testq	%rax, %rax
	je	.L631
	movq	-456(%rbp), %r8
	movq	%r10, -424(%rbp)
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
	movq	-424(%rbp), %r10
.L631:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L632
	movl	$3, %edx
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	*%rax
.L632:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L633
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L633:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movl	$304, %esi
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L645
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L645:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22307:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_140LdaLookupGlobalSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_140LdaLookupGlobalSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter12_GLOBAL__N_128LdaLookupGlobalSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC88:
	.string	"LdaLookupGlobalSlot"
	.section	.text._ZN2v88internal11interpreter12_GLOBAL__N_128LdaLookupGlobalSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter12_GLOBAL__N_128LdaLookupGlobalSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter12_GLOBAL__N_128LdaLookupGlobalSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE:
.LFB22299:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	$32, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rbx
	movq	%r12, %rdi
	movq	%rbx, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$434, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r13, %rdi
	leaq	.LC88(%rip), %rsi
	leaq	-288(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal11interpreter20InterpreterAssembler19BytecodeOperandUImmEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r15
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-416(%rbp), %r15
	call	_ZN2v88internal11interpreter20InterpreterAssembler34GotoIfHasContextExtensionUpToDepthENS0_8compiler5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEEPNS3_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18LoadFeedbackVectorEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	-400(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-128(%rbp), %r8
	movl	$3, %edx
	movdqa	-448(%rbp), %xmm0
	movq	%r8, %rsi
	movq	%r8, %rdi
	movq	%r8, -448(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%r12, -128(%rbp)
	movq	%r12, -96(%rbp)
	movq	%r12, -88(%rbp)
	call	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	movq	%rbx, %xmm0
	pushq	$1
	movq	%r15, %rdi
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_7ContextEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS2_10TypeofModeEEUlvE1_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rbx
	movq	-432(%rbp), %r11
	movq	-448(%rbp), %r8
	movq	%rax, %xmm2
	movl	$1, %r9d
	movq	%r12, -120(%rbp)
	leaq	_ZNSt17_Function_handlerIFN2v88internal8compiler5TNodeINS1_4NameEEEvEZNS1_11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS1_10TypeofModeEEUlvE1_E9_M_invokeERKSt9_Any_data(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	movq	%r11, %rdx
	movq	-424(%rbp), %rsi
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	leaq	-160(%rbp), %r10
	pushq	%rbx
	movq	%r10, %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%r10, -448(%rbp)
	movq	%r12, -160(%rbp)
	movl	$0, -128(%rbp)
	movq	%r8, -456(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal17AccessorAssembler12LoadGlobalICEPNS0_8compiler4NodeES4_RKSt8functionIFNS2_5TNodeINS0_7ContextEEEvEERKS5_IFNS6_INS0_4NameEEEvEENS0_10TypeofModeEPNS0_9ExitPointENS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-112(%rbp), %rax
	popq	%rdx
	movq	-448(%rbp), %r10
	popq	%rcx
	testq	%rax, %rax
	je	.L647
	movq	-456(%rbp), %r8
	movq	%r10, -424(%rbp)
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
	movq	-424(%rbp), %r10
.L647:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L648
	movl	$3, %edx
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	*%rax
.L648:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L649
	leaq	-88(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L649:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movl	$303, %esi
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L661
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L661:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22299:
	.size	_ZN2v88internal11interpreter12_GLOBAL__N_128LdaLookupGlobalSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE, .-_ZN2v88internal11interpreter12_GLOBAL__N_128LdaLookupGlobalSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"../deps/v8/src/interpreter/interpreter-generator.cc:3357"
	.section	.rodata._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE.str1.1,"aMS",@progbits,1
.LC90:
	.string	"Wide"
.LC91:
	.string	"ExtraWide"
.LC92:
	.string	"LdaZero"
.LC94:
	.string	"LdaSmi"
.LC95:
	.string	"LdaUndefined"
.LC96:
	.string	"LdaNull"
.LC97:
	.string	"LdaTheHole"
.LC98:
	.string	"LdaTrue"
.LC99:
	.string	"LdaFalse"
.LC100:
	.string	"LdaConstant"
.LC101:
	.string	"LdaGlobal"
.LC102:
	.string	"LdaGlobalInsideTypeof"
.LC103:
	.string	"PushContext"
.LC104:
	.string	"PopContext"
.LC105:
	.string	"LdaCurrentContextSlot"
	.section	.rodata._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE.str1.8
	.align 8
.LC106:
	.string	"LdaImmutableCurrentContextSlot"
	.section	.rodata._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE.str1.1
.LC107:
	.string	"StaCurrentContextSlot"
.LC108:
	.string	"LdaLookupContextSlot"
	.section	.rodata._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE.str1.8
	.align 8
.LC109:
	.string	"LdaLookupContextSlotInsideTypeof"
	.section	.rodata._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE.str1.1
.LC110:
	.string	"StaLookupSlot"
.LC111:
	.string	"Ldar"
.LC112:
	.string	"Star"
.LC113:
	.string	"Mov"
.LC114:
	.string	"LdaNamedProperty"
.LC115:
	.string	"LdaModuleVariable"
.LC116:
	.string	"StaModuleVariable"
.LC117:
	.string	"StaNamedProperty"
.LC118:
	.string	"StaNamedOwnProperty"
.LC119:
	.string	"Add"
.LC120:
	.string	"Sub"
.LC121:
	.string	"Mul"
.LC122:
	.string	"Div"
.LC123:
	.string	"Mod"
.LC124:
	.string	"Exp"
.LC125:
	.string	"BitwiseOr"
.LC126:
	.string	"BitwiseXor"
.LC127:
	.string	"BitwiseAnd"
.LC128:
	.string	"ShiftLeft"
.LC129:
	.string	"ShiftRight"
.LC130:
	.string	"ShiftRightLogical"
.LC131:
	.string	"AddSmi"
.LC132:
	.string	"SubSmi"
.LC133:
	.string	"MulSmi"
.LC134:
	.string	"DivSmi"
.LC135:
	.string	"ModSmi"
.LC136:
	.string	"ExpSmi"
.LC137:
	.string	"BitwiseOrSmi"
.LC138:
	.string	"BitwiseXorSmi"
.LC139:
	.string	"BitwiseAndSmi"
.LC140:
	.string	"ShiftLeftSmi"
.LC141:
	.string	"ShiftRightSmi"
.LC142:
	.string	"ShiftRightLogicalSmi"
.LC143:
	.string	"Inc"
.LC144:
	.string	"Dec"
.LC145:
	.string	"Negate"
.LC146:
	.string	"BitwiseNot"
.LC147:
	.string	"TypeOf"
.LC148:
	.string	"GetSuperConstructor"
.LC149:
	.string	"CallProperty0"
.LC150:
	.string	"CallProperty1"
.LC151:
	.string	"CallProperty2"
.LC152:
	.string	"CallUndefinedReceiver0"
.LC153:
	.string	"CallUndefinedReceiver1"
.LC154:
	.string	"CallUndefinedReceiver2"
.LC155:
	.string	"CallNoFeedback"
.LC156:
	.string	"InvokeIntrinsic"
.LC157:
	.string	"TestEqual"
.LC158:
	.string	"TestEqualStrict"
.LC159:
	.string	"TestLessThan"
.LC160:
	.string	"TestGreaterThan"
.LC161:
	.string	"TestLessThanOrEqual"
.LC162:
	.string	"TestGreaterThanOrEqual"
.LC163:
	.string	"TestTypeOf"
.LC164:
	.string	"ToNumber"
.LC165:
	.string	"ToNumeric"
.LC166:
	.string	"ToString"
.LC167:
	.string	"CreateEmptyArrayLiteral"
.LC168:
	.string	"CreateEmptyObjectLiteral"
.LC169:
	.string	"GetTemplateObject"
.LC170:
	.string	"CreateClosure"
.LC171:
	.string	"Jump"
.LC172:
	.string	"JumpConstant"
.LC173:
	.string	"JumpIfNullConstant"
.LC174:
	.string	"JumpIfNotNullConstant"
.LC175:
	.string	"JumpIfUndefinedConstant"
.LC176:
	.string	"JumpIfNotUndefinedConstant"
.LC177:
	.string	"JumpIfTrueConstant"
.LC178:
	.string	"JumpIfFalseConstant"
.LC179:
	.string	"JumpIfTrue"
.LC180:
	.string	"JumpIfFalse"
.LC181:
	.string	"JumpIfNull"
.LC182:
	.string	"JumpIfNotNull"
.LC183:
	.string	"JumpIfUndefined"
.LC184:
	.string	"JumpIfNotUndefined"
.LC185:
	.string	"ForInPrepare"
.LC186:
	.string	"ForInNext"
.LC187:
	.string	"StackCheck"
.LC188:
	.string	"Return"
.LC189:
	.string	"Illegal"
	.section	.text._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE
	.type	_ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE, @function
_ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE:
.LFB23666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	leaq	-592(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-784(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-720(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$840, %rsp
	movl	%edx, -880(%rbp)
	leaq	.LC89(%rip), %rdx
	movb	%sil, -856(%rbp)
	movq	41136(%rdi), %rsi
	movl	%ecx, -876(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdi, -872(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	xorl	%edx, %edx
	cmpb	$0, _ZN2v88internal31FLAG_untrusted_code_mitigationsE(%rip)
	movl	%ebx, %edi
	setne	%dl
	addl	$1, %edx
	movl	%edx, -864(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes8ToStringENS1_8BytecodeE@PLT
	movl	-876(%rbp), %ecx
	movq	%r15, %rdi
	movl	-864(%rbp), %edx
	movq	-872(%rbp), %r11
	movq	%rax, %r9
	leaq	16+_ZTVN2v88internal29InterpreterDispatchDescriptorE(%rip), %rsi
	leaq	1600+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	pushq	%rcx
	movq	%rsi, %xmm0
	movq	%rax, %xmm1
	movq	%r12, %rcx
	pushq	%rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r11, %rsi
	movaps	%xmm0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerStateC1EPNS0_7IsolateEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorENS0_4Code4KindEPKcNS0_24PoisoningMitigationLevelEi@PLT
	popq	%rdx
	popq	%rcx
	cmpb	$-74, %bl
	ja	.L664
	movzbl	-856(%rbp), %edx
	leaq	.L666(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE,"a",@progbits
	.align 4
	.align 4
.L666:
	.long	.L848-.L666
	.long	.L847-.L666
	.long	.L846-.L666
	.long	.L845-.L666
	.long	.L844-.L666
	.long	.L843-.L666
	.long	.L842-.L666
	.long	.L841-.L666
	.long	.L840-.L666
	.long	.L839-.L666
	.long	.L838-.L666
	.long	.L837-.L666
	.long	.L836-.L666
	.long	.L835-.L666
	.long	.L834-.L666
	.long	.L833-.L666
	.long	.L832-.L666
	.long	.L831-.L666
	.long	.L830-.L666
	.long	.L829-.L666
	.long	.L828-.L666
	.long	.L827-.L666
	.long	.L826-.L666
	.long	.L825-.L666
	.long	.L824-.L666
	.long	.L823-.L666
	.long	.L822-.L666
	.long	.L821-.L666
	.long	.L820-.L666
	.long	.L819-.L666
	.long	.L818-.L666
	.long	.L817-.L666
	.long	.L816-.L666
	.long	.L815-.L666
	.long	.L814-.L666
	.long	.L813-.L666
	.long	.L812-.L666
	.long	.L811-.L666
	.long	.L810-.L666
	.long	.L809-.L666
	.long	.L808-.L666
	.long	.L807-.L666
	.long	.L806-.L666
	.long	.L805-.L666
	.long	.L804-.L666
	.long	.L803-.L666
	.long	.L802-.L666
	.long	.L801-.L666
	.long	.L800-.L666
	.long	.L799-.L666
	.long	.L798-.L666
	.long	.L797-.L666
	.long	.L796-.L666
	.long	.L795-.L666
	.long	.L794-.L666
	.long	.L793-.L666
	.long	.L792-.L666
	.long	.L791-.L666
	.long	.L790-.L666
	.long	.L789-.L666
	.long	.L788-.L666
	.long	.L787-.L666
	.long	.L786-.L666
	.long	.L785-.L666
	.long	.L784-.L666
	.long	.L783-.L666
	.long	.L782-.L666
	.long	.L781-.L666
	.long	.L780-.L666
	.long	.L779-.L666
	.long	.L778-.L666
	.long	.L777-.L666
	.long	.L776-.L666
	.long	.L775-.L666
	.long	.L774-.L666
	.long	.L773-.L666
	.long	.L772-.L666
	.long	.L771-.L666
	.long	.L770-.L666
	.long	.L769-.L666
	.long	.L768-.L666
	.long	.L767-.L666
	.long	.L766-.L666
	.long	.L765-.L666
	.long	.L764-.L666
	.long	.L763-.L666
	.long	.L762-.L666
	.long	.L761-.L666
	.long	.L760-.L666
	.long	.L759-.L666
	.long	.L758-.L666
	.long	.L757-.L666
	.long	.L756-.L666
	.long	.L755-.L666
	.long	.L754-.L666
	.long	.L753-.L666
	.long	.L752-.L666
	.long	.L751-.L666
	.long	.L750-.L666
	.long	.L749-.L666
	.long	.L748-.L666
	.long	.L747-.L666
	.long	.L746-.L666
	.long	.L745-.L666
	.long	.L744-.L666
	.long	.L743-.L666
	.long	.L742-.L666
	.long	.L741-.L666
	.long	.L740-.L666
	.long	.L739-.L666
	.long	.L738-.L666
	.long	.L737-.L666
	.long	.L736-.L666
	.long	.L735-.L666
	.long	.L734-.L666
	.long	.L733-.L666
	.long	.L732-.L666
	.long	.L731-.L666
	.long	.L730-.L666
	.long	.L729-.L666
	.long	.L728-.L666
	.long	.L727-.L666
	.long	.L726-.L666
	.long	.L725-.L666
	.long	.L724-.L666
	.long	.L723-.L666
	.long	.L722-.L666
	.long	.L721-.L666
	.long	.L720-.L666
	.long	.L719-.L666
	.long	.L718-.L666
	.long	.L717-.L666
	.long	.L716-.L666
	.long	.L715-.L666
	.long	.L714-.L666
	.long	.L713-.L666
	.long	.L712-.L666
	.long	.L711-.L666
	.long	.L710-.L666
	.long	.L709-.L666
	.long	.L708-.L666
	.long	.L707-.L666
	.long	.L706-.L666
	.long	.L705-.L666
	.long	.L704-.L666
	.long	.L703-.L666
	.long	.L702-.L666
	.long	.L701-.L666
	.long	.L700-.L666
	.long	.L699-.L666
	.long	.L698-.L666
	.long	.L697-.L666
	.long	.L696-.L666
	.long	.L695-.L666
	.long	.L694-.L666
	.long	.L693-.L666
	.long	.L692-.L666
	.long	.L691-.L666
	.long	.L690-.L666
	.long	.L689-.L666
	.long	.L688-.L666
	.long	.L687-.L666
	.long	.L686-.L666
	.long	.L685-.L666
	.long	.L684-.L666
	.long	.L683-.L666
	.long	.L682-.L666
	.long	.L681-.L666
	.long	.L680-.L666
	.long	.L679-.L666
	.long	.L678-.L666
	.long	.L677-.L666
	.long	.L676-.L666
	.long	.L675-.L666
	.long	.L674-.L666
	.long	.L673-.L666
	.long	.L672-.L666
	.long	.L671-.L666
	.long	.L670-.L666
	.long	.L669-.L666
	.long	.L668-.L666
	.long	.L667-.L666
	.long	.L665-.L666
	.section	.text._ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE
.L665:
	movzbl	-880(%rbp), %ecx
	movl	$-74, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3233, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC189(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler5AbortENS0_11AbortReasonE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12GenerateCodeEPNS1_18CodeAssemblerStateERKNS0_16AssemblerOptionsE@PLT
	cmpb	$0, _ZN2v88internal27FLAG_trace_ignition_codegenE(%rip)
	movq	%rax, %r14
	jne	.L853
.L849:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerStateD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L854
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L853:
	.cfi_restore_state
	leaq	-320(%rbp), %r8
	leaq	-400(%rbp), %r10
	movq	%r8, %rdi
	movq	%r8, -864(%rbp)
	movq	%r10, -856(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	-856(%rbp), %r10
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r9
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	%r10, %rdi
	movq	%r9, -320(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	%ebx, %edi
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rbx
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	movq	(%r14), %rax
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes8ToStringENS1_8BytecodeE@PLT
	movq	-856(%rbp), %r10
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal4Code11DisassembleEPKcRSom@PLT
	movq	-856(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-864(%rbp), %r8
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r9
	movq	%rax, -400(%rbp)
	movq	%r8, %rdi
	movq	%r9, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L849
.L667:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_114AbortAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L668:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_124IncBlockCounterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L669:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_117DebuggerAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L670:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120GetIteratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L671:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_124ResumeGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L672:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_125SuspendGeneratorAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L673:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_131SwitchOnGeneratorStateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L674:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_141ThrowSuperAlreadyCalledIfNotHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L675:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowSuperNotCalledIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L676:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_134ThrowReferenceErrorIfHoleAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L677:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-85, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2914, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC188(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler29UpdateInterruptBudgetOnReturnEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L678:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_116ReThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L679:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_114ThrowAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L680:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126SetPendingMessageAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L681:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-89, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2858, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC187(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L682:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_118ForInStepAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L683:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-91, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3122, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC186(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_118ForInNextAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L684:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_122ForInContinueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L685:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-93, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3060, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC185(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_121ForInPrepareAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L686:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123ForInEnumerateAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L687:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_130SwitchOnSmiNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L688:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_125JumpIfJSReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L689:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_130JumpIfUndefinedOrNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L690:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-98, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2287, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC184(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L691:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-99, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2267, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC183(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L692:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-100, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2247, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC182(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12NullConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L693:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-101, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2227, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC181(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12NullConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L694:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-102, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2142, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC180(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L695:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-103, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2118, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC179(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L696:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129JumpIfToBooleanFalseAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L697:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128JumpIfToBooleanTrueAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L698:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_137JumpIfToBooleanFalseConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L699:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_136JumpIfToBooleanTrueConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L700:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_133JumpIfJSReceiverConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L701:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-109, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2154, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC178(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L702:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-110, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2130, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC177(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L703:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_138JumpIfUndefinedOrNullConstantAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L704:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-112, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2298, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC176(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L705:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-113, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2277, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC175(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L706:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-114, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2257, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC174(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12NullConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler20JumpIfTaggedNotEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L707:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-115, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2237, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC173(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12NullConstantEv@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17JumpIfTaggedEqualENS0_8compiler5TNodeINS0_6ObjectEEES6_PNS3_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L708:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-116, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2108, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC172(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler43LoadAndUntagConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L709:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-117, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2099, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC171(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler23BytecodeOperandUImmWordEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler4JumpEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L710:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_117JumpLoopAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L711:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRestParameterAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L712:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateUnmappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L713:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_130CreateMappedArgumentsAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L714:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126CreateWithContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L715:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126CreateEvalContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L716:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_130CreateFunctionContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L717:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateCatchContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L718:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateBlockContextAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L719:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-127, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2677, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC170(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_122CreateClosureAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L720:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$-128, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2635, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC169(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126GetTemplateObjectAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L721:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120CloneObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L722:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$126, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2600, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC168(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	leaq	-816(%rbp), %r9
	movq	-720(%rbp), %rsi
	movq	%r9, %rdi
	movq	%rax, -864(%rbp)
	movq	%r9, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-864(%rbp), %r8
	movq	-856(%rbp), %r9
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal28ConstructorBuiltinsAssembler28EmitCreateEmptyObjectLiteralEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	-856(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L723:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateObjectLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L724:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$124, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$2506, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC167(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateEmptyArrayLiteralAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L725:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_132CreateArrayFromIterableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L726:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_127CreateArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L727:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128CreateRegExpLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L728:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$120, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1368, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC166(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-856(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L729:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_117ToObjectAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L730:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$118, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1350, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC165(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L731:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$117, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1343, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC164(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17ToNumberOrNumericENS0_6Object10ConversionE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L732:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_115ToNameAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L733:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$115, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1981, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC163(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_119TestTypeOfAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L734:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_122TestUndefinedAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L735:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_117TestNullAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L736:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_125TestUndetectableAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L737:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_115TestInAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L738:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123TestInstanceOfAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L739:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_127TestReferenceEqualAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L740:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$108, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1874, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC162(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$21, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L741:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$107, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1866, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC161(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$19, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L742:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$106, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1858, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC160(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$20, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L743:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$105, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1851, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC159(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$18, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L744:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$104, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1844, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC158(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$17, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L745:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$103, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1837, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC157(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129InterpreterCompareOpAssembler21CompareOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L746:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128ConstructWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L747:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_118ConstructAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L748:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$100, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1692, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC156(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26BytecodeOperandIntrinsicIdEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rdx, -808(%rbp)
	movq	%rax, -816(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-856(%rbp), %r8
	leaq	-816(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter23GenerateInvokeIntrinsicEPNS1_20InterpreterAssemblerEPNS0_8compiler4NodeES6_RKNS2_15RegListNodePairE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L749:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_122CallJSRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L750:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_127CallRuntimeForPairAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L751:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120CallRuntimeAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L752:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123CallWithSpreadAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L753:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$95, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1669, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC155(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler29GetRegisterListAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rdx, -808(%rbp)
	movq	%rax, -816(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-856(%rbp), %r9
	movq	%r12, %rdi
	leaq	-816(%rbp), %rcx
	movq	%rax, %rdx
	movl	$2, %r8d
	movq	%r9, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler17CallJSAndDispatchEPNS0_8compiler4NodeES5_RKNS2_15RegListNodePairENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L754:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$94, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1665, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC154(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$2, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L755:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$93, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1661, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC153(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L756:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$92, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1657, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC152(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L757:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_130CallUndefinedReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L758:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$90, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1649, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC151(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$2, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L759:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$89, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1645, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC150(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L760:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$88, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1641, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC149(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126InterpreterJSCallAssembler7JSCallNEiNS0_19ConvertReceiverModeE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L761:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_121CallPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L762:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_124CallAnyReceiverAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L763:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$85, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1535, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC148(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-856(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19GetSuperConstructorENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L764:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertySloppyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L765:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_129DeletePropertyStrictAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L766:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$82, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1494, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC147(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6TypeofEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L767:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_119LogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L768:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128ToBooleanLogicalNotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L769:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$79, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1116, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC146(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_119BitwiseNotAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L770:
	leaq	-712(%rbp), %r8
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movl	$78, %edx
	movq	%r8, %rdi
	movq	%r8, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerE(%rip), %rax
	movq	%r15, %rdi
	movl	$1327, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC145(%rip), %rsi
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv
	movq	-856(%rbp), %r8
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	movq	%rax, -720(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L771:
	leaq	-712(%rbp), %r8
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movl	$77, %edx
	movq	%r8, %rdi
	movq	%r8, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerE(%rip), %rax
	movq	%r15, %rdi
	movl	$1437, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC144(%rip), %rsi
	movq	%rax, -720(%rbp)
	movl	$16, -604(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$15, -604(%rbp)
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv
	movq	-856(%rbp), %r8
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	movq	%rax, -720(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L772:
	leaq	-712(%rbp), %r8
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movl	$76, %edx
	movq	%r8, %rdi
	movq	%r8, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerE(%rip), %rax
	movq	%r15, %rdi
	movl	$1432, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC143(%rip), %rsi
	movq	%rax, -720(%rbp)
	movl	$16, -604(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$14, -604(%rbp)
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssembler19UnaryOpWithFeedbackEv
	movq	-856(%rbp), %r8
	leaq	16+_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE(%rip), %rax
	movq	%rax, -720(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L773:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$75, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1174, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC142(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$11, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L774:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$74, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1165, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC141(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$10, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L775:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$73, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1156, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC140(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$9, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L776:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$72, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1109, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC139(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L777:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$71, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1102, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC138(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$8, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L778:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$70, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1095, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC137(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler22BitwiseBinaryOpWithSmiENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L779:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$69, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$946, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC136(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L780:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$68, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$939, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC135(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L781:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$67, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$932, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC134(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L782:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$66, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$925, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC133(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L783:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$65, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$918, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC132(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L784:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$64, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$911, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC131(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler23BinaryOpSmiWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L785:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$63, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1088, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC130(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$11, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L786:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$62, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1078, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC129(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$10, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L787:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$61, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1068, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC128(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$9, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L788:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$60, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1058, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC127(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L789:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$59, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1051, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC126(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$8, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L790:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$58, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$1044, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC125(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135InterpreterBitwiseBinaryOpAssembler27BitwiseBinaryOpWithFeedbackENS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L791:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$57, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$904, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC124(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L792:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$56, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$897, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC123(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L793:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$55, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$890, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC122(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L794:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$54, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$883, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC121(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L795:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$53, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$876, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC120(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L796:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$52, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$869, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC119(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	_ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128InterpreterBinaryOpAssembler20BinaryOpWithFeedbackEMNS0_17BinaryOpAssemblerEFPNS0_8compiler4NodeES7_S7_S7_S7_S7_bE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L797:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_127CollectTypeProfileAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L798:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_133StaDataPropertyInLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L799:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126StaInArrayLiteralAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L800:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_125StaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L801:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$47, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$615, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC118(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-848(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE@PLT
	movq	-848(%rbp), %rax
	movq	%r12, %rdi
	leaq	-816(%rbp), %rsi
	movq	%rax, -816(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -808(%rbp)
	movq	-832(%rbp), %rax
	movq	%rax, -800(%rbp)
	call	_ZN2v88internal11interpreter12_GLOBAL__N_138InterpreterStoreNamedPropertyAssembler16StaNamedPropertyENS0_8CallableENS0_17NamedPropertyTypeE.isra.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L802:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135StaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L803:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$45, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$605, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC117(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-848(%rbp), %rdi
	movl	$379, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-848(%rbp), %rax
	movq	%r12, %rdi
	leaq	-816(%rbp), %rsi
	movq	%rax, -816(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -808(%rbp)
	movq	-832(%rbp), %rax
	movq	%rax, -800(%rbp)
	call	_ZN2v88internal11interpreter12_GLOBAL__N_138InterpreterStoreNamedPropertyAssembler16StaNamedPropertyENS0_8CallableENS0_17NamedPropertyTypeE.isra.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L804:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$44, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$771, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC116(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126StaModuleVariableAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L805:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$43, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$727, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC115(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_126LdaModuleVariableAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L806:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_125LdaKeyedPropertyAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L807:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_135LdaNamedPropertyNoFeedbackAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L808:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$40, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$508, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC114(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_125LdaNamedPropertyAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L809:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$39, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$149, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC113(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L810:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$38, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$140, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC112(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L811:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$37, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$131, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC111(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L812:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$36, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$451, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC110(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_122StaLookupSlotAssembler12GenerateImplEv
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L813:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_140LdaLookupGlobalSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L814:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$34, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$386, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC109(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$304, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_137InterpreterLookupContextSlotAssembler17LookupContextSlotENS0_7Runtime10FunctionIdE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L815:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_134LdaLookupSlotInsideTypeofAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L816:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128LdaLookupGlobalSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L817:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$31, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$378, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC108(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$303, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_137InterpreterLookupContextSlotAssembler17LookupContextSlotENS0_7Runtime10FunctionIdE
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L818:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_122LdaLookupSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L819:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$29, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$305, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC107(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-864(%rbp), %rcx
	movq	-856(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19StoreContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L820:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123StaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L821:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$27, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$279, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC106(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-856(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L822:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$26, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$268, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC105(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler18BytecodeOperandIdxEi@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	movq	-856(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L823:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_132LdaImmutableContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L824:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123LdaContextSlotAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L825:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$23, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$821, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC104(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler26LoadRegisterAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L826:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$22, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$810, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC103(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14GetAccumulatorEv@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal11interpreter20InterpreterAssembler10GetContextEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler27StoreRegisterAtOperandIndexEPNS0_8compiler4NodeEi@PLT
	movq	-856(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler10SetContextENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L827:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_118StaGlobalAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L828:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$20, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$201, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC102(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS0_10TypeofModeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L829:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$19, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$190, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC101(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_130InterpreterLoadGlobalAssembler9LdaGlobalEiiNS0_10TypeofModeE.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L830:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$18, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$82, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC100(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler35LoadConstantPoolEntryAtOperandIndexEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L831:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$17, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$123, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC99(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L832:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$115, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC98(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L833:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$15, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$107, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC97(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L834:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$14, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$99, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC96(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12NullConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L835:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$13, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$91, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC95(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L836:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$12, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$73, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC94(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler21BytecodeOperandImmSmiEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L837:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$11, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$64, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC92(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14NumberConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11interpreter20InterpreterAssembler14SetAccumulatorEPNS0_8compiler4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssembler8DispatchEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L838:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak6Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L839:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak5Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L840:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak4Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L841:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak3Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L842:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak2Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L843:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak1Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L844:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_120DebugBreak0Assembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L845:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_128DebugBreakExtraWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L846:
	movzbl	-880(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter12_GLOBAL__N_123DebugBreakWideAssembler8GenerateEPNS0_8compiler18CodeAssemblerStateENS1_12OperandScaleE
	jmp	.L664
.L847:
	movzbl	-880(%rbp), %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3226, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC91(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L848:
	movzbl	-880(%rbp), %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerC2EPNS0_8compiler18CodeAssemblerStateENS1_8BytecodeENS1_12OperandScaleE@PLT
	movl	$3219, %ecx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rdx
	leaq	.LC90(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal11interpreter20InterpreterAssembler12DispatchWideENS1_12OperandScaleE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter20InterpreterAssemblerD2Ev@PLT
	jmp	.L664
.L854:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23666:
	.size	_ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE, .-_ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE:
.LFB30403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30403:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter23GenerateBytecodeHandlerEPNS0_7IsolateENS1_8BytecodeENS1_12OperandScaleEiRKNS0_16AssemblerOptionsE
	.weak	_ZN2v88internal13MachineTypeOfINS0_9EnumCacheEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_9EnumCacheEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_9EnumCacheEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_9EnumCacheEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_9EnumCacheEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_9EnumCacheEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10FixedArrayEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.data.rel.ro._ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE,"aw"
	.align 8
	.type	_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE, @object
	.size	_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE, 56
_ZTVN2v88internal11interpreter12_GLOBAL__N_123UnaryNumericOpAssemblerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerE,"aw"
	.align 8
	.type	_ZTVN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerE, @object
	.size	_ZTVN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerE, 56
_ZTVN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD1Ev
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115NegateAssemblerD0Ev
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl7FloatOpEPNS0_8compiler4NodeE
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_119NegateAssemblerImpl8BigIntOpEPNS0_8compiler4NodeE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerE,"aw"
	.align 8
	.type	_ZTVN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerE, @object
	.size	_ZTVN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerE, 56
_ZTVN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD1Ev
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_112IncAssemblerD0Ev
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler7FloatOpEPNS0_8compiler4NodeE
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler8BigIntOpEPNS0_8compiler4NodeE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerE,"aw"
	.align 8
	.type	_ZTVN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerE, @object
	.size	_ZTVN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerE, 56
_ZTVN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD1Ev
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_112DecAssemblerD0Ev
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler5SmiOpENS0_8compiler5TNodeINS0_3SmiEEEPNS4_21CodeAssemblerVariableEPNS4_18CodeAssemblerLabelES9_
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler7FloatOpEPNS0_8compiler4NodeE
	.quad	_ZN2v88internal11interpreter12_GLOBAL__N_115IncDecAssembler8BigIntOpEPNS0_8compiler4NodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC28:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC85:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC86:
	.long	4
	.long	5
	.long	6
	.long	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
