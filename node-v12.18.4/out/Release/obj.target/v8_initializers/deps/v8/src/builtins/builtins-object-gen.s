	.file	"builtins-object-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation:
.LFB27072:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27072:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation:
.LFB27076:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27076:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation:
.LFB27080:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27080:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation:
.LFB27084:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L25
	cmpl	$3, %edx
	je	.L26
	cmpl	$1, %edx
	je	.L30
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27084:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %r12
	movq	(%rax), %r14
	movq	8(%rax), %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$162, %edx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %r9
	movl	$2, %edi
	movq	%rax, %r8
	movq	%r13, %xmm1
	movl	$1, %ecx
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	pushq	%rsi
	punpcklqdq	%xmm1, %xmm0
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26318:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB27083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r13
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %r12
	movq	%r12, -120(%rbp)
	movq	%r12, %rdi
	movq	%r12, -128(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal17CodeStubAssembler13IsCallableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS6_26GenerateObjectToStringImplEvENKS7_clEvEUlvE0_EENS2_8compiler5TNodeIT_EENSA_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSP_St18_Manager_operation(%rip), %rdx
	movq	%rax, %rsi
	leaq	-120(%rbp), %rax
	movq	%rcx, %xmm0
	movl	$7, %r8d
	movq	%rax, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rcx
	movq	%rax, %xmm1
	leaq	-128(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -112(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, %xmm2
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, %r12
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L40
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L40:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L39
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L39:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27083:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB27075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler20ObjectStringConstantEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27075:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB27071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler22FunctionStringConstantEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27071:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvENKUlvE0_clEvEUlvE_ZZNS9_26GenerateObjectToStringImplEvENKSA_clEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB27079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler19ArrayStringConstantEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27079:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB26319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L58
	cmpl	$3, %edx
	je	.L59
	cmpl	$1, %edx
	je	.L65
.L60:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L60
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26319:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.rodata._ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"[object "
.LC2:
	.string	"]"
	.section	.text._ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_:
.LFB21953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rsi
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%rbx, %r9
	movq	%r14, %rdx
	pushq	%rdi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	movl	$1, %ecx
	movq	-136(%rbp), %xmm0
	pushq	%r13
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r15, -128(%rbp)
	movhps	-152(%rbp), %xmm0
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	xorl	%esi, %esi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	-96(%rbp), %rax
	movl	$1, %ecx
	pushq	%r13
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-136(%rbp), %xmm0
	movq	%r15, -128(%rbp)
	movhps	-144(%rbp), %xmm0
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21953:
	.size	_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_, .-_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_, @function
_ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_:
.LFB21960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$6, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1, %r9d
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	-56(%rbp), %r10
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rax, %r13
	movq	%rax, %rsi
	movl	$24, %edx
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movl	$32, %edx
	movq	%r13, %rsi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movl	$40, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$48, %edx
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21960:
	.size	_ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_, .-_ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	.section	.text._ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_, @function
_ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_:
.LFB21961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$37, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1, %r9d
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	-56(%rbp), %r10
	movl	$24, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movl	$32, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movl	$40, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$48, %edx
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21961:
	.size	_ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_, .-_ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	.section	.text._ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler20IsPropertyEnumerableENS0_8compiler5TNodeINS0_7Uint32TEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler20IsPropertyEnumerableENS0_8compiler5TNodeINS0_7Uint32TEEE
	.type	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler20IsPropertyEnumerableENS0_8compiler5TNodeINS0_7Uint32TEEE, @function
_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler20IsPropertyEnumerableENS0_8compiler5TNodeINS0_7Uint32TEEE:
.LFB21962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$56, %ecx
	movl	$3, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	.cfi_endproc
.LFE21962:
	.size	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler20IsPropertyEnumerableENS0_8compiler5TNodeINS0_7Uint32TEEE, .-_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler20IsPropertyEnumerableENS0_8compiler5TNodeINS0_7Uint32TEEE
	.section	.text._ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler22IsPropertyKindAccessorENS0_8compiler5TNodeINS0_7Uint32TEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler22IsPropertyKindAccessorENS0_8compiler5TNodeINS0_7Uint32TEEE
	.type	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler22IsPropertyKindAccessorENS0_8compiler5TNodeINS0_7Uint32TEEE, @function
_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler22IsPropertyKindAccessorENS0_8compiler5TNodeINS0_7Uint32TEEE:
.LFB21963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	.cfi_endproc
.LFE21963:
	.size	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler22IsPropertyKindAccessorENS0_8compiler5TNodeINS0_7Uint32TEEE, .-_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler22IsPropertyKindAccessorENS0_8compiler5TNodeINS0_7Uint32TEEE
	.section	.text._ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler18IsPropertyKindDataENS0_8compiler5TNodeINS0_7Uint32TEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler18IsPropertyKindDataENS0_8compiler5TNodeINS0_7Uint32TEEE
	.type	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler18IsPropertyKindDataENS0_8compiler5TNodeINS0_7Uint32TEEE, @function
_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler18IsPropertyKindDataENS0_8compiler5TNodeINS0_7Uint32TEEE:
.LFB21964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	.cfi_endproc
.LFE21964:
	.size	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler18IsPropertyKindDataENS0_8compiler5TNodeINS0_7Uint32TEEE, .-_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler18IsPropertyKindDataENS0_8compiler5TNodeINS0_7Uint32TEEE
	.section	.text._ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler25FastGetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSObjectEEEPNS2_18CodeAssemblerLabelES9_NS1_11CollectTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler25FastGetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSObjectEEEPNS2_18CodeAssemblerLabelES9_NS1_11CollectTypeE
	.type	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler25FastGetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSObjectEEEPNS2_18CodeAssemblerLabelES9_NS1_11CollectTypeE, @function
_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler25FastGetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSObjectEEEPNS2_18CodeAssemblerLabelES9_NS1_11CollectTypeE:
.LFB21966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	leaq	-896(%rbp), %rax
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1000, %rsp
	.cfi_offset 3, -56
	movl	%r9d, -984(%rbp)
	movq	%r8, -936(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -912(%rbp)
	leaq	-880(%rbp), %rbx
	movq	%rbx, %xmm0
	movhps	-912(%rbp), %xmm0
	movaps	%xmm0, -960(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -976(%rbp)
	movq	%rax, -968(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %r13
	leaq	-848(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-720(%rbp), %rcx
	movq	%rcx, %r11
	movq	%rcx, -1000(%rbp)
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-592(%rbp), %rcx
	movq	%rcx, %r11
	movq	%rcx, -992(%rbp)
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$1023, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movl	$1023, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-944(%rbp), %r13
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-936(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%r14, %rdx
	movl	$4, %r8d
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, -928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	subq	$8, %rsp
	movq	%r14, %r8
	movq	%r13, %rdx
	pushq	$1
	movl	$5, %r9d
	movq	%rax, %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler23FillFixedArrayWithValueENS0_12ElementsKindEPNS0_8compiler4NodeES5_S5_NS0_9RootIndexENS1_13ParameterModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-912(%rbp), %rdi
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	%rbx, -904(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	-920(%rbp), %r10
	movdqa	-960(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%r10, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%r10, -1032(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadMapDescriptorsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rax, %rbx
	leaq	-464(%rbp), %rax
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-336(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -960(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-904(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-920(%rbp), %rcx
	movq	-960(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-904(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -1008(%rbp)
	call	_ZN2v88internal17CodeStubAssembler24LoadKeyByDescriptorEntryENS0_8compiler5TNodeINS0_15DescriptorArrayEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1024(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8IsSymbolENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1008(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler28LoadDetailsByDescriptorEntryENS0_8compiler5TNodeINS0_15DescriptorArrayEEENS3_INS0_7IntPtrTEEE@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1016(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -1008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1008(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1016(%rbp), %r9
	movl	$56, %ecx
	movq	%r12, %rdi
	movl	$3, %edx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1008(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	-904(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21TruncateIntPtrToInt32ENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10ToKeyIndexINS0_15DescriptorArrayEEENS0_8compiler5TNodeINS0_7IntPtrTEEENS5_INS0_7Uint32TEEE@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%r15, (%rsp)
	movq	-1032(%rbp), %r10
	movq	-976(%rbp), %rsi
	movq	%rax, %r8
	movq	-1016(%rbp), %r9
	movq	%r10, %rdx
	call	_ZN2v88internal17CodeStubAssembler26LoadPropertyFromFastObjectEPNS0_8compiler4NodeES4_NS2_5TNodeINS0_15DescriptorArrayEEES4_S4_PNS2_21CodeAssemblerVariableE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-984(%rbp), %edi
	popq	%rcx
	movq	-1024(%rbp), %r11
	movq	%rax, %r10
	popq	%rsi
	testl	%edi, %edi
	je	.L84
.L81:
	movq	-912(%rbp), %rdi
	movq	%r10, -976(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	-928(%rbp), %rsi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	movq	-976(%rbp), %r10
	pushq	$1
	movl	$4, %r8d
	movq	%r12, %rdi
	movq	-928(%rbp), %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	-912(%rbp), %rbx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-904(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-960(%rbp), %r14
	movq	-920(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-936(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-928(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	-968(%rbp), %rsi
	movq	%rax, %rcx
	movl	$32, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_14FixedArrayBaseEEENS3_INS0_3SmiEEEPNS2_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-904(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-992(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1000(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-944(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%r11, -1016(%rbp)
	movq	%rax, -1008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	subq	$8, %rsp
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	$32
	movq	-968(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	pushq	$0
	movl	$2, %esi
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler40AllocateUninitializedJSArrayWithElementsENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEENS4_INS0_3SmiEEEPNS3_4NodeESA_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEEi@PLT
	addq	$32, %rsp
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, %rbx
	movq	%rax, -976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -984(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	movq	-1016(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-984(%rbp), %rdx
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -984(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	-1008(%rbp), %r10
	movq	-984(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, (%rsp)
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	popq	%rax
	movq	-976(%rbp), %r10
	popq	%rdx
	jmp	.L81
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21966:
	.size	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler25FastGetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSObjectEEEPNS2_18CodeAssemblerLabelES9_NS1_11CollectTypeE, .-_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler25FastGetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSObjectEEEPNS2_18CodeAssemblerLabelES9_NS1_11CollectTypeE
	.section	.text._ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE
	.type	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE, @function
_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE:
.LFB21965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-448(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -464(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-192(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13IsJSObjectMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-472(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler26GotoIfMapHasSlowPropertiesENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelE@PLT
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17IsEmptyFixedArrayENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movl	-464(%rbp), %r9d
	movq	-456(%rbp), %r8
	movq	%r12, %rdi
	call	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler25FastGetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSObjectEEEPNS2_18CodeAssemblerLabelES9_NS1_11CollectTypeE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	pushq	$0
	xorl	%r9d, %r9d
	movl	$2, %esi
	pushq	$1
	movq	-472(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-480(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	-464(%rbp), %ecx
	popq	%rax
	movq	%rbx, -64(%rbp)
	popq	%rdx
	movl	$1, %r8d
	movq	%r15, %rdx
	testl	%ecx, %ecx
	leaq	-64(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	jne	.L87
	movl	$239, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, -64(%rbp)
	movq	%r15, %rdx
	movl	$240, %esi
	movq	-464(%rbp), %rcx
	movl	$1, %r8d
.L91:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	$246, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, -64(%rbp)
	movq	%r15, %rdx
	movl	$247, %esi
	movq	-464(%rbp), %rcx
	movl	$1, %r8d
	jmp	.L91
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21965:
	.size	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE, .-_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE
	.section	.text._ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler30FinalizeValuesOrEntriesJSArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10FixedArrayEEENS3_INS0_7IntPtrTEEENS3_INS0_3MapEEEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler30FinalizeValuesOrEntriesJSArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10FixedArrayEEENS3_INS0_7IntPtrTEEENS3_INS0_3MapEEEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler30FinalizeValuesOrEntriesJSArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10FixedArrayEEENS3_INS0_7IntPtrTEEENS3_INS0_3MapEEEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler30FinalizeValuesOrEntriesJSArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10FixedArrayEEENS3_INS0_7IntPtrTEEENS3_INS0_3MapEEEPNS2_18CodeAssemblerLabelE:
.LFB21981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$32, %r9d
	xorl	%r8d, %r8d
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_14FixedArrayBaseEEENS3_INS0_3SmiEEEPNS2_4NodeEi@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21981:
	.size	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler30FinalizeValuesOrEntriesJSArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10FixedArrayEEENS3_INS0_7IntPtrTEEENS3_INS0_3MapEEEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler30FinalizeValuesOrEntriesJSArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10FixedArrayEEENS3_INS0_7IntPtrTEEENS3_INS0_3MapEEEPNS2_18CodeAssemblerLabelE
	.section	.rodata._ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Object.prototype.toLocaleString"
	.section	.text._ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv
	.type	_ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv, @function
_ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv:
.LFB21990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17IsNullOrUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3456(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-240(%rbp), %r11
	movl	$710, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r15, %r9
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	-224(%rbp), %rax
	pushq	%rdi
	movq	%rbx, %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movhps	-264(%rbp), %xmm0
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, -248(%rbp)
	leaq	-80(%rbp), %rax
	movl	$1, %ecx
	pushq	%rax
	movq	%r10, -256(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-272(%rbp), %r11
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	movl	$3, %ebx
	xorl	%esi, %esi
	movq	-280(%rbp), %rcx
	pushq	%rbx
	movq	%rax, %r8
	movq	%r15, %r9
	movq	-264(%rbp), %xmm0
	movq	-224(%rbp), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	pushq	%rcx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movl	$1, %ecx
	movhps	-272(%rbp), %xmm0
	movq	%r10, -256(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$27, %edx
	movq	%r15, %rsi
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L98:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21990:
	.size	_ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv, .-_ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv
	.section	.rodata._ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../deps/v8/src/builtins/builtins-object-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"ObjectPrototypeToLocaleString"
	.section	.text._ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB21986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$352, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$485, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L103
.L100:
	movq	%r13, %rdi
	call	_ZN2v88internal38ObjectPrototypeToLocaleStringAssembler41GenerateObjectPrototypeToLocaleStringImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L100
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21986:
	.size	_ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins38Generate_ObjectPrototypeToLocaleStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal38ObjectPrototypeHasOwnPropertyAssembler41GenerateObjectPrototypeHasOwnPropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal38ObjectPrototypeHasOwnPropertyAssembler41GenerateObjectPrototypeHasOwnPropertyImplEv
	.type	_ZN2v88internal38ObjectPrototypeHasOwnPropertyAssembler41GenerateObjectPrototypeHasOwnPropertyImplEv, @function
_ZN2v88internal38ObjectPrototypeHasOwnPropertyAssembler41GenerateObjectPrototypeHasOwnPropertyImplEv:
.LFB21999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-976(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1232(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1104(%rbp), %rbx
	subq	$1320, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -1272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -1312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-848(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-720(%rbp), %rcx
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1280(%rbp), %r10
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r10, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1336(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1296(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	leaq	-1264(%rbp), %r15
	movq	%r12, %rdi
	leaq	-592(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rdi
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1248(%rbp), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -1280(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	leaq	-464(%rbp), %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -1288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-336(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -1320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	-1320(%rbp)
	movq	%r15, %rcx
	movq	-1280(%rbp), %r9
	movq	-1288(%rbp), %r8
	pushq	%r13
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-1272(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler9TryToNameEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1344(%rbp), %r11
	pushq	%r13
	movq	%r12, %rdi
	pushq	%r14
	movq	-1312(%rbp), %r9
	movq	%rax, %r8
	movq	-1304(%rbp), %rcx
	movq	-1296(%rbp), %rsi
	movq	%r11, %rdx
	movq	%r11, -1352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17TryHasOwnPropertyEPNS0_8compiler4NodeES4_S4_S4_PNS2_18CodeAssemblerLabelES6_S6_@PLT
	addq	$32, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdi
	movq	%rax, -1344(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1344(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	movq	-1352(%rbp), %r11
	pushq	%r13
	movq	-1312(%rbp), %r9
	movq	%rax, %r8
	pushq	%r14
	movq	-1304(%rbp), %rcx
	movq	%r11, %rdx
	pushq	%r14
	movq	-1296(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16TryLookupElementEPNS0_8compiler4NodeES4_NS2_11SloppyTNodeINS0_6Int32TEEENS5_INS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESB_SB_SB_@PLT
	movq	-1320(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-208(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1344(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1344(%rbp), %r11
	pushq	%r13
	movq	%r15, %rcx
	movq	-1280(%rbp), %r9
	movq	-1288(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r12, %rdi
	pushq	%r11
	movq	-1272(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler20TryInternalizeStringEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	movq	-1344(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler29IsSpecialReceiverInstanceTypeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1344(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1360(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -1280(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1272(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler8IsNumberENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNameENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1312(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1328(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	-1296(%rbp), %xmm0
	movl	$2, %r8d
	movl	$243, %esi
	movhps	-1272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L108:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21999:
	.size	_ZN2v88internal38ObjectPrototypeHasOwnPropertyAssembler41GenerateObjectPrototypeHasOwnPropertyImplEv, .-_ZN2v88internal38ObjectPrototypeHasOwnPropertyAssembler41GenerateObjectPrototypeHasOwnPropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins38Generate_ObjectPrototypeHasOwnPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"ObjectPrototypeHasOwnProperty"
	.section	.text._ZN2v88internal8Builtins38Generate_ObjectPrototypeHasOwnPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins38Generate_ObjectPrototypeHasOwnPropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins38Generate_ObjectPrototypeHasOwnPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins38Generate_ObjectPrototypeHasOwnPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB21995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$368, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$480, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L113
.L110:
	movq	%r13, %rdi
	call	_ZN2v88internal38ObjectPrototypeHasOwnPropertyAssembler41GenerateObjectPrototypeHasOwnPropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L110
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21995:
	.size	_ZN2v88internal8Builtins38Generate_ObjectPrototypeHasOwnPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins38Generate_ObjectPrototypeHasOwnPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEv
	.type	_ZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEv, @function
_ZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEv:
.LFB22008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler22UintPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	movq	%rax, -320(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rbx
	movq	-272(%rbp), %rdi
	movhps	-312(%rbp), %xmm0
	movq	%r12, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_21ObjectAssignAssembler24GenerateObjectAssignImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	-320(%rbp), %rcx
	leaq	-304(%rbp), %rsi
	movl	$1, %r9d
	movq	%rax, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L116
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L116:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-312(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22008:
	.size	_ZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEv, .-_ZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEv
	.section	.rodata._ZN2v88internal8Builtins21Generate_ObjectAssignEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"ObjectAssign"
	.section	.text._ZN2v88internal8Builtins21Generate_ObjectAssignEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_ObjectAssignEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_ObjectAssignEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_ObjectAssignEPNS0_8compiler18CodeAssemblerStateE:
.LFB22004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$438, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$459, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L127
.L124:
	movq	%r13, %rdi
	call	_ZN2v88internal21ObjectAssignAssembler24GenerateObjectAssignImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L124
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22004:
	.size	_ZN2v88internal8Builtins21Generate_ObjectAssignEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_ObjectAssignEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal19ObjectKeysAssembler22GenerateObjectKeysImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19ObjectKeysAssembler22GenerateObjectKeysImplEv
	.type	_ZN2v88internal19ObjectKeysAssembler22GenerateObjectKeysImplEv, @function
_ZN2v88internal19ObjectKeysAssembler22GenerateObjectKeysImplEv:
.LFB22018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-736(%rbp), %r14
	leaq	-720(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-576(%rbp), %rbx
	subq	$776, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-704(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-192(%rbp), %rcx
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-744(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -744(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$1023, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movl	$1023, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-744(%rbp), %rsi
	leaq	8(%r12), %rdi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17IsEmptyFixedArrayENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-752(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-768(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler28IsEmptySlowElementDictionaryENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-752(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-800(%rbp), %r10
	movq	-792(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	movq	%r11, %rcx
	movq	%r10, -808(%rbp)
	movq	%r11, -768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-768(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -800(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-784(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadMapDescriptorsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	subq	$8, %rsp
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	$32
	movq	-784(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	pushq	$0
	movl	$2, %esi
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler40AllocateUninitializedJSArrayWithElementsENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEENS4_INS0_3SmiEEEPNS3_4NodeESA_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEEi@PLT
	addq	$32, %rsp
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rdx, -792(%rbp)
	movl	$1, %edx
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	pushq	$0
	movl	$2, %ecx
	movq	%r12, %rdi
	pushq	$0
	movq	-792(%rbp), %r8
	movq	%rax, %r9
	movl	$2, %esi
	pushq	$1
	movq	-768(%rbp), %rdx
	pushq	$0
	pushq	%rbx
	pushq	%rbx
	call	_ZN2v88internal17CodeStubAssembler22CopyFixedArrayElementsENS0_12ElementsKindEPNS0_8compiler4NodeES2_S5_S5_S5_S5_NS0_16WriteBarrierModeENS1_13ParameterModeENS1_18HoleConversionModeEPNS3_26TypedCodeAssemblerVariableINS0_5BoolTEEE@PLT
	movq	-784(%rbp), %rsi
	addq	$48, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-808(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -768(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-776(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-744(%rbp), %rax
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movq	-760(%rbp), %rdx
	movl	$1, %r8d
	movl	$245, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-744(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -760(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, -744(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-744(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-760(%rbp), %rsi
	movq	%rax, %rdx
	movl	$32, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_14FixedArrayBaseEEENS3_INS0_3SmiEEEPNS2_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-800(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-768(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L132:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22018:
	.size	_ZN2v88internal19ObjectKeysAssembler22GenerateObjectKeysImplEv, .-_ZN2v88internal19ObjectKeysAssembler22GenerateObjectKeysImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_ObjectKeysEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"ObjectKeys"
	.section	.text._ZN2v88internal8Builtins19Generate_ObjectKeysEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_ObjectKeysEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_ObjectKeysEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_ObjectKeysEPNS0_8compiler18CodeAssemblerStateE:
.LFB22014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$469, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$475, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L137
.L134:
	movq	%r13, %rdi
	call	_ZN2v88internal19ObjectKeysAssembler22GenerateObjectKeysImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L134
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22014:
	.size	_ZN2v88internal8Builtins19Generate_ObjectKeysEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_ObjectKeysEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal34ObjectGetOwnPropertyNamesAssembler37GenerateObjectGetOwnPropertyNamesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34ObjectGetOwnPropertyNamesAssembler37GenerateObjectGetOwnPropertyNamesImplEv
	.type	_ZN2v88internal34ObjectGetOwnPropertyNamesAssembler37GenerateObjectGetOwnPropertyNamesImplEv, @function
_ZN2v88internal34ObjectGetOwnPropertyNamesAssembler37GenerateObjectGetOwnPropertyNamesImplEv:
.LFB22027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-864(%rbp), %r14
	leaq	-848(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$904, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -880(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-832(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r11, -936(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-704(%rbp), %rcx
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rbx, -896(%rbp)
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-576(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-448(%rbp), %rcx
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	movq	%r9, -904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rbx, -872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-888(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -912(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler36IsCustomElementsReceiverInstanceTypeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	leaq	8(%r12), %rdi
	movq	%rbx, -888(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17IsEmptyFixedArrayENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-896(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler28IsEmptySlowElementDictionaryENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-896(%rbp), %rbx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -896(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-912(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$1023, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movl	$1023, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-904(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-912(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$1047552, %ecx
	movl	$10, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler10DecodeWordENS0_8compiler11SloppyTNodeINS0_5WordTEEEjj@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-936(%rbp), %r11
	movq	-928(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r10, %rcx
	movq	%r11, -944(%rbp)
	movq	%r10, -912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-912(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -936(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-920(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadMapDescriptorsENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$1800, %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -912(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	subq	$8, %rsp
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	$32
	movq	-920(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	pushq	$0
	movl	$2, %esi
	pushq	$1
	call	_ZN2v88internal17CodeStubAssembler40AllocateUninitializedJSArrayWithElementsENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEENS4_INS0_3SmiEEEPNS3_4NodeESA_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEEi@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rdx, -928(%rbp)
	movl	$1, %edx
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	pushq	$0
	movl	$2, %ecx
	movq	%r12, %rdi
	pushq	$0
	movq	-928(%rbp), %r8
	movq	%rax, %r9
	movl	$2, %esi
	pushq	$1
	movq	-912(%rbp), %rdx
	pushq	$0
	pushq	%rbx
	pushq	%rbx
	leaq	-64(%rbp), %rbx
	call	_ZN2v88internal17CodeStubAssembler22CopyFixedArrayElementsENS0_12ElementsKindEPNS0_8compiler4NodeES2_S5_S5_S5_S5_NS0_16WriteBarrierModeENS1_13ParameterModeENS1_18HoleConversionModeEPNS3_26TypedCodeAssemblerVariableINS0_5BoolTEEE@PLT
	movq	-920(%rbp), %rsi
	addq	$48, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rcx
	movl	$242, %esi
	movq	%r12, %rdi
	movq	-888(%rbp), %rax
	movq	-880(%rbp), %rdx
	movl	$1, %r8d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -912(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-912(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-944(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -912(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rcx
	movl	$241, %esi
	movq	%r12, %rdi
	movq	-888(%rbp), %rax
	movq	-880(%rbp), %rdx
	movl	$1, %r8d
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-872(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -872(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, -880(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-880(%rbp), %rcx
	movq	%rax, %rdx
	movl	$32, %r9d
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_14FixedArrayBaseEEENS3_INS0_3SmiEEEPNS2_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-936(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L142:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22027:
	.size	_ZN2v88internal34ObjectGetOwnPropertyNamesAssembler37GenerateObjectGetOwnPropertyNamesImplEv, .-_ZN2v88internal34ObjectGetOwnPropertyNamesAssembler37GenerateObjectGetOwnPropertyNamesImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_ObjectGetOwnPropertyNamesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"ObjectGetOwnPropertyNames"
	.section	.text._ZN2v88internal8Builtins34Generate_ObjectGetOwnPropertyNamesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_ObjectGetOwnPropertyNamesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_ObjectGetOwnPropertyNamesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_ObjectGetOwnPropertyNamesEPNS0_8compiler18CodeAssemblerStateE:
.LFB22023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$554, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$470, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L147
.L144:
	movq	%r13, %rdi
	call	_ZN2v88internal34ObjectGetOwnPropertyNamesAssembler37GenerateObjectGetOwnPropertyNamesImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L144
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22023:
	.size	_ZN2v88internal8Builtins34Generate_ObjectGetOwnPropertyNamesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_ObjectGetOwnPropertyNamesEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins21Generate_ObjectValuesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"ObjectValues"
	.section	.text._ZN2v88internal8Builtins21Generate_ObjectValuesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_ObjectValuesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_ObjectValuesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_ObjectValuesEPNS0_8compiler18CodeAssemblerStateE:
.LFB22035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$657, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$488, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L153
.L150:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L150
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22035:
	.size	_ZN2v88internal8Builtins21Generate_ObjectValuesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_ObjectValuesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21ObjectValuesAssembler24GenerateObjectValuesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ObjectValuesAssembler24GenerateObjectValuesImplEv
	.type	_ZN2v88internal21ObjectValuesAssembler24GenerateObjectValuesImplEv, @function
_ZN2v88internal21ObjectValuesAssembler24GenerateObjectValuesImplEv:
.LFB22039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE
	.cfi_endproc
.LFE22039:
	.size	_ZN2v88internal21ObjectValuesAssembler24GenerateObjectValuesImplEv, .-_ZN2v88internal21ObjectValuesAssembler24GenerateObjectValuesImplEv
	.section	.rodata._ZN2v88internal8Builtins22Generate_ObjectEntriesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"ObjectEntries"
	.section	.text._ZN2v88internal8Builtins22Generate_ObjectEntriesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_ObjectEntriesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_ObjectEntriesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_ObjectEntriesEPNS0_8compiler18CodeAssemblerStateE:
.LFB22044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$665, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$466, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L161
.L158:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L158
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22044:
	.size	_ZN2v88internal8Builtins22Generate_ObjectEntriesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_ObjectEntriesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal22ObjectEntriesAssembler25GenerateObjectEntriesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ObjectEntriesAssembler25GenerateObjectEntriesImplEv
	.type	_ZN2v88internal22ObjectEntriesAssembler25GenerateObjectEntriesImplEv, @function
_ZN2v88internal22ObjectEntriesAssembler25GenerateObjectEntriesImplEv:
.LFB22048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal36ObjectEntriesValuesBuiltinsAssembler21GetOwnValuesOrEntriesENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_11CollectTypeE
	.cfi_endproc
.LFE22048:
	.size	_ZN2v88internal22ObjectEntriesAssembler25GenerateObjectEntriesImplEv, .-_ZN2v88internal22ObjectEntriesAssembler25GenerateObjectEntriesImplEv
	.section	.text._ZN2v88internal37ObjectPrototypeIsPrototypeOfAssembler40GenerateObjectPrototypeIsPrototypeOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37ObjectPrototypeIsPrototypeOfAssembler40GenerateObjectPrototypeIsPrototypeOfImplEv
	.type	_ZN2v88internal37ObjectPrototypeIsPrototypeOfAssembler40GenerateObjectPrototypeIsPrototypeOfImplEv, @function
_ZN2v88internal37ObjectPrototypeIsPrototypeOfAssembler40GenerateObjectPrototypeIsPrototypeOfImplEv:
.LFB22057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-192(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-328(%rbp), %r9
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler19HasInPrototypeChainEPNS0_8compiler4NodeES4_NS2_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSReceiverENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-328(%rbp), %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler8ToObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22057:
	.size	_ZN2v88internal37ObjectPrototypeIsPrototypeOfAssembler40GenerateObjectPrototypeIsPrototypeOfImplEv, .-_ZN2v88internal37ObjectPrototypeIsPrototypeOfAssembler40GenerateObjectPrototypeIsPrototypeOfImplEv
	.section	.rodata._ZN2v88internal8Builtins37Generate_ObjectPrototypeIsPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"ObjectPrototypeIsPrototypeOf"
	.section	.text._ZN2v88internal8Builtins37Generate_ObjectPrototypeIsPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_ObjectPrototypeIsPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_ObjectPrototypeIsPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_ObjectPrototypeIsPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$674, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$481, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L173
.L170:
	movq	%r13, %rdi
	call	_ZN2v88internal37ObjectPrototypeIsPrototypeOfAssembler40GenerateObjectPrototypeIsPrototypeOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L170
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22053:
	.size	_ZN2v88internal8Builtins37Generate_ObjectPrototypeIsPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_ObjectPrototypeIsPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32ObjectPrototypeToStringAssembler35GenerateObjectPrototypeToStringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32ObjectPrototypeToStringAssembler35GenerateObjectPrototypeToStringImplEv
	.type	_ZN2v88internal32ObjectPrototypeToStringAssembler35GenerateObjectPrototypeToStringImplEv, @function
_ZN2v88internal32ObjectPrototypeToStringAssembler35GenerateObjectPrototypeToStringImplEv:
.LFB22066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$487, %edx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%rbx, %r9
	movl	$1, %edi
	movq	%rax, %r8
	movl	$1, %ecx
	movq	%r13, -48(%rbp)
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22066:
	.size	_ZN2v88internal32ObjectPrototypeToStringAssembler35GenerateObjectPrototypeToStringImplEv, .-_ZN2v88internal32ObjectPrototypeToStringAssembler35GenerateObjectPrototypeToStringImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_ObjectPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"ObjectPrototypeToString"
	.section	.text._ZN2v88internal8Builtins32Generate_ObjectPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_ObjectPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_ObjectPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_ObjectPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB22062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$720, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$478, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L183
.L180:
	movq	%r13, %rdi
	call	_ZN2v88internal32ObjectPrototypeToStringAssembler35GenerateObjectPrototypeToStringImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L180
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22062:
	.size	_ZN2v88internal8Builtins32Generate_ObjectPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_ObjectPrototypeToStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23ObjectToStringAssembler26GenerateObjectToStringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectToStringAssembler26GenerateObjectToStringImplEv
	.type	_ZN2v88internal23ObjectToStringAssembler26GenerateObjectToStringImplEv, @function
_ZN2v88internal23ObjectToStringAssembler26GenerateObjectToStringImplEv:
.LFB22075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3152(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2512(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-3024(%rbp), %rbx
	subq	$3480, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-3280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r14, -3496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, -3440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-2896(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -3464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-2768(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -3416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-2640(%rbp), %rcx
	movq	%rcx, %r15
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r15, -3432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-2384(%rbp), %rcx
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rbx, -3368(%rbp)
	leaq	-2256(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, -3384(%rbp)
	leaq	-3312(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	leaq	-2128(%rbp), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -3392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-2000(%rbp), %rdx
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -3408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1872(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r9, -3376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-1744(%rbp), %rdx
	movq	%rdx, %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r15, -3456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-1616(%rbp), %rdx
	movq	%rdx, %r15
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r15, -3400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1488(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r10
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -3424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1360(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r8, %r10
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r10, %rdi
	movq	%r10, -3448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1232(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r15
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r15, -3472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -3328(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-3296(%rbp), %rsi
	movq	%r15, %rcx
	movl	$8, %edx
	movq	%rsi, %r10
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -3336(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-3384(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -3344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23IsPrimitiveInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-3408(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%eax, %eax
	movl	$1066, %r8d
	leaq	-256(%rbp), %rdi
	movl	$23, %ecx
	movl	$1061, %edx
	movl	$1075, %esi
	movl	$1056, %r10d
	rep stosq
	movw	%r8w, -176(%rbp)
	movq	-3456(%rbp), %r8
	movl	$1057, %eax
	movl	$1105, %ecx
	movl	$1104, %r9d
	movq	%r14, -120(%rbp)
	movl	$1040, %r11d
	movw	%r10w, -144(%rbp)
	movq	-3464(%rbp), %r10
	movl	$1058, %edi
	movq	%r8, -200(%rbp)
	movq	-3432(%rbp), %r8
	movw	%ax, -256(%rbp)
	movl	$1024, %eax
	movw	%cx, -224(%rbp)
	movq	-3368(%rbp), %rcx
	movw	%r9w, -160(%rbp)
	movq	-3376(%rbp), %r9
	movw	%r11w, -128(%rbp)
	movq	-3392(%rbp), %r11
	movw	%ax, -112(%rbp)
	movl	$1067, %eax
	movq	%r10, -232(%rbp)
	movq	-3440(%rbp), %r10
	movq	%r8, -168(%rbp)
	movq	-3448(%rbp), %r8
	movw	%ax, -96(%rbp)
	movl	$1041, %eax
	movq	%r11, -248(%rbp)
	movw	%ax, -80(%rbp)
	movw	%dx, -240(%rbp)
	movq	%r11, %rdx
	movw	%si, -208(%rbp)
	movw	%di, -192(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -216(%rbp)
	movq	%r10, -184(%rbp)
	movq	%rcx, -152(%rbp)
	leaq	-400(%rbp), %rcx
	movq	%r14, -136(%rbp)
	movq	%r9, -104(%rbp)
	movq	%r13, -88(%rbp)
	movq	%r8, -72(%rbp)
	movdqu	-136(%rbp), %xmm7
	movdqu	-152(%rbp), %xmm0
	movdqu	-168(%rbp), %xmm6
	movq	%r8, -264(%rbp)
	movabsq	$4398046512144, %r8
	movdqu	-184(%rbp), %xmm1
	movdqu	-200(%rbp), %xmm5
	movq	%r8, -368(%rbp)
	movabsq	$4471060956203, %r8
	punpcklqdq	%xmm7, %xmm0
	movdqu	-216(%rbp), %xmm2
	movdqu	-248(%rbp), %xmm3
	movq	%r14, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	movdqa	-256(%rbp), %xmm0
	punpcklqdq	%xmm6, %xmm1
	movaps	%xmm1, -320(%rbp)
	punpcklqdq	%xmm5, %xmm2
	movdqu	-232(%rbp), %xmm5
	movdqa	%xmm0, %xmm1
	punpcklwd	-240(%rbp), %xmm0
	punpckhwd	-240(%rbp), %xmm1
	movaps	%xmm2, -336(%rbp)
	punpcklqdq	%xmm5, %xmm3
	movq	%r9, -280(%rbp)
	movl	$12, %r9d
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	movaps	%xmm3, -352(%rbp)
	punpckhwd	%xmm1, %xmm2
	movdqa	-224(%rbp), %xmm1
	movq	%r13, -3480(%rbp)
	punpcklwd	%xmm2, %xmm0
	movq	%r13, -272(%rbp)
	leaq	-592(%rbp), %r13
	movdqa	%xmm1, %xmm2
	punpcklwd	-208(%rbp), %xmm1
	punpckhwd	-208(%rbp), %xmm2
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm2, %xmm1
	punpckhwd	%xmm2, %xmm3
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movdqa	-192(%rbp), %xmm1
	movdqa	%xmm1, %xmm2
	punpcklwd	-176(%rbp), %xmm1
	punpckhwd	-176(%rbp), %xmm2
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm2, %xmm1
	punpckhwd	%xmm2, %xmm3
	movdqa	-160(%rbp), %xmm2
	punpcklwd	%xmm3, %xmm1
	movdqa	%xmm2, %xmm3
	punpcklwd	-144(%rbp), %xmm2
	punpckhwd	-144(%rbp), %xmm3
	movdqa	%xmm2, %xmm4
	punpcklwd	%xmm3, %xmm2
	punpckhwd	%xmm3, %xmm4
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm4, %xmm2
	punpcklwd	%xmm2, %xmm1
	punpckhwd	%xmm2, %xmm3
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm0
	punpcklwd	%xmm1, %xmm2
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm2, -400(%rbp)
	movq	%r8, -360(%rbp)
	movq	-3344(%rbp), %r10
	leaq	-352(%rbp), %r8
	movq	%r10, %rsi
	movq	%r10, -3504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r14
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3952(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -3352(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-432(%rbp), %r11
	xorl	%esi, %esi
	movl	$2, %edi
	pushq	%rdi
	movq	-3328(%rbp), %r9
	movq	%rax, %r8
	movq	%r15, %xmm0
	pushq	%r11
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-576(%rbp), %rax
	movq	%r14, -720(%rbp)
	leaq	-720(%rbp), %r14
	movhps	-3352(%rbp), %xmm0
	movq	%r14, %rdx
	movq	%r11, -3352(%rbp)
	movaps	%xmm0, -432(%rbp)
	movq	%r15, -3344(%rbp)
	leaq	-848(%rbp), %r15
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3344(%rbp), %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	-3328(%rbp), %rdx
	movl	$207, %esi
	movq	%rcx, -432(%rbp)
	movq	-3352(%rbp), %rcx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler25ArgumentsToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21ArrayToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$30, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23BooleanToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3488(%rbp), %r8
	movq	-3336(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20DateToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21ErrorToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24FunctionToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$107, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22NumberToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3488(%rbp), %r8
	movq	-3336(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22ObjectToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3504(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -3488(%rbp)
	call	_ZN2v88internal17CodeStubAssembler20IsStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-3400(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3488(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler20IsBigIntInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-3472(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsBooleanMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-3416(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsHeapNumberMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-3384(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsSymbolMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-3424(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20NullToStringConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler25UndefinedToStringConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3352(%rbp), %rcx
	movl	$4, %esi
	movq	-3328(%rbp), %rdx
	movq	-3344(%rbp), %rax
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %xmm6
	movq	%r12, %rdi
	movq	-3360(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, -720(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -592(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6IsTrueENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation(%rip), %rdx
	movq	%r12, %rdi
	leaq	-464(%rbp), %r9
	movq	%rax, %rsi
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rdx, %xmm0
	movq	-3352(%rbp), %rcx
	movq	%rax, %xmm7
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6StringEZNS2_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS6_26GenerateObjectToStringImplEvEUlvE0_EENS2_8compiler5TNodeIT_EENS9_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSO_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6StringEZNS1_23ObjectToStringAssembler26GenerateObjectToStringImplEvEUlvE_ZNS9_26GenerateObjectToStringImplEvEUlvE0_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movl	$7, %r8d
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, %xmm5
	movq	%r13, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movq	%rdx, %xmm0
	movq	%r9, %rdx
	punpcklqdq	%xmm5, %xmm0
	movq	%r14, -464(%rbp)
	movq	%r9, -3488(%rbp)
	movaps	%xmm0, -448(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, -3360(%rbp)
	movq	-448(%rbp), %rax
	testq	%rax, %rax
	je	.L186
	movq	-3488(%rbp), %r9
	movl	$3, %edx
	movq	%r9, %rsi
	movq	%r9, %rdi
	call	*%rax
.L186:
	movq	-416(%rbp), %rax
	testq	%rax, %rax
	je	.L187
	movq	-3352(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L187:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r12, %rdi
	leaq	3952(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-3352(%rbp), %rsi
	movl	$2, %edi
	movq	%r14, %rdx
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	movq	-3328(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-3344(%rbp), %xmm0
	movq	%rax, -720(%rbp)
	movq	-576(%rbp), %rax
	movhps	-3488(%rbp), %xmm0
	movaps	%xmm0, -432(%rbp)
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-3456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22RegexpToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$178, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22StringToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3360(%rbp), %r8
	movq	-3336(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$180, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ObjectToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3360(%rbp), %r8
	movq	-3336(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$27, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ObjectToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3360(%rbp), %r8
	movq	-3336(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-1104(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -3488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-976(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r9, -3504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler27LoadJSPrimitiveWrapperValueEPNS0_8compiler4NodeE@PLT
	movq	-3344(%rbp), %rsi
	movq	-3336(%rbp), %rdi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-3488(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15IsHeapNumberMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-3488(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler12IsBooleanMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-3504(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler11IsSymbolMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20IsBigIntInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3488(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22NumberToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3504(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23BooleanToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StringToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22ObjectToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22ObjectToStringConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3360(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -3520(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3488(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -3512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-3336(%rbp), %rax
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	%rax, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-3336(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -3504(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$268435456, %esi
	movq	%r12, %rdi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3488(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rcx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-3360(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3504(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-3336(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler25ToStringTagSymbolConstantEv@PLT
	movq	-3344(%rbp), %rdx
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8ToObjectENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -3344(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-3520(%rbp), %r9
	movl	$710, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-976(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	-3352(%rbp), %rsi
	movq	-3512(%rbp), %r10
	movq	%rax, %r8
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	movq	-3328(%rbp), %r9
	movq	%r10, %rdx
	xorl	%esi, %esi
	movq	-3344(%rbp), %xmm0
	movq	%rax, -1104(%rbp)
	movq	%r12, %rdi
	movq	-960(%rbp), %rax
	movhps	-3360(%rbp), %xmm0
	movaps	%xmm0, -432(%rbp)
	movq	%rax, -1096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -3344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3344(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3344(%rbp), %r8
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-3472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3400(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3376(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3392(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3384(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L196:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22075:
	.size	_ZN2v88internal23ObjectToStringAssembler26GenerateObjectToStringImplEv, .-_ZN2v88internal23ObjectToStringAssembler26GenerateObjectToStringImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_ObjectToStringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"ObjectToString"
	.section	.text._ZN2v88internal8Builtins23Generate_ObjectToStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_ObjectToStringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_ObjectToStringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_ObjectToStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB22071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$726, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$487, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L201
.L198:
	movq	%r13, %rdi
	call	_ZN2v88internal23ObjectToStringAssembler26GenerateObjectToStringImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L198
.L202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22071:
	.size	_ZN2v88internal8Builtins23Generate_ObjectToStringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_ObjectToStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins31Generate_ObjectPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"ObjectPrototypeValueOf"
	.section	.text._ZN2v88internal8Builtins31Generate_ObjectPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_ObjectPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_ObjectPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_ObjectPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1041, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$479, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L207
.L204:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L204
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22084:
	.size	_ZN2v88internal8Builtins31Generate_ObjectPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_ObjectPrototypeValueOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31ObjectPrototypeValueOfAssembler34GenerateObjectPrototypeValueOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31ObjectPrototypeValueOfAssembler34GenerateObjectPrototypeValueOfImplEv
	.type	_ZN2v88internal31ObjectPrototypeValueOfAssembler34GenerateObjectPrototypeValueOfImplEv, @function
_ZN2v88internal31ObjectPrototypeValueOfAssembler34GenerateObjectPrototypeValueOfImplEv:
.LFB22088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22088:
	.size	_ZN2v88internal31ObjectPrototypeValueOfAssembler34GenerateObjectPrototypeValueOfImplEv, .-_ZN2v88internal31ObjectPrototypeValueOfAssembler34GenerateObjectPrototypeValueOfImplEv
	.section	.text._ZN2v88internal38CreateObjectWithoutPropertiesAssembler41GenerateCreateObjectWithoutPropertiesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal38CreateObjectWithoutPropertiesAssembler41GenerateCreateObjectWithoutPropertiesImplEv
	.type	_ZN2v88internal38CreateObjectWithoutPropertiesAssembler41GenerateCreateObjectWithoutPropertiesImplEv, @function
_ZN2v88internal38CreateObjectWithoutPropertiesAssembler41GenerateCreateObjectWithoutPropertiesImplEv:
.LFB22097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-608(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-96(%rbp), %rbx
	subq	$680, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-352(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L228
.L212:
	movq	-664(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-688(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-680(%rbp), %rdx
	leaq	-640(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler18BranchIfJSReceiverENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-624(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L229
.L214:
	movq	-696(%rbp), %rsi
	movl	$150, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22AllocateNameDictionaryEi@PLT
	movq	-672(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L230
.L216:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	-672(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-696(%rbp), %rsi
	movl	$108, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L231
.L218:
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20LoadMapPrototypeInfoENS0_8compiler11SloppyTNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movl	$40, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-696(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler19IsStrongReferenceToENS0_8compiler5TNodeINS0_11MaybeObjectEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-696(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler23GetHeapObjectAssumeWeakENS0_8compiler5TNodeINS0_11MaybeObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L232
.L220:
	movq	-672(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -696(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movq	-696(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L233
.L222:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-664(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	-704(%rbp), %rdx
	movl	$238, %esi
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rbx, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$680, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	leaq	-224(%rbp), %r15
	xorl	%edx, %edx
	leaq	-80(%rbp), %r14
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, -96(%rbp)
	movq	$25, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movdqa	.LC16(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8104637026473570928, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movb	$101, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L212
	call	_ZdlPv@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	-80(%rbp), %rcx
	leaq	-648(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -696(%rbp)
	movq	$47, -648(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-648(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movdqa	.LC20(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$7310032045075098741, %rcx
	movq	%rdx, -80(%rbp)
	movl	$29285, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC21(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movl	$1986618723, 40(%rax)
	movw	%dx, 44(%rax)
	movb	$41, 46(%rax)
	movups	%xmm0, 16(%rax)
	movq	-648(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-696(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L222
	call	_ZdlPv@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	-80(%rbp), %rax
	movl	$24941, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, -96(%rbp)
	movq	%rax, -696(%rbp)
	movabsq	$7598819836125474377, %rax
	movq	%rax, -80(%rbp)
	movl	$543519841, -72(%rbp)
	movw	%cx, -68(%rbp)
	movb	$112, -66(%rbp)
	movq	$15, -88(%rbp)
	movb	$0, -65(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-696(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L220
	call	_ZdlPv@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	-80(%rbp), %rcx
	leaq	-648(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -696(%rbp)
	movq	$30, -648(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-648(%rbp), %rdx
	movl	$28518, %esi
	movq	%r12, %rdi
	movdqa	.LC19(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8104637026473570928, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movw	%si, 28(%rax)
	movq	%rbx, %rsi
	movl	$1852383333, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-648(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-696(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L218
	call	_ZdlPv@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	-80(%rbp), %rcx
	leaq	-648(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -712(%rbp)
	movq	$23, -648(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-648(%rbp), %rdx
	movl	$25974, %edi
	movq	%rbx, %rsi
	movdqa	.LC18(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movw	%di, 20(%rax)
	movq	%r12, %rdi
	movl	$1768252261, 16(%rax)
	movb	$114, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-648(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-712(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L216
	call	_ZdlPv@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	-80(%rbp), %rcx
	leaq	-648(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -712(%rbp)
	movq	$17, -648(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-648(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movdqa	.LC17(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movb	$108, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-648(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-712(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L214
	call	_ZdlPv@PLT
	jmp	.L214
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22097:
	.size	_ZN2v88internal38CreateObjectWithoutPropertiesAssembler41GenerateCreateObjectWithoutPropertiesImplEv, .-_ZN2v88internal38CreateObjectWithoutPropertiesAssembler41GenerateCreateObjectWithoutPropertiesImplEv
	.section	.rodata._ZN2v88internal8Builtins38Generate_CreateObjectWithoutPropertiesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"CreateObjectWithoutProperties"
	.section	.text._ZN2v88internal8Builtins38Generate_CreateObjectWithoutPropertiesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins38Generate_CreateObjectWithoutPropertiesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins38Generate_CreateObjectWithoutPropertiesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins38Generate_CreateObjectWithoutPropertiesEPNS0_8compiler18CodeAssemblerStateE:
.LFB22093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1049, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$461, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L239
.L236:
	movq	%r13, %rdi
	call	_ZN2v88internal38CreateObjectWithoutPropertiesAssembler41GenerateCreateObjectWithoutPropertiesImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L236
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22093:
	.size	_ZN2v88internal8Builtins38Generate_CreateObjectWithoutPropertiesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins38Generate_CreateObjectWithoutPropertiesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21ObjectCreateAssembler24GenerateObjectCreateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ObjectCreateAssembler24GenerateObjectCreateImplEv
	.type	_ZN2v88internal21ObjectCreateAssembler24GenerateObjectCreateImplEv, @function
_ZN2v88internal21ObjectCreateAssembler24GenerateObjectCreateImplEv:
.LFB22106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-912(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-864(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1016, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r15, -1040(%rbp)
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	-912(%rbp), %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	-912(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -1000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-736(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-608(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L250
	leaq	-96(%rbp), %rax
	movq	%rax, -1024(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -968(%rbp)
.L242:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1032(%rbp), %r14
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18BranchIfJSReceiverENS0_8compiler11SloppyTNodeINS0_6ObjectEEEPNS2_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L252
.L244:
	movq	-992(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1016(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -992(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler20IsSpecialReceiverMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	-992(%rbp), %rsi
	leaq	8(%r12), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapBitField3ENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$2097152, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$1047552, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-944(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1016(%rbp), %r15
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-352(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-928(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -976(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-480(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -1008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-968(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1008(%rbp), %rcx
	movq	-968(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-968(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1000(%rbp), %rsi
	movl	$150, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22AllocateNameDictionaryEi@PLT
	movq	-976(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	-976(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1000(%rbp), %rsi
	movl	$108, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20LoadMapPrototypeInfoENS0_8compiler11SloppyTNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %r8
	jne	.L253
.L246:
	movq	%rbx, %xmm0
	movq	%r8, %rsi
	movl	$1800, %ecx
	movq	%r12, %rdi
	movhps	-992(%rbp), %xmm0
	movl	$40, %edx
	movaps	%xmm0, -992(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler19IsStrongReferenceToENS0_8compiler5TNodeINS0_11MaybeObjectEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23GetHeapObjectAssumeWeakENS0_8compiler5TNodeINS0_11MaybeObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-976(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	-1040(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-968(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-976(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movdqa	-992(%rbp), %xmm0
	movl	$238, %esi
	movq	-1024(%rbp), %rcx
	movq	-1000(%rbp), %rdx
	movl	$2, %r8d
	movq	%r12, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-1016(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	addq	$1016, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	leaq	-224(%rbp), %rax
	xorl	%edx, %edx
	movq	$27, -224(%rbp)
	movq	%rax, %rsi
	movq	%r15, %rdi
	leaq	-80(%rbp), %r14
	movq	%r15, -1024(%rbp)
	movq	%r14, -96(%rbp)
	movq	%rax, -968(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC23(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8390052652824666170, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$28793, %ecx
	movw	%cx, 24(%rax)
	movb	$101, 26(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L242
	call	_ZdlPv@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L253:
	movq	-1024(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	leaq	-952(%rbp), %rsi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -1048(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$39, -952(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-952(%rbp), %rdx
	movdqa	.LC25(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-1024(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$26222, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC26(%rip), %xmm0
	movl	$1231384697, 32(%rax)
	movw	%dx, 36(%rax)
	movb	$111, 38(%rax)
	movups	%xmm0, 16(%rax)
	movq	-952(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-1048(%rbp), %rcx
	movq	-1056(%rbp), %r8
	cmpq	%rcx, %rdi
	je	.L246
	movq	%r8, -1048(%rbp)
	call	_ZdlPv@PLT
	movq	-1048(%rbp), %r8
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L252:
	movq	-1024(%rbp), %r15
	movq	-968(%rbp), %rsi
	xorl	%edx, %edx
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	$28, -224(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC24(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8243118316935192634, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$1936025972, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L244
	call	_ZdlPv@PLT
	jmp	.L244
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22106:
	.size	_ZN2v88internal21ObjectCreateAssembler24GenerateObjectCreateImplEv, .-_ZN2v88internal21ObjectCreateAssembler24GenerateObjectCreateImplEv
	.section	.rodata._ZN2v88internal8Builtins21Generate_ObjectCreateEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"ObjectCreate"
	.section	.text._ZN2v88internal8Builtins21Generate_ObjectCreateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_ObjectCreateEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_ObjectCreateEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_ObjectCreateEPNS0_8compiler18CodeAssemblerStateE:
.LFB22102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1113, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$460, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L259
.L256:
	movq	%r13, %rdi
	call	_ZN2v88internal21ObjectCreateAssembler24GenerateObjectCreateImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L256
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22102:
	.size	_ZN2v88internal8Builtins21Generate_ObjectCreateEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_ObjectCreateEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17ObjectIsAssembler20GenerateObjectIsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17ObjectIsAssembler20GenerateObjectIsImplEv
	.type	_ZN2v88internal17ObjectIsAssembler20GenerateObjectIsImplEv, @function
_ZN2v88internal17ObjectIsAssembler20GenerateObjectIsImplEv:
.LFB22115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movl	$1, %r9d
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17BranchIfSameValueENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_18CodeAssemblerLabelES7_NS1_13SameValueModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L264
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L264:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22115:
	.size	_ZN2v88internal17ObjectIsAssembler20GenerateObjectIsImplEv, .-_ZN2v88internal17ObjectIsAssembler20GenerateObjectIsImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_ObjectIsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"ObjectIs"
	.section	.text._ZN2v88internal8Builtins17Generate_ObjectIsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_ObjectIsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_ObjectIsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_ObjectIsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1209, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$472, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L269
.L266:
	movq	%r13, %rdi
	call	_ZN2v88internal17ObjectIsAssembler20GenerateObjectIsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L266
.L270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22111:
	.size	_ZN2v88internal8Builtins17Generate_ObjectIsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_ObjectIsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal31CreateIterResultObjectAssembler34GenerateCreateIterResultObjectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31CreateIterResultObjectAssembler34GenerateCreateIterResultObjectImplEv
	.type	_ZN2v88internal31CreateIterResultObjectAssembler34GenerateCreateIterResultObjectImplEv, @function
_ZN2v88internal31CreateIterResultObjectAssembler34GenerateCreateIterResultObjectImplEv:
.LFB22124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$80, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1, %r9d
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %r13
	movq	%rax, %rsi
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rcx
	movl	$8, %r8d
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22124:
	.size	_ZN2v88internal31CreateIterResultObjectAssembler34GenerateCreateIterResultObjectImplEv, .-_ZN2v88internal31CreateIterResultObjectAssembler34GenerateCreateIterResultObjectImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_CreateIterResultObjectEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"CreateIterResultObject"
	.section	.text._ZN2v88internal8Builtins31Generate_CreateIterResultObjectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_CreateIterResultObjectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_CreateIterResultObjectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_CreateIterResultObjectEPNS0_8compiler18CodeAssemblerStateE:
.LFB22120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1223, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$350, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L277
.L274:
	movq	%r13, %rdi
	call	_ZN2v88internal31CreateIterResultObjectAssembler34GenerateCreateIterResultObjectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L274
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22120:
	.size	_ZN2v88internal8Builtins31Generate_CreateIterResultObjectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_CreateIterResultObjectEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins20Generate_HasPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"HasProperty"
	.section	.text._ZN2v88internal8Builtins20Generate_HasPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_HasPropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_HasPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_HasPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1240, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC30(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$159, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L283
.L280:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11HasPropertyENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_NS1_21HasPropertyLookupModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L280
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22129:
	.size	_ZN2v88internal8Builtins20Generate_HasPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_HasPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20HasPropertyAssembler23GenerateHasPropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20HasPropertyAssembler23GenerateHasPropertyImplEv
	.type	_ZN2v88internal20HasPropertyAssembler23GenerateHasPropertyImplEv, @function
_ZN2v88internal20HasPropertyAssembler23GenerateHasPropertyImplEv:
.LFB22133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	call	_ZN2v88internal17CodeStubAssembler11HasPropertyENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_NS1_21HasPropertyLookupModeE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22133:
	.size	_ZN2v88internal20HasPropertyAssembler23GenerateHasPropertyImplEv, .-_ZN2v88internal20HasPropertyAssembler23GenerateHasPropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_InstanceOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"InstanceOf"
	.section	.text._ZN2v88internal8Builtins19Generate_InstanceOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_InstanceOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_InstanceOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_InstanceOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1248, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$490, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L291
.L288:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler10InstanceOfEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L288
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22138:
	.size	_ZN2v88internal8Builtins19Generate_InstanceOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_InstanceOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal19InstanceOfAssembler22GenerateInstanceOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19InstanceOfAssembler22GenerateInstanceOfImplEv
	.type	_ZN2v88internal19InstanceOfAssembler22GenerateInstanceOfImplEv, @function
_ZN2v88internal19InstanceOfAssembler22GenerateInstanceOfImplEv:
.LFB22142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler10InstanceOfEPNS0_8compiler4NodeES4_S4_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22142:
	.size	_ZN2v88internal19InstanceOfAssembler22GenerateInstanceOfImplEv, .-_ZN2v88internal19InstanceOfAssembler22GenerateInstanceOfImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_OrdinaryHasInstanceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"OrdinaryHasInstance"
	.section	.text._ZN2v88internal8Builtins28Generate_OrdinaryHasInstanceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_OrdinaryHasInstanceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_OrdinaryHasInstanceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_OrdinaryHasInstanceEPNS0_8compiler18CodeAssemblerStateE:
.LFB22147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1257, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$489, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L299
.L296:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19OrdinaryHasInstanceEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L300
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L296
.L300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22147:
	.size	_ZN2v88internal8Builtins28Generate_OrdinaryHasInstanceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_OrdinaryHasInstanceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28OrdinaryHasInstanceAssembler31GenerateOrdinaryHasInstanceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrdinaryHasInstanceAssembler31GenerateOrdinaryHasInstanceImplEv
	.type	_ZN2v88internal28OrdinaryHasInstanceAssembler31GenerateOrdinaryHasInstanceImplEv, @function
_ZN2v88internal28OrdinaryHasInstanceAssembler31GenerateOrdinaryHasInstanceImplEv:
.LFB22151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19OrdinaryHasInstanceEPNS0_8compiler4NodeES4_S4_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22151:
	.size	_ZN2v88internal28OrdinaryHasInstanceAssembler31GenerateOrdinaryHasInstanceImplEv, .-_ZN2v88internal28OrdinaryHasInstanceAssembler31GenerateOrdinaryHasInstanceImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_GetSuperConstructorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"GetSuperConstructor"
	.section	.text._ZN2v88internal8Builtins28Generate_GetSuperConstructorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_GetSuperConstructorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_GetSuperConstructorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_GetSuperConstructorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1265, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC33(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$110, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L307
.L304:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19GetSuperConstructorENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L304
.L308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22156:
	.size	_ZN2v88internal8Builtins28Generate_GetSuperConstructorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_GetSuperConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28GetSuperConstructorAssembler31GenerateGetSuperConstructorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28GetSuperConstructorAssembler31GenerateGetSuperConstructorImplEv
	.type	_ZN2v88internal28GetSuperConstructorAssembler31GenerateGetSuperConstructorImplEv, @function
_ZN2v88internal28GetSuperConstructorAssembler31GenerateGetSuperConstructorImplEv:
.LFB22160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19GetSuperConstructorENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE22160:
	.size	_ZN2v88internal28GetSuperConstructorAssembler31GenerateGetSuperConstructorImplEv, .-_ZN2v88internal28GetSuperConstructorAssembler31GenerateGetSuperConstructorImplEv
	.section	.text._ZN2v88internal30CreateGeneratorObjectAssembler33GenerateCreateGeneratorObjectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30CreateGeneratorObjectAssembler33GenerateCreateGeneratorObjectImplEv
	.type	_ZN2v88internal30CreateGeneratorObjectAssembler33GenerateCreateGeneratorObjectImplEv, @function
_ZN2v88internal30CreateGeneratorObjectAssembler33GenerateCreateGeneratorObjectImplEv:
.LFB22169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler30IsFunctionWithPrototypeSlotMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$56, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$68, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22DoesntHaveInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler35LoadSharedFunctionInfoBytecodeArrayENS0_8compiler11SloppyTNodeINS0_18SharedFunctionInfoEEE@PLT
	movl	$771, %ecx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$42, %edx
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-360(%rbp), %r8
	movl	$516, %ecx
	movq	%r12, %rdi
	movl	$40, %edx
	movq	%rax, %r15
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordSarENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rax, %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	subq	$8, %rsp
	movq	%r15, %r8
	movq	%r12, %rdi
	pushq	$1
	movq	%rax, %rcx
	movl	$4, %r9d
	movl	$3, %esi
	movq	-360(%rbp), %r11
	movq	%r11, %rdx
	movq	%r11, -368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23FillFixedArrayWithValueENS0_12ElementsKindEPNS0_8compiler4NodeES5_S5_NS0_9RootIndexENS1_13ParameterModeE@PLT
	movq	-376(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	movq	%rbx, %rcx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$8, %r8d
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$32, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-344(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$40, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-352(%rbp), %rcx
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$72, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-368(%rbp), %r11
	movl	$7, %r8d
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$56, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$-2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$64, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-360(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$1064, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$88, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-344(%rbp), %rdx
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %rcx
	movhps	-352(%rbp), %xmm0
	movl	$2, %r8d
	movl	$111, %esi
	movq	%r12, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L314:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22169:
	.size	_ZN2v88internal30CreateGeneratorObjectAssembler33GenerateCreateGeneratorObjectImplEv, .-_ZN2v88internal30CreateGeneratorObjectAssembler33GenerateCreateGeneratorObjectImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_CreateGeneratorObjectEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"CreateGeneratorObject"
	.section	.text._ZN2v88internal8Builtins30Generate_CreateGeneratorObjectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_CreateGeneratorObjectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_CreateGeneratorObjectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_CreateGeneratorObjectEPNS0_8compiler18CodeAssemblerStateE:
.LFB22165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1272, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC34(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$351, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L319
.L316:
	movq	%r13, %rdi
	call	_ZN2v88internal30CreateGeneratorObjectAssembler33GenerateCreateGeneratorObjectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L316
.L320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22165:
	.size	_ZN2v88internal8Builtins30Generate_CreateGeneratorObjectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_CreateGeneratorObjectEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23ObjectBuiltinsAssembler17AddToDictionaryIfENS0_8compiler5TNodeINS0_5BoolTEEENS3_INS0_14NameDictionaryEEENS0_6HandleINS0_4NameEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectBuiltinsAssembler17AddToDictionaryIfENS0_8compiler5TNodeINS0_5BoolTEEENS3_INS0_14NameDictionaryEEENS0_6HandleINS0_4NameEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal23ObjectBuiltinsAssembler17AddToDictionaryIfENS0_8compiler5TNodeINS0_5BoolTEEENS3_INS0_14NameDictionaryEEENS0_6HandleINS0_4NameEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal23ObjectBuiltinsAssembler17AddToDictionaryIfENS0_8compiler5TNodeINS0_5BoolTEEENS3_INS0_14NameDictionaryEEENS0_6HandleINS0_4NameEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE:
.LFB22179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	movl	$1, %r8d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	%rsi, -208(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rcx, -200(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-208(%rbp), %r11
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-200(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L324
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L324:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22179:
	.size	_ZN2v88internal23ObjectBuiltinsAssembler17AddToDictionaryIfENS0_8compiler5TNodeINS0_5BoolTEEENS3_INS0_14NameDictionaryEEENS0_6HandleINS0_4NameEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal23ObjectBuiltinsAssembler17AddToDictionaryIfENS0_8compiler5TNodeINS0_5BoolTEEENS3_INS0_14NameDictionaryEEENS0_6HandleINS0_4NameEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal23ObjectBuiltinsAssembler22FromPropertyDescriptorEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectBuiltinsAssembler22FromPropertyDescriptorEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal23ObjectBuiltinsAssembler22FromPropertyDescriptorEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal23ObjectBuiltinsAssembler22FromPropertyDescriptorEPNS0_8compiler4NodeES4_:
.LFB22180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$8, %edx
	subq	$888, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler31LoadAndUntagToWord32ObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	movq	%r12, %rdi
	movl	$490, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	leaq	-832(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rax, %r15
	movq	%r10, -856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-704(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r11, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	leaq	-576(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-448(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	movl	$394, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-856(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$106, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-864(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -896(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -928(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$40, %edx
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-864(%rbp), %r9
	movq	-856(%rbp), %r8
	movq	%rax, %rdx
	call	_ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	-872(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-896(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	movl	$24, %edx
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-864(%rbp), %r9
	movq	-856(%rbp), %r8
	movq	%rax, %rdx
	leaq	-320(%rbp), %r15
	call	_ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	-872(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$151, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler22AllocateNameDictionaryEi@PLT
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23AllocateJSObjectFromMapENS0_8compiler11SloppyTNodeINS0_3MapEEENS3_INS0_10HeapObjectEEENS3_INS0_10FixedArrayEEENS_4base5FlagsINS1_14AllocationFlagEiEENS1_17SlackTrackingModeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -896(%rbp)
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -912(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12IsNotTheHoleENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-904(%rbp), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	leaq	3544(%rcx), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-912(%rbp), %r9
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-864(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-904(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$32, %esi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-904(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-904(%rbp), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	leaq	3600(%rcx), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-912(%rbp), %r9
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-864(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -912(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12IsNotTheHoleENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-904(%rbp), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	leaq	2608(%rcx), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-912(%rbp), %r9
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-864(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r9, %rcx
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$40, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler12IsNotTheHoleENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-904(%rbp), %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	leaq	3272(%rcx), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-864(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-904(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-904(%rbp), %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	leaq	2464(%rcx), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-864(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$8, %esi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-904(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rcx
	movq	%r12, %rdi
	leaq	2280(%rcx), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-864(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler3AddINS0_14NameDictionaryEEEvNS0_8compiler5TNodeIT_EENS5_INS0_4NameEEENS5_INS0_6ObjectEEEPNS4_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-872(%rbp), %rbx
	movq	-896(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-888(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-920(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-928(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L328
	addq	$888, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L328:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22180:
	.size	_ZN2v88internal23ObjectBuiltinsAssembler22FromPropertyDescriptorEPNS0_8compiler4NodeES4_, .-_ZN2v88internal23ObjectBuiltinsAssembler22FromPropertyDescriptorEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE:
.LFB22182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdi, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$312, %rsp
	movq	%rdx, -344(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler25IsFunctionTemplateInfoMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-344(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L332
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L332:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22182:
	.size	_ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal23ObjectBuiltinsAssembler19FromPropertyDetailsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ObjectBuiltinsAssembler19FromPropertyDetailsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal23ObjectBuiltinsAssembler19FromPropertyDetailsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal23ObjectBuiltinsAssembler19FromPropertyDetailsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelE:
.LFB22181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$8, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$504, %rsp
	movq	%rsi, -472(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%r8, -480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-448(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r11, -488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-192(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -536(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler14IsAccessorPairENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-488(%rbp), %r11
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-488(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$1800, %ecx
	movq	%r13, %rsi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$16, %edx
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$32, %esi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-496(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-496(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-520(%rbp), %rcx
	movq	-480(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -504(%rbp)
	movq	%rcx, %rsi
	call	_ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE
	movq	-480(%rbp), %rdx
	movq	-488(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal23ObjectBuiltinsAssembler22GetAccessorOrUndefinedEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelE
	movq	-512(%rbp), %r9
	movq	%r12, %rdi
	movq	-496(%rbp), %rcx
	movq	-504(%rbp), %r8
	movq	-472(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal23ObjectBuiltinsAssembler27ConstructAccessorDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-536(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$32, %esi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-480(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-480(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$8, %esi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-480(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rcx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-496(%rbp), %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	-488(%rbp), %r8
	movq	-472(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal23ObjectBuiltinsAssembler23ConstructDataDescriptorEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-504(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-472(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-472(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-528(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	addq	$504, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L336:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22181:
	.size	_ZN2v88internal23ObjectBuiltinsAssembler19FromPropertyDetailsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelE, .-_ZN2v88internal23ObjectBuiltinsAssembler19FromPropertyDetailsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal39ObjectGetOwnPropertyDescriptorAssembler42GenerateObjectGetOwnPropertyDescriptorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39ObjectGetOwnPropertyDescriptorAssembler42GenerateObjectGetOwnPropertyDescriptorImplEv
	.type	_ZN2v88internal39ObjectGetOwnPropertyDescriptorAssembler42GenerateObjectGetOwnPropertyDescriptorImplEv, @function
_ZN2v88internal39ObjectGetOwnPropertyDescriptorAssembler42GenerateObjectGetOwnPropertyDescriptorImplEv:
.LFB22178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1152(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1320, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	-1152(%rbp), %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	-1152(%rbp), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r14, %r13
	leaq	-208(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1248(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$99, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -80(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%r13, %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rax, %r8
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rcx, -336(%rbp)
	movq	%rbx, %rcx
	movq	-192(%rbp), %rax
	leaq	-336(%rbp), %rdx
	movq	%rbx, -1344(%rbp)
	movl	$1, %ebx
	pushq	%rbx
	leaq	-1104(%rbp), %rbx
	pushq	%rcx
	movl	$1, %ecx
	movq	%r13, -1240(%rbp)
	leaq	-848(%rbp), %r13
	movq	%rdx, -1256(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbx, -1264(%rbp)
	movl	$1, %r8d
	leaq	-976(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rax, -1288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-720(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r10
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-592(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r11
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -1304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1360(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadMapInstanceTypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler29IsSpecialReceiverInstanceTypeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	leaq	-1232(%rbp), %r8
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	%r8, %rdi
	movq	%r8, -1312(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-1216(%rbp), %r11
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -1272(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	pushq	-1304(%rbp)
	movq	%rbx, %r8
	movq	-1272(%rbp), %r9
	movq	-1312(%rbp), %rcx
	pushq	%r13
	movq	%r12, %rdi
	movq	-1264(%rbp), %rdx
	movq	-1288(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler9TryToNameEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	movq	-1304(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	%r13
	movq	%rbx, %r8
	movq	-1272(%rbp), %r9
	pushq	%r14
	movq	-1312(%rbp), %rcx
	movq	%r12, %rdi
	movq	-1264(%rbp), %rdx
	movq	-1288(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler20TryInternalizeStringEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES6_S8_S6_S6_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-464(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1200(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1184(%rbp), %r10
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -1328(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1168(%rbp), %r8
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -1336(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-1272(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1352(%rbp), %r11
	pushq	$1
	movq	%r12, %rdi
	movq	-1360(%rbp), %r10
	movq	-1240(%rbp), %rsi
	pushq	%r14
	movq	%r11, %r9
	pushq	-1256(%rbp)
	movq	-1248(%rbp), %r11
	pushq	-1336(%rbp)
	movq	%r10, %r8
	pushq	-1328(%rbp)
	movq	%r11, %rcx
	movq	%r11, %rdx
	pushq	-1280(%rbp)
	pushq	-1320(%rbp)
	pushq	%rax
	call	_ZN2v88internal17CodeStubAssembler17TryGetOwnPropertyEPNS0_8compiler4NodeES4_S4_S4_S4_S4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableES8_S8_S6_S6_NS1_18GetOwnPropertyModeE@PLT
	movq	-1320(%rbp), %rsi
	addq	$80, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1328(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1280(%rbp), %rdi
	movq	%rax, -1352(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1352(%rbp), %rcx
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	-1240(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal23ObjectBuiltinsAssembler19FromPropertyDetailsEPNS0_8compiler4NodeES4_S4_PNS2_18CodeAssemblerLabelE
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-1280(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1328(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1312(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1344(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	-1248(%rbp), %xmm0
	movq	-1240(%rbp), %rdx
	movl	$222, %esi
	movhps	-1288(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1296(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1240(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal23ObjectBuiltinsAssembler22FromPropertyDescriptorEPNS0_8compiler4NodeES4_
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-1296(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-1304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L340:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22178:
	.size	_ZN2v88internal39ObjectGetOwnPropertyDescriptorAssembler42GenerateObjectGetOwnPropertyDescriptorImplEv, .-_ZN2v88internal39ObjectGetOwnPropertyDescriptorAssembler42GenerateObjectGetOwnPropertyDescriptorImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_ObjectGetOwnPropertyDescriptorEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"ObjectGetOwnPropertyDescriptor"
	.section	.text._ZN2v88internal8Builtins39Generate_ObjectGetOwnPropertyDescriptorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_ObjectGetOwnPropertyDescriptorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_ObjectGetOwnPropertyDescriptorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_ObjectGetOwnPropertyDescriptorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1339, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$468, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L345
.L342:
	movq	%r13, %rdi
	call	_ZN2v88internal39ObjectGetOwnPropertyDescriptorAssembler42GenerateObjectGetOwnPropertyDescriptorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L342
.L346:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22174:
	.size	_ZN2v88internal8Builtins39Generate_ObjectGetOwnPropertyDescriptorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_ObjectGetOwnPropertyDescriptorEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_, @function
_GLOBAL__sub_I__ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_:
.LFB28734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28734:
	.size	_GLOBAL__sub_I__ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_, .-_GLOBAL__sub_I__ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23ObjectBuiltinsAssembler20ReturnToStringFormatEPNS0_8compiler4NodeES4_
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC16:
	.quad	8389754676633367105
	.quad	2322286632510186272
	.align 16
.LC17:
	.quad	8104637026473570896
	.quad	7815273814025642085
	.align 16
.LC18:
	.quad	8104637026473570896
	.quad	5932166637412753509
	.align 16
.LC19:
	.quad	7233184986725708372
	.quad	2334386829830614633
	.align 16
.LC20:
	.quad	7959358215658692931
	.quad	8246135037321111924
	.align 16
.LC21:
	.quad	2334395648808285295
	.quad	7935470565364626281
	.align 16
.LC23:
	.quad	8389754676633367105
	.quad	7738140083766702368
	.align 16
.LC24:
	.quad	8389754676633367105
	.quad	7738140083766702624
	.align 16
.LC25:
	.quad	7665776516279660364
	.quad	8386095522572952421
	.align 16
.LC26:
	.quad	8030593375065427301
	.quad	8390052652822569069
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
