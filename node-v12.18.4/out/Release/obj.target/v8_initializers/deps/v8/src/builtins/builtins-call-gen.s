	.file	"builtins-call-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB17126:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE17126:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation:
.LFB17127:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L5
	cmpl	$3, %edx
	je	.L6
	cmpl	$1, %edx
	je	.L10
.L6:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE17127:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation:
.LFB17131:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L12
	cmpl	$3, %edx
	je	.L13
	cmpl	$1, %edx
	je	.L17
.L13:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE17131:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE:
.LFB14503:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE@PLT
	.cfi_endproc
.LFE14503:
	.size	_ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins50Generate_CallFunction_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins50Generate_CallFunction_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins50Generate_CallFunction_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins50Generate_CallFunction_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE:
.LFB14504:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE@PLT
	.cfi_endproc
.LFE14504:
	.size	_ZN2v88internal8Builtins50Generate_CallFunction_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins50Generate_CallFunction_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins35Generate_CallFunction_ReceiverIsAnyEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_CallFunction_ReceiverIsAnyEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins35Generate_CallFunction_ReceiverIsAnyEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins35Generate_CallFunction_ReceiverIsAnyEPNS0_14MacroAssemblerE:
.LFB14505:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins21Generate_CallFunctionEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE@PLT
	.cfi_endproc
.LFE14505:
	.size	_ZN2v88internal8Builtins35Generate_CallFunction_ReceiverIsAnyEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins35Generate_CallFunction_ReceiverIsAnyEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins26Generate_CallBoundFunctionEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_CallBoundFunctionEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins26Generate_CallBoundFunctionEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins26Generate_CallBoundFunctionEPNS0_14MacroAssemblerE:
.LFB14506:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8Builtins30Generate_CallBoundFunctionImplEPNS0_14MacroAssemblerE@PLT
	.cfi_endproc
.LFE14506:
	.size	_ZN2v88internal8Builtins26Generate_CallBoundFunctionEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins26Generate_CallBoundFunctionEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins39Generate_Call_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_Call_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins39Generate_Call_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins39Generate_Call_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE:
.LFB14507:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE@PLT
	.cfi_endproc
.LFE14507:
	.size	_ZN2v88internal8Builtins39Generate_Call_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins39Generate_Call_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins42Generate_Call_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins42Generate_Call_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins42Generate_Call_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins42Generate_Call_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE:
.LFB14508:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE@PLT
	.cfi_endproc
.LFE14508:
	.size	_ZN2v88internal8Builtins42Generate_Call_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins42Generate_Call_ReceiverIsNotNullOrUndefinedEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins27Generate_Call_ReceiverIsAnyEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_Call_ReceiverIsAnyEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins27Generate_Call_ReceiverIsAnyEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins27Generate_Call_ReceiverIsAnyEPNS0_14MacroAssemblerE:
.LFB14509:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN2v88internal8Builtins13Generate_CallEPNS0_14MacroAssemblerENS0_19ConvertReceiverModeE@PLT
	.cfi_endproc
.LFE14509:
	.size	_ZN2v88internal8Builtins27Generate_Call_ReceiverIsAnyEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins27Generate_Call_ReceiverIsAnyEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins20Generate_CallVarargsEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_CallVarargsEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins20Generate_CallVarargsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins20Generate_CallVarargsEPNS0_14MacroAssemblerE:
.LFB14510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Builtins31Generate_CallOrConstructVarargsEPNS0_14MacroAssemblerENS0_6HandleINS0_4CodeEEE@PLT
	.cfi_endproc
.LFE14510:
	.size	_ZN2v88internal8Builtins20Generate_CallVarargsEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins20Generate_CallVarargsEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins27Generate_CallForwardVarargsEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_CallForwardVarargsEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins27Generate_CallForwardVarargsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins27Generate_CallForwardVarargsEPNS0_14MacroAssemblerE:
.LFB14511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE@PLT
	.cfi_endproc
.LFE14511:
	.size	_ZN2v88internal8Builtins27Generate_CallForwardVarargsEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins27Generate_CallForwardVarargsEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal8Builtins35Generate_CallFunctionForwardVarargsEPNS0_14MacroAssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_CallFunctionForwardVarargsEPNS0_14MacroAssemblerE
	.type	_ZN2v88internal8Builtins35Generate_CallFunctionForwardVarargsEPNS0_14MacroAssemblerE, @function
_ZN2v88internal8Builtins35Generate_CallFunctionForwardVarargsEPNS0_14MacroAssemblerE:
.LFB14512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	movq	%rax, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8Builtins38Generate_CallOrConstructForwardVarargsEPNS0_14MacroAssemblerENS1_19CallOrConstructModeENS0_6HandleINS0_4CodeEEE@PLT
	.cfi_endproc
.LFE14512:
	.size	_ZN2v88internal8Builtins35Generate_CallFunctionForwardVarargsEPNS0_14MacroAssemblerE, .-_ZN2v88internal8Builtins35Generate_CallFunctionForwardVarargsEPNS0_14MacroAssemblerE
	.section	.text._ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_
	.type	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_, @function
_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_:
.LFB14526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movq	%rsi, -176(%rbp)
	movq	%r8, %rsi
	movq	%rdx, -160(%rbp)
	movq	%r9, -184(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%rax, %rdx
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	pushq	$0
	movq	%r13, %r8
	movl	$2, %ecx
	pushq	$0
	movq	%rax, %r9
	movq	%r15, %rdx
	movl	$4, %esi
	pushq	$1
	movq	%r12, %rdi
	pushq	$4
	pushq	%r14
	pushq	%r14
	call	_ZN2v88internal17CodeStubAssembler22CopyFixedArrayElementsENS0_12ElementsKindEPNS0_8compiler4NodeES2_S5_S5_S5_S5_NS0_16WriteBarrierModeENS1_13ParameterModeENS1_18HoleConversionModeEPNS3_26TypedCodeAssemblerVariableINS0_5BoolTEEE@PLT
	addq	$48, %rsp
	cmpq	$0, -160(%rbp)
	movq	-176(%rbp), %xmm1
	je	.L40
	movq	%rbx, %xmm2
	movhps	-160(%rbp), %xmm1
	movq	%r12, %rdi
	movq	-184(%rbp), %xmm0
	movaps	%xmm1, -176(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbp), %rcx
	leaq	-96(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm0
	movq	%rax, -144(%rbp)
	movq	-112(%rbp), %rax
	movl	$5, %r9d
	leaq	-144(%rbp), %rsi
	movq	%r13, -64(%rbp)
	movq	%rax, -136(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L35:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%r13, %xmm3
	movq	%rbx, %xmm0
	movhps	-184(%rbp), %xmm1
	movq	%r12, %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbp), %rcx
	leaq	-96(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm0
	movq	%rax, -144(%rbp)
	movq	-112(%rbp), %rax
	leaq	-144(%rbp), %rsi
	movl	$4, %r9d
	movaps	%xmm1, -96(%rbp)
	movq	%rax, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	jmp	.L35
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14526:
	.size	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_, .-_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_
	.section	.text._ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE
	.type	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE, @function
_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE:
.LFB14513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	subq	$1368, %rsp
	movq	%rsi, -1344(%rbp)
	movq	%rdi, %rsi
	movq	%rdx, -1320(%rbp)
	xorl	%edx, %edx
	movq	%r8, -1296(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1120(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-864(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-736(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	testq	%r14, %r14
	je	.L49
	leaq	-608(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-480(%rbp), %r14
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	%r15, -1392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r14, -1336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1344(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsConstructorENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	movq	-1296(%rbp), %rdx
	movq	%rax, %rcx
	movl	$1, %r8d
	movl	$169, %esi
	movq	%rax, -1376(%rbp)
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-352(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsConstructorENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1304(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %r8d
	movl	$169, %esi
	movq	-1320(%rbp), %rdi
	movq	-1376(%rbp), %rcx
	movq	-1296(%rbp), %rdx
	movq	%rdi, -96(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
.L44:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1288(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$147, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1328(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movl	$149, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1328(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsJSArrayMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-1288(%rbp), %rcx
	movq	-1352(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1264(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1360(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-1352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	8(%r12), %r9
	movq	%rbx, %rsi
	movq	%r9, %rdi
	movq	%r9, -1400(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$24, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler31LoadAndUntagToWord32ObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	movq	-1360(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movl	$11, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1384(%rbp)
	call	_ZN2v88internal17CodeStubAssembler25IsElementsKindGreaterThanENS0_8compiler5TNodeINS0_6Int32TEEENS0_12ElementsKindE@PLT
	movq	-1288(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1384(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-1312(%rbp), %rcx
	movq	-1368(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1296(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler32IsPrototypeInitialArrayPrototypeENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	movq	-1288(%rbp), %r15
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler32IsNoElementsProtectorCellInvalidEv@PLT
	movq	-1312(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1400(%rbp), %r9
	movq	%rbx, %rsi
	movq	%rax, %r15
	movq	%r9, %rdi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1384(%rbp)
	call	_ZN2v88internal17CodeStubAssembler24LoadFixedArrayBaseLengthENS0_8compiler11SloppyTNodeINS0_14FixedArrayBaseEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1288(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1384(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10SmiToInt32ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1360(%rbp), %r15
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1376(%rbp), %rcx
	movl	$1, %r8d
	movq	-1296(%rbp), %rdx
	movl	$143, %esi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler31LoadAndUntagToWord32ObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1312(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1336(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1304(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, -1384(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler18IsFixedDoubleArrayENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1336(%rbp), %rcx
	movq	-1304(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpq	$0, -1320(%rbp)
	movq	%r12, %rdi
	je	.L50
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm1
	movq	%r12, %rdi
	movq	-1344(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-1376(%rbp), %r8
	movq	-1296(%rbp), %rcx
	movhps	-1320(%rbp), %xmm0
	movl	$5, %r9d
	movq	%r15, -64(%rbp)
	movq	-1392(%rbp), %rsi
	movaps	%xmm0, -96(%rbp)
	movq	-1384(%rbp), %xmm0
	movq	%rax, -608(%rbp)
	movq	-208(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -600(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L46:
	movq	-1304(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-1320(%rbp), %rdx
	movq	-1384(%rbp), %r9
	pushq	%rax
	pushq	-1296(%rbp)
	movq	-1344(%rbp), %rsi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1360(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1352(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1312(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	-352(%rbp), %r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-224(%rbp), %r13
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r14, -1304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1344(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler10IsCallableENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rax
	movq	%r12, %rdi
	movq	%r15, -96(%rbp)
	movq	-1296(%rbp), %rdx
	movq	%rax, %rcx
	movl	$1, %r8d
	movl	$161, %esi
	movq	%rax, -1376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	leaq	-608(%rbp), %rax
	movq	%rax, -1392(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, -1336(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L50:
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %xmm2
	movq	%r12, %rdi
	movq	-1344(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-1376(%rbp), %r8
	movq	-1296(%rbp), %rcx
	movhps	-1384(%rbp), %xmm0
	movq	%rax, -608(%rbp)
	movq	-1392(%rbp), %rsi
	movl	$4, %r9d
	movq	-208(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -600(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	jmp	.L46
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14513:
	.size	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE, .-_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE
	.section	.text._ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE
	.type	_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE, @function
_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE:
.LFB14527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-608(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$776, %rsp
	movq	%r9, -712(%rbp)
	movq	%rsi, -768(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rdx, -760(%rbp)
	xorl	%edx, %edx
	movq	%r8, -776(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%r15, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-704(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-688(%rbp), %rcx
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -728(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-672(%rbp), %rsi
	movl	$4, %edx
	movq	%rsi, %r11
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -736(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler12IsJSArrayMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-712(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler32IsPrototypeInitialArrayPrototypeENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler32IsNoElementsProtectorCellInvalidEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30ArrayIteratorProtectorConstantEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-784(%rbp), %r8
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %r15
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-736(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$24, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler31LoadAndUntagToWord32ObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	movq	-752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	leaq	8(%r12), %r10
	movq	%rbx, %rsi
	movq	%r10, %rdi
	movq	%r10, -800(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	-728(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler29IsElementsKindLessThanOrEqualENS0_8compiler5TNodeINS0_6Int32TEEENS0_12ElementsKindE@PLT
	movq	-720(%rbp), %r15
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler29IsElementsKindLessThanOrEqualENS0_8compiler5TNodeINS0_6Int32TEEENS0_12ElementsKindE@PLT
	movq	-744(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movl	$11, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler29IsElementsKindLessThanOrEqualENS0_8compiler5TNodeINS0_6Int32TEEENS0_12ElementsKindE@PLT
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-224(%rbp), %r14
	movq	%r15, -720(%rbp)
	leaq	-656(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22IteratorSymbolConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -792(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-640(%rbp), %r11
	movl	$710, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -784(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %r10
	movl	$2, %edi
	xorl	%esi, %esi
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r9
	movq	%rax, %r8
	movq	%rbx, %xmm0
	pushq	%r10
	movq	-624(%rbp), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r9, -656(%rbp)
	movq	-712(%rbp), %r9
	movl	$1, %ecx
	movhps	-792(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r10, -792(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -816(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16TaggedIsCallableENS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-784(%rbp), %rdi
	movl	$394, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-792(%rbp), %rcx
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-712(%rbp), %r9
	pushq	%rbx
	movq	-624(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	movhps	-816(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movq	%rsi, -656(%rbp)
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movl	$24, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler31LoadAndUntagToWord32ObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	movq	-752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-800(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	movq	-728(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-736(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-720(%rbp), %rbx
	movq	-744(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$69, %edx
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-728(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-752(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	cmpq	$0, -760(%rbp)
	movq	%rax, -816(%rbp)
	je	.L57
	movq	-776(%rbp), %xmm1
	movq	%r12, %rdi
	movhps	-816(%rbp), %xmm1
	movaps	%xmm1, -816(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-816(%rbp), %xmm1
	movq	%r12, %rdi
	movq	-768(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-792(%rbp), %r8
	movq	-712(%rbp), %rcx
	movq	%rax, -640(%rbp)
	movq	-784(%rbp), %rsi
	movhps	-760(%rbp), %xmm0
	movl	$5, %r9d
	movq	-208(%rbp), %rax
	movq	%rbx, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -632(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
.L54:
	movq	-744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-752(%rbp), %r15
	movq	%rax, %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-720(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-736(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-728(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	pushq	%r14
	movq	%rbx, %r8
	movq	%r12, %rdi
	pushq	-712(%rbp)
	movq	-760(%rbp), %rdx
	movq	%rax, %rcx
	movq	-776(%rbp), %r9
	movq	-768(%rbp), %rsi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructDoubleVarargsENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EENS3_INS0_16FixedDoubleArrayEEENS3_INS0_6Int32TEEESB_NS3_INS0_7ContextEEESB_
	movq	-736(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	%rbx, %xmm2
	movq	%rax, %xmm1
	movq	%r12, %rdi
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -816(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-816(%rbp), %xmm1
	movq	%r12, %rdi
	movq	-768(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-792(%rbp), %r8
	movq	-712(%rbp), %rcx
	movq	%rax, -640(%rbp)
	movq	-784(%rbp), %rsi
	movhps	-776(%rbp), %xmm0
	movl	$4, %r9d
	movq	-208(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	jmp	.L54
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14527:
	.size	_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE, .-_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE
	.section	.rodata._ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-call-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"CallWithArrayLike"
	.section	.text._ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE:
.LFB14535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$367, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$14, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L63
.L60:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L60
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14535:
	.size	_ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_CallWithArrayLikeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26CallWithArrayLikeAssembler29GenerateCallWithArrayLikeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26CallWithArrayLikeAssembler29GenerateCallWithArrayLikeImplEv
	.type	_ZN2v88internal26CallWithArrayLikeAssembler29GenerateCallWithArrayLikeImplEv, @function
_ZN2v88internal26CallWithArrayLikeAssembler29GenerateCallWithArrayLikeImplEv:
.LFB14539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal32CallOrConstructBuiltinsAssembler28CallOrConstructWithArrayLikeENS0_8compiler5TNodeINS0_6ObjectEEENS2_11SloppyTNodeIS4_EES5_NS3_INS0_7ContextEEE
	.cfi_endproc
.LFE14539:
	.size	_ZN2v88internal26CallWithArrayLikeAssembler29GenerateCallWithArrayLikeImplEv, .-_ZN2v88internal26CallWithArrayLikeAssembler29GenerateCallWithArrayLikeImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_CallWithSpreadEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"CallWithSpread"
	.section	.text._ZN2v88internal8Builtins23Generate_CallWithSpreadEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_CallWithSpreadEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_CallWithSpreadEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_CallWithSpreadEPNS0_8compiler18CodeAssemblerStateE:
.LFB14544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$375, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$13, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L71
.L68:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%edx, %edx
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%rax, %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L68
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14544:
	.size	_ZN2v88internal8Builtins23Generate_CallWithSpreadEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_CallWithSpreadEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23CallWithSpreadAssembler26GenerateCallWithSpreadImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23CallWithSpreadAssembler26GenerateCallWithSpreadImplEv
	.type	_ZN2v88internal23CallWithSpreadAssembler26GenerateCallWithSpreadImplEv, @function
_ZN2v88internal23CallWithSpreadAssembler26GenerateCallWithSpreadImplEv:
.LFB14548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r9
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal32CallOrConstructBuiltinsAssembler25CallOrConstructWithSpreadENS0_8compiler5TNodeINS0_6ObjectEEES5_S5_NS3_INS0_6Int32TEEENS3_INS0_7ContextEEE
	.cfi_endproc
.LFE14548:
	.size	_ZN2v88internal23CallWithSpreadAssembler26GenerateCallWithSpreadImplEv, .-_ZN2v88internal23CallWithSpreadAssembler26GenerateCallWithSpreadImplEv
	.section	.text._ZN2v88internal32CallOrConstructBuiltinsAssembler21GetCompatibleReceiverENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_10HeapObjectEEENS3_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32CallOrConstructBuiltinsAssembler21GetCompatibleReceiverENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_10HeapObjectEEENS3_INS0_7ContextEEE
	.type	_ZN2v88internal32CallOrConstructBuiltinsAssembler21GetCompatibleReceiverENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_10HeapObjectEEENS3_INS0_7ContextEEE, @function
_ZN2v88internal32CallOrConstructBuiltinsAssembler21GetCompatibleReceiverENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_10HeapObjectEEENS3_INS0_7ContextEEE:
.LFB14549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-864(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-832(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-576(%rbp), %rbx
	subq	$920, %rsp
	movq	%rdx, -904(%rbp)
	movl	$7, %edx
	movq	%rcx, -912(%rbp)
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r14, %rcx
	movl	$1, %r8d
	movl	$1, %edx
	movq	%r15, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rcx
	movl	$1, %edx
	leaq	-704(%rbp), %r9
	movl	$1, %r8d
	movq	%r15, -192(%rbp)
	movq	%r9, %rdi
	movq	%r9, -952(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -896(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	leaq	-848(%rbp), %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -944(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rsi
	movl	$7, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rcx
	movl	$1, %edx
	leaq	-448(%rbp), %r11
	movl	$1, %r8d
	movq	%r13, -192(%rbp)
	movq	%r11, %rdi
	movq	%r11, -920(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rcx
	movl	$1, %edx
	leaq	-320(%rbp), %rax
	movl	$1, %r8d
	movq	%r13, -192(%rbp)
	movq	%rax, %rdi
	movq	%rax, -888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1, %r8d
	leaq	-872(%rbp), %rcx
	movq	%r13, -872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-920(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-920(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -928(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-920(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movl	$1105, %edx
	movq	%rax, %rsi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-920(%rbp), %r8
	movq	%r12, %rdi
	movl	$68, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-928(%rbp), %r11
	movq	-888(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE(%rip), %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$1800, %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-920(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-904(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-952(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	movq	%r9, -928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-904(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler25IsFunctionTemplateInfoMapENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-904(%rbp), %r8
	movl	$72, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %ecx
	movq	%r8, %rsi
	movl	%ecx, -920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-904(%rbp), %r8
	movl	$24, %edx
	movq	%r12, %rdi
	movl	-920(%rbp), %ecx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-944(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadMapPrototypeENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-904(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler18IsJSGlobalProxyMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	-896(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$56, %edx
	movq	-912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-936(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-928(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -888(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-888(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$920, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14549:
	.size	_ZN2v88internal32CallOrConstructBuiltinsAssembler21GetCompatibleReceiverENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_10HeapObjectEEENS3_INS0_7ContextEEE, .-_ZN2v88internal32CallOrConstructBuiltinsAssembler21GetCompatibleReceiverENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_10HeapObjectEEENS3_INS0_7ContextEEE
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB17130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	16(%rax), %rcx
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	24(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	(%rcx), %rcx
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler21GetCompatibleReceiverENS0_8compiler5TNodeINS0_10JSReceiverEEENS3_INS0_10HeapObjectEEENS3_INS0_7ContextEEE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17130:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.type	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE, @function
_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE:
.LFB14553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	movq	%rdi, %rsi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-560(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -592(%rbp)
	movq	%rcx, %rdx
	movq	%rcx, -624(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -584(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	leaq	-512(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	%rax, -576(%rbp)
	testb	$-3, %r15b
	je	.L94
	leaq	-384(%rbp), %r14
	leaq	-256(%rbp), %r13
	leaq	-96(%rbp), %rbx
.L82:
	leaq	-576(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %ecx
	leaq	-568(%rbp), %rax
	movq	-592(%rbp), %rsi
	movq	%r15, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -608(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movdqa	-608(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%r15, -384(%rbp)
	movq	%rax, %rsi
	movq	%rax, -568(%rbp)
	leaq	-584(%rbp), %rax
	leaq	-128(%rbp), %r15
	movaps	%xmm0, -256(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r12, -232(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	movq	%rax, %rsi
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, -128(%rbp)
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_10JSReceiverEZNS1_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS9_24CallFunctionTemplateModeENS2_5TNodeINS1_20FunctionTemplateInfoEEENSB_INS1_7IntPtrTEEENSB_INS1_7ContextEEEEUlvE_ZNS9_20CallFunctionTemplateESA_SD_SF_SH_EUlvE0_EENSB_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movl	$7, %r8d
	punpcklqdq	%xmm2, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_10JSReceiverEZNS2_32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS6_24CallFunctionTemplateModeENS2_8compiler5TNodeINS2_20FunctionTemplateInfoEEENS9_INS2_7IntPtrTEEENS9_INS2_7ContextEEEEUlvE_ZNS6_20CallFunctionTemplateES7_SB_SD_SF_EUlvE0_EENS9_IT_EENS8_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSV_St18_Manager_operation(%rip), %rcx
	movq	%rax, %xmm3
	movaps	%xmm0, -80(%rbp)
	movq	%rcx, %xmm0
	movq	%rbx, %rcx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, -608(%rbp)
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L84
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L84:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L83
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	-384(%rbp), %r14
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	leaq	-256(%rbp), %r13
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadMapBitFieldENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-608(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-592(%rbp), %rsi
	movl	$88, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23LoadAndUntagObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-608(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %rbx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rcx
	movl	$135, %esi
	movq	%r12, %rdi
	movq	-584(%rbp), %rdx
	movq	-576(%rbp), %rax
	movl	$1, %r8d
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	testb	%r15b, %r15b
	jne	.L82
	movq	-576(%rbp), %rax
	movq	%rax, -608(%rbp)
.L83:
	movzwl	_ZN2v88internal13MachineTypeOfINS0_15CallHandlerInfoEvE5valueE(%rip), %ecx
	movq	-592(%rbp), %rsi
	movl	$48, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7ForeignEvE5valueE(%rip), %ecx
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE(%rip), %ecx
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$24, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %ecx
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	-584(%rbp), %r15
	movq	%rax, -592(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	-616(%rbp), %xmm0
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	movq	-240(%rbp), %rax
	movl	$4, %r9d
	movhps	-624(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-592(%rbp), %xmm0
	movq	%rax, -376(%rbp)
	movhps	-608(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$600, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L96:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14553:
	.size	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE, .-_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.section	.rodata._ZN2v88internal8Builtins41Generate_CallFunctionTemplate_CheckAccessEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"CallFunctionTemplate_CheckAccess"
	.section	.text._ZN2v88internal8Builtins41Generate_CallFunctionTemplate_CheckAccessEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_CallFunctionTemplate_CheckAccessEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins41Generate_CallFunctionTemplate_CheckAccessEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins41Generate_CallFunctionTemplate_CheckAccessEPNS0_8compiler18CodeAssemblerStateE:
.LFB14560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$569, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$17, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L101
.L98:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L98
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14560:
	.size	_ZN2v88internal8Builtins41Generate_CallFunctionTemplate_CheckAccessEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins41Generate_CallFunctionTemplate_CheckAccessEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal41CallFunctionTemplate_CheckAccessAssembler44GenerateCallFunctionTemplate_CheckAccessImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal41CallFunctionTemplate_CheckAccessAssembler44GenerateCallFunctionTemplate_CheckAccessImplEv
	.type	_ZN2v88internal41CallFunctionTemplate_CheckAccessAssembler44GenerateCallFunctionTemplate_CheckAccessImplEv, @function
_ZN2v88internal41CallFunctionTemplate_CheckAccessAssembler44GenerateCallFunctionTemplate_CheckAccessImplEv:
.LFB14564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	xorl	%esi, %esi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.cfi_endproc
.LFE14564:
	.size	_ZN2v88internal41CallFunctionTemplate_CheckAccessAssembler44GenerateCallFunctionTemplate_CheckAccessImplEv, .-_ZN2v88internal41CallFunctionTemplate_CheckAccessAssembler44GenerateCallFunctionTemplate_CheckAccessImplEv
	.section	.rodata._ZN2v88internal8Builtins53Generate_CallFunctionTemplate_CheckCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"CallFunctionTemplate_CheckCompatibleReceiver"
	.section	.text._ZN2v88internal8Builtins53Generate_CallFunctionTemplate_CheckCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins53Generate_CallFunctionTemplate_CheckCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins53Generate_CallFunctionTemplate_CheckCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins53Generate_CallFunctionTemplate_CheckCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE:
.LFB14569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$579, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$18, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L109
.L106:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L106
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14569:
	.size	_ZN2v88internal8Builtins53Generate_CallFunctionTemplate_CheckCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins53Generate_CallFunctionTemplate_CheckCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal53CallFunctionTemplate_CheckCompatibleReceiverAssembler56GenerateCallFunctionTemplate_CheckCompatibleReceiverImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal53CallFunctionTemplate_CheckCompatibleReceiverAssembler56GenerateCallFunctionTemplate_CheckCompatibleReceiverImplEv
	.type	_ZN2v88internal53CallFunctionTemplate_CheckCompatibleReceiverAssembler56GenerateCallFunctionTemplate_CheckCompatibleReceiverImplEv, @function
_ZN2v88internal53CallFunctionTemplate_CheckCompatibleReceiverAssembler56GenerateCallFunctionTemplate_CheckCompatibleReceiverImplEv:
.LFB14573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	movl	$1, %esi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.cfi_endproc
.LFE14573:
	.size	_ZN2v88internal53CallFunctionTemplate_CheckCompatibleReceiverAssembler56GenerateCallFunctionTemplate_CheckCompatibleReceiverImplEv, .-_ZN2v88internal53CallFunctionTemplate_CheckCompatibleReceiverAssembler56GenerateCallFunctionTemplate_CheckCompatibleReceiverImplEv
	.section	.rodata._ZN2v88internal8Builtins62Generate_CallFunctionTemplate_CheckAccessAndCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"CallFunctionTemplate_CheckAccessAndCompatibleReceiver"
	.section	.text._ZN2v88internal8Builtins62Generate_CallFunctionTemplate_CheckAccessAndCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins62Generate_CallFunctionTemplate_CheckAccessAndCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins62Generate_CallFunctionTemplate_CheckAccessAndCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins62Generate_CallFunctionTemplate_CheckAccessAndCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE:
.LFB14578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$590, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$19, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L117
.L114:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$2, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L114
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14578:
	.size	_ZN2v88internal8Builtins62Generate_CallFunctionTemplate_CheckAccessAndCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins62Generate_CallFunctionTemplate_CheckAccessAndCompatibleReceiverEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal62CallFunctionTemplate_CheckAccessAndCompatibleReceiverAssembler65GenerateCallFunctionTemplate_CheckAccessAndCompatibleReceiverImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal62CallFunctionTemplate_CheckAccessAndCompatibleReceiverAssembler65GenerateCallFunctionTemplate_CheckAccessAndCompatibleReceiverImplEv
	.type	_ZN2v88internal62CallFunctionTemplate_CheckAccessAndCompatibleReceiverAssembler65GenerateCallFunctionTemplate_CheckAccessAndCompatibleReceiverImplEv, @function
_ZN2v88internal62CallFunctionTemplate_CheckAccessAndCompatibleReceiverAssembler65GenerateCallFunctionTemplate_CheckAccessAndCompatibleReceiverImplEv:
.LFB14582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	movl	$2, %esi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal32CallOrConstructBuiltinsAssembler20CallFunctionTemplateENS1_24CallFunctionTemplateModeENS0_8compiler5TNodeINS0_20FunctionTemplateInfoEEENS4_INS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.cfi_endproc
.LFE14582:
	.size	_ZN2v88internal62CallFunctionTemplate_CheckAccessAndCompatibleReceiverAssembler65GenerateCallFunctionTemplate_CheckAccessAndCompatibleReceiverImplEv, .-_ZN2v88internal62CallFunctionTemplate_CheckAccessAndCompatibleReceiverAssembler65GenerateCallFunctionTemplate_CheckAccessAndCompatibleReceiverImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE:
.LFB18750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE18750:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_CallFunction_ReceiverIsNullOrUndefinedEPNS0_14MacroAssemblerE
	.weak	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE:
	.byte	5
	.byte	0
	.weak	_ZN2v88internal13MachineTypeOfINS0_7ForeignEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7ForeignEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7ForeignEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7ForeignEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7ForeignEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7ForeignEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_15CallHandlerInfoEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_15CallHandlerInfoEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_15CallHandlerInfoEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_15CallHandlerInfoEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_15CallHandlerInfoEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_15CallHandlerInfoEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
