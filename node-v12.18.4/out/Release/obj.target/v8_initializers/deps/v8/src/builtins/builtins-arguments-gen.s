	.file	"builtins-arguments-gen.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsES4_S4_S4_S4_S4_NS1_17CodeStubAssembler13ParameterModeEiEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsES4_S4_S4_S4_S4_NS1_17CodeStubAssembler13ParameterModeEiEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsES4_S4_S4_S4_S4_NS1_17CodeStubAssembler13ParameterModeEiEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%rsi), %r13
	movq	16(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	8(%rbx), %rdx
	movq	%r12, %rdi
	movq	%r13, %r8
	movq	%rax, %rcx
	movl	$8, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	16(%rbx), %rsi
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	movl	$1, %ecx
	popq	%r12
	movl	$8, %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	.cfi_endproc
.LFE26490:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsES4_S4_S4_S4_S4_NS1_17CodeStubAssembler13ParameterModeEiEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsES4_S4_S4_S4_S4_NS1_17CodeStubAssembler13ParameterModeEiEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movq	(%rsi), %r12
	movl	$8, %esi
	movq	16(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	24(%rbx), %rdi
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	(%rbx), %rdi
	movl	40(%rbx), %r14d
	movq	24(%rbx), %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, %r8
	testl	%r14d, %r14d
	jne	.L10
.L5:
	movq	32(%rbx), %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movl	$8, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	40(%rbx), %ecx
	movq	(%rbx), %rsi
	movl	$1, %edx
	movq	24(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, %r8
	jmp	.L5
	.cfi_endproc
.LFE26503:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%rsi), %r12
	movq	(%rbx), %rdi
	movq	16(%rbx), %rsi
	call	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	movq	16(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movl	$1800, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadBufferObjectEPNS0_8compiler4NodeEiNS0_11MachineTypeE@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	movq	%r12, %rcx
	movq	%rax, %r8
	popq	%r12
	movl	$8, %esi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	.cfi_endproc
.LFE26499:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS2_8compiler4NodeES6_S6_S6_S6_NS2_17CodeStubAssembler13ParameterModeEiEUlS6_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS2_8compiler4NodeES6_S6_S6_S6_NS2_17CodeStubAssembler13ParameterModeEiEUlS6_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS2_8compiler4NodeES6_S6_S6_S6_NS2_17CodeStubAssembler13ParameterModeEiEUlS6_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB26491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L14
	cmpl	$3, %edx
	je	.L15
	cmpl	$1, %edx
	je	.L21
.L16:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L16
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26491:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS2_8compiler4NodeES6_S6_S6_S6_NS2_17CodeStubAssembler13ParameterModeEiEUlS6_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS2_8compiler4NodeES6_S6_S6_S6_NS2_17CodeStubAssembler13ParameterModeEiEUlS6_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB26500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L23
	cmpl	$3, %edx
	je	.L24
	cmpl	$1, %edx
	je	.L30
.L25:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L25
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26500:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB26504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L32
	cmpl	$3, %edx
	je	.L33
	cmpl	$1, %edx
	je	.L39
.L34:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$48, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movq	%rax, (%rbx)
	movups	%xmm2, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L34
	movl	$48, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26504:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	.type	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi, @function
_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi:
.LFB22437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$120, %rsp
	movq	%rdi, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L55
	movl	16(%rbp), %eax
	movq	%r8, %rsi
	leal	16(%rax), %r15d
	testl	%r9d, %r9d
	je	.L66
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, -144(%rbp)
.L41:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler25IsIntPtrOrSmiConstantZeroEPNS0_8compiler4NodeENS1_13ParameterModeE@PLT
	movb	%al, -136(%rbp)
	testb	%al, %al
	je	.L43
	movslq	%r15d, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rax, %rsi
.L44:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8AllocateENS0_8compiler5TNodeINS0_7IntPtrTEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, %r15
	je	.L45
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	$27, -104(%rbp)
	movq	%r8, %rdi
	leaq	-104(%rbp), %rsi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%r12, %rdi
	movabsq	$7665811700904652389, %rcx
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %r8
	movq	%rdx, -80(%rbp)
	movl	$25445, %edx
	movups	%xmm0, (%rax)
	movq	%r8, %rsi
	movq	%rcx, 16(%rax)
	movw	%dx, 24(%rax)
	movq	-96(%rbp), %rdx
	movb	$116, 26(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-152(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23EmptyFixedArrayConstantEv@PLT
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler16StoreObjectFieldEPNS0_8compiler4NodeEiS4_@PLT
	movq	%r13, %rcx
	testl	%ebx, %ebx
	jne	.L67
.L47:
	movl	$6, %r8d
	movl	$24, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rcx, -144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	cmpb	$0, -136(%rbp)
	je	.L68
	movq	$0, -136(%rbp)
	cmpq	$0, -120(%rbp)
	je	.L69
.L52:
	movl	$16, %r8d
	movl	%ebx, %ecx
	movq	%r13, %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rax, %rcx
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler34SloppyArgumentsElementsMapConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	testl	%ebx, %ebx
	jne	.L70
.L50:
	movq	-120(%rbp), %rcx
	movl	$8, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
.L51:
	movq	-128(%rbp), %rax
	movq	%r13, %xmm0
	movhps	-136(%rbp), %xmm0
	movq	%r15, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	movq	-128(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%rax, -144(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
	movq	-144(%rbp), %rsi
	leal	16(%r15), %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	%rax, %rsi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rcx, -144(%rbp)
	movl	16(%rbp), %r15d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L68:
	movl	16(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEEi@PLT
	movq	-144(%rbp), %rcx
	movl	$8, %edx
	movq	%r12, %rdi
	movl	$6, %r8d
	movq	%rax, %rsi
	movq	%rax, %r14
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayMapConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	cmpq	$0, -120(%rbp)
	jne	.L52
	movq	-136(%rbp), %rcx
	movl	$8, %r8d
.L65:
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, %rcx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$7, %r8d
	movq	%r14, %rcx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%rax, -120(%rbp)
	jmp	.L50
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22437:
	.size	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi, .-_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	.section	.text._ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	.type	_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi, @function
_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi:
.LFB22487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	xorl	%r8d, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r9, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$192, %rsp
	movq	%rdx, -208(%rbp)
	movl	16(%rbp), %r15d
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movl	%r15d, %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	24(%rbp), %eax
	pushq	%rax
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	-128(%rbp), %rax
	xorl	%r9d, %r9d
	movl	%r15d, %r8d
	movq	-208(%rbp), %r10
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -224(%rbp)
	movq	-136(%rbp), %rax
	leaq	-192(%rbp), %r12
	movq	%r10, %rcx
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movl	$5, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$15, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	popq	%rsi
	popq	%rdi
	movq	$0, -152(%rbp)
	movq	$0, -160(%rbp)
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rax, -176(%rbp)
	movq	$0, -168(%rbp)
	subq	%rdx, %rcx
	cmpq	$7, %rcx
	jbe	.L81
	leaq	8(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L74:
	movq	%rdx, -168(%rbp)
	movq	%rbx, %xmm0
	movl	$24, %edi
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS2_8compiler4NodeES6_S6_S6_S6_NS2_17CodeStubAssembler13ParameterModeEiEUlS6_E_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rbx
	movq	%rcx, -152(%rbp)
	movhps	-216(%rbp), %xmm0
	movq	%r12, (%rdx)
	movaps	%xmm0, -208(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movl	%r15d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movdqa	-208(%rbp), %xmm0
	movq	%r12, 16(%rax)
	movq	%r13, %rdi
	leaq	-176(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsES4_S4_S4_S4_S4_NS1_17CodeStubAssembler13ParameterModeEiEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	movq	%rax, %xmm1
	movq	%rbx, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L75
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L75:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	movq	-224(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	8(%rax), %rcx
	jmp	.L74
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22487:
	.size	_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi, .-_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	.section	.text._ZN2v88internal26ArgumentsBuiltinsAssembler24EmitFastNewRestParameterEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ArgumentsBuiltinsAssembler24EmitFastNewRestParameterEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal26ArgumentsBuiltinsAssembler24EmitFastNewRestParameterEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal26ArgumentsBuiltinsAssembler24EmitFastNewRestParameterEPNS0_8compiler4NodeES4_:
.LFB22505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	xorl	%esi, %esi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-528(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$1, %edx
	subq	$552, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	movq	%rbx, %rcx
	leaq	8(%r12), %rsi
	movq	%r15, %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -536(%rbp)
	movq	%rbx, -568(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler25GetArgumentsFrameAndCountENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEE@PLT
	movq	-504(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-496(%rbp), %rdx
	movq	%rbx, -544(%rbp)
	leaq	-448(%rbp), %rbx
	movq	%rdx, -552(%rbp)
	movl	$8, %edx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	-480(%rbp), %r11
	movl	$1, %r8d
	movq	%r13, -480(%rbp)
	movq	%r11, %rcx
	movq	%r11, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-552(%rbp), %rdx
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, -576(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	-576(%rbp), %r9
	movq	-536(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -560(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-584(%rbp), %r10
	movq	%r12, %rdi
	movq	-576(%rbp), %r9
	movl	$1, %r8d
	movl	$48, %ecx
	movq	%r10, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler39GotoIfFixedArraySizeDoesntFitInNewSpaceEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEiNS1_13ParameterModeE@PLT
	pushq	$32
	movq	%r12, %rdi
	movq	-576(%rbp), %r9
	movq	-552(%rbp), %r8
	movq	-544(%rbp), %rcx
	pushq	$1
	movq	-512(%rbp), %rdx
	movq	-560(%rbp), %rsi
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-592(%rbp), %r11
	xorl	%r8d, %r8d
	movq	-536(%rbp), %rcx
	movq	-560(%rbp), %rdx
	movl	$1, %r9d
	movq	%r12, %rsi
	movl	$32, (%rsp)
	movq	%r11, %rdi
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	-464(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-584(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -536(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-568(%rbp), %rax
	movl	$1, %r8d
	movl	$309, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-536(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22505:
	.size	_ZN2v88internal26ArgumentsBuiltinsAssembler24EmitFastNewRestParameterEPNS0_8compiler4NodeES4_, .-_ZN2v88internal26ArgumentsBuiltinsAssembler24EmitFastNewRestParameterEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewStrictArgumentsEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewStrictArgumentsEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewStrictArgumentsEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewStrictArgumentsEPNS0_8compiler4NodeES4_:
.LFB22506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	leaq	-448(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-528(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$536, %rsp
	movq	%rdx, -544(%rbp)
	movl	$8, %edx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r12, %rsi
	movq	%r15, %rcx
	movl	$1, %edx
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r13, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r11, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12BIntConstantEi@PLT
	movq	-544(%rbp), %rcx
	leaq	8(%r12), %rsi
	movq	%rbx, %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler25GetArgumentsFrameAndCountENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEE@PLT
	movl	$48, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-504(%rbp), %r9
	movl	$1, %r8d
	movq	%r9, %rsi
	movq	%r9, -552(%rbp)
	call	_ZN2v88internal17CodeStubAssembler39GotoIfFixedArraySizeDoesntFitInNewSpaceEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEiNS1_13ParameterModeE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$149, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	-552(%rbp), %r9
	movq	-536(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -568(%rbp)
	movq	%r9, %rsi
	movq	%r9, -560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-576(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	pushq	$32
	movq	%r12, %rdi
	movq	-560(%rbp), %r9
	movq	-568(%rbp), %r10
	movq	-536(%rbp), %r8
	pushq	$1
	movq	-512(%rbp), %rdx
	movq	%r9, %rcx
	movq	%r10, %rsi
	movq	%r10, -560(%rbp)
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-552(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-560(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	-536(%rbp), %rcx
	movl	$1, %r9d
	leaq	-480(%rbp), %rdi
	movl	$32, (%rsp)
	movq	%r10, %rdx
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	-464(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-544(%rbp), %rax
	movl	$1, %r8d
	movl	$313, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-552(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L90:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22506:
	.size	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewStrictArgumentsEPNS0_8compiler4NodeES4_, .-_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewStrictArgumentsEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS0_8compiler4NodeES4_:
.LFB22507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-800(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-480(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$904, %rsp
	movq	%rsi, -808(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rdx, -872(%rbp)
	movl	$8, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12BIntConstantEi@PLT
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-608(%rbp), %rdi
	movl	$1, %r8d
	movq	%rax, -832(%rbp)
	movq	%rbx, -848(%rbp)
	movq	%rdi, -888(%rbp)
	movq	%r15, -224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-352(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rdi, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-808(%rbp), %rdx
	movq	%r14, %rcx
	leaq	-752(%rbp), %rdi
	leaq	8(%r12), %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler25GetArgumentsFrameAndCountENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSFunctionEEE@PLT
	movq	-752(%rbp), %rcx
	movq	-832(%rbp), %rbx
	movq	%r12, %rdi
	movq	-744(%rbp), %r14
	movq	%rcx, -896(%rbp)
	movq	-736(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rcx, -856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-856(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-880(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L117
	leaq	-656(%rbp), %rax
	movq	%rax, -816(%rbp)
.L92:
	movq	-856(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMinENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movl	$1, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-848(%rbp), %rdx
	movl	$72, %ecx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler39GotoIfFixedArraySizeDoesntFitInNewSpaceEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEiNS1_13ParameterModeE@PLT
	movq	-808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$46, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r14, %rcx
	pushq	$40
	movq	-816(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	movl	$1, %r9d
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	-648(%rbp), %rax
	movl	$32, %edx
	movq	-640(%rbp), %rbx
	movq	-872(%rbp), %rcx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	movq	-656(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rbx, -920(%rbp)
	movq	%rax, -824(%rbp)
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	-824(%rbp), %rsi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	-808(%rbp), %rcx
	movq	-824(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	-824(%rbp), %rsi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	-864(%rbp), %rcx
	movq	-824(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%r9
	popq	%r10
	jne	.L119
.L94:
	movl	$1, %ecx
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$15, %r8d
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-904(%rbp), %rsi
	movl	$15, %r8d
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$2, %edx
	movq	%rax, -928(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	-896(%rbp), %rcx
	movq	-816(%rbp), %rdi
	movl	$1, %r8d
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	leaq	-784(%rbp), %rax
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-816(%rbp), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZNK2v88internal17CodeStubArguments10AtIndexPtrEPNS0_8compiler4NodeENS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-840(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	$0, -696(%rbp)
	movq	$0, -704(%rbp)
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rax, -720(%rbp)
	movq	$0, -712(%rbp)
	subq	%rdx, %rcx
	cmpq	$7, %rcx
	jbe	.L120
	leaq	8(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L97:
	movq	-840(%rbp), %rax
	movq	%rdx, -712(%rbp)
	movl	$24, %edi
	movq	%rcx, -696(%rbp)
	movq	%rax, (%rdx)
	movq	%rcx, -704(%rbp)
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movq	%r12, %xmm0
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	-840(%rbp), %rcx
	movhps	-864(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	leaq	-720(%rbp), %rsi
	movups	%xmm0, (%rax)
	movq	-928(%rbp), %rdx
	movl	$-8, %r9d
	movq	%rcx, 16(%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	pushq	$0
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	movq	%rbx, %rcx
	pushq	$1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13BuildFastLoopERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEEPNS3_4NodeESA_RKSt8functionIFvSA_EEiNS1_13ParameterModeENS1_16IndexAdvanceModeE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, -928(%rbp)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L98
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L98:
	leaq	-688(%rbp), %rax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, -936(%rbp)
	jne	.L121
.L99:
	leaq	-768(%rbp), %rbx
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19IntPtrOrSmiConstantEiNS1_13ParameterModeE@PLT
	movq	-856(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-904(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15TheHoleConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	$0, -680(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -672(%rbp)
	movq	$0, -664(%rbp)
	movq	16(%rax), %rdx
	movq	24(%rax), %rcx
	subq	%rdx, %rcx
	cmpq	$7, %rcx
	jbe	.L122
	leaq	8(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L102:
	movq	%rcx, -664(%rbp)
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rbx, (%rdx)
	movq	%rcx, -672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-824(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-904(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-832(%rbp), %rsi
	movl	$15, %r8d
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$2, %edx
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movl	$48, %edi
	movq	$0, -80(%rbp)
	movq	%rax, -824(%rbp)
	call	_Znwm@PLT
	movq	-864(%rbp), %rcx
	movq	%r13, %r8
	movq	-904(%rbp), %r9
	movq	%rbx, (%rax)
	movq	-936(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rcx, 8(%rax)
	movq	-856(%rbp), %rcx
	movq	%r9, 32(%rax)
	movq	-928(%rbp), %rdx
	movl	$-8, %r9d
	movq	%rcx, 16(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS2_8compiler4NodeES6_EUlS6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movq	%r12, 24(%rax)
	movq	%rcx, %xmm0
	movq	-824(%rbp), %rcx
	movl	$1, 40(%rax)
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsES4_S4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	pushq	$0
	movq	%rax, %xmm2
	pushq	$1
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13BuildFastLoopERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEEPNS3_4NodeESA_RKSt8functionIFvSA_EEiNS1_13ParameterModeENS1_16IndexAdvanceModeE@PLT
	movq	-80(%rbp), %rax
	popq	%rcx
	popq	%rsi
	testq	%rax, %rax
	je	.L103
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L103:
	movq	-920(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-840(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L123
.L104:
	movq	-848(%rbp), %rdx
	movl	$1, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$56, %ecx
	call	_ZN2v88internal17CodeStubAssembler39GotoIfFixedArraySizeDoesntFitInNewSpaceEPNS0_8compiler4NodeEPNS2_18CodeAssemblerLabelEiNS1_13ParameterModeE@PLT
	movq	-808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$147, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	pushq	$40
	movq	%r14, %r9
	movq	%r14, %rcx
	pushq	$1
	movq	-832(%rbp), %r8
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-896(%rbp), %rdx
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler33ConstructParametersObjectFromArgsEPNS0_8compiler4NodeES4_S4_S4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-872(%rbp), %rcx
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$8, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	popq	%rax
	popq	%rdx
	jne	.L124
.L106:
	movq	-808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$147, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	pushq	$40
	movq	-832(%rbp), %rcx
	movl	$1, %r9d
	movq	%rax, %rdx
	movq	-816(%rbp), %rdi
	call	_ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	movq	-640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-872(%rbp), %r14
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$8, %r8d
	movq	%r14, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	-888(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$311, %esi
	movq	-808(%rbp), %rdx
	movl	$1, %r8d
	movq	%r14, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-848(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	leaq	-656(%rbp), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	movq	$40, -656(%rbp)
	movq	%rax, -816(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-656(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8386658438684308340, %rcx
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC2(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-656(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L92
	call	_ZdlPv@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-816(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$29, -656(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-656(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$7881706585693643877, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$1919251557, 24(%rax)
	movb	$115, 28(%rax)
	movups	%xmm0, (%rax)
	movq	-656(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L94
	call	_ZdlPv@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%edx, %edx
	movq	%rax, %rsi
	leaq	-80(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movq	$25, -688(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-688(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8243122671947182689, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movb	$115, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-688(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L99
	call	_ZdlPv@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L124:
	movq	-816(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$29, -656(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-656(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	.LC7(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$5725047568814929255, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$1667590754, 24(%rax)
	movb	$116, 28(%rax)
	movups	%xmm0, (%rax)
	movq	-656(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L106
	call	_ZdlPv@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L123:
	movq	-816(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	$37, -656(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-656(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	.LC5(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC6(%rip), %xmm0
	movl	$1667590754, 32(%rax)
	movb	$116, 36(%rax)
	movups	%xmm0, 16(%rax)
	movq	-656(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L104
	call	_ZdlPv@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	8(%rax), %rcx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	8(%rax), %rcx
	jmp	.L97
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22507:
	.size	_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS0_8compiler4NodeES4_, .-_ZN2v88internal26ArgumentsBuiltinsAssembler26EmitFastNewSloppyArgumentsEPNS0_8compiler4NodeES4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi, @function
_GLOBAL__sub_I__ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi:
.LFB28799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28799:
	.size	_GLOBAL__sub_I__ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi, .-_GLOBAL__sub_I__ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26ArgumentsBuiltinsAssembler23AllocateArgumentsObjectEPNS0_8compiler4NodeES4_S4_NS0_17CodeStubAssembler13ParameterModeEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	7596553777019711049
	.quad	7887324063362737530
	.align 16
.LC1:
	.quad	8079568118343557453
	.quad	8243122671947182689
	.align 16
.LC2:
	.quad	8102098240684640800
	.quad	7954884667833991545
	.align 16
.LC3:
	.quad	2336920844630780230
	.quad	8102082851181064046
	.align 16
.LC4:
	.quad	2336920844630780230
	.quad	8079568118343557485
	.align 16
.LC5:
	.quad	7881706585693646670
	.quad	6001645133637186661
	.align 16
.LC6:
	.quad	8232995117639625811
	.quad	5725047568814929255
	.align 16
.LC7:
	.quad	6001645159441263941
	.quad	8232995117639625811
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
