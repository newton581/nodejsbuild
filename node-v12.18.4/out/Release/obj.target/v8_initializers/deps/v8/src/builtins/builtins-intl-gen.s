	.file	"builtins-intl-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB25896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE(%rip), %r14d
	movq	(%rsi), %rdx
	movq	16(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	16(%rbx), %r13
	movq	%rax, %rsi
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	24(%rbx), %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	(%rbx), %rdi
	movq	16(%rbx), %r14
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	32(%rbx), %rdx
	movq	%r13, %r8
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	8(%rbx), %r15
	movq	16(%rbx), %r8
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	16(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-56(%rbp), %r8
	movq	%r14, %rdx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	(%rbx), %rsi
	addq	$24, %rsp
	popq	%rbx
	movl	$1, %ecx
	popq	%r12
	movl	$1, %edx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	.cfi_endproc
.LFE25896:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB25898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L10
	cmpl	$3, %edx
	je	.L11
	cmpl	$1, %edx
	je	.L17
.L12:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L12
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25898:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEv
	.type	_ZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEv, @function
_ZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEv:
.LFB21915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r14
	leaq	-224(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-352(%rbp), %rbx
	subq	$696, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -664(%rbp)
	call	_ZN2v88internal17CodeStubAssembler24LoadStringLengthAsWord32ENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %rdx
	movq	(%r12), %rsi
	leaq	-576(%rbp), %rax
	movl	$1, %ecx
	movq	%rax, %rdi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal23ToDirectStringAssemblerC1EPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	movq	-648(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal23ToDirectStringAssembler11TryToDirectEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-544(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -712(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27IsOneByteStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24AllocateSeqOneByteStringENS0_8compiler5TNodeINS0_7Uint32TEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler17Uint32GreaterThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22PointerToSeqStringDataEPNS0_8compiler4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	leaq	-640(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r11
	movq	%rax, %rcx
	movq	%rdx, -680(%rbp)
	movl	$5, %edx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	-648(%rbp), %rdi
	movq	%r14, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal23ToDirectStringAssembler15TryToSequentialENS1_17StringPointerKindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-624(%rbp), %r13
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	-672(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal17ExternalReference26intl_to_latin1_lower_tableEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	$0, -584(%rbp)
	movq	$0, -592(%rbp)
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rax, -608(%rbp)
	movq	$0, -600(%rbp)
	subq	%rdx, %rcx
	cmpq	$15, %rcx
	jbe	.L27
	leaq	16(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L20:
	movq	-680(%rbp), %xmm0
	movq	%r13, %xmm1
	movq	%rdx, -600(%rbp)
	movl	$40, %edi
	movq	%rcx, -584(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdx)
	movaps	%xmm0, -736(%rbp)
	movq	%rcx, -592(%rbp)
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm0
	leaq	-96(%rbp), %r10
	movq	-688(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%r10, %r8
	movl	$1, %r9d
	movq	%r12, %rdi
	movups	%xmm0, (%rax)
	movq	%r12, %xmm0
	movq	-672(%rbp), %rdx
	leaq	-608(%rbp), %rsi
	movhps	-704(%rbp), %xmm0
	movq	%rcx, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movups	%xmm0, 16(%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rcx, %xmm0
	movq	-696(%rbp), %rcx
	pushq	$1
	movq	%rax, %xmm2
	pushq	$1
	punpcklqdq	%xmm2, %xmm0
	movq	%r10, -688(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13BuildFastLoopERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEEPNS3_4NodeESA_RKSt8functionIFvSA_EEiNS1_13ParameterModeENS1_16IndexAdvanceModeE@PLT
	movq	-80(%rbp), %rax
	popq	%rcx
	movq	-688(%rbp), %r10
	popq	%rsi
	testq	%rax, %rax
	je	.L21
	movq	%r10, -672(%rbp)
	movq	%r10, %rsi
	movq	%r10, %rdi
	movl	$3, %edx
	call	*%rax
	movq	-672(%rbp), %r10
.L21:
	movq	%r13, %rdi
	movq	%r10, -688(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	leaq	-560(%rbp), %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal17ExternalReference30intl_convert_one_byte_to_lowerEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-672(%rbp), %rdx
	movq	-688(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1800, %eax
	movl	$2, %r8d
	movw	%ax, -96(%rbp)
	movq	-656(%rbp), %rax
	movq	%r10, %rcx
	movq	%rdx, -88(%rbp)
	movl	$1800, %edx
	movw	%dx, -80(%rbp)
	movl	$1800, %edx
	movq	%r10, -656(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	-656(%rbp), %r10
	movl	$184, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	-664(%rbp), %rax
	movl	$1, %r8d
	movq	%r10, %rcx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	-512(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	-528(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-648(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$16, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	16(%rax), %rcx
	jmp	.L20
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21915:
	.size	_ZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEv, .-_ZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-intl-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"StringToLowerCaseIntl"
	.section	.text._ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE:
.LFB21911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$35, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$1056, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L33
.L30:
	movq	%r13, %rdi
	call	_ZN2v88internal30StringToLowerCaseIntlAssembler33GenerateStringToLowerCaseIntlImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L30
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21911:
	.size	_ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"String.prototype.toLowerCase"
	.section	.text._ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv
	.type	_ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv, @function
_ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv:
.LFB21958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler12ToThisStringENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$1056, %edx
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-48(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%rbx, %r9
	movl	$1, %edi
	movq	%rax, %r8
	movl	$1, %ecx
	movq	%r13, -48(%rbp)
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21958:
	.size	_ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv, .-_ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_StringPrototypeToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"StringPrototypeToLowerCaseIntl"
	.section	.text._ZN2v88internal8Builtins39Generate_StringPrototypeToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_StringPrototypeToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_StringPrototypeToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_StringPrototypeToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE:
.LFB21954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$131, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$1054, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L43
.L40:
	movq	%r13, %rdi
	call	_ZN2v88internal39StringPrototypeToLowerCaseIntlAssembler42GenerateStringPrototypeToLowerCaseIntlImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L40
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21954:
	.size	_ZN2v88internal8Builtins39Generate_StringPrototypeToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_StringPrototypeToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21IntlBuiltinsAssembler20AllocateEmptyJSArrayENS0_8compiler5TNodeINS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21IntlBuiltinsAssembler20AllocateEmptyJSArrayENS0_8compiler5TNodeINS0_7ContextEEE
	.type	_ZN2v88internal21IntlBuiltinsAssembler20AllocateEmptyJSArrayENS0_8compiler5TNodeINS0_7ContextEEE, @function
_ZN2v88internal21IntlBuiltinsAssembler20AllocateEmptyJSArrayENS0_8compiler5TNodeINS0_7ContextEEE:
.LFB21960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	pushq	$0
	movq	%rbx, %r8
	movq	%r13, %rcx
	pushq	$0
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%r9d, %r9d
	movl	$2, %esi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21960:
	.size	_ZN2v88internal21IntlBuiltinsAssembler20AllocateEmptyJSArrayENS0_8compiler5TNodeINS0_7ContextEEE, .-_ZN2v88internal21IntlBuiltinsAssembler20AllocateEmptyJSArrayENS0_8compiler5TNodeINS0_7ContextEEE
	.section	.text._ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc
	.type	_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc, @function
_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc:
.LFB21959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$280, %rsp
	movl	%ecx, -308(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	%rbx, %r8
	movl	$1091, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ThrowIfNotInstanceTypeEPNS0_8compiler4NodeES4_NS0_12InstanceTypeEPKc@PLT
	movq	-256(%rbp), %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpl	$182, -308(%rbp)
	je	.L52
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21IntlBuiltinsAssembler20AllocateEmptyJSArrayENS0_8compiler5TNodeINS0_7ContextEEE
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
.L49:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$393, %edx
	leaq	-288(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -80(%rbp)
	movl	$1, %edi
	xorl	%esi, %esi
	leaq	-80(%rbp), %rbx
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rbx
	movq	%r13, %r9
	leaq	-304(%rbp), %rdx
	movl	$1, %ecx
	movq	%rax, -304(%rbp)
	movq	-272(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-320(%rbp), %xmm0
	movq	%rax, %xmm1
	movl	-308(%rbp), %esi
	movl	$2, %r8d
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19EmptyStringConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L49
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21959:
	.size	_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc, .-_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc
	.section	.rodata._ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"ListFormatPrototypeFormat"
	.section	.rodata._ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Intl.ListFormat.prototype.format"
	.section	.text._ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE:
.LFB21965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$191, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$1009, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L58
.L55:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$182, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	.LC6(%rip), %r8
	call	_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L55
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21965:
	.size	_ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_ListFormatPrototypeFormatEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal34ListFormatPrototypeFormatAssembler37GenerateListFormatPrototypeFormatImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34ListFormatPrototypeFormatAssembler37GenerateListFormatPrototypeFormatImplEv
	.type	_ZN2v88internal34ListFormatPrototypeFormatAssembler37GenerateListFormatPrototypeFormatImplEv, @function
_ZN2v88internal34ListFormatPrototypeFormatAssembler37GenerateListFormatPrototypeFormatImplEv:
.LFB21969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$182, %ecx
	popq	%r12
	movq	%rax, %rsi
	leaq	.LC6(%rip), %r8
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc
	.cfi_endproc
.LFE21969:
	.size	_ZN2v88internal34ListFormatPrototypeFormatAssembler37GenerateListFormatPrototypeFormatImplEv, .-_ZN2v88internal34ListFormatPrototypeFormatAssembler37GenerateListFormatPrototypeFormatImplEv
	.section	.rodata._ZN2v88internal8Builtins41Generate_ListFormatPrototypeFormatToPartsEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"ListFormatPrototypeFormatToParts"
	.align 8
.LC8:
	.string	"Intl.ListFormat.prototype.formatToParts"
	.section	.text._ZN2v88internal8Builtins41Generate_ListFormatPrototypeFormatToPartsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_ListFormatPrototypeFormatToPartsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins41Generate_ListFormatPrototypeFormatToPartsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins41Generate_ListFormatPrototypeFormatToPartsEPNS0_8compiler18CodeAssemblerStateE:
.LFB21974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$198, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$1010, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L66
.L63:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$183, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	.LC8(%rip), %r8
	call	_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L63
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21974:
	.size	_ZN2v88internal8Builtins41Generate_ListFormatPrototypeFormatToPartsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins41Generate_ListFormatPrototypeFormatToPartsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal41ListFormatPrototypeFormatToPartsAssembler44GenerateListFormatPrototypeFormatToPartsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal41ListFormatPrototypeFormatToPartsAssembler44GenerateListFormatPrototypeFormatToPartsImplEv
	.type	_ZN2v88internal41ListFormatPrototypeFormatToPartsAssembler44GenerateListFormatPrototypeFormatToPartsImplEv, @function
_ZN2v88internal41ListFormatPrototypeFormatToPartsAssembler44GenerateListFormatPrototypeFormatToPartsImplEv:
.LFB21978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$183, %ecx
	popq	%r12
	movq	%rax, %rsi
	leaq	.LC8(%rip), %r8
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21IntlBuiltinsAssembler16ListFormatCommonENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6Int32TEEENS0_7Runtime10FunctionIdEPKc
	.cfi_endproc
.LFE21978:
	.size	_ZN2v88internal41ListFormatPrototypeFormatToPartsAssembler44GenerateListFormatPrototypeFormatToPartsImplEv, .-_ZN2v88internal41ListFormatPrototypeFormatToPartsAssembler44GenerateListFormatPrototypeFormatToPartsImplEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE:
.LFB28179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28179:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins30Generate_StringToLowerCaseIntlEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE:
	.byte	2
	.byte	3
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
