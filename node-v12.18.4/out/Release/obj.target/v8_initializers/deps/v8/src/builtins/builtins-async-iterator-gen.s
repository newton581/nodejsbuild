	.file	"builtins-async-iterator-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB26062:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26062:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB26072:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26072:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26061:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rsi
	movl	$1800, %ecx
	movl	$32, %edx
	movq	%r8, %rdi
	jmp	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	.cfi_endproc
.LFE26061:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_:
.LFB26071:
	.cfi_startproc
	endbr64
	movq	(%rcx), %rsi
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	.cfi_endproc
.LFE26071:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB26068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L24
	cmpl	$3, %edx
	je	.L25
	cmpl	$1, %edx
	je	.L31
.L26:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26068:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS2_8compiler4NodeES7_S7_NS2_6HandleINS2_6StringEEERKSt8functionIFvS7_S7_PNS5_18CodeAssemblerLabelEEEPKcNSC_4TypeES7_EUlS7_E_E10_M_managerERSt9_Any_dataRKSN_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS2_8compiler4NodeES7_S7_NS2_6HandleINS2_6StringEEERKSt8functionIFvS7_S7_PNS5_18CodeAssemblerLabelEEEPKcNSC_4TypeES7_EUlS7_E_E10_M_managerERSt9_Any_dataRKSN_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS2_8compiler4NodeES7_S7_NS2_6HandleINS2_6StringEEERKSt8functionIFvS7_S7_PNS5_18CodeAssemblerLabelEEEPKcNSC_4TypeES7_EUlS7_E_E10_M_managerERSt9_Any_dataRKSN_St18_Manager_operation:
.LFB26031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L33
	cmpl	$3, %edx
	je	.L34
	cmpl	$1, %edx
	je	.L40
.L35:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L35
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26031:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS2_8compiler4NodeES7_S7_NS2_6HandleINS2_6StringEEERKSt8functionIFvS7_S7_PNS5_18CodeAssemblerLabelEEEPKcNSC_4TypeES7_EUlS7_E_E10_M_managerERSt9_Any_dataRKSN_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS2_8compiler4NodeES7_S7_NS2_6HandleINS2_6StringEEERKSt8functionIFvS7_S7_PNS5_18CodeAssemblerLabelEEEPKcNSC_4TypeES7_EUlS7_E_E10_M_managerERSt9_Any_dataRKSN_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_:
.LFB26067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	(%rbx), %r9
	movq	%r12, %rdi
	movq	8(%rbx), %r13
	movq	%rax, -136(%rbp)
	movq	%r9, -144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %r10
	movl	$350, %edx
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -160(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %r11
	movl	$2, %edi
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	movq	-144(%rbp), %r9
	movq	%r13, %xmm0
	pushq	%r11
	movq	-96(%rbp), %rax
	leaq	-128(%rbp), %r13
	movl	$1, %ecx
	movhps	-136(%rbp), %xmm0
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r11, -152(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	16(%rbx), %r12
	movq	(%rbx), %r9
	movq	%rax, -136(%rbp)
	movq	%r12, %rdi
	movq	%r9, -144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-160(%rbp), %r10
	movl	$495, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	-152(%rbp), %r11
	pushq	%rdi
	movq	%rax, %r8
	movq	%r14, %xmm0
	movq	-96(%rbp), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r15, -128(%rbp)
	pushq	%r11
	movq	-144(%rbp), %r9
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	16(%rbx), %rdi
	addq	$32, %rsp
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26067:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodES4_S4_S4_NS1_6HandleINS1_6StringEEERKSt8functionIFvS4_S4_PNS2_18CodeAssemblerLabelEEEPKcNSC_4TypeES4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodES4_S4_S4_NS1_6HandleINS1_6StringEEERKSt8functionIFvS4_S4_PNS2_18CodeAssemblerLabelEEEPKcNSC_4TypeES4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodES4_S4_S4_NS1_6HandleINS1_6StringEEERKSt8functionIFvS4_S4_PNS2_18CodeAssemblerLabelEEEPKcNSC_4TypeES4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB26030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %r12
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r13, %r9
	movl	$2, %edi
	movq	%rax, %r8
	movq	%rbx, %xmm0
	movl	$1, %ecx
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-120(%rbp), %xmm0
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L48
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26030:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodES4_S4_S4_NS1_6HandleINS1_6StringEEERKSt8functionIFvS4_S4_PNS2_18CodeAssemblerLabelEEEPKcNSC_4TypeES4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodES4_S4_S4_NS1_6HandleINS1_6StringEEERKSt8functionIFvS4_S4_PNS2_18CodeAssemblerLabelEEEPKcNSC_4TypeES4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_, @function
_ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_:
.LFB22065:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$1224, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -1112(%rbp)
	movq	%rcx, -1216(%rbp)
	movq	32(%rbp), %r13
	movq	%r8, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal24PromiseBuiltinsAssembler24AllocateAndInitJSPromiseEPNS0_8compiler4NodeE@PLT
	testq	%r13, %r13
	movq	-1120(%rbp), %r10
	movq	%rax, -1208(%rbp)
	je	.L56
.L50:
	leaq	-1104(%rbp), %rax
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%r10, -1184(%rbp)
	leaq	-368(%rbp), %r15
	leaq	-240(%rbp), %r13
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	24(%rbp), %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-1008(%rbp), %rdx
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1062, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15HasInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEENS0_12InstanceTypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$231, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$62, %esi
	movq	%r12, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-496(%rbp), %rdx
	movq	%rdx, %rcx
	movq	%rax, %rsi
	movq	%rdx, -1160(%rbp)
	movl	$2, %edx
	movq	%rcx, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-112(%rbp), %rdx
	movl	$6, %edi
	movq	-1176(%rbp), %xmm0
	movq	%rdx, %rsi
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rax, %r8
	movhps	-1200(%rbp), %xmm0
	pushq	%rsi
	movq	%r14, %xmm3
	xorl	%esi, %esi
	movaps	%xmm0, -112(%rbp)
	movq	-1112(%rbp), %r9
	movq	%r12, %rdi
	movq	-1136(%rbp), %xmm0
	movq	%rcx, -624(%rbp)
	movq	-480(%rbp), %rax
	leaq	-624(%rbp), %rcx
	movhps	-1144(%rbp), %xmm0
	movq	%rdx, -1136(%rbp)
	movq	%rcx, %rdx
	movaps	%xmm0, -96(%rbp)
	movq	-1168(%rbp), %xmm0
	movq	%rcx, -1144(%rbp)
	movl	$1, %ecx
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, -616(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movl	$24, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1800, %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1184(%rbp), %r10
	movq	%rax, -1168(%rbp)
	cmpq	$0, 16(%r10)
	movq	%rax, -752(%rbp)
	popq	%rax
	popq	%rdx
	je	.L53
	leaq	-752(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -1176(%rbp)
	movq	%rax, %rsi
	call	*24(%r10)
	cmpq	$0, 16(%rbx)
	movq	%rax, %r14
	je	.L52
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1152(%rbp), %rax
	cmpq	$0, 16(%rbx)
	movq	%rax, -624(%rbp)
	movq	-1208(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	-1120(%rbp), %rax
	movq	%rax, -368(%rbp)
	je	.L53
	movq	-1160(%rbp), %rdx
	movq	-1144(%rbp), %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
.L52:
	movq	-1168(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$4, %ebx
	movhps	-1216(%rbp), %xmm0
	movaps	%xmm0, -1200(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1136(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r15, %rdx
	movq	%r12, %rdi
	pushq	%rcx
	movdqa	-1200(%rbp), %xmm0
	movq	%r14, %xmm1
	movl	$1, %ecx
	movq	-1112(%rbp), %r9
	movq	%rax, -368(%rbp)
	movhps	-1168(%rbp), %xmm1
	leaq	-1072(%rbp), %r14
	movq	-224(%rbp), %rax
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1128(%rbp), %rcx
	movq	-1120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	leaq	-880(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1176(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1144(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1160(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1168(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1184(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15IsJSReceiverMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1152(%rbp), %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	leaq	-1088(%rbp), %r11
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%rax, %rbx
	movq	%r11, -1200(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	%r14, %rdi
	movl	$8, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-1184(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1216(%rbp), %r11
	movq	-1176(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -1184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1184(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -1256(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1168(%rbp), %rbx
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$32, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	2368(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-1040(%rbp), %r11
	movl	$710, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -1184(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-1136(%rbp), %rcx
	movq	%rbx, -1168(%rbp)
	movl	$2, %ebx
	movq	%rax, %r8
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-1056(%rbp), %r10
	movq	-1112(%rbp), %r9
	pushq	%rcx
	movhps	-1216(%rbp), %xmm0
	movq	%r10, %rdx
	movl	$1, %ecx
	movq	%rax, -1056(%rbp)
	movq	-1024(%rbp), %rax
	movq	%r10, -1224(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -1048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1128(%rbp), %rcx
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	-1120(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movq	%r12, %rdi
	leaq	3544(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1184(%rbp), %rdi
	movl	$710, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	-1136(%rbp), %rsi
	movq	-1224(%rbp), %r10
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rsi
	movq	-1112(%rbp), %r9
	movq	%r10, %rdx
	xorl	%esi, %esi
	movq	-1168(%rbp), %xmm0
	movq	%rax, -1056(%rbp)
	movq	%r12, %rdi
	movq	-1024(%rbp), %rax
	movq	%r10, -1248(%rbp)
	movhps	-1216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -1048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1128(%rbp), %rcx
	movq	-1120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1216(%rbp), %rsi
	movq	-1200(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$231, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$68, %esi
	movq	%r12, %rdi
	movq	%rax, -1232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1216(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1184(%rbp), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1040(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1168(%rbp), %rbx
	xorl	%esi, %esi
	movq	-1232(%rbp), %xmm0
	movq	-1136(%rbp), %rcx
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-1248(%rbp), %r10
	movhps	-1240(%rbp), %xmm0
	movq	%rbx, -80(%rbp)
	movl	$5, %ebx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rbx
	movq	-1112(%rbp), %r9
	movq	%r10, %rdx
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	movq	-1216(%rbp), %xmm0
	movq	%rax, -1056(%rbp)
	movq	-1024(%rbp), %rax
	movhps	-1224(%rbp), %xmm0
	movq	%r10, -1168(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -1048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1128(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1160(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9IsBooleanENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -1160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-1184(%rbp), %rdi
	movl	$92, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -112(%rbp)
	movl	$1, %ebx
	xorl	%esi, %esi
	movq	-1136(%rbp), %rcx
	pushq	%rbx
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-1168(%rbp), %r10
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-1112(%rbp), %r9
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -1056(%rbp)
	movq	-1024(%rbp), %rax
	movq	%r10, %rdx
	movq	%rax, -1048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1200(%rbp), %rbx
	movq	%rax, -1184(%rbp)
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r14
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%rbx, %rdi
	movl	$2, %ebx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1160(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1144(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1176(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1256(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1152(%rbp), %rsi
	movl	$212, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$510, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1136(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	-1152(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rdi
	pushq	%rcx
	movq	-224(%rbp), %rax
	movl	$1, %ecx
	movq	-1144(%rbp), %xmm0
	movq	%r14, -368(%rbp)
	movq	%rax, -360(%rbp)
	movhps	-1168(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1128(%rbp), %rcx
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	-1120(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler15GotoIfExceptionEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPNS1_21CodeAssemblerVariableE@PLT
	movq	-1184(%rbp), %r10
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal22AsyncBuiltinsAssembler19CreateUnwrapClosureEPNS0_8compiler4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1152(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$504, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	-1136(%rbp), %rcx
	movhps	-1144(%rbp), %xmm0
	movq	%rax, %r8
	movq	%r12, %rdi
	movl	$4, %ebx
	movaps	%xmm0, -112(%rbp)
	movq	-1152(%rbp), %xmm0
	movq	-1112(%rbp), %r9
	pushq	%rbx
	movq	-224(%rbp), %rax
	pushq	%rcx
	movhps	-1208(%rbp), %xmm0
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r14, -368(%rbp)
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1128(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$494, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -96(%rbp)
	movl	$3, %ebx
	xorl	%esi, %esi
	movq	-1136(%rbp), %rcx
	pushq	%rbx
	movq	%rax, %r8
	movq	%r15, %rdx
	movq	%r14, -368(%rbp)
	movq	-1208(%rbp), %r14
	movq	%r12, %rdi
	movq	-1112(%rbp), %r9
	movq	-224(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movq	%r14, %xmm0
	movhps	-1144(%rbp), %xmm0
	movq	%rax, -360(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1128(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	-1120(%rbp), %r10
	movq	%rax, %r13
	jmp	.L50
.L53:
	call	_ZSt25__throw_bad_function_callv@PLT
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22065:
	.size	_ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_, .-_ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_
	.section	.rodata._ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"[Async-from-Sync Iterator].prototype.next"
	.section	.text._ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv
	.type	_ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv, @function
_ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv:
.LFB22093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-96(%rbp), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	subq	$8, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%rax, %rsi
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	pushq	$0
	movq	%r13, %rdx
	movq	%rax, %xmm2
	movq	%r12, %xmm1
	pushq	$0
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rax
	movq	%r13, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movq	$0, -112(%rbp)
	pushq	%rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	movq	%r14, %rcx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_
	movq	-80(%rbp), %rax
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L59
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L59:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L58
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L58:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22093:
	.size	_ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv, .-_ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv
	.section	.rodata._ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../deps/v8/src/builtins/builtins-async-iterator-gen.cc"
	.align 8
.LC3:
	.string	"AsyncFromSyncIteratorPrototypeNext"
	.section	.text._ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE:
.LFB22089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$239, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$691, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L74
.L71:
	movq	%r13, %rdi
	call	_ZN2v88internal43AsyncFromSyncIteratorPrototypeNextAssembler46GenerateAsyncFromSyncIteratorPrototypeNextImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L71
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22089:
	.size	_ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"[Async-from-Sync Iterator].prototype.return"
	.section	.text._ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv
	.type	_ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv, @function
_ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv:
.LFB22106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$24, %edi
	movq	$0, -112(%rbp)
	movq	%rax, -136(%rbp)
	call	_Znwm@PLT
	movq	%r13, %xmm2
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	-136(%rbp), %xmm0
	movq	%r12, 16(%rax)
	movq	%rax, -128(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	movq	%rax, %rbx
	call	_Znwm@PLT
	subq	$8, %rsp
	movq	%r15, %r9
	movq	%r13, %rcx
	movq	-136(%rbp), %xmm1
	addq	$3200, %rbx
	movq	%r12, 16(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS2_8compiler4NodeES7_S7_NS2_6HandleINS2_6StringEEERKSt8functionIFvS7_S7_PNS5_18CodeAssemblerLabelEEEPKcNSC_4TypeES7_EUlS7_E_E10_M_managerERSt9_Any_dataRKSN_St18_Manager_operation(%rip), %rdx
	movq	%rbx, %xmm4
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rbx
	movq	%r12, %rdi
	movdqa	%xmm1, %xmm0
	movq	%rbx, %r8
	movq	%xmm1, %rsi
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodES4_S4_S4_NS1_6HandleINS1_6StringEEERKSt8functionIFvS4_S4_PNS2_18CodeAssemblerLabelEEEPKcNSC_4TypeES4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	movq	%rax, %xmm5
	leaq	.LC4(%rip), %rax
	pushq	$0
	pushq	$0
	punpcklqdq	%xmm5, %xmm0
	pushq	%rax
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_
	movq	-80(%rbp), %rax
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L77
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L77:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L76
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L76:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22106:
	.size	_ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv, .-_ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv
	.section	.rodata._ZN2v88internal8Builtins45Generate_AsyncFromSyncIteratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"AsyncFromSyncIteratorPrototypeReturn"
	.section	.text._ZN2v88internal8Builtins45Generate_AsyncFromSyncIteratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins45Generate_AsyncFromSyncIteratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins45Generate_AsyncFromSyncIteratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins45Generate_AsyncFromSyncIteratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE:
.LFB22102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$254, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$693, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L92
.L89:
	movq	%r13, %rdi
	call	_ZN2v88internal45AsyncFromSyncIteratorPrototypeReturnAssembler48GenerateAsyncFromSyncIteratorPrototypeReturnImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L89
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22102:
	.size	_ZN2v88internal8Builtins45Generate_AsyncFromSyncIteratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins45Generate_AsyncFromSyncIteratorPrototypeReturnEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"[Async-from-Sync Iterator].prototype.throw"
	.section	.text._ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv
	.type	_ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv, @function
_ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv:
.LFB22116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlPNS2_8compiler4NodeES6_PNS4_18CodeAssemblerLabelEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	%r12, -128(%rbp)
	movq	%rax, %r13
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeES4_PNS2_18CodeAssemblerLabelEEZNS1_44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEvEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7factoryEv@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	%r13, %xmm0
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, 16(%rax)
	subq	$8, %rsp
	leaq	-96(%rbp), %r8
	movq	%r12, %rdi
	addq	$3432, %rbx
	movq	%rax, -96(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS2_8compiler4NodeES7_S7_NS2_6HandleINS2_6StringEEERKSt8functionIFvS7_S7_PNS5_18CodeAssemblerLabelEEEPKcNSC_4TypeES7_EUlS7_E_E10_M_managerERSt9_Any_dataRKSN_St18_Manager_operation(%rip), %rdx
	movq	%rbx, %xmm2
	leaq	-128(%rbp), %rbx
	movq	%r8, -136(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, %r9
	movups	%xmm0, (%rax)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodES4_S4_S4_NS1_6HandleINS1_6StringEEERKSt8functionIFvS4_S4_PNS2_18CodeAssemblerLabelEEEPKcNSC_4TypeES4_EUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rdx, %xmm0
	movq	%r15, %rdx
	movq	%rax, %xmm3
	leaq	.LC6(%rip), %rax
	pushq	%r14
	pushq	$1
	punpcklqdq	%xmm3, %xmm0
	pushq	%rax
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_130AsyncFromSyncBuiltinsAssembler36Generate_AsyncFromSyncIteratorMethodEPNS0_8compiler4NodeES5_S5_RKSt8functionIFS5_S5_EERKS6_IFvS5_S5_PNS3_18CodeAssemblerLabelEEEPKcNSB_4TypeES5_
	movq	-80(%rbp), %rax
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L95
	movq	-136(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L95:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L94
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L94:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22116:
	.size	_ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv, .-_ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv
	.section	.rodata._ZN2v88internal8Builtins44Generate_AsyncFromSyncIteratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"AsyncFromSyncIteratorPrototypeThrow"
	.section	.text._ZN2v88internal8Builtins44Generate_AsyncFromSyncIteratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins44Generate_AsyncFromSyncIteratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins44Generate_AsyncFromSyncIteratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins44Generate_AsyncFromSyncIteratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$281, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$692, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L110
.L107:
	movq	%r13, %rdi
	call	_ZN2v88internal44AsyncFromSyncIteratorPrototypeThrowAssembler47GenerateAsyncFromSyncIteratorPrototypeThrowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L107
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22112:
	.size	_ZN2v88internal8Builtins44Generate_AsyncFromSyncIteratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins44Generate_AsyncFromSyncIteratorPrototypeThrowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE:
.LFB28443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28443:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins43Generate_AsyncFromSyncIteratorPrototypeNextEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
