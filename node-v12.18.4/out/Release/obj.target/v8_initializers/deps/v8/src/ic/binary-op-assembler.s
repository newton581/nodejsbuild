	.file	"binary-op-assembler.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB15301:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15301:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB15305:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15305:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB15312:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15312:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB15316:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L25
	cmpl	$3, %edx
	je	.L26
	cmpl	$1, %edx
	je	.L30
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15316:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB15320:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L32
	cmpl	$3, %edx
	je	.L33
	cmpl	$1, %edx
	je	.L37
.L33:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15320:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB15324:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L39
	cmpl	$3, %edx
	je	.L40
	cmpl	$1, %edx
	je	.L44
.L40:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15324:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation:
.LFB15328:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L46
	cmpl	$3, %edx
	je	.L47
	cmpl	$1, %edx
	je	.L51
.L47:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15328:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB15332:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L53
	cmpl	$3, %edx
	je	.L54
	cmpl	$1, %edx
	je	.L58
.L54:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15332:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_:
.LFB15304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13CodeAssembler10Float64SubENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15304:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_:
.LFB15315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13CodeAssembler10Float64MulENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15315:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_:
.LFB15323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13CodeAssembler10Float64DivENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15323:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_:
.LFB15331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13CodeAssembler10Float64ModENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15331:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_:
.LFB15311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	movq	(%rcx), %r14
	call	_ZN2v88internal17CodeStubAssembler6SmiMulENS0_8compiler5TNodeINS0_3SmiEEES5_@PLT
	movq	(%rbx), %r13
	movq	%rax, %rsi
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movabsq	$30064771072, %rcx
	movabsq	$4294967296, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17SelectSmiConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEENS0_3SmiES6_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15311:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_:
.LFB15327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdx), %rdx
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	movq	(%rcx), %r14
	call	_ZN2v88internal17CodeStubAssembler6SmiModENS0_8compiler5TNodeINS0_3SmiEEES5_@PLT
	movq	(%rbx), %r13
	movq	%rax, %rsi
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movabsq	$30064771072, %rcx
	movabsq	$4294967296, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17SelectSmiConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEENS0_3SmiES6_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15327:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_
	.section	.text._ZZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0, @function
_ZZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0:
.LFB17263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$8, %edx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-336(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%rcx, -352(%rbp)
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movq	%r8, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movzbl	0(%r13), %r8d
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	-192(%rbp), %r13
	xorl	$1, %r8d
	movzbl	%r8b, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	(%rbx), %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	-352(%rbp), %r9
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler9TrySmiDivENS0_8compiler5TNodeINS0_3SmiEEES5_PNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-344(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-344(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-352(%rbp), %r9
	movq	(%rbx), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-352(%rbp), %r8
	movq	-344(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64DivENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$312, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17263:
	.size	_ZZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0, .-_ZZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_:
.LFB15319:
	.cfi_startproc
	endbr64
	movq	%rsi, %r9
	movq	(%rcx), %r8
	movq	(%rdx), %rcx
	leaq	8(%rdi), %rsi
	movq	(%r9), %rdx
	jmp	_ZZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0
	.cfi_endproc
.LFE15319:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_
	.section	.text._ZZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0, @function
_ZZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0:
.LFB17264:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%rcx, -352(%rbp)
	movq	(%rdi), %rsi
	xorl	%ecx, %ecx
	movq	%r8, -344(%rbp)
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	(%rbx), %rsi
	movl	$8, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movzbl	(%r14), %r8d
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	leaq	-192(%rbp), %r14
	xorl	%edx, %edx
	xorl	$1, %r8d
	movq	%r14, %rdi
	movzbl	%r8b, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	-352(%rbp), %r9
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler9TrySmiSubENS0_8compiler5TNodeINS0_3SmiEEES5_PNS2_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-344(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movl	$7, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-344(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-352(%rbp), %r9
	movq	(%rbx), %r8
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-352(%rbp), %r8
	movq	-344(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64SubENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$312, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L83:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17264:
	.size	_ZZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0, .-_ZZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_:
.LFB15300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	(%rcx), %r8
	leaq	8(%rdi), %rsi
	movq	(%rdx), %rcx
	movq	(%r9), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_bENKUlS4_S4_PNS2_21CodeAssemblerVariableEE_clES4_S4_S6_.isra.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15300:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_
	.section	.text._ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.type	_ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, @function
_ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b:
.LFB13299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-864(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2136, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %ebx
	movq	%r9, -2160(%rbp)
	movq	%rsi, -2072(%rbp)
	movq	%rdi, %rsi
	movq	%r8, -2152(%rbp)
	movl	$1, %r8d
	movl	%ebx, -2112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1760(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -2120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1632(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -2096(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1504(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1376(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -2144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1248(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -2080(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1120(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -2040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -2128(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2000(%rbp), %rax
	movl	$13, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2048(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1984(%rbp), %rax
	movl	$13, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2056(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1968(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2016(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1952(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2024(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	$1, %ebx
	movq	%r13, -2168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movzbl	%bl, %ebx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-736(%rbp), %rax
	movl	%ebx, %r8d
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14TaggedIsNotSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-2136(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	movq	%rax, -2088(%rbp)
	jne	.L97
.L87:
	leaq	-352(%rbp), %rax
	cmpb	$0, -2112(%rbp)
	leaq	-224(%rbp), %r13
	movq	%rax, -2008(%rbp)
	je	.L98
.L89:
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L99
.L90:
	movl	%ebx, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9TrySmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_PNS2_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-2016(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2024(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-2056(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, -2112(%rbp)
	je	.L100
.L92:
	movq	%r14, %xmm2
	movq	%r15, %xmm1
	movq	%r15, %rsi
	movq	%r12, %rdi
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -2112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-2056(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$7, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-2016(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rdi
	movq	%rbx, -2056(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64AddENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	-2024(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-608(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r10, -2176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movl	$67, %edx
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-2176(%rbp), %r10
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-2176(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-2032(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2096(%rbp), %rcx
	movq	-2032(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-2008(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20IsStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-2008(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler20IsBigIntInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2144(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsBigIntENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2128(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler20IsStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r15, -2144(%rbp)
	movq	%rax, %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r15
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-2016(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$706, %edx
	leaq	-1920(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	-2088(%rbp), %rsi
	movdqa	-2112(%rbp), %xmm1
	pushq	%rdi
	movq	-2072(%rbp), %r9
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	movq	-1904(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-1936(%rbp), %rdx
	movaps	%xmm1, -96(%rbp)
	movq	%r15, -1936(%rbp)
	movq	%rax, -1928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-2024(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2176(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movl	$67, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-2032(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2144(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-2008(%rbp), %rdi
	movl	$788, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-2088(%rbp), %rsi
	movq	%rbx, %rdx
	movl	$2, %edi
	pushq	%rdi
	movdqa	-2112(%rbp), %xmm1
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rsi
	movq	-2072(%rbp), %r9
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	-336(%rbp), %rax
	movaps	%xmm1, -96(%rbp)
	movq	%r15, -480(%rbp)
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-2024(%rbp), %rbx
	addq	$32, %rsp
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rdi
	movq	%rbx, -2024(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$32, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-2016(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$127, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-2152(%rbp), %rcx
	movq	-2160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-2072(%rbp), %rsi
	movl	$183, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$15, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$127, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%rbx, %rdi
	movq	%rbx, -2016(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2080(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rbx, -2080(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movl	$2, %ebx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$434, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-2088(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movdqa	-2112(%rbp), %xmm1
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-2072(%rbp), %r9
	movq	-2008(%rbp), %rdx
	movq	-208(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm1, -96(%rbp)
	movq	%r15, -352(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-2024(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2040(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-2016(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-2152(%rbp), %rcx
	movq	-2160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-2136(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2168(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-2056(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-2048(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-2128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movl	$26989, %edx
	movq	%r12, %rdi
	leaq	-80(%rbp), %r13
	movq	%rax, %rsi
	movabsq	$5989914399755954284, %rcx
	movq	%r13, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movw	%dx, -72(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L87
	call	_ZdlPv@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L99:
	movq	-2088(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -2176(%rbp)
	movq	$21, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-2088(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$1869182049, 16(%rax)
	movb	$110, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-2176(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L90
	call	_ZdlPv@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-2008(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2096(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2056(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-2008(%rbp), %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2096(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-2056(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L92
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13299:
	.size	_ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, .-_ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.section	.text._ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb
	.type	_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb, @function
_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb:
.LFB13300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-864(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	subq	$1960, %rsp
	movq	24(%rbp), %rax
	movq	%r9, -1992(%rbp)
	movl	40(%rbp), %r14d
	movq	16(%rbp), %r13
	movq	%rsi, -1968(%rbp)
	movq	%rdi, %rsi
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movq	%r8, -1984(%rbp)
	movl	$1, %r8d
	movq	%rax, -1976(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1960(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1632(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1504(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1376(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1920(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1248(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1120(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-992(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -1912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1824(%rbp), %rax
	movl	$13, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1952(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1808(%rbp), %rax
	movl	$13, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1928(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1792(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1864(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-1776(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1896(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r15, -2000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	%r14d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-736(%rbp), %rax
	xorl	$1, %r8d
	movq	%r12, %rsi
	movzbl	%r8b, %r8d
	movq	%rax, %rdi
	movq	%rax, -1936(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler14TaggedIsNotSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1936(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L124
.L103:
	testb	%r14b, %r14b
	je	.L125
.L105:
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L126
.L106:
	movq	-1856(%rbp), %rax
	cmpq	$0, 16(%r13)
	movq	%rbx, -608(%rbp)
	movq	%rax, -1832(%rbp)
	movq	-1864(%rbp), %rax
	movq	%rax, -480(%rbp)
	je	.L112
	leaq	-480(%rbp), %rax
	leaq	-608(%rbp), %r15
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%r15, %rdx
	leaq	-1832(%rbp), %rsi
	movq	%rax, -1904(%rbp)
	call	*24(%r13)
	movq	-1896(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1936(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L127
.L109:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1944(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	testb	%r14b, %r14b
	je	.L128
.L111:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1952(%rbp), %r13
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1928(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1960(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1864(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1928(%rbp), %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, -352(%rbp)
	movq	%rax, -224(%rbp)
	movq	-1976(%rbp), %rax
	cmpq	$0, 16(%rax)
	je	.L112
	leaq	-224(%rbp), %r14
	leaq	-352(%rbp), %r13
	movq	%rax, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	*24(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	-1896(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1944(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1904(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1976(%rbp)
	call	_ZN2v88internal17CodeStubAssembler20IsBigIntInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1976(%rbp), %r8
	movl	$67, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1872(%rbp), %rcx
	movq	-1904(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1864(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1920(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1864(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1872(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsBigIntENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1872(%rbp), %rcx
	movq	-1912(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler20IsBigIntInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	-1912(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movl	$67, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17InstanceTypeEqualENS0_8compiler11SloppyTNodeINS0_6Int32TEEEi@PLT
	movq	-1872(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1864(%rbp), %r15
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	32(%rbp), %edi
	testl	%edi, %edi
	je	.L129
	movq	-1856(%rbp), %xmm1
	movq	%rbx, %xmm2
	movl	32(%rbp), %esi
	movq	%r12, %rdi
	leaq	-96(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -1856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%rbx, %rcx
	movl	$23, %esi
	movq	%r12, %rdi
	movdqa	-1856(%rbp), %xmm1
	movq	-1968(%rbp), %rdx
	movl	$3, %r8d
	movq	%rax, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-1896(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$127, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-1864(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1880(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpl	$3, 32(%rbp)
	je	.L115
	jg	.L116
	cmpl	$1, 32(%rbp)
	je	.L117
	cmpl	$2, 32(%rbp)
	jne	.L114
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$436, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-1856(%rbp), %xmm5
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movaps	%xmm5, -96(%rbp)
	movq	%rax, -344(%rbp)
.L123:
	movl	$2, %edi
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdx
	pushq	%rdi
	movq	-1968(%rbp), %r9
	movq	%r12, %rdi
	pushq	%rbx
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1896(%rbp), %rbx
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1888(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1864(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1984(%rbp), %rcx
	movq	-1992(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2000(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1928(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1952(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-1912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1944(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1960(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	cmpl	$4, 32(%rbp)
	jne	.L114
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$438, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-1856(%rbp), %xmm7
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -344(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-1856(%rbp), %xmm0
	movq	%rbx, %xmm3
	movq	%r12, %rdi
	movl	$2, %ebx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -1856(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$789, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rcx
	pushq	%rbx
	movq	%r13, %rdx
	pushq	%rcx
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movdqa	-1856(%rbp), %xmm0
	movq	-1968(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1896(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$127, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1880(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	popq	%rcx
	popq	%rsi
.L114:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$435, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-1856(%rbp), %xmm4
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movaps	%xmm4, -96(%rbp)
	movq	%rax, -344(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %rcx
	xorl	%edx, %edx
	movq	$21, -224(%rbp)
	leaq	-224(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -1904(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$1869182049, 16(%rax)
	movb	$110, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-1904(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L106
	call	_ZdlPv@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %r15
	movb	$0, -70(%rbp)
	movabsq	$5989914399755954284, %rax
	movl	$26989, %r9d
	movq	%r15, -96(%rbp)
	movq	%rax, -80(%rbp)
	movw	%r9w, -72(%rbp)
	movq	$10, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L103
	call	_ZdlPv@PLT
	testb	%r14b, %r14b
	jne	.L105
.L125:
	leaq	-352(%rbp), %r15
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-224(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r9, %rdi
	movq	%r12, %rsi
	movq	%r9, -1904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-1904(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1904(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1920(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12SmiToFloat64ENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1928(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1904(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %r13
	movb	$0, -66(%rbp)
	movabsq	$7935469438780008556, %rax
	movl	$26989, %r8d
	movq	%r13, -96(%rbp)
	movq	%rax, -80(%rbp)
	movl	$1394635887, -72(%rbp)
	movw	%r8w, -68(%rbp)
	movq	$14, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L109
	call	_ZdlPv@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	-352(%rbp), %r13
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-224(%rbp), %r14
	movq	%r13, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12IsHeapNumberENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1920(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-1928(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-1960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$437, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-1856(%rbp), %xmm6
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -352(%rbp)
	movq	-208(%rbp), %rax
	movaps	%xmm6, -96(%rbp)
	movq	%rax, -344(%rbp)
	jmp	.L123
.L112:
	call	_ZSt25__throw_bad_function_callv@PLT
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13300:
	.size	_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb, .-_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb
	.section	.text._ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.type	_ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, @function
_ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b:
.LFB13301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_(%rip), %r10
	movq	%r10, %xmm1
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_SubtractWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_(%rip), %r10
	movq	%r10, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	subq	$88, %rsp
	movl	16(%rbp), %eax
	movq	%fs:40, %r11
	movq	%r11, -40(%rbp)
	xorl	%r11d, %r11d
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %r11
	movq	%rdi, -80(%rbp)
	movb	%al, -104(%rbp)
	movq	%r11, %xmm0
	movzbl	%al, %eax
	pushq	%rax
	punpcklqdq	%xmm1, %xmm0
	pushq	$1
	pushq	%r12
	pushq	%r13
	movaps	%xmm0, -64(%rbp)
	movq	%r14, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rdi, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb
	addq	$32, %rsp
	movq	%rax, %r14
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L132
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L132:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L131
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L131:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	leaq	-24(%rbp), %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L142:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13301:
	.size	_ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, .-_ZN2v88internal17BinaryOpAssembler29Generate_SubtractWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.section	.text._ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.type	_ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, @function
_ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b:
.LFB13313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %r10
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %r11
	movq	%r10, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_(%rip), %rax
	movq	%rdi, -80(%rbp)
	movq	%rax, %xmm1
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler29Generate_MultiplyWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_(%rip), %rax
	movq	%rdi, -112(%rbp)
	movq	%rax, %xmm2
	movzbl	16(%rbp), %eax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	%r11, %xmm0
	pushq	%rax
	punpcklqdq	%xmm2, %xmm0
	pushq	$2
	pushq	%r12
	pushq	%r13
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb
	addq	$32, %rsp
	movq	%rax, %r14
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L144
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L144:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L143
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L143:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	leaq	-24(%rbp), %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L154:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13313:
	.size	_ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, .-_ZN2v88internal17BinaryOpAssembler29Generate_MultiplyWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.section	.text._ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.type	_ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, @function
_ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b:
.LFB13316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_(%rip), %r10
	movq	%r10, %xmm1
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler27Generate_DivideWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_(%rip), %r10
	movq	%r10, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	subq	$88, %rsp
	movl	16(%rbp), %eax
	movq	%fs:40, %r11
	movq	%r11, -40(%rbp)
	xorl	%r11d, %r11d
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %r11
	movq	%rdi, -80(%rbp)
	movb	%al, -104(%rbp)
	movq	%r11, %xmm0
	movzbl	%al, %eax
	pushq	%rax
	punpcklqdq	%xmm1, %xmm0
	pushq	$3
	pushq	%r12
	pushq	%r13
	movaps	%xmm0, -64(%rbp)
	movq	%r14, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rdi, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb
	addq	$32, %rsp
	movq	%rax, %r14
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L156
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L156:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L155
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L155:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	leaq	-24(%rbp), %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L166:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13316:
	.size	_ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, .-_ZN2v88internal17BinaryOpAssembler27Generate_DivideWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.section	.text._ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.type	_ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, @function
_ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b:
.LFB13319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_E0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %r10
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS2_8compiler4NodeES6_S6_S6_S6_bEUlS6_S6_PNS4_21CodeAssemblerVariableEE_E10_M_managerERSt9_Any_dataRKSB_St18_Manager_operation(%rip), %r11
	movq	%r10, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_EZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_E0_E9_M_invokeERKSt9_Any_dataOS4_SC_(%rip), %rax
	movq	%rdi, -80(%rbp)
	movq	%rax, %xmm1
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_S4_PNS2_21CodeAssemblerVariableEEZNS1_17BinaryOpAssembler28Generate_ModulusWithFeedbackES4_S4_S4_S4_S4_bEUlS4_S4_S6_E_E9_M_invokeERKSt9_Any_dataOS4_SE_OS6_(%rip), %rax
	movq	%rdi, -112(%rbp)
	movq	%rax, %xmm2
	movzbl	16(%rbp), %eax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	%r11, %xmm0
	pushq	%rax
	punpcklqdq	%xmm2, %xmm0
	pushq	$4
	pushq	%r12
	pushq	%r13
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal17BinaryOpAssembler36Generate_BinaryOperationWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_RKSt8functionIFS4_S4_S4_PNS2_21CodeAssemblerVariableEEERKS5_IFS4_S4_S4_EENS0_9OperationEb
	addq	$32, %rsp
	movq	%rax, %r14
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L168
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L168:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L167
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L167:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	leaq	-24(%rbp), %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13319:
	.size	_ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, .-_ZN2v88internal17BinaryOpAssembler28Generate_ModulusWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.section	.text._ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.type	_ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, @function
_ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b:
.LFB13322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movl	$127, %esi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler14UpdateFeedbackEPNS0_8compiler4NodeES4_S4_@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$439, %edx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	movq	%rbx, %r9
	leaq	-112(%rbp), %rdx
	movl	$2, %edi
	movq	%rax, %r8
	movdqa	-128(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L182
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13322:
	.size	_ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, .-_ZN2v88internal17BinaryOpAssembler33Generate_ExponentiateWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, @function
_GLOBAL__sub_I__ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b:
.LFB17198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17198:
	.size	_GLOBAL__sub_I__ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b, .-_GLOBAL__sub_I__ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17BinaryOpAssembler24Generate_AddWithFeedbackEPNS0_8compiler4NodeES4_S4_S4_S4_b
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	2336649604460864880
	.quad	8243118315559021939
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
